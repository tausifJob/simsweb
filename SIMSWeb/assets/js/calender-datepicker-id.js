﻿/// <reference path="core.js" />

$(document).ready(function () {  //This function for Date Picker
    //for common & Incidence module
    $("#date_schedule1, #date_schedule2, #con_serve_date, #incidence_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });


    //for Assignment & License module
    $("#start_date, #submisssion_date, #frzdate,  #txt_Due_Date, #school_lic_expiry_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for  Fleet Module
    $("#startdate, #end_date_c, #start_dt_forupdate_trans, #from_dt_cancel_trans, #end_dt_update_trans, #dob, #service_next_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });


    //for common & Incidence module
    $("#date_schedule3, #date_schedule4, #date_schedule5").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for  Fleet Module
    $("#startdate, #start_date, #end_date, #end_date_c, #start_dt_forupdate_trans, #from_dt_cancel_trans, #end_dt_update_trans, #dob, #service_next_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Assignment & License module
    $("#submisssion_date, #frzdate, #subscription_date,  #expiry_date, #agree_date, #txt_Due_Date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Setup module
    $("#ex_date, #alert_date, #to_date, #alb_edit_pub_date, #alb_edit_exp_date, #alb_crt_pub_date, #alb_crt_exp_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });
    //for Collaboration module
    $("#dp_expirydate, #dp_publishdate, #expirydate, #fromdate, #todate").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for School Setup module
    $("#dp_endDate, #cal_end_date, #cal_start_date, #cex_from_date, #cex_to_date, #term_start_date, #term_end_date, #dp_visa_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for School Setup -> section term module
    $("#term_1_start_dt, #ay_dp_start_date, #ay_dp_endDate, #term_2_start_dt, #term_3_start_dt, #term_4_start_dt, #term_5_start_dt, #term_6_start_dt,#term_1_end_dt, #term_2_end_dt, #term_3_end_dt, #term_4_end_dt, #term_5_end_dt, #term_6_end_dt").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Library module
    $("#lib_entry_date,#renew_date,#teacher_renew_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Payroll module
    $("#f_date, #effective_up_date, #effective_date,#insu_exp_id, #insurance_date, #reg_date, #date_if_purchase, #dob, #sims_student_visa_expiry_date, #lic_expiry, #td_visa_issue_date, #dept_expiry_date, #dp_visa_date, #lic_issue_date, #sims_student_national_id_issue_date, #sims_student_national_id_expiry_date, #vehicle_exp_from_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });


    //for Assignment Fee Module
    $("#cc_startdate, #cc_end_date, #cc_canceldate, #canceldate, #con_from_date, #st_enddate").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });


    //for Assignment Security Module
    $("#ResetPassword1").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Student Management Module
    $("#expected_date1, #std_admi_fees, #adm_fees_cheque_date,#txt_comm_date,#admission_start_date,#issue_date, #dp_comm_date, #dp_visa_issue_date,#dp_visa_expiry_date,#dp_national_issue_date,#dp_national_expiry_date,#dp_sibling_dob,#dp_cur_school_from_date,#dp_cur_school_to_date,#dp_health_issue_date,#dp_health_exp_date,#End_Date,#end_date,#from_date,#From_Date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Student Management Module
    $("#adm_dp_admdate, #adm_dp_joinDate, #adm_dp_comm_date, #adm_dp_birth_date, #adm_dp_pass_issue_date, #adm_dp_passexpdate, #adm_dp_visa_issue_date, #adm_dp_visa_expiry_date, #adm_dp_national_issue_date, #adm_dp_national_expiry_date, #adm_dp_sibling_dob, #adm_dp_cur_school_from_date, #adm_dp_cur_school_to_date, #adm_dp_health_issue_date, #adm_dp_health_exp_date,#joineddate,#issuedate,#dp_text6,#dp_text7,#dp_text8,#dp_text9,#dp_text10,#dp_text11,#dp_text12,#dp_text13,#dp_text14,#dp_text15,#dp_text16,#dp_text17,#dp_text18,#dp_text19,#dp_text20,#dp_text21,#dp_text22,#dp_text23,#dp_text24,#cancel_date,#tc_approve_date,#date_of_admission,#pprofile_mother_expry_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Student Management Admission Module
    $("#father_passport_expiry, #father_passport_issue, #pp_issue_date, #parent_p_expirt, #pprofile_issue_date, #pp_mother_national_id_issue, #guardian_national_id_expire, #guardian_national_id_issue, #guardian_passport_expiry_date, #guardian_passport_issue_date, #pp_mother_national_id_expiry, #pprofile_issue_date, #parent_p_issue, #pp_expiry_date, #dp_admdate, #cd, #cattc_cancel_date, #TC_approval_date, #cTCa_dob, #date_of_leaving, #date_of_appn_certificate, #admns_dp_visa_issue_date, #attendance_date, #attendance_end_date, #caf_dp_admdate, #caf_cancel_date, #caa_dp_entry_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Student Management Admission cancel Module
    $("#fee_approval_date, #To_approval_date, #cmb_Start_date_1, #dp_birth_date, #dp_visa_issue_date, #dp_visa_expiry_date, #dp_sibling_dob, #dp_cur_school_from_date, #dp_cur_school_to_date, #dp_health_exp_date, #dp_health_issue_date, #dp_national_expiry_date, #creation_start_date, #creation_end_date, #Weekly_assess_start, #Weekly_assess_end, #uss_effect_from, #Subsexpdate, #Subsexpdate_join, #Subsexpdate_doc, #student_passport_expiry_date, #student_passport_issue_date, #dp_visa_exp_date, #dp_visa_issue_date, #student_national_id_issue, #student_national_id_expiry, #father_passport_expiry_date, #father_passport_issue_date, #father_national_id_issue_date, #father_national_id_expiry_date, #mother_passport_issue_date, #mother_passport_expiry_date, #parent_mother_national_id_expiry, #parent_mother_national_id_issue, #parent_guardian_passport_expiry, #parent_guardian_passport_issue, #guardian_national_id_expiry_date, #guardian_national_id_issue_date, #health_card_expiry_date, #health_card_issue_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Inventory Module
    $("#GRN_date, #cs_dp_receiptDate, #cmb_date_from,#cs_to_date, #cs_from_date, #sd_to_date, #sd_from_date,#close_date,#expecteddate,#prov_date,#transaction_date,#cheque_date,#dp_date,#from_date1,#to_date1,#request_date,#require_date,#doc_date,#from_date2,#to_date2,#from_date3,#to_date3,#Entry_date,#lading_date,#reference_date,#create_date,#modification_date,#supplier_C_Price_Date,#stock_check_date,#last_receipt_date,#last_issue_date,#cat_Price_date,#price_date,#is_price_date,#rd_date,#ex_prov_date,#transaction_date1,#cheque_date1").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for HRMS Module
    $("#leave_start_date, #mconf_expiry_date, #passport_expiry_date, #upgradation_date,#temporary_join_date,#DOJ,#passport_issue_date,#expiry_reminder_date1,#visa_issue_date,#expiry_date1,#expiry_reminder_date,#expiry_date2,#expiry_reminder_date2,#gosi_sart_date,#id_issue_date,#expiry_date3,#dp_endadmdate").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for HRMS Module
    $("#nation_expiry_date,#nation_issue_date,#visa_expiry_date,#passport_issue_date,#passport_expiry_date,#visa_expiry_reminder_date,#agreement_expiry_date,#agreement_start_date,#agreement_expiry_reminder_date,#regularization_date,#grade_effect_from_date,#gosi_start_date,#id_expiry_date,#khda_approval_date,#left_date,#health_expiry_date,#health_issue_date,#rta_issue_date,#rta_expiry_date,#department_effect_from_date,#work_permit_issue_date,#work_permit_expiry_date,#resident_in_Qatar_since,#qatar_id_issue_date,#qatar_id_expiry_date,#qatar_id_expiry_reminder_date,#grade_effect_from_date,#upgradation_date,#finalize_date,#passport_issue_date1,#visa_issue_date1,#issue_date_date1,#expiry_date5,#expiry_date4").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Finance Module
    $("#getDate, #dp_entry_date, #fromdate, #todate, #slqdps_Post_Date,  #pdcprs_start_date1, #pdcprs_end_date1, #slqdps_PostDate1, #CM_Effective_Date, #pdcprs_start_date, #pdcprs_end_date, #fy_cmb_END_date, #crdps_PostDate, #cpdps_PostDate, #fy_cmb_Start_date, #slq_PostDate1, #slq_toDate, #pdcs_to_date, #pdcs_from_date, #pvdps_frm_date, #pvdps_to_date, #pcm_creation_date, #pcm_current_date, #pdcpre_enddate,  #pdcpre_startdate, #pdcpre_clearance, #pdcpc_startdate, #pdcpc_enddate, #pdcpr_enddate,  #pdcpr_startdate, #PDCB_PostDate, #pdcprs_enddate, #pdcprs_startdate, #pdc_chq_startdate,  #pdc_chq_enddate, #PDCB_DocDate, #PDCB_ChequeDate, #VR_todate, #VR_fromdate, #PCR_PostDate, #PCR_DocDate, #PCR_pty_ref_date, #e_p_date, #csh_recipt_PostDate, #PCD_PostDate, #PCD_DocDate, #csh_recipt_DocDate, #cash_pay_PostDate, #cash_pay_DocDate, #JV_PostDate, #JV_DocDate, #schedule_enddate, #pty_re_date, #schedule_startdate, #dp_sale_date, #declaredatedp_entry_date, #dp_amend_date, #dp_recdate, #dp_stdate, #dp_expdate, #gl_from_date, #gl_to_date, #clearance_Date, #creation_date, #current_date, #DocDate, #PostDate, #ChequeDate, #pty_ref_date, #txt_party_ref_date1, #txt_party_ref_date2").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });


    //for Grade Book Module
    $("#assign_date, #due_date, #attandance_date, #agenda_date, #publishdate, #date1, #date_of_birth, #cat_start_date, #cat_end_date, #cat_publish_date, #assign_start_date, #assign_end_date, #assign_publish_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Report Card Module
    $("#declaredate").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Health Module
    $("#vaccination_date,#renewal_date,#visit_date,#production_date,#jandate,#febdate,#mardate,#aprdate,#maydate,#jundate,#juldate,#augdate,#sepdate,#octdate,#novdate,#desdate").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Attandance Module
    $("#attendance_date,#inactive_date,#day_wise_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });


    //for Setup module
    $("#ex_date, #startdate, #enddate, #alert_date, #alb_edit_pub_date, #alb_edit_exp_date, #alb_crt_pub_date, #alb_crt_exp_date, #portal_expiry_date, #portal_publish_date,#freeze_date,#submission_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Collaboration module
    $("#dp_expirydate, #dp_publishdate, #expirydate, #fromdate, #todate").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for School Setup module
    $("#dp_start_date, #dp_endDate, #cal_end_date, #cal_start_date, #cex_from_date, #cex_to_date, #term_start_date, #term_end_date, #dp_visa_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for School Setup -> section term module
    $("#term_1_start_dt, #term_2_start_dt, #term_3_start_dt, #term_4_start_dt, #term_5_start_dt, #term_6_start_dt,#term_1_end_dt, #term_2_end_dt, #term_3_end_dt, #term_4_end_dt, #term_5_end_dt, #term_6_end_dt").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Library module
    $("#cmb_create_date,#renew_date,#teacher_renew_date,#publication_date,#printing_date,#entry_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Payroll module
    $("#f_date, #effective_up_date, #effective_date,#insu_exp_id, #insurance_date, #trans_expiry_date, #reg_date, #date_if_purchase, #dob, #sims_student_visa_expiry_date, #lic_expiry, #dept_expiry_date, #dp_visa_date, #lic_issue_date, #sims_student_national_id_issue_date, #sims_student_national_id_expiry_date, #vehicle_exp_from_date,#retro_from_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });


    //for Assignment Fee Module
    $("#dp_receiptDate, #canceldate, #created_date, #st_startdate, #con_to_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Assignment Security Module
    $("#ResetPassword1").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Student Management Module
    $("#birth_from_date,#dp_expiry_date, #MothNatExpDate, #MothNatIssueDate, #visaIssueDate, #visaExpiryDate, #commenceeDate, #passportIssueDate, #MothNatIssueDate, #GuaPassIssueDate, #GuaNatIDIssueDate, #GuaNatIDExpDate, #GuaPassExpDate, #MothNatExpDate, #passportExpDate, #visaIssueDate, #StudNationalID, #studNatExpDate, #regDate, #MothExpDate,  #fathPassIssueDate, #MothPassIssueDate, #MothPassExpDate, #MothPassIssueDate, #fathExpDate, #visaExpiryDate,  #admission_end_date,#birth_end_date, #expected_date,#dp_canceldate, #dp_joinDate, #dp_comm_date, #dp_pass_issue_date, #dp_passexpdate,#dp_visa_issue_date,#dp_visa_expiry_date,#dp_national_issue_date,#dp_national_expiry_date,#dp_sibling_dob,#dp_cur_school_from_date,#dp_cur_school_to_date,#dp_health_issue_date,#dp_health_exp_date,#End_Date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });
    //for Student Management Module
    $("#studDOB,#joineddate,#commenceddate,#dp_text6,#dp_text7,#dp_text8,#dp_text9,#dp_text10,#dp_text11,#dp_text12,#dp_text13,#dp_text14,#dp_text15,#dp_text16,#dp_text17,#dp_text18,#dp_text19,#dp_text20,#dp_text21,#dp_text22,#dp_text23,#dp_text24,#cancel_date,#tc_DOB,#date_of_application").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });


    //for Inventory Module
    $("#GRN_date, #cs_dp_receiptDate, #cmb_date_from, #grn_date_to, #cs_from_date, #sd_to_date, #sd_from_date,#close_date,#expecteddate,#prov_date,#transaction_date,#cheque_date,#dp_date,#from_date1,#to_date1,#request_date,#require_date,#doc_date,#from_date2,#to_date2,#from_date3,#to_date3,#Entry_date,#lading_date,#reference_date,#create_date,#modification_date,#supplier_C_Price_Date,#stock_check_date,#last_receipt_date,#last_issue_date,#cat_Price_date,#price_date,#is_price_date,#rd_date,#ex_prov_date,#transaction_date1,#cheque_date1,#cs_prove,#ln_date,#CSDate,#dt_lc_date1,#dt_lc_open_date1,#dt_lc_valid_date1,dt_lc_close_date1,#dt_lc_open_date,#dt_lc_close_date,#invoice_date,#date_pi_creation_date,#date_pi_invoice_received_date,#date_pi_debit_note_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for HRMS Module
    $("#leave_start_date,#posting_date,#upgradation_date,#temporary_join_date,#DOJ,#passport_issue_date,#expiry_reminder_date1,#visa_issue_date,#expiry_date1,#expiry_reminder_date,#expiry_date2,#expiry_reminder_date2,#gosi_sart_date,#id_issue_date,#expiry_date3,#dp_endadmdate,#regularization_date,#nation_expiry_date,#nation_issue_date,#visa_expiry_date,#passport_issue_date,#passport_expiry_date,#visa_expiry_reminder_date,#agreement_expiry_date,#agreement_start_date,#agreement_expiry_reminder_date,#regularization_date,#grade_effect_from_date,#gosi_start_date,#id_expiry_date,#khda_approval_date,#left_date,#health_expiry_date,#health_issue_date,#rta_issue_date,#rta_expiry_date,#department_effect_from_date,#work_permit_issue_date,#work_permit_expiry_date,#resident_in_Qatar_since,#qatar_id_issue_date,#qatar_id_expiry_date,#qatar_id_expiry_reminder_date,#grade_effect_from_date,#upgradation_date,#finalize_date,#passport_issue_date1,#visa_issue_date1,#issue_date_date1,#expiry_date5,#expiry_date4,#qatar_since_date,#health_card_from_date,#health_card_to_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Finance Module
    $("#cmb_Start_date, #slqdps_Post_Date,  #pdcprs_start_date1, #pdcprs_end_date1, #slqdps_PostDate1, #CM_Effective_Date, #pdcprs_start_date, #pdcprs_end_date, #fy_cmb_END_date, #crdps_PostDate, #cpdps_PostDate, #fy_cmb_Start_date, #slq_PostDate1, #slq_toDate, #pdcs_to_date, #pdcs_from_date, #pvdps_frm_date, #pvdps_to_date, #pcm_creation_date, #pcm_current_date, #pdcpre_enddate,  #pdcpre_startdate, #pdcpre_clearance, #pdcpc_startdate, #pdcpc_enddate, #pdcpr_enddate,  #pdcpr_startdate, #PDCB_PostDate, #pdcprs_enddate, #pdcprs_startdate, #pdc_chq_startdate,  #pdc_chq_enddate, #PDCB_DocDate, #PDCB_ChequeDate, #VR_todate, #VR_fromdate, #PCR_PostDate, #PCR_DocDate, #PCR_pty_ref_date, #e_p_date, #csh_recipt_PostDate, #PCD_PostDate, #PCD_DocDate, #csh_recipt_DocDate, #cash_pay_PostDate, #cash_pay_DocDate, #JV_PostDate, #JV_DocDate, #schedule_enddate, #pty_re_date, #schedule_startdate, #dp_sale_date, #declaredatedp_entry_date, #dp_amend_date, #dp_recdate, #dp_stdate, #dp_expdate, #gl_from_date, #gl_to_date, #clearance_Date, #creation_date, #current_date, #DocDate, #PostDate, #ChequeDate, #pty_ref_date, #txt_party_ref_date1, #txt_party_ref_date2").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Grade Book Module
    $("#assign_date, #due_date,#attandance_date,#cmb_END_date,#publishdate,#date_of_adm,#date1,#tc_date, #cat_start_date, #cat_end_date, #cat_publish_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Report Card Module
    $("#declaredate").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Health Module
    $("#vaccination_date,#renewal_date,#visit_date,#production_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    //for Attandance Module
    $("#attendance_date,#inactive_date").kendoDatePicker({
        format: "dd-MM-yyyy"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });

    $("#newlearning").kendoDatePicker({
        format: "yyyy-MM-dd"
        //format: "dd/MM/yyyy"
        //format: "yyyy/MM/dd"
    });
});





$(document).ready(function () {  //This function for Time Picker
    $("#txt_start_time, #txt_end_time, #time_zone, #expiry_time, #cal_end_time, #cal_start_time, #due_time").kendoTimePicker();
});



$("#datetimepicker").kendoDateTimePicker({
    value: new Date()
});



$("#dp_visa_expiry_date_new, #dp_birth_date_new,#dp_comm_date_new").kendoDatePicker({
    //format: "dd-MM-yyyy"
    //format: "dd/MM/yyyy"
    format: "yyyy-MM-dd"
});














