﻿function showReport(data) {
   // document.getElementById('rptViwer').style.display = "block";
    console.log(data);
    var viewer = $("#reportViewer1").data("telerik_ReportViewer");
    if (viewer == null) {
        $("#reportViewer1").telerik_ReportViewer(data);
        try {
            $("#reportViewer1").refreshReport();
        } catch (e) {

        }
    }
    else {
        viewer.reportSource({
            report: viewer.reportSource().report,
            parameters: data.reportSource.parameters
        });
        viewer.refreshReport();
    }
}

function showReportInPopUp(data) {
    var win = window.open("", 'hello', 'toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=no, resizable=yes, copyhistory=no, width=500, height=400');
    var d = document.getElementById('reportViewer1')
    d.attributes["id"].value = "r1"
    win.document.body.appendChild(d);
    console.log(win.document.body);
    //$("#reportViewer1").telerik_ReportViewer(data);
}
function closeReport() {
    var reData = {
        serviceUrl: 'http://localhost/SIMSAPI/api/reports/',
        reportSource: {
            report: "SimsReports.Sims.SIMR51, SimsReports",
            parameters: { 'fee_rec_no': ''}
        },
        selector: '#reportViewer1',
        viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
        scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
        scale: 1.0,
        ready: function () {
            this.refreshReport();
        },
    };
    $("#reportViewer1").telerik_ReportViewer(reData);
    document.getElementById('rptViwer').style.display = "none";
}