module.exports = function(config){
    config.set({
        basePath : './',
        files : [
            'assets/libs/bower/angular/angular.js',
            'assets/libs/bower/angular-mocks/angular-mocks.js',
            'assets/libs/bower/angular-route/angular-route.js',
            'app/app.js',
            'app/components/**/*.js',
            'specs/**/*_spec.js'
        ],
        autoWatch : true,
        frameworks: ['jasmine'],

        // Start these browsers, currently available:
        // - Chrome
        // - ChromeCanary
        // - Firefox
        // - Opera
        // - Safari (only Mac)
        // - PhantomJS
        // - IE (only Windows)
        browsers : ['Chrome'],
        colors: true,

        // level of logging
        // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
        logLevel: config.LOG_INFO,
        plugins : [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine',
            'karma-junit-reporter'
        ],

        junitReporter : {
            outputFile: 'tests/junit-reporter-log.xml',
            suite: 'unit'
        }
    });
};
