﻿
//var $stateProviderRef = null;

(function () {
    'use strict';

    var sims = angular.module('sims', [
        'pdf',
        'ngRoute',
         'dndLists',
        'ui.router',
        'ngCookies',
        'gettext',
        'config',
         'ngIdle',
         
        'sims.module.core',
        'sims.module.users',
        'sims.module.main',
        'sims.module.Setup',
        'sims.module.Fee',
        'sims.module.Student',
        'sims.module.SchoolSetup',
        'sims.module.Attendance',
        'sims.module.Collaboration',
        'sims.module.HRMS',
        'sims.module.Scheduling',
        'sims.module.Fleet',
         'sims.module.Gradebook',
         'sims.module.Assignment',
         'sims.module.Health',
         'sims.module.Finance',
        'sims.module.Common',
         'sims.module.Incidence',
         'sims.module.Library',
         'sims.module.Inventory',
         'sims.module.ReportCard',
      'sims.module.Security',


    ]);

})();