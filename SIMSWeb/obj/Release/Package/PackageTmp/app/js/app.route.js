﻿/// <reference path="../modules/Fee/views/ManualInventoryFeePosting.html" />
/// <reference path="../modules/Fee/views/ManualInventoryFeePosting.html" />


(function () {
    'use strict';
    var sims = angular.module('sims');

    sims.config(function ($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/login');

        //var temp = [
        //    {state: "login", url: "/login", turl: "", controllear: ""} 
        //]

        //;
        // $stateProviderRef = $stateProvider;

        $stateProvider
            .state('login', {
                url: '/login',
                templateUrl: 'app/modules/users/views/login.html',
                controller: 'LoginController'
            })
            //TODO: URL to be provided
            //Parent
            //For Employee Document Detail
             //.state('main.Per510', {
             //    url: "/EmployeeDocumentDetails",
             //    templateUrl: 'app/modules/HRMS/views/EmployeeDocumentDetails.html',
             //    controller: 'EmployeeDocumentDetailsCont'
             //})



              .state('main.tchApp', {
                  url: "/teacherApplication",
                  templateUrl: 'app/modules/Setup/views/UserApplicationsForTeacherApp.html',
                  controller: 'UserApplicationsForTeacherAppCont'
              })

            .state('main.SylCrt', {
                url: "/SyllabusCreation",
                templateUrl: 'app/modules/SchoolSetup/views/SyllabusCreation.html',
                controller: 'SyllabusCreationCont'
            })
            .state('main.SylCtn', {
                url: "/SyllabusUnitCreation",
                templateUrl: 'app/modules/SchoolSetup/views/SyllabusUnitCreation.html',
                controller: 'SyllabusUnitCreationCont'
            })

            .state('main.EmLPrl', {
                url: "/EmployeeMasterPrl",
                templateUrl: 'app/modules/HRMS/views/EmployeeMasterPearl.html',
                controller: 'EmployeeMasterPearlCont'
            })


                        .state('main.UpdOES', {
                            url: "/UpdateEmployeeOES",
                            templateUrl: 'app/modules/HRMS/views/UpdateEmployeeOES.html',
                            controller: 'UpdateEmployeeOESCont'
                        })

              .state('main.TrnEMP', {
                  url: "/TransferEmployee",
                  templateUrl: 'app/modules/HRMS/views/TransferEmployee.html',
                  controller: 'TransferEmployeeCont'
              })

             .state('main.EmpDps', {
                 url: "/CreateEditEmp",
                 templateUrl: 'app/modules/HRMS/views/CreateEditEmployeeDpsmis.html',
                 controller: 'CreateEditEmployeeDpsmisCont'
             })


             .state('main.EmpGrt', {
                 url: "/EmpGratuityView",
                 templateUrl: 'app/modules/HRMS/views/EmpGratuityView.html',
                 controller: 'EmpGratuityViewCont'
             })

             .state('main.UAdisw', {
                 url: "/UpdtEmployee",
                 templateUrl: 'app/modules/HRMS/views/UpdateEmployeeADISW.html',
                 controller: 'UpdateEmployeeADISWCont'
             })


              .state('main.UpEmpM', {
                  url: "/UpdateEmployeeMaster",
                  templateUrl: 'app/modules/HRMS/views/UpdateEmployeeMasterAdisw.html',
                  controller: 'UpdateEmployeeMasterAdiswCont'
              })


            .state('main.EmLgAD', {
                url: "/EmployeeLongForm",
                templateUrl: 'app/modules/HRMS/views/EmployeeLongFormAdisw.html',
                controller: 'EmployeeLongFormAdiswCont'
            })

            .state('main.CdEmpW', {
                url: "/EmployeeShortFrmA",
                templateUrl: 'app/modules/HRMS/views/CreateEditEmployeeAdisw.html',
                controller: 'CreateEditEmployeeAdiswCont'
            })

             .state('main.AttendanceDashboard', {
                 url: "/AttendanceDashboard",
                 templateUrl: 'app/modules/Attendance/views/AttendanceDashboard.html',
                 controller: 'AttendanceDashboardCont'
             })

              .state('main.AdmissionDashboard', {
                  url: "/admissionDashboard1",
                  templateUrl: 'app/modules/Attendance/views/AdmissionDashboard.html',
                  controller: 'AdmissionDashboardCont'
              })

             .state('main.Admcan', {
                 url: "/ADMCancellation",
                 templateUrl: 'app/modules/Student/views/ADMCencellation.html',
                 controller: 'ADMCancellationCont'
             })


             .state('main.Sim765', {
                 url: "/studentpromotion",
                 templateUrl: 'app/modules/Student/views/studentpromotion.html',
                 controller: 'studentpromotionCont'
             })

             .state('main.ExRefM', {
                 url: "/ExamReferenceMaterial",
                 templateUrl: 'app/modules/Gradebook/views/ExamReferenceMaterial.html',
                 controller: 'ExamReferenceMaterialCont'
             })


             .state('main.Sim770', {
                 url: "/WeeklyAssessment",
                 templateUrl: 'app/modules/Student/views/WeeklyAssessment.html',
                 controller: 'WeeklyAssessmentCont'
             })


             .state('main.Sim771', {
                 url: "/WeeklyAssessmentShow",
                 templateUrl: 'app/modules/SchoolSetup/views/WeeklyAssessmentShow.html',
                 controller: 'WeeklyAssessmentShowCont'
             })



            .state('main.Sim522', {
                url: "/Admissionclasses",
                templateUrl: 'app/modules/Common/views/Admissionclasses.html',
                controller: 'AdmissionclassesCont'
            })
             .state('main.UserMenu', {
                 url: "/menu",
                 templateUrl: 'app/modules/Common/views/Menu.html',
                 controller: 'MenuController'
             })
              .state('main.SIM41R', {
                  url: "/AdmissionStatus",
                  templateUrl: 'app/modules/Student/views/AdmissionStatusRpt.html',
                  controller: 'AdmissionStatusController'
              })
              .state('main.stdImg', {
                  url: "/UpdateStudentImage",
                  templateUrl: 'app/modules/Student/views/UpdateStudentImage.html',
                  controller: 'UpdateStudentImageCont'
              })
             .state('main.Per99V', {
                 url: "/VacancyApp",
                 templateUrl: 'app/modules/Common/views/VacancyApplication.html',
                 controller: 'VacancyApplController'
             })
              .state('main.sim543', {
                  url: "/ReportCardComment",
                  templateUrl: 'app/modules/Gradebook/views/Comment.html',
                  controller: 'CommentController'
              })
             .state('main.sim600', {
                 url: "/AgendaApproval",
                 templateUrl: 'app/modules/Gradebook/views/AgendaApproval.html',
                 controller: 'AgendaApprovalCont'
             })
             .state('main.Per256', {
                 url: "/QualificationMaster",
                 templateUrl: 'app/modules/HRMS/views/QualificationMaster.html',
                 controller: 'QualificationMasterCont'
             })
              .state('main.Fin017', {
                  url: "/FixedAssetDepreciationCalculation",
                  templateUrl: 'app/modules/Finance/views/FixedAssetDepreciationCalculation.html',
                  controller: 'FixedAssetDepreciationCalculationCont'
              })
            .state('main.Sims617', {
                url: "/LateFeeRules",
                templateUrl: 'app/modules/Fee/views/LateFee.html',
                controller: 'StudentLateFeeController'
            })
             .state('main.Sims618', {
                 url: "/StudentLateFee",
                 templateUrl: 'app/modules/Fee/views/latefeerule.html',
                 controller: 'LateFeeRuleDefineController'
             })
            .state('main.Sims619', {
                url: "/StudentLateFee",
                templateUrl: 'app/modules/Fee/views/LateRuleFormula.html',
                controller: 'LateRuleFormulaController'
            })
             .state('main.ReportCardParameter', {
                 url: "/ReportCardParameter",
                 templateUrl: 'app/modules/Setup/views/ReportCardParameter.html',
                 controller: 'ReportCardParameterCont'
             })
             .state('main.EmployeeShortForm', {
                 url: "/EmployeeShortForm",
                 templateUrl: 'app/modules/HRMS/views/EmployeeShortForm.html',
                 controller: 'EmployeeShortFormCont'
             })

            .state('main.Com099', {
                url: "/emailtoDefaulters",
                templateUrl: 'app/modules/Collaboration/views/EmailToDefaulters.html',
                controller: 'DefaulterStatusController'
            })
            .state('main.Sim048', {
                url: "/Workflow",
                templateUrl: 'app/modules/Setup/views/Workflow.html',
                controller: 'WorkflowCont'
            })

                //.state('main.Sim510', {
                //    url: "/SearchUser",
                //    templateUrl: 'app/modules/Setup/views/SearchUser.html',
                //    controller: 'SearchUserCont'
                //})

            .state('main.Sim507', {
                url: "/MySearch",
                templateUrl: 'app/modules/Setup/views/MySearch.html',
                controller: 'MySearchCont'
            })

             .state('main.Sim510', {
                 url: "/SearchUser",
                 templateUrl: 'app/modules/Setup/views/SearchUser.html',
                 controller: 'SearchUserCont'
             })

               .state('main.Com014', {
                   url: "/ResetPassword",
                   templateUrl: 'app/modules/Security/views/ResetPassword.html',
                   controller: 'ResetPasswordCont'
               })

             .state('main.ReportCard', {
                 url: "/ReportCard",
                 templateUrl: 'app/modules/Setup/views/ReportCard.html',
                 controller: 'ReportCardCont'
             })

              .state('main.Per510', {
                  url: "/EmployeeDocumentDetails",
                  templateUrl: 'app/modules/HRMS/views/EmployeeDocumentDetails.html',
                  controller: 'EmployeeDocumentDetailsCont'
              })


            .state('main.TimeLine', {
                url: "/TimeLine",
                templateUrl: 'app/modules/Setup/views/TimeLine.html',
                controller: 'TimeLineCont'
            })

                .state('main.InvSlD', {
                    url: "/SaleDocumentList",
                    templateUrl: 'app/modules/Inventory/views/SaleDocumentList.html',
                    controller: 'SaleDocumentListCont'
                })


                           .state('main.deliveryorder', {
                               url: "/deliveryorder",
                               templateUrl: 'app/modules/Inventory/views/DeliveryOrder.html',
                               controller: 'DeliveryOrderCont'
                           })


             .state('main.FeePPM', {
                 url: "/FeePostingPayModeInvs",
                 templateUrl: 'app/modules/Inventory/views/FeePostingPayModeInvs.html',
                 controller: 'FeePostingPayModeInvsCont'
             })
              .state('main.Sim615', {
                  url: "/FeeReceipt",
                  templateUrl: 'app/modules/Fee/views/FeeReceipt.html',
                  controller: 'FeeReceiptCont'
              })


            .state('main.Sim700', {
                url: "/FeeCollection",
                templateUrl: 'app/modules/Fee/views/FeeCollection.html',
                controller: 'FeeCollectionCont'
            })

             .state('main.Sim501', {
                 url: "/PPFieldStatus",
                 templateUrl: 'app/modules/Setup/views/PPFieldStatus.html',
                 controller: 'PPFieldStatusCont',
             })
              .state('main.Sim135', {
                  url: "/libraryTrans",
                  templateUrl: 'app/modules/Library/views/LibraryTran.html',
                  controller: 'LibraryTranController'
              })
            .state('main.Sim567', {
                url: "/ProspectDashboard",
                templateUrl: 'app/modules/Student/views/ProspectDashboard.html',
                controller: 'ProspectDashboardCont',
                params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
            })


            .state('main.Sim034', {
                url: "/ProspectAdmissionNIS",
                templateUrl: 'app/modules/Student/views/ProspectAdmissionNIS.html',
                controller: 'ProspectAdmissionNISCont',
            })

               .state('main.Sim037', {
                   url: "/ProspectAdmissionShort",
                   templateUrl: 'app/modules/Student/views/ProspectAdmissionShort.html',
                   controller: 'ProspectAdmissionShortCont'
               })


            .state('main.ProspectAdmission', {
                url: "/ProspectAdmission",
                templateUrl: 'app/modules/Student/views/ProspectAdmission.html',
                controller: 'ProspectAdmissionCont',
                params: { Pros_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
            })


             .state('main.emailt', {
                 url: "/Email",
                 templateUrl: 'app/modules/Setup/views/Email.html',
                 controller: 'EmailCont'
             })


            .state('main.AuditAlert', {
                url: "/TimeLineAlert",
                templateUrl: 'app/modules/Setup/views/TimeLineAlert.html',
                controller: 'TimeLineAlertCont'
            })


             .state('main.Email', {
                 url: "/EmailTransaction",
                 templateUrl: 'app/modules/Setup/views/EmailCommunication.html',
                 controller: 'EmailCommunicationCont'
             })


            .state('main', {
                url: "/main",
                templateUrl: 'app/modules/main/views/main.html',
                controller: 'MainController'
            })

              .state('main.BankRe', {
                  url: "/BankReconciliation",
                  templateUrl: 'app/modules/Finance/views/BankReconciliation.html',
                  controller: 'BankReconciliationCont'
              })

            .state('main.Fin006', {
                url: "/AssetLiteral",
                templateUrl: 'app/modules/Finance/views/AssetLiteral.html',
                controller: 'AssetLiteralCont'
            })
            .state('main.Fin155', {
                url: "/FeePosting",
                templateUrl: 'app/modules/Finance/views/FeePosting.html',
                controller: 'FeePostingCont'
            })
            .state('main.Fin021', {
                url: "/AssetMaster",
                templateUrl: 'app/modules/Finance/views/AssetMaster.html',
                controller: 'AssetMasterCont'
            })
             .state('main.Fin013', {
                 url: "/ViewEditGLPeriodLedger",
                 templateUrl: 'app/modules/Finance/views/ViewEditGLPeriodLedger.html',
                 controller: 'ViewEditGLPeriodLedgerCont'
             })
            .state('main.Fin016', {
                url: "/SubledgerMaster",
                templateUrl: 'app/modules/Finance/views/SubledgerMaster.html',
                controller: 'SubledgerMasterCont'
            })
            .state('main.Fin012', {
                url: "/CreateEditFinancialPeriod",
                templateUrl: 'app/modules/Finance/views/CreateEditFinancialPeriod.html',
                controller: 'CreateEditFinancialPeriodCont'
            })
             .state('main.Sim022', {
                 url: "/FeeType",
                 templateUrl: 'app/modules/Fee/views/FeeType.html',
                 controller: 'FeeTypeCont'
             })
              .state('main.Com018', {
                  url: "/ViewCirculars",
                  templateUrl: 'app/modules/Collaboration/views/ViewCircular.html',
                  controller: 'ViewCircularCont'
              })
             .state('main.CirBrs', {
                 url: "/ViewCirculars_BRS",
                 templateUrl: 'app/modules/Collaboration/views/ViewCircular_BRS.html',
                 controller: 'ViewCircularCont'
             })

             .state('main.Sim166', {
                 url: "/Alert",
                 templateUrl: 'app/modules/Collaboration/views/Alert.html',
                 controller: 'AlertCont'
             })

            .state('main.Com017', {
                url: "/CreateCirculars",
                templateUrl: 'app/modules/Collaboration/views/CreateCircular.html',
                controller: 'CreateCircularCont'
            })

            .state('main.simapp', {
                url: "/CollaborationDetails",
                templateUrl: 'app/modules/Collaboration/views/CollaborationDetails.html',
                controller: 'CollaborationDetailsCont'
            })

                //Quota
             .state('main.DefineQuota', {
                 url: "/DefineAdmissionQuota",
                 templateUrl: 'app/modules/Student/views/DefineQuota.html',
                 controller: 'DefineQuota'
             })

            .state('main.Sim566', {
                url: "/UpdateFeeCategory",
                templateUrl: 'app/modules/Student/views/UpdateFeeCategory.html',
                controller: 'UpdateFeeCategoryCont'
            })
            .state('main.stdEA', {
                url: "/StudentEANumber",
                templateUrl: 'app/modules/Student/views/StudentEANumber.html',
                controller: 'StudentEANumberCont'
            })


              .state('main.Sim527', {
                  url: "/AgendaCreation",
                  templateUrl: 'app/modules/Gradebook/views/AgendaCreation.html',
                  controller: 'AgendaCreationCont'
              })
             .state('main.Inv001', {
                 url: "/UnitOfMeasurement",
                 templateUrl: 'app/modules/Inventory/views/UnitOfMeasurement.html',
                 controller: 'UnitOfMeasurementCont'
             })

             .state('main.Inv002', {
                 url: "/UomConversions",
                 templateUrl: 'app/modules/Inventory/views/UomConversions.html',
                 controller: 'UomConversionsCont'
             })

            .state('main.Inv009', {
                url: "/SupplierDescriptionTypes",
                templateUrl: 'app/modules/Inventory/views/SupplierDescriptionTypes.html',
                controller: 'SupplierDescriptionTypesCont'
            })
             .state('main.Inv010', {
                 url: "/SupplierDescription",
                 templateUrl: 'app/modules/Inventory/views/SupplierDescription.html',
                 controller: 'SupplierDescriptionCont'
             })

               .state('main.Inv100', {
                   url: "/Bin",
                   templateUrl: 'app/modules/Inventory/views/Bins.html',
                   controller: 'BinsCont'
               })

             .state('main.Inv126', {
                 url: "/SalesType",
                 templateUrl: 'app/modules/Inventory/views/SalesType.html',
                 controller: 'SalesTypeCont'
             })

              .state('main.Inv046', {
                  url: "/AdjustmentReason",
                  templateUrl: 'app/modules/Inventory/views/AdjustmentReason.html',
                  controller: 'AdjustmentReasonCont'
              })

              .state('main.Inv128', {
                  url: "/SaleTypeMarkUp",
                  templateUrl: 'app/modules/Inventory/views/SaleTypeMarkUp.html',
                  controller: 'SaleTypeMarkUpCont'
              })


            //Inventory Module

                .state('main.Inv020', {
                    url: "/CompanyParameter",
                    templateUrl: 'app/modules/Inventory/views/CompanyParameter.html',
                    controller: 'CompanyParameterCont'
                })

         .state('main.Inv071', {
             url: "/DocumentType",
             templateUrl: 'app/modules/Inventory/views/DocumentType.html',
             controller: 'DocumentTypeCont'
         })

        .state('main.Inv018', {
            url: "/CreateCompany",
            templateUrl: 'app/modules/Inventory/views/CreateCompany.html',
            controller: 'CreateCompanyCont'
        })

        .state('main.Inv131', {
            url: "/ROLSettings",
            templateUrl: 'app/modules/Inventory/views/ROLSettings.html',
            controller: 'ROLSettingsCont'
        })

          .state('main.Inv058', {
              url: "/RequestDetails",
              templateUrl: 'app/modules/Inventory/views/RequestDetails.html',
              controller: 'RequestDetailsCont'
          })

        //.state('main.Inv044', {
        //    url: "/CreateOrder",
        //    templateUrl: 'app/modules/Inventory/views/CreateOrder.html',
        //    controller: 'CreateOrderCont'
        //})

            .state('main.InSREQ', {
                url: "/StoresRequest",
                templateUrl: 'app/modules/Inventory/views/StoresRequest.html',
                controller: 'StoresRequestCont'
            })

            .state('main.ApIREQ', {
                url: "/ItemRequestApproval",
                templateUrl: 'app/modules/Inventory/views/ItemRequestApprove.html',
                controller: 'ItemRequestApproveCont'
            })

            .state('main.Inv044', {
                url: "/CreateOrder",
                templateUrl: 'app/modules/Inventory/views/CrOrder.html',
                controller: 'CrOrderCont'
            })

             .state('main.ordapp', {
                 url: "/ApproveOrder",
                 templateUrl: 'app/modules/Inventory/views/OrderApprove.html',
                 controller: 'OrderApproveCont'
             })

            .state('main.CrtRFQ', {
                url: "/CreateRfq",
                templateUrl: 'app/modules/Inventory/views/Rfq.html',
                controller: 'RfqCont'
            })

             .state('main.DirOrd', {
                 url: "/DirectPO",
                 templateUrl: 'app/modules/Inventory/views/DirectOrder.html',
                 controller: 'DirectOrderCont'
             })


              .state('main.Inv021', {
                  url: "/ItemMaster",
                  templateUrl: 'app/modules/Inventory/views/ItemMaster.html',
                  controller: 'ItemMasterCont'
              })


             .state('main.Inv143', {
                 url: "/InterestType",
                 templateUrl: 'app/modules/Inventory/views/InterestType.html',
                 controller: 'InterestTypeCont'
             })

               .state('main.Inv027', {
                   url: "/PurchaseExpensesType",
                   templateUrl: 'app/modules/Inventory/views/PurchaseExpensesType.html',
                   controller: 'PurchaseExpensesTypeCont'
               })
              .state('main.Inv133', {
                  url: "/CustomClearance",
                  templateUrl: 'app/modules/Inventory/views/CustomClearance.html',
                  controller: 'CustomClearanceCont'
              })
              .state('main.Inv134', {
                  url: "/CustomExpenses",
                  templateUrl: 'app/modules/Inventory/views/CustomExpenses.html',
                  controller: 'CustomExpensesCont'
              })
              .state('main.Inv132_1', {
                  url: "/CreateCostingSheet",
                  templateUrl: 'app/modules/Inventory/views/CreateCostingSheet.html',
                  controller: 'CreateCostingSheetCont'
              })
            .state('main.Inv132', {
                url: "/CostingSheet",
                templateUrl: 'app/modules/Inventory/views/CostingSheet.html',
                controller: 'CostingSheetCont'
            })
             //Inventory Module END
              .state('main.Sim092', {
                  url: "/PortalReference",
                  templateUrl: 'app/modules/Setup/views/PortalReference.html',
                  controller: 'PortalReferenceCont'
              })

            .state('main.LerArb', {
                url: "/LearnArabic",
                templateUrl: 'app/modules/Setup/views/LearnArabic.html',
                controller: 'LearnArabicCont'
            })


             .state('main.Sim526', {
                 url: "/UploadDocument",
                 templateUrl: 'app/modules/Student/views/UploadRemainingDocument.html',
                 controller: 'UploadRemainingDocumentCont'
             })

              .state('main.Sim184', {
                  url: "/BoardExam",
                  templateUrl: 'app/modules/SchoolSetup/views/BoardExam.html',
                  controller: 'BoardExamCont'
              })

              .state('main.Sim183', {
                  url: "/Board",
                  templateUrl: 'app/modules/SchoolSetup/views/Board.html',
                  controller: 'BoardCont'
              })

              .state('main.Sim185', {
                  url: "/StudentBoardExam",
                  templateUrl: 'app/modules/Student/views/StudentBoardExam.html',
                  controller: 'StudentBoardExamCont'
              })

             //.state('main.Sim525', {
             //    url: "/TeacherAssignmentUpload",
             //    templateUrl: 'app/modules/Setup/views/TeacherAssignmentUpload.html',
             //    controller: 'TeacherAssignmentUploadCont'
             //})


              .state('main.Si509A', {
                  url: "/AssignmentViewWithApproval",
                  templateUrl: 'app/modules/Assignment/views/AssignmentViewWithApproval.html',
                  controller: 'AssignmentViewWithApprovalCont'
              })

            .state('main.Si508A', {
                url: "/AssignmentApprove",
                templateUrl: 'app/modules/Assignment/views/AssignmentApprove.html',
                controller: 'AssignmentApproveCont'
            })


              .state('main.Sim509', {
                  url: "/TeacherAssignmentView",
                  templateUrl: 'app/modules/Assignment/views/TeacherAssignmentView.html',
                  controller: 'TeacherAssignmentViewCont'
              })

              .state('main.Sim508', {
                  url: "/TeacherAssignmentUpload",
                  templateUrl: 'app/modules/Assignment/views/TeacherAssignmentUpload.html',
                  controller: 'TeacherAssignmentUploadCont'
              })

            .state('main.Sim685', {
                url: "/AddExamPaper",
                templateUrl: 'app/modules/Assignment/views/AddExamPaper.html',
                controller: 'AddExamPaperCont'
            })
              .state('main.Pers255', {
                  url: "/EmployeeImageView",
                  templateUrl: 'app/modules/HRMS/views/EmployeeImageView.html',
                  controller: 'EmployeeImageViewCont'
              })

              .state('main.Pers021', {
                  url: "/EmployeeGrade",
                  templateUrl: 'app/modules/Attendance/views/EmployeeGrade.html',
                  controller: 'EmployeeGradeCont'
              })

            .state('main.Sim612', {
                url: "/HomeRoomAttendance",
                templateUrl: 'app/modules/Attendance/views/HomeRoomAttendance.html',
                controller: 'HomeRoomAttendanceCont'
            })


             .state('main.Per254', {
                 url: "/DefineCategory",
                 templateUrl: 'app/modules/HRMS/views/DefineCatgory.html',
                 controller: 'DefineCategoryCont'
             })

             .state('main.Pers061', {
                 url: "/OtherLoan",
                 templateUrl: 'app/modules/HRMS/views/OtherLoan.html',
                 controller: 'OtherLoanCont'
             })

               .state('main.Pers090', {
                   url: "/SettlementType",
                   templateUrl: 'app/modules/HRMS/views/SettlementType.html',
                   controller: 'SettlementTypeCont'
               })

             .state('main.Pers312', {
                 url: "/EmployeeDocumentUpload",
                 templateUrl: 'app/modules/HRMS/views/EmployeeDocumentUpload.html',
                 controller: 'EmployeeDocumentUploadCont'
             })

             .state('main.Fin010', {
                 url: "/CreateEditDepartment",
                 templateUrl: 'app/modules/Finance/views/CreateEditDepartment.html',
                 controller: 'CreateEditDepartmentCont'
             })

            .state('main.Fin116', {
                url: "/ScheduleGroupDetails",
                templateUrl: 'app/modules/Finance/views/ScheduleGroupDetails.html',
                controller: 'ScheduleGroupDetailsCont'
            })

             .state('main.Fin117', {
                 url: "/ScheduleGroup",
                 templateUrl: 'app/modules/Finance/views/ScheduleGroup.html',
                 controller: 'ScheduleGroupCont'
             })


              .state('main.Sim175', {
                  url: "/StudentImageView",
                  templateUrl: 'app/modules/Student/views/StudentImageView.html',
                  controller: 'StudentImageViewCont'
              })
             .state('main.Com016', {
                 url: "/ViewNews",
                 templateUrl: 'app/modules/Collaboration/views/ViewNews.html',
                 controller: 'ViewNewsCont'
             })
            .state('main.NewBrs', {
                url: "/ViewNews_BRS",
                templateUrl: 'app/modules/Collaboration/views/ViewNews_BRS.html',
                controller: 'ViewNewsCont'
            })
            .state('main.Com015', {
                url: "/CreateNews",
                templateUrl: 'app/modules/Collaboration/views/CreateNews.html',
                controller: 'CreateNewsCont'
            })

            .state('main.InvFee', {
                url: "/InventoryFeePosting",
                templateUrl: 'app/modules/Inventory/views/InventoryFeePosting.html',
                controller: 'InventoryFeePostingCont'
            })

             .state('main.NewUpp', {
                 url: "/CreateNews1",
                 templateUrl: 'app/modules/Collaboration/views/CreateNw.html',
                 controller: 'CreateNwCont'
             })

            .state('main.VNewup', {
                url: "/ViewNews_MultipleFiles",
                templateUrl: 'app/modules/Collaboration/views/ViewNews_MultipleFiles.html',
                controller: 'ViewNews_MultipleFilesCont'
            })

               .state('main.Fn102S', {
                   url: "/PDCSubmission",
                   templateUrl: 'app/modules/Finance/views/PDCSubmission.html',
                   controller: 'PDCSubmissionCont'
               })

             .state('main.cir', {
                 url: "/Circulars",
                 templateUrl: 'app/modules/Collaboration/views/Circular.html',
                 controller: 'CircularCont'
             })
                 .state('main.news', {
                     url: "/News",
                     templateUrl: 'app/modules/Collaboration/views/News.html',
                     controller: 'NewsCont'
                 })
             .state('main.Sim021', {
                 url: "/FeeCategory",
                 templateUrl: 'app/modules/Fee/views/FeeCategory.html',
                 controller: 'FeeCategoryCont'
             })


             .state('main.Sim531', {
                 url: "/StudentPayingAgent",
                 templateUrl: 'app/modules/Fee/views/StudentPayingAgent.html',
                 controller: 'StudentPayingAgentCont'
             })

             .state('main.Sim532', {
                 url: "/PayingAgentMapping",
                 templateUrl: 'app/modules/Fee/views/PayingAgentMapping.html',
                 controller: 'PayingAgentMappingCont'
             })

            .state('main.Per330', {
                url: "/UpdateHoldEmpStatus",
                templateUrl: 'app/modules/HRMS/views/UpdateHoldEmpStatus.html',
                controller: 'UpdateHoldEmpStatusCont'
            })

             .state('main.Per352', {
                 url: "/PayrollPubFin",
                 templateUrl: 'app/modules/HRMS/views/PayrollPubFinStatus.html',
                 controller: 'PayrollPubFinStatusCont'
             })

              .state('main.Per078', {
                  url: "/Payroll_Genration",
                  templateUrl: 'app/modules/HRMS/views/GeneratePayroll.html',
                  controller: 'GeneratePayrollCont'
              })

             .state('main.Per314', {
                 url: "/UpdateLeaveBalance",
                 templateUrl: 'app/modules/HRMS/views/UpdateLeaveBal.html',
                 controller: 'UpdateLeaveBalCont'
             })

            .state('main.Per340', {
                url: "/UpdatePayrollStatus",
                templateUrl: 'app/modules/HRMS/views/UpdatePayrollStatus.html',
                controller: 'UpdatePayrollStatusCont'
            })

            .state('main.Per072', {
                url: "/PayrollEmployee",
                templateUrl: 'app/modules/HRMS/views/PayrollEmployee.html',
                controller: 'PayrollEmployeeCont'
            })

              .state('main.Pr072A', {
                  url: "/PayrollEmployeeDPSD",
                  templateUrl: 'app/modules/HRMS/views/PayrollEmployeeDPSD.html',
                  controller: 'PayrollEmployee_DPSDCont'
              })

            .state('main.Per351', {
                url: "/PayableIncrement",
                templateUrl: 'app/modules/HRMS/views/PayableIncrement.html',
                controller: 'PayableIncrementCont'
            })

             .state('main.Per350', {
                 url: "/PayrollEmployee_1",
                 templateUrl: 'app/modules/HRMS/views/PayrollEmployee_1.html',
                 controller: 'PayrollEmployee_1Cont'
             })

             .state('main.Sim045', {
                 url: "/Subject",
                 templateUrl: 'app/modules/Setup/views/Subject.html',
                 controller: 'SubjectCont'
             })
           .state('main.Sim046', {
               url: "/SubjectGroup",
               templateUrl: 'app/modules/Setup/views/SubjectGroup.html',
               controller: 'SubjectGroupCont'
           })
          .state('main.Sim047', {
              url: "/SubjectType",
              templateUrl: 'app/modules/Setup/views/SubjectType.html',
              controller: 'SujectTypeCont'
          })
         .state('main.Sim031', {
             url: "/Parameter",
             templateUrl: 'app/modules/Setup/views/Parameter.html',
             controller: 'ParameterCont'
         })
            .state('main.Sim012', {
                url: "/AdmissionCriteria",
                templateUrl: 'app/modules/Student/views/AdmissionCriteria.html',
                controller: 'AdmissionCriteriaCont'
            })
             .state('main.Sim038', {
                 url: "/SectionScreening",
                 templateUrl: 'app/modules/Student/views/SectionScreening.html',
                 controller: 'SectionScreeningCont'
             })
          .state('main.Si010C', {
              url: "/CancelAdmission",
              templateUrl: 'app/modules/Student/views/CancelAdmission.html',
              controller: 'CancelAdmissionCont'
          })
          .state('main.SimSht', {
              url: "/AdmissionShort",
              templateUrl: 'app/modules/Student/views/AdmissionShort.html',
              controller: 'AdmissionShortCont'
          })

               .state('main.SNSht', {
                   url: "/AdmissionShort_SN",
                   templateUrl: 'app/modules/Student/views/AdmissionShort_SN.html',
                   controller: 'AdmissionShort_SNCont'
               })

             .state('main.PeaSht', {
                 url: "/AdmissionShort_pearl",
                 templateUrl: 'app/modules/Student/views/AdmissionShort_pearl.html',
                 controller: 'AdmissionShort_pearlCont'
             })


          .state('main.Sim010', {
              url: "/Admission",
              templateUrl: 'app/modules/Student/views/Admission.html',
              controller: 'AdmissionCont',
              params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
          })

             .state('main.AdmSN', {
                 url: "/Admission_SN",
                 templateUrl: 'app/modules/Student/views/Admission_SN.html',
                 controller: 'Admission_SNCont',
                 params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
             })


              .state('main.Admdps', {
                  url: "/Admission_dpsd",
                  templateUrl: 'app/modules/Student/views/Admission_dpsd.html',
                  controller: 'Admission_dpsdCont',
                  params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
              })

             .state('main.Admajb', {
                 url: "/Admission_ajb",
                 templateUrl: 'app/modules/Student/views/Admission_ajb.html',
                 controller: 'Admission_ajbCont',
                 params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
             })

            .state('main.Admaji', {
                url: "/Admission_aji",
                templateUrl: 'app/modules/Student/views/Admission_aji.html',
                controller: 'Admission_ajiCont',
                params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
            })

             .state('main.Sim000', {
                 url: "/AdmissionDashboard",
                 templateUrl: 'app/modules/Student/views/AdmissionDashboard.html',
                 controller: 'AdmissionDashboardCont',
                 params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
             })

            .state('main.DasSN', {
                url: "/AdmissionDashboard_SN",
                templateUrl: 'app/modules/Student/views/AdmissionDashboard_SN.html',
                controller: 'AdmissionDashboard_SNCont',
                params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
            })

            .state('main.Dasadi', {
                url: "/AdmissionDashboard_adisw",
                templateUrl: 'app/modules/Student/views/AdmissionDashboard_adisw.html',
                controller: 'AdmissionDashboard_adiswCont',
                params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
            })

             .state('main.Dasajb', {
                 url: "/AdmissionDashboard_ajb",
                 templateUrl: 'app/modules/Student/views/AdmissionDashboard_ajb.html',
                 controller: 'AdmissionDashboard_ajbCont',
                 params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
             })

            .state('main.Dasaji', {
                url: "/AdmissionDashboard_aji",
                templateUrl: 'app/modules/Student/views/AdmissionDashboard_aji.html',
                controller: 'AdmissionDashboard_ajiCont',
                params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
            })

             .state('main.Dasdps', {
                 url: "/AdmissionDashboard_dpsd",
                 templateUrl: 'app/modules/Student/views/AdmissionDashboard_dpsd.html',
                 controller: 'AdmissionDashboard_dpsdCont',
                 params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
             })

             .state('main.brsSht', {
                 url: "/AdmissionShortBrs",
                 templateUrl: 'app/modules/Student/views/AdmissionShort_brs.html',
                 controller: 'AdmissionShort_brsCont'
             })

             .state('main.admbrs', {
                 url: "/AdmissionBrs",
                 templateUrl: 'app/modules/Student/views/Admission_brs.html',
                 controller: 'Admission_brsCont',
                 params: { admission_num: {}, Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
             })



              .state('main.Dasbrs', {
                  url: "/AdmissionDashboardBrs",
                  templateUrl: 'app/modules/Student/views/AdmissionDashboard_brs.html',
                  controller: 'AdmissionDashboard_brsCont',
                  params: { Class: { curr_code: '', academic_year: '', grade_code: '', sims_appl_parameter_reg: '' } }
              })

             .state('main.LPOGRN', {
                 url: "/LPOGRN",
                 templateUrl: 'app/modules/Inventory/views/LpoGrn.html',
                 controller: 'LpoGrnCont'
             })
           .state('main.Sim003', {
               url: "/Curriculum",
               templateUrl: 'app/modules/Setup/views/Curriculum.html',
               controller: 'CurriculumCont'
           })

            .state('main.Sim201', {
                url: "/StudentDevice",
                templateUrl: 'app/modules/Student/views/StudentDevice.html',
                controller: 'StudentDeviceCont'
            })

            .state('main.Sim108', {
                url: "/RequestCertificate",
                templateUrl: 'app/modules/Student/views/RequestCertificate.html',
                controller: 'RequestCertificateCont'
            })

            .state('main.Sim505', {
                url: "/CertificateTCParameter",
                templateUrl: 'app/modules/Student/views/CertificateTCParameter.html',
                controller: 'CertificateTCParameterCont'
            })

            .state('main.Sim681', {
                url: "/CertificateTCParameterBrs",
                templateUrl: 'app/modules/Student/views/CertificateTCParameter_brs.html',
                controller: 'CertificateTCParameter_brsCont'
            })

            .state('main.Sim684', {
                url: "/CertificateTCApprove_Pearl",
                templateUrl: 'app/modules/Student/views/CertificateTCApprove_Pearl.html',
                controller: 'CertificateTCApprove_PearlCont'
            })
             .state('main.Sim186', {
                 url: "/HeadTeacherMapping",
                 templateUrl: 'app/modules/SchoolSetup/views/HeadTeacherMapping.html',
                 controller: 'HeadTeacherMappingCont'
             })
            .state('main.Sim187', {
                url: "/SupervisorMapping",
                templateUrl: 'app/modules/SchoolSetup/views/SupervisorMapping.html',
                controller: 'SupervisorMappingCont'
            })
          .state('main.Sim001', {
              url: "/AcademicYear",
              templateUrl: 'app/modules/Setup/views/AcademicYear.html',
              controller: 'AcademicYearCont'
          })
              .state('main.Sim518', {
                  url: "/AttendanceRule",
                  templateUrl: 'app/modules/Student/views/AttendanceRule.html',
                  controller: 'AttendanceRuleCont'
              })
            .state('main.Per308', {
                url: "/ActiveUser",
                templateUrl: 'app/modules/HRMS/views/ActiveUser.html',
                controller: 'ActiveUserCont'
            })

         .state('main.Sim035', {
             url: "/Section",
             templateUrl: 'app/modules/Setup/views/Section.html',
             controller: 'SectionCont1'
         })
             .state('main.Sim164', {
                 url: "/Moderator",
                 templateUrl: 'app/modules/Setup/views/Moderator.html',
                 controller: 'ModeratorCont'
             })
            .state('main.Com004', {
                url: "/CommonSequence",
                templateUrl: 'app/modules/Setup/views/CommonSequence.html',
                controller: 'CommonSequenceCont'
            })
            .state('main.Sim165', {
                url: "/ModeratorUser",
                templateUrl: 'app/modules/Setup/views/ModeratorUser.html',
                controller: 'ModeratorUserCont'
            })
             .state('main.Sim155', {
                 url: "/StudentAttendance",
                 templateUrl: 'app/modules/Attendance/views/StudentAttendance.html',
                 controller: 'StudentAttendanceCont'
             })

               .state('main.Sim044', {
                   url: "/UpdateStudentSection",
                   templateUrl: 'app/modules/Student/views/UpdateStudentSection.html',
                   controller: 'UpdateStudentSectionController'
               })

                .state('main.Sim008', {
                    url: "/Nationality",
                    templateUrl: 'app/modules/Setup/views/Nationality.html',
                    controller: 'NationalityController'
                })

                .state('main.Sim009', {
                    url: "/Religion",
                    templateUrl: 'app/modules/Setup/views/Religion.html',
                    controller: 'ReligionController'
                })
               .state('main.Sim005', {
                   url: "/GradeLevel",
                   templateUrl: 'app/modules/Setup/views/GradeLevel.html',
                   controller: 'GradeLevelCont'
               })
               .state('main.Sim004', {
                   url: "/CurriculumLevel",
                   templateUrl: 'app/modules/Setup/views/CurriculumLevel.html',
                   controller: 'CurriculumLevelController'
               })

              .state('main.Sim024', {
                  url: "/Grade",
                  templateUrl: 'app/modules/SchoolSetup/views/Grade.html',
                  controller: 'GradeCont'
              })
               .state('main.Sim054', {
                   url: "/Term",
                   templateUrl: 'app/modules/SchoolSetup/views/Term.html',
                   controller: 'TermCont'
               })

            .state('main.Sim036', {
                url: "/SectionFee",
                templateUrl: 'app/modules/Fee/views/SectionFee.html',
                controller: 'SectionFeeController'
            })
           .state('main.Sim017', {
               url: "/Fee Concession",
               templateUrl: 'app/modules/Fee/views/StudentFeeConcession.html',
               controller: 'StudentFeeConcessionCont'
           })
           .state('main.Sim018', {
               url: "/StudentFeeConcessionTransaction",
               templateUrl: 'app/modules/Fee/views/StudentFeeConcessionTransaction.html',
               controller: 'StudentFeeConcessionTransactionCont'
           })

             .state('main.Sim016', {
                 url: "/ConcessionCancel",
                 templateUrl: 'app/modules/Fee/views/ConcessionCancel.html',
                 controller: 'ConcessionCancelCont'
             })
             .state('main.Sim553', {
                 url: "/TransportFee",
                 templateUrl: 'app/modules/Fee/views/SFSTransport.html',
                 controller: 'SFCTransportController',
                 params: { 'sel': {} }
             })
           .state('main.Sim043', {
               url: "/FeeDet",
               templateUrl: 'app/modules/Fee/views/SFS.html',
               controller: 'SFCController',
               params: { 'sel': {} }
           })
            .state('main.Sim043Search', {
                url: "/FeeTransaction",
                templateUrl: 'app/modules/Fee/views/FeeTransaction.html',
                controller: 'FeeTransactionController',
                params: { 'IP': {}, 'sel': {} }
            })
             .state('main.Sim043AJB', {
                 url: "/FeeTransactionAJB",
                 templateUrl: 'app/modules/Fee/views/FeeTransaction.html',
                 controller: 'FeeTransactionControllerAJB',
                 params: { 'IP': {}, 'sel': {} }
             })
            .state('main.PA', {
                url: "/FeeTransPayingAgent",
                templateUrl: 'app/modules/Fee/views/FeeTransPayingAgent.html',
                controller: 'FeeTransPayingAgent',
                params: { 'sel': {} }
            })
            //.state('main.PATrans', {
            //    url: "/FeeTransPA",
            //    templateUrl: 'app/modules/Fee/views/FeeTransPayingAgent.html',
            //    controller: 'FeeTransPayingAgent',
            //    params: { 'IP': {}, 'sel': {} }
            //})
          .state('main.Sim178', {
              url: "/CopySectionFee",
              templateUrl: 'app/modules/Fee/views/CopySectionFee.html',
              controller: 'CopySectionFeeCont'
          })

          .state('main.Sim059', {
              url: "/SectionTerm",
              templateUrl: 'app/modules/Student/views/SectionTerm.html',
              controller: 'SectionTermCont'
          })

          .state('main.Com003', {
              url: "/ComnModule",
              templateUrl: 'app/modules/Setup/views/ComnModule.html',
              controller: 'CommonModuleCont'
          })

          .state('main.Com001', {
              url: "/CommonApplication",
              templateUrl: 'app/modules/Setup/views/CommonApplication.html',
              controller: 'CommonApplicationCont'
          })




          .state('main.Sim089', {
              url: "/StudentSectionSubject",
              templateUrl: 'app/modules/Setup/views/StudentSectionSubject.html',
              controller: 'StudentSectionSubjectCont'
          })

          .state('main.Mog001', {
              url: "/SchoolLicence",
              templateUrl: 'app/modules/SchoolSetup/views/SchoolLicence.html',
              controller: 'SchoolLicenceCont'
          })

                .state('main.Sim502', {
                    url: "/DefineStudentFee",
                    templateUrl: 'app/modules/Fee/views/DefineStudentFee.html',
                    controller: 'DefineStudentFeeCont'
                })

        //  //END

        //  //kk

          .state('main.EmpSht', {
              url: "/CreateEditEmployee",
              templateUrl: 'app/modules/HRMS/views/CreateEditEmployee.html',
              controller: 'CreateEditEmployeeCont'
          })

             .state('main.STerm', {
                 url: "/MedicalStudentTermDetail",
                 templateUrl: 'app/modules/Health/views/MedicalStudentTermDetail.html',
                 controller: 'MedicalStudentTermDetailCont'
             })

               .state('main.UpdateEmployee', {
                   url: "/UpdateEmployee",
                   templateUrl: 'app/modules/HRMS/views/UpdateEmployee.html',
                   controller: 'UpdateEmployeeCont'
               })

                .state('main.Sim015', {
                    url: "/SchoolCalender",
                    templateUrl: 'app/modules/SchoolSetup/views/SchoolCalender.html',
                    controller: 'SchoolCalenderController'
                })
               .state('main.StaffCalendar', {
                   url: "/StaffCalendar",
                   templateUrl: 'app/modules/SchoolSetup/views/StaffCalendar.html',
                   controller: 'StaffCalenderCont'
               })

             .state('main.Sim548', {
                 url: "/StudLeaveApprove",
                 templateUrl: 'app/modules/SchoolSetup/views/StudLeaveApprove.html',
                 controller: 'StudLeaveApproveCont'
             })

            .state('main.result', {
                url: "/GenerateResult",
                templateUrl: 'app/modules/Gradebook/views/GenerateResult.html',
                controller: 'ResultGenerationCont'
            })

             .state('main.Sim540', {
                 url: "/StaffCalendarNew",
                 templateUrl: 'app/modules/SchoolSetup/views/StaffCalenderNew.html',
                 controller: 'StaffCalenderNewCont'
             })

          .state('main.Per099', {
              url: "/EmployeeMaster",
              templateUrl: 'app/modules/HRMS/views/EmployeeMaster.html',
              controller: 'EmployeeMasterCont'

          })

             .state('main.EmpOES', {
                 url: "/EmployeeMasterOES",
                 templateUrl: 'app/modules/HRMS/views/EmployeeMasterOES.html',
                 controller: 'EmployeeMasterOESCont'

             })
               .state('main.Per234', {
                   url: "/Designation",
                   templateUrl: 'app/modules/HRMS/views/Designation.html',
                   controller: 'DesignationCont'
               })

          .state('main.Per229', {
              url: "/Destination",
              templateUrl: 'app/modules/HRMS/views/Destination.html',
              controller: 'DestinationCont'
          })

           .state('main.Per258', {
               url: "/QualificationEmployee",
               templateUrl: 'app/modules/HRMS/views/QualificationEmployee.html',
               controller: 'QualificationEmployeeCont'
           })


              .state('main.Sim019', {
                  url: "/DocumentDetails",
                  templateUrl: 'app/modules/Student/views/DocumentDetails.html',
                  controller: 'DocumentDetailsCont'
              })

              .state('main.Sim020', {
                  url: "/EmployeeDocumentMaster",
                  templateUrl: 'app/modules/HRMS/views/EmployeeDocumentMaster.html',
                  controller: 'EmployeeDocumentMasterCont'
              })

        .state('main.Sim539', {
            url: "/AgendaView",
            templateUrl: 'app/modules/Student/views/AgendaView.html',
            controller: 'AgendaViewCont'
        })

            .state('main.Sim195', {
                url: "/AgendaConfiguration",
                templateUrl: 'app/modules/Student/views/AgendaConfiguration.html',
                controller: 'AgendaConfigurationCont'
            })

            .state('main.Per235', {
                url: "/PaysParameter",
                templateUrl: 'app/modules/HRMS/views/PaysParameter.html',
                controller: 'PaysParameterCont'
            })

              .state('main.Sim099', {
                  url: "/MedicalStudentSusceptibility",
                  templateUrl: 'app/modules/Health/views/medicalStudentSusceptibility.html',
                  controller: 'MedicalStudentSusceptibilityCont'
              })

                .state('main.Sim098', {
                    url: "/StudentHealth",
                    templateUrl: 'app/modules/Health/views/StudentHealth.html',
                    controller: 'StudentHealthCont'
                })

             .state('main.Fin149', {
                 url: "/FinnParameter",
                 templateUrl: 'app/modules/Finance/views/FinnParameter.html',
                 controller: 'FinnParameterCont'
             })


              .state('main.Fin226', {
                  url: "/PettyCashDocument",
                  templateUrl: 'app/modules/Finance/views/PettyCashDocument.html',
                  controller: 'PettyCashDocumentCont'
              })

            .state('main.Fin227', {
                url: "/PettyCashMasters",
                templateUrl: 'app/modules/Finance/views/PettyCashMasters.html',
                controller: 'PettyCashMastersCont'
            })

              .state('main.Fin229', {
                  url: "/PettyCashApprove",
                  templateUrl: 'app/modules/Finance/views/PettyCashApprove.html',
                  controller: 'PettyCashApproveCont'
              })


            .state('main.Fin123', {
                url: "/SLQuery",
                templateUrl: 'app/modules/Finance/views/SLQuery.html',
                controller: 'SLQueryCont'
            })

            .state('main.Fin123_DPS', {
                url: "/SLQuery_DPS",
                templateUrl: 'app/modules/Finance/views/SLQuery_DPS.html',
                controller: 'SLQuery_DPSCont'
            })

              .state('main.Ucw240', {
                  url: "/Achievement",
                  templateUrl: 'app/modules/HRMS/views/Achievement.html',
                  controller: 'AchievementCont'
              })

             .state('main.Ucw242', {
                 url: "/GoalTarget",
                 templateUrl: 'app/modules/HRMS/views/GoalTarget.html',
                 controller: 'GoalTargetCont'
             })

             .state('main.Ucw243', {
                 url: "/GoalTargetKPI",
                 templateUrl: 'app/modules/HRMS/views/GoalTargetKPI.html',
                 controller: 'GoalTargetKPICont'
             })

               .state('main.Ucw244', {
                   url: "/GoalTargetKPIMeasure",
                   templateUrl: 'app/modules/HRMS/views/GoalTargetKPIMeasure.html',
                   controller: 'GoalTargetKPIMeasureCont'
               })

              .state('main.Per305', {
                  url: "/GoalTargetKPIMeasureAchievement",
                  templateUrl: 'app/modules/HRMS/views/GoalTargetKPIMeasureAchievement.html',
                  controller: 'GoalTargetKPIMeasureAchievementCont'
              })

                  .state('main.Per302', {
                      url: "/SipEvaluationSchedule",
                      templateUrl: 'app/modules/HRMS/views/SipEvaluationSchedule.html',
                      controller: 'SipEvaluationScheduleCont'
                  })
        ///end




               .state('main.Com010', {
                   url: "/RoleApplication",
                   templateUrl: 'app/modules/Setup/views/RoleApplication.html',
                   controller: 'RoleApplicationCont'
               })

              .state('main.Com006', {
                  url: "/UserApplications",
                  templateUrl: 'app/modules/Setup/views/UserApplications.html',
                  controller: 'UserApplicationsCont'
              })

                          .state('main.ApplicationModule', {
                              url: "/ApplicationModule",
                              templateUrl: 'app/modules/Setup/views/ApplicationModule.html',
                              controller: 'ApplicationModuleCont'
                          })




          .state('main.Sim042', {
              url: "/StudentDatabase",
              templateUrl: 'app/modules/Student/views/StudentDatabase.html',
              controller: 'StudentDatabaseCont'
          })

            .state('main.Sim680', {
                url: "/StudentDatabaseVal",
                templateUrl: 'app/modules/Student/views/StudentDatabaseVal.html',
                controller: 'StudentDatabaseValCont'
            })

              .state('main.Sim039', {
                  url: "/SectionSubject",
                  templateUrl: 'app/modules/SchoolSetup/views/SectionSubject.html',
                  controller: 'SectionSubjectCont'
              })

              .state('main.Sim179', {
                  url: "/ClassTeacherMapping",
                  templateUrl: 'app/modules/SchoolSetup/views/ClassTeacherMapping.html',
                  controller: 'ClassTeacherMappingCont'
              })
              .state('main.Sim173', {
                  url: "/EmployeeTeacherCreation",
                  templateUrl: 'app/modules/HRMS/views/EmployeeTeacherCreation.html',
                  controller: 'EmployeeTeacherCreationCont'
              })

               .state('main.Sim174', {
                   url: "/StudentAttendanceMonthly",
                   templateUrl: 'app/modules/Attendance/views/StudentAttendanceMonthly.html',
                   controller: 'StudentAttendanceMonthlyCont'
               })

          .state('main.Comn005', {
              url: "/CreateEditUser",
              templateUrl: 'app/modules/Setup/views/CreateEditUser.html',
              controller: 'CreateEditUserCont'
          })

           .state('main.Sim029', {
               url: "/House",
               templateUrl: 'app/modules/SchoolSetup/views/House.html',
               controller: 'HouseCont'
           })

         .state('main.Sim168', {
             url: "/HouseAllocation",
             templateUrl: 'app/modules/Student/views/HouseAllocation.html',
             controller: 'HouseAllocationCont'
         })

          .state('main.Sim533', {
              url: "/FeeTerm",
              templateUrl: 'app/modules/Fee/views/FeeTerm.html',
              controller: 'FeeTermCont'
          })

               .state('main.Sim061', {
                   url: "/Bell",
                   templateUrl: 'app/modules/Scheduling/views/Bell.html',
                   controller: 'BellCont'
               })

          .state('main.Sim062', {
              url: "/BellSlotGroup",
              templateUrl: 'app/modules/Scheduling/views/BellSlotGroup.html',
              controller: 'BellSlotGroupCont'
          })

        .state('main.Sim063', {
            url: "/BellSlotRoom",
            templateUrl: 'app/modules/Scheduling/views/BellSlotRoom.html',
            controller: 'BellSlotRoomCont'
        })

         .state('main.Sim064', {
             url: "/BellDay",
             templateUrl: 'app/modules/Scheduling/views/BellDay.html',
             controller: 'BellDayCont'
         })

         .state('main.Sim065', {
             url: "/BellDetails",
             templateUrl: 'app/modules/Scheduling/views/BellDetails.html',
             controller: 'BellDetailsCont'
         })

        .state('main.Sim060', {
            url: "/BellSectionSubjectTeacher",
            templateUrl: 'app/modules/Scheduling/views/BellSectionSubjectTeacher.html',
            controller: 'BellSectionSubjectTeacherCont'
        })


             //.state('main.Sim086', {
             //    url: "/TransportVehicleDetails",
             //    templateUrl: 'app/modules/Fleet/views/TransportVehicleDetails.html',
             //    controller: 'TransportVehicleDetailsCont'
             //})


          //.state('main.Sim083', {
          //    url: "/TransportRouteDetails",
          //    templateUrl: 'app/modules/Fleet/views/TransportRouteDetails.html',
          //    controller: 'TransportRouteDetailsCont'
          //})

               .state('main.Sim083', {
                   url: "/TransportRoute",
                   templateUrl: 'app/modules/Fleet/views/TransportRoute.html',
                   controller: 'TransportRouteCont'
               })

             .state('main.Sim084', {
                 url: "/TransportStop",
                 templateUrl: 'app/modules/Fleet/views/TransportStop.html',
                 controller: 'TransportStopCont'
             })

               .state('main.Sim085', {
                   url: "/TransportRouteStop",
                   templateUrl: 'app/modules/Fleet/views/TransportRouteStop.html',
                   controller: 'TransportRouteStopCont'
               })


               .state('main.vc0001', {
                   url: "/VehicleConsumption",
                   templateUrl: 'app/modules/Fleet/views/VehicleConsumption.html',
                   controller: 'VehicleConsumptionCont'
               })

               .state('main.Sim086', {
                   url: "/TransportVehicle",
                   templateUrl: 'app/modules/Fleet/views/TransportVehicle.html',
                   controller: 'TransportVehicleCont'
               })

             .state('main.Sim087', {
                 url: "/TransportCaretaker",
                 templateUrl: 'app/modules/Fleet/views/TransportCaretaker.html',
                 controller: 'TransportCaretakerCont'
             })

             .state('main.Sim088', {
                 url: "/TransportDriver",
                 templateUrl: 'app/modules/Fleet/views/TransportDriver.html',
                 controller: 'TransportDriverCont'
             })

               .state('main.Sim079', {
                   url: "/TransportFees",
                   templateUrl: 'app/modules/Fleet/views/TransportFees.html',
                   controller: 'TransportFeesCont'
               })
               .state('main.Sim519', {
                   url: "/ApproveStudentTransport",
                   templateUrl: 'app/modules/Fleet/views/ApproveStudentTransport.html',
                   controller: 'ApproveStudentTransportCont'
               })

              .state('main.Sim081', {
                  url: "/TransportRouteStudent",
                  templateUrl: 'app/modules/Fleet/views/TransportRouteStud.html',
                  controller: 'TransportRouteStudCont'
              })

             .state('main.Sim525', {
                 url: "/TransportCancelUpdate",
                 templateUrl: 'app/modules/Fleet/views/TransportCancelUpdate.html',
                 controller: 'TransportCancelUpdateCont'
             })
                //.state('main.Sim519', {
                //    url: "/ApproveStudentTransport",
                //    templateUrl: 'app/modules/Fleet/views/ApproveStudentTransport.html',
                //    controller: 'ApproveStudentTransportCont'
                //})

              .state('main.Sim182', {
                  url: "/CouncilMaster",
                  templateUrl: 'app/modules/Setup/views/CouncilMaster.html',
                  controller: 'CouncilMasterCont'
              })

               .state('main.Sim189', {
                   url: "/CouncilStudentMapping",
                   templateUrl: 'app/modules/Setup/views/CouncilStudentMapping.html',
                   controller: 'CouncilStudentMappingCont'
               })

         .state('main.Sim056', {
             url: "/CityMaster",
             templateUrl: 'app/modules/Setup/views/CityMaster.html',
             controller: 'CityMasterCont'
         })


        .state('main.Sim162', {
            url: "/Building",
            templateUrl: 'app/modules/Setup/views/Building.html',
            controller: 'BuildingCont'
        })

         .state('main.Sim163', {
             url: "/ILocation",
             templateUrl: 'app/modules/Setup/views/ILocation.html',
             controller: 'ILocationCont'
         })

             .state('main.Per244', {
                 url: "/CommonCode",
                 templateUrl: 'app/modules/HRMS/views/CommonCode.html',
                 controller: 'CommonCodeCont'
             })

         .state('main.Per058', {
             url: "/PayCode",
             templateUrl: 'app/modules/HRMS/views/PayCode.html',
             controller: 'PayCodeCont'
         })

            .state('main.Per066', {
                url: "/PayCode_1",
                templateUrl: 'app/modules/HRMS/views/PayCode_1.html',
                controller: 'PayCode_1Cont'
            })

               .state('main.Per130', {
                   url: "/LoanAmount",
                   templateUrl: 'app/modules/HRMS/views/LoanAmount.html',
                   controller: 'LoanAmountCont'
               })

               .state('main.Per131', {
                   url: "/LoanCode",
                   templateUrl: 'app/modules/HRMS/views/LoanCode.html',
                   controller: 'LoanCodeCont'
               })



        .state('main.Per134', {
            url: "/LoanRegister",
            templateUrl: 'app/modules/HRMS/views/LoanRegister.html',
            controller: 'LoanRegisterCont'
        })

         .state('main.Per132', {
             url: "/LoanConfirmation",
             templateUrl: 'app/modules/HRMS/views/LoanConfirmation.html',
             controller: 'LoanConfirmationCont'
         })


         .state('main.Per135', {
             url: "/ManagerConfirmation",
             templateUrl: 'app/modules/HRMS/views/ManagerConfirmation.html',
             controller: 'ManagerConfirmationCont'
         })


        .state('main.Sim055', {
            url: "/State",
            templateUrl: 'app/modules/Setup/views/State.html',
            controller: 'StateCont'
        })

          .state('main.Sim057', {
              url: "/Region",
              templateUrl: 'app/modules/Setup/views/Region.html',
              controller: 'RegionCont'
          })

        .state('main.Sim002', {
            url: "/Country",
            templateUrl: 'app/modules/Setup/views/Country.html',
            controller: 'CountryCont'
        })

        .state('main.Sim006', {
            url: "/Ethnicity",
            templateUrl: 'app/modules/Setup/views/Ethnicity.html',
            controller: 'EthnicityCont'
        })

         .state('main.Sim007', {
             url: "/Language",
             templateUrl: 'app/modules/Setup/views/Language.html',
             controller: 'LanguageCont'
         })


        .state('main.Sim040', {
            url: "/CreateEditSibling",
            templateUrl: 'app/modules/Setup/views/CreateEditSibling.html',
            controller: 'CreateEditSiblingCont'
        })

        .state('main.Sim161', {
            url: "/HomeroomTeacher",
            templateUrl: 'app/modules/Setup/views/HomeroomTeacher.html',
            controller: 'HomeroomTeacherCont'
        })

        .state('main.Sim160', {
            url: "/HomeroomStudent",
            templateUrl: 'app/modules/Setup/views/HomeroomStudent.html',
            controller: 'HomeroomStudentCont'
        })

            .state('main.Sim500', {
                url: "/ClassWiseAttendance",
                templateUrl: 'app/modules/Attendance/views/ClassWiseAttendance.html',
                controller: 'ClassWiseAttendanceCont'
            })

             .state('main.Sim156', {
                 url: "/AttendanceCode",
                 templateUrl: 'app/modules/Attendance/views/AttendanceCode.html',
                 controller: 'AttendanceCodeCont'
             })

             .state('main.Sim025', {
                 url: "/CalendarException",
                 templateUrl: 'app/modules/SchoolSetup/views/CalendarException.html',
                 controller: 'CalendarExceptionCont'
             })

              .state('main.EmpDshBrd', {
                  url: "/EmpDashboard",
                  templateUrl: 'app/modules/HRMS/views/EmpDashboard.html',
                  controller: 'EmpDashboardCont'
              })



        .state('main.Sim158', {
            url: "/HomeRoom",
            templateUrl: 'app/modules/Setup/views/HomeRoom.html',
            controller: 'HomeRoomCont'
        })

                    .state('main.Sim611', {
                        url: "/MenuDragDrop",
                        templateUrl: 'app/modules/Setup/views/MenuDragDrop.html',
                        controller: 'MenuDragDropCont'
                    })

                          //.state('main.Sim093', {
                          //    url: "/medicalExamination",
                          //    templateUrl: 'app/modules/Health/views/medicalExamination.html',
                          //    controller: 'medicalExaminationCont'

                          //})

               .state('main.Sim094', {
                   url: "/medicalImmunization",
                   templateUrl: 'app/modules/Health/views/medicalImmunization.html',
                   controller: 'medicalImmunizationCont'

               })

             .state('main.Sim095', {
                 url: "/medicalImmunizationGroup",
                 templateUrl: 'app/modules/Health/views/medicalImmunizationGroup.html',
                 controller: 'medicalImmunizationGroupCont'

             })


             .state('main.Sim097', {
                 url: "/medicalMedicine",
                 templateUrl: 'app/modules/Health/views/medicalMedicine.html',
                 controller: 'medicalMedicineCont'

             })

                .state('main.Sim100', {
                    url: "/medicalSusceptibility",
                    templateUrl: 'app/modules/Health/views/medicalSusceptibility.html',
                    controller: 'medicalSusceptibilityCont'

                })
              .state('main.Sim102', {
                  url: "/medicalVisitExamination",
                  templateUrl: 'app/modules/Health/views/medicalVisitExamination.html',
                  controller: 'medicalVisitExaminationCont'

              })

            .state('main.Sim103', {
                url: "/medicalVisitMedicine",
                templateUrl: 'app/modules/Health/views/medicalVisitMedicine.html',
                controller: 'medicalVisitMedicineCont'

            })


              .state('main.Sim096', {
                  url: "/medicalImmunizationStudent",
                  templateUrl: 'app/modules/Health/views/medicalImmunizationStudent.html',
                  controller: 'medicalImmunizationStudentCont'

              })

             .state('main.sim536', {
                 url: "/SubjectwisetAttendance",
                 templateUrl: 'app/modules/Attendance/views/SubjectwisetAttendance.html',
                 controller: 'SubjectwisetAttendanceCont'
             })

               .state('main.Com019', {
                   url: "/UserApplicationsFins",
                   templateUrl: 'app/modules/Setup/views/UserApplicationsFins.html',
                   controller: 'UserApplicationsFinsCont'
               })


         .state('main.Sim070', {
             url: "/Gradebook",
             templateUrl: 'app/modules/Gradebook/views/Gradebook.html',
             controller: 'GradebookCont'
         })

             .state('main.SubTrt', {
                 url: "/TraitMaster",
                 templateUrl: 'app/modules/Gradebook/views/TraitMaster.html',
                 controller: 'TraitMasterCont'
             })

            .state('main.StuTrt', {
                url: "/StudentTrait",
                templateUrl: 'app/modules/Gradebook/views/StudentTrait.html',
                controller: 'StudentTraitCont'
            })



        .state('main.Per060', {
            url: "/Contract",
            templateUrl: 'app/modules/HRMS/views/Contract.html',
            controller: 'ContractCont'
        })

         ///// Incidence ////

            .state('main.Sim151', {
                url: "/ActionCategory",
                templateUrl: 'app/modules/Incidence/views/ActionCategory.html',
                controller: 'ActionCategoryCont'
            })

         .state('main.Sim150', {
             url: "/IncidenceAction",
             templateUrl: 'app/modules/Incidence/views/IncidenceAction.html',
             controller: 'IncidenceActionCont'
         })
        .state('main.Sim152', {
            url: "/IncidenceConsequence",
            templateUrl: 'app/modules/Incidence/views/IncidenceConsequence.html',
            controller: 'IncidenceConsequenceCont'
        })
        .state('main.Sim149', {
            url: "/Incidence",
            templateUrl: 'app/modules/Incidence/views/Incidence.html',
            controller: 'IncidenceCont'
        })
        .state('main.Sim153', {
            url: "/IncidenceTimeline",
            templateUrl: 'app/modules/Incidence/views/IncidenceTimeline.html',
            controller: 'IncidenceTimelineCont'
        })
         .state('main.Sim154', {
             url: "/IncidenceWarning",
             templateUrl: 'app/modules/Incidence/views/IncidenceWarning.html',
             controller: 'IncidenceWarningCont'
         })


         .state('main.Per309', {
             url: "/InActiveUser",
             templateUrl: 'app/modules/HRMS/views/InActiveUser.html',
             controller: 'InActiveUserCont'
         })

            .state('main.PerAF', {
                url: "/AirFare",
                templateUrl: 'app/modules/HRMS/views/AirFare.html',
                controller: 'AirFareCont'
            })
              .state('main.PerAFE', {
                  url: "/AirFareEmployee",
                  templateUrl: 'app/modules/HRMS/views/AirFareEmployee.html',
                  controller: 'AirFareEmployeeCont'
              })
            .state('main.Per092', {
                url: "/ShiftMaster",
                templateUrl: 'app/modules/HRMS/views/ShiftMaster.html',
                controller: 'ShiftMasterCont'
            })
            .state('main.Per148', {
                url: "/ShiftTemplate",
                templateUrl: 'app/modules/HRMS/views/ShiftTemplate.html',
                controller: 'ShiftTemplateCont'
            })
             .state('main.Per104', {
                 url: "/EmployeeShift",
                 templateUrl: 'app/modules/HRMS/views/EmployeeShift.html',
                 controller: 'EmployeeShiftCont'
             })

            .state('main.Per109', {
                url: "/PaysGradeChange",
                templateUrl: 'app/modules/HRMS/views/PaysGradeChange.html',
                controller: 'PaysGradeChangeCont'
            })

                        .state('main.Per326', {
                            url: "/EmpAttendanceDaily",
                            templateUrl: 'app/modules/HRMS/views/EmpAttendanceDaily.html',
                            controller: 'EmpAttendanceDailyCont'
                        })

                        .state('main.Per322', {
                            url: "/AttendanceMachine",
                            templateUrl: 'app/modules/HRMS/views/AttendanceMachine.html',
                            controller: 'AttendanceMachineCont'
                        })


            /**********************************************/

            .state('main.Per315', {
                url: "/PaysAttendanceCode",
                templateUrl: 'app/modules/HRMS/views/PaysAttendanceCode.html',
                controller: 'PaysAttendanceCodeCont'
            })


             .state('main.Fin151', {
                 url: "/AssetMasterView",
                 templateUrl: 'app/modules/Finance/views/AssetMasterView.html',
                 controller: 'AssetMasterViewCont'
             })

         .state('main.Fin001', {
             url: "/CreateEditAccountCode",
             templateUrl: 'app/modules/Finance/views/CreateEditAccountCode.html',
             controller: 'CreateEditAccountCodeCont'
         })

         .state('main.Fin014', {
             url: "/GLAccountsCreateView",
             templateUrl: 'app/modules/Finance/views/GLAccountsCreateView.html',
             controller: 'GLAccountsCreateViewCont'
         })

            .state('main.Per313', {
                url: "/EmployeeAttendanceMonthly",
                templateUrl: 'app/modules/Attendance/views/EmployeeAttendanceMonthly.html',
                controller: 'EmployeeAttendanceMonthlyCont'
            })

          .state('main.Fin118', {
              url: "/Schedule",
              templateUrl: 'app/modules/Finance/views/Schedule.html',
              controller: 'ScheduleCont'
          })

            .state('main.Fin143', {
                url: "/SubledgerMatching",
                templateUrl: 'app/modules/Finance/views/SubledgerMatching.html',
                controller: 'SubledgerMatchingCont'
            })

           .state('main.Fin108', {
               url: "/PaymentTerm",
               templateUrl: 'app/modules/Finance/views/PaymentTerm.html',
               controller: 'PaymentTermCont'
           })

           .state('main.Sim115', {
               url: "/LibraryAttribute",
               templateUrl: 'app/modules/Library/views/LibraryAttribute.html',
               controller: 'LibraryAttributeCont'
           })


           .state('main.Sim117', {
               url: "/LibraryCatalogueAttribute",
               templateUrl: 'app/modules/Library/views/LibraryCatalogueAttribute.html',
               controller: 'LibraryCatalogueAttributeCont'
           })

 		.state('main.Sim119', {
 		    url: "/LibraryCatalogue",
 		    templateUrl: 'app/modules/Library/views/LibraryCatalogue.html',
 		    controller: 'LibraryCatalogueCont'
 		})

             .state('main.Sim120', {
                 url: "/LibraryFee",
                 templateUrl: 'app/modules/Library/views/LibraryFee.html',
                 controller: 'LibraryFeeCont'
             })

             .state('main.Sim118', {
                 url: "/LibraryCategory",
                 templateUrl: 'app/modules/Library/views/LibraryCategory.html',
                 controller: 'LibraryCategoryCont'
             })

            .state('main.Sim121', {
                url: "/LibraryItemMaster",
                templateUrl: 'app/modules/Library/views/LibraryItemMaster.html',
                controller: 'LibraryItemMasterCont',
                params: { 'ob': {} }
            })

            .state('main.Sim682', {
                url: "/LibraryItemSearch",
                templateUrl: 'app/modules/Library/views/LibraryItemSearch.html',
                controller: 'LibraryItemSearchCont'
            })

              .state('main.Sim126', {
                  url: "/LibraryMembershipType",
                  templateUrl: 'app/modules/Library/views/LibraryMembershipType.html',
                  controller: 'LibraryMembershipTypeCont'
              })

            .state('main.Sim127', {
                url: "/LibraryPrivilege",
                templateUrl: 'app/modules/Library/views/LibraryPrivilege.html',
                controller: 'LibraryPrivilegeCont'
            })

            .state('main.Sim128', {
                url: "/LibraryItemRequest",
                templateUrl: 'app/modules/Library/views/LibraryItemRequest.html',
                controller: 'LibraryItemRequestCont'
            })

            .state('main.Sim129', {
                url: "/LibraryRequestDetail",
                templateUrl: 'app/modules/Library/views/LibraryRequestDetail.html',
                controller: 'LibraryRequestDetailCont'
            })

             .state('main.Sim123', {
                 url: "/LibraryLocation",
                 templateUrl: 'app/modules/Library/views/LibraryLocation.html',
                 controller: 'LibraryLocationCont'
             })

            .state('main.Sim675', {
                url: "/LibraryTxnHomeroom",
                templateUrl: 'app/modules/Library/views/LibraryTxnHomeroom.html',
                controller: 'LibraryTxnHomeroomCont'
            })

                  .state('main.Sim131', {
                      url: "/LibrarySubcategory",
                      templateUrl: 'app/modules/Library/views/LibrarySubcategory.html',
                      controller: 'LibrarySubcategoryCont'
                  })

            .state('main.Sim671', {
                url: "/LibraryEmpMembership",
                templateUrl: 'app/modules/Library/views/LibraryEmpMembership.html',
                controller: 'LibraryEmpMembershipCont'
            })

             .state('main.Fin060', {
                 url: "/FinancialDocument",
                 templateUrl: 'app/modules/Finance/views/FinancialDocument.html',
                 controller: 'FinancialDocumentCont'
             })

            .state('main.Fin060_DPS', {
                url: "/FinancialDocument_DPS",
                templateUrl: 'app/modules/Finance/views/FinancialDocument_DPS.html',
                controller: 'FinancialDocument_DPSCont'
            })

              .state('main.Sim515', {
                  url: "/FinancialYear",
                  templateUrl: 'app/modules/Finance/views/FinancialYear.html',
                  controller: 'FinancialYearCont'
              })


                  .state('main.Fin138', {
                      url: "/BankMaster",
                      templateUrl: 'app/modules/Finance/views/BankMaster.html',
                      controller: 'BankMasterCont'
                  })

             .state('main.Fin011', {
                 url: "/CreditEditDivision",
                 templateUrl: 'app/modules/Finance/views/CreditEditDivision.html',
                 controller: 'CreditEditDivisionCont'
             })

                 .state('main.Fin205', {
                     url: "/UserAccount",
                     templateUrl: 'app/modules/Finance/views/UserAccount.html',
                     controller: 'UserAccountCont'
                 })

                  .state('main.Fin207', {
                      url: "/FeePostingPaymentMode",
                      templateUrl: 'app/modules/Finance/views/FeePostingPaymentMode.html',
                      controller: 'FeePostingPaymentModeCont'
                  })


           .state('main.Fin069', {
               url: "/LedgerControl",
               templateUrl: 'app/modules/Finance/views/LedgerCntrl.html',
               controller: 'LedgerCntrlCont'
           })


            .state('main.FIN206', {
                url: "/DocumentUserAccount",
                templateUrl: 'app/modules/Finance/views/DocumentUserAccount.html',
                controller: 'DocumentUserAccountCont'
            })

            .state('main.Fin043', {
                url: "/CurrencyMaster",
                templateUrl: 'app/modules/Finance/views/CurrencyMaster.html',
                controller: 'CurrencyMasterCont'
            })
            .state('main.Fin003', {
                url: "/CompanyMaster",
                templateUrl: 'app/modules/Finance/views/CompanyMaster.html',
                controller: 'CompanyMasterCont'
            })

        .state('main.Sim116', {
            url: "/LibraryBin",
            templateUrl: 'app/modules/Library/views/LibraryBin.html',
            controller: 'LibraryBinCont'
        })

        .state('main.Sim122', {
            url: "/LibraryItemStyle",
            templateUrl: 'app/modules/Library/views/LibraryItemStyle.html',
            controller: 'LibraryItemStyleCont'
        })

            .state('main.Sim130', {
                url: "/LibraryStatus",
                templateUrl: 'app/modules/Library/views/LibraryStatus.html',
                controller: 'LibraryStatusCont'
            })

         .state('main.Sim134', {
             url: "/LibrarySubscriptionType",
             templateUrl: 'app/modules/Library/views/LibrarySubscriptionType.html',
             controller: 'LibrarySubscriptionTypeCont'
         })

         .state('main.Sim137', {
             url: "/LibraryTransactionType",
             templateUrl: 'app/modules/Library/views/LibraryTransactionType.html',
             controller: 'LibraryTransactionTypeCont'
         })

            .state('main.Sim503', {
                url: "/RenewLibrary",
                templateUrl: 'app/modules/Library/views/RenewLibrary.html',
                controller: 'RenewLibraryCont'
            })

            .state('main.Sim136', {
                url: "/LibraryTransactionDetail",
                templateUrl: 'app/modules/Library/views/LibraryTransactionDetail.html',
                controller: 'LibraryTransactionDetailCont'
            })

          .state('main.per059', {
              url: "/CompanyMaster",
              templateUrl: 'app/modules/HRMS/views/CompanyMaster.html',
              controller: 'CompanyMasterCont'
          })

          .state('main.Per137', {
              url: "/OvertimeHour",
              templateUrl: 'app/modules/HRMS/views/OvertimeHour.html',
              controller: 'OvertimeHourCont'
          })
              .state('main.Per138', {
                  url: "/OvertimeRate",
                  templateUrl: 'app/modules/HRMS/views/OvertimeRate.html',
                  controller: 'OvertimeRateCont'
              })


          .state('main.Sim143', {
              url: "/Co_Scholastic",
              templateUrl: 'app/modules/Gradebook/views/Co_Scholastic.html',
              controller: 'Co_ScholasticCont'
          })
          .state('main.Sim013', {
              url: "/ConcessionApproval",
              templateUrl: 'app/modules/Fee/views/ConcessionApproval.html',
              controller: 'ConcessionApprovalCont'
          })
         .state('main.Sm043C', {
             url: "/CFR",
             templateUrl: 'app/modules/Fee/views/CFR.html',
             controller: 'CFRCont'
         })

         .state('main.Sim43a', {
             url: "/FeeRefund",
             templateUrl: 'app/modules/Fee/views/FeeRefund.html',
             controller: 'FeeRefundCont'
         })

        .state('main.InvLPO', {
            url: "/InvLPO",
            templateUrl: 'app/modules/Inventory/views/InvLPO.html',
            controller: 'InvLPOCont'
        })

        .state('main.InvCSE', {
            url: "/InvCSE",
            templateUrl: 'app/modules/Inventory/views/CostingSheetEXP.html',
            controller: 'CostingSheetEXPCont'
        })

        .state('main.Inv042', {
            url: "/Bank",
            templateUrl: 'app/modules/Inventory/views/Bank.html',
            controller: 'BankCont'
        })

            .state('main.Inv006', {
                url: "/SupplierMaster",
                templateUrl: 'app/modules/Inventory/views/SupplierMaster.html',
                controller: 'SupplierMasterCont'
            })

            .state('main.Inv052', {
                url: "/InventorySection",
                templateUrl: 'app/modules/Inventory/views/Section.html',
                controller: 'SectionCont'
            })

            .state('main.Inv062', {
                url: "/InventoryLocation",
                templateUrl: 'app/modules/Inventory/views/Location.html',
                controller: 'LocationCont'
            })

            .state('main.Inv064', {
                url: "/TradeTerm",
                templateUrl: 'app/modules/Inventory/views/TradeTerm.html',
                controller: 'TradeTermCont'
            })
             .state('main.Com007', {
                 url: "/UserAudit",
                 templateUrl: 'app/modules/Security/views/UserAudit.html',
                 controller: 'UserAuditCont'
             })

        .state('main.Per78c', {
            url: "/Updatepaysheet",
            templateUrl: 'app/modules/HRMS/views/Updatepaysheet.html',
            controller: 'UpdatepaysheetCont'
        })
         .state('main.Sim43b', {
             url: "/Sims43bRefund",
             templateUrl: 'app/modules/Fee/views/Sims043Refund.html',
             controller: 'Sims043RefundCont'
         })


         .state('main.Com005', {
             url: "/CreateEditUser",
             templateUrl: 'app/modules/Setup/views/CreateEditUser.html',
             controller: 'CreateEditUserCont'
         })

              ///////////Amruta///////////////
              .state('main.sim041', {
                  url: "/SMTP",
                  templateUrl: 'app/modules/Setup/views/SMTP.html',
                  controller: 'SMTPCont'
              })
                    ///////////Amruta_End///////////////



            .state('main.PDCSubmission', {
                url: "/PDCSubmission",
                templateUrl: 'app/modules/Finance/views/PDCSubmission.html',
                controller: 'PDCSubmissionCont'
            })

              .state('main.Fn102T', {
                  url: "/PDCReSubmission",
                  templateUrl: 'app/modules/Finance/views/PDCReSubmission.html',
                  controller: 'PDCReSubmissionCont'
              })

                          .state('main.Fin150', {
                              url: "/JVCreate",
                              templateUrl: 'app/modules/Finance/views/JVCreate.html',
                              controller: 'JVCreateCont'
                          })

              .state('main.Fin150_DPS', {
                  url: "/JVCreate_DPS",
                  templateUrl: 'app/modules/Finance/views/JVCreate_DPS.html',
                  controller: 'JVCreate_DPSCont'
              })



            .state('main.Fin064', {
                url: "/GLQuery",
                templateUrl: 'app/modules/Finance/views/GLQuery.html',
                controller: 'GLQueryCont'
            })

              .state('main.Fn102R', {
                  url: "/PDCReturn",
                  templateUrl: 'app/modules/Finance/views/PDCReturn.html',
                  controller: 'PDCReturnCont'
              })


            .state('main.Fn102C', {
                url: "/PDCCancel",
                templateUrl: 'app/modules/Finance/views/PDCCancel.html',
                controller: 'PDCCancelCont'
            })


            .state('main.Fn102Z', {
                url: "/PDCRealised",
                templateUrl: 'app/modules/Finance/views/PDCRealised.html',
                controller: 'PDCRealisedCont'
            })



        .state('main.Sim550', {
            url: "/Class Wise Fee Collection",
            templateUrl: 'app/modules/Fee/views/ClassWiseFeeCollection.html',
            controller: 'ClassWiseFeeCollectionCont'
        })
        ////////////////////////END//////////////
        ////////////Ganesh////////////////

              ///////// ////Ganesh/////////////////////////

        .state('main.Sim565', {
            url: "/ProspectFees",
            templateUrl: 'app/modules/Student/views/ProspectFees.html',
            controller: 'ProspectFeesCont'
        })

           .state('main.Sim521', {
               url: "/AdmissionFees",
               templateUrl: 'app/modules/Student/views/AdmissionFees.html',
               controller: 'AdmissionFeesCont'
           })

            .state('main.Sim563', {
                url: "/AdmissionFee",
                templateUrl: 'app/modules/Student/views/AdmissionFee.html',
                controller: 'AdmissionFeeCont'
            })

          .state('main.Sim105', {
              url: "/IssueCertificate",
              templateUrl: 'app/modules/Student/views/IssueCertificate.html',
              controller: 'IssueCertificateCont'
          })

         .state('main.Com012', {
             url: "/Exceptions",
             templateUrl: 'app/modules/Common/views/Exceptions.html',
             controller: 'ExceptionsCont'
         })


        .state('main.Com050', {
            url: "/AlertTransaction",
            templateUrl: 'app/modules/Common/views/AlertTransaction.html',
            controller: 'AlertTransactionCont'
        })

        /////////////END/////////////


        .state('main.Sim078', {
            url: "/album",
            templateUrl: 'app/modules/Setup/views/album.html',
            controller: 'AlbumController'
        })

 .state('main.albmV', {
     url: "/albumView",
     templateUrl: 'app/modules/Setup/views/albumView.html',
     controller: 'AlbumController'
 })
            //.state('main.EmpDocMaster', {
            //    url: "/EmployeeDocumentMaster",
            //    templateUrl: 'app/modules/HRMS/views/EmployeeDocumentMaster.html',
            //    controller: 'EmployeeDocumentMasterCont'
            //})





        .state('main.Sim541', {
            url: "/FeeInvoice",
            templateUrl: 'app/modules/Fee/views/FeeInvoice.html',
            controller: 'FeeInvoiceCont'
        })

         .state('main.Fin141', {
             url: "/BankPayment",
             templateUrl: 'app/modules/Finance/views/BankPayment.html',
             controller: 'BankPaymentCont'
         })

        .state('main.Fin141_DPS', {
            url: "/BankPayment_DPS",
            templateUrl: 'app/modules/Finance/views/BankPayment_DPS.html',
            controller: 'BankPayment_DPSCont'
        })

        ///ganesh///
        .state('main.Fin212', {
            url: "/PDCBills",
            templateUrl: 'app/modules/Finance/views/PDCBills.html',
            controller: 'PDCBillsCont'
        })

            //pooja
          .state('main.Com052', {
              url: "/EmailSmsToParent",
              templateUrl: 'app/modules/Collaboration/views/EmailSmsToParent.html',
              controller: 'EmailSmsToParentCont'
          })

            .state('main.Per306', {
                url: "/MarkEmpResignation",
                templateUrl: 'app/modules/HRMS/views/MarkEmpResignation.html',
                controller: 'MarkEmpResignationCont'
            })

             .state('main.Per323', {
                 url: "/GratuityCriteria",
                 templateUrl: 'app/modules/HRMS/views/GratuityCriteria.html',
                 controller: 'GratuityCriteriaCont'
             })

             .state('main.Per324', {
                 url: "/GratuityCalculation",
                 templateUrl: 'app/modules/HRMS/views/GratuityCalculation.html',
                 controller: 'GratuityCalculationCont'
             })

             .state('main.Per233', {
                 url: "/PaysGrade",
                 templateUrl: 'app/modules/HRMS/views/PaysGrade.html',
                 controller: 'PaysGradeCont'
             })

        //ganesh
        .state('main.Fin213', {
            url: "/PdcPaymentSubmission",
            templateUrl: 'app/modules/Finance/views/PdcPaymentSubmission.html',
            controller: 'PdcPaymentSubmissionCont'
        })

             .state('main.Fin214', {
                 url: "/PdcPaymentReSubmission",
                 templateUrl: 'app/modules/Finance/views/PdcPaymentReSubmission.html',
                 controller: 'PdcPaymentResubmissionCont'
             })


         .state('main.Inv118', {
             url: "/SupplierDepartments",
             templateUrl: 'app/modules/Inventory/views/SupplierDepartments.html',
             controller: 'SupplierDepartmentsCont'
         })

    .state('main.Inv011', {
        url: "/supplierGroup",
        templateUrl: 'app/modules/Inventory/views/supplierGroup.html',
        controller: 'supplierGroupCont'
    })



    .state('main.Inv017', {
        url: "/deliveryMode",
        templateUrl: 'app/modules/Inventory/views/deliveryMode.html',
        controller: 'deliveryMode'
    })


        .state('main.Sim110', {
            url: "/CertificateUserGroup",
            templateUrl: 'app/modules/Student/views/CertificateUserGroup.html',
            controller: 'CertificateUserGroupCont'
        })

        .state('main.Sim176', {
            url: "/StudentReport",
            templateUrl: 'app/modules/Student/views/Studentreport.html',
            controller: 'StudentreportCont'
        })

        //Ganesh
        .state('main.Fin215', {
            url: "/PdcPaymentReturn",
            templateUrl: 'app/modules/Finance/views/PdcPaymentReturn.html',
            controller: 'PdcPaymentReturnCont'
        })
            .state('main.Fin216', {
                url: "/PdcPaymentCancel",
                templateUrl: 'app/modules/Finance/views/PdcPaymentCancel.html',
                controller: 'PdcPaymentCancelCont'
            })
             .state('main.Fin217', {
                 url: "/PdcPaymentRealize",
                 templateUrl: 'app/modules/Finance/views/PdcPaymentRealize.html',
                 controller: 'PdcPaymentRealizeCont'
             })

            .state('main.Fin209', {
                url: "/VoucherReversal",
                templateUrl: 'app/modules/Finance/views/VoucherReversal.html',
                controller: 'VoucherReversalCont'
            })

             .state('main.Fin231', {
                 url: "/PDCReversal",
                 templateUrl: 'app/modules/Finance/views/PDCReversal.html',
                 controller: 'PDCReversalCont'
             })

         .state('main.Fin099', {
             url: "/PDCRececipt",
             templateUrl: 'app/modules/Finance/views/PDCReceipt(BankReceipt).html',
             controller: 'PDCReceipt(BankReceipt)Cont'
         })

         .state('main.Fin152', {
             url: "/CashPayment",
             templateUrl: 'app/modules/Finance/views/CashPayment.html',
             controller: 'CashPaymentCont'
         })

         .state('main.Fin153', {
             url: "/CashReceipt",
             templateUrl: 'app/modules/Finance/views/CashReceipt.html',
             controller: 'CashReceiptCont'
         })
           .state('main.Fin145', {
               url: "/BankReceipt",
               templateUrl: 'app/modules/Finance/views/BankReceipt.html',
               controller: 'BankReceiptCont'
           })

             .state('main.Fin152_DPS', {
                 url: "/CashPayment_DPS",
                 templateUrl: 'app/modules/Finance/views/CashPayment_DPS.html',
                 controller: 'CashPayment_DPSCont'
             })

         .state('main.Fin153_DPS', {
             url: "/CashReceipt_DPS",
             templateUrl: 'app/modules/Finance/views/CashReceipt_DPS.html',
             controller: 'CashReceipt_DPSCont'
         })
           .state('main.Fin145_DPS', {
               url: "/BankReceipt_DPS",
               templateUrl: 'app/modules/Finance/views/BankReceipt_DPS.html',
               controller: 'BankReceipt_DPSCont'
           })

            .state('main.Fin157_DPS', {
                url: "/PC_Disbursement_DPS",
                templateUrl: 'app/modules/Finance/views/PC_Disbursement_DPS.html',
                controller: 'PC_Disbursement_DPSCont'
            })

            .state('main.Fin158_DPS', {
                url: "/PC_Reimbursement_DPS",
                templateUrl: 'app/modules/Finance/views/PC_Reimbursement_DPS.html',
                controller: 'PC_Reimbursement_DPSCont'
            })

             .state('main.Fin230', {
                 url: "/PettyCash",
                 templateUrl: 'app/modules/Finance/views/Petty_Cash_DPS.html',
                 controller: 'Petty_Cash_DPSCont'
             })

         .state('main.Sim139', {
             url: "/ReportCardAllocation",
             templateUrl: 'app/modules/ReportCard/views/ReportCardAllocation.html',
             controller: 'ReportCardAllocationCont'
         })
            .state('main.Fin156', {
                url: "/UpdateVoucher",
                templateUrl: 'app/modules/Finance/views/UpdateVoucher.html',
                controller: 'UpdateVoucherCont'
            })
            .state('main.Fin156_DPS', {
                url: "/UpdateVoucher_DPS",
                templateUrl: 'app/modules/Finance/views/UpdateVoucher_DPS.html',
                controller: 'UpdateVoucher_DPSCont'
            })
             .state('main.MO', {
                 url: "/ManageOrders",
                 templateUrl: 'app/modules/inventory/views/ManageOrder.html',
                 controller: 'ManageOrderCont'
             })

             .state('main.Invs005', {
                 url: "/DeliveryReasons",
                 templateUrl: 'app/modules/Inventory/views/DeliveryReasons.html',
                 controller: 'DeliveryReasonsCont'
             })

             .state('main.Inv124', {
                 url: "/InvsParameter",
                 templateUrl: 'app/modules/Inventory/views/InvsParameter.html',
                 controller: 'InvsParameterCont'
             })

             .state('main.Inv063', {
                 url: "/ProductCode",
                 templateUrl: 'app/modules/Inventory/views/ProductCode.html',
                 controller: 'ProductCodeCont'
             })

              .state('main.Inv035', {
                  url: "/SalesDoc",
                  templateUrl: 'app/modules/Inventory/views/SalesDocument.html',
                  controller: 'SalesDocumentCont'
              })


              .state('main.InvExc', {
                  url: "/SalesDocExchange",
                  templateUrl: 'app/modules/Inventory/views/SalesDocumentExchange.html',
                  controller: 'SalesDocumentExchangeCont'
              })

             //Ganesh
                .state('main.Sim146', {
                    url: "/ReportCardLevel",
                    templateUrl: 'app/modules/ReportCard/views/ReportCardLevel.html',
                    controller: 'ReportCardLevelCont'
                })

             .state('main.Sim138', {
                 url: "/ReportCardFrm",
                 templateUrl: 'app/modules/ReportCard/views/ReportCardFrm.html',
                 controller: 'ReportCardFrmCont'
             })

             .state('main.Per329', {
                 url: "/CreateAttendanceForEmployee",
                 templateUrl: 'app/modules/Attendance/views/CreateAttendanceForEmployee.html',
                 controller: 'CreateAttendanceForEmployeeCont'
             })
            .state('main.Sim564', {
                url: "/StudentImmunizationApply",
                templateUrl: 'app/modules/Health/views/StudentImmunizationApply.html',
                controller: 'StudentImmunizationApplyCont'
            })
             .state('main.Inv051', {
                 url: "/ShipmentDetails",
                 templateUrl: 'app/modules/Inventory/views/ShipmentDetails.html',
                 controller: 'ShipmentDetailsCont'
             })
              .state('main.Inv201', {
                  url: "/ServiceMaster",
                  templateUrl: 'app/modules/Inventory/views/ServiceMaster.html',
                  controller: 'ServiceMasterCont'
              })
             .state('main.Inv202', {
                 url: "/ServiceSuppliermapping",
                 templateUrl: 'app/modules/Inventory/views/ServiceSupplierMapping.html',
                 controller: 'ServiceSupplierCont'
             })
            .state('main.Inv059', {
                url: "/Salesman",
                templateUrl: 'app/modules/Inventory/views/Salesman.html',
                controller: 'SalesmanCont'
            })
            .state('main.Inv040', {
                url: "/ItemSupplier",
                templateUrl: 'app/modules/Inventory/views/ItemSupplier.html',
                controller: 'ItemSupplierCont'
            })
             .state('main.Inv036', {
                 url: "/InternalCustomer",
                 templateUrl: 'app/modules/Inventory/views/InternalCustomer.html',
                 controller: 'InternalCustomerCont'
             })

            .state('main.GRN', {
                url: "/GRN",
                templateUrl: 'app/modules/Inventory/views/GRN.html',
                controller: 'GRN'
            })

            .state('main.GRNApproval', {
                url: "/GRNApproval",
                templateUrl: 'app/modules/Inventory/views/GRNApprove.html',
                controller: 'GRNApproveCont'
            })
            .state('main.CostingSheetApproval', {
                url: "/CostingSheetApproval",
                templateUrl: 'app/modules/Inventory/views/CostingSheetApprove.html',
                controller: 'CostingSheetApprovalCont'
            })

          .state('main.communication', {
              url: "/Chat",
              templateUrl: 'app/modules/Common/views/Communication.html',
              controller: 'CommunicationController'
          })

        .state('main.Com008', {
            url: "/UserGroupMaster",
            templateUrl: 'app/modules/Security/views/UserGroup.html',
            controller: 'UserGroupCont'
        })


              .state('main.sim204', {
                  url: "/EmployeeReport",
                  templateUrl: 'app/modules/HRMS/views/EmployeeReport.html',
                  controller: 'EmployeeReportCont'
              })

            .state('main.Per311', {
                url: "/EmployeeReport",
                templateUrl: 'app/modules/HRMS/views/EmployeeReport.html',
                controller: 'EmployeeReportCont'
            })

            .state('main.Com009', {
                url: "/UserRole",
                templateUrl: 'app/modules/Security/views/UserRole.html',
                controller: 'UserRoleCont'
            })

              .state('main.Fin054', {
                  url: "/DocumentUser",
                  templateUrl: 'app/modules/Finance/views/DocumentUser.html',
                  controller: 'DocumentUserCont'
              })

             .state('main.Fin228', {
                 url: "/DocumentUser_PC",
                 templateUrl: 'app/modules/Finance/views/DocumentUser_PC.html',
                 controller: 'DocumentUser_PCCont'
             })


             .state('main.Fin17A', {
                 url: "/DepreciationJVPost",
                 templateUrl: 'app/modules/Finance/views/DepreciationJVPost.html',
                 controller: 'DepreciationJVPostCont'
             })

        .state('main.Fin218', {
            url: "/AssetAllocation",
            templateUrl: 'app/modules/Finance/views/AssetAllocation.html',
            controller: 'AssetAllocationCont'
        })


            .state('main.Cls10T', {
                url: "/ClassListTeacherView",
                templateUrl: 'app/modules/Student/views/ClassListTeacher.html',
                controller: 'ClassListTeacherCont'
            })


        .state('main.Fin221', {
            url: "/CostCentre",
            templateUrl: 'app/modules/Finance/views/CostCentre.html',
            controller: 'CostCentreCont'
        })
        .state('main.Fin220', {
            url: "/GlCostCentreMapping",
            templateUrl: 'app/modules/Finance/views/GlCostCentreMapping.html',
            controller: 'GlCostCentreMappingCont'
        })
        .state('main.Per300', {//Per300
            url: "/EmployeeLeaveApplication",
            templateUrl: 'app/modules/Hrms/views/EmployeeLeaveApplication.html',
            controller: 'EmployeeLeaveApplicationCont',
        })
         .state('main.Per116', {
             url: "/LeaveType",
             templateUrl: 'app/modules/HRMS/views/LeaveType.html',
             controller: 'LeaveTypeCont'
         })
         .state('main.Per260', {
             url: "/EmployeeLeaveAssign",
             templateUrl: 'app/modules/HRMS/views/EmployeeLeaveAssign.html',
             controller: 'EmployeeLeaveAssignCont'
         })
         .state('main.Per303', {//Per303
             url: "/EmployeeLeaveDetails",
             templateUrl: 'app/modules/Hrms/views/EmployeeLeaveDetails.html',
             controller: 'EmployeeLeaveDetailsCont',
         })
         .state('main.Per318', {//Per318
             url: "/TransLeaveApproval",
             templateUrl: 'app/modules/Hrms/views/TransLeaveApproval.html',
             controller: 'TransLeaveApprovalCont',
         })

            //Cancel Admission
            .state('main.Sim668', {
                url: "/TcCertificateRequiest",
                templateUrl: 'app/modules/Setup/views/TcCertificateRequiest.html',
                controller: 'TcCertificateRequiestCont'
            })
             .state('main.Sim627', {
                 url: "/admissionquota",
                 templateUrl: 'app/modules/Student/views/AdmisisonQuota.html',
                 controller: 'AdmisisonQuotaCont'
             })

            .state('main.Sim624', {
                url: "/CancelAdmissionApproval",
                templateUrl: 'app/modules/Student/views/CancelAdmissionApproval.html',
                controller: 'CancelAdmissionApprovalCont'
            })

            .state('main.Sim625', {
                url: "/CancelAdmToTC",
                templateUrl: 'app/modules/Student/views/CancelAdmToTC.html',
                controller: 'CancelAdmToTCCont'
            })
            .state('main.Sim626', {
                url: "/TCFromCancelAdm",
                templateUrl: 'app/modules/Student/views/TCFromCancelAdm.html',
                controller: 'TCFromCancelAdmCont',
                params: { edt: '' }
            })

            .state('main.Sim619', {
                url: "/TcToCancelAdm",
                templateUrl: 'app/modules/Student/views/TcToCancelAdm.html',
                controller: 'TcToCancelAdmCont'
            })
            .state('main.Sim620', {
                url: "/CancelAdmFromTc",
                templateUrl: 'app/modules/Student/views/CancelAdmFromTc.html',
                controller: 'CancelAdmFromTcCont',
                params: { tcsend: '' }
            })

            .state('main.Sim623', {
                url: "/TcWithClearance",
                templateUrl: 'app/modules/Student/views/TcWithClearance.html',
                controller: 'TcWithClearanceCont',
                params: { edt: '' }
            })

            .state('main.Sim622', {
                url: "/CancelAdmissionClearFees",
                templateUrl: 'app/modules/Student/views/CancelAdmissionClear.html',
                controller: 'CancelAdmissionClearCont',
                params: { data: '', back: '' },
            })

        //My Change
        .state('main.sim215', {
            url: "/ManualFeePosting",
            templateUrl: 'app/modules/Fee/views/ManualFeePosting.html',
            controller: 'ManualFeePostingCont',
        })
            .state('main.Inv301', {
                url: "/ManualInventoryFeePosting",
                templateUrl: 'app/modules/Fee/views/ManualInventoryFeePosting.html',
                controller: 'ManualInventoryFeePostingCont',
            })

            .state('main.Per335', {
                url: "/EmployeeReportOES",
                templateUrl: 'app/modules/HRMS/views/EmployeeReportOES.html',
                controller: 'EmployeeReportCont'
            })

             .state('main.Inv203', {
                 url: "/PendingRequest",
                 templateUrl: 'app/modules/Inventory/views/PendingRequest.html',
                 controller: 'PendingRequestCont',
             })

             .state('main.Inv204', {
                 url: "/PendingOrder",
                 templateUrl: 'app/modules/Inventory/views/PendingOrder.html',
                 controller: 'PendingOrderCont',
             })

            .state('main.Inv205', {
                url: "/ReprintOrders",
                templateUrl: 'app/modules/Inventory/views/ReprintOrders.html',
                controller: 'ReprintOrdersCont',
            })
             .state('main.Inv175', {
                 url: "/RequestApprove",
                 templateUrl: 'app/modules/Inventory/views/RequestApprove.html',
                 controller: 'RequestApproveCont'
             })

             .state('main.Fin234', {
                 url: "/VehicleExpense",
                 templateUrl: 'app/modules/Finance/views/VehicleExpense.html',
                 controller: 'VehicleExpenseCont'
             })

        .state('main.Inv047', {
            url: "/StockAdjustment",
            templateUrl: 'app/modules/Inventory/views/StockAdjustment.html',
            controller: 'StockAdjustmentCont',
        })

            .state('main.Sim561', {
                url: "/StudentDocumentListNew",
                templateUrl: 'app/modules/Student/views/StudentDocumentListNew.html',
                controller: 'StudentDocumentListNewCont'
            })


            .state('main.SimFee', {
                url: "/FeesSplit",
                templateUrl: 'app/modules/Fee/views/FeesSplit.html',
                controller: 'FeesSplitCont'
            })

            .state('main.SimIEM', {
                url: "/EmailInvoice",
                templateUrl: 'app/modules/Fee/views/SimsInvoiceEmail.html',
                controller: 'SimsInvoiceEmailCont'
            })

            .state('main.SimSEL', {
                url: "/AbsentStudentsNotification",
                templateUrl: 'app/modules/Student/views/AbsentStudentsNotification.html',
                controller: 'AbsentStudentsNotificationCont'
            })

            .state('main.Inv669', {
                url: "/SupplierLocation",
                templateUrl: 'app/modules/Inventory/views/SupplierLocation.html',
                controller: 'SupplierLocationCont'
            })

             .state('main.Sim669', {
                 url: "/SupplierLocation",
                 templateUrl: 'app/modules/Inventory/views/SupplierLocation.html',
                 controller: 'SupplierLocationCont'
             })

              .state('main.Per319', {
                  url: "/VacancyMaster",
                  templateUrl: 'app/modules/HRMS/views/VacancyMaster.html',
                  controller: 'VacancyMasterCont'
              })

             .state('main.Per331', {
                 url: "/VacancyView",
                 templateUrl: 'app/modules/HRMS/views/VacancyView.html',
                 controller: 'VacancyViewCont'
             })

             .state('main.Sim101', {
                 url: "/MedicalVisit",
                 templateUrl: 'app/modules/Health/views/MedicalVisit.html',
                 controller: 'MedicalVisitCont'
             })

            //.state('main.Fin226', {
            //    url: "/PrintVouchers",
            //    templateUrl: 'app/modules/Finance/views/PrintVouchers.html',
            //    controller: 'PrintVouchersCont'
            //})

             .state('main.FINR15', {
                 url: "/PrintVoucher",
                 templateUrl: 'app/modules/Finance/views/PrintVoucher.html',
                 controller: 'PrintVoucherCont'
             })

            .state('main.FINR15_DPS', {
                url: "/PrintVoucher_DPS",
                templateUrl: 'app/modules/Finance/views/PrintVoucher_DPS.html',
                controller: 'PrintVoucher_DPSCont'
            })


         .state('main.Inv148', {
             url: "/InterDepartmentTransfer",
             templateUrl: 'app/modules/Inventory/views/InterDepartmentTransfer.html',
             controller: 'InterDepartmentTransferCont',
         })

         .state('main.InvRes', {
             url: "/CancelSalesReceipt",
             templateUrl: 'app/modules/Inventory/views/CancelSalesReceipt.html',
             controller: 'CancelSalesReceiptCont',
         })

             .state('main.Inv038', {
                 url: "/ItemDepartment",
                 templateUrl: 'app/modules/Inventory/views/ItemDepartment.html',
                 controller: 'ItemDepartmentCont'
             })
             .state('main.Inv160', {
                 url: "/SaleDocumentList",
                 templateUrl: 'app/modules/Inventory/views/SaleDocumentList.html',
                 controller: 'SaleDocumentListCont',
             })

             .state('main.Inv065', {
                 url: "/ItemAssembly",
                 templateUrl: 'app/modules/Inventory/views/ItemAssembly.html',
                 controller: 'ItemAssemblyCont'
             })


             .state('main.Sim616', {
                 url: "/ParentRegistration",
                 templateUrl: 'app/modules/Student/views/ParentRegistration.html',
                 controller: 'ParentRegistrationCont'
             })

            .state('main.Per304', {
                url: "/LeaveDetails",
                templateUrl: 'app/modules/HRMS/views/LeaveDetails.html',
                controller: 'LeaveDetailsCont'
            })
             .state('main.Per312', {
                 url: "/EmployeeDocumentUploadNew",
                 templateUrl: 'app/modules/HRMS/views/EmployeeDocumentUploadNew.html',
                 controller: 'EmployeeDocumentUploadNewCont'
             })

            .state('main.Sim613', {
                url: "/Honour",
                templateUrl: 'app/modules/Setup/views/Honour.html',
                controller: 'HonourCont'
            })

        .state('main.Sim614', {
            url: "/UserHonour",
            templateUrl: 'app/modules/Setup/views/UserHonour.html',
            controller: 'UserHonourCont'
        })


               .state('main.Inv200', {
                   url: "/VehicleConsumptionDetails",
                   templateUrl: 'app/modules/Inventory/views/VehicleConsumptionDetails.html',
                   controller: 'VehicleConsumptionDetailsCont'
               })

             .state('main.Sim171', {
                 url: "/timeTableUpload",
                 templateUrl: 'app/modules/Scheduling/views/timeTableUpload.html',
                 controller: 'timeTableUploadCont'
             })

             .state('main.Sim104', {
                 url: "/CertificateMaster",
                 templateUrl: 'app/modules/Student/views/CertificateMaster.html',
                 controller: 'CertificateMasterCont'
             })

            //Re-Admission
            .state('main.Sim670', {
                url: "/ReAdmission",
                templateUrl: 'app/modules/Student/views/ReAdmission.html',
                controller: 'ReAdmissionCont'
            })


             .state('main.Pe078V', {
                 url: "/ViewPayslip",
                 templateUrl: 'app/modules/HRMS/views/ViewPayslip.html',
                 controller: 'ViewPayslipCont'
             })

              .state('main.aquota', {
                  url: "/admissionQuota",
                  templateUrl: 'app/modules/Student/views/admissionQuotaMaster.html',
                  controller: 'admissionQuotaMasterCont'
              })

            .state('main.uquota', {
                url: "/admissionQuotaUser",
                templateUrl: 'app/modules/Student/views/admissionQuotaUser.html',
                controller: 'admissionQuotaUserCont'
            })

            .state('main.rpts', {
                url: "/Receipt",
                templateUrl: 'app/modules/Inventory/views/receipt.html',
                controller: 'ReceiptCont'
            })

             .state('main.GlbMap', {
                 url: "/GlobalSearchMapping",
                 templateUrl: 'app/modules/Setup/views/GlobalSearchMapping.html',
                 controller: 'GlobalSearchMappingCont'
             })


            .state('main.Inv149', {
                url: "/ApprovedIDT",
                templateUrl: 'app/modules/Inventory/views/ApprovedIDT.html',
                controller: 'ApprovedIDTCont',
            })

            .state('main.inv138', {
                url: "/Inventory",
                templateUrl: 'app/modules/Inventory/views/Inv138.html',
                controller: 'Inv138Controller'
            })
	    .state('main.SAA003', {
	        url: "/ActivityGroupMaster",
	        templateUrl: 'app/modules/Gradebook/views/ActivityGroupMaster.html',
	        controller: 'ActivityGroupMasterCont'
	    })

        .state('main.SAA005', {
            url: "/ActivityExamMaster",
            templateUrl: 'app/modules/Gradebook/views/ActivityExamMaster.html',
            controller: 'ActivityExamMasterCont'
        })

        .state('main.SAA004', {
            url: "/ActivityIndicatorGroup",
            templateUrl: 'app/modules/Gradebook/views/ActivitiyIndicator.html',
            controller: 'ActivityIndicatorCont'
        }).state('main.SAA001', {
            url: "/SAA001",
            templateUrl: 'app/modules/Gradebook/views/AcitivityConfiguration.html',
            controller: 'ActivityConfigurationController'
        }).state('main.SAA002', {
            url: "/SAA002",
            templateUrl: 'app/modules/Gradebook/views/studentactivityentry.html',
            controller: 'StudentActivityEntryController'
        })

         .state('main.repcat', {
             url: "/ReportCardAttendance",
             templateUrl: 'app/modules/Gradebook/views/ReportCardAttendance.html',
             controller: 'ReportCardAttendanceCont'
         })

        .state('main.quaemp', {
            url: "/QualificationEmployeeADISW",
            templateUrl: 'app/modules/HRMS/views/QualificationEmployee_ADISW.html',
            controller: 'QualificationEmployeeADISWCont'
        })

         .state('main.SEE001', {
            url: "/ScholasticEvalExamMaster",
            templateUrl: 'app/modules/Gradebook/views/ScholasticEvalExamMaster.html',
            controller: 'ScholasticEvalExamMasterCont'
         })

         .state('main.SEE002', {
             url: "/ScholasticEvalGroupMaster",
             templateUrl: 'app/modules/Gradebook/views/ScholasticEvalGroupMaster.html',
             controller: 'ScholasticEvalGroupMasterCont'
         })

        .state('main.SEE003', {
            url: "/ScholasticEvalIndicatorGroupMaster",
            templateUrl: 'app/modules/Gradebook/views/ScholasticEvalIndicatorGroupMaster.html',
            controller: 'ScholasticEvalIndicatorGroupMasterCont'
        })


    })

})();