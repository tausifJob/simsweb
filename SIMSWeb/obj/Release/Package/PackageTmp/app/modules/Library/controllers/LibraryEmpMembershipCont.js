﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, savefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Library');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

    simsController.controller('LibraryEmpMembershipCont',
   ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

       $scope.pagesize = "5";
       $scope.pageindex = 0;
       $scope.save_btn = true;
       $scope.pager = false;
       $scope.pageno = false;
       var dataforSave = [];
       $scope.display = false;
       $scope.table = true;
       $scope.searchtable = false;
       $scope.mem = false;

       var user = $rootScope.globals.currentUser.username;

       var dt = new Date();
       $scope.sims_library_joining_date = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();

       $timeout(function () {
           $("#fixTable").tableHeadFixer({ 'top': 1 });
       }, 100);


       $('*[data-datepicker="true"] input[type="text"]').datepicker({
           todayBtn: true,
           orientation: "top left",
           autoclose: true,
           todayHighlight: true
       });

       $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
           $('input[type="text"]', $(this).parent()).focus();
       });



       $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
           debugger
           $scope.ComboBoxValues = AllComboBoxValues.data;
           $scope.temp = {
               sims_academic_year: $scope.ComboBoxValues[0].sims_acadmic_year,
               s_cur_code: $scope.ComboBoxValues[0].sims_cur_code,
           };

           console.log($scope.ComboBoxValues);
       });


       $http.get(ENV.apiUrl + "api/LibraryEmpMembership/GetAllLibraryMembershipType").then(function (certificate) {
           $scope.member_data = certificate.data;
       });


       // Form Close
       $scope.close = function () {
           $scope.table = true;
           $scope.display = false;
           $scope.grid2 = false;
           $scope.grid1 = false;
           $scope.sims_library_renewed_date = '';
           // $scope.save_btn = false;
       }

       //Search Records
       $scope.searched1 = function (valLists, toSearch) {
           return _.filter(valLists,

           function (i) {
               /* Search Text in all  fields */
               return searchUtil1(i, toSearch);
           });
       };

       $scope.search1 = function () {
           $scope.todos = $scope.searched1($scope.student1, $scope.searchText);
           $scope.totalItems = $scope.todos.length;
           $scope.currentPage = '1';
           if ($scope.searchText == '') {
               $scope.todos = $scope.student1;
           }
           $scope.makeTodos();
       }

       function searchUtil1(item, toSearch) {
           /* Search Text in all 3 fields */
           return (item.sims_teacher_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                   item.sims_teacher_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                   item.sims_employee_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1

                   //item.emp_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                   //item.em_number == toSearch
               ) ? true : false;
       }


       //Employee
       $scope.searchemployee = function () {
           debugger
           $scope.grid = true;
           $scope.pager = true;
           $scope.pageno = true;
           $scope.searchtableemployee = true;
           $scope.mem = true;
           $scope.curr = true;
           $scope.acad = true;
           $scope.date = true;
           $scope.save = true;
           $scope.ImageView = false;
           if ($scope.temp == undefined || $scope.temp == null) {
               $scope.temp = '';
           }
           //if (name == undefined || name == '\"\"') {
           //    name = '';
           //}
           $http.get(ENV.apiUrl + "api/LibraryEmpMembership/GetEmployeeDetails?emcode=" + $scope.temp.emp_number).then(function (GetEmp) {
               debugger
               $scope.student = GetEmp.data;
               $scope.totalItems = $scope.student.length;
               $scope.todos = $scope.student;
               $scope.makeTodos();
               $scope.busy = false;
               console.log($scope.student);
               if (GetEmp.data.length > 0) { }
               else {
                   $scope.Reset();
                   $scope.ImageView = true;
               }
           });
       }

       $scope.CheckAllChecked = function () {

           main = document.getElementById('mainchk');

           if (main.checked == true) {
               for (var i = 0; i < $scope.student.length; i++) {
                   var v = document.getElementById($scope.student[i].em_number + i);
                   v.checked = true;
               }
           }
           else {
               for (var i = 0; i < $scope.student.length; i++) {
                   var v = document.getElementById($scope.student[i].em_number + i);
                   v.checked = false;
                   main.checked = false;
                   $scope.row1 = '';
               }
           }
       }

       $scope.checkonebyonedelete = function () {

           $("input[type='checkbox']").change(function (e) {
               if ($(this).is(":checked")) { //If the checkbox is checked
                   $(this).closest('tr').addClass("row_selected");
                   //Add class on checkbox checked
                   $scope.color = '#edefef';
               }
               else {
                   $(this).closest('tr').removeClass("row_selected");
                   //Remove class on checkbox uncheck
                   $scope.color = '#edefef';
               }
           });

           main = document.getElementById('mainchk');
           if (main.checked == true) {
               main.checked = false;
               $scope.color = '#edefef';
               $scope.row1 = '';
           }
       }

       $scope.Saveemp = function () {
           debugger
           savefin = [];
           var flag = false;
           if ($scope.temp.s_cur_code === undefined) {
               swal('', 'Enter Curriculum');
               return;
           }
           if ($scope.temp.sims_academic_year === undefined) {
               swal('', 'Enter Academic Year');
               return;
           }
           if ($scope.temp.sims_library_membership_type_code === undefined) {
               swal('', 'Enter Membership Type');
               return;
           }
           for (var i = 0; i < $scope.student.length; i++) {
               var v = document.getElementById($scope.student[i].em_number + i);
               if (v.checked == true) {
                   var modulecode = ({
                       'sims_employee_code': $scope.student[i].em_number,
                       'sims_library_membership_type': $scope.temp.sims_library_membership_type_code,
                       'sims_library_joining_date': $scope.sims_library_joining_date,
                       'cur_code': $scope.temp.s_cur_code,
                       'academic_year': $scope.temp.sims_academic_year,
                   });
                   flag = true;
                   savefin.push(modulecode);
               }
               //else {
               //    swal('', 'Please Select Employees');
               //    return;
               //}
           }
           if (flag == false) {
               swal('', 'Please Select Records');
               flag = true;
               return;
           }

           $http.post(ENV.apiUrl + "api/LibraryEmpMembership/Updateemp_RenewLibrary", savefin).then(function (savedata) {
               $scope.msg1 = savedata.data;
               if ($scope.msg1 == true) {
                   swal({ title: "Success!", text: "Membership Created Successfully", width: 300, height: 200 });
                   savefin = [];
                   $scope.student = [];
               }
               else {
                   swal({ title: "Oops!", text: "Sorry! Membership Not Created", width: 300, height: 200 });
               }
           });
           savefin = [];
           $('tr').removeClass("row_selected");
           main.checked = false;
           $scope.Reset();
       }


       //Search Records
       $scope.searched2 = function (valLists, toSearch) {
           return _.filter(valLists,

           function (i) {
               /* Search Text in all  fields */
               return searchUtil2(i, toSearch);
           });
       };

       $scope.search2 = function () {
           $scope.todos = $scope.searched2($scope.student1, $scope.searchText);
           $scope.totalItems = $scope.todos.length;
           $scope.currentPage = '1';
           if ($scope.searchText == '') {
               $scope.todos = $scope.student1;
           }
           $scope.makeTodos();
       }

       function searchUtil2(item, toSearch) {
           /* Search Text in all 3 fields */
           return (item.emp_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                   item.em_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
       }


       //Pages
       $scope.size = function (str) {
           console.log(str);
           $scope.pagesize = str;
           $scope.currentPage = 1;
           $scope.numPerPage = str;
           console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
       }

       $scope.index = function (str) {
           $scope.pageindex = str;
           $scope.currentPage = str;
           console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
           main.checked = false;
           $scope.CheckAllChecked();
           $scope.CheckAllChecked1();
       }

       $scope.student = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

       $scope.makeTodos = function () {

           var rem = parseInt($scope.totalItems % $scope.numPerPage);
           if (rem == '0') {
               $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
           }
           else {
               $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
           }

           var begin = (($scope.currentPage - 1) * $scope.numPerPage);
           var end = parseInt(begin) + parseInt($scope.numPerPage);

           console.log("begin=" + begin); console.log("end=" + end);

           $scope.student = $scope.todos.slice(begin, end);
       };

       //Cancel Button
       $scope.Reset = function () {
           $scope.pager = false;
           $scope.pageno = false;
           $scope.mem = false;
           $scope.temp = "";
           $scope.table = true;
           $scope.searchtable = false;
           $scope.grid = false;
           $scope.student = [];
           $scope.edt = "";
           main.checked = false;


           //$scope.sims_library_renewed_date = '';
       }

   }])

})();