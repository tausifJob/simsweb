﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Library');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('LibraryCatalogueAttributeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.edit = false;
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/LibraryCatalogueAttribute/getAllLibraryCatalogueAttribute").then(function (res1) {
                $scope.CreDiv = res1.data;
                $scope.totalItems = $scope.CreDiv.length;
                $scope.todos = $scope.CreDiv;
                $scope.makeTodos();
            });


            //Fill Combo catalougecode
            $http.get(ENV.apiUrl + "api/LibraryCatalogueAttribute/GetcatalogueName").then(function (docstatus1) {
                $scope.catacode = docstatus1.data;
                console.log($scope.docstatus1);
            }); 

            // fill combo attribute code
            $http.get(ENV.apiUrl + "api/LibraryCatalogueAttribute/GetAttributeCode").then(function (docstatus1) {
                $scope.attributecode = docstatus1.data;
                console.log($scope.docstatus1);
            });


            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_library_catalogue_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_library_attribute_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {

                $scope.disabled = false;

                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.divcode_readonly = false;
                $scope.temp = "";

            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                if (Myform) {
                    datasend = [];
                    var data = $scope.temp;
                    $scope.exist = false;
                    data.opr = 'I';

                    for (var i = 0; i < $scope.CreDiv.length; i++) {
                        if ($scope.CreDiv[i].sims_library_catalogue_code == data.sims_library_catalogue_code && $scope.CreDiv[i].sims_library_attribute_code == data.sims_library_attribute_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist)
                    {
                        message.strMessage = "Record already exists ";
                        message.systemMessage = string.Empty;
                        message.messageType = MessageType.Success;
                        //// $rootScope.strMessage = 'Record Already exists';
                        //swal({ title: "Alert", text: "Record Already exists", imageUrl: "assets/img/check.png", });
                        //// $('#message').modal('show');
                    }
                    else {

                        datasend.push(data);
                        $http.post(ENV.apiUrl + "api/LibraryCatalogueAttribute/CUDLibraryCatalogueAttribute", datasend).then(function (msg) {
                            $scope.msg1 = msg.data;

                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", showCloseButton: true, width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record allready present", showCloseButton: true, width: 300, height: 200 });
                            }

                            $http.get(ENV.apiUrl + "api/LibraryCatalogueAttribute/getAllLibraryCatalogueAttribute").then(function (res1) {
                                $scope.CreDiv = res1.data;
                                $scope.totalItems = $scope.CreDiv.length;
                                $scope.todos = $scope.CreDiv;
                                $scope.makeTodos();

                            });

                        });
                        datasend = [];
                    }
                }
                $scope.table = true;
                $scope.display = false;
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].sims_library_catalogue_code);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].sims_library_catalogue_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }
         
            $scope.OkDelete = function ()
            {
                debugger;
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i + $scope.filteredTodos[i].sims_library_catalogue_code);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_library_catalogue_code': $scope.filteredTodos[i].cat_code,
                            'sims_library_attribute_code': $scope.filteredTodos[i].att_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/LibraryCatalogueAttribute/CUDLibraryCatalogueAttribute", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/LibraryCatalogueAttribute/getAllLibraryCatalogueAttribute").then(function (res1) {
                                                $scope.CreDiv = res1.data;
                                                $scope.totalItems = $scope.CreDiv.length;
                                                $scope.todos = $scope.CreDiv;
                                                $scope.makeTodos();

                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted ", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/LibraryCatalogueAttribute/getAllLibraryCatalogueAttribute").then(function (res1) {
                                                $scope.CreDiv = res1.data;
                                                $scope.totalItems = $scope.CreDiv.length;
                                                $scope.todos = $scope.CreDiv;
                                                $scope.makeTodos();

                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i + $scope.filteredTodos[i].sims_library_catalogue_code);
                                if (v.checked == true) {
                                    v.checked = false;
                                    //  $scope.row1 = '';
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });


        }])

})();
