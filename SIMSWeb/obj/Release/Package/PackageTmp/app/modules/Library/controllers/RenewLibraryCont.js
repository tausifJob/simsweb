﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, savefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Library');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

    simsController.controller('RenewLibraryCont',
   ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

       $scope.pagesize = "5";
       $scope.pageindex = 0;
       $scope.save_btn = false;
       $scope.save_btn1 = false;
       $scope.save_btn2 = false;
       var dataforSave = [];
       $scope.Update_btn = false;
       $scope.display = false;
       $scope.table = true;
       $scope.searchtable = false;
       var user = $rootScope.globals.currentUser.username;

       var dt = new Date();
       $scope.sims_library_renewed_date = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();

       $timeout(function () {
           $("#fixTable").tableHeadFixer({ 'top': 1 });
       }, 100);

       $timeout(function () {
           $("#fixTable1").tableHeadFixer({ 'top': 1 });
       }, 100);

       $timeout(function () {
           $("#fixTable2").tableHeadFixer({ 'top': 1 });
       }, 100);

       $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
           $scope.ComboBoxValues = AllComboBoxValues.data;
           $scope.temp = {
               sims_academic_year: $scope.ComboBoxValues[0].sims_acadmic_year,
               s_cur_code: $scope.ComboBoxValues[0].sims_cur_code,
           };
           console.log($scope.ComboBoxValues);
       });

       $('*[data-datepicker="true"] input[type="text"]').datepicker({
           todayBtn: true,
           orientation: "top left",
           autoclose: true,
           todayHighlight: true
       });

       $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
           $('input[type="text"]', $(this).parent()).focus();
       });


       $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
           $('input[type="text"]', $(this).parent()).focus();
       });

       $scope.countData = [
                    { val: 5, data: 5 },
                    { val: 10, data: 10 },
                    { val: 15, data: 15 },
       ]
       // Form Close
       $scope.close = function () {
           $scope.table = true;
           $scope.display = false;
           $scope.grid2 = false;
           $scope.grid1 = false;
           $scope.sims_library_renewed_date = '';
           // $scope.save_btn = false;
       }

       //Pages
       //$scope.size = function (str) {
       //    console.log(str);
       //    $scope.pagesize = str;
       //    $scope.currentPage = 1;
       //    $scope.numPerPage = str;
       //    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
       //}

       $scope.size = function (str) {
           if (str == 5 || str == 10 || str == 15) {
               $scope.pager = true;
           }
           else {
               $scope.pager = false;
           }
           console.log(str);
           $scope.pagesize = str;
           $scope.currentPage = 1;
           $scope.numPerPage = str;
           console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
       }

       $scope.index = function (str) {
           $scope.pageindex = str;
           $scope.currentPage = str;
           console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
       }

       $scope.student = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

       $scope.makeTodos = function () {

           var rem = parseInt($scope.totalItems % $scope.numPerPage);
           if (rem == '0') {
               $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
           }
           else {
               $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
           }

           var begin = (($scope.currentPage - 1) * $scope.numPerPage);
           var end = parseInt(begin) + parseInt($scope.numPerPage);

           console.log("begin=" + begin); console.log("end=" + end);

           $scope.student = $scope.todos.slice(begin, end);
       };


       //Student

       $scope.searchstudent = function () {
           debugger
           $scope.save_btn = true;
           $scope.busy = true;
           $scope.searchtable = true;
           $scope.ImageView = false;

           if ($scope.temp == undefined || $scope.temp.s_cur_code == undefined || $scope.temp.s_cur_code == null || $scope.temp.sims_academic_year == undefined || $scope.temp.sims_academic_year == null) {
               swal('', 'Please Select Curriculum And Academic Year');
               $scope.searchtable = false;
               return;
           }

           $http.get(ENV.apiUrl + "api/RenewLibrary/getSearchStudent?data=" + JSON.stringify($scope.temp)).then(function (Allstudent) {
               debugger
               $scope.student1 = Allstudent.data;
               //$scope.totalItems = $scope.student1.length;
               //$scope.todos = $scope.student1;
               //$scope.makeTodos();
               //$scope.busy = false;
               //$scope.searchtable = true;
               //console.log($scope.student1);
               //if (Allstudent.data.length > 0) { }
               //else {
               //    $scope.ImageView = true;
               //} 
               if ($scope.student1.length > 0) {
                   debugger
                   //$scope.table = true;
                   $scope.pager = true;
                   if ($scope.countData.length > 3) {
                       $scope.countData.splice(3, 1);
                       $scope.countData.push({ val: $scope.student1.length, data: 'All' })
                   }
                   else {
                       $scope.countData.push({ val: $scope.student1.length, data: 'All' })
                   }
                   $scope.totalItems = $scope.student1.length;
                   $scope.todos = $scope.student1;
                   $scope.makeTodos();
               }
               else {
                   $scope.table = false;
                   swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                   $scope.filteredTodos = [];
               }
           });
       }

       $scope.CheckAllChecked = function () {
           main = document.getElementById('mainchk');

           if (main.checked == true) {
               for (var i = 0; i < $scope.student.length; i++) {
                   var v = document.getElementById($scope.student[i].s_enroll_no + i);
                   v.checked = true;
                   $('tr').addClass("row_selected");
               }
           }
           else {
               for (var i = 0; i < $scope.student.length; i++) {
                   var v = document.getElementById($scope.student[i].s_enroll_no + i);
                   v.checked = false;
                   main.checked = false;
                   $scope.row1 = '';
                   $('tr').removeClass("row_selected");
               }
           }

       }

       $scope.checkonebyone = function () {

           $("input[type='checkbox']").change(function (e) {
               if ($(this).is(":checked")) { //If the checkbox is checked
                   $(this).closest('tr').addClass("row_selected");
                   //Add class on checkbox checked
                   $scope.color = '#edefef';
               }
               else {
                   $(this).closest('tr').removeClass("row_selected");
                   //Remove class on checkbox uncheck
                   $scope.color = '#edefef';
               }
           });

           main = document.getElementById('mainchk');
           if (main.checked == true) {
               main.checked = false;
               $scope.color = '#edefef';
               $scope.row1 = '';
           }
       }

       $scope.Savedata = function () {
           debugger;
           savefin = [];

           if ($scope.sims_library_renewed_date === undefined) {
               swal('', 'Enter Renew Date');
               return;
           }

           for (var i = 0; i < $scope.student.length; i++) {
               var v = document.getElementById($scope.student[i].s_enroll_no + i);
               if (v.checked == true) {
                   $scope.student[i].sims_library_renewed_date = $scope.sims_library_renewed_date;
                   $scope.student[i].cur_code = $scope.temp.s_cur_code;
                   $scope.student[i].academic_year = $scope.temp.sims_academic_year;
                   savefin.push($scope.student[i]);
               }
               v.checked = false;
           }

           $http.post(ENV.apiUrl + "api/RenewLibrary/Update_RenewLibrary", savefin).then(function (savedata) {
               $scope.msg1 = savedata.data;
               if ($scope.msg1 == true) {
                   swal({ title: "Success!", text: "Membership Renewed Successfully", width: 300, height: 200 });
                   dataforUpdate = [];
                   $scope.filteredTodos = [];
               }
               else {
                   swal({ title: "Oops!", text: "Sorry! Please Check Membership Status", width: 300, height: 200 });
               }
           });
           savefin = [];
           $('tr').removeClass("row_selected");
           main.checked = false;
           $scope.Clear();
       }


       //Search Records
       $scope.searched = function (valLists, toSearch) {
           return _.filter(valLists,

           function (i) {
               /* Search Text in all  fields */
               return searchUtil(i, toSearch);
           });
       };

       $scope.search = function () {
           $scope.todos = $scope.searched($scope.student1, $scope.searchText);
           $scope.totalItems = $scope.todos.length;
           $scope.currentPage = '1';
           if ($scope.searchText == '') {
               $scope.todos = $scope.student1;
           }
           $scope.makeTodos();
       }

       function searchUtil(item, toSearch) {
           /* Search Text in all 3 fields */
           return (item.s_sname_in_english.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                   item.s_enroll_no == toSearch
                   //item.sims_teacher_code == toSearch ||
                   //item.sims_employee_code == toSearch||
                   //item.sims_teacher_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                   //item.emp_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                   //item.em_number == toSearch
               ) ? true : false;
       }


       //Teacher 

       $scope.searchteacher = function () {
           debugger
           $scope.save_btn1 = true;
           $scope.searchtableteacher = true;
           $scope.ImageView = false;
           if ($scope.student_info == undefined || $scope.student_info == null) {
               $scope.student_info = '';
           }

           $http.get(ENV.apiUrl + "api/RenewLibrary/getSearchteacher?teacherid=" + $scope.student_info.sims_teacher_code + "&emno=" + $scope.student_info.sims_employee_code + "&libno=" + $scope.student_info.sims_library_user_number + "&data=" + $scope.student_info.sims_teacher_name).then(function (GetTeacher) {

               $scope.student1 = GetTeacher.data;

               //$scope.totalItems = $scope.student1.length;
               //$scope.todos = $scope.student1;
               //$scope.makeTodos();
               //$scope.busy = false;
               //console.log($scope.student1);
               //if (GetTeacher.data.length > 0) { }
               //else {
               //    $scope.ImageView = true;
               //}
               if ($scope.student1.length > 0) {
                   //$scope.table = true;
                   $scope.pager = true;
                   if ($scope.countData.length > 3) {
                       $scope.countData.splice(3, 1);
                       $scope.countData.push({ val: $scope.student1.length, data: 'All' })
                   }
                   else {
                       $scope.countData.push({ val: $scope.student1.length, data: 'All' })
                   }
                   $scope.totalItems = $scope.student1.length;
                   $scope.todos = $scope.student1;
                   $scope.makeTodos();
               }
               else {
                   $scope.table = false;
                   swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                   $scope.filteredTodos = [];
               }
           });
       }

       $scope.CheckAllChecked1 = function () {
           main = document.getElementById('Checkbox1');
           for (var i = 0; i < $scope.student.length; i++) {
               var v = document.getElementById($scope.student[i].sims_employee_code);
               if (main.checked == true) {
                   v.checked = true;
                   $('tr').addClass("row_selected");
               }
               else {
                   v.checked = false;
                   main.checked = false;
                   $scope.row1 = '';
                   $('tr').removeClass("row_selected");
               }
           }

       }

       $scope.checkonebyone1 = function () {
           $("input[type='checkbox']").change(function (e) {
               if ($(this).is(":checked")) { //If the checkbox is checked
                   $(this).closest('tr').addClass("row_selected");
                   //Add class on checkbox checked
                   $scope.color = '#edefef';
               }
               else {
                   $(this).closest('tr').removeClass("row_selected");
                   //Remove class on checkbox uncheck
                   $scope.color = '#edefef';
               }
           });

           main = document.getElementById('mainchk');
           if (main.checked == true) {
               main.checked = false;
               $scope.color = '#edefef';
               $scope.row1 = '';
           }
       }

       $scope.Saveteacher = function () {
           savefin = [];
           if ($scope.sims_library_renewed_date === undefined) {
               swal('', 'Enter Renew Date');
               return;
           }
           for (var i = 0; i < $scope.student.length; i++) {
               var v = document.getElementById($scope.student[i].sims_employee_code);
               if (v.checked == true) {
                   $scope.student[i].sims_library_renewed_date = $scope.sims_library_renewed_date;
                   savefin.push($scope.student[i]);
               }
               v.checked = false;
           }
           $http.post(ENV.apiUrl + "api/RenewLibrary/Updateteacher_RenewLibrary", savefin).then(function (savedata) {
               $scope.msg1 = savedata.data;
               if ($scope.msg1 == true) {
                   swal({ title: "Success!", text: "Membership Renewed Successfully", width: 300, height: 200 });
                   dataforUpdate = [];
                   $scope.filteredTodos = [];
               }
               else {
                   swal({ title: "Oops!", text: "Sorry! Please Check Membership Status", width: 300, height: 200 });
               }
           });
           savefin = [];
           main.checked = false;
           $('tr').removeClass("row_selected");
           $scope.Clear();
       }


       //Search Records Teacher
       $scope.searched1 = function (valLists, toSearch) {
           return _.filter(valLists,

           function (i) {
               /* Search Text in all  fields */
               return searchUtil1(i, toSearch);
           });
       };

       $scope.search1 = function () {
           $scope.todos = $scope.searched1($scope.student1, $scope.searchText);
           $scope.totalItems = $scope.todos.length;
           $scope.currentPage = '1';
           if ($scope.searchText == '') {
               $scope.todos = $scope.student1;
           }
           $scope.makeTodos();
       }

       function searchUtil1(item, toSearch) {
           /* Search Text in all 3 fields */
           return (item.sims_teacher_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                   item.sims_teacher_code == toSearch ||
                   item.sims_employee_code == toSearch

                   //item.emp_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                   //item.em_number == toSearch
               ) ? true : false;
       }


       //Employee
       $scope.searchemployee = function (emcode) {
           $scope.save_btn2 = true;
           $scope.searchtableemployee = true;
           $scope.ImageView = false;
           if (emcode == undefined || emcode == '\"\"') {
               emcode = '';
           }
           $http.get(ENV.apiUrl + "api/RenewLibrary/getSearchemployee?emcode=" + emcode).then(function (GetEmp) {
               $scope.student1 = GetEmp.data;
               //$scope.totalItems = $scope.student1.length;
               //$scope.todos = $scope.student1;
               //$scope.makeTodos();
               //$scope.busy = false;
               //console.log($scope.student1);
               //if (GetEmp.data.length > 0) { }
               //else {
               //    $scope.ImageView = true;
               //}
               if ($scope.student1.length > 0) {
                   //$scope.table = true;
                   $scope.pager = true;
                   if ($scope.countData.length > 3) {
                       $scope.countData.splice(3, 1);
                       $scope.countData.push({ val: $scope.student1.length, data: 'All' })
                   }
                   else {
                       $scope.countData.push({ val: $scope.student1.length, data: 'All' })
                   }
                   $scope.totalItems = $scope.student1.length;
                   $scope.todos = $scope.student1;
                   $scope.makeTodos();
               }
               else {
                   $scope.table = false;
                   swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                   $scope.filteredTodos = [];
               }
           });
       }

       $scope.CheckAllChecked2 = function () {
           main = document.getElementById('Checkbox3');
           for (var i = 0; i < $scope.student.length; i++) {
               var v = document.getElementById($scope.student[i].em_number);
               if (main.checked == true) {
                   v.checked = true;
               }
               else {
                   v.checked = false;
                   main.checked = false;
                   $scope.row1 = '';
               }
           }

       }

       $scope.checkonebyone2 = function () {

           $("input[type='checkbox']").change(function (e) {
               if ($(this).is(":checked")) { //If the checkbox is checked
                   $(this).closest('tr').addClass("row_selected");
                   //Add class on checkbox checked
                   $scope.color = '#edefef';
               }
               else {
                   $(this).closest('tr').removeClass("row_selected");
                   //Remove class on checkbox uncheck
                   $scope.color = '#edefef';
               }
           });

           main = document.getElementById('mainchk');
           if (main.checked == true) {
               main.checked = false;
               $scope.color = '#edefef';
               $scope.row1 = '';
           }
       }

       $scope.Saveemp = function () {

           savefin = [];
           if ($scope.sims_library_renewed_date === undefined) {
               swal('', 'Enter Renew Date');
               return;
           }
           for (var i = 0; i < $scope.student.length; i++) {
               var v = document.getElementById($scope.student[i].em_number);
               if (v.checked == true) {
                   $scope.student[i].sims_library_renewed_date = $scope.sims_library_renewed_date;
                   savefin.push($scope.student[i]);
               }
               v.checked = false;
           }
           $http.post(ENV.apiUrl + "api/RenewLibrary/Updateemp_RenewLibrary", savefin).then(function (savedata) {
               $scope.msg1 = savedata.data;
               if ($scope.msg1 == true) {
                   swal({ title: "Success!", text: "Membership Renewed Successfully", width: 300, height: 200 });
                   dataforUpdate = [];
                   $scope.filteredTodos = [];
               }
               else {
                   swal({ title: "Oops!", text: "Sorry! Please Check Membership Status", width: 300, height: 200 });
               }
           });
           savefin = [];
           $('tr').removeClass("row_selected");

       }


       //Search Records Employee
       $scope.searched2 = function (valLists, toSearch) {
           return _.filter(valLists,

           function (i) {
               /* Search Text in all  fields */
               return searchUtil2(i, toSearch);
           });
       };

       $scope.search2 = function () {
           $scope.todos = $scope.searched2($scope.student1, $scope.searchText);
           $scope.totalItems = $scope.todos.length;
           $scope.currentPage = '1';
           if ($scope.searchText == '') {
               $scope.todos = $scope.student1;
           }
           $scope.makeTodos();
       }

       function searchUtil2(item, toSearch) {
           /* Search Text in all 3 fields */
           return (item.emp_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                   item.em_number == toSearch) ? true : false;
       }

       //Pages
       //$scope.size = function (str) {
       //    console.log(str);
       //    $scope.pagesize = str;
       //    $scope.currentPage = 1;
       //    $scope.numPerPage = str;
       //    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
       //}

       $scope.size = function (str) {
           if (str == 5 || str == 10 || str == 15) {
               $scope.pager = true;
           }
           else {
               $scope.pager = false;
           }
           console.log(str);
           $scope.pagesize = str;
           $scope.currentPage = 1;
           $scope.numPerPage = str;
           console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
       }

       $scope.index = function (str) {
           $scope.pageindex = str;
           $scope.currentPage = str;
           console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
       }

       $scope.student = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

       $scope.makeTodos = function () {

           var rem = parseInt($scope.totalItems % $scope.numPerPage);
           if (rem == '0') {
               $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
           }
           else {
               $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
           }

           var begin = (($scope.currentPage - 1) * $scope.numPerPage);
           var end = parseInt(begin) + parseInt($scope.numPerPage);

           console.log("begin=" + begin); console.log("end=" + end);

           $scope.student = $scope.todos.slice(begin, end);
       };


       //Cancel Button
       $scope.Clear = function () {
           $scope.pagesize = "5";
           $scope.pageindex = 0;
           //$scope.temp = "";
           $scope.table = true;
           $scope.display = false;
           $scope.searchtable = false;
           $scope.searchtableteacher = false;
           $scope.searchtableemployee = false;
           $scope.searchText = "";
           $scope.student_info = "";
           $scope.edt = "";
           //$scope.temp.s_cur_code = "";
           // $scope.temp.sims_academic_year = "";
           $scope.temp.search_std_grade_name = "";
           $scope.temp.search_std_section_name = "";
           $scope.student = [];
           $('tr').removeClass("row_selected");
           $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
               $scope.ComboBoxValues = AllComboBoxValues.data;
               $scope.temp = {
                   sims_academic_year: $scope.ComboBoxValues[0].sims_acadmic_year,
                   s_cur_code: $scope.ComboBoxValues[0].sims_cur_code,
               };
               console.log($scope.ComboBoxValues);
           });

       }

       $scope.hide1 = function () {
           $scope.searchtable = false;
       }

       $scope.hide2 = function () {
           $scope.searchtableteacher = false;
       }

       $scope.hide3 = function () {
           $scope.searchtableemployee = false;
       }
   }])

})();
