﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, savefin = [], deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Library');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('LibraryItemSearchCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = false;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.Accessno = false;
            $scope.accessnolabel = false;
            $scope.list = false;
            $scope.clr_btn = false;
            $scope.acccodelist_readonly = true;
            var user = $rootScope.globals.currentUser.username;

            

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $(function () {
                $('#cmb_catalogue').multipleSelect({
                    width: '100%'
                });
            });

            $(function () {
                $('#cmb_category').multipleSelect({
                    width: '100%'
                });
            });

            $(function () {
                $('#cmb_subcategory').multipleSelect({
                    width: '100%'
                });
            });


            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                // main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.countData = [

                  { val: 5, data: 5 },
                  { val: 10, data: 10 },
                  { val: 15, data: 15 },

            ]



            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }


                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            // Form Close
            $scope.close = function () {
                $scope.table = true;
                $scope.display = false;
                $scope.grid2 = false;
                $scope.grid1 = false;
                $scope.save_btn = false;
            }

            $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetLibraryCatelogue").then(function (LibraryCatalogue) {
                $scope.Library_catalogue_code = LibraryCatalogue.data;
                console.log($scope.Library_catalogue_code);
                setTimeout(function () {
                    debugger;
                    $('#cmb_catalogue').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });

            $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetLibraryCategory").then(function (CategoryTypes) {
                $scope.Library_category_code = CategoryTypes.data;
                console.log($scope.Library_category_code);
                setTimeout(function () {
                    debugger;
                    $('#cmb_category').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });

            $scope.getSubCat = function (sub_cat) {
                debugger
                $http.get(ENV.apiUrl + "api/LibraryItemSearch/GetLibrarySubCategory?SubCategory=" + sub_cat).then(function (SubCategoryTypes) {
                    $scope.Library_subcategory_code = SubCategoryTypes.data;
                    console.log($scope.Library_subcategory_code);
                    setTimeout(function () {
                        debugger;
                        $('#cmb_subcategory').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });

            }

            $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetBookStyle").then(function (BookStyleCode) {
                $scope.BookStyle_code = BookStyleCode.data;
            });

            $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetBookStatus").then(function (BookStatus) {
                $scope.Book_Status = BookStatus.data;
            });

            $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetBin").then(function (GetBin) {
                $scope.Bin_Status = GetBin.data;
            });

            $scope.Cancelled = function () {
                $('#myModal').modal('hide');
                $scope.searchtable = false;
                $scope.info = "";
            }

            //Show Data  
            $scope.Preview = function () {
                debugger;
                $scope.tempnew = {};
                $scope.tempnew.sims_library_item_number = $scope.temp.sims_library_item_number;
                $scope.tempnew.sims_library_isbn_number = $scope.temp.sims_library_isbn_number;
                $scope.tempnew.sims_library_item_title = $scope.temp.sims_library_item_title;
                $scope.tempnew.sims_library_item_desc = $scope.temp.sims_library_item_desc;
                $scope.tempnew.sims_library_item_category = $scope.temp.sims_library_item_category + "";
                $scope.tempnew.sims_library_item_subcategory = $scope.temp.sims_library_item_subcategory + "";
                $scope.tempnew.sims_library_item_catalogue_code = $scope.temp.sims_library_item_catalogue_code + "";
                $scope.tempnew.sims_library_item_author1 = $scope.temp.sims_library_item_author1;
                $scope.tempnew.sims_library_item_name_of_publisher = $scope.temp.sims_library_item_name_of_publisher;
                $scope.tempnew.sims_library_item_bin_code = $scope.temp.sims_library_item_bin_code;
                $scope.tempnew.sims_library_item_status_code = $scope.temp.sims_library_item_status_code;
                $scope.tempnew.sims_library_item_bar_code = $scope.temp.sims_library_item_bar_code;


                $http.post(ENV.apiUrl + "api/LibraryItemSearch/ItemSearch", $scope.tempnew).then(function (Allstudent) {
                    $scope.filteredTodos = Allstudent.data;
                    if ($scope.filteredTodos.length > 0) {
                        $scope.table = true;
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.filteredTodos.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.filteredTodos.length, data: 'All' })
                        }
                        $scope.totalItems = $scope.filteredTodos.length;
                        $scope.todos = $scope.filteredTodos;
                        $scope.makeTodos();
                    }
                    else {
                        $scope.table = false;
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                        $scope.filteredTodos = [];
                    }


                    //$scope.filteredTodos = Allstudent.data;
                    //$scope.totalItems = $scope.filteredTodos.length;
                    //$scope.todos = $scope.filteredTodos;
                    //$scope.makeTodos();
                    //$scope.busy = false;
                    //$scope.searchtable = true;
                    //console.log($scope.filteredTodos);
                    if (Allstudent.data.length > 0) { }
                    else {
                        swal({ title: "Alert", text: "Sorry! No data found", showCloseButton: true, width: 300, height: 200 });
                    }
                });
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                debugger
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.list = false;
                $scope.clr_btn = false;

            }

            //Reset
            $scope.Reset = function () {
                $scope.temp = '';
                $scope.tempnew = '';
                $state.go($state.current, {}, { reload: true });
            }

            //Navigate To Item Master
            $scope.New = function () {
                $state.go('main.Sim121', { 'ob': true });
            }

            $scope.onlyNumbers = function (event) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $scope.onlyNumbers1 = function (event) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $scope.onlyNumbers2 = function (event) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 45,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57,
                    'A': 65, 'B': 66, 'C': 67, 'D': 68, 'E': 69, 'F': 70, 'G': 71, 'H': 72, 'I': 73, 'J': 74, 'K': 75, 'L': 76, 'M': 77,
                    'N': 78, 'O': 79, 'P': 80, 'Q': 81, 'R': 82, 'S': 83, 'T': 84, 'U': 85, 'V': 86, 'W': 87, 'X': 88, 'Y': 89, 'Z': 90,
                    'a': 97, 'b': 98, 'c': 99, 'd': 100, 'e': 101, 'f': 102, 'g': 103, 'h': 104, 'i': 105, 'j': 106, 'k': 107, 'l': 108, 'm': 109, 'n': 110, 'o': 111,
                    'p': 112, 'q': 113, 'r': 114, 's': 115, 't': 116, 'u': 117, 'v': 118, 'w': 119, 'x': 120, 'y': 121, 'z': 122
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };


            $scope.Export = function () {
                debugger
                    var check = true;
                    debugger;

                    if (check == true) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " LibraryItems.xls ?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 390,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                debugger;
                                var blob = new Blob([document.getElementById('printdata3').innerHTML], {
                                    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                                });
                                saveAs(blob, $scope.obj1.lic_school_name + " LibraryItems" + ".xls");
                            }
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Report Not Save", showCloseButton: true, width: 380, })
                    }
                
            }


        }])

})();