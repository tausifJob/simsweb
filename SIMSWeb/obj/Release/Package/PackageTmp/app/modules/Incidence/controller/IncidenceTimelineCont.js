﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Incidence');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('IncidenceTimelineCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = "5";

            $http.get(ENV.apiUrl + "api/incidence/getIncidenceTimelineByIndex").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.obj = res.data;
                if ($scope.obj.length != 0) {

                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.makeTodos();
                }
                else {
                    swal({ text: 'Sorry Data Not Found', width: 380, showCloseButton: true });
                }

            });

            $scope.maindata = function () {

                $http.get(ENV.apiUrl + "api/incidence/getIncidenceTimelineByIndex").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.obj = res.data;
                    $scope.currentPage = 1;
                    if ($scope.obj.length != 0) {

                        $scope.totalItems = $scope.obj.length;
                        $scope.todos = $scope.obj;
                        $scope.makeTodos();
                    }
                    else {
                        swal({ text: 'Sorry Data Not Found', width: 380, showCloseButton: true });
                    }

                });

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.CheckMultiple();
                }
            };

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.btn_save = false;
                $scope.btn_update = true;
                $scope.btn_delete = false;
                $scope.edt = str;

                var data = angular.copy(str);
                $scope.edt = data;

                if (main.checked == true) {

                    for (var i = 0; i < $scope.obj.length; i++) {
                        var t = $scope.obj[i].sims_incidence_timeline_code; console.log(t);
                        var v = document.getElementById(t);
                        v.checked = false;
                        $scope.row1 = '';
                        main.checked = false;

                    }
                }
            }
            
            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.btn_save = true;
                $scope.btn_update = false;
                $scope.btn_delete = false;
                $scope.edt = "";
                $scope.MyForm.$setPristine();
                $http.get(ENV.apiUrl + "api/incidence/getAutoGenerateTimeline").then(function (AutoGenerateTimeline) {

                    $scope.AutoGenerate_Timeline = AutoGenerateTimeline.data;
                    $scope.edt = {sims_incidence_timeline_code:$scope.AutoGenerate_Timeline[0].sims_incidence_timeline_code};
                })

            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;

            }

            $scope.SaveData = function (isvalid) {

                if (isvalid) {
                    var datasend = [];
                    var data = '';
                    data = $scope.edt;
                    data.opr = 'I';
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/incidence/CUDTimelineDetails",datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: 'Record  Inserted Successfully', width: 380, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;
                            $scope.maindata();
                        }
                        else {
                            swal({ text: 'Record Not Inserted', width: 380, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;
                           
                        }
                    });
                }
            }

            $scope.Update = function () {
                var datasend = [];
                var data = '';
                data = $scope.edt;
                data.opr = 'U';
                datasend.push(data);
                $http.post(ENV.apiUrl + "api/incidence/CUDTimelineDetails" ,datasend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ text: 'Record  Updated Successfully', width: 380, showCloseButton: true });
                        $scope.display = false;
                        $scope.grid = true;
                        $scope.maindata();
                    }
                    else {
                        swal({ text: 'Record Not Updated', width: 380, showCloseButton: true });
                        $scope.display = false;
                        $scope.grid = true;
                       
                    }

                });
            }

            $scope.Delete = function () {

                            var datasend = [];
                            $scope.flag = false;
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_incidence_timeline_code);
                                if (v.checked == true) {
                                    $scope.flag = true;
                                    $scope.filteredTodos[i].opr = 'D';
                                    datasend.push($scope.filteredTodos[i]);
                                }
                            }

                            if ($scope.flag) {
                                swal({
                                    title: '',
                                    text: "Are you sure you want to Delete?",
                                    showCloseButton: true,
                                    showCancelButton: true,
                                    confirmButtonText: 'Yes',
                                    width: 380,
                                    cancelButtonText: 'No',

                                }).then(function (isConfirm) {
                                    if (isConfirm) 
                                    {
                                        $http.post(ENV.apiUrl + "api/incidence/CUDTimelineDetails", datasend).then(function (msg) {
                                            $scope.msg1 = msg.data;
                                            if ($scope.msg1 == true) {
                                                swal({ text: 'Record  Deleted Successfully', width: 380, showCloseButton: true });
                                                $scope.display = false;
                                                $scope.grid = true;
                                                $scope.maindata();
                                            }
                                            else {
                                                swal({ text: 'Record Not Deleted', width: 300, showCloseButton: true });
                                                $scope.display = false;
                                                $scope.grid = true;
                                               
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    $scope.row1 = '';
                                                    $scope.color = '#edefef';
                                                }
                                                $scope.multipledelete();
                                            }
                                        })


                                        }
                                    else {

                                        main = document.getElementById('mainchk');
                                        if (main.checked == true) {
                                            main.checked = false;
                                            $scope.row1 = '';
                                            $scope.color = '#edefef';
                                        }
                                        $scope.multipledelete();
                                           
                                        }
                                })
                                }
                                else {
                                    swal({ text: 'Select At Least One Record To Delete', width: 380, showCloseButton: true });

                                }
            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                  
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                   
                }
            }

            $('.clockpicker ').clockpicker({
                autoclose: true
            });

            $scope.multipledelete = function () { del=[]
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_incidence_timeline_code;
                    var v = document.getElementById(t);
                if (main.checked == true) {
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                }
                else {
                        v.checked = false;
                        main.checked = false;
                        $('tr').removeClass("row_selected");
                    }

                }
            }

            $scope.delete_onebyone = function (str) {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_incidence_timeline_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_incidence_timeline_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

        }])
})();