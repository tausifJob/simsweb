﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Incidence');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('IncidenceCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.pagesize = "5";
         
            $http.get(ENV.apiUrl + "api/incidence/getIncidencesByIndex").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.obj = res.data;
              
                if ($scope.obj.length != 0) {
                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.makeTodos();
                }
                else {
                    swal({ text: 'Sorry Data Not Found', width: 380, showCloseButton: true });
                }

                $scope.active = false;
                $scope.inactive = false;

            });


            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;

                console.log($scope.ComboBoxValues);
            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.CheckMultiple();
                }
            };

            $scope.maindata = function () {

                $http.get(ENV.apiUrl + "api/incidence/getIncidencesByIndex").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.obj = res.data;
                 
                    $scope.currentPage = 1;
                    if ($scope.obj.length != 0) {
                        $scope.totalItems = $scope.obj.length;
                        $scope.todos = $scope.obj;
                        $scope.makeTodos();
                    }
                    else {
                        swal({ text: 'Sorry Data Not Found', width: 380, showCloseButton: true });
                    }

                    $scope.active = false;
                    $scope.inactive = false;

                });

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/incidence/getAllBuildingCode").then(function (AllBuildingCode) {
                $scope.All_Building_Code = AllBuildingCode.data;
            });
            $http.get(ENV.apiUrl + "api/incidence/getAllLocationCode").then(function (AllLocationCode) {
                $scope.All_Location_Code = AllLocationCode.data;
            });
            $http.get(ENV.apiUrl + "api/incidence/getAllWarningCode").then(function (AllWarningCode) {
                $scope.All_Warning_Code = AllWarningCode.data;
            });
            $http.get(ENV.apiUrl + "api/incidence/getAllActionCodes").then(function (getAllActionCodes) {

                $scope.All_Action_Codes = getAllActionCodes.data;
            });
            $http.get(ENV.apiUrl + "api/incidence/getllActionTypeNames").then(function (AllActionTypeNames) {
                $scope.All_Action_TypeNames = AllActionTypeNames.data;
            });
            $http.get(ENV.apiUrl + "api/incidence/getAllConsequenceCode").then(function (AllConsequenceCode) {
                $scope.All_Consequence_Code = AllConsequenceCode.data;
            });
            $http.get(ENV.apiUrl + "api/incidence/getConsequenceTypeNames").then(function (ConsequenceTypeNames) {

                $scope.Consequence_Type_Names = ConsequenceTypeNames.data;
            });

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                var data = angular.copy(str);
                $scope.edt = data;
                if (main.checked == true) {

                    for (var i = 0; i < $scope.obj.length; i++) {
                        var t = $scope.obj[i].sims_incidence_number; console.log(t);
                        var v = document.getElementById(t);
                        v.checked = false;

                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.Update = function () {

                var datasend = [];
                var data = '';
                data = $scope.edt;
                data.opr = 'U';
                datasend.push(data);

                $http.post(ENV.apiUrl + "api/incidence/CUDIncidenceDetails", datasend).then(function (msg) {

                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: 'Record  Updated Successfully', width: 380, showCloseButton: true });
                        $scope.display = false;
                        $scope.grid = true;
                        $scope.maindata();
                    }
                    else {
                        swal({ text: 'Record Not Updated', width: 380, showCloseButton: true });
                        $scope.display = false;
                        $scope.grid = true;
                       
                    }

                });

            }

            $scope.Save = function (isvalid) {
                if (isvalid) {
                    var datasend = [];
                    var data = '';
                    data = $scope.edt;
                    data.opr = 'I';
                    datasend.push(data);

                    $http.post(ENV.apiUrl + "api/incidence/CUDIncidenceDetails", datasend).then(function (msg) {

                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: 'Record  Inserted Successfully', width: 380, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;
                            $scope.currentPage = 1;
                            $scope.maindata();
                        }
                        else {
                            swal({ text: 'Record Not Inserted', width: 380, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;
                           
                        }

                    });
                }

            }

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";

                $scope.MyForm.$setPristine();

                $http.get(ENV.apiUrl + "api/incidence/getAutoGenerateIncidenceNumber").then(function (AutoGenerateIncidenceNumber) {
                    $scope.Auto_GenerateIncidence_Number = AutoGenerateIncidenceNumber.data;
                    var sims_incidence_number = document.getElementById('txt_Incidence_Number');
                    sims_incidence_number.value = $scope.Auto_GenerateIncidence_Number[0].sims_incidence_number;
                })
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
            }

            $scope.Reset = function () {

                $scope.temp = '';
            }

            $scope.Delete = function () {

                var datasend = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_incidence_number);
                    if (v.checked == true) {
                        $scope.flag = true;
                        $scope.filteredTodos[i].opr = 'D';
                        datasend.push($scope.filteredTodos[i]);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {


                            $http.post(ENV.apiUrl + "api/incidence/CUDIncidenceDetails", datasend).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: 'Record  Deleted Successfully', width: 380, showCloseButton: true });
                                    $scope.display = false;
                                    $scope.grid = true;
                                    $scope.currentPage = 1;
                                    $scope.maindata();
                                }
                                else {
                                    swal({ text: 'Record Not Deleted', width: 300, showCloseButton: true });
                                    $scope.display = false;
                                    $scope.grid = true;
                                    main = document.getElementById('mainchk');
                                    if (main.checked == true) {
                                        main.checked = false;
                                        $scope.row1 = '';
                                        $scope.color = '#edefef';
                                    }
                                    $scope.check();
                                }
                            })


                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                                $scope.row1 = '';
                                $scope.color = '#edefef';
                            }
                            $scope.check();
                        }
                    })
                }
                else {
                    swal({ text: 'Select At Least One Record To Delete', width: 380, showCloseButton: true });

                }

            }

            $scope.check = function () {
                main = document.getElementById('mainchk');
                del = [null];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_incidence_number; console.log(t);
                    var v = document.getElementById(t);
                    if (main.checked == true) {
                        // $scope.obj1 = res.data;
                        v.checked = true;
                        del.push({ id: t });
                        $scope.row1 = 'row_selected';

                    }
                    else {
                        // $scope.obj1 = res.data;
                        main.checked = false;
                        v.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }
            }

            $scope.check1 = function (str) {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }

            }


            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format:"mm-dd-yyyy"

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            var dom;
            $scope.flag = true;
            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='12'>" +
                    "<table class='inner-table' width='100%' cellpadding='5' cellspacing='0' style='border-color:red;border:solid;border-width:02px'>" +
                    "<tbody>" +
                     "<tr> <td class='semi-bold'>" + "ACTION CODE" + "</td> <td class='semi-bold'>" + "ACTION TYPE" + " </td><td class='semi-bold'>" + "ACTION DESCRIPTION" + "</td><td class='semi-bold'>" + "ACTION POINT" + "</td><td class='semi-bold'>" + "WARNING CODE" + " </td>" +
                    "<td class='semi-bold'>" + "SERVER CLOSE FLAG" + "</td></tr>" +

                      "<tr><td>" + (info.sims_incidence_action_code) + "</td> <td>" + (info.sims_incidence_action_type) + " </td><td>" + (info.sims_incidence_action_desc) + "</td><td>" + (info.sims_incidence_action_point) + "</td><td>" + (info.sims_incidence_warning_code) + "</td>" +
                    "<td>" + (info.sims_incidence_consequence_serve_close_flag) + "</td></tr>" +


                     "<tr> <td class='semi-bold'>" + "CONSEQUENCE CODE" + "</td> <td class='semi-bold'>" + "CONSEQUENCE DESCRIPTION" + " </td><td class='semi-bold'>" + "CONSEQUENCE TYPE" + "</td><td class='semi-bold'>" + "CONSEQUENCE POINT" + "</td> <td class='semi-bold'>" + "SERVER START DATE" + " </td><td class='semi-bold'>" + "USER CODE" + "</td></tr>" +
                     "<tr><td>" + (info.sims_incidence_consequence_code) + "</td> <td>" + (info.sims_incidence_consequence_desc) + " </td><td>" + (info.sims_incidence_consequence_type) + "</td><td>" + (info.sims_incidence_consequence_point) + "</td> <td>" + (info.sims_incidence_consequence_serve_start_date) + " </td><td>" + (info.sims_incidence_user_code) + "</td></tr>" +


                     "<tr> <td class='semi-bold'>" + "USER CODE UPDATED" + "</td> <td class='semi-bold'>" + "ESCALATION FLAG" + " </td><td class='semi-bold'>" + "SMS FLAG" + "</td><td class='semi-bold'>" + "EMAIL FLAG" + "</td> <td class='semi-bold'>" + "PORTAL FLAG" + " </td><td class='semi-bold'>" + "ROLL OVER FLAG" + "</td></tr>" +
                     "<tr><td>" + (info.sims_incidence_user_code_updated) + "</td> <td>" + (info.sims_incidence_action_escalation_flag) + " </td><td>" + (info.sims_incidence_action_sms_flag) + "</td><td>" + (info.sims_incidence_action_email_flag) + "</td> <td>" + (info.sims_incidence_action_portal_flag) + " </td><td>" + (info.sims_incidence_action_roll_over_flag) + "</td></tr>" +
                    " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }

            };


            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_incidence_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_incidence_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }


            $scope.SearchStudent = function () {
                $scope.temp = '';
                $scope.student = [];
                $scope.searchtable = false;
                $("#MyModal").modal({}).draggable();
                $('#MyModal').modal('show');
            }


            $scope.SearchSudent = function () {

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }


                $scope.searchtable = false;
                $scope.busy = true;
                $scope.sibling_result



                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.temp)).then(function (Allstudent) {

                    $scope.student = Allstudent.data;
                    $scope.searchtable = true;
                    $scope.busy = false;


                });
            }

            $scope.StudentEnrollNumber = function (info) {

                $scope.edt = { sims_incidence_enroll_number: info.s_enroll_no };
            }

        }])
})();