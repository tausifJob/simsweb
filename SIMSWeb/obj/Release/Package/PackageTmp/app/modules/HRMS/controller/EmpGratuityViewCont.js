﻿(function () {
    'use strict';
    var prv;
    var nxt;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmpGratuityViewCont',
      ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

          var att_date1, att_date;
          $scope.emptab = false;
          $scope.data1 = [];

         

          $http.get(ENV.apiUrl + "api/EmpGrtview/getCompanyName").then(function (comp) {
              debugger;
              $scope.comp_data = comp.data;
              $scope.temp = {
                  'company_code': $scope.comp_data[0].company_code
              }
              $scope.getDept($scope.temp.company_code);
          });
          
          $scope.getDept = function (company_code) {
              
              $http.get(ENV.apiUrl + "api/EmpGrtview/getDeptName?comp_code=" + company_code).then(function (dept) {
                  $scope.dept_data = dept.data;
               
                  $scope.temp = {
                      'company_code': $scope.comp_data[0].company_code,
                      'dept_code': $scope.dept_data[0].dept_code
                  }
              });
          }
         
          $http.get(ENV.apiUrl + "api/EmpGrtview/getDesignation").then(function (designation) {
                  $scope.designation_data = designation.data;
              });

          $http.get(ENV.apiUrl + "api/EmpGrtview/getYear").then(function (year) {
              $scope.year_data = year.data;
          });

          $timeout(function () {
              $("#fixtable").tableHeadFixer({ 'top': 1 });
          }, 100);

          $scope.propertyName = null;
          $scope.reverse = false;

          $scope.sortBy = function (propertyName) {
              $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
              $scope.propertyName = propertyName;
          };

          $scope.clear = function () {
                  $scope.emptab = false;
                  $scope.temp.employee_no = "";
                  $scope.temp.company_code = "";
                  $scope.temp.dept_code = "";
                  $scope.temp.gr_code = "";
                  $scope.temp.staff_type_code = "";
                  $scope.temp.dg_code = "";
                  $scope.temp.sims_academic_year = "";
                  $scope.temp.sims_academic_month = "";
                  $scope.temp.sort_code = "";
                  $scope.edt.att_emp_id = "";
              }
        
          $scope.getMonthbyYear = function (str) {
              
                  $scope.data = [];
                  prv = parseInt(str) - 1;
                  nxt = parseInt(str);

                  var start_date = new Date();
                  start_date = moment(prv + '/08/01').format('YYYY/MM/DD');

                  var end_date = new Date();
                  end_date = moment(nxt + '/08/01').format('YYYY/MM/DD')
                  var check_date = start_date;

                  for (; new Date(check_date) < new Date(end_date) ;) {
                      check_date = moment(check_date, "YYYY/MM/DD").add('months', 1);
                      check_date = moment(check_date).format('YYYY/MM/DD');
                      $scope.data.push({ date: moment(check_date).format('MMMM-YYYY'), dateCode: moment(check_date).format('MM') })
                  }
              }

          $scope.getDays_in_month = function (sims_month_name, sims_year) {
              debugger;
              var sims_year;
              if (sims_month_name == "09" || sims_month_name == "10" || sims_month_name == "11" || sims_month_name == "12") {
                  sims_year = parseInt(sims_year) - 1;
              }
              else {
                  sims_year = sims_year;
              }

              $scope.yr_mon = sims_year + sims_month_name;
              }
          
          $scope.getEmpGrt = function (comp_code,dept_code,desg_code,emp_no,yrmon)
          {
              debugger;
              $http.get(ENV.apiUrl + "api/EmpGrtview/getAllGratuityData?comp_code=" + comp_code + "&dept_code=" + dept_code + "&desg_code=" + desg_code + "&emp_no=" + emp_no
                  + "&yearmonth=" + $scope.yr_mon).then(function (allEmpGrtData) {
                      debugger;
                      $scope.all_EmpGrt_Data = allEmpGrtData.data;
                      $scope.emptab = true;
              });

          }

      }])

})();
