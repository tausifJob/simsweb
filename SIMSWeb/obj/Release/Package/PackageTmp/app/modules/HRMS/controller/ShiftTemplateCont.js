﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ShiftTemplateCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.expanded = true;
            $scope.display = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.grid = true;
            $scope.edit_data = false;
            $scope.shifttemplate_data = [];
            var sh_template_name = [];
            var data1 = [];
            var deletecode = [];
            $scope.valstatus = false;
            $scope.tempid = false;

            $http.get(ENV.apiUrl + "api/common/ShiftMaster/getCompany").then(function (res) {
                debugger
                $scope.comp_data = res.data;
                $scope.edt = {
                    'sh_company_code': $scope.comp_data[0].sh_company_code
                }
                console.log($scope.comp_data);
            });

            $timeout(function () {
                $("#fixedtable,#fixedtable1,#fixedtable2,#fixedtable3").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.getdetails = function (comp_code) {
                $http.get(ENV.apiUrl + "api/common/ShiftTemplate/GetShiftTemplateDetails?company_code=" + comp_code).then(function (res) {
                    $scope.display = true;
                    $scope.grid = true;
                    $scope.shifttemplate_data = res.data;
                });
            }

            $scope.get_ischecked = function (shift_temp) {
                shift_temp.ischecked = true;
            }

            $scope.btn_create = function (comp_code) {
                $scope.tem = [];
                $scope.temp = false;


                if (comp_code === undefined) {
                    swal({ title: "Alert", text: "Please Select Company", showCloseButton: true, width: 380, });
                }
                else {
                    $http.get(ENV.apiUrl + "api/common/ShiftTemplate/GetAllShift").then(function (res) {
                        $scope.shift_data = res.data;
                    });

                    $http.get(ENV.apiUrl + "api/common/ShiftTemplate/getAllTemplateName?company_code=" + $scope.edt.sh_company_code).then(function (res) {
                        $scope.AllTemplateName = res.data;
                    });

                    $('#shiftModal').modal({ backdrop: 'static', keyboard: true });
                }


            }

            $scope.getTempalte = function (temp_name) {
                $scope.tempid = true;

                $http.get(ENV.apiUrl + "api/common/ShiftTemplate/GetIdOfTemplateName?templateName=" + temp_name).then(function (res) {
                    $scope.shifttemp_data = res.data;
                    console.log($scope.shifttemp_data);

                    for (var i = 0; i < $scope.shifttemp_data.length; i++) {
                        $scope.tem.sh_template_id = $scope.shifttemp_data[i].sh_template_id,
                        $scope.tem.sh_template_name = temp_name,
                        $scope.tem.sh_day_status = $scope.shifttemp_data[i].sh_template_status
                    }

                    $http.get(ENV.apiUrl + "api/common/ShiftTemplate/GetAllShiftEdit?company_code=" + $scope.edt.sh_company_code + "&template_id=" + $scope.tem.sh_template_id).then(function (res) {
                        $scope.ShiftEdit_data = res.data;
                        $scope.shift_data = $scope.ShiftEdit_data;
                        console.log($scope.shift_data);
                        $scope.valstatus = false;
                    });

                    $http.get(ENV.apiUrl + "api/common/ShiftTemplate/GetStatusTemplate?company_code=" + $scope.edt.sh_company_code + "&template_id=" + $scope.tem.sh_template_id).then(function (res) {
                        $scope.StatusTemplate_data = res.data;
                        console.log($scope.StatusTemplate_data);

                        if ($scope.StatusTemplate_data > 0) {
                            $scope.tem.sh_day_status = true;
                        }
                        else {
                            $scope.tem.sh_day_status = false;
                        }
                    });
                });



            }

            $scope.btn_edit = function (comp_code) {
                $scope.tem = [];
                $scope.temp = false;

                if (comp_code === undefined) {
                    swal({ title: "Alert", text: "Please Select Company", showCloseButton: true, width: 380, });
                }
                else {
                    $http.get(ENV.apiUrl + "api/common/ShiftTemplate/GetAllShift").then(function (res) {
                        $scope.shift_data = res.data;
                    });

                    $http.get(ENV.apiUrl + "api/common/ShiftTemplate/getAllTemplateName?company_code=" + $scope.edt.sh_company_code).then(function (res) {
                        $scope.AllTemplateName = res.data;
                    });

                    $('#shiftEditModal').modal({ backdrop: 'static', keyboard: true });

                }


            }

            $scope.btn_Editclear = function () {
                $scope.tem = [];

                $http.get(ENV.apiUrl + "api/common/ShiftTemplate/GetAllShift").then(function (res) {
                    $scope.shift_data = res.data;
                    $scope.valstatus = true;
                });
            }

            $scope.btn_clear = function () {
                $scope.tem = [];
                $scope.tempid = false;
                $scope.temp = false;
                $scope.btn_create();

            }

            $scope.gettempId = function () {
                $scope.temp = true;

                console.log($scope.tem.sh_template_id);
                $http.get(ENV.apiUrl + "api/common/ShiftTemplate/getCheckedTemplateId?TemplateId=" + $scope.tem.sh_template_id).then(function (res) {
                    $scope.TemplateId_data = res.data;
                    if ($scope.TemplateId_data == true) {
                        swal({ title: "Alert", text: "Template Id is Already Exist", showCloseButton: true, width: 380, });
                        $scope.tem.sh_template_id = "";
                    }
                });


            }

            $scope.chk_day_status = function () {
                if ($scope.tem.sh_day_status == false) {
                    $http.get(ENV.apiUrl + "api/common/ShiftTemplate/getCheckEmployeeMapped?company_code=" + $scope.edt.sh_company_code + "&template_id=" + $scope.tem.sh_template_id).then(function (res) {
                        $scope.EmployeeMapped_data = res.data;
                        if (EmployeeMapped_data == true) {
                            swal({ title: "Alert", text: "Sorry\nRecord Cannot Uncheck because its already mapped to employee", showCloseButton: true, width: 380, });
                        }
                    });

                }
            }

            $scope.get_isdaychecked = function (shift_day) {
                shift_day.isdaychecked = true;
            }

            $scope.getSubmitData = function (isvalidate) {

                var data1 = [];
                var data = [];
                $scope.insert = false;
                if (isvalidate) {
                    for (var i = 0; i < $scope.shift_data.length; i++) {
                        for (var j = 0; j < $scope.shift_data[i].assign.length; j++) {
                            if ($scope.shift_data[i].assign[j].isdaychecked == true) {
                                if ($scope.tem.sh_day_status == true) {
                                    var data = {
                                        sh_company_code: $scope.edt.sh_company_code,
                                        sh_template_name: $scope.tem.sh_template_name,
                                        sh_template_status: $scope.tem.sh_day_status,
                                        sh_template_id: $scope.tem.sh_template_id,
                                        sh_day_code: $scope.shift_data[i].sh_day_code,
                                        sh_shift_id: $scope.shift_data[i].assign[j].sh_shift_id,
                                        opr: 'I'
                                    };
                                    $scope.insert = true;
                                    data1.push(data);
                                    console.log(data1);
                                }
                            }
                        }
                    }

                    if ($scope.insert) {
                        $http.post(ENV.apiUrl + "api/common/ShiftTemplate/CUDshift_data", data1).then(function (res) {
                            $scope.shift_rec = res.data;

                            if ($scope.shift_rec == true) {
                                swal({ title: "Alert", text: "Record Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $('#shiftModal').modal('hide');
                                        $scope.getdetails($scope.edt.sh_company_code);
                                    }
                                });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $('#shiftModal').modal('hide');
                                        $scope.getdetails($scope.edt.sh_company_code);
                                    }
                                });
                            }
                        });
                    }
                    //else
                    //{
                    //    swal({ title: "Alert", text: "Please Select Atleast One Shift", showCloseButton: true, width: 380, });
                    //}

                    $scope.myForm1.$setPristine();
                    $scope.myForm1.$setUntouched();
                }

            }

            $scope.getEditData = function (isvalidate) {
                debugger;
                var data1 = [];
                var data = [];
                $scope.update = false;
                if (isvalidate) {
                    for (var i = 0; i < $scope.shift_data.length; i++) {
                        for (var j = 0; j < $scope.shift_data[i].assign.length; j++) {
                            if ($scope.shift_data[i].assign[j].isdaychecked == true) {
                                if ($scope.tem.sh_day_status == true) {
                                    var data = {
                                        sh_company_code: $scope.edt.sh_company_code,
                                        sh_template_name: $scope.tem.sh_template_name,
                                        sh_template_status: $scope.tem.sh_day_status,
                                        sh_template_id: $scope.tem.sh_template_id,
                                        sh_day_code: $scope.shift_data[i].sh_day_code,
                                        sh_shift_id: $scope.shift_data[i].assign[j].sh_shift_id,
                                        opr: 'E'
                                    };
                                    $scope.update = true;
                                    data1.push(data);
                                }
                            }
                        }
                    }

                    if ($scope.update) {
                        $http.post(ENV.apiUrl + "api/common/ShiftTemplate/CUDshift_data", data1).then(function (res) {
                            $scope.shift_rec = res.data;

                            if ($scope.shift_rec == true) {
                                swal({ title: "Alert", text: "Record Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $('#shiftEditModal').modal('hide');
                                        $scope.getdetails($scope.edt.sh_company_code);
                                    }
                                });
                            }
                            else {
                                swal({ title: "Alert", text: "Record data Not Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $('#shiftEditModal').modal('hide');
                                        $scope.getdetails($scope.edt.sh_company_code);
                                    }
                                });
                            }
                        });
                    }
                    $scope.myForm2.$setPristine();
                    $scope.myForm2.$setUntouched();
                }

                // $scope.myForm2.$setPristine();
                // $scope.myForm2.$setUntouched();
            }

            $scope.Delete = function () {
                sh_template_name = [];
                var data1 = [];
                var data = [];
                $scope.del = false;


                for (var i = 0; i < $scope.shifttemplate_data.length; i++) {
                    var t = $scope.shifttemplate_data[i].sh_template_name;
                    var v = document.getElementById(t + i);

                    if ($scope.shifttemplate_data[i].ischecked == true) {
                        if (v.checked == true) {
                            data = {
                                sh_template_name: $scope.shifttemplate_data[i].sh_template_name,
                                opr: 'D'
                            };

                            $scope.del = true;
                        }
                        data1.push(data);
                    }
                }
                if ($scope.del) {
                    $http.post(ENV.apiUrl + "api/common/ShiftTemplate/CUDDelete_Shift_template", data1).then(function (res) {
                        $scope.shift_rec = res.data;
                        if ($scope.shift_rec == true) {
                            swal({ title: "Alert", text: "Shift Template Data Deleted", imageUrl: "assets/img/check.png", },
                                  function () {
                                      $scope.getdetails($scope.edt.sh_company_code);
                                  }
                                );
                        }
                        else {
                            swal({ title: "Alert", text: "Shift Template Data not deleted", imageUrl: "assets/img/notification-alert.png", },
                                 function () {
                                     $scope.getdetails($scope.edt.sh_company_code);
                                 }
                                );
                        }

                    });
                }
                else {
                    swal({ title: "Alert", text: "Select Atleast One Record", showCloseButton: true, width: 380, });

                }

            }

            $scope.checkAll = function () {
                sh_template_name = [];
                main = document.getElementById('mainchk');

                console.log($scope.shifttemplate_data);
                if (main.checked == true) {
                    for (var i = 0; i < $scope.shifttemplate_data.length; i++) {
                        var t = $scope.shifttemplate_data[i].sh_template_name;
                        var v = document.getElementById(t + i);

                        //  if ($scope.filteredTodos[i].sims_head_teacher_code != "" && $scope.filteredTodos[i].sims_grade_name != "" && $scope.filteredTodos[i].sims_section_name != "")
                        {
                            v.checked = true;
                            sh_template_name = sh_template_name + $scope.shifttemplate_data[i].sh_template_name + ',';
                        }
                    }
                }
                else {

                    for (var i = 0; i < $scope.shifttemplate_data.length; i++) {
                        var t = $scope.shifttemplate_data[i].sh_template_name;
                        var v = document.getElementById(t + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        sh_template_name = [];
                    }
                }
                console.log(sh_template_name);
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.shifttemplate_data.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.shifttemplate_data.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.DeleteNew = function (shift_temp) {
                var data1 = [];
                var data = [];
                $scope.del = false;

                $scope.shift_temp = shift_temp;


                //  if ($scope.shift_temp.ischecked == true)
                //   {
                data = {
                    sh_template_name: $scope.shift_temp.sh_template_name,
                    opr: 'D'
                };

                $scope.del = true;

                data1.push(data);

                swal({
                    title: '',
                    text: "Are you sure you want to Delete?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        $http.post(ENV.apiUrl + "api/common/ShiftTemplate/CUDDelete_Shift_template", data1).then(function (res) {
                            $scope.shift_rec = res.data;
                            if ($scope.shift_rec == true) {
                                swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Deleted Successfully", showCloseButton: true, width: 380, });
                            }

                            $scope.getdetails($scope.edt.sh_company_code);
                        });
                    }
                });
                // }

            }

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edit_data = true;
                $scope.edt = str;
            }

            $scope.New = function () {
                var autoid;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edit_data = true;
                $scope.edt = [];
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;

            }

        }])
})();