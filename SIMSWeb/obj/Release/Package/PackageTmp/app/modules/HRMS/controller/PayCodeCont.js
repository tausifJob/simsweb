﻿(function () {
    'use strict';
    var opr = '';
    var paycode = [];
    var main;
    var data1 = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PayCodeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';

            $scope.table1 = true;
            $http.get(ENV.apiUrl + "api/PayCode/getPayCode").then(function (getPayCode_Data) {
                $scope.PayCode_Data = getPayCode_Data.data;
                $scope.totalItems = $scope.PayCode_Data.length;
                $scope.todos = $scope.PayCode_Data;
                $scope.makeTodos();
                console.log($scope.PayCode_Data);

            });

            $http.get(ENV.apiUrl + "api/common/getCompanyName").then(function (getCompanyName_Data) {
                $scope.CompanyName_Data = getCompanyName_Data.data;
            });

            $http.get(ENV.apiUrl + "api/common/getDeductionTag").then(function (getDeductionTag_Data) {
                $scope.DeductionTag_Data = getDeductionTag_Data.data;
            });

            $http.get(ENV.apiUrl + "api/common/getCitiExpTag").then(function (getCitiExpTag_Data) {
                $scope.CitiExpTag_Data = getCitiExpTag_Data.data;
            });
            $scope.pagesize = '5';

            $scope.table1 = true;
            $http.get(ENV.apiUrl + "api/PayCode/getPayCode").then(function (getPayCode_Data) {
                $scope.PayCode_Data = getPayCode_Data.data;
                $scope.totalItems = $scope.PayCode_Data.length;
                $scope.todos = $scope.PayCode_Data;
                $scope.makeTodos();
                console.log($scope.PayCode_Data);

            });

            $http.get(ENV.apiUrl + "api/common/getCompanyName").then(function (getCompanyName_Data) {
                $scope.CompanyName_Data = getCompanyName_Data.data;
            });

            $http.get(ENV.apiUrl + "api/common/getDeductionTag").then(function (getDeductionTag_Data) {
                $scope.DeductionTag_Data = getDeductionTag_Data.data;
            });

            $http.get(ENV.apiUrl + "api/common/getCitiExpTag").then(function (getCitiExpTag_Data) {
                $scope.CitiExpTag_Data = getCitiExpTag_Data.data;
            });

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.operation = false;
            $scope.editmode = false;
            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['chk_all'] = false;
                $scope.row1 = '';
            }
            $scope.edt = "";

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.editmode = false;
                //    opr = 'S';
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = {};
                $scope.edt['pc_company_grade_tag'] = true;

            }

            $scope.up = function (str) {

                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.operation = true;

                $scope.edt = {
                    pc_code: str.pc_code,
                    pc_company_code: str.pc_company_code,
                    company_name: str.company_name,
                    pc_earn_dedn_tag: str.pc_earn_dedn_tag,
                    pc_citi_exp_tag: str.pc_citi_exp_tag,
                    pc_company_grade_tag: str.pc_company_grade_tag,
                    cp_desc: str.cp_desc,
                    cp_change_desc: str.cp_change_desc,
                    cp_display_order: str.cp_display_order
                }

            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.edt = {
                    pc_company_code: '',
                    pc_earn_dedn_tag: '',
                    pc_citi_exp_tag: '',
                    pc_company_grade_tag: '',
                    cp_desc: '',
                    cp_change_desc: '',
                    cp_display_order: ''

                }

            }

            $scope.Save = function (myForm) {
                if (myForm) {
                    data1 = [];
                    var data = $scope.edt;
                    data.opr = 'I';
                    $scope.exist = false;
                    for (var i = 0; i < $scope.PayCode_Data.length; i++) {
                        if ($scope.PayCode_Data[i].pc_company_code == data.pc_company_code &&
                            $scope.PayCode_Data[i].pc_citi_exp_tag == data.pc_citi_exp_tag &&
                            $scope.PayCode_Data[i].pc_earn_dedn_tag == data.pc_earn_dedn_tag &&
                            $scope.PayCode_Data[i].cp_desc == data.cp_desc &&
                            $scope.PayCode_Data[i].cp_change_desc == data.cp_change_desc &&
                            $scope.PayCode_Data[i].cp_display_order == data.cp_display_order) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ title: 'Alert', text: "Display order Already exists", width: 300, height: 200 });
                    }

                    else {
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/PayCode/PayCodeCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: 'Alert', text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: 'Alert', text: "Record not Inserted", width: 300, height: 200 });
                            }

                            $scope.operation = false;
                            $http.get(ENV.apiUrl + "api/PayCode/getPayCode").then(function (getPayCode_Data) {
                                $scope.PayCode_Data = getPayCode_Data.data;
                                $scope.totalItems = $scope.PayCode_Data.length;
                                $scope.todos = $scope.PayCode_Data;
                                $scope.makeTodos();

                            });
                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                }
            }

            $scope.Update = function (myForm) {
                if (myForm) {
                    data1 = [];
                    var data = {
                        'pc_code': $scope.edt.pc_code,
                        'pc_company_code': $scope.edt.pc_company_code,
                        'pc_earn_dedn_tag': $scope.edt.pc_earn_dedn_tag,
                        'pc_citi_exp_tag': $scope.edt.pc_citi_exp_tag,
                        'pc_company_grade_tag': $scope.edt.pc_company_grade_tag,
                        'cp_desc': $scope.edt.cp_desc,
                        'cp_change_desc': $scope.edt.cp_change_desc,
                        'cp_display_order': $scope.edt.cp_display_order
                    }
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/PayCode/PayCodeCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            debugger
                            swal({ title: 'Alert', text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: 'Alert', text: "Record Not Updated", width: 300, height: 200 });
                        }
                        $scope.operation = false;
                        $http.get(ENV.apiUrl + "api/PayCode/getPayCode").then(function (getPayCode_Data) {
                            $scope.PayCode_Data = getPayCode_Data.data;
                            $scope.totalItems = $scope.PayCode_Data.length;
                            $scope.todos = $scope.PayCode_Data;
                            $scope.makeTodos();

                        });
                    })

                    $scope.operation = false;
                    $scope.table1 = true;
                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                debugger
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].pc_code);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].pc_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            //$scope.deleterecord = function () {

            //    paycode = [];

            //    for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //        var v = document.getElementById($scope.filteredTodos[i].pc_code);

            //        if (v.checked == true)

            //            paycode = paycode + $scope.filteredTodos[i].pc_code + ',';
            //    }

            //    var deletelocacode = ({
            //        'pc_code': paycode,
            //        'opr': 'D'
            //    });

            //    $http.post(ENV.apiUrl + "api/PayCode/PayCodeCUD", deletelocacode).then(function (msg) {
            //        $scope.msg1 = msg.data;
            //        $scope.table1 = true;
            //        $scope.operation = false;
            //        $rootScope.strMessage = $scope.msg1.strMessage;

            //        $http.get(ENV.apiUrl + "api/PayCode/getPayCode").then(function (getPayCode_Data) {
            //            $scope.PayCode_Data = getPayCode_Data.data;
            //            $scope.totalItems = $scope.PayCode_Data.length;
            //            $scope.todos = $scope.PayCode_Data;
            //            $scope.makeTodos();
            //            if ($scope.msg1 == true) {

            //                $rootScope.strMessage = 'Record Deleted Successfully';

            //                $('#message').modal('show');
            //            }
            //            else {


            //                $rootScope.strMessage = 'Record Not Deleted';

            //                $('#message').modal('show');
            //            }

            //        });
            //    });

            //}

            $scope.deleterecord = function () {
                paycode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].pc_code);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deleteroutecode = ({
                            'pc_code': $scope.filteredTodos[i].pc_code,
                            opr: 'D'
                        });
                        paycode.push(deleteroutecode);
                    }
                }
                debugger
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/PayCode/PayCodeCUD", paycode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/PayCode/getPayCode").then(function (getPayCode_Data) {
                                                $scope.PayCode_Data = getPayCode_Data.data;
                                                $scope.totalItems = $scope.PayCode_Data.length;
                                                $scope.todos = $scope.PayCode_Data;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/PayCode/getPayCode").then(function (getPayCode_Data) {
                                                $scope.PayCode_Data = getPayCode_Data.data;
                                                $scope.totalItems = $scope.PayCode_Data.length;
                                                $scope.todos = $scope.PayCode_Data;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                main.checked = false;
                $scope.currentPage = true;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.PayCode_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.PayCode_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                     item.company_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.cp_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.dedn_tag.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.cp_display_order == toSearch ||
                     item.pc_code == toSearch) ? true : false;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

        }])
})();
