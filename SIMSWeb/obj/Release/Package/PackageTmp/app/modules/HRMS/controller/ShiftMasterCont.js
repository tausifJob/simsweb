﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ShiftMasterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.grid = true;
            $scope.edit_data = false;
            $scope.shift_data = [];
            $scope.edit_code = false;
            var data1 = [];
            var deletecode = [];

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $http.get(ENV.apiUrl + "api/common/ShiftMaster/getShiftMaster").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.shift_data = res.data;
                $scope.totalItems = $scope.shift_data.length;
                $scope.todos = $scope.shift_data;
                $scope.makeTodos();
                $scope.grid = true;
                for (var i = 0; i < $scope.totalItems; i++) {
                    $scope.shift_data[i].icon = "fa fa-plus-circle";
                }
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edit_data = true;
                $scope.edit_code = true;
                // $scope.edt = str;
                $scope.edt =
                    {
                        sh_company_code: str.sh_company_code,
                        sh_shift_id: str.sh_shift_id,
                        sh_shift_desc: str.sh_shift_desc,
                        sh_shift1_in: str.sh_shift1_in,
                        sh_shift1_out: str.sh_shift1_out,
                        sh_shift2_in: str.sh_shift2_in,
                        sh_shift2_out: str.sh_shift2_out,
                        sh_shift1_in_greece_start: str.sh_shift1_in_greece_start,
                        sh_shift1_in_greece_end: str.sh_shift1_in_greece_end,
                        sh_shift1_out_greece_start: str.sh_shift1_out_greece_start,
                        sh_shift1_out_greece_end: str.sh_shift1_out_greece_end,
                        sh_shift2_in_greece_start: str.sh_shift2_in_greece_start,
                        sh_shift2_in_greece_end: str.sh_shift2_in_greece_end,
                        sh_shift2_out_greece_start: str.sh_shift2_out_greece_start,
                        sh_shift2_out_greece_end: str.sh_shift2_out_greece_end,

                    }
            }

            $http.get(ENV.apiUrl + "api/common/ShiftMaster/getCompany").then(function (res) {
                $scope.comp_data = res.data;
                console.log($scope.comp_data);
            });

            $scope.New = function () {
                var autoid;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edit_data = true;
                $scope.edit_code = false;
                $scope.edt = [];

                $http.get(ENV.apiUrl + "api/common/ShiftMaster/getShiftId").then(function (res) {
                    autoid = res.data;
                    $scope.edt['sh_shift_id'] = autoid;
                });
            }

            $scope.get_shift1_out = function () {
                // TimeSpan t1 = new TimeSpan();
                // t1 = rdt_shift1_in.SelectedTime.Value;
                //TimeSpan interval = new TimeSpan(1, 00, 00);
                //if (!string.IsNullOrEmpty(t1.ToString()))
                //{
                //    rdt_shift1_out.IsEnabled = true;
                //    rdt_shift1_out.StartTime = rdt_shift1_in.SelectedTime.Value + interval;
                //}
            }

            $scope.Save = function (isvalidate) {
                var data1 = [];

                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = ({
                            sh_company_code: $scope.edt.sh_company_code,
                            sh_shift_id: $scope.edt.sh_shift_id,
                            sh_shift_desc: $scope.edt.sh_shift_desc,
                            sh_shift1_in: $scope.edt.sh_shift1_in,
                            sh_shift1_out: $scope.edt.sh_shift1_out,
                            sh_shift2_in: $scope.edt.sh_shift2_in,
                            sh_shift2_out: $scope.edt.sh_shift2_out,
                            sh_shift1_in_greece_start: $scope.edt.sh_shift1_in_greece_start,
                            sh_shift1_in_greece_end: $scope.edt.sh_shift1_in_greece_end,
                            sh_shift1_out_greece_start: $scope.edt.sh_shift1_out_greece_start,
                            sh_shift1_out_greece_end: $scope.edt.sh_shift1_out_greece_end,
                            sh_shift2_in_greece_start: $scope.edt.sh_shift2_in_greece_start,
                            sh_shift2_in_greece_end: $scope.edt.sh_shift2_in_greece_end,
                            sh_shift2_out_greece_start: $scope.edt.sh_shift2_out_greece_start,
                            sh_shift2_out_greece_end: $scope.edt.sh_shift2_out_greece_end,
                            opr: 'I'
                        });

                        data1.push(data);

                        $http.post(ENV.apiUrl + "api/common/ShiftMaster/ShiftMasterINSERT", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            console.log($scope.msg1);
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Shift Master Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal({ title: "Alert", text: "Shift Master Not Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $scope.currentPage = 1;
                $http.get(ENV.apiUrl + "api/common/ShiftMaster/getShiftMaster").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.shift_data = res.data;
                    $scope.totalItems = $scope.shift_data.length;
                    $scope.todos = $scope.shift_data;
                    $scope.makeTodos();
                    $scope.grid = true;

                    for (var i = 0; i < $scope.totalItems; i++) {
                        $scope.shift_data[i].icon = "fa fa-plus-circle";
                    }
                });


                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }


            $scope.Update = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {

                    var data = ({
                        sh_company_code: $scope.edt.sh_company_code,
                        sh_shift_id: $scope.edt.sh_shift_id,
                        sh_shift_desc: $scope.edt.sh_shift_desc,
                        sh_shift1_in: $scope.edt.sh_shift1_in,
                        sh_shift1_out: $scope.edt.sh_shift1_out,
                        sh_shift2_in: $scope.edt.sh_shift2_in,
                        sh_shift2_out: $scope.edt.sh_shift2_out,
                        sh_shift1_in_greece_start: $scope.edt.sh_shift1_in_greece_start,
                        sh_shift1_in_greece_end: $scope.edt.sh_shift1_in_greece_end,
                        sh_shift1_out_greece_start: $scope.edt.sh_shift1_out_greece_start,
                        sh_shift1_out_greece_end: $scope.edt.sh_shift1_out_greece_end,
                        sh_shift2_in_greece_start: $scope.edt.sh_shift2_in_greece_start,
                        sh_shift2_in_greece_end: $scope.edt.sh_shift2_in_greece_end,
                        sh_shift2_out_greece_start: $scope.edt.sh_shift2_out_greece_start,
                        sh_shift2_out_greece_end: $scope.edt.sh_shift2_out_greece_end,
                        opr: 'U'
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/ShiftMaster/ShiftMasterINSERT", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Shift Master Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });

                        }
                        else {
                            swal({ title: "Alert", text: "Shift Master Not Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                    });
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Delete = function () {
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);

                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sh_company_code': $scope.filteredTodos[i].sh_company_code,
                            'sh_shift_id': $scope.filteredTodos[i].sh_shift_id,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }


                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/common/ShiftMaster/ShiftMasterINSERT", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Shift Master Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });

                                }
                                else {
                                    swal({ title: "Alert", text: "Shift Master Not Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sh_shift_id + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
            }


            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sh_shift_id + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sh_shift_id + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $('.clockpicker').clockpicker({
                autoclose: true
            });


            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.shift_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.shift_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sh_shift_id + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sh_shift_id.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sh_shift_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            var dom;
            $scope.flag = true;
            $scope.icon = "fa fa-plus-circle";

            $scope.expand = function (info, $event) {
                console.log(info);

                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +

                         "<tr><td class='semi-bold'>" + "Shift1 In Grace Start" + "</td>" + "<td class='semi-bold'colsapn='2'>" + "Shift1 In Grace End" + "" + "<td class='semi-bold'colsapn='2'>" + "Shift1 Out Grace Start" + "<td class='semi-bold'>" + "Shift1 Out Grace End" + "</td></tr>" +
                          "<tr><td>" + (info.sh_shift1_in_greece_start) + "</td> " + "<td colsapn='2'>" + (info.sh_shift1_in_greece_end) + "" + "<td colsapn='2'>" + (info.sh_shift1_out_greece_start) + "<td>" + (info.sh_shift1_out_greece_end) + "</td></tr>" +
                           "<tr><td class='semi-bold'>" + "Shift2 In Grace Start" + "<td class='semi-bold'>" + "Shift2 In Grace End" + "</td> " + "<td class='semi-bold'colsapn='2'>" + "Shift2 Out Grace Start" + "</td> " + "<td class='semi-bold'colsapn='2'>" + "Shift2 Out Grace End" + "</td></tr>" +
                          "<tr><td>" + (info.sh_shift2_in_greece_start) + "<td>" + (info.sh_shift2_in_greece_end) + "</td>" + "<td colsapn='2'>" + (info.sh_shift2_out_greece_start) + "</td>" + "<td colsapn='2'>" + (info.sh_shift2_out_greece_end) + "</td></tr>" +
                        "</tbody>" +
                        " </table></td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    info.icon = "fa fa-plus-circle";
                    $scope.flag = true;
                }
            };

        }])
})();