﻿(function () {
    'use strict';
    var main = '';
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('UpdatepaysheetCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.updatepaysheet = true;
            var d = new Date();
            var n = d.getMonth() + 1;
            $scope.deletepaysheettable = false;
            $scope.update_payroll_show = true;
            $scope.pagesize = '5';
            var user = $rootScope.globals.currentUser.username;
            $http.get(ENV.apiUrl + "api/updatepaysheet/GetCurrentFinYearFromFinsParameter").then(function (GetCurrentFinYearFromFinsParameter) {
                $scope.GetCurrentFinYearFromFinsParameter = GetCurrentFinYearFromFinsParameter.data;
            });

            $http.get(ENV.apiUrl + "api/updatepaysheet/GetAllPayCodes").then(function (GetAllPayCodes) {
                $scope.GetAllPayCodes = GetAllPayCodes.data;
            });

            //$http.get(ENV.apiUrl + "api/updatepaysheet/GetMonth").then(function (GetMonth) {
            //    $scope.GetMonth = GetMonth.data;

            //    $scope.edt = { sd_year_month_code: n.toString() };
            //    console.log($scope.GetMonth);
            //    console.log(n);
            //});

            $http.post(ENV.apiUrl + "api/OvertimeHour/Year_month").then(function (Year_month) {
                $scope.Year_month = Year_month.data;
                debugger;
                var d = new Date().getFullYear();
                var n = new Date().getMonth() + 1;
                var ym = d + '' + n;

                $scope.edt = { year_month: ym };
            });

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2015&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;
            });

            var year_month = '';
            var newvalue = false;
            $scope.deletbtn = true;

            $scope.ShowUpdatepaysheet = function () {
                $scope.updatepaysheet = true;
                $scope.deletepaysheet = false;
                $scope.update_payroll_show = true;
                $scope.delete_payroll_show = false;
                debugger;
                var d = new Date().getFullYear();
                var n = new Date().getMonth() + 1;
                var ym = d + '' + n;

                $scope.edt = { year_month: ym };
            }

            $scope.ShowDeletepaysheet = function () {
                $scope.btn_ResetEmployee_Click();
                $scope.updatepaysheet = false;
                $scope.deletepaysheet = true;
                $scope.update_payroll_show = false;
                $scope.delete_payroll_show = true;
                debugger;
                var d = new Date().getFullYear();
                var n = new Date().getMonth() + 1;
                var ym = d + '' + n;

                $scope.edt1 = { year_month: ym };
            }

            /********************************************************Delete Paysheet Code***************************************************/
            $scope.GetEmployeeDatafordelete = function () {

                if ($scope.edt1 == '' || $scope.edt1 == undefined) {
                    swal('', 'Please select month.');
                }
                else {
                    $http.post(ENV.apiUrl + "api/updatepayrollstatus/CheckPayrollStatus?year_month=" + $scope.edt1.year_month).then(function (CheckPayrollStatus) {
                        $scope.CheckPayrollStatus = CheckPayrollStatus.data;
                        debugger;
                        if ($scope.CheckPayrollStatus != '') {
                            swal({ text: $scope.CheckPayrollStatus, showCloseButton: true, width: 380 });

                        }
                        else {
                            year_month = $scope.edt1.year_month
                            $scope.deletepaysheettable = true;
                            $http.get(ENV.apiUrl + "api/updatepaysheet/GetEmployeesFromSalaryDetails?year_month=" + year_month).then(function (GetEmployeesFromSalaryDetails) {
                                $scope.GetEmployeesFromSalaryDetails = GetEmployeesFromSalaryDetails.data;
                                if ($scope.GetEmployeesFromSalaryDetails.length > 0)
                                    $scope.deletbtn = false;
                            });
                        }
                    });

                }
            }

            $scope.Delete = function () {

                $scope.flag = false;
                var del = [];
                for (var i = 0; i < $scope.GetEmployeesFromSalaryDetails.length; i++) {
                    debugger;
                    var t = $scope.GetEmployeesFromSalaryDetails[i].code;
                    var v = document.getElementById(t);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var c = {
                            sd_year_month: $scope.edt1.year_month,
                            sd_number: $scope.GetEmployeesFromSalaryDetails[i].code
                        }
                        del.push(c);
                    }

                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/updatepaysheet/Payroll_Delete", del).then(function (msg1) {
                                $scope.msg = msg1.data;
                                $scope.deletbtn = true;
                                main.checked = false;
                                swal({ title: "", text: $scope.msg, showCloseButton: true, width: 380 }).then(function (isConfirm) {

                                    $scope.GetEmployeesFromSalaryDetails = [];
                                    main = document.getElementById('mainchk');
                                    main.checked = false;
                                });
                            });
                        }
                        else {
                            debugger;
                            main = document.getElementById('mainchk');
                            for (var i = 0; i < $scope.GetEmployeesFromSalaryDetails.length; i++) {
                                var t = $scope.GetEmployeesFromSalaryDetails[i].code;
                                var v = document.getElementById(t);
                                v.checked = false;
                                $scope.row1 = "";
                                main.checked = false;
                                year_month = $scope.edt1.year_month;
                                $scope.deletepaysheettable = true;
                                $http.get(ENV.apiUrl + "api/updatepaysheet/GetEmployeesFromSalaryDetails?year_month=" + year_month).then(function (GetEmployeesFromSalaryDetails) {
                                    $scope.GetEmployeesFromSalaryDetails = GetEmployeesFromSalaryDetails.data;


                                });
                            }
                        }
                        $http.post(ENV.apiUrl + "api/updatepayrollstatus/CheckPayrollStatus?year_month=" + $scope.edt1.year_month).then(function (CheckPayrollStatus) {
                            $scope.CheckPayrollStatus = CheckPayrollStatus.data;
                            debugger;
                            if ($scope.CheckPayrollStatus != '') {
                                swal({ text: $scope.CheckPayrollStatus, showCloseButton: true, width: 380 });
                            }
                            else {
                                year_month = $scope.edt1.year_month;
                                $scope.deletepaysheettable = true;
                                $http.get(ENV.apiUrl + "api/updatepaysheet/GetEmployeesFromSalaryDetails?year_month=" + year_month).then(function (GetEmployeesFromSalaryDetails) {
                                    $scope.GetEmployeesFromSalaryDetails = GetEmployeesFromSalaryDetails.data;
                                    if ($scope.GetEmployeesFromSalaryDetails.length > 0)
                                        $scope.deletbtn = false;
                                });
                            }
                        });

                    });
                }
                else {
                    swal({ title: "", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.edt = "";
            }

            $scope.btn_SearchEmployee_Click = function () {

                $http.post(ENV.apiUrl + "api/updatepayrollstatus/CheckPayrollStatus?year_month=" + $scope.edt.year_month).then(function (CheckPayrollStatus) {
                    $scope.CheckPayrollStatus = CheckPayrollStatus.data;

                    if ($scope.CheckPayrollStatus != '') {
                        swal({ text: $scope.CheckPayrollStatus, showCloseButton: true, width: 380 });
                        $scope.filteredTodos = [];
                        $scope.Updateemployee = false;
                    }
                    else {
                        $scope.To_Updateemployee = false;

                        $scope.edt.sd_year_month = $scope.edt.year_month;
                        debugger;
                        $http.post(ENV.apiUrl + "api/updatepaysheet/Employee_Genrated_salary_details", $scope.edt).then(function (Get_Genrated_salary_details) {
                            $scope.Get_Genrated_salary_details = Get_Genrated_salary_details.data;
                            $scope.totalItems = $scope.Get_Genrated_salary_details.length;
                            $scope.todos = $scope.Get_Genrated_salary_details;
                            $scope.makeTodos();

                            $scope.Updateemployee = true;
                        });
                    }
                });

            }


            $scope.btn_ResetEmployee_Click = function () {
                $scope.edt = '';
                $scope.To_Updateemployee = false;
                $scope.GetSalaryDetailsByEmployee = '';

                $scope.edt = { sd_year_month_code: n.toString() };
                $scope.filteredTodos = [];
                $scope.Updateemployee = false;
            }

            $scope.CheckMultiple = function () {
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.GetEmployeesFromSalaryDetails.length; i++) {
                    var t = $scope.GetEmployeesFromSalaryDetails[i].code;
                    var v = document.getElementById(t);
                    if (main.checked == true) {
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $scope.color = '#edefef';
                    }
                    else {
                        v.checked = false;
                        $scope.row1 = '';
                        $scope.color = '#edefef';
                        main.checked = false;
                    }
                }
            }

            $scope.CheckOneByOne = function (str) {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }

            }

            $scope.searched1 = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil1(i, toSearch);
                });
            };

            $scope.search1 = function () {
                $scope.todos1 = $scope.searched1($scope.GetEmployeesFromSalaryDetails, $scope.searchText1);
                $scope.totalItems1 = $scope.todos1.length;
                $scope.currentPage1 = '1';
                if ($scope.searchText == '') {
                    $scope.todos1 = $scope.GetEmployeesFromSalaryDetails;
                }
                $scope.makeTodos1();
            }

            function searchUtil1(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }


            /************************************************************Update code**********************************/



            $scope.getDetailsEmployeeToUpdate = function (info) {


                $scope.employeenameforcmb = [];
                var data = {
                    em_login_code: info.sd_number,
                    emp_name: info.emp_name
                }
                debugger;
                $scope.employeenameforcmb.push(data);
                $scope.temp = { em_login_code: info.sd_number };


                $scope.Updateemployee = false;
                //$scope.edt.sd_year_month = $scope.edt.sd_year_month;
                $http.get(ENV.apiUrl + "api/updatepaysheet/GetSalaryDetailsByEmployee?year_month=" + $scope.edt.year_month + '&sd_number=' + info.em_login_code).then(function (GetSalaryDetailsByEmployee) {
                    $scope.GetSalaryDetailsByEmployee = GetSalaryDetailsByEmployee.data;
                    debugger;
                    $scope.totalItems = $scope.GetSalaryDetailsByEmployee.length;
                    $scope.todos = $scope.GetSalaryDetailsByEmployee;
                    $scope.makeTodos();
                    $scope.edt.total = 0;
                    for (var j = 0; j < $scope.GetSalaryDetailsByEmployee.length; j++) {
                        $scope.edt.total = $scope.edt.total + $scope.GetSalaryDetailsByEmployee[j].sd_amount;
                    }
                    $scope.To_Updateemployee = true;
                });
            }

            $scope.btn_closeupdate_click = function () {
                $scope.temp = '';
                newvalue = false;
                $scope.btn_SearchEmployee_Click();
            }

            $scope.btn_Employeepayrollupdate_click = function () {
                var employeedata = '';
                var sd_company_code = '1';
                var name = '';
                debugger;

                if (newvalue != true) {
                    for (var i = 0; i < $scope.GetSalaryDetailsByEmployee.length; i++) {
                        if ($scope.GetSalaryDetailsByEmployee[i].ischange == true) {
                            year_month = $scope.GetSalaryDetailsByEmployee[i].sd_year_month;
                            sd_company_code = $scope.GetSalaryDetailsByEmployee[i].sd_company_code;
                            employeedata = employeedata + $scope.GetSalaryDetailsByEmployee[i].sd_pay_code + '!' + $scope.GetSalaryDetailsByEmployee[i].sd_number + '!' + $scope.GetSalaryDetailsByEmployee[i].sd_amount + '!' + $scope.GetSalaryDetailsByEmployee[i].sd_remark + '/';
                            //name=name+
                        }
                    }

                    var data = {
                        sd_number: user,
                        sd_company_code: sd_company_code,
                        sd_year_month: year_month,
                        sd_pay_code: employeedata,
                        name: ''

                    }

                }
                else {
                    if ($scope.temp.sd_remark == undefined)
                        $scope.temp.sd_remark = '';
                    var data = {
                        sd_number: user,
                        sd_company_code: $scope.GetSalaryDetailsByEmployee[0].sd_company_code,
                        sd_year_month: $scope.GetSalaryDetailsByEmployee[0].sd_year_month,
                        sd_pay_code: $scope.temp.sd_pay_code + '!' + $scope.temp.em_login_code + '!' + $scope.temp.sd_amount + '!' + $scope.temp.sd_remark + '/',
                        name: ''

                    }
                }

                $http.post(ENV.apiUrl + "api/updatepaysheet/Update_Employee_Salary_Details", data).then(function (msg1) {
                    $scope.msg = msg1.data;
                    if ($scope.msg == true) {
                        $scope.temp = '';
                        newvalue = false;
                        swal({ text: 'payroll Updated Successfully', width: 380 });
                        $scope.btn_SearchEmployee_Click();
                    }
                    else {
                        $scope.temp = '';
                        swal({ text: 'payroll Not Updated', width: 380 });
                        $scope.btn_SearchEmployee_Click();
                    }
                });
            }

            $scope.checkedforedit = function (info) {

                info.ischange = true;
                $scope.edt.total = 0;
                for (var j = 0; j < $scope.GetSalaryDetailsByEmployee.length; j++) {
                    $scope.edt.total = $scope.edt.total + $scope.GetSalaryDetailsByEmployee[j].sd_amount;
                }
            }

            $scope.Insertnew = function (info) {
                for (var i = 0; i < $scope.GetSalaryDetailsByEmployee.length; i++) {
                    if (info == $scope.GetSalaryDetailsByEmployee[i].sd_pay_code) {
                        $scope.temp = {
                            sd_pay_code: '',
                            em_login_code: $scope.GetSalaryDetailsByEmployee[i].sd_number,
                        };
                    } else {
                        newvalue = true;
                    }

                }


            }

            $scope.Insertnew1 = function (info) {
                newvalue = true;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                debugger;
                $scope.filteredTodos = $scope.todos.slice(begin, end);

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.Homeroom_MultipleDelete();
                }
            };

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.onlyNumbers = function (event) {
                debugger;
                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event

                    }

                }

                event.preventDefault();


            };


            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Get_Genrated_salary_details, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Get_Genrated_salary_details;
                }
                $scope.makeTodos();
            }

            $scope.isNumberKey = function (evt) {
                debugger;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode != 46 && charCode > 31
                  && (charCode < 48 || charCode > 57))
                    return false;

                return true;
            }


            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1 });
                $("#Table2").tableHeadFixer({ 'top': 1 });
                $("#Table3").tableHeadFixer({ 'top': 1 });
            }, 100);


            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.em_login_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.emp_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }
        }])
})();