﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('GoalTargetCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.chkMaxMin = function () {
                if ($scope.temp.sims_sip_goal_target_max_point <= $scope.temp.sims_sip_goal_target_min_point) {
                    swal({ title: "Alert", text: "Maximum Point should greater than Minimum Point", width: 300, height: 200 });
                }
            }

            $http.get(ENV.apiUrl + "api/GoalTarget/getGoalName").then(function (goalName) {
                $scope.goal_Name = goalName.data;
                console.log($scope.goal_Name);
            });

            $http.get(ENV.apiUrl + "api/GoalTarget/getAcademicYear").then(function (Acyear) {
                $scope.Acc_year = Acyear.data;
                console.log($scope.Acc_year);
            });

            $http.get(ENV.apiUrl + "api/GoalTarget/getAllGoalTarget").then(function (res1) {
                $scope.Goal_target = res1.data;
                $scope.totalItems = $scope.Goal_target.length;
                $scope.todos = $scope.Goal_target;
                $scope.makeTodos();
            });

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Goal_target, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Goal_target;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_sip_goal_target_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_sip_goal_target_max_point.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_sip_goal_target_min_point.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_sip_goal_desc == toSearch) ? true : false;
            }

            $scope.compareDate = function () {
                if ($scope.temp.sims_sip_goal_target_end_date < $scope.temp.sims_sip_goal_target_start_date) {
                    swal({ title: "Alert", text: "End Date Should greater than Start Date", width: 300, height: 200 });
                }
            }

            $scope.New = function () {
                $scope.temp = "";
                $scope.disabled = false;
                $scope.gtcreadonly = true;
                $scope.gtdreadonly = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            var datasend = [];
            $scope.savedata = function (Myform) {
                if (Myform) {
                    var data = {
                        sims_sip_academic_year: $scope.temp.sims_sip_academic_year
                   , sims_sip_goal_code: $scope.temp.sims_sip_goal_code
                   , sims_sip_goal_target_code: $scope.temp.sims_sip_goal_target_code
                   , sims_sip_goal_target_desc: $scope.temp.sims_sip_goal_target_desc
                   , sims_sip_goal_target_start_date: $scope.temp.sims_sip_goal_target_start_date
                   , sims_sip_goal_target_end_date: $scope.temp.sims_sip_goal_target_end_date
                   , sims_sip_goal_target_min_point: $scope.temp.sims_sip_goal_target_min_point
                   , sims_sip_goal_target_max_point: $scope.temp.sims_sip_goal_target_max_point
                   , sims_sip_goal_target_status: $scope.temp.sims_sip_goal_target_status
                   , opr: 'I'
                    };
                    datasend.push(data);

                    $http.post(ENV.apiUrl + "api/GoalTarget/CUDGoalTarget", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                        }

                        $http.get(ENV.apiUrl + "api/GoalTarget/getAllGoalTarget").then(function (res1) {
                            $scope.Goal_target = res1.data;
                            $scope.totalItems = $scope.Goal_target.length;
                            $scope.todos = $scope.Goal_target;
                            $scope.makeTodos();

                        });
                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.sims_sip_academic_year = "";
                $scope.temp.sims_sip_goal_code = "";
                $scope.temp.sims_sip_goal_target_desc = "";
                $scope.temp.sims_sip_goal_target_start_date = "";
                $scope.temp.sims_sip_goal_target_end_date = "";
                $scope.temp.sims_sip_goal_target_min_point = "";
                $scope.temp.sims_sip_goal_target_max_point = "";
                $scope.temp.sims_sip_goal_target_status = "";
            }

            $scope.edit = function (str) {

                $scope.gtcreadonly = true;
                $scope.gtdreadonly = true;
                $scope.gdisabled = true;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;

                $scope.temp = {
                    sims_sip_academic_year: str.sims_sip_academic_year
                   , sims_sip_goal_code: str.sims_sip_goal_code
                    , sims_sip_goal_target_code: str.sims_sip_goal_target_code
                   , sims_sip_goal_target_desc: str.sims_sip_goal_target_desc
                   , sims_sip_goal_target_start_date: str.sims_sip_goal_target_start_date
                   , sims_sip_goal_target_end_date: str.sims_sip_goal_target_end_date
                   , sims_sip_goal_target_min_point: str.sims_sip_goal_target_min_point
                   , sims_sip_goal_target_max_point: str.sims_sip_goal_target_max_point
                    , sims_sip_goal_target_status: str.sims_sip_goal_target_status
                };
            }

            var dataupdate = [];
            $scope.update = function (Myform) {

                if (Myform) {
                    var data = $scope.temp;
                    data.opr = 'U';
                    dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/GoalTarget/CUDGoalTarget", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }

                        $http.get(ENV.apiUrl + "api/GoalTarget/getAllGoalTarget").then(function (res1) {
                            $scope.Goal_target = res1.data;
                            $scope.totalItems = $scope.Goal_target.length;
                            $scope.todos = $scope.Goal_target;
                            $scope.makeTodos();
                        });
                    });
                    dataupdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                debugger;
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_sip_goal_target_code': $scope.filteredTodos[i].sims_sip_goal_target_code,
                            'sims_sip_goal_code': $scope.filteredTodos[i].sims_sip_goal_code,
                            'opr': "D"
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/GoalTarget/CUDGoalTarget", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/GoalTarget/getAllGoalTarget").then(function (res1) {
                                                $scope.Goal_target = res1.data;
                                                $scope.totalItems = $scope.Goal_target.length;
                                                $scope.todos = $scope.Goal_target;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted ", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/GoalTarget/getAllGoalTarget").then(function (res1) {
                                                $scope.Goal_target = res1.data;
                                                $scope.totalItems = $scope.Goal_target.length;
                                                $scope.todos = $scope.Goal_target;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;

                                        }
                                    });
                                }

                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }

                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });


        }])

})();
