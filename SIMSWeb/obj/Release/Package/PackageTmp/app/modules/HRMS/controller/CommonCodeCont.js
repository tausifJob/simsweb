﻿(function () {
    'use strict';
    var opr = '';
    var comncode = [];
    var main;
    var data1 = [];

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CommonCodeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';

            $scope.table1 = true;
            $http.get(ENV.apiUrl + "api/CommonCode/getCommonCode").then(function (getCommonCode_Data) {
                $scope.CommonCode_Data = getCommonCode_Data.data;
                $scope.totalItems = $scope.CommonCode_Data.length;
                $scope.todos = $scope.CommonCode_Data;
                $scope.makeTodos();
            });

            $scope.operation = false;
            $scope.editmode = false;
            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }
            $scope.edt = "";

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.editmode = false;
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.clCodeReadonly = false;

            }

            $scope.up = function (str) {

                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.clCodeReadonly = true;

                $scope.edt = {
                    cl_code: str.cl_code,
                    cl_desc: str.cl_desc,
                    cl_change_desc: str.cl_change_desc
                }
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.edt = {
                    cl_code: '',
                    cl_desc: '',
                    cl_change_desc: ''
                }
            }

            $scope.Save = function (myForm) {
                if (myForm) {
                    data1 = [];
                    var data =
                    {
                        cl_code: $scope.edt.cl_code,
                        cl_desc: $scope.edt.cl_desc,
                        cl_change_desc: $scope.edt.cl_change_desc,
                        opr: 'I'
                    }
                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/CommonCode/CommonCodeCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == 1) {
                            swal({ title: 'Alert', text: "Record Inserted Successfully", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == 2) {
                            swal({ title: 'Alert', text: "Common Code Already Exists", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: 'Alert', text: "Record not Inserted", width: 300, height: 200 });
                        }
                        $scope.operation = false;
                        $http.get(ENV.apiUrl + "api/CommonCode/getCommonCode").then(function (getCommonCode_Data) {
                            $scope.CommonCode_Data = getCommonCode_Data.data;
                            $scope.totalItems = $scope.CommonCode_Data.length;
                            $scope.todos = $scope.CommonCode_Data;
                            $scope.makeTodos();
                        });
                    });

                    $scope.table1 = true;
                    $scope.operation = false;
                }
            }

            $scope.Update = function () {
                data1 = [];
                var data =
                    {
                        cl_code: $scope.edt.cl_code,
                        cl_desc: $scope.edt.cl_desc,
                        cl_change_desc: $scope.edt.cl_change_desc,
                        opr: 'U'
                    }
                data1.push(data);
                $http.post(ENV.apiUrl + "api/CommonCode/CommonCodeCUD", data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == 1) {
                        swal({ title: 'Alert', text: "Record Updated Successfully", width: 300, height: 200 });
                    }
                    else {
                        swal({ title: 'Alert', text: "Record not Updated", width: 300, height: 200 });
                    }
                    $scope.operation = false;
                    $http.get(ENV.apiUrl + "api/CommonCode/getCommonCode").then(function (getCommonCode_Data) {
                        $scope.CommonCode_Data = getCommonCode_Data.data;
                        $scope.totalItems = $scope.CommonCode_Data.length;
                        $scope.todos = $scope.CommonCode_Data;
                        $scope.makeTodos();
                    });
                })
                $scope.operation = false;
                $scope.table1 = true;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].cl_code);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].cl_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.deleterecord = function () {
                comncode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].cl_code);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletecommoncode = ({
                            'cl_code': $scope.filteredTodos[i].cl_code,
                            opr: 'D'
                        });
                        comncode.push(deletecommoncode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/CommonCode/CommonCodeCUD", comncode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == 1) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/CommonCode/getCommonCode").then(function (getCommonCode_Data) {
                                                $scope.CommonCode_Data = getCommonCode_Data.data;
                                                $scope.totalItems = $scope.CommonCode_Data.length;
                                                $scope.todos = $scope.CommonCode_Data;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/CommonCode/getCommonCode").then(function (getCommonCode_Data) {
                                                $scope.CommonCode_Data = getCommonCode_Data.data;
                                                $scope.totalItems = $scope.CommonCode_Data.length;
                                                $scope.todos = $scope.CommonCode_Data;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].cl_code);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                main.checked = false;
                $scope.currentPage = true;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.CommonCode_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CommonCode_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                     item.cl_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.cl_change_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.cl_code == toSearch) ? true : false;
            }


        }])
})();
