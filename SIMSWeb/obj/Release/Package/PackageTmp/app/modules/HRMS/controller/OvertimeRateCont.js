﻿(function () {
    'use strict';
    var opr = '';
    var overtimeratecode = [];
    var main;
    var data1 = [];
    var data = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('OvertimeRateCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '5';
            $scope.table1 = true;
            $scope.operation = false;
            $scope.editmode = false;

            $http.get(ENV.apiUrl + "api/OverTimeRate/getOverTimeRateDetails").then(function (getOverTimeRateDetails_data) {
                $scope.OverTimeRateDetails = getOverTimeRateDetails_data.data;
                $scope.totalItems = $scope.OverTimeRateDetails.length;
                $scope.todos = $scope.OverTimeRateDetails;
                $scope.makeTodos();

            });


            $http.get(ENV.apiUrl + "api/common/getCompanyName").then(function (getCompanyName_Data) {
                $scope.CompanyName_Data = getCompanyName_Data.data;
            });

            $http.get(ENV.apiUrl + "api/OverTimeRate/getformuladesc").then(function (getformuladesc_data) {
                debugger
                $scope.formuladesc = getformuladesc_data.data;
            });

            $scope.getDepartments = function (comp) {

                $http.get(ENV.apiUrl + "api/common/getDepartmentNameByCompany?comp=" + $scope.edt.dr_company_code).then(function (getDepartmentName_Data) {
                    $scope.DepartmentName = getDepartmentName_Data.data;
                });

                $http.get(ENV.apiUrl + "api/common/getGradeNameByCompany?comp=" + $scope.edt.dr_company_code).then(function (getGradeNameByCompany_Data) {
                    $scope.GradeNames = getGradeNameByCompany_Data.data;
                });
            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.New = function () {

                $scope.editmode = false;
                $scope.newmode = true;
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

                $scope.edt = {
                    dr_company_code: "",
                    dr_company_name: "",
                    dr_dept_code: "",
                    dr_dept_name: "",
                    dr_grade_code: "",
                    dr_grade_name: "",
                    dr_ot_formula: "",
                    dr_ot_formula_name: "",
                    dr_ot_rate: "",
                    dr_holiday_ot_formula: "",
                    dr_holiday_ot_formula_name: "",
                    dr_holiday_ot_rate: "",
                    dr_special_ot_formula: "",
                    dr_special_ot_formula_name: "",
                    dr_special_ot_rate: "",
                    dr_special_holi_ot_formula: "",
                    dr_special_holi_ot_formula_name: "",
                    dr_special_holi_ot_rate: "",
                    dr_travel_rate: "",
                }


            }

            $scope.up = function (str) {

                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.readonly = true;

                $scope.edt = {
                    dr_company_code: str.dr_company_code,
                    dr_company_name: str.dr_company_name,
                    dr_dept_code: str.dr_dept_code,
                    dr_dept_name: str.dr_dept_name,
                    dr_grade_code: str.dr_grade_code,
                    dr_grade_name: str.dr_grade_name,
                    dr_ot_formula: str.dr_ot_formula,
                    dr_ot_formula_name: str.dr_ot_formula_name,
                    dr_ot_rate: str.dr_ot_rate,
                    dr_holiday_ot_formula: str.dr_holiday_ot_formula,
                    dr_holiday_ot_formula_name: str.dr_holiday_ot_formula_name,
                    dr_holiday_ot_rate: str.dr_holiday_ot_rate,
                    dr_special_ot_formula: str.dr_special_ot_formula,
                    dr_special_ot_formula_name: str.dr_special_ot_formula_name,
                    dr_special_ot_rate: str.dr_special_ot_rate,
                    dr_special_holi_ot_formula: str.dr_special_holi_ot_formula,
                    dr_special_holi_ot_formula_name: str.dr_special_holi_ot_formula_name,
                    dr_special_holi_ot_rate: str.dr_special_holi_ot_rate,
                    dr_travel_rate: str.dr_travel_rate
                }
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

                $scope.edt = {
                    dr_company_code: "",
                    dr_company_name: "",
                    dr_dept_code: "",
                    dr_dept_name: "",
                    dr_grade_code: "",
                    dr_grade_name: "",
                    dr_ot_formula: "",
                    dr_ot_formula_name: "",
                    dr_ot_rate: "",
                    dr_holiday_ot_formula: "",
                    dr_holiday_ot_formula_name: "",
                    dr_holiday_ot_rate: "",
                    dr_special_ot_formula: "",
                    dr_special_ot_formula_name: "",
                    dr_special_ot_rate: "",
                    dr_special_holi_ot_formula: "",
                    dr_special_holi_ot_formula_name: "",
                    dr_special_holi_ot_rate: "",
                    dr_travel_rate: "",
                }

            }

            $scope.Save = function (myForm) {
                if (myForm) {
                    data = [];
                    data1 = [];
                    data = $scope.edt;
                    data.opr = 'I';
                    $scope.exist = false;
                    for (var i = 0; i < $scope.OverTimeRateDetails.length; i++) {
                        if ($scope.OverTimeRateDetails[i].dr_company_code == data.dr_company_code &&
                            $scope.OverTimeRateDetails[i].dr_dept_code == data.dr_dept_code &&
                            $scope.OverTimeRateDetails[i].dr_grade_code == data.dr_grade_code
                           ) {
                            $scope.exist = true;
                        }

                    }
                    if ($scope.exist) {
                        swal({ title: 'Alert', text: "Record Already Exists", width: 300, height: 200 });
                    }
                    else {
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/OverTimeRate/OvertimeRateDetailsCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                            }
                            $http.get(ENV.apiUrl + "api/OverTimeRate/getOverTimeRateDetails").then(function (getOverTimeRateDetails_data) {
                                $scope.OverTimeRateDetails = getOverTimeRateDetails_data.data;
                                $scope.totalItems = $scope.OverTimeRateDetails.length;
                                $scope.todos = $scope.OverTimeRateDetails;
                                $scope.makeTodos();

                            });
                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                }
            }

            $scope.Update = function (myForm) {
                if (myForm) {
                    data = [];
                    data1 = [];
                    data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/OverTimeRate/OvertimeRateDetailsCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                        $http.get(ENV.apiUrl + "api/OverTimeRate/getOverTimeRateDetails").then(function (getOverTimeRateDetails_data) {
                            $scope.OverTimeRateDetails = getOverTimeRateDetails_data.data;
                            $scope.totalItems = $scope.OverTimeRateDetails.length;
                            $scope.todos = $scope.OverTimeRateDetails;
                            $scope.makeTodos();
                        });
                    });
                    $scope.operation = false;
                    $scope.table1 = true;
                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].dr_company_code + i);
                    if (main.checked == true) {
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }

                    else {
                        v.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }
            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.deleterecord = function () {
                overtimeratecode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].dr_company_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deleteratecode = ({
                            'dr_company_code': $scope.filteredTodos[i].dr_company_code,
                            'dr_dept_code': $scope.filteredTodos[i].dr_dept_code,
                            'dr_grade_code': $scope.filteredTodos[i].dr_grade_code,
                            opr: 'D'
                        });
                        overtimeratecode.push(deleteratecode);
                    }
                }
                debugger
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/OverTimeRate/OvertimeRateDetailsCUD", overtimeratecode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/OverTimeRate/getOverTimeRateDetails").then(function (getOverTimeRateDetails_data) {
                                                $scope.OverTimeRateDetails = getOverTimeRateDetails_data.data;
                                                $scope.totalItems = $scope.OverTimeRateDetails.length;
                                                $scope.todos = $scope.OverTimeRateDetails;
                                                $scope.makeTodos();

                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/OverTimeRate/getOverTimeRateDetails").then(function (getOverTimeRateDetails_data) {
                                                $scope.OverTimeRateDetails = getOverTimeRateDetails_data.data;
                                                $scope.totalItems = $scope.OverTimeRateDetails.length;
                                                $scope.todos = $scope.OverTimeRateDetails;
                                                $scope.makeTodos();

                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].dr_company_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }

                $scope.currentPage = true;

            }


            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.OverTimeRateDetails, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.OverTimeRateDetails;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */

                return (

                     item.dr_dept_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.dr_grade_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.dr_ot_formula_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.dr_ot_rate.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.dr_holiday_ot_formula_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.dr_holiday_ot_rate.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.dr_special_ot_formula_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.dr_special_ot_rate.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.dr_special_holi_ot_formula_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.dr_special_holi_ot_rate.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.dr_travel_rate.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 == toSearch) ? true : false;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


        }])
})();





