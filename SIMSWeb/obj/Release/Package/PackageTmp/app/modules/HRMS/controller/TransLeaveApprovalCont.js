﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.HRMS');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TransLeaveApprovalCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.EmpleaveType_data = [];
            var data1 = [];

            $scope.username = $rootScope.globals.currentUser.username;

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $http.get(ENV.apiUrl + "api/common/TransLeaveApproval/GetStatusforApproval").then(function (res) {
                $scope.status_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/TransLeaveApproval/GetTrasactionDetailsforApproval?emp_id=" + $scope.username).then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.EmpleaveType_data = res.data;
                $scope.totalItems = $scope.EmpleaveType_data.length;
                $scope.todos = $scope.EmpleaveType_data;
                $scope.makeTodos();
                $scope.grid = true;
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.getApproval = function (trans_id) {
                $('#LeaveModal').modal({ backdrop: 'static', keyboard: true });
                $http.get(ENV.apiUrl + "api/common/TransLeaveApproval/GetLeaveDetailsinWorkflow?transaction_id=" + trans_id).then(function (res) {
                    $scope.LeaveDetailsworkflow_data = res.data;
                });
            }

            $scope.save = function () {
                var data1 = [];
                $scope.insert = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].sims_workflow_no1 == true) {
                        var data = {
                            sims_workflow_no: $scope.filteredTodos[i].sims_workflow_no,
                            sims_workflow_description: $scope.filteredTodos[i].sims_workflow_description,
                            sims_workflow_transdate: $scope.filteredTodos[i].sims_workflow_transdate,
                            sims_workflow_status_code: $scope.filteredTodos[i].sims_workflow_status_code,
                            sims_workflow_modcode: $scope.filteredTodos[i].sims_workflow_modcode,
                            sims_workflow_applcode: $scope.filteredTodos[i].sims_workflow_applcode,
                            sims_workflow_remark: $scope.filteredTodos[i].sims_workflow_remark
                        };
                        $scope.insert = true;
                        data1.push(data);
                    }

                }

                if ($scope.insert) {
                    $http.post(ENV.apiUrl + "api/common/TransLeaveApproval/CUpdateTransactionDetails", data1).then(function (res) {
                        $scope.Transaction_data = res.data;

                        if ($scope.Transaction_data == true) {
                            swal({ title: "Alert", text: "Transaction Completed Successfully", showCloseButton: true, width: 380, });
                            $scope.getGrid();
                        }
                        else {
                            swal({ title: "Alert", text: "Transaction Not Completed", showCloseButton: true, width: 380, });
                            $scope.getGrid();
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
            }

            $scope.getGrid = function () {
                $http.get(ENV.apiUrl + "api/common/TransLeaveApproval/GetTrasactionDetailsforApproval?emp_id=" + $scope.username).then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.EmpleaveType_data = res.data;
                    $scope.totalItems = $scope.EmpleaveType_data.length;
                    $scope.todos = $scope.EmpleaveType_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                });
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.EmpleaveType_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.EmpleaveType_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_workflow_description.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

        }])
})();