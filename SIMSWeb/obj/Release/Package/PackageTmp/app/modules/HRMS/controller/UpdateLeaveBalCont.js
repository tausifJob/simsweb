﻿(function () {
    'use strict';
    var main = '';
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('UpdateLeaveBalCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.busyindicator = false;
            $scope.grid = true;
            $scope.color = '#edefef';
            $scope.rgvtbl = false;
            $scope.btnhide = true;
            $scope.edt = '';



            $http.get(ENV.apiUrl + "api/UpdateLeaveBal/GetDept").then(function (GetDept) {
                $scope.GetDept = GetDept.data;

            });

            $http.get(ENV.apiUrl + "api/UpdateLeaveBal/GetDesg").then(function (GetDesg) {
                $scope.GetDesg = GetDesg.data;

            });

            $http.get(ENV.apiUrl + "api/UpdateLeaveBal/Getleaves").then(function (Getleaves) {
                $scope.Getleaves = Getleaves.data;

            });
            $scope.changeno = function (obj) {
                
                console.log(obj)
                $scope.maindata = obj;

                if ($scope.maindata.leave_max >= 365 || $scope.maindata.leave_max <= 0) {
                    swal('', 'Please enter 0 to 365 days.');
                    $scope.maindata.leave_max = $scope.maindata.leave_max_old;
                }
                else {

                    if (parseFloat($scope.maindata.leave_taken) > parseFloat($scope.maindata.leave_max)) {
                        swal('', 'Maximum leave not less than taken leave.');
                        $scope.maindata.leave_max = $scope.maindata.leave_max_old;
                    }
                    else
                        $scope.maindata.leave_max_old = $scope.maindata.leave_max;
                }
            }

            $scope.onlyNumbers = function (event, leave_max) {
                
                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }

                }

                event.preventDefault();


            };

            $scope.Show_data = function () {

                $http.post(ENV.apiUrl + "api/UpdateLeaveBal/LeaveDetails?dept=" + $scope.edt.dep_code + "&desg=" + $scope.edt.dg_code + "&l_type=" + $scope.edt.leave_code + "&empcode=" + $scope.edt.emp_code).then(function (GetLeaveDetails) {
                    $scope.GetLeaveDetails = GetLeaveDetails.data;

                    if ($scope.GetLeaveDetails.length > 0) {
                        $scope.btnhide = false;
                        $scope.rgvtbl = true;
                    }
                    else {
                        $scope.rgvtbl = false;
                        $scope.btnhide = true;
                        swal('','Record not found.');
                    }
                });
               

            }




            $scope.genrate = function () {
                
                var datacoll = [];
                for (var i = 0; i < $scope.GetLeaveDetails.length; i++) {
                    var data = {
                        emp_code: $scope.GetLeaveDetails[i].emp_code,
                        leave_code: $scope.GetLeaveDetails[i].leave_code,
                        leave_max: $scope.GetLeaveDetails[i].leave_max
                    }
                    datacoll.push(data);
                }

                $http.post(ENV.apiUrl + "api/UpdateLeaveBal/EmpUpdateLeaveBal", datacoll).then(function (msg1) {
                    $scope.msg = msg1.data;
                    if ($scope.msg)
                        swal('', 'Record updated successfully.');
                    
                    $scope.reset_form();

                });
            }


            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);


            $scope.reset_form = function () {
                $scope.edt = '';
                $scope.GetLeaveDetails = [];
                $scope.btnhide = true;
                $scope.rgvtbl = false;

            }

          




         


        }])
})();