﻿(function () {
    'use strict';
    var main, temp1, opr, edt;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeLongFormAdiswCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.imageUrl = ENV.apiUrl + '/Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/';

            $http.get(ENV.apiUrl + "api/HRMS/Adisw/getEmpMasterData").then(function (res) {

                $scope.empstat = true;
                $scope.obj = res.data;
                console.log($scope.obj);
                $scope.operation = true;
                $scope.Save_btn = true;
                $scope.Update_btn = false;
                $scope.display = true;
                $scope.roleList = true;

            });
            $scope.edt = {};
            $scope.temp = {};
            $scope.globalSrch = function () {

                $rootScope.visible_stud = false;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = false;
                $rootScope.visible_Employee = true;
                $rootScope.chkMulti = false;

                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                //$scope.temp.em_login_code = '';
                //$scope.temp.EmpName = '';
                $scope.showBtn = false;
            }

            $scope.AutoFill = function () {
                debugger;
                var sum_addr = $scope.temp.em_apartment_number + ',' + $scope.temp.em_building_number + ',' + $scope.temp.em_street_number + ',' + $scope.temp.em_area_number;
                $scope.temp['em_summary_address'] = sum_addr;

            }

            $http.get(ENV.apiUrl + "api/HRMS/Adisw/getCompany").then(function (resc) {
                $scope.company_data = resc.data;
                $scope.temp = {
                    'em_Company_Code': $scope.company_data[0].em_Company_Code,
                }
            });


            $http.get(ENV.apiUrl + "api/HRMS/Adisw/getCurLevel").then(function (curlevel) {
                $scope.curlevel_data = curlevel.data;
            });
            $http.get(ENV.apiUrl + "api/HRMS/Adisw/getQualification").then(function (qualification) {
                $scope.qualification_data = qualification.data;
            });
            $http.get(ENV.apiUrl + "api/HRMS/Adisw/getDesignation").then(function (designation) {
                $scope.designation_data = designation.data;
            });
            $http.get(ENV.apiUrl + "api/HRMS/Adisw/getSubjects").then(function (subjects) {
                $scope.subjects_data = subjects.data;
            });


            $scope.$on('global_cancel', function (str) {

                console.log($scope.SelectedUserLst);
                if ($scope.SelectedUserLst.length > 0) {

                    $scope.temp = {};
                    $scope.temp = {
                        'enroll_number1': $scope.SelectedUserLst[0].em_login_code
                    }
                    $scope.temp['name'] = $scope.SelectedUserLst[0].empName;
                    $scope.EmpSearch1($scope.SelectedUserLst[0].em_login_code)
                }
            });

            $scope.searchGlobalClick = function () {
                $scope.Global_Search_by_employee();
            }

            $http.get(ENV.apiUrl + "api/HRMS/Adisw/getRole").then(function (rolesdata) {
                $scope.roles_data = rolesdata.data;
                console.log($scope.roles_data);
            });

            $scope.setGrade_Dept = function (str) {

                $scope.temp.em_dept_effect_from = str;
                $scope.temp.em_grade_effect_from = str;
            }
            $http.get(ENV.apiUrl + "api/HRMS/Adisw/getLicSchool").then(function (licschooldata) {
                $scope.lic_school_data = licschooldata.data;
                if ($scope.lic_school_data[0].lic_school_country == 'Qatar') {
                    $scope.uae = false;
                    $scope.qatar = true;
                }
                else {
                    $scope.uae = true;
                    $scope.qatar = false;
                }
                console.log($scope.lic_school_data);
            });


            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.Save_btn = true;
                $scope.Update_btn = false;
            }


            $scope.getstate = function (country) {
                $http.get(ENV.apiUrl + "api/HRMS/Adisw/getState?em_Country_Code=" + country).then(function (res) {
                    $scope.stt = res.data;
                });
            }

            $scope.getcity = function (state) {
                $http.get(ENV.apiUrl + "api/HRMS/Adisw/getCity?em_State_Code=" + state).then(function (res) {
                    $scope.ctt = res.data;
                });
            }


            $scope.EmpSearch1 = function (emplogcode) {

                $scope.Save_btn = false;
                $scope.Update_btn = true;
                $http.get(ENV.apiUrl + "api/HRMS/Adisw/getSearchEmpMaster?em_login_code=" + $scope.temp.enroll_number1).then(function (res) {
                    $scope.temp1 = res.data;
                    $scope.temp = {
                        enroll_number1: emplogcode,
                        em_login_code: $scope.temp1[0].em_login_code,
                        em_number: $scope.temp1[0].em_number,
                        em_service_status_code: $scope.temp1[0].em_service_status_code,
                        em_status_code: $scope.temp1[0].em_status_code,
                        em_apartment_number: $scope.temp1[0].em_apartment_number,
                        em_family_name: $scope.temp1[0].em_family_name,
                        em_street_number: $scope.temp1[0].em_street_number,
                        em_area_number: $scope.temp1[0].em_area_number,
                        em_phone: $scope.temp1[0].em_phone,
                        em_fax: $scope.temp1[0].em_fax,
                        em_post: $scope.temp1[0].em_post,
                        em_emergency_contact_name2: $scope.temp1[0].em_emergency_contact_name2,
                        em_emergency_contact_number2: $scope.temp1[0].em_emergency_contact_number2,
                        em_joining_ref: $scope.temp1[0].em_joining_ref,
                        em_ethnicity_code: $scope.temp1[0].em_ethnicity_code,
                        em_handicap_status: $scope.temp1[0].em_handicap_status,
                        em_stop_salary_indicator: $scope.temp1[0].em_stop_salary_indicator,
                        em_agreement: $scope.temp1[0].em_agreement,
                        em_punching_status: $scope.temp1[0].em_punching_status,
                        em_bank_cash_tag: $scope.temp1[0].em_bank_cash_tag,
                        em_citi_exp_tag: $scope.temp1[0].em_citi_exp_tag,
                        em_leave_tag: $scope.temp1[0].em_leave_tag,
                        em_passport_issue_place: $scope.temp1[0].em_passport_issue_place,
                        em_passport_issuing_authority: $scope.temp1[0].em_passport_issuing_authority,
                        em_pssport_exp_rem_date: $scope.temp1[0].em_pssport_exp_rem_date,
                        em_visa_issuing_place: $scope.temp1[0].em_visa_issuing_place,
                        em_visa_issuing_authority: $scope.temp1[0].em_visa_issuing_authority,
                        em_visa_exp_rem_date: $scope.temp1[0].em_visa_exp_rem_date,
                        em_visa_type_code: $scope.temp1[0].em_visa_type_code,
                        em_agreement_start_date: $scope.temp1[0].em_agreement_start_date,
                        em_agreement_exp_date: $scope.temp1[0].em_agreement_exp_date,
                        em_agreemet_exp_rem_date: $scope.temp1[0].em_agreemet_exp_rem_date,
                        em_punching_id: $scope.temp1[0].em_punching_id,
                        em_dependant_full: $scope.temp1[0].em_dependant_full,
                        em_dependant_half: $scope.temp1[0].em_dependant_half,
                        em_dependant_infant: $scope.temp1[0].em_dependant_infant,
                        em_Bank_Code: $scope.temp1[0].em_Bank_Code,
                        em_bank_ac_no: $scope.temp1[0].em_bank_ac_no,
                        em_iban_no: $scope.temp1[0].em_iban_no,
                        em_route_code: $scope.temp1[0].em_route_code,
                        em_bank_swift_code: $scope.temp1[0].em_bank_swift_code,
                        em_gpf_ac_no: $scope.temp1[0].em_gpf_ac_no,
                        em_pan_no: $scope.temp1[0].em_pan_no,
                        em_labour_card_no: $scope.temp1[0].em_labour_card_no,
                        em_gosi_ac_no: $scope.temp1[0].em_gosi_ac_no,
                        em_gosi_start_date: $scope.temp1[0].em_gosi_start_date,
                        em_national_id: $scope.temp1[0].em_national_id,
                        em_secret_question_code: $scope.temp1[0].em_secret_question_code,
                        em_secret_answer: $scope.temp1[0].em_secret_answer,
                        em_building_number: $scope.temp1[0].em_building_number,
                        em_Company_Code: $scope.temp1[0].em_Company_Code,
                        em_Dept_Code: $scope.temp1[0].em_Dept_Code,
                        em_Designation_Code: $scope.temp1[0].em_Designation_Code,
                        em_Dest_Code: $scope.temp1[0].em_Dest_Code,
                        em_Grade_Code: $scope.temp1[0].em_Grade_Code,
                        em_Marital_Status_Code: $scope.temp1[0].em_Marital_Status_Code,
                        em_Nation_Code: $scope.temp1[0].em_Nation_Code,
                        em_Country_Code: $scope.temp1[0].em_Country_Code,
                        em_State_Code: $scope.temp1[0].em_State_Code,
                        em_City_Code: $scope.temp1[0].em_City_Code,
                        em_Religion_Code: $scope.temp1[0].em_Religion_Code,
                        em_Salutation_Code: $scope.temp1[0].em_Salutation_Code,
                        em_Sex_Code: $scope.temp1[0].em_Sex_Code,
                        em_Staff_Type_Code: $scope.temp1[0].em_Staff_Type_Code,
                        em_blood_group_code: $scope.temp1[0].em_blood_group_code,
                        em_date_of_birth: $scope.temp1[0].em_date_of_birth,
                        em_date_of_join: $scope.temp1[0].em_date_of_join,
                        em_dept_effect_from: $scope.temp1[0].em_dept_effect_from,
                        em_email: $scope.temp1[0].em_email,
                        em_emergency_contact_name1: $scope.temp1[0].em_emergency_contact_name1,
                        em_emergency_relation1: $scope.temp1[0].em_emergency_relation1,
                        em_emergency_relation2: $scope.temp1[0].em_emergency_relation2,
                        em_emergency_contact_number1: $scope.temp1[0].em_emergency_contact_number1,
                        em_first_name: $scope.temp1[0].em_first_name,
                        em_grade_effect_from: $scope.temp1[0].em_grade_effect_from,
                        em_last_name: $scope.temp1[0].em_last_name,
                        em_middle_name: $scope.temp1[0].em_middle_name,
                        em_mobile: $scope.temp1[0].em_mobile,
                        em_name_ot: $scope.temp1[0].em_name_ot,
                        em_national_id_expiry_date: $scope.temp1[0].em_national_id_expiry_date,
                        em_national_id_issue_date: $scope.temp1[0].em_national_id_issue_date,
                        em_passport_exp_date: $scope.temp1[0].em_passport_exp_date,
                        em_passport_issue_date: $scope.temp1[0].em_passport_issue_date,
                        em_passport_no: $scope.temp1[0].em_passport_no,
                        em_summary_address: $scope.temp1[0].em_summary_address,
                        em_summary_address_local_language: $scope.temp1[0].em_summary_address_local_language,
                        em_visa_exp_date: $scope.temp1[0].em_visa_exp_date,
                        em_stop_salary_from: $scope.temp1[0].em_stop_salary_from,
                        em_visa_issue_date: $scope.temp1[0].em_visa_issue_date,
                        em_visa_no: $scope.temp1[0].em_visa_no,
                        comn_role_code: $scope.temp1[0].comn_role_code,
                        comn_role_name: $scope.temp1[0].comn_role_name,
                        em_img: $scope.temp1[0].em_img,
                        em_status: $scope.temp1[0].em_status,
                        //adec
                        em_adec_approval_number: $scope.temp1[0].em_adec_approval_number,
                        em_adec_approval_Date: $scope.temp1[0].em_adec_approval_Date,
                        pays_qualification_code: $scope.temp1[0].pays_qualification_code,
                        dg_code: $scope.temp1[0].dg_code,
                        sims_subject_code: $scope.temp1[0].sims_subject_code,
                        sims_cur_level_code: $scope.temp1[0].sims_cur_level_code,
                        em_health_card_number: $scope.temp1[0].em_health_card_number,
                        em_health_card_issue_date: $scope.temp1[0].em_health_card_issue_date,
                        em_health_card_expiry_date: $scope.temp1[0].em_health_card_expiry_date,
                        em_rta_number: $scope.temp1[0].em_rta_number,
                        em_rta_issue_date: $scope.temp1[0].em_rta_issue_date,
                        em_rta_expiry_date: $scope.temp1[0].em_rta_expiry_date,
                        em_left_date: $scope.temp1[0].em_left_date,
                        em_left_reason: $scope.temp1[0].em_left_reason
                    }


                    $scope.getstate($scope.temp.em_Country_Code);
                    $scope.getcity($scope.temp.em_State_Code);
                });
            }


            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };

            $scope.file_changed = function (element) {
                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;
                    });
                };
                reader.readAsDataURL(photofile);
            };


            $scope.uploadImgClickF = function () {
                $scope.uploadImgClickFShow = true;
            }

            $scope.uploadImgCancel = function () {
                $scope.uploadImgClickFShow = false;
            }

            $scope.image_click = function () {
                $('#upload_img').modal('show');
                $scope.ImgLoaded = true;
            }

            $scope.globalSearch = function () { $('#MyModal').modal('show'); $scope.showBtn = false; }

            $scope.EmployeeDetails = function () {

                $scope.NodataEmployee = false;
                $http.post(ENV.apiUrl + "api/common/SearchEmployee?data=" + JSON.stringify($scope.temp)).then(function (SearchEmployee_Data) {
                    $scope.Employee_Details = SearchEmployee_Data.data;
                    console.log($scope.EmployeeDetails);
                    $scope.EmployeeTable = true;

                    if (SearchEmployee_Data.data.length > 0) {
                    }
                    else {
                        $scope.NodataEmployee = true;
                    }
                });

            }

            $scope.EmployeeAdd = function (info) {
                $scope.temp = {
                    enroll_number: info.em_login_code,
                    emp_name: info.empName,
                    emp_desg: info.desg_name,
                    emp_dept: info.dept_name
                };
            }

            $scope.SaveData = function (isvalid) {
                debugger;
                if (isvalid) {
                    var data = $scope.temp;
                    if ($scope.photo_filename === undefined) {
                        data.em_img = null;
                    }
                    else {
                        data.em_img = '.' + $scope.photo_filename.split("/")[1];
                    }
                    data.opr = "I";
                    $http.post(ENV.apiUrl + "api/HRMS/Adisw/CUMasterEmployee", data).then(function (res) {
                        $scope.CUDobj = res.data;
                        swal({ text: $scope.CUDobj.strMessage, timer: 5000 });

                        var empid = $scope.CUDobj.strMessage.split(" ")[2];
                        var request = {
                            method: 'POST',
                            url: ENV.apiUrl + '/api/file/upload?filename=' + empid + "&location=" + "/EmployeeImages",
                            data: formdata,
                            headers: {
                                'Content-Type': undefined
                            }
                        };

                        $http(request).success(function (d) {
                            //alert(d);
                        });
                    });
                    $scope.temp = "";
                }
            }

            $scope.UpdateData = function () {
                debugger;
                var data = $scope.temp;
                if ($scope.photo_filename === undefined) {
                }
                else {
                    data.em_img = $scope.temp.em_login_code + '.' + $scope.photo_filename.split("/")[1];
                }

                data.opr = 'U';
                data.subopr = 'Up';

                $http.post(ENV.apiUrl + "api/HRMS/Adisw/CUMasterEmployee", data).then(function (res) {
                    debugger;
                    $scope.CUDobj = res.data;
                    swal({ text: $scope.CUDobj.strMessage, timer: 5000 });
                });

                if ($scope.ImgLoaded == true) {
                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + '/api/file/upload?filename=' + $scope.temp.em_login_code + "&location=" + "/EmployeeImages",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    $http(request).success(function (d) {
                        //alert(d);
                    });
                }
                $scope.temp = "";
                $scope.em_img = "";
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

        }]
        )


    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

})();