﻿
(function () {
    'use strict';

    var main;
    var subject_code = [], subject_code1 = [], sib_enroll_code = [];

    var cur_code;
    var section_code, sectioncodemodal1;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('LeaveDetailsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.parent = false;
            $scope.student = false;
            $scope.buttons = false;
            $scope.currentab = '';
            var arr_check = [];
            var username1 = $rootScope.globals.currentUser.username;
            $scope.users = false;

            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;
            $scope.cdetails_data = [];

            $scope.pagesize_cancel = "5";
            $scope.pageindex_cancel = "1";
            $scope.filteredTodos_cancel = [], $scope.currentPage_cancel = 1, $scope.numPerPage_cancel = 5, $scope.maxSize_cancel = 5;
            $scope.cdetails_data_cancel = [];
            $scope.cancel_approvedlist = [];

            $scope.pagesize_update = "5";
            $scope.pageindex_update = "1";
            $scope.filteredTodos_update = [], $scope.currentPage_update = 1, $scope.numPerPage_update = 5, $scope.maxSize_update = 5;
            $scope.cdetails_data_update = [];
            $scope.update_approvedlist = [];

            $scope.pagesize_lwp = "5";
            $scope.pageindex_lwp = "1";
            $scope.filteredTodos_lwp = [], $scope.currentPage_lwp = 1, $scope.numPerPage_lwp = 5, $scope.maxSize_lwp = 5;
            $scope.cdetails_data_lwp = [];
            $scope.lwp_approvedlist = [];

            $scope.pagesize_holiday = "5";
            $scope.pageindex_holiday = "1";
            $scope.filteredTodos_holiday = [], $scope.currentPage_holiday = 1, $scope.numPerPage_holiday = 5, $scope.maxSize_holiday = 5;
            $scope.cdetails_data_holiday = [];
            $scope.lwp_approvedlist = [];

            $rootScope.visible_stud = false;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = true;

            $rootScope.chkMulti = true;
            $scope.edt =
                {
                    leave_type2: '',
                    leave_type1: ''
                }

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixedtable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixedtable2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixedtable3").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixedtable4").tableHeadFixer({ 'top': 1 });
            }, 100);


            $http.get(ENV.apiUrl + "api/leavedetails/GET_ManagerConfirmTag").then(function (ManagerConfirmTag) {
                $scope.Manager_ConfirmTag = ManagerConfirmTag.data;
                console.log($scope.Manager_ConfirmTag);
            });

            $http.get(ENV.apiUrl + "api/leavedetails/GetAllEMPLeaveDetails?comp_code=1").then(function (res) {

                $scope.cdetails_data = res.data;
                $scope.totalItems = $scope.cdetails_data.length;
                $scope.todos = $scope.cdetails_data;
                $scope.makeTodos();
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.cdetails_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.cdetails_data;
                }
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            function searchUtil(item, toSearch) {
                /* Search Text in all 2 fields */
                return (item.empName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.em_login_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $http.get(ENV.apiUrl + "api/leavedetails/Get_EMPLeaveDetails").then(function (Get_EMPLeaveDetails) {
                $scope.EMPLeaveDetails = Get_EMPLeaveDetails.data;
            });

            $http.get(ENV.apiUrl + "api/leavedetails/GET_ManagerConfirmTag").then(function (ManagerConfirmTag) {
                $scope.Manager_ConfirmTag = ManagerConfirmTag.data;
                console.log($scope.Manager_ConfirmTag);
            });

            $scope.GetLeaveDetails = function () {

                $http.get(ENV.apiUrl + "api/leavedetails/GetEMPLeaveDetails?leave_code=" + $scope.edt.leave_type).then(function (GetEMPLeaveDetails) {

                    $scope.GetEmployeeLeaveDetails = GetEMPLeaveDetails.data;

                    console.log($scope.GetEmployeeLeaveDetails);
                });
            }

            $scope.UserSearch_Cancel = function (str) {
                $scope.filteredTodos_cancel = [];
                debugger;
                //var leave_t = $scope.edt.leave_type2;

                if ($scope.edt.leave_type1 == 'undefined' || $scope.edt.leave_type1 == '') {
                    swal({ title: "Alert", text: "Please Select Leave Type.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }
                else {
                    $scope.Get_cancel_employee_list = [];
                    $('#CancelApprovedLeavesModel').modal('show')
                }
            }

            var sendata = [];

            $scope.ApprovedLeaveCancel = function () {

                for (var i = 0; i < $scope.filteredTodos_cancel.length; i++) {

                    if ($scope.filteredTodos_cancel[i].lt_no_cancel == true) {
                        sendata.push($scope.filteredTodos_cancel[i]);
                    }
                }

                $http.post(ENV.apiUrl + "api/leavedetails/CALeaves", sendata).then(function (LeaveDetails) {
                    $scope.Leave_Details = LeaveDetails.data;
                    if ($scope.Leave_Details == true) {
                        swal('', 'Approved Leave Cancelled');
                        $scope.EMPLeave_Details = [];
                        $scope.filteredTodos_cancel = [];
                        $scope.edt.leave_type1 = null;
                        $scope.searchText_cancel = null;
                        //$scope.SelectedUserLst = null;
                    }
                    else {
                        swal('', 'Approved Leave Not Cancelled');
                    }
                });
                sendata = [];
            }

            $scope.CheckEmployeeLeave = function () {

                $('#MyModal').modal('show')

                $http.get(ENV.apiUrl + "api/leavedetails/Get_dept_Id").then(function (res) {
                    $scope.Getdepartment = res.data;
                    console.log($scope.Getdepartment);
                });
            }

            var datasend = [];

            $scope.SubmitApprovedLeave = function () {
                debugger;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].lt_mgr_confirm_tag == 'C') {
                        datasend.push($scope.filteredTodos[i]);
                    }
                }
                $http.post(ENV.apiUrl + "api/leavedetails/Update_pay_leavedoc", datasend).then(function (res) {
                    debugger;
                    $scope.o1 = res.data;
                    console.log($scope.o1);

                    if ($scope.o1 == true) {
                        swal('', 'Selected Leaves Approved Successfully.');
                    }

                    $http.get(ENV.apiUrl + "api/leavedetails/GET_ManagerConfirmTag").then(function (ManagerConfirmTag) {
                        $scope.Manager_ConfirmTag = ManagerConfirmTag.data;
                        console.log($scope.Manager_ConfirmTag);
                    });

                    $http.get(ENV.apiUrl + "api/leavedetails/GetAllEMPLeaveDetails?comp_code=1").then(function (res) {

                        $scope.cdetails_data = res.data;
                        $scope.totalItems = $scope.cdetails_data.length;
                        $scope.todos = $scope.cdetails_data;
                        $scope.makeTodos();
                    });
                });
            }

            $scope.Useredit = function (info) {

                debugger;
                if ($scope.approvedleave == true) {
                    $scope.EMPLeave_Details1 = [];
                    $scope.EMPLeave_Details = [];

                    $http.post(ENV.apiUrl + "api/leavedetails/CAL_geteavesforEmp?empid=" + info.em_login_code + "&leave_code=" + $scope.edt.leave_type1).then(function (Get_EMPLeave_Details) {
                        $scope.EMPLeave_Details1 = Get_EMPLeave_Details.data;
                        $('#Employee').modal('hide');
                        if ($scope.EMPLeave_Details1.length > 0) {
                            $scope.EMPLeave_Details = Get_EMPLeave_Details.data;
                        }
                        else {
                            swal('', 'Record Not Found');
                        }
                    });
                }
                else if ($scope.lwp == true) {

                    $scope.GET_LWPforEmployees1 = [];
                    $scope.GET_LWPforEmployees = [];
                    $http.get(ENV.apiUrl + "api/leavedetails/GET_LWPforEmployees?empcode=" + info.em_login_code + "&date=" + $scope.edt.date).then(function (GET_LWPforEmployees) {
                        $scope.GET_LWPforEmployees1 = GET_LWPforEmployees.data;

                        if ($scope.GET_LWPforEmployees1.length > 0) {
                            $scope.GET_LWPforEmployees = GET_LWPforEmployees.data;

                            console.log($scope.GET_LWPforEmployees1);
                        }
                        else {
                            swal('', 'Record Not Found');
                        }
                    });
                }
            }

            var lwpsenddata = [];

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $http.get(ENV.apiUrl + "api/leavedetails/Getdepartment").then(function (Getdepartment) {
                $scope.Getdepartment = Getdepartment.data;

            });

            $scope.SearchAllTeacher = function () {

                $scope.searchtable_cancelapprove = false;
                $scope.searchtable_updateapprove = false;

                debugger;
                console.log('In searchallteacher');
                console.log($scope.edt.comp_code);
                console.log($scope.edt.login_code);
                console.log($scope.edt.leave_type);
                $scope.busy = true;

                $http.get(ENV.apiUrl + "api/leavedetails/Get_Employee_leave_balance?leave_code=" + $scope.edt.leave_type2 + "&dept_code=" + $scope.edt.comp_code + "&login_code=" + $scope.edt.login_code).then(function (Get_approed_employee_list) {
                    $scope.Get_approed_employee_list = Get_approed_employee_list.data;
                    $scope.busy = false;
                    $scope.searchtable = true;
                });
            }


            $scope.Reset = function () {

                $scope.edt.leave_type2 = ''
                $scope.edt.comp_code = ''
                $scope.edt.login_code = ''
            }

            //////////////////////////////////////////////
            //Cancel Approved Leaves

            $scope.Search_forCancelApprovedLeaves = function () {
                debugger;
                var leave_t = $scope.edt.leave_type1;

                if (leave_t == '' || leave_t == 'undefined') {
                    swal({ title: "Alert", text: "Please Select Leave Type.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }
                else {
                    //main = document.getElementById('mainchk1');
                    //if (main.checked == true) {
                    //    main.checked = false;
                    //}

                    $scope.searchtable = false;
                    $scope.searchtable_updateapprove = false;
                    debugger;
                    console.log('In searchallteacher');
                    console.log($scope.edt.comp_code);
                    console.log($scope.edt.login_code);
                    console.log($scope.edt.leave_type);
                    $scope.busy = true;

                    $http.get(ENV.apiUrl + "api/leavedetails/Get_approed_employee_list?leave_code=" + $scope.edt.leave_type1 + "&dept_code=" + $scope.edt.comp_code + "&login_code=" + $scope.edt.login_code).then(function (Get_cancel_employee_list) {
                        $scope.Get_cancel_employee_list = Get_cancel_employee_list.data;
                        $scope.busy = false;
                        $scope.searchtable_cancelapprove = true;
                    });
                }
            }

            $scope.GetEmployeeleaveDetails = function (info) {
                $http.get(ENV.apiUrl + "api/leavedetails/GetEmployeePendingLeaveDetails?emp_code=" + info.em_number).then(function (Get_EmployeePendingLeaveDetails) {
                    $scope.EmployeePendingLeaveDetails = Get_EmployeePendingLeaveDetails.data;
                    $('#MyModal').modal('hide')
                });
            }

            $scope.MultipleSelect = function () {

                $scope.enroll_number = [];
                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.student.length; i++) {
                        var t = $scope.student[i].s_enroll_no;
                        var v = document.getElementById(t + i);
                        v.checked = true;

                        $scope.enroll_number.push($scope.student[i]);
                    }
                }
                else {
                    for (var i = 0; i < $scope.student.length; i++) {
                        var t = $scope.student[i].s_enroll_no;
                        var v = document.getElementById(t + i);
                        v.checked = false;
                        $scope.enroll_number = [];
                    }
                }


            }

            $scope.DataEnroll = function () {
                debugger;
                for (var i = 0; i < $scope.Get_cancel_employee_list.length; i++) {
                    var t = $scope.Get_cancel_employee_list[i].lt_no_cancel;
                    var v = document.getElementById(t + i);
                    if (v.checked == true)
                        $scope.cancel_approvedlist.push($scope.Get_cancel_employee_list[i]);
                }

                $scope.Get_cancel_employee_list = [];

                $scope.cdetails_data_cancel = $scope.cancel_approvedlist;
                $scope.totalItems_cancel = $scope.cdetails_data_cancel.length;
                $scope.todos_cancel = $scope.cdetails_data_cancel;
                $scope.makeTodos_cancel();
            }

            $scope.makeTodos_cancel = function () {
                var rem_cancel = parseInt($scope.totalItems_cancel % $scope.numPerPage_cancel);
                if (rem_cancel == '0') {
                    $scope.pagersize_cancel = parseInt($scope.totalItems_cancel / $scope.numPerPage_cancel);
                }
                else {
                    $scope.pagersize_cancel = parseInt($scope.totalItems_cancel / $scope.numPerPage_cancel) + 1;
                }
                var begin_cancel = (($scope.currentPage_cancel - 1) * $scope.numPerPage_cancel);
                var end_cancel = parseInt(begin_cancel) + parseInt($scope.numPerPage_cancel);

                $scope.filteredTodos_cancel = $scope.todos_cancel.slice(begin_cancel, end_cancel);
            };

            $scope.size_cancel = function (str) {
                console.log(str);
                $scope.pagesize_cancel = str;
                $scope.currentPage_cancel = 1;
                $scope.numPerPage_cancel = str; console.log("numPerPage=" + $scope.numPerPage_cancel); $scope.makeTodos_cancel();
            }

            $scope.index_cancel = function (str) {
                $scope.pageindex_cancel = str;
                $scope.currentPage_cancel = str; console.log("currentPage=" + $scope.currentPage_cancel); $scope.makeTodos_cancel();
                main_cancel.checked = false;
                $scope.row1 = '';
            }

            $scope.search_cancel = function () {
                $scope.todos_cancel = $scope.searched_cancel($scope.cdetails_data_cancel, $scope.searchText_cancel);
                $scope.totalItems_cancel = $scope.todos_cancel.length;
                $scope.currentPage_cancel = '1';
                if ($scope.searchText_cancel == '') {
                    $scope.todos_cancel = $scope.cdetails_data_cancel;
                }
                $scope.makeTodos_cancel();
            }

            $scope.searched_cancel = function (valLists_cancel, toSearch_cancel) {

                return _.filter(valLists_cancel,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil_cancel(i, toSearch_cancel);
                });
            };

            function searchUtil_cancel(item, toSearch) {
                /* Search Text in all 2 fields */
                return (item.empName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.em_login_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos_cancel.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos_cancel.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            ///////////////////////////////////////////////////////////////
            //Update Approved Leave

            $scope.UserSearch_update = function (str) {
                $scope.filteredTodos_update = [];
                debugger;
                //var leave_t = $scope.edt.leave_type2;

                if ($scope.edt.leave_type2 == 'undefined' || $scope.edt.leave_type2 == '') {
                    swal({ title: "Alert", text: "Please Select Leave Type.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }
                else {
                    $scope.Get_update_employee_list = [];
                    // $scope.filteredTodos_update = [];
                    $('#UpdateApprovedLeavesModel').modal('show')
                }
            }

            $scope.Reset_update = function () {

                $scope.edt.update_login_code = ''
                $scope.edt.update_dept_code = ''
            }

            $scope.Search_forupdateApprovedLeaves = function () {
                debugger;
                // var leave_t = $scope.edt.leave_type2;

                if ($scope.edt.leave_type2 == 'undefined') {
                    swal({ title: "Alert", text: "Please Select Leave Type.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }
                else {
                    //main = document.getElementById('mainchk1');
                    //if (main.checked == true) {
                    //    main.checked = false;
                    //}

                    $scope.searchtable = false;
                    $scope.searchtable_cancelapprove = false;

                    debugger;
                    console.log('In searchallteacher');
                    console.log($scope.edt.comp_code);
                    console.log($scope.edt.login_code);
                    console.log($scope.edt.leave_type);
                    $scope.busy = true;

                    $http.get(ENV.apiUrl + "api/leavedetails/Get_approed_employee_list?leave_code=" + $scope.edt.leave_type3 + "&dept_code=" + $scope.edt.update_dept_code + "&login_code=" + $scope.edt.update_login_code).then(function (Get_update_employee_list) {
                        $scope.Get_update_employee_list = Get_update_employee_list.data;
                        $scope.busy = false;
                        $scope.searchtable_updateapprove = true;
                    });
                }
            }

            $scope.DataEnroll_update = function () {
                debugger;
                $scope.update_approvedlist = [];
                for (var i = 0; i < $scope.Get_update_employee_list.length; i++) {
                    var t = $scope.Get_update_employee_list[i].lt_no_update;
                    var v = document.getElementById(t + i);
                    if (v.checked == true)
                        $scope.update_approvedlist.push($scope.Get_update_employee_list[i]);
                }

                $scope.Get_update_employee_list = [];

                for (var i = 0; i < $scope.update_approvedlist.length; i++) {
                    // var date = 
                    $scope.update_approvedlist[i].lt_start_date = moment($scope.update_approvedlist[i].lt_start_date).format('YYYY/MM/DD');
                    $scope.update_approvedlist[i].lt_end_date = moment($scope.update_approvedlist[i].lt_end_date).format('YYYY/MM/DD');
                }

                $scope.cdetails_data_update = $scope.update_approvedlist;
                $scope.totalItems_update = $scope.cdetails_data_update.length;
                $scope.todos_update = $scope.cdetails_data_update;
                $scope.makeTodos_update();
            }

            $scope.makeTodos_update = function () {
                var rem_update = parseInt($scope.totalItems_update % $scope.numPerPage_update);
                if (rem_update == '0') {
                    $scope.pagersize_update = parseInt($scope.totalItems_update / $scope.numPerPage_update);
                }
                else {
                    $scope.pagersize_update = parseInt($scope.totalItems_update / $scope.numPerPage_update) + 1;
                }
                var begin_update = (($scope.currentPage_update - 1) * $scope.numPerPage_update);
                var end_update = parseInt(begin_update) + parseInt($scope.numPerPage_update);

                $scope.filteredTodos_update = $scope.todos_update.slice(begin_update, end_update);
            };

            $scope.size_update = function (str) {
                console.log(str);
                $scope.pagesize_update = str;
                $scope.currentPage_update = 1;
                $scope.numPerPage_update = str; console.log("numPerPage=" + $scope.numPerPage_update); $scope.makeTodos_update();
            }

            $scope.index_update = function (str) {
                $scope.pageindex_update = str;
                $scope.currentPage_update = str; console.log("currentPage=" + $scope.currentPage_update); $scope.makeTodos_update();
                main_update.checked = false;
                $scope.row1 = '';
            }

            $scope.search_update = function () {
                $scope.todos_update = $scope.searched_update($scope.cdetails_data_update, $scope.searchText_update);
                $scope.totalItems_update = $scope.todos_update.length;
                $scope.currentPage_update = '1';
                if ($scope.searchText_update == '') {
                    $scope.todos_update = $scope.cdetails_data_update;
                }
                $scope.makeTodos_update();
            }

            $scope.searched_update = function (valLists_update, toSearch_update) {

                return _.filter(valLists_update,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil_update(i, toSearch_update);
                });
            };

            function searchUtil_update(item, toSearch) {
                /* Search Text in all 2 fields */
                return (item.empName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.em_login_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            var sendata_update = [];

            $scope.ApprovedLeaveUpdate = function () {
                debugger;
                for (var i = 0; i < $scope.filteredTodos_update.length; i++) {
                    //$scope.filteredTodos_update[i].lt_end_date_new=
                    if ($scope.filteredTodos_update[i].lt_no_update == true) {
                        sendata_update.push($scope.filteredTodos_update[i]);
                    }
                }

                $http.post(ENV.apiUrl + "api/leavedetails/UpdateLeaves", sendata_update).then(function (LeaveDetails_update) {
                    $scope.LeaveDetails_update = LeaveDetails_update.data;
                    if ($scope.LeaveDetails_update == true) {
                        swal('', 'Approved Leave Updated');
                        $scope.EMPLeave_Details = [];
                        $scope.filteredTodos_update = [];
                        $scope.edt.leave_type2 = null;
                        $scope.searchText_update = null;
                        //$scope.SelectedUserLst = null;
                    }
                    else {
                        swal('', 'Approved Leave Not Updated');
                    }
                });
                sendata_update = [];
            }

            $scope.GetUpdateApprovedData_Clear = function () {
                // $scope.filteredTodos_update = [];
                //$scope.edt.leave_type2 = '';
            }

            /////////////////////////////////////////////////////////////
            //MarK LWP EMPLOYEE
            $scope.UserSearch_lwp = function (str) {

                if ($scope.edt.lstart_date == undefined || $scope.edt.lstart_date == '') {
                    swal({ title: "Alert", text: "Please Select Leave Start Date.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }
                else {
                    $scope.global_Search_click();
                    $('#Global_Search_Modal').modal({ backdrop: "static" });
                }
            }
            $scope.$on('global_cancel', function (str) {
                console.log($scope.SelectedUserLst);
                debugger;
                if ($scope.currentab == 'lwp') {
                    if ($scope.SelectedUserLst.length > 0) {
                        $scope.cdetails_data_lwp = $scope.SelectedUserLst;
                        $scope.totalItems_lwp = $scope.cdetails_data_lwp.length;
                        $scope.todos_lwp = $scope.cdetails_data_lwp;
                        $scope.makeTodos_lwp();
                    }
                }
                else if ($scope.currentab == 'holiday') {
                    if ($scope.SelectedUserLst.length > 0) {
                        $scope.filteredTodos_holiday = $scope.SelectedUserLst;

                        if ($scope.filteredTodos_holiday.length > 0)
                        { $scope.users = true }
                    }
                }
            });

            $scope.makeTodos_lwp = function () {
                var rem_lwp = parseInt($scope.totalItems_lwp % $scope.numPerPage_lwp);
                if (rem_lwp == '0') {
                    $scope.pagersize_lwp = parseInt($scope.totalItems_lwp / $scope.numPerPage_lwp);
                }
                else {
                    $scope.pagersize_lwp = parseInt($scope.totalItems_lwp / $scope.numPerPage_lwp) + 1;
                }
                var begin_lwp = (($scope.currentPage_lwp - 1) * $scope.numPerPage_lwp);
                var end_lwp = parseInt(begin_lwp) + parseInt($scope.numPerPage_lwp);

                $scope.filteredTodos_lwp = $scope.todos_lwp.slice(begin_lwp, end_lwp);
            };

            $scope.size_lwp = function (str) {
                console.log(str);
                $scope.pagesize_lwp = str;
                $scope.currentPage_lwp = 1;
                $scope.numPerPage_lwp = str; console.log("numPerPage=" + $scope.numPerPage_lwp); $scope.makeTodos_lwp();
            }

            $scope.index_lwp = function (str) {
                $scope.pageindex_lwp = str;
                $scope.currentPage_lwp = str; console.log("currentPage=" + $scope.currentPage_lwp); $scope.makeTodos_lwp();
                main_lwp.checked = false;
                $scope.row1 = '';
            }

            $scope.search_lwp = function () {
                $scope.todos_lwp = $scope.searched_lwp($scope.cdetails_data_lwp, $scope.searchText_lwp);
                $scope.totalItems_lwp = $scope.todos_lwp.length;
                $scope.currentPage_lwp = '1';
                if ($scope.searchText_lwp == '') {
                    $scope.todos_lwp = $scope.cdetails_data_lwp;
                }
                $scope.makeTodos_lwp();
            }

            $scope.searched_lwp = function (valLists_lwp, toSearch_lwp) {

                return _.filter(valLists_lwp,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil_lwp(i, toSearch_lwp);
                });
            };

            function searchUtil_lwp(item, toSearch) {
                /* Search Text in all 2 fields */
                return (item.empName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.em_login_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.AssignLWPforEmployees = function () {
                debugger;
                for (var i = 0; i < $scope.filteredTodos_lwp.length; i++) {
                    if ($scope.filteredTodos_lwp[i].ndays != 'undefined') {
                        $scope.filteredTodos_lwp[i].lstart_date = $scope.edt.lstart_date;
                        lwpsenddata.push($scope.filteredTodos_lwp[i])
                    }
                }
                $http.post(ENV.apiUrl + "api/leavedetails/Assign_LWPforEmployees", lwpsenddata).then(function (Assign_LWPforEmployees) {
                    $scope.AssignLWPforEmp = Assign_LWPforEmployees.data;
                    debugger;
                    console.log($scope.AssignLWPforEmp);
                    if ($scope.AssignLWPforEmp == true) {
                        swal({
                            text: 'Assigned LWP Successfully',
                            width: 300,
                            height: 300
                        });
                        $scope.GET_LWPforEmployees = [];
                        $scope.filteredTodos_lwp = null;
                        $scope.edt.lstart_date = null;
                        $scope.searchText_lwp = null;
                        //$scope.SelectedUserLst = null;
                    }
                    else if ($scope.AssignLWPforEmp == false) {
                        swal({
                            text: 'Assigned LWP Successfully.',
                            width: 300,
                            height: 300
                        });
                        $scope.GET_LWPforEmployees = [];
                        $scope.filteredTodos_lwp = null;
                        $scope.edt.lstart_date = null;
                        $scope.searchText_lwp = null;
                        //$scope.SelectedUserLst = null;
                    }
                })
            }

            //////////////////////////////////////////////////////////////
            //Staff Attendance for Holiday

            $scope.GetCurrentTab = function (str) {

                if (str == 'lwp') { $scope.currentab = 'lwp' }
                if (str == 'holiday') { $scope.currentab = 'holiday' }
            }

            $scope.UserSearch_holiday = function (str) {
                if ($scope.edt.mnth != undefined) {
                    debugger;
                    $scope.global_Search_click();
                    $('#Global_Search_Modal').modal({ backdrop: "static" });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Month.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }
            }

            $scope.data = [];

            $scope.GetUsers = function (empnumber, empname) {
                console.log(empname);
                debugger;
                if (document.getElementById(empnumber).checked == true) {
                    var data1 = { str: empnumber, month: $scope.edt.mnth };

                    $scope.data.push(data1);
                    data1 = {};
                }
                else {
                    for (var i = $scope.data.length; i--;) {
                        if ($scope.data[i].str == empnumber) {
                            $scope.data.splice(i, 1);
                        }
                    }
                }
            }

            $scope.ViewHolidays = function () {
                $http.post(ENV.apiUrl + "api/leavedetails/EMPAllHolidays", $scope.data).then(function (Getholiday) {
                    for (var i = 0; i < Getholiday.data.length; i++) {
                        // var date = 
                        Getholiday.data[i].holiday = moment(Getholiday.data[i].holiday).format('YYYY/MM/DD')
                    }
                    $scope.Getholidays = Getholiday.data;
                    if ($scope.Getholidays.length > 0) {

                    }
                    else {
                        swal({
                            text: 'Holidays not defined for the selected month for these employees.',
                            width: 300,
                            height: 300
                        });
                    }
                });
            }

            var holidaysenddata = [];
            var holidaysenddata_temp = [];

            $scope.AssignHolidaysforEmployees = function () {
                debugger;

                console.log($scope.filteredTodos_holiday);
                console.log($scope.Getholidays);
                var j = 0;

                //for (var j = 0; j < $scope.filteredTodos_holiday.length; j++)
                //{
                //     if ($scope.filteredTodos_holiday[j].flag == true)
                //     {
                //         for (var i = 0; i < $scope.Getholidays.length; i++)
                //         {
                //             if ($scope.Getholidays[i].holiday_flag == true)
                //             {
                //                 console.log($scope.filteredTodos_holiday[j].em_number);
                //                console.log($scope.Getholidays[i].holiday);

                //                $scope.Getholidays[i].em_login_code = $scope.filteredTodos_holiday[j].em_number;

                //                //holidaysenddata.push($scope.Getholidays[i])
                //                 //console.log(holidaysenddata);

                //                holidaysenddata.push($scope.Getholidays[i]);
                //                holidaysenddata_temp = holidaysenddata;
                //            }                            
                //        }                        
                //    }
                // }
                $scope.data = [];
                for (var j = 0; j < $scope.filteredTodos_holiday.length; j++) {
                    if ($scope.filteredTodos_holiday[j].flag == true) {
                        for (var i = 0; i < $scope.Getholidays.length; i++) {
                            if ($scope.Getholidays[i].holiday_flag == true) {

                                console.log($scope.filteredTodos_holiday[j].em_number);
                                console.log($scope.Getholidays[i].holiday);

                                $scope.Getholidays[i].em_login_code = $scope.filteredTodos_holiday[j].em_number;

                                var data1 = { em_login_code: $scope.Getholidays[i].em_login_code, holiday: $scope.Getholidays[i].holiday };

                                $scope.data.push(data1);
                            }
                        }
                    }
                }

                console.log($scope.data);
                $http.post(ENV.apiUrl + "api/leavedetails/AssignHolidaysforEmployees_1", $scope.data).then(function (Assign_HolidayforEmployees) {
                    $scope.AssignHolidayforEmp = Assign_HolidayforEmployees.data;
                    if ($scope.AssignHolidayforEmp == true) {
                        swal({
                            text: 'Holidays Assigned Successfully.',
                            width: 300,
                            height: 300
                        });
                        holidaysenddata = [];
                        $scope.Getholidays = [];
                        $scope.data = [];
                    } else {
                        swal({
                            text: 'Holidays Not Assigned.',
                            width: 300,
                            height: 300
                        });
                        holidaysenddata = [];
                        $scope.Getholidays = [];
                        $scope.data = [];
                    }
                });

                //$http.post(ENV.apiUrl + "api/leavedetails/AssignHolidaysforEmployees", holidaysenddata).then(function (Assign_HolidayforEmployees) {
                //    $scope.AssignHolidayforEmp = Assign_HolidayforEmployees.data;

                //if ($scope.AssignHolidayforEmp == true) {
                //    swal({
                //        text: 'Assigned Holidays Successfully',
                //        width: 300,
                //        height: 300
                //    });
                //    // $scope.GET_LWPforEmployees = [];
                //}
                //else {
                //    swal({
                //        text: 'Not Assigned Holidays',
                //        width: 300,
                //        height: 300
                //    });
                //}
                //})
            }

        }])
})();