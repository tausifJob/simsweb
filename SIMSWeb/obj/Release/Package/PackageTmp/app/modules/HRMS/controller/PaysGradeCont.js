﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('PaysGradeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.paysgrade_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.edit_code = false;

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.countData = [
                 { val: 5, data: 5 },
                 { val: 10, data: 10 },
                 { val: 15, data: 15 },

            ]

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $http.get(ENV.apiUrl + "api/common/ShiftMaster/getCompany").then(function (res) {
                $scope.comp_data = res.data;
                console.log($scope.comp_data);
            });

            $scope.Getcatinfo = function (comp) {
                $http.get(ENV.apiUrl + "api/common/PaysGrade/Getcategorycode?comp_code=" + comp).then(function (res) {
                    $scope.cat_data = res.data;
                    console.log($scope.cat_data);
                });
            }

            $http.get(ENV.apiUrl + "api/common/PaysGrade/Get_paygrade").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.paysgrade_data = res.data;
                if ($scope.paysgrade_data.length > 0)
                {
                    $scope.pager = true;
                    if ($scope.countData.length > 3)
                    {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.paysgrade_data.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.paysgrade_data.length, data: 'All' })
                    }

                    $scope.totalItems = $scope.paysgrade_data.length;
                    $scope.todos = $scope.paysgrade_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                }
                else {
                    swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                    $scope.filteredTodos = [];
                }

            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edit_code = true;

                $scope.edt =
                    {
                        gr_company_code: str.gr_company_code,
                        gr_code: str.gr_code,
                        gr_desc: str.gr_desc,
                        gr_category_code: str.gr_category_code,
                        gr_print_order: str.gr_print_order,
                        gr_min_basic: str.gr_min_basic,
                        gr_max_basic: str.gr_max_basic,
                        gr_amt: str.gr_amt
                    }
                $scope.Getcatinfo(str.gr_company_code);
            }

            $scope.getchkamount = function () {

                if ($scope.edt.gr_min_basic != '' && $scope.edt.gr_max_basic != '') {
                    if (parseFloat($scope.edt.gr_min_basic) > parseFloat($scope.edt.gr_max_basic)) {
                        swal({ title: "Alert", text: "Minimum Basic Can Not Grater than Maximum Basic", showCloseButton: true, width: 380, });
                        $scope.edt.gr_min_basic = '';
                    }
                }
            }

            $scope.Save = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        if ($scope.edt.gr_amt == '') {
                            $scope.edt.gr_amt = 0;
                        }
                        var data = ({
                            gr_company_code: $scope.edt.gr_company_code,
                            gr_code: $scope.edt.gr_code,
                            gr_desc: $scope.edt.gr_desc,
                            gr_category_code: $scope.edt.gr_category_code,
                            gr_print_order: $scope.edt.gr_print_order,
                            gr_min_basic: parseFloat($scope.edt.gr_min_basic),
                            gr_max_basic: parseFloat($scope.edt.gr_max_basic),
                            gr_amt: $scope.edt.gr_amt,
                            opr: 'I'
                        });

                        data1.push(data);


                        $http.post(ENV.apiUrl + "api/common/PaysGrade/CUDPaysGrade", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Pays Grade Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal({ title: "Alert", text: "Pays Grade Not Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                        });

                        data1 = [];
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $scope.currentPage = 1;

                $http.get(ENV.apiUrl + "api/common/PaysGrade/Get_paygrade").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.paysgrade_data = res.data;
                    if ($scope.paysgrade_data.length > 0)
                    {
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.paysgrade_data.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.paysgrade_data.length, data: 'All' })
                        }

                        $scope.totalItems = $scope.paysgrade_data.length;
                        $scope.todos = $scope.paysgrade_data;
                        $scope.makeTodos();
                        $scope.grid = true;
                    }
                    else
                    {
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                        $scope.filteredTodos = [];
                    }

                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.row1 = '';

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    if ($scope.edt.gr_amt == '') {
                        $scope.edt.gr_amt = 0;
                    }
                    var data = ({
                        gr_company_code: $scope.edt.gr_company_code,
                        gr_code: $scope.edt.gr_code,
                        gr_desc: $scope.edt.gr_desc,
                        gr_category_code: $scope.edt.gr_category_code,
                        gr_print_order: $scope.edt.gr_print_order,
                        gr_min_basic: $scope.edt.gr_min_basic,
                        gr_max_basic: $scope.edt.gr_max_basic,
                        gr_amt: $scope.edt.gr_amt,
                        opr: 'U'
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/PaysGrade/CUDPaysGrade", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Pays Grade Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal({ title: "Alert", text: "Pays Grade Not Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                    });
                    data1 = [];
                }
            }

            $scope.Delete = function () {
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById("test-" + i);

                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'gr_company_code': $scope.filteredTodos[i].gr_company_code,
                            'gr_code': $scope.filteredTodos[i].gr_code,
                            'gr_desc': $scope.filteredTodos[i].gr_desc,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/common/PaysGrade/CUDPaysGrade", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Pays Grade Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Pays Grade Not Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById("test-" + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }

            }

            $scope.New = function () {
                $scope.edit_code = false;
                var autoid;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = [];

                $http.get(ENV.apiUrl + "api/common/PaysGrade/getAutoGenerateSequncepers233").then(function (res) {
                    $scope.obj2 = res.data;
                    console.log($scope.obj2);
                    $scope.edt['gr_code'] = $scope.obj2;
                });
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }
            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }
            $scope.size = function (str) {
                if (str == 5 || str == 10 || str == 15) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.paysgrade_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.paysgrade_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.gr_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.gr_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

        }])
})();