﻿(function () {
    'use strict';
    var main = '';
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PayableIncrementCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.busyindicator = false;
            $scope.grid = true;
            $scope.color = '#edefef';
            $scope.rgvtbl = false;
            $scope.btnhide = true;
            $scope.dept_code = '';
            $scope.desg_code = '';
            $scope.grade = '';
            $scope.pay = '';
            $scope.edt = {};

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.setStart = function (date) {
                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                var date1 = year + "-" + month + "-" + day;
                $scope.pa_effective_from = date1;

            }

            $scope.setEnd = function (date) {
                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                var date1 = year + "-" + month + "-" + day;
                $scope.pa_effective_upto = date1;

            }

            $http.get(ENV.apiUrl + "api/PayableIncrement/GetDept").then(function (GetDept) {
                $scope.GetDept = GetDept.data;

            });

            $http.get(ENV.apiUrl + "api/PayableIncrement/GetDesg").then(function (GetDesg) {
                $scope.GetDesg = GetDesg.data;

            });

            $http.get(ENV.apiUrl + "api/PayableIncrement/GetGrade").then(function (GetGrade) {
                $scope.GetGrade = GetGrade.data;

            });

            $http.get(ENV.apiUrl + "api/PayableIncrement/GetPayCode").then(function (GetPayCode) {
                $scope.GetPayCode = GetPayCode.data;
            });

            $http.get(ENV.apiUrl + "api/PayableIncrement/GetinvType").then(function (GetinvType) {
                $scope.GetinvType = GetinvType.data;
                $scope.edt = { inc_type: $scope.GetinvType[0].inc_type };
            });

            var date = new Date();
            var month = (date.getMonth() + 1);
            var day = date.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            $scope.pa_effective_from = date.getFullYear() + '-' + (month) + '-' + (day);

            $scope.changeno = function (obj) {

                //console.log(obj)
                //$scope.maindata = obj;

                //if ($scope.maindata.leave_max >= 365 || $scope.maindata.leave_max <= 0) {
                //    swal('', 'Please enter 0 to 365 days.');
                //    $scope.maindata.leave_max = $scope.maindata.leave_max_old;
                //}
                //else {

                //    if (parseFloat($scope.maindata.leave_taken) > parseFloat($scope.maindata.leave_max)) {
                //        swal('', 'Maximum leave not less than taken leave.');
                //        $scope.maindata.leave_max = $scope.maindata.leave_max_old;
                //    }
                //    else
                //        $scope.maindata.leave_max_old = $scope.maindata.leave_max;
                //}
            }

            $scope.onlyNumbers = function (event, leave_max) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }

                }

                event.preventDefault();


            };

            $scope.Show_data = function () {

                debugger;
                if ($scope.edt.dept_code == undefined)
                    $scope.edt.dept_code = '';
                if ($scope.edt.desg_code == undefined)
                    $scope.edt.desg_code = '';
                if ($scope.edt.gr_code == undefined)
                    $scope.edt.gr_code = '';
                if ($scope.edt.paycode_code == undefined)
                    $scope.edt.paycode_code = '';
                //if ($scope.edt.inc_type == '' || $scope.edt.inc_type == undefined) {
                //    swal('','Please select increment type.');
                //}
                //else
                {
                    {

                        $scope.busy = true;
                        var datalst = {
                            paycode_code: $scope.edt.paycode_code,
                            dept_code: $scope.edt.dept_code,
                            desg_code: $scope.edt.desg_code,
                            gr_code: $scope.edt.gr_code,
                            em_number: $scope.edt.emp_number
                        }
                        $http.post(ENV.apiUrl + "api/PayableIncrement/EmpDetails", datalst).then(function (EmpDetails) {
                            $scope.GetEmpDetails = EmpDetails.data;

                            if ($scope.GetEmpDetails.length > 0) {
                                $scope.btnhide = false;
                                $scope.rgvtbl = true;
                            }
                            else {
                                $scope.rgvtbl = false;
                                $scope.btnhide = true;
                                swal('', 'Record not found.');
                            }
                            $scope.busy = false;
                        });

                    }
                }

            }

            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.reset_form = function () {
                $scope.edt = {};
                $scope.GetEmpDetails = [];
                $scope.btnhide = true;
                $scope.rgvtbl = false;
                var date = new Date();
                var month = (date.getMonth() + 1);
                var day = date.getDate();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                $scope.pa_effective_from = date.getFullYear() + '-' + (month) + '-' + (day);
                $scope.pa_effective_upto = '';


            }

            $scope.abc_form = function () {
                $('#ALLEMPpayroll').modal('hide');
            }

            $scope.Show_data_employee = function () {
                if ($scope.edt.dept_code == undefined)
                    $scope.edt.dept_code = '';
                if ($scope.edt.desg_code == undefined)
                    $scope.edt.desg_code = '';
                if ($scope.edt.gr_code == undefined)
                    $scope.edt.gr_code = '';
                if ($scope.edt.paycode_code == undefined)
                    $scope.edt.paycode_code = '';
                //if ($scope.edt.inc_type == '' || $scope.edt.inc_type == undefined) {
                //    swal('','Please select increment type.');
                //}
                //else
                {
                    {

                        var datalst = {
                            paycode_code: $scope.edt.paycode_code,
                            dept_code: $scope.edt.dept_code,
                            desg_code: $scope.edt.desg_code,
                            gr_code: $scope.edt.gr_code,
                            em_number: $scope.edt.emp_number
                        }
                        $http.post(ENV.apiUrl + "api/PayableIncrement/AllEmpDetails", datalst).then(function (AllEmpDetails) {
                            $scope.AllEmpDetails = AllEmpDetails.data;

                            if ($scope.AllEmpDetails.length > 0) {
                            }
                            else {
                                swal('', 'Record not found.');
                            }
                        });

                    }
                }
                $('#ALLEMPpayroll').modal('show');
            }


            $scope.genrate = function () {
                var datalst = [];
                $scope.busy = true;
                $scope.rgvtbl = false;
                $scope.btnhide = true;
                debugger;

                for (var i = 0; i < $scope.GetEmpDetails.length; i++) {
                    if ($scope.GetEmpDetails[i].pay_amount > 0 && $scope.GetEmpDetails[i].inc_period > 0) {
                        var data = {

                            paycode_code: $scope.GetEmpDetails[i].paycode_code,
                            dept_code: $scope.edt.dept_code,
                            desg_code: $scope.edt.desg_code,
                            gr_code: $scope.edt.gr_code,
                            inc_type: $scope.GetEmpDetails[i].inc_type,
                            inc_fixed_amout: $scope.GetEmpDetails[i].pay_amount,
                            inc_per: $scope.GetEmpDetails[i].pay_amount,
                            inc_period: $scope.GetEmpDetails[i].inc_period,
                            em_number: $scope.edt.emp_number


                        }
                        datalst.push(data);

                    }
                }
                $http.post(ENV.apiUrl + "api/PayableIncrement/UpdatePaysPayable", datalst).then(function (UpdatePaysPayable) {
                    $scope.UpdatePaysPayable = UpdatePaysPayable.data;
                    if ($scope.UpdatePaysPayable) {
                        swal('', 'Record successfully updated.');
                    }
                    else
                        swal('', 'Record not updated.');
                    $scope.busy = false;
                    $scope.reset_form();
                });

            }







        }])
})();