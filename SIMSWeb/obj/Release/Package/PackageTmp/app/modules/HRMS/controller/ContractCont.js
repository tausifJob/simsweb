﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ContractCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            var main = '';
            $scope.display = false;
            $scope.busyindicator = false;
            $scope.grid = true;

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2015&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;

            });

            $http.get(ENV.apiUrl + "api/contract/Get_Contract").then(function (Get_Contract) {
                $scope.Contract_data = Get_Contract.data;
                $scope.totalItems = $scope.Contract_data.length;
                $scope.todos = $scope.Contract_data;
                $scope.makeTodos();

            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.CheckMultiple();
                }
            };

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            var datasend = [];
            $scope.Submit = function (isvalid) {
                debugger
                if (isvalid) {
                    var data = $scope.edt;
                    data.opr = 'I';
                    datasend.push(data);
                    //var data = $scope.edt;
                    //data.opr = "I";
                    $http.post(ENV.apiUrl + "api/contract/CUD_Contract", datasend).then(function (InsertEmployeeLeaveAssign) {
                        $scope.EmployeeLeaveAssign = InsertEmployeeLeaveAssign.data;
                        $scope.grid = true;
                        $scope.display = false;
                        if ($scope.EmployeeLeaveAssign == true) {
                            swal({
                                text: 'Contract Created Successfully',
                                width: 380,
                                showCloseButton: true
                            });
                            $http.get(ENV.apiUrl + "api/contract/Get_Contract").then(function (Get_Contract) {
                                $scope.Contract_data = Get_Contract.data;
                                $scope.totalItems = $scope.Contract_data.length;
                                $scope.todos = $scope.Contract_data;
                                $scope.makeTodos();

                            });

                        }
                        else {
                            swal({
                                text: 'Contract Not Created',
                                width: 380,
                                showCloseButton: true

                            });
                        }

                    });
                    datasend = [];
                }
            }

            var updatefin = [];
            $scope.Update = function () {

                var data = $scope.edt;
                data.opr = "U";
                updatefin.push(data);
                $http.post(ENV.apiUrl + "api/contract/CUD_Contract", updatefin).then(function (InsertEmployeeLeaveAssign) {
                    $scope.EmployeeLeaveAssign = InsertEmployeeLeaveAssign.data;
                    $scope.grid = true;
                    $scope.display = false;
                    if ($scope.EmployeeLeaveAssign == true) {
                        swal({
                            text: 'Contract Updated Successfully',
                            width: 380,
                            showCloseButton: true

                        });
                        $http.get(ENV.apiUrl + "api/contract/Get_Contract").then(function (Get_Contract) {
                            $scope.Contract_data = Get_Contract.data;
                            $scope.totalItems = $scope.Contract_data.length;
                            $scope.todos = $scope.Contract_data;
                            $scope.makeTodos();
                        });

                    }
                    else {
                        swal({
                            text: 'Contract Not Updated',
                            width: 380,
                            showCloseButton: true

                        });
                    }
                    updatefin = [];
                });

            }

            var deletefin = [];
            $scope.Delete = function () {
                debugger
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i + "-test");
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'cn_code': $scope.filteredTodos[i].cn_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            debugger;
                            $http.post(ENV.apiUrl + "api/contract/CUD_Contract", deletefin).then(function (msg) {
                                debugger
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/contract/Get_Contract").then(function (Get_Contract) {
                                                $scope.Contract_data = Get_Contract.data;
                                                $scope.totalItems = $scope.Contract_data.length;
                                                $scope.todos = $scope.Contract_data;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                            }

                                            $scope.CheckAllChecked();
                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/contract/Get_Contract").then(function (Get_Contract) {
                                                $scope.Contract_data = Get_Contract.data;
                                                $scope.totalItems = $scope.Contract_data.length;
                                                $scope.todos = $scope.Contract_data;
                                                $scope.makeTodos();
                                            });
                                            main.checked = false;
                                            $('tr').removeClass("row_selected");
                                        }
                                    });
                                }

                            });
                            deletefin = [];
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i + "-test");
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.New = function () {
                $scope.grid = false;
                $scope.display = true;
                $scope.edt = "";
                $scope.btn_save = true;
            }

            $scope.edit = function (str) {
                $scope.grid = false;
                $scope.display = true;
                $scope.btn_save = false;
                var data = angular.copy(str);
                $scope.edt = data;
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.edt = "";
            }

            var dom;
            $scope.flag = true;
            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='12'>" +
                    "<table class='inner-table' width='100%' cellpadding='5' cellspacing='0' style='border-color:red;border:solid;border-width:02px'>" +
                    "<tbody>" +
                    "<tr style='text-align:center'><td class='semi-bold'>" + "CONSEQUENCE WORKING DAY" + "</td> <td>" + "CONSEQUENCE LEAVE DAY" + " </td><td>" + "MIN WORKING DAY" + "</td><td class='semi-bold''>" + "DURATION" + "</td><td class='semi-bold'>" + "FIRST LEAVE DAY" + "</td></tr>" +
                    "<tr style='text-align:center'><td >" + (info.cn_cons_cl_working_days) + "</td> <td>" + (info.cn_cons_cl_leave_days) + " </td><td>" + (info.cn_min_work_days) + "</td><td>" + (info.cn_duration) + "</td><td>" + (info.cn_first_cl_leave_days) + "</td></tr>" +
                    " </table1></td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {

                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }
            }
            $scope.CheckMultiple = function () {
                debugger
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + "-test");
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + "-test");
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.CheckOneByOne = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }



            $scope.uniCharCode = function ($event, name) {

                if ($event.keyCode >= 48 && $event.keyCode <= 57 || $event.keyCode >= 0 && $event.keyCode <= 8 || $event.keyCode >= 13 && $event.keyCode <= 16) {
                    $scope[name] = false;
                }
                else {
                    $scope[name] = true;
                }
            }



        }])
})();