﻿(function () {
    'use strict';
    var opr = '';
    var deletecode = [];
    var deletecalcualationcode = [];
    var main;
    var main1;
    var data1 = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('GratuityCriteriaCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.grid = true;
            $scope.edit_data = false;
            $scope.GratCri_data = [];
            $scope.edit_code = false;

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            console.log($scope.finnDetail)

            $scope.countData = [
                  { val: 5, data: 5 },
                  { val: 10, data: 10 },
                  { val: 15, data: 15 },

            ]

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $http.get(ENV.apiUrl + "api/GratuityCriteria/Get_paysGratuityCriteria").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.GratCri_data = res.data;
                if ($scope.GratCri_data.length > 0) {
                    $scope.pager = true;
                    if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.GratCri_data.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.GratCri_data.length, data: 'All' })
                    }

                    $scope.totalItems = $scope.GratCri_data.length;
                    $scope.todos = $scope.GratCri_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                }
                else {
                    swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                    $scope.filteredTodos = [];
                }

            });


            $scope.getgrid = function () {
                $scope.currentPage = 1;

                $http.get(ENV.apiUrl + "api/GratuityCriteria/Get_paysGratuityCriteria").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.GratCri_data = res.data;
                    if ($scope.GratCri_data.length > 0) {
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.GratCri_data.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.GratCri_data.length, data: 'All' })
                        }

                        $scope.totalItems = $scope.GratCri_data.length;
                        $scope.todos = $scope.GratCri_data;
                        $scope.makeTodos();
                        $scope.grid = true;
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                        $scope.filteredTodos = [];
                    }

                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.row1 = '';

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.size = function (str) {
                if (str == 5 || str == 10 || str == 15) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $timeout(function () {
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            setTimeout(function () {
                $("#cmb_acc_code,#cmb_bank_code,#cmb_cash_code,#cmb_airfare_code,#cmb_prov_acc_code").select2();
            }, 100);


            $http.get(ENV.apiUrl + "api/GratuityCriteria/Get_All_pays_grade").then(function (res) {
                $scope.paysGrade_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/GratuityCriteria/GetAll_Account_Names?comp_cd=" + $scope.finnDetail.company).then(function (res) {
                $scope.accNo_data = res.data;
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.New = function () {
                $scope.min_day_text = true;
                $scope.min_day_yrs = false;
                $scope.min_day_month = false;
                $scope.min_day_day = false;

                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.editmode = false;
                $scope.newmode = true;
                $scope.edt = [];
                $scope.edt = {

                    grt_criteria_code: "",
                    grt_criteria_name: "",
                    grt_year: "",
                    grt_month: "",
                    grt_days: "",
                    grt_prov_acct_code: ""
                }
                $("#cmb_acc_code").select2("val", "");
                $("#cmb_bank_code").select2("val", "");
                $("#cmb_cash_code").select2("val", "");
                $("#cmb_airfare_code").select2("val", "");
                $("#cmb_prov_acc_code").select2("val", "");
            }

            $scope.edit = function (str) {
                $scope.min_day_text = false;
                $scope.min_day_yrs = true;
                $scope.min_day_month = true;
                $scope.min_day_day = true;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.editmode = true;
                $scope.newmode = false;
                debugger;
                $scope.edt =
                    {
                        grt_criteria_code: str.grt_criteria_code,
                        grt_criteria_name: str.grt_criteria_name,
                        grt_criteria_min_days: str.grt_criteria_min_days,
                        grt_year: str.year,
                        grt_month: str.month,
                        grt_days: str.days,
                        grt_pay_grade: str.grt_pay_grade,
                        grt_acct_code: str.grt_acct_code,
                        grt_acct_name: str.grt_acct_name,



                        grt_bank_acct_code: str.grt_bank_acct_code,
                        grt_bank_acct_name: str.grt_bank_acct_name,

                        grt_cash_acct_code: str.grt_cash_acct_code,
                        grt_cash_acct_name: str.grt_cash_acct_name,

                        grt_airfare_acct_code: str.grt_airfare_acct_code,
                        grt_airfare_acct_name: str.grt_airfare_acct_name,

                        grt_prov_acct_code: str.grt_prov_acct_code,
                        grt_prov_acct_name: str.grt_prov_acct_name,
                        grt_status: str.grt_status,
                    }

                $("#cmb_acc_code").select2("val", str.grt_acct_code);
                $("#cmb_prov_acc_code").select2("val", str.grt_prov_acct_code);
                $("#cmb_bank_code").select2("val", str.grt_bank_acct_code);
                $("#cmb_cash_code").select2("val", str.grt_cash_acct_code);
                $("#cmb_airfare_code").select2("val", str.grt_airfare_acct_code);

            }

            $scope.Save = function (myForm) {
                debugger
                if (myForm) {
                    data1 = [];
                    data = [];
                    if ($scope.edt.grt_year != undefined) {
                        var year = $scope.edt.grt_year * 365;
                    }
                    else {
                        var year = 0;
                    }
                    if ($scope.edt.grt_month != undefined) {
                        var month = $scope.edt.grt_month * 30;
                    }
                    else {
                        var month = 0;
                    }
                    if ($scope.edt.grt_days != undefined) {
                        if ($scope.edt.grt_days > 0) {
                            var day = $scope.edt.grt_days * 1;
                        }
                        else {
                            var day = $scope.edt.grt_days * 0;
                        }
                    }
                    else {
                        var day = 0;
                    }

                    $scope.min_days = (year + month + day);

                    var data = ({
                        grt_criteria_code: $scope.edt.grt_criteria_code,
                        grt_criteria_name: $scope.edt.grt_criteria_name,
                        grt_criteria_min_days: $scope.min_days,
                        grt_pay_grade: $scope.edt.grt_pay_grade,
                        grt_acct_code: $scope.edt.grt_acct_code,
                        grt_bank_acct_code: $scope.edt.grt_bank_acct_code,
                        grt_cash_acct_code: $scope.edt.grt_cash_acct_code,
                        grt_airfare_acct_code: $scope.edt.grt_airfare_acct_code,
                        grt_prov_acct_code: $scope.edt.grt_prov_acct_code,
                        grt_status: $scope.edt.grt_status,
                        opr: 'I'
                    });

                    $scope.samerecord = false;
                    if (data.grt_acct_code == data.grt_bank_acct_code ||
                            data.grt_acct_code == data.grt_cash_acct_code ||
                            data.grt_acct_code == data.grt_prov_acct_code ||
                            data.grt_bank_acct_code == data.grt_cash_acct_code) {
                        swal({ title: "Alert", text: "Account Code, Bank Code or Cash Code should not be same", width: 300, height: 200 });

                    }

                    else {
                        $scope.exist = false;

                        for (var i = 0; i < $scope.GratCri_data.length; i++) {
                            if ($scope.GratCri_data[i].grt_criteria_code == data.grt_criteria_code)
                                $scope.exist = true;
                        }

                        if ($scope.exist) {
                            swal({ title: "Alert", text: "Record Already Exits", width: 300, height: 200 });
                        }

                        else {
                            data1.push(data);
                            $http.post(ENV.apiUrl + "api/GratuityCriteria/CUDInsertpaysGratuityCriteria", data1).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                                }
                                $scope.getgrid();
                            });
                        }
                    }
                }
            }

            $scope.Update = function (myForm) {
                debugger
                if (myForm) {
                    data1 = [];
                    data = [];
                    if ($scope.edt.grt_year != undefined) {
                        var year = $scope.edt.grt_year * 365;
                    }
                    else {
                        var year = 0;
                    }
                    if ($scope.edt.grt_month != undefined) {
                        var month = $scope.edt.grt_month * 30;
                    }
                    else {
                        var month = 0;
                    }
                    if ($scope.edt.grt_days != undefined) {
                        if ($scope.edt.grt_days > 0) {
                            var day = $scope.edt.grt_days * 1;
                        }
                        else {
                            var day = $scope.edt.grt_days * 0;
                        }
                    }
                    else {
                        var day = 0;
                    }

                    $scope.min_days = (year + month + day);

                    var data = ({
                        grt_criteria_code: $scope.edt.grt_criteria_code,
                        grt_criteria_name: $scope.edt.grt_criteria_name,
                        grt_criteria_min_days: $scope.min_days,
                        grt_pay_grade: $scope.edt.grt_pay_grade,
                        grt_acct_code: $scope.edt.grt_acct_code,
                        grt_bank_acct_code: $scope.edt.grt_bank_acct_code,
                        grt_cash_acct_code: $scope.edt.grt_cash_acct_code,
                        grt_airfare_acct_code: $scope.edt.grt_airfare_acct_code,
                        grt_prov_acct_code: $scope.edt.grt_prov_acct_code,
                        grt_status: $scope.edt.grt_status,
                        opr: 'U'
                    });

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/GratuityCriteria/CUDInsertpaysGratuityCriteria", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                        $scope.operation = false;
                        $scope.getgrid();

                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.edt = {

                    grt_criteria_code: "",
                    grt_criteria_name: "",
                    grt_year: "",
                    grt_month: "",
                    grt_days: "",
                    grt_prov_acct_code: ""
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Delete = function () {
                var deletecode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].grt_criteria_code + i);

                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'grt_criteria_code': $scope.filteredTodos[i].grt_criteria_code,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/GratuityCriteria/CUDDeleteGratuityCrite", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                //if ($scope.msg1 != '' || $scope.msg1 != null) {
                                //    swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 });
                                //}
                                //else {
                                //    swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 });
                                //}
                                debugger
                                if ($scope.msg1 != '' || $scope.msg1 != null) {
                                    swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].grt_criteria_code + i);

                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }

            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].grt_criteria_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].grt_criteria_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.GratCri_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.GratCri_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.grt_criteria_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.grt_criteria_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();

                $scope.chk = {}
                $scope.chk['check_all'] = false;
                $scope.row1 = '';
            }

            $scope.check_acccode = function () {
                debugger
                if ($scope.edt.grt_acct_code == $scope.edt.grt_bank_acct_code) {

                    swal({ title: "Alert", text: "Account Code and Bank Code should not be same", width: 300, height: 200 });

                }
                if ($scope.edt.grt_acct_code == $scope.edt.grt_cash_acct_code) {

                    swal({ title: "Alert", text: "Account Code and Cash Code should not be same", width: 300, height: 200 });

                }
            }

            $scope.check_bankcode = function () {
                debugger
                if ($scope.edt.grt_bank_acct_code == $scope.edt.grt_cash_acct_code) {

                    swal({ title: "Alert", text: "Bank Code and Cash Code should not be same", width: 300, height: 200 });

                }
                if ($scope.edt.grt_acct_code == $scope.edt.grt_bank_acct_code) {

                    swal({ title: "Alert", text: "Bank Code and Account Code should not be same", width: 300, height: 200 });

                }
            }

            $scope.check_cashcode = function () {

                if ($scope.edt.grt_bank_acct_code == $scope.edt.grt_cash_acct_code) {

                    swal({ title: "Alert", text: "Cash Code and Bank Code should not be same", width: 300, height: 200 });

                }
                if ($scope.edt.grt_acct_code == $scope.edt.grt_cash_acct_code) {

                    swal({ title: "Alert", text: "Account Code and Cash Code should not be same", width: 300, height: 200 });

                }
            }

            //------------------------------- Graduity_Criteria_Calculation--------------------------------------------//

            $scope.OpengraCalculation = function (gratCri) {
                $scope.Modaltable = true;
                $scope.modaloperation = false;

                $('#MyModalGrt').modal('show');
                $scope.pagesize1 = '5';
                $scope.temp = {
                    grt_applicable_days: "",
                    grt_working_days: "",
                    grt_status: "",
                }
                $scope.Modaltable = true;
                $scope.modaloperation = true;
                $scope.savebtn1 = true;
                $scope.grt_criteria_code = gratCri.grt_criteria_code;

                $scope.size1 = function (str1) {
                    console.log(str1);
                    $scope.pagesize1 = str1;
                    $scope.currentPage1 = 1;
                    $scope.numPerPage1 = str1; console.log("numPerPage1=" + $scope.numPerPage1); $scope.makeTodos1();
                }
                $scope.index1 = function (str1) {
                    $scope.pageindex1 = str1;
                    $scope.currentPage1 = str1; console.log("currentPage1=" + $scope.currentPage1); $scope.makeTodos1();

                    $scope.chkmd = {}
                    $scope.chkmd['check_all_modal'] = false;
                }

                $scope.filteredTodos1 = [], $scope.currentPage1 = 1, $scope.numPerPage1 = 5, $scope.maxSize1 = 5;

                $scope.makeTodos1 = function () {
                    var rem1 = parseInt($scope.totalItems1 % $scope.numPerPage1);
                    if (rem1 == '0') {
                        $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1);
                    }
                    else {
                        $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1) + 1;
                    }
                    var begin1 = (($scope.currentPage1 - 1) * $scope.numPerPage1);
                    var end1 = parseInt(begin1) + parseInt($scope.numPerPage1);

                    console.log("begin1=" + begin1); console.log("end1=" + end1);

                    $scope.filteredTodos1 = $scope.todos1.slice(begin1, end1);
                };

                $http.get(ENV.apiUrl + "api/GratuityCriteria/getpaysGratuityCalculation?criteria_code=" + $scope.grt_criteria_code).then(function (Get_paysGratuityCalculation_data) {
                    $scope.paysGratuityCalculation = Get_paysGratuityCalculation_data.data;
                    $scope.totalItems1 = $scope.paysGratuityCalculation.length;
                    $scope.todos1 = $scope.paysGratuityCalculation;
                    $scope.makeTodos1();
                });

                $scope.New1 = function () {
                    $scope.temp = {
                        grt_applicable_days: "",
                        grt_working_days: "",
                        grt_status: "",
                    }
                    $scope.savebtn1 = true;
                    $scope.updatebtn1 = false;
                    $scope.modaloperation = true;
                    $scope.Modaltable = false;
                    $scope.myFormmodal.$setPristine();
                    $scope.myFormmodal.$setUntouched();
                }

                $scope.up1 = function (str1) {
                    $scope.modaloperation = true;
                    $scope.Modaltable = false;

                    $scope.savebtn1 = false;
                    $scope.updatebtn1 = true;
                    $scope.readonly = true;

                    $scope.temp = {
                        grt_applicable_days: str1.grt_applicable_days,
                        grt_working_days: str1.grt_working_days,
                        grt_status: str1.grt_status
                    };


                }

                $scope.cancel1 = function () {
                    $('#MyModalGrt').modal('hide');
                    $scope.modaloperation = false;
                    $scope.Modaltable = true;
                    $scope.temp = {
                        grt_applicable_days: "",
                        grt_working_days: "",
                        grt_status: "",
                    }

                    $scope.myFormmodal.$setPristine();
                    $scope.myFormmodal.$setUntouched();
                }

                $scope.Save1 = function (myFormmodal) {
                    if (myFormmodal) {
                        data1 = [];
                        var data = {

                            grt_criteria_code: $scope.grt_criteria_code,
                            grt_applicable_days: $scope.temp.grt_applicable_days,
                            grt_working_days: $scope.temp.grt_working_days,
                            grt_status: $scope.temp.grt_status,
                            opr: 'I',
                        };
                        $scope.exist = false;
                        debugger
                        for (var i = 0; i < $scope.paysGratuityCalculation.length; i++) {
                            if ($scope.paysGratuityCalculation.length > 0) {
                                $scope.exist = true;
                            }
                        }
                        if ($scope.exist) {
                            swal({ title: 'Alert', text: "You can insert only one record against each Gratuity Criteria Code", width: 300, height: 200 })
                        }
                        else {
                            debugger
                            data1.push(data);
                            $http.post(ENV.apiUrl + "api/GratuityCriteria/CUDInsertGratuityCalculation", data1).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: 'Alert', text: "Record Inserted Successfully", width: 300, height: 200 })
                                }
                                else {
                                    swal({ title: 'Alert', text: "Record Not Inserted", width: 300, height: 200 })
                                }
                                $http.get(ENV.apiUrl + "api/GratuityCriteria/getpaysGratuityCalculation?criteria_code=" + $scope.grt_criteria_code).then(function (Get_paysGratuityCalculation_data) {
                                    $scope.paysGratuityCalculation = Get_paysGratuityCalculation_data.data;
                                    $scope.totalItems1 = $scope.paysGratuityCalculation.length;
                                    $scope.todos1 = $scope.paysGratuityCalculation;
                                    $scope.makeTodos1();
                                });
                            })
                        }
                        $scope.modaloperation = false;
                        $scope.Modaltable = true;
                    }
                }

                //$scope.Save1 = function (myFormmodal) {
                //    if (myFormmodal) {
                //        data1 = [];
                //        var data = {

                //            grt_criteria_code: $scope.grt_criteria_code,
                //            grt_applicable_days: $scope.temp.grt_applicable_days,
                //            grt_working_days: $scope.temp.grt_working_days,
                //            grt_status:$scope.temp.grt_status,
                //            opr: 'I',
                //        };
                //        $scope.exist = false;

                //        for (var i = 0; i < $scope.paysGratuityCalculation.length; i++) {
                //            if ($scope.paysGratuityCalculation[i].grt_criteria_code == data.grt_applicable_days &&
                //                 $scope.paysGratuityCalculation[i].grt_applicable_days == data.grt_applicable_days &&
                //                $scope.paysGratuityCalculation[i].grt_working_days == data.grt_working_days)
                //            {
                //                $scope.exist = true;
                //            }
                //        }
                //        if ($scope.exist) {
                //            swal({ title: 'Alert', text: "Record Already Exits", width: 300, height: 200 })
                //        }
                //        else {
                //            debugger
                //            data1.push(data);
                //            $http.post(ENV.apiUrl + "api/GratuityCriteria/CUDInsertGratuityCalculation", data1).then(function (msg) {
                //                $scope.msg1 = msg.data;
                //                if ($scope.msg1 == true) {
                //                    swal({ title: 'Alert', text: "Record Inserted Successfully", width: 300, height: 200 })
                //                }
                //                else {
                //                    swal({ title: 'Alert', text: "Record Not Inserted", width: 300, height: 200 })
                //                }
                //                $http.get(ENV.apiUrl + "api/GratuityCriteria/getpaysGratuityCalculation?criteria_code=" + $scope.grt_criteria_code).then(function (Get_paysGratuityCalculation_data) {
                //                    $scope.paysGratuityCalculation = Get_paysGratuityCalculation_data.data;
                //                    $scope.totalItems1 = $scope.paysGratuityCalculation.length;
                //                    $scope.todos1 = $scope.paysGratuityCalculation;
                //                    $scope.makeTodos1();
                //                });
                //            })
                //        }
                //        $scope.modaloperation = false;
                //        $scope.Modaltable = true;
                //    }
                //}

                $scope.Update1 = function (myFormmodal) {
                    if (myFormmodal) {
                        data1 = [];

                        var data = {
                            grt_criteria_code: $scope.grt_criteria_code,
                            grt_applicable_days: $scope.temp.grt_applicable_days,
                            grt_working_days: $scope.temp.grt_working_days,
                            grt_status: $scope.temp.grt_status,
                            opr: 'U',
                        };
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/GratuityCriteria/CUDInsertGratuityCalculation", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: 'Alert', text: "Record Updated Successfully", width: 300, height: 200 })
                            }
                            else {
                                swal({ title: 'Alert', text: "Record Not Updated", width: 300, height: 200 })
                            }
                            $scope.modaloperation = false;
                            $http.get(ENV.apiUrl + "api/GratuityCriteria/getpaysGratuityCalculation?criteria_code=" + $scope.grt_criteria_code).then(function (Get_paysGratuityCalculation_data) {
                                $scope.paysGratuityCalculation = Get_paysGratuityCalculation_data.data;
                                $scope.totalItems1 = $scope.paysGratuityCalculation.length;
                                $scope.todos1 = $scope.paysGratuityCalculation;
                                $scope.makeTodos1();
                            });
                        })
                        $scope.modaloperation = false;
                        $scope.Modaltable = true;
                    }
                }

                $scope.CheckAllCheckedModal = function () {
                    debugger
                    var main1 = document.getElementById('mainchk1');
                    for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                        var v = document.getElementById($scope.filteredTodos1[i].grt_criteria_code + $scope.filteredTodos1[i].grt_applicable_days + i);
                        if (main1.checked == true) {
                            v.checked = true;
                            //$('tr').addClass("row_selected");
                        }

                        else {
                            v.checked = false;
                            //  $('tr').removeClass("row_selected");
                        }
                    }

                }

                $scope.checkonebyonedeleteModal = function () {

                    if (main1.checked == true) {
                        main1.checked = false;
                        $scope.row1 = '';
                    }
                }


                $scope.deleterecord1 = function () {

                    deletecalcualationcode = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                        var v = document.getElementById($scope.filteredTodos1[i].grt_criteria_code + $scope.filteredTodos1[i].grt_applicable_days + i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deleteLedgerAccountCntrl = {
                                grt_criteria_code: $scope.filteredTodos1[i].grt_criteria_code,
                                //grt_applicable_days: $scope.filteredTodos[i].grt_applicable_days,
                                //grt_working_days: $scope.filteredTodos[i].grt_working_days,
                                opr: 'D',
                            };
                            deletecalcualationcode.push(deleteLedgerAccountCntrl);
                        }
                    }

                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                $http.post(ENV.apiUrl + "api/GratuityCriteria/CUDInsertGratuityCalculation", deletecalcualationcode).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/GratuityCriteria/getpaysGratuityCalculation?criteria_code=" + $scope.grt_criteria_code).then(function (Get_paysGratuityCalculation_data) {
                                                    $scope.paysGratuityCalculation = Get_paysGratuityCalculation_data.data;
                                                    $scope.totalItems1 = $scope.paysGratuityCalculation.length;
                                                    $scope.todos1 = $scope.paysGratuityCalculation;
                                                    $scope.makeTodos1();
                                                });
                                                main1 = document.getElementById('mainchk1');
                                                if (main1.checked == true) {
                                                    main1.checked = false;
                                                    {
                                                        $scope.row2 = '';
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/GratuityCriteria/getpaysGratuityCalculation?criteria_code=" + $scope.grt_criteria_code).then(function (Get_paysGratuityCalculation_data) {
                                                    $scope.paysGratuityCalculation = Get_paysGratuityCalculation_data.data;
                                                    $scope.totalItems1 = $scope.paysGratuityCalculation.length;
                                                    $scope.todos1 = $scope.paysGratuityCalculation;
                                                    $scope.makeTodos1();
                                                });
                                                main1 = document.getElementById('mainchk1');
                                                if (main1.checked == true) {
                                                    main1.checked = false;
                                                    {

                                                    }
                                                }
                                            }
                                        });
                                    }

                                });
                            }
                            else {
                                for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                                    var v = document.getElementById($scope.filteredTodos1[i].grt_criteria_code + $scope.filteredTodos1[i].grt_applicable_days + i);
                                    if (v.checked == true) {
                                        v.checked = false;

                                        main1.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                    }

                    $scope.currentPage1 = true;
                    main1.checked = false;
                }
            }

            //------------------------------- Graduity_Criteria_Calculation_END----------------------------------------//
        }])
})();
