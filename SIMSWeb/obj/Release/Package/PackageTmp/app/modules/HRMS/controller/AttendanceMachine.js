﻿(function () {
    'use strict';
    var modulecode = [];
    var del = [];
    var data = [];
    var main;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AttendanceMachineCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
          
            $scope.itemsPerPage = '50';

            var date = new Date();
            var month = (date.getMonth() + 1);
            var day = date.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            var todaydate = date.getFullYear() + '-' + (month) + '-' + (day)
            $scope.edt = { sdate: todaydate, edate: todaydate };

            $scope.showdate = function (date, name1) {
                var date = new Date(date);
                var month = (date.getMonth() + 1);
                var day = date.getDate();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                $scope.edt[name1] = date.getFullYear() + '-' + (month) + '-' + (day)
            }

            var first = '';
            var last = '';

 
            var t = document.getElementsByName('first');
            for (var i = 0; i < t.length; i++) {
                if (t[i].checked == true) {
                    first = t[i].value;
                }
            }

            var k = document.getElementsByName('last');
            for (var i = 0; i < k.length; i++) {
                if (k[i].checked == true) {
                    last = k[i].value;
                }
            }

            $scope.SelectRadio = function (val) {
                main = document.getElementById('showChk');
                if (main.checked == true) {
                    main.checked = false;
                }
                first = val;
                }

            $scope.SelectRadio1 = function (val) {
                main = document.getElementById('showChk');
                if (main.checked == true) {
                    main.checked = false;
                }
                last = val;
                
            }
            $scope.movebtn = true;
            $scope.show = function (edit) {

                if (edit != undefined) {
                    if (edit.sdate != '' && edit.edate != '') {
                        main = document.getElementById('showChk');
                        if (main.checked == true) {
                            edit.first = '';
                            edit.last = '';
                        } else {
                            edit.first = first;
                            edit.last = last;
                        }

                        console.log(edit);
                        $http.get(ENV.apiUrl + "api/attendanceMachine/getAttendaceMachineDetails?sdate=" + edit.sdate + "&edate=" + edit.edate + "&punchin=" + edit.first + "&punchout=" + edit.last).then(function (res) {
                            $scope.obj = res.data;
                            $scope.div_hide = false;
                            $scope.display = false;
                            $scope.grid = true;
                            $scope.save1 = false;
                            $scope.value = false;
                            $scope.movebtn = false;
                            console.log($scope.obj);
                            $scope.totalItems = $scope.obj.length;
                            $scope.todos = $scope.obj;
                            $scope.makeTodos();
                        });
                    }
                } else {
                    swal('', 'Select Date to Display Record');
                }
            }

            $scope.save = function () {
               
                $scope.row1 = '';
                var datasend=[];
                debugger;

                for(var i=0;i<$scope.obj.length;i++)
                {
                    var data = {
                        opr:     'I'
                    ,att_emp_id: $scope.obj[i].att_emp_id
                    ,att_date:       $scope.obj[i].att_date
                    ,att_shift1_in:  $scope.obj[i].att_shift1_in
                    ,att_shift1_out: $scope.obj[i].att_shift1_out
                    , att_sdate: $scope.edt.sdate
                    , att_edate: $scope.edt.edate
                
                };
                datasend.push(data);
                }
            

                  
                $http.post(ENV.apiUrl + "api/attendanceMachine/CUDAttendaceMachine", datasend).then(function (msg) {
                    if (msg.data) {
                       //data = [];
                        swal('', 'Record Inserted Successfully');
                        $scope.currentPage = 0;
                    } else {
                        //data = [];
                        swal('', 'Record Not Inserted');
                    }
                })

            }
            $scope.Reset = function () {
                $scope.movebtn = true;
                $scope.currentPage = 0;
                $scope.notshow = false;
                $scope.edt = '';
                var t = document.getElementsByName('first');
                for (var i = 0; i < t.length; i++) {
                    if (t[i].checked == true) {
                        t[i].checked = false;
                    }
                }

                var k = document.getElementsByName('last');
                for (var i = 0; i < k.length; i++) {
                    if (k[i].checked == true) {
                        k[i].checked = false;
                    }
                }
                $scope.filteredTodos = '';
                
            }
            $scope.showAll = function (edit) {
                main = document.getElementById('showChk');
                if (main.checked == true) {
                    var t = document.getElementsByName('first');
                    for (var i = 0; i < t.length; i++) {
                        if (t[i].checked == true) {
                            t[i].checked = false;
                            edit.first = '';
                        }
                    }

                    var k = document.getElementsByName('last');
                    for (var i = 0; i < k.length; i++) {
                        if (k[i].checked == true) {
                            k[i].checked = false;
                            edit.last = '';

                        }
                    }

                } else {
                    var t = document.getElementsByName('first');
                    for (var i = 0; i < t.length; i++) {
                        if (t[0].checked == false) {
                            t[0].checked = true;
                            edit.first = first;
                        }
                    }

                    var k = document.getElementsByName('last');
                    for (var i = 0; i < k.length; i++) {
                        if (k[1].checked == false) {
                            k[1].checked = true;
                            edit.last = last;

                        }
                    }
                }


            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 50, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                console.log($scope.filteredTodos);
            };

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }
          
            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;


            };

            $scope.insertonebyone = function () {
              
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }


            }
            
            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
            var dom;
            var count = 0;
        

            $scope.searched = function (valLists, toSearch) {


                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.att_emp_id.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.empname.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.att_shift1_in.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }
           

         
            
        }])
})();