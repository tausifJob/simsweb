﻿(function () {
    'use strict';
    var opr = '';
    var paycode = [];
    var main;
    var data1 = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PayCode_1Cont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';

            $scope.table1 = true;
            $http.get(ENV.apiUrl + "api/PayCodeDetails/getPayCodeDetails").then(function (getPayCodeDetails) {
                $scope.PayCode_Data = getPayCodeDetails.data;
                debugger;
                $scope.totalItems = $scope.PayCode_Data.length;
                $scope.todos = $scope.PayCode_Data;
                $scope.makeTodos();
                console.log($scope.PayCode_Data);

            });

            $http.get(ENV.apiUrl + "api/PayCodeDetails/Get_gc_company_Code").then(function (Get_gc_company_Code) {
                $scope.Get_gc_company_Code = Get_gc_company_Code.data;
            });

            $http.get(ENV.apiUrl + "api/PayCodeDetails/GetGradeCode").then(function (GetGradeCode) {
                $scope.GetGradeCode = GetGradeCode.data;
            });

            $http.get(ENV.apiUrl + "api/PayCodeDetails/GetPayCode").then(function (GetPayCode) {
                $scope.GetPayCode = GetPayCode.data;

            });

            $http.get(ENV.apiUrl + "api/PayCodeDetails/GetAccountNo").then(function (GetAccountNo) {
                $scope.GetAccountNo = GetAccountNo.data;

            });
            $scope.pagesize = '5';

            $scope.table1 = true;

            $scope.onlyNumbers = function (event) {
                debugger
                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $scope.pay_code_change = function (str) {
                var abc = '';
                for (var i = 0; i < $scope.GetPayCode.length; i++) {
                    if (str == $scope.GetPayCode[i].pd_pay_code)
                        abc = $scope.GetPayCode[i].paysEarnDed;
                }
                if (abc == 'E') {
                    $scope.credit_acc_dis = true;
                    $scope.debit_acc_dis = false;
                    $scope.edt.pd_credit_acno = '';
                    $scope.edt.pd_debit_acno = '';

                }
                else {
                    $scope.credit_acc_dis = false;
                    $scope.debit_acc_dis = true;
                    $scope.edt.pd_credit_acno = '';
                    $scope.edt.pd_debit_acno = '';
                }
            }

            $scope.operation = false;
            $scope.editmode = false;
            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }
            $scope.edt = "";

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.editmode = false;
                //    opr = 'S';
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = {};
                $scope.credit_acc_dis = true;
                $scope.debit_acc_dis = true;
                $scope._dis = false;

            }

            $scope.up = function (str) {
                debugger;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.operation = true;
                $scope._dis = true;
                if (str.pd_credit_acno == '') {
                    $scope.credit_acc_dis = true;
                }
                if (str.pd_debit_acno == '') {

                    $scope.debit_acc_dis = true;

                }

                $scope.edt = {
                    pd_grade_code: str.pd_grade_code
                , pd_grade_code_nm: str.pd_grade_code_nm
                , pd_pay_code: str.pd_pay_code
                , pd_pay_code_nm: str.pd_pay_code_nm
                , pd_company_code: str.pd_company_code
                , company_name: str.company_name
                , pd_amount: str.pd_amount
                , pd_credit_acno: str.pd_credit_acno
                , pd_credit_acno_nm: str.pd_credit_acno_nm
                , pd_debit_acno: str.pd_debit_acno
                , pd_debit_acno_nm: str.pd_debit_acno_nm
                , pd_cash_acno: str.pd_cash_acno
                , pd_cash_acno_nm: str.pd_cash_acno_nm
                , pd_bank_acno: str.pd_bank_acno
                , pd_bank_acno_nm: str.pd_bank_acno_nm
                }

            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.edt = {
                    pc_company_code: '',
                    pc_earn_dedn_tag: '',
                    pc_citi_exp_tag: '',
                    pc_company_grade_tag: '',
                    cp_desc: '',
                    cp_change_desc: '',
                    cp_display_order: ''

                }

            }

            $scope.Save = function (myForm) {
                debugger;

                data1 = [];
                var data = $scope.edt;
                data.opr = 'I';
                $scope.exist = false;
                for (var i = 0; i < $scope.PayCode_Data.length; i++) {
                    if ($scope.PayCode_Data[i].pd_grade_code == data.pd_grade_code &&
                        $scope.PayCode_Data[i].pd_pay_code == data.pd_pay_code &&
                        $scope.PayCode_Data[i].pd_company_code == data.pd_company_code &&
                        $scope.PayCode_Data[i].pd_amount == data.pd_amount &&
                        $scope.PayCode_Data[i].pd_credit_acno == data.pd_credit_acno &&
                        $scope.PayCode_Data[i].pd_debit_acno == data.pd_debit_acno &&
                        $scope.PayCode_Data[i].pd_cash_acno == data.pd_cash_acno &&
                        $scope.PayCode_Data[i].pd_bank_acno == data.pd_bank_acno) {
                        $scope.exist = true;
                    }
                }
                if ($scope.exist) {
                    swal({ title: 'Alert', text: "Display order Already exists", width: 300, height: 200 });
                }

                else {
                    data1.push(data);
                    debugger;
                    console.log(data1);
                    $http.post(ENV.apiUrl + "api/PayCodeDetails/PayCodeDetailsCUD", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: 'Alert', text: "Record Inserted Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: 'Alert', text: "Record not Inserted", width: 300, height: 200 });
                        }

                        $scope.operation = false;
                        $http.get(ENV.apiUrl + "api/PayCodeDetails/getPayCodeDetails").then(function (getPayCodeDetails) {
                            $scope.PayCode_Data = getPayCodeDetails.data;
                            $scope.totalItems = $scope.PayCode_Data.length;
                            $scope.todos = $scope.PayCode_Data;
                            $scope.makeTodos();

                        });
                    });
                }
                $scope.table1 = true;
                $scope.operation = false;
            }

            $scope.Update = function (myForm) {

                data1 = [];
                var data = {
                    'pd_grade_code': $scope.edt.pd_grade_code
            , 'pd_pay_code': $scope.edt.pd_pay_code
            , 'pd_company_code': $scope.edt.pd_company_code
            , 'pd_amount': $scope.edt.pd_amount
            , 'pd_credit_acno': $scope.edt.pd_credit_acno
            , 'pd_debit_acno': $scope.edt.pd_debit_acno
            , 'pd_cash_acno': $scope.edt.pd_cash_acno
            , 'pd_bank_acno': $scope.edt.pd_bank_acno

                }
                data.opr = 'U';
                data1.push(data);
                $http.post(ENV.apiUrl + "api/PayCodeDetails/PayCodeDetailsCUD", data).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        debugger
                        swal({ title: 'Alert', text: "Record Updated Successfully", width: 300, height: 200 });
                    }
                    else {
                        swal({ title: 'Alert', text: "Record Not Updated", width: 300, height: 200 });
                    }
                    $scope.operation = false;
                    $http.get(ENV.apiUrl + "api/PayCodeDetails/getPayCodeDetails").then(function (getPayCodeDetails) {
                        $scope.PayCode_Data = getPayCodeDetails.data;
                        $scope.totalItems = $scope.PayCode_Data.length;
                        $scope.todos = $scope.PayCode_Data;
                        $scope.makeTodos();

                    });
                })

                $scope.operation = false;
                $scope.table1 = true;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                debugger
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('test-' + i);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('test-' + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }


            $scope.deleterecord = function () {
                paycode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById('test-' + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deleteroutecode = ({

                            'pd_grade_code': $scope.filteredTodos[i].pd_grade_code
                        , 'pd_pay_code': $scope.filteredTodos[i].pd_pay_code
                        , 'pd_company_code': $scope.filteredTodos[i].pd_company_code
                        , 'pd_amount': $scope.filteredTodos[i].pd_amount
                        , 'pd_credit_acno': $scope.filteredTodos[i].pd_credit_acno
                        , 'pd_debit_acno': $scope.filteredTodos[i].pd_debit_acno
                        , 'pd_cash_acno': $scope.filteredTodos[i].pd_cash_acno
                        , 'pd_bank_acno': $scope.filteredTodos[i].pd_bank_acno
                           , opr: 'D'
                        });
                        paycode.push(deleteroutecode);
                    }
                }
                debugger
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/PayCodeDetails/PayCodeDetailsD", paycode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/PayCodeDetails/getPayCodeDetails").then(function (getPayCodeDetails) {
                                                $scope.PayCode_Data = getPayCodeDetails.data;
                                                $scope.totalItems = $scope.PayCode_Data.length;
                                                $scope.todos = $scope.PayCode_Data;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/PayCodeDetails/getPayCodeDetails").then(function (getPayCodeDetails) {
                                                $scope.PayCode_Data = getPayCodeDetails.data;
                                                $scope.totalItems = $scope.PayCode_Data.length;
                                                $scope.todos = $scope.PayCode_Data;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                main.checked = false;
                $scope.currentPage = true;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.PayCode_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.PayCode_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                     item.company_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.pd_grade_code_nm.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.pd_pay_code_nm.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.cp_display_order == toSearch ||
                     item.pc_code == toSearch) ? true : false;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

        }])
})();
