﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AirFareCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.edt = {};
            $http.post(ENV.apiUrl + "api/common/AirFare/grade").then(function (grade) {
                $scope.grade = grade.data;
            });

            $http.post(ENV.apiUrl + "api/common/AirFare/nations").then(function (nations) {
                $scope.nations = nations.data;
            });

            $scope.selectnat = function (str) {
                $http.post(ENV.apiUrl + "api/common/AirFare/destination?nationcode=" + str).then(function (destination) {
                    $scope.destination = destination.data;
                });
            }

            $http.post(ENV.apiUrl + "api/common/AirFare/Classget").then(function (Classget) {
                $scope.Classget = Classget.data;
            });

            $http.post(ENV.apiUrl + "api/common/AirFare/applicableMonth").then(function (applicableMonth) {
                $scope.applicableMonth = applicableMonth.data;
            });

            $http.post(ENV.apiUrl + "api/common/AirFare/accountName").then(function (accountName) {
                $scope.accountName = accountName.data;
            });
            $scope.busy = true;
            $scope.rgvtbl = false;
            $http.post(ENV.apiUrl + "api/common/AirFare/AirFareDetails").then(function (AirFareDetails) {
                $scope.AirFareDetails = AirFareDetails.data;
                $scope.busy = false;
                $scope.rgvtbl = true;
            });


            $scope.delete_data = function (str) {

                var data = {
                    gr_code: str.gr_code,
                    ds_code: str.ds_code,
                    nation_code: str.nation_code
                }

                debugger;
                $http.post(ENV.apiUrl + "api/common/AirFare/AirFaredelete", data).then(function (AirFaredelete) {
                    $scope.AirFaredelete = AirFaredelete.data;
                    if ($scope.AirFaredelete)
                        swal('', 'Record deleted successfully.');
                    else
                        swal('', 'Record not deleted.');
                    $scope.busy = true;
                    $scope.rgvtbl = false;
                    $http.post(ENV.apiUrl + "api/common/AirFare/AirFareDetails").then(function (AirFareDetails) {
                        $scope.AirFareDetails = AirFareDetails.data;
                        $scope.busy = false;
                        $scope.rgvtbl = true;
                    });
                });



            }


            $scope.select_chk_upgrade = function () {
                debugger;

            }

            $scope.save_data = function () {
                debugger;
                var upgrade_after = '', upgrade_freq = '';

                if ($scope.edt.upgrade_flag == undefined) {
                    $scope.edt.upgrade_flag = false;
                }

                if ($scope.edt.upgrade_flag) {
                    upgrade_after = $scope.edt.update_after;
                    upgrade_freq = $scope.edt.update_freq;
                }

                if ((upgrade_after == '' || upgrade_freq == '' || upgrade_after == undefined || upgrade_freq == undefined) && $scope.edt.upgrade_flag)
                    swal('', 'Upgrade after and upgrade frequence fields are mandatory.');
                else {



                    if ($scope.edt.gr_code == '' || $scope.edt.gr_code == undefined ||
                    $scope.edt.nation_code == '' || $scope.edt.nation_code == undefined ||
                    $scope.edt.ds_code == '' || $scope.edt.ds_code == undefined ||
                    $scope.edt.af_class == '' || $scope.edt.af_class == undefined ||
                    $scope.edt.af_applicable_frequency == '' || $scope.edt.af_applicable_frequency == undefined ||
                    $scope.edt.af_adult_rate == '' || $scope.edt.af_adult_rate == undefined ||
                    $scope.edt.pays_month_code == '' || $scope.edt.pays_month_code == undefined ||
                    $scope.edt.af_gl_acct_no == '' || $scope.edt.af_gl_acct_no == undefined) {
                        swal('', 'All fields are mandatory.');
                    }
                    else {
                        var data = {
                            gr_code: $scope.edt.gr_code,
                            ds_code: $scope.edt.ds_code,
                            nation_code: $scope.edt.nation_code,
                            af_class: $scope.edt.af_class,
                            af_applicable_frequency: $scope.edt.af_applicable_frequency,
                            af_adult_rate: $scope.edt.af_adult_rate,
                            af_applicable_month: $scope.edt.pays_month_code,
                            upgrade_flag: $scope.edt.upgrade_flag,
                            update_after: upgrade_after,
                            update_freq: upgrade_freq,
                            af_payroll_flag: $scope.edt.af_payroll_flag,
                            af_gl_acct_no: $scope.edt.af_gl_acct_no
                        }

                        debugger;
                        $http.post(ENV.apiUrl + "api/common/AirFare/insertAirFare", data).then(function (insertAirFare) {
                            $scope.insertAirFare = insertAirFare.data;
                            if ($scope.insertAirFare)
                                swal('', 'Record insert successfully.');
                            else
                                swal('', 'Record not insert.');

                            $scope.busy = true;
                            $scope.rgvtbl = false;
                            $http.post(ENV.apiUrl + "api/common/AirFare/AirFareDetails").then(function (AirFareDetails) {
                                $scope.AirFareDetails = AirFareDetails.data;
                                $scope.busy = false;
                                $scope.rgvtbl = true;
                            });
                        });
                        $scope.reset_data();

                       
                    }
                }





            }

            $scope.reset_data = function () {
                $scope.edt = {};
            }


            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });

            }, 100);
        }])
})();