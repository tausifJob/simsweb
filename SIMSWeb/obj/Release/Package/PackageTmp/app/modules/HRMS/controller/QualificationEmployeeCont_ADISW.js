﻿
(function () {
    'use strict';
    var temp, obj1, opr, popobj, data1;
    var modulecode = [], datasend = [];
    var main;

    var simsController = angular.module('sims.module.HRMS');

    simsController.directive("limitTo", [function () {
        return {
            restrict: "A",
            link: function (scope, elem, attrs) {
                var limit = parseInt(attrs.limitTo);
                angular.element(elem).on("keydown", function () {
                    if (event.keyCode > 47 && event.keyCode < 127) {
                        if (this.value.length == limit)
                            return false;
                    }
                });
            }
        }
    }]);

    simsController.directive('thead', function () {
        return {
            restrict: 'A',
            scope: {
                ngModel: '='
            },
            link: function (scope, elem) {
                window.onscroll = function () {
                    elem.floatThead({
                        scrollingTop: 45,
                    });
                    elem.floatThead('reflow');
                }
            }
        };
    })

    simsController.controller('QualificationEmployeeADISWCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.grid = true;
            $scope.display = false;
            $scope.editmode = false;
            $scope.popobj = temp;
            $scope.qualiList = [];
            $scope.temp = {};
            $scope.popobj = {};

            $http.get(ENV.apiUrl + "api/Qualification/getAllQualification?data=" + JSON.stringify(data1)).then(function (res1) {
                $scope.obj1 = res1.data;
                $scope.totalItems = $scope.obj1.length;
                $scope.todos = $scope.obj1;
                $scope.makeTodos();
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ "left": 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ "left": 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2015&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;
                $scope.temp.emp_qual_company_code = $scope.ComboBoxValues[0].fins_comp_code;
                $scope.popobj.emp_qual_company_code = $scope.ComboBoxValues[0].fins_comp_code;
                console.log($scope.ComboBoxValues);
                $scope.getEmp($scope.ComboBoxValues[0].fins_comp_code);
            });

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $http.get(ENV.apiUrl + "api/Qualification/getAllEmployeeName").then(function (res1) {
                $scope.empname = res1.data;
            });

            $http.get(ENV.apiUrl + "api/Qualification/getQualificationType").then(function (qualtype) {
                $scope.qual_type_data = qualtype.data;
            });

            //$scope.getEmp = function (deptcode, comcode) {
            //    $http.get(ENV.apiUrl + "api/Qualification/getEmployeeName?em_dept_code=" + deptcode + "&em_company_code=" + comcode).then(function (res1) {
            //        $scope.display = true;
            //        $scope.empname = res1.data;
            //        console.log($scope.empname);
            //    });
            //}
            $scope.getEmp = function (comcode) {
                $http.get(ENV.apiUrl + "api/Qualification/getEmployeeName?em_company_code=" + comcode).then(function (res1) {
                    //$scope.display = true;
                    $scope.empname = res1.data;
                    console.log($scope.empname);
                });
            }

            $scope.AddEnable = function () { $scope.addDisabled = false; }

            $scope.addCol = function (str) {

                $scope.combo_multiple = true;
                var t = document.getElementById("qual_name");
                var selectedText = t.options[t.selectedIndex].text;

                for (var i = 0; i < $scope.qualiList.length; i++) {
                    if ($scope.qualiList[i].emp_qual_qual_code == str) {
                        swal({ text: 'Qualification Already Added !!', timer: 5000 });
                        return false;
                    }
                }

                var data = {
                    'emp_qual_qual_name': selectedText,
                    'emp_qual_qual_code': str
                }
                $scope.qualiList.push(data);

                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            $scope.Removerow = function ($event, index, str) {
                str.splice(index, 1);
            }

            $scope.savedata = function (Myform) {

                var demo = [];
                var demo1 = [];
                if (Myform) {

                    if ($scope.qualiList.length == 0) {
                        swal({ text: 'Please Add Qualification', timer: 5000 });
                        return false;
                    }

                    for (var i = 0; i < $scope.qualiList.length; i++) {
                        modulecode = $scope.qualiList[i].emp_qual_qual_code;
                        var data = {
                            'emp_qual_em_code': $scope.popobj.emp_qual_em_code,
                            'emp_qual_qual_code': modulecode,
                            'emp_qual_institute': $scope.qualiList[i].emp_qual_institute,
                            'emp_qual_type_code': $scope.qualiList[i].emp_qual_type_code,
                            'em_qual_from_year': $scope.qualiList[i].em_qual_from_year,
                            'em_qual_year': $scope.qualiList[i].em_qual_to_year,
                            'emp_qual_percentage': $scope.qualiList[i].emp_qual_percentage,
                            'emp_qual_remark': $scope.qualiList[i].emp_qual_remark,
                            'em_qual_status': $scope.qualiList[i].em_qual_status,
                            'opr': 'I'
                        }
                        demo1.push(data);

                    }
                    console.log(demo1);
                    console.log("module code", modulecode);

                    $http.post(ENV.apiUrl + "api/Qualification/CUDQualification?year=", demo1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        swal({ text: $scope.msg1.strMessage, timer: 5000 });
                        $scope.display = false;
                        //var data1 = {
                        //    emp_qual_company_code: $scope.popobj.emp_qual_company_code,
                        //    emp_qual_dept_code: $scope.popobj.emp_qual_dept_code,
                        //}
                        $http.get(ENV.apiUrl + "api/Qualification/getAllQualification?data=" + JSON.stringify(data1)).then(function (res1) {
                            $scope.obj1 = res1.data;
                            $scope.totalItems = $scope.obj1.length;
                            $scope.todos = $scope.obj1;
                            $scope.makeTodos();
                        });
                        demo = [];
                        demo1 = [];
                        $scope.grid = true;
                        $scope.display = false;
                        $scope.temp.emp_name_id = '';
                    })

                }
            }

            $scope.update = function () {
                var demo1 = [];
                for (var i = 0; i < $scope.qualiList.length; i++) {
                    modulecode = $scope.qualiList[i].emp_qual_qual_code;
                    var data = {
                        'emp_qual_em_code': $scope.popobj.emp_qual_em_code,
                        'emp_qual_qual_code': modulecode,

                        'emp_qual_institute': $scope.qualiList[i].emp_qual_institute,
                        'emp_qual_type_code': $scope.qualiList[i].emp_qual_type_code,
                        'em_qual_from_year': $scope.qualiList[i].em_qual_from_year,

                        'em_qual_year': $scope.qualiList[i].em_qual_to_year,
                        'emp_qual_percentage': $scope.qualiList[i].emp_qual_percentage,
                        'emp_qual_remark': $scope.qualiList[i].emp_qual_remark,
                        'em_qual_status': $scope.qualiList[i].em_qual_status,
                        'opr': 'U'
                    }
                    demo1.push(data);
                }

                console.log(demo1);

                $http.post(ENV.apiUrl + "api/Qualification/UpadateQualification?simsobj2=", demo1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    swal({ text: $scope.msg1.strMessage, timer: 5000 });
                    $scope.display = false;
                    var data = {
                        emp_qual_qual_code: $scope.popobj.emp_qual_qual_code1,
                        emp_qual_dept_code: $scope.popobj.emp_qual_dept_code1,
                        emp_qual_company_code: $scope.popobj.emp_qual_company_code1
                    }
                    $http.get(ENV.apiUrl + "api/Qualification/getAllQualification?data=" + JSON.stringify(data)).then(function (res1) {
                        $scope.obj1 = res1.data;
                        $scope.totalItems = $scope.obj1.length;
                        $scope.todos = $scope.obj1;
                        $scope.makeTodos();
                    });
                    $scope.grid = true;
                    $scope.display = false;
                })

            }

            $scope.GetData = function () {

                $http.get(ENV.apiUrl + "api/Qualification/getAllQualification?data=" + JSON.stringify($scope.temp)).then(function (res1) {
                    $scope.obj1 = res1.data;
                    $scope.totalItems = $scope.obj1.length;
                    $scope.todos = $scope.obj1;
                    $scope.makeTodos();
                });
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj1, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj1;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.emp_qual_dept_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.emp_qual_em_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.emp_qual_qual_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.emp_qual_remark.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.em_qual_year == toSearch) ? true : false;
            }

            $scope.edit = function (str) {
                debugger;
                $scope.popobj = {
                    'emp_qual_qual_code': str.emp_qual_qual_code,
                    'emp_qual_qual_name': str.emp_qual_qual_name,
                    'emp_qual_company_code': str.emp_qual_company_code,
                    'emp_qual_company_name': str.emp_qual_company_name,
                    'emp_qual_dept_code': str.emp_qual_dept_code,
                    'emp_qual_dept_name': str.emp_qual_dept_name,
                    'emp_qual_em_code': str.emp_qual_em_code,
                    'emp_qual_em_name': str.emp_qual_em_name,
                    'emp_qual_institute': str.emp_qual_institute,
                    'emp_qual_type_code': str.emp_qual_type_code,
                    'em_qual_from_year': str.em_qual_from_year,
                    'emp_qual_percentage': str.emp_qual_percentage,
                    'em_qual_status': str.em_qual_status,
                    'em_qual_to_year': str.em_qual_to_year,
                    'emp_qual_remark': str.emp_qual_remark
                }
                $scope.getQualiByEmpcode($scope.popobj.emp_qual_em_code);
                $scope.grid = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.qualOnEdit = false;
                $scope.addBtnOnEdit = false;
            }

            $scope.getQualiByEmpcode = function (empcode) {
                $http.get(ENV.apiUrl + "api/Qualification/getQualificationByEmployee?emp_code=" + empcode).then(function (res1) {
                    $scope.qualiList = res1.data;
                });
                $scope.combo_multiple = true;
            }


            $scope.onKeyPress = function ($event) {
                if ($event.keyCode == 13) {
                    $scope.GetData();
                }
            };

            $scope.clear = function () {
                $scope.temp.emp_qual_dept_code = '';
                $scope.temp.emp_qual_qual_code = '';
                $scope.temp.emp_name_id = '';
                $scope.popobj = "";
            }

            $scope.Cancel = function () {
                $scope.display = false;
                $scope.grid = true;
                $scope.temp.emp_qual_dept_code = '';
                $scope.temp.emp_qual_qual_code = '';
                $scope.temp.emp_name_id = '';
            }

            $scope.formopen = function () {
                $scope.popobj["emp_qual_dept_code"] = '';
                $scope.popobj["emp_qual_em_code"] = '';
                $scope.popobj["emp_qual_qual_code"] = '';
                $scope.qualiList = [];
                $scope.qualOnEdit = true;
                $scope.addBtnOnEdit = true;
                $scope.qual_rmk_readonly = false;
                $scope.qual_yr_readonly = false;
                $scope.addDisabled = true;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.Quali = '';
                $scope.disabled = false;
                $scope.chkdisabled = true;
                $scope.grid = false;
                $scope.Update_btn = false;
                $scope.save_btn = true;
                $scope.display = true;
                $scope.combo_single = false;
                $scope.combo_multiple = false;
                $scope.popobj["em_qual_status"] = true;
                $scope.Qualification_Name = '';
                $http.get(ENV.apiUrl + "api/Qualification/getQualificationName").then(function (QualificationName) {
                    $scope.Qualification_Name = QualificationName.data;
                })
            }
        }])
})();
