﻿(function () {
    'use strict';
    var opr = '';
    var LoanAmountcode = [];
    var main;
    var data1 = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('LoanAmountCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.table1 = true;

            $http.get(ENV.apiUrl + "api/LoanAmountDetails/getLoanAmount").then(function (getLoanAmount_Data) {
                $scope.LoanAmount_Data = getLoanAmount_Data.data;
                $scope.totalItems = $scope.LoanAmount_Data.length;
                $scope.todos = $scope.LoanAmount_Data;
                $scope.makeTodos();
                console.log($scope.LoanAmount_Data);
            });

            $http.get(ENV.apiUrl + "api/common/getCompanyName").then(function (getCompanyName_Data) {
                $scope.CompanyName_Data = getCompanyName_Data.data;
            });

            $scope.GetLoandesc = function () {
                $http.get(ENV.apiUrl + "api/common/getAllLoanDesc?Company_code=" + $scope.edt.la_company_code).then(function (getAllLoanDesc_Data) {
                    $scope.AllLoanDesc_Data = getAllLoanDesc_Data.data;
                    console.log($scope.AllLoanDesc_Data);
                });
                debugger;
                $http.get(ENV.apiUrl + "api/common/getGradeDesc?Company_code=" + $scope.edt.la_company_code).then(function (getGradeDesc_Data) {
                    $scope.Grade_Desc_Data = getGradeDesc_Data.data;
                    console.log($scope.Grade_Desc_Data);
                });
            }

            $http.get(ENV.apiUrl + "api/common/getAllLedger").then(function (getAllLedger_Data) {
                $scope.AllLedger_Data = getAllLedger_Data.data;
                console.log($scope.AllLedger_Data);
            });

            $http.get(ENV.apiUrl + "api/common/getCreditDebit").then(function (getCreditDebit_Data) {
                $scope.CreditDebit_Data = getCreditDebit_Data.data;
            });

            $scope.operation = false;
            $scope.editmode = false;
            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }
            $scope.edt = "";

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.editmode = false;
                opr = 'S';
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();


            }

            $scope.up = function (str) {
                opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.operation = true;
              //  $scope.edt=str;
              
                $scope.edt = {
                            la_company_code: str.la_company_code
                          , la_no_of_installment: str.la_no_of_installment
                          , la_ledger_code: str.la_ledger_code
                          , la_debit_acno: str.la_debit_acno
                          , la_amount: str.la_amount
                          , la_loan_code: str.la_loan_code
                          , la_grade_code:str.la_grade_code
                    };
              $scope.GetLoandesc(str.la_company_code);
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
            }

            $scope.Save = function (myForm) {
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'I';
                   
                    $scope.exist = false;
                    for (var i = 0; i < $scope.LoanAmount_Data.length; i++) {
                        if ($scope.LoanAmount_Data[i].la_company_code == data.la_company_code && $scope.LoanAmount_Data[i].la_grade_code == data.la_grade_code && $scope.LoanAmount_Data[i].la_loan_code == data.la_loan_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ title: "Alert", text: "Record Already exists", width: 300, height: 200 });
                    }
                    else {
                    data1.push(data);
                    
                        $http.post(ENV.apiUrl + "api/LoanAmountDetails/LoanAmountCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            $scope.operation = false;
                            $http.get(ENV.apiUrl + "api/LoanAmountDetails/getLoanAmount").then(function (getLoanAmount_Data) {
                                $scope.LoanAmount_Data = getLoanAmount_Data.data;
                                $scope.totalItems = $scope.LoanAmount_Data.length;
                                $scope.todos = $scope.LoanAmount_Data;
                                $scope.makeTodos();

                                $scope.msg1 = msg.data;

                                if ($scope.msg1 == true) {

                                    swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });

                                }
                                else {

                                    swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });

                                }

                            });

                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                    data1 = [];
                }

            }

            $scope.Update = function (myForm) {
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'U';
                    $scope.edt = "";
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/LoanAmountDetails/LoanAmountCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;

                        $scope.operation = false;
                        $http.get(ENV.apiUrl + "api/LoanAmountDetails/getLoanAmount").then(function (getLoanAmount_Data) {
                            $scope.LoanAmount_Data = getLoanAmount_Data.data;
                            $scope.totalItems = $scope.LoanAmount_Data.length;
                            $scope.todos = $scope.LoanAmount_Data;
                            $scope.makeTodos();

                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                            }
                        });
                        data1 = [];
                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.deleterecord = function () {
                
                LoanAmountcode = [];
                swal({
                    title: "Are you sure?", text: "to Delete Record", width: 300, height: 200, showCancelButton: true,
                    confirmButtonColor: "#DD6B55", confirmButtonText: "Yes", closeOnConfirm: false
                },
                
                     function (isConfirm) {
                         if (isConfirm) {
                             
                             for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                 var v = document.getElementById(i);
                                 if (v.checked == true) {
                                     var deletelocacode = ({
                                         'la_grade_code': $scope.filteredTodos[i].la_grade_code,
                                         'la_company_code': $scope.filteredTodos[i].la_company_code,
                                         'la_loan_code': $scope.filteredTodos[i].la_loan_code,
                                         opr: 'D'
                                     });
                                     LoanAmountcode.push(deletelocacode);
                                 }
                             }
                             $http.post(ENV.apiUrl + "api/LoanAmountDetails/LoanAmountCUD", LoanAmountcode).then(function (msg) {
                                 $scope.msg1 = msg.data;
                                 if ($scope.msg1 == true) {
                                     swal({ title: "Alert", text: "Record Deleted Successfully", width: 300, height: 200 });
                                     $http.get(ENV.apiUrl + "api/LoanAmountDetails/getLoanAmount").then(function (getLoanAmount_Data) {
                                         $scope.LoanAmount_Data = getLoanAmount_Data.data;
                                         $scope.totalItems = $scope.LoanAmount_Data.length;
                                         $scope.todos = $scope.LoanAmount_Data;
                                         $scope.makeTodos();
                                     });
                                 }
                                 else {

                                     swal({ title: "Alert", text: "Record Not Deleted", width: 300, height: 200 });
                                 }
                             });
                             $scope.table1 = true;
                         }
                     });
                $scope.currentPage = '';
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.LoanAmount_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.LoanAmount_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                    item.gradeName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.companyName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.loanName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.la_amount.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.la_no_of_installment == toSearch) ? true : false;
                     
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);
        }])
})();
