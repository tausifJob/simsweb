﻿(function () {
    'use strict';
    var CurrentDate;
    var day;
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmpDashboardCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {


            var total = 0; var Ltotal = 0; var tot, Ltot; var remain = 0; var tremain; var maxdays;
            var username = $rootScope.globals.currentUser.username;
            $scope.LeaveTaken = 0;
            //Emp Attendance
            $scope.CurrentDate = new Date();
            var today = new Date();
            // $scope.day = CurrentDate.getDay();
            //var d = new Date();
            //var n = d.getDay();
            var grade;
            var section;
            $scope.records = true;
            $scope.tabledata = false;





            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable3").tableHeadFixer({ 'top': 1 });
            }, 100);
            $timeout(function () {
                $("#fixTable2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.getStaffDetails = function () {
                $('#MyModal3').modal('show');
                debugger;
                $http.get(ENV.apiUrl + "api/common/Empdashboard/getStaffPersonalData").then(function (getStaff) {
                    $scope.staff = getStaff.data;
                });
            }

            $scope.getResignedStaffDetails = function () {
                $('#MyModal4').modal('show');
                debugger;
                $http.get(ENV.apiUrl + "api/common/Empdashboard/getResignedstaffData").then(function (getRegStaff) {
                    $scope.Resignedstaff = getRegStaff.data;
                });
            }

            //WorldMAp API CONTROLLER
            $http.get(ENV.apiUrl + "api/common/Empdashboard/GetCountryCountData").then(function (res) {
                $scope.country_count = res.data;
                console.log($scope.country_count);

                $timeout(function () {

                    var lic_lat = 21.524627;
                    var lic_long = 77.871094;

                    var location = new google.maps.LatLng(lic_lat, lic_long);
                    var mapOptions = {
                        zoom: 2,
                        center: location,
                        mapTypeId: google.maps.MapTypeId.TERRAIN
                    }

                    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

                    var infowindow = new google.maps.InfoWindow();
                    var marker, i;

                    for (i = 0; i < $scope.country_count.length; i++) {

                        var marker = new google.maps.Marker({
                            map: map,
                            position: new google.maps.LatLng($scope.country_count[i].sims_country_latitude, $scope.country_count[i].sims_country_longitude),
                            title: $scope.country_count[i].sims_country_name_en + " Boys-" + $scope.country_count[i].male_student_count + " Girls-" + $scope.country_count[i].female_student_count,

                        });

                        debugger;

                        google.maps.event.addListener(marker, 'click', (function (marker, i) {

                            return function () {
                                infowindow.setContent($scope.country_count[i].sims_country_name_en + ' Boys-' + $scope.country_count[i].male_student_count + ' Girls-' + $scope.country_count[i].female_student_count);
                                infowindow.open(map, marker);
                            }
                            $scope.piechartsDataShow();
                        })(marker, i));



                    }

                }, 500)



            });

            $http.get(ENV.apiUrl + "api/common/Empdashboard/getHRMSData").then(function (gethrresult) {
                $scope.hrms_data = gethrresult.data;


                $scope.chartLabels = [];
                $scope.chartData = [];
                $scope.chartColor = [];

                var pieChartData = [];

                for (var i = 0; i < $scope.hrms_data.length; i++) {
                    var color = Math.floor(Math.random() * 16777216).toString(16);
                    color: '#000000'.slice(0, -color.length) + color;
                    $scope.hrms_data[i].color = color;

                    $scope.chartLabels.push($scope.hrms_data[i].visa_type);
                    $scope.chartData.push(parseInt($scope.hrms_data[i].visa_type_count));
                    $scope.chartColor.push($scope.hrms_data[i].color);
                }


            });

            $http.get(ENV.apiUrl + "api/common/Empdashboard/getStudentData").then(function (getstud_data) {
                debugger;
                $scope.getstudent_data = getstud_data.data;

                $scope.labels = [];
                $scope.data = [];
                // $scope.value = [];

                var doughnutData = [];

                for (var i = 0; i < $scope.getstudent_data.length; i++) {
                    $scope.labels.push($scope.getstudent_data[i].gender);
                    $scope.data.push(parseInt($scope.getstudent_data[i].gender_count));
                    // $scope.value.push($scope.hrms_data[i].gender_code)
                }

                $scope.grid = true;

            });

            $http.get(ENV.apiUrl + "api/common/Empdashboard/getStaffDivisionCount").then(function (div_count) {
                $scope.divcount = div_count.data;
                $scope.chartLabels1 = [];
                $scope.chartData1 = [];
                $scope.chartColor1 = [];
                var pieChartData1 = [];
                for (var i = 0; i < $scope.divcount.length; i++) {
                    var color = Math.floor(Math.random() * 16777216).toString(16);
                    color: '#000001'.slice(0, -color.length) + color;
                    $scope.divcount[i].color = color;

                    $scope.chartLabels1.push($scope.divcount[i].gd_division_desc);
                    $scope.chartData1.push(parseInt($scope.divcount[i].staff_count));
                    $scope.chartColor1.push($scope.divcount[i].color);
                }

            });

            $http.get(ENV.apiUrl + "api/common/Empdashboard/getStaffDepartmentCount").then(function (div_count) {
                $scope.divcount = div_count.data;
                $scope.chartLabels2 = [];
                $scope.chartData2 = [];
                $scope.chartColor2 = [];
                var pieChartData2 = [];
                for (var i = 0; i < $scope.divcount.length; i++) {
                    var color = Math.floor(Math.random() * 16777216).toString(16);
                    color: '#000001'.slice(0, -color.length) + color;
                    $scope.divcount[i].color = color;

                    $scope.chartLabels2.push($scope.divcount[i].department);
                    $scope.chartData2.push(parseInt($scope.divcount[i].staff_count));
                    $scope.chartColor2.push($scope.divcount[i].color);
                }

            });



            //$http.get(ENV.apiUrl + "api/common/Empdashboard/GetCountryCountData").then(function (res) {
            //    $scope.country_count = res.data;
            //    console.log($scope.country_count);

            //    $scope.$timeout(function () {
            //        var lic_lat = 21.524627;
            //        var lic_long = 77.871094;

            //        var location = new google.maps.LatLng(lic_lat, lic_long);
            //        var mapOptions = {
            //            zoom: 2,
            //            center: location,
            //            mapTypeId: google.maps.MapTypeId.TERRAIN
            //        }

            //        var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

            //        var infowindow = new google.maps.InfoWindow();
            //        var marker, i;

            //        for (i = 0; i < $scope.country_count.length; i++) {

            //            var marker = new google.maps.Marker({
            //                map: map,
            //                position: new google.maps.LatLng($scope.country_count[i].sims_country_latitude, $scope.country_count[i].sims_country_longitude),
            //                title: $scope.country_count[i].sims_country_name_en + " Boys-" + $scope.country_count[i].male_student_count + " Girls-" + $scope.country_count[i].female_student_count,

            //            });

            //            google.maps.event.addListener(marker, 'click', (function (marker, i) {

            //                return function () {
            //                    infowindow.setContent($scope.country_count[i].sims_country_name_en + ' Boys-' + $scope.country_count[i].male_student_count + ' Girls-' + $scope.country_count[i].female_student_count);
            //                    infowindow.open(map, marker);
            //                }
            //                $scope.piechartsDataShow();
            //            })(marker, i));
            //        }
            //    },500)

            //});



            $http.get(ENV.apiUrl + "api/common/Empdashboard/GetCountryCountTotal").then(function (result) {
                $scope.total_counts = result.data;
                console.log($scope.total_counts);
            });
            //


            //New API
            $http.get(ENV.apiUrl + "api/common/Empdashboard/GetDashboardData").then(function (res1) {
                debugger;
                $scope.portal_data = res1.data;
                console.log($scope.portal_data);

                if (res1.data.length > 0) {
                    $scope.portal_data = res1.data;
                }
                else {
                    $scope.portal_data_flg = '1'
                    $scope.portal_data.push({})
                }
                console.log($scope.portal_data)
            });



            $scope.leaveDesc = 'View Leave Balance';
            var d = new Date();
            var weekday = new Array(7);
            weekday[0] = "Sunday";
            weekday[1] = "Monday";
            weekday[2] = "Tuesday";
            weekday[3] = "Wednesday";
            weekday[4] = "Thursday";
            weekday[5] = "Friday";
            weekday[6] = "Saturday";
            var n = weekday[d.getDay()];
            $scope.day = n;

            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            var yyyy = today.getFullYear();
            var att_date = yyyy + '-' + mm + '-' + dd;
            var cur_year = yyyy;
            var Co_Code = '1';
            $scope.record = false;
            var cur_date1 = dd + '-' + mm + '-' + yyyy

            $scope.cur_date = cur_date1;

            $scope.stud_att_date = att_date;

            $http.get(ENV.apiUrl + "api/common/Empdashboard/GetAllDetailsEmp?curr_date=" + att_date).then(function (res) {
                debugger;
                $scope.all_data_emp = res.data;
                $scope.present = $scope.all_data_emp[0].presents;
                $scope.abs = $scope.all_data_emp[0].absents;
                $scope.lvwi = $scope.all_data_emp[0].leavewithougthallowns;
                $scope.lev = $scope.all_data_emp[0].leave;
                $scope.um = $scope.all_data_emp[0].unmark;
                $scope.jtot = $scope.all_data_emp[0].total;

            });

            $scope.presentflag = function () {
                debugger

                $http.get(ENV.apiUrl + "api/common/Empdashboard/GetAllPerEmpDet?curr_date=" + att_date + "&status=" + 'P').then(function (res) {
                    debugger;
                    $scope.all_data_emp_perticular = res.data;
                    $scope.all_data_emp_perticular1 = res.data;
                    if ($scope.all_data_emp_perticular1.length > 0) {
                        $('#MyModal2').modal('show');
                        $scope.tabledata = true;
                    }
                    else {
                        swal({ title: "Alert", text: "Data not Availiable", width: 300, height: 200 });
                    }

                });

            }

            $scope.absentflag = function () {
                $scope.all_data_emp_perticular = [];
                debugger
                $http.get(ENV.apiUrl + "api/common/Empdashboard/GetAllPerEmpDet?curr_date=" + att_date + "&status=" + 'A').then(function (res) {
                    debugger;
                    $scope.all_data_emp_perticular = res.data;
                    $scope.all_data_emp_perticular2 = res.data;
                    if ($scope.all_data_emp_perticular2.length > 0) {
                        $('#MyModal2').modal('show');
                        $scope.tabledata = true;
                    }
                    else {
                        swal({ title: "Alert", text: "Data not Availiable", width: 300, height: 200 });
                    }

                });


            }

            $scope.unmarkflag = function () {
                $scope.all_data_emp_perticular = [];
                debugger
                $http.get(ENV.apiUrl + "api/common/Empdashboard/GetAllPerEmpDet?curr_date=" + att_date + "&status=" + 'UM').then(function (res) {
                    debugger;
                    $scope.all_data_emp_perticular = res.data;
                    $scope.all_data_emp_perticular3 = res.data;
                    if ($scope.all_data_emp_perticular3.length > 0) {
                        $('#MyModal2').modal('show');
                        $scope.tabledata = true;
                    }
                    else {

                        swal({ title: "Alert", text: "Data not Availiable", width: 300, height: 200 });
                    }

                });

            }

            $scope.leavewithoutallws = function () {
                $scope.all_data_emp_perticular = [];
                debugger
                $http.get(ENV.apiUrl + "api/common/Empdashboard/GetAllPerEmpDet?curr_date=" + att_date + "&status=" + 'W').then(function (res) {
                    debugger;
                    $scope.all_data_emp_perticular = res.data;
                    $scope.all_data_emp_perticular4 = res.data;
                    if ($scope.all_data_emp_perticular4.length > 0) {
                        $('#MyModal2').modal('show');
                        $scope.tabledata = true;
                    }
                    else {

                        swal({ title: "Alert", text: "Data not Availiable", width: 300, height: 200 });
                    }
                });

            }

            $scope.leaveflag = function () {
                $scope.all_data_emp_perticular = [];
                debugger
                $http.get(ENV.apiUrl + "api/common/Empdashboard/GetAllPerEmpDet?curr_date=" + att_date + "&status=" + 'L').then(function (res) {
                    debugger;
                    $scope.all_data_emp_perticular = res.data;
                    $scope.all_data_emp_perticular5 = res.data;

                    if ($scope.all_data_emp_perticular5.length > 0) {
                        $('#MyModal2').modal('show');
                        $scope.tabledata = true;
                    }
                    else {
                        swal({ title: "Alert", text: "Data not Availiable", width: 300, height: 200 });
                    }
                });

            }

            $http.get(ENV.apiUrl + "api/common/Empdashboard/Getdivisionmale?co_code=" + Co_Code).then(function (res) {
                debugger;
                $scope.division = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/Empdashboard/getEmpAttendance?emp_att_date=" + att_date + "&emp_att_emp_id=" + username).then(function (empatten) {
                debugger;
                $scope.emp_atten = empatten.data;
                console.log($scope.emp_atten);
                $scope.user_name = $scope.emp_atten[0].em_first_name;
                $scope.check_in = $scope.emp_atten[0].em_check_in;
                $scope.check_out = $scope.emp_atten[0].em_check_out;
                //if ($scope.emp_atten[0].emp_atten_flag == 'Y') {
                //    $scope.att_status = 'Present';
                //}
                //else if ($scope.emp_atten[0].emp_atten_flag == 'H') {
                //    $scope.att_status = 'Holiday';
                //}
                //else if ($scope.emp_atten[0].emp_atten_flag == 'L') {
                //    $scope.att_status = 'Leave'
                //}
                //else { $scope.att_status = 'Weekend' }


            });

            var v = '0.00';
            $scope.emp_absent_stud_percentage = v;

            $scope.demo = [];

            $http.get(ENV.apiUrl + "api/common/Empdashboard/getEmpGradePercentage?&emp_att_emp_id=" + username + "&att_date_temp=" + att_date).then(function (empstgrade) {
                $scope.emp_grade = empstgrade.data;

                if ($scope.emp_grade.length > 0) {
                    $timeout(function () {
                        for (var i = 0 ; i < $scope.emp_grade.length; i++) {
                            var name = '#test1' + i;
                            $(name).easyPieChart({
                                lineWidth: 10,
                                height: 50,
                                barColor: 'red',
                                trackColor: '#e5e9ec',
                                scaleColor: false
                            });
                        }
                    }, 100);

                    $timeout(function () {
                        for (var i = 0 ; i < $scope.emp_grade.length; i++) {
                            var name = '#test2' + i;
                            $(name).easyPieChart({
                                lineWidth: 10,
                                height: 50,
                                barColor: 'gray',
                                trackColor: '#e5e9ec',
                                scaleColor: false
                            });
                        }
                    }, 100);



                    $timeout(function () {
                        for (var i = 0 ; i < $scope.emp_grade.length; i++) {
                            var name = '#test3' + i;
                            $(name).easyPieChart({
                                lineWidth: 10,
                                height: 50,
                                barColor: 'yellow',
                                trackColor: '#e5e9ec',
                                scaleColor: false
                            });
                        }
                    }, 100);

                    $timeout(function () {
                        for (var i = 0 ; i < $scope.emp_grade.length; i++) {
                            var name = '#test' + i;
                            $(name).easyPieChart({
                                lineWidth: 10,
                                height: 50,
                                barColor: 'green',
                                trackColor: '#e5e9ec',
                                scaleColor: false
                            });
                        }
                    }, 100);
                }
                else {
                    $scope.strMessage = 'Records Not Available';
                }
            });

            $scope.PreAbTardy = function (grade, section, str, stud_att_date) {

                debugger;
                $http.get(ENV.apiUrl + "api/common/Empdashboard/getStudPerEnrollNumber?sims_grade_code=" + grade + "&sims_section_code=" + section + "&code=" + str + "&emp_code=" + username + "&stud_att_date=" + $scope.stud_att_date).then(function (studEnroll) {
                    $scope.stud_Enroll = studEnroll.data;
                    console.log($scope.stud_Enroll);

                    if (str == 'P') {
                        $scope.StudentData = 'Present Student';

                    }
                    else if (str == 'A') {

                        $scope.StudentData = 'Absent Student';
                    }
                    else {

                        $scope.StudentData = 'Trady Student';
                    }

                    $('#MyModal').modal('show');

                });

            }

            $http.get(ENV.apiUrl + "api/common/Empdashboard/getEmpSubjectName?emp_att_emp_id=" + username).then(function (empsub) {

                $scope.emp_sub = empsub.data;
                if ($scope.emp_sub.length == 0) {
                    $scope.one = true;
                    $scope.two = true;

                    $scope.strMessage1 = 'Records Not Found';
                }
                else {
                    $scope.one = false;
                    $scope.two = false;

                }
            });

            $http.get(ENV.apiUrl + "api/common/Empdashboard/getLeaveTotal?att_emp_id=" + username).then(function (leaveTotal) {
                debugger;
                $scope.leave_Total = leaveTotal.data;
                console.log($scope.leave_Total);
                for (var i = 0; i < $scope.leave_Total.length; i++) {
                    Ltotal = parseInt(Ltotal) + parseInt($scope.leave_Total[i].le_max_days_allowed);
                    console.log(Ltotal);
                }
                $scope.Ltot = Ltotal;
            });

            $http.get(ENV.apiUrl + "api/common/Empdashboard/getLeaveType?att_emp_id=" + username).then(function (leaveType) {

                $scope.leave_type = leaveType.data;
                console.log($scope.leave_type);
                if ($scope.username == "admin") {
                    $scope.leave_type = "";
                }

            });

            $scope.getRemainLeave = function (leavemaxdays, leave) {

                for (var i = 0; i < $scope.leave_type.length; i++) {
                    if (leavemaxdays == $scope.leave_type[i].emp_leave_code) {
                        $scope.maxdays = $scope.leave_type[i].le_max_days_allowed;
                        tremain = parseInt($scope.maxdays) - parseFloat($scope.leave_type[i].le_days_taken);
                    }
                }

                $scope.remain = tremain;

                $scope.leaveDesc = leave.emp_leave_name + ' =' + leave.le_max_days_allowed;

            }

            $scope.subclick = function (str) {

                //$http.get(ENV.apiUrl + "api/common/Empdashboard/getEmpgradebook?emp_academic_year=" + cur_year + "&emp_att_emp_id=" + username + "&sub_code=" + str).then(function (empstgradebook) {
                //    $scope.empstgrade_book = empstgradebook.data;
                //});

                $http.get(ENV.apiUrl + "api/common/Empdashboard/getGradeSection?emp_academic_year=" + cur_year + "&emp_att_emp_id=" + username + "&sub_code=" + str).then(function (empstgradebook) {
                    $scope.empstgrade_book = empstgradebook.data;
                });

            }


            $scope.immportStaff = function () {

                var check = true;
                debugger;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " Staff Details.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger;
                            var blob = new Blob([document.getElementById('printdata2').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + " Staff Details" + ".xls");


                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Report Not Save", showCloseButton: true, width: 380, })
                }

            }

            $scope.immportStaffResigned = function () {
                var check = true;
                debugger;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " StaffResignedDetails.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger;
                            var blob = new Blob([document.getElementById('printdata3').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + " StaffResignedDetails" + ".xls");
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Report Not Save", showCloseButton: true, width: 380, })
                }
            }

            $scope.immport = function () {
                //var blob = new Blob([document.getElementById('printdata').innerHTML], {
                //    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                //});
                //saveAs(blob, "EMPInfo.xls");
                var check = true;
                debugger;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " Employee Attendance.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            var blob = new Blob([document.getElementById('printdata').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + " Employee Attendance" + ".xls");
                            //alasql('SELECT * INTO XLSX("Report.xlsx",{headers:true}) \ FROM HTML("#printdata",{headers:true,skipdisplaynone:true})');

                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Report Not Save", showCloseButton: true, width: 380, })
                }

            }

            $scope.print = function (div) {
                debugger;
                var docHead = document.head.outerHTML;
                var printContents = document.getElementById(div).outerHTML;
                var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";

                var newWin = window.open("", "_blank", winAttr);
                var writeDoc = newWin.document;
                writeDoc.open();
                writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                writeDoc.close();
                newWin.focus();
            }


        }])
})();
