﻿(function () {
    'use strict';
    var obj1, obj2, edt, main;
    var opr = '';
    var empdocdetailcode = [];
    var categorycode = [];

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('EmployeeDocumentUploadCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.EmployeeDetail = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.editmode = false;
            var formdata = new FormData();
            var data1 = [];

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {
                $scope.filesize = true;

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);

                    var i = 0;
                    if ($files[i].size > 800000) {
                        $scope.filesize = false;
                        $scope.edt.photoStatus = false;

                        swal({ title: "Alert", text: "File Should Not Exceed 800Kb.", imageUrl: "assets/img/notification-alert.png", });

                    }
                    else {




                    }

                });
            };

            $scope.file_changed = function (element, str) {

                var photofile = element.files[0];

                $scope.photo_filename = (photofile.name);

                $scope.edt1 = { sims_timetable_filename: $scope.photo_filename }
                $.extend($scope.edt, $scope.edt1)
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;

                    });
                };
                reader.readAsDataURL(photofile);

                if (element.files[0].size < 200000) {

                }
            };

            $scope.index = function (str) {
                $scope.pageindex = str;

                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $http.get(ENV.apiUrl + "api/EmpDocumentUp/getEmpDocCode?pays_doc_code").then(function (res) {
                $scope.empcode = res.data;
                console.log($scope.empcode);
            })

            $scope.getempdesc = function (pays_doc_code) {
               
                $http.get(ENV.apiUrl + "api/EmpDocumentUp/getEmpDocCode?pays_doc_code=" +$scope.edt.pays_doc_code).then(function (res) {
                    $scope.empcode = res.data;
                   
                    for (var i = 0; i < $scope.empcode.length; i++)
                    {

                        if ($scope.empcode[i].pays_doc_code == pays_doc_code)
                        {
                            $scope.edt = {
                                            pays_doc_code: $scope.empcode[i].pays_doc_code,
                                            pays_doc_mod_code: $scope.empcode[i].pays_doc_mod_code,
                                            pays_doc_code: $scope.empcode[i].pays_doc_code,
                                         };
                        }
                    }
                })

            };
         
            $http.get(ENV.apiUrl + "api/EmpDocumentUp/getEmpDocumentDetail").then(function (Emp_Doc_Data) {
                $scope.EmpDocData = Emp_Doc_Data.data;
                $scope.totalItems = $scope.EmpDocData.length;
                $scope.todos = $scope.EmpDocData;
                $scope.makeTodos();

                console.log($scope.EmpDocData);
            });

            $http.get(ENV.apiUrl + "api/EmpDocumentUp/getEmpDocumentDetail").then(function (res) {
                $scope.EmployeeDataoperation = false;
                $scope.EmployeeDetail = true;
                $scope.obj = res.data;
                console.log($scope.obj);
            });

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].pays_doc_mod_code;

                        var v = document.getElementById(t + i);
                        v.checked = true;
                        categorycode = categorycode + $scope.filteredTodos[i].pays_doc_mod_code + ','
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].pays_doc_mod_code;
                        var v = document.getElementById(t + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }
            }

            $scope.check_once = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
                else {
                    main.checked = false;
                    $scope.row1 = '';
                }
            }

            $scope.deleterecord = function () {
              
                empdocdetailcode = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].pays_doc_mod_code;
                    var v = document.getElementById(t + i);

                    if (v.checked == true)
                        empdocdetailcode = empdocdetailcode + $scope.filteredTodos[i].pays_doc_mod_code + ',';
                }

                var deleteempdoccode = ({
                    'pays_doc_mod_code': empdocdetailcode,

                    'opr': 'D'
                });

                $http.post(ENV.apiUrl + "api/EmpDocumentUp/EmpDocDetailCUD", deleteempdoccode).then(function (msg) {
                    debugger;
                    $scope.msg1 = msg.data;
                    $scope.EmployeeDetail = true;
                    $scope.EmployeeDataoperation = false;
                    $rootScope.strMessage = $scope.msg1.strMessage;
                    $http.get(ENV.apiUrl + "api/EmpDocumentUp/getEmpDocumentDetail").then(function (get_empdetail) {
                        $scope.Employee_Data = get_empdetail.data;
                        $scope.totalItems = $scope.Employee_Data.length;
                        $scope.todos = $scope.Employee_Data;
                        $scope.makeTodos();

                        if ($scope.msg1 == true) {
                            $rootScope.strMessage = 'Record Deleted Successfully';
                            $('#message').modal('show');
                        }
                        else {

                            $rootScope.strMessage = 'Record Not Deleted';
                            $('#message').modal('show');
                        }
                    });
                });
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.cancel = function () {
                $scope.EmployeeDetail = true;
                $scope.EmployeeDataoperation = false; 
                $scope.SearchEmpList = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
            }
        
            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.newmode = true;
                $scope.check = true;
                //$scope.edt = '';
                $scope.editmode = false;
                opr = 'S';
                $scope.readonly = false;
                $scope.EmployeeDetail = false;
                $scope.EmployeeDataoperation = true;
                $scope.savebtn = true;
                //$scope.newbtn = false;
                $scope.updatebtn = false;
                $scope.uploadbtn = false;
                $scope.edt = {};


            }

            $scope.Save = function (myForm) {
               
                if (myForm) {
                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + 'api/file/uploadDocument?filename=' + $scope.edt.em_first_name + "&location=" + "UploadedFiles",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };

                    $http(request).success(function (d) {

                        var data = $scope.edt;
                        data.opr = 'I';
                        data.em_first_name = $scope.edt.em_first_name;
                        data.em_first_name = d;
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/EmpDocumentUp/EmpDocDetailCUD?simsobj=", data1).then(function (msg) {
                            $scope.EmployeeDataoperation = false;
                            $http.get(ENV.apiUrl + "api/EmpDocumentUp/getEmpDocumentDetail").then(function (EmpDoc_Data) {
                                $scope.EmpDocData = EmpDoc_Data.data;
                                $scope.totalItems = $scope.EmpDocData.length;
                                $scope.todos = $scope.EmpDocData;
                                $scope.makeTodos();

                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    $rootScope.strMessage = 'Record Inserted Successfully';
                                    $('#message').modal('show');
                                }
                                else {
                                    $rootScope.strMessage = 'Record Not Inserted';
                                    $('#message').modal('show');
                                }
                            });
                        });
                    });

                    $scope.EmployeeDetail = true;
                    $scope.EmployeeDataoperation = false;
                }
            }

            $scope.searchempdata = function () {
                $scope.searchemp = false;
                $scope.SearchEmpList = true;
                $('#SearchEmpList').modal('show');
            }

            $scope.SearchEmployee = function () {
                $scope.searchemp = true;

                $http.post(ENV.apiUrl + "api/EmpDocumentUp/searchEmployeeList").then(function (Employee_Data) {
                    $scope.EmployeeData = Employee_Data.data;
                    $scope.totalItems = $scope.EmployeeData.length;
                    $scope.todos = $scope.EmployeeData;
                    $scope.makeTodos();

                    console.log($scope.EmployeeData);
                });

                $scope.Add = function (str) {

                    $scope.edt["em_first_name"] = str.em_first_name;
                   

                }

            }
          
            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.EmployeeData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.EmployeeData;
                }
                $scope.makeTodos();
            }

            //function searchUtil(item, toSearch) {
            //    /* Search Text in all 3 fields */
            //    return (item.em_first_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.em_first_name == toSearch) ? true : false;
            //}

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                     item.em_first_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.em_login_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.em_email.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.em_mobile.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.em_login_code == toSearch ||
                     item.em_login_code == toSearch) ? true : false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
            $scope.createdate = function (end_date, start_date, name) {

               
                var month1 = end_date.split("/")[0];
                var day1 = end_date.split("/")[1];
                var year1 = end_date.split("/")[2];
                var new_end_date = year1 + "/" + month1 + "/" + day1;
                //var new_end_date = day1 + "/" + month1 + "/" + year1;

                var year = start_date.split("/")[0];
                var month = start_date.split("/")[1];
                var day = start_date.split("/")[2];
                var new_start_date = year + "/" + month + "/" + day;
                // var new_start_date = day + "/" + month + "/" + year;

                if (new_end_date < new_start_date) {

                    $rootScope.strMessage = "Please Select Future Date";
                    $('#message').modal('show');

                    $scope.edt[name] = '';
                }
                else {

                    $scope.edt[name] = new_end_date;
                }
            }

            $scope.showdate = function (date, name) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                $scope.edt[name] = year + "/" + month + "/" + day;
            }

            simsController.directive('ngFiles', ['$parse', function ($parse) {

                function fn_link(scope, element, attrs) {
                    var onChange = $parse(attrs.ngFiles);
                    element.on('change', function (event) {
                        onChange(scope, { $files: event.target.files });
                    });
                };

                return {
                    link: fn_link
                }
            }])
       
        }]);

})();