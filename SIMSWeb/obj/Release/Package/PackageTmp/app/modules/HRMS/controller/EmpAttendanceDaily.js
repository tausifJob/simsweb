﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmpAttendanceDailyCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', 'XLSXReaderService', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, XLSXReaderService, $filter) {

            $scope.rgvtbl = false;
            $scope.edt = [];
            $scope.edt1 = [];
            $scope.edt2 = [];
            var del = [];
            $http.get(ENV.apiUrl + "api/EmpAttendanceDailyController/getPers326Dept").then(function (Pers326_get_Dept) {
                $scope.dep = Pers326_get_Dept.data;
            });

            $http.get(ENV.apiUrl + "api/EmpAttendanceDailyController/getPers326desg").then(function (Pers326_get_desg) {
                $scope.deg = Pers326_get_desg.data;
            });

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            var maindata = [];

            $scope.btn_reset_Click = function () {

                $scope.Pers326_Get_attednce_deatils = '';
                $scope.edt = [];
                $scope.edt1 = [];
                $scope.edt2 = [];
                $scope.rgvtbl = false;
            }

            $scope.btn_Submit = function (str, str1) {


                if (str == undefined) {
                    $scope.edt.dep_code = '';
                }
                if (str1 == undefined) {
                    $scope.edt.dg_code = '';
                }



                $scope.busy = true;
                $scope.rgvtbl = false;
                maindata = [];


                $http.post(ENV.apiUrl + "api/EmpAttendanceDailyController/Pers326_Get_attednce_deatils?sdate=" + $scope.edt1.sdate + "&edate=" + $scope.edt2.edate + "&dept=" + $scope.edt.dep_code + "&desg=" + $scope.edt.dg_code).then(function (Pers326_Get_attednce_deatils) {

                    $scope.Pers326_Get_attednce_deatils1 = Pers326_Get_attednce_deatils.data;

                    var check = document.getElementById('Checkbox8');

                    if (check.checked == true) {
                        for (var i = 0; i < $scope.Pers326_Get_attednce_deatils1.length; i++) {
                            maindata.push($scope.Pers326_Get_attednce_deatils1[i]);
                        }
                    }
                    else {
                        for (var i = 0; i < $scope.Pers326_Get_attednce_deatils1.length; i++) {
                            if ($scope.Pers326_Get_attednce_deatils1[i].leave_code != 'P') {
                                maindata.push($scope.Pers326_Get_attednce_deatils1[i]);
                            }
                        }
                    }




                    //if (check.checked == true) {
                    //    for (var i = 0; i < $scope.Pers326_Get_attednce_deatils1.length; i++) {
                    //        maindata.push($scope.Pers326_Get_attednce_deatils1[i]);
                    //    }
                    //}
                    //else {
                    //    for (var i = 0; i < $scope.Pers326_Get_attednce_deatils1.length; i++) {
                    //        if ($scope.Pers326_Get_attednce_deatils1[i].leave_code != 'P') {
                    //            
                    //            maindata.push($scope.Pers326_Get_attednce_deatils1[i]);
                    //        }
                    //    }
                    //}



                    $scope.Pers326_Get_attednce_deatils = maindata;
                    $scope.busy = false;
                    $scope.rgvtbl = true;
                });
                $scope.busy = false;
                $scope.rgvtbl = true;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.btn_Employeeupdate_click = function () {
                $scope.rgvtbl = false;
                $scope.busy = true;
                var senddataobject = [];
                for (var i = 0; i < $scope.Pers326_Get_attednce_deatils1.length; i++) {
                    senddataobject.push($scope.Pers326_Get_attednce_deatils1[i]);
                }
                console.log(senddataobject);
                $http.post(ENV.apiUrl + "api/EmpAttendanceDailyController/Pers326_update_att_data", senddataobject).then(function (result) {
                    $scope.result = result.data;
                    $scope.busy = false;
                    if ($scope.result) {
                        swal('', 'Record Sucessfully Updated...!!!');
                        $scope.btn_reset_Click();
                    }
                    else {
                        swal('', 'Record Is Not Updated...!!!');
                        $scope.rgvtbl = true;
                    }
                });
            }
            $scope.uploadClick1 = function () {
            }

            ///////////////////////////// Upload Doc Code //////////////////////////////////////////

            $scope.display = true;
            $scope.save = true;
            $scope.items = [];

            var date = new Date();
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss');
            $scope.showPreview = false;
            $scope.showJSONPreview = true;
            $scope.json_string = "";
            //$scope.itemsPerPage = '50';
            //$scope.currentPage = 0;
            //$scope.filesize = false;

            //$scope.size = function (pagesize) {
            //    $scope.itemsPerPage = pagesize;

            //}

            //$scope.range = function () {
            //    var rangeSize = 5;
            //    var ret = [];
            //    var start;

            //    start = $scope.currentPage;
            //    if (start > $scope.pageCount() - rangeSize) {
            //        start = $scope.pageCount() - rangeSize + 1;
            //    }

            //    for (var i = start; i < start + rangeSize; i++) {
            //        if (i >= 0)
            //            ret.push(i);
            //    }

            //    return ret;


            //};

            //$scope.prevPage = function () {
            //    if ($scope.currentPage > 0) {
            //        $scope.currentPage--;
            //    }
            //};

            //$scope.prevPageDisabled = function () {
            //    return $scope.currentPage === 0 ? "disabled" : "";
            //};

            ////$scope.pageCount = function () {

            ////    return Math.ceil($scope.sheets.Sheet1.length / $scope.itemsPerPage) - 1;
            ////};

            //$scope.nextPage = function () {
            //    if ($scope.currentPage < $scope.pageCount()) {
            //        $scope.currentPage++;
            //    }
            //};

            //$scope.nextPageDisabled = function () {
            //    return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            //};

            //$scope.setPage = function (n) {
            //    $scope.currentPage = n;
            //};

            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {

                // $scope.filesize = true;
                // if ($files) {
                $('#uploadModal').modal({ backdrop: 'static', keyboard: false });

                //}
                $scope.flag = 0;
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                    var i = 0;
                    console.log($files[i].size);

                });
            };
            $scope.file_changed = function (element) {
                // SpinnerDialog.show('loading','...');
                $scope.busy = true;
                $scope.rgvtbl = false;
                var photofile = element.files[0];
                console.log(element.files[0].size);
                if (element.files[0].size > 600000) {
                    swal('', 'dsdfsd')
                }
                else {
                    $scope.filename = element.files[0];
                    $scope.photo_filename = (photofile.type);
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $scope.$apply(function () {
                            $scope.prev_img = e.target.result;
                        });
                    };
                    reader.readAsDataURL(photofile);
                    $scope.sheets = [];
                    $scope.excelFile = element.files[0];
                    XLSXReaderService.readFile($scope.excelFile, $scope.showPreview, $scope.showJSONPreview).then(function (xlsxData) {
                        console.log(xlsxData.sheets);
                        $scope.display = false;
                        $scope.save = false;
                        console.log(xlsxData.sheets.Sheet1[0]);
                        var size = Object.keys(xlsxData.sheets.Sheet1[0]).length;
                        console.log(size);
                        if (size <= 4) {
                            $scope.sheets = xlsxData.sheets
                            console.log($scope.sheets)
                            $scope.sheetName = { sheet: 'Sheet1' };
                            var temp = "a";
                            $("#sheet_name").val(temp);
                        } else {
                            $scope.sheets = "";
                            swal({ title: "Document Upload Failed,Does not match Column", imageUrl: "assets/img/check.png", });
                        }
                    });
                    // if (element.files[0].size<200000) {
                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + '/api/file/uploadDocument?filename=' + $scope.filename.name + $filter('date')(new Date(), 'yyyy-MM-dd') + '-' + $filter('date')(new Date(), 'HH-mm-ss') + "&location=" + "Docs/Student",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    $http(request).success(function (d) {
                        //alert(d);
                        console.log(d);
                        //                        swal({ title: "Document Uploaded Successfully", imageUrl: "assets/img/check.png", });
                        var data = {
                            sims_admission_user_code: $rootScope.globals.currentUser.username,
                            sims_admission_doc_path: d
                        }

                        $http.post(ENV.apiUrl + "api/document/insertdoc", data).then(function (res) {
                            $scope.ins = res.data;
                            if (res.data) {
                                swal({ title: "Document Uploaded Successfully", imageUrl: "assets/img/check.png", });
                                $http.get(ENV.apiUrl + "api/document/getDocument?enroll=S1001").then(function (res) {
                                    $scope.items = res.data;
                                    console.log(res.data);
                                });
                            } else {
                                swal({ title: "Document Upload Failed, Check Connection", imageUrl: "assets/img/check.png", });
                            }

                        });
                    });
                }
            };
            $scope.uploadClick = function () {
                // $scope.filesize = true;
                formdata = new FormData();

            }
            if ($rootScope.globals.studentsLoaded) {
                $scope.getstudentList();
            }

            $scope.insertData = function () {
                if ($scope.datasend != "") {
                    console.log($scope.datasend);
                    var datearr = []
                    for (var i = 0; i < $scope.datasend.length; i++) {

                        datearr.push($scope.datasend[i].att_date);

                    }

                    var max = '';
                    for (var i = 0; i < datearr.length; i++) {
                        if (datearr[i] > max) {
                            max = datearr[i];
                        }
                    }

                    var earliestDate = datearr[0];
                    for (var i = 1; i < datearr.length ; i++) {
                        var currentDate = datearr[i];
                        if (currentDate < earliestDate) {
                            earliestDate = currentDate;
                        }
                    }

                    console.log(max);
                    console.log(earliestDate);



                    console.log($scope.datasend);
                    $http.post(ENV.apiUrl + "api/EmpAttendanceDailyController/MarkEmpDailyAttendance", $scope.datasend).then(function (res) {
                        console.log(res.data);

                        if (res.data) {
                            $scope.edt1 = { sdate: earliestDate };
                            $scope.edt2 = { edate: max };
                            $('#uploadModal').modal('hide');
                            $scope.busy = true;
                            $scope.rgvtbl = false;
                            maindata = [];
                            $http.post(ENV.apiUrl + "api/EmpAttendanceDailyController/Pers326_Get_attednce_deatils?sdate=" + $scope.edt1.sdate + "&edate=" + $scope.edt2.edate + "&dept=" + '' + "&desg=" + '').then(function (Pers326_Get_attednce_deatils) {

                                $scope.Pers326_Get_attednce_deatils1 = Pers326_Get_attednce_deatils.data;

                                var check = document.getElementById('Checkbox8');

                                if (check.checked == true) {
                                    for (var i = 0; i < $scope.Pers326_Get_attednce_deatils1.length; i++) {
                                        maindata.push($scope.Pers326_Get_attednce_deatils1[i]);
                                    }
                                }
                                else {
                                    for (var i = 0; i < $scope.Pers326_Get_attednce_deatils1.length; i++) {
                                        if ($scope.Pers326_Get_attednce_deatils1[i].leave_code != 'P') {
                                            maindata.push($scope.Pers326_Get_attednce_deatils1[i]);
                                        }
                                    }
                                }



                                $scope.Pers326_Get_attednce_deatils = maindata;
                                $scope.busy = false;
                                $scope.rgvtbl = true;
                            });



                            swal(res.data + ' Records Inserted Successfully');
                            $scope.datasend = [];
                            $scope.CheckMultiple();
                        } else {
                            $scope.datasend = [];
                            swal('Records Not Inserted');
                        }
                    });
                } else {
                    swal('', 'Please Select Record to Insert');
                }
            }

            $scope.cancel = function () {
                $scope.display = true;
                $scope.sheets = "";
            }


            $scope.showPreviewChanged = function (selectedSheetName) {
                $scope.sheets[$scope.selectedSheetName];
            }

            $scope.textFocus = function (key, val) {
                if (key == "Date" || key == "Emp_No") {
                    $scope.value = true;
                } else {
                    $scope.value = false;
                }
            }

            $scope.textChange = function (item, key, val) {
                if (key == "Clock_In" || key == "Clock_Out") {
                    item[key] = val;
                }

                var main = document.getElementById($scope.chekindex);
                main.checked = false;
                var rm = del.indexOf($scope.chekindex);
                $scope.datasend.splice(rm, 1);
            }
            $scope.$on('student_change', function () {
                $scope.getstudentList();
            });

            $scope.datasend = [];

            $scope.Checkval = function (val, index) {
                val.opr = "Q";
                $scope.values = val;
                $scope.chekindex = index;
                var main = document.getElementById(index);

                if (main.checked == true) {
                    var data = {
                        no: val.Emp_No,
                        att_date: val.Date,
                        s1in: val.Clock_In,
                        s1out: val.Clock_Out,
                        opr: "Q",
                    }
                    $scope.datasend.push(data);

                } else {
                    main.checked = false;
                    var rm = del.indexOf(index);
                    $scope.datasend.splice(rm, 1);
                }
                $scope.filecount = "You Selected " + $scope.datasend.length + " Records";
            }

            $scope.CheckMultiple = function () {
                var main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.sheets.Sheet1.length; i++) {
                    var v = document.getElementById(i);
                    if (main.checked == true) {
                        if (v.checked != true) {
                            v.checked = true;
                            var t = $scope.sheets.Sheet1[i];
                            $scope.Checkval(t, i);
                        }
                    }
                    else {
                        v.checked = false;
                        var t = $scope.sheets.Sheet1[i];
                        main.checked = false;
                        $scope.Checkval(t, i);
                    }
                }

            }

            $scope.downloadFormat = function () {
                $scope.url = "http://localhost:90/SIMSAPI/Content/format/attendance/attendance.xls";
                window.open($scope.url, '_self', 'location=yes');

            }

        }]);


    simsController.directive('ngFiles', ['$parse', '$scope', function ($parse, $scope) {
        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])


    simsController.factory("XLSXReaderService", ['$q', '$rootScope',
    function ($q, $rootScope) {
        var service = function (data) {
            angular.extend(this, data);
        }

        service.readFile = function (file, readCells, toJSON) {
            var deferred = $q.defer();

            XLSXReader(file, readCells, toJSON, function (data) {
                $rootScope.$apply(function () {
                    deferred.resolve(data);
                });
            });

            return deferred.promise;
        }


        return service;
    }
    ])



})();