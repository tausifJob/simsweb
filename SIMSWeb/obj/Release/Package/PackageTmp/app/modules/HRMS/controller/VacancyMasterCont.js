﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('VacancyMasterCont',
         ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

             $scope.Savebtn = true;

             $scope.edt = {
                 pays_vacancy_status: true,
             }

             var file_name_en = '';
             var main, deletefin = [];

             var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd');
             var dateyear1 = moment(dateyear);
             dateyear1.add(1, 'months');
             dateyear1 = moment(dateyear1).format('YYYY-MM-DD');

             $scope.dt =
             {
                 sims_from_date: dateyear,
             }

             $scope.dt1 =
             {
                 sims_to_date: dateyear1,
             }

             $('*[data-datepicker="true"] input[type="text"]').datepicker({
                 todayBtn: true,
                 orientation: "top left",
                 autoclose: true,
                 todayHighlight: true,
                 format: 'yyyy-mm-dd'
             });

             $('#text-editor').wysihtml5();

             $scope.getShow = function () {


                 $scope.dt =
                 {
                     sims_from_date: dateyear,
                 }

                 $scope.dt1 =
                 {
                     sims_to_date: dateyear1,
                 }

                 $scope.edt = {
                     pays_vacancy_status: true,
                 }

                 $http.get(ENV.apiUrl + "api/StudentReport/getCompanyDetails").then(function (get_compname) {
                     $scope.getcompname = get_compname.data;
                     $scope.temp = { pays_vacancy_company_code: $scope.getcompname[0].pays_vacancy_company_code };

                     $http.get(ENV.apiUrl + "api/StudentReport/getDepartmentDetails?comp_code=" + $scope.temp.pays_vacancy_company_code).then(function (getdepttype) {
                         $scope.depttype = getdepttype.data;
                         $scope.temp1 = { pays_vacancy_dept_code: $scope.depttype[0].pays_vacancy_dept_code };
                     });

                     $http.get(ENV.apiUrl + "api/StudentReport/getDesignationDetails?comp_code=" + $scope.temp.pays_vacancy_company_code).then(function (getdesgtype) {
                         $scope.desgtype = getdesgtype.data;
                         $scope.temp2 = { pays_vacancy_profile_desg_code: $scope.desgtype[0].pays_vacancy_profile_desg_code };

                     });

                     $http.get(ENV.apiUrl + "api/StudentReport/getVacancytypeDetails?comp_code=" + $scope.temp.pays_vacancy_company_code).then(function (getvectype) {
                         $scope.vecktype = getvectype.data;
                         $scope.temp3 = { pays_vacancy_type: $scope.vecktype[0].pays_vacancy_type };

                     });

                     $http.get(ENV.apiUrl + "api/StudentReport/getVacancyLevelDetails?comp_code=" + $scope.temp.pays_vacancy_company_code).then(function (getveclevel) {
                         $scope.veclevel = getveclevel.data;
                         $scope.temp4 = { pays_vacancy_level: $scope.veclevel[0].pays_vacancy_level };

                     });

                 });

                 //main.checked = false;
                 //$scope.CheckAllChecked();

             }

             $scope.getShow();

             $scope.Showdata = function () {
                 var desg = ''
                 var exp = ''
                 var date = ''
                 var todate = ''

                 $http.get(ENV.apiUrl + "api/StudentReport/getVacancyDetails?desg=" + desg + "&exp=" + exp + "&date=" + date + "&todate=" + todate).then(function (getvd) {
                     $scope.getvds = getvd.data;
                 });

             }

             $scope.Showdata();

             $('*[data-datepicker="true"] input[type="text"]').datepicker({

                 todayBtn: true,
                 orientation: "top left",
                 autoclose: true,
                 todayHighlight: true,
                 format: 'yyyy-mm-dd'
             });

             $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                 $('input[type="text"]', $(this).parent()).focus();
             });

             $scope.check_date = function () {
                 debugger;
                 if ($scope.dt1.sims_to_date < $scope.dt.sims_from_date) {
                     swal({ title: 'Alert', text: "Please select future date", width: 300, height: 200 })
                     $scope.dt1.sims_to_date = dateyear1;
                 }
             }

             $scope.cancel = function () {
                 debugger
                 var img = document.getElementById("file1");
                 img = "";
                 $scope.sims_driver_img = "";
                 $scope.prev_img = "";
                 $scope.temp = {
                     sims_Employee: 'emp_true'
                 }
                 $scope.table1 = true;
                 $scope.operation = false;
                 $scope.temp = "";
                 $scope.myForm.$setPristine();
                 $scope.myForm.$setUntouched();

                 $scope.edt = {
                     dg_code: "",
                     sims_employee_code: "",
                     sims_driver_name: "",
                     sims_driver_type: "",
                     sims_user_code: "",
                     sims_driver_img: "",
                     sims_driver_experience_years: "",

                     sims_driver_gender: "",
                     sims_driver_mobile_number1: "",
                     sims_driver_mobile_number2: "",
                     sims_driver_mobile_number3: "",
                     sims_driver_address: "",
                     sims_driver_status: "",
                     sims_driver_date_of_birth: "",


                     sims_driver_driving_license_number: "",
                     sims_driver_license_issue_date: "",
                     sims_driver_license_expiry_date: "",
                     sims_driver_license_place_of_issue: "",
                     sims_driver_license_vehicle_category: "",
                     sims_driver_license_vehicle_mode: "",
                     sims_driver_visa_number: "",

                     sims_driver_visa_type: "",
                     sims_driver_visa_issuing_authority: "",
                     sims_driver_visa_issuing_place: "",
                     sims_driver_visa_issue_date: "",
                     sims_driver_visa_expiry_date: "",
                     sims_driver_national_id: "",
                     sims_driver_national_id_issue_date: "",

                     sims_driver_national_id_expiry_date: ""
                 }
             }

             //$scope.comn_alert_message1 = $('#text-editor').val();

             //$('#text-editor').data('wysihtml5').editor.composer.disable();

             //$('#text-editor').data('wysihtml5').editor.composer.enable();

             //$('#text-editor').data("wysihtml5").editor.clear();

             $scope.Save = function () {
                 debugger;
                 if (file_name_en == undefined || file_name_en == '') {
                     file_name_en = null;
                 }
                 //if (myForm) {
                 var Savedata = [];
                 var Save_sa = ({
                     'pays_vacancy_company_code': $scope.temp.pays_vacancy_company_code,
                     'pays_vacancy_dept_code': $scope.temp1.pays_vacancy_dept_code,
                     'pays_vacancy_profile_desg_code': $scope.temp2.pays_vacancy_profile_desg_code,
                     'pays_vacancy_type': $scope.temp3.pays_vacancy_type,
                     'pays_vacancy_desc': $('#text-editor').val(),
                     'pays_vacancy_roles': $scope.edt.pays_vacancy_roles,
                     'pays_vacancy_level': $scope.temp4.pays_vacancy_level,
                     'pays_vacancy_min_experience': $scope.edt.pays_vacancy_min_experience,
                     'pays_vacancy_posting_date': $scope.dt.sims_from_date,
                     'pays_vacancy_expiry_date': $scope.dt1.sims_to_date,
                     'pays_vacancy_status': $scope.edt.pays_vacancy_status,
                     //'pays_vacancy_doc': $scope.photofile.name,
                     'pays_vacancy_doc': file_name_en,
                     'opr': 'I',
                 });
                 Savedata.push(Save_sa);

                 $http.post(ENV.apiUrl + "api/StudentReport/CUDVacancyDetails", Savedata).then(function (msg) {

                     $scope.msg1 = msg.data;
                     if ($scope.msg1.strMessage != undefined) {

                         swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });

                     }
                     $scope.Showdata();
                     $scope.Reset();
                 });
                 //}
             }

             $scope.delete = function () {
                 debugger;
                 var Savedata1 = [];
                 var t = $scope.getvds.length;
                 for (var i = 0; i < t; i++) {
                     var v = document.getElementById($scope.getvds[i].pays_vacancy_id + i);
                     if (v.checked == true) {
                         $scope.flag = true;
                         var Save_sa1 = ({
                             'pays_vacancy_id': $scope.getvds[i].pays_vacancy_id,
                             'opr': 'D',
                         });
                         Savedata1.push(Save_sa1);
                     }
                     else {
                         swal({ title: "Alert", text: "Please Select Atleast One Record...", width: 380, height: 200 });
                     }
                 }
                 $http.post(ENV.apiUrl + "api/StudentReport/CUDVacancyDetails", Savedata1).then(function (msg) {

                     $scope.msg1 = msg.data;
                     if ($scope.msg1.strMessage != undefined) {

                         swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                     }
                     $scope.Showdata();
                     $scope.Reset();
                 });

             }

             $scope.Reset = function () {

                 $scope.temp.pays_vacancy_company_code = '',
                 $scope.temp1.pays_vacancy_dept_code = '',
                 $scope.temp2.pays_vacancy_profile_desg_code = '',
                 $scope.temp3.pays_vacancy_type = '',
                 $('#text-editor').data("wysihtml5").editor.clear();
                 $scope.edt.pays_vacancy_roles = '',
                 $scope.temp4.pays_vacancy_level = '',
                 $scope.edt.pays_vacancy_min_experience = '',
                 $scope.dt.sims_from_date = '',
                 $scope.dt1.sims_to_date = '',
                 $scope.Showdata();
                 $scope.getShow();
             }

             $scope.CheckAllChecked = function () {
                 main = document.getElementById('mainchk');
                 if (main.checked == true) {
                     for (var i = 0; i < $scope.getvds.length; i++) {
                         var v = document.getElementById($scope.getvds[i].pays_vacancy_id + i);
                         v.checked = true;
                         $scope.row1 = 'row_selected';
                         $('tr').addClass("row_selected");
                     }
                 }
                 else {

                     for (var i = 0; i < $scope.getvds.length; i++) {
                         var v = document.getElementById($scope.getvds[i].pays_vacancy_id + i);
                         v.checked = false;
                         main.checked = false;
                         $scope.row1 = '';
                         $('tr').removeClass("row_selected");
                     }
                 }

             }

             $scope.checkonebyoneselect = function () {

                 $("input[type='checkbox']").change(function (e) {

                     if ($(this).is(":checked")) {
                         $(this).closest('tr').addClass("row_selected");
                         $scope.color = '#edefef';
                     }
                     else {
                         $(this).closest('tr').removeClass("row_selected");
                         $scope.color = '#edefef';
                     }
                 });

                 main = document.getElementById('mainchk');
                 if (main.checked == true) {
                     main.checked = false;
                     $("input[type='checkbox']").change(function (e) {
                         if ($(this).is(":checked")) {
                             $(this).closest('tr').addClass("row_selected");
                         }
                         else {
                             $(this).closest('tr').removeClass("row_selected");
                         }
                     });
                 }
             }

             $timeout(function () {
                 $("#fixTable").tableHeadFixer({ 'top': 1 });
             }, 100);

             var formdata = new FormData();

             $scope.getTheFiles = function ($files) {
                 $scope.filesize = true;

                 angular.forEach($files, function (value, key) {
                     formdata.append(key, value);

                     var i = 0;
                     if ($files[i].size > 800000) {
                         $scope.filesize = false;
                         $scope.edt.photoStatus = false;
                         swal({ title: "Alert", text: "File Should Not Exceed 800Kb.", imageUrl: "assets/img/notification-alert.png", });
                     }
                     else { }
                 });
             };

             $scope.file_changed = function (element) {

                 debugger
                 var photofile = element.files[0];

                 $scope.photo_filename = (photofile.name);

                 $scope.edt['sims_driver_img'] = $scope.photo_filename;

                 $scope.photo_filename = (photofile.type);
                 var reader = new FileReader();
                 reader.onload = function (e) {
                     $scope.$apply(function () {
                         $scope.prev_img = e.target.result;

                     });
                 };
                 reader.readAsDataURL(photofile);
                 if ($scope.edt.sims_employee_code == "" || $scope.edt.sims_employee_code == null) {
                     var request = {
                         method: 'POST',
                         url: ENV.apiUrl + 'api/file/upload?filename=' + $scope.edt.sims_driver_name + '&location=DriverImage',
                         data: formdata,
                         headers: {
                             'Content-Type': undefined
                         }
                     };

                 }
                 else {
                     var request = {
                         method: 'POST',
                         url: ENV.apiUrl + 'api/file/upload?filename=' + $scope.edt.sims_employee_code + '&location=DriverImage',
                         data: formdata,
                         headers: {
                             'Content-Type': undefined
                         }
                     };

                 }


                 $http(request).success(function (d) {

                     $scope.edt['sims_driver_img'] = $scope.edt.sims_driver_img;
                     $scope.edt['sims_driver_img'] = d;
                 });


             };

             var formdata = new FormData();
             $scope.getTheFiles = function ($files) {
                 debugger;
                 angular.forEach($files, function (value, key) {
                     formdata.append(key, value);
                 });
             };
             var file_name = '';

             var file_doc = [];

             $scope.file_changed = function (element) {
                 debugger;
                 file_name = '';
                 var v = new Date();
                 //if ($scope.teacher_doc.length > 4)
                 //{
                 //    swal('', 'Upload maximum 5 files.');
                 //
                 //}
                 //else
                 //{

                 //file_name = 'Assignment_' + v.getDay() + '_' + v.getMonth() + '_' + v.getYear() + '_' + v.getHours() + '_' + v.getMonth() + '_' + v.getSeconds();
                 $scope.photofile = element.files[0];
                 file_name_en = '';

                 file_name_en = $scope.photofile.name;
                 var fortype = $scope.photofile.type.split("/")[1];
                 if (fortype == 'png' || fortype == 'jpeg' || fortype == 'jpg' || fortype == 'pdf') {

                     if ($scope.photofile.size > 200000) {
                         swal('', 'File size limit not exceed upto 200kb.')
                     }
                     else {
                         $scope.photo_filename = ($scope.photofile.type);
                         console.clear();
                         console.log($scope.edt);

                         var reader = new FileReader();
                         reader.onload = function (e) {
                             $scope.$apply(function () {
                                 $scope.prev_img = e.target.result;
                                 var request = {
                                     method: 'POST',
                                     url: ENV.apiUrl + 'api/StudentReport/upload?filename=' + file_name_en + "&location=" + "/Docs/Vacancy/",
                                     data: formdata,
                                     headers: {
                                         'Content-Type': undefined
                                     }


                                 };
                                 $http(request).success(function (d) {
                                     debugger;
                                     var t = {
                                         sims509_doc: d,
                                         sims509_doc_name: file_name_en
                                     }
                                     $scope.teacher_doc.push(t);
                                     file_doc = [];
                                     for (var i = 0; i < $scope.teacher_doc.length; i++) {
                                         var c = {
                                             sims509_doc: $scope.teacher_doc[i].sims509_doc,
                                             sims509_doc_name: $scope.teacher_doc[i].sims509_doc_name,
                                             sims_doc_line: i + 1
                                         }
                                         file_doc.push(c);
                                     }
                                     $scope.teacher_doc = [];
                                     debugger;
                                     $scope.teacher_doc = file_doc;


                                     //$scope.maindata.file = d;


                                 });

                             });
                         };
                         reader.readAsDataURL($scope.photofile);
                     }
                 }
                 else {
                     swal('', 'PDF and Image Files are allowed.');
                 }
                 //}

             }

         }])

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }

    }])

})();