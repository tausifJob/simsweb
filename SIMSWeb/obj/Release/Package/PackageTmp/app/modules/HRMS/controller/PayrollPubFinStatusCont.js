﻿(function () {
    'use strict';
    var del = [];
    var main;
    var main1;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PayrollPubFinStatusCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var d = new Date();
            var n = d.getMonth() + 1;
            $scope.edt = {};
            $scope.EmployeeDetails = [];
            $scope.fi_date = true;
            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.select_date_finalize = function (date) {
                debugger;
                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                var date1 = year + "-" + month + "-" + day;
                $scope.finalize_date = date1;
            }

            $scope.select_date_publish = function (date) {
                debugger;
                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                var date1 = year + "-" + month + "-" + day;
                $scope.publish_date = date1;
            }




            $http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/PubYear").then(function (PubYear) {
                $scope.PubYear = PubYear.data;
                $scope.fin_year = $scope.PubYear[0].fin_year;
                $scope.fin_year_desc = $scope.PubYear[0].fin_year_desc;

            });

            $http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/PubMonth").then(function (PubMonth) {
                $scope.PubMonth = PubMonth.data;
                $scope.edt = { sd_year_month_code: n.toString() };

            });

            $scope.Show_data = function () {
                $scope.busy = true;
                $scope.rgvtbl = false;
                $http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/EmployeeDetails?year_month=" + $scope.fin_year + $scope.edt.sd_year_month_code).then(function (EmployeeDetails) {
                    $scope.EmployeeDetails = EmployeeDetails.data;
                    debugger;
                    main = document.getElementById('mainchk');
                    main.checked = true;
                    for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                        if ($scope.EmployeeDetails[i].holdstatus == false) {
                            main.checked = false;
                        }
                    }
                    if ($scope.EmployeeDetails.length > 0) {
                        //    var ear = 0, ded = 0, tot = 0;
                        //    for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                        //        ear = ear + parseFloat($scope.EmployeeDetails[i].earn);
                        //        ded = ded + parseFloat($scope.EmployeeDetails[i].ded);
                        //        tot = tot + (ear - ded);
                        //    }
                        //    $scope.Earning = ear;
                        //    $scope.Deduction = ded;
                        //    $scope.Total = tot;
                        $scope.busy = false;
                        $scope.rgvtbl = true;
                    }
                    else {
                        swal('', 'Payroll not genrated for selected month.');
                        $scope.busy = false;
                        $scope.rgvtbl = false;
                    }

                });

                //$http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/PubFinDetails").then(function (PubFinDetails) {
                //    $scope.PubFinDetails = PubFinDetails.data;
                //    $scope.publish_date
                //    $scope.fin_status
                //    $scope.finalize_date


                //});
            }


            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.le_leave_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                        item.acno == toSearch) ? true : false;
            }

            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');

                for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                    if (main.checked == true) {
                        $scope.EmployeeDetails[i].holdstatus = true;
                        $('tr').addClass("row_selected");
                    }

                    else {
                        $scope.EmployeeDetails[i].holdstatus = false;
                        $('tr').removeClass("row_selected");

                    }

                }
            }

            $scope.onhold_update = function () {
                console.log($scope.EmployeeDetails);
                var datalst = [];
                for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                    var data = {
                        holdstatus: $scope.EmployeeDetails[i].holdstatus,
                        year_month: +$scope.fin_year + $scope.edt.sd_year_month_code,
                        emp_number: $scope.EmployeeDetails[i].emp_number
                    }
                    datalst.push(data);
                }

                $http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/UpdatePubFinStatus", datalst).then(function (UpdatePubFinStatus) {
                    $scope.UpdatePubFinStatus = UpdatePubFinStatus.data;
                    debugger;
                    if ($scope.UpdatePubFinStatus) {
                        swal('', 'Record updated successfully.');

                        $scope.busy = false;
                        $scope.rgvtbl = true;
                        $scope.reset_form();
                    }
                    else {
                        $scope.Show_data();
                        swal('', 'Record not updated.');
                        $scope.busy = false;
                        $scope.rgvtbl = false;
                        $scope.reset_form();

                    }



                });
            }

            $scope.finlize_click = function () {
                $scope.busy = true;
                $scope.rgvtbl = false;
                $http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/FinDetails?year_month=" + $scope.fin_year + $scope.edt.sd_year_month_code).then(function (FinDetails) {
                    $scope.FinDetails = FinDetails.data;
                    debugger;
                    if ($scope.FinDetails.length > 0) {
                        var ear = 0, ded = 0, tot = 0;
                        for (var i = 0; i < $scope.FinDetails.length; i++) {
                            if ($scope.FinDetails[i].pc_earn_dedn_tag == 'E')
                                ear = ear + parseFloat($scope.FinDetails[i].amount);
                            else
                                ded = ded + parseFloat($scope.FinDetails[i].amount);


                        }
                        tot = tot + (ear - ded);
                        $scope.Earning = ear;
                        $scope.Deduction = ded;
                        $scope.Total = tot;
                        $('#myModal').modal({ backdrop: 'static', keyboard: false });
                    }
                    else {
                        swal('', 'Payroll not genrated for current month.');
                    }
                    $scope.busy = false;


                });
            }

            $scope.reset_form = function () {
                $http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/PubYear").then(function (PubYear) {
                    $scope.PubYear = PubYear.data;
                    $scope.fin_year = $scope.PubYear[0].fin_year;
                    $scope.fin_year_desc = $scope.PubYear[0].fin_year_desc;
                });

                $http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/PubMonth").then(function (PubMonth) {
                    $scope.PubMonth = PubMonth.data;
                    $scope.edt = { sd_year_month_code: n.toString() };
                });
                $scope.EmployeeDetails = [];
                $scope.rgvtbl = false;
            }

            $scope.Save = function () {
                if ($scope.finalize_date == undefined || $scope.finalize_date == '') {
                    swal('', 'Please select finalize date.')
                }
                else {
                    $http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/UpdateFin?year_month=" + $scope.fin_year + $scope.edt.sd_year_month_code + "&finalize_date=" + $scope.finalize_date).then(function (UpdateFin) {
                        $scope.UpdateFin = UpdateFin.data;
                        if ($scope.UpdateFin)
                            swal('', 'done');
                        else
                            swal('', 'not');

                    });
                }
            }

            $scope.close_btn = function () {
                $('#myModal').modal('hide');
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);


        }])
})();