﻿(function () {
    'use strict';
    var del = [];
    var main, temp, opr;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('UpdateEmployeeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var username = $rootScope.globals.currentUser.username;
            $scope.Emp_ID = username;

            $scope.CompReadonly = true;
            $scope.DOJReadonly = true;
            $scope.DestReadonly = true;
            $scope.DesgReadonly = true;
            $scope.StaffTypeReadonly = true;
            $scope.DeptReadonly = true;
            $scope.DeptEFReadonly = true;
            $scope.GradeReadonly = true;
            $scope.GradeEFReadonly = true;
            $scope.pass_num = true;
            $scope.pass_issue_date = true;
            $scope.pass_expiry_date = true;
            $scope.visa_num = true;
            $scope.visa_issue_date = true;
            $scope.visa_expiry_date = true;
            $scope.emirated_id = true;
            $scope.emi_issue_date = true;
            $scope.emi_expiry_date = true;

            $scope.searchText = false;
            $scope.ImgLoaded = false;
            $scope.showBtn = false;

         
            $scope.searchBtn = false;
            $scope.searchEmp = function (empcode, $event) {
                $scope.searchBtn = false;
            }


            $http.get(ENV.apiUrl + "api/HRMS/getLicSchool").then(function (licschooldata) {
                $scope.lic_school_data = licschooldata.data;
                debugger;
                if ($scope.lic_school_data[0].lic_school_country == 'Qatar') {
                    $scope.uae = false;
                    $scope.qatar = true;
                }
                else {
                    $scope.uae = true;
                    $scope.qatar = false;
                }
                console.log($scope.lic_school_data);
            });


            $http.get(ENV.apiUrl + "api/HRMS/getCompany").then(function (resc) {
                $scope.company_data = resc.data;
            });

            $http.get(ENV.apiUrl + "api/HRMS/getDestination").then(function (res2) {
                $scope.dest_data = res2.data;
                $scope.temp = {
                    'em_Company_Code': $scope.company_data[0].em_Company_Code,
                    'em_Dest_Code': $scope.dest_data[0].em_Dest_Code,

                }
            });
            $scope.EmployeeDetails = function () {

                $scope.NodataEmployee = false;
                $http.post(ENV.apiUrl + "api/common/SearchEmployee?data=" + JSON.stringify($scope.temp)).then(function (SearchEmployee_Data) {
                    $scope.Employee_Details = SearchEmployee_Data.data;
                    console.log($scope.EmployeeDetails);
                    $scope.EmployeeTable = true;

                    if (SearchEmployee_Data.data.length > 0) {
                        //   $scope.EmployeeTable = true;
                    }
                    else {
                        $scope.NodataEmployee = true;
                        //  $scope.EmployeeTable = false;
                    }


                });

            }

            $scope.EmployeeAdd = function (info) {
                debugger;
                $scope.temp = {
                    enroll_number: info.em_login_code,
                    emp_name: info.empName,
                    emp_desg: info.desg_name,
                    emp_dept: info.dept_name
                };
            }

            $scope.saveBtnDisEn = false;
            $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/';

            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };

            $scope.file_changed = function (element) {
                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;
                    });
                };
                reader.readAsDataURL(photofile);
            };

            $scope.checkDOJ = function () {
                if ($scope.temp.em_date_of_birth >= $scope.temp.em_date_of_join) {
                    swal({ title: "Alert", text: "Date Of Join Should greater than Date of Birth", width: 300, height: 200 });
                    $scope.temp.em_date_of_join = "";
                }
            }

            $scope.checkPassport = function () {
                if ($scope.temp.em_passport_exp_date < $scope.temp.em_passport_issue_date) {
                    swal({ title: "Alert", text: "Passport Expiry Date Should greater than Passport Issue Date", width: 300, height: 200 });
                    $scope.temp.em_passport_exp_date = "";
                }
            }

            $scope.checkVisa = function () {
                if ($scope.temp.em_visa_exp_date < $scope.temp.em_visa_issue_date) {
                    swal({ title: "Alert", text: "Visa Expiry Date Should greater than Visa Issue Date", width: 300, height: 200 });
                    $scope.temp.em_visa_exp_date = "";
                }
            }

            $scope.checkEmiratesID = function () {
                if ($scope.temp.em_national_id_expiry_date < $scope.temp.em_national_id_issue_date) {
                    swal({ title: "Alert", text: "Emirates ID Expiry Date Should greater than Emirates ID Issue Date", width: 300, height: 200 });
                    $scope.temp.em_national_id_expiry_date = "";
                }
            }

            $scope.Cancel = function () {
                $scope.searchText = false;
                $scope.temp = "";
                $scope.emplogcode = "";
                $scope.searchBtn = true;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.ImgLoaded = false;
            }

            $scope.globalSearch = function () { $('#MyModal').modal('show'); $scope.showBtn = false; }

            $http.get(ENV.apiUrl + "api/HRMS/getEmpData").then(function (res) {
            $http.get(ENV.apiUrl + "api/HRMS/getSearchEmpMaster?em_login_code=" + $scope.Emp_ID).then(function (res) {

                $scope.temp1 = res.data;

                $scope.temp = {

                    em_login_code: $scope.temp1[0].em_login_code,
                    em_number: $scope.temp1[0].em_number,
                    em_service_status_code: $scope.temp1[0].em_service_status_code,
                    em_status_code: $scope.temp1[0].em_status_code,
                    em_apartment_number: $scope.temp1[0].em_apartment_number,
                    em_family_name: $scope.temp1[0].em_family_name,
                    em_street_number: $scope.temp1[0].em_street_number,
                    em_area_number: $scope.temp1[0].em_area_number,
                    em_phone: $scope.temp1[0].em_phone,
                    em_fax: $scope.temp1[0].em_fax,
                    em_post: $scope.temp1[0].em_post,
                    em_emergency_contact_name2: $scope.temp1[0].em_emergency_contact_name2,
                    em_emergency_contact_number2: $scope.temp1[0].em_emergency_contact_number2,
                    em_joining_ref: $scope.temp1[0].em_joining_ref,
                    em_ethnicity_code: $scope.temp1[0].em_ethnicity_code,
                    em_handicap_status: $scope.temp1[0].em_handicap_status,
                    em_stop_salary_indicator: $scope.temp1[0].em_stop_salary_indicator,
                    em_agreement: $scope.temp1[0].em_agreement,
                    em_punching_status: $scope.temp1[0].em_punching_status,
                    em_bank_cash_tag: $scope.temp1[0].em_bank_cash_tag,
                    em_citi_exp_tag: $scope.temp1[0].em_citi_exp_tag,
                    em_leave_tag: $scope.temp1[0].em_leave_tag,
                    em_passport_issue_place: $scope.temp1[0].em_passport_issue_place,
                    em_passport_issuing_authority: $scope.temp1[0].em_passport_issuing_authority,
                    em_pssport_exp_rem_date: $scope.temp1[0].em_pssport_exp_rem_date,
                    em_visa_issuing_place: $scope.temp1[0].em_visa_issuing_place,
                    em_visa_issuing_authority: $scope.temp1[0].em_visa_issuing_authority,
                    em_visa_exp_rem_date: $scope.temp1[0].em_visa_exp_rem_date,
                    em_visa_type_code: $scope.temp1[0].em_visa_type_code,
                    em_agreement_start_date: $scope.temp1[0].em_agreement_start_date,
                    em_agreement_exp_date: $scope.temp1[0].em_agreement_exp_date,
                    em_agreemet_exp_rem_date: $scope.temp1[0].em_agreemet_exp_rem_date,
                    em_punching_id: $scope.temp1[0].em_punching_id,
                    em_dependant_full: $scope.temp1[0].em_dependant_full,
                    em_dependant_half: $scope.temp1[0].em_dependant_half,
                    em_dependant_infant: $scope.temp1[0].em_dependant_infant,
                    em_Bank_Code: $scope.temp1[0].em_Bank_Code,
                    em_bank_ac_no: $scope.temp1[0].em_bank_ac_no,
                    em_iban_no: $scope.temp1[0].em_iban_no,
                    em_route_code: $scope.temp1[0].em_route_code,
                    em_bank_swift_code: $scope.temp1[0].em_bank_swift_code,
                    em_gpf_ac_no: $scope.temp1[0].em_gpf_ac_no,
                    em_pan_no: $scope.temp1[0].em_pan_no,
                    em_labour_card_no: $scope.temp1[0].em_labour_card_no,
                    em_gosi_ac_no: $scope.temp1[0].em_gosi_ac_no,
                    em_gosi_start_date: $scope.temp1[0].em_gosi_start_date,
                    em_national_id: $scope.temp1[0].em_national_id,
                    em_secret_question_code: $scope.temp1[0].em_secret_question_code,
                    em_secret_answer: $scope.temp1[0].em_secret_answer,

                    em_building_number: $scope.temp1[0].em_building_number,
                    em_Company_Code: $scope.temp1[0].em_Company_Code,
                    em_Dept_Code: $scope.temp1[0].em_Dept_Code,
                    em_Designation_Code: $scope.temp1[0].em_Designation_Code,
                    em_Dest_Code: $scope.temp1[0].em_Dest_Code,
                    em_Grade_Code: $scope.temp1[0].em_Grade_Code,
                    em_Marital_Status_Code: $scope.temp1[0].em_Marital_Status_Code,
                    em_Nation_Code: $scope.temp1[0].em_Nation_Code,
                    em_Country_Code: $scope.temp1[0].em_Country_Code,
                    em_State_Code: $scope.temp1[0].em_State_Code,
                    em_City_Code: $scope.temp1[0].em_City_Code,
                    em_Religion_Code: $scope.temp1[0].em_Religion_Code,
                    em_Salutation_Code: $scope.temp1[0].em_Salutation_Code,
                    em_Sex_Code: $scope.temp1[0].em_Sex_Code,
                    em_Staff_Type_Code: $scope.temp1[0].em_Staff_Type_Code,
                    em_blood_group_code: $scope.temp1[0].em_blood_group_code,
                    em_date_of_birth: $scope.temp1[0].em_date_of_birth,
                    em_date_of_join: $scope.temp1[0].em_date_of_join,
                    em_dept_effect_from: $scope.temp1[0].em_dept_effect_from,
                    em_email: $scope.temp1[0].em_email,
                    em_emergency_contact_name1: $scope.temp1[0].em_emergency_contact_name1,
                    em_emergency_contact_number1: $scope.temp1[0].em_emergency_contact_number1,
                    em_first_name: $scope.temp1[0].em_first_name,
                    em_grade_effect_from: $scope.temp1[0].em_grade_effect_from,
                    em_last_name: $scope.temp1[0].em_last_name,
                    em_middle_name: $scope.temp1[0].em_middle_name,
                    em_mobile: $scope.temp1[0].em_mobile,
                    em_name_ot: $scope.temp1[0].em_name_ot,
                    em_national_id_expiry_date: $scope.temp1[0].em_national_id_expiry_date,
                    em_national_id_issue_date: $scope.temp1[0].em_national_id_issue_date,
                    em_passport_exp_date: $scope.temp1[0].em_passport_exp_date,
                    em_passport_issue_date: $scope.temp1[0].em_passport_issue_date,
                    em_passport_no: $scope.temp1[0].em_passport_no,
                    em_summary_address: $scope.temp1[0].em_summary_address,
                    em_summary_address_local_language: $scope.temp1[0].em_summary_address_local_language,
                    em_visa_exp_date: $scope.temp1[0].em_visa_exp_date,
                    em_stop_salary_from: $scope.temp1[0].em_stop_salary_from,
                    em_visa_issue_date: $scope.temp1[0].em_visa_issue_date,
                    em_visa_no: $scope.temp1[0].em_visa_no,
                    em_img: $scope.temp1[0].em_img,

                }

                $scope.prev_img =ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/'+ $scope.temp1[0].em_img;

            });
            $scope.obj = res.data;
            $scope.operation = true;
            $scope.Save_btn = true;
            $scope.display = true;
            });
            $scope.show_Update_Emp = function () {
                debugger;
                $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/';
                $http.get(ENV.apiUrl + "api/HRMS/getSearchEmpMaster?em_login_code=" + $scope.Emp_ID).then(function (res) {

                    $scope.temp1 = res.data;

                    debugger

                    $scope.temp = {

                        em_login_code: $scope.temp1[0].em_login_code,
                        em_number: $scope.temp1[0].em_number,
                        em_service_status_code: $scope.temp1[0].em_service_status_code,
                        em_status_code: $scope.temp1[0].em_status_code,
                        em_apartment_number: $scope.temp1[0].em_apartment_number,
                        em_family_name: $scope.temp1[0].em_family_name,
                        em_street_number: $scope.temp1[0].em_street_number,
                        em_area_number: $scope.temp1[0].em_area_number,
                        em_phone: $scope.temp1[0].em_phone,
                        em_fax: $scope.temp1[0].em_fax,
                        em_post: $scope.temp1[0].em_post,
                        em_emergency_contact_name2: $scope.temp1[0].em_emergency_contact_name2,
                        em_emergency_contact_number2: $scope.temp1[0].em_emergency_contact_number2,
                        em_joining_ref: $scope.temp1[0].em_joining_ref,
                        em_ethnicity_code: $scope.temp1[0].em_ethnicity_code,
                        em_handicap_status: $scope.temp1[0].em_handicap_status,
                        em_stop_salary_indicator: $scope.temp1[0].em_stop_salary_indicator,
                        em_agreement: $scope.temp1[0].em_agreement,
                        em_punching_status: $scope.temp1[0].em_punching_status,
                        em_bank_cash_tag: $scope.temp1[0].em_bank_cash_tag,
                        em_citi_exp_tag: $scope.temp1[0].em_citi_exp_tag,
                        em_leave_tag: $scope.temp1[0].em_leave_tag,
                        em_passport_issue_place: $scope.temp1[0].em_passport_issue_place,
                        em_passport_issuing_authority: $scope.temp1[0].em_passport_issuing_authority,
                        em_pssport_exp_rem_date: $scope.temp1[0].em_pssport_exp_rem_date,
                        em_visa_issuing_place: $scope.temp1[0].em_visa_issuing_place,
                        em_visa_issuing_authority: $scope.temp1[0].em_visa_issuing_authority,
                        em_visa_exp_rem_date: $scope.temp1[0].em_visa_exp_rem_date,
                        em_visa_type_code: $scope.temp1[0].em_visa_type_code,
                        em_agreement_start_date: $scope.temp1[0].em_agreement_start_date,
                        em_agreement_exp_date: $scope.temp1[0].em_agreement_exp_date,
                        em_agreemet_exp_rem_date: $scope.temp1[0].em_agreemet_exp_rem_date,
                        em_punching_id: $scope.temp1[0].em_punching_id,
                        em_dependant_full: $scope.temp1[0].em_dependant_full,
                        em_dependant_half: $scope.temp1[0].em_dependant_half,
                        em_dependant_infant: $scope.temp1[0].em_dependant_infant,
                        em_Bank_Code: $scope.temp1[0].em_Bank_Code,
                        em_bank_ac_no: $scope.temp1[0].em_bank_ac_no,
                        em_iban_no: $scope.temp1[0].em_iban_no,
                        em_route_code: $scope.temp1[0].em_route_code,
                        em_bank_swift_code: $scope.temp1[0].em_bank_swift_code,
                        em_gpf_ac_no: $scope.temp1[0].em_gpf_ac_no,
                        em_pan_no: $scope.temp1[0].em_pan_no,
                        em_labour_card_no: $scope.temp1[0].em_labour_card_no,
                        em_gosi_ac_no: $scope.temp1[0].em_gosi_ac_no,
                        em_gosi_start_date: $scope.temp1[0].em_gosi_start_date,
                        em_national_id: $scope.temp1[0].em_national_id,
                        em_secret_question_code: $scope.temp1[0].em_secret_question_code,
                        em_secret_answer: $scope.temp1[0].em_secret_answer,

                        em_building_number: $scope.temp1[0].em_building_number,
                        em_Company_Code: $scope.temp1[0].em_Company_Code,
                        em_Dept_Code: $scope.temp1[0].em_Dept_Code,
                        em_Designation_Code: $scope.temp1[0].em_Designation_Code,
                        em_Dest_Code: $scope.temp1[0].em_Dest_Code,
                        em_Grade_Code: $scope.temp1[0].em_Grade_Code,
                        em_Marital_Status_Code: $scope.temp1[0].em_Marital_Status_Code,
                        em_Nation_Code: $scope.temp1[0].em_Nation_Code,
                        em_Country_Code: $scope.temp1[0].em_Country_Code,
                        em_State_Code: $scope.temp1[0].em_State_Code,
                        em_City_Code: $scope.temp1[0].em_City_Code,
                        em_Religion_Code: $scope.temp1[0].em_Religion_Code,
                        em_Salutation_Code: $scope.temp1[0].em_Salutation_Code,
                        em_Sex_Code: $scope.temp1[0].em_Sex_Code,
                        em_Staff_Type_Code: $scope.temp1[0].em_Staff_Type_Code,
                        em_blood_group_code: $scope.temp1[0].em_blood_group_code,
                        em_date_of_birth: $scope.temp1[0].em_date_of_birth,
                        em_date_of_join: $scope.temp1[0].em_date_of_join,
                        em_dept_effect_from: $scope.temp1[0].em_dept_effect_from,
                        em_email: $scope.temp1[0].em_email,
                        em_emergency_contact_name1: $scope.temp1[0].em_emergency_contact_name1,
                        em_emergency_contact_number1: $scope.temp1[0].em_emergency_contact_number1,
                        em_first_name: $scope.temp1[0].em_first_name,
                        em_grade_effect_from: $scope.temp1[0].em_grade_effect_from,
                        em_last_name: $scope.temp1[0].em_last_name,
                        em_middle_name: $scope.temp1[0].em_middle_name,
                        em_mobile: $scope.temp1[0].em_mobile,
                        em_name_ot: $scope.temp1[0].em_name_ot,
                        em_national_id_expiry_date: $scope.temp1[0].em_national_id_expiry_date,
                        em_national_id_issue_date: $scope.temp1[0].em_national_id_issue_date,
                        em_passport_exp_date: $scope.temp1[0].em_passport_exp_date,
                        em_passport_issue_date: $scope.temp1[0].em_passport_issue_date,
                        em_passport_no: $scope.temp1[0].em_passport_no,
                        em_summary_address: $scope.temp1[0].em_summary_address,
                        em_summary_address_local_language: $scope.temp1[0].em_summary_address_local_language,
                        em_visa_exp_date: $scope.temp1[0].em_visa_exp_date,
                        em_stop_salary_from: $scope.temp1[0].em_stop_salary_from,
                        em_visa_issue_date: $scope.temp1[0].em_visa_issue_date,
                        em_visa_no: $scope.temp1[0].em_visa_no,
                        em_img: $scope.temp1[0].em_img,

                    }

                });
            }

            $scope.image_click = function () {
                $scope.ImgLoaded = true;
            }

            $scope.UpdateData = function () {
                debugger;
                //var data = $scope.temp;
                //if ($scope.photo_filename === undefined) { }
                //else {
                //    data.em_img = '.' + $scope.photo_filename.split("/")[1];
                //}
                //data.opr = 'U';

                var data = $scope.temp;
                if ($scope.photo_filename === undefined) {
                }
                else {
                    $scope.em_img = '';
                    //data.em_img ='.' + $scope.photo_filename.split("/")[1];
                    data.em_img = $scope.temp.em_login_code + '.' + $scope.photo_filename.split("/")[1];

                }
                data.opr = 'U';
                data.subopr = 'Up';



                $http.post(ENV.apiUrl + "api/HRMS/CUMasterEmployee", data).then(function (res) {
                    $scope.CUDobj = res.data;
                    swal({ text: $scope.CUDobj.strMessage, timer: 10000 });
                    //$scope.prev_img = '';
                    $scope.show_Update_Emp();
                    $state.go("main.EmpDshBrd");

                });

                if ($scope.ImgLoaded == true) {
                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + '/api/file/upload?filename=' + $scope.temp.em_login_code + "&location=" + "/EmployeeImages",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    $http(request).success(function (d) {
                        //alert(d);
                    });
                }
                $scope.searchText = false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });
            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
        }]
        )

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])
})();