﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('GoalTargetKPIMeasureAchievementCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.search_btn = true;
            $scope.clr_btn = true;
            $scope.Update_btn = false;
            $scope.newdisplay = false;
            $scope.table = true;
            $scope.mainPage = true;

            $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasureAchievement/getAllGoalTargetKPIMeasureAchievement").then(function (res1) {
                $scope.goalTargetKPImeasureAchieve = res1.data;
                $scope.totalItems = $scope.goalTargetKPImeasureAchieve.length;
                $scope.todos = $scope.goalTargetKPImeasureAchieve;
                $scope.makeTodos();
                console.log($scope.goalTargetKPImeasureAchieve);
            });

            $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasureAchievement/getAcademicYear").then(function (AcademicYear) {
                $scope.Academic_Year = AcademicYear.data;
                console.log($scope.Academic_Year);
            });

            $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasureAchievement/getGoalName").then(function (GoalName) {
                $scope.Goal_Name = GoalName.data;
                console.log($scope.Goal_Name);
            });

            $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasureAchievement/getMeasure").then(function (measureData) {
                $scope.measure_Data = measureData.data;
                console.log($scope.measure_Data);
            });

            $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasureAchievement/getAchievement").then(function (achieveData) {
                $scope.achieve_Data = achieveData.data;
                console.log($scope.achieve_Data);
            });

            $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasureAchievement/getAchievementCategory").then(function (achieveCatData) {
                $scope.achieve_Cat_Data = achieveCatData.data;
                console.log($scope.achieve_Cat_Data);
            });

            $scope.getGoalTarget = function (goalCode) {
                $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasureAchievement/getGoalTargetName?sims_sip_goal_code=" + goalCode).then(function (goalTarget) {
                    $scope.goal_Target = goalTarget.data;
                    console.log($scope.goal_Target);
                });
            }

            $scope.getGoalTargetKPI = function (goalCode, tarCode) {
                $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasureAchievement/getGoalTargetKPIMeasure?sims_sip_goal_code=" + goalCode + "&sims_sip_goal_target_code=" + tarCode).then(function (goalTargetKpi) {
                    $scope.goal_Target_Kpi = goalTargetKpi.data;
                    console.log($scope.goal_Target_Kpi);
                });
            }

            $scope.goal_target_kpi_display = function (accYear, goalCode, tarCode, kpicode) {
                $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasureAchievement/getGoalTargetKPIMeasureDisplay?sims_sip_academic_year=" + accYear + "&sims_sip_goal_code=" + goalCode + "&sims_sip_goal_target_code=" + tarCode + "&sims_sip_goal_target_kpi_code=" + kpicode).then(function (goalTargetKpiDisplay) {
                    $scope.goalTarget_Kpi_Display = goalTargetKpiDisplay.data;
                    console.log($scope.goalTarget_Kpi_Display);
                });
            }

            $scope.clear = function () { $scope.temp = ''; }

            $scope.DataEnroll = function () {
                for (var i = 0; i < $scope.student.length; i++) {
                    var t = $scope.student[i].s_enroll_no;
                    var v = document.getElementById(t);
                    if (v.checked == true)

                        $scope.temp = {
                            enroll_number: $scope.student[i].s_enroll_no
                        };
                }
            }

            //$http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
            //    $scope.ComboBoxValues = AllComboBoxValues.data;
            //    console.log($scope.ComboBoxValues);
            //});

            //$http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
            //    $scope.curriculum = AllCurr.data;
            //    console.log($scope.curriculum);

            //});

            //$scope.getacyr = function (str) {
            //    console.log(str);
            //    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
            //        $scope.Academic_year = Academicyear.data;
            //        console.log($scope.Academic_year);
            //    })
            //}

            //$http.get(ENV.apiUrl + "api/common/getAllAcademicYear").then(function (get_AllAcademicYear) {
            //    $scope.ActiveYear = get_AllAcademicYear.data;
            //});

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.goalTargetKPImeasureAchieve, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.goalTargetKPImeasureAchieve;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_sip_goal_target_kpi_measure_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_sip_achievement_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_sip_goal_target_kpi_achievement_category_desc == toSearch) ? true : false;
            }

            $scope.New = function () {
                $scope.AccYearDisabled = false;
                $scope.goalDisabled = false;
                $scope.goalTargetDisabled = false;
                $scope.goalTKpiDisabled = false;
                $scope.disabled = false;
                $scope.readonly = true;
                $scope.table = false;
                $scope.newdisplay = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = "";
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            var datasend = []; var data;
            $scope.savedata = function (Myform) {

                if (Myform) {
                    var data = $scope.temp;
                    data.opr = "I";
                    //data.sims_sip_goal_target_kpi_measure_code = $scope.temp.sims_sip_goal_target_kpi_measure_code_m;

                    //for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    //    if ($scope.filteredTodos[i].sims_enrollment_number != $scope.temp.enroll_number) {


                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/GoalTargetKPIMeasureAchievement/CUDGoalTargetKPIMeasureAchievement", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                        }
                        $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasureAchievement/getAllGoalTargetKPIMeasureAchievement").then(function (res1) {
                            $scope.goalTargetKPImeasureAchieve = res1.data;
                            $scope.totalItems = $scope.goalTargetKPImeasureAchieve.length;
                            $scope.todos = $scope.goalTargetKPImeasureAchieve;
                            $scope.makeTodos();
                        });
                        $scope.temp.sims_goal_kpi_academic_year = "";
                        $scope.temp.sims_kpi_goal_code = "";
                        $scope.temp.sims_kpi_goal_target_code = "";
                        $scope.temp.sims_sip_goal_target_kpi_code = "";
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();
                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.newdisplay = false;
                    // }
                    // }
                    //else {

                    //    swal({ text: 'Record Already Exist', width: 300, height: 300 });
                    //}
                }
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.newdisplay = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.sims_goal_kpi_academic_year = "";
                $scope.temp.sims_kpi_goal_code = "";
                $scope.temp.sims_kpi_goal_target_code = "";
                $scope.temp.sims_sip_goal_target_kpi_code = "";
                $scope.temp.sims_kpi_measure_code = "";
                $scope.temp.sims_sip_achievement_code = "";
                $scope.temp.sims_achievement_category_code = "";
                $scope.temp.sims_sip_goal_target_kpi_achievement_status = "";
            }

            $scope.clear = function () { $scope.temp = ''; }

            $scope.edit = function (str) {
                $scope.AccYearDisabled = true;
                $scope.goalDisabled = true;
                $scope.goalTargetDisabled = true;
                $scope.goalTKpiDisabled = true;
                $scope.table = false;
                $scope.newdisplay = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                debugger
                $scope.temp = {

                    sims_goal_kpi_academic_year: str.sims_sip_academic_year,
                    sims_kpi_goal_code: str.sims_sip_goal_code,
                    sims_kpi_goal_target_code: str.sims_sip_goal_target_code,
                    sims_sip_goal_target_kpi_code: str.sims_sip_goal_target_kpi_code,
                    sims_sip_goal_target_kpi_measure_code_m: str.sims_sip_goal_target_kpi_measure_code_m,
                    sims_sip_goal_target_kpi_measure_desc_m: str.sims_sip_goal_target_kpi_measure_desc_m,
                    sims_sip_goal_target_kpi_measure_code: str.sims_sip_goal_target_kpi_measure_code,
                    sims_sip_goal_target_kpi_measure_desc: str.sims_sip_goal_target_kpi_measure_desc,
                    sims_sip_achievement_code: str.sims_sip_goal_target_kpi_achievement_code,
                    sims_achievement_category_code: str.sims_sip_goal_target_kpi_achievement_category_code,
                    sims_sip_goal_target_kpi_achievement_status: str.sims_sip_goal_target_kpi_achievement_status,
                    old_sims_sip_goal_target_kpi_measure_code: str.sims_sip_goal_target_kpi_measure_code,
                    old_sims_sip_goal_target_kpi_achievement_code: str.sims_sip_goal_target_kpi_achievement_code,

                };
                $scope.getGoalTarget(str.sims_sip_goal_code);
                $scope.getGoalTargetKPI(str.sims_sip_goal_code, str.sims_sip_goal_target_code);
            }

            var dataupdate = []; var data;
            $scope.update = function (Myform) {
                if (Myform) {
                    var data = $scope.temp;
                    data.opr = "U";
                    dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/GoalTargetKPIMeasureAchievement/CUDGoalTargetKPIMeasureAchievement", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;

                        $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasureAchievement/getAllGoalTargetKPIMeasureAchievement").then(function (res1) {
                            $scope.goalTargetKPImeasureAchieve = res1.data;
                            $scope.totalItems = $scope.goalTargetKPImeasureAchieve.length;
                            $scope.todos = $scope.goalTargetKPImeasureAchieve;
                            $scope.makeTodos();

                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                            }
                        });
                    });
                    dataupdate = [];
                    $scope.table = true;
                    $scope.newdisplay = false;
                }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_kpi_goal_code': $scope.filteredTodos[i].sims_sip_goal_code,
                            'sims_kpi_goal_target_code': $scope.filteredTodos[i].sims_sip_goal_target_code,
                            'sims_sip_goal_target_kpi_code': $scope.filteredTodos[i].sims_sip_goal_target_kpi_code,
                            'sims_sip_goal_target_kpi_measure_code_m': $scope.filteredTodos[i].sims_sip_goal_target_kpi_measure_code,
                            'sims_sip_achievement_code': $scope.filteredTodos[i].sims_sip_goal_target_kpi_achievement_code,
                            'opr': "D"
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/GoalTargetKPIMeasureAchievement/CUDGoalTargetKPIMeasureAchievement", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasureAchievement/getAllGoalTargetKPIMeasureAchievement").then(function (res1) {
                                                $scope.goalTargetKPImeasureAchieve = res1.data;
                                                $scope.totalItems = $scope.goalTargetKPImeasureAchieve.length;
                                                $scope.todos = $scope.goalTargetKPImeasureAchieve;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted ", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasureAchievement/getAllGoalTargetKPIMeasureAchievement").then(function (res1) {
                                                $scope.goalTargetKPImeasureAchieve = res1.data;
                                                $scope.totalItems = $scope.goalTargetKPImeasureAchieve.length;
                                                $scope.todos = $scope.goalTargetKPImeasureAchieve;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

        }])

})();
