﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ActiveUserCont',
        ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.display = true;
            $scope.grid = true;
            $scope.itemsPerPage = '10';
            $scope.currentPage = 0;
            var str, cnt;
            $scope.getgriddata = [];
            var main;
            $scope.btn_save = true;
            $scope.btn_delete = false;
            var login_id = [];
            var date = new Date();
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss');

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            if ($http.defaults.headers.common['schoolId'] == 'siserp') {
                $scope.hidebtn = true;
            }
            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                // console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            //var maindata = [];
            $scope.getsearch_details = function (user, type, name) {
                var maindata = [];

                $scope.btn_save = false;
                $scope.filteredTodos = [];
                $scope.maindata = [];
                $scope.totalItems = [];
                $scope.todos = [];
                var data = {};
                $scope.getgriddata = [];

                if (name == undefined) {
                    name = "";
                }

                ///   Employee
                if (user == 'emp') {
                    if (type = 'firstname') {
                        $scope.maindata = [];
                        $scope.filteredTodos = [];
                        $http.post(ENV.apiUrl + "api/common/ActiveUser/Search_Employee_by_first_name?first_name=" + name).then(function (res) {
                            $scope.getgriddata = res.data;

                            for (var i = 0; i < $scope.getgriddata.length; i++) {
                                var d1 = new Date();
                                var month = d1.getMonth() + 1;
                                var day = d1.getDate();
                                if (month < 10)
                                    month = "0" + month;
                                if (day < 10)
                                    day = "0" + day;
                                $scope.todaydate1 = d1.getFullYear() + "-" + (month) + "-" + (day);

                                var data = {
                                    login_id: $scope.getgriddata[i].login_id,
                                    activation_date: $scope.todaydate1,
                                    status: $scope.getgriddata[i].status,
                                    user_name: $scope.getgriddata[i].user_name,
                                };

                                maindata.push(data);
                            }


                            $scope.totalItems = maindata.length;
                            $scope.todos = maindata;
                            $scope.makeTodos();

                        });
                    }
                    if (type == 'lastname') {
                        $http.post(ENV.apiUrl + "api/common/ActiveUser/Search_Employee_by_last_name?last_name=" + name).then(function (res) {
                            $scope.getgriddata = res.data;

                            for (var i = 0; i < $scope.getgriddata.length; i++) {
                                var d1 = new Date();
                                var month = d1.getMonth() + 1;
                                var day = d1.getDate();
                                if (month < 10)
                                    month = "0" + month;
                                if (day < 10)
                                    day = "0" + day;
                                $scope.todaydate1 = d1.getFullYear() + "-" + (month) + "-" + (day);

                                var data = {
                                    login_id: $scope.getgriddata[i].login_id,
                                    activation_date: $scope.todaydate1,
                                    status: $scope.getgriddata[i].status,
                                    user_name: $scope.getgriddata[i].user_name,
                                };

                                maindata.push(data);
                            }
                            $scope.totalItems = maindata.length;
                            $scope.todos = maindata;
                            $scope.makeTodos();
                            //console.log($scope.getgriddata);
                            //$scope.totalItems = $scope.getgriddata.length;
                            //$scope.todos = $scope.getgriddata;
                            //$scope.makeTodos();
                        });
                    }
                    if (type == 'loginid') {
                        $http.post(ENV.apiUrl + "api/common/ActiveUser/Search_Employee_by_login_id?login=" + name).then(function (res) {
                            $scope.getgriddata = res.data;
                            for (var i = 0; i < $scope.getgriddata.length; i++) {
                                var d1 = new Date();
                                var month = d1.getMonth() + 1;
                                var day = d1.getDate();
                                if (month < 10)
                                    month = "0" + month;
                                if (day < 10)
                                    day = "0" + day;
                                $scope.todaydate1 = d1.getFullYear() + "-" + (month) + "-" + (day);

                                var data = {
                                    login_id: $scope.getgriddata[i].login_id,
                                    activation_date: $scope.todaydate1,
                                    status: $scope.getgriddata[i].status,
                                    user_name: $scope.getgriddata[i].user_name,
                                };

                                maindata.push(data);
                            }


                            $scope.totalItems = maindata.length;
                            $scope.todos = maindata;
                            $scope.makeTodos();
                            //console.log($scope.getgriddata);
                            //$scope.totalItems = $scope.getgriddata.length;
                            //$scope.todos = $scope.getgriddata;
                            //$scope.makeTodos();
                        });
                    }

                }
                ///student
                if (user == 'student') {
                    if (type == 'firstname') {
                        $scope.filteredTodos = [];
                        $scope.maindata = [];
                        $http.post(ENV.apiUrl + "api/common/ActiveUser/Search_Student_by_first_name?first_name=" + name).then(function (res) {
                            $scope.getgriddata = res.data;
                            for (var i = 0; i < $scope.getgriddata.length; i++) {
                                var d1 = new Date();
                                var month = d1.getMonth() + 1;
                                var day = d1.getDate();
                                if (month < 10)
                                    month = "0" + month;
                                if (day < 10)
                                    day = "0" + day;
                                $scope.todaydate1 = d1.getFullYear() + "-" + (month) + "-" + (day);

                                data = {
                                    login_id: $scope.getgriddata[i].login_id,
                                    activation_date: $scope.todaydate1,
                                    status: $scope.getgriddata[i].status,
                                    user_name: $scope.getgriddata[i].user_name,
                                };

                                maindata.push(data);
                            }
                            $scope.totalItems = maindata.length;
                            $scope.todos = maindata;
                            $scope.makeTodos();
                            //  $scope.filteredTodos = maindata;
                            //console.log($scope.getgriddata);
                            //$scope.totalItems = $scope.getgriddata.length;
                            //$scope.todos = $scope.getgriddata;
                            //$scope.makeTodos();
                        });
                    }
                    if (type == 'lastname') {
                        $http.post(ENV.apiUrl + "api/common/ActiveUser/Search_Student_by_last_name?last_name=" + name).then(function (res) {
                            $scope.getgriddata = res.data;
                            for (var i = 0; i < $scope.getgriddata.length; i++) {
                                var d1 = new Date();
                                var month = d1.getMonth() + 1;
                                var day = d1.getDate();
                                if (month < 10)
                                    month = "0" + month;
                                if (day < 10)
                                    day = "0" + day;
                                $scope.todaydate1 = d1.getFullYear() + "-" + (month) + "-" + (day);

                                data = {
                                    login_id: $scope.getgriddata[i].login_id,
                                    activation_date: $scope.todaydate1,
                                    status: $scope.getgriddata[i].status,
                                    user_name: $scope.getgriddata[i].user_name,
                                };

                                maindata.push(data);
                            }
                            $scope.totalItems = maindata.length;
                            $scope.todos = maindata;
                            $scope.makeTodos();
                            //console.log($scope.getgriddata);
                            //$scope.totalItems = $scope.getgriddata.length;
                            //$scope.todos = $scope.getgriddata;
                            //$scope.makeTodos();
                        });
                    }
                    if (type == 'loginid') {
                        $http.post(ENV.apiUrl + "api/common/ActiveUser/Search_Student_by_login_id?login=" + name).then(function (res) {
                            $scope.getgriddata = res.data;
                            for (var i = 0; i < $scope.getgriddata.length; i++) {
                                var d1 = new Date();
                                var month = d1.getMonth() + 1;
                                var day = d1.getDate();
                                if (month < 10)
                                    month = "0" + month;
                                if (day < 10)
                                    day = "0" + day;
                                $scope.todaydate1 = d1.getFullYear() + "-" + (month) + "-" + (day);

                                var data = {
                                    login_id: $scope.getgriddata[i].login_id,
                                    activation_date: $scope.todaydate1,
                                    status: $scope.getgriddata[i].status,
                                    user_name: $scope.getgriddata[i].user_name,
                                };

                                maindata.push(data);
                            }
                            $scope.totalItems = maindata.length;
                            $scope.todos = maindata;
                            $scope.makeTodos();
                            //console.log($scope.getgriddata);
                            //$scope.totalItems = $scope.getgriddata.length;
                            //$scope.todos = $scope.getgriddata;
                            //$scope.makeTodos();
                        });
                    }
                }

                ///   Parent
                if (user == 'parent') {
                    if (type == 'firstname') {
                        $http.post(ENV.apiUrl + "api/common/ActiveUser/Search_Parent_by_first_name?first_name=" + name).then(function (res) {
                            $scope.getgriddata = res.data;
                            for (var i = 0; i < $scope.getgriddata.length; i++) {
                                var d1 = new Date();
                                var month = d1.getMonth() + 1;
                                var day = d1.getDate();
                                if (month < 10)
                                    month = "0" + month;
                                if (day < 10)
                                    day = "0" + day;
                                $scope.todaydate1 = d1.getFullYear() + "-" + (month) + "-" + (day);

                                data = {
                                    login_id: $scope.getgriddata[i].login_id,
                                    activation_date: $scope.todaydate1,
                                    status: $scope.getgriddata[i].status,
                                    user_name: $scope.getgriddata[i].user_name,
                                };

                                maindata.push(data);
                            }
                            $scope.totalItems = maindata.length;
                            $scope.todos = maindata;
                            $scope.makeTodos();
                            //console.log($scope.getgriddata);
                            //$scope.totalItems = $scope.getgriddata.length;
                            //$scope.todos = $scope.getgriddata;
                            //$scope.makeTodos();
                        });
                    }
                    if (type == 'lastname') {
                        $http.post(ENV.apiUrl + "api/common/ActiveUser/Search_Parent_by_last_name?last_name=" + name).then(function (res) {
                            $scope.getgriddata = res.data;
                            for (var i = 0; i < $scope.getgriddata.length; i++) {
                                var d1 = new Date();
                                var month = d1.getMonth() + 1;
                                var day = d1.getDate();
                                if (month < 10)
                                    month = "0" + month;
                                if (day < 10)
                                    day = "0" + day;
                                $scope.todaydate1 = d1.getFullYear() + "-" + (month) + "-" + (day);

                                var data = {
                                    login_id: $scope.getgriddata[i].login_id,
                                    activation_date: $scope.todaydate1,
                                    status: $scope.getgriddata[i].status,
                                    user_name: $scope.getgriddata[i].user_name,
                                };

                                maindata.push(data);
                            }
                            $scope.totalItems = maindata.length;
                            $scope.todos = maindata;
                            $scope.makeTodos();
                            //console.log($scope.getgriddata);
                            //$scope.totalItems = $scope.getgriddata.length;
                            //$scope.todos = $scope.getgriddata;
                            //$scope.makeTodos();
                        });
                    }
                    if (type == 'loginid') {
                        $http.post(ENV.apiUrl + "api/common/ActiveUser/Search_Parent_by_login_id?login=" + name).then(function (res) {
                            $scope.getgriddata = res.data;

                            for (var i = 0; i < $scope.getgriddata.length; i++) {
                                var d1 = new Date();
                                var month = d1.getMonth() + 1;
                                var day = d1.getDate();
                                if (month < 10)
                                    month = "0" + month;
                                if (day < 10)
                                    day = "0" + day;
                                $scope.todaydate1 = d1.getFullYear() + "-" + (month) + "-" + (day);

                                var data = {
                                    login_id: $scope.getgriddata[i].login_id,
                                    activation_date: $scope.todaydate1,
                                    status: $scope.getgriddata[i].status,
                                    user_name: $scope.getgriddata[i].user_name,
                                };

                                maindata.push(data);
                            }
                            $scope.totalItems = maindata.length;
                            $scope.todos = maindata;
                            $scope.makeTodos();
                            //console.log($scope.getgriddata);
                            //$scope.totalItems = $scope.getgriddata.length;
                            //$scope.todos = $scope.getgriddata;
                            //$scope.makeTodos();
                        });
                    }
                }

            }

            function convertdate(dt) {
                if (dt != null) {
                    var d1 = new Date(dt);
                    var month = d1.getMonth() + 1;
                    var day = d1.getDate();
                    if (month < 10)
                        month = "0" + month;
                    if (day < 10)
                        day = "0" + day;
                    var d = d1.getFullYear() + "-" + (month) + "-" + (day);

                    console.log(d);

                    ///$scope.active_user = { activation_date: d };
                    $scope.convertdated = d;

                    return d;
                }
            }

            $scope.checkdate = function (dt) {
                if (dt != null) {
                    var d1 = new Date(dt);
                    var month = d1.getMonth() + 1;
                    var day = d1.getDate();
                    if (month < 10)
                        month = "0" + month;
                    if (day < 10)
                        day = "0" + day;
                    var d = d1.getFullYear() + "-" + (month) + "-" + (day);

                    console.log(d);

                    $scope.active_user = { activation_date: d };
                }
            }

            $scope.Save = function (isvalidate) {
                var data1 = [];
                var data = [];
                $scope.emp = false;
                $scope.parent = false;
                $scope.student = false;
                if (isvalidate) {
                    console.log($rootScope.globals.currentUser.username);
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        if ($scope.filteredTodos[i].login_id1 == true) {
                            if ($scope.edt.sims_user == 'emp') {
                                var data = {
                                    login_id: $scope.filteredTodos[i].login_id,
                                    activation_date: convertdate($scope.filteredTodos[i].activation_date),
                                    sims_student_status_updation_user_code: $rootScope.globals.currentUser.username
                                };
                                $scope.emp = true;
                                data1.push(data);
                            }
                            if ($scope.edt.sims_user == 'student') {
                                var data = {
                                    login_id: $scope.filteredTodos[i].login_id,
                                    activation_date: convertdate($scope.filteredTodos[i].activation_date),
                                    sims_student_status_updation_user_code: $rootScope.globals.currentUser.username
                                };
                                $scope.student = true;
                                data1.push(data);
                            }
                            if ($scope.edt.sims_user == 'parent') {
                                var data = {
                                    login_id: $scope.filteredTodos[i].login_id,
                                    activation_date: convertdate($scope.filteredTodos[i].activation_date),
                                    sims_student_status_updation_user_code: $rootScope.globals.currentUser.username
                                };
                                $scope.parent = true;
                                data1.push(data);
                            }

                        }
                    }

                    if ($scope.parent) {
                        $http.post(ENV.apiUrl + "api/common/ActiveUser/Insert_Parent_activation_history", data1).then(function (res) {
                            $scope.getparent = res.data;

                            if ($scope.getparent == true) {
                                swal({ title: "Alert", text: "Active User Mapped Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getsearch_details($scope.edt.sims_user, $scope.edt.sims_type, $scope.edt.sims_name);
                                    }
                                });
                            }
                            else {
                                swal({ title: "Alert", text: "Active User Not Mapped Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getsearch_details($scope.edt.sims_user, $scope.edt.sims_type, $scope.edt.sims_name);
                                    }
                                });
                            }

                        });
                    }
                    if ($scope.emp) {
                        $http.post(ENV.apiUrl + "api/common/ActiveUser/Insert_Employee_activation_history", data1).then(function (res) {
                            $scope.getEmp = res.data;

                            if ($scope.getEmp == true) {
                                swal({ title: "Alert", text: "Active User Mapped Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getsearch_details($scope.edt.sims_user, $scope.edt.sims_type, $scope.edt.sims_name);
                                    }
                                });
                            }
                            else {
                                swal({ title: "Alert", text: "Active User Not Mapped Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getsearch_details($scope.edt.sims_user, $scope.edt.sims_type, $scope.edt.sims_name);
                                    }
                                });
                            }

                        });
                    }
                    if ($scope.student) {
                        $http.post(ENV.apiUrl + "api/common/ActiveUser/Insert_Student_activation_history", data1).then(function (res) {
                            $scope.getStudent = res.data;

                            if ($scope.getStudent == true) {
                                swal({ title: "Alert", text: "Active User Mapped Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getsearch_details($scope.edt.sims_user, $scope.edt.sims_type, $scope.edt.sims_name);
                                    }
                                });
                            }
                            else {
                                swal({ title: "Alert", text: "Active User Not Mapped Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getsearch_details($scope.edt.sims_user, $scope.edt.sims_type, $scope.edt.sims_name);
                                    }
                                });
                            }

                        });
                    }

                }
                else
                {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
            }


            $scope.Cancel = function () {
                $scope.edt = "";
                $scope.filteredTodos = "";
            }

            $scope.get_msgtype = function (active_user) {
                if (active_user == 'emp') {
                    $scope.edt.sims_name = "";
                    $scope.edt.sims_student = '';
                    $scope.edt.sims_parent = '';
                }
                else if (active_user == 'parent') {
                    $scope.edt.sims_name = "";
                    $scope.edt.sims_student = '';
                    $scope.edt.sims_Emp = '';
                }
                else if (active_user == 'student') {
                    $scope.edt.sims_name = "";
                    $scope.edt.sims_Emp = '';
                    $scope.edt.sims_parent = '';
                }
            }

            $scope.get_ischecked = function (active_user) {
                active_user.ischecked = true;

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }

            }

            $scope.get_nametypechecked = function (name) {
                if (name == 'firstname') {
                    $scope.edt.sims_name = "";
                    $scope.edt.sims_last_name = '';
                    $scope.edt.sims_login_id = '';
                }
                if (name == 'lastname') {
                    $scope.edt.sims_name = "";
                    $scope.edt.sims_first_name = '';
                    $scope.edt.sims_login_id = '';
                }
                if (name == 'loginid') {
                    $scope.edt.sims_name = "";
                    $scope.edt.sims_last_name = '';
                    $scope.edt.sims_first_name = '';
                }
            }


            $scope.checkAll = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

                //login_id = [];
                //main = document.getElementById('mainchk');

                //if (main.checked == true)
                //{
                //    for (var i = 0; i < $scope.filteredTodos.length; i++)
                //    {
                //        var t = $scope.filteredTodos[i].login_id;
                //        var v = document.getElementById(t + i);

                //        if ($scope.filteredTodos[i].login_id != "")
                //        {
                //            v.checked = true;
                //            login_id = login_id + $scope.filteredTodos[i].login_id + ',';
                //        }
                //    }
                //}
                //else
                //{

                //    for (var i = 0; i < $scope.filteredTodos.length; i++)
                //    {
                //        var t = $scope.filteredTodos[i].login_id;
                //        var v = document.getElementById(t+i);
                //        v.checked = false;
                //        main.checked = false;
                //        $scope.row1 = '';
                //        login_id = [];
                //    }
                //}
                //console.log(login_id);
            }


            $('*[data-timepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true

            });

            $(document).on('touch click', '*[data-timepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
                //$('input[type="text"]').css('z-index', 99999999999999);
            });

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
            }



        }])


})();