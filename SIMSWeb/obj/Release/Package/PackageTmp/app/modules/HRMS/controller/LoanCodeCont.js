﻿(function () {
    'use strict';
    var opr = '';
    var loancode = [];
    var main;

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('LoanCodeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';


            $scope.table1 = true;
            $http.get(ENV.apiUrl + "api/LoanCodeDetails/getAllLoanCode").then(function (getAllLoanCode_Data) {
                $scope.AllLoanCode_Data = getAllLoanCode_Data.data;
                $scope.totalItems = $scope.AllLoanCode_Data.length;
                $scope.todos = $scope.AllLoanCode_Data;
                $scope.makeTodos();
                console.log($scope.AllLoanCode_Data);
            });

            $http.get(ENV.apiUrl + "api/common/getCompanyName").then(function (getCompanyName_Data) {
                $scope.CompanyName_Data = getCompanyName_Data.data;
            });

            $http.get(ENV.apiUrl + "api/common/getAllLedger").then(function (getAllLedger_Data) {
                $scope.AllLedger_Data = getAllLedger_Data.data;
                console.log($scope.AllLedger_Data);
            });

            $http.get(ENV.apiUrl + "api/common/getCreditDebit").then(function (getCreditDebit_Data) {
                $scope.CreditDebit_Data = getCreditDebit_Data.data;
            });

            $scope.operation = false;
            $scope.editmode = false;
            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }
            $scope.edt = "";

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.editmode = false;
                opr = 'S';
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
              

            }

            $scope.up = function (str) {
                opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.operation = true;

                $scope.edt = str;

            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
            }

            $scope.Save = function (myForm) {
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'I';
                    $scope.exist = false;
                    for (var i = 0; i < $scope.AllLoanCode_Data.length; i++) {
                        if ($scope.AllLoanCode_Data[i].lc_code == data.lc_code) {
                            $scope.exist = true;
                        }

                    }
                    if ($scope.exist) {
                        $rootScope.strMessage = 'Display order Already exists';

                        $('#message').modal('show');
                    }

                    else {

                      debugger
                      $http.post(ENV.apiUrl + "api/LoanCodeDetails/LoanCodeCUD", data).then(function (msg) {
                            $scope.msg1 = msg.data;
                            $scope.operation = false;
                            $http.get(ENV.apiUrl + "api/LoanCodeDetails/getAllLoanCode").then(function (getAllLoanCode_Data) {
                                $scope.AllLoanCode_Data = getAllLoanCode_Data.data;
                                $scope.totalItems = $scope.AllLoanCode_Data.length;
                                $scope.todos = $scope.AllLoanCode_Data;
                                $scope.makeTodos();

                                $scope.msg1 = msg.data;

                               
                                if ($scope.msg1 == true) {

                                    $rootScope.strMessage = 'Record Inserted Successfully';

                                    $('#message').modal('show');
                                }
                                else {

                                    $rootScope.strMessage = 'Record not Inserted';

                                    $('#message').modal('show');
                                }

                            });

                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                }

            }

            $scope.Update = function () {

                var data = $scope.edt;
                data.opr = 'U';

                $http.post(ENV.apiUrl + "api/LoanCodeDetails/LoanCodeCUD", data).then(function (msg) {
                    $scope.msg1 = msg.data;

                    $scope.operation = false;
                    $http.get(ENV.apiUrl + "api/LoanCodeDetails/getAllLoanCode").then(function (getAllLoanCode_Data) {
                        $scope.AllLoanCode_Data = getAllLoanCode_Data.data;
                        $scope.totalItems = $scope.AllLoanCode_Data.length;
                        $scope.todos = $scope.AllLoanCode_Data;
                        $scope.makeTodos();
                        if ($scope.msg1 == true) {

                            $rootScope.strMessage = 'Record Updated Successfully';

                            $('#message').modal('show');
                        }
                        else {


                            $rootScope.strMessage = 'Record Not Updated';

                            $('#message').modal('show');
                        }
                    });

                })


                $scope.operation = false;
                $scope.table1 = true;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].lc_code;
                        var v = document.getElementById(t);
                        v.checked = true;
                        loancode = loancode + $scope.filteredTodos[i].lc_code + ','
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].lc_code;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
            }

            $scope.deleterecord = function () {

                loancode = [];

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].lc_code;
                    var v = document.getElementById(t);

                    if (v.checked == true)

                        loancode = loancode + $scope.filteredTodos[i].lc_code + ',';
                }

                var deletelocacode = ({
                    'lc_code': loancode,
                    'opr': 'D'
                });

                $http.post(ENV.apiUrl + "api/LoanCodeDetails/LoanCodeCUD", deletelocacode).then(function (msg) {
                    $scope.msg1 = msg.data;
                    $scope.table1 = true;
                    $scope.operation = false;
                    $rootScope.strMessage = $scope.msg1.strMessage;
                    $http.get(ENV.apiUrl + "api/LoanCodeDetails/getAllLoanCode").then(function (getAllLoanCode_Data) {
                        $scope.AllLoanCode_Data = getAllLoanCode_Data.data;
                        $scope.totalItems = $scope.AllLoanCode_Data.length;
                        $scope.todos = $scope.AllLoanCode_Data;
                        $scope.makeTodos();
                        if ($scope.msg1 == true) {

                            $rootScope.strMessage = 'Record Deleted Successfully';

                            $('#message').modal('show');
                        }
                        else {


                            $rootScope.strMessage = 'Record Not Deleted';

                            $('#message').modal('show');
                        }

                    });
                });

            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.AllLoanCode_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.AllLoanCode_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                     item.companyName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.ledgerName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.lc_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.lc_debit_acno_nm.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.lc_code == toSearch) ? true : false;
            }


        }])
})();
