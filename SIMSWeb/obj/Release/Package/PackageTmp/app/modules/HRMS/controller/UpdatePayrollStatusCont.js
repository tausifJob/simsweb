﻿(function () {
    'use strict';
    var main = '';
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('UpdatePayrollStatusCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.busyindicator = false;
            $scope.grid = true;
            $scope.color = '#edefef';
            $scope.rgvtbl = true;






            $http.get(ENV.apiUrl + "api/updatepayrollstatus/GetYearMonthStatus").then(function (GetYearMonthStatus) {

                $scope.GetYearMonthStatus = GetYearMonthStatus.data;


            });

            $scope.updatestatus = function (str) {



                $http.post(ENV.apiUrl + "api/updatepayrollstatus/UpdatePayrollStatus?year_month=" + str.us_year_month_code + "&status=" + str.us_status).then(function (UpdatePayrollStatus) {
                    $scope.UpdatePayrollStatus = UpdatePayrollStatus.data;

                });


            }

            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);









        }])
})();