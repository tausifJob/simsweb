﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('EmployeeLeaveApplicationCont',
        ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.display = true;
            $scope.grid = true;
            $scope.div_pendingview = true;
            $scope.div_trans = false;
            $scope.div_emp_search = false;
            var data1 = [];
            var data = [];
            $scope.year_data = [];
            $scope.div_pending = false;
            $scope.enroll_number_lists = [];
            $scope.Holileave_data = [];
            $scope.test_emp = [];
            $scope.role_data = [];
            $scope.emprole_data = [];
            var main;
            var date = new Date();
            $scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');

            $scope.username = $rootScope.globals.currentUser.username;
            $rootScope.visible_stud = false;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = true;
            $rootScope.chkMulti = false;


            var get_dates = [];
            var forbidden = [];
            var dates = [];
            var newd = [];

            $timeout(function () {
                $("#fixedtable,#fixedtable1,#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

           

            $scope.$on('global_cancel', function (str) {
                console.log($scope.SelectedUserLst);
                $scope.Empmodal_cancel();
            });

            $scope.getEmployee1 = function () {
                $scope.edt = '';
                $scope.leavebal_data = '';
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }

            $scope.getDate = function () {
                var st = document.getElementById('dp_start_date');
                st.setAttribute('data-date-start-date', $scope.yearcurrentstart);
                st.setAttribute('data-date-end-date', $scope.yearcurrentyend);

                var end = document.getElementById('dp_endadmdate');
                end.setAttribute('data-date-start-date', $scope.yearcurrentstart);
                end.setAttribute('data-date-end-date', $scope.yearcurrentyend);
            }

            $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_Current_AcademicYear").then(function (res) {
                $scope.year_data = res.data;
                $scope.yearcurrentstart = $scope.year_data.yearcurrentstart;
                $scope.yearcurrentyend = $scope.year_data.yearcurrentyend;
                $scope.getDate();
            });

            $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_RoleDetails").then(function (res) {
                $scope.role_data = res.data;

                $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_empRoleDetails?emp_id=" + $scope.username).then(function (res) {
                    $scope.emprole_data = res.data;
                    for (var i = 0; i < $scope.role_data.length; i++) {
                        for (var j = 0; j < $scope.emprole_data.length; j++) {
                            if ($scope.role_data[i].sims_param_role_field == $scope.emprole_data[j].sims_param_role_id) {
                                $scope.div_emp_search = true;
                            }
                        }
                    }
                });

            });

            $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_EmpInfo?emp_id=" + $scope.username).then(function (res) {
                $scope.emp_data = res.data;
                if (!jQuery.isEmptyObject($scope.emp_data)) {
                    $scope.comp_code = $scope.emp_data[0].comp_code;
                    $scope.dep_code = $scope.emp_data[0].dept_no;

                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_EMPLeaveDetails?compcode=" + $scope.emp_data[0].comp_code + "&employee_id=" + $scope.username).then(function (res) {
                        $scope.leaveType_data = res.data;
                    });

                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_EMPAllLeaveDates?compcode=" + $scope.comp_code + "&employee_id=" + $scope.username).then(function (res) {
                        $scope.leavedate_data = res.data;

                        for (var j = 0; j < $scope.leavedate_data.length; j++) {
                            var dates = getDates(new Date($scope.leavedate_data[j].lt_start_date), new Date($scope.leavedate_data[j].lt_end_date));
                            get_dates.push(dates);
                        }

                        for (var k = 0; k <= get_dates.length; k++) {
                            newd = get_dates[k];
                            for (var l = 0; l < newd.length; l++) {
                                var date = new Date(newd[l]);
                                var month = (date.getMonth() + 1);
                                var day = date.getDate();
                                $scope.d = date.getFullYear() + '-' + parseInt(month) + '-' + parseInt(day);
                                forbidden.push($scope.d);

                                $('#dp_start_date').datepicker({
                                    beforeShowDay: function (Date) {
                                        var curr_day = Date.getDate();
                                        var curr_month = Date.getMonth() + 1;
                                        var curr_year = Date.getFullYear();
                                        var curr_date = curr_year + '-' + parseInt(curr_month) + '-' + parseInt(curr_day)
                                        if (!jQuery.isEmptyObject(forbidden)) {
                                            if (forbidden.indexOf(curr_date) > -1) return false;
                                        }
                                    }
                                });

                                $('#dp_endadmdate').datepicker({
                                    beforeShowDay: function (Date) {
                                        var curr_day = Date.getDate();
                                        var curr_month = Date.getMonth() + 1;
                                        var curr_year = Date.getFullYear();
                                        var curr_date = curr_year + '-' + parseInt(curr_month) + '-' + parseInt(curr_day)
                                        if (!jQuery.isEmptyObject(forbidden)) {
                                            if (forbidden.indexOf(curr_date) > -1) return false;
                                        }
                                    }
                                });
                            };
                            // forbidden = ["2015-12-1", "2015-08-2", "2015-08-3", "2015-08-4", "2015-08-5", "2015-08-6", "2015-08-7", "2015-08-8", "2015-08-9", "2015-08-10"];

                        }
                    });

                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_empHolidayDetails?emp_id=" + $scope.username).then(function (res) {
                        $scope.Holileave_data = res.data;
                    });
                }
            });

            $scope.Empmodal_cancel = function () {
                for (var i = 0; i < $scope.SelectedUserLst.length; i++) {
                    //if ($scope.SelectedUserLst[i].em_login_code1 == true)
                    // {
                    $scope.enroll_number_lists.push($scope.SelectedUserLst[i]);
                    $scope.test_emp = $scope.SelectedUserLst[i];
                    $scope.emp_name = $scope.test_emp.em_login_code;
                    // }
                }

                $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_EmpInfo?emp_id=" + $scope.emp_name).then(function (res) {
                    $scope.emp_data = res.data;
                    if (!jQuery.isEmptyObject($scope.emp_data)) {
                        $scope.comp_code = $scope.emp_data[0].comp_code;
                        $scope.dep_code = $scope.emp_data[0].dept_no;

                        $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_EMPLeaveDetails?compcode=" + $scope.emp_data[0].comp_code + "&employee_id=" + $scope.emp_name).then(function (res) {
                            $scope.leaveType_data = res.data;
                        });

                        $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_EMPAllLeaveDates?compcode=" + $scope.emp_data[0].comp_code + "&employee_id=" + $scope.emp_name).then(function (res) {
                            $scope.leavedate_data = res.data;

                            for (var j = 0; j < $scope.leavedate_data.length; j++) {
                                var dates = getDates(new Date($scope.leavedate_data[j].lt_start_date), new Date($scope.leavedate_data[j].lt_end_date));
                                get_dates.push(dates);
                            }

                            for (var k = 0; k <= get_dates.length; k++) {
                                newd = get_dates[k];
                                for (var l = 0; l < newd.length; l++) {
                                    var date = new Date(newd[l]);
                                    var month = (date.getMonth() + 1);
                                    var day = date.getDate();
                                    $scope.d = date.getFullYear() + '-' + parseInt(month) + '-' + parseInt(day);
                                    forbidden.push($scope.d);

                                    $('#dp_start_date').datepicker({
                                        beforeShowDay: function (Date) {
                                            var curr_day = Date.getDate();
                                            var curr_month = Date.getMonth() + 1;
                                            var curr_year = Date.getFullYear();
                                            var curr_date = curr_year + '-' + parseInt(curr_month) + '-' + parseInt(curr_day)
                                            if (!jQuery.isEmptyObject(forbidden)) {
                                                if (forbidden.indexOf(curr_date) > -1) return false;
                                            }
                                        }
                                    });

                                    $('#dp_endadmdate').datepicker({
                                        beforeShowDay: function (Date) {
                                            var curr_day = Date.getDate();
                                            var curr_month = Date.getMonth() + 1;
                                            var curr_year = Date.getFullYear();
                                            var curr_date = curr_year + '-' + parseInt(curr_month) + '-' + parseInt(curr_day)
                                            if (!jQuery.isEmptyObject(forbidden)) {
                                                if (forbidden.indexOf(curr_date) > -1) return false;
                                            }
                                        }
                                    });
                                };
                                // forbidden = ["2015-12-1", "2015-08-2", "2015-08-3", "2015-08-4", "2015-08-5", "2015-08-6", "2015-08-7", "2015-08-8", "2015-08-9", "2015-08-10"];

                            }
                        });

                        $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_empHolidayDetails?emp_id=" + $scope.username).then(function (res) {
                            $scope.Holileave_data = res.data;
                        });
                    }
                });

                // $scope.EmployeeDetails = [];
            }

            var getDates = function (startDate, endDate) {
                dates = [];
                var currentDate = startDate,
                 addDays = function (days) {
                     var date = new Date(this.valueOf());
                     date.setDate(date.getDate() + days);
                     return date;
                 };
                while (currentDate <= endDate) {
                    currentDate = $filter('date')(currentDate, "yyyy-MM-dd");

                    var datadate = {
                        checkdates: currentDate
                    }

                    if ($scope.checkeddateflag == true) {
                        dates.push(datadate);
                    }
                    else {
                        dates.push(currentDate);
                    }
                    currentDate = addDays.call(currentDate, 1);
                }
                return dates;
            };

            $scope.getLeaveBalance = function (leave_code) {
                if ($scope.div_emp_search == false) {
                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_EMPLeaveMaxDays?le_leave_code=" + leave_code + "&compcode=" + $scope.comp_code + "&employee_id=" + $scope.username).then(function (res) {
                        $scope.leavebal_data = res.data;
                    });

                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/GetLeaveDetails?emp_code=" + $scope.username + "&comp_code=" + $scope.comp_code).then(function (res) {
                        $scope.leavedeatails_data = res.data;
                        if ($scope.leavedeatails_data.length > 0) {
                            $scope.div_pending = true;
                        }
                    });
                }
                else {
                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_EMPLeaveMaxDays?le_leave_code=" + leave_code + "&compcode=" + $scope.comp_code + "&employee_id=" + $scope.emp_name).then(function (res) {
                        $scope.leavebal_data = res.data;
                    });

                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/GetLeaveDetails?emp_code=" + $scope.emp_name + "&comp_code=" + $scope.comp_code).then(function (res) {
                        $scope.leavedeatails_data = res.data;
                        if ($scope.leavedeatails_data.length > 0) {
                            $scope.div_pending = true;
                        }
                    });
                }
            }

            $scope.UserSearch = function () {
                $('#Employee').modal('show');
                $scope.div_pendingview = true;
                $scope.div_trans = false;

                if ($scope.div_emp_search == false) {
                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/GetLeaveDetails?emp_code=" + $scope.username + "&comp_code=" + $scope.comp_code).then(function (res) {
                        $scope.leavedeatails_data = res.data;
                    });
                }
                else {
                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/GetLeaveDetails?emp_code=" + $scope.emp_name + "&comp_code=" + $scope.comp_code).then(function (res) {
                        $scope.leavedeatails_data = res.data;
                    });
                }
            }

            $scope.viewDetails = function (lt_no) {
               
                $http.get(ENV.apiUrl + "api/common/EmployeeLeave/GetStatusofTransactionforEmployee?transactionId=" + lt_no).then(function (res) {
                    $scope.Empleavedeatails_data = res.data;
                    $scope.div_pendingview = false;
                    $scope.div_trans = true;
                });
            }

            $scope.edt =
           {
               sims_start_chk: false,
               sims_end_chk: false
           }

            $scope.getDateLeaves = function () {
                $scope.checkeddateflag = true;

                if (($scope.edt.lt_start_date != '' || $scope.edt.lt_start_date != undefined) && ($scope.edt.lt_end_date != '' || $scope.edt.lt_start_date != undefined)) {
                    if ($scope.edt.lt_end_date >= $scope.edt.lt_start_date) {
                        var cnt = 0;
                        var holi_data = [];
                        var dates;
                        for (var j = 0; j < $scope.Holileave_data.length; j++) {
                            dates = getDates(new Date($scope.Holileave_data[j].lt_start_date), new Date($scope.Holileave_data[j].lt_end_date));
                            var dates1 = getDates(new Date($scope.edt.lt_start_date), new Date($scope.edt.lt_end_date));
                            if (dates.length > 0) {
                                for (var k = 0; k < dates.length; k++) {
                                    for (var l = 0; l < dates1.length; l++) {
                                        if (dates1[l].checkdates == dates[k].checkdates) {
                                            cnt = cnt + 1;
                                        }
                                    }
                                }
                            }
                        }

                        
                        var date2 = new Date($scope.edt.lt_end_date);
                        var date1 = new Date($scope.edt.lt_start_date);
                        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

                        if ($scope.edt.sims_start_chk == true && $scope.edt.sims_end_chk == true) {
                            diffDays = parseInt(diffDays) + 1;
                        }
                        if (($scope.edt.sims_start_chk == false || $scope.edt.sims_start_chk == undefined) && ($scope.edt.sims_end_chk == false || $scope.edt.sims_end_chk == undefined)) {
                            diffDays = parseInt(diffDays) + 1;
                        }
                        if ($scope.edt.sims_start_chk == true && ($scope.edt.sims_end_chk == false || $scope.edt.sims_end_chk == undefined)) {
                            diffDays = parseInt(diffDays) + 0.5;
                        }
                        if (($scope.edt.sims_start_chk == false || $scope.edt.sims_start_chk == undefined) && $scope.edt.sims_end_chk == true) {
                            diffDays = parseInt(diffDays) + 0.5;
                        }

                        diffDays = diffDays - cnt;

                        $scope.edt.lt_days = diffDays + ' ' + "Days";

                        if (($scope.leavebal_data - diffDays) > 0) {
                            $scope.edt.remain_leave = ($scope.leavebal_data - diffDays);
                            $scope.edt.lwp_leave = "0";
                        }
                        else {
                            $scope.edt.lwp_leave = (diffDays - $scope.leavebal_data);
                            $scope.edt.remain_leave = "0";
                        }
                    }

                }

                if ($scope.edt.lt_end_date < $scope.edt.lt_start_date) {
                    swal({ title: "Alert", text: "End Date is Less than Start Date", showCloseButton: true, width: 380, });
                    $scope.edt.lt_end_date = '';
                    $scope.edt.lt_start_date = '';
                }
            }

            $scope.Save = function (isvalid) {
                if (isvalid) {
                    var lt_hours;
                    var date2 = new Date($scope.edt.lt_end_date);
                    var date1 = new Date($scope.edt.lt_start_date);
                    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

                    if ($scope.edt.sims_start_chk == true && $scope.edt.sims_end_chk == true) {
                        diffDays = diffDays + 1;
                        lt_hours = "1.1";
                    }
                    if ($scope.edt.sims_start_chk == false && $scope.edt.sims_end_chk == false) {
                        diffDays = diffDays + 1;
                        lt_hours = "0";
                    }
                    if ($scope.edt.sims_end_chk == true) {
                        diffDays = diffDays + 0.5;
                        lt_hours = "0.1";
                    }
                    if ($scope.edt.sims_start_chk == true) {
                        diffDays = diffDays + 0.5;
                        lt_hours = "0.1";
                    }
                    $scope.edt.lt_days = diffDays;
                    $scope.edt.lt_hours = lt_hours;

                    var data1 = [];
                    var data = ({
                        em_login_code: $scope.username,
                        comp_code: $scope.emp_data[0].comp_code,
                        leave_code: $scope.edt.leave_code,
                        lt_start_date: $scope.edt.lt_start_date,
                        lt_end_date: $scope.edt.lt_end_date,
                        lt_days: $scope.edt.lt_days,
                        lt_remarks: $scope.edt.lt_remarks,
                        lt_mgr_confirm_tag: "N",
                        lt_converted_hours: "",
                        lt_hours: $scope.edt.lt_hours,

                    });
                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/EmployeeLeave/CUDInsertEMPLeaveDetails", data1).then(function (res) {
                        $scope.msg = res.data;

                        //if ($scope.msg!='' ||$scope.msg!=null)
                        if ($scope.msg > 0) {
                            swal({ title: "Alert", text: "Leave Applied Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $http.post(ENV.apiUrl + "api/common/EmployeeLeave/CUDInsert_WorkflowDetails?transaction_id=" + $scope.msg + "&dept_code=" + $scope.emp_data[0].dept_no).then(function (res) {
                                        $scope.workflow_data = res.data;
                                    });
                                }
                            });
                            $scope.cancel();
                        }
                        else {
                            swal({ title: "Alert", text: "Leave Not Applied", showCloseButton: true, width: 380, });
                            $scope.cancel();
                        }
                    });
                }
            }

            $scope.Employeeleave_Reset = function () {
                $scope.div_trans = false;
                $scope.div_pendingview = true;
            }

            $scope.cancel = function () {
                $scope.edt = "";
                $scope.div_pending = false;
                $scope.leavebal_data = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            //$('*[data-datepicker="true"] input[type="text"]').datepicker({
            //    todayBtn: true,
            //    orientation: "top left",
            //    autoclose: true,
            //    todayHighlight: true,
            //  //  format: 'yyyy-mm-dd',
            //});

            //$(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
            //   $('input[type="text"]', $(this).parent()).focus();
            //});

        }])


})();