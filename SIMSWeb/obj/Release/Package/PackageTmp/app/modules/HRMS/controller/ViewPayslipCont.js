﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ViewPayslipCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
      
            $scope.display = true;
            $scope.Financial = true;
            $http.get(ENV.apiUrl + "api/common/ViewPayslip/Getallperioddata").then(function (periodsName) {
                $scope.periods_Name = periodsName.data;
                console.log($scope.periods_Name);
            });

            
            $http.get(ENV.apiUrl + "api/common/ViewPayslip/GetReportName").then(function (Reportname) {
                $scope.Report_Name = Reportname.data;
                console.log($scope.Report_Name);
            });
            //args.ParameterValues["paysheet_year"] = pay_year;
            //args.ParameterValues["paysheet_month"] = pay_month;
            //args.ParameterValues["search"] = usr_name;

            $http.get(ENV.apiUrl + "api/common/ViewPayslip/getCurrentYear").then(function (a_year) {
                debugger
                $scope.year_no = a_year.data[0].year1;
                console.log($scope.periods_Name);
            });


            $scope.Report = function (year,paramenter) {
                debugger;
                var data = {
                    location: $scope.Report_Name[0].fins_appl_form_field_value1,
                    parameter: {
                        paysheet_year: year,
                        paysheet_month: paramenter,
                        search: $rootScope.globals.currentUser.username
                    },            
                    state: 'main.Pe078V'
                }

                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')
            }


        }])

})();
