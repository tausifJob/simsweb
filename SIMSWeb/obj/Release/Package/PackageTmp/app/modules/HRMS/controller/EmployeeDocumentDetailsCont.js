﻿(function () {
    'use strict';

    var main, strMessage;
    var grade_code, SectionSubject5 = [], subject_code = [], subject_code1 = [];

    var cur_code;
    var section_code, sectioncodemodal1;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

    simsController.controller('EmployeeDocumentDetailsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = true;
            $scope.busyindicator = false;
            var GetMapped_Employee = [];

            $http.get(ENV.apiUrl + "api/employeedoc/getEmpDetails").then(function (AllComboBoxValues) {
                
                $scope.ComboBoxValues = AllComboBoxValues.data;

            });
            $http.get(ENV.apiUrl + "api/employeedoc/getDeptDetails").then(function (AllComboBoxValues1) {
                
                $scope.ComboBoxValues1 = AllComboBoxValues1.data;

            });
            $http.get(ENV.apiUrl + "api/employeedoc/getDesgDetails").then(function (AllComboBoxValues2) {
                
                $scope.ComboBoxValues2 = AllComboBoxValues2.data;

            });
            $http.get(ENV.apiUrl + "api/employeedoc/getDescDetails").then(function (AllComboBoxValues3) {
                
                $scope.ComboBoxValues3 = AllComboBoxValues3.data;

            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.Show = function () {
                ;
                var dep, des = "";
                $scope.GetMapped_Employee = [];

                if ($scope.ComboBoxValues1 == undefined) {
                    $scope.ComboBoxValues1 = "";
                }
                else {
                    dep = $scope.ComboBoxValues1.codp_dept_name;
                }
                if ($scope.ComboBoxValues2 == undefined) {
                    $scope.ComboBoxValues2 = "";
                }
                else {
                    des = $scope.
                    ComboBoxValues2.dg_desc;
                }
                $scope.showEmployeeTeacherData(dep, des);

            }

            $scope.check_all = function () {
                
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.check_once = function () {
                
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }

            $scope.insertrecord = function () {
                ;
                $scope.flag1 = false;
                if ($scope.ComboBoxValues3.pays_doc_desc == undefined || $scope.ComboBoxValues3.pays_doc_desc == "") {
                    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please select Description name", showCloseButton: true, width: 380, });

                }
                if (!$scope.flag1) {
                    ;
                    $scope.flag = false;
                    var send = [];
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        0
                        var v = document.getElementById(i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var insertmodulecode = ({
                                'pays_doc_mod_code': "",
                                'pays_doc_code': $scope.ComboBoxValues3.pays_doc_desc,
                                'pays_doc_srno': $scope.filteredTodos[i].pays_doc_srno,
                                'pays_doc_empl_id': $scope.filteredTodos[i].em_login_code,
                                'pays_doc_desc': $scope.filteredTodos[i].pays_doc_desc,
                                'pays_doc_path': $scope.filteredTodos[i].pays_doc_path,
                                'pays_doc_issue_date': $scope.filteredTodos[i].pays_doc_issue_date,
                                'pays_doc_expiry_date': $scope.filteredTodos[i].pays_doc_expiry_date,
                                'pays_doc_status': $scope.filteredTodos[i].pays_doc_status,
                                opr: 'I'
                            });
                            send.push(insertmodulecode);
                        }
                    }
                    $scope.flag1 = false;
                    if ($scope.ComboBoxValues3 == undefined || $scope.ComboBoxValues3.pays_doc_desc == "") {
                        $scope.flag1 = true;
                        swal({ title: "Alert", text: "Please select Account name", showCloseButton: true, width: 380, });

                    }
                    if ($scope.flag1) {
                    }
                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Insert?",
                            showCloseButton: true,
                            confirmButtonText: 'Yes',
                            showCancelButton: true,
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                ;
                                $http.post(ENV.apiUrl + "api/employeedoc/CUDgetAllRecords", send).then(function (msg) {
                                    
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record inserted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {

                                                Show();
                                                //GetMapped_Employee=[];
                                            }

                                            $scope.currentPage = true;
                                            //$scope.GetMapped_Employee = [];

                                        });

                                    }

                                    else {
                                        swal({ text: "Record Already Mapped, cant inserted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/employeedoc/getTableDetails").then(function (Board_Data) {
                                                    $scope.BoardData = Board_Data.data;
                                                    $scope.totalItems = $scope.BoardData.length;
                                                    $scope.todos = $scope.BoardData;
                                                    $scope.makeTodos();
                                                    main.checked = false;
                                                    $('tr').removeClass("row_selected");
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                            else {
                                main = document.getElementById('all_chk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }

                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById(i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                    }


                }


            }

            $scope.deleterecord = function () {
                ;
                $scope.flag = false;
                var del = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'pays_doc_srno': $scope.filteredTodos[i].pays_doc_srno,
                            'pays_doc_empl_id': $scope.filteredTodos[i].em_login_code,
                            opr: 'D'

                        });
                        del.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        confirmButtonText: 'Yes',
                        showCancelButton: true,
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            ;
                            $http.post(ENV.apiUrl + "api/employeedoc/CUDDeleteAllRecords", del).then(function (msg) {
                                
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            //showEmployeeTeacherData(ComboBoxValues1.codp_comp_code, ComboBoxValues2.dg_desc)
                                            $scope.pays_doc_desc = "";
                                            $scope.pays_doc_issue_date = "";
                                            $scope.pays_doc_expiry_date = "";

                                            //Show();

                                        }

                                        $scope.currentPage = true;
                                    });
                                }
                                else {
                                    swal({ text: "Record  cant Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/employeedoc/getTableDetails").then(function (Board_Data) {
                                                $scope.BoardData = Board_Data.data;
                                                $scope.totalItems = $scope.BoardData.length;
                                                $scope.todos = $scope.BoardData;
                                                $scope.makeTodos();
                                                main.checked = false;
                                                $('tr').removeClass("row_selected");
                                            });
                                        }
                                    });
                                }
                            });
                        }
                        else {
                            main = document.getElementById('all_chk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
            }

            $scope.showEmployeeTeacherData = function (dep, des) {
                
                $http.get(ENV.apiUrl + "api/employeedoc/getTableDetails?dept=" + dep + "&desg=" + des).then(function (res1) {
                    
                    $scope.GetMapped_Employee = res1.data;
                    $scope.totalItems = $scope.GetMapped_Employee.length;
                    $scope.todos = $scope.GetMapped_Employee;
                    $scope.makeTodos();
                    console.log(res1.data);
                    console.log(pays_doc_issue_date)
                });
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.Reset = function () {

                $scope.temp = '';
            }

            $scope.Save = function (myForm) {
                
                if (myForm) {
                    data1 = [];
                    var data = {

                        //'pays_doc_mod_code':"",
                        //'pays_doc_code': $scope.ComboBoxValues3.pays_doc_desc,
                        //'pays_doc_srno': $scope.filteredTodos[i].pays_doc_srno,
                        //'pays_doc_empl_id': $scope.filteredTodos[i].em_login_code,
                        //'pays_doc_desc':$scope.filteredTodos[i].pays_doc_desc,
                        'pays_doc_path': $scope.filteredTodos[i].pays_doc_path,
                        //'pays_doc_issue_date':$scope.filteredTodos[i].pays_doc_issue_date,
                        //'pays_doc_expiry_date': $scope.filteredTodos[i].pays_doc_expiry_date,
                        //'pays_doc_status':$scope.filteredTodos[i].pays_doc_status,
                        opr: 'I'
                    }
                    data1.push(data);
                    //if ($scope.exist) {
                    //    swal({ title: "Alert", text: "Record Already exists", width: 300, height: 200 });
                    //}
                    //else {
                    $http.post(ENV.apiUrl + "api/employeedoc/CUDgetAllRecords", data1).then(function (msg) {
                        $scope.EmployeeDataoperation = false;
                        $http.get(ENV.apiUrl + "api/employeedoc/getTableDetails").then(function (EmpData) {
                            $scope.Emp_Data = EmpData.data;
                            $scope.totalItems = $scope.Emp_Data.length;
                            $scope.todos = $scope.Emp_Data;
                            $scope.makeTodos();
                            $scope.msg1 = msg.Emp_Data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record not Inserted Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record  Inserted Successfully", width: 300, height: 200 });
                            }
                        });
                    });
                    $scope.EmployeeDetail = true;

                }

                //}
            }

            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };
            var file_name = '';
            $scope.maindata = '';
            $scope.Objecttodata = function (str) {
                $scope.maindata = str;
                formdata = new FormData();
                file_name = 'Doc_';
                $scope.dd = JSON.stringify(str);

                file_name = file_name + JSON.parse($scope.dd).em_login_code + '_';
                for (var i = 0; i < $scope.ComboBoxValues3.length; i++) {
                    if ($scope.ComboBoxValues3[i].pays_doc_code == $scope.edt.pays_doc_code)
                        file_name = file_name + $scope.ComboBoxValues3[i].pays_doc_desc + '_';
                }

                file_name = file_name.replace(' ', '_');
                console.log(file_name);

            }

            $scope.file_changed = function (element) {
                ;
                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);

                var fortype = photofile.type.split("/")[1];
                if (fortype == 'png' || fortype == 'jpeg' || fortype == 'jpg' || fortype == 'pdf') {

                    if (photofile.size > 200000) {
                        swal('', 'File size limit not exceed upto 200kb.')
                    }
                    else {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $scope.$apply(function () {

                                $scope.prev_img = e.target.result;
                                var request = {
                                    method: 'POST',
                                    url: ENV.apiUrl + 'api/file/upload?filename=' + file_name + "&location=" + "Images/EmployeeImages",
                                    data: formdata,
                                    headers: {
                                        'Content-Type': undefined
                                    }
                                };
                                $http(request).success(function (d) {
                                    ;
                                    console.log($scope.maindata);
                                    $scope.maindata['pays_doc_path'] = d;
                                    //obj['pays_doc_path'] = d;
                                    $scope.docname = d;
                                    console.log($scope.docname);
                                    formdata = new FormData();
                                });

                            });
                        };
                    }

                    reader.readAsDataURL(photofile);
                }
                else {
                    swal('', 'PDF and Image Files are allowed.');
                }

            };
            // $scope.uploadImgClickFShow = false;

            $scope.uploadImgClickM = function () {
                $scope.uploadImgClickMShow = true;
            }

            $scope.uploadImgCancel = function () {
                $scope.uploadImgClickFShow = false;

                //  $scope.prev_img = "";
            }

            $scope.uploadImg = function () {

            }

            $scope.up = function (objone) {

                $scope.edt = {

                    pays_doc_desc: objone.pays_doc_desc,
                    pays_doc_path: objone.pays_doc_path,
                    pays_doc_issue_date: objone.pays_doc_issue_date,
                    pays_doc_expiry_date: objone.pays_doc_expiry_date,


                };
                //$scope.newmode = true;
                //$scope.check = true;
                //$scope.edt = '';
                //$scope.editmode = true;

                $scope.readonly = true;
                $scope.EmployeeDetail = true;
                $scope.EmployeeDataoperation = true;
                $scope.savebtn = false;
                //$scope.newbtn = false;
                $scope.updatebtn = true;
                $scope.uploadbtn = false;
                //$scope.edt = {};

            }

            $scope.Update = function (myForm) {

                data1 = [];
                if (myForm) {
                    var data = ({
                        pays_doc_desc: $scope.edt.pays_doc_desc,
                        pays_doc_path: $scope.edt.pays_doc_path,
                        pays_doc_issue_date: $scope.edt.pays_doc_issue_date,
                        pays_doc_expiry_date: $scope.edt.pays_doc_expiry_date,
                        opr: 'U'
                    });

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/employeedoc/CUDgetAllRecords", data1).then(function (msg) {
                        $scope.EmployeeDataoperation = false;
                        $http.get(ENV.apiUrl + "api/employeedoc/getTableDetails").then(function (EmpData) {
                            $scope.Emp_Data = EmpData.data;
                            $scope.totalItems = $scope.Emp_Data.length;
                            $scope.todos = $scope.Emp_Data;
                            $scope.makeTodos();
                            $scope.msg1 = msg.Emp_Data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record not inserted Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record  inserted Successfully", width: 300, height: 200 });
                            }
                        });
                    });
                    $scope.EmployeeDetail = true;

                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.GetMapped_Employee, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.GetMapped_Employee;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (//item.em_login_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.pays_doc_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        //item.pays_doc_issue_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.em_first_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.em_login_code == toSearch) ? true : false;
            }

            $scope.Change = function (emp_no) {

                emp_no.ischange = true;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }])
})();