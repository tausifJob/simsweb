﻿(function () {
    'use strict';
    var formdata = new FormData();

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeImageViewCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '8';
            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $http.get(ENV.apiUrl + "api/HRMS/EmpImgShow/getDepartmentName").then(function (res) {
                $scope.departmentname = res.data;
            })

            $http.get(ENV.apiUrl + "api/HRMS/EmpImgShow/getGradeName").then(function (res) {
                $scope.gradename = res.data;
            })

            $http.get(ENV.apiUrl + "api/HRMS/EmpImgShow/getStaffType").then(function (res) {
                $scope.staffname = res.data;
            })

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 8;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.Reset = function () {
                debugger;
                $scope.edt.codp_dept_no = "";
                $scope.edt.gr_code = "";
                $scope.edt.sims_appl_parameter = "";
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.EmployeeData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.EmployeeData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.em_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.em_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.em_full_name == toSearch) ? true : false;
            }

            $scope.Show_Data = function () {
                debugger;
                $scope.table1 = true;
                $scope.ImageView = false;

                $http.get(ENV.apiUrl + "api/HRMS/EmpImgShow/getEmployeeDetail?codp_dept_no=" + $scope.edt.codp_dept_no + "&gr_code=" + $scope.edt.gr_code + "&sims_appl_parameter=" + $scope.edt.sims_appl_parameter).then(function (Employee_Data) {
                    debugger;
                    $scope.EmployeeData = Employee_Data.data;
                    $scope.totalItems = $scope.EmployeeData.length;
                    //$scope.imageUrl = ENV.apiUrl + 'Content/sjs/Images/EmployeeImages/';
                    $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/';
                    $scope.todos = $scope.EmployeeData;
                    $scope.makeTodos();
                    console.log($scope.EmployeeData);
                    if (Employee_Data.data.length > 0) { }
                    else {
                        $scope.ImageView = true;
                    }
                });

            }

            var imagename = '';

            $scope.UploadImageModal = function (str) {

                imagename = str.em_number;

                $('#myModal').modal('show');
            }

            $scope.delete = function (obj) {

                var deleteEmployeeImage = ({
                    'em_number': obj.em_number,
                    'em_img': obj.em_img,
                    'opr': 'N'
                });

                $http.post(ENV.apiUrl + "api/HRMS/EmpImgShow/CUDEmployeeDetails", deleteEmployeeImage).then(function (msg) {
                    $scope.msg1 = msg.data;
                    debugger
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Image Deleted Successfully", width: 300, height: 200 });

                    }
                    else {

                        swal({ title: "Alert", text: "Image Not Deleted", width: 300, height: 200 });
                    }
                });
                $http.get(ENV.apiUrl + "api/HRMS/EmpImgShow/getEmployeeDetail").then(function (Employee_Data) {
                    $scope.EmployeeData = Employee_Data.data;
                    $scope.totalItems = $scope.EmployeeData.length;
                    $scope.todos = $scope.EmployeeData;
                    $scope.makeTodos();
                    $scope.msg1 = msg.data;

                });
            }


            $scope.file_changed = function (element) {

                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;
                    });
                };
                reader.readAsDataURL(photofile);

                $scope.Upload = function () {

                    var data = [];
                    var t = $scope.photo_filename.split("/")[1];
                    console.log(t);
                    data = {
                        em_number: imagename,
                        em_img: imagename + "." + t,
                        opr: "K"
                    }
                    console.log(data);

                    $http.post(ENV.apiUrl + "api/HRMS/EmpImgShow/CUDUpdateEmpPics", data).then(function (res) {
                        $scope.result = res.data;
                        debugger
                        $('#myModal').modal('hide');
                        if ($scope.result == true) {
                            swal({ title: "Alert", text: "Image Upload Successfully", width: 300, height: 200 });

                        }


                        var request = {
                            method: 'POST',
                            url: ENV.apiUrl + '/api/file/upload?filename=' + data.em_number + "&location=" + "/EmployeeImages",
                            data: formdata,
                            headers: {
                                'Content-Type': undefined
                            }
                        };
                        $http(request)
                      .success(function (d) {
                      }, function () {
                          alert("Err");
                      });

                    }, function () {
                        $('#ErrorMessage').modal({ backdrop: "static" });


                    });

                }


                $scope.getTheFiles = function ($files) {
                    $scope.filesize = true;

                    angular.forEach($files, function (value, key) {
                        formdata.append(key, value);

                        var i = 0;
                        if ($files[i].size > 800000) {
                            $scope.filesize = false;
                            $scope.edt.photoStatus = false;

                            swal({ title: "Alert", text: "File Should Not Exceed 800Kb.", imageUrl: "assets/img/notification-alert.png", });

                        }
                        else {

                        }

                    });
                };
            }


            simsController.directive('ngFiles', ['$parse', function ($parse) {

                function fn_link(scope, element, attrs) {
                    var onChange = $parse(attrs.ngFiles);
                    element.on('change', function (event) {
                        onChange(scope, { $files: event.target.files });
                    });
                };

                return {
                    link: fn_link
                }
            }])

        }])
})();
