﻿(function () {
    'use strict';
    var opr = '';
    var deletecode = [];
    var main;
    var data1 = [];
    var data = [];

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('DefineCategoryCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '5';
            $scope.table1 = true;
            $scope.operation = false;
            $scope.editmode = false;
            $scope.countData = [
                 { val: 5, data: 5 },
                 { val: 10, data: 10 },
                 { val: 15, data: 15 },
            ]

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.size = function (str) {
                if (str == 5 || str == 10 || str == 15) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage);
                $scope.makeTodos();
            }

            $http.get(ENV.apiUrl + "api/DefineCategoryDetails/getCompanyName").then(function (res1) {
                $scope.comp = res1.data;
            });
            //$scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            //console.log($scope.finnDetail)

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/DefineCategoryDetails/getDefineCategory").then(function (getDefineCategory_data) {
                    $scope.DefineCategory = getDefineCategory_data.data;
                    //$scope.totalItems = $scope.DefineCategory.length;
                    //$scope.todos = $scope.DefineCategory;
                    //$scope.makeTodos();
                    if ($scope.DefineCategory.length > 0) {
                        $scope.table1 = true;
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.DefineCategory.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.DefineCategory.length, data: 'All' })
                        }
                        $scope.totalItems = $scope.DefineCategory.length;
                        $scope.todos = $scope.DefineCategory;
                        $scope.makeTodos();
                    }
                    else {

                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                        $scope.table1 = true;
                    }
                });
            }
            $http.get(ENV.apiUrl + "api/DefineCategoryDetails/getDefineCategory").then(function (getDefineCategory_data) {
                $scope.DefineCategory = getDefineCategory_data.data;
                //$scope.totalItems = $scope.DefineCategory.length;
                //$scope.todos = $scope.DefineCategory;
                //$scope.makeTodos();
                if ($scope.DefineCategory.length > 0) {
                    $scope.table1 = true;
                    $scope.pager = true;
                    if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.DefineCategory.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.DefineCategory.length, data: 'All' })
                    }
                    $scope.totalItems = $scope.DefineCategory.length;
                    $scope.todos = $scope.DefineCategory;
                    $scope.makeTodos();
                }
                else {

                    swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                    $scope.table1 = true;
                }
            });

            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.newmode = true;

                $scope.temp = {
                    ca_code: '',
                    ca_company_code: '',
                    desg_company_code: '',
                    comp_name: '',
                    ca_desc: '',
                    ca_process_tag: ''
                }
                $scope.editmode = false;
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;

            }

            $scope.up = function (str) {
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.operation = true;
                debugger;
                $scope.temp = {
                    desg_company_code: str.ca_company_code,
                    //comp_name:str.desg_company_code_name,
                    ca_code: str.ca_code,
                    //ca_company_code: str.ca_company_code,
                    comp_name: str.comp_name,
                    ca_desc: str.ca_desc,
                    ca_process_tag: str.ca_process_tag
                }

            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.temp = {
                    ca_code: '',
                    ca_company_code: '',
                    comp_name: '',
                    ca_desc: '',
                    ca_process_tag: '',
                    desg_company_code: '',
                }
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            data1 = [];
            $scope.Save = function (myForm) {
                if (myForm) {

                    var flag = false;
                    var data = $scope.temp;
                    for (var i = 0; i < $scope.DefineCategory.length; i++) {
                        if ($scope.DefineCategory[i].ca_desc == $scope.temp.ca_desc) {
                            swal({ text: "Record Already Exist", showCloseButton: true, width: 380, });
                            flag = true;
                            break;
                        }
                    }

                    if (!flag) {
                        var data = {
                            desg_company_code: $scope.temp.desg_company_code,
                            ca_desc: $scope.temp.ca_desc,
                            ca_process_tag: $scope.temp.ca_process_tag,
                            opr: 'I'
                        };
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/DefineCategoryDetails/DefineCategoryCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ text: "Record Not Inserted", width: 300, height: 200 });
                            }
                            $scope.operation = false;
                            $scope.getgrid();
                        });
                        data1 = [];
                        $scope.table1 = true;
                        $scope.operation = false;
                    }
                }
            }

            $scope.Update = function () {
                data1 = [];
                var data = {
                    desg_company_code: $scope.temp.desg_company_code,
                    ca_desc: $scope.temp.ca_desc,
                    ca_code: $scope.temp.ca_code,
                    ca_process_tag: $scope.temp.ca_process_tag,
                    opr: 'U'
                };
                data1.push(data);
                $http.post(ENV.apiUrl + "api/DefineCategoryDetails/DefineCategoryCUD", data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", width: 300, height: 200 });
                    }
                    else {
                        swal({ text: "Record Not Updated", width: 300, height: 200 });
                    }
                    $scope.operation = false;
                    $scope.getgrid();
                });
                data1 = [];
                $scope.operation = false;
                $scope.table1 = true;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].ca_code + i);
                    if (main.checked == true) {
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }

                    else {
                        v.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.deleterecord = function () {

                deletecode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].ca_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletecategorycode = ({
                            'ca_code': $scope.filteredTodos[i].ca_code,

                            'opr': 'D'
                        });
                        deletecode.push(deletecategorycode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/DefineCategoryDetails/DefineCategoryCUD", deletecode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                $scope.currentPage = true;
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].ca_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $scope.row1 = '';
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.DefineCategory, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.DefineCategory;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                     item.ca_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.comp_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.ca_code == toSearch) ? true : false;
            }

        }])

})();