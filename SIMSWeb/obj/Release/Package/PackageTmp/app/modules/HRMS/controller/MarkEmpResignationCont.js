﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('MarkEmpResignationCont',
        ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.EmpResign = true;
            $scope.EmpMark = true;
            $scope.display = true;
            $scope.grid = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.div_mark = false;
            var str, cnt;
            var data1 = [];
            var data = [];
            $scope.edt = { com_code: undefined, dep_code: undefined, em_number: undefined }
            $scope.edit_code = false;
            $scope.head_teacher_mapping = [];
            var main, autoid;
            $scope.btn_save = true;
            $scope.btn_delete = false;
            var head_teacher_code = [];
            $scope.Order_lists = [];
            $scope.test_shift = [];
            $scope.orderdetails_data = [];
            $scope.shipment_lists = [];
            var date = new Date();
            $scope.f = false;
            // $scope.btn_order = false;
            $scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');
            console.log($scope.ddMMyyyy);

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            //  $scope.username = $rootScope.globals.currentUser.username;
            console.log($scope.username);

            $timeout(function () {
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $http.get(ENV.apiUrl + "api/common/MarkEmpResignation/getCompanydetails").then(function (res) {
                $scope.comp_data = res.data;
            });


            $scope.getdepartment = function (comp_code) {
                if (comp_code != 'undefined') {
                    $http.get(ENV.apiUrl + "api/common/MarkEmpResignation/getCompanyDepartments?company_code=" + comp_code).then(function (res) {
                        $scope.dept_data = res.data;
                    });
                }
            }

            $scope.getEmpReg = function (comp_code, dept_code, em_no) {
                $scope.EmpResign = true;
                $scope.EmpMark = true;
                $http.get(ENV.apiUrl + "api/common/MarkEmpResignation/getEmployeDetails?login_code=" + em_no + "&com_code=" + comp_code + "&dep_code=" + dept_code).then(function (res) {
                    $scope.empReg_data = res.data;
                    console.log($scope.empReg_data);
                    $scope.totalItems = $scope.empReg_data.length;
                    $scope.todos = $scope.empReg_data;
                    $scope.makeTodos();
                    $scope.div_mark = true;
                    //for (var i = 0; i < $scope.totalItems; i++)
                    //{
                    //    if ($scope.empReg_data.com_code == 'R')
                    //    {
                    //        swal({ title: "Alert", text: "Employee Resignation Already Marked", showCloseButton: true, width: 380, });
                    //    }
                    //    else if ($scope.empReg_data.com_code == 'N')
                    //    {
                    //        swal({ title: "Alert", text: "Employee was Not Found", showCloseButton: true, width: 380, });
                    //    }
                    //}

                });
            }

            $scope.getGratuity = function (em_no) {
                debugger;
                var allgratuityData = [];

                $http.get(ENV.apiUrl + "api/common/MarkEmpResignation/getAmt_ApplandWorkingDays?empl_code=" + em_no).then(function (res1) {
                    $scope.amt_Appl_working_days = res1.data;
                    if ($scope.amt_Appl_working_days.length > 0) {
                        debugger;
                        $scope.allgratuityData = {
                            grt_amount: $scope.amt_Appl_working_days[0].grt_amount,
                            grt_applicable_days: $scope.amt_Appl_working_days[0].grt_applicable_days,
                            grt_working_days: $scope.amt_Appl_working_days[0].grt_working_days
                        }
                    }
                    else {
                        $scope.allgratuityData = [];
                    }

                });


                $('#GratuityModal').modal({ backdrop: 'static', keyboard: true });

                $scope.temp =
                   {
                       em_number: em_no,
                       grt_leave_include_flag: true,
                       grt_cash_bank_flag: "true"

                   }

                $scope.chk_includeLeave();

            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.getMark = function (info) {
                swal({
                    title: '',
                    text: "Mark Resignation For Employee Name :" + info.em_full_name + "(" + info.em_login_code + ")" + " \n Mark to confirm?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        //var amount = 0, paid_amount = 0;
                        //console.log(info.lo_amount);
                        //if (info.lo_amount == undefined)
                        //    amount = 0;
                        //else
                        //    amount = info.lo_amount;

                        //if (info.lo_paid_amount == undefined)
                        //    paid_amount = 0;
                        //else
                        //    paid_amount = info.lo_paid_amount;

                        //if (info.reg_date==undefined)
                        //    info.reg_date = $scope.ddMMyyyy;
                        //else
                        //    info.reg_date = info.reg_date;

                        $http.post(ENV.apiUrl + "api/common/MarkEmpResignation/MarkEmployeeDetails", info).then(function (res) {
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Resignation Marked Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {

                                    $scope.EmpResign = true;
                                    $scope.EmpMark = false;
                                    if (isConfirm) {
                                        if ($scope.edt.com_code != undefined || $scope.edt.dep_code != undefined || $scope.edt.em_number != undefined)
                                            $scope.getEmpReg($scope.edt.com_code, $scope.edt.dep_code, $scope.edt.em_number);
                                        else
                                            $scope.getEmpReg('', '', '');

                                    }
                                });

                            }
                            else {
                                swal({ title: "Alert", text: "Resignation Not Marked", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        if ($scope.edt.com_code != undefined || $scope.edt.dep_code != undefined || $scope.edt.em_number != undefined)
                                            $scope.getEmpReg($scope.edt.com_code, $scope.edt.dep_code, $scope.edt.em_number);
                                        else
                                            $scope.getEmpReg('', '', '');
                                    }
                                });
                            }

                        });
                    }
                });

            }

            //$scope.cancel_btn = function () {
            //    $scope.temp = [];
            //    $('#GratuityModal').modal({ backdrop: 'static', keyboard: true });
            //}

            $scope.commcancel_btn = function () {
                $('#GratuityModal').modal('hide');
                $scope.temp = [];
            }

            $scope.cancel_reg = function () {
                $scope.edt = [];
                $scope.div_mark = false;
                $scope.filteredTodos = [];
                // $('#GratuityModal').modal('hide');
            }

            $scope.commOk_btn = function (isvalidate) {
                $scope.temp = "";
                $scope.allgratuityData = "";
                data1 = [];
                data = [];

                if (isvalidate) {
                    var data = ({
                        em_number: $scope.temp.em_number,
                        // em_login_code: $scope.temp.em_login_code,
                        grt_amount: $scope.allgratuityData.grt_amount,
                        grt_applicable_days: $scope.allgratuityData.grt_applicable_days,
                        grt_working_days: $scope.allgratuityData.grt_working_days,
                        grt_leave_include_flag: $scope.temp.grt_leave_include_flag,
                        grt_emp_leave_days: $scope.temp.grt_emp_leave_days,
                        grt_cash_bank_flag: $scope.temp.grt_cash_bank_flag,
                        grt_airfare_flag: $scope.temp.grt_airfare_flag,
                        grt_airfare_amount: $scope.temp.grt_airfare_amount,
                        opr: 'I',
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/MarkEmpResignation/CUDInsert_pays_gratuity_empl", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Pays Gratuity Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    //$scope.getgrid();
                                    $('#GratuityModal').modal('hide');
                                }
                            });
                            if ($scope.edt.com_code != undefined || $scope.edt.dep_code != undefined || $scope.edt.em_number != undefined)
                                $scope.getEmpReg($scope.edt.com_code, $scope.edt.dep_code, $scope.edt.em_number);
                            else
                                $scope.getEmpReg('', '', '');
                        }
                        else {
                            swal({ title: "Alert", text: "Pays Gratuity Not Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    //  $scope.getgrid();
                                    $('#GratuityModal').modal('hide');
                                }
                            });

                            if ($scope.edt.com_code != undefined || $scope.edt.dep_code != undefined || $scope.edt.em_number != undefined)
                                $scope.getEmpReg($scope.edt.com_code, $scope.edt.dep_code, $scope.edt.em_number);
                            else
                                $scope.getEmpReg('', '', '');
                        }
                    });

                    $scope.myForm1.$setPristine();
                    $scope.myForm1.$setUntouched();
                }
            }



            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.chk_includeLeave = function () {
                if ($scope.temp.grt_leave_include_flag == true) {
                    $scope.div_incleave = true;
                }
                else {
                    $scope.div_incleave = false;
                }
            }

            $scope.getRegDate = function (info) {
                console.log(info.reg_date);
                $('#dateModal').modal({ backdrop: 'static', keyboard: true });
                info.ischecked = true;

                $scope.temp1 =
                    {
                        reg_date: info.reg_date
                    }
            }

            $scope.getDatedetails = function () {
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].ischecked == true) {
                        $scope.filteredTodos[i].reg_date = $scope.temp1.reg_date;
                        $scope.filteredTodos[i].ischecked = false;
                    }
                }
                // $scope.temp.reg_date =null;
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.empReg_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.empReg_data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.em_login_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.em_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.dg_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }])


})();