﻿(function () {
    'use strict';
    var del = [], feecodes = [], All_Fee_codesMul = [];
    var main, temp, opr;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeShortFormCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.emp_register_no = true;
            $scope.searchText = false;
            $scope.emp_readonly = true;
            $scope.ImgLoaded = false;
            $scope.roleText = false;
            $scope.roleList = true;
            $scope.uae = false;
            $scope.qatar = true;
            $scope.national_id_readnly = false;
            $scope.iban_number_readnly = false;
            $scope.bank_ac_no_readnly = false;

            $http.get(ENV.apiUrl + "api/HRMS/empShortFrm/getRole").then(function (rolesdata) {
                $scope.roles_data = rolesdata.data;
                console.log($scope.roles_data);
            });

            $http.get(ENV.apiUrl + "api/HRMS/empShortFrm/getLicSchool").then(function (licschooldata) {
                $scope.lic_school_data = licschooldata.data;
                debugger;
                if ($scope.lic_school_data[0].lic_school_country == 'Qatar') {
                    $scope.uae = false;
                    $scope.qatar = true;
                }
                else {
                    $scope.uae = true;
                    $scope.qatar = false;
                }
                console.log($scope.lic_school_data);
            });

            $http.get(ENV.apiUrl + "api/HRMS/empShortFrm/getEmpData").then(function (res) {
                $scope.obj = res.data;
                $scope.operation = true;
                $scope.Save_btn = true;
                $scope.display = true;
            });

            $http.get(ENV.apiUrl + "api/HRMS/empShortFrm/getCompany").then(function (resc) {
                $scope.company_data = resc.data;
            });
            $http.get(ENV.apiUrl + "api/HRMS/empShortFrm/getCountry").then(function (res1) {
                $scope.country_data = res1.data;
            });
            $http.get(ENV.apiUrl + "api/HRMS/empShortFrm/getDestination").then(function (res2) {
                $scope.dest_data = res2.data;
                $scope.temp = {
                    'em_Company_Code': $scope.company_data[0].em_Company_Code,
                    'em_Dest_Code': $scope.dest_data[0].em_Dest_Code,
                    'em_Country_Code': $scope.country_data[23].em_Country_Code,
                }
            });

            //$scope.searchBtn = true;
            $scope.searchEmp = function (empcode, $event) {
                $scope.searchBtn = false;
                //if ($event.keyCode == 13) {
                //    $scope.EmpSearch1(empcode);
                //}
                //if (emplogcode == 6) {
                //    $scope.searchText = true;
                //}
            }

            $scope.saveBtnDisEn = false;
            $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/';

            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };

            $scope.file_changed = function (element) {
                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;
                        //$scope.temp.em_img = element.files[0].name;
                        //console.log($scope.temp.em_img);
                    });
                };
                reader.readAsDataURL(photofile);
            };

            $scope.globalSearch = function () { $('#MyModal').modal('show'); $scope.showBtn = false; }

            $scope.EmployeeDetails = function () {

                $scope.NodataEmployee = false;
                $http.post(ENV.apiUrl + "api/common/SearchEmployee?data=" + JSON.stringify($scope.temp)).then(function (SearchEmployee_Data) {
                    $scope.Employee_Details = SearchEmployee_Data.data;
                    console.log($scope.EmployeeDetails);
                    $scope.EmployeeTable = true;

                    if (SearchEmployee_Data.data.length > 0) {
                    }
                    else {
                        $scope.NodataEmployee = true;
                    }
                });

            }

            $scope.EmployeeAdd = function (info) {
                $scope.temp = {
                    enroll_number: info.em_login_code,
                    emp_name: info.empName,
                    emp_desg: info.desg_name,
                    emp_dept: info.dept_name
                };
            }

            $scope.checkDOJ = function (str) {
                $scope.temp.em_dept_effect_from = str;
                $scope.temp.em_grade_effect_from = str;
                if ($scope.temp.em_date_of_birth >= $scope.temp.em_date_of_join) {
                    swal({ title: "Alert", text: "Date Of Join Should greater than Date of Birth", width: 300, height: 200 });
                    $scope.temp.em_date_of_join = "";
                }
            }

            $scope.checkPassport = function () {
                if ($scope.temp.em_passport_exp_date < $scope.temp.em_passport_issue_date) {
                    swal({ title: "Alert", text: "Passport Expiry Date Should greater than Passport Issue Date", width: 300, height: 200 });
                    $scope.temp.em_passport_exp_date = "";
                }
            }

            $scope.checkVisa = function () {
                if ($scope.temp.em_visa_exp_date < $scope.temp.em_visa_issue_date) {
                    swal({ title: "Alert", text: "Visa Expiry Date Should greater than Visa Issue Date", width: 300, height: 200 });
                    $scope.temp.em_visa_exp_date = "";
                }
            }

            $scope.checkEmiratesID = function () {
                if ($scope.temp.em_national_id_expiry_date < $scope.temp.em_national_id_issue_date) {
                    swal({ title: "Alert", text: "Emirates ID Expiry Date Should greater than Emirates ID Issue Date", width: 300, height: 200 });
                    $scope.temp.em_national_id_expiry_date = "";
                }
            }

            $scope.Cancel = function () {
                $scope.emp_register_no = true;
                $scope.searchText = false;
                $scope.national_id_readnly = false;
                $scope.iban_number_readnly = false;
                $scope.bank_ac_no_readnly = false;
                $scope.roleList = true;
                $scope.roleText = false;
                $scope.temp = "";
                $scope.emplogcode = "";
                // $scope.searchBtn = true;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.Save_btn = true;
                $scope.Update_btn = false;
                $scope.ImgLoaded = false;
                $scope.prev_img = '';
                $scope.emplogcode = "";
                $scope.temp.em_img = "";
            }

            $scope.SaveData = function (isvalid) {
              
                if (isvalid) {
                    //$scope.saveBtnDisEn = true;
                    var data = $scope.temp;
                    var reg_id = "EMP"+ $scope.temp.emp_id;
                    var em_reg_id =  reg_id;
                    console.log("Emp_ID");
                    console.log(em_reg_id);

                    if ($scope.photo_filename === undefined) {
                        data.em_img = null;
                    }
                    else {
                        data.em_img = '.' + $scope.photo_filename.split("/")[1];
                        data.em_dept_effect_from = convertdate($scope.temp.em_dept_effect_from);
                        data.em_grade_effect_from = convertdate($scope.temp.em_grade_effect_from);
                        data.em_date_of_join = convertdate($scope.temp.em_date_of_join);
                        data.em_date_of_birth = convertdate($scope.temp.em_date_of_birth);
                        data.em_pssport_issue_date = convertdate($scope.temp.em_pssport_issue_date);
                        data.em_passport_exp_date = convertdate($scope.temp.em_passport_exp_date);
                        data.em_visa_issue_date = convertdate($scope.temp.em_visa_issue_date);
                        data.em_visa_exp_date = convertdate($scope.temp.em_visa_exp_date);
                        data.em_national_id_issue_date = convertdate($scope.temp.em_national_id_issue_date);
                        data.em_national_id_expiry_date = convertdate($scope.temp.em_national_id_expiry_date);
                    }
                   
                    var sendobject = ({
                        em_login_code:em_reg_id,
                        em_Salutation_Code: $scope.temp.em_Salutation_Code,
                        em_first_name: $scope.temp.em_first_name,
                        em_middle_name: $scope.temp.em_middle_name,
                        em_last_name: $scope.temp.em_last_name,
                        em_name_ot: $scope.temp.em_name_ot,
                        em_Company_Code: $scope.temp.em_Company_Code,
                        em_img: data.em_img,
                        em_Dest_Code: $scope.temp.em_Dest_Code,
                        em_Designation_Code: $scope.temp.em_Designation_Code,
                        em_Dept_Code: $scope.temp.em_Dept_Code,
                        em_dept_effect_from: data.em_dept_effect_from,
                        em_Staff_Type_Code: $scope.temp.em_Staff_Type_Code,
                        em_Grade_Code: $scope.temp.em_Grade_Code,
                        em_grade_effect_from: data.em_grade_effect_from,
                        em_date_of_join: data.em_date_of_join,
                        em_blood_group_code: $scope.temp.em_blood_group_code,
                        em_date_of_birth: data.em_date_of_birth,
                        em_Sex_Code: $scope.temp.em_Sex_Code,
                        em_Marital_Status_Code: $scope.temp.em_Marital_Status_Code,
                        em_Country_Code: $scope.temp.em_Country_Code,
                        em_Nation_Code: $scope.temp.em_Nation_Code,
                        em_Religion_Code: $scope.temp.em_Religion_Code,
                        em_email: $scope.temp.em_email,
                        em_mobile: $scope.temp.em_mobile,
                        em_emergency_contact_name1: $scope.temp.em_emergency_contact_name1,
                        em_summary_address: $scope.temp.em_summary_address,
                        em_emergency_contact_number1: $scope.temp.em_emergency_contact_number1,
                        em_passport_no: $scope.temp.em_passport_no,
                        em_pssport_issue_date: data.em_pssport_issue_date,
                        em_passport_exp_date: data.em_passport_exp_date,
                        em_visa_no: $scope.temp.em_visa_no,
                        em_visa_type_code: $scope.temp.em_visa_type_code,
                        em_visa_issue_date: data.em_visa_issue_date,
                        em_visa_exp_date: data.em_visa_exp_date,
                        em_national_id: $scope.temp.em_national_id,
                        em_national_id_issue_date: data.em_national_id_issue_date,
                        em_national_id_expiry_date: data.em_national_id_expiry_date,
                        em_iban_no: $scope.temp.em_iban_no,
                        em_bank_ac_no: $scope.temp.em_bank_ac_no,
                        comn_role_code: $scope.temp.comn_role_code,
                        opr: 'H'
                    });
                    r
                    $http.post(ENV.apiUrl + "api/HRMS/empShortFrm/CUCreateEditEmployee", sendobject).then(function (res) {
                        $scope.CUDobj = res.data;

                        if ($scope.CUDobj !="") {

                            debugger;
                            var empid = $scope.CUDobj.split("/")[1];
                            swal({ title: "Alert", text: $scope.CUDobj.split("/")[0], width: 300, height: 200 });
                            
                            if (empid == 1) {

                                var request = {
                                    method: 'POST',
                                    url: ENV.apiUrl + '/api/file/upload?filename=' + em_reg_id + "&location=" + "/EmployeeImages",
                                    data: formdata,
                                    headers: {
                                        'Content-Type': undefined
                                    }

                                };

                                $http(request).success(function (d) {
                                    var alert = (d);
                                });

                                $scope.temp = "";
                            }
                        }
                       
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();
                       
                    });

                }
            }

            $scope.EmpSearch1 = function (emplogcode) {
                $scope.emp_register_no = false;
                // if (isvalid) {
                $scope.national_id_readnly = true;
                $scope.iban_number_readnly = true;
                $scope.bank_ac_no_readnly = true;
                $scope.Save_btn = false;
                $scope.Update_btn = true;
                $scope.searchText = true;
                $scope.roleText = true;
                $scope.roleList = false;
                $scope.roleReadOnly = true;

                $http.get(ENV.apiUrl + "api/HRMS/empShortFrm/getSearchEmpMaster?em_login_code=" + emplogcode).then(function (res) {

                    $scope.temp1 = res.data;

                    $scope.temp = {

                        em_login_code: $scope.temp1[0].em_login_code,
                        em_number: $scope.temp1[0].em_number,
                        em_service_status_code: $scope.temp1[0].em_service_status_code,
                        em_status_code: $scope.temp1[0].em_status_code,
                        em_apartment_number: $scope.temp1[0].em_apartment_number,
                        em_family_name: $scope.temp1[0].em_family_name,
                        em_street_number: $scope.temp1[0].em_street_number,
                        em_area_number: $scope.temp1[0].em_area_number,
                        em_phone: $scope.temp1[0].em_phone,
                        em_fax: $scope.temp1[0].em_fax,
                        em_post: $scope.temp1[0].em_post,
                        em_emergency_contact_name2: $scope.temp1[0].em_emergency_contact_name2,
                        em_emergency_contact_number2: $scope.temp1[0].em_emergency_contact_number2,
                        em_joining_ref: $scope.temp1[0].em_joining_ref,
                        em_ethnicity_code: $scope.temp1[0].em_ethnicity_code,
                        em_handicap_status: $scope.temp1[0].em_handicap_status,
                        em_stop_salary_indicator: $scope.temp1[0].em_stop_salary_indicator,
                        em_agreement: $scope.temp1[0].em_agreement,
                        em_punching_status: $scope.temp1[0].em_punching_status,
                        em_bank_cash_tag: $scope.temp1[0].em_bank_cash_tag,
                        em_citi_exp_tag: $scope.temp1[0].em_citi_exp_tag,
                        em_leave_tag: $scope.temp1[0].em_leave_tag,
                        em_passport_issue_place: $scope.temp1[0].em_passport_issue_place,
                        em_passport_issuing_authority: $scope.temp1[0].em_passport_issuing_authority,
                        em_pssport_exp_rem_date: $scope.temp1[0].em_pssport_exp_rem_date,
                        em_visa_issuing_place: $scope.temp1[0].em_visa_issuing_place,
                        em_visa_issuing_authority: $scope.temp1[0].em_visa_issuing_authority,
                        em_visa_exp_rem_date: $scope.temp1[0].em_visa_exp_rem_date,
                        em_visa_type_code: $scope.temp1[0].em_visa_type_code,
                        em_agreement_start_date: $scope.temp1[0].em_agreement_start_date,
                        em_agreement_exp_date: $scope.temp1[0].em_agreement_exp_date,
                        em_agreemet_exp_rem_date: $scope.temp1[0].em_agreemet_exp_rem_date,
                        em_punching_id: $scope.temp1[0].em_punching_id,
                        em_dependant_full: $scope.temp1[0].em_dependant_full,
                        em_dependant_half: $scope.temp1[0].em_dependant_half,
                        em_dependant_infant: $scope.temp1[0].em_dependant_infant,
                        em_Bank_Code: $scope.temp1[0].em_Bank_Code,
                        em_bank_ac_no: $scope.temp1[0].em_bank_ac_no,
                        em_iban_no: $scope.temp1[0].em_iban_no,
                        em_route_code: $scope.temp1[0].em_route_code,
                        em_bank_swift_code: $scope.temp1[0].em_bank_swift_code,
                        em_gpf_ac_no: $scope.temp1[0].em_gpf_ac_no,
                        em_pan_no: $scope.temp1[0].em_pan_no,
                        em_labour_card_no: $scope.temp1[0].em_labour_card_no,
                        em_gosi_ac_no: $scope.temp1[0].em_gosi_ac_no,
                        em_gosi_start_date: $scope.temp1[0].em_gosi_start_date,
                        em_national_id: $scope.temp1[0].em_national_id,
                        em_secret_question_code: $scope.temp1[0].em_secret_question_code,
                        em_secret_answer: $scope.temp1[0].em_secret_answer,

                        em_building_number: $scope.temp1[0].em_building_number,
                        em_Company_Code: $scope.temp1[0].em_Company_Code,
                        em_Dept_Code: $scope.temp1[0].em_Dept_Code,
                        em_Designation_Code: $scope.temp1[0].em_Designation_Code,
                        em_Dest_Code: $scope.temp1[0].em_Dest_Code,
                        em_Grade_Code: $scope.temp1[0].em_Grade_Code,
                        em_Marital_Status_Code: $scope.temp1[0].em_Marital_Status_Code,
                        em_Nation_Code: $scope.temp1[0].em_Nation_Code,
                        em_Country_Code: $scope.temp1[0].em_Country_Code,
                        em_State_Code: $scope.temp1[0].em_State_Code,
                        em_City_Code: $scope.temp1[0].em_City_Code,
                        em_Religion_Code: $scope.temp1[0].em_Religion_Code,
                        em_Salutation_Code: $scope.temp1[0].em_Salutation_Code,
                        em_Sex_Code: $scope.temp1[0].em_Sex_Code,
                        em_Staff_Type_Code: $scope.temp1[0].em_Staff_Type_Code,
                        em_blood_group_code: $scope.temp1[0].em_blood_group_code,
                        em_date_of_birth: $scope.temp1[0].em_date_of_birth,
                        em_date_of_join: $scope.temp1[0].em_date_of_join,
                        em_dept_effect_from: $scope.temp1[0].em_dept_effect_from,
                        em_email: $scope.temp1[0].em_email,
                        em_emergency_contact_name1: $scope.temp1[0].em_emergency_contact_name1,
                        em_emergency_contact_number1: $scope.temp1[0].em_emergency_contact_number1,
                        em_first_name: $scope.temp1[0].em_first_name,
                        em_grade_effect_from: $scope.temp1[0].em_grade_effect_from,
                        em_last_name: $scope.temp1[0].em_last_name,
                        em_middle_name: $scope.temp1[0].em_middle_name,
                        em_mobile: $scope.temp1[0].em_mobile,
                        em_name_ot: $scope.temp1[0].em_name_ot,
                        em_national_id_expiry_date: $scope.temp1[0].em_national_id_expiry_date,
                        em_national_id_issue_date: $scope.temp1[0].em_national_id_issue_date,
                        em_passport_exp_date: $scope.temp1[0].em_passport_exp_date,
                        em_passport_issue_date: $scope.temp1[0].em_passport_issue_date,
                        em_passport_no: $scope.temp1[0].em_passport_no,
                        em_summary_address: $scope.temp1[0].em_summary_address,
                        em_summary_address_local_language: $scope.temp1[0].em_summary_address_local_language,
                        em_visa_exp_date: $scope.temp1[0].em_visa_exp_date,
                        em_stop_salary_from: $scope.temp1[0].em_stop_salary_from,
                        em_visa_issue_date: $scope.temp1[0].em_visa_issue_date,
                        em_visa_no: $scope.temp1[0].em_visa_no,
                        //em_visa_type_code: $scope.temp1[0].em_visa_type_code,
                        comn_role_code: $scope.temp1[0].comn_role_code,
                        comn_role_name: $scope.temp1[0].comn_role_name,
                        em_img: $scope.temp1[0].em_img,


                    }

                    console.log($scope.imageUrl + $scope.temp.em_img)
                    //$scope.getstate($scope.temp.em_Country_Code);
                    //$scope.getcity($scope.temp.em_State_Code);
                });
                //}
            }

            $scope.image_click = function () {
                $scope.ImgLoaded = true;
            }

            $scope.UpdateData = function () {
              
                $scope.national_id_readnly = false;
                $scope.iban_number_readnly = false;
                $scope.bank_ac_no_readnly = false;
                $scope.roleList = true;
                $scope.roleText = false;

                var data = $scope.temp;
                if ($scope.photo_filename === undefined) { }
                else { data.em_img = '.' + $scope.photo_filename.split("/")[1]; }
                data.opr = 'U';

                $http.post(ENV.apiUrl + "api/HRMS/empShortFrm/CUMasterEmployee", data).then(function (res) {
                    $scope.CUDobj = res.data;
                    swal({ text: $scope.CUDobj.strMessage, timer: 10000 });
                });

                if ($scope.ImgLoaded == true) {
                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + '/api/file/upload?filename=' + $scope.temp.em_login_code + "&location=" + "/EmployeeImages",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    $http(request).success(function (d) {
                        //alert(d);
                    });
                }
                $scope.searchText = false;
                $scope.temp = [];
                $scope.prev_img = '';
                $scope.emplogcode = "";
                $scope.temp.em_img = "";
                $scope.Save_btn = true;
                $scope.Update_btn = false;

            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

        }]
        )

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])



})();
