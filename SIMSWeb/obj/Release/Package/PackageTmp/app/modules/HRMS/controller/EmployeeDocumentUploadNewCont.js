﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeDocumentUploadNewCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.pagesize1 = "5";
            $scope.pageindex1 = 0;
            $scope.save_btn = true;
            $scope.display = false;
            $scope.temp = {
                show: 'all'
            }
            var formdata = new FormData();
            var user = $rootScope.globals.currentUser.username.toLowerCase();

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
                $("#example_wrapper").scrollbar();

            }, 100);



            $scope.countData = [
                     { val: 5, data: 5 },
                     { val: 10, data: 10 },
                     { val: 15, data: 15 },

            ]


            $http.get(ENV.apiUrl + "api/EmpDocumentupload/getcompany").then(function (res) {

                $scope.companyNameData = res.data;
                $scope.temp['company_code'] = $scope.companyNameData[0].company_code
                $scope.getOthercomboData($scope.companyNameData[0].company_code);
            });
            var issueoexpirydate = false;
            $scope.getRegDate = function (info, str) {

                issueoexpirydate = false;
                console.log(info);
                $('#dateModal').modal({ backdrop: 'static', keyboard: true });
                info.ischecked = true;
                if (str == 'issue') {
                    issueoexpirydate = true;
                    $scope.temp1 =
                        {
                            reg_date: info.pays_doc_issue_date
                        }
                }
                else {
                    issueoexpirydate = false;
                    $scope.temp1 =
                       {
                           reg_date: info.pays_doc_expiry_date
                       }
                }
            }

            $scope.getDatedetails = function () {

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    for (var j = 0; j < $scope.filteredTodos[i].arr.length; j++) {
                        if ($scope.filteredTodos[i].arr[j].ischecked == true) {
                            if (issueoexpirydate == true)
                                $scope.filteredTodos[i].arr[j].pays_doc_issue_date = $scope.temp1.reg_date;
                            else
                                $scope.filteredTodos[i].arr[j].pays_doc_expiry_date = $scope.temp1.reg_date;
                            $scope.filteredTodos[i].arr[j].ischecked = false;
                            break;
                        }
                    }
                }
                // $scope.temp.reg_date =null;
            }

            $scope.Upload = function () {
                debugger;
                var t = $scope.photo_filename.split("/")[1];
                $scope.filename1 = $scope.filename + '.' + fortype;
                var data = [];
                var senddata = [];
                if ($scope.photo_filename == undefined || $scope.photo_filename == '') {
                    swal({ title: "Alert", text: "Plese Choose File First", width: 300, height: 200 });
                }
                else {

                    if (saveorupdate == true) {
                        for (var i = 0; i < datasave.length; i++) {
                            data = {
                                pays_doc_empl_id: datasave[i].pays_doc_empl_id,
                                pays_doc_mod_code: datasave[i].pays_doc_mod_code,
                                pays_doc_code: datasave[i].pays_doc_code,
                                pays_doc_desc: datasave[i].pays_doc_desc,
                                pays_doc_path: $scope.filename1,
                                pays_doc_issue_date: datasave[i].pays_doc_issue_date,
                                pays_doc_expiry_date: datasave[i].pays_doc_expiry_date,
                                pays_doc_status: true,
                                opr: "I"
                            }
                        }
                    }
                    else {
                        for (var i = 0; i < dataupdate.length; i++) {
                            data = {
                                pays_doc_srno: dataupdate[i].pays_doc_srno,
                                pays_doc_empl_id: dataupdate[i].pays_doc_empl_id,
                                pays_doc_mod_code: dataupdate[i].pays_doc_mod_code,
                                pays_doc_code: dataupdate[i].pays_doc_code,
                                pays_doc_desc: dataupdate[i].pays_doc_desc,
                                pays_doc_path: $scope.filename1,
                                pays_doc_issue_date: dataupdate[i].pays_doc_issue_date,
                                pays_doc_expiry_date: dataupdate[i].pays_doc_expiry_date,
                                pays_doc_status: true,
                                opr: "U"
                            }
                        }
                    }

                    senddata.push(data);



                    $http.post(ENV.apiUrl + "api/EmpDocumentupload/InsertUpdateDocumentDetail", senddata).then(function (msg) {
                        $scope.msg1 = msg.data;


                        $('#myModal').modal('hide');
                        if ($scope.msg1 == true) {

                            swal({ title: "Alert", text: "Employee Document Upload Successfully", width: 300, height: 200 });
                            $scope.show();
                            $scope.photo_filename = '';

                        }

                        else {
                            swal({ title: "Alert", text: "Employee Document Not Upload Successfully", width: 300, height: 200 });
                            $scope.show();

                        }

                        var request = {
                            method: 'POST',
                            url: ENV.apiUrl + '/api/EmpDocumentupload/upload?filename=' + $scope.filename1 + "&location=" + "EmployeeDocument",
                            data: formdata,
                            headers: {
                                'Content-Type': undefined
                            }
                        };
                        $http(request)
                      .success(function (d) {
                      },
                      function () {
                          alert("Err");
                      });

                    },
                        function () {
                            $('#ErrorMessage').modal({ backdrop: "static" });


                        });
                }

            }

            $scope.size = function (str) {
                if (str == 5 || str == 10 || str == 15) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }

                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);


                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.finalData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.finalData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.pays_doc_empl_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.pays_doc_empl_id.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.pays_doc_desc1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.pays_doc_path.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.pays_doc_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.pays_doc_srno == toSearch) ? true : false;
            }
            var datasave = [];
            var dataupdate = [];
            var saveorupdate = false;

            $scope.UploadImageModal = function (str) {

                datasave = [];
                dataupdate = [];
                saveorupdate = false;
                document.getElementById('file1').value = '';
                $scope.filename = str.pays_doc_empl_id + '_' + str.pays_doc_desc1;
                if (str.pays_doc_srno == '') {
                    saveorupdate = true;
                    var data = {
                        pays_doc_empl_id: str.pays_doc_empl_id,
                        pays_doc_mod_code: str.pays_doc_mod_code,
                        pays_doc_code: str.pays_doc_code,
                        pays_doc_desc: str.pays_doc_desc,
                        pays_doc_path: str.pays_doc_path,
                        pays_doc_issue_date: str.pays_doc_issue_date,
                        pays_doc_expiry_date: str.pays_doc_expiry_date,
                        pays_doc_status: true,
                    }
                    datasave.push(data);
                }
                else {
                    saveorupdate = false;
                    var data = {
                        pays_doc_srno: str.pays_doc_srno,
                        pays_doc_empl_id: str.pays_doc_empl_id,
                        pays_doc_mod_code: str.pays_doc_mod_code,
                        pays_doc_code: str.pays_doc_code,
                        pays_doc_desc: str.pays_doc_desc,
                        pays_doc_path: str.pays_doc_path,
                        pays_doc_issue_date: str.pays_doc_issue_date,
                        pays_doc_expiry_date: str.pays_doc_expiry_date,
                        pays_doc_status: true,
                    }
                    dataupdate.push(data);

                }

                $scope.pays_doc_empl_id = str.pays_doc_empl_id;
                $scope.pays_doc_desc1 = str.pays_doc_desc1;
                $('#myModal').modal('show');
            }

            $scope.getTheFiles = function ($files) {


                //FileList[0].File.name = $scope.filename + '.' + fortype;
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);

                });
            };

            var fortype = '';
            $scope.file_changed = function (element) {
                debugger;
                var photofile = element.files[0];
                //  var fortype = photofile.type.split("/")[1];
                //if (fortype == 'png' || fortype == 'jpeg' || fortype == 'jpg' || fortype == 'pdf') {
                //    if (photofile.size > 200000) {
                //        swal('', 'File size limit not exceed upto 200kb.')
                //    }
                //    else {


                $scope.edt = { employeeDocument: $scope.filename }
                $.extend($scope.edt, $scope.edt)
                //$scope.photo_filename = (photofile.type);

                $scope.photo_filename = (photofile.name);

                var len = 0;
                len = $scope.photo_filename.split('.');
                fortype = $scope.photo_filename.split('.')[len.length - 1];
                // $scope.edt1 = { sims_timetable_filename: $scope.photo_filename }
                // $.extend($scope.edt, $scope.edt1)
                $scope.photo_filename = (photofile.type);

                //element.files[0].FileList.File.name = $scope.filename + '.' + fortype;
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        // $scope.prev_img = e.target.result;


                    });
                };
                reader.readAsDataURL(photofile);

                //    }
                //}
                //else {
                //    swal('', 'PDF and Image Files are allowed.');
                //}
            };





            $scope.getOthercomboData = function (companycode) {
                $http.get(ENV.apiUrl + "api/EmpDocumentupload/getDepartmentNames?company_code=" + companycode).then(function (res) {
                    $scope.DepartmentNameData = res.data;
                    //$scope.temp['dept_code'] = $scope.DepartmentNameData[0].dept_code
                });


                $http.get(ENV.apiUrl + "api/EmpDocumentupload/getDesignation?company_code=" + companycode).then(function (res) {
                    $scope.DesignationData = res.data;
                });

                $http.get(ENV.apiUrl + "api/EmpDocumentupload/getDocumentName").then(function (res) {
                    $scope.DocumentName = res.data;
                });

            }

            $scope.reset = function () {
                $scope.temp1 = '';
                $scope.temp = '';
                $scope.temp = {
                    show: 'all'
                }
            }

            $scope.show = function () {

                if ($scope.temp.company_code == undefined || $scope.temp.company_code == "") {
                    swal({ title: "Alert", text: "Please Select Company", width: 300, height: 200 });
                }
                else {
                    $http.post(ENV.apiUrl + "api/EmpDocumentupload/DocumentdetailData", $scope.temp).then(function (res) {
                        debugger;
                        $scope.DocumentdetailsData = res.data;
                        if ($scope.DocumentdetailsData.length > 0) {
                            $scope.table = true;
                            $scope.pager = true;
                        }
                        else {
                            $scope.table = false;
                            swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                        }

                        $scope.feetemp = res.data;
                        $scope.PAcols = [];
                        var fc = false;


                        $scope.allDt = [];
                        for (var d = 0; d < $scope.feetemp.length; d++) {
                            var dt = $scope.feetemp[d].pays_doc_empl_name;
                            if ($scope.allDt[dt] == undefined) {
                                $scope.allDt[dt] = getRecDateWise(dt)
                            }
                        }

                        $scope.finalData = [];
                        for (var item in $scope.allDt) {
                            var ob = {
                                'dt': item,
                                'arr': $scope.allDt[item]
                            };
                            $scope.finalData.push(ob);
                        }

                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.DocumentdetailsData.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.DocumentdetailsData.length, data: 'All' })
                        }
                        $scope.totalItems = $scope.finalData.length;
                        $scope.todos = $scope.finalData;
                        $scope.makeTodos();


                    });
                }
            }

            $scope.downloaddoc = function (str) {
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeDocument/' + str;
                window.open($scope.url);
            }


            function DeleteColumns(arr) {
                var l = arr.length;
                var temp = [];
                for (var i = 0; i < l; i++) {
                    for (var v in arr[i].PAvals) {
                        var ob = { 'mode': v, 'value': 0 };
                        temp.push(ob);
                    }
                    break;
                }
                for (var i = 0; i < l; i++) {
                    for (var t = 0; t < temp.length; t++) {
                        temp[t].value = parseFloat(temp[t].value) + parseFloat(arr[i].PAvals[temp[t].mode]);
                    }
                }
                var finalPA = {};
                for (var x = 0; x < temp.length; x++) {
                    if (temp[x].value != 0) {
                        finalPA[temp[x].mode] = temp[x].value;
                    }
                }
                for (var i = 0; i < l; i++) {
                    var n = {};
                    var he = [];
                    for (var x in finalPA) {
                        n[x] = arr[i].PAvals[x];
                        he.push(x);
                    }
                    arr[i].PAvals = n;
                    arr[i].PAcols = he;
                }
            }

            function getRecDateWise(dt) {
                var a = [];
                for (var i = 0; i < $scope.feetemp.length; i++) {
                    if ($scope.feetemp[i].pays_doc_empl_name == dt) {
                        a.push($scope.feetemp[i]);
                    }
                }
                DeleteColumns(a);
                return a;
            }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });





        }])

})();
