﻿(function () {
    'use strict';
    var del = [], shiftdetails = [];
    var main;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeShiftCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.display = true;
            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.grid = true;
            $scope.shifttemplate_data = [];
            var data2 = [];
            $scope.div_submit = true;
            $scope.gridNew = false;
            $scope.div_shift = false;
            $scope.BUSY = false;
            $scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');

            $http.get(ENV.apiUrl + "api/common/ShiftMaster/getCompany").then(function (res) {
                $scope.comp_data = res.data;
                console.log($scope.comp_data[0].sh_company_code);
                $scope.edt['sh_company_code'] = $scope.comp_data[0].sh_company_code;
                $scope.getdepartment($scope.comp_data[0].sh_company_code);
            });

            $timeout(function () {
                $("#fixedtable,#fixedtable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.edt = {
                sh_start_date: $scope.ddMMyyyy,
            }

            $scope.getdepartment = function (comp_code) {
                $http.get(ENV.apiUrl + "api/common/EmployeeShift/GetDepartmentName?comp_code=" + comp_code).then(function (res) {
                    $scope.dept_data = res.data;
                });

                $http.get(ENV.apiUrl + "api/common/EmployeeShift/GetDesignationName?comp_code=" + comp_code).then(function (res) {
                    $scope.desg_data = res.data;
                });
            }

            $scope.btn_display = function () {
                $scope.div_shift = true;
                $scope.div_submit = false;
                if ($scope.edt.sh_company_code != undefined) {
                    $http.get(ENV.apiUrl + "api/common/ShiftTemplate/getAllTemplateName?company_code=" + $scope.edt.sh_company_code).then(function (res) {
                        $scope.AllTemplateName = res.data;
                    });
                }

                if ($scope.edt.code != null || $scope.edt.code != "") {
                    $http.get(ENV.apiUrl + "api/common/EmployeeShift/GetAllEmployeeName?comp_code=" + $scope.edt.sh_company_code + "&dept_code=" + $scope.edt.code + "&desg_code=" + $scope.edt.em_desg_code).then(function (res) {
                        $scope.AllEmployeeName_data = res.data;
                        console.log($scope.AllEmployeeName_data);
                    });
                }
            }

            $scope.getTempaltedetails = function (template_id) {
                if (template_id != null || template_id != "") {
                    $scope.gridNew = true;
                    $http.get(ENV.apiUrl + "api/common/ShiftTemplate/GetTemplateDetails?company_code=" + $scope.edt.sh_company_code + "&template_id=" + template_id).then(function (res) {
                        $scope.shifttemplate_data = res.data;
                        console.log($scope.shifttemplate_data);

                    });
                }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.AllEmployeeName_data.length; i++) {
                    if (main.checked == true) {

                        $scope.AllEmployeeName_data[i].sh_template_id1 = true;

                        $('tr').addClass("row_selected");
                    }

                    else {
                        $scope.AllEmployeeName_data[i].sh_template_id1 = false;
                        $('tr').removeClass("row_selected");
                        main.checked = false;
                    }
                }


            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.btn_SubmitNew = function () {
                var data = [];
                var data2 = [];
                $scope.insert = false;
                $scope.BUSY = true;
                shiftdetails = [];


                if ($scope.edt.sh_template_id != undefined) {
                    for (var i = 0; i < $scope.AllEmployeeName_data.length; i++) {
                        //if ($scope.AllEmployeeName_data[i].sh_template_id1 == true)
                        //var v = document.getElementById(i);

                        // if (v.checked == true)
                        if ($scope.AllEmployeeName_data[i].sh_template_id1 == true) {
                            shiftdetails = shiftdetails + $scope.AllEmployeeName_data[i].em_number + ',';
                            $scope.insert = true;

                        }

                    }

                    if ($scope.insert) {
                        for (var j = 0; j < $scope.shifttemplate_data.length; j++) {
                            var data =
                                {
                                    em_number: shiftdetails,
                                    sh_template_id: $scope.edt.sh_template_id,
                                    sh_start_date: $scope.edt.sh_start_date,
                                    em_company_code: $scope.edt.sh_company_code,
                                    em_dept_code: $scope.edt.code,
                                    sh_day_status: $scope.shifttemplate_data[j].sh_day_status,
                                    sh_shift_day1: $scope.shifttemplate_data[j].sh_shift_day1,
                                    sh_shift_day2: $scope.shifttemplate_data[j].sh_shift_day2,
                                    sh_shift_day3: $scope.shifttemplate_data[j].sh_shift_day3,
                                    sh_shift_day4: $scope.shifttemplate_data[j].sh_shift_day4,
                                    sh_shift_day5: $scope.shifttemplate_data[j].sh_shift_day5,
                                    sh_shift_day6: $scope.shifttemplate_data[j].sh_shift_day6,
                                    sh_shift_day7: $scope.shifttemplate_data[j].sh_shift_day7,
                                    sh_shift_status: $scope.shifttemplate_data[j].sh_shift_status,
                                    sh_template_name: $scope.shifttemplate_data[j].sh_template_name,
                                    sh_template_status: $scope.shifttemplate_data[j].sh_template_status,
                                };
                            data2.push(data);
                        }

                        $http.post(ENV.apiUrl + "api/common/EmployeeShift/CUDInsertEmployeeShift", data2).then(function (res) {
                            $scope.shift_rec = res.data;
                            if ($scope.shift_rec == "1") {
                                swal({ title: "Alert", text: "Record Saved Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.btn_display();
                                        $scope.btn_cancel();
                                    }
                                });
                            }

                            if ($scope.shift_rec == "2") {
                                swal({ title: "Alert", text: "Record Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.btn_display();
                                        $scope.btn_cancel();
                                    }
                                });
                            }
                            if ($scope.shift_rec == "4") {
                                swal({ title: "Alert", text: "Applicable from Date is Greater than Financial Period", showCloseButton: true, width: 380, });
                            }
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                        $scope.BUSY = false;
                    }
                }
                else {
                    swal({ title: "Alert", text: "Please Select Shift Template", showCloseButton: true, width: 380, })
                }
            }

            $scope.btn_cancel = function () {
                $scope.BUSY = false;
                $scope.edt = [];
                main.checked = false;
                $scope.div_shift = false;
                $scope.div_submit = true;
                $scope.gridNew = false;
                $scope.AllEmployeeName_data = [];
                $scope.edt = {
                    sh_start_date: $scope.ddMMyyyy,
                }

                $http.get(ENV.apiUrl + "api/common/ShiftMaster/getCompany").then(function (res) {
                    $scope.comp_data = res.data;
                    console.log($scope.comp_data[0].sh_company_code);
                    $scope.edt['sh_company_code'] = $scope.comp_data[0].sh_company_code;
                    $scope.getdepartment($scope.comp_data[0].sh_company_code);
                });

            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }])
})();