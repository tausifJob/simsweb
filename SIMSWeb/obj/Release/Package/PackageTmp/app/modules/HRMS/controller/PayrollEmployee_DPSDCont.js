﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('PayrollEmployee_DPSDCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.grid1 = false;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


            $http.get(ENV.apiUrl + "api/PaysGradeChange/Get_gc_company_Code").then(function (compcode) {
                debugger;
                $scope.comp_code = compcode.data;

            });

            $scope.getdept = function (dept) {

                $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/Get_dept_Code?company_code=" + dept).then(function (Department) {
                    $scope.dept = Department.data;
                });
            }

            $scope.getdesg = function (design) {
                $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/Get_designation_Code?company_code=" + design).then(function (Designation) {
                    $scope.designation = Designation.data;
                });
            }

            $scope.submit = function () {
                debugger;
                $scope.busy = true;
                $scope.grid1 = true;
                $scope.ImageView = false;
                if ($scope.temp.gc_company_code == undefined || $scope.temp.gc_company_code == '') {
                    $scope.temp.gc_company_code = '';
                }
                if ($scope.temp.em_desg_code == undefined || $scope.temp.em_desg_code == '') {
                    $scope.temp.em_desg_code = '';
                }
                if ($scope.temp.dg_code == undefined || $scope.temp.dg_code == '') {
                    $scope.temp.dg_code = '';
                }
                if ($scope.temp.em_number == undefined || $scope.temp.em_number == '') {
                    $scope.temp.em_number = '';
                }
                $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/GetPayrollEmployee?com_code=" + $scope.temp.gc_company_code + '&dept_code=' + $scope.temp.dg_code + '&deg_code=' + $scope.temp.em_desg_code + '&empid=' + $scope.temp.em_number).then(function (Payroll) {

                    $scope.filteredTodos = Payroll.data;
                    $scope.totalItems = $scope.filteredTodos.length;
                    $scope.todos = $scope.filteredTodos;
                    //$scope.makeTodos();
                    $scope.busy = false;
                    $scope.page1 = true;
                    console.log($scope.filteredTodos);
                    if (Payroll.data.length > 0) { }
                    else {
                        $scope.ImageView = true;
                    }
                });
            }

            //DATA Cancel
            $scope.Cancel = function () {
                //$scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                //$scope.grid1 = false;
                //$scope.filteredTodos = '';
                //$scope.todos.slice = '';
                //$scope.page1 = false;
                $('#myModal').modal('hide');
            }

            //DATA Reset
            $scope.reset = function () {
                debugger
                $scope.temp = "";
                $scope.j = [];
                $scope.filteredTodos = '';
                $scope.todos.slice = '';
                $scope.page1 = false;
                $scope.table = true;
                $scope.grid1 = false;
                $scope.display = true;
            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = true;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.number = true;
                $scope.company = true;
                $scope.total = false;
                $scope.divcode_readonly = true;
                $scope.grid2 = false;
                $('#myModal').modal('show');
                $scope.save_btn = false;
                $scope.temp.em_number = str.em_number;
                $scope.Employee_name = str.sh_emp_name;
                var dt = new Date();
                $scope.pa_effective_from = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
                $scope.Show();
            }

            $scope.Closehistory = function () {
                $scope.grid3 = false;
                $scope.grid2 = true;
            }

            //Show
            $scope.Show = function () {
                debugger;
                $scope.total = true;
                $scope.grid2 = true;
                $scope.save_btn = true;
                $scope.busy = true;
                $scope.Employee_Details = $scope.temp.em_number;
                $http.post(ENV.apiUrl + "api/PayrollEmployeeDPSD/RemaingPaycode?sd_number=" + $scope.temp.em_number + '&start=' + $scope.pa_effective_from + '&end=' + $scope.rdp_effective_upto).then(function (RemaingPaycode) {
                    $scope.RemaingPaycode = RemaingPaycode.data;

                });

                $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/GetSalaryGeneratedForSelectedMonthOrNot?sd_number=" + $scope.temp.em_number + '&start=' + $scope.pa_effective_from + '&end=' + $scope.rdp_effective_upto).then(function (Sal) {
                    $scope.sal = Sal.data;
                    $scope.total = 0;
                    for (var j = 0; j < Sal.data.length; j++) {
                        $scope.total = $scope.total + Sal.data[j].pd_amount;
                    }
                });

                //DATA SAVE INSERT
                $scope.History = function () {
                    $http.post(ENV.apiUrl + "api/PayrollEmployeeDPSD/HistoryPaycode?sd_number=" + $scope.temp.em_number).then(function (HistoryPaycode) {
                        $scope.HistoryPaycode = HistoryPaycode.data;
                        debugger;
                        if ($scope.HistoryPaycode.length > 0) {
                            $scope.grid2 = false;
                            $scope.grid3 = true;
                            
                        }
                        else
                            swal('', 'No history found.');
                    });


                }


                $scope.Reset = function () {
                    $scope.temp.pd_pay_code = '';
                    $scope.pd_amount = '';
                    var dt = new Date();
                    $scope.pa_effective_from = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
                    $scope.rdp_effective_upto = '';

                }
                $scope.ADD_NEW = function () {

                    var abc = [];
                    debugger;
                    if ($scope.temp.pd_pay_code == '' || $scope.temp.pd_pay_code == undefined) {
                        swal('', 'Please select paycode.');
                    }
                    else {
                        if ($scope.pd_amount == '' || $scope.pd_amount == undefined) {
                            swal('', 'Please enter amount.');
                        }
                        else {
                            var data = {
                                em_number: $scope.Employee_Details,
                                em_company_code: '01',
                                pa_effective_from: $scope.pa_effective_from,
                                code: $scope.rdp_effective_upto,
                                pd_pay_code: $scope.temp.pd_pay_code,
                                pd_amount: $scope.pd_amount
                            }
                            abc.push(data);
                            $http.post(ENV.apiUrl + "api/PayrollEmployeeDPSD/InsertUpdatePaysPayable", abc).then(function (pqrs) {
                                debugger
                                $scope.msg2 = pqrs.data;
                                if ($scope.msg2 == true) {
                                    swal({ title: "Alert", text: "Record Added Successfully", width: 300, height: 200 });
                                    $scope.Reset();
                                    $scope.Show();
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Added", width: 300, height: 200 });
                                }
                            });
                        }
                    }
                }

                var datasend = [];
                $scope.Save = function () {
                    debugger;
                    datasend = [];


                    for (var i = 0; i < $scope.sal.length; i++) {
                        if ($scope.sal[i].ischange == true) {
                            $scope.sal[i].em_number = $scope.temp.em_number;
                            $scope.sal[i].em_company_code = $scope.temp.gc_company_code;
                            if ($scope.sal[i].pd_amount != null && $scope.sal[i].pa_effective_from == null) {
                                $scope.sal[i].pa_effective_from = $scope.pa_effective_from;
                            }
                            datasend.push($scope.sal[i]);
                        }
                    }


                    $http.post(ENV.apiUrl + "api/PayrollEmployeeDPSD/InsertUpdatePaysPayable", datasend).then(function (Sal) {
                        debugger
                        $scope.msg1 = Sal.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Inserted/Updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Inserted/Updated", width: 300, height: 200 });
                        }
                        $scope.getgrid();
                    });
                    $scope.getgrid();
                    $scope.Cancel();
                }

                $scope.SetIschange = function (info) {
                    info.ischange = true;
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/GetSalaryGeneratedForSelectedMonthOrNot?sd_number=" + $scope.temp.em_number + '&start=' + $scope.pa_effective_from + '&end=' + $scope.rdp_effective_upto).then(function (Sal) {
                    $scope.sal = Sal.data;
                });
            }

            $scope.onlyNumbers = function (event) {
                debugger
                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };


        }])

})();