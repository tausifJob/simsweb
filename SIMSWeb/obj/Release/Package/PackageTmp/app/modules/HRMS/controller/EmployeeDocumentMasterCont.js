﻿(function () {
    'use strict';
    var obj1, obj2, edt, main;
    var opr = '';
    var empdocdetailcode = [];
    var categorycode = [];

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeDocumentMasterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.EmployeeDetail = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.editmode = false;
            var formdata = new FormData();
            var data1 = [];
            debugger
            var username = $rootScope.globals.currentUser.username;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.countData = [
                     { val: 5, data: 5 },
                     { val: 10, data: 10 },
                     { val: 15, data: 15 },

            ]
            $scope.size = function (str) {
                if (str == 5 || str == 10 || str == 15) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;

                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.getempoyeegrid = function () {
                $http.get(ENV.apiUrl + "api/EmpDocumentMaster/getEmpDocumentDetail").then(function (getEmpDocumentDetail_Data) {
                    debugger
                    $scope.EmpDocData = getEmpDocumentDetail_Data.data;

                    if ($scope.EmpDocData.length > 0) {

                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.EmpDocData.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.EmpDocData.length, data: 'All' })
                        }
                        $scope.totalItems = $scope.EmpDocData.length;
                        $scope.todos = $scope.EmpDocData;
                        $scope.makeTodos();
                    }
                    else {

                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                        $scope.totalItems = $scope.EmpDocData.length;
                        $scope.todos = $scope.EmpDocData;
                        $scope.makeTodos();
                    }



                    console.log($scope.EmpDocData);
                })

            }

            $http.get(ENV.apiUrl + "api/EmpDocumentMaster/getEmpDocumentDetail").then(function (getEmpDocumentDetail_Data) {
                debugger
                $scope.EmpDocData = getEmpDocumentDetail_Data.data;

                if ($scope.EmpDocData.length > 0) {

                    $scope.pager = true;
                    if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.EmpDocData.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.EmpDocData.length, data: 'All' })
                    }
                    $scope.totalItems = $scope.EmpDocData.length;
                    $scope.todos = $scope.EmpDocData;
                    $scope.makeTodos();
                }
                else {

                    swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                    $scope.totalItems = $scope.EmpDocData.length;
                    $scope.todos = $scope.EmpDocData;
                    $scope.makeTodos();
                }



                console.log($scope.EmpDocData);
            })

       


            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {

                        var v = document.getElementById($scope.filteredTodos[i].pays_doc_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].pays_doc_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }


            }

            $scope.check_once = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }

            $scope.deleterecord = function () {

                data1 = [];

                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].pays_doc_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'pays_doc_code': $scope.filteredTodos[i].pays_doc_code,
                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        confirmButtonText: 'Yes',
                        showCancelButton: true,
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/EmpDocumentMaster/CUDgetAllRecords", deleteleave).then(function (msg) {
                                debugger
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getempoyeegrid();
                                        }
                                        main = document.getElementById('all_chk');
                                        if (main.checked == true) {
                                            main.checked = false;
                                            {
                                                $scope.row1 = '';
                                            }
                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                                else {
                                    swal({ text: "Record Already Mapped, can't Delete", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getempoyeegrid();
                                        }
                                        main = document.getElementById('all_chk');
                                        if (main.checked == true) {
                                            main.checked = false;
                                            {
                                                $scope.row1 = '';
                                            }
                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].pays_doc_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.cancel = function () {
                $scope.EmployeeDetail = true;
                $scope.EmployeeDataoperation = false;
                $scope.searchemp = false;
                $('#myModal').modal('hide');
                $scope.edt = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                debugger
                data1 = [];
                $scope.myForm.$setPristine();
            
                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = {
                    pays_doc_status: true,

                }
                //$scope.edt = '';
                $scope.editmode = true;
                //$scope.Status = false;
                $scope.readonly = true;
                $scope.EmployeeDetail = false;
                $scope.EmployeeDataoperation = true;
                $scope.savebtn = true;

                //$scope.newbtn = false;
                //$scope.updatebtn = true;
                $scope.updatebtn = false;
                // $scope.edt = {};


            }

            $scope.Save = function (myForm) {
                debugger
                data1 = [];
                if (myForm) {
                    var flag = false;
                    var data = {
                        pays_doc_created_by: username,
                        pays_doc_desc: $scope.edt.pays_doc_desc,
                        pays_doc_status:$scope.edt.pays_doc_status,
                        opr: 'I'
                    }
                    data1.push(data);
                    if ($scope.filteredTodos.length == 0)
                    {
                        flag = true;
                    }

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        if ($scope.filteredTodos[i].pays_doc_desc == $scope.edt.pays_doc_desc) {
                            swal({ title: "Alert", text: "Record Already Exist", showCloseButton: true, width: 380, });
                            flag = false;
                            break;
                        }

                        else {
                            flag = true;
                        }
                    }
                    if (flag == true) {
                        $http.post(ENV.apiUrl + "api/EmpDocumentMaster/CUDgetAllRecords", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            $scope.EmployeeDataoperation = false;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record not Inserted", width: 300, height: 200 });
                            }
                            $scope.getempoyeegrid();
                        });
                        $scope.EmployeeDetail = true;

                    }
                }
            }

            $scope.up = function (objone) {

                $scope.edt = {
                    pays_doc_status: objone.pays_doc_status,
                    pays_doc_desc: objone.pays_doc_desc,
                    pays_doc_mod_code: objone.pays_doc_mod_code,
                    pays_doc_code: objone.pays_doc_code

                };
                //$scope.newmode = true;
                //$scope.check = true;
                //$scope.edt = '';
                //$scope.editmode = true;

                $scope.readonly = true;
                $scope.EmployeeDetail = false;
                $scope.EmployeeDataoperation = true;
                $scope.savebtn = false;
                //$scope.newbtn = false;
                $scope.updatebtn = true;
                $scope.uploadbtn = false;
                //$scope.edt = {};

            }

            $scope.Update = function (myForm) {

                data1 = [];
                if (myForm) {
                    var data = ({
                        pays_doc_desc: $scope.edt.pays_doc_desc,
                        pays_doc_code: $scope.edt.pays_doc_code,
                        pays_doc_status: $scope.edt.pays_doc_status,
                        opr: 'U'
                    });

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/EmpDocumentMaster/CUDgetAllRecords", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.EmployeeDataoperation = false;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record  updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record not updated", width: 300, height: 200 });
                        }
                        $scope.getempoyeegrid();
                    });
                    $scope.EmployeeDetail = true;

                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
            $scope.createdate = function (end_date, start_date, name) {

                var month1 = end_date.split("/")[0];
                var day1 = end_date.split("/")[1];
                var year1 = end_date.split("/")[2];
                var new_end_date = year1 + "/" + month1 + "/" + day1;
                //var new_end_date = day1 + "/" + month1 + "/" + year1;

                var year = start_date.split("/")[0];
                var month = start_date.split("/")[1];
                var day = start_date.split("/")[2];
                var new_start_date = year + "/" + month + "/" + day;
                // var new_start_date = day + "/" + month + "/" + year;

                if (new_end_date < new_start_date) {

                    $rootScope.strMessage = "Please Select Future Date";
                    $('#message').modal('show');

                    $scope.edt[name] = '';
                }
                else {

                    $scope.edt[name] = new_end_date;
                }
            }
            $scope.showdate = function (date, name) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                $scope.edt[name] = year + "/" + month + "/" + day;
            }
            simsController.directive('ngFiles', ['$parse', function ($parse) {

                function fn_link(scope, element, attrs) {
                    var onChange = $parse(attrs.ngFiles);
                    element.on('change', function (event) {
                        onChange(scope, { $files: event.target.files });
                    });
                };

                return {
                    link: fn_link
                }
            }])

        }]);

})();