﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var report_card_code;   
    var simsController = angular.module('sims.module.ReportCard');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ReportCardFrmCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

           
            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
           
           
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            //Select Data SHOW



            //Bind Combo

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCurriculum) {
                $scope.Curriculum = getCurriculum.data;
                //$scope.temp['sims_cur_code'] = $scope.Curriculum[0].sims_cur_code;
                //$scope.getacademicYear($scope.Curriculum[0].sims_cur_code);
                console.log($scope.Curriculum);
            });

            $http.get(ENV.apiUrl + "api/ReportCard/getReportCard").then(function (res1) {

                $scope.ReportcardData = res1.data;
                $scope.CountAll = $scope.ReportcardData.length;
                $scope.totalItems = $scope.ReportcardData.length;
                $scope.todos = $scope.ReportcardData;
                $scope.makeTodos();

            });

            $scope.GetCombo = function (curcode,year,grade,section) {
                debugger;
                $http.get(ENV.apiUrl + "api/ReportCard/getReportCardLevelName?cur_code=" + curcode + "&academic_year=" + year + "&grade_code=" + grade + "&section_name=" + section).then(function (res) {
                    debugger;
                    $scope.levelNameData = res.data;

                });


                $http.get(ENV.apiUrl + "api/ReportCard/getTermDetails?cur_code="+curcode+"&academic_year="+year).then(function (res) {
                    $scope.TermData = res.data;

                });


                $http.get(ENV.apiUrl + "api/ReportCard/getParamDesc?cur_code=" + curcode + "&academic_year=" + year + "&grade_code=" + grade + "&section_name=" + section).then(function (res) {
                    $scope.ParamDescData = res.data;

                });
            }

            $scope.getacademicYear = function (str) {
                $scope.curriculum_code = str;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (getAcademicYear) {
                    $scope.AcademicYear = getAcademicYear.data;
                    //$scope.temp['sims_academic_year'] = $scope.AcademicYear[0].sims_academic_year;
                    // $scope.getGrade($scope.Curriculum[0].sims_cur_code,$scope.AcademicYear[0].sims_academic_year)
                    console.log($scope.AcademicYear);
                })
            }


            $scope.getGrade = function (str1, str2) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str1 + "&academic_year=" + str2).then(function (getAllGrades) {
                    $scope.AllGrades = getAllGrades.data;
                  
                   
                })
            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                //$scope.CheckAllChecked();
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.ReportcardData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.ReportcardData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_grade_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_section_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_report_card_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_level_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_report_card_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.term_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_fee_amount == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {
                debugger;
                $scope.disabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = [];
                $scope.CurriculumReadonly = false;
                $scope.AcademicYearReadonly = false;
                $scope.GradeReadonly = false;
                $scope.LevelReadonly = false;
                $scope.SectionReadonly = false;
                report_card_code = '';
                $scope.temp = {
                    declaredate: yyyy + '-' + mm + '-' + dd,
                    publishdate: yyyy + '-' + mm + '-' + dd,
                    status: true,
                }
                $scope.edt = {
                    username:"Admin",
                }
             
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            $scope.myFunct = function (keyEvent) {
                debugger;
                if (keyEvent.which == 13)
                    $scope.temp.declaredate = yyyy + '-' + mm + '-' + dd;
            }

            $scope.myFunct1 = function (keyEvent) {
                debugger;
                if (keyEvent.which == 13)
                    $scope.temp.publishdate = yyyy + '-' + mm + '-' + dd;
            }


            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = [];
                $scope.table = true;
                $scope.display = false;
                $scope.edt = [];
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.sectionrbt = false;
                $scope.sectiontxt = true;
                $scope.Update_btn = true;
                $scope.readonlyAdjustableFeeType = true;
                $scope.CurriculumReadonly = true;
                $scope.AcademicYearReadonly = true;
                $scope.GradeReadonly = true;
                $scope.LevelReadonly = true;
                $scope.SectionReadonly = true;
               
              
                $scope.edt = {
                    username: str.user_name,
                }
                report_card_code = str.sims_report_card_code;
                $scope.getacademicYear(str.sims_cur_code);
                $scope.getGrade(str.sims_cur_code, str.sims_academic_year);
                $scope.getsection(str.sims_cur_code, str.sims_grade_code, str.sims_academic_year);
                $scope.GetCombo(str.sims_cur_code, str.sims_academic_year, str.sims_grade_code, str.sims_section_code);
                $scope.temp = {
                    status: str.sims_report_card_freeze_status,
                    sims_cur_code: str.sims_cur_code,
                    sims_academic_year: str.sims_academic_year,
                    sims_grade_code: str.sims_grade_code,
                    sims_section_code: str.sims_section_code,
                    sims_level_code: str.sims_level_code,
                    name: str.sims_report_card_name,
                    Description: str.sims_report_card_desc,
                    declaredate: str.sims_report_card_declaration_date,
                    publishdate: str.sims_report_card_publish_date,

                    sims_report_card_parameter_master_code: str.sims_report_card_parameter_master_code,
                    sims_term_code: str.sims_report_card_term,
                };

            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger;

                if (Myform) {
                   
                        datasend = [];
                        data = [];

                        var data = {
                            sims_cur_code: $scope.temp.sims_cur_code,
                            sims_academic_year: $scope.temp.sims_academic_year,
                            sims_grade_code: $scope.temp.sims_grade_code,
                            sims_section_code: $scope.temp.sims_section_code,
                            sims_level_code: $scope.temp.sims_level_code,
                            sims_report_card_name: $scope.temp.name,
                            sims_report_card_desc:$scope.temp.Description,
                            sims_report_card_declaration_date:$scope.temp.declaredate,
                            sims_report_card_publish_date:$scope.temp.publishdate,
                            sims_report_card_created_user:$scope.edt.username,
                            sims_report_card_freeze_status:$scope.temp.status,
                            param_desc: $scope.temp.sims_report_card_parameter_master_code,
                            term_name:$scope.temp.sims_term_code,
                        }
                        data.opr = 'I';
                        datasend.push(data);

                        $http.post(ENV.apiUrl + "api/ReportCard/InsertUpdateDeleteReportCard", datasend).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 380 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Inserted", width: 380 });
                            }
                            $scope.getgrid();
                        });
                        $scope.table = true;
                        $scope.display = false;
                    }

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                debugger;
                $scope.flag1 = false;
                if (Myform) {
                    var sectioncode1;
                    var sectioncode = [];


                    dataforUpdate = [];

                    var data = {
                        sims_cur_code: $scope.temp.sims_cur_code,
                        sims_academic_year: $scope.temp.sims_academic_year,
                        sims_grade_code: $scope.temp.sims_grade_code,
                        sims_section_code: $scope.temp.sims_section_code,
                        sims_level_code: $scope.temp.sims_level_code,
                        sims_report_card_name: $scope.temp.name,
                        sims_report_card_desc: $scope.temp.Description,
                        sims_report_card_declaration_date: $scope.temp.declaredate,
                        sims_report_card_publish_date: $scope.temp.publishdate,
                        sims_report_card_created_user: $scope.edt.username,
                        sims_report_card_freeze_status: $scope.temp.status,
                        param_desc: $scope.temp.sims_report_card_parameter_master_code,
                        term_name: $scope.temp.sims_term_code,
                        sims_report_card_code: report_card_code,
                    }

                    data.opr = "U";

                    dataforUpdate.push(data);

                    $http.post(ENV.apiUrl + "api/ReportCard/InsertUpdateDeleteReportCard", dataforUpdate).then(function (msg) {
                        debugger;
                        $scope.updateResult = msg.data;
                        if ($scope.updateResult == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 380 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 380 });
                        }
                        $scope.getgrid();
                    });

                    $scope.table = true;
                    $scope.display = false;
                }
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_report_card_name + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_report_card_name + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/ReportCard/getReportCard").then(function (res1) {

                    $scope.ReportcardData = res1.data;
                    $scope.totalItems = $scope.ReportcardData.length;
                    $scope.todos = $scope.ReportcardData;
                    $scope.makeTodos();

                });
            }

            $scope.Delete = function () {
                debugger;
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    //var t = $scope.filteredTodos[i].uom_code;
                    var v = document.getElementById($scope.filteredTodos[i].sims_report_card_name + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sims_cur_code': $scope.filteredTodos[i].sims_cur_code,
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                            'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                            'sims_level_code': $scope.filteredTodos[i].sims_level_code,
                            'sims_report_card_code': $scope.filteredTodos[i].sims_report_card_code,
                            //'sims_report_card_desc': $scope.filteredTodos[i].sims_report_card_desc,
                            //'sims_report_card_declaration_date': $scope.filteredTodos[i].sims_report_card_declaration_date,
                            //'sims_report_card_publish_date': $scope.filteredTodos[i].sims_report_card_publish_date,
                            //'sims_report_card_created_user': $scope.filteredTodos[i].sims_report_card_created_user,
                            //'sims_report_card_freeze_status': $scope.filteredTodos[i].sims_report_card_freeze_status,
                            //'param_desc': $scope.filteredTodos[i].param_desc,
                            //'term_name': $scope.filteredTodos[i].sims_report_card_term,
                            'opr': 'D',

                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {

                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/ReportCard/InsertUpdateDeleteReportCard", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;



                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Already Mapped.Can't be Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }

                            });
                        }
                        else {

                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_report_card_name + i);
                                //var t = $scope.filteredTodos[i].sims_fee_category;
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");


                                }


                            }

                        }

                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }

                $scope.currentPage = str;
            }

            $(function () {
                $('#app_on1').multipleSelect({
                    width: '100%'
                });
            });



            $scope.getsection = function (str, str1, str2) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + str + "&grade_code=" + str1 + "&academic_year=" + str2).then(function (Allsection) {
                    $scope.getSectionFromGrade = Allsection.data;
                    setTimeout(function () {
                        $('#app_on1').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                })
            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


        }])

})();
