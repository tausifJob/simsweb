﻿(function () {
    'use strict';
    var data1 = [];
    var dataforInsert = [];
    var dataforUpdate = [];
    var temp, main;
    var deleteReportCard = [];
    var simsController = angular.module('sims.module.ReportCard');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ReportCardAllocationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.edt = [];
            $scope.Expand1 = true
            $scope.Expand = true;
            $scope.Reset = true;
            $scope.savebtn = false;
            $scope.giveallocation1 = false;
            $scope.searched_data = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.SelectAllCategory = function (info) {
                debugger
                for (var j = 0; j < info.categories.length; j++) {
                    if (info.ischecked == true) {
                        info.categories[j].ischecked = true;

                        for (var k = 0; k < info.categories[j].assignMents.length; k++) {

                            info.categories[j].assignMents[k].ischecked = true;
                        }

                        info.cnt = parseInt(info.cnt) + 1;
                    }
                    else {
                        info.categories[j].ischecked = false;
                        info.cnt = 0;

                        for (var k = 0; k < info.categories[j].assignMents.length || k < info.categories[j].assignMents.undefined; k++) {


                            info.categories[j].assignMents[k].ischecked = false;
                        }
                    }
                }
            }

            $scope.SelectOneByOneCategory = function (str, info, info1) {

                var cnt1 = parseInt(str.categories.length);

                var check = document.getElementById(info1.sims_gb_cat_code + info1.sims_gb_number);

                if (check.checked == true) {
                    info1.ischecked = true;

                    str.cnt = parseInt(str.cnt) + 1;
                    if (cnt1 == str.cnt) {
                        str.ischecked = true;
                    }

                    else {
                        str.ischecked = false;
                    }
                }
                else {
                    str.cnt = parseInt(str.cnt) - 1;
                    str.ischecked = false;
                    info1.ischecked = false;
                }

            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                debugger
                $scope.curriculum = res.data;
                //$scope.edt['sims_cur_code'] = curriculum.data[0].sims_cur_code;
                //$scope.getacyr(curriculum.data[0].sims_cur_code);
                //$scope.edt['sims_cur_code'] = res.data[0].sims_cur_short_name_en;
            })

            $scope.getacyr = function (str) {

                $scope.cur_code = str;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                })

            }

            $scope.getGrade = function (str, str1) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str + "&academic_year=" + str1).then(function (gradedata) {
                    debugger;
                    $scope.grade = gradedata.data;
                })
            }

            $scope.getsection = function (cur_code, grade_code, academic_year) {

                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + cur_code + "&grade_code=" + grade_code + "&academic_year=" + academic_year).then(function (Allsection) {
                    $scope.section1 = Allsection.data;
                })
            };

            $scope.getTerms = function (cur_code, academic_year) {
                $http.get(ENV.apiUrl + "api/ReportCardAllocation/GetAllterm?cur_code=" + cur_code + "&academic_year=" + academic_year).then(function (getAllTerms_Data) {
                    $scope.AllTerms_Data = getAllTerms_Data.data;
                });
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 8;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.Reset1 = function () {


                $scope.datatable = false;
                $scope.search_data_model = [];
                $scope.edt.marks = '';
                $scope.edt['edt.markupdate'] = false;
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.StudenteData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.StudenteData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in 2 fields */
                return (item.sims_student_passport_full_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_student_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_student_enroll_number == toSearch) ? true : false;
            }

            $scope.Fetch_Data = function () {
                if ($scope.edt.sims_cur_code == undefined || $scope.edt.sims_cur_code == "") {
                    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please select curriculum", showCloseButton: true, width: 380, });

                }
                else if ($scope.edt.sims_academic_year == undefined || $scope.edt.sims_academic_year == "") {
                    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please select curriculum Year", showCloseButton: true, width: 380, });
                }
                else if ($scope.edt.sims_grade_code == undefined || $scope.edt.sims_grade_code == "") {
                    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please select Grade", showCloseButton: true, width: 380, });
                }
                else if ($scope.edt.sims_section_name == undefined || $scope.edt.sims_section_name == "") {
                    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please select Section", showCloseButton: true, width: 380, });
                }
                else if ($scope.edt.sims_term_code == undefined || $scope.edt.sims_term_code == "") {
                    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please select Term", showCloseButton: true, width: 380, });
                }
                else {
                    data1 = [];
                    debugger
                    $http.get(ENV.apiUrl + "api/ReportCardAllocation/Get_report_card_code?cur_code=" + $scope.edt.sims_cur_code + "&acadmic_yr=" + $scope.edt.sims_academic_year + "&grade_code=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_name + "&term=" + $scope.edt.sims_term_code).then(function (result) {
                        $scope.reportcardcode = result.data;
                        console.log($scope.reportcardcode);
                    });
                    var data = {
                        term_code: $scope.edt.sims_term_code,
                        academic_year: $scope.edt.sims_academic_year,
                        grade_code: $scope.edt.sims_grade_code,
                        section_code: $scope.edt.sims_section_name,
                        OPR: "AS"
                    }
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/ReportCardAllocation/GbListProps", data1).then(function (result) {
                        $scope.reportcardcodeallocation = result.data;
                        console.log($scope.reportcardcodeallocation);
                    });
                    $scope.savebtn = true;
                    $scope.giveallocation1 = true;
                }
            }

            $scope.giveallocation = function () {
                $('#myModal_Give_Allocation').modal('show');
                //if ($scope.temp.sims_report_card_master_code == true) {
                $http.get(ENV.apiUrl + "api/ReportCardAllocation/Get_report_card_code?cur_code=" + $scope.edt.sims_cur_code + "&acadmic_yr=" + $scope.edt.sims_academic_year + "&grade_code=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_name + "&term=" + $scope.edt.sims_term_code).then(function (result) {
                    debugger
                    $scope.reportcardcode = result.data;
                    console.log($scope.reportcardcode);
                    setTimeout(function () {
                        debugger;
                        $('#cmb_report_Code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
                // }
            }
            $(function () {
                $('#cmb_report_Code').multipleSelect({
                    width: '100%'
                });
            });


            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#txtMarks").removeAttr("disabled");
                        $("#txtMarks").focus();
                    } else {
                        $("#txtMarks").attr("disabled", "disabled");
                    }
                });
            });

            $scope.getComboboxvalue = function () {
                var reportcard, reportcard1;
                if ($scope.edt.sims_report_card_master_code.length == 0 || $scope.edt == undefined) {
                    dept_no = '';
                }
                else {
                    reportcard1 = $scope.edt.sims_report_card_master_code;
                    reportcard = reportcard + ',' + reportcard1;
                }
            }

            $scope.report = function (str) {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $scope.edt.reportcardcheck = true;
                        $scope.sims_report_card_master_code = $scope.sims_report_card_master_code + '/' + str.sims_report_card_master_code;

                        $http.get(ENV.apiUrl + "api/ReportCardAllocation/Get_Report_card_allocation").then(function (result) {
                            debugger
                            $scope.search_data = result.data;
                            console.log($scope.search_data);
                            $scope.searched_data = true;
                        });
                    }
                    else {
                        $scope.edt.reportcardcheck = false;
                    }
                });

            }

            //$scope.chk_gb_name = function () {
            //    $http.get(ENV.apiUrl + "api/ReportCardAllocation/Get_Report_card_allocation?cur_code=" + $scope.edt.sims_cur_code + "&acadmic_yr=" + $scope.edt.sims_academic_year + "&grade_code=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_name + "&report_card=" + '01').then(function (result) {
            //        debugger
            //        $scope.search_data = result.data;
            //        console.log($scope.search_data);
            //        $scope.searched_data = true;
            //        setTimeout(function () {
            //            debugger;
            //            $('#cate').change(function () {
            //                console.log($(this).val());
            //            }).multipleSelect({
            //                width: '100%'
            //            });
            //        }, 1000);
            //    });
            //}

            $scope.changeAllMark = function () {
                debugger;
                for (var i = 0; i < $scope.search_data_model.length; i++) {
                    $scope.search_data_model[i].sims_report_card_allocation = $scope.edt.marks;
                }
            }

            $scope.show_data = function () {
                debugger
                $scope.edt.report_card_code = '01'
                $http.get(ENV.apiUrl + "api/ReportCardAllocation/Get_Report_card_allocation?cur_code=" + $scope.edt.sims_cur_code + "&acadmic_yr=" + $scope.edt.sims_academic_year + "&grade_code=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_name + "&report_card=" + $scope.edt.report_card_code).then(function (result) {
                    debugger
                    $scope.search_data_model = result.data;
                    console.log($scope.search_data_model);
                    $scope.datatable = true;
                });
            }

            $scope.Reset = function () {
                $scope.edt.sims_cur_code = "";
                $scope.edt.sims_academic_year = "";
                $scope.edt.sims_grade_code = "";
                $scope.edt.sims_section_name = "";
                $scope.edt.sims_term_code = "";
                $scope.searched_data = false;
                $scope.reportcardcode = [];
                $scope.reportcardcodeallocation = [];
                $scope.search_data = [];
                $scope.savebtn = false;
                $scope.giveallocation1 = false;
                $scope.edt['reportcardcheck'] = false;

            }

            $scope.check_all = function () {
                debugger
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.search_data_model.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.search_data_model.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }
            $scope.check_once = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.Update = function () {
                debugger
                var flag = false;
                for (var i = 0; i < $scope.search_data_model.length; i++) {
                    //if ($scope.filteredTodos[i].sims_library_item_qty == "0") {
                    //    swal('', 'Please Enter Quantity Atleast 1');
                    //    //flag = false;
                    //    return;
                    //}
                    if ($scope.search_data_model[i].sims_report_card_status != 'A') {

                        $scope.search_data_model[i].opr = 'U';
                        $scope.search_data_model[i];
                        dataforUpdate.push($scope.search_data_model[i]);
                        flag = true;
                    }
                }

                if (flag == true) {
                    $http.post(ENV.apiUrl + "api/ReportCardAllocation/Sims506_Update_Report_card_allocation", dataforUpdate).then(function (msg) {

                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                            dataforUpdate = [];
                            $scope.filteredTodos = [];
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                    })
                }
                else {

                    swal('', 'Record Already Updated');
                }


            }

            $scope.Save = function () {
                debugger
                dataforInsert = [];
                if ($scope.edt.reportcardcheck == undefined || $scope.edt.reportcardcheck == "") {
                    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please Select Report Card", showCloseButton: true, width: 380, });
                }

                    //else if ($scope.i.ischecked == undefined || $scope.i.ischecked == "") {
                    //    $scope.flag1 = true;
                    //    swal({ title: "Alert", text: "Please select Assignment", showCloseButton: true, width: 380, });
                    //}

                else {
                    var f = false;
                    for (var i = 0; i < $scope.reportcardcodeallocation.length; i++) {
                        debugger;
                        if ($scope.reportcardcodeallocation[i].ischecked == true) {
                            f = true;

                        }
                    }
                    if (!f) {
                        swal({ title: "Alert", text: "Please select Assignment", showCloseButton: true, width: 380, });
                    }
                    else {
                        for (var i = 0; i < $scope.reportcardcodeallocation.length; i++) {
                            debugger;

                            if ($scope.reportcardcodeallocation[i].categories == undefined) {
                                $scope.reportcardcodeallocation[i].categories = '';
                                // $scope.reportcardcodeallocation[i].categories = 0;
                            }
                            for (var j = 0; j < $scope.reportcardcodeallocation[i].categories.length; j++) {
                                debugger;
                                if ($scope.reportcardcodeallocation[i].categories[j].assignMents == undefined) {
                                    $scope.reportcardcodeallocation[i].categories[j].assignMents = '';
                                    //$scope.reportcardcodeallocation[i].categories[j].assignMents = 0;


                                }

                                for (var k = 0; k < $scope.reportcardcodeallocation[i].categories[j].assignMents.length; k++) {
                                    debugger;
                                    if ($scope.reportcardcodeallocation[i].categories[j].assignMents[k].ischecked == true) {
                                        debugger;
                                        for (var m = 0; m < $scope.search_data.length; m++) {
                                            var data = {
                                                sims_cur_code: $scope.search_data[m].sims_cur_code
                                            , sims_academic_year: $scope.search_data[m].sims_academic_year
                                            , sims_grade_code: $scope.search_data[m].sims_grade_code
                                            , sims_section_code: $scope.search_data[m].sims_section_code
                                            , sims_report_card_level: $scope.search_data[m].sims_report_card_level
                                            , sims_report_card_master_code: $scope.search_data[m].sims_report_card_master_code
                                            , sims_report_card_serial_number: $scope.search_data[m].sims_report_card_serial_number
                                            , sims_report_card_allocation: $scope.search_data[m].sims_report_card_allocation
                                            , sims_report_card_status: $scope.search_data[m].sims_report_card_status
                                            , sims_gb_number: $scope.reportcardcodeallocation[i].categories[j].sims_gb_number
                                            , sims_gb_cat_code: $scope.reportcardcodeallocation[i].categories[j].assignMents[k].sims_gb_cat_number
                                            , sims_gb_cat_assign_number: $scope.reportcardcodeallocation[i].categories[j].assignMents[k].assignment_number
                                            };
                                            dataforInsert.push(data);
                                        }
                                    }

                                }

                            }

                        }

                        $http.post(ENV.apiUrl + "api/ReportCardAllocation/Sims506_Insert_Report_Card", dataforInsert).then(function (msg) {
                            debugger
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 380 });
                                dataforInsert = [];
                                $scope.search_data = [];
                                $scope.edt.sims_cur_code = "";
                                $scope.edt.sims_academic_year = "";
                                $scope.edt.sims_grade_code = "";
                                $scope.edt.sims_section_name = "";
                                $scope.edt.sims_term_code = "";
                                $scope.searched_data = false;
                                $scope.reportcardcode = [];
                                $scope.reportcardcodeallocation = [];
                                //  $scope.search_data = [];
                                $scope.savebtn = false;
                                $scope.giveallocation1 = false;
                                $scope.edt['reportcardcheck'] = false;
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Inserted", width: 380 });
                            }
                        });
                    }
                }
            }
            $scope.delete = function (obj) {
                debugger
                $scope.flag = true;
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?" + obj.sims_section_name + obj.sims_grade_name + obj.sims_gb_name,
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger
                            //for (var i = 0; i < $scope.search_data.length; i++) {
                            var deleteReportCardDetail = ({
                                'sims_academic_year': obj.sims_academic_year,
                                'sims_cur_code': obj.sims_cur_code,
                                'sims_grade_code': obj.sims_grade_code,
                                'sims_section_name': obj.sims_section_code,
                                'sims_report_card_level': obj.sims_report_card_level,
                                'sims_report_card_code': obj.sims_report_card_master_code,
                                'sims_gb_number': obj.sims_gb_number,
                                'sims_gb_cat_assign_number': obj.sims_gb_cat_assign_number,
                                'sims_gb_cat_code': obj.sims_gb_cat_code,
                            });
                            deleteReportCard.push(deleteReportCardDetail);
                            //}
                            $http.post(ENV.apiUrl + "api/ReportCardAllocation/Sims506_Delete_Report_card", deleteReportCard).then(function (msg) {
                                $scope.msg1 = msg.data;
                                debugger
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", width: 300, height: 200 });
                                }
                                else {

                                    swal({ title: "Alert", text: "Record Not Deleted", width: 300, height: 200 });
                                }
                                deleteReportCard = [];
                            });
                        }
                    });
                    $http.get(ENV.apiUrl + "api/ReportCardAllocation/Get_Report_card_allocation").then(function (Student_Data) {
                        $scope.AttendanceCode_Data = Student_Data.data;
                        $scope.totalItems = $scope.AttendanceCode_Data.length;
                        $scope.todos = $scope.AttendanceCode_Data;
                        $scope.makeTodos();

                    });
                }


            }
        }])
})();
