﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.users');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('LoginController',
        ['$scope', '$state', '$rootScope', 'AuthenticationService', '$timeout','$location', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, AuthenticationService, $timeout,$location, gettextCatalog, $http, ENV) {
           
            $scope.username = "";
            $scope.password = "";
            $scope.schoolUrl = "";
            $scope.user = {};
            $scope.signup = function () { }
            $scope.showForgetPaaword = false;
            $scope.toggleLoginScreen = function () {
                $scope.showForgetPaaword = !$scope.showForgetPaaword;
            };

            if (document.referrer != "") {
                var referringURL = document.referrer;
                $scope.schoolUrl = referringURL;
                var conStr = $scope.schoolUrl.substring($scope.schoolUrl.indexOf('/') + 2, $scope.schoolUrl.indexOf('.'));
                $http.defaults.headers.common['schoolId'] = 'sis';


                console.log(referringURL);
                var local = referringURL.substring(referringURL.indexOf("?"), referringURL.length);
                console.log(local);

                //location.href = "http://page.com/login" + local; 
            } else {


                //$scope.schoolUrl = window.location.href;
              //  var conStr = $scope.schoolUrl.substring($scope.schoolUrl.indexOf('/') + 2, $scope.schoolUrl.indexOf('.'));
                //$http.defaults.headers.common['schoolId'] = 'sis';

                $scope.schoolUrl = window.location.href;
                var conStr = $scope.schoolUrl.substring($scope.schoolUrl.indexOf('/') + 2, $scope.schoolUrl.indexOf('.'));
                if (conStr == 'www' || conStr == 'WWW') {
                    $scope.schoolUrl = window.location.href;

                    var conStr = $scope.schoolUrl.split('.')[1];
                    $http.defaults.headers.common['schoolId'] = conStr;

                }
                else {
                    $http.defaults.headers.common['schoolId'] = "sis";
                }
            }
            $scope.SchoolLogo = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/SchoolLogo/';

            //$http.get(ENV.apiUrl + "api/connection/getConnection?schoolID=" + conStr).then(function (res) {
            //    console.log(res.data);
            //});


         

            $scope.Login = function () {
               
                if ($scope.showForgetPaaword) {
                    $scope.toggleLoginScreen();
                } else {
                  
                    $scope.showMessage = false;
                   
                    AuthenticationService.Login($scope.username, $scope.password, function (response) {
                        console.log(response);
                        $scope.showMessage = true;
                        if (response.success == false) {
                            $scope.showMessage = true;
                            $scope.messageClass = "alert alert-warning alert-dismissable";
                            $scope.message = "Invalid username or password!!!";
                        } else {
                            $scope.showMessage = true;
                            $scope.messageClass = "alert alert-success alert-dismissable";
                            AuthenticationService.ClearCredentials();
                            AuthenticationService.SetCredentials($scope.username, $scope.password);
                            $scope.message = "SuccessFully Authenticated!!!";
                            $timeout(function () {
                                $state.go("main.EmpDshBrd");
                               // $state.go("main.common");
                            });
                        }                        
                    });


                  

                }
            };


            var qs = $location.search();
            if (!angular.equals({}, qs)) {
                var t = qs.q.split("-")
                if (t.length >= 2) {
                    $scope.username = t[1];
                    $scope.password = t[0];
                }
                console.log($scope.username + '' + $scope.password)
                $scope.Login();
            }

            //$(window).bind('keydown', function (event) {

            //    if (event.keyCode == '13') {





            //        $scope.Login();

            //    }
            //});

            $scope.onKeyPress = function ($event) {
                if ($event.keyCode == 13) {

                    $scope.Login();
                    // console.log("enetr");
                }
            };

         
            $http.get(ENV.apiUrl + "api/common/getSchoolDetails").then(function (res) {
                $scope.display = true;
                $scope.obj1 = res.data;
                console.log(res.data);
                $scope.SchoolDetails = res.data;
               
            });

            $scope.forgotPassword_link = function () {
                $scope.email_id = false;

                $http.get(ENV.apiUrl + "api/authenticate/GetGroups").then(function (res) {
                    $scope.grp = res.data;
                    console.log($scope.grp);

                });
                $('#UploadDocModal').modal({ backdrop: 'static', keyboard: false });

            }


            $scope.getemailId = function () {
                $scope.email_id = true;
            }


            $scope.Save = function () {
                //  if (isvalidate)
                if ($scope.edt.sims_email_id != undefined) {
                    var code = "";
                    $http.get(ENV.apiUrl + "api/authenticate/GetUserName?strEmailID=" + $scope.edt.sims_email_id + "&strCode=" + code + "&strgroupname=" + $scope.edt.comn_user_group_code).then(function (res) {
                        $scope.emaildata = res.data;
                        console.log($scope.emaildata);
                        if (res.data != 'false') {

                            $scope.edt.mail_success = "Your Username is :'" + $scope.emaildata + "'";
                        }
                        else {
                            $scope.edt.mail_success = "No User Name is Found!!!";
                        }

                    });
                }
                if ($scope.edt.sims_email_id == undefined) {
                    $scope.edt.mail_success = "Please Enter Email";
                }

            }

            $scope.Cancel = function () {
                $scope.edt.mail_success_lbl = "";
                $scope.edt.mail_success = "";
                $scope.edt.sims_email_id = "";

                $scope.edt.comn_user_group_code = null;
                $scope.email_id = false;
            }

            $scope.edt =
             {
                 sims_user_name_en: undefined
             }

            $scope.pwdsave = function () {
                debugger
                $scope.fbtn = true;
                if (!$scope.flgMobi) {
                    if ($scope.securitydata == $scope.edt.emailId) {
                        $scope.pwd = false;

                        // $http.get(ENV.apiUrl + "api/authenticate/getResetPasswordNew?strUsername=" + $scope.edt.sims_user_name_en).then(function (res_pwd) {
                        var data = {
                            emailsendto: $scope.securitydata,
                            sender_emailid: $scope.securitydata,
                            subject: 'Parent Password',
                            //body: 'Your Password is:' + res_pwd.data,
                            body: 'please click here for reset password http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/ppn/ChangePassword.html#?foo=' + $scope.edt.sims_user_name_en
                        }

                        $http.get(ENV.apiUrl + "api/authenticate/GetSendEmail?strUsername=" + Base64.encode($scope.edt.sims_user_name_en) + "&email=" + $scope.securitydata).then(function (res_pwd) {
                            $scope.fbtn = false;
                            $scope.pwd = true;
                            $scope.edt.pwd_success = 'Your password is sent to your email id';
                        });
                        //  });
                    }
                    else {
                        $scope.pwd = true;

                        $scope.edt.pwd_success = "Please Enter Correct Email!!!";
                        $scope.fbtn = false;

                    }
                }
                else {
                    $scope.pwd = false;

                    debugger
                    if ($scope.securitydata == $scope.edt.emailId) {

                        // $http.get(ENV.apiUrl + "api/authenticate/getResetPasswordNew?strUsername=" + $scope.edt.sims_user_name_en).then(function (res_pwd) {
                        //$scope.pwd = true;
                        //var data = {
                        //    emailsendto: $scope.securitydata,
                        //    sender_emailid: $scope.securitydata,
                        //    //subject: 'Parent Password',
                        //    //body: 'Your Password is:' + res_pwd.data,
                        //    body: 'please click here for reset password http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/ppn/ChangePassword.html#?foo=' + $scope.edt.sims_user_name_en
                        //}



                        $http.post(ENV.apiUrl + "api/authenticate/sendmobile?strUsername=" + $scope.edt.sims_user_name_en + '&mobile=' + $scope.securitydata + '&msg=' + 'please click here for reset password http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/ppn/ChangePassword.html#?foo=' + $scope.edt.sims_user_name_en, data).then(function (res_pwd) {
                            // $scope.fbtn = false;
                            $scope.pwd = true;

                            $scope.edt.pwd_success = 'Your password is sent to your mobile number';
                            $scope.fbtn = false;
                        });
                        //  });
                    }
                    else {
                        $scope.pwd = true;

                        $scope.edt.pwd_success = "Please Enter Correct Mobile!!!";
                        $scope.fbtn = false;

                    }

                }
                //if ($scope.edt.sims_user_name_en != null || $scope.edt.sims_user_name_en != undefined) {
                //    if ($scope.securitydata != null) {
                //        var str = $scope.securitydata.indexOf('~');
                //        var ques_code = $scope.securitydata.substr(str + 1);
                //    }

                //    if ($scope.edt.sims_seq_ans != undefined) {
                //        if ($scope.edt.sims_seq_ans != null || $scope.edt.sims_seq_ans != "") {
                //            $http.get(ENV.apiUrl + "api/authenticate/getResetPassword?strUsername=" + $scope.edt.sims_user_name_en + "&strSecurityCode=" + ques_code + "&strSecurityAns=" + $scope.edt.sims_seq_ans).then(function (res_pwd) {
                //                $scope.resetpwd = res_pwd.data;
                //                console.log($scope.resetpwd);
                //                if (res_pwd.data != 'false') {
                //                    $scope.edt.pwd_success = "Your New Password is:'" + $scope.resetpwd + "'";
                //                }
                //                else if (res_pwd.data == 'false') {
                //                    $scope.edt.pwd_success = "Please Fill Correct Information!!!";
                //                }
                //            });
                //        }
                //        else {
                //            $scope.edt.pwd_success = "Please Enter Security Answer!!!";
                //        }
                //    }
                //    else if ($scope.edt.sims_seq_ans == undefined) {
                //        $scope.edt.sims_seq_ans = "";
                //        $scope.edt.pwd_success = "Please Enter Security Answer!!!";
                //    }
                //}
                //else if ($scope.edt.sims_user_name_en == null || $scope.edt.sims_user_name_en == undefined) {

                //    $scope.edt.pwd_success = "Please Enter User Name!!!";
                //}
            }

            $scope.Cancel1 = function () {
                $scope.edt.sims_user_name_en = "";
                $scope.edt.pwd_success = "";
                $scope.edt.pwd_success_lbl = "";
                $scope.edt.sims_seq_ans = "";
                $scope.edt.sims_seq_que = "";

                $scope.edt.comn_user_group_code = "";
                //  $scope.edt = undefined;
                //  $scope.email_id = false;
                $('#UploadDocModal').modal('hide');

            }

            $scope.pwd = false;
            var arr;


            $scope.getsecurity = function () {

                if ($scope.edt.sims_user_name_en == null || $scope.edt.sims_user_name_en == '' || $scope.edt.sims_user_name_en == undefined) {
                    $scope.pwd = true;
                    $scope.edt.pwd_success = "Please Enter User Name!!!";

                }

                else {

                    $scope.pwd = false;

                    $http.get(ENV.apiUrl + "api/authenticate/GetEmail?strUsername=" + $scope.edt.sims_user_name_en).then(function (res) {
                        $scope.securitydata = res.data;
                        debugger;
                        console.log($scope.securitydata);
                        if ($scope.securitydata != "") {
                            // $scope.pwd = true;

                            //var str = $scope.securitydata.indexOf('~');
                            // arr = $scope.securitydata.substr(0, str);
                            //  $scope.edt.sims_seq_que = arr;
                        }
                        else if ($scope.securitydata == null || $scope.securitydata == "" || $scope.securitydata == undefined) {
                            $scope.edt.sims_seq_que = "";
                            //for Mobile
                            $scope.flgMobi = true;

                            $http.get(ENV.apiUrl + "api/authenticate/GetMobile?strUsername=" + $scope.edt.sims_user_name_en).then(function (res) {
                                debugger
                                $scope.securitydata = res.data;
                                if (res.data == "") {
                                    swal({ title: "Alert", text: "Your email and mobile number is not found please contact to administrator", imageUrl: "assets/img/notification-alert.png", });

                                }
                            });
                            // $scope.edt.pwd_success = "No Email id is Found.Check User Name!!!";
                        }

                    });
                }
                //else if ($scope.edt.sims_user_name_en == undefined) {
                //    $scope.edt.pwd_success = "Please Enter User Name!!!";
                //}
            }

           


        }]);

    var Base64 = {

        keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',

        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output +
                    this.keyStr.charAt(enc1) +
                    this.keyStr.charAt(enc2) +
                    this.keyStr.charAt(enc3) +
                    this.keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);

            return output;
        },

       

    };
})();