﻿(function () {
    'use strict';
    var obj1, obj2, temp, main;
    var opr = '';
    var BoardExamcode = [];

    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('BoardExamCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.edt = "";
            $scope.BoardExamDetail = true;
            $scope.editmode = false;
            var formdata = new FormData();
            var data1 = [];

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage);
                $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.check_all();
            }
            $scope.edt = "";

            $scope.cancel = function () {
                $scope.BoardExamDetail = true;
                $scope.BoardExamData1 = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $http.get(ENV.apiUrl + "api/BoardExamDetail/getBoardCode").then(function (res) {
                $scope.bcode = res.data;

            })

            $scope.New = function () {
                $scope.newmode = true
                $scope.edt = "";
                $scope.check = true;
                $scope.editmode = false;
                opr = 'S';
                $scope.readonly = false;
                $scope.BoardExamDetail = false;
                $scope.BoardExamData1 = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                // $scope.edt['sims_board_exam_status'] = true;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.BoardExamData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.BoardExamData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_board_exam_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_board_code == toSearch) ? true : false;
            }

            $http.get(ENV.apiUrl + "api/BoardExamDetail/getBoardExamDetail").then(function (Board_Exam_Data) {
                $scope.BoardExamData = Board_Exam_Data.data;
                $scope.totalItems = $scope.BoardExamData.length;
                $scope.todos = $scope.BoardExamData;
                $scope.makeTodos();

            });

            $http.get(ENV.apiUrl + "api/BoardExamDetail/getBoardCode").then(function (res) {
                $scope.bcode = res.data;
            })

            $scope.Save = function (myForm) {
                debugger
                if (myForm) {

                    var data = $scope.edt;
                    // $scope.edt = "";
                    data.opr = 'I';
                    $scope.exist = false;
                    data1.push(data);

                    for (var i = 0; i < $scope.BoardExamData.length; i++) {
                        if ($scope.BoardExamData[i].sims_board_code == data.sims_board_code && $scope.BoardExamData[i].sims_board_exam_code == data.sims_board_exam_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ text: "Record Already exists", width: 300, height: 200 });
                    }

                    else {
                        $http.post(ENV.apiUrl + "api/BoardExamDetail/BoardExamCUD?simsobj=", data1).then(function (msg) {

                            $scope.BoardExamData1 = false;
                            $http.get(ENV.apiUrl + "api/BoardExamDetail/getBoardExamDetail").then(function (Board_Exam_Data) {
                                $scope.BoardExamData = Board_Exam_Data.data;
                                $scope.totalItems = $scope.BoardExamData.length;
                                $scope.todos = $scope.BoardExamData;
                                $scope.makeTodos();

                                $scope.msg1 = msg.data;

                                if ($scope.msg1 == true) {

                                    swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                                }

                            });

                        });

                        $scope.BoardExamDetail = true;
                        $scope.BoardExamData1 = false;
                    }
                    data1 = [];
                }
            }

            $scope.up = function (str) {
                opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.BoardExamDetail = false;
                $scope.BoardExamData1 = true;

                $scope.edt = {
                    sims_board_code: str.sims_board_code
                            , sims_board_exam_code: str.sims_board_exam_code
                            , sims_board_exam_short_name: str.sims_board_exam_short_name
                            , sims_board_exam_name: str.sims_board_exam_name
                            , sims_board_exam_year: str.sims_board_exam_year
                            , sims_board_exam_fee: str.sims_board_exam_fee
                            , sims_board_exam_status: str.sims_board_exam_status


                };

            }

            $http.get(ENV.apiUrl + "api/BoardExamDetail/BoardExamCUD").then(function (Board_Exam_Data) {
                $scope.BoardExamData = Board_Exam_Data.data;
                $scope.totalItems = $scope.BoardExamData.length;
                $scope.todos = $scope.BoardExamData;
                $scope.makeTodos();

                console.log($scope.BoardExamData);
            });

            $scope.Update = function (myForm) {
                debugger
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'U';
                    $scope.edt = "";
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/BoardExamDetail/BoardExamCUD?simsobj=", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.BoardExamData1 = false;
                        $http.get(ENV.apiUrl + "api/BoardExamDetail/getBoardExamDetail").then(function (Board_Exam_Data) {
                            $scope.BoardExamData = Board_Exam_Data.data;
                            $scope.totalItems = $scope.BoardExamData.length;
                            $scope.todos = $scope.BoardExamData;
                            formdata = new FormData();
                            $scope.makeTodos();

                            if ($scope.msg1 == true) {
                                swal({ text: "Record Updated Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ text: "Record Not Updated", width: 300, height: 200 });
                            }
                        });

                    })
                    $scope.BoardExamData1 = false;
                    $scope.BoardExamDetail = true;
                }
            }

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.check_once = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }

            }

            $scope.deleterecord = function () {
                debugger;
                var data1 = [];
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_board_code': $scope.filteredTodos[i].sims_board_code,
                            'sims_board_exam_code': $scope.filteredTodos[i].sims_board_exam_code,
                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/BoardExamDetail/BoardExamCUD", deleteleave).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BoardExamDetail/getBoardExamDetail").then(function (Board_Exam_Data) {
                                                $scope.BoardExamData = Board_Exam_Data.data;
                                                $scope.totalItems = $scope.BoardExamData.length;
                                                $scope.todos = $scope.BoardExamData;
                                                $scope.makeTodos();
                                                main.checked = false;
                                            });
                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                                else {
                                    swal({ text: "Record Already Mapped, can't Delete", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BoardExamDetail/getBoardExamDetail").then(function (Board_Exam_Data) {
                                                $scope.BoardExamData = Board_Exam_Data.data;
                                                $scope.totalItems = $scope.BoardExamData.length;
                                                $scope.todos = $scope.BoardExamData;
                                                $scope.makeTodos();
                                                main.checked = false;
                                                $('tr').removeClass("row_selected");
                                            });
                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            main = document.getElementById('all_chk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
             
                $scope.currentPage = str;
            }



        }]);

})();