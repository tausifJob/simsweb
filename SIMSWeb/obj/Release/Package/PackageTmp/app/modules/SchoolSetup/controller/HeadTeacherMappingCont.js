﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });



    simsController.controller('HeadTeacherMappingCont',
        ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.display = true;
            $scope.grid = true;
            $scope.itemsPerPage = '50';
            $scope.currentPage = 0;
            var str, cnt;
            $scope.head_teacher_mapping = [];
            $scope.teacher_lists = [];
            $scope.test_teacher = [];
            $scope.page_div = false;
            var main;
            $scope.btn_save = true;
            $scope.btn_delete = false;
            var head_teacher_code = [];
            var date = new Date();
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss');

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixedtable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                debugger
                $scope.obj2 = res.data;
                if (res.data.length > 0) {
                    $scope.sims_cur_code = res.data[0].sims_cur_code;
                    $scope.GetData();
                }
            });


            $scope.GetData = function () {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.sims_cur_code).then(function (academicyears) {
                    debugger;
                    $scope.acad = academicyears.data;
                    if (academicyears.data.length > 0) {
                        $scope.sims_academic_year = academicyears.data[0].sims_academic_year;
                    }
                });

                $http.get(ENV.apiUrl + "api/ERP/HeadTeacherMapping/getSubjects?cur_code=" + $scope.sims_cur_code).then(function (res) {
                    $scope.subject = res.data;
                });
            }

            $scope.chk_subject = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/ERP/HeadTeacherMapping/getSims_Head_Teacher?cur_code=" + $scope.sims_cur_code + "&acad_yr=" + $scope.sims_academic_year + "&subject_code=" + $scope.edt.sims_subject_code).then(function (res) {
                    debugger
                    $scope.head_teacher_mapping = res.data;
                    $scope.page_div = true;
                    $scope.totalItems = $scope.head_teacher_mapping.length;
                    $scope.todos = $scope.head_teacher_mapping;
                    $scope.makeTodos();
                });
            }



            $scope.getTeacher = function () {
                // $('.nav-tabs a[href="#Global_Search_Modal"]').tab('show')
                $('#Teacher_Search_Modal').modal({ backdrop: 'static', keyboard: true });
                $scope.div_teacher = true;
                $scope.global_teacher_table = false;
            }

            $scope.Search_by_Teacher = function () {
                $scope.global_teacher_table = false;
                $scope.busy = true;

                $scope.searchbyparent = {
                    search_teacher_id: $scope.global_Search.search_teacher_id,
                    search_teacher_name: $scope.global_Search.search_teacher_name
                }

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchTeacher?data=" + JSON.stringify($scope.searchbyparent)).then(function (res) {
                    $scope.Teacher_result = res.data;
                    $scope.global_teacher_table = true;
                    $scope.busy = false;
                    console.log($scope.Teacher_result);
                });
            }

            $scope.Teachermodal_cancel = function () {
                $scope.glbl_obj = '';
                $scope.global_Search = '';
                //  $scope.Teacher_result = '';
                for (var i = 0; i < $scope.Teacher_result.length; i++) {
                    if ($scope.Teacher_result[i].teacher_id1 == true) {
                        $scope.teacher_lists.push($scope.Teacher_result[i]);
                        $scope.test_teacher = $scope.Teacher_result[i];
                        console.log($scope.teacher_lists);

                        $scope.tem =
                            {
                                sims_head_teacher_code: $scope.test_teacher.teacher_Name
                            }
                        $scope.edt.sims_head_teacher_code = $scope.tem.sims_head_teacher_code;
                    }

                }

                $scope.getTeacherData();

                $scope.Teacher_result = [];
            }

            $scope.getTeacherData = function () {
                if ($scope.test_teacher.teacher_id != '') {
                    $http.get(ENV.apiUrl + "api/ERP/HeadTeacherMapping/getTeacherDetails?head_teacher_code=" + $scope.test_teacher.user_name).then(function (res) {
                        $scope.Teacher = res.data;
                        if (res.data) {
                            //  console.log(res.data);
                            $scope.btn_save = false;

                            $http.get(ENV.apiUrl + "api/ERP/HeadTeacherMapping/getSims_Head_Teacher?cur_code=" + $scope.sims_cur_code + "&acad_yr=" + $scope.sims_academic_year + "&subject_code=" + $scope.edt.sims_subject_code).then(function (res) {
                                $scope.head_teacher_mapping = res.data;
                                $scope.page_div = true;
                                $scope.totalItems = $scope.head_teacher_mapping.length;
                                $scope.todos = $scope.head_teacher_mapping;
                                $scope.makeTodos();
                            });
                        }

                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Enter Teacher Code", showCloseButton: true, width: 380, });
                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 50, $scope.maxSize = 50;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                // console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.Save = function (isvalidate) {
                debugger
                var data1 = [];
                var data = [];
                $scope.insert = false;
                $scope.update = false;
                if (isvalidate) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        if ($scope.filteredTodos[i].ischecked == true) {
                            if ($scope.filteredTodos[i].sims_head_teacher_code == "" && $scope.filteredTodos[i].sims_head_teacher_status != false) {
                                var data = {
                                    sims_cur_code: $scope.filteredTodos[i].sims_cur_code,
                                    sims_academic_year: $scope.filteredTodos[i].sims_academic_year,
                                    sims_subject_code: $scope.filteredTodos[i].sims_subject_code,
                                    //sims_head_teacher_code: $scope.edt.sims_head_teacher_code,
                                    sims_head_teacher_code: $scope.test_teacher.user_name,
                                    sims_grade_code: $scope.filteredTodos[i].sims_grade_code,
                                    sims_section_code: $scope.filteredTodos[i].sims_section_code,
                                    sims_head_teacher_status: $scope.filteredTodos[i].sims_head_teacher_status,
                                    opr: 'I'
                                };

                                $scope.insert = true;
                                data1.push(data);


                            }
                            else if ($scope.filteredTodos[i].sims_head_teacher_code != "") {
                                var data = {
                                    sims_cur_code: $scope.filteredTodos[i].sims_cur_code,
                                    sims_academic_year: $scope.filteredTodos[i].sims_academic_year,
                                    sims_subject_code: $scope.filteredTodos[i].sims_subject_code,
                                    sims_head_teacher_code: $scope.test_teacher.user_name,
                                    // sims_head_teacher_code: $scope.filteredTodos[i].sims_head_teacher_code,
                                    sims_grade_code: $scope.filteredTodos[i].sims_grade_code,
                                    sims_section_code: $scope.filteredTodos[i].sims_section_code,
                                    sims_head_teacher_status: $scope.filteredTodos[i].sims_head_teacher_status,
                                    opr: 'U'
                                };

                                $scope.update = true;
                                data1.push(data);
                            }
                        }
                        //else
                        //{
                        //    swal({ title: "Alert", text: "Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", });
                        //}
                    }

                    if ($scope.insert) {
                        $http.post(ENV.apiUrl + "api/ERP/HeadTeacherMapping/CUD_Head_Teacher_Mapping", data1).then(function (res) {
                            $scope.Teacher = res.data;

                            if ($scope.Teacher == true) {
                                swal({ title: "Alert", text: "Head Teacher Mapped Successfully", showCloseButton: true, width: 380, });
                            }
                            else {
                                swal({ title: "Alert", text: "Head Teacher Not Mapped Successfully", showCloseButton: true, width: 380, });
                            }

                            $scope.getTeacherData();
                            // $scope.getsearch_details($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.edt.sims_subject_code, $scope.edt.sims_head_teacher_code);
                        });
                    }
                    else {
                        $http.post(ENV.apiUrl + "api/ERP/HeadTeacherMapping/CUD_Head_Teacher_Mapping", data1).then(function (res) {
                            $scope.Teacher = res.data;
                            if ($scope.Teacher == true) {
                                swal({ title: "Alert", text: "Head Teacher Mapping Updated Successfully", showCloseButton: true, width: 380, });
                            }
                            else {
                                swal({ title: "Alert", text: "Head Teacher Mapping Not Updated Successfully", showCloseButton: true, width: 380, });
                            }
                            $scope.getTeacherData();
                            // $scope.getsearch_details($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.edt.sims_subject_code, $scope.edt.sims_head_teacher_code);
                        });
                    }

                }
            }

            $scope.DeleteNew = function (head_teacher) {
                head_teacher_code = [];
                var data1 = [];
                var data = [];
                $scope.del = false;

                $scope.head_teacher = head_teacher;


                if ($scope.head_teacher.sims_head_teacher_code != "" && $scope.head_teacher.sims_grade_name != "" && $scope.head_teacher.sims_section_name != "") {
                    data = {
                        sims_cur_code: $scope.head_teacher.sims_cur_code,
                        sims_academic_year: $scope.head_teacher.sims_academic_year,
                        sims_subject_code: $scope.head_teacher.sims_subject_code,
                        sims_head_teacher_code: $scope.head_teacher.sims_head_teacher_code,
                        //sims_head_teacher_code: $scope.filteredTodos[i].sims_head_teacher_code,
                        sims_grade_code: $scope.head_teacher.sims_grade_code,
                        sims_section_code: $scope.head_teacher.sims_section_code,
                        sims_head_teacher_status: $scope.head_teacher.sims_head_teacher_status,
                        opr: 'D'
                    };

                    $scope.del = true;

                    data1.push(data);
                }
                else {
                    swal({ title: "Alert", text: "Head Teacher Is Not Mapped To Any Subject", showCloseButton: true, width: 380, });
                }

                if ($scope.del) {

                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/ERP/HeadTeacherMapping/CUD_Head_Teacher_Mapping", data1).then(function (res) {
                                $scope.Teacher = res.data;
                                if ($scope.Teacher == true) {
                                    swal({ title: "Alert", text: "Head Teacher Mapping Deleted", showCloseButton: true, width: 380, });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record is mapped, Cannot be deleted", showCloseButton: true, width: 380, });
                                }

                                $scope.getTeacherData();
                                // $scope.getsearch_details($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.edt.sims_subject_code, $scope.edt.sims_head_teacher_code);
                            });
                        }
                    });
                }
            }

            $scope.Delete = function () {
                head_teacher_code = [];
                var data1 = [];
                var data = [];
                $scope.del = false;


                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_head_teacher_code;
                    var v = document.getElementById(t + i);

                    if ($scope.filteredTodos[i].ischecked == true) {
                        if ($scope.filteredTodos[i].sims_head_teacher_code != "" && $scope.filteredTodos[i].sims_grade_name != "" && $scope.filteredTodos[i].sims_section_name != "") {
                            if (v.checked == true) {
                                data = {
                                    sims_cur_code: $scope.filteredTodos[i].sims_cur_code,
                                    sims_academic_year: $scope.filteredTodos[i].sims_academic_year,
                                    sims_subject_code: $scope.filteredTodos[i].sims_subject_code,
                                    sims_head_teacher_code: $scope.test_teacher.user_name,
                                    //sims_head_teacher_code: $scope.filteredTodos[i].sims_head_teacher_code,
                                    sims_grade_code: $scope.filteredTodos[i].sims_grade_code,
                                    sims_section_code: $scope.filteredTodos[i].sims_section_code,
                                    sims_head_teacher_status: $scope.filteredTodos[i].sims_head_teacher_status,
                                    opr: 'D'
                                };

                                $scope.del = true;
                            }
                            data1.push(data);

                        }
                        else {
                            swal({ title: "Alert", text: "Head Teacher Is Not Mapped To Any Subject", showCloseButton: true, width: 380, });
                        }
                    }
                }
                if ($scope.del) {
                    $http.post(ENV.apiUrl + "api/ERP/HeadTeacherMapping/CUD_Head_Teacher_Mapping", data1).then(function (res) {
                        $scope.Teacher = res.data;
                        if ($scope.Teacher == true) {
                            swal({ title: "Alert", text: "Head Teacher Mapping Deleted", showCloseButton: true, width: 380, });
                        }
                        else {
                            swal({ title: "Alert", text: "Record is mapped, Cannot be deleted", showCloseButton: true, width: 380, });
                        }
                        $scope.getTeacherData();
                        // $scope.getsearch_details($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.edt.sims_subject_code, $scope.edt.sims_head_teacher_code);
                    });
                }
                else {
                    swal({ title: "Alert", text: "Select Atleast One Record", showCloseButton: true, width: 380, });

                }

            }

            $scope.Cancel = function () {
                $scope.edt = "";
                $scope.page_div = false;
                $scope.head_teacher_mapping = [];
                $scope.filteredTodos = [];
                $scope.currentPage = 0;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.get_ischecked = function (head_teacher) {
                head_teacher.ischecked = true;

            }

            $scope.checkAll = function () {
                head_teacher_code = [];
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].sims_head_teacher_code;
                        var v = document.getElementById(t + i);

                        if ($scope.filteredTodos[i].sims_head_teacher_code != "" && $scope.filteredTodos[i].sims_grade_name != "" && $scope.filteredTodos[i].sims_section_name != "") {
                            v.checked = true;
                            head_teacher_code = head_teacher_code + $scope.filteredTodos[i].sims_head_teacher_code + ',';
                        }
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].sims_head_teacher_code;
                        var v = document.getElementById(t + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        head_teacher_code = [];
                    }
                }
                console.log(head_teacher_code);
            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }


            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.headsearch = function () {
                $scope.todos = $scope.searched($scope.head_teacher_mapping, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.head_teacher_mapping;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_grade_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_section_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                 item.acno == toSearch) ? true : false;
            }






        }])


})();