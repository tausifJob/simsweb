﻿(function () {
    'use strict';

    var main, temp, del = [];

    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('GradeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.sms = true;
            var data1 = [];
            $scope.grades = [];
            var deletecode = [];
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.edit_code = false;
            $scope.edt = {};
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $timeout(function () {
                $("#example_wrapper").scrollbar();
                //$(".scroll-wrapper").css({ 'height': '300px' });
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);


            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };


            $scope.getAcdyr = function (crcode) {
                $http.get(ENV.apiUrl + "api/ERP/Grade/getAcademicYear?curCode=" + crcode).then(function (res) {
                    $scope.acyr = res.data;
                    if (res.data.length > 0) {
                        $scope.edt['grade_academic_year'] = $scope.acyr[0].grade_academic_year;
                    }

                });
            }

            $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum").then(function (res) {
                $scope.curr = res.data;
                if (res.data.length > 0) {
                    $scope.edt['grade_cur_code'] = $scope.curr[0].grade_cur_code;
                    $scope.getAcdyr($scope.curr[0].grade_cur_code);
                }

            });

            $http.get(ENV.apiUrl + "api/ERP/Grade/getGrade").then(function (res) {
                $scope.dis1 = true;
                $scope.dis2 = false;
                $scope.grades = res.data;
                $scope.totalItems = $scope.grades.length;
                $scope.todos = $scope.grades;
                $scope.makeTodos();
                $scope.grid = true;
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edit = function (str) {
                $scope.New();
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edit_code = true;
                $scope.edt =
                    {
                        grade_cur_code: str.grade_cur_code,
                        grade_academic_year: str.grade_academic_year,
                        grade_code: str.grade_code,
                        grade_name_en: str.grade_name_en,
                        grade_name_ar: str.grade_name_ar,
                        grade_name_fr: str.grade_name_fr,
                        grade_name_ot: str.grade_name_ot,
                        sims_grade_status: str.sims_grade_status,
                    }
                $scope.getAcdyr($scope.edt.grade_cur_code);
            }

            $scope.New = function () {
                $scope.edit_code = false;
                $scope.dis1 = false;
                $scope.dis2 = true;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt['grade_name_en'] = '';
                $scope.edt['grade_name_ar'] = '';
                $scope.edt['grade_name_fr'] = '';
                $scope.edt['grade_name_ot'] = '';
                $scope.edt['sims_grade_status'] = false;
            }

            $scope.Save = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = ({
                            grade_cur_code: $scope.edt.grade_cur_code,
                            grade_academic_year: $scope.edt.grade_academic_year,
                            grade_code: $scope.edt.grade_code,
                            grade_name_en: $scope.edt.grade_name_en,
                            grade_name_ar: $scope.edt.grade_name_ar,
                            grade_name_fr: $scope.edt.grade_name_fr,
                            grade_name_ot: $scope.edt.grade_name_ot,
                            sims_grade_status: $scope.edt.sims_grade_status,
                            opr: 'I'
                        });

                        data1.push(data);

                        $http.post(ENV.apiUrl + "api/ERP/Grade/CUDInsertGrade", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            $rootScope.strMessage = $scope.msg1.strMessage;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Grade  Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal({ title: "Alert", text: "Grade  Already Exists", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }

            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/ERP/Grade/getGrade").then(function (res) {
                    $scope.dis1 = true;
                    $scope.dis2 = false;
                    $scope.grades = res.data;
                    $scope.totalItems = $scope.grades.length;
                    $scope.todos = $scope.grades;
                    $scope.makeTodos();
                    $scope.grid = true;
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {
                var data1 = [];

                if (isvalidate) {
                    var data = ({
                        grade_cur_code: $scope.edt.grade_cur_code,
                        grade_academic_year: $scope.edt.grade_academic_year,
                        grade_code: $scope.edt.grade_code,
                        grade_name_en: $scope.edt.grade_name_en,
                        grade_name_ar: $scope.edt.grade_name_ar,
                        grade_name_fr: $scope.edt.grade_name_fr,
                        grade_name_ot: $scope.edt.grade_name_ot,
                        sims_grade_status: $scope.edt.sims_grade_status,
                        opr: 'U'
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/ERP/Grade/CUDGradeUpdate", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Grade  Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal({ title: "Alert", text: " Grade  Not Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                    });
                }
            }

            $scope.Delete = function () {
                var deletecode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i + $scope.filteredTodos[i].grade_code);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'grade_cur_code': $scope.filteredTodos[i].grade_cur_code,
                            'grade_academic_year': $scope.filteredTodos[i].grade_academic_year,
                            'grade_code': $scope.filteredTodos[i].grade_code,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/ERP/Grade/CUDGradeUpdate", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Grade Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });

                                }
                                else {
                                    swal({ title: "Alert", text: "Record Already Mapped.Can't be Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i + $scope.filteredTodos[i].grade_code);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }

                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
            }

            $scope.cancel = function () {
                $scope.dis1 = true;
                $scope.dis2 = false;
                //$scope.edt = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].grade_code);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].grade_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function (info) {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.grades, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.grades;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].grade_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.grade_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.grade_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.grade_academic_year_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                        ) ? true : false;
            }

            var dom;
            $scope.expand = function (info, $event) {
                console.log(info);
                $(dom).remove();
                dom = $("<tr><td class='details' colspan='11'>" +
                    "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                    "<tbody>" +
                    "<tr><td class='semi-bold'>" + "Name (Arabic):" + "</td> " + "<td class='semi-bold'colsapn='2'>" + "Name (French):" +
                    "</td>" + "<td class='semi-bold'colsapn='2'>" + "Name (Regional):" + "</tr>" +
                    "<tr> <td colsapn='2'>" + (info.sims_section_name_ar) + " </td>" + "<td colsapn='2'>" + (info.sims_section_name_fr) + " </td>" + "<td colsapn='2'>" + (info.sims_section_name_ot) + " </td> </tr>" +
                    //"<tr><td class='semi-bold'>" + "Regional:" + "</td> <td colsapn='2'>" + (info.module_name_ot) + " </td>" + "<td class='semi-bold' colsapn='2'>" + "Keywords:" + "<td colsapn='1'>" + (info.module_keywords) + " </td></tr>" +
                    "</tbody>" +
                    " </table></td></tr>")
                $($event.currentTarget).parents("tr").after(dom);


            };

        }]);
})();

