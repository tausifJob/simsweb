﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SupervisorMappingCont',
        ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.display = true;
            $scope.grid = true;
            $scope.page_div = false;
            $scope.itemsPerPage = '10';
            $scope.currentPage = 0;
            var str, cnt;
            $scope.supervisor_mapping = [];
            var main;
            $scope.btn_save = true;
            $scope.btn_delete = false;
            var sims_supervisor_code = [];
            $scope.enroll_number_lists = [];
            $scope.test_emp = [];

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixedtable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.edt = {};
                $scope.obj2 = res.data;
               // $scope.edt['sims_cur_code'] = $scope.obj2[0].sims_attendance_cur_code;
                $scope.GetData();
            });


            $scope.GetData = function (cur) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.edt.sims_cur_code).then(function (res) {
                    $scope.acad = res.data;
                    $scope.edt['sims_cur_code'] = cur;//$scope.obj2[0].sims_attendance_cur_code;
                    $scope.edt['sims_academic_year'] = $scope.acad[0].sims_academic_year;
                });
            }

            $scope.getEmployee = function () {
                $('#Emp_Search_Modal').modal({ backdrop: 'static', keyboard: true });
                $scope.divemp = true;
                $scope.EmployeeTable = false;
            }

            $scope.Search_by_employee = function () {
                $scope.NodataEmployee = false;
                $http.post(ENV.apiUrl + "api/common/GlobalSearch/SearchEmployee?data=" + JSON.stringify($scope.temp)).then(function (SearchEmployee_Data) {
                    $scope.EmployeeDetails = SearchEmployee_Data.data;
                    console.log($scope.EmployeeDetails);
                    $scope.EmployeeTable = true;
                });

            }

            $scope.Empmodal_cancel = function () {
                $scope.glbl_obj = '';
                $scope.global_Search = '';

                for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                    if ($scope.EmployeeDetails[i].em_login_code1 == true) {
                        $scope.enroll_number_lists.push($scope.EmployeeDetails[i]);
                        $scope.test_emp = $scope.EmployeeDetails[i];
                        console.log($scope.enroll_number_lists);
                        $scope.temp =
                         {
                             sims_supervisor_code: $scope.test_emp.empName,
                         }
                    }
                }

                $scope.getsearch_details();
                $scope.EmployeeDetails = [];
            }


            $scope.getsearch_details = function ()//cur_code, acad_yr,supervisor_code
            {
                if ($scope.test_emp.em_login_code != '') {
                    $http.get(ENV.apiUrl + "api/ERP/SupervisorMapping/getSupervisorDetails?supervisor_code=" + $scope.test_emp.em_login_code).then(function (res) {
                        $scope.Supervisor = res.data;
                        if (res.data) {
                            $scope.btn_save = false;
                            $http.get(ENV.apiUrl + "api/ERP/SupervisorMapping/GetAllSims_Supervisor?cur_code=" + $scope.edt.sims_cur_code + "&acad_yr=" + $scope.edt.sims_academic_year).then(function (res) {
                                $scope.supervisor_mapping = res.data;
                                $scope.page_div = true;
                                $scope.totalItems = $scope.supervisor_mapping.length;
                                $scope.todos = $scope.supervisor_mapping;
                                $scope.makeTodos();
                            });
                        }

                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Enter Supervisor Code", showCloseButton: true, width: 380, });
                }


            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                // console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.Save = function (isvalidate) {
                var data1 = [];
                var data = [];
                $scope.insert = false;
                $scope.update = false;
                if (isvalidate) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        debugger;
                        if ($scope.filteredTodos[i].ischecked == true) {
                            if ($scope.filteredTodos[i].sims_supervisor_code == "" && $scope.filteredTodos[i].sims_supervisor_status != false) {
                                var data = {
                                    sims_cur_code: $scope.filteredTodos[i].sims_cur_code,
                                    sims_academic_year: $scope.filteredTodos[i].sims_academic_year,
                                    sims_grade_code: $scope.filteredTodos[i].sims_grade_code,
                                    sims_section_code: $scope.filteredTodos[i].sims_section_code,
                                    sims_supervisor_code: $scope.test_emp.em_login_code,
                                    //sims_supervisor_code: $scope.edt.sims_supervisor_code,
                                    sims_supervisor_status: $scope.filteredTodos[i].sims_supervisor_status,
                                    opr: 'I',
                                };

                                $scope.insert = true;
                                data1.push(data);


                            }
                            else if ($scope.filteredTodos[i].sims_supervisor_code != "") {
                                var data = {
                                    sims_cur_code: $scope.filteredTodos[i].sims_cur_code,
                                    sims_academic_year: $scope.filteredTodos[i].sims_academic_year,
                                    sims_grade_code: $scope.filteredTodos[i].sims_grade_code,
                                    sims_section_code: $scope.filteredTodos[i].sims_section_code,
                                    sims_supervisor_code: $scope.test_emp.em_login_code,
                                    //sims_supervisor_code: $scope.edt.sims_supervisor_code,
                                    sims_supervisor_status: $scope.filteredTodos[i].sims_supervisor_status,
                                    opr: 'U'
                                };

                                $scope.update = true;
                                data1.push(data);
                            }
                        }
                    }

                    if ($scope.insert) {
                        $http.post(ENV.apiUrl + "api/ERP/SupervisorMapping/CUD_Supervisor_Mapping", data1).then(function (res) {
                            $scope.Supervisor = res.data;

                            if ($scope.Supervisor == true) {
                                swal({ title: "Alert", text: "Supervisor Mapped Successfully", showCloseButton: true, width: 380, });
                            }
                            else {
                                swal({ title: "Alert", text: "Supervisor Not Mapped Successfully", showCloseButton: true, width: 380, });
                            }
                            $scope.getsearch_details($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.edt.sims_supervisor_code);
                        });
                    }
                    else {
                        $http.post(ENV.apiUrl + "api/ERP/SupervisorMapping/CUD_Supervisor_Mapping", data1).then(function (res) {
                            $scope.Supervisor = res.data;
                            if ($scope.Supervisor == true) {
                                swal({ title: "Alert", text: "Supervisor Mapping Updated Successfully", showCloseButton: true, width: 380, });
                            }
                            else {
                                swal({ title: "Alert", text: "Supervisor Mapping Not Updated Successfully", showCloseButton: true, width: 380, });
                            }
                            $scope.getsearch_details($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.edt.sims_supervisor_code);
                        });
                    }

                }
            }

            $scope.DeleteNew = function (supervisor_data) {
                sims_supervisor_code = [];
                var data1 = [];
                var data = [];
                $scope.del = false;

                $scope.supervisor = supervisor_data;

                if ($scope.supervisor.sims_supervisor_code != undefined && $scope.supervisor.sims_grade_name != undefined && $scope.supervisor.sims_section_name != undefined) {
                    debugger;

                    data = {
                        sims_cur_code: $scope.supervisor.sims_cur_code,
                        sims_academic_year: $scope.supervisor.sims_academic_year,
                        sims_grade_code: $scope.supervisor.sims_grade_code,
                        sims_section_code: $scope.supervisor.sims_section_code,
                        sims_supervisor_code: $scope.supervisor.sims_supervisor_code,
                        // sims_supervisor_code: $scope.filteredTodos[i].sims_supervisor_code,
                        sims_supervisor_status: $scope.supervisor.sims_supervisor_status,
                        opr: 'D'
                    };

                    $scope.del = true;

                    data1.push(data);

                }
                else {
                    swal({ title: "Alert", text: "Supervisor Is Not Mapped To Any Subject", showCloseButton: true, width: 380, });
                }

                if ($scope.del) {
                    $http.post(ENV.apiUrl + "api/ERP/SupervisorMapping/CUD_Supervisor_Mapping", data1).then(function (res) {
                        $scope.Supervisor = res.data;
                        if ($scope.Supervisor == true) {
                            swal({ title: "Alert", text: "Supervisor Mapping Deleted", showCloseButton: true, width: 380, });
                        }
                        else {
                            swal({ title: "Alert", text: "Record is mapped, Cannot be deleted", showCloseButton: true, width: 380, });
                        }
                        $scope.getsearch_details();
                        //$scope.getsearch_details($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.edt.sims_supervisor_code);
                    });
                }
                else {
                    swal({ title: "Alert", text: "Select Atleast One Record", showCloseButton: true, width: 380, });

                }

            }

            $scope.Delete = function () {
                sims_supervisor_code = [];
                var data1 = [];
                var data = [];
                $scope.del = false;


                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_supervisor_code;
                    var v = document.getElementById(t + i);

                    if ($scope.filteredTodos[i].ischecked == true) {
                        if ($scope.filteredTodos[i].sims_supervisor_code != "" && $scope.filteredTodos[i].sims_grade_name != "" && $scope.filteredTodos[i].sims_section_name != "") {
                            if (v.checked == true) {
                                data = {
                                    sims_cur_code: $scope.filteredTodos[i].sims_cur_code,
                                    sims_academic_year: $scope.filteredTodos[i].sims_academic_year,
                                    sims_grade_code: $scope.filteredTodos[i].sims_grade_code,
                                    sims_section_code: $scope.filteredTodos[i].sims_section_code,
                                    sims_supervisor_code: $scope.test_emp.em_login_code,
                                    // sims_supervisor_code: $scope.filteredTodos[i].sims_supervisor_code,
                                    sims_supervisor_status: $scope.filteredTodos[i].sims_supervisor_status,
                                    opr: 'D'
                                };

                                $scope.del = true;
                            }
                            data1.push(data);

                        }
                        else {
                            swal({ title: "Alert", text: "Supervisor Is Not Mapped To Any Subject", showCloseButton: true, width: 380, });
                        }
                    }
                }
                if ($scope.del) {
                    $http.post(ENV.apiUrl + "api/ERP/SupervisorMapping/CUD_Supervisor_Mapping", data1).then(function (res) {
                        $scope.Supervisor = res.data;
                        if ($scope.Supervisor == true) {
                            swal({ title: "Alert", text: "Head Teacher Mapping Deleted", showCloseButton: true, width: 380, });
                        }
                        else {
                            swal({ title: "Alert", text: "Record is mapped, Cannot be deleted", showCloseButton: true, width: 380, });
                        }
                        $scope.getsearch_details($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.edt.sims_supervisor_code);
                    });
                }
                else {
                    swal({ title: "Alert", text: "Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", });

                }

            }

            $scope.Cancel = function () {
                $scope.temp.sims_supervisor_code = "";
                $scope.page_div = false;
                $scope.edt = "";
                $scope.filteredTodos = [];
                $scope.supervisor_mapping = [];
                $scope.filteredTodos = [];
                $scope.currentPage = 0;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.get_ischecked = function (supervisor) {
                // debugger;
                supervisor.ischecked = true;

            }

            $scope.checkAll = function () {
                sims_supervisor_code = [];
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].sims_supervisor_code;
                        var v = document.getElementById(t + i);

                        if ($scope.filteredTodos[i].sims_supervisor_code != "" && $scope.filteredTodos[i].sims_grade_name != "" && $scope.filteredTodos[i].sims_section_name != "") {
                            v.checked = true;
                            sims_supervisor_code = sims_supervisor_code + $scope.filteredTodos[i].sims_supervisor_code + ',';
                        }
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].sims_supervisor_code;
                        var v = document.getElementById(t + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        sims_supervisor_code = [];
                    }
                }
                console.log(sims_supervisor_code);
            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }


            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.supervisor_mapping, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.supervisor_mapping;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_supervisor_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_grade_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    || item.sims_section_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

        }])


})();