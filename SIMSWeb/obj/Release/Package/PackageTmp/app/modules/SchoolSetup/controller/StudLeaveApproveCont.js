﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StudLeaveApproveCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'];
            $scope.itemsPerPage = '5';
            $scope.currentPage = 0;
            $scope.view = true;
            $scope.create = false;
            $scope.edt = {};
            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            var username = $rootScope.globals.currentUser.username;


            $scope.gradeDetails = [];
            $scope.sectionDetails = [];

            $http.get(ENV.apiUrl + "api/StudLeaveApprove/GetLeaveCode").then(function (GetLeaveCode) {
                $scope.GetLeaveCode = GetLeaveCode.data;

            });

            $http.post(ENV.apiUrl + "api/StudLeaveApprove/ClassDetails?empid=" + $rootScope.globals.currentUser.username).then(function (ClassDetails) {
                $scope.ClassDetails = ClassDetails.data;
                var data = [];

                for (var i = 0; i < $scope.ClassDetails.length; i++) {
                    var data1 = {
                        sims_grade_code: $scope.ClassDetails[i].sims_grade_code,
                        sims_grade_name: $scope.ClassDetails[i].sims_grade_name,
                    }
                    data.push(data1);
                }
                debugger;

                //$scope.gradeDetails = 
                var unique = {};
                var distinct = [];
                for (var i = 0; i < data.length; i++) {
                    if (typeof (unique[data[i].sims_grade_code]) == "undefined") {
                        distinct.push(data[i]);
                    }
                    unique[data[i].sims_grade_code] = 0;
                }
                $scope.gradeDetails = distinct;
            });

            $scope.selectgrade = function (str) {
                debugger;
                for (var i = 0; i < $scope.ClassDetails.length; i++) {
                    if ($scope.ClassDetails[i].sims_grade_code == str) {
                        var data1 = {
                            sims_section_code: $scope.ClassDetails[i].sims_section_code,
                            sims_section_name: $scope.ClassDetails[i].sims_section_name,
                        }
                        $scope.sectionDetails.push(data1);
                    }

                }
            }

            $scope.link = function (str) {
                //$scope.fileUrl = "api.mograsys.com/ppapi/Content/" + $http.defaults.headers.common['schoolId'] + "/Docs/Attachment/" + str;
                console.log($rootScope.globals.currentSchool.lic_website_url + "/images/CircularFiles/" + str);
                window.open("http://api.mograsys.com/ppapi/Content/" + $http.defaults.headers.common['schoolId'] + "/Docs/Attachment/" + str, "_new");
            }


            //var date = new Date();
            //var month = (date.getMonth() + 1);
            //var day = date.getDate();
            //if (month < 10)
            //    month = "0" + month;
            //if (day < 10)
            //    day = "0" + day;
            //$scope.startDate = date.getFullYear() + '-' + (month) + '-' + (day);
            //$scope.endDate = date.getFullYear() + '-' + (month) + '-' + (day);

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.setStart = function (date) {
                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                var date1 = year + "-" + month + "-" + day;
                $scope.startDate = date1;

            }
            $scope.setEnd = function (date) {
                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                var date1 = year + "-" + month + "-" + day;
                $scope.endDate = date1;
            }

            $scope.size = function (pagesize) {
                $scope.itemsPerPage = pagesize;
            }

            $scope.reset_details = function () {
                $scope.edt = {};
                $scope.maindata = [];
                $scope.LeaveDetails = [];
                $scope.rgvtbl = false;
                $scope.sectionDetails = [];
                $scope.startDate = '';
                $scope.endDate = '';
            }



            $scope.show_details = function () {
                if ($scope.edt.sims_grade_code == undefined)
                    $scope.edt.sims_grade_code = '';
                if ($scope.edt.sims_section_code == undefined)
                    $scope.edt.sims_section_code = '';
                if ($scope.edt.startDate == undefined)
                    $scope.edt.startDate = '';
                if ($scope.edt.endDate == undefined)
                    $scope.edt.endDate = '';


                var data = {
                    emp_id: username,
                    sims_leave_code: $scope.edt.sims_leave_code,
                    sims_grade_code: $scope.edt.sims_grade_code,
                    sims_section_code: $scope.edt.sims_section_code,
                    sims_leave_start_date: $scope.startDate,
                    sims_leave_end_date: $scope.endDate,
                }

                $http.post(ENV.apiUrl + "api/StudLeaveApprove/LeaveDetails", data).then(function (LeaveDetails) {
                    $scope.LeaveDetails = LeaveDetails.data;
                    if ($scope.LeaveDetails.length > 0) {
                        $scope.rgvtbl = true;
                    }
                    else {
                        $scope.rgvtbl = false;
                        swal('', 'No any leaves are appled');
                    }


                });
            }

            //Search

            $scope.maindata = [];
            $scope.row_click = function (str) {
                debugger;
                $scope.maindata = str;
                if ($scope.maindata.ord1 == $scope.maindata.ord2) {
                    $scope.super_show_remark = false;
                    $scope.show_remark = true;
                    $scope.super_remark = '';
                    $scope.Remark = 'Supervisor Remark :'
                }
                else {
                    $scope.super_show_remark = true;
                    $scope.show_remark = true;
                    $scope.super_remark = 'Supervisor Remark :';
                    $scope.Remark = 'Principal Remark :'
                }

                //  $scope.stud_details = $scope.maindata.sims_Stud_name + '(' + $scope.maindata.sims_enroll_number + ')';

                $('#myModal').modal('show');

                $scope.maindata

            }

            $scope.leave_approve = function () {
                debugger;
                var data = {
                    sims_leave_number: $scope.maindata.sims_leave_number,
                    sims_enroll_number: $scope.maindata.sims_enroll_number,
                    emp_id: username,
                    sims_leave_status: 'A',
                    sims_leave_remark_new: $scope.maindata.sims_leave_remark_new,
                    sims_leave_remark_old: $scope.maindata.sims_leave_remark_old,
                    sims_leave_start_date: $scope.maindata.sims_leave_start_date,
                    sims_leave_end_date: $scope.maindata.sims_leave_end_date
                }

                $http.post(ENV.apiUrl + "api/StudLeaveApprove/LeaveUpdate", data).then(function (LeaveUpdate) {
                    $scope.LeaveUpdate = LeaveUpdate.data;
                    if ($scope.LeaveUpdate) {
                        swal('', 'Leave Approved successfully.');
                    }
                    $('#myModal').modal('hide');

                    var data1 = {
                        emp_id: username,
                        sims_leave_code: $scope.edt.sims_leave_code,
                        sims_grade_code: $scope.edt.sims_grade_code,
                        sims_section_code: $scope.edt.sims_section_code,
                        sims_leave_start_date: $scope.startDate,
                        sims_leave_end_date: $scope.endDate,
                    }

                    $http.post(ENV.apiUrl + "api/StudLeaveApprove/LeaveDetails", data1).then(function (LeaveDetails) {
                        $scope.LeaveDetails = LeaveDetails.data;
                        if ($scope.LeaveDetails.length > 0) {
                            $scope.rgvtbl = true;
                        }
                        else {
                            $scope.rgvtbl = false;
                        }

                    });
                });
            }

            $scope.leave_reject = function () {
                debugger;
                var data = {
                    sims_leave_number: $scope.maindata.sims_leave_number,
                    sims_enroll_number: $scope.maindata.sims_enroll_number,
                    emp_id: username,
                    sims_leave_status: 'R',
                    sims_leave_remark_new: $scope.maindata.sims_leave_remark_new,
                    sims_leave_remark_old: $scope.maindata.sims_leave_remark_old,
                    sims_leave_start_date: $scope.maindata.sims_leave_start_date,
                    sims_leave_end_date: $scope.maindata.sims_leave_end_date
                }

                $http.post(ENV.apiUrl + "api/StudLeaveApprove/LeaveUpdate", data).then(function (LeaveUpdate) {
                    $scope.LeaveUpdate = LeaveUpdate.data;
                    if ($scope.LeaveUpdate) {
                        swal('', 'Leave Rejected successfully.');
                    }
                    $('#myModal').modal('hide');

                    var data1 = {
                        emp_id: username,
                        sims_leave_code: $scope.edt.sims_leave_code,
                        sims_grade_code: $scope.edt.sims_grade_code,
                        sims_section_code: $scope.edt.sims_section_code,
                        sims_leave_start_date: $scope.startDate,
                        sims_leave_end_date: $scope.endDate,
                    }

                    $http.post(ENV.apiUrl + "api/StudLeaveApprove/LeaveDetails", data1).then(function (LeaveDetails) {
                        $scope.LeaveDetails = LeaveDetails.data;
                        if ($scope.LeaveDetails.length > 0) {
                            $scope.rgvtbl = true;
                        }
                        else {
                            $scope.rgvtbl = false;
                        }

                    });
                });
            }

            $scope.leave_cancel = function () {
                $('#myModal').modal('hide');
            }








        }])
})();