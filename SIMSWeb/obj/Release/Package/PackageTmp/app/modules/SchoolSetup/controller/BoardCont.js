﻿(function () {
    'use strict';
    var obj1, obj2, temp, main;
    var opr = '';
    var Boardcode = [];
    var data1 = [];
    var deletecategorycode = [];
    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('BoardCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.edt = "";
            $scope.BoardDetail = true;
            $scope.editmode = false;
            var formdata = new FormData();

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.check_all();
            }

            $scope.cancel = function () {
                $scope.BoardDetail = true;
                $scope.BoardData1 = false;
                //$scope.edt = "";
                $scope.edt.sims_board_code = "";
                $scope.edt.sims_board_short_name = "";
                $scope.edt.sims_board_name = "";
                $scope.edt.sims_board_contact_no = "";
                $scope.edt.sims_board_address = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.newmode = true;
                //$scope.check = true;
                $scope.edt = "";
                $scope.editmode = false;
                opr = 'S';
                $scope.readonly = false;
                $scope.BoardDetail = false;
                $scope.BoardData1 = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.edt.sims_board_short_name = "";
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                console.log("begin=" + begin); console.log("end=" + end);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.BoardData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.BoardData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.check_all();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                    item.sims_board_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_board_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_board_short_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_board_address.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_board_contact_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_board_contact_no == toSearch) ? true : false;

            }

            $http.get(ENV.apiUrl + "api/BoardDetail/getBoardDetail").then(function (Board_Data) {
                $scope.BoardData = Board_Data.data;
                $scope.totalItems = $scope.BoardData.length;
                $scope.todos = $scope.BoardData;
                $scope.makeTodos();
                console.log($scope.BoardData);
            });

            $http.get(ENV.apiUrl + "api/BoardDetail/getBoardDetail").then(function (res) {
                $scope.BoardData1 = false;
                $scope.BoardDetail = true;
                $scope.obj = res.data;
                console.log($scope.obj);
            });

            $scope.Save = function (myForm) {
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'I';
                    $scope.edt = "";
                    $scope.exist = false;
                    data1.push(data);
                    for (var i = 0; i < $scope.BoardData.length; i++) {
                        if ($scope.BoardData[i].sims_board_code == data.sims_board_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ title: "Alert", text: "Record Already exists", width: 300, height: 200 });
                    }

                    else {
                        $http.post(ENV.apiUrl + "api/BoardDetail/BoardCUD?simsobj=", data1).then(function (msg) {
                            $scope.BoardData1 = false;
                            $http.get(ENV.apiUrl + "api/BoardDetail/getBoardDetail").then(function (Board_Data) {
                                $scope.BoardData = Board_Data.data;
                                $scope.totalItems = $scope.BoardData.length;
                                $scope.todos = $scope.BoardData;
                                $scope.makeTodos();
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                                }
                            });
                        });
                        $scope.BoardDetail = true;
                        $scope.BoardData1 = false;
                    }
                    data1 = [];
                }
            }

            $scope.up = function (str) {
                opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.BoardDetail = false;
                $scope.BoardData1 = true;
                $scope.edt = {
                    sims_board_code: str.sims_board_code
                              , sims_board_short_name: str.sims_board_short_name
                              , sims_board_name: str.sims_board_name
                              , sims_board_contact_no: str.sims_board_contact_no
                              , sims_board_address: str.sims_board_address
                              , sims_board_status: str.sims_board_status


                };

            }

            $scope.Update = function (myForm) {

                if (myForm) {

                    var data = $scope.edt;
                    data.opr = 'U';

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/BoardDetail/BoardCUD?simsobj=", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.BoardData1 = false;
                        $http.get(ENV.apiUrl + "api/BoardDetail/getBoardDetail").then(function (Board_Data) {
                            $scope.BoardData = Board_Data.data;
                            $scope.totalItems = $scope.BoardData.length;
                            $scope.todos = $scope.BoardData;
                            formdata = new FormData();
                            $scope.makeTodos();

                            if ($scope.msg1 == true) {
                                swal({ text: "Record Updated Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ text: "Record Not Updated", width: 300, height: 200 });
                            }
                        });

                    })
                    $scope.BoardData1 = false;
                    $scope.BoardDetail = true;

                }
            }

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.check_once = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }

            $scope.deleterecord = function () {
                debugger;
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_board_code': $scope.filteredTodos[i].sims_board_code,
                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        confirmButtonText: 'Yes',
                        showCancelButton: true,
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/BoardDetail/BoardCUD", deleteleave).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BoardDetail/getBoardDetail").then(function (Board_Data) {
                                                $scope.BoardData = Board_Data.data;
                                                $scope.totalItems = $scope.BoardData.length;
                                                $scope.todos = $scope.BoardData;
                                                $scope.makeTodos();
                                                main.checked = false;
                                            });
                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                        }

                                        $scope.currentPage = true;
                                    });
                                }
                                else {
                                    swal({ text: "Record Already Mapped, can't Delete", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BoardDetail/getBoardDetail").then(function (Board_Data) {
                                                $scope.BoardData = Board_Data.data;
                                                $scope.totalItems = $scope.BoardData.length;
                                                $scope.todos = $scope.BoardData;
                                                $scope.makeTodos();
                                                main.checked = false;
                                                $('tr').removeClass("row_selected");
                                            });
                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                        }
                                    });
                                }
                            });
                        }
                        else {
                            main = document.getElementById('all_chk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
               
                $scope.currentPage = str;
            }


        }]);

})();


