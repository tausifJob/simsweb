﻿
(function () {
    'use strict';
    var sectionlist = [];
    var le = 0;
    var main, strMessage;

    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('ClassTeacherMappingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.pagesize = "10";
            $scope.table = false;
            $scope.busyindicator = false;
            var Employee_code = '';

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2015&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;
            });

            $scope.getteacherdata = function () {


                if ($scope.temp == undefined) {
                    swal('', 'Select Curriculum And Academic Year')
                }
                else if ($scope.temp.sims_cur_code == undefined || $scope.temp.academic_year == undefined) {

                    swal('', 'Select Curriculum And Academic Year')

                }
                else {

                    $scope.busyindicator = true;
                    $scope.table = false;
                    $http.get(ENV.apiUrl + "api/classteachermapping/GetAllTeacher_Name?data=" + JSON.stringify($scope.temp)).then(function (AllTeacher_Name) {

                        $scope.All_Teacher_Name1 = AllTeacher_Name.data;
                        $scope.All_Teacher_Name = angular.copy($scope.All_Teacher_Name1);
                        $scope.totalItems = $scope.All_Teacher_Name.length;
                        $scope.todos = $scope.All_Teacher_Name;
                        $scope.makeTodos();
                        $scope.table = true;

                        $scope.busyindicator = false;

                    });

                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {

                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.CheckAllChecked();
                }
            };

            $scope.SaveTeacherData = function () {

                debugger;
                var Sdata = [{}];
                for (var i = 0; i < $scope.All_Teacher_Name.length; i++) {

                    if ($scope.All_Teacher_Name[i].ischange == true) {
                        var le = Sdata.length;
                        Sdata[le] = {
                            'sims_cur_code': $scope.temp.sims_cur_code,
                            'academic_year': $scope.temp.academic_year,
                            'sims_grade_code': $scope.All_Teacher_Name[i].sims_grade_code,
                            'sims_section_code': $scope.All_Teacher_Name[i].sims_section_code,
                            'sims_teacher_code': $scope.All_Teacher_Name[i].sims_teacher_code,
                            'opr': 'I'
                        }
                    }
                }

                Sdata.splice(0, 1);
                var data = Sdata;

                $scope.busyindicator = true;
                $scope.table = false;
                $http.post(ENV.apiUrl + "api/classteachermapping/InsertUpdateClassTeacher?simsobj=", data).then(function (msg) {


                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {

                        swal({ text: 'Information Updated Successfully', width: 320, showCloseButton: true })
                        $scope.getteacherdata();
                    }

                    else {

                        swal({ text: 'Information Updated', width: 320, showCloseButton: true })

                    }
                    $http.get(ENV.apiUrl + "api/classteachermapping/GetAllTeacher_Name?data=" + JSON.stringify($scope.temp)).then(function (AllTeacher_Name) {

                        $scope.All_Teacher_Name = AllTeacher_Name.data;
                        //console.log($scope.All_Teacher_Name);

                        $scope.totalItems = $scope.All_Teacher_Name.length;
                        $scope.todos = $scope.All_Teacher_Name;
                        $scope.makeTodos();
                        $scope.table = true;

                        $scope.busyindicator = false;

                    });

                });
            }

            $scope.CheckClassTeacherExist = function (str) {

                str.ischange = true;

                var ischeck = str.sims_section_code;

                var data = {
                    'sims_cur_code': $scope.temp.sims_cur_code,
                    'academic_year': $scope.temp.academic_year,
                    'sims_grade_code': str.sims_grade_code,
                    'sims_section_code': str.sims_section_code,
                    'opr': 'C'

                };

                $http.post(ENV.apiUrl + "api/classteachermapping/CheckExist?simsobj=", data).then(function (isexistteacher) {

                    $scope.isexist_teacher = isexistteacher.data;

                    for (var i = 0; i < $scope.isexist_teacher.length; i++) {

                        if ($scope.isexist_teacher[i].isexist == true) {
                            str.sims_section_code = '';
                            strMessage = $scope.isexist_teacher[i].sims_grade_name + '    ' + 'Of' + '    ' + $scope.isexist_teacher[i].sims_section_name + '      ' + 'This Class Already Assigned To' + '    ' + $scope.isexist_teacher[i].sims_teacher_name;
                            swal({ text: strMessage, width: 380, showCloseButton: true });
                            $scope.All_Teacher_Name = angular.copy($scope.All_Teacher_Name1);
                            break;
                        }
                        else if (str.sims_section_code == $scope.isexist_teacher[i].sims_section_code && str.sims_teacher_code == $scope.isexist_teacher[i].sims_teacher_code) {
                            str.sims_section_code = $scope.isexist_teacher[i].sims_section_code;
                        }

                        else {

                            le = sectionlist.length;
                            str.sims_section_code = str.sims_section_code;
                            sectionlist = [{ section: str.sims_section_code, teacher_code: str.sims_teacher_code }];


                            if (le > 0) {
                                $scope.flag = true;
                                for (var n = 0; n < sectionlist.length; n++) {
                                    if (str.sims_section_code != sectionlist[n].section && str.sims_teacher_code == sectionlist[n].teacher_code) {
                                        str.sims_section_code = sectionlist[n].section;
                                    }
                                    else {
                                        $scope.flag = false;
                                        for (var j = 0; j < $scope.All_Teacher_Name.length; j++) {
                                            if (str.sims_section_code == $scope.All_Teacher_Name[j].sims_section_code && str.sims_teacher_code != $scope.All_Teacher_Name[j].sims_teacher_code) {
                                                str.sims_section_code = '';
                                                strMessage = 'This Class Already Assigned To' + '    ' + $scope.All_Teacher_Name[j].sims_teacher_name;
                                                swal({ text: strMessage, width: 380, showCloseButton: true });
                                                for (var s = 0; s < sectionlist.length; s++) {
                                                    if (str.sims_teacher_code == sectionlist[s].teacher_code) {
                                                        var index = sectionlist.indexOf(str.sims_section_code);
                                                        sectionlist.splice(index, 1);
                                                        str.sims_section_code = '';
                                                        break;
                                                    }
                                                }
                                                break;
                                            }

                                        }

                                    }

                                }
                                if ($scope.flag == true) {
                                    str.sims_section_code == str.sims_section_code;
                                    sectionlist = [{ section: str.sims_section_code }];
                                }

                            }


                        }
                    }

                });

            }

            $scope.CheckAllChecked = function () {

                Employee_code = [];
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_teacher_code;
                    var v = document.getElementById(t);
                    if (main.checked == true) {
                        v.checked = true;
                        Employee_code = Employee_code + $scope.filteredTodos[i].sims_teacher_code + ','
                        $scope.row1 = 'row_selected';
                    }
                    else {
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }
            }

            $scope.CheckOnebyOneDelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
            }

            $scope.DeleteTeacherData = function () {

                if ($scope.temp.sims_cur_code == "" || $scope.temp.sims_cur_code == undefined || $scope.temp.academic_year == "" || $scope.temp.academic_year == undefined) {
                    swal({ text: 'Select Curriculum And Academic Year', width: 350, showCloseButton: true, timer: 2000 });
                }

                $scope.currentPage = 1;
                var flag = false;


                var Employee_code_list = [];


                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_teacher_code);

                    if (v.checked == true) {
                        flag = true;
                        $scope.filteredTodos[i].sims_cur_code = $scope.temp.sims_cur_code;
                        $scope.filteredTodos[i].academic_year = $scope.temp.academic_year;
                        $scope.filteredTodos[i].opr = 'D';
                        Employee_code_list.push($scope.filteredTodos[i]);
                    }
                }

                if (flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/classteachermapping/IUDnsertClassTeacher", Employee_code_list).then(function (msg) {

                                $scope.msg1 = msg.data;

                                if ($scope.msg1 == true) {

                                    swal({ text: 'Information Deleted Successfully', width: 350, showCloseButton: true });
                                    $scope.table = true;
                                    $scope.busyindicator = false;
                                    $scope.currentPage = 1;
                                    Employee_code = '';
                                    $scope.row1 = '';
                                    $scope.filteredTodos = '';
                                    $scope.getteacherdata();

                                }

                                else {
                                    $scope.table = true;
                                    $scope.currentPage = 1;
                                    $scope.busyindicator = false;
                                    swal({ text: 'Information Not Deleted', width: 350, showCloseButton: true });
                                }
                            });

                        }
                        else {
                            $scope.filteredTodos = [];
                            $scope.currentPage = 1;
                            $scope.table = true;
                            $scope.busyindicator = false;
                            Employee_code = '';
                            $scope.row1 = '';
                            $scope.makeTodos();
                        }

                    });
                }
                else {
                    swal({ text: 'Select At Least One Recoed To Delete', width: 350, showCloseButton: true });
                }
            }

            $scope.size = function (str) {

                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {



                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.All_Teacher_Name, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.All_Teacher_Name;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_teacher_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_employee_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.Change = function (teacher) {

                teacher.ischange = true;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


        }])
})();