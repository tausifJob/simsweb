﻿(function () {
    'use strict';
    angular.module('sims.module.SchoolSetup', [
        'sims',
        'gettext'
    ]);
})();