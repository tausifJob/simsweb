﻿(function () {
    'use strict';
    var formdata = new FormData();
    var daysd1, daysd2, daysd3 = 0;
    var lic_grace_30, lic_grace_60, lic_grace_90;
    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SchoolLicenceCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $http.get(ENV.apiUrl + "api/SchoolLicense/GetSchoolDetails").then(function (res) {
                $scope.display = true;
                $scope.obj = res.data;
                $scope.ro = true;

                $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/SchoolLogo/' + $scope.obj.lic_school_logo;
                console.log($scope.obj);
                var location = new google.maps.LatLng($scope.obj.lic_lat, $scope.obj.lic_long);
                var mapOptions = {
                    zoom: 8,
                    center: location,
                    mapTypeId: google.maps.MapTypeId.TERRAIN
                }

                $scope.map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

                $scope.markers = [];

                var infoWindow = new google.maps.InfoWindow();

                var marker = new google.maps.Marker({
                    map: $scope.map,
                    position: location,
                    title: $scope.obj.lic_school_name
                });
                marker.content = '<div class="infoWindowContent">' + $scope.obj.lic_school_address + '</div>';

                google.maps.event.addListener(marker, 'click', function () {
                    infoWindow.setContent('<h2>' + marker.title + '</h2>' + marker.content);
                    infoWindow.open($scope.map, marker);
                });
                $scope.markers.push(marker);

                $scope.openInfoWindow = function (e, selectedMarker) {
                    e.preventDefault();
                    google.maps.event.trigger(selectedMarker, 'click');
                }

            });


            $scope.edit = function () {

                $scope.eo = true;
                $scope.ro = false;
                $scope.reg = { sims_Region_name_en: $scope.obj.lic_school_region };
                $scope.obj.lic_school_curriculum = $scope.obj.lic_school_curriculum;
                $scope.getcountyr($scope.obj.lic_school_region);
                // $scope.obj.ex
            }

            $scope.getcountyr = function (str1) {
                $http.get(ENV.apiUrl + "api/SchoolLicense/GetAllCountries?region_code=" + str1).then(function (res1) {
                    $scope.country1 = res1.data;
                })
            }

            $http.get(ENV.apiUrl + "api/SchoolLicense/GetAllRegions").then(function (res) {
                $scope.display = true;
                $scope.region = res.data;
            })

            $http.get(ENV.apiUrl + "api/SchoolLicense/GetAllCurr").then(function (res) {
                $scope.display = true;
                $scope.curri = res.data;
            })

            $http.get(ENV.apiUrl + "api/SchoolLicense/GetExpTime").then(function (res) {
                debugger
                $scope.ExTime = res.data[0];
            })

            $scope.cancel = function () {
                $scope.eo = false;
                $scope.ro = true;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.obj.Subsexpdate = "";
            }

            $scope.Subdate = function (date) {

                var date = new Date(date);
                var month = (date.getMonth() + 1);
                var day = date.getDate();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                var date1 = date.getFullYear() + '-' + month + '-' + day;
                return date1;

            }


            $scope.Subsexpdate = function (date) {
                debugger
                $scope.dateoldd.lic_subscription_expiry_date;

                var month1 = $scope.dateoldd.lic_subscription_expiry_date.split("-")[0];
                var day1 = $scope.dateoldd.lic_subscription_expiry_date.split("-")[1];
                var year1 = $scope.dateoldd.lic_subscription_expiry_date.split("-")[2];
                var new_end_date = year1 + "-" + month1 + "-" + day1;

                var date = new Date(date);
                var month = (date.getMonth() + 1);
                var day = date.getDate();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                var expiry_date = date.getFullYear() + '-' + month + '-' + day;

                //return expiry_date;
                var expiyy = month - month1;
                if (expiyy == 1) {
                    var count = 30;
                    $scope.daysd1 = count;
                    $scope.lic_grace_30 = $scope.daysd1;
                }
                else if (expiyy == 2) {

                    count = 60;
                    $scope.daysd2 = count;
                    $scope.lic_grace_60 = $scope.daysd2;
                }
                else {
                    count = 90;
                    $scope.daysd3 = count;
                    $scope.lic_grace_90 = $scope.daysd3;
                }

                // $scope.daysd = count;

            }

            $scope.agreedate = function (date) {

                var date = new Date(date);
                var month = (date.getMonth() + 1);
                var day = date.getDate();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                var agree_date = date.getFullYear() + '-' + month + '-' + day;

                return agree_date;
            }

            //$scope.Save = function () {

            //    //$scope.obj.lic_grace_30 =

            //    // $scope.obj.lic_grace_60 =

            //    // $scope.obj.lic_grace_90 =

            //    $scope.obj.opr = 'U';

            //    $http.get(ENV.apiUrl + "api/SchoolLicense/GetSchoolDetails", $scope.obj).then(function (msg) {
            //        $scope.msg1 = msg.data;
            //        $scope.obj.opr = 'S';
            //        $http.get(ENV.apiUrl + "api/SchoolLicense/GetSchoolDetails", $scope.obj).then(function (res) {
            //            $scope.obj = res.data;
            //            $scope.eo = false;
            //            $scope.ro = true;
            //            swal({ text: $scope.msg1.strMessage, width: 380 });
            //        });
            //    });
            //}

            $scope.getinfo = function () {
                $http.get(ENV.apiUrl + "api/SchoolLicense/GetSchoolDetails").then(function (res) {
                    $scope.obj = res.data;
                    $scope.eo = false;
                    $scope.ro = true;
                });
            }
            $scope.Save = function () {
                debugger
                $scope.obj['lic_grace_30'] = $scope.daysd1,
                $scope.obj['lic_grace_60'] = $scope.daysd2,
                 $scope.obj['lic_grace_90'] = $scope.daysd3
                var senddata = [];
                var data = $scope.obj
                data.opr = "U";
                senddata.push(data);

                $http.post(ENV.apiUrl + "api/SchoolLicense/UpdateSchoolDetails", senddata).then(function (msg) {
                    debugger;
                    $scope.msg1 = msg.data;

                    $scope.getinfo();

                    if ($scope.daysd1 == 30) {
                        swal({ title: "Alert", text: "School Liecense Updated Sucessfully with 30 days Grace Period", showCloseButton: true, width: 380, })

                    }
                    $rootScope.strMessage = $scope.msg1.strMessage;
                    $('#message').modal('show');
                })
                var location = new google.maps.LatLng($scope.obj.lic_lat, $scope.obj.lic_long);
                var mapOptions = {
                    zoom: 8,
                    center: location,
                    mapTypeId: google.maps.MapTypeId.TERRAIN
                }

                $scope.map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

                $scope.markers = [];

                var infoWindow = new google.maps.InfoWindow();

                var marker = new google.maps.Marker({
                    map: $scope.map,
                    position: location,
                    title: $scope.obj.lic_school_name
                });
                marker.content = '<div class="infoWindowContent">' + $scope.obj.lic_school_address + '</div>';

                google.maps.event.addListener(marker, 'click', function () {
                    infoWindow.setContent('<h2>' + marker.title + '</h2>' + marker.content);
                    infoWindow.open($scope.map, marker);
                });
                $scope.markers.push(marker);

                $scope.openInfoWindow = function (e, selectedMarker) {
                    e.preventDefault();
                    google.maps.event.trigger(selectedMarker, 'click');
                }


            }

            $http.get(ENV.apiUrl + "api/SchoolLicense/GetOldDate1").then(function (dateexists) {
                debugger
                $scope.dateoldd = dateexists.data[0];
                console.log($scope.dateoldd);
            });

            $scope.getTheFiles = function ($files) {
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };

            $scope.file_school_logo = function (element, name) {
                debugger;
                var str = $scope.obj.lic_product_name;
                var photofile = element.files[0];
                var photo_filename = (photofile.type);
                $scope.obj[name] = str + '.' + photo_filename.split("/")[1];
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.school_logo = e.target.result;
                        //$scope.temp.em_img = element.files[0].name;
                        //console.log($scope.temp.em_img);
                    });
                };
                reader.readAsDataURL(photofile);

                var request = {
                    method: 'POST',
                    url: ENV.apiUrl + '/api/file/upload?filename=' + str + '&location=SchoolLogo',
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http(request)
                       .success(function (d) {

                       }, function () {
                           alert("Err");
                       });
            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.getdatee = function () {

            }

            $(document).ready(function () {

                $("#Subsexpdate,#Subsexpdate").datepicker({
                    changeMonth: true,
                    changeYear: true,
                    firstDay: 1,
                    dateFormat: 'dd/mm/yy',
                })

                $("#Subsexpdate").datepicker({ dateFormat: 'dd-mm-yy' });
                $("#Subsexpdate").datepicker({ dateFormat: 'dd-mm-yy' });

                $('#Subsexpdate').change(function () {
                    var start = $('#Subsexpdate').datepicker('getDate');
                    var end = $('#Subsexpdate').datepicker('getDate');

                    if (start < end) {
                        var days = (end - start) / 1000 / 60 / 60;
                        $('#days').val(days);
                    }
                    else {


                        $('#days').val("");
                    }
                }); //end change function
            }); //end ready

        }]);
})();


