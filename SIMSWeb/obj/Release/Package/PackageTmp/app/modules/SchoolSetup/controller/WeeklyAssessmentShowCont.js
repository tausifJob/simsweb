﻿(function () {
    'use strict';


    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('WeeklyAssessmentShowCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
          
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/assessmentreport/getAssessmentmonthdetails?enroll_number=" + 'P1011').then(function (Assessmentmonthdetails) {
                $scope.Assessmentmonthdetails = Assessmentmonthdetails.data;

            });

            $scope.getweek = function () {
                $http.get(ENV.apiUrl + "api/assessmentreport/getAssessmentweekdetails?enroll_number=" + 'P1011' + "&month_name=" + $scope.edt.month_code).then(function (Assessmentweekdetails) {
                    $scope.Assessmentweekdetails = Assessmentweekdetails.data;

                });
            }

            $scope.getSubject = function () {

                $http.get(ENV.apiUrl + "api/assessmentreport/getAssessmentsubjectdetails?enroll_number=" + 'P1011' + "&cat_code=" + $scope.edt.month_code + "&assign_code=" + $scope.edt.Week_code).then(function (Assessmentsubjectdetails) {
                    $scope.Assessmentsubjectdetails = Assessmentsubjectdetails.data;

                });
            }

            $scope.Reset = function () {
                $scope.edt = '';
            }

            $scope.Show = function () {

                $scope.table = false;
                $scope.busy = true;

                var data={};
                data.sims_cur_code             ='01';
                data.sims_academic_year        ='2017';
                data.sims_enroll_number        ='P1011';
                data.sims_gb_cat_code          =$scope.edt.month_code;
                data.sims_gb_cat_assign_number =$scope.edt.Week_code;
                data.sims_gb_number = $scope.edt.subject_code;



                $http.post(ENV.apiUrl + "api/assessmentreport/AssessmentChilddetails", data).then(function (AssessmentChilddetails) {
                    $scope.AssessmentChilddetails = AssessmentChilddetails.data;
                    $scope.busy = false;

                    console.log($scope.AssessmentChilddetails);

                    if ($scope.AssessmentChilddetails.length > 0) {
                        $scope.table = true;
                    }
                    else {

                        swal({ text: 'Data Not Found', width: 300, showCloseButton: true });

                    }
                });
            }

        }])
})();