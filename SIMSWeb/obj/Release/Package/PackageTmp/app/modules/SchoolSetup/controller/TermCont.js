﻿(function () {
    'use strict';
    var del = [];
    var main;
    var date1, date3;
    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TermCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.term_data = [];
            $scope.edit_code = false;
            var data1 = [];
            var deletecode = [];

            $timeout(function () {
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.edt = {};

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.obj2 = res.data;
                $scope.edt['sims_cur_code']=$scope.obj2[0].sims_attendance_cur_code
                $scope.Getinfo($scope.obj2[0].sims_attendance_cur_code);
            });

            $scope.Getinfo = function (curcode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curcode).then(function (res) {
                    $scope.acyr = res.data;
                    //$scope.edt['sims_academic_year'] = $scope.acyr[0].sims_academic_year
                });
            }

            $http.get(ENV.apiUrl + "api/term/getTermByIndex").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.term_data = res.data;
                $scope.totalItems = $scope.term_data.length;
                $scope.todos = $scope.term_data;
                $scope.makeTodos();
                for (var i = 0; i < $scope.totalItems; i++) {
                    $scope.term_data[i].icon = "fa fa-plus-circle";
                }
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };


            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edt =
                    {
                        sims_cur_code: str.sims_cur_code,
                        sims_academic_year: str.sims_academic_year,
                        sims_term_code: str.sims_term_code,
                        sims_term_desc_en: str.sims_term_desc_en,
                        sims_term_desc_ar: str.sims_term_desc_ar,
                        sims_term_desc_fr: str.sims_term_desc_fr,
                        sims_term_desc_ot: str.sims_term_desc_ot,
                        sims_term_status: str.sims_term_status,
                        sims_term_start_date: convertdate(str.sims_term_start_date),
                        sims_term_end_date: convertdate(str.sims_term_end_date),
                    }
                //$scope.edit_code = true;
                $scope.edit_code = false;
                $scope.Getinfo($scope.edt.sims_cur_code);
            }

            function convertdate(dt) {
                if (dt != null) {
                    var d1 = new Date(dt);
                    var month = d1.getMonth() + 1;
                    var day = d1.getDate();
                    if (month < 10)
                        month = "0" + month;
                    if (day < 10)
                        day = "0" + day;
                    var d = d1.getFullYear() + "-" + (month) + "-" + (day);
                    console.log(d);
                    $scope.convertdated = d;
                    return d;
                }
            }

            $scope.createdate = function (date) {
                $scope.edt.sims_term_start_date = convertdate(date);
            }

            $scope.createdate1 = function (date) {
                $scope.edt.sims_term_end_date = convertdate(date);
            }

            $scope.Save = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = ({
                            sims_cur_code: $scope.edt.sims_cur_code,
                            sims_academic_year: $scope.edt.sims_academic_year,
                            sims_term_code: $scope.edt.sims_term_code,
                            sims_term_desc_en: $scope.edt.sims_term_desc_en,
                            sims_term_desc_ar: $scope.edt.sims_term_desc_ar,
                            sims_term_desc_fr: $scope.edt.sims_term_desc_fr,
                            sims_term_desc_ot: $scope.edt.sims_term_desc_ot,
                            sims_term_status: $scope.edt.sims_term_status,
                            sims_term_start_date: $scope.edt.sims_term_start_date,
                            sims_term_end_date: $scope.edt.sims_term_end_date,
                            opr: 'I'
                        });

                        data1.push(data);

                        $http.post(ENV.apiUrl + "api/term/CUDInsertTerm", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Term Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal({ title: "Alert", text: "Term Already Exists", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                        })
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/term/getTermByIndex").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.term_data = res.data;
                    $scope.totalItems = $scope.term_data.length;
                    $scope.todos = $scope.term_data;
                    $scope.makeTodos();
                    for (var i = 0; i < $scope.totalItems; i++) {
                        $scope.term_data[i].icon = "fa fa-plus-circle";
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    var data = ({
                        sims_cur_code: $scope.edt.sims_cur_code,
                        sims_academic_year: $scope.edt.sims_academic_year,
                        sims_term_code: $scope.edt.sims_term_code,
                        sims_term_desc_en: $scope.edt.sims_term_desc_en,
                        sims_term_desc_ar: $scope.edt.sims_term_desc_ar,
                        sims_term_desc_fr: $scope.edt.sims_term_desc_fr,
                        sims_term_desc_ot: $scope.edt.sims_term_desc_ot,
                        sims_term_status: $scope.edt.sims_term_status,
                        sims_term_start_date: $scope.edt.sims_term_start_date,
                        sims_term_end_date: $scope.edt.sims_term_end_date,
                        opr: 'U'
                    });

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/term/CUDTerm", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Term Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal({ title: "Alert", text: "Term Not Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                    })
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
               
                $scope.edit_code = false;
              
                $scope.edt['sims_term_code'] = '';
                $scope.edt['sims_term_desc_en'] = '';
                $scope.edt['sims_term_desc_ar'] = '';
                $scope.edt['sims_term_desc_fr'] = '';
                $scope.edt['sims_term_desc_ot'] = '';
                $scope.edt['sims_term_status'] = '';
                $scope.edt['sims_term_start_date'] = '';
                $scope.edt['sims_term_end_date'] = '';
                $scope.edt['sims_term_status'] = true;
            }

            $scope.Delete = function () {
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    //var v = document.getElementById(i);
                    var v = document.getElementById($scope.filteredTodos[i].sims_academic_year + i);

                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sims_cur_code': $scope.filteredTodos[i].sims_cur_code,
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            'sims_term_code': $scope.filteredTodos[i].sims_term_code,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/term/CUDTerm", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Term  Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });

                                }
                                else {
                                    swal({ title: "Alert", text: "Term Not Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                //var v = document.getElementById(i);
                                var v = document.getElementById($scope.filteredTodos[i].sims_academic_year + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }

                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_academic_year + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_academic_year + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            var dom;
            var count = 0;
            $scope.flag = true;
            $scope.icon = "fa fa-plus-circle";

            $scope.expand = function (info, $event) {
                console.log(info);
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +

                         "<tr> <td class='semi-bold'>" + gettextCatalog.getString('Arabic') + "</td> <td class='semi-bold'>" + gettextCatalog.getString('French') + " </td><td class='semi-bold'>" + gettextCatalog.getString('Regional') + "</td><td class='semi-bold'>" + gettextCatalog.getString('Start Date') + " </td>" +
                        "<td class='semi-bold'>" + gettextCatalog.getString('End Date') +

                          "<tr><td>" + info.sims_term_desc_ar + "</td> <td>" + info.sims_term_desc_fr + " </td><td>" + info.sims_term_desc_ot + "</td><td>" + info.sims_term_start_date + " </td>" +
                        "<td>" + info.sims_term_end_date +

                        " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);

                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    info.icon = "fa fa-plus-circle";
                    $scope.flag = true;
                }

            };

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.term_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.term_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_academic_year + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_term_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_term_desc_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }]
        )
})();