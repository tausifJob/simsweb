﻿(function () {
    'use strict';
    var main;

    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

    var formdata = new FormData();

    simsController.controller('SyllabusCreationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var user = $rootScope.globals.currentUser.username;

            var now = new Date();
            var month = (now.getMonth() + 1);
            var day = now.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            $scope.doc_date = month + '/' + day + '/' + now.getFullYear();


            $scope.busyindicator = true;
            $scope.headcolor = '1';
            $scope.syllabusnames = true;
            $scope.syllabusscreen = true;
            $scope.Grid = true;
            $scope.savebtn = true;
            $scope.pagesize = '5';
            $scope.Expand2 = true;
            $scope.Expand1 = true;
            $scope.busy = false;
            $scope.edt = {};
            $scope.unit = {};
            $scope.topic = {};
            $scope.sub_topic = {};
            $scope.edt["sims_syllabus_status"] = true;
            $scope.unit["sims_syllabus_unit_status"] = true;
            $scope.topic['sims_syllabus_unit_topic_status'] = true;
            $scope.topic['sims_syllabus_unit_sub_topic_status'] = true;

            $scope.edt['sims_syllabus_created_by'] = user;
            $scope.sub_topic['sims_syllabus_unit_sub_topic_created_by'] = user;

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;
                $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.getacyr($scope.curriculum[0].sims_cur_code);
            });

            $scope.getacyr = function (str) {
                $scope.cur_code = str;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    $scope.edt['sims_academic_year'] = $scope.Academic_year[0].sims_academic_year;
                    $scope.getsections(str, $scope.Academic_year[0].sims_academic_year);
                })
            }

            $scope.getsection = function (str, str1, str2) {
                $http.get(ENV.apiUrl + "api/StudentSectionSubject/getsection_name?curcode=" + str + "&academicyear=" + str1 + "&gradecode=" + str2 + "&login_user=" + user).then(function (Allsection) {
                    $scope.section1 = Allsection.data;

                })
            };

            $scope.getsections = function (str, str1) {
                $http.get(ENV.apiUrl + "api/StudentSectionSubject/getgrade_name?curcode=" + str + "&academicyear=" + str1 + "&login_user=" + user).then(function (res) {
                    $scope.grade = res.data;

                });
            };

            $scope.get = function (section) {
                $http.get(ENV.apiUrl + "api/StudentSectionSubject/getSectionsubject_name?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_code + "&login_user=" + user).then(function (getSectionsubject_name) {
                    $scope.cmb_subject_name = getSectionsubject_name.data;
                });
            }

            $scope.Getsyllabus = function () {

                $http.post(ENV.apiUrl + "api/syllabus_lesson/syllabus_lesson", $scope.edt).then(function (res) {
                    if (res.data !== null) {
                        $scope.Get_syllabus_lesson = res.data;
                    }
                    else {
                        swal({ text: 'Sorry Data Not Found' });
                    }

                });
            }

            $scope.SyllabusCreationShow = function (color) {

                $scope.headcolor = color;
                $scope.syllabusscreen = true;
                $scope.Unitscreen = false;
                $scope.Topicscreen = false;
                $scope.SubTopicscreen = false;
                $scope.savebtn = true;
                $scope.syllabusnames = true;
                $scope.ComposeSessonscreen = false;
                $scope.ApproveLessonscreen = false;

            }

            $scope.UnitCreationShow = function (color) {

                $scope.headcolor = color;
                $scope.syllabusscreen = false;
                $scope.Unitscreen = true;
                $scope.Topicscreen = false;
                $scope.SubTopicscreen = false;
                $scope.savebtn = true;
                $scope.syllabusnames = true;
                $scope.ComposeSessonscreen = false;
                $scope.ApproveLessonscreen = false;

            }

            $scope.TopicCreationShow = function (color) {

                $scope.headcolor = color;
                $scope.syllabusscreen = false;
                $scope.Unitscreen = false;
                $scope.Topicscreen = true;
                $scope.SubTopicscreen = false;
                $scope.savebtn = true;
                $scope.syllabusnames = true;
                $scope.ComposeSessonscreen = false;
                $scope.ApproveLessonscreen = false;


            }

            $scope.SubTopicCreationShow = function (color) {

                $scope.headcolor = color;
                $scope.syllabusscreen = false;
                $scope.Unitscreen = false;
                $scope.Topicscreen = false;
                $scope.SubTopicscreen = true;
                $scope.savebtn = true;
                $scope.syllabusnames = true;
                $scope.ComposeSessonscreen = false;

                $scope.ApproveLessonscreen = false;

            }

            $scope.ComposeLessonShow = function (color) {

                $scope.headcolor = color;
                $scope.syllabusscreen = false;
                $scope.Unitscreen = false;
                $scope.Topicscreen = false;
                $scope.SubTopicscreen = false;
                $scope.savebtn = false;
                $scope.syllabusnames = false;
                $scope.ComposeSessonscreen = true;
                $scope.ApproveLessonscreen = false;

            }

            $scope.ApproveLessonShow = function (color) {

                $scope.headcolor = color;
                $scope.syllabusscreen = false;
                $scope.Unitscreen = false;
                $scope.Topicscreen = false;
                $scope.SubTopicscreen = false;
                $scope.savebtn = false;
                $scope.syllabusnames = false;
                $scope.ComposeSessonscreen = false;
                $scope.ApproveLessonscreen = true;
            }

            $scope.Submit_unit_data = function (isvalid) {
                if (isvalid) {

                    $scope.saveunitobject = [];

                    for (var i = 0; i < $scope.Get_syllabus_lesson.length; i++) {
                        if ($scope.Get_syllabus_lesson[i].ischecked == true) {
                            var data = {};
                            data.sims_cur_code = $scope.Get_syllabus_lesson[i].sims_cur_code;
                            data.sims_academic_year = $scope.Get_syllabus_lesson[i].sims_academic_year;
                            data.sims_grade_code = $scope.Get_syllabus_lesson[i].sims_grade_code;
                            data.sims_section_code = $scope.Get_syllabus_lesson[i].sims_section_code;
                            data.sims_subject_code = $scope.Get_syllabus_lesson[i].sims_subject_code;
                            data.sims_syllabus_code = $scope.Get_syllabus_lesson[i].sims_syllabus_code;
                            data.sims_syllabus_unit_code = $scope.unit.sims_syllabus_unit_code;
                            data.sims_syllabus_unit_name = $scope.unit.sims_syllabus_unit_name;
                            data.sims_syllabus_unit_short_desc = $scope.unit.sims_syllabus_unit_short_desc;
                            data.sims_syllabus_unit_creation_date = $scope.unit.sims_syllabus_unit_creation_date;
                            data.sims_syllabus_unit_created_by = $scope.unit.sims_syllabus_unit_created_by;
                            data.sims_syllabus_unit_approval_date = $scope.unit.sims_syllabus_unit_approval_date;
                            data.sims_syllabus_unit_approved_by = $scope.unit.sims_syllabus_unit_approved_by;
                            data.sims_syllabus_unit_approvar_remark = $scope.unit.sims_syllabus_unit_approvar_remark;
                            data.sims_syllabus_unit_status = $scope.unit.sims_syllabus_unit_status;
                            data.opr = 'IU';
                            $scope.saveunitobject.push(data);
                        }
                    }


                    if ($scope.saveunitobject.length > 0) {

                        $http.post(ENV.apiUrl + "api/syllabus_lesson/CUD_syllabus_Units", $scope.saveunitobject).then(function (res) {
                            if (res.data == true) {
                                swal({ text: 'Unit Created Successfully', width: 320, showCloseButton: true });
                                $scope.unit = {};
                                $scope.edt["sims_subject_code"] = '';
                                $scope.Getsyllabus();
                            }
                            else {
                                swal({ text: 'Unit Not Created', width: 320, showCloseButton: true });
                            }

                        });
                    }
                    else {

                        swal({ text: 'Select Atleast One Syllabus', width: 320, showCloseButton: true });
                    }
                }

            }

            $scope.Update_unit_data = function () {


                $scope.unit["opr"] = 'UU';

                $scope.updatedataobject = [];
                $scope.updatedataobject.push($scope.unit);

                $http.post(ENV.apiUrl + "api/syllabus_lesson/CUD_syllabus_Units", $scope.updatedataobject).then(function (res) {
                    if (res.data == true) {
                        swal({ text: 'Unit Updated Successfully', width: 320, showCloseButton: true });
                        $scope.unit = {};
                        $scope.edt["sims_subject_code"] = '';
                        $scope.Getsyllabus();
                    }
                    else {
                        swal({ text: 'Unit Not Updated', width: 320, showCloseButton: true });
                    }

                });

            }

            $scope.Editunit = function (info) {

                $scope.headcolor = '2';
                $scope.syllabusscreen = false;
                $scope.Unitscreen = true;
                $scope.Topicscreen = false;
                $scope.SubTopicscreen = false;

                $scope.savebtn = false;

                $scope.catcolor = info.sims_syllabus_unit_code + info.sims_syllabus_code;
                $scope.unit = angular.copy(info);

                $scope.topic['sims_cur_code'] = info.sims_cur_code;
                $scope.topic['sims_academic_year'] = info.sims_academic_year;
                $scope.topic['sims_grade_code'] = info.sims_grade_code;
                $scope.topic['sims_section_code'] = info.sims_section_code;
                $scope.topic['sims_subject_code'] = info.sims_subject_code;
                $scope.topic['sims_syllabus_code'] = info.sims_syllabus_code;
                $scope.topic['sims_syllabus_unit_code'] = info.sims_syllabus_unit_code;

                $http.post(ENV.apiUrl + "api/syllabus_lesson/syllabus_Topic", $scope.topic).then(function (res) {
                    if (res.data !== null) {
                        $scope.syllabus_Units_Topics = res.data;
                        info.unit_topic = $scope.syllabus_Units_Topics;
                    }
                    else {
                        swal({ text: 'Sorry Data Not Found' });
                    }

                });
            }

            $scope.Submit_topic_data = function (isvalid) {

                if (isvalid) {
                    $scope.TopicDataObject = [];

                    for (var i = 0; i < $scope.Get_syllabus_lesson.length; i++) {
                        if ($scope.Get_syllabus_lesson[i].unit_lst == undefined) {
                            $scope.Get_syllabus_lesson[i].unit_lst = [];
                        }
                        for (var j = 0; j < $scope.Get_syllabus_lesson[i].unit_lst.length; j++) {
                            if ($scope.Get_syllabus_lesson[i].unit_lst[j].ischecked == true) {

                                var data = {};

                                data.sims_cur_code = $scope.Get_syllabus_lesson[i].unit_lst[j].sims_cur_code;
                                data.sims_academic_year = $scope.Get_syllabus_lesson[i].unit_lst[j].sims_academic_year;
                                data.sims_grade_code = $scope.Get_syllabus_lesson[i].unit_lst[j].sims_grade_code;
                                data.sims_section_code = $scope.Get_syllabus_lesson[i].unit_lst[j].sims_section_code;
                                data.sims_subject_code = $scope.Get_syllabus_lesson[i].unit_lst[j].sims_subject_code;
                                data.sims_syllabus_code = $scope.Get_syllabus_lesson[i].unit_lst[j].sims_syllabus_code;
                                data.sims_syllabus_unit_code = $scope.Get_syllabus_lesson[i].unit_lst[j].sims_syllabus_unit_code;
                                data.sims_syllabus_unit_topic_name = $scope.topic.sims_syllabus_unit_topic_name;
                                data.sims_syllabus_unit_topic_short_desc = $scope.topic.sims_syllabus_unit_topic_short_desc;
                                data.sims_syllabus_unit_topic_creation_date = $scope.topic.sims_syllabus_unit_topic_creation_date;
                                data.sims_syllabus_unit_topic_created_by = $scope.topic.sims_syllabus_unit_topic_created_by;
                                data.sims_syllabus_unit_topic_status = $scope.topic.sims_syllabus_unit_topic_status
                                data.opr = 'TI';
                                $scope.TopicDataObject.push(data);

                            }

                        }
                    }
                    if ($scope.TopicDataObject.length > 0) {

                        $http.post(ENV.apiUrl + "api/syllabus_lesson/CUD_syllabus_Topics", $scope.TopicDataObject).then(function (res) {
                            if (res.data == true) {
                                swal({ text: 'Topic Created Successfully', width: 320, showCloseButton: true });
                                $scope.unit = {};
                                $scope.topic = {};
                                $scope.edt["sims_subject_code"] = '';
                                $scope.Getsyllabus();
                            }
                            else {
                                swal({ text: 'Topic Not Created', width: 320, showCloseButton: true });
                            }

                        });

                    } else {

                        swal({ text: 'Select Atleast One Syllabus Unit', width: 320, showCloseButton: true });
                    }

                }
            }

            $scope.Update_topic_data = function () {

                $scope.topic['opr'] = 'TU';

                $scope.topicUpdateObject = [];

                $scope.topicUpdateObject.push($scope.topic);
                $http.post(ENV.apiUrl + "api/syllabus_lesson/CUD_syllabus_Topics", $scope.topicUpdateObject).then(function (res) {
                    if (res.data == true) {
                        swal({ text: 'Topic Updated Successfully', width: 320, showCloseButton: true });
                        $scope.unit = {};
                        $scope.topic = {};
                        $scope.edt["sims_subject_code"] = '';
                        $scope.Getsyllabus();
                    }
                    else {
                        swal({ text: 'Topic Not Updated', width: 320, showCloseButton: true });
                    }

                });

            }

            $scope.btn_topic_cancel_Click = function () {

                $scope.topic['sims_syllabus_unit_topic_name'] = '';
                $scope.topic['sims_syllabus_unit_topic_short_desc'] = '';
                $scope.topic['sims_syllabus_unit_topic_creation_date'] = '';
                $scope.topic['sims_syllabus_unit_topic_created_by'] = '';

                $scope.savebtn = true;

            }

            $scope.Edittopic = function (info) {

                $scope.headcolor = '3';

                $scope.assignmentcolor = info.sims_syllabus_unit_code + info.sims_syllabus_code + info.sims_syllabus_unit_topic_code;
                $scope.topic = angular.copy(info);

                $scope.syllabusscreen = false;
                $scope.Unitscreen = false;
                $scope.Topicscreen = true;
                $scope.SubTopicscreen = false;
                $scope.savebtn = false;

                $scope.sub_topic['sims_cur_code'] = info.sims_cur_code;
                $scope.sub_topic['sims_academic_year'] = info.sims_academic_year;
                $scope.sub_topic['sims_grade_code'] = info.sims_grade_code;
                $scope.sub_topic['sims_section_code'] = info.sims_section_code;
                $scope.sub_topic['sims_subject_code'] = info.sims_subject_code;
                $scope.sub_topic['sims_syllabus_code'] = info.sims_syllabus_code;
                $scope.sub_topic['sims_syllabus_unit_code'] = info.sims_syllabus_unit_code;
                $scope.sub_topic['sims_syllabus_unit_topic_code'] = info.sims_syllabus_unit_topic_code;
                $scope.sub_topic['sims_syllabus_unit_sub_topic_code'] = info.sims_syllabus_unit_sub_topic_code;

                $http.post(ENV.apiUrl + "api/syllabus_lesson/syllabus_Topic_Sub_Topics", $scope.sub_topic).then(function (res) {
                    if (res.data !== null) {
                        $scope.syllabus_Units_Sub_Topics = res.data;
                        info.unit_sub_topic = $scope.syllabus_Units_Sub_Topics;
                    }
                    else {
                        swal({ text: 'Sorry Data Not Found' });
                    }

                });

            }

            $scope.Submit_Sub_topic_data = function (isvalid) {

                if (isvalid) {

                    var data = {};

                    data.sims_cur_code = $scope.sub_topic.sims_cur_code;
                    data.sims_academic_year = $scope.sub_topic.sims_academic_year;
                    data.sims_grade_code = $scope.sub_topic.sims_grade_code;
                    data.sims_section_code = $scope.sub_topic.sims_section_code;
                    data.sims_subject_code = $scope.sub_topic.sims_subject_code;
                    data.sims_syllabus_code = $scope.sub_topic.sims_syllabus_code;
                    data.sims_syllabus_unit_code = $scope.sub_topic.sims_syllabus_unit_code;
                    data.sims_syllabus_unit_topic_code = $scope.sub_topic.sims_syllabus_unit_topic_code;
                    data.sims_syllabus_unit_sub_topic_code = $scope.sub_topic.sims_syllabus_unit_sub_topic_code;
                    data.sims_syllabus_unit_sub_topic_name = $scope.sub_topic.sims_syllabus_unit_sub_topic_name;
                    data.sims_syllabus_unit_sub_topic_short_desc = $scope.sub_topic.sims_syllabus_unit_sub_topic_short_desc;
                    data.sims_syllabus_unit_sub_topic_creation_date = $scope.sub_topic.sims_syllabus_unit_sub_topic_creation_date;
                    data.sims_syllabus_unit_sub_topic_created_by = $scope.sub_topic.sims_syllabus_unit_sub_topic_created_by;
                    data.sims_syllabus_unit_sub_topic_status = $scope.sub_topic.sims_syllabus_unit_sub_topic_status;
                    data.opr = 'IS'

                    $http.post(ENV.apiUrl + "api/syllabus_lesson/CUD_syllabus_Sub_Topics", data).then(function (res) {
                        if (res.data == true) {
                            swal({ text: 'Sub Topic Created Successfully', width: 320, showCloseButton: true });
                            $scope.unit = {};
                            $scope.sub_topic = {};
                            $scope.topic = {};
                            $scope.edt["sims_subject_code"] = '';
                            $scope.Getsyllabus();
                        }
                        else {
                            swal({ text: 'Sub Topic Not Created', width: 320, showCloseButton: true });
                        }

                    });

                }
            }

            $scope.Update_Sub_topic_data = function () {

                $scope.sub_topic['opr'] = 'US';

                $http.post(ENV.apiUrl + "api/syllabus_lesson/CUD_syllabus_Sub_Topics", $scope.sub_topic).then(function (res) {
                    if (res.data == true) {
                        swal({ text: 'Sub Topic Updated Successfully', width: 320, showCloseButton: true });
                        $scope.unit = {};
                        $scope.sub_topic = {};
                        $scope.topic = {};
                        $scope.edt["sims_subject_code"] = '';
                        $scope.Getsyllabus();
                    }
                    else {
                        swal({ text: 'Sub Topic Not Updated', width: 320, showCloseButton: true });
                    }

                });

            }

            $scope.EditSubtopic = function (info) {

                $scope.headcolor = '4';
                $scope.syllabusscreen = false;
                $scope.Unitscreen = false;
                $scope.Topicscreen = false;
                $scope.SubTopicscreen = true;

                $scope.savebtn = false;
                $scope.sub_topic = angular.copy(info);
            }

            $scope.btn_cancel_Click = function () {

                $scope.edt["sims_grade_code"] = '';
                $scope.edt["sims_section_code"] = '';
                $scope.edt["sims_subject_code"] = '';
                $scope.edt["sims_syllabus_code"] = '';
                $scope.edt["sims_syllabus_description"] = '';
                $scope.edt["sims_syllabus_created_by"] = '';
                $scope.edt["sims_syllabus_remark"] = '';
                $scope.edt["sims_syllabus_start_date"] = '';
                $scope.edt["sims_syllabus_end_date"] = '';
                $scope.edt["sims_syllabus_creation_date"] = '';


                $scope.savebtn = true;
                $scope.SyllabusDetails.$setUntouched();
                $scope.SyllabusDetails.$setPristine();

            }

            $scope.btn_unit_cancel_Click = function () {

                $scope.unit['sims_syllabus_unit_name'] = '';
                $scope.unit['sims_syllabus_unit_short_desc'] = '';
                $scope.unit['sims_syllabus_unit_created_by'] = '';
                $scope.unit['sims_syllabus_unit_approvar_remark'] = '';
                $scope.unit['sims_syllabus_unit_creation_date'] = '';
                $scope.unit['sims_syllabus_unit_approval_date'] = '';
                $scope.unit['sims_syllabus_unit_approved_by'] = '';
                $scope.unit['sims_syllabus_unit_code'] = '';
                $scope.savebtn = true;


                $scope.unitDetails.$setUntouched();
                $scope.unitDetails.$setPristine();
            }

            $scope.btn_unit_sub_top_cancel_Click = function () {
                //$scope.sub_topic['sims_cur_code']= '';
                //$scope.sub_topic['sims_academic_year']= '';
                //$scope.sub_topic['sims_grade_code']= '';
                //$scope.sub_topic['sims_section_code']= '';
                //$scope.sub_topic['sims_subject_code']= '';
                //$scope.sub_topic['sims_syllabus_code']= '';
                //$scope.sub_topic['sims_syllabus_unit_code']= '';
                //$scope.sub_topic['sims_syllabus_unit_topic_code']= '';
                //$scope.sub_topic['sims_syllabus_unit_sub_topic_code']= '';

                $scope.sub_topic['sims_syllabus_unit_sub_topic_name'] = '';
                $scope.sub_topic['sims_syllabus_unit_sub_topic_short_desc'] = '';
                $scope.sub_topic['sims_syllabus_unit_sub_topic_creation_date'] = '';
                $scope.sub_topic['sims_syllabus_unit_sub_topic_created_by'] = '';

                $scope.savebtn = true;

            }

            $scope.CommaSeparetdTopic = function (info) {

                $scope.topic_object = [];

                info = info.split(',');
                for (var i = 0; i < info.length; i++) {
                    var data = {};
                    data.sims_syllabus_unit_topic_name = info[i];
                    $scope.topic_object.push(data);

                }
            }

            $scope.Getsyllabus_topic_subtopic = function () {

                $scope.lesson_plan = [];
                $scope.busy = true;

                $http.post(ENV.apiUrl + "api/syllabus_lesson/syllabus_Topic_Sub_Topics_lesson_compose", $scope.edt).then(function (res) {
                    if (res.data.length > 0) {
                        $scope.lesson_plan = res.data;
                        $scope.busy = false;

                    }
                    else {
                        swal({ text: 'Data Not Found', width: 320, showCloseButton: true });
                        $scope.busy = false;

                    }
                });
            }

            $scope.Cancel_topic_subtopic = function () {

                $scope.edt['sims_grade_code'] = '';
                $scope.edt['sims_section_code'] = '';
                $scope.edt['sims_subject_code'] = '';
                $scope.edt['sims_syllabus_start_date'] = '';
                $scope.edt['sims_syllabus_end_date'] = '';

            }

            $scope.getTheFiles = function ($files) {

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };

            $scope.file_changed = function (element) {

                var str = '';

                var photofile = element.files[0];
                var photo_filename = (photofile.type);
                $scope.lesson_plan[0]['sims_syllabus_lesson_plan_doc_path'] = photofile.name;

                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        //$scope.mother_photo = e.target.result;
                    });
                };
                reader.readAsDataURL(photofile);

                var request = {
                    method: 'POST',
                    url: ENV.apiUrl + '/api/file/upload?filename=' + photofile.name.split('.')[0] + '&location=Syllabus',
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http(request)
                       .success(function (d) {

                       }, function () {
                           alert("Err");
                       });
            };

            $scope.Submit_compose_lesson_plan = function (info) {

                info['sims_syllabus_lesson_plan_doc_status'] = 'A';
                info['sims_syllabus_lesson_plan_doc_upload_date'] = $scope.doc_date;
                info['sims_syllabus_lesson_plan_doc_uploaded_by'] = user;
                info['sims_syllabus_lesson_plan_creation_date'] = $scope.doc_date;
                info['sims_syllabus_lesson_plan_created_by'] = user;
                info['sims_syllabus_lesson_plan_status'] = true;

                $http.post(ENV.apiUrl + "api/syllabus_lesson/CUDsyllabus_Topic_Sub_Topics_lesson_compose", info).then(function (res) {
                    if (res.data == true) {
                        info = {};
                        swal({ text: 'Lesson Plan Composed Successfully', width: 320, showCloseButton: true });
                    }
                    else {
                        swal({ text: 'Lesson Plan Not Composed', width: 320, showCloseButton: true });
                    }
                });

            }

            $scope.Getsyllabus_Approve_data = function () {

                $scope.Approve_plan = [];
                $scope.busy = true;

                $http.post(ENV.apiUrl + "api/syllabus_lesson/syllabus_Topic_Sub_Topics_lesson_Approve", $scope.edt).then(function (res) {
                    if (res.data.length > 0) {
                        $scope.Approve_plan = res.data;
                        $scope.busy = false;
                    } else {
                        swal({ text: 'Data Not Found', width: 320, showCloseButton: true });
                        $scope.busy = false;


                    }
                });
            }

            $scope.Cancel_Approve_data = function () {

                $scope.edt['sims_grade_code'] = '';
                $scope.edt['sims_section_code'] = '';
                $scope.edt['sims_subject_code'] = '';
                $scope.edt['sims_syllabus_start_date'] = '';
                $scope.edt['sims_syllabus_end_date'] = '';

            }

            $scope.Reset_All_Data = function () {

                $scope.Approve_plan = [];
            }

            $scope.Approved_lesson_plan_data = function (info) {

                info['sims_syllabus_lesson_plan_approval_date'] = $scope.doc_date;
                info['sims_syllabus_lesson_plan_approved_by'] = user;
                info['sims_syllabus_lesson_plan_status'] = 'P';

                $http.post(ENV.apiUrl + "api/syllabus_lesson/CUD_syllabus_Topic_Sub_Topics_lesson_Approve", info).then(function (res) {
                    if (res.data == true) {
                        info = {};
                        swal({ text: 'Lesson Plan Approved Successfully', width: 320, showCloseButton: true });
                    }
                    else {
                        swal({ text: 'Lesson Plan Not Approved', width: 320, showCloseButton: true });
                    }
                });
                
            }

            $scope.Reject_lesson_plan_data = function (info) {


                info['sims_syllabus_lesson_plan_approval_date'] = $scope.doc_date;
                info['sims_syllabus_lesson_plan_approved_by'] = user;
                info['sims_syllabus_lesson_plan_status'] = 'R';

                $http.post(ENV.apiUrl + "api/syllabus_lesson/CUD_syllabus_Topic_Sub_Topics_lesson_Approve", info).then(function (res) {
                    if (res.data == true) {
                        info = {};
                        swal({ text: 'Lesson Plan Rejected Successfully', width: 320, showCloseButton: true });
                    }
                    else {
                        swal({ text: 'Lesson Plan Not Rejected', width: 320, showCloseButton: true });
                    }
                });

            }


            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = angular.copy($scope.todos.slice(begin, end));

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
            };

            $scope.New = function () {

                main = document.getElementById('mainchk');
                main.checked = false;

                $scope.edt = {};
                $scope.savebtn = true;
                $scope.Grid = false;
                $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.getacyr($scope.curriculum[0].sims_cur_code);

                $scope.Check_Multiple_Record();
            }

            $scope.Edit = function (info) {
                $scope.headcolor = '1';
                $scope.syllabusscreen = true;
                $scope.Unitscreen = false;
                $scope.Topicscreen = false;
                $scope.SubTopicscreen = false;

                $scope.edt = angular.copy(info);
                $scope.savebtn = false;
                $scope.Expand = info.sims_syllabus_code;
                $scope.cur_gb_name = info.sims_syllabus_code;
                $scope.unit['sims_cur_code'] = info.sims_cur_code;
                $scope.unit['sims_academic_year'] = info.sims_academic_year;
                $scope.unit['sims_grade_code'] = info.sims_grade_code;
                $scope.unit['sims_section_code'] = info.sims_section_code;
                $scope.unit['sims_subject_code'] = info.sims_subject_code;
                $scope.unit['sims_syllabus_code'] = info.sims_syllabus_code;


                $http.post(ENV.apiUrl + "api/syllabus_lesson/syllabus_Unit", $scope.unit).then(function (res) {
                    if (res.data !== null) {
                        $scope.syllabus_Units = res.data;
                        info.unit_lst = $scope.syllabus_Units;
                    }
                    else {
                        swal({ text: 'Sorry Data Not Found' });
                    }

                });



            }

            $scope.Check_Single_Record = function (enroll, subject) {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }

            }

            $scope.Check_Multiple_Record = function () {
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if (main.checked == true) {
                        $scope.filteredTodos[i].sims_status = true;
                        $('tr').addClass("row_selected");
                    }
                    else {
                        $scope.filteredTodos[i].sims_status = false;
                        $('tr').removeClass("row_selected");

                    }
                }
            }

            $scope.Submit = function (isvalid) {

                if (isvalid) {
                    var data = {};

                    data.sims_cur_code = $scope.edt.sims_cur_code;
                    data.sims_academic_year = $scope.edt.sims_academic_year;
                    data.sims_grade_code = $scope.edt.sims_grade_code;
                    data.sims_section_code = $scope.edt.sims_section_code;
                    data.sims_subject_code = $scope.edt.sims_subject_code;
                    data.sims_syllabus_code = $scope.edt.sims_syllabus_code;
                    data.sims_syllabus_description = $scope.edt.sims_syllabus_description;
                    data.sims_syllabus_start_date = $scope.edt.sims_syllabus_start_date;
                    data.sims_syllabus_end_date = $scope.edt.sims_syllabus_end_date;
                    data.sims_syllabus_remark = $scope.edt.sims_syllabus_remark;
                    data.sims_syllabus_creation_date = $scope.edt.sims_syllabus_creation_date;
                    data.sims_syllabus_created_by = $scope.edt.sims_syllabus_created_by;
                    data.sims_syllabus_status = $scope.edt.sims_syllabus_status;
                    data.opr = 'I';


                    $http.post(ENV.apiUrl + "api/syllabus_lesson/CUDsyllabuslesson", data).then(function (res) {
                        if (res.data == true) {

                            swal({ text: 'Syllabus Created Successfully', width: 320, showCloseButton: true });

                            $http.get(ENV.apiUrl + "api/syllabus_lesson/Get_syllabus_lesson").then(function (res) {
                                if (res.data !== null) {
                                    $scope.Grid = true;
                                    $scope.obj = res.data;
                                    $scope.totalItems = $scope.obj.length;
                                    $scope.todos = $scope.obj;
                                    $scope.makeTodos();
                                }
                                else {
                                    swal({ text: 'Sorry Data Not Found', width: 320, showCloseButton: true });
                                }

                            });

                        }
                        else {
                            swal({ text: 'Syllabus Not Created', width: 320, showCloseButton: true });
                        }

                    });
                }
            }

            $scope.Update = function () {

                $scope.edt['opr'] = 'U';
                $http.post(ENV.apiUrl + "api/syllabus_lesson/CUDsyllabuslesson", $scope.edt).then(function (res) {
                    if (res.data == true) {
                        $scope.edt["sims_subject_code"] = '';
                        $scope.edt["sims_syllabus_description"] = '';
                        $scope.edt["sims_syllabus_created_by"] = '';
                        $scope.edt["sims_syllabus_remark"] = '';
                        $scope.edt["sims_syllabus_start_date"] = '';
                        $scope.edt["sims_syllabus_end_date"] = '';
                        $scope.edt["sims_syllabus_creation_date"] = '';

                        swal({ text: 'Syllabus Updated Successfully', width: 320, showCloseButton: true });
                        $scope.Getsyllabus();
                    }
                    else {
                        swal({ text: 'Syllabus Not Updated', width: 320, showCloseButton: true });
                    }
                });
            }

            $scope.DeleteRecord = function () {

                $scope.deleteobject = [];

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].sims_status == true) {
                        $scope.deleteobject.push($scope.filteredTodos[i]);
                    }
                }

                if ($scope.deleteobject.length > 0) {


                    swal({
                        text: "Are you sure? you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/syllabus_lesson/CUDeletesyllabuslesson", $scope.deleteobject).then(function (res) {
                                if (res.data == true) {
                                    swal({ text: 'Record Deleted Successfully', width: 320, showCloseButton: true });

                                    $http.get(ENV.apiUrl + "api/syllabus_lesson/Get_syllabus_lesson").then(function (res) {
                                        if (res.data !== null) {
                                            $scope.Grid = true;
                                            $scope.obj = res.data;
                                            $scope.totalItems = $scope.obj.length;
                                            $scope.todos = $scope.obj;
                                            $scope.makeTodos();
                                        }
                                        else {
                                            swal({ text: 'Sorry Data Not Found', width: 320, showCloseButton: true });
                                        }

                                    });

                                }
                                else {
                                    swal({ text: 'Record Not Deleted', width: 320, showCloseButton: true });
                                }
                            });
                        } else {

                            main.checked = false;
                            $scope.Check_Multiple_Record();
                        }
                    });
                }
                else {
                    swal({ text: 'Select Atleast One Record To Delete', width: 320, showCloseButton: true });
                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            var dom;
            $scope.flag = true;

            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                "<table class='inner-table1' cellpadding='5' cellspacing='0'  style='width:100%;border:solid;border-width:02px'>" +
                "<tbody>" +
                "<tr> <td class='semi-bold'>" + "Created By" + "</td> <td class='semi-bold'>" + "Remark" + " </td><td class='semi-bold'>" + "Syllabus Start Date" + "</td><td class='semi-bold'>" + "Syllabus End Date" + "</td></tr>" +
                "<tr> <td>" + (info.sims_syllabus_created_by) + "</td> <td>" + (info.sims_syllabus_remark) + " </td><td>" + (info.sims_syllabus_start_date) + "</td><td>" + (info.sims_syllabus_end_date) + "</td></tr>" +

                " </table></td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    //$(dom).remove();
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }
            }

        }]);

})();



