﻿(function () {
    'use strict';
    var obj1, temp, opr, comp, edt;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Health');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StudentHealthCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.search_btn = true;
            $scope.clr_btn = true;
            $scope.Update_btn = false;
            $scope.newdisplay = false;
            $scope.table = true;
            var cur;
            var year;
            var grade;
            var section;
            $scope.hide_img = false;
            debugger;
            $scope.imgurl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/';
            //$scope.imgurl = "http://api.mograsys.com/APIERP/Content/adisw" + '/Images/StudentImage/';

            $http.get(ENV.apiUrl + "api/StudentHealth/getBloodGroup").then(function (res) {

                $scope.empstat = true;
                $scope.bloodGroup = res.data;
                console.log($scope.bloodGroup);
                $scope.operation = true;
                $scope.Save_btn = true;
                $scope.Update_btn = false;
                $scope.display = true;

            });

            $scope.calculateBMI = function (kg, htc) {
                debugger;
                var m = 0, bmi = 0, diff = 0, f_bmi = 0, h2 = 0;
                m = htc / 100;
                h2 = m * m;

                bmi = kg / h2;

                f_bmi = Math.floor(bmi);

                diff = bmi - f_bmi;
                diff = diff * 10;

                diff = Math.round(diff);
                if (diff == 10) {
                    // Need to bump up the whole thing instead
                    f_bmi += 1;
                    diff = 0;
                }
                bmi = f_bmi + "." + diff;
                $scope.temp.sims_health_bmi = bmi;
            }

            $http.get(ENV.apiUrl + "api/StudentHealth/getAllStudentHealth").then(function (res1) {
                $scope.studHealth = res1.data;
                $scope.totalItems = $scope.studHealth.length;
                $scope.todos = $scope.studHealth;
                $scope.makeTodos();
                console.log($scope.studHealth);
            });

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                $scope.getAccYear($scope.edt.sims_cur_code);
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                    $scope.getGrade($scope.edt.sims_cur_code, $scope.temp.sims_academic_year);
                });
            }

            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    console.log($scope.Grade_code);
                });
            }

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    console.log($scope.Section_code);
                });
            }

            $scope.stud_Details_fun = function (curcode, acYear, gCode, sCode) {
                cur = $scope.edt.sims_cur_code;
                year = $scope.temp.sims_academic_year;
                grade = $scope.temp.sims_grade_code;
                section = $scope.temp.sims_section_code;
                $http.get(ENV.apiUrl + "api/StudentHealth/getStudHealthSearch?sims_cur_code=" + curcode + "&sims_academic_year=" + acYear + "&sims_grade_code=" + gCode + "&sims_section_code=" + sCode).then(function (StudHealthDetails) {
                    $scope.Stud_Health_Details = StudHealthDetails.data;
                    $scope.totalItems = $scope.Stud_Health_Details.length;
                    $scope.todos = $scope.Stud_Health_Details;
                    $scope.makeTodos();
                });
            }

            $scope.SearchStudentWindow = function () {

                $scope.searchtable = false;
                $scope.student = '';
                $('#MyModal').modal('show');
            }

            $scope.SearchSudent = function () {
                debugger;
                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    main.checked = false;
                }
                $scope.searchtable = false;
                $scope.busy = true;
                $scope.sibling_result;
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.temp)).then(function (Allstudent) {

                    $scope.student = Allstudent.data;
                    //$scope.StudEnrollment = student[0].s_enroll_no;
                    $scope.searchtable = true;
                    $scope.busy = false;
                });
            }

            $scope.DataEnroll = function () {
                for (var i = 0; i < $scope.student.length; i++) {
                    var t = $scope.student[i].s_enroll_no;
                    var v = document.getElementById(t);
                    if (v.checked == true) {
                        $scope.temp = {
                            enroll_number: $scope.student[i].s_enroll_no
                        };
                        $http.get(ENV.apiUrl + "api/StudentHealth/getGenderDOB?S_number=" + $scope.temp.enroll_number).then(function (getStudData) {
                            $scope.get_studData = getStudData.data;
                            $('#style_new').html($scope.get_studData[0].sub_class);
                            $scope.hide_img = true;
                        });
                    }
                }
            }

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=0&routeCode=0").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;
                console.log($scope.ComboBoxValues);
            });

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                console.log($scope.curriculum);

            });

            $scope.getacyr = function (str) {
                console.log(str);
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    console.log($scope.Academic_year);
                })
            }

            $http.get(ENV.apiUrl + "api/common/getAllAcademicYear").then(function (get_AllAcademicYear) {
                $scope.ActiveYear = get_AllAcademicYear.data;
            });

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    $scope.filteredTodos[i].icon = 'fa fa-plus-circle';
                }


            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                debugger;
                $scope.todos = $scope.searched($scope.Stud_Health_Details, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Stud_Health_Details;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_student_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }
            //||
            //item.sims_blood_group_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
            //item.sims_health_card_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sager == toSearch
            $scope.New = function () {
                $scope.medication = true;
                $scope.disability = true;
                $scope.restriction = true;
                $scope.hearing = true;
                $scope.vision = true;
                $scope.other = true;
                $scope.other_disability = true;
                $scope.disabled = false;
                $scope.readonly = true;
                $scope.table = false;
                $scope.newdisplay = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                //$scope.temp = "";
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                $scope.temp[name1] = year + "/" + month + "/" + day;
            }

            var datasend = []; var data;

            $scope.savedata = function () {//Myform

                // if (Myform) {
                var data = $scope.temp;
                data.opr = "I";

                //for (var i = 0; i < $scope.filteredTodos.length; i++) {
                //    if ($scope.filteredTodos[i].sims_enrollment_number != $scope.temp.enroll_number) {

                datasend.push(data);

                $http.post(ENV.apiUrl + "api/StudentHealth/CUDStudentHealth", datasend).then(function (msg) {
                    $scope.msg1 = msg.data;

                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                        $scope.clear();
                    }
                    else {
                        swal({ title: "Alert", text: "Record cannot be inserted, Student Health Record Already Exits", width: 300, height: 200 });
                    }

                    $http.get(ENV.apiUrl + "api/StudentHealth/getAllStudentHealth").then(function (res1) {
                        $scope.studHealth = res1.data;
                        $scope.totalItems = $scope.studHealth.length;
                        $scope.todos = $scope.studHealth;
                        $scope.makeTodos();
                    });

                    //$scope.stud_Details_fun(cur,year,grade,section);

                });
                $scope.table = true;
                $scope.newdisplay = false;
                datasend = [];
                // }
                // }
                //else {

                //    swal({ text: 'Record Already Exist', width: 300, height: 300 });
                //}
                //}
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.newdisplay = false;
                $scope.hide_img = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                //$scope.temp.dest_code = "";
                //$scope.temp.country_code = "";
                //$scope.temp.dest_name = "";
            }

            $scope.clear = function () {
                $scope.temp = '';
                $scope.hide_img = false;
            }

            $scope.edit = function (str) {
                debugger;
                $scope.medication = true;
                $scope.disability = true;
                $scope.restriction = true;
                $scope.hearing = true;
                $scope.vision = true;
                $scope.other = true;
                $scope.other_disability = true;

                $scope.table = false;
                $scope.newdisplay = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.temp = {

                    enroll_number: str.sims_enrollment_number,
                    sims_blood_group_code: str.sims_blood_group_code,
                    sims_health_card_number: str.sims_health_card_number,
                    sims_health_card_issue_date: str.sims_health_card_issue_date,
                    sims_health_card_expiry_date: str.sims_health_card_expiry_date,
                    sims_health_card_issuing_authority: str.sims_health_card_issuing_authority,
                    sims_height: str.sims_height,
                    sims_wieght: str.sims_wieght,
                    sims_teeth: str.sims_teeth,
                    sims_health_bmi: str.sims_health_bmi,
                    sims_medication_status: str.sims_medication_status,
                    sims_medication_desc: str.sims_medication_desc,
                    sims_disability_status: str.sims_disability_status,
                    sims_disability_desc: str.sims_disability_desc,
                    sims_health_restriction_status: str.sims_health_restriction_status,
                    sims_health_restriction_desc: str.sims_health_restriction_desc,
                    sims_health_hearing_status: str.sims_health_hearing_status,
                    sims_health_hearing_desc: str.sims_health_hearing_desc,
                    sims_health_vision_status: str.sims_health_vision_status,
                    sims_health_vision_desc: str.sims_health_vision_desc,
                    sims_health_other_status: str.sims_health_other_status,
                    sims_health_other_desc: str.sims_health_other_desc,
                    sims_regular_hospital_name: str.sims_regular_hospital_name,
                    sims_regular_hospital_phone: str.sims_regular_hospital_phone,
                    sims_regular_doctor_name: str.sims_regular_doctor_name,
                    sims_regular_doctor_phone: str.sims_regular_doctor_phone,
                    sims_has_your_child_any_other_disability: str.sims_health_other_disability_desc,
                    sims_has_your_child_any_other_disability_status: str.sims_health_other_disability_status,
                };
            }

            var dataupdate = []; var data;
            $scope.update = function () {//Myform
                // if (Myform) {
                var data = $scope.temp;
                data.opr = "U";

                dataupdate.push(data);
                $http.post(ENV.apiUrl + "api/StudentHealth/CUDStudentHealth", dataupdate).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                    }

                    //$http.get(ENV.apiUrl + "api/StudentHealth/getAllStudentHealth").then(function (res1) {
                    //    $scope.studHealth = res1.data;
                    //    $scope.totalItems = $scope.studHealth.length;
                    //    $scope.todos = $scope.studHealth;
                    //    $scope.makeTodos();
                    //
                    //    
                    //});
                    debugger;
                    $scope.stud_Details_fun(cur, year, grade, section);
                })
                $scope.table = true;
                $scope.newdisplay = false;
                dataupdate = [];
                //}
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.OkDelete = function () {
                var data1 = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'enroll_number': $scope.filteredTodos[i].sims_enrollment_number,
                            'opr': "D"
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/StudentHealth/CUDStudentHealth", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/StudentHealth/getAllStudentHealth").then(function (res1) {
                                                $scope.studHealth = res1.data;
                                                $scope.totalItems = $scope.studHealth.length;
                                                $scope.todos = $scope.studHealth;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted ", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/StudentHealth/getAllStudentHealth").then(function (res1) {
                                                $scope.studHealth = res1.data;
                                                $scope.totalItems = $scope.studHealth.length;
                                                $scope.todos = $scope.studHealth;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                data1 = [];
            }

            $scope.StudAdd = function (info) {
                $scope.temp = { comn_user_name: info.s_enroll_no };
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            var dom;
            $scope.flag = true;
            $scope.expand = function (info, $event) {

                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0' width='100%' style='border:solid;border-width:02px'>" +
                        "<tbody>" +

                         "<tr> <td class='semi-bold'>" + "Issuing Authority" + "</td> <td class='semi-bold'>" + "Teeth" + " </td><td class='semi-bold'>" + "Medication Status" + "</td><td class='semi-bold'>" + "Medication Desc" + "</td>" +
                         "<td class='semi-bold'>" + "Disability Status" + "</td> <td class='semi-bold'>" + "Disability Desc" + " </td><td class='semi-bold'>" + "Restriction Status" + "</td><td class='semi-bold'>" + "Restriction Desc" + "</td></tr>" +

                         "<tr><td>" + (info.sims_health_card_issuing_authority) + "</td> <td>" + (info.sims_teeth) + " </td><td>" + (info.sims_medication_status) + "</td><td>" + (info.sims_medication_desc) + "</td>" +
                        "<td>" + (info.sims_disability_status) + "</td> <td>" + (info.sims_disability_desc) + " </td><td>" + (info.sims_health_restriction_status) + "</td><td>" + (info.sims_health_restriction_desc) + "</td></tr>" +


                         "<tr><td class='semi-bold'>" + "Hearing Status" + "</td> <td class='semi-bold'>" + "Hearing Desc" + " </td><td class='semi-bold'>" + "Vision Status" + "</td><td class='semi-bold'>" + "Vision Desc" + "</td>" +
                         "<td class='semi-bold'>" + "Other Status" + "</td> <td class='semi-bold'>" + "Other Desc" + " </td><td class='semi-bold'>" + "Hospital Name" + "</td><td class='semi-bold'>" + "Hospital Phone No" + "</td></tr>" +


                         "<tr><td>" + (info.sims_health_hearing_status) + "</td> <td>" + (info.sims_health_hearing_desc) + " </td><td>" + (info.sims_health_vision_status) + "</td><td>" + (info.sims_health_vision_desc) + "</td>" +
                        "<td>" + (info.sims_health_other_status) + "</td> <td>" + (info.sims_health_other_desc) + " </td><td>" + (info.sims_regular_hospital_name) + "</td><td>" + (info.sims_regular_hospital_phone) + "</td></tr>" +
                         "<tr> <td class='semi-bold'>" + "Doctor Name" + "</td> <td class='semi-bold'>" + "Doctor Phone No" + "</td><td  colspan='7'></td></tr>" +

                         "<tr><td>" + (info.sims_regular_doctor_name) + "</td> <td>" + (info.sims_regular_doctor_phone) + " </td><td  colspan='7'></td></tr></table>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }
            }

            $scope.CheckedReadonly = function (str) {
                var d = document.getElementById(str);
                if (d.checked == true) {

                    $scope[str] = false;
                }
                else { $scope[str] = true; }
            }
        }])
})();
