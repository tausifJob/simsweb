﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Health');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('medicalMedicineCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.display = false;
            $scope.itemsPerPage = "5";
            $scope.currentPage = 0;
            $scope.items = [];
            $scope.url = ENV.apiUrl + 'Content' + '/' + $http.defaults.headers.common['schoolId'];
            var photofile;

            $http.get(ENV.apiUrl + "api/common/MedicalMedicine/GetSims_MedicalMedicine").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.items = res.data;
                console.log(res.data);
            });

            $http.get(ENV.apiUrl + "api/common/MedicalMedicine/GetMedicineType").then(function (res) {
                $scope.cmbMedicineType = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/MedicalMedicine/GetMedicineAlert").then(function (res) {
                $scope.cmbAlert = res.data;
            });


            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;

            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.items.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;
            };

            $scope.edit = function (str) {
                $scope.edt = str;
                $scope.display = true;
                $scope.grid = false;
                $scope.update1 = true;
                $scope.editflg = true;
                $scope.save1 = false;
                $scope.curChange(str.sims_timetable_cur);
                $scope.academicChange(str.sims_academic_year);
                $scope.gradeChange(str.sims_grade);
                //  $scope.edt = str;


            }

            $scope.Update = function (newEventForm) {
                if (newEventForm.$valid) {




                    var data = $scope.edt;
                    data.opr = 'U';


                    $http.post(ENV.apiUrl + "api/common/MedicalMedicine/InsertUpdateSims_MedicalMedicine", data).then(function (res) {
                        $scope.ins = res.data;
                        if (res.data) {
                            swal({ title: "Alert", text: "Record Updated Successfully.", imageUrl: "assets/img/check.png", });
                            $http.get(ENV.apiUrl + "api/common/MedicalMedicine/GetSims_MedicalMedicine").then(function (res) {
                                $scope.display = false;
                                $scope.grid = true;
                                $scope.items = res.data;
                                $scope.currentPage = 0;
                            });

                        }
                        else
                            swal({ title: "Alert", text: "Record Not Updated .", imageUrl: "assets/img/notification-alert.png", });


                    });




                }
            }

            $scope.Save = function (newEventForm) {

                if (newEventForm.$valid) {



                    var data = $scope.edt;
                    data.opr = 'I';

                    $http.post(ENV.apiUrl + "api/common/MedicalMedicine/InsertUpdateSims_MedicalMedicine", data).then(function (res) {
                        $scope.ins = res.data;
                        if (res.data) {
                            swal({ title: "Alert", text: "Record Inserted Successfully.", imageUrl: "assets/img/check.png", });
                            $http.get(ENV.apiUrl + "api/common/MedicalMedicine/GetSims_MedicalMedicine").then(function (res) {
                                $scope.display = false;
                                $scope.grid = true;
                                $scope.items = res.data;
                                $scope.currentPage = 0;
                            });

                        }
                        else
                            swal({ title: "Alert", text: "Record Not Inserted.", imageUrl: "assets/img/notification-alert.png", });


                    });




                }
            }

            $scope.New = function () {
                $scope.edt = "";
                $scope.grid = false;
                $scope.display = true;
                $scope.save1 = true;
                $scope.editflg = false;
                //$scope.edt.sims_reference_status = true;

            }


            var main, del = '', delvar = [];
            //$scope.chk = function () {
            //    main = document.getElementById('chk_min');
            //    del = [];
            //    if (main.checked == true) {
            //        for (var i = 0; i < $scope.items.length; i++) {
            //            var v = document.getElementById($scope.items[i].sims_medicine_code + i);
            //            $scope.items[i].sims_medicine_code1 = true;
            //        }
            //    }
            //    else {
            //        for (var i = 0; i < $scope.items.length; i++) {
            //            $scope.items[i].sims_medicine_code1 = false;
            //            main.checked = false;
            //            $scope.row1 = '';
            //        }
            //    }

            $scope.chk = function () {
                main = document.getElementById('chk_min');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.items.length; i++) {
                        var v = document.getElementById($scope.items[i].sims_medicine_code + i);

                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.items.length; i++) {
                        var v = document.getElementById($scope.items[i].sims_medicine_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }
            $scope.Delete = function () {
                debugger
                var deletefin = [];
                del = '';
                for (var i = 0; i < $scope.items.length; i++) {
                    var v = document.getElementById($scope.items[i].sims_medicine_code + i);
                    if (v.checked == true) {
                        var data = ({
                            'sims_medicine_code': $scope.items[i].sims_medicine_code,
                            opr: 'D'
                        });
                        deletefin.push(data);
                    }
                }
               
                $http.post(ENV.apiUrl + "api/common/MedicalMedicine/dataforDelete", deletefin).then(function (res) {
                    if (res.data) {
                        swal({ title: "Alert", text: "Record Deleted Successfully.", imageUrl: "assets/img/check.png", });
                        $http.get(ENV.apiUrl + "api/common/MedicalMedicine/GetSims_MedicalMedicine").then(function (res) {
                            $scope.display = false;
                            $scope.grid = true;
                            $scope.items = res.data;
                            $scope.currentPage = 0;
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Deleted.", imageUrl: "assets/img/notification-alert.png", });

                    }
                });

            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.update1 = false;
                $scope.save1 = false;
                $scope.editflg = false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });



        }]);



})();


