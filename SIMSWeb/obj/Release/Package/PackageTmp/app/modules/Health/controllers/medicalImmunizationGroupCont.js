﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Health');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('medicalImmunizationGroupCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.display = false;
            $scope.itemsPerPage = "5";
            $scope.currentPage = 0;
            $scope.items = [];
            $scope.url = ENV.apiUrl + 'Content' + '/' + $http.defaults.headers.common['schoolId'];
            var photofile;

            $http.get(ENV.apiUrl + "api/common/MedicalExamination/GetSims_MedicalImmunizationGroup").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.items = res.data;

                console.log(res.data);


            });


            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;

            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.items.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;
            };

            $scope.edit = function (str) {
                $scope.edt = str;
                $scope.display = true;
                $scope.grid = false;
                $scope.update1 = true;
                $scope.editflg = true;
                $scope.curChange(str.sims_timetable_cur);
                $scope.academicChange(str.sims_academic_year);
                $scope.gradeChange(str.sims_grade);
                //  $scope.edt = str;


            }

            $scope.Update = function (newEventForm) {
                if (newEventForm.$valid) {




                    var data = $scope.edt;
                    data.opr = 'U';


                    $http.post(ENV.apiUrl + "api/common/MedicalExamination/InsertSims_MedicalImmunizationGroup", data).then(function (res) {
                        $scope.ins = res.data;
                        if (res.data) {
                            swal({ title: "Alert", text: "Record Updated Successfully.", imageUrl: "assets/img/check.png", });
                            $http.get(ENV.apiUrl + "api/common/MedicalExamination/GetSims_MedicalImmunizationGroup").then(function (res) {
                                $scope.display = false;
                                $scope.grid = true;
                                $scope.items = res.data;
                                $scope.currentPage = 0;
                            });

                        }
                        else
                            swal({ title: "Alert", text: "Record Not Updated .", imageUrl: "assets/img/notification-alert.png", });


                    });




                }
            }

            $scope.Save = function (newEventForm) {

                if (newEventForm.$valid) {
                    var data = $scope.edt;
                    data.opr = 'I';
                    $http.post(ENV.apiUrl + "api/common/MedicalExamination/InsertSims_MedicalImmunizationGroup", data).then(function (res) {
                        $scope.ins = res.data;
                        if (res.data) {
                            swal({ title: "Alert", text: "Record Inserted Successfully.", imageUrl: "assets/img/check.png", });
                            $http.get(ENV.apiUrl + "api/common/MedicalExamination/GetSims_MedicalImmunizationGroup").then(function (res) {
                                $scope.display = false;
                                $scope.grid = true;
                                $scope.items = res.data;
                                $scope.currentPage = 0;
                            });
                        }
                        else
                            swal({ title: "Alert", text: "Record Not Inserted.", imageUrl: "assets/img/notification-alert.png", });
                    });

                }
            }

            $scope.New = function () {
                $scope.edt = "";
                $scope.grid = false;
                $scope.display = true;
                $scope.save1 = true;
                $scope.editflg = false;

                //$scope.edt.sims_reference_status = true;

            }

            var main, del = '', delvar = [];

            //$scope.chk = function () {
            //    main = document.getElementById('chk_min');
            //    del = [];
            //    if (main.checked == true) {
            //        for (var i = 0; i < $scope.items.length; i++) {
            //            $scope.items[i].sims_immunization_age_group_code1 = true;
            //        }
            //    }
            //    else {
            //        for (var i = 0; i < $scope.items.length; i++) {
            //            $scope.items[i].sims_immunization_age_group_code1 = false;
            //            main.checked = false;
            //            $scope.row1 = '';
            //        }
            //    }
            //}

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.items.length; i++) {
                        var v = document.getElementById($scope.items[i].sims_immunization_age_group_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.items.length; i++) {
                        var v = document.getElementById($scope.items[i].sims_immunization_age_group_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyoneselect = function () {

                $("input[type='checkbox']").change(function (e) {

                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.Delete = function () {
                debugger
                del = '';

                for (var i = 0; i < $scope.items.length; i++) {

                    if ($scope.items[i].sims_immunization_age_group_code1 == true) {
                        del = del + $scope.items[i].sims_immunization_age_group_code + ','
                    }

                }
                // var data = $scope.items[0];
                var data = { sims_immunization_age_group_code: del, opr: 'X' }
                //data.sims_serial_number = del;
                //data.opr = 'X';
                //delvar = [];
                //delvar.push({ sims_timetable_number: del });

                $http.post(ENV.apiUrl + "api/common/MedicalExamination/InsertSims_MedicalImmunizationGroup", data).then(function (res) {

                    if (res.data) {
                        swal({ title: "Alert", text: "Record Deleted Successfully.", imageUrl: "assets/img/check.png", });


                        $http.get(ENV.apiUrl + "api/common/MedicalExamination/GetSims_MedicalImmunizationGroup").then(function (res) {
                            $scope.display = false;
                            $scope.grid = true;
                            $scope.items = res.data;
                            $scope.currentPage = 0;
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Deleted.", imageUrl: "assets/img/notification-alert.png", });

                    }
                });

            }

            $scope.OkDelete = function () {

                var data1 = [];
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.items.length; i++) {
                    var v = document.getElementById($scope.items[i].sims_immunization_age_group_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_immunization_age_group_code': $scope.items[i].sims_immunization_age_group_code,
                            opr: 'X'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '', text: "Are you sure you want to Delete?", showCloseButton: true, showCancelButton: true, confirmButtonText: 'Yes', width: 380, cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger;
                            //$http.post(ENV.apiUrl + "api/MedicalStudentSusceptibility/CUDMedicalVisit", deleteleave).then(function (msg) {
                            $http.post(ENV.apiUrl + "api/common/MedicalExamination/Delete_sims_medical_immunization_age_group", deleteleave).then(function (res) {

                                if (res.data) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully.", imageUrl: "assets/img/check.png", });

                                    if (isConfirm) {
                                        main = document.getElementById('mainchk');

                                        if (main.checked == true) {
                                            main.checked = false;
                                            $scope.row1 = '';
                                        }

                                        $http.get(ENV.apiUrl + "api/common/MedicalExamination/GetSims_MedicalImmunizationGroup").then(function (res) {
                                            $scope.display = false;
                                            $scope.grid = true;
                                            $scope.items = res.data;
                                            $scope.currentPage = 0;
                                        });
                                    }

                                    $scope.currentPage = true;
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Deleted Successfully.", imageUrl: "assets/img/check.png", });
                                }


                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.items.length; i++) {
                                var v = document.getElementById($scope.items[i].sims_immunization_age_group_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }

                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }

                //$scope.currentPage = str;
                $scope.row1 = '';
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.update1 = false;
                $scope.save1 = false;
                $scope.editflg = false;
            }

        }]);

})();


