﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Health');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('MedicalStudentTermDetailCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.display = false;
            $scope.table1 = true;
            $scope.fee_no = true;
            $scope.table = true;
            $scope.visit_no = true;
            var check_no = null;
            $scope.chku = false;
            $scope.edt = [];

            //CUR
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.temp = {
                    'sims_cur_code': $scope.curriculum[0].sims_cur_code
                }
                $scope.getAccYear($scope.temp.sims_cur_code)
            });

            //Academic year
            $http.get(ENV.apiUrl + "api/MedicalStudentSusceptibility/getTerm").then(function (AllTerm) {
                $scope.getAllTerm = AllTerm.data;
                $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
            });
            //Grade
            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
                $scope.getSection($scope.temp.sims_cur_code, $scope.temp.sims_grade_code, $scope.temp.sims_academic_year)
            }

            //Section
            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }


            $scope.Submit = function ()
            {
                debugger
                $http.get(ENV.apiUrl + "api/MedicalStudentSusceptibility/getAllTermDetails?cur=" + $scope.temp.sims_cur_code + "&a_year=" + $scope.temp.sims_academic_year + "&grades=" + $scope.temp.sims_grade_code + "&section=" + $scope.temp.sims_section_code + "&term=" + $scope.temp.sims_term_code).then(function (AllTermDetails) {
                    debugger
                    $scope.getAllTerm_details = AllTermDetails.data;
                    $scope.totalItems = $scope.getAllTerm_details.length;
                    $scope.todos = $scope.getAllTerm_details;
                    $scope.makeTodos();
                });
            }
           
            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    debugger;
                    $scope.temp = {
                        'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                        'sims_academic_year': $scope.Acc_year[0].sims_academic_year
                    }
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                });

            }

            $rootScope.visible_stud = true;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = false;
            $rootScope.chkMulti = false;

            $scope.Search = function () {
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                //  $scope.display = true;
                //$scope.SelectedUserLst = [];
            }

            $scope.$on('global_cancel', function () {
                debugger;
                if ($scope.SelectedUserLst.length > 0) {
                    debugger
                    $scope.edt =
                        {
                            sims_enrollment_number: $scope.SelectedUserLst[0].s_enroll_no,
                            name: $scope.SelectedUserLst[0].name
                        }
                }
                $scope.em_number = $scope.edt.em_number;
            });


            $scope.user = $rootScope.globals.currentUser.username;

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {

                $scope.todos = $scope.searched($scope.getAllTerm_details, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.getAllTerm_details;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.std_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_health_card_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;

            }

            $scope.New = function () {

                $scope.temp = '';
                $scope.edt = '';
                $scope.display = true;
                $scope.table = false;
                $scope.table1 = false;
                $scope.instaminimuma = true;
                $scope.update1 = false;
              
                $scope.Update_btn = false;
                $scope.show_btn = true;


            }

            $scope.submit = function () {
                debugger;
                //if (isvalid)
                var deleteintcode = [];
                var Savedata = [];
                $scope.insert = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].datastatus == true) {
                       
                            var deleteintcode = ({
                                'sims_cur_code': $scope.temp.sims_cur_code,
                                'sims_academic_year': $scope.temp.sims_academic_year,
                                'sims_term_code': $scope.temp.sims_term_code,
                                'sims_enrollment_number': $scope.filteredTodos[i].sims_enroll_number,
                                'sims_health_card_number': $scope.filteredTodos[i].sims_health_card_number,
                                'sims_health_restriction_status': $scope.filteredTodos[i].sims_health_restriction_status,
                                'sims_health_restriction_desc': $scope.filteredTodos[i].sims_health_restriction_desc,
                                'sims_medication_status': $scope.filteredTodos[i].sims_medication_status,
                                'sims_medication_desc': $scope.filteredTodos[i].sims_medication_desc,
                                'sims_height': $scope.filteredTodos[i].sims_height,
                                'sims_wieght': $scope.filteredTodos[i].sims_wieght,
                                'sims_teeth': $scope.filteredTodos[i].sims_teeth,
                                'sims_health_bmi': $scope.filteredTodos[i].sims_health_bmi,
                                'sims_health_other_remark': $scope.filteredTodos[i].sims_health_other_remark,
                                opr: 'I',
                            });
                            $scope.insert = true;
                        Savedata.push(deleteintcode);
                    }
                }
                if ($scope.insert) {
                    $http.post(ENV.apiUrl + "api/MedicalStudentSusceptibility/CUDMedicalTermDetails", Savedata).then(function (msg) {
                        debugger;
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Inserted Successfully", width: 380, height: 200 });
                            $scope.Submit();
                        }
                        else {
                            swal({ title: "Alert", text: "Student Already Present", width: 380, height: 200 });
                        }

                    });
                }
                    $scope.display = false;
                    $scope.table = true;
                
            }


            $scope.calculateBMI = function (o) {

                debugger;
                var m = 0, bmi = 0, diff = 0, f_bmi = 0, h2 = 0;
                m = o.sims_height / 100;
                h2 = m * m;
                
                bmi = o.sims_wieght / h2;

                f_bmi = Math.floor(bmi);

                diff = bmi - f_bmi;
                diff = diff * 10;

                diff = Math.round(diff);
                if (diff == 10) {
                    // Need to bump up the whole thing instead
                    f_bmi += 1;
                    diff = 0;
                }
                bmi = f_bmi + "." + diff;
                o.sims_health_bmi = bmi;
            }
           

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].sims_enrollment_number);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].sims_enrollment_number);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            //$scope.checkonebyonedelete = function () {

            //    $("input[type='checkbox']").change(function (e) {
            //        if ($(this).is(":checked")) {
            //            $(this).closest('tr').addClass("row_selected");
            //            $scope.color = '#edefef';
            //        } else {
            //            $(this).closest('tr').removeClass("row_selected");
            //            $scope.color = '#edefef';
            //        }
            //    });

            //    main = document.getElementById('mainchk');
            //    if (main.checked == true) {
            //        main.checked = false;
            //        $scope.color = '#edefef';
            //        $scope.row1 = '';
            //    }
            //}


            $scope.checkonebyonedelete = function (str) {
                debugger
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                        //  str.status = true;
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                        //str.status = false;
                    }
                });
                str.status = str.datastatus;
            }

            $scope.edit = function (j) {
                debugger;
                $scope.temp = {
                    'sims_cur_code':j.sims_cur_code,
                    'sims_academic_year':j.sims_academic_year,
                    'sims_term_code':j.sims_term_code,
                  
                    'sims_health_card_number':j.sims_health_card_number,
                    'sims_height':j.sims_height,
                    'sims_wieght':j.sims_wieght,
                    'sims_teeth':j.sims_teeth,
                    'sims_health_bmi':j.sims_health_bmi,
                    'sims_health_restriction_status':j.sims_health_restriction_status,
                    'sims_health_restriction_desc':j.sims_health_restriction_desc,
                    'sims_medication_status':j.sims_medication_status,
                    'sims_medication_desc':j.sims_medication_desc,
                    'sims_health_other_remark': j.sims_health_other_remark
                }
                $scope.edt =
                    {
                        'sims_enrollment_number': j.sims_enrollment_number,
                    }

              
                $scope.Update_btn = true;
                $scope.show_btn = false;
                $scope.display = true;
                $scope.table = false;
                $scope.table1 = false;
            }

            $scope.update = function () {
                debugger
                var Savedata1 = [];

                var deleteintcode1 = ({
                    'sims_cur_code': $scope.temp.sims_cur_code,
                    'sims_academic_year': $scope.temp.sims_academic_year,
                    'sims_term_code': $scope.temp.sims_term_code,
                    'sims_enrollment_number': $scope.edt.sims_enrollment_number,
                    'sims_health_card_number': $scope.temp.sims_health_card_number,
                    'sims_health_restriction_status': $scope.temp.sims_health_restriction_status,
                    'sims_health_restriction_desc': $scope.temp.sims_health_restriction_desc,
                    'sims_medication_status': $scope.temp.sims_medication_status,
                    'sims_medication_desc': $scope.temp.sims_medication_desc,
                    'sims_height': $scope.temp.sims_height,
                    'sims_wieght': $scope.temp.sims_wieght,
                    'sims_teeth': $scope.temp.sims_teeth,
                    'sims_health_bmi': $scope.temp.sims_health_bmi,
                    'sims_health_other_remark': $scope.temp.sims_health_other_remark,

                    opr: 'U',
                });
                Savedata1.push(deleteintcode1);

                $http.post(ENV.apiUrl + "api/MedicalStudentSusceptibility/CUDMedicalTermDetails", Savedata1).then(function (msg) {
                    debugger;
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Record Updated Successfully", width: 380, height: 200 });
                        $http.get(ENV.apiUrl + "api/MedicalStudentSusceptibility/getAllTermDetails").then(function (AllTermDetails) {
                            $scope.getAllTerm_details = AllTermDetails.data;
                            $scope.totalItems = $scope.getAllTerm_details.length;
                            $scope.todos = $scope.getAllTerm_details;
                            $scope.makeTodos();
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Updated", width: 380, height: 200 });
                    }

                });
                $scope.display = false;
                $scope.table = true;

            }

            $scope.OkDelete = function () {
                debugger;
                var deletefin1 = [];
                var deletemodulecode=[];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].datastatus == true) {
                            $scope.flag = true;
                            var deletemodulecode = ({
                                'sims_academic_year': $scope.temp.sims_academic_year,
                                'sims_term_code': $scope.temp.sims_term_code,
                                'sims_enrollment_number': $scope.filteredTodos[i].sims_enroll_number,
                                opr: 'D'
                            });
                            deletefin1.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger
                            $http.post(ENV.apiUrl + "api/MedicalStudentSusceptibility/CUDMedicalTermDetails", deletefin1).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.Submit();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted ", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.Submit();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i + $scope.filteredTodos[i].sims_enrollment_number);
                                if (v.checked == true) {
                                    v.checked = false;
                                    //  $scope.row1 = '';
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }

            $scope.Cancel=function()
            {
                $scope.Submit();
            }

           

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

        }])
})();
