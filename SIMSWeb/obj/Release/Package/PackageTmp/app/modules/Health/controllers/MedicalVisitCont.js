﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Health');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('MedicalVisitCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.display = false;
            $scope.save1 = false;
            $scope.update1 = false;
            $scope.cancel1 = false;
            $scope.table1 = true;
            $scope.fee_no = true;
            $scope.table = true;
            $scope.visit_no = true;
            var check_no = null;
            $scope.chku = false;
            $scope.stud_info = false;
            $scope.Emp_info = false;
            $scope.emp_pre = false;
            $scope.stud_pre = false;
            $scope.Emp_number = false;
            $scope.Stud_number = false;
            $scope.ref_user = false;
            $scope.edt = [];
            $scope.imgurl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/';
            $scope.imgurle = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/employeeImages/';
            debugger;
            $scope.user = $rootScope.globals.currentUser.username;

            var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.dt = {
                sims_from_date: dateyear,
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.showdata = function () {
                $http.get(ENV.apiUrl + "api/MedicalStudentSusceptibility/getMedicalVisit").then(function (res1) {
                    $scope.obj = res1.data;
                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.makeTodos();
                    if ($scope.obj.length == 0) {
                        swal({ title: 'Alert', text: "No Record Found...", showCloseButton: true, width: 450, height: 200 });
                        $scope.display = false;
                        $scope.table = false;
                        $scope.table1 = true;
                    }
                    else {
                        $scope.display = false;
                        $scope.table = true;
                        $scope.table1 = true;
                    }
                })
            }

            $scope.showdata();

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {

                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_medical_visit_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sagar == toSearch) ? true : false;

            }

            $http.get(ENV.apiUrl + "api/MedicalStudentSusceptibility/getAllMedicalMedicineCode").then(function (getmedtype) {
                $scope.medtype = getmedtype.data;
                $scope.temp2 = { sims_medicine_code: $scope.medtype[0].sims_medicine_code };
            });

            $scope.New = function () {

                $scope.no_selection = false;

                $scope.dt = {
                    sims_from_date: dateyear,
                }

                $scope.edt['medical_attended_by'] = $rootScope.globals.currentUser.username;

                $http.get(ENV.apiUrl + "api/MedicalStudentSusceptibility/getAutoGenerateMedicalvisitNo").then(function (res) {
                    $scope.temp1 = {
                        sims_medical_visit_number: res.data,
                    }
                });

                $http.get(ENV.apiUrl + "api/MedicalStudentSusceptibility/getAllMedicalMedicineCode").then(function (getmedtype) {
                    $scope.medtype = getmedtype.data;
                    $scope.temp2 = { sims_medicine_code: $scope.medtype[0].sims_medicine_code };
                });

                $scope.edt = { sims_status: true, }
                $scope.clearvalue();
                $scope.display = true;
                $scope.table = false;
                $scope.table1 = false;
                $scope.instaminimuma = true;
                $scope.update1 = false;
                $scope.save1 = true;
                var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd hh:mm:ss');
                debugger;
                $scope.dt = {
                    sims_from_date:dateyear
                }
            }

            $scope.CheckInstallment = function () {
                debugger
                if ($scope.edt.sims_library_fee_installment_mode == true) {
                    $scope.instaminimuma = false;
                }
                else {
                    $scope.instaminimuma = true;
                }
            }

            $scope.obj1 = [];
            $scope.obj2 = [];
            debugger
            $scope.submit = function (isvalid) {
               debugger
                if(isvalid)
                {
                    $scope.stud_info = false;
                    $scope.Emp_info = false;
                    debugger;
              
                    var Savedata = [];
                    var deleteintcode =
                        ({
                            'sims_medical_visit_number': $scope.temp1.sims_medical_visit_number,
                            'medical_visit_date': $scope.dt.sims_from_date,
                            'sims_enroll_number': $scope.edt.sims_enroll_number,
                            'sims_employee_number': $scope.edte.sims_employee_number,
                            'medical_attended_by': $scope.user,
                            'medical_complaint_desc': $scope.edt1.medical_complaint_desc,
                            'medical_attendant_observation': $scope.edt2.medical_attendant_observation,
                            'medical_health_education': $scope.edt3.medical_health_education,
                            'medical_followup_comment': $scope.edt4.medical_followup_comment,
                            'sims_medicine_code': $scope.temp2.sims_medicine_code,
                            'sims_medicine_dosage': $scope.edt5.sims_medicine_dosage,
                            'sims_medicine_duration': $scope.edt6.sims_medicine_duration,
                            'sims_user_code_created_by': $scope.edtu.sims_user_code_created_by,
                            'sims_status': $scope.edt.sims_status,
                            'sims_blood_pressure': $scope.edt5.sims_blood_pressure,
                            'sims_temperature': $scope.edt5.sims_temperature,
                            'sims_pulse_rate': $scope.edt5.sims_pulse_rate,
                            'sims_respiratory_rate': $scope.edt5.sims_respiratory_rate,
                            'sims_spo2': $scope.edt5.sims_spo2,
                            'sims_drug_allergy': $scope.edt12.sims_drug_allergy,

                            opr: 'I',
                        });
                    Savedata.push(deleteintcode);

                    $http.post(ENV.apiUrl + "api/MedicalStudentSusceptibility/CUDMedicalVisit", Savedata).then(function (msg) {
                        ;
                        $scope.msg1 = msg.data;
                        if ($scope.msg1.strMessage != undefined) {
                            if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                                $scope.showdata();
                                $scope.currentPage = true;

                            }
                        }
                        $scope.chku = false;
                        $scope.clearvalue();
                    });
                }
            }
        

            $scope.cancel = function () {
                $scope.showdata();
                $scope.display = false;
                $scope.table = true;
                $scope.table1 = true;
                $scope.clearvalue();
                $scope.stud_info = false;
                $scope.Emp_info = false;
                $scope.enroll_no = false;
                $scope.stud_pre = false;
                $scope.enployee_no = false;
                $scope.emp_pre = false;

            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_medical_visit_number + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_medical_visit_number + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyoneselect = function () {

                $("input[type='checkbox']").change(function (e) {

                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.edit = function (j) {
                debugger;
                $scope.no_selection = true;
                $scope.visit_no = true;
                $scope.Emp_number = true;
                $scope.Stud_number = true;
                $scope.update1 = true;
                $scope.ref_user = true;
                $scope.save1 = false;
                // $scope.edt = j;
                $scope.edt['sims_enroll_number']=j.sims_enroll_number;
                $scope.edte = { 'sims_employee_number': j.sims_employee_number }
                $scope.edt["sims_status"] = j.sims_status;
                //$scope.Dropdownvalue();
                $scope.onPriChange = {
                    sims_medicine_code: j.sims_medicine_code,
                    sims_medicine_name: j.sims_medicine_name,
                }
                //edt.sims_enroll_number
                for (var i = 0; i < $scope.medtype.length; i++) {
                    if ($scope.medtype[i].sims_medicine_code == $scope.onPriChange.sims_medicine_code) {
                        $scope.temp2.sims_medicine_code = $scope.medtype[i].sims_medicine_code;
                        break;
                    }
                }
                debugger;
                if (j.sims_enroll_number != "-") {
                    $scope.enroll_no = true;
                    $scope.enployee_no = false;
                    $scope.edt['sims_enroll_number'] = j.sims_enroll_number;
                }

                if (j.sims_employee_number != "-") {
                    $scope.enroll_no = false;
                    $scope.enployee_no = true;
                    $scope.edte = { 'sims_employee_number': j.sims_employee_number }
                }

                $scope.temp1 = {
                    'sims_medical_visit_number': j.sims_medical_visit_number,
                }
                $scope.temp2 = {
                    'sims_medicine_code': j.sims_medicine_code,
                }

                $scope.edt1 = {
                    'medical_complaint_desc': j.medical_complaint_desc,
                }

                $scope.edt2 = {
                    'medical_attendant_observation': j.medical_attendant_observation,
                }

                $scope.edt3 = {
                    'medical_health_education': j.medical_health_education,
                }

                $scope.edt4 = {
                    'medical_followup_comment': j.medical_followup_comment,
                }
                debugger
                $scope.edt12 = {
                    'sims_drug_allergy': j.sims_drug_allergy
                }

                $scope.edt5 = {
                    'sims_medicine_dosage': j.sims_medicine_dosage,
                    'sims_blood_pressure':j.sims_blood_pressure,
                    'sims_temperature':j.sims_temperature,
                    'sims_pulse_rate':j.sims_pulse_rate,
                    'sims_respiratory_rate':j.sims_respiratory_rate,
                    'sims_spo2':j.sims_spo2,
                }

                debugger;
                var a = [];
                var b = j.medical_visit_date;
                a = b.split(" ");
                $scope.dt['sims_from_date'] = a[0];

               // $scope.dt['sims_from_date'] = j.medical_visit_date;
                

                $scope.edt6 = {
                    'sims_medicine_duration': j.sims_medicine_duration,
                }

                $scope.edtu = {
                   
                    'sims_user_code_created_name': j.sims_user_code_created_by
                }
                // 'sims_user_code_created_name': j.sims_user_code_created_by,

                
                if (j.sims_enroll_number != null && j.sims_enroll_number!="-") {
                    $http.get(ENV.apiUrl + "api/MedicalStudentSusceptibility/getGenderDOB?S_number=" + j.sims_enroll_number + "&E_number=" + null).then(function (getStudData) {
                        debugger
                        $scope.get_studData = getStudData.data;
                        $scope.Emp_info = false;
                        $scope.stud_info = true;
                        $scope.stud_pre = true;
                        $scope.emp_pre = false;
                    });
                }
                else {
                    $http.get(ENV.apiUrl + "api/MedicalStudentSusceptibility/getGenderDOB?S_number=" + null + "&E_number=" + j.sims_employee_number).then(function (getEmpData) {
                        debugger
                        $scope.get_EmpData = getEmpData.data;
                        $scope.Emp_info = true;
                        $scope.stud_info = false;
                        $scope.stud_pre = false;
                        $scope.emp_pre = true;
                    });
                }

                $scope.display = true;
                $scope.table = false;
                $scope.table1 = false;
            }

            $scope.Update = function () {
                debugger
                var Savedata = [];
                var deleteintcode = ({
                    'sims_medical_visit_number': $scope.temp1.sims_medical_visit_number,
                    'medical_visit_date': $scope.dt.sims_from_date,
                    'sims_enroll_number': $scope.edt.sims_enroll_number,
                    'sims_employee_number': $scope.edte.sims_employee_number,
                    'medical_attended_by': $scope.user,
                    'medical_complaint_desc': $scope.edt1.medical_complaint_desc,
                    'medical_attendant_observation': $scope.edt2.medical_attendant_observation,
                    'medical_health_education': $scope.edt3.medical_health_education,
                    'medical_followup_comment': $scope.edt4.medical_followup_comment,
                    'sims_medicine_code': $scope.temp2.sims_medicine_code,
                    'sims_medicine_dosage': $scope.edt5.sims_medicine_dosage,
                    'sims_medicine_duration': $scope.edt6.sims_medicine_duration,
                    'sims_user_code_created_by': $scope.edtu.sims_user_code_created_name,
                    'sims_status': $scope.edt.sims_status,
                    'sims_blood_pressure':$scope.edt5.sims_blood_pressure,
                    'sims_temperature':$scope.edt5.sims_temperature,
                    'sims_pulse_rate':$scope.edt5.sims_pulse_rate,
                    'sims_respiratory_rate':$scope.edt5.sims_respiratory_rate,
                    'sims_spo2':$scope.edt5.sims_spo2,
                    'sims_drug_allergy': $scope.edt12.sims_drug_allergy,
                    opr: 'U',
                });
                Savedata.push(deleteintcode);

                $http.post(ENV.apiUrl + "api/MedicalStudentSusceptibility/CUDMedicalVisit", Savedata).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1.strMessage != undefined) {
                        if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                            swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                            $scope.showdata();
                            $scope.currentPage = true;
                        }
                    }
                    else {
                        swal({ title: "Alert", text: "Please Select Atleast One Student...", showCloseButton: true, width: 380, });
                    }
                    $scope.clearvalue();
                });

            }

            $scope.OkDelete = function () {
                $scope.enployee_no = false;
                $scope.enroll_no = false;
                $scope.stud_pre = false;
                $scope.emp_pre = false;

                var data1 = [];
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_medical_visit_number + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_medical_visit_number': $scope.filteredTodos[i].sims_medical_visit_number,
                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '', text: "Are you sure you want to Delete?", showCloseButton: true, showCancelButton: true, confirmButtonText: 'Yes', width: 380, cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/MedicalStudentSusceptibility/CUDMedicalVisit", deleteleave).then(function (msg) {
                                {

                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1.strMessage != undefined) {

                                        if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                            swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 }).then(function (isConfirm) {
                                                if (isConfirm) {
                                                    $scope.showdata();
                                                    main = document.getElementById('mainchk');
                                                    if (main.checked == true) {
                                                        main.checked = false;
                                                        $scope.row1 = '';
                                                    }
                                                }
                                                $scope.currentPage = true;
                                            });
                                        }
                                    }
                                }
                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_library_fee_number + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }

                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }

                $scope.currentPage = str;
                $scope.row1 = '';
            }

            $scope.clearvalue = function () {

                $scope.temp1 = { sims_medical_visit_number: '', }
                $scope.dt = { sims_from_date: '', }
                $scope.edt = { sims_enroll_number: '' }
                $scope.edte = { sims_employee_number: '', }
                $scope.edt1 = { medical_complaint_desc: '', }
                $scope.edt2 = { medical_attendant_observation: '', }
                $scope.edt3 = { medical_health_education: '', }
                $scope.edt4 = { medical_followup_comment: '', }
                $scope.temp2 = { sims_medicine_code: '', }
                $scope.edt5 = {
                    sims_medicine_dosage: '',
                    sims_medicine_dosage: '',
                    sims_blood_pressure: '',
                    sims_temperature: '',
                    sims_pulse_rate: '',
                    sims_respiratory_rate: '',
                    sims_spo2: '',

                }
                $scope.edt6 = { sims_medicine_duration: '', }
                $scope.edt = { sims_user_code_created_by: '', }
                $scope.edt = { sims_status: true, }
                $scope.edtu = { sims_user_code_created_name: '', }
                $scope.searchText = '';
                $scope.MyForm.$setPristine();
                $scope.MyForm.$setUntouched();
            }

            $scope.SearchUser = function () {
                $scope.chku = true;
                $rootScope.chkMulti = true;
                $rootScope.visible_User = true;
                $scope.visible_Employee = false;
                $rootScope.visible_stud = false;
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }

            $scope.SearchEnroll = function (str) {
                $rootScope.chkMulti = true;
                check_no = str;
               
                if (str == 'enroll_true') {
                   

                    $scope.get_studData = '';

                    $scope.employee_no = false;
                    $scope.enroll_no = true;
                    //      $scope.stud_info = true;
                    $scope.Emp_info = false;

                    $scope.emp_pre = false;
                    $scope.stud_pre = true;

                    $scope.visible_Employee = false;
                    $rootScope.visible_User = false;
                    $rootScope.visible_stud = true;

                    $scope.global_Search_click();
                    $('#Global_Search_Modal').modal({ backdrop: "static" });


                    $scope.edt.sims_student_name = '';
                    $scope.edt.s_class = '';
                    $scope.get_studData[0].sims_student_gender = '';
                    $scope.get_studData[0].sims_student_dob = '';
                    $scope.get_studData = "";
                    $scope.edt = [];


                }

                else if (str == 'emp_true') {
                    $scope.get_EmpData = '';
                    $scope.enroll_no = false;
                    $scope.stud_info = false;
                    $scope.enployee_no = true;
                    $scope.Emp_info = true;
                    $scope.emp_pre = true;
                    $scope.stud_pre = false;
                    $rootScope.visible_stud = false;
                    $rootScope.visible_User = false;
                    $rootScope.visible_Employee = true;
                    $scope.chkMulti = false;
                    $scope.global_Search_click();
                    $('#Global_Search_Modal').modal({ backdrop: "static" });
                }
            }

            $scope.Stude_prev=function()
            {
                debugger
                $http.get(ENV.apiUrl + "api/MedicalStudentSusceptibility/getStudEmpData?S_number=" + $scope.edt.sims_enroll_number + "&E_number=" + null).then(function (getStudData_model) {
                    debugger
                    $scope.get_studData_model = getStudData_model.data;
                    if ($scope.get_studData_model.length > 0) {
                        $('#MyModal').modal('show');
                    }
                    else {
                        swal({ title: 'Alert', text: "Previous Details Not Present...", showCloseButton: true, width: 450, height: 200 });
                    }
                });
                
               
            }

            $scope.Emp_prev = function ()
            {
                debugger
                $http.get(ENV.apiUrl + "api/MedicalStudentSusceptibility/getStudEmpData?S_number=" + null + "&E_number=" + $scope.edte.sims_employee_number).then(function (getEmpData_model) {
                    debugger
                    $scope.get_EmpData_model = getEmpData_model.data;
                    if ($scope.get_EmpData_model.length > 0) {
                        $('#MyModal1').modal('show');
                    }
                    else {
                        swal({ title: 'Alert', text: "Previous Details Not Present...", showCloseButton: true, width: 450, height: 200 })
                    }
                });
            }
           
            $scope.emp_blur=function()
            {
                debugger
                $http.get(ENV.apiUrl + "api/MedicalStudentSusceptibility/getGenderDOB?S_number=" + null + "&E_number=" + $scope.edte.sims_employee_number).then(function (getEmpData) {
                    debugger
                    $scope.get_EmpData = getEmpData.data;
                });
            }

            $scope.stud_blur=function()
            {
                $http.get(ENV.apiUrl + "api/MedicalStudentSusceptibility/getGenderDOB?S_number=" + $scope.edt.sims_enroll_number + "&E_number=" + null).then(function (getStudData) {
                    debugger
                    $scope.get_studData = getStudData.data;
                    $scope.temp.sims_empno = '';
                    $scope.stud_info = true;
                });
            }

            $scope.$on('global_cancel', function () {
                debugger;
                var t = $scope.SelectedUserLst.length;
                if ($scope.chku == false) {
                    if (check_no == 'enroll_true') {
                        $scope.edt =
                    {
                        sims_enroll_number: $scope.SelectedUserLst[0].s_enroll_no,
                        sims_student_name: $scope.SelectedUserLst[0].name,
                        s_class: $scope.SelectedUserLst[0].s_class,
                        //sims_cur_code: $scope.SelectedUserLst[0].s_cur_code,
                        //sims_academic_year: $scope.SelectedUserLst[0].sims_academic_year,
                    }
                        $scope.edte =
                           {
                               sims_employee_number: '',
                           }
                        debugger
                        $http.get(ENV.apiUrl + "api/MedicalStudentSusceptibility/getGenderDOB?S_number=" + $scope.edt.sims_enroll_number + "&E_number="+null).then(function (getStudData) {
                            debugger
                            $scope.get_studData = getStudData.data;
                            $scope.temp.sims_empno = '';
                            $scope.stud_info = true;
                        });

                    }
                    else if (check_no == 'emp_true') {
                        debugger
                        $scope.edte =
                           {
                               sims_employee_number: $scope.SelectedUserLst[0].em_login_code,
                               empName: $scope.SelectedUserLst[0].empName,
                           }
                        $scope.edt =
                           {
                               sims_enroll_number: '',
                           }

                        $http.get(ENV.apiUrl + "api/MedicalStudentSusceptibility/getGenderDOB?S_number="+null+"&E_number="+ $scope.edte.sims_employee_number).then(function (getEmpData) {
                            debugger
                            $scope.get_EmpData = getEmpData.data;
                        });

                    }
                }
                           

                else {
                    debugger
                    $scope.edtu =
                      {
                          sims_user_code_created_name: $scope.SelectedUserLst[0].name,
                          sims_user_code_created_by: $scope.SelectedUserLst[0].user_code,
                      }

                }
                //   }
                // }

                       
              
            });

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

        }])
})();
