﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, main1, modulecode = [];
    var cur_code1, ac_year1, grd_code1, sec_code1;

    var simsController = angular.module('sims.module.Health');//  
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StudentImmunizationApplyCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.table = false;
            $scope.busyindicator = false;
            $scope.Savevisible = false;
            $scope.paginghidden = false;
            $scope.tableupdate = false;
            $scope.Save = false;
            $scope.Update = false;

            $scope.pagesize = "5";
            $scope.pageindex = "1";

            $timeout(function () {
                $("#example").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                });
            }

            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
            }

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }

            $http.get(ENV.apiUrl + "api/MedicalStudentImmunizationApply/getStudentImmunizationdesc").then(function (AllImmu) {
                $scope.Studentimmunizationdesc = AllImmu.data;
            });

            $scope.Show_Data = function (cur_code, ac_year, grd_code, sec_code) {
                if (cur_code != undefined && ac_year != undefined) {
                    cur_code1 = cur_code;
                    ac_year1 = ac_year;
                    grd_code1 = grd_code;
                    sec_code1 = sec_code;
                    $http.get(ENV.apiUrl + "api/MedicalStudentImmunizationApply/getStudentApply?cur_name=" + cur_code + "&academic_year=" + ac_year + "&grd_code=" + grd_code + "&sec_code=" + sec_code).then(function (res) {
                        $scope.studentname = res.data;
                        $scope.allrecord = res.data;
                        if ($scope.studentname.length == 0) {
                            swal({ title: 'Alert', text: "Please Try Again Record Not Found...", showCloseButton: true, width: 450, height: 200 });
                        }
                        else {
                            $scope.table = true;
                            $scope.busyindicator = false;
                            $scope.ShowCheckBoxes = true;
                            $scope.Pagershow = true;
                            $scope.Savevisible = true;
                            $scope.paginghidden = false;
                            $scope.tableupdate = false;
                            $scope.Save = true;
                            $scope.Update = false;
                            $scope.row2 = '';
                            $scope.searchTextupdate = '';
                            main1.checked = false;
                        }
                    })
                }
                else {
                    swal({ title: 'Alert', text: "Please Select Curriculum & Academic Year Then Try Again...", showCloseButton: true, width: 450, height: 200 });
                }
            }

            $scope.Update_Data = function (cur_code, ac_year, grd_code, sec_code) {
                if (cur_code != undefined && ac_year != undefined) {
                    $http.get(ENV.apiUrl + "api/MedicalStudentImmunizationApply/getStudentForupdate?cur_name=" + cur_code + "&academic_year=" + ac_year + "&grd_code=" + grd_code + "&sec_code=" + sec_code).then(function (res) {
                        $scope.studentimmuupdate = res.data;
                        $scope.allrecordforupdate = res.data;
                        if ($scope.studentimmuupdate.length == 0) {
                            swal({ title: 'Alert', text: "Please Try Again Record Not Found...", showCloseButton: true, width: 450, height: 200 });
                        }
                        else {
                            debugger;
                            $scope.table = false;
                            $scope.busyindicator = false;
                            $scope.ShowCheckBoxes = true;
                            $scope.tableupdate = true;
                            $scope.Save = false;
                            $scope.Update = true;
                            $scope.Savevisible = true;
                            $scope.row1 = '';
                            $scope.searchText = '';
                            main.checked = false;
                        }
                    })
                }
                else {
                    swal({ title: 'Alert', text: "Please Select Curriculum & Academic Year Then Try Again...", showCloseButton: true, width: 450, height: 200 });
                }
            }

            $scope.clear = function () {
                $http.get(ENV.apiUrl + "api/MedicalStudentImmunizationApply/getStudentApply?cur_name=" + cur_code1 + "&academic_year=" + ac_year1 + "&grd_code=" + grd_code1 + "&sec_code=" + sec_code1).then(function (res) {
                    $scope.studentname = res.data;
                    $scope.allrecord = res.data;
                    if ($scope.studentname.length == 0) {
                        swal({ title: 'Alert', text: "Please Try Again Record Not Found...", showCloseButton: true, width: 450, height: 200 });
                    }
                    else {
                        $scope.table = true;
                        $scope.busyindicator = false;
                        $scope.ShowCheckBoxes = true;
                        $scope.Pagershow = true;
                        $scope.Savevisible = true;
                        $scope.paginghidden = false;
                        $scope.tableupdate = false;
                        $scope.Save = true;
                        $scope.Update = false;
                        $scope.row2 = '';
                        $scope.searchTextupdate = '';
                        main1.checked = false;
                    }
                })
            }

            $scope.selectdosage = function (str, obj) {
                for (var i = 0; i < $scope.Studentimmunizationdesc.length; i++) {
                    if (str == $scope.Studentimmunizationdesc[i].student_immunization_code) {
                        obj['student_immunization_dosage1'] = $scope.Studentimmunizationdesc[i].student_immunization_dosage1
                        obj['student_immunization_dosage2'] = $scope.Studentimmunizationdesc[i].student_immunization_dosage2
                        obj['student_immunization_dosage3'] = $scope.Studentimmunizationdesc[i].student_immunization_dosage3
                        obj['student_immunization_dosage4'] = $scope.Studentimmunizationdesc[i].student_immunization_dosage4
                        obj['student_immunization_dosage5'] = $scope.Studentimmunizationdesc[i].student_immunization_dosage5
                        obj['student_immunization_code'] = $scope.Studentimmunizationdesc[i].student_immunization_code
                        obj['student_immunization_desc'] = $scope.Studentimmunizationdesc[i].student_immunization_desc
                    }
                }
            }

            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.studentname.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.studentname.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.CheckAllselect = function () {

                main1 = document.getElementById('mainchk1');
                if (main1.checked == true) {
                    for (var i = 0; i < $scope.studentimmuupdate.length; i++) {
                        var v = document.getElementById($scope.studentimmuupdate[i].sims_enroll_number);
                        v.checked = true;
                        $scope.row2 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.studentimmuupdate.length; i++) {
                        var v = document.getElementById($scope.studentimmuupdate[i].sims_enroll_number);
                        v.checked = false;
                        main1.checked = false;
                        $scope.row2 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.checkonebyoneselect = function () {

                $("input[type='checkbox']").change(function (e)
                {

                    if ($(this).is(":checked"))
                    {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else
                    {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main1 = document.getElementById('mainchk1');
                if (main1.checked == true) {
                    main1.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
                //debugger;

            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {

                $scope.studentname = $scope.searched($scope.studentname, $scope.searchText);
                if ($scope.searchText == "") {
                    $scope.studentname = $scope.allrecord;
                }
                else {
                    $scope.studentname = $scope.studentname;
                }
            }

            function searchUtil(item, toSearch) {

                return (item.student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.student_age_group.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.student_age_group == toSearch) ? true : false;
            }

            $(document).ready(function () {
                $('input[type="date"]').datepicker();
            });

            $scope.Reset = function () {
                $scope.edt =
                {
                    sims_cur_code: '',
                    sims_academic_year: '',
                    sims_grade_code: '',
                    sims_section_code: '',
                }
                $scope.table = false;
                $scope.Savevisible = false;
                $scope.tableupdate = false;
                $scope.Save = false;
                $scope.Update = false;
                $scope.studentname = [];
                $scope.example = '';
                $scope.studentimmuupdate = [];
                $scope.j = '';
                $scope.busyindicator = false;
                $scope.ShowCheckBoxes = false;
                $scope.Pagershow = false;
                $scope.paginghidden = false;
                $scope.row2 = '';
                $scope.row1 = '';
                $scope.searchTextupdate = '';
                $('tr').removeClass("row_selected");
                main.checked = false;
                main1.checked = false;
            }

            $scope.Savedata = function () {
                var Savedata = [];
                var date1 = null;
                for (var i = 0; i < $scope.studentname.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        var Vacdate = $scope.studentname[i].Vacdate;
                        Vacdate = moment(Vacdate).format('YYYY/MM/DD');
                        var deleteintcode = ({
                            'sims_enroll_number': $scope.studentname[i].sims_enroll_number,
                            'sims_immunization_age_group_code': $scope.studentname[i].sims_immunization_age_group_code,
                            'student_immunization_code': $scope.studentname[i].student_immunization_code,
                            'sims_vaccination_date': Vacdate,
                            'sims_vaccination_renewal_date': date1,
                            'student_immunization_desc': $scope.studentname[i].student_immunization_desc,
                            'student_immunization_dosage1': $scope.studentname[i].student_immunization_dosage1,
                            'student_immunization_dosage2': $scope.studentname[i].student_immunization_dosage2,
                            'student_immunization_dosage3': $scope.studentname[i].student_immunization_dosage3,
                            'student_immunization_dosage4': $scope.studentname[i].student_immunization_dosage4,
                            'student_immunization_dosage5': $scope.studentname[i].student_immunization_dosage5,
                            opr: 'I',
                        });
                        Savedata.push(deleteintcode);
                    }
                }


                $http.post(ENV.apiUrl + "api/MedicalStudentImmunizationApply/StudentApplyImmunization", Savedata).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1.strMessage != undefined) {
                        if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                            $scope.clear();
                            swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                        }
                    }
                    else {
                        $scope.clear();
                        swal({ title: "Alert", text: "Please Select Atleast One Student...", showCloseButton: true, width: 380, });
                    }
                });
            }

            $scope.Updatedata = function () {
                var Savedata = [];
                var date1 = null;
                for (var i = 0; i < $scope.studentimmuupdate.length; i++) {
                    var v = document.getElementById($scope.studentimmuupdate[i].sims_enroll_number);
                    debugger;
                    if (v.checked == true) {
                        var Rvacdate = $scope.studentimmuupdate[i].VacRenewdate;
                        Rvacdate = moment(Rvacdate).format('YYYY/MM/DD');
                        var deleteintcode = ({
                            'sims_enroll_number': $scope.studentimmuupdate[i].sims_enroll_number,
                            'sims_immunization_age_group_code': $scope.studentimmuupdate[i].sims_immunization_age_group_code,
                            'student_immunization_code': $scope.studentimmuupdate[i].student_immunization_code,
                            'sims_vaccination_date': $scope.studentimmuupdate[i].sims_vaccination_date,
                            'sims_vaccination_renewal_date': Rvacdate,
                            'student_immunization_desc': $scope.studentimmuupdate[i].student_immunization_desc,
                            'student_immunization_dosage1': $scope.studentimmuupdate[i].student_immunization_dosage1,
                            'student_immunization_dosage2': $scope.studentimmuupdate[i].student_immunization_dosage2,
                            'student_immunization_dosage3': $scope.studentimmuupdate[i].student_immunization_dosage3,
                            'student_immunization_dosage4': $scope.studentimmuupdate[i].student_immunization_dosage4,
                            'student_immunization_dosage5': $scope.studentimmuupdate[i].student_immunization_dosage5,
                            opr: 'U',
                        });
                        Savedata.push(deleteintcode);
                    }
                }

                $http.post(ENV.apiUrl + "api/MedicalStudentImmunizationApply/StudentApplyImmunization", Savedata).then(function (msg) {
                    debugger;
                    $scope.msg1 = msg.data;
                    if ($scope.msg1.strMessage != undefined) {

                        if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != 'undefined') {
                            swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                        }
                        
                    }
                    else {
                        swal({ title: "Alert", text: "Please Select CheckBox...", width: 380, height: 200 });
                    }
                });
            }

            $scope.searchedupdate = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtilupdate(i, toSearch);
                });
            };

            $scope.searchforupdate = function () {
                debugger;
                $scope.studentimmuupdate = $scope.searchedupdate($scope.studentimmuupdate, $scope.searchTextupdate);
                if ($scope.searchTextupdate == "") {
                    $scope.studentimmuupdate = $scope.allrecordforupdate;
                }
                else {
                    $scope.studentimmuupdate = $scope.studentimmuupdate;
                }
            }

            function searchUtilupdate(item, toSearch) {

                return (item.student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.student_age_group.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.student_age_group == toSearch) ? true : false;
            }

        }])
})();
