﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Health');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('medicalSusceptibilityCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.display = false;
            $scope.itemsPerPage = "5";
            $scope.currentPage = 0;
            $scope.items = [];
            $scope.url = ENV.apiUrl + 'Content' + '/' + $http.defaults.headers.common['schoolId'];
            var photofile;

            $http.get(ENV.apiUrl + "api/common/MedicalSusceptibility/GetSims_MedicalSusceptibility").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.items = res.data;

                console.log(res.data);


            });


            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;

            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.items.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;
            };

            $scope.edit = function (str) {
                $scope.edt = str;
                $scope.display = true;
                $scope.grid = false;
                $scope.update1 = true;
                $scope.save1 = false;
                $scope.editflg = true;

                //  $scope.edt = str;


            }


            $scope.Update = function (newEventForm) {
                if (newEventForm.$valid) {
                    var data = $scope.edt;
                    data.opr = 'U';
                    $http.post(ENV.apiUrl + "api/common/MedicalSusceptibility/InsertSims_MedicalSusceptibility", data).then(function (res) {
                        $scope.ins = res.data;
                        if (res.data) {
                            swal({ title: "Alert", text: "Record Updated Successfully.", imageUrl: "assets/img/check.png", });
                            $http.get(ENV.apiUrl + "api/common/MedicalSusceptibility/GetSims_MedicalSusceptibility").then(function (res) {
                                $scope.display = false;
                                $scope.grid = true;
                                $scope.items = res.data;
                                $scope.currentPage = 0;
                            });

                        }
                        else
                            swal({ title: "Alert", text: "Record Not Updated .", imageUrl: "assets/img/notification-alert.png", });


                    });
                }
            }

            $scope.Save = function (newEventForm) {

                if (newEventForm.$valid) {



                    var data = $scope.edt;
                    data.opr = 'I';

                    $http.post(ENV.apiUrl + "api/common/MedicalSusceptibility/InsertSims_MedicalSusceptibility", data).then(function (res) {
                        $scope.ins = res.data;
                        if (res.data) {
                            swal({ title: "Alert", text: "Record Inserted Successfully.", imageUrl: "assets/img/check.png", });
                            $http.get(ENV.apiUrl + "api/common/MedicalSusceptibility/GetSims_MedicalSusceptibility").then(function (res) {
                                $scope.display = false;
                                $scope.grid = true;
                                $scope.items = res.data;
                                $scope.currentPage = 0;
                            });

                        }
                        else
                            swal({ title: "Alert", text: "Record Not Inserted.", imageUrl: "assets/img/notification-alert.png", });


                    });




                }
            }


            $scope.New = function () {
                $scope.edt = "";
                $scope.grid = false;
                $scope.display = true;
                $scope.save1 = true;
                $scope.editflg = false;

                //$scope.edt.sims_reference_status = true;

            }


            var main, del = '', delvar = [];
            //$scope.chk = function () {
            //    main = document.getElementById('chk_min');
            //    del = [];
            //    if (main.checked == true) {
            //        for (var i = 0; i < $scope.items.length; i++) {
            //            $scope.items[i].sims_susceptibility_code1 = true;
            //        }
            //    }
            //    else {
            //        for (var i = 0; i < $scope.items.length; i++) {
            //            $scope.items[i].sims_susceptibility_code1 = false;
            //            main.checked = false;
            //            $scope.row1 = '';
            //        }
            //    }
            //}

            $scope.chk = function () {
                main = document.getElementById('chk_min');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.items.length; i++) {
                        var v = document.getElementById($scope.items[i].sims_susceptibility_code + i);

                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.items.length; i++) {
                        var v = document.getElementById($scope.items[i].sims_susceptibility_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            //$scope.Delete = function () {
            //    debugger
            //    del = '';
            // var   delvar=[];
            // for (var i = 0; i < $scope.items.length; i++)
            // {
            //         if ($scope.items[i].sims_susceptibility_code1 == true)
            //         {
            //            del = del + $scope.items[i].sims_susceptibility_code + ','
            //         }
            //  }
            //    var data = { sims_susceptibility_code: del, opr: 'X' }
            //    $http.post(ENV.apiUrl + "api/common/MedicalSusceptibility/InsertSims_MedicalSusceptibility", data).then(function (res) {
            //        if (res.data) {
            //            swal({ title: "Alert", text: "Record Deleted Successfully.", imageUrl: "assets/img/check.png", });
            //            $http.get(ENV.apiUrl + "api/common/MedicalSusceptibility/GetSims_MedicalSusceptibility").then(function (res) {
            //                $scope.display = false;
            //                $scope.grid = true;
            //                $scope.items = res.data;
            //                $scope.currentPage = 0;
            //            });
            //        }
            //        else {
            //            swal({ title: "Alert", text: "Record Not Deleted.", imageUrl: "assets/img/notification-alert.png", });
            //        }
            //    });
            //}


            $scope.Delete = function () {
                debugger
                var deletefin = [];
                del = '';
                for (var i = 0; i < $scope.items.length; i++) {
                    var v = document.getElementById($scope.items[i].sims_susceptibility_code + i);
                    if (v.checked == true) {
                        var data = ({
                            'sims_susceptibility_code': $scope.items[i].sims_susceptibility_code,
                            opr: 'D'
                        });
                        deletefin.push(data);
                    }
                }
                $http.post(ENV.apiUrl + "api/common/MedicalSusceptibility/dataforDeletemedical", deletefin).then(function (res) {
                    if (res.data) {
                        swal({ title: "Alert", text: "Record Deleted Successfully.", imageUrl: "assets/img/check.png", });
                        $http.get(ENV.apiUrl + "api/common/MedicalSusceptibility/GetSims_MedicalSusceptibility").then(function (res) {
                            $scope.display = false;
                            $scope.grid = true;
                            $scope.items = res.data;
                            $scope.currentPage = 0;
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Deleted.", imageUrl: "assets/img/notification-alert.png", });
                    }
                });
            }


            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.update1 = false;
                $scope.save1 = false;
                $scope.editflg = false;
            }



        }]);



})();


