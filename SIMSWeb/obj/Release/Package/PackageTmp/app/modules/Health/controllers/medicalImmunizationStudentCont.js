﻿(function () {
    'use strict';
  
    var simsController = angular.module('sims.module.Health');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('medicalImmunizationStudentCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.display = false;
            $scope.itemsPerPage = "5";
            $scope.currentPage = 0;
            $scope.items = [];
            $scope.url = ENV.apiUrl + 'Content' + '/' + $http.defaults.headers.common['schoolId'];
            var photofile;

            $http.get(ENV.apiUrl + "api/Medical_Immunization_Student/GetStudent_Medical_Immunization").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.items = res.data;

                console.log(res.data);


            });


            $http.get(ENV.apiUrl + "api/common/Medical_Immunization/GetSims_MedicalImmunization").then(function (res) {
               
                $scope.cmbImmunization = res.data;

               

            });

            
            $scope.CmbimmunizationChange = function (str) {
                debugger

                var immunization_desc = $.grep($scope.cmbImmunization, function (fruit) {
                    return fruit.sims_immunization_code == str;
                })[0].sims_immunization_desc;


                var sims_immunization_age_group = $.grep($scope.cmbImmunization, function (fruit) {
                    return fruit.sims_immunization_code == str;
                })[0].sims_immunization_age_group_code;


                
                $scope.edt['sims_immunization_desc'] = immunization_desc;
                $scope.edt['sims_immunization_age_group_code'] = sims_immunization_age_group;

              //  $scope.edt = { sims_immunization_desc: fruitName }

            }
            $http.get(ENV.apiUrl + "api/common/Medical_Immunization/GetAllMedicalImmunizationAgeGroupCode").then(function (res) {

                $scope.cmbgroupCode = res.data;

                //console.log($scope.cmbVisit);


            });


            //$http.get(ENV.apiUrl + "api/common/MedicalExamination/GetAllMedicalExaminationName").then(function (res) {

            //    $scope.cmbExaminationName = res.data;

            //});

           

            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;

            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.items.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;
            };

            $scope.edit = function (str) {
                $scope.edt = str;
                $scope.display = true;
                $scope.grid = false;
                $scope.update1 = true;
                $scope.save1 = false;

                $scope.editflg = true;
                $scope.curChange(str.sims_timetable_cur);
                $scope.academicChange(str.sims_academic_year);
                $scope.gradeChange(str.sims_grade);
                //  $scope.edt = str;


            }


            $scope.Update = function (newEventForm) {
                if (newEventForm.$valid) {

                   

                  
                        var data = $scope.edt;
                        data.opr = 'U';
                       

                        $http.post(ENV.apiUrl + "api/Medical_Immunization_Student/InsertStudent_Medical_Immunization", data).then(function (res) {
                            $scope.ins = res.data;
                            if (res.data) {
                                swal({ title: "Alert", text: "Record Updated Successfully.", imageUrl: "assets/img/check.png", });
                                $http.get(ENV.apiUrl + "api/Medical_Immunization_Student/GetStudent_Medical_Immunization").then(function (res) {
                                    $scope.display = false;
                                    $scope.grid = true;
                                    $scope.items = res.data;
                                    $scope.currentPage = 0;
                                });

                            }
                            else
                                swal({ title: "Alert", text: "Record Not Updated .", imageUrl: "assets/img/notification-alert.png", });


                        });



                   
                }
            }

            $scope.Save = function (newEventForm) {

                if (newEventForm.$valid) {



                        var data = $scope.edt;
                        data.opr = 'I';
                        
                        $http.post(ENV.apiUrl + "api/Medical_Immunization_Student/InsertStudent_Medical_Immunization", data).then(function (res) {
                            $scope.ins = res.data;
                            if (res.data) {
                                swal({ title: "Alert", text: "Record Inserted Successfully.", imageUrl: "assets/img/check.png", });
                                $http.get(ENV.apiUrl + "api/Medical_Immunization_Student/GetStudent_Medical_Immunization").then(function (res) {
                                    $scope.display = false;
                                    $scope.grid = true;
                                    $scope.items = res.data;
                                    $scope.currentPage = 0;
                                });

                            }
                            else
                                swal({ title: "Alert", text: "Record Not Inserted.", imageUrl: "assets/img/notification-alert.png", });


                        });



                   
                }
            }


            $scope.New = function () {
                $scope.edt = "";
                $scope.grid = false;
                $scope.display = true;
                $scope.save1 = true;
                $scope.editflg = false;
                $scope.update1 = false;

                //$scope.edt.sims_reference_status = true;

            }


            var main, del = '', delvar = [];
            $scope.chk = function () {

                main = document.getElementById('chk_min');
                del = [];
                if (main.checked == true) {
                    for (var i = 0; i < $scope.items.length; i++) {
                        $scope.items[i].sims_immunization_code1 = true;


                    }
                }
                else {
                    for (var i = 0; i < $scope.items.length; i++) {
                        $scope.items[i].sims_immunization_code1 = false;

                        main.checked = false;

                        $scope.row1 = '';
                    }
                }


            }


            $scope.Delete = function () {
                var data = [];

                debugger
                del = '';

                for (var i = 0; i < $scope.items.length; i++) {
                    
                    if ($scope.items[i].sims_immunization_code1 == true) {


                       // data.push($scope.items[i]);
                        var data = $scope.items[i];
                        data.opr = 'D';

                        $http.post(ENV.apiUrl + "api/Medical_Immunization_Student/InsertStudent_Medical_Immunization", data).then(function (res) {
                            $scope.ins = res.data;
                           

                        });


                    }

                }

                if ($scope.ins) {
                    swal({ title: "Alert", text: "Record Inserted Successfully.", imageUrl: "assets/img/check.png", });
                    $http.get(ENV.apiUrl + "api/Medical_Immunization_Student/GetStudent_Medical_Immunization").then(function (res) {
                        $scope.display = false;
                        $scope.grid = true;
                        $scope.items = res.data;
                        $scope.currentPage = 0;
                    });

                }
                else
                    swal({ title: "Alert", text: "Record Not Inserted.", imageUrl: "assets/img/notification-alert.png", });


             

               
            }



            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.update1 = false;
                $scope.save1 = false;
                $scope.editflg = false;
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy',
               
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


        

        }]);

  

})();


