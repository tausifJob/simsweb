﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var file_doc = [];
    var file_temp_doc = [];
    var sims_doc_line = 1;
    var simsController = angular.module('sims.module.Assignment');
    simsController.controller('AddExamPaperCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.Update_btn = true;
            $scope.search_btn = false;
            $scope.display = true;
            $scope.table = true;
            $scope.exampaper = false;
            $scope.uploading_doc1 = true;
            $scope.del_btn = false;
            $scope.temp = {};
            $scope.temp.sims_exam_paper_status = true;
            //$scope.username = $rootScope.globals.currentUser.username;
            var user = $rootScope.globals.currentUser.username;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $(function () {
                $('#cmb_Section').multipleSelect({
                    width: '100%'
                });
            });
            $(function () {
                $('#cmb_Section2').multipleSelect({
                    width: '100%'
                });
            });



            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCurriculum) {

                $scope.Curriculum = getCurriculum.data;
                $scope.temp.sims_cur_code = $scope.Curriculum[0].sims_cur_code
                $scope.getacademicYear($scope.Curriculum[0].sims_cur_code);
            });

            $scope.getacademicYear = function (str) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (getAcademicYear) {
                    $scope.AcademicYear = getAcademicYear.data;

                    $scope.temp.sims_academic_year = $scope.AcademicYear[0].sims_academic_year
                    $scope.getGrade($scope.Curriculum[0].sims_cur_code, $scope.AcademicYear[0].sims_academic_year);

                })
            }

            $scope.getGrade = function (str1, str2) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str1 + "&academic_year=" + str2).then(function (getAllGrades) {
                    debugger
                    $scope.AllGrades = getAllGrades.data;
                    $http.get(ENV.apiUrl + "api/AddExamPaper/getExamCode?cur=" + str1 + "&year=" + str2).then(function (AllExam) {
                        $scope.Exam = AllExam.data;
                        console.log($scope.Exam);
                    });
                })


            }

            $scope.getsection = function (str, str1, str2) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + str + "&grade_code=" + str1 + "&academic_year=" + str2).then(function (Allsection) {
                    $scope.getSectionFromGrade = Allsection.data;
                    setTimeout(function () {
                        debugger;
                        $('#cmb_Section').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                    setTimeout(function () {
                        debugger;
                        $('#cmb_Section2').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                })
            };

            $scope.getsubject = function () {
                $http.get(ENV.apiUrl + "api/AddExamPaper/getSubject?cur=" + $scope.temp.sims_cur_code + "&aca=" + $scope.temp.sims_academic_year + "&grd=" + $scope.temp.sims_grade_code + "&section=" + $scope.temp.sims_section_code).then(function (getSubject) {
                    debugger;
                    $scope.subjectdata = getSubject.data;
                    console.log($scope.subjectdata);

                });
            }



            //var datasend = [];
            //$scope.savedata = function (Myform) {

            //    if (Myform) {
            //        var data = {
            //         sims_cur_code: $scope.temp.sims_cur_code
            //       , sims_doc_mod_code: $scope.temp.sims_doc_mod_code
            //       , sims_doc_code: $scope.temp.sims_doc_code
            //       , sims_doc_desc: $scope.temp.sims_doc_desc
            //       , sims_doc_create_date: $scope.temp.sims_doc_create_date
            //       , sims_doc_created_by: $scope.username
            //       , sims_doc_status: $scope.temp.sims_doc_status
            //       , opr: 'I'
            //        };
            //        datasend.push(data);

            //        $http.post(ENV.apiUrl + "api/DocumentMaster/CUDDocumentMaster", datasend).then(function (msg) {
            //            $scope.msg1 = msg.data;

            //            if ($scope.msg1 == true) {
            //                swal({ title: "Alert", text: "Record Inserted Successfully", showCloseButton: true, width: 380, });
            //            }
            //            else {
            //                swal({ title: "Alert", text: "Record Not Inserted", showCloseButton: true, width: 380, });
            //            }

            //            $http.get(ENV.apiUrl + "api/DocumentMaster/getAllDocumentMaster").then(function (res1) {
            //                $scope.obj1 = res1.data;
            //                $scope.totalItems = $scope.obj1.length;
            //                $scope.todos = $scope.obj1;
            //                $scope.makeTodos();
            //            });

            //        });
            //        datasend = [];
            //        $scope.table = true;
            //        $scope.display = false;
            //    }
            //}

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.file_doc = "";
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $state.go($state.current, {}, { reload: true });

            }

            $scope.edit = function (str) {

                $scope.curdisabled = true;
                $scope.docdisabled = true;
                $scope.readonly = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;

                console.log(str);
                $scope.temp = {
                    sims_cur_code: str.sims_doc_cur_code
                   , sims_doc_mod_code: str.sims_doc_mod_code
                   , sims_doc_code: str.sims_doc_code
                   , sims_doc_desc: str.sims_doc_desc
                   , sims_doc_create_date: str.sims_doc_create_date
                   , sims_doc_created_by: str.sims_doc_created_by
                   , sims_doc_status: str.sims_doc_status

                };
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };
            var file_name = '';
            var file_name_name = '';
            $scope.file_doc = [];

            $scope.file_changed = function (element) {
                debugger;
                file_name = '';
                file_name_name = '';
                var v = new Date();
                if ($scope.file_doc == undefined) {
                    $scope.file_doc = '';
                }
                if ($scope.temp.sims_cur_code == undefined) {
                    swal('', 'Please enter Curriculum')
                    return;
                }
                if ($scope.temp.sims_academic_year == undefined) {
                    swal('', 'Please enter Academic Year')
                    return;
                }
                if ($scope.temp.sims_grade_code == undefined) {
                    swal('', 'Please enter Grade')
                    return;
                }
                if ($scope.temp.sims_subject_code == undefined) {
                    swal('', 'Please enter Subject')
                    return;
                }
                if ($scope.temp.sims_exam_code == undefined) {
                    swal('', 'Please enter Examination')
                    return;
                }
                if ($scope.file_doc.length > 4) {
                    swal('', 'Upload maximum 5 files.');
                    return;
                }

                else {
                    var c = document.getElementById('Select7');
                    var grade = c.options[c.selectedIndex].text;
                    $scope.temp.sims_grade_name = grade.replace(/ /g, "");
                    var s = document.getElementById('Select3');
                    var subj = s.options[s.selectedIndex].text;
                    $scope.temp.sims_subject_name_en = subj.replace(/ /g, "");
                    file_name = 'ExamPaper' + '-' + $scope.temp.sims_academic_year + '_' + $scope.temp.sims_grade_name + '_' + $scope.temp.sims_subject_name_en;
                    $scope.photofile = element.files[0];
                    file_name_name = $scope.photofile.name;
                    var len = 0;
                    len = file_name_name.split('.');
                    var fortype = file_name_name.split('.')[len.length - 1];
                    var formatbool = false;
                    if (fortype == 'pdf' || fortype == 'doc' || fortype == 'docx') {
                        formatbool = true;
                    }

                    //for (var i = 0; i < $scope.GetTextFormat.length; i++) {
                    //    if ($scope.GetTextFormat[i].file_format == fortype)
                    //        formatbool = true;
                    //}
                    if (formatbool == true) {
                        if ($scope.photofile.size > 2097152) {
                            swal('', 'File size limit not exceed upto 2 MB.')
                        }
                        else {
                            $scope.uploading_doc1 = false;
                            $scope.photo_filename = ($scope.photofile.type);
                            console.log($scope.edt);

                            var reader = new FileReader();
                            reader.onload = function (e) {
                                $scope.$apply(function () {
                                    $scope.prev_img = e.target.result;
                                    var request = {
                                        method: 'POST',
                                        url: ENV.apiUrl + 'api/AddExamPaper/upload?filename=' + file_name_name + "&location=" + "Images/ExamPapers",
                                        data: formdata,
                                        headers: {
                                            'Content-Type': undefined
                                        }
                                    };
                                    $http(request).success(function (d) {

                                        debugger;
                                        var t = {
                                            sims508_doc: d,
                                            sims_doc_line: sims_doc_line,
                                            sims508_doc_name: file_name,
                                        }
                                        $scope.file_doc.push(t);
                                        sims_doc_line++;
                                        $scope.uploading_doc1 = true;
                                        //file_doc = [];
                                        //for (var i = 0; i < $scope.teacher_doc.length; i++) {
                                        //    var c = {
                                        //        sims509_doc: $scope.teacher_doc[i].sims508_doc,
                                        //        sims_doc_line: i + 1
                                        //    }
                                        //    file_doc.push(c);
                                        //}




                                        //$scope.maindata.file = d;


                                    });

                                });
                            };
                            reader.readAsDataURL($scope.photofile);
                        }
                    }
                    else {
                        swal('', '.' + fortype + ' File format not allowed.');
                    }
                }

            }

            $scope.downloaddoc1 = function (str) {
                debugger;
                //  $scope.url = "http://localhost:90/SIMSAPI/Content/Assignment/TeacherDoc/" + str;
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Assignment/TeacherDoc/' + str;
                window.open($scope.url);
            }

            $scope.downloaddoc2 = function (str) {
                debugger;
                //  $scope.url = "http://localhost:90/SIMSAPI/Content/Assignment/TeacherDoc/" + str;
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/ExamPapers/' + str;
                window.open($scope.url);
            }

            $scope.getOldPapers = function () {
                debugger;
                var section_code = [];
                var section = $scope.temp.sims_section_code;
                section_code = section_code + ',' + section;
                var sec = section_code.substr(section_code.indexOf(',') + 1);
                $http.get(ENV.apiUrl + "api/AddExamPaper/getOldPapers?cur=" + $scope.temp.sims_cur_code + "&aca=" + $scope.temp.sims_academic_year + "&grd=" + $scope.temp.sims_grade_code + "&sec=" + sec + "&exam=" + $scope.temp.sims_exam_code).then(function (AllPapers) {
                    $scope.file_attached = AllPapers.data;
                    //if ($scope.file_attached.length == 0) {
                    //    swal('', 'No Records Found');
                    //}
                    console.log($scope.file_attached);
                });
            }

            $scope.CancelFileUpload = function (idx) {
                $scope.images.splice(idx, 1);
                console.log($scope.images);
            };

            var sims_student_enrolls = [];
            $scope.saveUploadedFiles = function (isvalid) {

                if ($scope.file_doc.length == undefined || $scope.file_doc.length == "") {
                    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please select Atleast One File", showCloseButton: true, width: 380, });
                }

                else {
                    debugger;
                    var datasend = [];
                    if (isvalid) {


                        debugger

                        var data = {
                            sims_academic_year: $scope.edt.sims_academic_year,
                            sims_cur_code: $scope.edt.sims_cur_code,
                            sims_grade_code: $scope.edt.sims_grade_code,
                            sims_section_code: $scope.edt.sims_section_code,
                            sims_subject_Code: $scope.edt.sims_subject_code,

                            //sims_bell_teacher_code: $rootScope.globals.currentUser.username,
                            //sims_assignment_title: $scope.edt.sims_assignment_title,
                            //sims_assignment_desc: $scope.edt.sims_assignment_desc,
                            //sims_assignment_start_date: $scope.edt.sims_assignment_start_date,
                            //sims_assignment_submission_date: $scope.edt.sims_assignment_submission_date,
                            //sims_assignment_freeze_date: $scope.edt.sims_assignment_freeze_date,
                            //sims_assignment_status: $scope.edt.sims_assignment_status,
                            //sims_term_code: $scope.edt.sims_term_code,

                        }
                        datasend.push(data);
                        $http.post(ENV.apiUrl + "api/AddExamPaper/insert_Assignment", datasend).then(function (res) {
                            $scope.AssignmentData = res.data;
                            if ($scope.AssignmentData == true) {
                                debugger;
                                $scope.Insert_Sims_assignment_doc();
                            }
                            else {
                                swal('', 'Assignment Not Created');
                            }

                        });

                    }

                }
            }

            $scope.savedata = function (Myform) {
                debugger;
                var datasend = [];

                var section_code = [];


                var section = $scope.temp.sims_section_code;
                section_code = section_code + ',' + section;
                var sec = section_code.substr(section_code.indexOf(',') + 1);
                if (Myform) {

                    if ($scope.file_doc.length > 0) {

                        for (var j = 0 ; j < $scope.file_doc.length; j++) {
                            var data = {
                                sims_cur_code: $scope.temp.sims_cur_code,
                                sims_academic_year: $scope.temp.sims_academic_year,
                                sims_grade_code: $scope.temp.sims_grade_code,
                                sims_section_code: sec,
                                sims_subject_code: $scope.temp.sims_subject_code,
                                sims_exam_code: $scope.temp.sims_exam_code,
                                sims_doc_desc: $scope.temp.sims_doc_desc,
                                sims_exam_paper_status: $scope.temp.sims_exam_paper_status,
                                sims_exam_paper_path: $scope.file_doc[j].sims508_doc,
                                sims_exam_paper_display_name: $scope.file_doc[j].sims508_doc_name,
                                sims_exam_paper_created_by: user,
                                trans_type: 'I',

                            }
                            datasend.push(data);
                        }


                        $http.post(ENV.apiUrl + "api/AddExamPaper/insert_Assignment", datasend).then(function (res) {
                            $scope.AssignmentDocData = res.data;
                            if ($scope.AssignmentDocData == true) {
                                swal('', 'Paper Uploaded Successfully!!');
                                $scope.Cancel();
                            }
                            else {
                                swal('', 'Paper Not Uploaded');
                                //$scope.Cancel();
                            }
                        });
                    }
                }
            }


            simsController.directive('ngFiles', ['$parse', function ($parse) {

                function fn_link(scope, element, attrs) {
                    var onChange = $parse(attrs.ngFiles);
                    element.on('change', function (event) {
                        onChange(scope, { $files: event.target.files });
                    });
                };

                return {
                    link: fn_link
                }
            }])

            $scope.deletedoc = function (str) {
                debugger;
                file_temp_doc = [];
                //for (var i = 0; i < $scope.file_doc.length; i++) {
                //    if ($scope.file_doc[i].sims_doc_line == str.sims_doc_line && $scope.file_doc[i].sims508_doc == str.sims508_doc) {
                //        $scope.file_doc.splice(i, 1);
                //        sims_doc_line--;
                //        sims_doc_line: i--;
                //    }
                //}

                for (var i = 0; i < $scope.file_doc.length; i++) {
                    if ($scope.file_doc[i].sims_doc_line != str.sims_doc_line) {
                        var x = {
                            sims508_doc: $scope.file_doc[i].sims508_doc,
                            sims508_doc_name: $scope.file_doc[i].sims508_doc_name,
                        }
                        file_temp_doc.push(x);
                    }
                }
                $scope.file_doc = [];
                console.log(file_temp_doc);
                var j = 1;
                for (var i = 0; i < file_temp_doc.length; i++) {
                    var c = {
                        sims508_doc: file_temp_doc[i].sims508_doc,
                        sims508_doc_name: file_temp_doc[i].sims508_doc_name,
                        sims_doc_line: i + 1
                    }
                    $scope.file_doc.push(c);
                    //sims_doc_line = i + 1;
                    j++;
                }
                sims_doc_line = j;
                console.log(file_doc);


            }

            $scope.show = function () {
                $('#myModal1').modal('show');
                $scope.search_btn = true;
                $scope.exampaper = false;
                $scope.temp = {};
                $scope.papers = [];
                $scope.del_btn = false;
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCurriculum) {

                    $scope.Curriculum = getCurriculum.data;
                    $scope.temp.sims_cur_code = $scope.Curriculum[0].sims_cur_code
                    $scope.getacademicYear($scope.Curriculum[0].sims_cur_code);
                });
            }

            $scope.search = function () {
                debugger
                var section_code = [];
                $scope.exampaper = true;
                $scope.del_btn = true;
                var section = $scope.temp.sims_section_code;
                section_code = section_code + ',' + section;
                var sec = section_code.substr(section_code.indexOf(',') + 1);
                $http.get(ENV.apiUrl + "api/AddExamPaper/getAllPapers?cur=" + $scope.temp.sims_cur_code + "&aca=" + $scope.temp.sims_academic_year + "&grd=" + $scope.temp.sims_grade_code + "&sec=" + sec + "&exam=" + $scope.temp.sims_exam_code).then(function (AllPapers) {
                    $scope.papers = AllPapers.data;
                    if ($scope.papers.length == 0) {
                        swal('', 'No Records Found');
                    }
                    console.log($scope.papers);
                });
            }

            $scope.submit = function () {
                debugger
                for (var i = 0; i < $scope.papers.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_cur_code': $scope.temp.sims_cur_code,
                            'sims_academic_year': $scope.temp.sims_academic_year,
                            'sims_grade_code': $scope.papers[i].sims_grade_code,
                            'sims_section_code': $scope.papers[i].sims_section_code,
                            'sims_exam_code': $scope.papers[i].sims_exam_code,
                            'sims_exam_paper_srl_no': $scope.papers[i].sims_exam_paper_srl_no,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                $http.post(ENV.apiUrl + "api/AddExamPaper/Inactivate", deletefin).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Papers Inactivated Successfully", showCloseButton: true, width: 380, })
                        $('#myModal1').modal('hide');
                        $scope.Cancel();
                    }
                });

            }

            $scope.Close = function () {
                $scope.temp = "";
                $scope.temp = {};
                $scope.temp.sims_exam_paper_status = true;
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCurriculum) {

                    $scope.Curriculum = getCurriculum.data;
                    $scope.temp.sims_cur_code = $scope.Curriculum[0].sims_cur_code
                    $scope.getacademicYear($scope.Curriculum[0].sims_cur_code);
                });
            }


            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.papers.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.papers.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function (info) {

                for (var i = 0; i < $scope.papers.length; i++) {
                    var d = document.getElementById(i);
                    if (d.checked == true) {
                        $scope.papers[i].ischange = true;
                    }
                    else {
                        $scope.papers[i].ischange = false;
                    }
                }

                $("input[type='radio']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        //$scope.color = '#edefef';
                    }
                });

                //main = document.getElementById('$index');
                //if (main.checked == true) {

                //    main.checked = false;
                //    $scope.color = '#edefef';
                //    $scope.row1 = '';
                //}
            }

        }])

})();