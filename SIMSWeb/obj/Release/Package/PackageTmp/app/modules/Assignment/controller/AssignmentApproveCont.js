﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Assignment');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('AssignmentApproveCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'];
            $scope.itemsPerPage = '5';
            $scope.currentPage = 0;
            $scope.view = true;
            $scope.create = false;
            $scope.edt = {};
            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            var username = $rootScope.globals.currentUser.username;


            $scope.gradeDetails = [];
            $scope.sectionDetails = [];

            $http.post(ENV.apiUrl + "api/AssingmentApproval/cur").then(function (cur) {
                $scope.cur = cur.data;
                debugger;

                $scope.edt = { sims_cur_code: $scope.cur[0].sims_cur_code };
                $scope.selectcur($scope.edt.sims_cur_code);
            });

            $scope.selectcur = function (str) {
                debugger;
                $http.post(ENV.apiUrl + "api/AssingmentApproval/aca?cur=" + str).then(function (aca) {
                    $scope.aca = aca.data;
                    $scope.edt['sims_academic_year'] = $scope.aca[0].sims_academic_year;
                    $scope.selectaca($scope.aca[0].sims_academic_year)
             
                });
            }

            $scope.selectaca = function (str) {

                var data = {
                    sims_cur_code:$scope.edt.sims_cur_code,
                    sims_assignment_teacher_code: username,
                    sims_academic_year: str
                }

                $http.post(ENV.apiUrl + "api/AssingmentApproval/grade", data).then(function (grade) {
                    $scope.grade = grade.data;
                });
            }

            $scope.selectgrd = function (str) {

                var data = {
                    sims_cur_code: $scope.edt.sims_cur_code,
                    sims_academic_year: $scope.edt.sims_academic_year,
                    sims_assignment_teacher_code: username,
                    sims_grade_code: str
                }

                $http.post(ENV.apiUrl + "api/AssingmentApproval/section", data).then(function (section) {
                    $scope.section = section.data;
                });
            }

            $scope.selectsec = function (str) {

                var data = {
                    sims_cur_code: $scope.edt.sims_cur_code,
                    sims_academic_year: $scope.edt.sims_academic_year,
                    sims_grade_code: $scope.edt.sims_grade_code,
                    sims_assignment_teacher_code: username,
                    sims_section_code: str
                }

                $http.post(ENV.apiUrl + "api/AssingmentApproval/subject", data).then(function (subject) {
                    $scope.subject = subject.data;
                });
            }


            

          

            $scope.link = function (str) {
                //$scope.fileUrl = "api.mograsys.com/ppapi/Content/" + $http.defaults.headers.common['schoolId'] + "/Docs/Attachment/" + str;
                console.log($rootScope.globals.currentSchool.lic_website_url + "/images/CircularFiles/" + str);
                window.open(ENV.apiUrl + "/Content/" + $http.defaults.headers.common['schoolId'] + "/Assignment/TeacherDoc/" + str, "_new");
            }


            //var date = new Date();
            //var month = (date.getMonth() + 1);
            //var day = date.getDate();
            //if (month < 10)
            //    month = "0" + month;
            //if (day < 10)
            //    day = "0" + day;
            //$scope.startDate = date.getFullYear() + '-' + (month) + '-' + (day);
            //$scope.endDate = date.getFullYear() + '-' + (month) + '-' + (day);

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.setStart = function (date) {
                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                var date1 = year + "-" + month + "-" + day;
                $scope.startDate = date1;

            }
            $scope.setEnd = function (date) {
                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                var date1 = year + "-" + month + "-" + day;
                $scope.endDate = date1;
            }

            $scope.size = function (pagesize) {
                $scope.itemsPerPage = pagesize;
            }

            $scope.reset_details = function () {
                $scope.edt = {};
                $scope.maindata = [];
                $scope.rgvtbl = false;
		$scope.startDate='';
		$scope.endDate = '';
		$http.post(ENV.apiUrl + "api/AssingmentApproval/cur").then(function (cur) {
		    $scope.cur = cur.data;
		    debugger;

		    $scope.edt = { sims_cur_code: $scope.cur[0].sims_cur_code };
		    $scope.selectcur($scope.edt.sims_cur_code);
		});


            }



            $scope.show_details = function () {
                if ($scope.edt.sims_grade_code == undefined)
                    $scope.edt.sims_grade_code = '';
                if ($scope.edt.sims_section_code == undefined)
                    $scope.edt.sims_section_code = '';
                if ($scope.edt.startDate == undefined)
                    $scope.edt.startDate = '';
                if ($scope.edt.endDate == undefined)
                    $scope.edt.endDate = '';


                var data = {
                    sims_cur_code:$scope.edt.sims_cur_code,
                    sims_academic_year:$scope.edt.sims_academic_year,
                    sims_grade_code:$scope.edt.sims_grade_code,
                    sims_section_code:$scope.edt.sims_section_code,
                    sims_subject_code: $scope.edt.sims_subject_code,
                    sims_assignment_start_date: $scope.edt.startDate,
                    sims_assignment_freeze_date: $scope.edt.endDate
                }

                $http.post(ENV.apiUrl + "api/AssingmentApproval/AssignmentDetails", data).then(function (AssignmentDetails) {
                    $scope.AssignmentDetails = AssignmentDetails.data;

                    if ($scope.AssignmentDetails.length > 0) {
                        $scope.rgvtbl = true;
                    }
                    else {
                        $scope.rgvtbl = false;
                        swal('', 'No any records found.');
                    }


                });
            }

            //Search

            $scope.maindata = [];
            $scope.row_click = function (str) {
                debugger;
                $scope.reject_flag = false;
                $scope.btn_disable = false;
                $scope.maindata = str;
                console.log($scope.maindata);
              
                $http.post(ENV.apiUrl + "api/AssingmentApproval/StudentAssignment?assign_no=" + $scope.maindata.sims_assignment_number).then(function (StudentAssignment) {
                    $scope.StudentAssignment = StudentAssignment.data;
                });
                $('#myModal').modal('show');
            }

            $scope.Assignment_approve = function (str) {
                debugger;
                var data = {
                    sims_assignment_status: 'A',
                    sims_assignment_number: str,
                    reject_remark: ''
                }

                $http.post(ENV.apiUrl + "api/AssingmentApproval/ApproveRejectAssignment", data).then(function (ApproveRejectAssignment) {
                    $scope.ApproveRejectAssignment = ApproveRejectAssignment.data;
                    if ($scope.ApproveRejectAssignment) {
                        swal('', 'Assignment Approved successfully.');
                    }
                    $('#myModal').modal('hide');

                    if ($scope.edt.sims_grade_code == undefined)
                        $scope.edt.sims_grade_code = '';
                    if ($scope.edt.sims_section_code == undefined)
                        $scope.edt.sims_section_code = '';
                    if ($scope.edt.startDate == undefined)
                        $scope.edt.startDate = '';
                    if ($scope.edt.endDate == undefined)
                        $scope.edt.endDate = '';


                    var data = {
                        sims_cur_code: $scope.edt.sims_cur_code,
                        sims_academic_year: $scope.edt.sims_academic_year,
                        sims_grade_code: $scope.edt.sims_grade_code,
                        sims_section_code: $scope.edt.sims_section_code,
                        sims_subject_code: $scope.edt.sims_subject_code,
                        sims_assignment_start_date: $scope.edt.startDate,
                        sims_assignment_freeze_date: $scope.edt.endDate
                    }

                    $http.post(ENV.apiUrl + "api/AssingmentApproval/AssignmentDetails", data).then(function (AssignmentDetails) {
                        $scope.AssignmentDetails = AssignmentDetails.data;

                        if ($scope.AssignmentDetails.length > 0) {
                            $scope.rgvtbl = true;
                        }
                        else {
                            $scope.rgvtbl = false;
                        }


                    });
                });
            }

            $scope.Assignment_reject = function () {
                debugger;
                $scope.reject_flag = true;
                $scope.btn_disable = true;
            }

            $scope.Reject_btn = function (str) {
                debugger;
                var data = {
                    sims_assignment_status: 'R',
                    sims_assignment_number: str,
                    reject_remark: $scope.Reject_resion
                }

                $http.post(ENV.apiUrl + "api/AssingmentApproval/ApproveRejectAssignment", data).then(function (ApproveRejectAssignment) {
                    $scope.ApproveRejectAssignment = ApproveRejectAssignment.data;
                    if ($scope.ApproveRejectAssignment) {
                        swal('', 'Assignment Rejected.');
                    }
                    $('#myModal').modal('hide');

                    if ($scope.edt.sims_grade_code == undefined)
                        $scope.edt.sims_grade_code = '';
                    if ($scope.edt.sims_section_code == undefined)
                        $scope.edt.sims_section_code = '';
                    if ($scope.edt.startDate == undefined)
                        $scope.edt.startDate = '';
                    if ($scope.edt.endDate == undefined)
                        $scope.edt.endDate = '';


                    var data = {
                        sims_cur_code: $scope.edt.sims_cur_code,
                        sims_academic_year: $scope.edt.sims_academic_year,
                        sims_grade_code: $scope.edt.sims_grade_code,
                        sims_section_code: $scope.edt.sims_section_code,
                        sims_subject_code: $scope.edt.sims_subject_code,
                        sims_assignment_start_date: $scope.edt.startDate,
                        sims_assignment_freeze_date: $scope.edt.endDate
                    }

                    $http.post(ENV.apiUrl + "api/AssingmentApproval/AssignmentDetails", data).then(function (AssignmentDetails) {
                        $scope.AssignmentDetails = AssignmentDetails.data;

                        if ($scope.AssignmentDetails.length > 0) {
                            $scope.rgvtbl = true;
                        }
                        else {
                            $scope.rgvtbl = false;
                        }


                    });
                });
            }

            $scope.leave_cancel = function () {
                $('#myModal').modal('hide');
            }



        }])
})();