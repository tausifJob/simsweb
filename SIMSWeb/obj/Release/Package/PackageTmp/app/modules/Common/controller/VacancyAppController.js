﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Common');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('VacancyApplController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
          // Pre-Required Functions and Variables
   $('*[data-datepicker="true"] input[type="text"]').datepicker(
                {
                    todayBtn: true,
                    orientation: "top left",
                    autoclose: true,
                    todayHighlight: true,
                    format: 'yyyy-mm-dd'
                });
         
                $scope.temp = {};
                $scope.newapp = false;
               
                $scope.temp.qual_details = [];
                $http.get(ENV.apiUrl + "api/Fee/SFS/getvacancyEmpData").then(function (res) {
                   $scope.obj = res.data;
                   console.log($scope.obj);
                   $scope.operation = true;
               });
                $http.get(ENV.apiUrl + "api/Fee/SFS/getvacancy").then(function (res) {
                    $scope.vacancyobj = res.data;
                });

                $http.get(ENV.apiUrl + "api/Fee/SFS/getqualificationLevels").then(function (qlevel) {
                    $scope.qlevel = qlevel.data;
                   
                });
                $scope.getQuali = function (level) {
                    $http.get(ENV.apiUrl + "api/Fee/SFS/getqualification?level_code=" + level).then(function (qual) {
                        $scope.qual = qual.data;
                    });
                }
                $scope.onVacancyChange = function (sal) 
                {
                    for(var x=0;x<$scope.vacancyobj.length;x++)
                    {
                        console.log($scope.vacancyobj[x]);
                        if ($scope.vacancyobj[x].sims_vacancy_code == sal) {
                            $scope.temp['em_Designation_Code'] = $scope.vacancyobj[x].em_Designation_Code;
                            break;
                        }
                           //$scope.obj.em_Designation_Code
                    }
                }
         
                $scope.reset = function () {
                    $scope.Myform.$setPristine();
                    $scope.Myform.$setUntouched();
                    $scope.temp = {};
                    $scope.prev_img = '';
                }

                $scope.newapplication = function () {
                    $scope.appicationID = '';
                    $scope.appicationPwd = '';
                    $scope.Myform.$setPristine();
                    $scope.Myform.$setUntouched();
                    $scope.newapp = false;
                    file_name = '';
                    file_name_doc = '';
                    file_name_en = '';
                    file_name_en_doc = '';
                    file_doc = [];
                    $state.go('main.vacancy');
                }
                $scope.createapplication = function (Myform) {
                    $scope.appicationID = '';
                    $scope.appicationPwd = '';
                    if (Myform) {
                        console.log($scope.temp);
                        $http.post(ENV.apiUrl + "api/Fee/SFS/vacancyapp", $scope.temp).then(function (res) {
                            console.log('RESULT', res.data);
                            if (res.data != '') {
                                $scope.newapp = true;
                                var xx = res.data.split('/');
                                $scope.appicationID = xx[0];
                                $scope.appicationPwd = xx[1];
                                $scope.temp = {};
                                $scope.prev_img = '';
                            }
                        });
                    }
                }
                
                $scope.removequal = function (info, $index) {
                    $scope.temp.qual_details.splice($index, 1);
                }
                $scope.AddQualification = function () {

                    $scope.obj = { 'pays_qualification_level': $scope.temp.pays_qualification_level, 'pays_qualification_code': $scope.temp.pays_qualification_code, 'pays_qualification_level_name': $scope.temp.pays_qualification_level_name, 'pays_qualification_name': $scope.temp.pays_qualification_name, 'pays_qualification_year': $scope.temp.pays_qualification_year, 'pays_qualification_remark': $scope.temp.pays_qualification_remark }
                    if ($scope.obj.pays_qualification_level == '' || $scope.obj.pays_qualification_level == undefined || $scope.obj.pays_qualification_code == '' || $scope.obj.pays_qualification_code == undefined || $scope.obj.pays_qualification_year == '' || $scope.obj.pays_qualification_year == undefined) {
                        swal({
                            text: 'Please Enter all the Details.',
                            width: 400,
                            height: 300
                        });
                    }
                    else {
                        for (var x = 0; x < $scope.qlevel.length; x++) {
                            if ($scope.obj.pays_qualification_level == $scope.qlevel[x].pays_qualification_level)
                                $scope.obj.pays_qualification_level_name = $scope.qlevel[x].pays_qualification_level_name;
                        }
                        for (var x = 0; x < $scope.qual.length; x++) {
                            if ($scope.obj.pays_qualification_code == $scope.qual[x].pays_qualification_code)
                                $scope.obj.pays_qualification_name = $scope.qual[x].pays_qualification_name;
                        }
                        $scope.temp['pays_qualification_level'] = '';
                        $scope.temp['pays_qualification_code'] = '';
                        $scope.temp['pays_qualification_year'] = '';
                        $scope.temp['pays_qualification_remark'] = '';
                        $scope.temp.qual_details.push($scope.obj);
                        $scope.obj = {};
                    }
                }


            /* EMPLOYEE PHOTO UPLOAD*/

            
            var formdata = new FormData();
            var file_name = '';
            var file_name_doc = '';
            var file_name_en = '';
            var file_name_en_doc = '';
            var file_doc = [];


            $scope.getTheFiles = function ($files) {
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };
            
            $scope.file_changed = function (element) {
                file_name = '';
                var v = new Date();
                file_name = 'vancancy_' + v.getDay() + '_' + v.getMonth() + '_' + v.getYear() + '_' + v.getHours() + '_' + v.getMonth() + '_' + v.getSeconds();
                $scope.photofile = element.files[0];
                file_name_en = '';
                file_name_en = $scope.photofile.name;
                var fortype = $scope.photofile.type.split("/")[1];
                if (fortype == 'png' || fortype == 'jpeg' || fortype == 'jpg') {

                    if ($scope.photofile.size > 200000) {
                        swal('', 'File size limit not exceed upto 200kb.')
                    }
                    else {
                        $scope.photo_filename = ($scope.photofile.type);
                        var dbfilename = '';
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $scope.$apply(function () {
                                $scope.prev_img = e.target.result;
                                var request = {
                                    method: 'POST',
                                    url: ENV.apiUrl + 'api/Fee/SFS/upload?filename=' + file_name + '.' + fortype + "&location=" + "Images/EmployeeImages/",
                                    data: formdata,
                                    headers: {
                                        'Content-Type': undefined
                                    }
                                };

                                $http(request).success(function (d) {
                                    $scope.temp.em_img = d;
                                });
                                //  $scope.temp.em_img = dbfilename;

                            });
                        };
                        reader.readAsDataURL($scope.photofile);
                        //$scope.temp.em_img = $scope.file_name_en;
                    }
                }
                else {
                    swal('', 'Only Image Type Files are allowed.');
                }
            }
            $scope.getTheFiles_doc = function ($files) {
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };


            $scope.file_changed_doc = function (element) {
                file_name_doc = '';
                var v = new Date();
                file_name_doc = 'vancancy_resume_' + v.getDay() + '_' + v.getMonth() + '_' + v.getYear() + '_' + v.getHours() + '_' + v.getMonth() + '_' + v.getSeconds();
                $scope.photofile_doc = element.files[0];
                file_name_en_doc = '';
                file_name_en_doc = $scope.photofile_doc.name;
                var fortype = $scope.photofile_doc.type.split("/")[1];
                if (fortype == 'pdf' ) {

                    if ($scope.photofile_doc.size > 200000) {
                        swal('', 'File size limit not exceed upto 200kb.')
                    }
                    else {
                        $scope.photo_filename_doc = ($scope.photofile_doc.type);
                        var dbfilename = '';
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $scope.$apply(function () {
                                $scope.prev_doc = e.target.result;
                                var request = {
                                    method: 'POST',
                                    url: ENV.apiUrl + 'api/Fee/SFS/upload?filename=' + file_name_doc + '.' + fortype + "&location=" + "Docs/",
                                    data: formdata,
                                    headers: {
                                        'Content-Type': undefined
                                    }
                                };

                                $http(request).success(function (d) {
                                    $scope.temp.em_doc = d;
                                });
                                //  $scope.temp.em_img = dbfilename;

                            });
                        };
                        reader.readAsDataURL($scope.photofile_doc);
                        //$scope.temp.em_img = $scope.file_name_en;
                    }


                }
                else {
                    swal('', 'Only Pdf Type Files are allowed.');
                }
            }

        //Events End
        }])

    simsController.directive('ngFiles', ['$parse', function ($parse) {
        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }]);
})();



