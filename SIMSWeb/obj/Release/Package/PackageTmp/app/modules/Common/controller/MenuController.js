﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Common');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('MenuController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$location', '$anchorScroll', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $location, $anchorScroll) {
            // Pre-Required Functions and Variables
            // Start

            $scope.url = $state.current.url;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1, 'z-index': 1 });
            }, 100);

            var user = $rootScope.globals.currentUser.username;
            $scope.lagn = true;
          
            console.log($rootScope.module);
            $scope.$on('lagnuage_chaged', function (str) {
                debugger;
                $scope.lagn = gettextCatalog.currentLanguage == "en" ? true : false;
                $scope.module_name = gettextCatalog.currentLanguage == "en" ? $rootScope.module.module_name_en : $rootScope.module.module_name_ar;
                $scope.module_help = $rootScope.module.module_desc;
                });

            $http.get(ENV.apiUrl + "api/common/GetMenu?user_code=" + user + "&module_code=" + $rootScope.module.module_code).then(function (menuData) {
                $scope.menuData = menuData.data;
                $scope.tags = $scope.menuData[0];
                $scope.applications = $scope.menuData[1];
            });

            $scope.$on('module-chage', function () {
              
                $scope.lagn = gettextCatalog.currentLanguage == "en" ? true : false;
                $scope.module_name = gettextCatalog.currentLanguage == "en" ? $rootScope.module.module_name_en : $rootScope.module.module_name_ar;
                $scope.module_help = $rootScope.module.module_desc;
                $http.get(ENV.apiUrl + "api/common/GetMenu?user_code=" + user + "&module_code=" + $rootScope.module.module_code).then(function (menuData) {
                    $scope.menuData = menuData.data;
                    $scope.tags = $scope.menuData[0];
                    $scope.applications = $scope.menuData[1];
                });
            })
            $scope.gotoapp = function (app) {
                var user = $rootScope.globals.currentUser.username;
                if (app.comn_appl_type == 'R') {
                    $rootScope.location = app.comn_appl_location;
                    setTimeout(function () { $state.go('main.ReportCard'); }, 100);
                    $state.go('main');
                }
                else {

                    if ($scope.fin == undefined) {

                        if (app.comn_appl_mod_code == '005') {
                            $http.get(ENV.apiUrl + "api/PDCSubmission/GetComp_Name?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                                $scope.cmbCompany = res.data;
                                if (res.data.length == 1) {
                                    $scope.companyName = res.data[0].comp_name;

                                    $scope.finnComp = res.data[0].comp_code;

                                    $http.get(ENV.apiUrl + "api/PDCSubmission/GetFinancial_year?comp_code=" + $scope.finnComp).then(function (res) {

                                        if (res.data.length > 0) {

                                            console.log('Finn');

                                            var data = {
                                                year: res.data[0].financial_year,
                                                company: $scope.finnComp,
                                                companyame: $scope.companyName
                                            }
                                            window.localStorage["Finn_comp"] = JSON.stringify(data)


                                            // $.cookie("finnComapny", $scope.finnComp);
                                            // window.localStorage("finnComapny", $scope.finnComp);

                                            //$.cookie("finnYear", res.data[0].financial_year);

                                            //if ($.cookie("finnComapny") != null) {

                                            //    console.log($.cookie("finnComapny"));
                                            //}

                                        }
                                    });

                                    //  AuthenticationService.SetCompany(res.data[0].comp_code, '2017');



                                }
                                else if (res.data.length > 1) {
                                    $scope.financeWindow();
                                }


                            });
                        }

                    }

                    if ($http.defaults.headers.common['schoolId'] == 'imert') {
                        if (app.comn_user_appl_code == 'Lms001' || app.comn_user_appl_code == '2') {
                            window.open('http://imert.learntron.net/learntron/index.html', "_new");

                        }
                    }

                    if ($http.defaults.headers.common['schoolId'] == 'gsis') {
                        if (app.comn_user_appl_code == 'Lms001')   //Online Access
                        {
                            window.open('http://lms.mograsys.com/mdlgsis/', "_new");
                        }
                    }

                    if ($http.defaults.headers.common['schoolId'] == 'adis') {
                        if (app.comn_user_appl_code == '2')   //Online Access
                        {
                            window.open('http://oa.mograsys.com/adis/OnlineAdmission.html', "_new");
                        }
                    }

                    if ($http.defaults.headers.common['schoolId'] == 'adisw') {


                        if (app.comn_user_appl_code == 'Lms001') {

                            $http.get(ENV.apiUrl + "api/common/getUserDetails_pass?uname=" + $rootScope.globals.currentUser.username).then(function (res) {
                                // $scope.obj = res.data;
                                debugger
                                console.log(res.data.table[0].comn_user_password);
                                window.open("http://lms.mograsys.com/mdladisw/autologin.php" + "?u=" + $rootScope.globals.currentUser.username + "&p=" + res.data.table[0].comn_user_password, "_new");
                                //  $state.go('main.dashboard');

                                //$('#load').show();

                                //$scope.uri = "http://www.google.com/";
                                //$scope.url = encodeURI($scope.uri);
                                //$("#load").attr("src", $sce.trustAsResourceUrl($scope.url));

                            });


                        }
                    }

                    if (app.comn_user_appl_code == 'gblsea') {

                        $scope.searchGlobalClick();


                    }

                    $state.go('main.' + app.comn_user_appl_code);

                   
                }
            }
            //Events End
        }])

})();
