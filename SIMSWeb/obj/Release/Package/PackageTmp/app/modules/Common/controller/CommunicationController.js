﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Common');
    simsController.controller('CommunicationController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$location', '$anchorScroll', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $location, $anchorScroll) {
            // Pre-Required Functions and Variables
            $scope.url = $state.current.url;
            $scope.current_subject = [];
            $scope.current_subject_group = [];
            $scope.from_user = $rootScope.globals.currentUser.username;//P10019
            $scope.to_user = '';//G04512
            $scope.to_user_dispaly = '';
            $scope.show_chat = true;
            $scope.show_users = true;
            $scope.sendofenter = true;
            $scope.show_chat1 = false;
            $scope.show_replay = false;
            $scope.show_forward = false;
            $scope.new_users = false;
            $scope.users_cc = false;
            $scope.newuser = false;

            $scope.new_user_cc = [];

            $scope.user_list_archived = [];
            /*Search*/

            $timeout(function () {
                $("#menu-wrapper1").scrollbar();
                $("#scroll-wrapper").css({ 'height': '480px' });
                $("#fixhead").tableHeadFixer({ 'top': 1 });
                $("#gradesection").tableHeadFixer({ 'top': 1 });

            }, 100);

            $rootScope.visible_stud = true;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = true;
            $rootScope.visible_User = true;
            $rootScope.visible_Employee = true;
            $rootScope.chkMulti = true;
            $scope.allsubjects = 1;
            $scope.opensubjects = 0;
            $scope.closedsubjects = 0;

            $scope.isActiveSubject = false;

            $scope.filter = function (val) {
                if (val == 0)
                    $scope.filtertodo = '';
                else if (val == 1)
                    $scope.filtertodo = 'A';
                else if (val == 2) {
                    $scope.filtertodo = 'I';
                }
            }

            $http.get(ENV.apiUrl + "api/Communication/GetAllPortalCommunication?user_code_from=" + $scope.from_user).then(function (portalnew_communication) {
                $scope.portalnew_communication = portalnew_communication.data;

                console.log($scope.portalnew_communication);
                $scope.show_chat = false;
                $scope.show_forward = false;

            });

            $scope.getSubjects = function () {
                $scope.allsubjects = 1;
                $scope.opensubjects = 0;
                $scope.closedsubjects = 0;
                $scope.filtertodo = '';
                $http.get(ENV.apiUrl + "api/Communication/GetPortalCommunication?user_code_from=" + $scope.from_user + "&user_code_to=" + $scope.to_user).then(function (portal_communication) {
                    $scope.portal_communication = portal_communication.data;
                    if ($scope.portal_communication.length > 0) {
                        $scope.getDetails($scope.portal_communication[0]);
                        //$scope.subject = $scope.portal_communication[0].sims_subject_name;
                    }
                });
            }

            $scope.getUsers = function () {
                $http.get(ENV.apiUrl + "api/Communication/getusersList?user_name=" + $scope.from_user).then(function (user_list) {
                    $scope.user_list = user_list.data;
                });
            }

            $scope.getUsers();
            $scope.message_body = '';
            $scope.subject = '';

            $scope.getChat = function (pc) {
                $scope.to_user = pc.sims_comm_recepient_id;
                $scope.to_user_dispaly = pc.sims_comm_user_name;

                $scope.show_chat = true;
                $scope.show_users = false;
                $scope.getSubjects();
                $scope.allsubjects = 1;
            }

            $scope.home = function () {
                $scope.show_chat = false;
                $scope.show_users = true;
                $scope.show_chat1 = false;
                $scope.show_replay = false;
                $scope.show_forward = false;

            }

            $scope.create_chat = function () {
                $scope.user_list_archived = [];
                $scope.user_list_archivedcc = [];
                $scope.new_users = '';
                $scope.new_user_cc = '';
                $scope.new_subject = '';
                $scope.new_message = '';

                $('#MyModal').modal({ backdrop: 'static', keyboard: true });
            }

            $scope.create_chat_shortCut = function (pc) {
                //sims_comm_user_name
                //sims_comm_recepient_id
                //$scope.list.sims_comm_user_name = value.name;
                //$scope.list.sims_comm_recepient_id = value.user_name;
                $scope.user_list_archived = [];
                $scope.user_list_archived.push(pc);
                $scope.new_subject = '';
                $scope.new_message = '';
                $('#MyModal').modal({ backdrop: 'static', keyboard: true });
            }

            $scope.getDetails = function (pc) {

                $scope.current_subject_group = pc;
                $scope.subject = pc.sims_subject_name;
                $scope.allsubjects = 1;

                $http.get(ENV.apiUrl + "api/Communication/GetPortalCommunicationHistory?user_code_from=" + pc.sims_comm_sender_id + "&user_code_to=" + pc.sims_comm_recepient_id + "&sims_subject_id=" + pc.sims_subject_id + "&comm_number=" + pc.sims_comn_tran_no).then(function (portal_communication_details) {
                    $scope.portal_communication_details = portal_communication_details.data;
                    pc.comnn_histr = $scope.portal_communication_details;
                    $scope.current_subject = pc.comnn_histr;
                    $scope.current_date = pc.sims_communication_date;
                    $scope.from_name = pc.sims_comm_user_name;
                    $scope.sims_comm_sender_id = pc.sims_comm_recepient_id;
                    $scope.sims_comm_recepient_id = pc.sims_comm_sender_id;
                    $scope.show_chat = true;
                    $scope.show_chat1 = true;
                    $scope.show_forward = false;


                    $scope.commn_message = $scope.portal_communication_details[0].sims_subject_name;
                    console.log($scope.portal_communication_details);
                });

                //Update Counter

                setTimeout(function () {
                    $http.post(ENV.apiUrl + "api/Communication/communicationcountupdate?communicationID=" + pc.sims_communication_number + "&subjectID=" + pc.sims_subject_id + "&user_code_from=" + $scope.from_user).then(function (updatecount) {
                        angular.forEach($scope.portal_communication, function (value, key) {
                            if (value.sims_communication_number == pc.sims_communication_number && value.sims_subject_id == pc.sims_subject_id)
                                value.sims_subject_new_message_count = 0;
                        });
                    });
                }, 1200);

                $scope.current_subject = pc.comnn_histr;
                $scope.subject = pc.sims_subject_name;
                $scope.isActiveSubject = pc.sims_subject_status == 'I' ? true : false;
                $('#messagebox').stop({ scrollTop: ($('#messagebox')[0].scrollHeight + 500) }, 1000);
            }

            $scope.back_to_details = function () {
                $scope.show_chat = true;
                $scope.show_chat1 = true;
                $scope.show_forward = false;
            }

            $scope.sendMessage = function () {

                var message_body =
                {
                    sims_communication_number: $scope.current_subject_group.sims_communication_number,
                    sims_subject_name: $scope.message_body,
                    sims_subject_id: $scope.current_subject_group.sims_subject_id,
                    sims_communication_date: new Date(),
                    sims_comm_sender_id: $scope.from_user,
                    sims_comm_recepient_id: $scope.from_user == $scope.current_subject_group.sims_comm_recepient_id ? $scope.current_subject_group.sims_comm_sender_id : $scope.current_subject_group.sims_comm_recepient_id
                }
                if ($scope.message_body != '') {
                    $http.post(ENV.apiUrl + "api/Communication/PortalCommunication", message_body).then(function (portal_communication_details) {
                        $scope.portal_communication_details = portal_communication_details.data;
                        $scope.current_subject.push(message_body);

                    });

                }

                $scope.message_body = '';
                $('#messagebox').stop().animate({
                    scrollTop: $('#messagebox')[0].scrollHeight
                }, 800);
            }

            $scope.sendMessageKP = function ($event) {
                if ($event.keyCode == 13 && $scope.sendofenter) {
                    $scope.sendMessage();
                }
            }

            $scope.add_new_users = function () {
                //var list = [
                //     { sims_comm_user_name: 'SHIRAZ MUHAMMED', sims_comm_recepient_id: 'P10389' },
                //    { sims_comm_user_name: 'THOOBA FATHIMA', sims_comm_recepient_id: 'P10391' },
                //    { sims_comm_user_name: 'DEYA', sims_comm_recepient_id: 'P10019' },
                //];
                $scope.newuser = true;

                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }

            $scope.add_new_users_cc = function () {
                //var list = [
                //     { sims_comm_user_name: 'SHIRAZ MUHAMMED', sims_comm_recepient_id: 'P10389' },
                //    { sims_comm_user_name: 'THOOBA FATHIMA', sims_comm_recepient_id: 'P10391' },
                //    { sims_comm_user_name: 'DEYA', sims_comm_recepient_id: 'P10019' },
                //];


                $scope.newuser = false;
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }

            $scope.user_list_archived = [];
            $scope.user_list_archivedcc = [];

            $scope.$on('global_cancel', function (str) {
                var flag = true;
                if ($scope.newuser == true) {
                    $scope.user_list_archived = [];
                } else {
                    $scope.user_list_archivedcc = [];
                }
                $scope.list = { sims_comm_user_name: '', sims_comm_recepient_id: '' };
                angular.forEach($scope.SelectedUserLst, function (value, key) {
                    $scope.list.sims_comm_user_name = value.name;
                    $scope.list.sims_comm_recepient_id = value.user_name;
                    angular.forEach($scope.user_list_archived, function (value_e, key_e) {
                        if (value_e.sims_comm_user_name == value.sims_comm_user_name && value_e.sims_comm_recepient_id == value_e.sims_comm_recepient_id)
                            flag = false;
                    });
                    if (flag)
                        if ($scope.newuser == true) {
                            $scope.new_users = true;
                            $scope.user_list_archived.push($scope.list);
                        }
                        else {
                            $scope.users_cc = true;
                            $scope.user_list_archivedcc.push($scope.list);
                        }
                    $scope.list = {};
                });
            });

            $scope.remove_new_users = function (str, $index) {
                str.splice($index, 1);
            }

            $scope.new_message = '';

            $scope.new_subject = '';

            $scope.send_forward_message = function () {

                if ($scope.user_list_archivedcc.length > 0) {
                    for (var i = 0; i < $scope.user_list_archivedcc.length; i++) {
                        $scope.user_list_archived.push($scope.user_list_archivedcc[i]);
                    }
                }
                if ($scope.new_user_cc != undefined || $scope.new_user_cc != '') {
                    for (var i = 0; i < AddUserCCRecipeint_Object.length; i++) {
                        $scope.user_list_archived.push(AddUserCCRecipeint_Object[i]);
                    }
                }
                if ($scope.new_user_search != undefined || $scope.new_user_search != '') {
                    for (var i = 0; i < AddUserRecipeint_Object.length; i++) {
                        $scope.user_list_archived.push(AddUserRecipeint_Object[i]);
                    }
                }

                if ($scope.sims_subject != '' && $scope.sims_subject_name != '') {

                    $http.post(ENV.apiUrl + "api/Communication/pstCommunication?message=" + $scope.sims_subject_name + "&subject=" + $scope.sims_subject + "&from_user=" + $scope.from_user, $scope.user_list_archived).then(function (res) {
                        $scope.msg = res.data;
                        swal({ showCloseButton: true, text: 'Message Sent Successfully', width: 320 });
                        $http.get(ENV.apiUrl + "api/Communication/GetAllPortalCommunication?user_code_from=" + $scope.from_user).then(function (portalnew_communication) {
                            $scope.portalnew_communication = portalnew_communication.data;
                            $scope.home();
                        });
                    });
                }

            }

            $scope.submit_message = function () {

                //var subject = {sims_communication_number:'',sims_communication_date:'', sims_subject_id:'',sims_subject_new_message_count:'',sims_subject_name: $scope.new_subject, sims_subject: $scope.new_message, sims_comm_sender_id: $scope.from_user ,sims_comm_recepient_id:''}
                //var data = [];
                //data[0] = subject;
                //data[1] = $scope.user_list_archived;


                if ($scope.user_list_archivedcc.length > 0) {
                    for (var i = 0; i < $scope.user_list_archivedcc.length; i++) {
                        $scope.user_list_archived.push($scope.user_list_archivedcc[i]);
                    }
                }

                if ($scope.new_user_cc != undefined || $scope.new_user_cc != '') {
                    for (var i = 0; i < AddUserCCRecipeint_Object.length; i++) {
                        $scope.user_list_archived.push(AddUserCCRecipeint_Object[i]);
                    }
                }
                if ($scope.new_user_search != undefined || $scope.new_user_search != '') {
                    for (var i = 0; i < AddUserRecipeint_Object.length; i++) {
                        $scope.user_list_archived.push(AddUserRecipeint_Object[i]);
                    }
                }

                if ($scope.new_message == undefined) $scope.new_message = '';
                if ($scope.new_subject == undefined) $scope.new_subject = '';

                if ($scope.new_message != '' && $scope.new_subject != '') {
                    $http.post(ENV.apiUrl + "api/Communication/pstCommunication?message=" + $scope.new_message + "&subject=" + $scope.new_subject + "&from_user=" + $scope.from_user, $scope.user_list_archived).then(function (res) {

                        swal({ showCloseButton: true, text: 'Message Sent Successfully', width: 320 });

                        AddUserRecipeint_Object = [];
                        AddUserCCRecipeint_Object = [];

                        $http.get(ENV.apiUrl + "api/Communication/GetAllPortalCommunication?user_code_from=" + $scope.from_user).then(function (portalnew_communication) {
                            $scope.portalnew_communication = portalnew_communication.data;
                            $scope.home();
                        });
                    });
                }
            }

            $scope.onfocusSelect = function (ctrl) {
                $(ctrl).select();
            }

            $scope.closesubject = function () {
                $scope.sims_subject_id = '';
                for (var i = 0; i < $scope.portalnew_communication.length; i++) {
                    if ($scope.portalnew_communication[i].ischecked == true) {
                        $scope.sims_subject_id = $scope.sims_subject_id + $scope.portalnew_communication[i].sims_subject_id + $scope.portalnew_communication[i].sims_communication_number + ',';
                    }
                }

                swal({
                    text: "Are you sure you want to Close this Subject? </br>Note : For Closed subject(s) you can't reply it. ",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        $http.post(ENV.apiUrl + "api/Communication/closecommunicationsubject?subjectID=" + $scope.sims_subject_id).then(function (updatecount) {
                            $scope.current_subject_group.sims_subject_status = 'I';
                            $scope.isActiveSubject = true;
                        });
                    }
                });
            }

            $scope.Repaly_message = function () {

                $scope.show_chat = true;
                $scope.show_chat1 = false;
                $scope.show_replay = true;
                $scope.show_forward = false;

            }

            $scope.Forward_message = function (info) {

                $scope.show_chat = true;
                $scope.show_chat1 = false;
                $scope.show_replay = false;
                $scope.show_forward = true;

                $scope.sims_subject_id = info.sims_subject_id;
                $scope.sims_subject = info.sims_subject;
                $scope.sims_subject_name = info.sims_subject_name;

                $scope.user_list_archived = [];
                $scope.user_list_archivedcc = [];
                $scope.new_users = '';
                $scope.new_user_cc = '';

            }

            $scope.Get_All_Parent_class_wise = function (str) {
                if (str.ischecked == true) {
                    $http.get(ENV.apiUrl + "api/Communication/getgradesectionwise_parent?grade_section=" + str.sims_grade_section_code).then(function (gradesectionwise_parent) {
                        $scope.gradesectionwise_parent = gradesectionwise_parent.data;
                        for (var i = 0; i < $scope.gradesectionwise_parent.length; i++) {
                            $scope.user_list_archived.push($scope.gradesectionwise_parent[i]);
                        }
                        $scope.new_users = true;

                    });
                }
                else {

                    for (var i = $scope.user_list_archived.length - 1; i >= 0; --i) {
                        if (str.sims_grade_section_code == $scope.user_list_archived[i].sims_grade_section_code) {
                            $scope.user_list_archived.splice(i, 1);
                        }
                    }
                    console.log($scope.user_list_archived);
                }
            }

            $scope.reset = function () {

                $scope.user_list_archived = [];
                $scope.user_list_archivedcc = [];
                $scope.new_subject = '';
                $scope.new_message = '';
                $scope.new_user_cc = '';
                $scope.new_user_search = '';
                for (var i = 0; i < $scope.getgradesection.length; i++) {
                    $scope.getgradesection[i].ischecked = false;
                }
            }

            $http.get(ENV.apiUrl + "api/Communication/getgradesection?user_name=" + $scope.from_user).then(function (getgradesection) {
                $scope.getgradesection = getgradesection.data;
            });

            var AddUserCCRecipeint_Object = [];

            $scope.Add_user_CC_seprate_commastring = function (str) {
                AddUserCCRecipeint_Object = [];
                str = str.split(',');
                for (var i = 0; i < str.length; i++) {
                    var data = {};
                    data.sims_comm_recepient_id = str[i];
                    AddUserCCRecipeint_Object.push(data);
                }
            }

            var AddUserRecipeint_Object = [];

            $scope.Add_user_seprate_commastring = function (str) {
                AddUserRecipeint_Object = [];
                str = str.split(',');
                for (var i = 0; i < str.length; i++) {
                    var data = {};
                    data.sims_comm_recepient_id = str[i];
                    AddUserRecipeint_Object.push(data);
                }
            }

            //Events End
        }])

    simsController.directive('selectOnClick', ['$window', function ($window) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                element.on('focus', function () {
                    if (!$window.getSelection().toString()) {
                        // Required for mobile Safari
                        this.setSelectionRange(0, this.value.length)
                    }
                });
            }
        };
    }]);

})();
