﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var alertnumber;
    var sdate, edate;
    var simsController = angular.module('sims.module.Common');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AlertTransactionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {


            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table1 = true;
            $scope.readonlyplain = false;
            $scope.readonlyusercode = false;
            //$scope.readonly = true;

            //$scope.cmbstatus = true;
            //$scope.checked = false;
            debugger;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = yyyy + '-' + mm + '-' + dd;
            $scope.edate = yyyy + '-' + mm + '-' + dd;
            $scope.temp = {
                startdate: yyyy + '-' + mm + '-' + dd,
                alertdate: yyyy + '-' + mm + '-' + dd,
                enddate: yyyy + '-' + mm + '-' + dd,
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //Date validation

            $scope.ChkDate = function (noissueafter) {
                debugger;
                //var date = new Date();
                //$scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy/MM/dd');
                var year1 = noissueafter.split("-")[0];
                var month1 = noissueafter.split("-")[1];
                var day1 = noissueafter.split("-")[2];
                var new_end_date = year1 + "-" + month1 + "-" + day1;

                if (new_end_date < $scope.temp.startdate) {
                    //$rootScope.strMessage = "Please Select Today or Next Date";
                    //$('#message').modal('show');
                    swal({ title: "Alert", text: "Please Select Next Date from start Date", showCloseButton: true, width: 380, });
                    $scope.temp.enddate = "";
                }

            }

            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/AlertTransaction/getAlertTransactionDatewise?from_date=" + $scope.temp.startdate + "&to_date=" + $scope.temp.enddate).then(function (res1) {

                $scope.AlertTransactionData = res1.data;
                $scope.totalItems = $scope.AlertTransactionData.length;
                $scope.todos = $scope.AlertTransactionData;
                $scope.makeTodos();

            });

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                //$scope.CheckAllChecked();
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.myFunct = function (keyEvent) {
                debugger;
                if (keyEvent.which == 13)
                    $scope.temp.startdate = yyyy + '-' + mm + '-' + dd;
            }

            $scope.myFunct1 = function (keyEvent) {
                debugger;
                if (keyEvent.which == 13)
                    $scope.temp.enddate = yyyy + '-' + mm + '-' + dd;
            }

            $scope.myFunct2 = function (keyEvent) {
                debugger;
                if (keyEvent.which == 13)
                    $scope.temp.alertdate = yyyy + '-' + mm + '-' + dd;
            }
            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.AlertTransactionData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.AlertTransactionData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.comn_alert_user_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.user_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.mod_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.comn_alert_date == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {
                $('#text-editor').data('wysihtml5').editor.composer.disable();
                $('#text-editor').data("wysihtml5").editor.clear();
                $scope.disabled = false;
                $scope.table1 = false;
                $scope.readonlyusercode = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = [];
                $scope.edt = [];
                $scope.edt = {
                    copy1: 'plain'
                }
                $scope.plaintextarea = "";
                $scope.temp = {

                    alertdate: yyyy + '-' + mm + '-' + dd,

                }
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            $scope.GlobalSearch = function () {

                debugger;
                $rootScope.visible_stud = false;
                $rootScope.visible_parent = true;
                $rootScope.visible_Employee = true;
                $rootScope.visible_UnassignedStudent = false;
                $rootScope.visible_stud = true;
                $rootScope.visible_teacher = false;
                $rootScope.chkMulti = true;
                $rootScope.visible_UnassignedParent = false;
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });



            }

            $scope.$on('global_cancel', function () {
                debugger;
                $scope.temp.user_code = '';
                $scope.temp.user_only_code = '';
                console.log($scope.SelectedUserLst);
                if ($scope.SelectedUserLst.length > 0) {
                    if ($scope.SelectedUserLst[0].tab == "tab2") {
                        for (var i = 0; i < $scope.SelectedUserLst.length; i++) {

                            $scope.temp.user_code = $scope.SelectedUserLst[i].s_parent_id + '-' + $scope.SelectedUserLst[i].name + ',' + $scope.temp.user_code;
                            $scope.temp.user_only_code = $scope.SelectedUserLst[i].s_parent_id + ',' + $scope.temp.user_only_code;

                            //$scope.temp.user_code = $scope.SelectedUserLst[i].s_parent_id  + ',' + $scope.temp.user_code;

                        }
                    }
                    else if ($scope.SelectedUserLst[0].tab == "tab1") {
                        for (var i = 0; i < $scope.SelectedUserLst.length; i++) {

                            $scope.temp.user_code = $scope.SelectedUserLst[i].s_enroll_no + '-' + $scope.SelectedUserLst[i].name + ',' + $scope.temp.user_code;
                            $scope.temp.user_only_code = $scope.SelectedUserLst[i].s_enroll_no + ',' + $scope.temp.user_only_code;


                            //$scope.temp.user_code = $scope.SelectedUserLst[i].s_enroll_no  + ',' + $scope.temp.user_code;

                        }
                    }
                    else if ($scope.SelectedUserLst[0].tab == "tab6") {
                        for (var i = 0; i < $scope.SelectedUserLst.length; i++) {
                            $scope.temp.user_code = $scope.SelectedUserLst[i].em_number + '-' + $scope.SelectedUserLst[i].name + ',' + $scope.temp.user_code;
                            $scope.temp.user_only_code = $scope.SelectedUserLst[i].em_number + ',' + $scope.temp.user_only_code;

                            //$scope.temp.user_code = $scope.SelectedUserLst[i].em_number  + ',' + $scope.temp.user_code;

                        }
                    }


                }
            });


            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = [];
                $scope.table1 = true;
                $scope.display = false;
                $('#text-editor').data('wysihtml5').editor.composer.disable();
                $('#text-editor').data("wysihtml5").editor.clear();
                $scope.temp = [];
                $scope.edt = [];
                $scope.edt = {
                    copy1: 'plain'
                }
                $scope.plaintextarea = "";
                $scope.temp = {
                    startdate: $scope.sdate,
                    alertdate: yyyy + '-' + mm + '-' + dd,
                    enddate: $scope.edate
                }
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger;
                $scope.alertnumber = 0;
                $scope.table1 = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.readonlyusercode = true;
                $scope.alertnumber = str.comn_alert_number;
                // $scope.sdttypeReadonly = true;
                $scope.temp = {

                    user_code: str.comn_alert_user_code,
                    mod_code: str.comn_alert_mod_code,
                    alertdate: str.comn_alert_date,
                    alert_code: str.comn_alert_type_code,
                    alert_priority_code: str.comn_alert_priority_code,
                    status: str.comn_alert_status
                };
                //var n=$(str.comn_alert_message).has("*") == true
                var n = str.comn_alert_message.includes("<h1>" || "<i>" || "<b>" || "<span class" || "<h2>" || "<h3>" || "<h4>" || "<h5>" || "<span>");
                if (n == true) {
                    $('#text-editor').data('wysihtml5').editor.composer.enable();
                    $scope.edt = {

                        copy1: 'style'
                    };
                    $scope.readonlyplain = true;
                    $('#text-editor').data('wysihtml5').editor.setValue(str.comn_alert_message)

                }
                else {
                    $('#text-editor').data('wysihtml5').editor.composer.disable();
                    $('#text-editor').data("wysihtml5").editor.clear();
                    $scope.edt = {
                        copy1: 'plain',
                        paintext: str.comn_alert_message
                    }
                    $scope.readonlyplain = false;
                }

            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger;
                if (Myform) {
                    datasend = [];
                    data = [];
                    var comn_alert_message1 = null;
                    if ($scope.edt.copy1 == "style") {
                        $scope.comn_alert_message1 = $('#text-editor').val();

                    }
                    else {
                        $scope.comn_alert_message1 = $scope.edt.paintext;
                    }
                   

                    var data = {
                        comn_alert_message: $scope.comn_alert_message1,
                        comn_alert_mod_code: $scope.temp.mod_code,
                        comn_alert_priority_code: $scope.temp.alert_priority_code,
                        comn_alert_type_code: $scope.temp.alert_code,
                        comn_alert_user_code: $scope.temp.user_only_code,
                        comn_alert_date: $scope.temp.alertdate,
                        comn_alert_status: $scope.temp.status
                    }
                    data.opr = 'Q';
                    datasend.push(data);


                    $http.post(ENV.apiUrl + "api/AlertTransaction/CUDAlertTransaction", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Inserted Successfully", width: 380 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Inserted", width: 380 });
                        }
                        $scope.temp = {
                            startdate: $scope.sdate,
                            alertdate: yyyy + '-' + mm + '-' + dd,
                            enddate: $scope.edate
                        }
                        $scope.getgrid();
                    });

                    $scope.table1 = true;
                    $scope.display = false;
                }

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {

                if (Myform) {
                    dataforUpdate = [];
                    var comn_alert_message1 = null;
                    if ($scope.edt.copy1 == "style") {
                        $scope.comn_alert_message1 = $('#text-editor').val();

                    }
                    else {
                        $scope.comn_alert_message1 = $scope.edt.paintext;
                    }
                    // var data = $scope.temp;
                    var data = {
                        comn_alert_message: $scope.comn_alert_message1,
                        comn_alert_mod_code: $scope.temp.mod_code,
                        comn_alert_priority_code: $scope.temp.alert_priority_code,
                        comn_alert_type_code: $scope.temp.alert_code,
                        comn_alert_user_code: $scope.temp.user_code,
                        comn_alert_date: $scope.temp.alertdate,
                        comn_alert_status: $scope.temp.status,
                        comn_alert_number: $scope.alertnumber
                    }
                    data.opr = "R";
                    dataforUpdate.push(data);
                    //dataupdate.push(data);
                    $http.post(ENV.apiUrl + "api/AlertTransaction/CUDAlertTransaction", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 380 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 380 });
                        }
                        $scope.temp = {
                            startdate: $scope.sdate,
                            alertdate: yyyy + '-' + mm + '-' + dd,
                            enddate: $scope.edate
                        }
                        $scope.getgrid();
                    });
                    dataforUpdate = [];
                    $scope.table1 = true;
                    $scope.display = false;
                }
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.getgrid = function () {
                $scope.sdate = $scope.temp.startdate;
                $scope.edate = $scope.temp.enddate;
                $http.get(ENV.apiUrl + "api/AlertTransaction/getAlertTransactionDatewise?from_date=" + $scope.temp.startdate + "&to_date=" + $scope.temp.enddate).then(function (res1) {

                    $scope.AlertTransactionData = res1.data;
                    $scope.totalItems = $scope.AlertTransactionData.length;
                    $scope.todos = $scope.AlertTransactionData;
                    $scope.makeTodos();

                });
            }





            $scope.Delete = function () {

                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    //var t = $scope.filteredTodos[i].uom_code;
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'comn_alert_number': $scope.filteredTodos[i].comn_alert_number,
                            'opr': 'N'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {

                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/AlertTransaction/CUDAlertTransaction", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Already Mapped.Can't be Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                //var t = $scope.filteredTodos[i].sims_fee_category;
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");


                                }


                            }

                        }

                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }

                $scope.currentPage = str;
            }

            //HTML5 editor
            $('#text-editor').wysihtml5();

            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                $scope.temp[name1] = year + "/" + month + "/" + day;
            }
            //bind Combo

            $http.get(ENV.apiUrl + "api/AlertTransaction/getAlertType").then(function (res1) {

                $scope.AlertTypeData = res1.data;
            });

            $http.get(ENV.apiUrl + "api/AlertTransaction/getAlertPriority").then(function (res1) {

                $scope.AlertPriorityData = res1.data;
            });

            $http.get(ENV.apiUrl + "api/AlertTransaction/getModuleCode").then(function (res1) {

                $scope.ModuleCodeData = res1.data;
            });
            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.edt = {
                copy1: 'plain'
            }
            $scope.radioclick = function (str) {

                if (str == 'plain') {
                    //document.getElementById('#text - editor').readOnly = true;
                    $scope.readonlyplain = false;
                    $('#text-editor').data('wysihtml5').editor.composer.disable();
                    // $scope.readonlystyle = true;

                }

                else {
                    //document.getElementById('#text - editor').readOnly = false;

                    $scope.readonlyplain = true;
                    $('#text-editor').data('wysihtml5').editor.composer.enable();
                    // $scope.readonlystyle = false;
                }

            }



        }])

})();
