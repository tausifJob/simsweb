﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Collaboration');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CreateNewsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'];
            $scope.itemsPerPage = '5';
            $scope.currentPage = 0;
            $scope.view = false;
            $scope.create = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            var arr_check = [];
            var arr_files = [];
            var arr_files1 = [];
            $scope.images = [];
            var checkboxes = new Array();
            var comn_files = "";
            $scope.obj = [];
            $scope.save1 = true;
            $scope.cdetails_data = [];

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;
            $scope.save_btn = false;

            //HTML5 editor
            $('#text-editor').wysihtml5();

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.obj2 = res.data;
                //console.log($scope.obj2);
            });

            $http.get(ENV.apiUrl + "api/common/News/getNewsType").then(function (res) {
                $scope.obj3 = res.data;
                //console.log($scope.obj3);
            });

            $http.get(ENV.apiUrl + "api/common/News/getNewsCategory").then(function (res) {
                $scope.obj4 = res.data;
                //console.log($scope.obj4);
            });

            $http.get(ENV.apiUrl + "api/common/News/getAllNewsUserGroup").then(function (res) {
                $scope.obj5 = res.data;
                //console.log($scope.obj5);
            });

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $http.get(ENV.apiUrl + "api/common/News/getNews?loggeduser=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.view = false;
                $scope.create = true;
                $scope.New();
                $scope.cdetails_data = res.data;
                $scope.totalItems = $scope.cdetails_data.length;
                $scope.todos = $scope.cdetails_data;
                $scope.makeTodos();
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.cdetails_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.cdetails_data;
                }
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            function searchUtil(item, toSearch) {
                /* Search Text in all 2 fields */
                return (item.sims_news_title.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_news_short_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.getgrid == function (str) {
                $http.get(ENV.apiUrl + "api/common/News/getNews?loggeduser=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.view = false;
                    $scope.create = true;
                    $scope.New();
                    $scope.cdetails_data = res.data;
                    $scope.totalItems = $scope.cdetails_data.length;
                    $scope.todos = $scope.cdetails_data;
                    $scope.makeTodos();
                });
            }

            $scope.publishnews = function (str) {
                var data = str;
                debugger;
                $http.post(ENV.apiUrl + "api/common/News/UpdateNewsPublishDate", data).then(function (res) {
                    $scope.display = true;
                    $scope.msg1 = res.data;

                    swal({ title: "Alert", text: "The Selected News Published Today.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.get(ENV.apiUrl + "api/common/News/getNews?loggeduser=" + $rootScope.globals.currentUser.username).then(function (res) {
                                $scope.view = false;
                                $scope.create = true;
                                $scope.New();
                                $scope.cdetails_data = res.data;
                                $scope.totalItems = $scope.cdetails_data.length;
                                $scope.todos = $scope.cdetails_data;
                                $scope.makeTodos();
                            });
                        }
                    });
                });
            };

            $scope.unpublishnews = function (str) {
                var data = str;
                debugger;
                $http.post(ENV.apiUrl + "api/common/News/UpdateNewsStatus", data).then(function (res) {

                    $scope.msg1 = res.data;

                    swal({ title: "Alert", text: "News Unpublished Successfully.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.get(ENV.apiUrl + "api/common/News/getNews?loggeduser=" + $rootScope.globals.currentUser.username).then(function (res) {
                                $scope.view = false;
                                $scope.create = true;
                                $scope.New();
                                $scope.cdetails_data = res.data;
                                $scope.totalItems = $scope.cdetails_data.length;
                                $scope.todos = $scope.cdetails_data;
                                $scope.makeTodos();
                            });
                        }
                    });
                });
            };
                       
            $scope.New = function () {
                $scope.view = false;
                $scope.create = true;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
                arr_check = [];
                $scope.obj5 = [];

                $http.get(ENV.apiUrl + "api/common/News/getAllNewsUserGroup").then(function (res) {
                    $scope.obj5 = res.data;
                    //console.log($scope.obj5);
                });

                $scope.edt =
               {
                   sims_news_status: true,
               }
                $scope.edt['sims_cur_code'] = $scope.obj2[0].sims_cur_code;
                $scope.Getacademicyear($scope.obj2[0].sims_cur_code);

                setTimeout(function () {
                    $scope.edt['sims_academic_year_desc'] = $scope.ob_acyear1[0].sims_academic_year;
                    
                }, 2000);

                $scope.images = [];
                $scope.images1 = [];

                $http.get(ENV.apiUrl + "api/common/News/getNewsNumber").then(function (res) {
                    $scope.obj6 = res.data;
                    document.getElementById("txt_number").disabled = true;
                    var v = document.getElementById('txt_number');
                    v.value = $scope.obj6[0].sims_news_number;
                    console.log($scope.obj6);
                });

                $('#text-editor').data("wysihtml5").editor.clear();
            }

            $scope.Getacademicyear = function (curcode) {
                console.log(curcode);
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curcode).then(function (res) {
                    $scope.ob_acyear1 = res.data;
                    console.log($scope.ob_acyear1);
                });
            }

            $scope.GetUserGroups = function (gname, gcode) {
                console.log(gname);
                if (document.getElementById(gcode).checked == true) {
                    //alert("In Checked=true");
                    arr_check.push(gname);
                }
                else {
                    //alert("In Checked=false");
                    for (var i = arr_check.length; i--;) {
                        if (arr_check[i] == gname) {
                            arr_check.splice(i, 1);
                        }
                    }
                }

                console.log(arr_check);
            }

            function convertdate(dt) {
                if (dt != null) {
                    var d1 = new Date(dt);
                    var month = d1.getMonth() + 1;
                    var day = d1.getDate();
                    if (month < 10)
                        month = "0" + month;
                    if (day < 10)
                        day = "0" + day;
                    var d = d1.getFullYear() + "-" + (month) + "-" + (day);
                    return d;
                }
            }

            $scope.Save = function (ifalt) {
                debugger;
                console.log('in create')
                $('#loader').modal({ backdrop: 'static', keyboard: false });
                $scope.edt.sims_news_ifalert = ifalt;
                var c_desc = document.getElementById('text-editor').value;

                var publish_date = new Date(convertdate($scope.edt.sims_news_publish_date));
                var expiry_date = new Date(convertdate($scope.edt.sims_news_expiry_date));
                var flag = true;

                if (arr_check.length <= 0) {
                    flag = false;
                    $('#loader').modal('hide');
                    swal({ title: "Alert", text: "Please Select atleast one User Group.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }

                if (publish_date > expiry_date) {
                    flag = false;
                    $('#loader').modal('hide');
                    swal({ title: "Alert", text: "Expiry date should be greater than publish date.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }
                if (c_desc == undefined || c_desc == '') {
                    flag = false;
                    $('#loader').modal('hide');
                    swal({ title: "Alert", text: "Please enter News Description.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }
                console.log($scope.edt.sims_news_publish_date);
                console.log($scope.edt.sims_news_expiry_date);
                console.log($scope.edt.sims_cur_code);
                console.log($scope.edt.sims_academic_year_desc);
                console.log($scope.edt.sims_news_type);
                console.log($scope.edt.sims_news_category);
                console.log($scope.edt.sims_news_title);
                if ($scope.edt.sims_news_publish_date != undefined && $scope.edt.sims_news_expiry_date != undefined &&
                    $scope.edt.sims_cur_code != undefined && $scope.edt.sims_academic_year_desc != undefined &&
                    $scope.edt.sims_news_type != undefined && $scope.edt.sims_news_category != undefined &&
                    $scope.edt.sims_news_title != undefined) {
                    if (flag == true) {

                        //comn_files = '';
                        //for (var j = 0 ; j < $scope.images.length; j++) {
                        //    comn_files = comn_files + "," + $scope.images[j].name;
                        //}

                        var data = $scope.edt;
                        console.log($scope.edt.sims_academic_year);
                        data.lst_news_groups = arr_check;
                        data.sims_news_number = document.getElementById('txt_number').value;
                        data.sims_news_desc = document.getElementById('text-editor').value;
                        data.sims_news_publish_date = convertdate($scope.edt.sims_news_publish_date);
                        data.sims_news_expiry_date = convertdate($scope.edt.sims_news_expiry_date);
                        data.opr = "I";
                        //console.log("document.getElementById('text-editor').value::::" + document.getElementById('text-editor').value);

                        $http.post(ENV.apiUrl + "api/common/News/CNews", data).then(function (res) {
                            $scope.view = false;
                            $scope.create = true;
                            $scope.msg1 = res.data;

                            //// alert("inserted");

                            //var request = {
                            //    method: 'POST',
                            //    url: ENV.apiUrl + '/api/file/upload_file?filename=' + document.getElementById('txt_number').value + "&location=" + "NewsFiles" + "&value=" + comn_files + "&update=N",
                            //    data: formdata,
                            //    headers: {
                            //        'Content-Type': undefined
                            //    }
                            //};

                            //$http(request).success(function (d) {

                            //});

                            swal({ title: "Alert", text: "News Created Successfully.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {

                                    arr_check = [];
                                    $scope.images = [];
                                    $('#loader').modal('hide');
                                    $scope.currentPage = 1
                                    $http.get(ENV.apiUrl + "api/common/News/getNews?loggeduser=" + $rootScope.globals.currentUser.username).then(function (res) {
                                        $scope.view = false;
                                        $scope.create = true;
                                        $scope.New();
                                        $scope.cdetails_data = res.data;
                                        $scope.totalItems = $scope.cdetails_data.length;
                                        $scope.todos = $scope.cdetails_data;
                                        $scope.makeTodos();
                                    });

                                    $scope.myForm.$setPristine();
                                    $scope.myForm.$setUntouched();
                                }
                            });

                        });
                    }
                }
            }

            $scope.Update = function () {
                $('#loader').modal({ backdrop: 'static', keyboard: false });

                var c_desc = document.getElementById('text-editor').value;

                var publish_date = new Date(convertdate($scope.edt.sims_news_publish_date));
                var expiry_date = new Date(convertdate($scope.edt.sims_news_expiry_date));
                var flag = true;

                if (arr_check.length <= 0) {
                    flag = false;
                    $('#loader').modal('hide');
                    swal({ title: "Alert", text: "Please Select atleast one User Group.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }

                if (publish_date > expiry_date) {
                    flag = false;
                    $('#loader').modal('hide');
                    swal({ title: "Alert", text: "Expiry date should be greater than publish date.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }
                if (c_desc == undefined || c_desc == '') {
                    flag = false;
                    $('#loader').modal('hide');
                    swal({ title: "Alert", text: "Please enter News Description.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }
                if (flag == true) {

                    //comn_files = '';
                    //for (var j = 0 ; j < $scope.images.length; j++) {
                    //    comn_files = comn_files + "," + $scope.images[j].name;
                    //}
                    var data = $scope.edt;
                    data.lst_news_groups = arr_check;
                    data.sims_news_number = document.getElementById('txt_number').value;
                    data.sims_news_desc = document.getElementById('text-editor').value;
                    console.log('before' + $scope.edt.sims_news_publish_date);
                    data.sims_news_publish_date = convertdate($scope.edt.sims_news_publish_date);
                    console.log('after' + data.sims_news_publish_date);
                    data.sims_news_expiry_date = convertdate($scope.edt.sims_news_expiry_date);
                    console.log(data.sims_news_publish_date);

                    if ($scope.edt.sims_news_file_path1 == undefined)
                        $scope.edt.sims_news_file_path1 = '';
                    if ($scope.edt.sims_news_file_path1_en == undefined)
                        $scope.edt.sims_news_file_path1_en = '';
                    if ($scope.edt.sims_news_file_path2 == undefined)
                        $scope.edt.sims_news_file_path2 = '';
                    if ($scope.edt.sims_news_file_path2_en == undefined)
                        $scope.edt.sims_news_file_path2_en = '';
                    if ($scope.edt.sims_news_file_path3 == undefined)
                        $scope.edt.sims_news_file_path3 = '';
                    if ($scope.edt.sims_news_file_path3_en == undefined)
                        $scope.edt.sims_news_file_path3_en = '';

                    data.opr = "U";
                    $http.post(ENV.apiUrl + "api/common/News/UNews", data).then(function (res) {
                        $scope.view = false;
                        $scope.create = true;
                        $scope.msg1 = res.data;

                        //var request = {
                        //    method: 'POST',
                        //    url: ENV.apiUrl + '/api/file/upload_file?filename=' + document.getElementById('txt_number').value + "&location=" + "NewsFiles" + "&value=" + comn_files + "&update=Y",
                        //    data: formdata,
                        //    headers: {
                        //        'Content-Type': undefined
                        //    }
                        //};

                        //$http(request).success(function (d) {
                        //    //$scope.getgrid();
                        //});

                        swal({ title: "Alert", text: "News Updated Successfully.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                arr_check = [];
                                $scope.images = [];
                                $('#loader').modal('hide');
                                $scope.currentPage = 1
                                $http.get(ENV.apiUrl + "api/common/News/getNews?loggeduser=" + $rootScope.globals.currentUser.username).then(function (res) {
                                    $scope.view = false;
                                    $scope.create = true;

                                    $scope.cdetails_data = res.data;
                                    $scope.totalItems = $scope.cdetails_data.length;
                                    $scope.todos = $scope.cdetails_data;
                                    $scope.makeTodos();
                                });

                                $scope.myForm.$setPristine();
                                $scope.myForm.$setUntouched();
                            }
                        });
                    });
                }
            }

            $scope.cancel = function () {
                $scope.view = false;
                $scope.create = true;

                arr_check = [];
                $scope.images = [];
                $('#loader').modal('hide');
                $scope.currentPage = 1
                $http.get(ENV.apiUrl + "api/common/News/getNews?loggeduser=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.view = false;
                    $scope.create = true;
                    $scope.New();
                    $scope.cdetails_data = res.data;
                    $scope.totalItems = $scope.cdetails_data.length;
                    $scope.todos = $scope.cdetails_data;
                    $scope.makeTodos();
                });

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            //File Download
            $scope.download = function (str) {
                window.open($scope.url + "/Images/NewsFiles/" + str, "_new");
            };

            /*start_attached Files*/

            var formdata = new FormData();
            $scope.images = [];
            $scope.images_att = [];

            $scope.getTheFiles = function ($files) {
                $scope.filesize = true;
                var i = 0, exists = "no";

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);

                    var i = 0;
                    if ($files[i].size > 2000000) {
                        $scope.filesize = false;
                        // $scope.edt.photoStatus = false;
                        swal({ title: "Alert", text: "File Should Not Exceed 2MB.", showCloseButton: true, width: 380, });
                    }
                    else {
                        arr_files.push($files[i].name);
                        if ($scope.images.length > 0) {
                            for (var j = 0 ; j < $scope.images.length; j++) {
                                if ($scope.images[j].name == $files[i].name) {
                                    exists = "yes";
                                    return;
                                }
                            }

                            if (exists == "no") {
                                $scope.images.push({
                                    name: $files[i].name,
                                    file: $files[i].size
                                });
                                if ($scope.images.length > 3) {
                                    $scope.images.splice(3, $scope.images.length);
                                    swal({ title: "Alert", text: "You cannot upload more than 3 files.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                        }
                                    });
                                    return;
                                }
                            }
                            exists = "no";
                        }
                        else {
                            $scope.images.push({
                                name: $files[i].name,
                                file: $files[i].size

                            });
                        }
                        // $scope.$apply();
                    }
                    i++;
                });
            };

            $scope.file_changed = function (element, str) {
                debugger;
                var photofile = element.files[0];
                var v = photofile.name.indexOf('.');
                $scope.name = photofile.name.substr(0, v);
                $scope.fileext = photofile.name.substr(v + 1);
                $scope.photo_filename = (photofile.type);

                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;

                    });
                };
                reader.readAsDataURL(photofile);

                if (element.files[0].size < 2000000) {
                    var filenm = '';
                    var cnumber = document.getElementById('txt_number').value;
                    //+ '_' + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss')

                    var ind = 0;

                    if ($scope.edt.sims_news_file_path1 == '' || $scope.edt.sims_news_file_path1 == undefined) {
                        filenm = cnumber + '_1';
                        $scope.edt.sims_news_file_path1 = filenm + '.' + $scope.fileext;
                        $scope.edt.sims_news_file_path1_en = element.files[0].name;
                    }
                    else if ($scope.edt.sims_news_file_path2 == '' || $scope.edt.sims_news_file_path2 == undefined) {
                        filenm = cnumber + '_2';
                        $scope.edt.sims_news_file_path2 = filenm + '.' + $scope.fileext;
                        $scope.edt.sims_news_file_path2_en = element.files[0].name;
                    }
                    else if ($scope.edt.sims_news_file_path3 == '' || $scope.edt.sims_news_file_path3 == undefined) {
                        filenm = cnumber + '_3';
                        $scope.edt.sims_news_file_path3 = filenm + '.' + $scope.fileext;
                        $scope.edt.sims_news_file_path3_en = element.files[0].name;
                    }

                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + 'api/file/uploadCircularDocument?filename=' + filenm + "&location=" + "NewsFiles",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    debugger
                    $http(request).success(function (d) {

                    });

                    console.log($scope.name.type);
                    console.log($scope.name);
                    console.log($scope.edt.sims_news_file_path1);
                    console.log($scope.edt.sims_news_file_path2);
                    console.log($scope.edt.sims_news_file_path3);
                }
            };

            $scope.uploadClick = function (str) {
                $scope.filesize = true;
                $scope.edt1 = str;
                $scope.ins = false;
                formdata = new FormData();
            }

            $scope.delpush = [];

            $scope.CancelFileUpload = function (idx) {
                debugger;
                $scope.delpush.push(idx);

                $scope.images.splice(idx, 1);
                if (idx == 0)
                    $scope.edt.sims_news_file_path1 = '';
                if (idx == 1)
                    $scope.edt.sims_news_file_path2 = '';
                if (idx == 2)
                    $scope.edt.sims_news_file_path3 = '';
                console.log($scope.images);
            };

            $scope.uploadImg = function () {
                alert("Upload ");
                var request = {
                    method: 'POST',
                    url: ENV.apiUrl + '/api/file/upload_file?filename=' + document.getElementById('txt_number').value + "&location=" + "NewsFiles",
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http(request).success(function (d) {
                    //alert(d);
                    //var data = {
                    //    sims_parent_number: $rootScope.globals.currentUser.username,
                    //    sims_parent_father_img: d
                    //}                   
                });
            }

            $scope.edit = function (str) {
                debugger;
                //alert("IN EDIT FUNCTION");
                $scope.view = false;
                $scope.create = true;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                console.log(str);
                $scope.edt = str;
                $scope.images = [];
                $scope.delpush = [];
                //Set User Group Checkboxes
                console.log($scope.edt.lst_news_groups);

                /// var inputs = document.getElementsByTagName("input");
                $('input:checkbox').each(function () {
                    var chk_id = $(this).attr("id");
                    var chk_val = $(this).attr("value");
                    var i = 0;
                    for (var i = 0; i < $scope.edt.lst_news_groups.length; i++) {
                        if ($scope.edt.lst_news_groups[i] == chk_id) {
                            console.log('chk_id::' + chk_id + 'uval::' + $scope.edt.lst_news_groups[i]);
                            this.checked = true;
                            arr_check.push(chk_val);
                        }
                    }

                    $scope.images = [];
                    $scope.del_images = [];
                    if ($scope.edt.sims_news_file_path1 == "") { }
                    else {
                        $scope.images.push({
                            name: $scope.edt.sims_news_file_path1,
                            file: ''
                        });
                    }
                    if ($scope.edt.sims_news_file_path2 == "") { }
                    else {
                        $scope.images.push({
                            name: $scope.edt.sims_news_file_path2,
                            file: ''
                        });
                    }
                    if ($scope.edt.sims_news_file_path3 == "") { }
                    else {
                        $scope.images.push({
                            name: $scope.edt.sims_news_file_path3,
                            file: ''
                        });
                    }

                });

                $scope.Getacademicyear($scope.edt.sims_cur_code);

                var $ta = $('textarea#text-editor');
                var w5ref = $ta.data('wysihtml5');
                if (w5ref) {
                    w5ref.editor.setValue($scope.edt.sims_news_desc);
                } else {
                    $ta.html($scope.edt.sims_news_desc);
                }
                wysihtml5.editor.setValue = $scope.edt.sims_news_desc;

            }

        }])

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {

            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])
})();