﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Collaboration');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('DefaulterStatusController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            $('*[data-datepicker="true"] input[type="text"]').datepicker(
                         {
                             todayBtn: true,
                             orientation: "top left",
                             autoclose: true,
                             todayHighlight: true,
                             format: 'yyyy-mm-dd'
                         });


            /* PAGER FUNCTIONS*/
            $scope.edt = [];
            $('#text-editor').wysihtml5();
            $scope.cur_change = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllAcademicYears?cur_code=" + $scope.edt.sims_cur_code).then(function (admissionYear) {
                    $scope.admissionYear = admissionYear.data;
                    console.log($scope.admissionYear);
                    for (var i = 0; i < $scope.admissionYear.length; i++) {
                        if ($scope.admissionYear[i].sims_academic_year_status == 'C') {
                            //   $scope.edt['sims_cur_code'] = cur_values;
                            $scope.edt['sims_academic_year'] = $scope.admissionYear[i].sims_academic_year;
                        }
                    }
                    $scope.acdm_yr_change();
                });
            }

            $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllCurName").then(function (admissionCur) {
                $scope.admissionCur = admissionCur.data;
                $scope.edt['sims_cur_code'] = admissionCur.data[0].sims_cur_code;
                $scope.cur_change();
            });

            $http.get(ENV.apiUrl + "api/Fee/SFS/GetDefaulterfeeTypes").then(function (feetypes) {
                $scope.feetypes = feetypes.data;
                setTimeout(function () {
                    $('#cmb_feeTypes').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);

            });

            $http.get(ENV.apiUrl + "api/Fee/SFS/GetDefaulterviewList").then(function (listtype) {
                $scope.listtype = listtype.data;
            });

            $http.get(ENV.apiUrl + "api/Fee/SFS/GetDefaulterviewMode").then(function (listmode) {
                $scope.listmode = listmode.data;
            });

            $scope.acdm_yr_change = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year).then(function (admissionGrade) {
                    $scope.admissionGrade = admissionGrade.data;
                    setTimeout(function () {
                        $('#cmb_grade').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });

                $http.get(ENV.apiUrl + "api/Fee/SFS/GetDefaulterTerms?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (terms) {
                    $scope.terms = terms.data;
                });
            }

            $scope.grades = '';
            $scope.grade_change = function () {
                $scope.grades = '';
                try {
                    $("#cmb_section").multipleSelect("uncheckAll");
                }
                catch (e) { }
                for (var i = 0; i < $scope.edt.sims_grade_code.length; i++)
                    $scope.grades = $scope.grades + ($scope.edt.sims_grade_code[i]) + ',';

                $http.get(ENV.apiUrl + "api/Fee/SFS/GetDefaulterSection?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year + "&g_code=" + $scope.grades).then(function (admissionsection) {
                    $scope.admissionsection = admissionsection.data;
                    console.log(admissionsection.data);
                    setTimeout(function () {
                        $('#cmb_section').change(function () {
                        }).multipleSelect({ width: '100%' });


                    }, 1000);

                });
            }
            $scope.btnPreview_click = function () {

                $scope.grades = '';
                $scope.sections = '';
                $scope.sims_fee_code = '';
                for (var i = 0; i < $scope.edt.sims_grade_code.length; i++)
                    $scope.grades = $scope.grades + ($scope.edt.sims_grade_code[i]) + ',';

                for (var i = 0; i < $scope.edt.sims_section_code.length; i++)
                    $scope.sections = $scope.sections + ($scope.edt.sims_section_code[i]) + ',';

                for (var i = 0; i < $scope.edt.sims_fee_code.length; i++)
                    $scope.sims_fee_code = $scope.sims_fee_code + ($scope.edt.sims_fee_code[i]) + ',';

                $scope.edt1 = {'sims_term_code':''};
                $scope.edt1.sims_cur_code = $scope.edt.sims_cur_code;
                $scope.edt1.sims_academic_year = $scope.edt.sims_academic_year;
                $scope.edt1.sims_grade_code = $scope.grades;
                $scope.edt1.sims_section_code = $scope.sections;
                $scope.edt1.sims_fee_code = $scope.sims_fee_code;
                $scope.edt1.sims_view_list_code = $scope.edt.sims_view_list_code;
                $scope.edt1.sims_view_mode_code = $scope.edt.sims_view_mode_code;
                $scope.edt1.sims_term_code = $scope.edt.sims_term_code;

                if ($scope.edt.sims_search != undefined)
                    $scope.edt1.sims_search = $scope.edt.sims_search;
                else
                    $scope.edt1.sims_search = undefined;

                if ($scope.edt.sims_amount != undefined)
                    $scope.edt1.sims_amount = $scope.edt.sims_amount;
                else
                    $scope.edt1.sims_amount = undefined;
                
                debugger;
                $http.post(ENV.apiUrl + "api/Fee/SFS/DefaultersList", $scope.edt1).then(function (defaulterdata) {
                    $scope.defaulterdata = defaulterdata.data;
                    console.log(defaulterdata.data);

                    $scope.summeryData = $scope.defaulterdata[1];
                    if ($scope.defaulterdata[0].length <= 0) {
                        swal(
                        {
                            showCloseButton: true,
                            text: 'No Data Found',
                            width: 350,
                            showCloseButon: true
                        });
                    }
                    else {
                        $scope.totalItems = $scope.summeryData.length;
                        $scope.todos = $scope.summeryData;
                        $scope.makeTodos();
                    }
                });
            }
            $scope.printData = function (info) {

            }
            $scope.reset = function () {
                $scope.admissions = [];
                $scope.filteredTodos = [];
                $scope.defaulterdata = [];
                $scope.edt = { 'sims_cur_code': $scope.edt.sims_cur_code, 'sims_academic_year': $scope.edt.sims_academic_year }
                $scope.acdm_yr_change();
                $scope.edt.sims_start_date = '';
                $scope.edt.sims_end_date = '';
                $scope.edt.admission_no = '';
                $scope.email_subject = '';
                $('#text-editor').data("wysihtml5").editor.clear();
                try {
                    $("#cmb_grade").multipleSelect("uncheckAll");
                }
                catch (e) { }
                try {
                    $("#cmb_section").multipleSelect("uncheckAll");
                }
                catch (e) { } try {
                    $("#cmb_feeTypes").multipleSelect("uncheckAll");
                }
                catch (e) { }
            }

            $scope.message = function () {
                $('#MyModal').modal({ backdrop: 'static', keyboard: true });
            }
            $(function () {
                $('#cmb_grade').multipleSelect({ width: '100%' });
                $('#cmb_section').multipleSelect({ width: '100%' });
                $('#cmb_feeTypes').multipleSelect({ width: '100%' });

            });

            /* PAGER FUNCTIONS*/

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 20, $scope.maxSize = 7, $scope.currentPage_ind = 0;

            $scope.makeTodos = function () {
                $scope.currentPage_ind = 0;

                if ((Math.round($scope.totalItems / $scope.numPerPage) * $scope.numPerPage) > $scope.totalItems)
                    $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage);
                else
                    $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage) + 1;

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
                var main = document.getElementById('chkSearch');
                main.checked = false;
                $scope.CheckMultipleSearch();
            };

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.currentPage_ind = str;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage_ind = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                console.log("PageNumber=" + $scope.numPerPage);


                $scope.makeTodos();
            }

            /* PAGER FUNCTIONS*/

            $scope.CheckMultipleSearch = function () {
                var main = document.getElementById('chkSearch');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if (main.checked == true) {
                        $scope.filteredTodos[i].isSelected = true;
                    }
                    else if (main.checked == false) {
                        $scope.row1 = '';
                        $scope.color = '#edefef';
                        main.checked = false;
                        $scope.filteredTodos[i].isSelected = false;
                    }
                }
            }
            $scope.CheckOneByOneSearch = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                    }
                });

                var main = document.getElementById('chkSearch');
                if (main.checked == true) {
                    main.checked = false;
                    //$scope.color = '#edefef';
                }

            }

            $scope.submitdata = function () {
                if ($scope.email_subject == undefined || document.getElementById('text-editor').value == undefined) {
                    swal(
                    {
                        showCloseButton: true,
                        text: 'Please Define Message Or Subject',
                        width: 350,
                        showCloseButon: true
                    });
                }
                else {
                    $scope.data = [];

                    for (var x = 0; x < $scope.filteredTodos.length; x++) {
                        if ($scope.filteredTodos[x].isSelected == true && $scope.filteredTodos[x].parent_email != '') {
                            $scope.filteredTodos[x].sims_cur_code = $scope.edt.sims_cur_code;
                            $scope.filteredTodos[x].sims_academic_year = $scope.edt.sims_academic_year;
                            $scope.filteredTodos[x].email_subject = $scope.email_subject;
                            $scope.message_details = "<table border=1 style='border-collapse:collapse;border-width:3px;width:450px'><tr><td Colspan=2>Fee Due Details</td></tr><tr><td Colspan=2>Enrollment No:" + $scope.filteredTodos[x].sims_enroll_number + "</td></tr><tr><td Colspan=2> Student Name:" + $scope.filteredTodos[x].student_name + "</td></tr><tr><td>Fee Type</td><td>Fee Amount</td></tr>";

                            for (var i = 0; i < $scope.defaulterdata[0].length; i++) {
                                if ($scope.defaulterdata[0][i].sims_enroll_number == $scope.filteredTodos[x].sims_enroll_number) {
                                    $scope.message_details = $scope.message_details += "<tr><td>" + $scope.defaulterdata[0][i].sims_fee_type_desc + "</td><td style='text-align:right'>" + $scope.defaulterdata[0][i].sims_balance_amount + "</td></tr>";
                                }
                            }
                            $scope.message_details = $scope.message_details += "<tr><td>Grand Total :</td><td style='text-align:right'>" + $scope.filteredTodos[x].sims_balance_amount_total + "</td></tr>";
                            $scope.message_details = $scope.message_details + "</table>";

                            $scope.filteredTodos[x].email_message = document.getElementById('text-editor').value + "<br/><br/>" + $scope.message_details;
                            $scope.data.push($scope.filteredTodos[x]);
                        }
                    }
                    console.log($scope.data);
                    if ($scope.data.length <= 0) {
                        swal(
                        {
                            showCloseButton: true,
                            text: 'Please Select student to send Email Message.',
                            width: 350,
                            showCloseButon: true
                        });
                    }
                    else {
                        console.log($scope.data);
                     
                        $http.post(ENV.apiUrl + "api/Fee/SFS/emailtoDefaulters", $scope.data).then(function (resl) {
                            if (resl.data == true) {
                                $('#MyModal').modal('hide');
                                $scope.email_subject = '';
                                $scope.email_message = '';
                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'Email Message Scheduled Or Sent Sucessfully.',
                                    width: 350,
                                    showCloseButon: true
                                });
                            }
                            else {
                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'Error in Sending Email Message.',
                                    width: 350,
                                    showCloseButon: true
                                });
                            }
                        });
                        
                    }
                    console.log($scope.data);
                }
            }

            //Events End
        }])
})();



