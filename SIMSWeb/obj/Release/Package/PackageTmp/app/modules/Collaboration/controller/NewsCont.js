﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Collaboration');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('NewsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'];

            $scope.itemsPerPage = '5';
            $scope.currentPage = 0;
            $scope.view = true;
            $scope.create = false;
            /// alert("In NewsCont::"+$scope.newsobj.userNewsCount);
            // $scope.newsobj.userCircularCount = 44;
            //$scope.newsobj = { userCircularCount: '44' }

            //alert("$scope.newsobj.userNewsCount::" + $scope.newsobj.userNewsCount);
            // document.getElementById('mainform.msgs-badge').textContent = $scope.newsobj.userCircularCount;

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.setStart = function (date) {
                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                var date1 = year + "-" + month + "-" + day;
                $scope.sdate = date1;

            }

            $scope.setEnd = function (date) {
                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                var date1 = year + "-" + month + "-" + day;
                $scope.edate = date1;
            }

            $scope.size = function (pagesize) {
                $scope.itemsPerPage = pagesize;
            }

            $http.get(ENV.apiUrl + "api/common/News/GetUserNews?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                debugger;
                $scope.create = false;
                $scope.view = true;
                $scope.obj = res.data;
                console.log($scope.obj);

            });

            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }
                return ret;
            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.obj.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;
            };



            //File Download
            $scope.download = function (str) {
                // console.log('School URL:::' + $rootScope.globals.currentSchool.lic_website_url);
                window.open($scope.url + "/Images/NewsFiles/" + str, "_new");
            };

            //Search
            $scope.search_click = function (str) {
                $http.get(ENV.apiUrl + "api/common/News/GetUserNewsBYDate?username=" + $rootScope.globals.currentUser.username + "&fromDate=" + $scope.sdate + "&toDate=" + $scope.edate).then(function (res) {
                    $scope.create = false;
                    $scope.view = true;
                    $scope.obj = res.data;
                    console.log($scope.obj);
                });
            };

            $scope.UpdateCStatus = function (cnum) {
                //alert("IN UpdateCStatus::news_number::" + cnum + "$rootScope.globals.currentUser.username::" + $rootScope.globals.currentUser.username);

                $http.post(ENV.apiUrl + "api/common/News/UpdateNewsAsRead?news_number=" + cnum + "&loggeduser=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.view = true;
                    $scope.create = false;
                    $scope.msg1 = res.data;
                    $rootScope.strMessage = $scope.msg1.strMessage;
                    console.log($scope.msg1);
                    //$('#message').modal('show');

                    $http.get(ENV.apiUrl + "api/common/News/GetUserNews?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                        $scope.create = false;
                        $scope.view = true;
                        $scope.obj = res.data;
                        console.log($scope.obj);
                    });
                });
            }
        }])
})();