﻿(function () {
    'use strict';
    var opr = '';
    var supliercode = [];
    var main;
    var data1 = [];
    var data = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SupplierDepartmentsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            debugger
            $scope.pagesize = '5';
            $scope.table1 = true;
            $scope.operation = false;
            $scope.editmode = false;

            $scope.countData = [
               { val: 5, data: 5 },
               { val: 10, data: 10 },
               { val: 15, data: 15 },
            ]

            $scope.size = function (str) {
                if (str == 5 || str == 10 || str == 15) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage);
                $scope.makeTodos();
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/SupplierDepartmentDetails/getSupplierGroupName").then(function (getSupplierGroupName_Data) {
                    $scope.SupplierGroupName = getSupplierGroupName_Data.data;

                    if ($scope.SupplierGroupName.length > 0) {
                        $scope.table1 = true;
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.SupplierGroupName.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.SupplierGroupName.length, data: 'All' })
                        }
                        $scope.totalItems = $scope.SupplierGroupName.length;
                        $scope.todos = $scope.SupplierGroupName;
                        $scope.makeTodos();
                    }
                    else {
                        $scope.table1 = true;
                        $scope.filteredTodos = [];
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                    }

                });
            }

            $http.get(ENV.apiUrl + "api/SupplierDepartmentDetails/getSupplierGroupName").then(function (getSupplierGroupName_Data) {
                $scope.SupplierGroupName = getSupplierGroupName_Data.data;

                if ($scope.SupplierGroupName.length > 0) {
                    $scope.table1 = true;
                    $scope.pager = true;
                    if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.SupplierGroupName.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.SupplierGroupName.length, data: 'All' })
                    }
                    $scope.totalItems = $scope.SupplierGroupName.length;
                    $scope.todos = $scope.SupplierGroupName;
                    $scope.makeTodos();
                }
                else {
                    $scope.table1 = true;
                    swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                }

            });

            $http.get(ENV.apiUrl + "api/CreateCostingSheet/getDepartments").then(function (getDepartments_Data) {
                $scope.Departments_Data = getDepartments_Data.data;
            });

            $http.get(ENV.apiUrl + "api/CreateCostingSheet/getSupplier").then(function (getSupplier_Data) {
                $scope.Supplier_Data = getSupplier_Data.data;
            });

            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.New = function () {
                $scope.edt = {
                    sup_code: '',
                    dep_code: '',
                }
                $scope.editmode = false;
                $scope.newmode = true;
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.up = function (str) {

                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.readonly = true;

                $scope.edt = {
                    sup_code: str.sup_code,
                    dep_code: str.dep_code,
                    sup_name: str.sup_name,
                    dep_name: str.dep_name
                }
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.edt = {
                    sup_code: '',
                    dep_code: '',
                }
            }

            $scope.Save = function (myForm) {

                if (myForm) {
                    data1 = [];
                    data = [];
                    var data = $scope.edt;
                    data.opr = 'I';

                    $scope.exist = false;
                    for (var i = 0; i < $scope.SupplierGroupName.length; i++) {
                        if ($scope.SupplierGroupName[i].sup_code == data.sup_code && $scope.SupplierGroupName[i].dep_code == data.dep_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ title: "Alert", text: "Record Already Exists", width: 300, height: 200 });
                    }
                    else {
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/SupplierDepartmentDetails/ItemMasterDetailsCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                            }
                            $scope.getgrid();
                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;

                }
            }

            $scope.Update = function (myForm) {
                if (myForm) {
                    data1 = [];
                    data = [];
                    var data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/SupplierDepartmentDetails/ItemMasterDetailsCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                        $scope.getgrid();

                    })
                    $scope.operation = false;
                    $scope.table1 = true;

                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sup_code + i);

                    if (main.checked == true) {
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }

                    else {
                        v.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.deleterecord = function () {
                supliercode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sup_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletecode = ({
                            'sup_code': $scope.filteredTodos[i].sup_code,
                            'dep_code': $scope.filteredTodos[i].dep_code,
                            opr: 'D'
                        });
                        supliercode.push(deletecode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/SupplierDepartmentDetails/ItemMasterDetailsCUD", supliercode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            debugger
                                            $scope.getgrid();
                                            //$scope.Grid_Data();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sup_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }

                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.SupplierGroupName, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.SupplierGroupName;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */

                return (
                     item.sup_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.dep_name.toLowerCase().indexOf(toSearch.toLowerCase())) ? true : false;


            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

        }])

})();
