﻿(function () {
    'use strict';
    var obj1, temp, opr, CurrentDate, comp;
    var main, deletefin = [], data = [], data1 = [], ItemDetails = [];
    var finanacecode = [];
    var check;
    var reqno;

    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ReprintOrdersCont',
         ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {
             $scope.priview_table = false;
             $scope.pagesize = "5";
             $scope.pageindex = 0;
             $scope.search_Req_Data = [];
             $scope.Main_table = false;
             $scope.Main_table_Service = false;
             $scope.temp = [];
             $scope.temp2 = [];
             var subopr = null, ret, rem, res, red, res, ree;
             console.clear();

             $timeout(function () {
                 $("#fixTable").tableHeadFixer({ 'top': 1 });
             }, 100);


             $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

             $scope.makeTodos = function () {
                 var rem = parseInt($scope.totalItems % $scope.numPerPage);
                 if (rem == '0') {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                 }
                 else {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                 }

                 var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                 var end = parseInt(begin) + parseInt($scope.numPerPage);

                 console.log("begin=" + begin); console.log("end=" + end);

                 $scope.filteredTodos = $scope.todos.slice(begin, end);
             };


             $scope.Transaction_no_byvar = function (ret, rem, res, red) {

                 if ($scope.temp.request_type_code == '01') {
                     subopr = 'F';
                     $scope.dis_forgc = false;
                     $scope.dis_forgc1 = false;
                 }
                 else if ($scope.temp.request_type_code == '02') {
                     subopr = 'G';
                     $scope.dis_forgc = false;
                     $scope.dis_forgc1 = false;
                 }
                 else if ($scope.temp.request_type_code == '03') {
                     subopr = 'H';
                     $scope.dis_forgc = true;
                     $scope.dis_forgc1 = false;
                 }
                 else if ($scope.temp.request_type_code == '04') {
                     subopr = 'J';
                     $scope.dis_forgc = true;
                     $scope.dis_forgc1 = false;
                 }
                 debugger;
                 $http.get(ENV.apiUrl + "api/RequestDetail/getTransactionNo?&subopr=" + subopr + "&req_type=" + ret + "&req_mode=" + rem + "&req_status=" + res + "&dept_code=" + red + "&user_code=" + '' + "&from_date=" + $scope.temp.rd_from_required + "&to_date=" + $scope.temp.rd_up_required).then(function (searchtransctionData) {

                     $scope.search_transaction_code = searchtransctionData.data;
                 });

             }

             $scope.getallvalues = function () {

                 $http.get(ENV.apiUrl + "api/RequestDetail/getRequestTypeNew").then(function (reqtypedetailsNew) {

                     $scope.req_type_detailsNew = reqtypedetailsNew.data;
                     $scope.temp['request_mode_code'] = $scope.req_type_detailsNew[0].request_mode_code;
                     ret = $scope.req_type_detailsNew[0].request_mode_code;
                     if ($scope.temp.request_mode_code == 'S') {
                         $scope.hide_itemne = true;
                         $scope.temp.reqQty = 0;
                     }
                     else {
                         $scope.hide_itemne = false;
                     }
                 });

                 $http.get(ENV.apiUrl + "api/RequestDetail/getRequestModeRP").then(function (reqtypemodeNew) {

                     $scope.req_mode_detailsNew = reqtypemodeNew.data;
                     $scope.temp['request_type_code'] = $scope.req_mode_detailsNew[0].request_type_code;
                     rem = $scope.req_mode_detailsNew[0].request_type_code;
                     if ($scope.temp.request_mode_code == 'S') {
                         $scope.hide_itemne = true;
                         $scope.temp.reqQty = 0;
                     }
                     else {
                         $scope.hide_itemne = false;
                     }
                 });

                 $http.get(ENV.apiUrl + "api/RequestDetail/getRequestStatusRP").then(function (reqtypestatusNew) {

                     $scope.req_status_detailsNew = reqtypestatusNew.data;
                     $scope.temp['request_status_code'] = $scope.req_status_detailsNew[0].request_status_code;
                     res = $scope.req_status_detailsNew[0].request_status_code;
                     if ($scope.temp.request_mode_code == 'S') {
                         $scope.hide_itemne = true;
                         $scope.temp.reqQty = 0;
                     }
                     else {
                         $scope.hide_itemne = false;
                     }
                 });

                 $http.get(ENV.apiUrl + "api/RequestDetail/getDeparments").then(function (deptdata) {
                     debugger;
                     $scope.dept_data = deptdata.data;
                     $scope.temp['dept_code'] = $scope.dept_data[0].dept_code;
                     red = $scope.dept_data[0].dept_code;
                     $scope.Transaction_no_byvar(ret, rem, res, red);
                 });
             };

             $scope.getallvalues();

             var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd');
             $scope.dt = { req_date: dateyear }

             $scope.SearchRequest = function () {
                 //$rootScope.globals.currentUser.username

                 if ($scope.temp.request_type_code == '01') {
                     subopr = 'A';
                 }
                 else if ($scope.temp.request_type_code == '02') {
                     subopr = 'B';
                 }
                 else if ($scope.temp.request_type_code == '03') {
                     subopr = 'C';
                 }
                 else if ($scope.temp.request_type_code == '04') {
                     subopr = 'E';
                 }

                 $http.get(ENV.apiUrl + "api/RequestDetail/getApprovalRequest?&subopr=" + subopr + "&req_type=" + $scope.temp.request_mode_code + "&req_mode=" + $scope.temp.request_type_code + "&req_status=" + $scope.temp.request_status_code + "&dept_code=" + $scope.temp.dept_code + "&user_code=" + null + "&from_date=" + $scope.temp.rd_from_required + "&to_date=" + $scope.temp.rd_up_required + "&transaction_no=" + $scope.temp.req_no).then(function (searchReqData) {

                     $scope.search_Req_Data = searchReqData.data;
                     $scope.totalItems = $scope.search_Req_Data.length;
                     $scope.todos = $scope.search_Req_Data;
                     $scope.makeTodos();

                     if ($scope.temp.request_mode_code == 'I') {
                         if ($scope.search_Req_Data.length > 0) {
                             $scope.Main_table = true;
                             $scope.Main_table_Service = false;
                             $scope.Grid = true;
                         }
                         else {
                             swal({ title: "Alert", text: "No Documents Found...", width: 300, height: 200 });
                         }
                     }

                     if ($scope.temp.request_mode_code == 'S') {
                         if ($scope.search_Req_Data.length > 0) {
                             $scope.Main_table_Service = true;
                             $scope.Main_table = false;
                             $scope.Grid = true;
                         }
                         else {
                             swal({ title: "Alert", text: "No Documents Found...", width: 300, height: 200 });
                             $scope.Main_table_Service = false;
                             $scope.Main_table = false;
                             $scope.Grid = false;
                         }
                     }
                 });
             }

             $scope.searched = function (valLists, toSearch) {
                 return _.filter(valLists,

                 function (i) {
                     /* Search Text in all  fields */
                     return searchUtil(i, toSearch);
                 });
             };

             //Search
             $scope.search = function () {
                 $scope.todos = $scope.searched($scope.getParameter, $scope.searchText);
                 $scope.totalItems = $scope.todos.length;
                 $scope.currentPage = '1';
                 if ($scope.searchText == '') {
                     $scope.todos = $scope.getParameter;
                 }
                 $scope.makeTodos();
                 main.checked = false;
                 $scope.CheckAllChecked();
             }

             function searchUtil(item, toSearch) {
                 /* Search Text in all 3 fields */
                 return (item.invs_appl_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.invs_appl_form_field.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.invs_appl_parameter.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.invs_appl_form_field_value1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.acno == toSearch) ? true : false;
             }

             $scope.Clear = function () {
                 $scope.im_item_codel = "";
                 $scope.temp.uom_code = "";
                 $scope.temp.reqQty = "";
                 $scope.temp.im_remark = "";
                 $scope.temp.req_remarks = "";
                 $scope.hide_suc = false;
                 $scope.delivery_mode();

                 //$scope.dt.req_date = "";
                 //$scope.temp.dept_code = "";
                 //$scope.temp.dm_code = "";
                 //$scope.temp.salesman_code = "";

             }

             $scope.Cancel = function () {
                 $scope.Clear();
                 $scope.im_inv_no = [];
                 $scope.table = true;
                 $scope.Main_table = false;
                 $scope.Main_table_Service = false;
                 $scope.display = false;
                 $scope.savedisabled = false;
                 $scope.updisabled = true;

                 $scope.temp.req_no = "";
                 //$scope.temp.dept_code = "";
                 //$scope.temp.dm_code = "";
                 $scope.temp.salesman_code = "";
                 $scope.temp.req_remarks = "";
                 $scope.Myform.$setPristine();
                 $scope.Myform.$setUntouched();
                 $scope.hide_suc = false;
                 $state.go('main.Inv058');

             }

             $scope.PrintServiceItemRe = function (str) {

                 $http.get(ENV.apiUrl + "api/RequestDetail/getApproveReport?Request_Type=" + $scope.temp.request_mode_code).then(function (approvereno) {
                     debugger
                     var f = str.req_no;
                     if ($scope.temp.request_type_code == '01') {
                         if ($scope.temp.request_mode_code == 'I') {
                             if ($scope.temp.request_status_code == '2') {
                                 $scope.t = 'Invs.INVR16';
                                 var data = {
                                     location: $scope.t,
                                     parameter: {
                                         req_from: f,
                                         req_to: f,
                                         req_list: 0,
                                     },
                                     state: 'main.Inv205'
                                 }
                             }
                             if ($scope.temp.request_status_code == '0') {
                                 $scope.t = 'Invs.Invr20';
                                 var data = {
                                     location: $scope.t,
                                     parameter: {
                                         Reqno: f,
                                         Reqtype: $scope.temp.request_mode_code,
                                         Subject: "Reprint Transaction",
                                     },
                                     state: 'main.Inv205'
                                 }
                             }
                             //$scope.t = approvereno.data;
                             //var data = {
                             //    location: $scope.t,
                             //    parameter: {
                             //        Reqno: f,
                             //        Reqtype: $scope.temp.request_mode_code,
                             //        Subject: "Reprint Transaction",

                             //    },
                             //    state: 'main.Inv205'
                             //}
                             window.localStorage["ReportDetails"] = JSON.stringify(data)
                             $state.go('main.ReportCardParameter')
                         }

                         else if ($scope.temp.request_mode_code == 'S') {
                             //$scope.t = approvereno.data;
                             if ($scope.temp.request_status_code == '2') {
                                 $scope.t = 'Invs.INVR16';
                                 var data = {
                                     location: $scope.t,
                                     parameter: {
                                         req_from: f,
                                         req_to: f,
                                         req_list: 0,
                                     },
                                     state: 'main.Inv205'
                                 }

                             }
                             if ($scope.temp.request_status_code == '0') {
                                 $scope.t = 'Invs.INVR25';
                                 var data = {
                                     location: $scope.t,
                                     parameter: {
                                         Reqno: f,
                                         Reqtype: $scope.temp.request_mode_code,
                                         Subject: "Reprint Transaction",

                                     },
                                     state: 'main.Inv205'
                                 }
                             }
                             //var data = {
                             //    location: $scope.t,
                             //    parameter: {
                             //        Reqno: f,
                             //        Reqtype: $scope.temp.request_mode_code,
                             //        Subject: "Reprint Transaction",

                             //    },
                             //    state: 'main.Inv205'
                             //}
                             window.localStorage["ReportDetails"] = JSON.stringify(data)
                             $state.go('main.ReportCardParameter')
                         }
                     }
                     else if ($scope.temp.request_type_code == '02') {
                         if ($scope.temp.request_mode_code == 'I') {
                             $scope.t = 'Invs.invr21';
                             var data = {
                                 location: $scope.t,
                                 parameter: {
                                     orderno: f,
                                     //Reqtype: $scope.temp.request_mode_code,
                                     //Subject: "Reprint Transaction",
                                 },
                                 state: 'main.Inv205'
                             }
                             window.localStorage["ReportDetails"] = JSON.stringify(data)
                             $state.go('main.ReportCardParameter')
                         }

                         else if ($scope.temp.request_mode_code == 'S') {
                             $scope.t = approvereno.data;
                             var data = {
                                 location: $scope.t,
                                 parameter: {
                                     orderno: f,
                                     //Reqtype: $scope.temp.request_mode_code,
                                     //Subject: "Reprint Transaction",
                                 },
                                 state: 'main.Inv205'
                             }
                             window.localStorage["ReportDetails"] = JSON.stringify(data)
                             $state.go('main.ReportCardParameter')
                         }
                     }



                     else if ($scope.temp.request_type_code == '03') {

                         $scope.t = 'Invs.INVR27';
                         var data = {
                             location: $scope.t,
                             parameter: {
                                 search: f,
                                 //Reqtype: $scope.temp.request_mode_code,
                                 //Subject: "Reprint Transaction",
                             },
                             state: 'main.Inv205'
                         }
                         window.localStorage["ReportDetails"] = JSON.stringify(data)
                         $state.go('main.ReportCardParameter')
                     }


                     else if ($scope.temp.request_type_code == '04') {

                         $scope.t = 'Invs.INVR28';
                         var data = {
                             location: $scope.t,
                             parameter: {
                                 cs_prov_no: f,
                                 //Reqtype: $scope.temp.request_mode_code,
                                 //Subject: "Reprint Transaction",
                             },
                             state: 'main.Inv205'
                         }
                         window.localStorage["ReportDetails"] = JSON.stringify(data)
                         $state.go('main.ReportCardParameter')
                     }

                 });
             }

             $scope.Transaction_no = function () {

                 if ($scope.temp.request_type_code == '01') {
                     subopr = 'F';
                     $scope.dis_forgc = false;
                     $scope.dis_forgc1 = false;
                 }
                 else if ($scope.temp.request_type_code == '02') {
                     subopr = 'G';
                     $scope.dis_forgc = false;
                     $scope.dis_forgc1 = false;
                 }
                 else if ($scope.temp.request_type_code == '03') {
                     subopr = 'H';
                     $scope.dis_forgc = true;
                     $scope.dis_forgc1 = false;
                 }
                 else if ($scope.temp.request_type_code == '04') {
                     subopr = 'J';
                     $scope.dis_forgc = true;
                     $scope.dis_forgc1 = false;
                 }
                 $http.get(ENV.apiUrl + "api/RequestDetail/getTransactionNo?&subopr=" + subopr + "&req_type=" + $scope.temp.request_mode_code + "&req_mode=" + $scope.temp.request_type_code + "&req_status=" + $scope.temp.request_status_code + "&dept_code=" + $scope.temp.dept_code + "&user_code=" + null + "&from_date=" + $scope.temp.rd_from_required + "&to_date=" + $scope.temp.rd_up_required).then(function (searchtransctionData) {

                     $scope.search_transaction_code = searchtransctionData.data;
                 });

             }

             $scope.Reset = function () {
                 $scope.temp = {
                     request_mode_code: '',
                     request_type_code: '',
                     request_status_code: '',
                     dept_code: '',
                     rd_from_required: '',
                     rd_up_required: '',
                     req_no: '',
                 }
                 $scope.getallvalues();
                 $scope.Main_table_Service = false;
                 $scope.Main_table = false;
                 $scope.Grid = false;
                 $scope.dis_forgc = false;

             }

             $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                 $('input[type="text"]', $(this).parent()).focus();
             });

             $('*[data-datepicker="true"] input[type="text"]').datepicker({
                 todayBtn: true,
                 orientation: "top left",
                 autoclose: true,
                 todayHighlight: true,
                 format: 'yyyy-mm-dd'
             });

         }])

})();
