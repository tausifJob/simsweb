﻿(function () {
    'use strict';
    var opr = '';
    var costingcode = [];
    var main;
    var data1 = [];
    var data = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ManageOrderCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.table1 = true;
            $scope.operation = false;
            $scope.editmode = false;

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['check_all'] = false;
                $scope.row1 = '';
            }

            $scope.searchPO = function () {
                $("#SearchShipmentModal").modal('show');
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.GetAllOrders = function () {
                $http.post(ENV.apiUrl + "api/AdjustmentReason/Order_getAll").then(function (res) {
                    console.log(res);
                    $scope.OD = res.data.table;
                    if ($scope.OD.length == 0) {
                        alert('Records not found');
                    }
                });
            }

            $scope.getOrdersDetails = function (row) {
                row.orderNO = row.ord_no;
                $http.post(ENV.apiUrl + "api/AdjustmentReason/OrderDET_getAll", row).then(function (res) {
                    $scope.rows = res.data.table;
                    var Allrows = res.data.table1;
                    for (var r in $scope.rows) {
                        $scope.rows[r]['sims_icon'] = "fa fa-plus-circle";
                        $scope.rows[r]['isexpanded'] = "none";
                        $scope.rows[r]['showClose'] = false;
                        $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].req_no, Allrows, 'req_no');
                    }
                    $scope.tob = $scope.rows[0]['subItems'][0];
                });
                $("#SearchShipmentModal").modal('hide');
            }

            function getSubitems(dno, a, v) {
                var arr = [];
                for (var i = 0; i < a.length; i++) {
                    if (a[i][v] == dno) {
                        arr.push(a[i]);
                    }
                }
                return arr;
            }

            $scope.expand = function (j, $event) {
                if (j.isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.isexpanded = "grid";
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.isexpanded = "none";
                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.addRequest = function () {
                $("#SearchRequestModal").modal('show');
            }

            $scope.GetAllRequest = function () {
                $http.post(ENV.apiUrl + "api/AdjustmentReason/Request_getAll").then(function (res) {
                    console.log(res);
                    $scope.RD = res.data.table;
                    if ($scope.RD.length == 0) {
                        alert('Records not found');
                    }
                });
            }

            $scope.getReqDetails = function (row) {
                row.orderNO = row.req_no;
                $http.post(ENV.apiUrl + "api/AdjustmentReason/RequestDET_getAll", row).then(function (res) {
                    var rowsx = res.data.table;
                    var Allrowsx = res.data.table1;
                    for (var r in rowsx) {
                        rowsx[r]['sims_icon'] = "fa fa-plus-circle";
                        rowsx[r]['isexpanded'] = "none";
                        rowsx[r]['showClose'] = true;
                        rowsx[r]['subItems'] = getSubitems(rowsx[r].req_no, Allrowsx, 'req_no');
                        $scope.rows.push(rowsx[r]);
                    }
                });
                $("#SearchRequestModal").modal('hide');
            }

            $scope.clear = function () {
                $scope.RD = [];
                $scope.rows = [];
                $scope.tob = [];
            }

            $scope.removeRequest = function (r, b) {
                r.showClose && Array.isArray(b) ? c() : false;
                function c() {
                    var x = false;
                    for (var i = 0; i < b.length; i++) {
                        if (b[i].req_no == r.req_no && b[i].showClose) {
                            b.splice(i, 1);
                            x = true;
                            break;
                        }
                    }
                    return x;
                }
            }
        }])
})();