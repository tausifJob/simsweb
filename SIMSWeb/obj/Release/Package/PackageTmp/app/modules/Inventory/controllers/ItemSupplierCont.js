﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ItemSupplierCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
     
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.itemsup_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.edit_code = false;

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $http.get(ENV.apiUrl + "api/common/ItemSupplier/GetAllSuppliers").then(function (res) {
                $scope.sup_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/ItemSupplier/GetAllItems").then(function (res) {
                $scope.item_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/ItemSupplier/GetAllCurrency").then(function (res) {
                $scope.curr_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/ItemSupplier/GetAllUOM").then(function (res) {
                $scope.uom_data = res.data;
            });

            $scope.Getiteminfo = function (item_code)
            {
                if (item_code != undefined)
                {
                    for (var i = 0; i < $scope.item_data.length; i++)
                    {
                        if (item_code == $scope.item_data[i].im_inv_no)
                        {
                            $scope.edt.item_name = $scope.item_data[i].im_inv_no + '-' + $scope.item_data[i].item_name;
                        }
                    }
                }
                else {
                    $scope.edt.item_name = "";
                }
            }

            $http.get(ENV.apiUrl + "api/common/ItemSupplier/Get_ItemSupplier").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.itemsup_data = res.data;
                $scope.totalItems = $scope.itemsup_data.length;
                $scope.todos = $scope.itemsup_data;
                $scope.makeTodos();
                $scope.grid = true;
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0')
                {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else
                {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edit = function (str)
            {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edit_code = true;

                console.log(str);
                
                $scope.edt = 
                    {
                        sup_code: str.sup_code,
                        im_inv_no: str.im_inv_no,
                        is_uom: str.is_uom,
                        is_cur_code: str.is_cur_code,
                        is_price: str.is_price,
                        is_price_date: str.is_price_date,
                        is_economic_order_qty: str.is_economic_order_qty,
                    }

                $scope.Getiteminfo(str.im_inv_no);
            }

            //function convertdate(dt) {
            //    if (dt != null) {
            //        var d1 = new Date(dt);
            //        var month = d1.getMonth() + 1;
            //        var day = d1.getDate();
            //        if (month < 10)
            //            month = "0" + month;
            //        if (day < 10)
            //            day = "0" + day;
            //        var d = d1.getFullYear() + "-" + (month) + "-" + (day);
            //        return d;
            //    }
            //}


            $scope.Save = function (isvalidate)
            {
                console.log($scope.edt);
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = ({
                            sup_code: $scope.edt.sup_code,
                            im_inv_no: $scope.edt.im_inv_no,
                            is_uom: $scope.edt.is_uom,
                            is_cur_code: $scope.edt.is_cur_code,
                            is_price: $scope.edt.is_price,
                            is_price_date: $scope.edt.is_price_date,
                            is_economic_order_qty: $scope.edt.is_economic_order_qty,
                            opr: 'I'
                        });

                        data1.push(data);


                        $http.post(ENV.apiUrl + "api/common/ItemSupplier/CUDItemSupplier", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Item Supplier Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal({ title: "Alert", text: "Item Supplier Already Exists", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                        });
                    }
                    else
                    {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function ()
            {
                $scope.currentPage = 1;

                $http.get(ENV.apiUrl + "api/common/ItemSupplier/Get_ItemSupplier").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.itemsup_data = res.data;
                    $scope.totalItems = $scope.itemsup_data.length;
                    $scope.todos = $scope.itemsup_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                });

                main = document.getElementById('mainchk');
                if (main.checked == true)
                {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate)
            {
                var data1 = [];
                if (isvalidate)
                {
                            var data = ({
                                sup_code: $scope.edt.sup_code,
                                im_inv_no: $scope.edt.im_inv_no,
                                is_uom: $scope.edt.is_uom,
                                is_cur_code: $scope.edt.is_cur_code,
                                is_price: $scope.edt.is_price,
                                is_price_date:$scope.edt.is_price_date,
                                is_economic_order_qty: $scope.edt.is_economic_order_qty,
                                opr: 'U'
                            });

                            data1.push(data);

                            $http.post(ENV.apiUrl + "api/common/ItemSupplier/ItemSupplier_updatedel", data1).then(function (res) {
                                $scope.display = true;
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true)
                                {
                                    swal({ title: "Alert", text: "Item Supplier Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm)
                                        {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                                else
                                {
                                    swal({ title: "Alert", text: "Item Supplier Not Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm)
                                        {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                            });
                }
            }

            $scope.Delete = function ()
            {
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++)
                {
                    var v = document.getElementById(i);

                    if (v.checked == true)
                    {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sup_code': $scope.filteredTodos[i].sup_code,
                            'im_inv_no': $scope.filteredTodos[i].im_inv_no,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/common/ItemSupplier/ItemSupplier_updatedel", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Item Supplier Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm)
                                        {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Item Supplier Not Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }

                            });
                        }
                        else
                        {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else
                {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                    
            }

            $scope.New = function ()
            {
                $scope.edit_code = false;
                var autoid;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = [];
            }

            $scope.cancel = function ()
            {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function ()
            {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true)
                {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.itemsup_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.itemsup_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.im_item_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sup_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.item_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format:"yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
      
        }])
})();