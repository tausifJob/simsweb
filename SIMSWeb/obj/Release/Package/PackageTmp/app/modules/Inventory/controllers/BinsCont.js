﻿(function () {
    'use strict';
    var opr = '';
    var data1 = [];
    var main, edt;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('BinsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.edt = "";
            $scope.BinsDetail = true;
            $scope.editmode = false;
            var formdata = new FormData();

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.cancel = function () {
                $scope.BinsDetail = true;
                $scope.BinssData = false;
                $scope.bin_desc = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                opr = 'S';
                $scope.editmode = false;
                $scope.edt = "";
                $scope.readonly = false;
                $scope.BinsDetail = false;
                $scope.BinssData = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();


            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                console.log("begin=" + begin); console.log("end=" + end);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.BinsData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.BinsData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.check_all();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                    item.bin_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.bin_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.bin_desc == toSearch) ? true : false;

            }

            $http.get(ENV.apiUrl + "api/BinsDetail/getBinsDetail").then(function (Bins_Data) {
                $scope.BinsData = Bins_Data.data;
                $scope.totalItems = $scope.BinsData.length;
                $scope.todos = $scope.BinsData;
                $scope.makeTodos();
                console.log($scope.BinsData);
            });

            $scope.Save = function (myForm) {
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'I';
                    $scope.edt = "";
                    $scope.exist = false;
                    data1.push(data);
                    for (var i = 0; i < $scope.BinsData.length; i++) {
                        if ($scope.BinsData[i].bin_code == data.bin_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ title: "Alert", text: "Record Already exists", width: 300, height: 200 });
                    }

                    else {
                        $http.post(ENV.apiUrl + "api/BinsDetail/BinsCUD?simsobj=", data1).then(function (msg) {
                            $scope.BinssData = false;
                            $http.get(ENV.apiUrl + "api/BinsDetail/getBinsDetail").then(function (Bins_Data) {
                                $scope.BinsData = Bins_Data.data;
                                $scope.totalItems = $scope.BinsData.length;
                                $scope.todos = $scope.BinsData;
                                $scope.makeTodos();
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                                }
                            });
                        });
                        $scope.BinsDetail = true;
                        $scope.BinssData = false;
                    }
                    data1 = [];
                }
            }

            $scope.up = function (str) {
                opr = 'U';
                $scope.editmode = true;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.BinsDetail = false;
                $scope.BinssData = true;
                $scope.readonly = true;
                $scope.edt = {
                    bin_code: str.bin_code
                              , bin_desc: str.bin_desc

                };

            }

            $scope.Update = function (myForm) {

                if (myForm) {

                    var data = $scope.edt;
                    data.opr = 'U';

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/BinsDetail/BinsCUD?simsobj=", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.BoardData1 = false;
                        $http.get(ENV.apiUrl + "api/BinsDetail/getBinsDetail").then(function (Bins_Data) {
                            $scope.BinsdData = Bins_Data.data;
                            $scope.totalItems = $scope.BinsdData.length;
                            $scope.todos = $scope.BinsdData;
                            formdata = new FormData();
                            $scope.makeTodos();

                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                            }
                        });

                    })
                    $scope.BinssData = false;
                    $scope.BinsDetail = true;

                }
            }

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.check_once = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }

            $scope.deleterecord = function () {
                debugger;
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'bin_code': $scope.filteredTodos[i].bin_code,
                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        confirmButtonText: 'Yes',
                        showCancelButton: true,
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/BinsDetail/BinsCUD", deleteleave).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BinsDetail/getBinsDetail").then(function (Bins_Data) {
                                                $scope.BinsData = Bins_Data.data;
                                                $scope.totalItems = $scope.BinsData.length;
                                                $scope.todos = $scope.BinsData;
                                                $scope.makeTodos();
                                            });

                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                        }

                                        $scope.currentPage = true;
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BinsDetail/getBinsDetail").then(function (Bins_Data) {
                                                $scope.BinsData = Bins_Data.data;
                                                $scope.totalItems = $scope.BinsData.length;
                                                $scope.todos = $scope.BinsData;
                                                $scope.makeTodos();
                                            });

                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                        }


                                    });
                                }
                            });
                        }
                        else {
                            main = document.getElementById('all_chk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    // main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }

                $scope.currentPage = str;
            }


        }]);

})();



