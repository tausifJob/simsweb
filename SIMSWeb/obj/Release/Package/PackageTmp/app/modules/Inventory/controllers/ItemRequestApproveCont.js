﻿(function () {
    'use strict';
    var obj1, temp, opr, CurrentDate, comp;
    var main, deletefin = [], data = [], data1 = [], ItemDetails = [];
    var finanacecode = [];
    var check;
    var reqno;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ItemRequestApproveCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {
            $scope.table = true;

            //$http.get(ENV.apiUrl + "api/CreateOrder/getRequestTypeNew").then(function (reqtypedetailsNew) {
            //    $scope.req_type_detailsNew = reqtypedetailsNew.data;
            //    $scope.temp['request_mode_code'] = $scope.req_type_detailsNew[0].request_mode_code;

            //});

            $scope.hide_save = false;
            $scope.hide_saves = false;

            $scope.IP = {};
            $scope.sel = {};
            $scope.req_stat = '01';

            //$http.get(ENV.apiUrl + "api/CreateOrder/getDeparments_approve").then(function (deptdata) {
            //    $scope.dept_data = deptdata.data;
            //    $scope.IP['dept_code'] = $scope.dept_data[0].dept_code;
            //    console.log($scope.dept_data);
            //});

            //var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd');
            //$scope.IP = {
            //    rd_from_required: dateyear,
            //    rd_up_required: dateyear,
            //}
            $scope.Getreqstat = function (str) {
                console.log(str);

                //if (str == '01') {
                //    $scope.hide_saves = true;
                //    $scope.hide_saves1 = false;
                //}
                //if (str == '02') {
                //    $scope.hide_saves1 = true;
                //    $scope.hide_saves = false;
                //}
            }

            $http.get(ENV.apiUrl + "api/CreateOrder/getRequestTypeNew").then(function (reqtypedetailsNew) {
                $scope.req_type_detailsNew = reqtypedetailsNew.data;
                $scope.temp['request_mode_code'] = $scope.req_type_detailsNew[0].request_mode_code;
            });

            $scope.SelectIetm = function () {
                debugger;
                $scope.IP.loginuser = $rootScope.globals.currentUser.username;
                $scope.IP.request_issuestatus = $scope.req_stat;
                $http.get(ENV.apiUrl + "api/ItemRequests/getRequestDetailsforapprove?data=" + JSON.stringify($scope.IP)).then(function (res) {
                    debugger;
                    $scope.rows = res.data.table;
                    $scope.Allrows = res.data.table1;
                    if ($scope.Allrows.length <= 0) {
                        // swal({ title: "Alert", text: "Records not found", showCloseButton: true, width: 380, });
                    }
                    else {
                        for (var r in $scope.rows) {
                            $scope.rows[r]['sims_icon'] = "fa fa-minus-circle";
                            $scope.rows[r]['isexpanded'] = "grid";
                            console.log($scope.rows[r].doc_prov_no);
                            $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].doc_prov_no);
                        }
                        $scope.hide_save = false;
                        $scope.hide_saves = true;
                        console.clear();
                    }

                    console.log(res.data.table);
                    console.log(res.data.table1);

                    if ($scope.req_stat == '01') {
                        $scope.hide_saves = true;
                        $scope.hide_saves1 = false;
                    }
                    if ($scope.req_stat == '02') {
                        $scope.hide_saves1 = true;
                        $scope.hide_saves = false;
                    }
                    if ($scope.req_stat == '03') {
                        $scope.hide_saves = true;
                        $scope.hide_saves1 = false;
                    }
                    if ($scope.req_stat == '04') {
                        $scope.hide_saves = true;
                        $scope.hide_saves1 = false;
                    }
                });
            }

            function getSubitems(dno) {
                debugger;
                var arr = [];

                for (var i = 0; i < $scope.Allrows.length; i++) {
                    console.log($scope.Allrows[i].doc_prov_no);
                    console.log(dno);
                    if ($scope.Allrows[i].doc_prov_no == dno) {
                        arr.push($scope.Allrows[i]);
                    }
                }
                return arr;
            }

            $scope.expand = function (j, $event) {
                if (j.isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.isexpanded = "grid";
                    console.log(j);
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.isexpanded = "none";
                }
            }

            //$scope.reset = function () {
            //    $scope.IP = {};
            //    $scope.rows = {};
            //    $scope.Allrows = {};
            //}

            $scope.checkRange = function (s) {
                var rq = parseInt(s.final_qnt);
                if (rq == NaN)
                    rq = 0;
                s.final_qnt = rq;
            }

            $scope.ordclick = function (gh) {
                var l = gh.subItems.length;
                for (var i = 0; i < l; i++) {
                    gh.subItems[i]['isChecked'] = gh.isChecked;
                }
            }

            $scope.ordlineclick = function (r, m) {
                var l = r.subItems.length;
                var f = false;
                if (m) {
                    f = true;
                    for (var i = 0; i < l; i++) {
                        f = f && r.subItems[i]['isChecked'];
                    }
                }
                r.isChecked = f;
            }

            $http.get(ENV.apiUrl + "api/AdjustmentReason/getDept").then(function (res) {
                $scope.sel['Dept'] = res.data.table;
            });

            $scope.save = function (s) {
                $scope.hide_save = true;
                $scope.hide_saves = true;
                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {
                    debugger;
                    for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                        if ($scope.rows[i].subItems[j].isChecked) {
                            var ob = {};
                            ob.od_order_qty = $scope.rows[i].subItems[j].dd_qty;
                            ob.final_qnt = $scope.rows[i].subItems[j].final_qnt;
                            ob.im_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.od_line_no = $scope.rows[i].subItems[j].dd_line_no;
                            ob.ord_no = $scope.rows[i].subItems[j].doc_prov_no;
                            ob.req_type = $scope.rows[i].subItems[j].req_type;
                            ob.req_no = $scope.rows[i].subItems[j].req_no;
                            ob.loguser = $rootScope.globals.currentUser.username;
                            ob.sims_workflow_no = $scope.rows[i].subItems[j].sims_workflow_no;
                            ob.reqcreatedby = $scope.rows[i].subItems[j].reqcreatedby;
                            //ob.adj_status = $scope.rows[i].subItems[j].adj_status;
                            ob.val = r;
                            ar.push(ob);
                        }
                    }
                }
                if (ar.length == 0) {
                    alert("Nothing Selected");
                    return;
                }

                $http.post(ENV.apiUrl + "api/ItemRequests/UpdateRequestDetailsforapprove", ar).then(function (res) {
                    console.log(res);

                    //swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });
                    //$scope.SelectIetm();
                    debugger;
                    //swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                    swal({ title: "Alert", text: "Request Approved Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {

                            $scope.Reset();
                        }
                    });

                });

            }

            $scope.reject = function (s) {
                $scope.hide_save = true;
                $scope.hide_saves = true;
                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {
                    debugger;
                    for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                        if ($scope.rows[i].subItems[j].isChecked) {
                            var ob = {};
                            ob.od_order_qty = $scope.rows[i].subItems[j].dd_qty;
                            ob.final_qnt = $scope.rows[i].subItems[j].final_qnt;
                            ob.im_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.od_line_no = $scope.rows[i].subItems[j].dd_line_no;
                            ob.ord_no = $scope.rows[i].subItems[j].doc_prov_no;
                            ob.req_type = $scope.rows[i].subItems[j].req_type;
                            ob.req_no = $scope.rows[i].subItems[j].req_no;
                            ob.loguser = $rootScope.globals.currentUser.username;
                            ob.sims_workflow_no = $scope.rows[i].subItems[j].sims_workflow_no;
                            ob.reqcreatedby = $scope.rows[i].subItems[j].reqcreatedby;
                            //ob.adj_status = $scope.rows[i].subItems[j].adj_status;
                            ob.val = r;
                            ar.push(ob);
                        }
                    }
                }
                if (ar.length == 0) {
                    alert("Nothing Selected");
                    return;
                }

                $http.post(ENV.apiUrl + "api/ItemRequests/UpdateRequestDetailsforreject", ar).then(function (res) {
                    console.log(res);

                    //swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });
                    //$scope.SelectIetm();

                    swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.Reset();
                        }
                    });

                });

            }

            $scope.Issueitems = function (s) {
                $scope.hide_save = true;
                $scope.hide_saves = true;
                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {
                    debugger;
                    for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                        if ($scope.rows[i].subItems[j].isChecked) {
                            var ob = {};
                            ob.od_order_qty = $scope.rows[i].subItems[j].dd_qty;
                            ob.final_qnt = $scope.rows[i].subItems[j].final_qnt;
                            ob.im_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.od_line_no = $scope.rows[i].subItems[j].dd_line_no;
                            ob.ord_no = $scope.rows[i].subItems[j].doc_prov_no;
                            ob.req_type = $scope.rows[i].subItems[j].req_type;
                            ob.req_no = $scope.rows[i].subItems[j].req_no;
                            ob.loguser = $rootScope.globals.currentUser.username;
                            ob.sims_workflow_no = $scope.rows[i].subItems[j].sims_workflow_no;
                            ob.reqcreatedby = $scope.rows[i].subItems[j].reqcreatedby;
                            //ob.adj_status = $scope.rows[i].subItems[j].adj_status;
                            ob.val = r;
                            ar.push(ob);
                        }
                    }
                }
                if (ar.length == 0) {
                    alert("Nothing Selected");
                    return;
                }

                $http.post(ENV.apiUrl + "api/ItemRequests/UpdateRequestDetailstoissue", ar).then(function (res) {
                    console.log(res.data);
                    debugger;
                    if (res.data != null && res.data != '') {
                        swal({
                            title: '',
                            text: "Sales Receipts " + res.data.substring(1) + "  issued successfully for selected requests.Do you want to print receipt?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',
                            allowOutsideClick: false,
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                var data = {
                                    location: 'Invs.INVR02DPSMIS',
                                    parameter: {
                                        doc_prov_no: res.data.substring(1),
                                    },
                                    state: 'main.ApIREQ',
                                    ready: function () {
                                        this.refreshReport();
                                    },
                                }
                                console.log(data);

                                window.localStorage["ReportDetails"] = JSON.stringify(data);
                                $state.go('main.ReportCardParameter');
                            }
                        });
                    }
                    //swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });
                    //$scope.SelectIetm();

                    //swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                    //swal({ title: "Alert", text: "Request Approved Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                    //    if (isConfirm) {

                    //        if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                    //            //var data = {
                    //            //    location: 'Invs.INVR02DPSMIS',
                    //            //    parameter: { doc_prov_no: d },
                    //            //    state: 'main.Inv035'
                    //            //}

                    //            var data = {
                    //                doc_prov_no: res.data,
                    //                sdate: '',
                    //                edate: '',
                    //                state: false,
                    //                doc_status: '',
                    //                back_state: 'main.Inv035'
                    //            }

                    //        }

                    //        else if ($http.defaults.headers.common['schoolId'] == 'brs') {
                    //            var data = {
                    //                location: 'Invs.INVR02BRS',
                    //                parameter: { doc_prov_no: res.data },
                    //                state: 'main.Inv035'
                    //            }
                    //        }
                    //        else {
                    //            var data = {
                    //                location: 'Invs.INVR02',
                    //                parameter: { doc_prov_no: res.data },
                    //                state: 'main.Inv035'
                    //            }
                    //        }
                    //        //$scope.disable_btn = false;
                    //        if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {

                    //            window.localStorage["ReportProvNumber"] = JSON.stringify(data)
                    //            $state.go('main.rpts')
                    //        }
                    //        else {

                    //            window.localStorage["ReportDetails"] = JSON.stringify(data)
                    //            $state.go('main.ReportCardParameter')
                    //        }

                    //        
                    //    }
                    //});
                    $scope.Reset();
                });

            }

            $scope.Reset = function () {
                $scope.IP = {
                    rd_from_required: '',
                    rd_up_required: '',
                    req_no: '',
                    req_range_no: '',
                    request_mode_code: '',
                    rd_item_desc: '',
                    loginuser: '',

                }
                $scope.req_stat = '01';
                $scope.SelectIetm();
            }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            })

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

        }])

})();
