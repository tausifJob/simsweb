﻿(function () {
    'use strict';
    var del = [];
    var main, lc_no1 = null;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('Inv138Controller',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.getLetterOfCredit = function () {
                $http.get(ENV.apiUrl + "api/common/LetterofCredits/getLetterOfCreditByIndex").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.obj = res.data;
                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.makeTodos();
                });
            }
            $scope.getLetterOfCredit();

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                $scope.selectedAll = false;
                $scope.isSelectAll();
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            ////Search
            $scope.searchLOC = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.cur_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sup_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.lc_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.sup_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.lc_bank_lc_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.lc_exchange_rate.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.lc_open_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || lc_no1 == toSearch) ? true : false;
            }


            $http.get(ENV.apiUrl + "api/common/LetterofCredits/getSupplierName").then(function (getSupplierName) {
                $scope.Supplier_Name = getSupplierName.data;

            });
            $http.get(ENV.apiUrl + "api/common/LetterofCredits/getBankName").then(function (getBankName) {
                $scope.Bank_Name = getBankName.data;

            });

            $http.get(ENV.apiUrl + "api/common/LetterofCredits/getCurrency").then(function (getCurrency) {
                $scope.Currency = getCurrency.data;

            });


            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.edt = {
                    bk_code: str.bk_code,
                    lc_date: str.lc_date,
                    excg_curcy_code: str.cur_code,
                    lc_exchange_rate: str.lc_exchange_rate,
                    sup_code: str.sup_code,
                    lc_bank_lc_no: str.lc_bank_lc_no,
                    lc_open_date: str.lc_open_date,
                    lc_beneficiary: str.lc_beneficiary,
                    lc_amount: str.lc_amount,
                    lc_revised_amount: str.lc_revised_amount,
                    lc_utilized_amount: str.lc_utilized_amount,
                    lc_cancelled_amount: str.lc_cancelled_amount,
                    lc_bank_charges: str.lc_bank_charges,
                    lc_valid_date: str.lc_valid_date,
                    lc_terms: str.lc_terms,
                    lc_remarks: str.lc_remarks,
                    lc_close_date: str.lc_close_date,
                    lc_no1: str.lc_no1
                }

            }

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.edt = {};
                $scope.edt.excg_curcy_code = "";

                $http.get(ENV.apiUrl + "api/common/LetterofCredits/getLCNO").then(function (getLCNO) {
                    $scope.LC_NO = getLCNO.data;
                    var edt = document.getElementById('txt_Lc_no1');
                    var lcNo = parseInt($scope.LC_NO[0].lc_no1);
                    if (isNaN(lcNo) || lcNo == "" || lcNo == undefined) {
                        edt.value = 1;
                    }
                    else {
                        edt.value = lcNo + 1;
                    }

                });
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            $scope.Save = function (myForm) {
                if (myForm) {
                    $scope.edt.opr = "I";
                    $scope.edt.lc_bank_charges = parseFloat($scope.edt.lc_bank_charges);
                    $scope.edt.lc_exchange_rate = parseFloat($scope.edt.lc_exchange_rate);
                    $scope.edt.lc_utilized_amount = parseFloat($scope.edt.lc_utilized_amount);
                    $scope.edt.lc_cancelled_amount = parseFloat($scope.edt.lc_cancelled_amount);
                    $scope.edt.lc_revised_amount = parseFloat($scope.edt.lc_revised_amount);
                    $scope.edt.lc_amount = parseFloat($scope.edt.lc_amount);

                    $http.post(ENV.apiUrl + "api/common/LetterofCredits/CUDLetterOfCreditsDetails", $scope.edt).then(function (res) {
                        $scope.display = true;
                        $scope.getLetterOfCredit();
                        $scope.msg1 = res.data;
                        swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 380, });
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();
                    });

                    //$http.post(ENV.apiUrl + "api/common/LetterofCredits/CUDLetterOfCreditsDetails?data=" + JSON.stringify($scope.edt) + "&opr=" + "I").then(function (res) {
                    //    $scope.display = true;
                    //    console.log(res);
                    //    $scope.obj = res.data;
                    //});
                }
            }

            $scope.Update = function (myForm) {
                if (myForm) {
                    $scope.edt.opr = "U";
                    $scope.edt.lc_bank_charges = parseFloat($scope.edt.lc_bank_charges);
                    $scope.edt.lc_exchange_rate = parseFloat($scope.edt.lc_exchange_rate);
                    $scope.edt.lc_utilized_amount = parseFloat($scope.edt.lc_utilized_amount);
                    $scope.edt.lc_cancelled_amount = parseFloat($scope.edt.lc_cancelled_amount);
                    $scope.edt.lc_revised_amount = parseFloat($scope.edt.lc_revised_amount);
                    $scope.edt.lc_amount = parseFloat($scope.edt.lc_amount);
                    $http.post(ENV.apiUrl + "api/common/LetterofCredits/CUDLetterOfCreditsDetails", $scope.edt).then(function (msg) {
                        $scope.msg1 = msg.data;
                        swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 380, });
                        $scope.display = false;
                        $scope.grid = true;
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();
                        $scope.getLetterOfCredit();
                    });

                    //$http.post(ENV.apiUrl + "api/common/LetterofCredits/CUDLetterOfCreditsDetails?data=" + JSON.stringify($scope.edt) + "&opr=U").then(function (msg) {
                    //    $scope.msg1 = msg.data;
                    //    $('#message').modal('show');
                    //    $scope.display = false;
                    //    $scope.grid = true;
                    //});
                }
            }

            $scope.deleteList = [];

            $scope.isSelectAll = function () {
                $scope.deleteList = [];
                if ($scope.selectedAll) {
                    $scope.selectedAll = true;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.deleteList.push($scope.filteredTodos[i].lc_no1);
                    }
                    $scope.row1 = 'row_selected';
                }
                else {
                    $scope.selectedAll = false;
                    $scope.row1 = '';
                }

                angular.forEach($scope.filteredTodos, function (item) {
                    item.selected = $scope.selectedAll;
                });

            }

            $scope.checkIfAllSelected = function () {
                var id = event.target.id;
                var value = event.target.value;
                if (document.getElementById(id).checked) {
                    $scope.deleteList.push(value);
                    if ($scope.deleteList.length == $scope.filteredTodos.length) {
                        $scope.selectedAll = true;
                    }
                    $("#" + id).closest('tr').addClass("row_selected");
                } else {
                    $scope.selectedAll = false;
                    $scope.row1 = '';
                    var index = $scope.deleteList.indexOf(value);
                    $scope.deleteList.splice(index, 1);
                    $("#" + id).closest('tr').removeClass("row_selected");
                }
            };

            $scope.OkDelete = function () {
                if ($scope.deleteList.length == 0) {
                    swal({ title: "", text: "Please select record", showCloseButton: true, width: 380, });
                }
                else {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            angular.forEach($scope.deleteList, function (val, key) {
                                var obj = { lc_no1: val, opr: 'D' };
                                $http.post(ENV.apiUrl + "api/common/LetterofCredits/CUDLetterOfCreditsDetails", obj).then(function (res) {
                                    $scope.msg1 = res.data;
                                    swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getLetterOfCredit();
                                            $scope.deleteList = [];
                                        }
                                    });
                                })
                            });
                        }
                    });

                }

            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            var dom;
            var count = 0;
            $scope.flag = true;

            $scope.expand = function (info, $event) {
                var iconId = $event.target.id;
                if ($scope.flag == true) {
                    $(dom).remove();
                    $("#" + iconId).addClass("fa fa-minus-circle");

                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                         "<tr> <td class='semi-bold'>" + "LC BENEFICIARY" + "</td> <td class='semi-bold'>" + "LC AMOUNT" + " </td><td class='semi-bold'>" + "LC REVISED AMOUNT" + "</td><td class='semi-bold'>" + "LC UTILIZED AMOUNT" + "</td><td class='semi-bold'>" + "LC CANCELLED AMOUNT" + " </td>" +
                        "<td class='semi-bold'>" + "LC BANK CHARGES" + "</td><td class='semi-bold'>" + "LC VALID DATE" + "</td></tr>" +

                          "<tr><td>" + (info.lc_beneficiary) + "</td> <td>" + (info.lc_amount) + " </td><td>" + (info.lc_revised_amount) + "</td><td>" + (info.lc_utilized_amount) + "</td><td>" + (info.lc_cancelled_amount) + "</td>" +
                        "<td>" + (info.lc_bank_charges) + "</td><td>" + (info.lc_valid_date) + "</td></tr>" +

                        "<tr> <td class='semi-bold'>" + "LC TERMS" + "</td> <td class='semi-bold'>" + "LC REMARKS" + " </td><td class='semi-bold'>" + "LC CLOSE DATE" + "</td></tr>" +
                        "<tr><td>" + (info.lc_terms) + "</td> <td>" + (info.lc_remarks) + " </td><td>" + (info.lc_close_date) + "</td></tr>" +

                        " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    $("#" + iconId).removeClass("fa fa-minus-circle");
                    $("#" + iconId).addClass("fa fa-plus-circle");
                    $scope.flag = true;
                }

            };

        }])
})();