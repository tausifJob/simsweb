﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('SupplierDescriptionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/SupplierDescription/getAllSupplierDescription").then(function (res1) {

                $scope.SupplierDescriptionData = res1.data;
                $scope.totalItems = $scope.SupplierDescriptionData.length;
                $scope.todos = $scope.SupplierDescriptionData;
                $scope.makeTodos();

            });

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                //$scope.CheckAllChecked();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Bind Combo

            $http.get(ENV.apiUrl + "api/SupplierDescription/getSupplierCode").then(function (res1) {

                $scope.SupplierData = res1.data;              
            });


            $http.get(ENV.apiUrl + "api/SupplierDescriptionTypes/getAllSupplierDescriptionTypes").then(function (res1) {

                $scope.SupplierTypeData = res1.data;
            });
            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.SupplierDescriptionData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.SupplierDescriptionData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sup_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sdt_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sd_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {

                $scope.disabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = [];
                $scope.temp.sup_code = "";
                $scope.temp.sup_name = ""; 
                $scope.sdt_type = "";
                $scope.sdt_desc = "";
                $scope.temp.sd_desc = "";
                $scope.sd_desc = "";
                $scope.sdttypecodeReadonly = false;
                $scope.supcodeReadonly = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = [];
                $scope.table = true;
                $scope.display = false;
                $scope.sdt_type = "";
                $scope.sdt_desc = "";
                $scope.temp.sdt_type = "";
                $scope.temp.sdt_desc = "";
                $scope.temp.sup_code = "";
                $scope.temp.sup_name = "";
                $scope.sdt_type = "";
                $scope.sdt_desc = "";
                $scope.temp.sd_desc = "";
                $scope.sd_desc = "";
                $scope.sdttypecodeReadonly = false;
                $scope.supcodeReadonly = false;
              
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {

                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.sdttypecodeReadonly = true;
                $scope.supcodeReadonly = true;
                $scope.temp = {
                    sup_code:str.sup_code,
                    sdt_type: str.sdt_type,
                    sd_desc: str.sd_desc
                };

            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger;
                if (Myform) {
                    datasend = [];
                    data = [];
                    // var data = $scope.temp;
                    var data = {
                        sup_code:$scope.temp.sup_code,
                        sdt_type: $scope.temp.sdt_type,
                        sd_desc: $scope.temp.sd_desc
                       
                    }
                    data.opr = 'I';
                    datasend.push(data);

                    $http.post(ENV.apiUrl + "api/SupplierDescription/CUDSupplierDescription", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Inserted Successfully", width: 380 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record allready present", width: 380 });
                        }
                        $scope.getgrid();
                    });

                    $scope.table = true;
                    $scope.display = false;
                }

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {

                if (Myform) {
                    dataforUpdate = [];
                    var data = {
                        sup_code: $scope.temp.sup_code,
                        sdt_type: $scope.temp.sdt_type,
                        sd_desc: $scope.temp.sd_desc
                    }
                    data.opr = "U";
                    dataforUpdate.push(data);
                    //dataupdate.push(data);
                    $http.post(ENV.apiUrl + "api/SupplierDescription/CUDSupplierDescription", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 380 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 380 });
                        }
                        $scope.getgrid();
                    });
                    dataforUpdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/SupplierDescription/getAllSupplierDescription").then(function (res1) {

                    $scope.SupplierDescriptionData = res1.data;
                    $scope.totalItems = $scope.SupplierDescriptionData.length;
                    $scope.todos = $scope.SupplierDescriptionData;
                    $scope.makeTodos();

                });
            }

            $scope.Delete = function () {

                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    //var t = $scope.filteredTodos[i].uom_code;
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sdt_type': $scope.filteredTodos[i].sdt_type, 
                            'sup_code': $scope.filteredTodos[i].sup_code,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {

                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/SupplierDescription/CUDSupplierDescription", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Already Mapped.Can't be Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }

                            });
                        }
                        else {

                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                //var t = $scope.filteredTodos[i].sims_fee_category;
                                
                                if (v.checked == true) {
                                    v.checked = false;                                   
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }


                            }

                        }

                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
               
                $scope.currentPage = str;
            }



            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                formate: "yyyy-mm-dd"
            });



        }])

})();
