﻿(function () {
    'use strict';
    var obj1, temp, opr, CurrentDate, comp;
    var main, deletefin = [], data = [], data1 = [], ItemDetails = [];
    var finanacecode = [];
    var check;
    var reqno;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('OrderApproveCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {
            $scope.table = true;

            $http.get(ENV.apiUrl + "api/CreateOrder/getRequestTypeNew").then(function (reqtypedetailsNew) {
                $scope.req_type_detailsNew = reqtypedetailsNew.data;
                $scope.temp['request_mode_code'] = $scope.req_type_detailsNew[0].request_mode_code;

            });

            $scope.hide_save = false;
            $scope.hide_saves = false;

            $scope.IP = {};
            $scope.sel = {};

            $http.get(ENV.apiUrl + "api/CreateOrder/getDeparments_approve").then(function (deptdata) {
                $scope.dept_data = deptdata.data;
                $scope.IP['dept_code'] = $scope.dept_data[0].dept_code;
                console.log($scope.dept_data);
            });

            //var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd');
            //$scope.IP = {
            //    rd_from_required: dateyear,
            //    rd_up_required: dateyear,
            //}


            $scope.SelectIetm = function () {
                debugger;


                $http.get(ENV.apiUrl + "api/CreateOrder/getOrderDetailsforapprove?data=" + JSON.stringify($scope.IP)).then(function (res) {
                    $scope.rows = res.data.table;
                    $scope.Allrows = res.data.table1;
                    if ($scope.Allrows.length <= 0) {
                        //$scope.remarks = '';
                        //$scope.hide_save = true;
                        //$scope.hide_saves = false;
                        swal({ title: "Alert", text: "Records not found", showCloseButton: true, width: 380, });
                    }
                    else {
                        for (var r in $scope.rows) {
                            $scope.rows[r]['sims_icon'] = "fa fa-minus-circle";
                            $scope.rows[r]['isexpanded'] = "grid";
                            $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].ord_no);
                        }
                        $scope.hide_save = false;
                        $scope.hide_saves = true;
                        console.clear();
                    }

                    console.log(res.data.table);
                    console.log(res.data.table1);
                });
            }

            $scope.SelectItem_1 = function () {
                debugger;
                $scope.remarks = '';
                $scope.hide_save = false;
                $scope.hide_saves = false;

                $http.get(ENV.apiUrl + "api/CreateOrder/getOrderDetailsforapprove?data=" + JSON.stringify($scope.IP)).then(function (res) {
                    $scope.rows = res.data.table;
                    $scope.Allrows = res.data.table1;
                    if ($scope.Allrows.length <= 0) {
                        //swal({ title: "Alert", text: "Records not found", showCloseButton: true, width: 380, });
                        $scope.remarks = '';
                        $scope.hide_save = false;
                        $scope.hide_saves = false;
                    }
                    else {
                        for (var r in $scope.rows) {
                            $scope.rows[r]['sims_icon'] = "fa fa-minus-circle";
                            $scope.rows[r]['isexpanded'] = "grid";
                            $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].ord_no);
                        }
                        $scope.hide_save = false;
                        $scope.hide_saves = true;
                        console.clear();
                    }

                    console.log(res.data.table);
                    console.log(res.data.table1);
                });
            }

            function getSubitems(dno) {
                var arr = [];
                for (var i = 0; i < $scope.Allrows.length; i++) {
                    if ($scope.Allrows[i].ord_no == dno) {
                        arr.push($scope.Allrows[i]);
                    }
                }
                return arr;
            }

            $scope.expand = function (j, $event) {
                if (j.isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.isexpanded = "grid";
                    console.log(j);
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.isexpanded = "none";
                }
            }

            $scope.reset = function () {
                $scope.IP = {};
                $scope.rows = {};
                $scope.Allrows = {};
            }

            $scope.checkRange = function (s) {
                var rq = parseInt(s.final_qnt);
                if (rq == NaN)
                    rq = 0;
                s.final_qnt = rq;
            }

            $scope.ordclick = function (gh) {
                var l = gh.subItems.length;
                for (var i = 0; i < l; i++) {
                    gh.subItems[i]['isChecked'] = gh.isChecked;
                }
            }

            $scope.ordlineclick = function (r, m) {
                var l = r.subItems.length;
                var f = false;
                if (m) {
                    f = true;
                    for (var i = 0; i < l; i++) {
                        f = f && r.subItems[i]['isChecked'];
                    }
                }
                r.isChecked = f;
            }

            $http.get(ENV.apiUrl + "api/AdjustmentReason/getDept").then(function (res) {
                $scope.sel['Dept'] = res.data.table;
            });

            $scope.save = function (s) {
                $scope.hide_save = true;
                $scope.hide_saves = true;
                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {
                    debugger;
                    for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                        if ($scope.rows[i].subItems[j].isChecked) {
                            var ob = {};
                            ob.od_order_qty = $scope.rows[i].subItems[j].od_order_qty;
                            ob.final_qnt = $scope.rows[i].subItems[j].final_qnt;
                            ob.im_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.od_line_no = $scope.rows[i].subItems[j].od_line_no;
                            ob.ord_no = $scope.rows[i].subItems[j].ord_no;
                            ob.req_type = $scope.rows[i].subItems[j].req_type;
                            ob.req_no = $scope.rows[i].subItems[j].req_no;
                            ob.req_remarks = $scope.remarks;
                            ob.ord_approved_by = $rootScope.globals.currentUser.username;
                            //ob.adj_status = $scope.rows[i].subItems[j].adj_status;
                            ob.val = r;
                            ar.push(ob);
                        }
                    }
                }
                if (ar.length == 0) {
                    alert("Nothing Selected");
                    return;
                }

                $http.post(ENV.apiUrl + "api/CreateOrder/UpdateOrderDetailsforapprove", ar).then(function (res) {
                    console.log(res);

                    //swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });
                    //$scope.SelectIetm();
                    if (res.data != null && res.data != '') {
                        swal({ title: "Alert", text: 'Purchase Order No ' + res.data + ' Approved.', showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                $scope.SelectItem_1();
                                var data = {
                                    location: 'Invs.invr21',
                                    parameter: {
                                        //orderno: $scope.msg1,
                                        orderno: res.data,
                                    },
                                    state: 'main.ordapp',
                                    ready: function () {
                                        this.refreshReport();
                                    },
                                }
                                console.log(data);

                                window.localStorage["ReportDetails"] = JSON.stringify(data);
                                $state.go('main.ReportCardParameter');
                            }
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Purchase Order Not Approved.", width: 300, height: 200 });
                    }

                });

            }

            $scope.reject = function (s) {
                $scope.hide_save = true;
                $scope.hide_saves = true;
                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {
                    debugger;
                    for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                        if ($scope.rows[i].subItems[j].isChecked) {
                            var ob = {};
                            ob.od_order_qty = $scope.rows[i].subItems[j].od_order_qty;
                            ob.final_qnt = $scope.rows[i].subItems[j].final_qnt;
                            ob.im_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.od_line_no = $scope.rows[i].subItems[j].od_line_no;
                            ob.ord_no = $scope.rows[i].subItems[j].ord_no;
                            ob.req_type = $scope.rows[i].subItems[j].req_type;
                            ob.req_no = $scope.rows[i].subItems[j].req_no;
                            ob.req_remarks = $scope.remarks;
                            ob.ord_approved_by = $rootScope.globals.currentUser.username;
                            //ob.adj_status = $scope.rows[i].subItems[j].adj_status;
                            ob.val = r;
                            ar.push(ob);
                        }
                    }
                }
                if (ar.length == 0) {
                    alert("Nothing Selected");
                    return;
                }

                $http.post(ENV.apiUrl + "api/CreateOrder/UpdateOrderDetailsforreject", ar).then(function (res) {
                    console.log(res);

                    //swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });
                    //$scope.SelectIetm();

                    swal({ title: "Alert", text: 'Purchase Order No ' + res.data + ' Rejected.', showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.SelectItem_1();

                        }
                    });
                });
            }

            //$scope.Reset = function () {
            //    $scope.IP = {
            //        rd_from_required: '',
            //        rd_up_required: '',
            //        req_no: '',
            //        req_range_no: '',
            //        request_mode_code: '',
            //        rd_item_desc: '',
            //    }
            //}

            //$scope.save = function (s) {
            //    $scope.hide_save = true;
            //    $scope.hide_saves = true;
            //    var ar = [];
            //    var r = Math.random() * 10;
            //    for (var i = 0; i < $scope.rows.length; i++) {
            //        for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
            //            if ($scope.rows[i].subItems[j].isChecked) {
            //                var ob = {};
            //                ob.od_order_qty = $scope.rows[i].subItems[j].od_order_qty;
            //                ob.final_qnt = $scope.rows[i].subItems[j].final_qnt;
            //                ob.im_inv_no = $scope.rows[i].subItems[j].im_inv_no;
            //                ob.od_line_no = $scope.rows[i].subItems[j].od_line_no;
            //                ob.ord_no = $scope.rows[i].subItems[j].ord_no;
            //                //ob.t_qty = $scope.rows[i].subItems[j].ad_qty;
            //                //ob.adj_status = $scope.rows[i].subItems[j].adj_status;
            //                ob.val = r;
            //                ar.push(ob);
            //            }
            //        }
            //    }
            //    if (ar.length == 0) {
            //        alert("Nothing Selected");
            //        return;
            //    }

            //    $http.post(ENV.apiUrl + "api/CreateOrder/UpdateOrderDetailsforapprove", ar).then(function (res) {
            //        console.log(res);

            //        swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });
            //        $scope.SelectIetm();
            //    });

            //}

            $scope.Reset = function () {
                $scope.IP = {
                    rd_from_required: '',
                    rd_up_required: '',
                    req_no: '',
                    req_range_no: '',
                    request_mode_code: '',
                    rd_item_desc: '',
                }
            }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            })

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

        }])

})();
