﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('InvsParameterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/InvsParameter/getInvsParameter").then(function (res1) {
                $scope.getParameter = res1.data;
                $scope.totalItems = $scope.getParameter.length;
                $scope.todos = $scope.getParameter;
                $scope.makeTodos();

            });


            $http.get(ENV.apiUrl + "api/InvsParameter/GetCompanyName").then(function (docstatus1) {
                $scope.Compname = docstatus1.data;
                console.log($scope.Compname);
            });

            $http.get(ENV.apiUrl + "api/InvsParameter/GetApplicationCode").then(function (docstatus1) {
                $scope.app_code = docstatus1.data;
                console.log($scope.app_code);
            });


            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.getParameter, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.getParameter;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.invs_appl_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.invs_appl_form_field.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.invs_appl_parameter.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.invs_appl_form_field_value1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {
                $scope.disabled = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.divcode_readonly = false;
                $scope.temp = "";
                $scope.temp = {};
                $scope.temp['sims_library_catalogue_status'] = true;
                $scope.temp["sims_library_catalogue_name"] = "";
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.divcode_readonly = true;
                 //  $scope.temp = str;

              
                $scope.temp = {
       
                    invs_comp_code:str.invs_comp_code,
                    invs_appl_code:str.invs_appl_code,
                    invs_appl_form_field:str.invs_appl_form_field,
                    invs_appl_parameter:str.invs_appl_parameter,
                    invs_appl_form_field_value1:str.invs_appl_form_field_value1,
                    invs_appl_form_field_value2:str.invs_appl_form_field_value2,
                    invs_appl_form_field_value3:str.invs_appl_form_field_value3,
                    invs_appl_form_field_value4:str.invs_appl_form_field_value4,


                    invs_comp_code_old: str.invs_comp_code,
                    invs_appl_code_old :str.invs_appl_code,
                    invs_appl_form_field_old :str.invs_appl_form_field ,
                    invs_appl_parameter_old: str.invs_appl_parameter,
                    invs_appl_form_field_value1_old :str.invs_appl_form_field_value1 ,
                    invs_appl_form_field_value2_old :str.invs_appl_form_field_value2, 
                    invs_appl_form_field_value3_old :str.invs_appl_form_field_value3 ,
                    invs_appl_form_field_value4_old :str.invs_appl_form_field_value4 ,
                };



            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                if (Myform) {
                    var data = $scope.temp;
                    data.opr = 'I';
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/InvsParameter/CUDInvsParamenter", datasend).then(function (msg) {
                        datasend = [];
                        $scope.msg1 = msg.data;
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record already present", width: 300, height: 200 });
                        }

                        $http.get(ENV.apiUrl + "api/InvsParameter/getInvsParameter").then(function (res1) {
                            $scope.getParameter = res1.data;
                            $scope.totalItems = $scope.getParameter.length;
                            $scope.todos = $scope.getParameter;
                            $scope.makeTodos();

                        });

                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                if (Myform) {

                    var data = $scope.temp;
                    data.opr = "U";
                    dataforUpdate.push(data);
                    //dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/InvsParameter/CUDInvsParamenter", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                        $http.get(ENV.apiUrl + "api/InvsParameter/getInvsParameter").then(function (res1) {
                            $scope.getParameter = res1.data;
                            $scope.totalItems = $scope.getParameter.length;
                            $scope.todos = $scope.getParameter;
                            $scope.makeTodos();
                        });

                    });
                    dataforUpdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            // Data DELETE RECORD

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.OkDelete = function () {
                debugger;
                deletefin = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'invs_comp_code': $scope.filteredTodos[i].invs_comp_code,
                            'invs_appl_code': $scope.filteredTodos[i].invs_appl_code,
                            'invs_appl_form_field': $scope.filteredTodos[i].invs_appl_form_field,
                            'invs_appl_parameter': $scope.filteredTodos[i].invs_appl_parameter,
                            'invs_appl_form_field_value1':$scope.filteredTodos[i].invs_appl_form_field_value1,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/InvsParameter/CUDInvsParamenter", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/InvsParameter/getInvsParameter").then(function (res1) {
                                                $scope.getParameter = res1.data;
                                                $scope.totalItems = $scope.getParameter.length;
                                                $scope.todos = $scope.getParameter;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/InvsParameter/getInvsParameter").then(function (res1) {
                                                $scope.getParameter = res1.data;
                                                $scope.totalItems = $scope.getParameter.length;
                                                $scope.todos = $scope.getParameter;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });


        }])

})();
