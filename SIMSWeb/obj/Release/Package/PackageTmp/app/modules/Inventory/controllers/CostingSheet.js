﻿(function () {
    'use strict';
    var opr = '';
    var costingcode = [];
    var main;
    var data1 = [];
    var data = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CostingSheetCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.table1 = true;
            $scope.set = {};
            $scope.set.showExp = false;
            $scope.ip = {};
            $scope.CSEData = [];
            $http.get(ENV.apiUrl + "api/CreateCostingSheet/getDepartments").then(function (getDepartments_Data) {
                $scope.Departments_Data = getDepartments_Data.data;
            });

            $http.get(ENV.apiUrl + "api/CreateCostingSheet/getSupplier").then(function (getSupplier_Data) {
                $scope.Supplier_Data = getSupplier_Data.data;
            });

            $http.post(ENV.apiUrl + "api/AdjustmentReason/ExpTypes").then(function (res) {
                $scope.pet_Data = res.data.table;
                console.log($scope.pet_Data);
            });

            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);


            var fob = {
                comp_code: $scope.finnDetail.company,
                fyear: $scope.finnDetail.year
            }

            $http.post(ENV.apiUrl + "api/AdjustmentReason/e059f8dab55a80d968e1336d8828a1a5015c35f6", fob).then(function (res) {
                $scope.lc_codes = res.data.table;
            });

            $scope.acct_change = function () {
                var x = {
                    lccade: $scope.edt.lccade,
                    comp_code: fob.comp_code,
                    fyear: fob.fyear
                }
                $http.post(ENV.apiUrl + "api/AdjustmentReason/f773e93705ce3ac9d8210bbccbab995da52e4e2d", x).then(function (res) {
                    $scope.Allcodes = res.data.table;
                });
            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['check_all'] = false;
                $scope.row1 = '';
            }

            $scope.New = function () {
                $scope.edt = "";
                $scope.check = true;
                $scope.editmode = false;
                $scope.newmode = true;
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
            }

            $scope.up = function (str) {
                $("#SearchCostingSheetModal").modal('hide');
                $scope.edt = {
                    req_type: 'I',
                    shipmentNO: str.shipmentNO
                }
                $scope.getAllShipmentDetails($scope.edt);
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.CreateCustomSheet, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreateCustomSheet;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */

                return (
                     item.sup_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.cs_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.dep_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.fa_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.cur_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                     item.loc_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.cs_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.cs_costaccount.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.cs_supl_other_charge.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.cs_forward_agent_charge.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                     item.cs_remarks.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.ccl_reg_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.dm_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.trt_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.lc_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                     item.cs_received_by.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                     item.cs_prov_no == toSearch) ? true : false;


            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            /*Shipment */
            $scope.searchShipment = function () {
                $("#SearchShipmentModal").modal('show');
            }

            $scope.Shipment_details_search = function () {
                $http.post(ENV.apiUrl + "api/AdjustmentReason/Search_Shipment_Details", $scope.s).then(function (getCreateCustomSheet_Data) {
                    console.log(getCreateCustomSheet_Data);
                    $scope.shipment_data = getCreateCustomSheet_Data.data.table;
                });
            }

            $scope.getAllShipmentDetails = function (row) {
                console.log(row);
                $http.post(ENV.apiUrl + "api/AdjustmentReason/Shipment_Order_Details", row).then(function (res) {
                    console.log(res);
                    $scope.OrderData = res.data.table;
                    $scope.ip = {}
                    if ($scope.OrderData.length > 0) {
                        $scope.ip.sr_shipment_no = $scope.OrderData[0].shipmentNO;
                        $scope.ip.dep_code = $scope.OrderData[0].dep_code;
                        $scope.ip.dep_name = $scope.OrderData[0].dep_name;
                        $scope.ip.cur_code = $scope.OrderData[0].cur_code;
                        $scope.ip.sup_code = $scope.OrderData[0].sup_code;
                        $scope.ip.sup_name = $scope.OrderData[0].sup_name;
                        $scope.ip.trt_code = $scope.OrderData[0].trt_code;
                    }
                    $scope.Total_Amount = 0;
                    $scope.OrderSUM();
                    $scope.grn = false;
                    $scope.cst = true;
                    //$scope.set.showExp = true;
                    for (var i = 0; i < $scope.OrderData.length; i++) {
                        $scope.Total_Amount = $scope.Total_Amount + (parseFloat($scope.OrderData[i].im_sell_price) * parseInt($scope.OrderData[i].so_shipped_qty))
                    }
                    $("#SearchShipmentModal").modal('hide');
                });
            }
            /*Shipment END*/

            /* GRN */
            $scope.searchGRN = function () {
                $("#SearchGRNModal").modal('show');
                $scope.shipment_data = [];
                $scope.table1 = true;
                $scope.currentPage = 1;
                //$scope.makeTodos();
            }

            $scope.GetAllGRN = function () {
                $http.post(ENV.apiUrl + "api/AdjustmentReason/AllGRN_Details", $scope.gr).then(function (res) {
                    $scope.GrnData = res.data.table;
                });
            }

            $scope.searchGRNDetails = function (row) {
                var sc = {};
                sc.od_grv_no = '';
                for (var i = 0; i < $scope.GrnData.length; i++) {
                    if ($scope.GrnData[i].isChecked) {
                        sc.od_grv_no = sc.od_grv_no + $scope.GrnData[i].od_grv_no + ',';
                    }
                }
                $http.post(ENV.apiUrl + "api/AdjustmentReason/GRN_DetailGet", sc).then(function (res) {
                    console.log(res);
                    $scope.ip = {}
                    $scope.OrderData = res.data.table;
                    if ($scope.OrderData.length > 0) {
                        $scope.ip.trt_code = $scope.OrderData[0].trt_code;
                        $scope.ip.cur_code = $scope.OrderData[0].cur_code;
                        $scope.ip.dep_code = $scope.OrderData[0].dep_code;
                        $scope.ip.dep_name = $scope.OrderData[0].dep_name;
                        $scope.ip.sup_code = $scope.OrderData[0].sup_code;
                        $scope.ip.sup_name = $scope.OrderData[0].sup_name;
                        $scope.ip.od_grv_no = sc.od_grv_no;
                    }
                    $scope.grn = true;
                    $scope.cst = false;
                    $scope.servi = false;
                    $scope.ip.cs_loading_note_date = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
                    $scope.ip.cs_prov_date = $scope.ip.cs_loading_note_date;
                    $scope.ip.sr_shipment_no = undefined;
                    //$scope.set.showExp = true;
                    $scope.OrderSUM();
                    $("#SearchGRNModal").modal('hide');
                });
            }

            $scope.resetGRN = function () {
                $scope.gr = {
                    grv_from_date: '',
                    grv_to_date: '',
                    od_grv_no: ''
                };
                $scope.GrnData = [];
            }
            /* GRN END*/

            /*Expenses*/
            $scope.searchExpenses = function () {
                $("#CreateEditEXPMain").modal('show');
            }

            $scope.getCostingExpensesDetails = function (row) {
                console.log(row);

                var ob = {};
                ob.cs_prov_no = $scope.ip.cs_prov_no;
                $scope.tob = ob;
                ob.val = Math.random() * 100;
                $http.post(ENV.apiUrl + "api/AdjustmentReason/SheetExpGET", ob).then(function (res) {
                    console.log(res);
                    $scope.CSEData = res.data.table;
                    $scope.Total_Amount = 0;
                    $scope.expenseSUM();
                    $scope.OrderSUM();
                    //for (var i = 0; i < $scope.CSEData.length; i++) {
                    //    //$scope.Total_Amount = $scope.Total_Amount + (parseFloat($scope.CSEData[i].im_sell_price) * parseInt($scope.OrderData[i].so_shipped_qty))
                    //}
                });
                $("#SearchCostingSheetModal").modal('hide');
            }

            $scope.clear = function () {
                $scope.tob = {};
                $scope.CSEData = [];
                $scope.expenseSUM();
                $scope.OrderSUM();
            }

            $scope.NewExpType = function () {
                //$scope.exp['lbl'] = 'New';
                $scope.edt = {};
                $scope.edt.cse_doc_no = '';
                $scope.edt.cse_doc_date = '';
                $scope.edt.cse_amount = '';
                $scope.edt.pet_desc = '';
                $scope.edt.pet_code = '';
                $scope.edt.isNew = true;
                $scope.edt.lccade = '';
                $scope.edt.acno = '';
                $scope.edt.cs_prov_no = $scope.ip.cs_prov_no;
                $("#CreateEditEXPMain").modal('hide');
                setTimeout(function () { $("#CreateEditEXP").modal('show'); }, 500)

            }

            $scope.createEditEXPclose = function () {

                $("#CreateEditEXP").modal('hide');
                setTimeout(function () { $("#CreateEditEXPMain").modal('show'); }, 500)
            }

            $scope.EditExpType = function (ob) {
                //$scope.exp['lbl'] = 'Edit';

                var x = {
                    lccade: ob.lccade,
                    comp_code: fob.comp_code,
                    fyear: fob.fyear
                }
                $http.post(ENV.apiUrl + "api/AdjustmentReason/f773e93705ce3ac9d8210bbccbab995da52e4e2d", x).then(function (res) {
                    $scope.Allcodes = res.data.table;
                    $scope.edt = {};
                    $scope.edt.cse_doc_no = ob.cse_doc_no;
                    $scope.edt.cse_doc_date = ob.cse_doc_date;
                    $scope.edt.cse_amount = ob.cse_amount;
                    $scope.edt.pet_desc = ob.pet_desc;
                    $scope.edt.pet_code = ob.pet_code;
                    $scope.edt.cs_prov_no = ob.cs_prov_no;
                    $scope.edt.lccade = ob.lccade;
                    $scope.edt.acno = ob.acno;
                    $scope.edt.isNew = false;
                });

                $("#CreateEditEXPMain").modal('hide');
                setTimeout(function () { $("#CreateEditEXP").modal('show'); }, 500)
            }

            $scope.saveExp = function (ob) {
                var edt = {};
                edt.cse_doc_no = ob.cse_doc_no;
                edt.cse_doc_date = ob.cse_doc_date;
                edt.cse_amount = ob.cse_amount;
                edt.pet_desc = ob.pet_desc;
                edt.pet_code = ob.pet_code;
                edt.cs_prov_no = ob.cs_prov_no;
                edt.lccade = ob.lccade;
                edt.acno = ob.acno;
                edt.isNew = ob.isNew ? 'T' : 'F';


                //$scope.CSEData.push(edt);
                $http.post(ENV.apiUrl + "api/AdjustmentReason/CostingSheetExpSave", edt).then(function (res) {
                    $scope.res = res.data.table;
                    $("#CreateEditEXP").modal('hide');
                    $scope.getCostingExpensesDetails(ob);
                    $scope.createEditEXPclose();
                });
                $scope.createEditEXPclose();
            }

            $scope.DeleteExpType = function (ob) {
                var edt = {};
                edt.cse_doc_no = ob.cse_doc_no;
                edt.cse_doc_date = ob.cse_doc_date;
                edt.cse_amount = ob.cse_amount;
                edt.pet_desc = ob.pet_desc;
                edt.pet_code = ob.pet_code;
                edt.cs_prov_no = ob.cs_prov_no;
                $http.post(ENV.apiUrl + "api/AdjustmentReason/CostingSheetExpUP", edt).then(function (res) {
                    $scope.res = res.data.table;
                    $("#CreateEditEXP").modal('hide');
                    $("#CreateEditEXPMain").modal('show');
                    $scope.getCostingExpensesDetails(ob);
                });
            }
            /*Expenses End*/

            /*Costing Sheet and details*/
            $scope.searchCostingSheets = function (id) {
                if (id == undefined || id == '') {
                    $("#CostingSheetModal").modal('show');
                }
                else {
                    $scope.searchCostingSheets1(id);
                }
            }

            $scope.resetCS = function () {
                $scope.cs = {};
                $scope.cs.cs_to_date = '';
                $scope.cs.cs_from_date = '';
                $scope.cs.cs_no = '';

                $scope.CSDataSearch = [];
            }

            $scope.searchCostingSheets1 = function (id) {
                $http.post(ENV.apiUrl + "api/AdjustmentReason/AllCostingSheetsGet", id).then(function (res) {
                    console.log(res);
                    $scope.CSDataSearch = res.data.table;
                });
            }

            //CS Details
            $scope.GetCostingSheetsDetails = function (id) {
                var ob = {};
                $("#CostingSheetModal").modal('hide');
                ob.orderLineNO = id.cs_prov_no;
                $http.post(ENV.apiUrl + "api/AdjustmentReason/CostingSheetsGetDetails", ob).then(function (res) {
                    console.log(res);
                    /*var data = res['table'];
                    if(data.)*/
                    $scope.ip = res.data.table[0];
                    $scope.OrderData = res.data.table1;
                    $scope.addlDetails = res.data.table2;
                    $scope.CSEData = res.data.table3;
                    $scope.expenseSUM();
                    $scope.OrderSUM();
                    $scope.forupdflag = true;
                    $scope.set.showFinal = true;
                    $scope.set.showExp = true;

                });
            }

            $scope.resetForm = function () {
                $scope.ip = {};
                $scope.OrderData = {};
                $scope.addlDetails = {};
                $scope.CSEData = [];
                $scope.expenseSUM();
                $scope.OrderSUM();
                $scope.ServiceData = [];
                $scope.forupdflag = false;
                $scope.showCsProvDateEdit = false;
                $scope.set.showExp = false;
                $scope.set.showFinal = false;
            }

            $scope.SaveCostingSheet = function (id) {
                if ($scope.forupdflag) {
                    $http.post(ENV.apiUrl + "api/AdjustmentReason/CostingSheet_Update", id).then(function (res) {
                        swal({ title: "Alert", text: 'Saved Successfully', showCloseButton: true, width: 380, });
                        $scope.forupdflag = true;
                    });
                }
                else {
                    $http.post(ENV.apiUrl + "api/AdjustmentReason/CostingSheet_Insert", id).then(function (res) {
                        var id = {};
                        if (res.data.table.length > 0) {
                            id.cs_prov_no = res.data.table[0].res;
                        }
                        var msg = {};
                        if (res.data.table1.length) {
                            msg = res.data.table1[0];
                        }
                        if (id.cs_prov_no == undefined) {
                            swal({ title: "Alert", text: 'Error', showCloseButton: true, width: 380, });
                        }
                        else {
                            $scope.GetCostingSheetsDetails(id);
                            swal({ title: "Alert", text: msg['msg'], showCloseButton: true, width: 380, });
                        }
                        $scope.forupdflag = true;
                    });
                }
            }

            //finalize
            $scope.finalizeCostingSheet = function () {
                var id = {};
                id.cs_prov_no = $scope.ip.cs_prov_no;
                $http.post(ENV.apiUrl + "api/AdjustmentReason/CostingSheet_Update", id).then(function (res) {
                    swal({ title: "Alert", text: msg['msg'], showCloseButton: true, width: 380, });
                    $scope.resetForm();
                });
            }

            $scope.expenseSUM = function () {
                var s = 0;
                for (var i = 0; i < $scope.CSEData.length; i++) {
                    s = s + parseFloat($scope.CSEData[i].cse_amount);
                }
                $scope.expenseSUMV = s.toFixed(2);
                $scope.gTotal = parseFloat(s) + parseFloat($scope.Total_Amount == undefined ? 0 : $scope.Total_Amount);
                return s.toFixed(2);
            }

            $scope.OrderSUM = function () {
                var s = 0;
                for (var i = 0; i < $scope.OrderData.length; i++) {
                    s = s + (parseFloat($scope.OrderData[i].rate) * parseInt($scope.OrderData[i].od_received_qty));
                }
                $scope.Total_Amount = s.toFixed(2);
                $scope.gTotal = parseFloat(s) + parseFloat($scope.expenseSUMV == undefined ? 0 : $scope.expenseSUMV);
                return s.toFixed(2);
            }

            /*Service Details*/
            $scope.searchSeriveModalShow = function () {
                $("#SearchServiceModal").modal('show');
            }

            $scope.service_details_search = function () {
                $http.post(ENV.apiUrl + "api/AdjustmentReason/ServiceGetAll", $scope.ser).then(function (res) {
                    $scope.ServiceData = res.data.table;
                });
            }

            $scope.serviceDetailSearch = function (id) {
                var ob = {}
                ob.param = id.ord_no;
                $http.post(ENV.apiUrl + "api/AdjustmentReason/ServiceGetDetails", ob).then(function (res) {
                    $("#SearchServiceModal").modal('hide');
                    $scope.OrderData = res.data.table;
                    if ($scope.OrderData.length > 0) {
                        $scope.ip.trt_code = $scope.OrderData[0].trt_code;
                        $scope.ip.cur_code = $scope.OrderData[0].cur_code;
                        $scope.ip.dep_code = $scope.OrderData[0].dep_code;
                        $scope.ip.dep_name = $scope.OrderData[0].dep_name;
                        $scope.ip.sup_code = $scope.OrderData[0].sup_code;
                        $scope.ip.sup_name = $scope.OrderData[0].sup_name;

                        $scope.ip.ord_no = id.ord_no;
                    }
                    $scope.grn = false;
                    $scope.cst = false;
                    $scope.servi = true;
                    $scope.ip.cs_loading_note_date = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
                    $scope.ip.cs_prov_date = $scope.ip.cs_loading_note_date;
                    $scope.ip.sr_shipment_no = undefined;
                });
            }

            $scope.resetServices = function () {
                $scope.ser = {};
                $scope.ServiceData = [];
            }

            $scope.Privew = function () {
                //$("#Costing_View").modal('show');
            }
        }])
})();