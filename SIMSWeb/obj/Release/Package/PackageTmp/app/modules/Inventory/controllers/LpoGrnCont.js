﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('LpoGrnCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.edt = [];
            var now = new Date();
            var month = (now.getMonth() + 1);
            var day = now.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            $scope.Invoice_date = month + '/' + day + '/' + now.getFullYear();

            $http.get(ENV.apiUrl + "api/LPOGRM/Get_Adj_Reasons").then(function (Get_Adj_Reasons) {
                $scope.Get_Adj_Reasons = Get_Adj_Reasons.data;
                $scope.edt['ar_code'] = $scope.Get_Adj_Reasons[0].ar_code;

            });

            $http.get(ENV.apiUrl + "api/LPOGRM/Get_Adj_Dttypes").then(function (Get_Adj_Dttypes) {
                $scope.Get_Adj_Dttypes = Get_Adj_Dttypes.data;
                $scope.edt['dco_code'] = $scope.Get_Adj_Dttypes[0].dco_code;

            });

            $http.get(ENV.apiUrl + "api/LPOGRM/Get_Adj_locations").then(function (Get_Adj_locations) {
                $scope.Get_Adj_locations = Get_Adj_locations.data;
                $scope.edt['loc_code'] = $scope.Get_Adj_locations[0].loc_code;

            });

            $http.get(ENV.apiUrl + "api/LPOGRM/Get_Adj_Suppliername").then(function (Get_Adj_Suppliername) {
                $scope.Get_Adj_Suppliername = Get_Adj_Suppliername.data;

            });

            $http.get(ENV.apiUrl + "api/LPOGRM/get_Invs021_get_supplier_group_name").then(function (Invs021_get_supplier_group_name) {
                $scope.Invs021_get_supplier_group_name = Invs021_get_supplier_group_name.data;

            });

            $http.get(ENV.apiUrl + "api/LPOGRM/get_Invs021_get_Categories").then(function (Invs021_get_Categories) {
                $scope.Invs021_get_Categories = Invs021_get_Categories.data;

            });

            $scope.getsubcategory_code = function () {

                $http.get(ENV.apiUrl + "api/LPOGRM/get_Invs021_get_SubCategories?pc_parentcode=" + $scope.temp1.category_code).then(function (Invs021_get_SubCategories) {
                    $scope.Invs021_get_SubCategories = Invs021_get_SubCategories.data;

                });
            }

            $http.get(ENV.apiUrl + "api/StockAdjustment/getAllDepartment").then(function (resd) {
                debugger;
                $scope.department = resd.data;
                $scope.isa4 = { dep_code: $scope.department[0].dep_code };
            });


            $scope.btn_Fetch_ItemDetails = function () {
                $scope.busy = true;
                $scope.searchtable = false;
                $http.post(ENV.apiUrl + "api/LPOGRM/Fetch_ItemDetails", $scope.temp1).then(function (Fetch_ItemDetails) {
                    $scope.Fetch_ItemDetails = Fetch_ItemDetails.data;
                    if ($scope.Fetch_ItemDetails.length <= 0) {
                        $scope.searchtable = false;
                        swal({ text: 'Sorry Data Not Found', width: 380, showCloseButton: true });
                    }
                    else {
                        $scope.searchtable = true;
                    }

                    $scope.busy = false;
                });
            }

            $scope.temp1 = { im_assembly_ind: true };

            $scope.getitemdetals = function (str) {

                $http.get(ENV.apiUrl + "api/LPOGRM/Get_itemdetails?itemcode=" + str).then(function (Get_itemdetails) {
                    $scope.Get_itemdetails = Get_itemdetails.data;

                    $scope.invs = {
                        im_item_code: str,
                        im_desc: $scope.Get_itemdetails.im_desc,
                        // oldRate: $scope.Get_itemdetails.im_malc_rate,
                        oldQty: $scope.Get_itemdetails.id_cur_qty,
                        im_inv_no: $scope.Get_itemdetails.im_inv_no
                    };
                });

            }

            $scope.CalculateRate = function (str) {
                var val = parseFloat(str.txt_value) / parseFloat(str.oldQty);
                str.oldRate = val;

                var val = parseFloat(str.newValue) / parseFloat(str.newQty);
                str.newRate = val;
            }

            $scope.newselecteditemobject = [];
            var loc_desc = '';

            $scope.AddSelecteditem = function (im_item_code, loc_code, ar_code, sup_sblgr_acno, dco_code, oldQty, txt_value, info, info1) {

                if (im_item_code != '' && im_item_code != undefined && loc_code != '' && loc_code != undefined && ar_code != '' && ar_code != undefined && sup_sblgr_acno != '' && sup_sblgr_acno != undefined && dco_code != '' && dco_code != undefined && oldQty != '' && oldQty != undefined && txt_value != '' && txt_value != undefined) {
                    for (var i = 0; i < $scope.Get_Adj_locations.length; i++) {
                        if (loc_code == $scope.Get_Adj_locations[i].loc_code) {
                            loc_desc = $scope.Get_Adj_locations[i].loc_name
                        }
                    }


                    var obj = {};
                    $scope.addtable = true;
                    obj.newRate = info.oldRate;
                    obj.newQty = info.oldQty;
                    obj.newValue = info.txt_value;
                    obj.im_item_code = info.im_item_code;
                    obj.im_desc = info.im_desc;
                    obj.loc_code = info1.loc_code;
                    obj.loc_desc = loc_desc;
                    obj.ar_code = info1.ar_code;
                    obj.sup_sblgr_acno = info1.sup_sblgr_acno;
                    obj.dco_code = info1.dco_code;
                    obj.remark = info1.remark;
                    obj.im_inv_no = info.im_inv_no;
                    $scope.newselecteditemobject.push(obj);

                    $scope.invs = '';
                }
                else {

                    swal({ text: 'Lcation , Reason Code ,Supplier Name , Doc Type , Item Code , Quantity And Value  Are Not Select Or Empty', width: 380, showCloseButton: true });
                }
            }

            $scope.Removeselectedrow = function ($event, index, str) {
                str.splice(index, 1);
            }

            $scope.getitemdetals_search = function (im_item_code, loc_code, ar_code, sup_sblgr_acno, dco_code, oldQty, txt_value, info, info1) {
                debugger
                $scope.searchtable = false;
                if (loc_code != '' && loc_code != undefined && ar_code != '' && ar_code != undefined && sup_sblgr_acno != '' && sup_sblgr_acno != undefined && dco_code != '' && dco_code != undefined) {
                    $scope.Fetch_ItemDetails = [];
                    $scope.temp1 = '';
                    $scope.temp1 = { im_assembly_ind: false };
                    $('#MyModal').modal('show');
                }
                else {

                    swal({ text: 'Select Lcation , Reason Code ,Supplier Name And Doc Type', width: 380, showCloseButton: true });
                }
            }

            $scope.getselected_items = function (str) {

                for (var i = 0; i < $scope.Get_Adj_locations.length; i++) {
                    if ($scope.edt.loc_code == $scope.Get_Adj_locations[i].loc_code) {
                        loc_desc = $scope.Get_Adj_locations[i].loc_name
                    }
                }

                var obj = {};
                $scope.addtable = true;
                obj.newRate = '0';
                obj.newQty = '0';
                obj.newValue = '0';
                obj.im_item_code = str.im_item_code;
                obj.im_desc = str.im_desc;
                obj.loc_code = $scope.edt.loc_code;
                obj.loc_desc = loc_desc;
                obj.ar_code = $scope.edt.ar_code;
                obj.sup_sblgr_acno = $scope.edt.sup_sblgr_acno;
                obj.dco_code = $scope.edt.dco_code;
                obj.remark = $scope.edt.remark;
                obj.im_inv_no = str.im_inv_no;
                $scope.newselecteditemobject.push(obj);


            }

            $scope.cancel = function () {
                $scope.invs = '';
                $scope.edt = '';
                $scope.MyForm.$setPristine();
            }

            $scope.Cancel = function () {
                $scope.newselecteditemobject = [];
                $scope.temp1 = '';
                $scope.edt = '';
                $scope.invs = '';
                $scope.addtable = false;
            }


            $scope.SaveData = function (isvalid) {
                debugger;
                if (isvalid) {
                    var datasend = [];
                    // var data = '';
                    if ($scope.newselecteditemobject.length != 0) {

                        //var data = $scope.edt;
                        console.log($scope.edt);
                        //  data.adj_grv_no = '1';
                        var data = {
                            ar_code: $scope.edt.ar_code,
                            dco_code: $scope.edt.dco_code,
                            adj_grv_no: '1',
                            loc_code: $scope.edt.loc_code,
                            remark: $scope.edt.remark,
                            sup_sblgr_acno: $scope.edt.sup_sblgr_acno,
                            dep_code: $scope.isa4.dep_code,

                        }
                        debugger;
                        $http.post(ENV.apiUrl + "api/LPOGRM/Insertadjustments", data).then(function (msg) {
                            debugger;
                            $scope.msg = msg.data;
                            if ($scope.msg != undefined && $scope.msg != '') {
                                for (var i = 0; i < $scope.newselecteditemobject.length; i++) {
                                    $scope.newselecteditemobject[i].adj_doc_no = $scope.msg;
                                    $scope.newselecteditemobject[i].remark = $scope.edt.remark;
                                    $scope.newselecteditemobject[i].ad_line_no = i + 1;
                                    $scope.newselecteditemobject[i].new_calmalcrate = $scope.newselecteditemobject[i].newRate;
                                    $scope.newselecteditemobject[i].dep_code = $scope.isa4.dep_code,
                                    $scope.newselecteditemobject[i].new_calqty = $scope.newselecteditemobject[i].newQty;
                                    datasend.push($scope.newselecteditemobject[i]);
                                }

                                $http.post(ENV.apiUrl + "api/LPOGRM/Insertadjustmentsdetails", datasend).then(function (msg1) {
                                    $scope.msg1 = msg1.data;
                                    if ($scope.msg1 == true) {
                                        $scope.newselecteditemobject = [];
                                        $scope.edt = '';
                                        $scope.MyForm.$setPristine();
                                        $scope.addtable = false;
                                        swal({ text: 'Record Inserted Successfully', width: 380, showCloseButton: true });
                                    }
                                    else {
                                        swal({ text: 'Record Not Inserted', width: 380, showCloseButton: true });
                                    }
                                });

                            }
                            else {
                                swal({ text: 'Record Not Inserted', width: 380, showCloseButton: true });
                            }

                        });
                    }
                    else {
                        swal({ text: 'There Is No Record To Insert', width: 380, showCloseButton: true });
                    }
                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy/MM/dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.Reset = function () {
                $scope.temp1 = '';
                $scope.temp1 = { im_assembly_ind: true };
            }


        }])
})();