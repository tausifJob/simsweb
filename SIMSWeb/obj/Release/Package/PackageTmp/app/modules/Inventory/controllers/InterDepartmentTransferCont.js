﻿(function () {
    'use strict';
    var del = [];
    var main;
    var date1;
    var date3;
    var simsController = angular.module('sims.module.Inventory');

    
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('InterDepartmentTransferCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.approvebutton = false;
            $scope.save_btn = true;
            var user = $rootScope.globals.currentUser.username;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.itemtable = false;
            $scope.temp1 = {
                im_inv_no: undefined, invs021_sg_name: undefined, im_item_code: undefined, im_desc: undefined, invs021_dep_code: undefined, sec_code: undefined, invs021_sup_code: undefined, im_assembly_ind_s: undefined, category_code: undefined, subcategory_code: undefined

            };
            salesman();
            $scope.itemsPerPage = '5';
            $scope.currentPage = 0;
            $scope.obj = [];
            $scope.itemList = [];
            $scope.temp = {};
            $scope.Finalize_btn = true;
            setTimeout(function () {
                $("#rcbItem").select2();

            }, 100);

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd
            }

            if (mm < 10) {
                mm = '0' + mm
            }

            today = yyyy + '/' + mm + '/' + dd;
            $scope.temp['creation_date'] = today;
            $scope.temp['creation_user'] = user;
            $scope.temp['doc_prov_date'] = today;

            $scope.paymodeList = [
                { name: 'Cash', value: 'CS' },
                 { name: 'Cheques', value: 'CH' },
                  { name: 'Bank Transfer', value: 'BT' },
            ]

            $scope.temp['dt_code'] = '00';
            $scope.temp['sal_type'] = '03';
            $scope.temp['dt_name'] = 'Internal Invoices'
            $scope.temp['paymentMode'] = 'CS';

            $http.get(ENV.apiUrl + "api/InvLPO/Get_Adj_locations").then(function (Get_Adj_locations) {
                $scope.Get_Adj_locations = Get_Adj_locations.data;

            });

            $scope.Calculateprice = function (info) {
                info.finalValue = parseFloat(info.im_sell_price) * parseFloat(info.qty)
            }

            $http.get(ENV.apiUrl + "api/InterDepartmentTransfer/GetAllItemsInSubCategoryforidt").then(function (res) {
                $scope.rcbItems = res.data;

            });

            function salesman() {
                $http.get(ENV.apiUrl + "api/InterDepartmentTransfer/GetSalesmanNameforidt?user_code=" + user).then(function (res) {
                    $scope.salesman = res.data;

                    $scope.temp['sm_code'] = $scope.salesman[0].sm_code

                    $scope.GetSalesDepartMents($scope.temp.sm_code);
                });
            }

            $http.get(ENV.apiUrl + "api/InterDepartmentTransfer/get_supplier_codeforidt").then(function (res) {
                $scope.cmb_supplier = res.data;
            });

            $http.get(ENV.apiUrl + "api/InterDepartmentTransfer/GetAllSupplierGroupNameforidt").then(function (res) {

                $scope.rcbGroup = res.data;

            });

            $http.get(ENV.apiUrl + "api/InterDepartmentTransfer/get_Categoriesidt").then(function (res) {

                $scope.rcbCategories = res.data;

            });

            //$http.get(ENV.apiUrl + "api/InterDepartmentTransfer/GetAllBanks").then(function (res) {

            //    $scope.bankList = res.data;

            //});

            $scope.Cancel = function () {
                $scope.temp['doc_total_amount'] = '';
                $scope.temp['doc_delivery_remarks'] = '';
                $scope.temp['doc_discount_amount'] = '';
                $scope.temp['overalldis'] = '';
                $scope.temp['loc_code'] = '';
                $scope.temp['up_name1'] = '';
                $scope.addtable = false;
                $scope.temp1 = {}
                $scope.GetItems = [];
                $scope.itemList = [];
                $scope.im_item_code1 = '';
                $scope.temp['paymentMode'] = 'CS';
                salesman();
            }

            $scope.getCategories = function (str) {
                $http.get(ENV.apiUrl + "api/InterDepartmentTransfer/get_SubCategoriesidt?pc_parentcode=" + str).then(function (res) {
                    $scope.rcbSubCategories = res.data;

                });

            }

            $scope.getSubCategories = function (str) {
                $scope.rcbItems = [];
                $http.get(ENV.apiUrl + "api/InterDepartmentTransfer/GetAllItemsInSubCategoryNewidt?pc_code=" + str).then(function (res) {
                    $scope.rcbItems = res.data;

                });

            }

            $scope.qtychange = function (info) {

                var maxqty = '';
                for (var i = 0; i < $scope.rcbItems.length; i++) {
                    if (im_item_code == $scope.rcbItems[i].im_item_code) {
                        maxqty = $scope.original_qty;
                    }
                }

                if (info.qty <= maxqty) {
                    info.finalValue = (info.qty * parseFloat(info.im_sell_price));
                    $scope.txt_discounted_price_TextChanged($scope.temp.overalldis);
                }
                else {
                    info.qty = '';
                    info.finalValue = '';
                }
            }

            $scope.qtychangeby = function (str) {
                str.dd_sell_value_final = (str.dd_qty * parseFloat(str.dd_sell_price));

                $scope.txt_discounted_price_TextChanged($scope.temp.overalldis);
            }

            var im_item_code = '';
            var im_inv_no = '';


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#Table1").tableHeadFixer({ 'top': 1 });
                $("#mycell").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.getitemCode = function (str) {

                $scope.item_desc = str;
                im_item_code = '';
                $scope.resitem = [];
                $scope.temp['qty'] = '';
                $scope.temp['im_sell_price'] = '';
                $scope.temp['finalValue'] = '';
                $scope.temp['gcode'] = '';

                var im_code = $scope.item_desc.split('#')[0];

                for (var i = 0; i < $scope.rcbItems.length; i++) {
                    if (im_code == $scope.rcbItems[i].im_item_code) {

                        im_item_code = $scope.rcbItems[i].im_item_code;
                        im_inv_no = $scope.rcbItems[i].im_inv_no;
                        $scope.temp.qty = 1;
                        $scope.temp.im_sell_price = $scope.rcbItems[i].im_sell_price;
                        $scope.temp.finalValue = (1 * parseFloat($scope.rcbItems[i].im_sell_price));
                        $scope.temp['gcode'] = $scope.rcbItems[i].sg_name
                        $scope.original_qty = $scope.rcbItems[i].original_qty;
                        break;
                    }
                }
                

                    $scope.resloc = [];

                    $scope.temp['loc_code'] = '';

                    $http.get(ENV.apiUrl + "api/InterDepartmentTransfer/GetItemCodeLocationsidt?im_inv_no=" + im_inv_no).then(function (res) {

                        $scope.resloc = res.data;
                        if ($scope.resloc.length > 0)
                            $scope.temp.loc_code = $scope.resloc[0].loc_code;
                        $scope.temp.loc_code
                    });

             

                document.getElementById('addbtn').focus();
            }

            setTimeout(function () {
                $("#rcbItem").select2();

            }, 100);

            $scope.add = function () {

                for (var i = 0; i < $scope.rcbItems.length; i++) {
                    if ($scope.item_desc == $scope.rcbItems[i].im_desc) {
                        $scope.im_inv_no = $scope.rcbItems[i].im_inv_no;
                        $scope.original_qty = $scope.rcbItems[i].original_qty;
                    }
                }

                if ($scope.im_inv_no == '' || $scope.im_inv_no == undefined || $scope.original_qty == undefined || $scope.original_qty == '' || $scope.temp.qty == '' || $scope.temp.qty == undefined || $scope.temp.im_sell_price =='' || $scope.temp.im_sell_price == undefined) {

                    swal({ text: 'Item Quantity Is 0 And Purchase Price Is 0', width: 320, showCloseButton: true });
                }
                else {

                    $scope.addtable = true;
                    $scope.totalFinal = 0;
                    $scope.flg = false;
                    for (var i = 0; i < $scope.itemList.length; i++) {
                        if ($scope.itemList[i].im_inv_no == $scope.im_inv_no) {

                            $scope.itemList[i].dd_qty += 1;
                            $scope.itemList[i].dd_sell_value_final += $scope.temp.im_sell_price
                            $scope.flg = true;
                            i = $scope.itemList.length;
                        }
                    }

                    //= false;
                    if ($scope.flg == false) {
                        $scope.addtable = true;
                        var data =
                            {
                                sg_name: $scope.temp.gcode,
                                sg_code: $scope.temp.gcode,
                                im_inv_no: $scope.im_inv_no,
                                im_item_code: im_item_code,
                                im_desc: $scope.item_desc,
                                item_location_name: $("#rcbLocation").find("option:selected").text(),
                                loc_code: $scope.temp.loc_code,
                                original_qty: $scope.original_qty,
                                dd_qty: $scope.temp.qty,
                                dd_sell_price: $scope.temp.im_sell_price,
                                dd_sell_value_final: $scope.temp.finalValue,

                            }
                        $scope.itemList.push(data)
                    }
                    $scope.im_inv_no = '';
                    $scope.original_qty = '';
                    $scope.Finalize_btn = false;
                    $scope.txt_discounted_price_TextChanged($scope.temp.overalldis);

                    document.getElementById("rcbItem").focus();
                    $scope.CancelItem();

                }
            }

            $scope.finalize = function (isvalidate) {

                $scope.cus_account_no = $scope.temp.cus_account_no

                $scope.maindatasend = [];
                $scope.approveddata = {};
                $scope.approveddata = $scope.temp;

                var datasend = [];

                if (isvalidate) {

                    $http.post(ENV.apiUrl + "api/InterDepartmentTransfer/Insert_Sale_Documentsidt", $scope.temp).then(function (msg) {
                        var d = msg.data;

                        if (d != "" || d != undefined) {

                            for (var i = 0; i < $scope.itemList.length; i++) {

                                var paymode = $scope.temp.paymentMode;

                                var data = {}
                                data.dep_code = $scope.temp.dep_code;
                                data.doc_prov_no = msg.data;
                                data.dd_line_no = i;
                                data.dd_status = '1';
                                data.dd_payment_mode = paymode;
                                data.im_inv_no = $scope.itemList[i].im_inv_no;
                                data.loc_code = $scope.itemList[i].loc_code;
                                data.dd_sell_price = $scope.itemList[i].dd_sell_price;
                                data.dd_sell_price_discounted = 0;
                                data.dd_sell_price_final = 0;
                                data.dd_sell_value_final = $scope.itemList[i].dd_sell_value_final;
                                data.dd_sell_cost_total = 0;
                                data.dd_qty = $scope.itemList[i].dd_qty;
                                data.dd_outstanding_qty = 0;
                                data.dd_line_discount_pct = 0;

                                if ($scope.temp.doc_discount_amount != '' || $scope.temp.doc_discount_amount != undefined) {
                                    data.dd_line_discount_amount = parseFloat(($scope.itemList[i].dd_sell_value_final * $scope.temp.overalldis) / 100);
                                }
                                else {
                                    data.dd_line_discount_amount = 0;
                                }
                                data.dd_line_total = 0;
                                data.dd_physical_qty = 0;

                                data.dd_cheque_number = $scope.temp.doc_cheque_no;
                                data.dd_cheque_date = $scope.temp.doc_cheque_date;
                                data.dd_cheque_bank_code = $scope.temp.doc_bank_code;

                                datasend.push(data)
                            }

                            $scope.maindatasend = datasend;

                            $http.post(ENV.apiUrl + "api/InterDepartmentTransfer/Insert_Sale_Documents_Detailsidt", datasend).then(function (msg) {
                                if (msg.data) {
                                    swal({ text: 'Provision No' + '   ' + d + '   ' + 'Inserted Successfully', width: 320, showCloseButton: true });
                                    $scope.approvebutton = true;
                                    $scope.Finalize_btn = true;
                                    $scope.Cancel();
                                    salesman();
                                }
                                else {
                                    swal({ text: "Record Not Inserted", width: 320, showCloseButton: true });

                                }
                            });
                        }
                        else {
                            swal({ text: 'Record Not Insert In Sale Document', width: 320, showCloseButton: true });
                        }
                    });
                }
            }

            $scope.itemSearch = function () {
                $scope.itemtable = false;
                $scope.GetItems = []
                $scope.temp1 = {}
                setTimeout(function () {
                    $('#Desc_Res').focus();
                }, 300);
                $('#itemSearch').modal('show');

                $scope.itemsetflg = false;
            }

            $scope.ItemSetSearch = function () {
                $scope.itemtable = false;

                $scope.GetItems = []
                $scope.temp1 = {}

                $('#itemSearch').modal('show');
                $scope.itemsetflg = true;
                $scope.temp1['im_assembly_ind'] = true;
                $http.post(ENV.apiUrl + "api/InterDepartmentTransfer/postgetItemSerchidt", $scope.temp1).then(function (res) {
                    if (res.data.length > 0) {
                        $scope.itemtable = true;
                        $scope.GetItems = res.data;
                    }
                    else {
                        $scope.itemtable = false;
                        swal({ text: "No Record Found.", width: 300, showCloseButton: true });

                    }


                });

            }

            $scope.getfetch = function () {

                if ($scope.itemsetflg) {
                    $scope.temp1['im_assembly_ind'] = true;
                }
                else {
                    $scope.temp1['im_assembly_ind'] = false;
                }

                $http.post(ENV.apiUrl + "api/InterDepartmentTransfer/postgetItemSerchidt", $scope.temp1).then(function (res) {
                    if (res.data.length > 0) {
                        $scope.GetItems = res.data;
                        $scope.itemtable = true;
                    }
                    else {
                        $scope.itemtable = false;
                        swal({ text: "No Record Found.", width: 300, showCloseButton: true });
                    }

                });
            }

            $scope.calculateTotal = function () {
                $scope.doc_total_amount = 0;
                for (var i = 0; i < $scope.itemList.length; i++) {

                    $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_final);
                }
                $scope.temp['totalFinal'] = $scope.totalFinal;

            }

            $scope.chk_click_item = function (obj, flg) {


                $scope.addtable = true;

                if ($scope.itemsetflg) {

                    $http.post(ENV.apiUrl + "api/InterDepartmentTransfer/Fetch_ItemDetails_ItemSetidt?im_inv_no=" + obj.im_inv_no).then(function (res) {

                        for (var i = 0; i < res.data.length; i++) {
                            var data =
                   {
                       sg_name: res.data[i].sg_name,
                       sg_code: res.data[i].sg_name,
                       im_inv_no: res.data[i].im_inv_no,
                       im_item_code: res.data[i].im_item_code,
                       im_desc: res.data[i].im_desc,
                       item_location_name: res.data[i].loc_code,
                       loc_code: res.data[i].loc_code,
                       original_qty: res.data[i].ia_component_qty,
                       dd_qty: 1,
                       dd_sell_price: parseFloat(res.data[i].im_sell_price),
                       dd_sell_value_final: parseFloat(res.data[i].im_sell_price),

                   }
                            $scope.itemList.push(data)

                        }
                        $scope.txt_discounted_price_TextChanged($scope.temp.overalldis);



                    });

                    $('#itemSearch').modal('hide');
                }
                else {
                    var data =
                    {
                        sg_name: obj.sg_name,
                        im_inv_no: obj.im_inv_no,
                        im_item_code: obj.im_item_code,
                        im_desc: obj.im_desc,
                        item_location_name: obj.loc_code,
                        loc_code: obj.loc_code,
                        original_qty: obj.id_cur_qty,
                        dd_qty: 1,
                        dd_sell_price: parseFloat(obj.im_sell_price),
                        dd_sell_value_final: parseFloat(obj.im_sell_price),

                    }

                    if (flg) {

                        $scope.itemList.push(data)
                        $scope.txt_discounted_price_TextChanged($scope.temp.overalldis);

                    }
                    else {
                        $scope.itemList.pop(data)
                        $scope.txt_discounted_price_TextChanged($scope.temp.overalldis);

                    }
                }

            }

            $scope.CancelItem = function () {
                $scope.temp['finalValue'] = ''
                $scope.temp['im_sell_price'] = ''
                $scope.temp['qty'] = ''
                $scope.temp['loc_code'] = ''
                $scope.temp['subcategory_code'] = ''
                $scope.temp['category_code'] = ''
                $scope.temp['gcode'] = ''
                $scope.rcbSubCategories = [];
                $scope.im_item_code1 = '';
                $("#rcbItem").select2("val", "");


            }

            $scope.totalAmtClick = function () {

                $scope.totalFinal = 0;

                for (var i = 0; i < $scope.itemList.length; i++) {

                    $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_final);
                }
                $scope.temp['doc_total_amount'] = $scope.totalFinal;


                if ($scope.temp.doc_discount_amount == undefined || $scope.temp.doc_discount_amount == '' || isNaN(parseFloat($scope.temp.doc_discount_amount))) {
                }
                else {
                    $scope.temp.doc_total_amount -= parseFloat($scope.temp.doc_discount_amount);
                }

                //   $scope.temp['totalFinal'] = $scope.totalFinal + (parseFloat($scope.temp.otherCharges) - parseFloat($scope.temp.disamt));



            }

            $scope.GetSalesDepartMents = function (str) {

                var dep_code = '';
                for (var i = 0; i < $scope.salesman.length; i++) {
                    if ($scope.salesman[i].sm_code == $scope.temp.sm_code) {
                        $scope.temp['dep_code'] = $scope.salesman[i].dep_code;
                    }
                }


                $scope.GetAllDepartments1 = [];
                $scope.GetAllDepartments2 = [];

                $scope.GetAllDepartmentsdemo1 = [];
                $scope.GetAllDepartmentsdemo2 = [];

                $http.get(ENV.apiUrl + "api/InterDepartmentTransfer/GetAllDepartmentsidt").then(function (GetAllDepartments) {

                    $scope.GetAllDepartments = GetAllDepartments.data;

                    for (var i = 0; i < $scope.GetAllDepartments.length; i++) {
                        if ($scope.GetAllDepartments[i].dep_code == $scope.temp.dep_code) {
                            $scope.GetAllDepartmentsdemo1.push($scope.GetAllDepartments[i])
                        }
                        else {
                            $scope.GetAllDepartmentsdemo2.push($scope.GetAllDepartments[i])
                        }
                    }

                    $scope.GetAllDepartments1 = $scope.GetAllDepartmentsdemo1;
                    $scope.GetAllDepartments2 = $scope.GetAllDepartmentsdemo2;

                    $scope.temp['dep_code_from'] = $scope.GetAllDepartments1[0].dep_code;

                });
            }

            $scope.GetInternalAccount = function (str) {
                $http.get(ENV.apiUrl + "api/InterDepartmentTransfer/GetInternal_accountcpdeidt?usercd=" + str).then(function (GetInternal_accountcpde) {

                    $scope.GetInternal_accountcpde = GetInternal_accountcpde.data;

                    console.log($scope.GetInternal_accountcpde);
                });

            }

            $scope.overallDis = function (str) {
                $scope.txt_discounted_price_TextChanged(str);

            }

            $scope.txt_discounted_price_TextChanged = function (str) {


                $scope.temp['doc_total_amount'] = 0;

                if (str != undefined && str != "") {
                    if (str <= 100 || str <= '100') {

                        $scope.totalFinal = 0;
                        for (var i = 0; i < $scope.itemList.length; i++) {

                            $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_final);
                        }
                        if (str == undefined || str == '') {
                            $scope.temp['doc_discount_amount'] = 0;
                        }
                        else {
                            $scope.temp['doc_discount_amount'] = (parseFloat($scope.totalFinal)) * (parseFloat(str) / 100);
                        }
                        $scope.df = ($scope.totalFinal) - $scope.temp.doc_discount_amount;

                        $scope.temp['doc_total_amount'] = $scope.totalFinal;
                        if ($scope.temp.otherCharges == undefined || $scope.temp.otherCharges == '') {
                        }
                        else {
                            $scope.temp.doc_total_amount += parseFloat($scope.temp.otherCharges);
                        }

                        if ($scope.temp.doc_discount_amount == undefined || $scope.temp.doc_discount_amount == '') {
                        }
                        else {
                            $scope.temp.doc_total_amount -= parseFloat($scope.temp.doc_discount_amount);
                        }

                    }
                    else {
                        $scope.totalFinal = 0;
                        for (var i = 0; i < $scope.itemList.length; i++) {

                            $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_final);
                        }

                        $scope.temp['doc_total_amount'] = $scope.totalFinal;
                        //  str = 0;
                        $scope.temp['overalldis'] = 0;
                        $scope.temp['doc_discount_amount'] = 0;
                        if ($scope.temp.otherCharges == undefined || $scope.temp.otherCharges == '') {
                        }
                        else {
                            $scope.temp.doc_total_amount += parseFloat($scope.temp.otherCharges);
                        }

                        if ($scope.temp.doc_discount_amount == undefined || $scope.temp.doc_discount_amount == '') {
                        }
                        else {
                            $scope.temp.doc_total_amount -= parseFloat($scope.temp.doc_discount_amount);
                        }

                        swal({ title: "Alert", text: "discount Cant greater than 100%.", imageUrl: "assets/img/notification-alert.png", });

                    }
                }
                else {
                    for (var i = 0; i < $scope.itemList.length; i++) {

                        $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_final);
                    }
                    $scope.temp['doc_total_amount'] = $scope.totalFinal;
                    $scope.temp['doc_discount_amount'] = '0';
                    $scope.temp['overalldis'] = '0';

                }


            }

            $scope.paymentChange = function (str) {
                if (str == 'CS') {
                    $scope.chequeShow = false;
                    $scope.bankShow = false;
                }
                else if (str == 'CH') {
                    $scope.chequeShow = true;
                    $scope.bankShow = false;
                }
                else if (str == 'BT') {
                    $scope.chequeShow = false;
                    $scope.bankShow = true;
                }

            }

            $scope.removeItem = function (obj, index) {

                $scope.itemList.splice(index, 1);

                if ($scope.itemList.length == 0) {
                    $scope.addtable = false;
                }

                $scope.txt_discounted_price_TextChanged($scope.temp.overalldis);
            }

            $scope.onlyNumbers = function (event) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


            $scope.Reset = function () {
                $scope.temp1 = '';
                $scope.itemtable = false;

            }

            $scope.SearchEmployee = function () {
                $scope.temp1 = '';
                $scope.GetMapped_Employee = [];
                $scope.table = false;
              
                setTimeout(function () {
                    $('#Select3').focus();
                }, 300);

                $('#stdSearch').modal('show');
            }

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2015&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;

            });

            $scope.table = false;
            $scope.busy = false;

            $scope.Show = function () {

                $scope.GetMapped_Employee1 = [];
                $scope.GetMapped_Employee = [];

                if ($scope.temp1.co_company_code != "" && $scope.temp1.co_company_code != undefined) {
                    $scope.busy = true;
                    $scope.table = false;
                    $http.get(ENV.apiUrl + "api/EmployeeLeave/GetEmployeeLeaveAssign?simsobj=" + JSON.stringify($scope.temp1)).then(function (GetMappedEmployee) {
                        $scope.GetMapped_Employee1 = GetMappedEmployee.data;

                        if (GetMappedEmployee.data.length > 0) {
                            $scope.GetMapped_Employee = $scope.GetMapped_Employee1;
                            $scope.table = true;
                            $scope.busy = false;

                            setTimeout(function () {
                                $("#mycell td:first").focus();

                            }, 300);
                           
                        }

                        else {
                            $scope.table = false;
                            $scope.busy = false;
                            swal({
                                text: 'Data Not Available',
                                width: 300,
                                height: 300
                            });
                        }
                    });
                }
                else {

                    $scope.table = false;
                    $scope.busy = false;
                    swal({
                        text: 'Please Select Company Name',
                        width: 300,
                        height: 300
                    });

                }

            }

            $scope.SelectedUserName = function (info) {
                $scope.temp.up_name1 = info.employee_name;
                $scope.temp.up_name = info.emp_code;

                setTimeout(function () {
                    $('#txt_doc_narration').focus();
                }, 300);

                $('#stdSearch').modal('hide');

            }

            $scope.ApprovedItemsToDepartMent = function () {
                $scope.falg = false;

                for (var i = 0; i < $scope.maindatasend.length; i++) {
                    $scope.maindatasend[i].cus_account_no = $scope.cus_account_no;
                    $scope.falg = true;
                }

                if ($scope.falg == true) {
                    swal({
                        text: "Are you sure you want to approved?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/ApproveIDT/CUDSale_Documents", $scope.maindatasend).then(function (Approve) {
                                $scope.Approve1 = Approve.data;

                                if ($scope.Approve1 == true) {
                                    swal({ text: 'Item Approved successfully', width: 300, showCloseButton: true });
                                    $scope.approvebutton = false;
                                }
                                else {
                                    swal({ text: 'Item not Approved', width: 300, showCloseButton: true })
                                }
                            })
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                                $scope.row1 = '';
                            }
                        }

                    });
                }
                else {
                    swal({ text: 'There Are No Record To Approved', width: 300, showCloseButton: true });
                }
            }
           

            $(window).bind('keydown', function (event) {

                if (event.ctrlKey || event.metaKey) {
                    switch (String.fromCharCode(event.which).toLowerCase()) {
                        case 'e':
                            event.preventDefault();
                            $scope.flag = false;
                         
                            $scope.table = false;
                           
                            setTimeout(function () {
                                $('#Select3').focus();
                                $scope.temp1 = '';
                                $scope.GetMapped_Employee = [];
                            }, 300);

                            $('#stdSearch').modal('show');
                           
                            break;
                        case 'q':
                            event.preventDefault();
                            $scope.temp1 = '';
                            $scope.GetMapped_Employee = [];
                            $scope.GetItems = [];
                            $scope.temp1 = {};
                            $('#stdSearch').modal('hide');
                            $('#itemSearch').modal('hide');
                            break;
                        case 'i':
                            event.preventDefault();
                           
                            $scope.flag = false;
                            $scope.itemtable = false;
                          
                            $scope.itemsetflg = false;
                            $scope.ctrl = true;

                            setTimeout(function () {
                                $scope.GetItems = [];
                                $scope.temp1 = {};
                                $('#Desc_Res').focus();
                            }, 300);

                            $('#itemSearch').modal('show');
                         

                    }
                }
            });

        }])
})();