﻿(function () {
    'use strict';
    var opr = '';
    var customcode = [];
    var main;
    var data1 = [];
    var data = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CustomClearanceCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {


            $scope.pagesize = '5';
            $scope.table1 = true;
            $scope.operation = false;
            $scope.editmode = false;

            $http.get(ENV.apiUrl + "api/CustomClearanceDetails/getCustomClearanceDetails").then(function (getCustomClearanceDetails_Data) {
                $scope.CustomClearance = getCustomClearanceDetails_Data.data;
                $scope.totalItems = $scope.CustomClearance.length;
                $scope.todos = $scope.CustomClearance;
                $scope.makeTodos();
                console.log($scope.CustomClearance);

            });

            $http.get(ENV.apiUrl + "api/common/getShipmentNo").then(function (getShipmentNo_Data) {
                $scope.ShipmentNo = getShipmentNo_Data.data;
            });

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.New = function () {
                $scope.edt = "";
                $scope.check = true;

                $scope.editmode = false;
                $scope.newmode = true;
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.up = function (str) {

                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.readonly = true;

                $scope.edt = {

                    ccl_reg_no: str.ccl_reg_no,
                    ccl_date: str.ccl_date,
                    sr_shipment_no: str.sr_shipment_no,
                    ccl_customs_entry_no: str.ccl_customs_entry_no,
                    ccl_customs_entry_date: str.ccl_customs_entry_date,
                    ccl_remarks: str.ccl_remarks,
                    ccl_bill_of_lading_date: str.ccl_bill_of_lading_date,
                    ccl_bill_of_lading: str.ccl_bill_of_lading,
                }
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Save = function (myForm) {
                debugger
                if (myForm) {
                    data = [];
                    data1 = [];
                    data = $scope.edt;
                    data.opr = 'I';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/CustomClearanceDetails/CustomClearanceCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        debugger
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                        }

                        $http.get(ENV.apiUrl + "api/CustomClearanceDetails/getCustomClearanceDetails").then(function (getCustomClearanceDetails_Data) {
                            $scope.CustomClearance = getCustomClearanceDetails_Data.data;
                            $scope.totalItems = $scope.CustomClearance.length;
                            $scope.todos = $scope.CustomClearance;
                            $scope.makeTodos();
                        });
                    });
                    $scope.table1 = true;
                    $scope.operation = false;
                }

            }

            $scope.Update = function (myForm) {
                debugger
                if (myForm) {
                    data = [];
                    data1 = [];
                    data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/CustomClearanceDetails/CustomClearanceCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                        $http.get(ENV.apiUrl + "api/CustomClearanceDetails/getCustomClearanceDetails").then(function (getCustomClearanceDetails_Data) {
                            $scope.CustomClearance = getCustomClearanceDetails_Data.data;
                            $scope.totalItems = $scope.CustomClearance.length;
                            $scope.todos = $scope.CustomClearance;
                            $scope.makeTodos();
                        });
                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                }

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].ccl_reg_no + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].ccl_reg_no + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.deleterecord = function () {

                customcode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].ccl_reg_no + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletecode = ({
                            'ccl_reg_no': $scope.filteredTodos[i].ccl_reg_no,
                            opr: 'D'
                        });
                        customcode.push(deletecode);
                    }
                }
                debugger
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger
                            $http.post(ENV.apiUrl + "api/CustomClearanceDetails/CustomClearanceCUD", customcode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/CustomClearanceDetails/getCustomClearanceDetails").then(function (getCustomClearanceDetails_Data) {
                                                $scope.CustomClearance = getCustomClearanceDetails_Data.data;
                                                $scope.totalItems = $scope.CustomClearance.length;
                                                $scope.todos = $scope.CustomClearance;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted, It Mapped", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/CustomClearanceDetails/getCustomClearanceDetails").then(function (getCustomClearanceDetails_Data) {
                                                $scope.CustomClearance = getCustomClearanceDetails_Data.data;
                                                $scope.totalItems = $scope.CustomClearance.length;
                                                $scope.todos = $scope.CustomClearance;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].ccl_reg_no + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }

                $scope.currentPage = true;
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.CustomClearance, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CustomClearance;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */

                return (
                     item.sr_shipment_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.ccl_customs_entry_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.ccl_remarks.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.ccl_bill_of_lading.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.ccl_reg_no == toSearch) ? true : false;


            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);



        }])
})();





