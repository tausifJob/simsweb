﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ItemGradeSectionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.display = false;
            $scope.busyindicator = false;
            $scope.grid = true;
            $scope.color = '#edefef';
            $scope.rgvtbl = false;
            $scope.buzy = false;
            
            var grade = '', sec = '';
            $scope.updatebtn = false;
            $scope.edt = '';




            $http.get(ENV.apiUrl + "api/ItemGradeSection/getCuriculum").then(function (getCuriculum) {
                $scope.getCuriculum = getCuriculum.data;

            });

            $scope.select_cur = function (str) {

                $http.get(ENV.apiUrl + "api/ItemGradeSection/getAcademicYear?curCode=" + str).then(function (getAcademicYear) {
                    $scope.getAcademicYear = getAcademicYear.data;

                });
            }

            $scope.select_aca = function (str) {

                $http.get(ENV.apiUrl + "api/ItemGradeSection/getAllGrades?cur_code=" + $scope.edt.cur + "&ac_year=" + str).then(function (getAllGrades) {
                    $scope.getAllGrades = getAllGrades.data;
                    setTimeout(function () {
                        $('#cmb_grade').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $scope.select_grade = function () {

                var grdcoll = [];
                for (var a = 0; a < $scope.edt.grade.length; a++) {
                    var d = {
                        cur: $scope.edt.cur,
                        aca: $scope.edt.aca,
                        grade: $scope.edt.grade[a]
                    }
                    grdcoll.push(d);
                }

                $http.post(ENV.apiUrl + "api/ItemGradeSection/AllSections", grdcoll).then(function (AllSections) {
                    $scope.AllSections = AllSections.data;

                    setTimeout(function () {
                        $('#cmb_section').change(function () {
                        }).multipleSelect({
                            width: '100%'
                        });
                        $("#cmb_section").multipleSelect("checkAll");
                    }, 1000);


                });
            }

            $http.get(ENV.apiUrl + "api/ItemGradeSection/getCat").then(function (getCat) {
                $scope.getCat = getCat.data;
            });

            $scope.select_cat = function (str) {

                $http.get(ENV.apiUrl + "api/ItemGradeSection/getSubCat?cat=" + str).then(function (getSubCat) {
                    $scope.getSubCat = getSubCat.data;

                });
            }

            $scope.Show_data = function () {
                var grdcoll = [];
                if ($scope.edt.cur == undefined || $scope.edt.cur == '' || $scope.edt.aca == undefined || $scope.edt.aca == '' || $scope.edt.section.length <= 0) {
                    swal('', 'Curriculum,Academic Year,Grade and Section are mandatory.');
                }
                else {
                    for (var a = 0; a < $scope.edt.section.length; a++) {
                        var d = {
                            cur: $scope.edt.cur,
                            aca: $scope.edt.aca,
                            section: $scope.edt.section[a],
                            cat: $scope.edt.cat,
                            sub_cat: $scope.edt.sub_cat,
                        }
                        grdcoll.push(d);
                    }

                    $scope.rgvtbl = false;
                    $scope.buzy = true;
                    $http.post(ENV.apiUrl + "api/ItemGradeSection/allData", grdcoll).then(function (allData) {
                        $scope.allData = allData.data;

                        if ($scope.allData.length > 0) {
                            $scope.updatebtn = true;
                            $scope.rgvtbl = true;
                            $scope.buzy = false
                        }


                    });
                }
            }

            $scope.Update_all = function () {
                console.log($scope.allData);
                var datacoll = [];
                var deletecoll = [];
                debugger;
                for (var j = 0; j < $scope.edt.section.length; j++) {

                    for (var i = 0; i < $scope.allData.length; i++) {
                        if ($scope.allData[i].isselect) {
                            var data = {
                                item_no: $scope.allData[i].item_no,
                                cur: $scope.edt.cur,
                                aca: $scope.edt.aca,
                                section: $scope.edt.section[j],
                                qty_for_parent: $scope.allData[i].qty_for_parent,
                                qty_flag: $scope.allData[i].qty_flag
                            }
                            datacoll.push(data);
                        }
                        else {
                            if ($scope.allData[i].grade != '') {
                                var data = {
                                    item_no: $scope.allData[i].item_no,
                                    cur: $scope.edt.cur,
                                    aca: $scope.edt.aca,
                                    section: $scope.edt.section[j],
                                    qty_for_parent: $scope.allData[i].qty_for_parent,
                                    qty_flag: $scope.allData[i].qty_flag
                                }
                                deletecoll.push(data);
                            }
                        }
                    }
                }

                if (deletecoll.length > 0)
                {
                    $http.post(ENV.apiUrl + "api/ItemGradeSection/UDeleteitemgrdsec", deletecoll).then(function (UDeleteitemgrdsec) {
                        $scope.UDeleteitemgrdsec = UDeleteitemgrdsec.data;
                     
                    });
                }

                if (datacoll.length > 0)
                    $http.post(ENV.apiUrl + "api/ItemGradeSection/Updateitemgrdsec", datacoll).then(function (Updateitemgrdsec) {
                        $scope.Updateitemgrdsec = Updateitemgrdsec.data;
                        if ($scope.Updateitemgrdsec)
                            swal('', 'Record inserted successfully.');
                        else
                            swal('', 'Record not inserted.');
                    });


            }

            $scope.changeno = function (obj) {
                debugger;
                console.log(obj)
                $scope.maindata = obj;

                if ((parseInt($scope.maindata.item_qty) < parseInt($scope.maindata.qty_for_parent))) {
                    swal('Please enter quantity in between 0 to ' + $scope.maindata.item_qty, '');
                    $scope.maindata.qty_for_parent = $scope.maindata.item_qty;
                }
                //else {

                //    if (parseFloat($scope.maindata.leave_taken) > parseFloat($scope.maindata.leave_max)) {
                //        swal('', 'Maximum leave not less than taken leave.');
                //        $scope.maindata.leave_max = $scope.maindata.leave_max_old;
                //    }
                //    else
                //        $scope.maindata.leave_max_old = $scope.maindata.leave_max;
                //}
            }

            $scope.reset_form = function () {
                $scope.edt = '';
                $scope.allData = [];
                $scope.updatebtn = false;
                main = document.getElementById('grademainchk');
                main.checked = false;
                $scope.rgvtbl = false;
                $scope.buzy = false;
                try {
                    $("#cmb_grade").multipleSelect("uncheckAll");
                }
                catch (e) {                }
                try {
                    $("#cmb_section").multipleSelect("uncheckAll");
                }
                catch (e) { }
                
            }

            $(function () {
                $('#cmb_grade').multipleSelect({ width: '100%' });
                $('#cmb_section').multipleSelect({ width: '100%' });
            });

            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1 });
                $("#Table2").tableHeadFixer({ 'top': 1 });
                $("#Table3").tableHeadFixer({ 'top': 1 });
                $("#Table4").tableHeadFixer({ 'top': 1 });
            }, 100);


            $scope.CheckGradeMultiple = function () {
                debugger;
                main = document.getElementById('grademainchk');
                for (var i = 0; i < $scope.allData.length; i++) {

                    
                    if (main.checked == true) {
                        $scope.allData[i].isselect = true;
                    }
                    else {
                        $scope.allData[i].isselect = false;
                        main.checked = false;
                    }
                }
            }

            $scope.gradeCheckOneByOne = function (str) {
                debugger;
                main = document.getElementById('grademainchk');

                for (var i = 0; i < $scope.allData.length; i++) {
                    if (str.item_no == $scope.allData[i].item_no)
                    {
                        if (str.isselect == false)
                            main.checked = false;
                    }
                }
            }


















            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


        }])
})();