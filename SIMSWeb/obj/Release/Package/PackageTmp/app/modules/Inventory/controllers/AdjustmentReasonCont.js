﻿(function () {
    'use strict';
    var opr = '';
    var data1 = [];
    var data = [];
    var main, edt;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('AdjustmentReasonCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.edt = "";
            $scope.adjustReasonDetail = true;
            $scope.editmode = false;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }
            $scope.cancel = function () {
                $scope.adjustReasonDetail = true;
                $scope.adjustReasonData = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                opr = 'S';
                $scope.editmode = false;
                $scope.edt = "";
                $scope.newmode = true;
                $scope.readonly = false;
                $scope.adjustReasonDetail = false;
                $scope.adjustReasonData = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                console.log("begin=" + begin); console.log("end=" + end);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.AdjustReason, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.AdjustReason;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.check_all();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                    item.dep_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.ar_account_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.ar_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.ar_desc == toSearch) ? true : false;

            }

            $http.get(ENV.apiUrl + "api/AdjustmentReason/getAdjustmentDetail").then(function (Adjust_Reason_Data) {
                $scope.AdjustReason = Adjust_Reason_Data.data;
                $scope.totalItems = $scope.AdjustReason.length;
                $scope.todos = $scope.AdjustReason;
                $scope.makeTodos();
                console.log($scope.AdjustReason);
            });

            $http.get(ENV.apiUrl + "api/AdjustmentReason/GetGLAccountNumber").then(function (docstatus1) {
                debugger
                $scope.GlACNO = docstatus1.data;
                console.log($scope.docstatus1);
            });

            $http.get(ENV.apiUrl + "api/AdjustmentReason/getDepartment").then(function (docstatus) {
                debugger
                $scope.getDepa = docstatus.data;
                console.log($scope.docstatus1);
            });

            $scope.Save = function (myForm) {
                if (myForm) {
                    data = [];
                    data1 = [];
                    data = $scope.edt;
                    data.opr = 'I';
                    $scope.exist = false;
                    for (var i = 0; i < $scope.AdjustReason.length; i++) {
                        if ($scope.AdjustReason[i].dep_code == data.dep_code && $scope.AdjustReason[i].ar_account_no == data.ar_account_no) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ title: "Alert", text: "Record Already exists", width: 300, height: 200 });
                    }

                    else {
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/AdjustmentReason/AdjustmentReasonCUD?simsobj=", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                            }
                            $http.get(ENV.apiUrl + "api/AdjustmentReason/getAdjustmentDetail").then(function (Adjust_Reason_Data) {
                                $scope.AdjustReason = Adjust_Reason_Data.data;
                                $scope.totalItems = $scope.AdjustReason.length;
                                $scope.todos = $scope.AdjustReason;
                                $scope.makeTodos();
                            });
                        });
                        $scope.adjustReasonDetail = true;
                        $scope.adjustReasonData = false;
                    }
                    data1 = [];
                }
            }

            $scope.up = function (str) {
                debugger
                opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.adjustReasonDetail = false;
                $scope.adjustReasonData = true;
                $scope.readonly = true;
               // $scope.edt = str;
                $scope.edt = {
                    ar_account_no :str.ar_account_no,
                    ar_desc   :str.ar_desc,
                    dep_code  :str.dep_code,
                    ar_indicator: str.ar_indicator,
                    ar_code: str.ar_code,
                };
             

            }

            $scope.Update = function (myForm) {
                debugger
                if (myForm) {
                    data = $scope.edt;
                    data1.push(data);
                    data.opr = 'U';
                    $http.post(ENV.apiUrl + "api/AdjustmentReason/AdjustmentReasonCUD?simsobj=", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                        $http.get(ENV.apiUrl + "api/AdjustmentReason/getAdjustmentDetail").then(function (Adjust_Reason_Data) {
                            $scope.AdjustReason = Adjust_Reason_Data.data;
                            $scope.totalItems = $scope.AdjustReason.length;
                            $scope.todos = $scope.AdjustReason;
                            $scope.makeTodos();
                           
                        });
                    })
                    $scope.adjustReasonData = false;
                    $scope.adjustReasonDetail = true;
                    data1 = [];

                }
            }

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.check_once = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }

            $scope.deleterecord = function () {
                debugger;
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'ar_code': $scope.filteredTodos[i].ar_code,
                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        confirmButtonText: 'Yes',
                        showCancelButton: true,
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/AdjustmentReason/AdjustmentReasonCUD", deleteleave).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/AdjustmentReason/getAdjustmentDetail").then(function (Adjust_Reason_Data) {
                                                $scope.AdjustReason = Adjust_Reason_Data.data;
                                                $scope.totalItems = $scope.AdjustReason.length;
                                                $scope.todos = $scope.AdjustReason;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                        }
                                               $scope.currentPage = true;
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/AdjustmentReason/getAdjustmentDetail").then(function (Adjust_Reason_Data) {
                                                $scope.AdjustReason = Adjust_Reason_Data.data;
                                                $scope.totalItems = $scope.AdjustReason.length;
                                                $scope.todos = $scope.AdjustReason;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                        }
                                    });
                                }
                            });
                        }
                        else {
                            main = document.getElementById('all_chk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }


        }]);

})();



