﻿(function () {
    'use strict';
    var del = [];
    var main;
    var date1;
    var date3;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('ApprovedIDTCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '5';
            $scope.save_btn = true;
            var user = $rootScope.globals.currentUser.username;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.grid = true;
            $scope.itemtable = false;
            $scope.temp1 = {
                im_inv_no: undefined, invs021_sg_name: undefined, im_item_code: undefined, im_desc: undefined, invs021_dep_code: undefined, sec_code: undefined, invs021_sup_code: undefined, im_assembly_ind_s: undefined, category_code: undefined, subcategory_code: undefined

            };
            getdata();
            $scope.itemsPerPage = '5';
            $scope.currentPage = 0;
            $scope.obj = [];
            $scope.itemList = [];
            $scope.temp = {};

            setTimeout(function () {
                $("#rcbItem").select2();

            }, 100);

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd
            }

            if (mm < 10) {
                mm = '0' + mm
            }

            today = yyyy + '/' + mm + '/' + dd;
            $scope.temp['creation_date'] = today;
            $scope.temp['creation_user'] = user;
            $scope.temp['doc_prov_date'] = today;

            $scope.paymodeList = [
                { name: 'Cash', value: 'CS' },
                 { name: 'Cheques', value: 'CH' },
                  { name: 'Bank Transfer', value: 'BT' },
            ]

            $scope.temp['dt_code'] = '00';

            $scope.temp['paymentMode'] = 'CS';


            function getdata() {
                $http.get(ENV.apiUrl + "api/ApproveIDT/GetAllItemsforApprove").then(function (GetAllItemsforApprove) {
                    $scope.GetAllItemsforApprove = GetAllItemsforApprove.data;
                    $scope.totalItems = $scope.GetAllItemsforApprove.length;
                    $scope.todos = $scope.GetAllItemsforApprove;
                    $scope.makeTodos();

                });


            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }

            };

            $scope.size = function (str) {
                
                if (str == 'All')
                {
                    $scope.filteredTodos = $scope.GetAllItemsforApprove;
                    $scope.currentPage = '1';
                }
                else
                {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.GetAllItemsforApprove, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.GetAllItemsforApprove;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.doc_prov_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.im_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  || item.sm_name == toSearch) ? true : false;
            }

            $scope.ViewDetails = function (info) {
                $scope.Itemdata = [];

                $scope.Itemdata = info.appIDTList;

                console.log($scope.Itemdata);

                $('#stdSearch').modal('show');
            }

            var dom;
            $scope.flag = true;
            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='10'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0' width='100%' style='border-color:red;border:solid;border-width:02px'><tbody>" +
                         "<tr><td class='semi-bold'>" + "ITEM QUANTITY" + "</td> <td class='semi-bold'>" + "SELL PRICE" + "</td><td class='semi-bold'>" + "TOTAL AMOUNT" +
                         "</td><td class='semi-bold'>" + "DISCOUNT AMOUNT" + "</td><td class='semi-bold'>" + "FINAL AMOUNT" + "</td></tr>" +
                          "<tr><td>" + info.dd_qty + "</td> <td>" + info.dd_sell_price + " </td><td>" + info.doc_total_amount +
                          "</td><td>" + info.doc_discount_amount + "</td><td>" + info.dd_sell_price_final + "</td> </tr>" +
                        " </table></td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                } else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }
            };

            $scope.UpdateData = function () {

                var datasend = [];
                $scope.falg = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var d = document.getElementById($scope.filteredTodos[i].doc_prov_no);
                    if (d.checked == true) {
                        $scope.falg = true;
                        datasend=$scope.filteredTodos[i].appIDTList;
                    }
                }

                if ($scope.falg == true) {
                    swal({
                        text: "Are you sure you want to Approve?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/ApproveIDT/CUDSale_Documents", datasend).then(function (Approve) {
                                $scope.Approve1 = Approve.data;

                                if ($scope.Approve1 == true) {
                                    swal({ text: 'Item Approved successfully', width: 300, showCloseButton: true });
                                    getdata();
                                }
                                else {
                                    swal({ text: 'Item not Approved', width: 300, showCloseButton: true })
                                }
                            })
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                                $scope.row1 = '';
                            }
                            $scope.checkAll();
                        }

                    });
                }
                else {
                    swal({ text: 'Select at least one record', width: 300, showCloseButton: true });
                }
            }

            $scope.checkAll=function()
            {
                main = document.getElementById('mainchk');

                for (var i = 0; i < $scope.filteredTodos.length; i++)
                {
                    var d = document.getElementById($scope.filteredTodos[i].doc_prov_no);
                    if (main.checked == true) {
                        d.checked = true;
                        $('tr').addClass("row_selected");
                    }
                    else {
                        d.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }

               
            }

            $scope.setClickedRow=function()
            {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }
            }

            $timeout(function () {
                
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#example").tableHeadFixer({ 'top': 1 });
            }, 100);
        }])
})();