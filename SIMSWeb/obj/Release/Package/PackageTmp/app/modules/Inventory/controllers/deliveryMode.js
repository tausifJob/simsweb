﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('deliveryMode',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            //$scope.cmbstatus = true;
            //$scope.checked = false;


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

        
            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/DeliveryMode/getDeliveryMode").then(function (getDeliveryMode_Data) {

                debugger;
                $scope.DeliveryMode = getDeliveryMode_Data.data;
                $scope.totalItems = $scope.DeliveryMode.length;
                $scope.todos = $scope.DeliveryMode;
                $scope.makeTodos();
               // $scope.makeTodos();

            });


            // Bind Value to the DropDown

            $http.get(ENV.apiUrl + "api/DeliveryMode/getType").then(function (getType_Data) {
                $scope.getType = getType_Data.data;
            });


            
            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();

                main.checked = false;
                $scope.row1 = '';
                // $scope.CheckAllChecked();

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);

                });
            };

            //Search
            $scope.search = function () {

                $scope.todos = $scope.searched($scope.DeliveryMode, $scope.searchText);   // DeliveryMode bind here
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.DeliveryMode;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */

                debugger;
                return (item.dm_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.dm_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.dm_type == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {

                $scope.disabled = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.divcode_readonly = false;
                $scope.temp = "";
              
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $http.get(ENV.apiUrl + "api/DeliveryMode/getAutoGenerateCode").then(function (getAutoGenerateCode) {

                    $scope.getAutoGenerateCode = getAutoGenerateCode.data;

                    $scope.temp = {dm_code: $scope.getAutoGenerateCode };
                        

                })
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                debugger;
                $scope.temp = undefined;
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

               
            }

            //DATA EDIT
            $scope.edit = function (str) {
               
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.divcode_readonly = true;
                // $scope.temp = str;

                $scope.temp = str;

            }

            //DATA SAVE INSERT
           
            $scope.savedata = function (Myform) {
                var datasend = [];
                if (Myform) {
                    var data = $scope.temp;
                    data.opr = 'I';
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/DeliveryMode/CUDDeliveryMode", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Inserted Successfully", width: 380, height: 200 });
                         

                            $http.get(ENV.apiUrl + "api/DeliveryMode/getDeliveryMode").then(function (getDeliveryMode_Data) {

                                $scope.DeliveryMode = getDeliveryMode_Data.data;
                                $scope.totalItems = $scope.DeliveryMode.length;
                                $scope.todos = $scope.DeliveryMode;
                                $scope.makeTodos();
                                // $scope.makeTodos();
                            });


                        }
                        else {
                            swal({ title: "Alert", text: "This value is allready present", width: 380, height: 200 });
                        }

                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                if (Myform) {

                    var data = $scope.temp;
                    data.opr = "U";
                    dataforUpdate.push(data);
                    //dataupdate.push(data);
                    $http.post(ENV.apiUrl + "api/DeliveryMode/CUDDeliveryMode", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {

                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });

                            $http.get(ENV.apiUrl + "api/DeliveryMode/getDeliveryMode").then(function (getDeliveryMode_Data) {

                                $scope.DeliveryMode = getDeliveryMode_Data.data;
                                $scope.totalItems = $scope.DeliveryMode.length;
                                $scope.todos = $scope.DeliveryMode;
                                $scope.makeTodos();
                                // $scope.makeTodos();
                            });

                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                      

                    });
                    dataforUpdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {

                debugger;

                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'dm_code': $scope.filteredTodos[i].dm_code,
                                                  
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/DeliveryMode/CUDDeliveryMode", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {


                                  $http.get(ENV.apiUrl + "api/DeliveryMode/getDeliveryMode").then(function (getDeliveryMode_Data) {

                                                $scope.DeliveryMode = getDeliveryMode_Data.data;
                                                $scope.totalItems = $scope.DeliveryMode.length;
                                                $scope.todos = $scope.DeliveryMode;
                                                $scope.makeTodos();
                                                // $scope.makeTodos();
                                            });


                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }

                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {

                                $http.get(ENV.apiUrl + "api/DeliveryMode/getDeliveryMode").then(function (getDeliveryMode_Data) {

                                                $scope.DeliveryMode = getDeliveryMode_Data.data;
                                                $scope.totalItems = $scope.DeliveryMode.length;
                                                $scope.todos = $scope.DeliveryMode;
                                                $scope.makeTodos();
                                                // $scope.makeTodos();
                                            });


                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            //main.checked = false;
                                            // $('tr').removeClass("row_selected");
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            debugger
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                    //  $scope.row1 = '';

                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });


        }])

})();
