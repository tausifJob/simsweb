﻿(function () {
    'use strict';
    var opr = '';
    var costingcode = [];
    var main;
    var data1 = [];
    var data = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CostingSheetApprovalCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.ip = {};
            $scope.CSEData = [];
            $scope.set = {};
            $scope.gTotal = 0;
            $http.get(ENV.apiUrl + "api/CreateCostingSheet/getDepartments").then(function (getDepartments_Data) {
                $scope.Departments_Data = getDepartments_Data.data;
            });

            $http.get(ENV.apiUrl + "api/CreateCostingSheet/getSupplier").then(function (getSupplier_Data) {
                $scope.Supplier_Data = getSupplier_Data.data;
            });

            $scope.up = function (str) {
                $("#SearchCostingSheetModal").modal('hide');
                $scope.edt = {
                    req_type: 'I',
                    shipmentNO: str.shipmentNO
                }
                $scope.getAllShipmentDetails($scope.edt);
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            /*Expenses*/
            $scope.searchExpenses = function () {
                $("#CreateEditEXPMain").modal('show');
            }

            $scope.getCostingExpensesDetails = function (row) {
                console.log(row);

                var ob = {};
                ob.cs_prov_no = $scope.ip.cs_prov_no;
                $scope.tob = ob;
                ob.val = Math.random() * 100;
                $http.post(ENV.apiUrl + "api/AdjustmentReason/SheetExpGET", ob).then(function (res) {
                    console.log(res);
                    $scope.CSEData = res.data.table;
                    $scope.Total_Amount = 0;
                    $scope.expenseSUM();
                    $scope.OrderSUM();
                    //for (var i = 0; i < $scope.CSEData.length; i++) {
                    //    //$scope.Total_Amount = $scope.Total_Amount + (parseFloat($scope.CSEData[i].im_sell_price) * parseInt($scope.OrderData[i].so_shipped_qty))
                    //}
                });
                $("#SearchCostingSheetModal").modal('hide');
            }

            $scope.clear = function () {
                $scope.tob = {};
                $scope.CSEData = [];
                $scope.expenseSUM();
                $scope.OrderSUM();
            }

            $scope.NewExpType = function () {
                //$scope.exp['lbl'] = 'New';
                $scope.edt = {};
                $scope.edt.cse_doc_no = '';
                $scope.edt.cse_doc_date = '';
                $scope.edt.cse_amount = '';
                $scope.edt.pet_desc = '';
                $scope.edt.pet_code = '';
                $scope.edt.isNew = true;
                $scope.edt.cs_prov_no = $scope.ip.cs_prov_no;
                $("#CreateEditEXPMain").modal('hide');
                setTimeout(function () { $("#CreateEditEXP").modal('show'); }, 500)

            }

            $scope.createEditEXPclose = function () {

                $("#CreateEditEXP").modal('hide');
                setTimeout(function () { $("#CreateEditEXPMain").modal('show'); }, 500)
            }

            $scope.EditExpType = function (ob) {
                //$scope.exp['lbl'] = 'Edit';
                $scope.edt = {};
                $scope.edt.cse_doc_no = ob.cse_doc_no;
                $scope.edt.cse_doc_date = ob.cse_doc_date;
                $scope.edt.cse_amount = ob.cse_amount;
                $scope.edt.pet_desc = ob.pet_desc;
                $scope.edt.pet_code = ob.pet_code;
                $scope.edt.cs_prov_no = ob.cs_prov_no;
                $scope.edt.isNew = false;
                $("#CreateEditEXPMain").modal('hide');
                setTimeout(function () { $("#CreateEditEXP").modal('show'); }, 500)
            }

            /*Expenses End*/

            /*Costing Sheet and details*/
            $scope.searchCostingSheets = function (id) {
                if (id == undefined || id == '') {
                    $("#CostingSheetModal").modal('show');
                }
                else {
                    $scope.searchCostingSheets1(id);
                }
            }

            $scope.resetCS = function () {
                $scope.cs = {};
                $scope.cs.cs_to_date = '';
                $scope.cs.cs_from_date = '';
                $scope.cs.cs_no = '';

                $scope.CSDataSearch = [];
            }

            $scope.searchCostingSheets1 = function (id) {
                $http.post(ENV.apiUrl + "api/AdjustmentReason/AllCostingSheetsGet", id).then(function (res) {
                    console.log(res);
                    $scope.CSDataSearch = res.data.table;
                });
            }

            //CS Details
            $scope.GetCostingSheetsDetails = function (id) {
                var ob = {};
                $("#CostingSheetModal").modal('hide');
                ob.orderLineNO = id.cs_prov_no;
                $http.post(ENV.apiUrl + "api/AdjustmentReason/CostingSheetsGetDetails", ob).then(function (res) {
                    console.log(res);
                    /*var data = res['table'];
                    if(data.)*/
                    $scope.ip = res.data.table[0];
                    $scope.OrderData = res.data.table1;
                    $scope.addlDetails = res.data.table2;
                    $scope.CSEData = res.data.table3;
                    $scope.expenseSUM();
                    $scope.OrderSUM();
                    $scope.forupdflag = true;
                    $scope.set.showFinal = true;
                    $scope.set.showExp = true;

                });
            }

            $scope.resetForm = function () {
                $scope.ip = {};
                $scope.OrderData = {};
                $scope.addlDetails = {};
                $scope.CSEData = [];
                $scope.expenseSUM();
                $scope.OrderSUM();
                $scope.ServiceData = [];
                $scope.forupdflag = false;
                $scope.showCsProvDateEdit = false;
                $scope.set.showExp = false;
                $scope.set.showFinal = false;
            }

            //finalize
            $scope.finalizeCostingSheet = function () {
                var id = {};
                id.cs_prov_no = $scope.ip.cs_prov_no;
                $http.post(ENV.apiUrl + "api/AdjustmentReason/FinalizeCostingSheet", id).then(function (res) {
                    swal({ title: "Alert", text: 'Saved successfully', showCloseButton: true, width: 380, });
                    var data = {
                        location: 'Invs.INVR28',
                        parameter: {
                            cs_prov_no: $scope.ip.cs_prov_no,
                        },
                        state: 'main.CostingSheetApproval'
                    }
                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                    $state.go('main.ReportCardParameter')
                    $scope.resetForm();
                });
            }

            $scope.expenseSUM = function () {
                var s = 0;
                for (var i = 0; i < $scope.CSEData.length; i++) {
                    s = s + parseFloat($scope.CSEData[i].cse_amount);
                }
                $scope.expenseSUMV = s.toFixed(2);
                $scope.gTotal = parseFloat(s) + parseFloat($scope.Total_Amount == undefined ? 0 : $scope.Total_Amount);
                return s.toFixed(2);
            }

            $scope.OrderSUM = function () {
                var s = 0;
                for (var i = 0; i < $scope.OrderData.length; i++) {
                    s = s + (parseFloat($scope.OrderData[i].rate) * parseInt($scope.OrderData[i].od_received_qty));
                }
                $scope.Total_Amount = s.toFixed(2);
                $scope.gTotal = parseFloat(s) + parseFloat($scope.expenseSUMV == undefined ? 0 : $scope.expenseSUMV);
                return s.toFixed(2);
            }

        }])
})();