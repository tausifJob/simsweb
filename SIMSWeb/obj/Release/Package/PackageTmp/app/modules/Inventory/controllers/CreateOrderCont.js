﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [], data = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CreateOrderCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.req_no = [];
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);



            //*********************All Combo*********************

            $http.get(ENV.apiUrl + "api/CreateOrder/GetDepartment").then(function (res1) {
                $scope.Department = res1.data;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetServiceTypes").then(function (res2) {
                $scope.ServiceTypes = res2.data;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetSupplierDepartment").then(function (res1) {
                $scope.supplier = res1.data;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetDeliveryMOde").then(function (res1) {
                $scope.Deliverymode = res1.data;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/Getpaymentmodes").then(function (res1) {
                $scope.paymentmode = res1.data;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetTradeterms").then(function (res1) {
                $scope.Trades = res1.data;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetLetterOfCredit").then(function (res1) {
                $scope.LOC = res1.data;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetForwardAgent").then(function (res1) {
                $scope.Agent = res1.data;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetCurrencyMaster").then(function (res1) {
                $scope.CurMaster = res1.data;
            });


            //***************************************************


            $scope.SearchRequest = function () {
                debugger;
                console.log($scope.temp.dep_code);
                console.log($scope.temp.servicevalue);
                if ($scope.temp.dep_code != undefined && $scope.temp.servicevalue != undefined) {
                    $('#MyModal1').modal('show');

                    $http.get(ENV.apiUrl + "api/CreateOrder/Get_RequestDetails_forOrder?dep_code=" + $scope.temp.dep_code + "&req_type=" + $scope.temp.servicevalue).then(function (res12) {
                        debugger;
                        $scope.search_Req_Data = res12.data;
                        $scope.search_Req_Data1 = angular.copy(res12.data);

                        //console.log($scope.search_Req_Data);
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Department and Request Type", width: 300, height: 200 });
                }
            }

            $scope.RemoveEnrollMentNo = function ($event, index, str) {
                str.splice(index, 1);
            }

            $scope.search_Req_Data_all = [];
            $scope.testALL = [];

            $scope.okbuttonclick = function () {

                for (var i = 0; i < $scope.search_Req_Data1.length; i++) {
                    debugger;
                    if ($scope.search_Req_Data1[i].req_no1 == true) {

                        debugger;
                        $scope.testALL.push($scope.search_Req_Data1[i]);
                        for (var a = 0; a < $scope.testALL.length; a++) {
                            $scope.reqnos = $scope.reqnos + ',' + $scope.testALL[a].req_no;
                        }

                        //$scope.search_Req_Data_all = $scope.search_Req_Data[i];
                        ////$scope.search_Req_Data_all[i].icon = 'fa fa-plus-circle';
                        //$scope.req_no.push($scope.search_Req_Data_all[i]);
                    }
                    $scope.Main_table = true;
                }
            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();

                main.checked = false;
                $scope.row1 = '';
                // $scope.CheckAllChecked();

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);


            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {

                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_library_attribute_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_library_attribute_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

            $scope.Cancel1 = function () {
                debugger;
                $scope.temp = "";
                $scope.search_Req_Data = null;
                $scope.testALL = null;
                $state.go('main.Inv044');
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

                swal({ title: "Alert", text: "In Cancel", width: 300, height: 200 });
                           }

            //DATA CANCEL
            $scope.Cancel = function () {
                debugger;
                $scope.temp = "";
                $scope.search_Req_Data = null;
                $scope.testALL = null;
                $state.go('main.Inv044');
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            //DATA SAVE INSERT
            var datasend = [];
            var datasend1 = [];
            var gofurther = true;
            $scope.savedata = function () {
                debugger;

                for (var i = 0; i < $scope.testALL.length; i++) {
                    for (var j = 0; j < $scope.testALL[i].req_details.length; j++) {
                        if ($scope.testALL[i].req_details[j].invs058_rd_line_no1 == true) {
                            if ($scope.temp.servicevalue == 'I') {
                                if ($scope.testALL[i].req_details[j].invs058_ordered_quantity == undefined || $scope.testALL[i].req_details[j].invs058_sup_price == undefined) {
                                    gofurther = false;
                                    swal({ title: "Alert", text: "Please enter Ordered Quantity/Supplier Price", width: 300, height: 200 });
                                    gofurther = true;

                                    return;
                                }
                            }
                        }
                    }
                }

                if (gofurther == true) {
                    if ($scope.temp.order_date != undefined && $scope.temp.dep_code != undefined && $scope.temp.servicevalue != undefined &&
                        $scope.temp.pm_code != undefined && $scope.temp.trt_code != undefined) {
                        for (var i = 0; i < $scope.testALL.length; i++) {
                            for (var j = 0; j < $scope.testALL[i].req_details.length; j++) {
                                if ($scope.testALL[i].req_details[j].invs058_rd_line_no1 == true) {
                                    var data = {
                                        'opr': 'D',
                                        'order_d_req_no': $scope.testALL[i].req_details[j].invs058_req_no,
                                        'order_d_rd_line_no': $scope.testALL[i].req_details[j].invs058_rd_line_no,
                                        'order_d_im_inv_no': $scope.testALL[i].req_details[j].invs058_im_inv_no,
                                        'order_d_uom_code': $scope.testALL[i].req_details[j].invs058_uom_code,
                                        'order_d_od_order_qty': $scope.testALL[i].req_details[j].invs058_ordered_quantity,
                                        'order_d_od_supplier_price': $scope.testALL[i].req_details[j].invs058_sup_price,
                                        'order_d_od_discount_pct': $scope.testALL[i].req_details[j].invs058_discount,
                                        'order_d_od_shipped_qty': $scope.testALL[i].req_details[j].order_d_od_shipped_qty,
                                        'order_d_od_received_qty': $scope.testALL[i].req_details[j].order_d_od_received_qty,
                                        'order_d_od_orig_line_no': $scope.testALL[i].req_details[j].order_d_od_orig_line_no,
                                        'order_d_od_status': $scope.testALL[i].req_details[j].order_d_od_status,
                                        'order_d_od_remarks': $scope.testALL[i].req_details[j].order_d_od_remarks
                                    }
                                    datasend.push(data);
                                }
                            }
                        }

                        $scope.data1 = {
                            'opr': 'O',
                            'order_ord_date': $scope.temp.order_date,
                            'order_dep_code': $scope.temp.dep_code,
                            'order_sup_code': $scope.temp.sup_code,
                            'order_cur_code': $scope.temp.excg_curcy_code,
                            'order_dm_code': $scope.temp.dm_code,
                            'order_ord_shipment_date': $scope.temp.expected_date,
                            'order_pm_code': $scope.temp.pm_code,
                            'order_trt_code': $scope.temp.trt_code,
                            'order_fa_code': $scope.temp.fa_code,
                            'order_lc_no': $scope.temp.lc_no,
                            'order_ord_supl_quot_ref': $scope.temp.supplier_qty_ref,
                            'invs058_req_type': $scope.temp.servicevalue,
                            'randomnumber': Math.floor((Math.random() * 100) + 1),
                        }

                        $http.post(ENV.apiUrl + "api/CreateOrder/SaveOrderDetails?data1=" + JSON.stringify($scope.data1), datasend).then(function (msg) {
                            $scope.msg1 = msg.data;
                            swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 });
                            if ($scope.msg1 != null && $scope.msg1 != '') {
                                swal({ title: "Alert", text: "Order Number(s) " + $scope.msg1 + "  inserted successfully", width: 300, height: 200 });
                                $scope.Cancel1();
                                $state.go('main.Inv044');
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                            }
                            $scope.grid_Display();
                        });
                        datasend = [];
                        datasend1 = [];
                        $scope.table = true;
                        $scope.display = false;
                        //}
                    }
                }

                else {
                    swal({ title: "Alert", text: "Please Select Order Date,Payment Mode and Trade Term", width: 300, height: 200 });
                }
            }
            

            var dom;
            $scope.flag = true;
            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                         "<tr>   <td class='semi-bold'>" + "" + "</td>  <td class='semi-bold'>" + "Request No." + "</td> <td class='semi-bold'>" + "Request Line No." + " </td><td class='semi-bold'>" + "Item Desc" + "</td>" +
                        "<td class='semi-bold'>" + "UOM" + "</td> <td class='semi-bold'>" + "Quantity" + " </td>" + "<td class='semi-bold'>" + "Ordered Quantity" + " </td>" + "<td class='semi-bold'>" + "Supplier Price" + " </td>" + "<td class='semi-bold'>" + "Discount" + " </td>" + "<td class='semi-bold'>" + "Remark" + " </td>" + "</tr>" +

                          "<tr> <td>" + '<input type="checkbox" value="1" ng-click="SingleCheckSave()">' +
                         "<td>" + (info.invs058_req_no) + "</td> <td>" + (info.invs058_rd_line_no) + " </td><td>" + (info.rd_item_desc) + "</td>" +
                         "<td>" + (info.uom_code) + "</td> <td>" + (info.rd_quantity) + "</td>" + "<td>" + '<input style="width: 90px; height: 25px" aria-controls="example" ng-model="info.pays_appl_form_field_value2" class="form-control input-sm">' + "<td>" + '<input style="width: 90px; height: 25px" aria-controls="example" ng-model="info.pays_appl_form_field_value2" class="form-control input-sm">' + "<td>" + '<input style="width: 90px; height: 25px" aria-controls="example" ng-model="info.pays_appl_form_field_value2" class="form-control input-sm">' + "<td>" + (info.rd_remarks) + " </td>" + "</tr>" +

                        " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.req_no.length; i++) {
                        $scope.req_no[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }

            };

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

           $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }])

})();
