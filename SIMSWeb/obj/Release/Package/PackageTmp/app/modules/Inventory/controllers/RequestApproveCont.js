﻿(function () {
    'use strict';
    var obj1, temp, opr, CurrentDate, comp;
    var main, deletefin = [], data = [], data1 = [], ItemDetails = [];
    var finanacecode = [];
    var check;
    var reqno;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('RequestApproveCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {
            $scope.table = true;

            $http.get(ENV.apiUrl + "api/RequestDetail/getRequestTypeNew").then(function (reqtypedetailsNew) {
                $scope.req_type_detailsNew = reqtypedetailsNew.data;
                $scope.temp['request_mode_code'] = $scope.req_type_detailsNew[0].request_mode_code;

            });

            $scope.hide_save = false;
            $scope.hide_saves = false;
            $scope.hide_table = false;

            $scope.IP = {};
            $scope.sel = {};

            $http.get(ENV.apiUrl + "api/RequestDetail/getDeparments").then(function (deptdata) {
                $scope.dept_data = deptdata.data;
                console.log($scope.dept_data);
            });

            $scope.SelectIetm = function () {
                debugger;
                $scope.IP.username = $rootScope.globals.currentUser.username;
                $http.get(ENV.apiUrl + "api/RequestDetail/getRequestDetailsforapprove?data=" + JSON.stringify($scope.IP)).then(function (res) {
                    $scope.rows = res.data.table;
                    $scope.Allrows = res.data.table1;
                    if ($scope.Allrows.length <= 0) {
                        swal({ title: "Alert", text: "Records not found", showCloseButton: true, width: 380, });
                        $scope.hide_save = true;
                        $scope.hide_saves = false;
                        $scope.hide_table = false;
                    }
                    else {
                        for (var r in $scope.rows) {
                            $scope.rows[r]['sims_icon'] = "fa fa-minus-circle";
                            $scope.rows[r]['isexpanded'] = "grid";
                            $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].req_no);
                            $scope.hide_save = false;
                            $scope.hide_saves = true;
                            $scope.hide_table = true;
                        }
                    }

                    console.clear();
                    console.log(res.data.table);
                    console.log(res.data.table1);
                });
            }

            function getSubitems(dno) {
                var arr = [];
                for (var i = 0; i < $scope.Allrows.length; i++) {
                    if ($scope.Allrows[i].req_no == dno) {
                        arr.push($scope.Allrows[i]);
                    }
                }
                return arr;
            }

            $scope.expand = function (j, $event) {
                if (j.isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.isexpanded = "grid";
                    console.log(j);
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.isexpanded = "none";
                }
            }

            $scope.reset = function () {
                $scope.IP = {};
                $scope.rows = {};
                $scope.Allrows = {};
            }

            $scope.checkRange = function (s) {
                var rq = parseInt(s.final_qnt);
                if (rq == NaN)
                    rq = 0;
                s.final_qnt = rq;
            }

            $scope.ordclick = function (gh) {
                var l = gh.subItems.length;
                for (var i = 0; i < l; i++) {
                    gh.subItems[i]['isChecked'] = gh.isChecked;
                }
            }

            $scope.ordlineclick = function (r, m) {
                var l = r.subItems.length;
                var f = false;
                if (m) {
                    f = true;
                    for (var i = 0; i < l; i++) {
                        f = f && r.subItems[i]['isChecked'];
                    }
                }
                r.isChecked = f;
            }

            $scope.save = function (s) {

                $scope.hide_save = true;
                $scope.hide_saves = true;

                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {

                    for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                        if ($scope.rows[i].subItems[j].isChecked) {
                            var ob = {};
                            ob.rd_quantity = $scope.rows[i].subItems[j].rd_quantity;
                            ob.final_qnt = $scope.rows[i].subItems[j].final_qnt;
                            ob.im_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.rd_line_no = $scope.rows[i].subItems[j].rd_line_no;
                            ob.req_no = $scope.rows[i].subItems[j].req_no;
                            ob.req_type = $scope.rows[i].subItems[j].req_type;
                            ob.sims_workflow_no = $scope.rows[i].subItems[j].sims_workflow_no;
                            ob.user_code = $rootScope.globals.currentUser.username;
                            ob.rd_remarks = $scope.rows[i].subItems[j].rd_remarks;
                            ob.req_remarks = $scope.IP.rd_remarks;
                            ob.val = r;
                            ar.push(ob);
                        }
                    }
                }
                if (ar.length == 0) {
                    swal({ title: "Alert", text: "Please Select Atleast One Request", showCloseButton: true, width: 380, });
                    $scope.hide_save = false;
                    $scope.hide_saves = true;

                }
                else {
                    $http.post(ENV.apiUrl + "api/RequestDetail/UpdateRequestDetailsforapprove", ar).then(function (res) {
                        console.log(res);

                        var arr = [];
                        var ob = {};
                        //for (var i = 0; i < $scope.rows.length; i++) {
                        //    if ($scope.rows[i].isChecked) {
                        //        ob.req_no = $scope.rows[i].req_no;
                        //        arr.push(ob.req_no);
                        //    }
                        //}

                        for (var i = 0; i < $scope.rows.length; i++) {
                            for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                                if ($scope.rows[i].subItems[j].isChecked) {
                                    ob.req_no = $scope.rows[i].req_no;
                                    arr.push(ob.req_no);
                                }
                            }
                        }

                        debugger;
                        //swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });
                        swal({ title: "Alert", text: 'Purchase Requisition Number:- ' + arr + "," + "" + ' Approved', showCloseButton: true, width: 380, });

                        $scope.IP = {
                            rd_remarks: '',
                        }

                        if ($scope.IP.req_no == undefined && $scope.IP.req_range_no == undefined && arr.length == 0) {
                            swal({ title: "Alert", text: 'Please Enter Request No From and Request No To OR Select Atleast One Request', showCloseButton: true, width: 380, });
                        }

                        else {
                            if ($scope.IP.req_no == undefined) {
                                $scope.IP.req_no = 0
                            }
                            if ($scope.IP.req_range_no == undefined) {
                                $scope.IP.req_range_no = 0
                            }

                            debugger;
                            $scope.t = 'Invs.INVR16';
                            if (arr.length == 0) {
                                var data = {
                                    location: $scope.t,
                                    parameter: {
                                        req_from: $scope.IP.req_no,
                                        req_to: $scope.IP.req_range_no,
                                        req_list: 0,
                                    },
                                    state: 'main.Inv175'
                                }
                            }
                            else {
                                var data = {
                                    location: $scope.t,
                                    parameter: {
                                        req_from: $scope.IP.req_no,
                                        req_to: $scope.IP.req_range_no,
                                        req_list: arr + "",
                                    },
                                    state: 'main.Inv175'
                                }
                            }
                            window.localStorage["ReportDetails"] = JSON.stringify(data)
                            $state.go('main.ReportCardParameter')
                        }


                        //$scope.SelectIetm();

                    });
                }
            }

            $scope.Reset = function () {
                $scope.IP = {
                    rd_from_required: '',
                    rd_up_required: '',
                    req_no: '',
                    req_range_no: '',
                    request_mode_code: '',
                    rd_item_desc: '',
                }
                $scope.hide_save = false;
                $scope.hide_saves = false;
                $scope.hide_table = false;
            }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            })

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            //$scope.Print = function () {
            //    var t = null;
            //    $http.get(ENV.apiUrl + "api/RequestDetail/getApproveReport").then(function (approvereno) {
            //        debugger;
            //        var arr = [];
            //        var ob = {};
            //        for (var i = 0; i < $scope.rows.length; i++) {
            //            if ($scope.rows[i].isChecked) {
            //                ob.req_no = $scope.rows[i].req_no;
            //                arr.push(ob.req_no);
            //            }
            //            //for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
            //            //    if ($scope.rows[i].subItems[j].isChecked) {
            //            //        ob.req_no = $scope.rows[i].subItems[j].req_no;
            //            //        arr.push(ob.req_no);
            //            //    }
            //            //}
            //        }

            //        if ($scope.IP.req_no == undefined && $scope.IP.req_range_no == undefined && arr.length == 0) {
            //            swal({ title: "Alert", text: 'Please Enter Request No From and Request No To OR Select Atleast One Request', showCloseButton: true, width: 380, });
            //        }

            //        else {
            //            if ($scope.IP.req_no == undefined) {
            //                $scope.IP.req_no = 0;
            //            }
            //            if ($scope.IP.req_range_no == undefined) {
            //                $scope.IP.req_range_no = 0;
            //            }

            //            debugger;
            //            $scope.t = approvereno.data;
            //            if (arr.length == 0) {
            //                var data = {
            //                    location: $scope.t,
            //                    parameter: {
            //                        req_from: $scope.IP.req_no,
            //                        req_to: $scope.IP.req_range_no,
            //                        req_list: 0,
            //                        //req_list_: arr.join,
            //                    },
            //                    state: 'main.Inv175'
            //                }
            //            }
            //            else {
            //                var data = {
            //                    location: $scope.t,
            //                    parameter: {
            //                        req_from: $scope.IP.req_no,
            //                        req_to: $scope.IP.req_range_no,
            //                        req_list: arr + "",
            //                        //req_list_: arr.join,
            //                    },
            //                    state: 'main.Inv175'
            //                }
            //            }
            //            window.localStorage["ReportDetails"] = JSON.stringify(data)
            //            $state.go('main.ReportCardParameter')
            //        }
            //    });
            //}

            //Invs.INVR16
            //req_from
            //req_to
            //parameters

            $scope.Reject = function () {
                debugger
                $scope.hide_save = true;
                $scope.hide_saves = true;

                var ar = [];
                //ar = $scope.rows.length;
                var r = Math.random() * 10;



                for (var i = 0; i < $scope.rows.length; i++) {

                    for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                        if ($scope.rows[i].subItems[j].isChecked) {
                            var ob = {};
                            ob.rd_quantity = $scope.rows[i].subItems[j].rd_quantity;
                            ob.final_qnt = $scope.rows[i].subItems[j].final_qnt;
                            ob.im_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.rd_line_no = $scope.rows[i].subItems[j].rd_line_no;
                            ob.req_no = $scope.rows[i].subItems[j].req_no;
                            ob.req_type = $scope.rows[i].subItems[j].req_type;
                            ob.rd_remarks = $scope.rows[i].subItems[j].rd_remarks;
                            ob.req_remarks = $scope.IP.rd_remarks;
                            ob.sims_workflow_no = $scope.rows[i].subItems[j].sims_workflow_no;
                            ob.user_code = $rootScope.globals.currentUser.username;
                            ob.val = r;
                            ar.push(ob);
                        }
                    }
                }

                if (ar.length == 0) {
                    swal({ title: "Alert", text: "Please Select Atleast  One Request", showCloseButton: true, width: 380, });
                    $scope.hide_save = false;
                    $scope.hide_saves = true;
                }
                else {
                    $http.post(ENV.apiUrl + "api/RequestDetail/UpdateRequestDetailsforrejection", ar).then(function (res) {
                        console.log(res);

                        swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });

                        $scope.IP = {
                            rd_remarks: '',
                        }

                        //$scope.SelectIetm();
                    });
                }
            }


        }])

})();
