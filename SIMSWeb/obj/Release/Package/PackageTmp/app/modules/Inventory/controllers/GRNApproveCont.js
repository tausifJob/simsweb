﻿(function () {
    'use strict';
    var obj1, temp, opr, CurrentDate, comp;
    var main, deletefin = [], data = [], data1 = [], ItemDetails = [];
    var finanacecode = [];
    var check;
    var reqno;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('GRNApproveCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {
            $scope.table = true;
            $scope.rptparm = '';

            $scope.hide_save = false;
            $scope.hide_saves = false;
            $scope.hide_table = false;

            $scope.IP = {};
            $scope.sel = {};
            $http.get(ENV.apiUrl + "api/RequestDetail/getDeparments").then(function (deptdata) {
                $scope.dept_data = deptdata.data;
                console.log($scope.dept_data);
            });

            $scope.SelectIetm = function () {
                $scope.IP.up_name = $rootScope.globals.currentUser.username;
                $scope.rptparm = '';
                $http.post(ENV.apiUrl + "api/AdjustmentReason/AdjDetailsGetFORApprove", $scope.IP).then(function (res) {
                    $scope.rows = res.data.table;
                    $scope.Allrows = res.data.table1;
                    if ($scope.Allrows && $scope.Allrows.length <= 0) {
                        swal({ title: "Alert", text: "Records not found", showCloseButton: true, width: 380, });
                        $scope.hide_save = true;
                        $scope.hide_saves = false;
                        $scope.hide_table = false;
                    }
                    else {
                        for (var r in $scope.rows) {
                            $scope.rows[r]['sims_icon'] = "fa fa-minus-circle";
                            $scope.rows[r]['isexpanded'] = "grid";
                            $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].ord_no + $scope.rows[r].od_grv_no);
                            $scope.hide_save = false;
                            $scope.hide_saves = true;
                            $scope.hide_table = true;
                        }
                    }
                });
            }

            function getSubitems(dno) {
                var arr = [];
                for (var i = 0; i < $scope.Allrows.length; i++) {
                    if ($scope.Allrows[i].ord_no + $scope.Allrows[i].od_grv_no == dno) {
                        arr.push($scope.Allrows[i]);
                    }
                }
                return arr;
            }

            $scope.expand = function (j, $event) {
                if (j.isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.isexpanded = "grid";
                    console.log(j);
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.isexpanded = "none";
                }
            }

            $scope.reset = function () {
                $scope.IP = {};
                $scope.rows = {};
                $scope.Allrows = {};
            }

            $scope.checkRange = function (s) {
                var rq = parseInt(s.final_qnt);
                if (rq == NaN)
                    rq = 0;
                s.final_qnt = rq;
            }

            $scope.ordclick = function (gh) {
                var l = gh.subItems.length;
                for (var i = 0; i < l; i++) {
                    gh.subItems[i]['isChecked'] = gh.isChecked;
                }
            }

            $scope.ordlineclick = function (r, m) {
                var l = r.subItems.length;
                var f = false;
                if (m) {
                    f = true;
                    for (var i = 0; i < l; i++) {
                        f = f && r.subItems[i]['isChecked'];
                    }
                }
                r.isChecked = f;
            }

            $scope.save = function (s) {
                $scope.hide_save = true;
                $scope.hide_saves = true;
                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {
                    if ($scope.rows[i].isChecked) {
                        var ob = {};
                        ob.ucode = $rootScope.globals.currentUser.username;
                        ob.odgrn = $scope.rows[i].ord_no + $scope.rows[i].od_grv_no;
                        $scope.rptparm = $scope.rptparm + $scope.rows[i].od_grv_no + ',';
                        ob.remark = $scope.IP.remark;
                        ob.st = 'A';
                        ob.val = r;
                        ar.push(ob);
                    }
                }
                if (ar.length == 0) {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                    $scope.hide_save = false;
                    $scope.hide_saves = true;
                }
                else {
                    $http.post(ENV.apiUrl + "api/AdjustmentReason/ApproveAdjDetails", ar).then(function (res) {
                        console.log(res);
                        var arr = [];
                        var ob = {};


                        var data = {
                            location: 'Invs.INVR27',
                            parameter: { search: $scope.rptparm },
                            state: 'main.GRNApproval'
                        }
                        window.localStorage["ReportDetails"] = JSON.stringify(data)
                        $state.go('main.ReportCardParameter')
                        $scope.SelectIetm();
                        $scope.hide_save = false;
                        $scope.hide_saves = false;
                        $scope.IP.remark = '';
                        swal({ title: "Alert", text: 'Updated Successfully', showCloseButton: true, width: 380, });
                    });
                }
            }

            $scope.Reset = function () {

                $scope.IP = {

                }
                $scope.rptparm = '';
                $scope.IP.remark = '';
                $scope.hide_save = false;
                $scope.hide_saves = false;
                $scope.hide_table = false;
            }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            })

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $scope.Reject = function () {
                $scope.hide_save = true;
                $scope.hide_saves = true;
                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {
                    if ($scope.rows[i].isChecked) {
                        var ob = {};
                        ob.ucode = $rootScope.globals.currentUser.username;
                        ob.odgrn = $scope.rows[i].ord_no + $scope.rows[i].od_grv_no;
                        ob.remark = $scope.IP.remark;
                        ob.st = 'X';
                        ob.val = r;
                        ar.push(ob);
                    }
                }
                if (ar.length == 0) {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                    $scope.hide_save = false;

                    $scope.hide_saves = true;
                }
                else {
                    $http.post(ENV.apiUrl + "api/AdjustmentReason/ApproveAdjDetails", ar).then(function (res) {
                        console.log(res);
                        var arr = [];
                        var ob = {};
                        $scope.SelectIetm();
                        $scope.IP.remark = '';
                        $scope.hide_save = false;
                        $scope.hide_saves = false;
                        swal({ title: "Alert", text: 'Updated Successfully', showCloseButton: true, width: 380, });
                    });
                }
            }
        }])
})();
