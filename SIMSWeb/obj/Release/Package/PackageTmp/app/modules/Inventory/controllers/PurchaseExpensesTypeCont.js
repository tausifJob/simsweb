﻿(function () {
    'use strict';
    var opr = '';
    var purchasecode = [];
    var main;
    var data1 = [];

    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PurchaseExpensesTypeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '5';
            $scope.table1 = true;
            $scope.operation = false;
            $scope.editmode = false;

            $http.get(ENV.apiUrl + "api/PurchaseExpenseType/getPurchaseExpenseTypeDetails").then(function (getPurchaseExpenseTypeDetails_Data) {
                $scope.PurchaseExpenseTypeData = getPurchaseExpenseTypeDetails_Data.data;
                $scope.totalItems = $scope.PurchaseExpenseTypeData.length;
                $scope.todos = $scope.PurchaseExpenseTypeData;
                $scope.makeTodos();

            });

            $http.get(ENV.apiUrl + "api/common/getglmaAccountNames").then(function (getglmaAccountNames_Data) {
                $scope.AccountNames = getglmaAccountNames_Data.data;
            });
         

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = "";
            }

            $scope.New = function () {
                $scope.edt = "";
                $scope.check = true;
                $scope.editmode = false;
                $scope.newmode = true;
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.up = function (str) {
                opr = 'U';
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.readonly = true;
               
                $scope.edt = {
                        pet_code: str.pet_code,
                        pet_desc: str.pet_desc,
                        pet_expense_acno: str.pet_expense_acno,
                        pet_expense_acname: str.pet_expense_acname,
                        pet_flag: str.pet_flag,
                }
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.edt = {
                    pet_code: "",
                    pet_desc: "",
                    pet_expense_acno: "",
                    pet_flag:""
                }
            }

            $scope.Save = function (myForm) {
                
                if (myForm) {
                    data1 = [];
                    data = [];
                    var data = $scope.edt;
                    data.opr = 'I';
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/PurchaseExpenseType/PurchaseExpenseTypeCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                            }

                            $http.get(ENV.apiUrl + "api/PurchaseExpenseType/getPurchaseExpenseTypeDetails").then(function (getPurchaseExpenseTypeDetails_Data) {
                                $scope.PurchaseExpenseTypeData = getPurchaseExpenseTypeDetails_Data.data;
                                $scope.totalItems = $scope.PurchaseExpenseTypeData.length;
                                $scope.todos = $scope.PurchaseExpenseTypeData;
                                $scope.makeTodos();
                            });
                        });

                        $scope.table1 = true;
                        $scope.operation = false;
                        
                    }
            }

            $scope.Update = function (myForm) {
                
                if (myForm) {
                    data1 = [];
                    data = [];
                    var data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/PurchaseExpenseType/PurchaseExpenseTypeCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                        $http.get(ENV.apiUrl + "api/PurchaseExpenseType/getPurchaseExpenseTypeDetails").then(function (getPurchaseExpenseTypeDetails_Data) {
                            $scope.PurchaseExpenseTypeData = getPurchaseExpenseTypeDetails_Data.data;
                            $scope.totalItems = $scope.PurchaseExpenseTypeData.length;
                            $scope.todos = $scope.PurchaseExpenseTypeData;
                            $scope.makeTodos();
                        });
                        
                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                 
                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

           
            $scope.deleterecord = function () {
                
                var dfdf = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletepurchasecode = ({
                            'pet_code': $scope.filteredTodos[i].pet_code,
                            opr: 'D'
                        });
                        dfdf.push(deletepurchasecode);
                    }
                }
               
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger
                            $http.post(ENV.apiUrl + "api/PurchaseExpenseType/PurchaseExpenseTypeCUD?data=", dfdf).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/PurchaseExpenseType/getPurchaseExpenseTypeDetails").then(function (getPurchaseExpenseTypeDetails_Data) {
                                                $scope.PurchaseExpenseTypeData = getPurchaseExpenseTypeDetails_Data.data;
                                                $scope.totalItems = $scope.PurchaseExpenseTypeData.length;
                                                $scope.todos = $scope.PurchaseExpenseTypeData;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/PurchaseExpenseType/getPurchaseExpenseTypeDetails").then(function (getPurchaseExpenseTypeDetails_Data) {
                                                $scope.PurchaseExpenseTypeData = getPurchaseExpenseTypeDetails_Data.data;
                                                $scope.totalItems = $scope.PurchaseExpenseTypeData.length;
                                                $scope.todos = $scope.PurchaseExpenseTypeData;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                purchasecode = [];
                $scope.currentPage = true;
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.PurchaseExpenseTypeData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.PurchaseExpenseTypeData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */

                return (
                     item.pet_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.pet_expense_acno.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.pet_expense_acname.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.pet_code == toSearch) ? true : false;


            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


        }])
})();





