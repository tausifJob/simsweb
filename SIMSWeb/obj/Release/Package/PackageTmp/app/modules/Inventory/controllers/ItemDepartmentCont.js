﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ItemDepartmentCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.pagesize1 = "5";
            $scope.pageindex1 = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.readonlytxt = true;
            $scope.temp = [];
            $scope.temp1 = [];
            //$scope.table = true;
            //$scope.table1 = true;
            $scope.updatebtn = true;
            var user = $rootScope.globals.currentUser.username.toLowerCase();

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
                $("#example_wrapper").scrollbar();
                $("#example_wrapper1").scrollbar();
            }, 100);


         
            $scope.countData = [
                     { val: 5, data: 5 },
                     { val: 10, data: 10 },
                     { val: 15, data: 15 },

            ]

            $scope.countData1 = [
                    { val: 5, data: 5 },
                    { val: 10, data: 10 },
                    { val: 15, data: 15 },

            ]
            $scope.onlyNumbers = function (event) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };
            $http.get(ENV.apiUrl + "api/ItemDepartment/getDepartmentNames").then(function (res) {
                $scope.DepartmentName = res.data;
                $scope.temp['dep_code'] = $scope.DepartmentName[0].dep_code
            });

            $http.get(ENV.apiUrl + "api/ItemDepartment/getRoleofUser?username=" + user).then(function (res) {
                $scope.userRole = res.data;
                if ($scope.userRole > 0) {
                    $scope.updatebtn = false;
                    $scope.readonlytxt = false;
                }
            });
            $http.get(ENV.apiUrl + "api/ItemDepartment/getLocationNames").then(function (res) {
                $scope.LocationName = res.data;
                $scope.temp1['loc_code'] = $scope.LocationName[0].loc_code
            });

            $http.get(ENV.apiUrl + "api/ItemDepartment/getCategories").then(function (res) {
                $scope.CategoryName = res.data;
            });
            $scope.getSubCategory=function(pc_code){
                $http.get(ENV.apiUrl + "api/ItemDepartment/getSubCategories?pc_parentcode=" + pc_code).then(function (res) {
                    $scope.SubCategoryName = res.data;
                });
            }
            $scope.size = function (str) {
                if (str == 5 || str == 10 || str == 15) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }
               
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }


            $scope.size1 = function (str) {
                if (str == 5 || str == 10 || str == 15) {
                    $scope.pager1 = true;
                }
                else {
                    $scope.pager1 = false;
                }

                $scope.pagesize1 = str;
                $scope.currentPage1 = 1;
                $scope.numPerPage1 = str;
                $scope.makeTodos1();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.index1 = function (str) {
              
                $scope.pageindex1 = str;
                $scope.currentPage1 = str;
                $scope.makeTodos1();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.filteredTodos1 = [], $scope.currentPage1 = 1, $scope.numPerPage1 = 5, $scope.maxSize1 = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.makeTodos1 = function () {
          
                var rem = parseInt($scope.totalItems1 % $scope.numPerPage1);
                if (rem == '0') {
                    $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1);
                }
                else {
                    $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1) + 1;
                }

                var begin = (($scope.currentPage1 - 1) * $scope.numPerPage1);
                var end = parseInt(begin) + parseInt($scope.numPerPage1);

              
                $scope.filteredTodos1 = $scope.todos1.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };
            $scope.searched1 = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil1(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.ItemDepartmentData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.ItemDepartmentData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.search1 = function () {
                $scope.todos1 = $scope.searched1($scope.ItemLocationData, $scope.searchText1);
                $scope.totalItems1 = $scope.todos1.length;
                $scope.currentPage1 = '1';
                if ($scope.searchText1 == '') {
                    $scope.todos1 = $scope.ItemLocationData;
                }
                $scope.makeTodos1();
                
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.im_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.dep_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.id_do_qty == toSearch ||
                        item.id_alloc_qty == toSearch ||
                        item.id_yob_qty == toSearch ||
                        item.id_cur_qty == toSearch) ? true : false;
            }

            function searchUtil1(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.im_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.loc_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.id_do_qty == toSearch ||
                        item.id_alloc_qty == toSearch ||
                        item.id_yob_qty == toSearch ||
                        item.id_cur_qty == toSearch) ? true : false;
            }

            $scope.getItemDepartmentData = function (dept, category, subcategory, itemno) {
                if (dept == undefined)
                    dept = '';
                if (category == undefined)
                    category = '';
                if (subcategory == undefined)
                    subcategory = '';
                if (itemno == undefined)
                    itemno = '';
                $http.get(ENV.apiUrl + "api/ItemDepartment/getItemDepartmentsData?invno=" + itemno + "&dept=" + dept + "&category=" + category + "&subcategory=" + subcategory).then(function (res) {
                  
                    $scope.ItemDepartmentData = res.data;
                    if ($scope.ItemDepartmentData.length>0) {
                        $scope.table = true;
                        $scope.pager = true;
                    }
                    else {
                        $scope.table = false;
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                    }
                    if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.ItemDepartmentData.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.ItemDepartmentData.length, data: 'All' })
                    }
                    $scope.totalItems = $scope.ItemDepartmentData.length;
                    $scope.todos = $scope.ItemDepartmentData;
                    $scope.makeTodos();
                });

            }

            $scope.getItemLocationData = function (location, category, subcategory, itemno) {
                if (location == undefined)
                    location = '';
                if (category == undefined)
                    category = '';
                if (subcategory == undefined)
                    subcategory = '';
                if (itemno == undefined)
                    itemno = '';
                $http.get(ENV.apiUrl + "api/ItemDepartment/getItemLocationData?invno=" + itemno + "&location=" + location + "&category=" + category + "&subcategory=" + subcategory).then(function (res) {
               
                    $scope.ItemLocationData = res.data;
                    if ($scope.ItemLocationData.length > 0) {
                        $scope.table1 = true;
                        $scope.pager1 = true;
                    }
                    else {
                        $scope.table1 = false;
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                    }
                    if ($scope.countData1.length > 3) {
                        $scope.countData1.splice(3, 1);
                        $scope.countData1.push({ val: $scope.ItemLocationData.length, data: 'All' })
                    }
                    else {
                        $scope.countData1.push({ val: $scope.ItemLocationData.length, data: 'All' })
                    }

                    $scope.totalItems1 = $scope.ItemLocationData.length;
                    $scope.todos1 = $scope.ItemLocationData;
                    $scope.makeTodos1();
                });
              
            }

            $scope.UpdateItemLocation = function (str) {
               
                var updateData = [];
                var data = {
                    opr: 'J',
                    im_inv_no: str.im_inv_no,
                    dep_code: str.loc_code,
                    id_cur_qty: str.id_cur_qty,
                    id_do_qty: str.id_do_qty,
                    id_alloc_qty: str.id_alloc_qty,
                    id_yob_qty: str.id_yob_qty,
                    id_foc_qty: str.id_foc_qty,
                }
                updateData.push(data);
                $http.post(ENV.apiUrl + "api/ItemDepartment/CUDItemDepartments", updateData).then(function (msg) {
                    debugger;
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                       
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Updated Successfully", width: 300, height: 200 });
                    }
                    if ($scope.temp1 == undefined)
                        $scope.temp1 = ''
                    $scope.getItemLocationData($scope.temp1.loc_code, $scope.temp1.category_code, $scope.temp1.subcategory_code, $scope.temp1.itemno);

                });
            }
            $scope.reset = function () {
                $scope.temp1 = [];
                $scope.temp = [];
            }

            $scope.UpdateItemDepartment = function (str) {
                var updateData = [];
                var data = {
                    opr: 'U',
                    im_inv_no: str.im_inv_no,
                    dep_code: str.dep_code,
                    id_cur_qty: str.id_cur_qty,
                    id_do_qty: str.id_do_qty,
                    id_alloc_qty: str.id_alloc_qty,
                    id_yob_qty: str.id_yob_qty,
                    id_foc_qty: str.id_foc_qty,
                }
                updateData.push(data);
                $http.post(ENV.apiUrl + "api/ItemDepartment/CUDItemDepartments", updateData).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Updated Successfully", width: 300, height: 200 });
                    }
                    if ($scope.temp == undefined)
                        $scope.temp=''
                    $scope.getItemDepartmentData($scope.temp.dep_code, $scope.temp.category_code, $scope.temp.subcategory_code,$scope.temp.itemno);
                });
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                formate: "yyyy-mm-dd"
            });



        }])

})();
