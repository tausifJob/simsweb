﻿(function () {
    'use strict';
    var opr = '';
    var deleteintcode = [];
    var main;
    var data1 = [];
    var inttype = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('InterestTypeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '5';
            $scope.table1 = true;
            $scope.operation = false;
            $scope.editmode = false;

            $http.get(ENV.apiUrl + "api/InterestType/getInterestTypeDetails").then(function (getInterestTypeDetails_Data) {
                $scope.InterestTypeData = getInterestTypeDetails_Data.data;
                $scope.totalItems = $scope.InterestTypeData.length;
                $scope.todos = $scope.InterestTypeData;
                $scope.makeTodos();

            });

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.New = function () {
                $scope.edt = "";
                $scope.check = true;
                $scope.editmode = false;
                $scope.newmode = true;
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.up = function (str) {
                opr = 'U';
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.readonly = true;

                $scope.edt = {
                    int_code: str.int_code,
                    int_desc: str.int_desc
                }
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.edt = {
                    int_code: "",
                    int_desc: "",
                }
            }

            $scope.Save = function (myForm) {

                if (myForm) {
                    data1 = [];
                    data = [];
                    var data = $scope.edt;
                    data.opr = 'I';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/InterestType/InterestTypeCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                        }
                        $http.get(ENV.apiUrl + "api/InterestType/getInterestTypeDetails").then(function (getInterestTypeDetails_Data) {
                            $scope.InterestTypeData = getInterestTypeDetails_Data.data;
                            $scope.totalItems = $scope.InterestTypeData.length;
                            $scope.todos = $scope.InterestTypeData;
                            $scope.makeTodos();
                        });
                    });

                    $scope.table1 = true;
                    $scope.operation = false;

                }
            }

            $scope.Update = function (myForm) {
                if (myForm) {
                    data1 = [];
                    data = [];
                    var data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/InterestType/InterestTypeCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                        $http.get(ENV.apiUrl + "api/InterestType/getInterestTypeDetails").then(function (getInterestTypeDetails_Data) {
                            $scope.InterestTypeData = getInterestTypeDetails_Data.data;
                            $scope.totalItems = $scope.InterestTypeData.length;
                            $scope.todos = $scope.InterestTypeData;
                            $scope.makeTodos();
                        });

                    })
                    $scope.operation = false;
                    $scope.table1 = true;

                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }


            //$scope.deleterecord = function () {

            //    var inttype = [];
            //    $scope.flag = false;
            //    for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //        var v = document.getElementById(i);
            //        if (v.checked == true) {
            //            $scope.flag = true;
            //            var deleteintcode = ({
            //                'int_code': $scope.filteredTodos[i].int_code,
            //                opr: 'D'
            //            });
            //            inttype.push(deleteintcode);
            //        }
            //    }

            //    if ($scope.flag) {
            //        swal({
            //            title: '',
            //            text: "Are you sure you want to Delete?",
            //            showCloseButton: true,
            //            showCancelButton: true,
            //            confirmButtonText: 'Yes',
            //            width: 380,
            //            cancelButtonText: 'No',

            //        }).then(function (isConfirm) {
            //            if (isConfirm) {
            //                debugger
            //                $http.post(ENV.apiUrl + "api/InterestType/InterestTypeCUD", inttype).then(function (msg) {
            //                    $scope.msg1 = msg.data;
            //                    if ($scope.msg1 == true) {
            //                        swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
            //                            if (isConfirm) {
            //                                $http.get(ENV.apiUrl + "api/InterestType/getInterestTypeDetails").then(function (getInterestTypeDetails_Data) {
            //                                    $scope.InterestTypeData = getInterestTypeDetails_Data.data;
            //                                    $scope.totalItems = $scope.InterestTypeData.length;
            //                                    $scope.todos = $scope.InterestTypeData;
            //                                    $scope.makeTodos();
            //                                });
            //                            }
            //                        });
            //                    }
            //                    else {
            //                        swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
            //                            if (isConfirm) {
            //                                $http.get(ENV.apiUrl + "api/InterestType/getInterestTypeDetails").then(function (getInterestTypeDetails_Data) {
            //                                    $scope.InterestTypeData = getInterestTypeDetails_Data.data;
            //                                    $scope.totalItems = $scope.InterestTypeData.length;
            //                                    $scope.todos = $scope.InterestTypeData;
            //                                    $scope.makeTodos();
            //                                });
            //                            }
            //                        });
            //                    }

            //                });
            //            }
            //            else {
            //                for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //                    var v = document.getElementById(i);
            //                    if (v.checked == true) {
            //                        v.checked = false;
            //                        $scope.CheckAllChecked();
            //                    }
            //                }
            //            }
            //        });
            //    }
            //    else {
            //        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
            //    }
            //    purchasecode = [];
            //    $scope.currentPage = true;

            //}

            $scope.deleterecord = function () {
                inttype = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deleteintcode = ({
                            'int_code': $scope.filteredTodos[i].int_code,
                            opr: 'D'
                        });
                        inttype.push(deleteintcode);
                    }
                }
                debugger
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/InterestType/InterestTypeCUD", inttype).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/InterestType/getInterestTypeDetails").then(function (getInterestTypeDetails_Data) {
                                                $scope.InterestTypeData = getInterestTypeDetails_Data.data;
                                                $scope.totalItems = $scope.InterestTypeData.length;
                                                $scope.todos = $scope.InterestTypeData;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/InterestType/getInterestTypeDetails").then(function (getInterestTypeDetails_Data) {
                                                $scope.InterestTypeData = getInterestTypeDetails_Data.data;
                                                $scope.totalItems = $scope.InterestTypeData.length;
                                                $scope.todos = $scope.InterestTypeData;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
                main.checked = false;
            }
            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.InterestTypeData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.InterestTypeData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */

                return (
                     item.int_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.int_code == toSearch) ? true : false;


            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


        }])
})();





