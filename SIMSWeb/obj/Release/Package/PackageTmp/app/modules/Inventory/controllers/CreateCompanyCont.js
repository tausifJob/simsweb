﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CreateCompanyCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/createcompany/getAllCompany").then(function (res1) {
                $scope.comp_data = res1.data;
                $scope.totalItems = $scope.comp_data.length;
                $scope.todos = $scope.comp_data;
                $scope.makeTodos();
            });

            $scope.grid_Display = function () {
                $http.get(ENV.apiUrl + "api/createcompany/getAllCompany").then(function (res1) {
                    $scope.comp_data = res1.data;
                    $scope.totalItems = $scope.comp_data.length;
                    $scope.todos = $scope.comp_data;
                    $scope.makeTodos();
                });
            }

            $http.get(ENV.apiUrl + "api/createcompany/getCurrency").then(function (res1) {
                $scope.currency = res1.data;
            });
            $http.get(ENV.apiUrl + "api/createcompany/getCompany").then(function (res1) {
                $scope.comp = res1.data;
            });

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.comp_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.comp_data;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.com_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.com_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.cur_code == toSearch) ? true : false;
            }

            $scope.New = function () {

                $scope.disabled = false;
                $scope.readonly = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = "";
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            var datasend = [];
            $scope.savedata = function (Myform) {
                 if (Myform) {
                debugger;
                var data = $scope.temp;
                data.opr = "I";
                datasend.push(data);
              
                $http.post(ENV.apiUrl + "api/createcompany/CUDCompany", datasend).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Record Inserted Successfully", width: 380, showCloseButton: true });
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Inserted", width: 380, showCloseButton: true });
                    }
                    $scope.grid_Display();
                });
                datasend = [];
                $scope.table = true;
                $scope.display = false;
                }
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.comp_code = "";
                $scope.temp.excg_curcy_code = "";
                $scope.temp.company_name = "";

            }

            $scope.edit = function (str) {
                debugger
                $scope.readonly = true;
                $scope.disabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.temp = {
                    comp_code: str.com_code,
                    comp_name: str.comp_name,
                    excg_curcy_code: str.cur_code,
                    excg_curc_desc: str.excg_curc_desc,
                    com_name: str.com_name
                };

            }

            var dataupdate = [];
            $scope.update = function () {//Myform
                //if (Myform) {
                var Udata = $scope.temp;
                Udata.opr = "U";
                dataupdate.push(Udata);

                $http.post(ENV.apiUrl + "api/createcompany/CUDCompany", dataupdate).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Record Updated Successfully", width: 380, showCloseButton: true });
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Updated", width: 380, showCloseButton: true });
                    }
                    $scope.grid_Display();
                });
                dataupdate = [];
                $scope.table = true;
                $scope.display = false;
                //}
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'comp_code': $scope.filteredTodos[i].com_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/createcompany/CUDCompany?data=", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.grid_Display();
                                        }
                                    });
                                    main = document.getElementById('mainchk');
                                    if (main.checked == true) {
                                        main.checked = false;
                                        $scope.row1 = '';
                                    }
                                    $scope.currentPage = true;
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted ", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.grid_Display();
                                        }
                                    });
                                    main = document.getElementById('mainchk');
                                    if (main.checked == true) {
                                        main.checked = false;
                                        $scope.row1 = '';
                                    }
                                    $scope.currentPage = true;
                                }

                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }

        }])

})();
