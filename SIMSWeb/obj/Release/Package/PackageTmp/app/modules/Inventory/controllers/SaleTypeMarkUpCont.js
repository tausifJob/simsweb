﻿(function () {
    'use strict';
    var opr = '';
    var data1 = [];
    var data = [];
    var main, edt;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SaleTypeMarkUpCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.edt = "";
            $scope.SaleTypeMarkDetail = true;
            $scope.editmode = false;
            $scope.newmode = false;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.cancel = function () {
                $scope.SaleTypeMarkDetail = true;
                $scope.SaleTypeMarkData = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                opr = 'S';
                $scope.editmode = false;
                $scope.newmode = true;
                $scope.checc = true;
                $scope.edt = "";
                $scope.newmode = true;
                $scope.readonly = false;
                $scope.SaleTypeMarkDetail = false;
                $scope.SaleTypeMarkData = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.check = function (clickedid)
            {
                if(clickedid==true)
                {
                        swal({
                            title: 'Alert',
                            text: "Do You Want To Update This Record?",
                            showCloseButton: true,
                            confirmButtonText: 'Yes',
                            showCancelButton: true,
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm)
                            {
                                $scope.edt.updateflag = true;
                            }
                            else
                            {
                                $scope.edt.updateflag = false;
                            }
                        });
                }
            };

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                console.log("begin=" + begin); console.log("end=" + end);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.SaleMarkData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.SaleMarkData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.check_all();
            }

            function searchUtil(item, toSearch) {
                return (
                    item.dep_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sal_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sal_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.stm_sale_acno.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.stm_cost_acno.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.stm_rate.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.dep_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.dep_name == toSearch) ? true : false;

            }

            $http.get(ENV.apiUrl + "api/SaleMark/getSaleTypeDetail").then(function (SaleMark_Data) {
                debugger
                $scope.SaleMarkData = SaleMark_Data.data;
                $scope.totalItems = $scope.SaleMarkData.length;
                $scope.todos = $scope.SaleMarkData;
                $scope.makeTodos();
                console.log($scope.SaleMarkData);
            });

            $http.get(ENV.apiUrl + "api/AdjustmentReason/GetGLAccountNumber").then(function (docstatus1) {
                debugger
                $scope.GlACNO = docstatus1.data;
                console.log($scope.docstatus1);
            });

            $http.get(ENV.apiUrl + "api/SaleMark/getDepartment").then(function (docstatus) {

                $scope.getDepa = docstatus.data;
                console.log($scope.docstatus1);
            });

            $http.get(ENV.apiUrl + "api/SaleMark/getSaleType").then(function (saledata) {
                debugger
                $scope.getSalesType = saledata.data;
                console.log($scope.getSalesType);
            });

            $scope.getempdesc = function (str)
            {
                $http.get(ENV.apiUrl + "api/SaleMark/getSaleType").then(function (res)
                {
                    $scope.alldata = res.data;
                    for (var i = 0; i < $scope.getSalesType.length; i++)
                    {
                        if ($scope.getSalesType[i].sal_type == str)
                        {
                             $scope.edt.stm_indicator= $scope.getSalesType[i].sal_indicator,
                             $scope.edt.stm_sale_acno= $scope.getSalesType[i].sale_acno,
                             $scope.edt.stm_cost_acno= $scope.getSalesType[i].cost_acno
                        }
                    }
                })

            };

            $scope.Save = function (myForm) {
                debugger
                if (myForm) {
                    data = [];
                    data1 = [];
                    data = $scope.edt;
                    data.opr = 'I';
                    $scope.exist = false;
                    if ($scope.updateflag == "True") {

                        $scope.edt.updateflag = data.updateflag;

                    }
                    else {

                        for (var i = 0; i < $scope.SaleMarkData.length; i++) {
                            if ($scope.SaleMarkData[i].dep_code == data.dep_code &&
                                $scope.SaleMarkData[i].sal_type == data.sal_type &&
                                $scope.SaleMarkData[i].stm_sale_acno == data.stm_sale_acno &&
                                 $scope.SaleMarkData[i].stm_cost_acno == data.stm_cost_acno) {
                                $scope.exist = true;
                            }
                        }
                        if ($scope.exist) {
                            swal({ title: "Alert", text: "Record Already exists", width: 300, height: 200 });
                        }

                        else {
                            data1.push(data);
                            $http.post(ENV.apiUrl + "api/SaleMark/SalesMarkCUD?simsobj=", data1).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                                }
                                $http.get(ENV.apiUrl + "api/SaleMark/getSaleTypeDetail").then(function (SaleMark_Data) {
                                    $scope.SaleMarkData = SaleMark_Data.data;
                                    $scope.totalItems = $scope.SaleMarkData.length;
                                    $scope.todos = $scope.SaleMarkData;
                                    $scope.makeTodos();
                                });
                            });
                            $scope.SaleTypeMarkDetail = true;
                            $scope.SaleTypeMarkData = false;
                        }
                        data1 = [];
                    }
                }
            }

            $scope.up = function (str) {
                debugger
                opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.checc = true;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.SaleTypeMarkDetail = false;
                $scope.SaleTypeMarkData = true;
                $scope.readonly = true;
                // $scope.edt = str;
                $scope.edt = {
                    depcodewithname: str.depcodewithname,
                    saletypewthname: str.saletypewthname,
                    stm_indicator: str.stm_indicator,
                    stm_rate: str.stm_rate,
                    stm_sale_acno: str.stm_sale_acno,
                    stm_cost_acno: str.stm_cost_acno,
                    dep_code: str.dep_code,
                    sal_type: str.sal_type,
                    old_sal_type:str.sal_type,
                };


            }

            $scope.Update = function (myForm) {
                debugger
                if (myForm) {
                    data = $scope.edt;
                    data1.push(data);
                    data.opr = 'U';
                    if ($scope.updateflag == "True") {

                        $scope.edt.updateflag = data.updateflag;

                    }
                    else {
                        $http.post(ENV.apiUrl + "api/SaleMark/SalesMarkCUD?simsobj=", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                            }
                            $http.get(ENV.apiUrl + "api/SaleMark/getSaleTypeDetail").then(function (SaleMark_Data) {
                                $scope.SaleMarkData = SaleMark_Data.data;
                                $scope.totalItems = $scope.SaleMarkData.length;
                                $scope.todos = $scope.SaleMarkData;
                                $scope.makeTodos();
                            });
                        })
                        $scope.SaleTypeMarkDetail = true;
                        $scope.SaleTypeMarkData = false;
                        data1 = [];

                    }
                }
            }

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.check_once = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }

            $scope.deleterecord = function () {
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'dep_code': $scope.filteredTodos[i].dep_code,
                            'sal_type': $scope.filteredTodos[i].sal_type,
                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        confirmButtonText: 'Yes',
                        showCancelButton: true,
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/SaleMark/SalesMarkCUD", deleteleave).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/SaleMark/getSaleTypeDetail").then(function (SaleMark_Data) {
                                                $scope.SaleMarkData = SaleMark_Data.data;
                                                $scope.totalItems = $scope.SaleMarkData.length;
                                                $scope.todos = $scope.SaleMarkData;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                        }

                                        $scope.currentPage = true;
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/SaleMark/getSaleTypeDetail").then(function (SaleMark_Data) {
                                                $scope.SaleMarkData = SaleMark_Data.data;
                                                $scope.totalItems = $scope.SaleMarkData.length;
                                                $scope.todos = $scope.SaleMarkData;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                        }
                                    });
                                }
                            });
                        }
                        else {
                            main = document.getElementById('all_chk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }

        }]);

})();



