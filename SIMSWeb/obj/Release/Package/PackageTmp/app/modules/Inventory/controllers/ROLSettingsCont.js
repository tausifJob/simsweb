﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var data = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ROLSettingsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.search_btn = true;
            $scope.clr_btn = true;
            $scope.Update_btn = false;
            $scope.mainPage = true;
            $scope.display = false;
            $scope.table = true;
            $scope.save_disable = true;;

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/rolsettings/getAllROLSettings").then(function (res1) {
                $scope.rol_Data = res1.data;
                $scope.totalItems = $scope.rol_Data.length;
                $scope.todos = $scope.rol_Data;
                $scope.makeTodos();
            });
            $scope.grid_Display = function () {
                $http.get(ENV.apiUrl + "api/rolsettings/getAllROLSettings").then(function (res1) {
                    $scope.rol_Data = res1.data;
                    $scope.totalItems = $scope.rol_Data.length;
                    $scope.todos = $scope.rol_Data;
                    $scope.makeTodos();
                });
            }


            $scope.rol_Settings_data_by_filter = function (sg_name, dep_code, pc_code, uom_code) {
                $http.get(ENV.apiUrl + "api/rolsettings/getROLSettingsbyFilter?sg_name=" + sg_name + "&dep_code=" + dep_code + "&pc_code=" + pc_code + "&uom_code=" + uom_code).then(function (res1) {
                    $scope.rol_Data = res1.data;
                    if ($scope.rol_Data.length > 0)
                    { }
                    else { swal({ text: "Records not found", width: 300, showCloseButton: true }); }
                    $scope.totalItems = $scope.rol_Data.length;
                    $scope.todos = $scope.rol_Data;
                    $scope.makeTodos();
                });
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.grid_Display();
                $scope.filteredTodos = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.com_code = "";
                $scope.temp.dep_code = "";
                $scope.temp.pc_code = "";
                $scope.temp.uom_code = "";
                $scope.temp.sg_name = "";

            }
            $scope.clear = function () {
                $scope.temp = "";
                $scope.Myform.$setPristine = "";
                $scope.Myform.$setUntouched = "";
                $scope.grid_Display();
            }

            $http.get(ENV.apiUrl + "api/rolsettings/getCompanyName").then(function (compname) {
                $scope.comp_name = compname.data;
            });
            $http.get(ENV.apiUrl + "api/rolsettings/getDeparments").then(function (deptname) {
                $scope.dept_name = deptname.data;

                $scope.temp = {
                    'com_code': $scope.comp_name[0].com_code,
                    'dep_code': $scope.dept_name[0].dep_code
                }

            });
            $http.get(ENV.apiUrl + "api/rolsettings/getProductName").then(function (prodname) {
                $scope.prod_name = prodname.data;
                console.log($scope.prod_name);
            });
            $http.get(ENV.apiUrl + "api/rolsettings/getUOM").then(function (uomname) {
                $scope.uom_nam = uomname.data;
                console.log($scope.uom_nam);
            });
            $http.get(ENV.apiUrl + "api/rolsettings/getSupplierName").then(function (suppname) {
                $scope.supp_name = suppname.data;
                console.log($scope.comp_name);
            });


            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.rol_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.rol_Data;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.uom_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.im_item_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.im_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.im_inv_no == toSearch) ? true : false;
            }



            var datasend = [];

            $scope.savedata = function (j) {//Myform
                // if (Myform) { 

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].ischange == true) {
                        data = {
                            'im_average_month_demand': $scope.filteredTodos[i].im_average_month_demand,
                            'im_approximate_lead_time': $scope.filteredTodos[i].im_approximate_lead_time,
                            'im_reorder_level': $scope.filteredTodos[i].im_reorder_level,
                            'im_inv_no': $scope.filteredTodos[i].im_inv_no,
                            opr: 'R',
                            opr_upd: 'U',
                        }
                        datasend.push(data);
                    }
                }
                $http.post(ENV.apiUrl + "api/rolsettings/CUDROLSettings", datasend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                    }
                    $scope.grid_Display();
                });
                datasend = [];
                $scope.table = true;
                $scope.display = false;
                //}
            }

           

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $scope.IsChangeFunction = function (info) {
                info.ischange = true;
                $scope.save_disable = false;
            }

        }])

})();
