﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CancelSalesReceiptCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $rootScope.visible_stud = true;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = false;
            $rootScope.chkMulti = false;
            $scope.temp = {};
            $scope.edt = {};
            $scope.print_btn = true;
            $scope.Byreceipt = true;
            var user = $rootScope.globals.currentUser.username;
            $scope.pagesize = '5';
            $scope.temp['doc_total_amount'] = '0.0';
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            if (dd < 10) {
                dd = '0' + dd
            }

            if (mm < 10) {
                mm = '0' + mm
            }

            today = yyyy + '/' + mm + '/' + dd;

            $scope.temp['creation_date'] = today;
            $scope.temp['creation_user'] = user;
            $scope.temp['doc_prov_date'] = today;
            $scope.edt['doc_date'] = today;

            $scope.edt['doc_prov_date'] = yyyy + '/' + (mm - 3) + '/' + dd;
            salesman();
            $scope.Finalize_btn = true;

            $scope.table1 = false;

            $scope.table = false;

            $scope.ByReceiptShow = function () {
                $scope.Byreceipt = true;
            }

            $scope.ByItemShow = function () {
                $scope.Byreceipt = false;
                $scope.filteredTodos = [];
                $scope.table1 = false;
                salesman();
            }

            $http.get(ENV.apiUrl + "api/InterDepartmentTransfer/GetAllDepartmentsidt").then(function (GetAllDepartments) {
                $scope.GetAllDepartments2 = GetAllDepartments.data;
                $scope.temp['dep_code'] = $scope.GetAllDepartments2[0].dep_code;

                console.log($scope.GetAllDepartments2);
            });

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2015&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;

            });

            $http.get(ENV.apiUrl + "api/InvLPO/Get_Adj_locations").then(function (Get_Adj_locations) {

                $scope.Get_Adj_locations = Get_Adj_locations.data;

                $scope.temp['loc_code'] = $scope.Get_Adj_locations[0].loc_code;
            });

            $scope.Show = function () {

                $scope.GetMapped_Employee1 = [];
                $scope.GetMapped_Employee = [];

                if ($scope.temp1.co_company_code != "" && $scope.temp1.co_company_code != undefined) {
                    $scope.busy = true;
                    $scope.table = false;
                    $http.get(ENV.apiUrl + "api/EmployeeLeave/GetEmployeeLeaveAssign?simsobj=" + JSON.stringify($scope.temp1)).then(function (GetMappedEmployee) {
                        $scope.GetMapped_Employee1 = GetMappedEmployee.data;

                        if (GetMappedEmployee.data.length > 0) {
                            $scope.GetMapped_Employee = $scope.GetMapped_Employee1;
                            $scope.table = true;
                            $scope.busy = false;

                            setTimeout(function () {
                                $("#mycell td:first").focus();

                            }, 300);

                        }

                        else {
                            $scope.table = false;
                            $scope.busy = false;
                            swal({
                                text: 'Data Not Available',
                                width: 300,
                                height: 300
                            });
                        }
                    });
                }
                else {

                    $scope.table = false;
                    $scope.busy = false;
                    swal({
                        text: 'Please Select Company Name',
                        width: 300,
                        height: 300
                    });

                }

            }

            $scope.SearchEmployee = function () {
                $scope.temp1 = '';
                $scope.GetMapped_Employee = [];
                $scope.table = false;

                setTimeout(function () {
                    $('#Select3').focus();
                }, 300);

                $('#stdSearch').modal('show');
            }

            $scope.SelectedUserName = function (info) {
                $scope.temp['name'] = info.employee_name;
                $scope.temp['up_name'] = info.emp_code;

                setTimeout(function () {
                    $('#txt_doc_narration').focus();
                }, 300);

                $('#stdSearch').modal('hide');

            }

            $http.get(ENV.apiUrl + "api/Sales_Doc/GetAllDocumentType").then(function (res) {
                $scope.rcbDocType = res.data;
                $scope.temp['doc_code'] = $scope.rcbDocType[0].dco_code
            });

            $http.get(ENV.apiUrl + "api/InterDepartmentTransfer/GetAllSalesTypeidt").then(function (res) {

                $scope.rcbSalesType = res.data;
                if ($scope.rcbSalesType.length > 0) {
                    $scope.temp['sal_type'] = res.data[1].sal_type;
                }

            });

            $http.get(ENV.apiUrl + "api/InterDepartmentTransfer/GetAllDepartmentsidt").then(function (GetAllDepartments) {
                $scope.GetAllDepartments2 = GetAllDepartments.data;
                $scope.temp['dep_code'] = $scope.GetAllDepartments2[0].dep_code;

            });

            $http.get(ENV.apiUrl + "api/InvLPO/Get_Adj_Dttypes").then(function (Get_Adj_Dttypes) {
                $scope.Get_Adj_Dttypes = Get_Adj_Dttypes.data;

                $scope.temp['dt_code'] = '01';
                $scope.edt['dt_code'] = '01';
            });

            $http.get(ENV.apiUrl + "api/Sales_Doc/GetAllSupplierGroupName").then(function (res) {
                $scope.rcbGroup = res.data;

            });

            $http.get(ENV.apiUrl + "api/InterDepartmentTransfer/GetAllItemsInSubCategoryidt").then(function (res) {
                $scope.rcbItems = res.data;

            });

            $http.get(ENV.apiUrl + "api/InterDepartmentTransfer/get_Categoriesidt").then(function (res) {

                $scope.rcbCategories = res.data;

            });

            $scope.getCategories = function (str) {
                $http.get(ENV.apiUrl + "api/InterDepartmentTransfer/get_SubCategoriesidt?pc_parentcode=" + str).then(function (res) {
                    $scope.rcbSubCategories = res.data;

                });

            }

            $scope.getSubCategories = function (str) {
                $scope.rcbItems = [];
                $http.get(ENV.apiUrl + "api/InterDepartmentTransfer/GetAllItemsInSubCategoryNewidt?pc_code=" + str).then(function (res) {
                    $scope.rcbItems = res.data;

                });

            }

            function salesman() {
                $http.get(ENV.apiUrl + "api/InterDepartmentTransfer/GetSalesmanNameforidt?user_code=" + user).then(function (res) {
                    $scope.salesman = res.data;
                    $scope.temp['sm_code'] = $scope.salesman[0].sm_code;
                    $scope.temp['sm_name'] = $scope.salesman[0].sm_name;

                });
            }

            $http.get(ENV.apiUrl + "api/InterDepartmentTransfer/GetAllItemsInSubCategoryforidt").then(function (res) {
                $scope.rcbItems = res.data;

            });

            $http.get(ENV.apiUrl + "api/InterDepartmentTransfer/GetSalesmanNameForCancelReceiptforidt").then(function (res) {
                $scope.salesman = res.data;
            });

            $scope.SearchItemforcancel = function () {

                $scope.items = [];

                $http.post(ENV.apiUrl + "api/InterDepartmentTransfer/DetailsItemsForCancelReceiptidt", $scope.edt).then(function (DetailsItemsForCancelReceipt) {
                    $scope.DetailsItemsForCancelReceipt = DetailsItemsForCancelReceipt.data;
                    $scope.Finalize_btn = false;

                    for (var i = 0; i < $scope.DetailsItemsForCancelReceipt.length; i++) {
                        for (var j = 0; j < $scope.DetailsItemsForCancelReceipt[i].appIDTList.length; j++) {
                            $scope.items.push($scope.DetailsItemsForCancelReceipt[i].appIDTList[j]);
                        }

                    }

                    if ($scope.DetailsItemsForCancelReceipt.length > 0) {
                        $scope.table1 = true;
                        $scope.items1 = $scope.DetailsItemsForCancelReceipt;

                        $scope.totalItems = $scope.DetailsItemsForCancelReceipt.length;
                        $scope.todos = $scope.DetailsItemsForCancelReceipt;
                        $scope.makeTodos();
                    }
                    else {
                        $scope.items1 = $scope.DetailsItemsForCancelReceipt;
                        $scope.totalItems = $scope.DetailsItemsForCancelReceipt.length;
                        $scope.todos = $scope.DetailsItemsForCancelReceipt;
                        $scope.makeTodos();
                        $scope.table1 = false;
                        swal({ text: 'Data Not Found', width: 300, showCloseButton: true });


                    }
                });

            }

            $scope.SearchItemforcancel1 = function () {


                $scope.items2 = [];

                $scope.im_item_code1;

                $scope.temp['im_item_code'] = $scope.im_item_code1.split('#')[0];
                $scope.item_name = $scope.im_item_code1.split('#')[1];

                $scope.table = true;

                for (var i = 0; i < $scope.rcbItems.length; i++) {
                    if ($scope.rcbItems[i].im_item_code == $scope.temp.im_item_code) {

                        $scope.temp['qty'] = '1';
                        $scope.temp['im_sell_price'] = '1';
                        $scope.temp['im_inv_no'] = $scope.rcbItems[i].im_inv_no;
                        $scope.temp['original_qty'] = $scope.rcbItems[i].original_qty;
                        $scope.temp['item_desc'] = $scope.rcbItems[i].im_desc;
                        $scope.temp['gcode'] = $scope.rcbItems[i].sg_name
                    }

                }
                $scope.Calculateprice($scope.temp);
            }

            $scope.Calculateprice = function (info) {

                info.finalValue = parseFloat(info.im_sell_price) * parseFloat(info.qty)
                document.getElementById('addbtn').focus();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                console.log($scope.filteredTodos);

            };

            $scope.Reset = function () {
                $scope.edt = {}
                $scope.edt['doc_date'] = today;
                $scope.edt['doc_prov_date'] = yyyy + '/' + (mm - 3) + '/' + dd;
            }

            $scope.Reset1 = function () {
                $scope.temp1 = {}
            }

            $scope.Cancel = function () {
                $scope.temp['doc_total_amount'] = '';
                $scope.temp['doc_delivery_remarks'] = '';
                $scope.temp['doc_discount_amount'] = '';
                $scope.temp['overalldis'] = '';
                $scope.temp['name'] = '';
                $scope.temp['up_name'] = '';
                $scope.temp['category_code'] = '';
                $scope.temp['subcategory_code'] = '';
                $scope.temp['gcode'] = '';
                $scope.addtable = false;
                $scope.temp1 = {}
                $scope.GetItems = [];
                $scope.itemList = [];
                $scope.im_item_code1 = '';
                $scope.Finalize_btn = true;
                $scope.temp['loc_code'] = $scope.Get_Adj_locations[0].loc_code;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#Table1").tableHeadFixer({ 'top': 1 });
                $("#mycell").tableHeadFixer({ 'top': 1 });
            }, 100);

            setTimeout(function () {
                $("#rcbItem").select2();

            }, 100);

            $scope.finalize = function (isvalidate) {

                $scope.cus_account_no = $scope.temp.cus_account_no
                $scope.maindatasend = [];
                $scope.approveddata = {};
                $scope.temp['doc_status'] = '4';


                $scope.approveddata = $scope.temp;

                var datasend = [];
                if (isvalidate) {

                    if ($scope.itemList.length > 0) {
                        $http.post(ENV.apiUrl + "api/InterDepartmentTransfer/Insert_Sale_Documentsidt", $scope.temp).then(function (msg) {
                            var d = msg.data;

                            if (d != "" || d != undefined) {

                                for (var i = 0; i < $scope.itemList.length; i++) {

                                    var data = {}
                                    data.dep_code = $scope.temp.dep_code;
                                    data.doc_prov_no = msg.data;
                                    data.dd_line_no = i;
                                    data.dd_status = '3';
                                    data.dd_payment_mode = '';
                                    data.im_inv_no = $scope.itemList[i].im_inv_no;
                                    data.loc_code = $scope.itemList[i].loc_code;
                                    data.dd_sell_price = $scope.itemList[i].dd_sell_price;
                                    data.dd_sell_price_discounted = 0;
                                    data.dd_sell_price_final = 0;
                                    data.dd_sell_value_final = $scope.itemList[i].dd_sell_value_final;
                                    data.dd_sell_cost_total = 0;
                                    data.dd_qty = $scope.itemList[i].dd_qty;
                                    data.dd_outstanding_qty = 0;
                                    data.dd_line_discount_pct = 0;

                                    if ($scope.temp.doc_discount_amount != '' || $scope.temp.doc_discount_amount != undefined) {
                                        data.dd_line_discount_amount = parseFloat(($scope.itemList[i].dd_sell_value_final * $scope.temp.overalldis) / 100);
                                    }
                                    else {
                                        data.dd_line_discount_amount = 0;
                                    }
                                    data.dd_line_total = 0;
                                    data.dd_physical_qty = 0;

                                    data.dd_cheque_number = '';
                                    data.dd_cheque_date = '';
                                    data.dd_cheque_bank_code = '';

                                    datasend.push(data)
                                }

                                $scope.maindatasend = datasend;

                                $http.post(ENV.apiUrl + "api/InterDepartmentTransfer/Insert_Sale_Documents_Detailsidt", datasend).then(function (msg) {
                                    $scope.printdata = msg.data;
                                    if (msg.data) {
                                        swal({ text: 'Provision No' + '   ' + d + '   ' + 'Inserted Successfully', width: 320, showCloseButton: true });
                                        $scope.approvebutton = true;
                                        $scope.Finalize_btn = true;
                                        $scope.print_btn = false;
                                        $scope.doc_prov_no_For_Print = d;
                                        $scope.Cancel();

                                    }
                                    else {
                                        swal({ text: "Record Not Inserted", width: 320, showCloseButton: true });

                                    }
                                });
                            }
                            else {
                                swal({ text: 'Record Not Insert In Sale Document', width: 320, showCloseButton: true });
                            }
                        });
                    }
                    else {

                        swal({ text: 'Add At Least One Record To Return', width: 300, showCloseButton: true });
                    }
                }
            }

            $scope.ViewItemdetails = function (info) {
                $scope.ViewItem1 = [];
                //var total = 0;
                //var data = [];
                //var qty=0;
                //var dd_sell_price=0;
                //for (var i = 0; i < info.appIDTList.length; i++) {
                //   total +=parseFloat(info.appIDTList[i].dd_sell_value_final);
                //   qty +=parseFloat(info.appIDTList[i].dd_qty);
                //   dd_sell_price +=parseFloat(info.appIDTList[i].dd_sell_price);
                //   data = {
                //       im_inv_no: '',
                //       im_item_code: '',
                //       im_desc: '',
                //       dd_qty: qty,
                //       dd_sell_price:dd_sell_price,
                //       dd_sell_value_final: total
                //    }
                //}
                //info.appIDTList.push(data);
                //$scope.ViewItem1 = info.appIDTList;
                // $('#itemSearch').modal('show');




                if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                    var data = {
                        location: 'Invs.INVR02DPSMIS',
                        parameter: {
                            doc_prov_no: info.doc_prov_no,
                        },
                        state: 'main.InvRes',
                        ready: function () {
                            this.refreshReport();
                        },
                    }

                }
                else if ($http.defaults.headers.common['schoolId'] == 'brs') {
                    var data = {
                        location: 'Invs.INVR02BRS',
                        parameter: {
                            doc_prov_no: info.doc_prov_no,
                        },
                        state: 'main.InvRes',
                        ready: function () {
                            this.refreshReport();
                        },
                    }
                }
                else {
                    var data = {
                        location: 'Invs.INVR02',
                        parameter: {
                            doc_prov_no: info.doc_prov_no,
                        },
                        state: 'main.InvRes',
                        ready: function () {
                            this.refreshReport();
                        },
                    }
                }

                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')


            }

            $scope.SelectAllItem = function () {

                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    var check = document.getElementById('mainchk');
                    if (check.checked == true) {
                        $scope.filteredTodos[i].item_status = true;
                        $('tr').addClass("row_selected");
                    }
                    else {
                        $scope.filteredTodos[i].item_status = false;
                        $('tr').removeClass("row_selected");
                    }

                }
            }

            $scope.SelectOneByOne = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                var main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }

            }

            $scope.SelectOneByOne1 = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                var main = document.getElementById('Checkbox1');
                if (main.checked == true) {
                    main.checked = false;
                }

            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $(window).bind('keydown', function (event) {

                if (event.ctrlKey || event.metaKey) {
                    switch (String.fromCharCode(event.which).toLowerCase()) {
                        case 'e':
                            event.preventDefault();
                            $scope.flag = false;

                            $scope.table = false;

                            setTimeout(function () {
                                $('#Select3').focus();
                                $scope.temp1 = '';
                                $scope.GetMapped_Employee = [];
                            }, 300);

                            $('#stdSearch').modal('show');

                            break;
                        case 'q':
                            event.preventDefault();
                            $scope.temp1 = '';
                            $scope.GetMapped_Employee = [];
                            $scope.GetItems = [];
                            $scope.temp1 = {};
                            $('#stdSearch').modal('hide');
                            $('#itemSearch').modal('hide');
                            break;
                        case 'i':
                            event.preventDefault();

                            $scope.flag = false;
                            $scope.itemtable = false;

                            $scope.itemsetflg = false;
                            $scope.ctrl = true;

                            setTimeout(function () {
                                $scope.GetItems = [];
                                $scope.temp1 = {};
                                $('#Desc_Res').focus();
                            }, 300);

                            $('#itemSearch').modal('show');


                    }
                }
            });

            $scope.size = function (str) {

                if (str == 'All') {

                    $scope.filteredTodos = $scope.items1;
                    $scope.numPerPage = $scope.items1.length,
                    $scope.maxSize = $scope.items1.length;
                    $scope.totalItems = $scope.items1.length;
                    $scope.todos = $scope.items1;
                    $scope.makeTodos();


                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.items1, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.items1;
                }

                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.doc_prov_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sm_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.im_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.CancelByReceipt = function () {

                var DataSendObject = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].item_status == true) {
                        $scope.flag = true;
                        $scope.doc_prov_no_For_Print = $scope.doc_prov_no_For_Print + $scope.filteredTodos[i].doc_prov_no + ',';
                        DataSendObject.push($scope.filteredTodos[i]);
                    }
                }

                if ($scope.flag == true) {
                    $http.post(ENV.apiUrl + "api/InterDepartmentTransfer/CUDDetailsItemsForCancelReceiptidt", DataSendObject).then(function (MSG) {
                        $scope.msg = MSG.data;

                        if ($scope.msg != "" && $scope.msg != undefined) {
                            swal({ text: "Provision No " + $scope.msg + " Return Successfylly", width: 320, showCloseButton: true });
                            $scope.Finalize_btn = true;
                            $scope.table1 = false;
                            $scope.print_btn = false;
                            $scope.SearchItemforcancel();
                        }
                        else {

                            swal({ text: "Item Not  Return", width: 320, showCloseButton: true });
                        }
                    });
                }
                else {
                    swal({ text: "Select At Least One Record", width: 320, showCloseButton: true });
                }
            }

            $scope.focustobutton = function () {
                document.getElementById('subbtn').focus();
            }

            $scope.itemList = [];

            $scope.add = function () {
                $scope.Finalize_btn = false;


                if ($scope.temp.im_inv_no == '' || $scope.temp.im_inv_no == undefined || $scope.temp.original_qty == undefined || $scope.temp.original_qty == '' || $scope.temp.qty == '' || $scope.temp.qty == undefined || $scope.temp.im_sell_price == '' || $scope.temp.im_sell_price == undefined) {

                    swal({ text: 'Item Quantity Is 0 And Purchase Price Is 0 Or select Item Name', width: 320, showCloseButton: true });
                }
                else {
                    $scope.Finalize_btn = false;
                    $scope.addtable = true;
                    $scope.totalFinal = 0;
                    $scope.flg = false;
                    for (var i = 0; i < $scope.itemList.length; i++) {
                        if ($scope.itemList[i].im_inv_no == $scope.temp.im_inv_no) {
                            $scope.itemList[i].dd_qty = parseInt($scope.itemList[i].dd_qty) + parseInt($scope.temp.qty);
                            $scope.itemList[i].dd_sell_value_final = ($scope.itemList[i].dd_qty) * (parseFloat($scope.temp.im_sell_price))

                            $scope.flg = true;
                            i = $scope.itemList.length;
                        }

                    }


                    if ($scope.flg == false) {
                        $scope.addtable = true;
                        var data =
                            {
                                sg_name: $scope.temp.gcode,
                                sg_code: $scope.temp.gcode,
                                im_inv_no: $scope.temp.im_inv_no,
                                im_item_code: $scope.temp.im_item_code,
                                im_desc: $scope.temp.item_desc,
                                item_location_name: $("#rcbLocation").find("option:selected").text(),
                                loc_code: $scope.temp.loc_code,
                                original_qty: $scope.temp.original_qty,
                                dd_qty: $scope.temp.qty,
                                dd_sell_price: $scope.temp.im_sell_price,
                                dd_sell_value_final: $scope.temp.finalValue,
                            }
                        $scope.Finalize_btn = false;
                        $scope.itemList.push(data)
                    }
                    $scope.Finalize_btn = false;
                    for (var i = 0; i < $scope.itemList.length; i++) {
                        $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_final);
                    }
                    $scope.temp.doc_total_amount = $scope.totalFinal;
                }

                $scope.temp.im_inv_no = '';
                $scope.CancelItem();
            }

            $scope.CancelItem = function () {

                $scope.temp['finalValue'] = ''
                $scope.temp['im_sell_price'] = ''
                $scope.temp['qty'] = ''
                $scope.temp['subcategory_code'] = ''
                $scope.temp['category_code'] = ''
                $scope.temp['gcode'] = ''
                $scope.rcbSubCategories = [];
                $scope.im_item_code1 = '';
                document.getElementById("rcbItem").focus();
            }

            $scope.onlyNumbers = function (event) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $scope.qtychangeby = function (str) {
                $scope.totalFinal = 0;
                str.dd_sell_value_final = (parseFloat(str.dd_qty) * parseFloat(str.dd_sell_price));

                for (var i = 0; i < $scope.itemList.length; i++) {
                    $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_final);
                }
                $scope.temp.doc_total_amount = $scope.totalFinal;
            }

            $scope.removeItem = function (obj, index) {

                $scope.totalFinal = 0;

                $scope.itemList.splice(index, 1);

                if ($scope.itemList.length == 0) {
                    $scope.addtable = false;
                    $scope.temp['doc_total_amount'] = '0.0';
                    $scope.Finalize_btn = true;
                }
                for (var i = 0; i < $scope.itemList.length; i++) {
                    $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_final);
                }
                $scope.temp.doc_total_amount = $scope.totalFinal;

            }

            $scope.enroll_change = function (str) {

                if ($scope.temp.sal_type == '05') {
                    //$scope.temp['up_name'] = $scope.SelectedUserLst[0].em_number;
                    //$scope.temp['name'] = $scope.SelectedUserLst[0].empName;
                    $http.post(ENV.apiUrl + "api/Sales_Doc/Insert_User_profiles?name=" + str).then(function (res) {

                        $scope.internalUser = res.data;
                        console.log($scope.internalUser)
                        //  $scope.rcbCategories = res.data;
                        //  if(res.data.length>0)
                        //   $scope.temp['name'] = res.data.studentname;

                    });

                }
                else if ($scope.temp.sal_type == '04') {
                    $http.get(ENV.apiUrl + "api/Sales_Doc/GetStudentInfo?enrollnum=" + str).then(function (res) {

                        //  $scope.rcbCategories = res.data;
                        //  if(res.data.length>0)
                        $scope.temp['name'] = res.data.studentname;

                    });

                }
                if (str == '' || str == undefined) {
                }
                else {
                    $scope.disable_btn = false;

                }
            }

            $scope.searchstudent = function () {
                $scope.busy = true;
                $scope.searchtable = false;
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.temp)).then(function (Allstudent) {
                    $scope.student = Allstudent.data;
                    $scope.busy = false;
                    $scope.searchtable = true;
                });
            }

            $scope.GetSalesTypeChange = function (str) {


                debugger;

                if (str == '03') {   //staff

                    $rootScope.visible_stud = false;
                    $rootScope.visible_parent = false;
                    $rootScope.visible_search_parent = false;
                    $rootScope.visible_teacher = false;
                    $rootScope.visible_User = false;
                    $rootScope.visible_Employee = true;

                    $rootScope.chkMulti = false;

                }
                else if (str == '04') { //student

                    $rootScope.visible_stud = true;
                    $rootScope.visible_parent = false;
                    $rootScope.visible_search_parent = false;
                    $rootScope.visible_teacher = false;
                    $rootScope.visible_User = false;
                    $rootScope.visible_Employee = false;
                    $rootScope.chkMulti = false;

                }
            }

            $scope.$on('global_cancel', function (str) {

                console.log($scope.SelectedUserLst);
                if ($scope.SelectedUserLst.length > 0) {
                    debugger
                    if ($scope.temp.sal_type == '03') {
                        $scope.temp['up_name'] = $scope.SelectedUserLst[0].em_number;
                        $scope.temp['name'] = $scope.SelectedUserLst[0].empName;

                    }
                    else if ($scope.temp.sal_type == '04') {
                        $scope.temp['up_name'] = $scope.SelectedUserLst[0].s_enroll_no;
                        $scope.temp['name'] = $scope.SelectedUserLst[0].name + ' Class: ' + $scope.SelectedUserLst[0].s_class;

                    }
                }

                if ($scope.temp.up_name == '' || $scope.temp.up_name == undefined) {
                }
                else {
                    $scope.disable_btn = false;

                }
                // $scope.getstudentList();
            });

            $scope.studentSearch = function () {

                debugger;

                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                //$('#stdSearch').modal('show');

            }

            $scope.PrintItemReceipt = function () {

                $scope.print_btn = true;

                swal({
                    title: '',
                    text: "Do you want print receipt",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                    allowOutsideClick: false,
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        //  $scope.OkRejectadm();
                        if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                            var data = {
                                location: 'Invs.INVR02DPSMIS',
                                parameter: { doc_prov_no: $scope.doc_prov_no_For_Print },
                                state: 'main.InvRes'
                            }

                        }
                        else if ($http.defaults.headers.common['schoolId'] == 'brs') {
                            var data = {
                                location: 'Invs.INVR02BRS',
                                parameter: { doc_prov_no: $scope.doc_prov_no_For_Print },
                                state: 'main.InvRes'
                            }

                        }
                        else {
                            var data = {
                                location: 'Invs.INVR02',
                                parameter: { doc_prov_no: $scope.doc_prov_no_For_Print },
                                state: 'main.InvRes'
                            }
                        }
                        window.localStorage["ReportDetails"] = JSON.stringify(data)
                        $state.go('main.ReportCardParameter')
                    }
                })

            }

            $scope.CancelByReceipt1 = function () {

                var check = document.getElementById('mainchk');
                if (check.checked == true) {
                    check.checked = false;
                    $scope.SelectAllItem();
                }
                else {
                    check.checked = false;
                    $scope.SelectAllItem();
                }



            }
        }])
})();