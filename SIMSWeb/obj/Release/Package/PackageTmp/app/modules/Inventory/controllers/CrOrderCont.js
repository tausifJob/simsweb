﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [], data = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });



    simsController.controller('CrOrderCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.req_no = [];
            $scope.selected_reqtype = '';
            $scope.flag_iforderdiscount = "false";
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);
            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);
            $timeout(function () {
                $("#fixTable2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.lcshow = false;

            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;
            $scope.cdetails_data = [];

            var date = new Date();
            $scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');

            $scope.temp =
                {
                    order_date: $scope.ddMMyyyy,
                    expected_date: $scope.ddMMyyyy,
                    invs058_order_apply_aexp: false
                }

            $scope.total_netamt = 0;
            $scope.total_grossamt = 0;
            $scope.totAmt = 0;

            //*********************All Combo*********************

            $http.get(ENV.apiUrl + "api/CreateOrder/GetDepartment").then(function (res1) {
                $scope.Department = res1.data;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetSuppliers").then(function (res11) {
                $scope.Suppliers = res11.data;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetServiceTypes").then(function (res2) {
                $scope.ServiceTypes = res2.data;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetSupplierDepartment").then(function (res1) {
                $scope.supplier = res1.data;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetDeliveryMOde").then(function (res1) {
                $scope.Deliverymode = res1.data;

                if ($scope.Deliverymode.length > 0)
                    $scope.temp['dm_code'] = $scope.Deliverymode[0].dm_code;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/Getpaymentmodes").then(function (res1) {
                $scope.paymentmode = res1.data;

                if ($scope.paymentmode.length > 0)
                    $scope.temp['pm_code'] = $scope.paymentmode[0].pm_code;

            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetTradeterms").then(function (res1) {
                $scope.Trades = res1.data;

                if ($scope.Trades.length > 0)
                    $scope.temp['trt_code'] = $scope.Trades[0].trt_code;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetLetterOfCredit").then(function (res1) {
                $scope.LOC = res1.data;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetForwardAgent").then(function (res1) {
                $scope.Agent = res1.data;

                if ($scope.Agent.length > 0)
                    $scope.temp['fa_code'] = $scope.Agent[0].fa_code;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetCurrencyMaster").then(function (res1) {
                debugger;
                $scope.CurMaster = res1.data;

                $http.get(ENV.apiUrl + "api/CreateOrder/GetCompanyCurrency").then(function (res_cur) {
                    console.log(res_cur);
                    if ($scope.CurMaster.length > 0)

                        $scope.temp['excg_curcy_code'] = res_cur.data;//$scope.CurMaster[0].excg_curcy_code;
                });
            });

            $scope.order_report_name = '';

            $http.get(ENV.apiUrl + "api/CreateOrder/Get_Order_report").then(function (res) {
                $scope.shipment_report_name = res.data;
            });

            $http.get(ENV.apiUrl + "api/CreateOrder/GetOrderAttributes").then(function (res2) {
                $scope.OrderAttrs = res2.data;
            });

            //***************************************************

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.cdetails_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.cdetails_data;
                }
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            function searchUtil(item, toSearch) {
                /* Search Text in all 2 fields */
                return (item.invs058_im_inv_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.invs058_rd_item_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.SearchRequest = function () {
                debugger;

                if ($scope.selected_reqtype != undefined && $scope.selected_reqtype != '') {
                    if ($scope.temp.servicevalue != $scope.selected_reqtype && $scope.testALL.length > 0) {
                        swal({ title: "Alert", text: "You have selected different Request Type than Previous one.", width: 300, height: 200 });

                        return;
                    }
                }

                $scope.selected_reqtype = $scope.temp.servicevalue;

                $http.get(ENV.apiUrl + "api/CreateOrder/GetSuppliers").then(function (res11_sup) {
                    $scope.Suppliers_srch = res11_sup.data;
                });

                $http.get(ENV.apiUrl + "api/CreateOrder/GetAllItemsInSubCategoryNew?dep_code=" + $scope.temp.dep_code + "&req_type=" + $scope.temp.servicevalue).then(function (res_item) {
                    $scope.rcbItems = res_item.data;
                });

                $http.get(ENV.apiUrl + "api/CreateOrder/GetAllRequestNos?dep_code=" + $scope.temp.dep_code + "&req_type=" + $scope.temp.servicevalue).then(function (res_req) {
                    $scope.rcbreqs = res_req.data;
                });

                setTimeout(function () {
                    $("#rcbItem").select2();
                    $("#rcbItem1").select2();

                }, 100);

                console.log($scope.temp.dep_code);
                console.log($scope.temp.servicevalue);
                if ($scope.temp.dep_code != undefined && $scope.temp.servicevalue != undefined) {
                    $scope.totAmt = 0;
                    // $('#MyModal1').modal('show');
                    $('#MyModal1').modal({ backdrop: 'static', keyboard: true });
                    $scope.srchsupplier = '';
                    $scope.srchitemcode = '';
                    $scope.srch_req_no = '';
                    var im_inv_no = '';
                    var im_item_code = '';
                    var im_desc = '';
                    $http.get(ENV.apiUrl + "api/CreateOrder/Get_ALLRequestDetails?dep_code=" + $scope.temp.dep_code + "&req_type=" + $scope.temp.servicevalue + "&srch_supcode=" + $scope.srchsupplier + "&srch_itemdesc=" + im_desc + "&srch_itemcode=" + im_item_code + "&srch_req_no=" + $scope.srch_req_no).then(function (res12) {
                        debugger;
                        $scope.search_Req_Data = res12.data;
                        $scope.search_Req_Data1 = angular.copy(res12.data);
                        $scope.cdetails_data = $scope.search_Req_Data1;
                        $scope.totalItems = $scope.cdetails_data.length;
                        $scope.todos = $scope.cdetails_data;
                        $scope.makeTodos();

                        //console.log($scope.search_Req_Data);
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Department and Request Type", width: 300, height: 200 });
                }
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].req_no1 = false;
                }

            };

            $scope.RemoveEnrollMentNo = function ($event, index, str) {
                str.splice(index, 1);
            }

            $scope.search_Req_Data_all = [];
            $scope.testALL = [];

            $scope.okbuttonclick = function () {
                debugger;
                //for (var i = 0; i < $scope.search_Req_Data1.length; i++) {
                //    if ($scope.search_Req_Data1[i].req_no1 == true) {

                //        $scope.testALL.push($scope.search_Req_Data1[i]);

                //        for (var a = 0; a < $scope.testALL.length; a++) {                            
                //            $scope.reqnos = $scope.reqnos + ',' + $scope.testALL[a].req_no;                            
                //        }
                //    }
                //    $scope.Main_table = true;
                //}

                //for (var i = 0; i < $scope.filteredTodos.length; i++) {
                //    if ($scope.filteredTodos[i].req_no1 == true) {

                //        //for (var j = 0; j < $scope.testALL.length; j++) {

                //            //if ($scope.testALL[j].invs058_req_no != $scope.filteredTodos[i].invs058_req_no && $scope.testALL[j].invs058_rd_line_no != $scope.filteredTodos[i].invs058_rd_line_no)

                //            $scope.testALL.push($scope.filteredTodos[i]);

                //            for (var a = 0; a < $scope.testALL.length; a++) {
                //                $scope.reqnos = $scope.reqnos + ',' + $scope.testALL[a].req_no;
                //            }
                //            //j++;
                //        //}
                //    }
                //    $scope.Main_table = true;
                //}
                //$scope.testALL = [];

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].req_no1 == true) {
                        $scope.flg_chk = true;
                        debugger;

                        for (var j = 0; j < $scope.testALL.length; j++) {

                            debugger;

                            if ($scope.filteredTodos[i].invs058_req_no == $scope.testALL[j].invs058_req_no && $scope.testALL[j].invs058_im_inv_no == $scope.filteredTodos[i].invs058_im_inv_no) {
                                $scope.flg_chk = false;
                            }
                        }


                        if ($scope.flg_chk)
                            $scope.testALL.push($scope.filteredTodos[i]);

                        for (var a = 0; a < $scope.testALL.length; a++) {
                            $scope.testALL[a].invs058_rd_line_no1 = true;

                            //if ($scope.temp.servicevalue == 'I') {
                            //    $scope.testALL.servicevalue = false;
                            //}
                            //else if ($scope.temp.servicevalue == 'S') {
                            //    $scope.testALL.servicevalue = true;
                            //}
                            //else { $scope.testALL.servicevalue = true; }

                            $scope.reqnos = $scope.reqnos + ',' + $scope.testALL[a].req_no;
                        }
                    }

                    $scope.Main_table = true;
                }

                $scope.srchsupplier = null;
                $scope.srchitemcode = null;
                $scope.srch_req_no = null;
            }

            $scope.checkreqtype = function () {

                if ($scope.selected_reqtype != undefined && $scope.selected_reqtype != '') {
                    debugger;
                    if ($scope.temp.servicevalue != $scope.selected_reqtype && $scope.testALL.length > 0) {

                        if ($scope.temp.servicevalue == 'I')
                            $scope.temp.servicevalue = 'S'
                        else if ($scope.temp.servicevalue == 'S')
                            $scope.temp.servicevalue = 'I'

                        swal({ title: "Alert", text: "You have selected different Request Type than Previous one.", width: 300, height: 200 });

                        return;
                    }
                }

            }

            $scope.SearchRequests = function () {
                debugger;
                var im_inv_no = $scope.srchitemcode.im_inv_no;
                var im_item_code = $scope.srchitemcode.im_item_code;
                var im_desc = $("#rcbItem").find("option:selected").text();

                if (im_item_code == undefined)
                    im_item_code = '';

                $http.get(ENV.apiUrl + "api/CreateOrder/Get_ALLRequestDetails?dep_code=" + $scope.temp.dep_code + "&req_type=" + $scope.temp.servicevalue + "&srch_supcode=" + $scope.srchsupplier + "&srch_itemdesc=" + im_desc + "&srch_itemcode=" + im_item_code + "&srch_req_no=" + $scope.srch_req_no).then(function (res12) {
                    debugger;
                    $scope.search_Req_Data = res12.data;
                    $scope.search_Req_Data1 = angular.copy(res12.data);
                    $scope.cdetails_data = $scope.search_Req_Data1;
                    $scope.totalItems = $scope.cdetails_data.length;
                    $scope.todos = $scope.cdetails_data;
                    $scope.makeTodos();

                    //console.log($scope.search_Req_Data);
                });
            }

            $scope.ResetRequests = function () {
                $scope.srchsupplier = '';
                $scope.srchitemcode = '';
                $scope.srch_req_no = '';
                $("#rcbItem").select2("val", "");
                var im_inv_no = '';
                var im_item_code = '';
                var im_desc = '';
                $scope.testALL = [];
                datasend = [];
                datasend1 = [];
                datasend2 = [];
                $scope.totAmt = 0;
                $scope.total_netamt = 0;
                $scope.total_grossamt = 0;

                $scope.flag_iforderdiscount = "false";
                $http.get(ENV.apiUrl + "api/CreateOrder/Get_ALLRequestDetails?dep_code=" + $scope.temp.dep_code + "&req_type=" + $scope.temp.servicevalue + "&srch_supcode=" + $scope.srchsupplier + "&srch_itemdesc=" + im_desc + "&srch_itemcode=" + im_item_code + "&srch_req_no=" + $scope.srch_req_no).then(function (res12) {
                    debugger;
                    $scope.search_Req_Data = res12.data;
                    $scope.search_Req_Data1 = angular.copy(res12.data);
                    $scope.cdetails_data = $scope.search_Req_Data1;
                    $scope.totalItems = $scope.cdetails_data.length;
                    $scope.todos = $scope.cdetails_data;
                    $scope.makeTodos();

                    //console.log($scope.search_Req_Data);
                });
            }

            $scope.Cancel1 = function () {
                debugger;
                $scope.temp = [];
                $scope.search_Req_Data = [];
                $scope.testALL = [];
                $scope.OrderAttrs = [];
                $scope.selected_reqtype = '';
                $scope.temp =
                {
                    order_date: $scope.ddMMyyyy,
                    expected_date: $scope.ddMMyyyy,
                }
                $scope.totAmt = 0;
                $http.get(ENV.apiUrl + "api/CreateOrder/GetOrderAttributes").then(function (res2) {
                    $scope.OrderAttrs = res2.data;
                });

                $scope.table = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.total_grossamt = '';
                $scope.total_netamt = '';
                $scope.total_grossamt = '';
                $scope.testALL = [];
                datasend = [];
                datasend1 = [];
                datasend2 = [];
                $scope.total_netamt = 0;
                $scope.total_grossamt = 0;
                $scope.totAmt = 0;
                $scope.flag_iforderdiscount = "false";

                $http.get(ENV.apiUrl + "api/CreateOrder/GetDepartment").then(function (res1) {
                    $scope.Department = res1.data;
                });

                $http.get(ENV.apiUrl + "api/CreateOrder/GetSuppliers").then(function (res11) {
                    $scope.Suppliers = res11.data;
                });

                $http.get(ENV.apiUrl + "api/CreateOrder/GetServiceTypes").then(function (res2) {
                    $scope.ServiceTypes = res2.data;
                });

                $http.get(ENV.apiUrl + "api/CreateOrder/GetSupplierDepartment").then(function (res1) {
                    $scope.supplier = res1.data;
                });

                $http.get(ENV.apiUrl + "api/CreateOrder/GetDeliveryMOde").then(function (res1) {
                    $scope.Deliverymode = res1.data;

                    if ($scope.Deliverymode.length > 0)
                        $scope.temp['dm_code'] = $scope.Deliverymode[0].dm_code;
                });

                $http.get(ENV.apiUrl + "api/CreateOrder/Getpaymentmodes").then(function (res1) {
                    $scope.paymentmode = res1.data;

                    if ($scope.paymentmode.length > 0)
                        $scope.temp['pm_code'] = $scope.paymentmode[0].pm_code;

                });

                $http.get(ENV.apiUrl + "api/CreateOrder/GetTradeterms").then(function (res1) {
                    $scope.Trades = res1.data;

                    if ($scope.Trades.length > 0)
                        $scope.temp['trt_code'] = $scope.Trades[0].trt_code;
                });

                $http.get(ENV.apiUrl + "api/CreateOrder/GetLetterOfCredit").then(function (res1) {
                    $scope.LOC = res1.data;
                });

                $http.get(ENV.apiUrl + "api/CreateOrder/GetForwardAgent").then(function (res1) {
                    $scope.Agent = res1.data;

                    if ($scope.Agent.length > 0)
                        $scope.temp['fa_code'] = $scope.Agent[0].fa_code;
                });

                $http.get(ENV.apiUrl + "api/CreateOrder/GetCurrencyMaster").then(function (res1) {
                    debugger;
                    $scope.CurMaster = res1.data;

                    $http.get(ENV.apiUrl + "api/CreateOrder/GetCompanyCurrency").then(function (res_cur) {
                        console.log(res_cur);
                        if ($scope.CurMaster.length > 0)

                            $scope.temp['excg_curcy_code'] = res_cur.data;//$scope.CurMaster[0].excg_curcy_code;
                    });
                });

                $scope.temp.invs058_order_apply_aexp = false;


                $state.go('main.Inv044');
                //swal({ title: "Alert", text: "In Cancel", width: 300, height: 200 });    
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                debugger;
                $scope.temp = "";
                $scope.search_Req_Data = null;
                $scope.testALL = null;
                $state.go('main.Inv044');
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            //DATA SAVE INSERT
            var datasend = [];
            var datasend1 = [];
            var datasend2 = [];
            var gofurther = true;
            var data = {};
            $scope.removeItem = function (obj, index) {
                debugger;
                $scope.testALL.splice(index, 1);
                $scope.orcalculateamt12();
            }

            $scope.savedata = function () {
                debugger;

                for (var i = 0; i < $scope.testALL.length; i++) {
                    //if ($scope.testALL[i].invs058_rd_line_no1 == true) {
                    if ($scope.temp.servicevalue == 'I' || $scope.temp.servicevalue == 'S') {
                        if ($scope.testALL[i].invs058_ordered_quantity == undefined || $scope.testALL[i].invs058_sup_price == undefined) {
                            gofurther = false;
                            swal({ title: "Alert", text: "Please enter Ordered Quantity/Supplier Price", width: 300, height: 200 });
                            gofurther = true;
                            return;
                        }
                    }
                    //}
                }

                if (gofurther == true) {
                    if ($scope.temp.order_date != undefined && $scope.temp.sup_code != undefined && $scope.temp.dep_code != undefined && $scope.temp.servicevalue != undefined &&
                        $scope.temp.pm_code != undefined && $scope.temp.trt_code != undefined) {

                        for (var i = 0; i < $scope.testALL.length; i++) {
                            //for (var j = 0; j < $scope.testALL[i].req_details.length; j++) {
                            // if ($scope.testALL[i].invs058_rd_line_no1 == true) {
                            data = {
                                'opr': 'D',
                                'order_d_req_no': $scope.testALL[i].invs058_req_no,
                                'order_d_rd_line_no': $scope.testALL[i].invs058_rd_line_no,
                                'order_d_im_inv_no': $scope.testALL[i].invs058_im_inv_no,
                                'order_d_uom_code': $scope.testALL[i].invs058_uom_code,
                                'order_d_od_order_qty': $scope.testALL[i].invs058_ordered_quantity,
                                'order_d_od_supplier_price': $scope.testALL[i].invs058_sup_price,
                                'order_d_od_discount_pct': $scope.testALL[i].invs058_discount,
                                'order_d_od_shipped_qty': $scope.testALL[i].order_d_od_shipped_qty,
                                'order_d_od_received_qty': $scope.testALL[i].order_d_od_received_qty,
                                'order_d_od_orig_line_no': $scope.testALL[i].order_d_od_orig_line_no,
                                'order_d_od_status': $scope.testALL[i].order_d_od_status,
                                'order_d_od_remarks': $scope.testALL[i].order_d_od_remarks,
                                'invs058_discount_amt': $scope.testALL[i].invs058_discount_amt
                            }
                            datasend.push(data);
                            //}
                            //}
                        }

                        $scope.temp.servicevalue = $scope.selected_reqtype;
                        var ifapplyexp = 0;
                        if ($scope.temp.invs058_order_apply_aexp == true)
                            ifapplyexp = 1;

                        var ord_addexp = 0;
                        var ord_discount_pct = 0;
                        var ord_discount_amt = 0;

                        for (var i = 0; i < $scope.OrderAttrs.length; i++) {
                            if ($scope.OrderAttrs[i].or_atamount != 0 && $scope.OrderAttrs[i].or_atamount != undefined && $scope.OrderAttrs[i].or_atamount != '') {
                                if ($scope.OrderAttrs[i].or_atstatus == 'A')
                                    ord_addexp = $scope.OrderAttrs[i].or_atamount;
                                if ($scope.OrderAttrs[i].or_atstatus == 'P')
                                    ord_discount_pct = $scope.OrderAttrs[i].or_atamount;
                                if ($scope.OrderAttrs[i].or_atstatus == 'S')
                                    ord_discount_amt = $scope.OrderAttrs[i].or_atamount;
                            }
                        }

                        $scope.data1 = {
                            'opr': 'O',
                            'order_ord_date': $scope.temp.order_date,
                            'order_dep_code': $scope.temp.dep_code,
                            'order_sup_code': $scope.temp.sup_code,
                            'order_cur_code': $scope.temp.excg_curcy_code,
                            'order_dm_code': $scope.temp.dm_code,
                            'order_ord_shipment_date': $scope.temp.expected_date,
                            'order_pm_code': $scope.temp.pm_code,
                            'order_trt_code': $scope.temp.trt_code,
                            'order_fa_code': $scope.temp.fa_code,
                            'order_lc_no': $scope.temp.lc_no,
                            'order_ord_supl_quot_ref': $scope.temp.supplier_qty_ref,
                            'invs058_req_type': $scope.temp.servicevalue,
                            'randomnumber': Math.floor((Math.random() * 100) + 1),
                            'invs058_order_createdby': $rootScope.globals.currentUser.username,
                            'invs058_heading': $scope.temp.invs058_heading,
                            'invs058_notes': $scope.temp.invs058_notes,
                            'invs058_order_apply_aexp': ifapplyexp,

                            'invs058_ord_addexps': ord_addexp,
                            'invs058_ord_dispct': ord_discount_pct,
                            'invs058_ord_disamt': ord_discount_amt,
                            'invs058_od_total_discount': '',
                            'invs058_od_total_expense': '',
                        }

                        if (datasend.length > 0) {
                            $http.post(ENV.apiUrl + "api/CreateOrder/SaveOrderDetails?data1=" + JSON.stringify($scope.data1), datasend).then(function (msg) {
                                $scope.msg1 = msg.data;

                                //$http.post(ENV.apiUrl + "api/CreateOrder/COrderDetails?ordno=" + $scope.msg1, datasend2).then(function (msg) {


                                swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 });
                                if ($scope.msg1 != null && $scope.msg1 != '') {
                                    //swal({ title: "Alert", text: "Order Number " + $scope.msg1 + "  inserted successfully", width: 300, height: 200 });

                                    swal({
                                        title: '',
                                        text: "Purchase Order No " + $scope.msg1 + "  successfully created.",
                                        showCloseButton: true,
                                        showCancelButton: true,
                                        confirmButtonText: 'Yes',
                                        width: 380,
                                        cancelButtonText: 'No',
                                        allowOutsideClick: false,
                                    }).then(function (isConfirm) {
                                        //if (isConfirm) {
                                        //    var data = {
                                        //                location: 'Invs.invr21',
                                        //                parameter: {
                                        //                    orderno: $scope.msg1,
                                        //                },
                                        //                state: 'main.Inv044',
                                        //                ready: function () {
                                        //                    this.refreshReport();
                                        //                },
                                        //            }
                                        //            console.log(data);

                                        //            window.localStorage["ReportDetails"] = JSON.stringify(data);
                                        //            $state.go('main.ReportCardParameter');
                                        //}
                                    });

                                    $scope.Cancel1();
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                                }
                                $scope.grid_Display();
                                //});
                                datasend = [];
                                $scope.selected_reqtype = '';
                                $scope.testALL = [];
                                $scope.flag_iforderdiscount = "false";
                                datasend1 = [];
                                datasend2 = [];
                                $scope.totAmt = 0;
                                $scope.table = true;
                                $scope.display = false;
                                //}
                            });
                        }
                        else {
                            swal({ title: "Alert", text: "Please Select Items to Create Order.", width: 300, height: 200 });
                        }
                    }
                }

                else {
                    swal({ title: "Alert", text: "Please Select Mandatory Fields", width: 300, height: 200 });
                }
            }

            $scope.onlyNumbers = function (event, str) {

                if (str > 100) {
                    swal({ title: "Alert", text: "Invalid Discount", width: 300, height: 200 });
                }

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $scope.Report = function (shipment_no) {
                console.log(str);
                var data = {
                    location: $scope.order_report_name,
                    parameter: {
                        ship: ord_no,
                    },
                    state: 'main.Inv044',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')
            }

            $scope.printConfrimDetails = function (str) {
                var data = {
                    location: 'Invs.INVR17',
                    parameter: { ord_no: str },
                    state: 'main.Inv044',
                    reg: param,
                    email: email
                }

                window.localStorage["ReportDetails"] = JSON.stringify(data)

                // printDetails
                $state.go('Report');
            }

            var dom;
            $scope.flag = true;
            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                         "<tr>   <td class='semi-bold'>" + "" + "</td>  <td class='semi-bold'>" + "Request No." + "</td> <td class='semi-bold'>" + "Request Line No." + " </td><td class='semi-bold'>" + "Item Desc" + "</td>" +
                        "<td class='semi-bold'>" + "UOM" + "</td> <td class='semi-bold'>" + "Quantity" + " </td>" + "<td class='semi-bold'>" + "Ordered Quantity" + " </td>" + "<td class='semi-bold'>" + "Supplier Price" + " </td>" + "<td class='semi-bold'>" + "Discount" + " </td>" + "<td class='semi-bold'>" + "Remark" + " </td>" + "</tr>" +

                          "<tr> <td>" + '<input type="checkbox" value="1" ng-click="SingleCheckSave()">' +
                         "<td>" + (info.invs058_req_no) + "</td> <td>" + (info.invs058_rd_line_no) + " </td><td>" + (info.rd_item_desc) + "</td>" +
                         "<td>" + (info.uom_code) + "</td> <td>" + (info.rd_quantity) + "</td>" + "<td>" + '<input style="width: 90px; height: 25px" aria-controls="example" ng-model="info.pays_appl_form_field_value2" class="form-control input-sm">' + "<td>" + '<input style="width: 90px; height: 25px" aria-controls="example" ng-model="info.pays_appl_form_field_value2" class="form-control input-sm">' + "<td>" + '<input style="width: 90px; height: 25px" aria-controls="example" ng-model="info.pays_appl_form_field_value2" class="form-control input-sm">' + "<td>" + (info.rd_remarks) + " </td>" + "</tr>" +

                        " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.req_no.length; i++) {
                        $scope.req_no[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }

            };

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.adddata = function (chkvalue, info) {
                debugger;
                if ($scope.temp.servicevalue == 'I') {
                    if (chkvalue == true) {
                        info.servicevalue = false;
                    }
                    else if (chkvalue == false) {
                        info.servicevalue = true;
                    }
                }
                else { info.servicevalue = true; }

                $scope.totAmt = 0;
                var totalamt = 0;
                for (var a = 0; a < $scope.testALL.length; a++) {
                    if ($scope.testALL[a].invs058_rd_line_no1 == true) {
                        if ($scope.testALL[a].invs058_ordered_quantity != undefined && $scope.testALL[a].invs058_sup_price != undefined) {
                            console.log($scope.testALL[a].invs058_rqvalue);
                            totalamt = totalamt + $scope.testALL[a].invs058_rqvalue;
                        }
                    }
                }

                if (totalamt != "NaN") {
                    $scope.totAmt = totalamt;
                } else { $scope.totAmt = 0; }
            }

            $scope.calamount = function (info, va) {
                debugger;
                $scope.totAmt = 0;
                if (va == 'amt') {
                    if (info.invs058_discount_amt != '' && info.invs058_discount_amt != undefined) {
                        info.invs058_discount = '';
                    }
                }
                if (va == 'dis') {
                    if (info.invs058_discount != '' && info.invs058_discount != undefined) {
                        if (info.invs058_discount > 100) {
                            info.invs058_discount = '';
                            swal({ title: "Alert", text: "Invalid Discount", width: 300, height: 200 });
                        }
                        info.invs058_discount_amt = '';
                    }
                }

                var totalamt = 0;
                var totalgrossamt = 0;
                var totalnetamt = 0;

                var totalamt1 = 0;
                var totalamt_gross1 = 0;
                for (var i = 0; i < $scope.testALL.length; i++) {
                    //if ($scope.testALL[i].invs058_rd_line_no1 == true) {
                    if ($scope.temp.servicevalue == 'I' || $scope.temp.servicevalue == 'S') {

                        if ($scope.testALL[i].invs058_ordered_quantity != undefined && $scope.testALL[i].invs058_ordered_quantity != '') {
                            if ($scope.testALL[i].invs058_sup_price != undefined && $scope.testALL[i].invs058_sup_price != '') {

                                totalamt1 = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                                $scope.testALL[i].invs058_gross_amt = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;

                                totalamt_gross1 = $scope.testALL[i].invs058_gross_amt;

                                if ($scope.flag_iforderdiscount == "false") {
                                    if ($scope.testALL[i].invs058_discount != undefined && $scope.testALL[i].invs058_discount != '') {
                                        var disamt = ((totalamt1 * $scope.testALL[i].invs058_discount) / 100);
                                        $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - disamt;
                                        totalamt1 = totalamt1 - disamt;
                                    }
                                    if ($scope.testALL[i].invs058_discount_amt != undefined && $scope.testALL[i].invs058_discount_amt != '') {
                                        var disamt = $scope.testALL[i].invs058_discount_amt;
                                        $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - $scope.testALL[i].invs058_discount_amt;
                                        totalamt1 = totalamt1 - disamt;
                                    }
                                }
                                else {
                                    $scope.testALL[i].invs058_discount = '';
                                    $scope.testALL[i].invs058_discount_amt = '';
                                }
                                totalnetamt = totalnetamt + totalamt1;
                                totalgrossamt = totalgrossamt + totalamt_gross1;
                            }
                            else {
                                $scope.testALL[i].invs058_rqvalue = 0;
                                $scope.testALL[i].invs058_gross_amt = 0;
                            }
                        }
                    }

                    totalamt = totalamt + totalamt1;
                    $scope.total_netamt = totalnetamt;
                    $scope.total_grossamt = totalgrossamt;
                    totalamt1 = 0;
                    totalamt_gross1 = 0;
                    // }
                }

                if (totalamt != NaN) {
                    $scope.totAmt = totalamt;
                    $scope.total_grossamt = totalgrossamt;
                    $scope.total_netamt = totalnetamt;
                }
                else { $scope.totAmt = 0; }


            }

            $scope.orcalculateamt = function (str) {
                debugger;

                var ttval1 = str.or_atstatus;
                if (ttval1 == 'P') {
                    if (str.or_atamount != 0 && str.or_atamount != '' && str.or_atamount != undefined) {
                        $scope.flag_iforderdiscount = "true";
                        for (var j = 0; j < $scope.OrderAttrs.length; j++) {
                            if ($scope.OrderAttrs[j].or_atstatus == 'S') {
                                $scope.OrderAttrs[j].or_atamount = 0;
                            }
                        }
                        if (str.or_atamount > 100) {
                            str.or_atamount = '';
                            swal({ title: "Alert", text: "Invalid Discount", width: 300, height: 200 });
                        }
                    }
                    else {
                        for (var j1 = 0; j1 < $scope.OrderAttrs.length; j1++) {
                            if ($scope.OrderAttrs[j1].or_atstatus == 'S') {
                                if ($scope.OrderAttrs[j1].or_atamount != 0 && $scope.OrderAttrs[j1].or_atamount != '' && $scope.OrderAttrs[j1].or_atamount != undefined) {

                                }
                                else {
                                    $scope.flag_iforderdiscount = "false";
                                }
                            }
                        }
                    }
                }

                if (ttval1 == 'S') {
                    if (str.or_atamount != 0 && str.or_atamount != '' && str.or_atamount != undefined) {
                        $scope.flag_iforderdiscount = "true";
                        for (var j = 0; j < $scope.OrderAttrs.length; j++) {
                            if ($scope.OrderAttrs[j].or_atstatus == 'P') {
                                $scope.OrderAttrs[j].or_atamount = 0;
                            }
                        }
                    }
                    else {
                        for (var j1 = 0; j1 < $scope.OrderAttrs.length; j1++) {
                            if ($scope.OrderAttrs[j1].or_atstatus == 'P') {
                                if ($scope.OrderAttrs[j1].or_atamount != 0 && $scope.OrderAttrs[j1].or_atamount != '' && $scope.OrderAttrs[j1].or_atamount != undefined) {

                                }
                                else {
                                    $scope.flag_iforderdiscount = "false";
                                }
                            }
                        }
                    }
                }

                var totalamt = 0;
                var totalgrossamt = 0;
                var totalnetamt = 0;

                var totalamt1 = 0;
                var totalamt_gross1 = 0;

                for (var i = 0; i < $scope.testALL.length; i++) {

                    //if ($scope.testALL[i].invs058_rd_line_no1 == true) {
                    if ($scope.temp.servicevalue == 'I' || $scope.temp.servicevalue == 'S') {

                        if ($scope.testALL[i].invs058_ordered_quantity != undefined && $scope.testALL[i].invs058_ordered_quantity != '') {
                            if ($scope.testALL[i].invs058_sup_price != undefined && $scope.testALL[i].invs058_sup_price != '') {

                                totalamt1 = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                                $scope.testALL[i].invs058_gross_amt = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;

                                totalamt_gross1 = $scope.testALL[i].invs058_gross_amt;

                                if ($scope.flag_iforderdiscount == "false") {
                                    if ($scope.testALL[i].invs058_discount != undefined && $scope.testALL[i].invs058_discount != '') {
                                        var disamt = ((totalamt1 * $scope.testALL[i].invs058_discount) / 100);
                                        $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - disamt;
                                        totalamt1 = totalamt1 - disamt;
                                    }
                                    if ($scope.testALL[i].invs058_discount_amt != undefined && $scope.testALL[i].invs058_discount_amt != '') {
                                        var disamt = $scope.testALL[i].invs058_discount_amt;
                                        $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - $scope.testALL[i].invs058_discount_amt;
                                        totalamt1 = totalamt1 - disamt;
                                    }
                                }
                                else {
                                    $scope.testALL[i].invs058_discount = '';
                                    $scope.testALL[i].invs058_discount_amt = '';
                                }
                                totalnetamt = totalnetamt + totalamt1;
                                totalgrossamt = totalgrossamt + totalamt_gross1;
                            }
                            else {
                                $scope.testALL[i].invs058_rqvalue = 0;
                                $scope.testALL[i].invs058_gross_amt = 0;
                            }
                        }
                    }

                    totalamt = totalamt + totalamt1;
                    $scope.total_netamt = totalnetamt;
                    // totalamt = totalnetamt;
                    $scope.total_grossamt = totalgrossamt;
                    totalamt1 = 0;
                    totalamt_gross1 = 0;
                }

                debugger;
                if ($scope.temp['invs058_order_apply_aexp'] == true) {//if checkbox is clicked

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var add = 0;

                            totalamt += (parseFloat(add));
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'P') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - ((parseFloat(totalamt) * parseFloat(percentage)) / 100);
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'S') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - $scope.OrderAttrs[i].or_atamount;
                        }
                    }
                }

                if ($scope.temp['invs058_order_apply_aexp'] == false) {//if checkbox is not clicked

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'P') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - ((parseFloat(totalamt) * parseFloat(percentage)) / 100);
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'S') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - $scope.OrderAttrs[i].or_atamount;
                        }


                        //if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                        //    if ($scope.OrderAttrs[i].or_atamount != '')
                        //        var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                        //    else
                        //        var add = 0;

                        //    totalamt += (parseFloat(add));
                        //}

                    }

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var add = 0;

                            totalamt += (parseFloat(add));
                        }
                    }
                }


                $scope.totAmt = totalamt;


            }

            $scope.orcalculateamt12 = function () {
                debugger;
                var totalamt = 0;
                var totalgrossamt = 0;
                var totalnetamt = 0;

                var totalamt1 = 0;
                var totalamt_gross1 = 0;
                $scope.total_grossamt = '';
                $scope.total_netamt = '';

                for (var i = 0; i < $scope.testALL.length; i++) {
                    //if ($scope.testALL[i].invs058_rd_line_no1 == true) {
                    if ($scope.temp.servicevalue == 'I' || $scope.temp.servicevalue == 'S') {
                        if ($scope.testALL[i].invs058_ordered_quantity != undefined && $scope.testALL[i].invs058_ordered_quantity != '') {
                            if ($scope.testALL[i].invs058_sup_price != undefined && $scope.testALL[i].invs058_sup_price != '') {

                                totalamt1 = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);
                                $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;
                                $scope.testALL[i].invs058_gross_amt = $scope.testALL[i].invs058_ordered_quantity * $scope.testALL[i].invs058_sup_price;

                                totalamt_gross1 = $scope.testALL[i].invs058_gross_amt;

                                if ($scope.flag_iforderdiscount == "false") {
                                    if ($scope.testALL[i].invs058_discount != undefined && $scope.testALL[i].invs058_discount != '') {
                                        var disamt = ((totalamt1 * $scope.testALL[i].invs058_discount) / 100);
                                        $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - disamt;
                                        totalamt1 = totalamt1 - disamt;
                                    }
                                    if ($scope.testALL[i].invs058_discount_amt != undefined && $scope.testALL[i].invs058_discount_amt != '') {
                                        var disamt = $scope.testALL[i].invs058_discount_amt;
                                        $scope.testALL[i].invs058_rqvalue = $scope.testALL[i].invs058_rqvalue - $scope.testALL[i].invs058_discount_amt;
                                        totalamt1 = totalamt1 - disamt;
                                    }
                                }
                                else {
                                    $scope.testALL[i].invs058_discount = '';
                                    $scope.testALL[i].invs058_discount_amt = '';
                                }

                                totalnetamt = totalnetamt + totalamt1;
                                totalgrossamt = totalgrossamt + totalamt_gross1;
                            }
                            else {
                                $scope.testALL[i].invs058_rqvalue = 0;
                                $scope.testALL[i].invs058_gross_amt = 0;
                            }
                        }
                    }

                    totalamt = totalamt + totalamt1;
                    $scope.total_netamt = totalnetamt;
                    $scope.total_grossamt = totalgrossamt;
                    totalamt1 = 0;
                    totalamt_gross1 = 0;
                    // }
                }



                debugger;
                if ($scope.temp['invs058_order_apply_aexp'] == true) {//if checkbox is clicked

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var add = 0;

                            totalamt += (parseFloat(add));
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'P') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - ((parseFloat(totalamt) * parseFloat(percentage)) / 100);
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'S') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - $scope.OrderAttrs[i].or_atamount;
                        }
                    }
                }

                if ($scope.temp['invs058_order_apply_aexp'] == false) {//if checkbox is not clicked

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'P') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - ((parseFloat(totalamt) * parseFloat(percentage)) / 100);
                        }

                        if ($scope.OrderAttrs[i].or_atstatus == 'S') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var percentage = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var percentage = 0;

                            if (parseInt(percentage) != 0)
                                totalamt = totalamt - $scope.OrderAttrs[i].or_atamount;
                        }


                        //if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                        //    if ($scope.OrderAttrs[i].or_atamount != '')
                        //        var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                        //    else
                        //        var add = 0;

                        //    totalamt += (parseFloat(add));
                        //}

                    }

                    for (var i = 0; i < $scope.OrderAttrs.length; i++) {

                        if ($scope.OrderAttrs[i].or_atstatus == 'A') {
                            if ($scope.OrderAttrs[i].or_atamount != '')
                                var add = angular.copy($scope.OrderAttrs[i].or_atamount);
                            else
                                var add = 0;

                            totalamt += (parseFloat(add));
                        }
                    }
                }


                $scope.totAmt = totalamt;

            }

            $scope.orcalculateamt_old = function (str) {
                debugger;
                console.log(str);
                $scope.totAmt = 0;
                var totalamt = 0;
                var totalamt1 = 0;
                for (var i = 0; i < $scope.testALL.length; i++) {
                    // if ($scope.testALL[i].invs058_rd_line_no1 == true) {
                    if ($scope.temp.servicevalue == 'I' || $scope.temp.servicevalue == 'S') {
                        if ($scope.testALL[i].invs058_ordered_quantity != undefined) {
                            if ($scope.testALL[i].invs058_sup_price != undefined) {

                                totalamt1 = parseFloat($scope.testALL[i].invs058_ordered_quantity) * parseFloat($scope.testALL[i].invs058_sup_price);

                                if ($scope.testALL[i].invs058_discount != undefined) {
                                    var disamt = ((totalamt1 * $scope.testALL[i].invs058_discount) / 100);
                                    totalamt1 = totalamt1 - disamt;
                                }
                            }
                        }
                    }

                    totalamt = totalamt + totalamt1;
                    totalamt1 = 0;
                    //}
                }

                if (totalamt != "NaN") {
                    $scope.totAmt = parseFloat(totalamt);
                }
                else { $scope.totAmt = 0; }
                console.log(str.or_atamount);
                var o_amt;
                if (str.or_atamount != '' && str.or_atamount != undefined) {
                    o_amt = str.or_atamount;
                    //Percentage
                    if (str.or_atstatus == 'P') {
                        if (str.or_atamount != '') {
                            var disamt = ((parseFloat($scope.totAmt) * parseFloat(str.or_atamount)) / 100);
                            var totalamount = $scope.totAmt - disamt;

                            if (totalamount != "NaN") {
                                $scope.totAmt = parseFloat(totalamount);
                            }
                        }
                        str.or_atamount = o_amt;
                    }

                    //Amount
                    if (str.or_atstatus == 'A') {
                        if (str.or_atamount != undefined) {
                            o_amt = str.or_atamount;
                            var totalamount = parseFloat($scope.totAmt) + parseFloat(str.or_atamount);
                            if (totalamount != "NaN") {
                                $scope.totAmt = parseFloat(totalamount);
                            }
                            str.or_atamount = o_amt;
                        }
                    }
                }
                else
                    str.or_atamount = 0;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $scope.CheckAllService = function () {
                var main1 = document.getElementById('mainchk1');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if (main1.checked == true) {
                        //var chkitem = document.getElementById('test-'+i);
                        //chkitem.checked = true;

                        $scope.filteredTodos[i]['req_no1'] = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                    else {
                        //var chkitem = document.getElementById('test-'+i);
                        //chkitem.checked = false;
                        main1.checked = false;
                        $scope.filteredTodos[i]['req_no1'] = false;;

                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }
            }

            $scope.checkonebyoneselectServices = function () {

                $("input[type='checkbox']").change(function (e) {

                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main1 = document.getElementById('mainchk1');
                if (main1.checked == true) {
                    main1.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }])

})();
