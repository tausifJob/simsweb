﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('GRN',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.IP = {};
            $scope.sel = {};
            $scope.rows = [];
            $scope.ex = {};
            $scope.show = function () {
                if ($scope.IP['sup_code']) {
                    $http.get(ENV.apiUrl + "api/AdjustmentReason/getAdjDetails?data=" + JSON.stringify($scope.IP)).then(function (res) {
                        $scope.rows = res.data.table;
                        $scope.Allrows = res.data.table1;
                        if ($scope.Allrows.length <= 0) {
                            swal({ title: "Alert", text: "Records not found", showCloseButton: true, width: 380, });
                        }
                        else {
                            for (var r in $scope.rows) {
                                $scope.rows[r]['sims_icon'] = "fa fa-plus-circle";
                                $scope.rows[r]['isexpanded'] = "none";
                                $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].adj_doc_no);
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Supplier", showCloseButton: true, width: 380, });
                }
            }

            function getSubitems(dno) {
                var arr = [];
                for (var i = 0; i < $scope.Allrows.length; i++) {
                    if ($scope.Allrows[i].adj_doc_no == dno) {
                        arr.push($scope.Allrows[i]);
                    }
                }
                return arr;
            }

            $scope.expand = function (j, $event) {
                if (j.isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.isexpanded = "grid";
                    console.log(j);
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.isexpanded = "none";
                }
            }

            $scope.reset = function () {
                $scope.IP = {};
                $scope.rows = {};
                $scope.Allrows = {};
            }

            $scope.checkRange = function (s) {
                var rq = parseInt(s.remqty);
                if (rq == NaN)
                    rq = 0;
                s.remqty = rq;
            }

            $http.get(ENV.apiUrl + "api/AdjustmentReason/getDept").then(function (res) {
                $scope.sel['Dept'] = res.data.table;
            });

            $http.get(ENV.apiUrl + "api/AdjustmentReason/getDSSup").then(function (res) {
                $scope.sel['SUP'] = res.data.table;
            });

            $scope.save = function () {
                $('#loader').modal({ backdrop: 'static', keyboard: false });
                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {
                    if ($scope.rows[i].isChecked) {
                        for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                            var ob = {};
                            ob.doc_no = $scope.rows[i].subItems[j].adj_doc_no;
                            ob.doc_line_no = $scope.rows[i].subItems[j].ad_line_no;
                            ob.item_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.r_qty = $scope.rows[i].subItems[j].remqty;
                            ob.grv_no = $scope.rows[i].subItems[j].adj_grv_no;
                            ob.t_qty = $scope.rows[i].subItems[j].ad_qty;
                            ob.adj_status = $scope.rows[i].subItems[j].adj_status;
                            ob.r_Type = $scope.rows[i].subItems[j]['r_Type'];
                            ob.req_no = $scope.rows[i].subItems[j]['req_no'];
                            ob.rd_line_no = $scope.rows[i].subItems[j]['rd_line_no'];
                            ob.remark = $scope.rows[i].subItems[j]['remarks'];
                            ob.grn_inv_no = $scope.ex.inv_no;
                            ob.grn_inv_date = $scope.ex.inv_date;
                            ob.ucode = $rootScope.globals.currentUser.username;
                            ob.typ = 's';
                            ob.val = r;
                            ar.push(ob);
                        }
                    }
                }
                if (ar.length == 0) {
                    swal({ title: "Alert", text: 'Nothing Selected', showCloseButton: true, width: 380, });
                    $('#loader').modal('hide');
                    return;
                }

                $http.post(ENV.apiUrl + "api/AdjustmentReason/saveAdjDetails", ar).then(function (res) {
                    console.log(res);
                    $scope.show();
                    $('#loader').modal('hide');
                    swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });
                });

            }

            $scope.Close = function () {
                $('#loader').modal({ backdrop: 'static', keyboard: false });
                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {
                    if ($scope.rows[i].isChecked) {
                        for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                            var ob = {};
                            ob.doc_no = $scope.rows[i].subItems[j].adj_doc_no;
                            ob.doc_line_no = $scope.rows[i].subItems[j].ad_line_no;
                            ob.item_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.r_qty = $scope.rows[i].subItems[j].remqty;
                            ob.grv_no = $scope.rows[i].subItems[j].adj_grv_no;
                            ob.t_qty = $scope.rows[i].subItems[j].ad_qty;
                            ob.adj_status = $scope.rows[i].subItems[j].adj_status;
                            ob.r_Type = $scope.rows[i].subItems[j]['r_Type'];
                            ob.req_no = $scope.rows[i].subItems[j]['req_no'];
                            ob.rd_line_no = $scope.rows[i].subItems[j]['rd_line_no'];
                            ob.remark = $scope.rows[i].subItems[j]['remarks'];
                            ob.grn_inv_no = $scope.ex.inv_no;
                            ob.grn_inv_date = $scope.ex.inv_date;
                            ob.ucode = $rootScope.globals.currentUser.username;
                            ob.typ = 'C';
                            ob.val = r;
                            ar.push(ob);
                        }
                    }
                }
                if (ar.length == 0) {
                    swal({ title: "Alert", text: 'Nothing Selected', showCloseButton: true, width: 380, });
                    $('#loader').modal('hide');
                    return;
                }

                $http.post(ENV.apiUrl + "api/AdjustmentReason/saveAdjDetails", ar).then(function (res) {
                    console.log(res);
                    $scope.show();
                    $('#loader').modal('hide');
                    swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });
                });
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
        }])
})();