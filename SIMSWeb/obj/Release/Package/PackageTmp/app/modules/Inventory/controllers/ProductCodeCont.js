﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ProductCodeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/ProductCode/getProductCode").then(function (res1) {
                $scope.getProduct = res1.data;
                $scope.totalItems = $scope.getProduct.length;
                $scope.todos = $scope.getProduct;
                $scope.makeTodos();
            });


            $http.get(ENV.apiUrl + "api/InvsParameter/GetCompanyName").then(function (docstatus1) {
                $scope.Compname = docstatus1.data;
                console.log($scope.Compname);
            });

            $http.get(ENV.apiUrl + "api/InvsParameter/GetApplicationCode").then(function (docstatus1) {
                $scope.app_code = docstatus1.data;
                console.log($scope.app_code);
            });


            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.getProduct, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.getProduct;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.search1 = function () {

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.pc_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.pc_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {
                $scope.disabled = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.divcode_readonly = false;
                $scope.temp = "";
                $scope.temp = {};
               
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.table = true;
                $scope.display = false;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp.pc_desc = "";
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = true;
                $scope.display = false;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.divcode_readonly = false;
                // $scope.temp = str;
                $scope.temp = {
                    pc_code: str.pc_code,
                    pc_desc: str.pc_desc,
                };
            }


            //MAin table insert
            var data1 = [];
            $scope.save = function (Myform) {
                debugger;
                data1 = [];
                if (Myform) {
                    data1 = [];
                    var data = $scope.temp;
                    data.opr = 'I';
                    debugger;
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/ProductCode/CUDProductCode", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            $scope.temp.pc_desc = '';
                        }
                        else {
                            swal({ title: "Alert", text: "Record not Inserted", width: 300, height: 200 });
                        }
                        $http.get(ENV.apiUrl + "api/ProductCode/getProductCode").then(function (res1) {
                            $scope.getProduct = res1.data;
                            $scope.totalItems = $scope.getProduct.length;
                            $scope.todos = $scope.getProduct;
                            $scope.makeTodos();
                        });
                    });
                    $scope.table = true;
                    $scope.display = false;

                }

            }

            $scope.update = function () {
                debugger
                data1 = [];
                var data = {
                    'pc_code': $scope.temp.pc_code,
                    'pc_desc': $scope.temp.pc_desc,
                }
                data.opr = 'U';
                data1.push(data);
                $http.post(ENV.apiUrl + "api/ProductCode/CUDProductCode", data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Record Updates Successfully", width: 300, height: 200 });
                    }
                    else {
                        swal({ title: "Alert", text: "Not Updated present", width: 300, height: 200 });
                    }
                    $http.get(ENV.apiUrl + "api/ProductCode/getProductCode").then(function (res1) {
                        $scope.getProduct = res1.data;
                        $scope.totalItems = $scope.getProduct.length;
                        $scope.todos = $scope.getProduct;
                        $scope.makeTodos();
                    });

                });
                $scope.table = true;
                $scope.display = false;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp["pc_desc"] = '';

            }


            // Data DELETE RECORD

            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].pc_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].pc_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.OkDelete = function () {
                debugger;
                deletefin = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].pc_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'pc_code': $scope.filteredTodos[i].pc_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/ProductCode/CUDProductCode", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/ProductCode/getProductCode").then(function (res1) {
                                                $scope.getProduct = res1.data;
                                                $scope.totalItems = $scope.getProduct.length;
                                                $scope.todos = $scope.getProduct;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/ProductCode/getProductCode").then(function (res1) {
                                                $scope.getProduct = res1.data;
                                                $scope.totalItems = $scope.getProduct.length;
                                                $scope.todos = $scope.getProduct;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].pc_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $scope.catagories = function () {
                $http.get(ENV.apiUrl + "api/ProductCode/GetCategoryNames").then(function (GetCategoryNames_Data) {
                    $scope.Category_Data = GetCategoryNames_Data.data;
                    console.log($scope.Category_Data);
                });
            }


            //******************************MODEL POPUP****************************************************************


          
            $scope.AddSubcatagory = function () {
                $('#MyModal').modal('show');
                $scope.display = true;
            }
            $scope.txt_cat = true;

            // $scope.Modaltable = true;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size1 = function (str1) {
                console.log(str1);
                $scope.pagesize1 = str1;
                $scope.currentPage1 = 1;
                $scope.numPerPage1 = str1; console.log("numPerPage1=" + $scope.numPerPage1); $scope.makeTodos1();
            }
            $scope.index1 = function (str1) {
                $scope.pageindex1 = str1;
                $scope.currentPage1 = str1; console.log("currentPage1=" + $scope.currentPage1); $scope.makeTodos1();
            }

            $scope.filteredTodos1 = [], $scope.currentPage1 = 1, $scope.numPerPage1 = 5, $scope.maxSize1 = 5;

            $scope.makeTodos1 = function () {
                var rem1 = parseInt($scope.totalItems1 % $scope.numPerPage1);
                if (rem1 == '0') {
                    $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1);
                }
                else {
                    $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1) + 1;
                }
                var begin1 = (($scope.currentPage1 - 1) * $scope.numPerPage1);
                var end1 = parseInt(begin1) + parseInt($scope.numPerPage1);

                console.log("begin1=" + begin1); console.log("end1=" + end1);

                $scope.filteredTodos1 = $scope.todos1.slice(begin1, end1);
            };
            $scope.modaloperation = false;
            $scope.Modaltable = true;


            $http.get(ENV.apiUrl + "api/ProductCode/getSubcategoryData").then(function (subcode) {
                $scope.AccountCode_Data = subcode.data;
                console.log($scope.AccountCode_Data);
            });

            $http.get(ENV.apiUrl + "api/ProductCode/GetCategoryNames").then(function (GetCategoryNames_Data) {
                $scope.Category_Data = GetCategoryNames_Data.data;
                console.log($scope.Category_Data);
            });

           
            $scope.CheckAllChecked1 = function () {
                debugger;
                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                        var v = document.getElementById("-test" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                        var v = document.getElementById("-test" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }
            $scope.New1 = function () {
                $scope.temp = '';
                opr = 'S';
                $scope.txt_cat = false;
                $scope.savebtn1 = true;
                $scope.updatebtn1 = false;
                $scope.modaloperation = true;
                $scope.Modaltable = false;
                $scope.tes_ing = false;
                $scope.combocat = true;
                $scope.catagories();

            }

            //Editcode
            $scope.up1 = function (str1) {
                debugger;
                $scope.modaloperation = true;
                $scope.Modaltable = false;
                // opr = 'U';
                $scope.savebtn1 = false;
                $scope.updatebtn1 = true;
                $scope.combocat = false;
                $scope.readonly = true;
                $scope.tes_ing = true;
                $scope.tester = true;
                $scope.txt_cat = true;

                $scope.edt =
                {
                    pc_code: str1.pc_code,
                    pc_desc: str1.catname,
                    pc_parent_code: str1.pc_parent_code,
                    subcatname: str1.subcatname
                }
            }


            $scope.cancel1 = function () {
                $scope.modaloperation = false;
                $scope.Modaltable = true;
                $scope.temp = "";
                $scope.myFormmodal.$setPristine();
            }


            ///Save Insert data for subCatagory
            var data1 = [];
            $scope.Save1 = function (myFormmodal) {
                debugger
                data1 = [];
                if (myFormmodal) {
                    data1 = [];
                    for (var i = 0; i < $scope.AccountCode_Data.length; i++) {
                        var pc_parent_code = $scope.AccountCode_Data[0].pc_parent_code;
                    }
                    var data =
                        {
                            'pc_desc': $scope.edt.subcatname,
                            'pc_parent_code': $scope.edt.pc_code,
                            'pc_code': $scope.temp.pc_code
                        }
                    // var data = $scope.temp;
                    data.opr = 'J';
                    debugger;
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/ProductCode/CUDProductCode", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record not Inserted", width: 300, height: 200 });
                        }
                        $http.get(ENV.apiUrl + "api/ProductCode/getSubcategoryData").then(function (subcode) {
                            $scope.AccountCode_Data = subcode.data;
                            console.log($scope.AccountCode_Data);
                        });
                    });
                    $scope.Modaltable = true;
                    $scope.modaloperation = false;
                    $scope.pc_desc = "";
                }

                $scope.edt["pc_code"] = '';
                $scope.edt["subcatname"] = '';

            }


            $scope.Update1 = function () {
                data1 = [];
                debugger
                for (var i = 0; i < $scope.AccountCode_Data.length; i++) {
                    var pc_parent_code = $scope.AccountCode_Data[i].pc_parent_code;
                }

                var data = {
                    'pc_code': $scope.edt.pc_code,
                    'pc_desc': $scope.edt.subcatname,
                    'pc_parent_code': pc_parent_code,
                }
                data.opr = 'U';
                data1.push(data);
                $http.post(ENV.apiUrl + "api/ProductCode/CUDProductCode", data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Record Updates Successfully", width: 300, height: 200 });
                    }
                    else {
                        swal({ title: "Alert", text: "Not Updated present", width: 300, height: 200 });
                    }
                    $http.get(ENV.apiUrl + "api/ProductCode/getSubcategoryData").then(function (subcode) {
                        $scope.AccountCode_Data = subcode.data;
                        console.log($scope.AccountCode_Data);
                    });

                });
                $scope.Modaltable = true;
                $scope.modaloperation = false;

            }


            $scope.deleterecord1 = function () {
                debugger;
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.AccountCode_Data.length; i++) {
                    var v = document.getElementById($scope.AccountCode_Data[i].pc_code + $scope.AccountCode_Data[i].pc_parent_code);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'pc_code': $scope.AccountCode_Data[i].pc_code,
                            opr: 'D'

                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/ProductCode/CUDProductCode", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/ProductCode/getSubcategoryData").then(function (subcode) {
                                                $scope.AccountCode_Data = subcode.data;
                                                console.log($scope.AccountCode_Data);
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record is Mapped ,Not Deleted ", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/ProductCode/getSubcategoryData").then(function (subcode) {
                                                $scope.AccountCode_Data = subcode.data;
                                                console.log($scope.AccountCode_Data);
                                            });
                                        }
                                    });
                                }

                            });
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }

            }

            //$scope.deleterecord1 = function () {
            //    debugger;
            //    deletefin = [];
            //    $scope.flag = false;

            //    for (var i = 0; i < $scope.AccountCode_Data.length; i++) {
            //        var v = document.getElementById($scope.AccountCode_Data[i].pc_code + $scope.AccountCode_Data[i].pc_parent_code);
            //        if (v.checked == true) {
            //            $scope.flag = true;
            //            var deletemodulecode = ({
            //                'pc_code': $scope.AccountCode_Data[i].pc_code,
            //                opr: 'D'
            //            });
            //            deletefin.push(deletemodulecode);
            //        }
            //    }
            //    if ($scope.flag) {
            //        swal({
            //            title: '',
            //            text: "Are you sure you want to Delete?",
            //            showCloseButton: true,
            //            showCancelButton: true,
            //            confirmButtonText: 'Yes',
            //            width: 380,
            //            cancelButtonText: 'No',

            //        }).then(function (isConfirm) {
            //            if (isConfirm) {
            //                $http.post(ENV.apiUrl + "api/ProductCode/CUDProductCode", deletefin).then(function (msg) {
            //                    $scope.msg1 = msg.data;
            //                    if ($scope.msg1 == true) {
            //                        swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
            //                            if (isConfirm) {
            //                                $http.get(ENV.apiUrl + "api/ProductCode/getSubcategoryData").then(function (subcode) {
            //                                    $scope.AccountCode_Data = subcode.data;
            //                                    console.log($scope.AccountCode_Data);
            //                                });
            //                            }
            //                        });
            //                    }
            //                    else {
            //                        swal({ title: "Alert", text: "Record is Mapped ,Not Deleted ", showCloseButton: true, width: 380, }).then(function (isConfirm) {
            //                            if (isConfirm) {
            //                                $http.get(ENV.apiUrl + "api/ProductCode/getSubcategoryData").then(function (subcode) {
            //                                    $scope.AccountCode_Data = subcode.data;
            //                                    console.log($scope.AccountCode_Data);
            //                                });
            //                            }
            //                        });
            //                    }

            //                });
            //            }
            //        });
            //    }
            //    else {
            //        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
            //    }

            //}
            //*************************************************************************************************************

        }])

})();
