﻿(function () {
    'use strict';
    var del = [];
    var main;
    var data = [];
    var data1 = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SalesmanCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.salesman_code = true;
            $scope.salesman_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.edt = [];
            $scope.edit_code = false;
            $scope.enroll_number_lists = [];
            $scope.test_emp = [];
            $scope.grid = true;
            $scope.display = false;

            $rootScope.visible_stud = false;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = true;
            $rootScope.chkMulti = false;

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);
            //teacher_id
            //em_number
            $scope.$on('global_cancel', function () {
                debugger;

                if ($scope.SelectedUserLst.length > 0) {

                    debugger
                    $scope.temp =
                        {
                           // sm_name: $scope.SelectedUserLst[0].teacher_Name,
                            sm_name: $scope.SelectedUserLst[0].empName,
                            em_number: $scope.SelectedUserLst[0].em_number
                        }

                }
                //$scope.SelectedUserLst = [];
            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $http.get(ENV.apiUrl + "api/Salesman/Get_Compny_name").then(function (res) {
                $scope.comp_data = res.data;
                console.log($scope.comp_data);
            });

            $scope.Getdeptinfo = function (comcode) {
                $http.get(ENV.apiUrl + "api/Salesman/Get_Dept_name?comp_code=" + comcode).then(function (res) {
                    $scope.dept_data = res.data;
                    console.log($scope.dept_data);
                });
            }

            $http.get(ENV.apiUrl + "api/Salesman/Get_Salesman_AllDetails").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.salesman_data = res.data;
                $scope.totalItems = $scope.salesman_data.length;
                $scope.todos = $scope.salesman_data;
                $scope.makeTodos();
                $scope.grid = true;

                for (var i = 0; i < $scope.totalItems; i++) {
                    $scope.salesman_data[i].icon = "fa fa-plus-circle";
                }
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edit_code = true;

                $scope.edt =
                    {
                        com_code: str.com_code,
                        sm_code: str.sm_code,
                        dep_code: str.dep_code,
                        sm_password: str.sm_password,
                        sm_min_comm: str.sm_min_comm,
                        sm_max_comm: str.sm_max_comm,
                        sm_max_discount: str.sm_max_discount,
                        sm_target_level_1: str.sm_target_level_1,
                        sm_pct_1_comm: str.sm_pct_1_comm,
                        sm_target_level_2: str.sm_target_level_2,
                        sm_pct_2_comm: str.sm_pct_2_comm,
                        sm_target_level_3: str.sm_target_level_3,
                        sm_max_disc_pass: str.sm_max_disc_pass,
                        sm_status: str.sm_status,
                        username: str.username,
                        sm_name: str.sm_name
                    }



                $scope.Getdeptinfo(str.com_code);


            }

            $scope.Save = function (myForm) {
                if (myForm) {
                    data = [];
                    data1 = [];
                    var data = ({
                        sm_code: $scope.edt.sm_code,
                        dep_code: $scope.edt.dep_code,
                        sm_password: $scope.edt.sm_password,
                        sm_min_comm: $scope.edt.sm_min_comm,
                        sm_max_comm: $scope.edt.sm_max_comm,
                        sm_max_discount: $scope.edt.sm_max_discount,
                        sm_target_level_1: $scope.edt.sm_target_level_1,
                        sm_pct_1_comm: $scope.edt.sm_pct_1_comm,
                        sm_target_level_2: $scope.edt.sm_target_level_2,
                        sm_pct_2_comm: $scope.edt.sm_pct_2_comm,
                        sm_target_level_3: $scope.edt.sm_target_level_3,
                        sm_max_disc_pass: $scope.edt.sm_max_disc_pass,
                        sm_status: $scope.edt.sm_status,
                        sm_name: $scope.temp.sm_name,
                        username: $scope.temp.em_number,
                        
                        opr: 'I'
                    });
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/Salesman/CUDSalesmanDetails", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                        }

                        $scope.getgrid();
                    });
                    $scope.grid = true;
                    $scope.display = false;
                }

            }

            $scope.getgrid = function () {
                $scope.currentPage = 1;

                $http.get(ENV.apiUrl + "api/Salesman/Get_Salesman_AllDetails").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.salesman_data = res.data;
                    $scope.totalItems = $scope.salesman_data.length;
                    $scope.todos = $scope.salesman_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                    for (var i = 0; i < $scope.totalItems; i++) {
                        $scope.salesman_data[i].icon = "fa fa-plus-circle";
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.getEmployee = function () {
                $scope.global_Search_click();

                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }

            $scope.Search_by_employee = function (comn_user_name, EmpName) {
                debugger
                $scope.NodataEmployee = false;
                var data = $scope.temp;
                $http.get(ENV.apiUrl + "api/TransRouteStudDetails/getSearchEmployee?data=" + JSON.stringify(data)).then(function (getSearchEmployee_data) {
                    $scope.EmployeeDetails = getSearchEmployee_data.data;
                    console.log($scope.EmployeeDetails);
                    $scope.EmployeeTable = true;
                    if (getSearchEmployee_data.data.length > 0) {
                        //   $scope.EmployeeTable = true;

                    }
                    else {
                        $scope.NodataEmployee = true;
                        //  $scope.EmployeeTable = false;
                    }
                });

            }

            $scope.Empmodal_cancel = function () {

                $scope.glbl_obj = '';
                $scope.global_Search = '';
                $scope.global_search_result = '';

                for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                    if ($scope.EmployeeDetails[i].comn_user_name == true) {
                        $scope.enroll_number_lists.push($scope.EmployeeDetails[i]);
                        $scope.test_emp = $scope.EmployeeDetails[i];
                        console.log($scope.enroll_number_lists);
                        $scope.edt['sm_name'] = $scope.test_emp.empName;
                    }
                }
                $scope.EmployeeDetails = [];
            }

            
            $scope.Update = function (myForm) {
                debugger
                if (myForm) {
                    data = [];
                    data1 = [];
                    var data = ({
                        sm_code: $scope.edt.sm_code,
                        dep_code: $scope.edt.dep_code,
                        sm_password: $scope.edt.sm_password,
                        sm_min_comm: $scope.edt.sm_min_comm,
                        sm_max_comm: $scope.edt.sm_max_comm,
                        sm_max_discount: $scope.edt.sm_max_discount,
                        sm_target_level_1: $scope.edt.sm_target_level_1,
                        sm_pct_1_comm: $scope.edt.sm_pct_1_comm,
                        sm_target_level_2: $scope.edt.sm_target_level_2,
                        sm_pct_2_comm: $scope.edt.sm_pct_2_comm,
                        sm_target_level_3: $scope.edt.sm_target_level_3,
                        sm_max_disc_pass: $scope.edt.sm_max_disc_pass,
                        sm_status: $scope.edt.sm_status,
                        sm_name: $scope.temp.sm_name,
                        username: $scope.temp.em_number,

                        opr: 'U'
                    });
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/Salesman/CUDSalesmanDetails", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                        $scope.getgrid();
                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                }

            }

            $scope.Delete = function () {
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sm_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sm_code': $scope.filteredTodos[i].sm_code,
                            'dep_code': $scope.filteredTodos[i].dep_code,
                            'sm_password': $scope.filteredTodos[i].sm_password,
                            'sm_min_comm': $scope.filteredTodos[i].sm_min_comm,
                            'sm_max_comm': $scope.filteredTodos[i].sm_max_comm,
                            'sm_max_discount': $scope.filteredTodos[i].sm_max_discount,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/Salesman/CUDSalesmanDetails", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Salesman Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Salesman Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sm_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }

            }

            $scope.New = function () {
                $scope.edit_code = false;
                var autoid;
                $scope.grid = false;
                $scope.display = true;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = [];

                $http.get(ENV.apiUrl + "api/Salesman/getAutoGeneratesmCode").then(function (res) {
                    autoid = res.data;
                    $scope.edt['sm_code'] = autoid;
                });
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sm_code+i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sm_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['chk_all'] = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.salesman_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.salesman_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sm_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sm_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }


            var dom;
            var count = 0;
            $scope.flag = true;
            $scope.icon = "fa fa-plus-circle";

            $scope.expand = function (info, $event) {
                console.log(info);
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";

                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                         "<tr> <td class='semi-bold'>" + "TARGET LEVEL 1" + "</td> <td class='semi-bold'>" + "LEVEL 2" + " </td><td class='semi-bold'>" + "LEVEL 3" + "</td><td class='semi-bold'>" + "PCT 1" + "</td><td class='semi-bold'>" + "PCT 2" + "</td> " +
                        "</tr>" +
                          "<tr><td>" + (info.sm_target_level_1) + "</td><td>" + (info.sm_target_level_2) + " </td><td>" + (info.sm_target_level_3) + "</td><td>" + (info.sm_pct_1_comm) + "</td><td>" + (info.sm_pct_2_comm) + "</td>" +
                        "</tr>" +
                        " </table></td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    info.icon = "fa fa-plus-circle";
                    $scope.flag = true;
                }
            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
      
        }])
})();