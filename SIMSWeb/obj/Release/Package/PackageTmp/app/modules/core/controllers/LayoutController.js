﻿(function () {
    'use strict';
    var core = angular.module('sims.module.core');
    core.controller("LayoutController", function ($scope, gettextCatalog) {
        $scope.title = gettextCatalog.getString("Mograsys2.0");
    });
})();