﻿(function () {
    'use strict';
    angular.module('sims.module.core', [
        'sims',
        'gettext'
    ]);
})();