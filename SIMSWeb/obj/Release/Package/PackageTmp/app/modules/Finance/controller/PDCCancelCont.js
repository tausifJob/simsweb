﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PDCCancelCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.tot_btn = true;
            $scope.Maintabledata = false;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.records = false;
            //$scope.cmbstatus = true;
            //$scope.checked = false;


          



            $scope.visible_textbox = function ()
            {
                debugger;
                $scope.records = true;
            }

            //Fill ComboBox Status
            $http.get(ENV.apiUrl + "api/Financialyear/getDocumentStatus").then(function (docstatus) {

                $scope.docstatus_Data = docstatus.data;
                console.log($scope.docstatus);
            });


            //Fill BAnk Name
            $http.get(ENV.apiUrl + "api/PDCSubmission/getBankName").then(function (docstatus1) {
                $scope.bank_name = docstatus1.data;
                console.log($scope.docstatus1);
            });

            //Fill department
            $http.get(ENV.apiUrl + "api/PDCSubmission/getDepartment").then(function (docstatus2) {
                $scope.Department = docstatus2.data;
                console.log($scope.docstatus2);
            }); 


            //Fill Ledger
            $http.get(ENV.apiUrl + "api/PDCSubmission/getLedgerName").then(function (docstatus3) {
                $scope.ledger = docstatus3.data;
                console.log($scope.docstatus3);
            });

            //Account number
            $http.get(ENV.apiUrl + "api/PDCSubmission/GetGLAccountNumber").then(function (docstatus4) {
                $scope.accno = docstatus4.data;
                console.log($scope.docstatus4);
            });


            //Select Data SHOW
            var showdetails = [];
            $scope.getViewDetails= function (dept_no,ldgr_code,slma_acno,from_date,to_date)
            {
                debugger;
                if (dept_no == undefined)
                    dept_no = '';
                if (ldgr_code == undefined)
                    ldgr_code = '';
                if (slma_acno == undefined)
                    slma_acno = '';
                if (from_date == undefined)
                    from_date = '';
                if (to_date == undefined)
                    to_date = '';

                $http.get(ENV.apiUrl + "api/PDCSubmission/getAllPDCSubmissionALL?dept_no=" + dept_no + "&ldgr_code=" + ldgr_code + "&slma_acno=" + slma_acno + "&from_date=" + from_date + "&to_date=" + to_date + "&pc_discd="+'OB').then(function (res1) {
                    $scope.pdc_details = res1.data;
                    $scope.totalItems = $scope.pdc_details.length;
                    $scope.todos = $scope.pdc_details;
                    $scope.makeTodos();

                    $scope.Maintabledata = true;

                    debugger;
                     $scope.total = 0;
                    for (var i = 0; i < res1.data.length; i++) {
                        $scope.total = $scope.total + res1.data[i].ps_amount;
                    }
                    console.log(total)
                });

               
                showdetails = []

                
            }
           

     
            //$http.get(ENV.apiUrl + "api/GoalTarget/getAcademicYear").then(function (Acyear) {
            //    $scope.Acc_year = Acyear.data;
            //    console.log($scope.Acc_year);
            //});
            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.fin_doc, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.fin_doc;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_financial_year.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_financial_year_status.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_doc_srno == toSearch) ? true : false;
            }

            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "/" + month + "/" + day;
            }


            //NEW BUTTON
            $scope.New = function () {

                $scope.disabled = false;
                $scope.gtcreadonly = true;
                $scope.gtdreadonly = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;

                $scope.cmbstatus = true;
                $scope.txtyear = false;

                $scope.temp = "";

            }

            var cntr=0;
            $scope.selectonebyone = function (str)
            {
                debugger;
                var d=document.getElementById(str);
                if (d.checked == true) {
                    cntr = cntr + 1;
                }
                else { cntr = cntr - 1;}
            }

           

            //DATA SAVE INSERT for cheque Transfer
            var datasend = [];
            $scope.savedata = function () {//Myform

               // if (Myform) {
                    debugger;
                  //  var data = $scope.temp;
                    //data.opr = 'I';
                    for (var i = 0; i < $scope.filteredTodos.length; i++)
                    {
                        var e = document.getElementById(i + i);
                        if (e.checked == true)
                        {
                           
                            $scope.filteredTodos[i].ps_ldgr_code_name = $rootScope.globals.currentUser.username;
                            $scope.filteredTodos[i].ps_dept_name = "Cancellation of Cheque";
                            
                            datasend.push($scope.filteredTodos[i]);
                        }
                    }
                    $http.post(ENV.apiUrl + "api/PDCSubmission/Finn102_PDC_Cancellation", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                       //swal({ text: $scope.msg1.strMessage, timer: 5000 });
                        swal({ title: "Alert", text: $scope.msg1.strMessage, imageUrl: "assets/img/check.png", });

                    });
                   // getViewDetails();
                    datasend = [];
            }


           


      
            $scope.chkrevert=function()
            {
                $('#MyModal').modal('show');
                $scope.display = true;

            }

            //Date FORMAT
            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "-" + month + "-" + day;
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
            }

            //DATA EDIT
            $scope.edit = function (str) {
                $scope.gtcreadonly = true;
                $scope.gtdreadonly = true;
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.txtyear = true;
                $scope.cmbstatus = false;
                $scope.temp = str;


            }


            //DATA UPADATE
            var dataupdate = [];
            $scope.update = function () {

                var data = $scope.temp;
                data.opr = "U";

                dataupdate.push(data);

                $http.post(ENV.apiUrl + "api/Financialyear/CUDFinantialYear", data).then(function (msg) {

                    $scope.msg1 = msg.data;
                    swal({ text: $scope.msg1.strMessage, timer: 5000 });

                    $http.get(ENV.apiUrl + "api/Financialyear/getAllFinancialYear").then(function (res1) {
                        $scope.fin_doc = res1.data;
                        $scope.totalItems = $scope.fin_doc.length;
                        $scope.todos = $scope.fin_doc;
                        $scope.makeTodos();
                        $scope.table = true;
                        $scope.display = false;
                    });

                });
                dataupdate = [];

            }


            //DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].sims_financial_year;
                        var v = document.getElementById(t);
                        v.checked = true;
                        finanacecode = finanacecode + $scope.filteredTodos[i].sims_financial_year + ',';
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].sims_financial_year;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
            }


            $scope.OkDelete = function () {
                finanacecode = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_financial_year;
                    var v = document.getElementById(t);

                    if (v.checked == true)
                        finanacecode = finanacecode + $scope.filteredTodos[i].sims_financial_year + ',';

                }
                var deletemodulecode = ({

                    'sims_financial_year': finanacecode,
                    'opr': "D"
                });

                $http.post(ENV.apiUrl + "api/Financialyear/CUDFinantialYear", deletemodulecode).then(function (msg) {
                    $scope.msg1 = msg.data;
                    swal({ text: $scope.msg1.strMessage, timer: 5000 });

                    $http.get(ENV.apiUrl + "api/Financialyear/getAllFinancialYear").then(function (res1) {
                        $scope.fin_doc = res1.data;
                        $scope.totalItems = $scope.fin_doc.length;
                        $scope.todos = $scope.fin_doc;
                        $scope.makeTodos();
                        $scope.table = true;
                        $scope.display = false;
                    });

                });
                deletefin = [];

            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            //Selection Procedure
            $scope.Getdates = function (str) {



                for (var i = 0; i < $scope.docyear_Data1.length; i++) {
                    if ($scope.docyear_Data1[i].sims_academic_year == str) {
                        $scope.temp = {
                            sims_financial_year_start_date: $scope.docyear_Data1[i].sims_financial_year_start_date,
                            sims_financial_year_end_date: $scope.docyear_Data1[i].sims_financial_year_end_date,
                            sims_financial_year: $scope.docyear_Data1[i].sims_academic_year,


                        };
                    }
                }



            }

        }])

})();