﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ViewEditGLPeriodLedgerCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.grid = true;
            $scope.edit_data = false;
            $scope.gl_period_ledgers_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.dept_comp_code;
            $scope.slma_acno_dept_no;
            $scope.finyr;
            $scope.valstatus = false;
            $scope.showlabels = false;
            $scope.showlabels1 = false;

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 12, $scope.maxSize = 12;

            $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
                debugger;
                $scope.comp_data = res.data;
                console.log($scope.comp_data);
                $scope.dept_comp_code = $scope.comp_data[0].dept_comp_code;

                $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetCurrentFinancialYear").then(function (res) {
                    $scope.finyr_data = res.data;
                    $scope.finyr = $scope.finyr_data;
                    document.getElementById('txt_year').value = $scope.finyr;

                    $http.get(ENV.apiUrl + "api/common/ViewEditPeriodLedger/GetAll_Gl_period_ledgers_deptno?comp_code=" + $scope.dept_comp_code + "&fin_yr=" + $scope.finyr).then(function (res) {
                        $scope.Gl_period_ledgers_deptno_data = res.data;
                    });
                });
            });


            setTimeout(function () {
                $("#cmb_accnt_no").select2();
            }, 100);

            $scope.getAccntNo = function (dept_code) {
                if (dept_code != undefined) {
                    $http.get(ENV.apiUrl + "api/common/ViewEditPeriodLedger/Get_account_code?comp_code=" + $scope.dept_comp_code + "&fin_yr=" + $scope.finyr + "&deptno=" + dept_code).then(function (res) {
                        $scope.account_code_data = res.data;
                        console.log($scope.account_code_data);
                    });
                }
            }

            $scope.getGLledgerdata = function (deptno, accnt_no) {
                debugger;
                if ($scope.edt.rev_budget_amount != undefined) {
                    $scope.edt.rev_budget_amount = '';
                }

                if ($scope.edt.rev_budget_quantity != undefined) {
                    $scope.edt.rev_budget_quantity = '';
                }

                if (deptno != undefined && accnt_no != undefined) {
                    $http.get(ENV.apiUrl + "api/common/ViewEditPeriodLedger/GetFins_gl_period_ledgers?comp_code=" + $scope.dept_comp_code + "&fin_yr=" + $scope.finyr + "&deptno=" + deptno + "&accnt_no=" + accnt_no).then(function (res) {
                        $scope.display = false;
                        $scope.grid = true;
                        $scope.gl_period_ledgers_data = res.data;
                        console.log($scope.gl_period_ledgers_data);
                        $scope.totalItems = $scope.gl_period_ledgers_data.length;
                        $scope.todos = $scope.gl_period_ledgers_data;
                        $scope.makeTodos();
                        $scope.grid = true;
                        $scope.showlabels = true;
                    });
                }
            }

            $scope.changebugdet = function (event, K) {
                if (event.key == "Tab" || event.key == "Enter") {
                    $scope.bugdet_allocation(K);
                }
            }

            $scope.changebugdet1 = function (event, b) {
                if (event.key == "Tab" || event.key == "Enter") {
                    $scope.bugdet_allocation(b);
                }
            }

            $scope.bugdet_allocation = function (str) {
                var data1 = [];
                var l = $scope.filteredTodos.length;
                for (var i = 0; i < l; i++) {

                    var data = {
                        glpr_comp_code: $scope.dept_comp_code,
                        glpr_year: $scope.finyr,
                        glpr_dept_no: $scope.edt.glpr_dept_no,
                        glpr_acct_code: $scope.edt.account_code,
                        glpr_prd_no: $scope.filteredTodos[i].glpr_prd_no,
                        glpr_bud_amt: $scope.filteredTodos[i].glpr_rev_bud_amt,
                        glpr_bud_qty: $scope.filteredTodos[i].glpr_rev_bud_qty,
                        glpr_rev_bud_amt: $scope.filteredTodos[i].glpr_rev_bud_amt,
                        glpr_rev_bud_qty: $scope.filteredTodos[i].glpr_rev_bud_qty,
                        opr: 'U'
                    };
                    data1.push(data);
                }

                data1.push(data);

                $http.post(ENV.apiUrl + "api/common/ViewEditPeriodLedger/CUDUpdateFins_gl_period_ledgers", data1).then(function (res) {
                    $scope.gl_period_ledgers = res.data;
                    debugger;
                    if ($scope.gl_period_ledgers == true) {
                        $scope.getGLledgerdata($scope.edt.glpr_dept_no, $scope.edt.account_code);
                    }
                    else {
                        swal({ title: "Alert", text: "Gl Period Ledger Not Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                $scope.getGLledgerdata($scope.edt.glpr_dept_no, $scope.edt.account_code);
                            }
                        });
                    }
                });
            }

            $scope.update = function () {
                debugger;
                var data1 = [];
                var l = $scope.filteredTodos.length;
                for (var i = 0; i < l; i++) {

                    var data = {
                        glpr_comp_code: $scope.dept_comp_code,
                        glpr_year: $scope.finyr,
                        glpr_dept_no: $scope.edt.glpr_dept_no,
                        glpr_acct_code: $scope.edt.account_code,
                        glpr_prd_no: $scope.filteredTodos[i].glpr_prd_no,
                        glpr_bud_amt: $scope.filteredTodos[i].glpr_rev_bud_amt,
                        glpr_bud_qty: $scope.filteredTodos[i].glpr_rev_bud_qty,
                        glpr_rev_bud_amt: $scope.filteredTodos[i].glpr_rev_bud_amt,
                        glpr_rev_bud_qty: $scope.filteredTodos[i].glpr_rev_bud_qty,
                        opr: 'U'
                    };
                    data1.push(data);
                }

                $http.post(ENV.apiUrl + "api/common/ViewEditPeriodLedger/CUDUpdateFins_gl_period_ledgers", data1).then(function (res) {
                    $scope.gl_period_ledgers = res.data;

                    if ($scope.gl_period_ledgers == true) {

                        $scope.getGLledgerdata($scope.edt.glpr_dept_no, $scope.edt.account_code);
                    }
                    else {
                        swal({ title: "Alert", text: "Gl Period Ledger Not Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                $scope.getGLledgerdata($scope.edt.glpr_dept_no, $scope.edt.account_code);
                            }
                        });
                    }
                });

            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.onlyNumbers1 = function (event) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return;
                    }
                }
                event.preventDefault();
            };

            $scope.calval = function () {

                if ($scope.edt.rev_budget_amount != undefined) {
                    if ($scope.edt.rev_budget_amount != '') {
                        var temp_value = parseInt($scope.edt.rev_budget_amount / 12);
                        var rem_amt = parseInt($scope.edt.rev_budget_amount - (temp_value * 12));

                        for (var i = 0; i < 11; i++) {
                            $scope.filteredTodos[i].glpr_rev_bud_amt = temp_value;
                        }
                        if (rem_amt > 0) {
                            $scope.filteredTodos[11].glpr_rev_bud_amt = temp_value + rem_amt;
                        }
                        else {
                            $scope.filteredTodos[11].glpr_rev_bud_amt = temp_value;
                        }
                    }
                }

                if ($scope.edt.rev_budget_quantity != undefined) {
                    if ($scope.edt.rev_budget_quantity != '') {
                        var temp_value1 = parseInt($scope.edt.rev_budget_quantity / 12);
                        var rem_amt1 = parseInt($scope.edt.rev_budget_quantity - (temp_value1 * 12));

                        for (var i = 0; i < 11; i++) {
                            $scope.filteredTodos[i].glpr_rev_bud_qty = temp_value1;
                        }
                        if (rem_amt1 > 0) {
                            $scope.filteredTodos[11].glpr_rev_bud_qty = temp_value1 + rem_amt1;
                        }
                        else {
                            $scope.filteredTodos[11].glpr_rev_bud_qty = temp_value1;
                        }
                    }
                }
            }

            $scope.changebugdetamount = function () {
                $scope.calval();
            }

        }])
})();






//(function () {
//    'use strict';
//    var del = [];
//    var main;
//    var simsController = angular.module('sims.module.Finance');
//    simsController.controller('ViewEditGLPeriodLedgerCont',
//        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

//            $scope.display = false;
//            $scope.pagesize = "5";
//            $scope.pageindex = "1";
//            $scope.grid = true;
//            $scope.edit_data = false;
//            $scope.gl_period_ledgers_data = [];
//            var data1 = [];
//            var deletecode = [];
//            $scope.dept_comp_code;
//            $scope.slma_acno_dept_no;
//            $scope.finyr;
//            $scope.valstatus = false;

//            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 12, $scope.maxSize = 12;

//            $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
//                $scope.comp_data = res.data;
//                console.log($scope.comp_data);
//                $scope.dept_comp_code = $scope.comp_data[0].dept_comp_code;

//                $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetCurrentFinancialYear").then(function (res) {
//                    $scope.finyr_data = res.data;
//                    $scope.finyr = $scope.finyr_data;
//                    document.getElementById('txt_year').value = $scope.finyr;

//                    $http.get(ENV.apiUrl + "api/common/ViewEditPeriodLedger/GetAll_Gl_period_ledgers_deptno?comp_code=" + $scope.dept_comp_code + "&fin_yr=" + $scope.finyr).then(function (res) {
//                        $scope.Gl_period_ledgers_deptno_data = res.data;
//                    });
//                });
//            });

//            $scope.getAccntNo = function (dept_code) {
//                if (dept_code != undefined) {
//                    $http.get(ENV.apiUrl + "api/common/ViewEditPeriodLedger/Get_account_code?comp_code=" + $scope.dept_comp_code + "&fin_yr=" + $scope.finyr + "&deptno=" + dept_code).then(function (res) {
//                        $scope.account_code_data = res.data;
//                        console.log($scope.account_code_data);
//                    });
//                }
//            }

//            $scope.getGLledgerdata = function (deptno, accnt_no) {

//                $http.get(ENV.apiUrl + "api/common/ViewEditPeriodLedger/GetFins_gl_period_ledgers?comp_code=" + $scope.dept_comp_code + "&fin_yr=" + $scope.finyr + "&deptno=" + deptno + "&accnt_no=" + accnt_no).then(function (res) {
//                    $scope.display = false;
//                    $scope.grid = true;
//                    $scope.gl_period_ledgers_data = res.data;
//                    console.log($scope.gl_period_ledgers_data);
//                    $scope.totalItems = $scope.gl_period_ledgers_data.length;
//                    $scope.todos = $scope.gl_period_ledgers_data;
//                    $scope.makeTodos();
//                    $scope.grid = true;
//                });
//            }

//            $scope.get_ischecked = function (mod) {
//                mod.ischecked = true;
//            }

//            $scope.update = function (str) {
//                var data1 = [];
//                //$scope.temp = str;
//                //for (var i = 0; i < $scope.filteredTodos.length; i++)
//                //{
//                //if ($scope.filteredTodos[i].ischecked == true)
//                //{
//                var data = {
//                    glpr_comp_code: $scope.dept_comp_code,
//                    glpr_year: $scope.finyr,
//                    glpr_dept_no: $scope.edt.glpr_dept_no,
//                    glpr_acct_code: $scope.edt.account_code,
//                    glpr_prd_no: str.glpr_prd_no,
//                    glpr_bud_amt: str.glpr_rev_bud_amt,
//                    glpr_bud_qty: str.glpr_rev_bud_qty,
//                    glpr_rev_bud_amt: str.glpr_rev_bud_amt,
//                    glpr_rev_bud_qty: str.glpr_rev_bud_qty,
//                    opr: 'U'
//                };

//                data1.push(data);

//                $http.post(ENV.apiUrl + "api/common/ViewEditPeriodLedger/CUDUpdateFins_gl_period_ledgers", data1).then(function (res) {
//                    $scope.gl_period_ledgers = res.data;

//                    if ($scope.gl_period_ledgers == true) {
//                        swal({ title: "Alert", text: "Gl Period Ledger Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
//                            if (isConfirm) {
//                                $scope.getGLledgerdata($scope.edt.glpr_dept_no, $scope.edt.account_code);
//                            }
//                        });
//                        str.status = false;

//                    }
//                    else {
//                        swal({ title: "Alert", text: "Gl Period Ledger Not Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
//                            if (isConfirm) {
//                                $scope.getGLledgerdata($scope.edt.glpr_dept_no, $scope.edt.account_code);
//                            }
//                        });
//                        str.status = false;
//                    }
//                });
//                // }
//                // }
//            }

//            $scope.cancel = function (str) {
//                $scope.getGLledgerdata($scope.edt.glpr_dept_no, $scope.edt.account_code);
//                str.status = false;
//            }

//            $scope.edit = function (str) {
//                str.status = true;
//            }

//            $scope.makeTodos = function () {
//                var rem = parseInt($scope.totalItems % $scope.numPerPage);
//                if (rem == '0') {
//                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
//                }
//                else {
//                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
//                }
//                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
//                var end = parseInt(begin) + parseInt($scope.numPerPage);

//                $scope.filteredTodos = $scope.todos.slice(begin, end);
//            };

//            $scope.size = function (str) {
//                console.log(str);
//                $scope.pagesize = str;
//                $scope.currentPage = 1;
//                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
//            }

//            $scope.index = function (str) {
//                $scope.pageindex = str;
//                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
//            }


//        }])
//})();