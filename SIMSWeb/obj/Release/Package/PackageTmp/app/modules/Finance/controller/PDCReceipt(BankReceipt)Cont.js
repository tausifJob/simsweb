﻿(function () {
    'use strict';
    var main;
    var data2 = [];
    var Bankdetails = [];
    var coad_pty_short_name = [];
    var status = "";
    var msg_flag1 = false;
    var bankslno;
    var cmbvalue = "";
    var DuplicateBankDetails = [];
    var DuplicateEditBankDetails = [];
    var sano, chno, acno, chdate, amt;
    var companyCode = "1";
    var payable_acc_code;//IN there payable_acc_code i have put the value of Receiveable account number.....
    var values = [];
    var accountDescription;


    var simsController = angular.module('sims.module.Finance');


    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PDCReceipt(BankReceipt)Cont',
        ['$scope', '$state', '$rootScope', '$filter', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $filter, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '5';
            $scope.edt = "";
            $scope.operation = true;
            $scope.table1 = true;
            $scope.Add = true;
            $scope.ldgrcode = false;
            $scope.btnDelete = false;
            $scope.preparebtn = true;
            $scope.verifybtn = true;
            $scope.authorizebtn = true;
            $scope.Bankdetails = "";
            $scope.Ac_code = false;
            $scope.Ac_code_main = true;

            //payable_acc_code = "1603070";
            $scope.Bankdetails = [];
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = yyyy + '-' + mm + '-' + dd;
            $scope.edate = yyyy + '-' + mm + '-' + dd;
            $scope.temp = {
                DocDate: yyyy + '-' + mm + '-' + dd,
                PostDate: yyyy + '-' + mm + '-' + dd,

            }
            $scope.temp1 = {
                ChequeDate: yyyy + '-' + mm + '-' + dd,
            }

            $scope.temp.gltd_doc_code = 'PDC Payment Document';


            var user = $rootScope.globals.currentUser.username.toLowerCase();
            //  $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            // var comp_code = $scope.finnDetail.company;
            //var finance_year = $scope.finnDetail.year;


            //var comp_code = $scope.global_finnComp;
            //var finance_year = $scope.global_acd;
            //console.log($scope.global_finnComp);
            //console.log($scope.global_acd);
            //$scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            //console.log($scope.finnDetail)

            //$http.post(ENV.apiUrl + "api/PDCBill/Checkstattus_doc_users?comp_code=" + companyCode + "&user=" + user).then(function (res) {
            //    debugger
            //    $scope.users = res.data;
            //    if ($scope.users[0] == "Y") {
            //        $scope.preparebtn = false;
            //    }

            //    if ($scope.users[1] == "Y") {
            //        $scope.verifybtn = false;
            //    }

            //    if ($scope.users[2] == "Y") {
            //        $scope.authorizebtn = false;
            //    }

            //});


            $http.get(ENV.apiUrl + "api/BankPayment/GetVerifyAuthorizeUsers?username=" + user).then(function (users_list) {
               
                $scope.users = users_list.data;
                console.log($scope.users);
                if ($scope.users.length != 0) {
                  
                    if ($scope.users[0] == user && $scope.users[1] == user) {
                        $scope.preparebtn = false;
                        $scope.verifybtn = false;
                        $scope.authorizebtn = false;
                    }
                    else if ($scope.users[0] == user && ($scope.users[1] == "" || $scope.users[1] != user)) {
                        $scope.verifybtn = false;
                        $scope.preparebtn = false;
                        $scope.authorizebtn = true;
                    }
                    else {
                        $scope.preparebtn = false;
                        $scope.verifybtn = true;
                        $scope.authorizebtn = true;
                    }
                }
            });

            $http.get(ENV.apiUrl + "api/PDCBill/getInsertMesage").then(function (res) {
              
                $scope.insertmsg = res.data;
            });


            //$http.get(ENV.apiUrl + "api/PDCBill/GetVerifyAuthorizeUsers?username=" + user).then(function (users_list) {
            //    debugger
            //    $scope.users = users_list.data;
            //    console.log($scope.users);
            //    if ($scope.users.length != 0) {
            //        debugger
            //        if ($scope.users[0] == user && $scope.users[1] == user) {
            //            $scope.preparebtn = false;
            //            $scope.verifybtn = false;
            //            $scope.authorizebtn = false;
            //        }
            //        else if ($scope.users[0] == user && $scope.users[0] != user) {
            //            $scope.verifybtn = false;
            //            $scope.preparebtn = true;
            //            $scope.authorizebtn = true;
            //        }
            //        else if ($scope.users[0] != user && $scope.users[0] != user) {
            //            $scope.verifybtn = true;
            //            $scope.preparebtn = false;
            //            $scope.authorizebtn = true;
            //        }
            //    }
            //});
            $scope.Authorizeuser = function () {
                debugger;
                //$http.post(ENV.apiUrl + "api/PDCBill/Checkstattus_doc_users?comp_code=" + companyCode + "&user=" + user).then(function (res) {
                //    debugger
                //    $scope.users = res.data;
                //    if ($scope.users[0] == "Y") {
                //        $scope.preparebtn = false;
                //    }

                //    if ($scope.users[1] == "Y") {
                //        $scope.verifybtn = false;
                //    }

                //    if ($scope.users[2] == "Y") {
                //        $scope.authorizebtn = false;
                //    }
                $http.get(ENV.apiUrl + "api/BankPayment/GetVerifyAuthorizeUsers?username=" + user).then(function (users_list) {
                    debugger
                    $scope.users = users_list.data;
                    console.log($scope.users);
                    if ($scope.users.length != 0) {
                        debugger
                        if ($scope.users[0] == user && $scope.users[1] == user) {
                            $scope.preparebtn = false;
                            $scope.verifybtn = false;
                            $scope.authorizebtn = false;
                        }
                        else if ($scope.users[0] == user && ($scope.users[1] == "" || $scope.users[1] != user)) {
                            $scope.verifybtn = false;
                            $scope.preparebtn = false;
                            $scope.authorizebtn = true;
                        }
                        else {
                            $scope.preparebtn = false;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                        }
                    }
           
                });

                //$http.get(ENV.apiUrl + "api/PDCBill/GetVerifyAuthorizeUsers?username=" + user).then(function (users_list) {
                //    debugger
                //    $scope.users = users_list.data;
                //    console.log($scope.users);
                //    if ($scope.users.length != 0) {
                //        debugger
                //        if ($scope.users[0] == user && $scope.users[1] == user) {
                //            $scope.preparebtn = false;
                //            $scope.verifybtn = false;
                //            $scope.authorizebtn = false;
                //        }
                //        else if ($scope.users[0] == user && $scope.users[0] != user) {
                //            $scope.verifybtn = false;
                //            $scope.preparebtn = true;
                //            $scope.authorizebtn = true;
                //        }
                //        else if ($scope.users[0] != user && $scope.users[0] != user) {
                //            $scope.verifybtn = true;
                //            $scope.preparebtn = false;
                //            $scope.authorizebtn = true;
                //        }
                //    }
                //});
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $scope.myFunct = function (keyEvent) {

                if (keyEvent.which == 13)
                    $scope.temp.PostDate = yyyy + '-' + mm + '-' + dd;
            }

            $scope.myFunct1 = function (keyEvent) {

                if (keyEvent.which == 13)
                    $scope.temp1.ChequeDate = yyyy + '-' + mm + '-' + dd;
            }

            $scope.size = function (str) {

                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }


            $http.get(ENV.apiUrl + "api/PDCBill/Get_PDC_PAYABLES_ACCOUNT").then(function (docstatus) {

                $scope.PaymentAccountData = docstatus.data;
                $scope.temp['payable_acc_code'] = $scope.PaymentAccountData[0].fins_appl_parameter;

            });
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);



                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.New = function () {
                $scope.operation = true;
                $scope.Add = true;
                $scope.updatebtn = false;
                $scope.Delete = false;
                $scope.edt = "";
            }

            $scope.totalAmountCount = function () {
                debugger;
                $scope.total = 0;

                for (var i = 0; i < $scope.Bankdetails.length; i++) {

                    $scope.total = parseFloat($scope.total) + parseFloat($scope.Bankdetails[i].gldd_doc_amount);

                }
            }

            $scope.GetAllGLAcc = function (acccode, cmpnycode) {


                $http.get(ENV.apiUrl + "api/PDCBill/GetAllGLAccountNos?glma_accountcode=" + acccode + "&cmpnycode=" + companyCode).then(function (docstatus) {

                    $scope.getAllAccNos = docstatus.data;

                });
            }

            $http.get(ENV.apiUrl + "api/PDCBill/Get_doc_narration?cmpnycode=" + companyCode + "&doccode=" + $scope.temp.gltd_doc_code).then(function (docstatus) {

                $scope.getdoc_narration = docstatus.data;

            });


            $http.get(ENV.apiUrl + "api/PDCBill/GetAllGLAccountNos?glma_accountcode=" + payable_acc_code + "&cmpnycode=" + companyCode).then(function (docstatus) {

                $scope.getAllAccNos = docstatus.data;

            });

            $http.get(ENV.apiUrl + "api/PDCBill/getBankDeatails").then(function (getBankDetails) {
                $scope.BankDetails = getBankDetails.data;

            });

            $http.get(ENV.apiUrl + "api/PDCBill/getPdcReceivableAcc").then(function (GetPdcReceivableAcc) {
                debugger
                $scope.BankReceiveableDetails1 = GetPdcReceivableAcc.data;

                $scope.temp['BankReceiveableDetails'] = $scope.BankReceiveableDetails1[0].fins_appl_parameter;


                var bnkrecedet = $scope.BankReceiveableDetails1[0].fins_appl_parameter;
                values = bnkrecedet.split("-");

                payable_acc_code = values[0];
                var value2 = values[1];

                console.log($scope.BankReceiveableDetails)
            });




            $scope.getSLAccNo = function (slcode) {
                $scope.getAllAccNos = [];
                if (slcode == "00" || slcode == undefined) {
                    $scope.GetAllGLAcc(payable_acc_code, companyCode);
                }
                else {
                    debugger;
                    $http.get(ENV.apiUrl + "api/PDCBill/GetSLAccNumber?pbslcode=" + slcode).then(function (docstatus3) {
                        debugger;
                        $scope.getAllAccNos = docstatus3.data;
                        if ($scope.getAllAccNos.lenght > 0) {
                            debugger;
                            if (cmbvalue == "") {
                                $scope.edt.gldd_acct_code = "";
                                $scope.edt1.coad_dept_no = "";
                                $("#cmb_acc_Code").select2("val", "");
                            }
                            else {
                                $("#cmb_acc_Code").select2("val", cmbvalue);
                                //$scope.edt.gldd_acct_code = cmbvalue;
                            }
                        }
                    });
                }
                //$("#cmb_acc_Code").val(cmbvalue);

                //$("#cmb_acc_Code").select2("val", cmbvalue);
            }


            $scope.getDepartcode = function (str) {


                for (var i = 0; $scope.getAllAccNos.length; i++) {
                    if ($scope.getAllAccNos[i].gldd_acct_code == str) {
                        $scope.edt1 = {
                            coad_dept_no: $scope.getAllAccNos[i].gldd_dept_code,
                            gldd_acct_name: $scope.getAllAccNos[i].gldd_acct_name,
                            gldd_dept_name: $scope.getAllAccNos[i].gldd_dept_name
                        }
                    }
                }

            }

            $http.get(ENV.apiUrl + "api/PDCBill/GetLedgercode?cmpnycode=" + companyCode).then(function (docstatus2) {

                $scope.LdgrCode = docstatus2.data;

            });

            $("#PostDate").datepicker({
                dateFormat: "yyyy-mm-dd",
                maxDate: new Date()
            });

            $scope.getBankCode = function (str) {

                bankslno = "";
                for (var i = 0; i < $scope.BankDetails.length; i++) {
                    if ($scope.BankDetails[i].slma_pty_bank_id == str) {
                        bankslno = $scope.BankDetails[i].pb_gl_acno;
                    }
                }
            }

            $scope.Insert_temp_docs = function () {

                var datasend = [];

                var verifyUser, verifyDate, authorize_user, authorize_date;
                if (status == "Verify") {
                    verifyUser = user;
                    verifyDate = $scope.temp.DocDate;
                }
                else if (status == "Authorize") {
                    verifyUser = user;
                    verifyDate = $scope.temp.DocDate;
                    authorize_user = "";
                    authorize_date = "";
                }
                else {
                    verifyUser = "";
                    verifyDate = "";
                    authorize_user = "";
                    authorize_date = "";

                }
                var data = {
                    gltd_comp_code: companyCode,
                    gltd_prepare_user: user,
                    gltd_doc_code: $scope.temp.gltd_doc_code,
                    gltd_doc_date: $scope.temp.DocDate,
                    gltd_cur_status: status,
                    gltd_post_date: $scope.temp.PostDate,
                    gltd_doc_narr: $scope.temp1.gltd_doc_narr1,
                    gltd_remarks: $scope.temp1.gltd_doc_remark,
                    gltd_prepare_date: $scope.temp.DocDate,
                    gltd_final_doc_no: "0",
                    gltd_verify_user: verifyUser,
                    gltd_verify_date: verifyDate,
                    gltd_authorize_user: authorize_user,
                    gltd_authorize_date: authorize_date,
                    gltd_paid_to: null,
                    gltd_cheque_no: $scope.edt2.chequeno1
                }
                datasend.push(data);

                $http.post(ENV.apiUrl + "api/PDCBill/Insert_Fins_temp_docs", datasend).then(function (msg) {
                    $scope.prvno = msg.data;

                    if ($scope.prvno != "" && $scope.prvno != null) {
                        $scope.Insert_Fins_temp_doc_details();
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not inserted", width: 300, height: 200 });
                    }
                });

            }

            //   data.em_img = '1' + $scope.photo_filename.split("-")[1];


            $scope.Insert_Fins_temp_doc_details = function () {
                debugger;
                var dataSend = [];
                var j = 1;

                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    var data = {
                        gldd_doc_narr: $scope.Bankdetails[i].gldd_doc_narr,
                        gldd_dept_code: $scope.Bankdetails[i].coad_dept_no,
                        gldd_ledger_code: $scope.Bankdetails[i].sllc_ldgr_code,
                        gldd_acct_code: $scope.Bankdetails[i].slma_acno,
                        pc_bank_code: $scope.Bankdetails[i].slma_pty_bank_id,
                        gldd_party_ref_no: $scope.Bankdetails[i].check_no,
                        gldd_party_ref_date: $scope.Bankdetails[i].ChequeDate,
                        gldd_doc_amount: parseFloat("-" + $scope.Bankdetails[i].gldd_doc_amount),
                        gldd_comp_code: companyCode,
                        gldd_doc_code: $scope.temp.gltd_doc_code,
                        gldd_prov_doc_no: $scope.prvno,
                        gldd_final_doc_no: "0",
                        gldd_line_no: j,

                    }
                    j++;
                    dataSend.push(data);

                }
                var data = {
                    gldd_comp_code: companyCode,
                    gldd_doc_code: $scope.temp.gltd_doc_code,
                    gldd_prov_doc_no: $scope.prvno,
                    gldd_final_doc_no: "0",
                    gldd_line_no: j,
                    gldd_ledger_code: "00",
                    gldd_acct_code: payable_acc_code,
                    gldd_doc_amount: $scope.total,
                    gldd_fc_amount_debit: 0,
                    gldd_fc_code: "",
                    gldd_fc_rate: 0,
                    gldd_doc_narr: $scope.temp1.gltd_doc_narr1,
                    gldd_dept_code: "",
                    gldd_party_ref_no: "",
                    gldd_party_ref_date: "",

                }
                dataSend.push(data);
                $http.post(ENV.apiUrl + "api/PDCBill/Insert_Fins_temp_doc_details", dataSend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if (status == "Authorize") {
                        msg_flag1 = true;
                        $scope.Authorize_Insert_temp_doc_details($scope.msg1);

                    }
                    else if (status == "Save") {
                        msg_flag1 = false;
                        $scope.Authorize_Insert_temp_doc_details($scope.msg1);

                    }
                    else if (status == "Verify") {
                        msg_flag1 = false;
                        $scope.Authorize_Insert_temp_doc_details($scope.msg1);

                    }
                    if ($scope.msg1 == true) {

                    }
                    else {
                        swal({ title: "Alert", text: "Record Not inserted", width: 300, height: 200 });
                    }
                });

            }

            $scope.Authorize_Insert_temp_doc_details = function (str) {

                if (msg_flag1) {
                    msg_flag1 = false;
                    var auth_date = $scope.temp.DocDate;
                    $http.post(ENV.apiUrl + "api/PDCBill/Authorize_Posting?comp_code=" + companyCode + "&doc_code=" + $scope.temp.gltd_doc_code + "&prv_no=" + $scope.prvno + "&auth_user=" + user + "&auth_date=" + auth_date).then(function (Auth) {
                        debugger
                        $scope.Docno = Auth.data;
                        if ($scope.Docno != "") {
                            swal({ title: "Alert", text: "Posted Successfully\n Doc Code is=" + $scope.Docno, width: 300, height: 200 });
                            $scope.ClearInsert();
                            $scope.Authorizeuser();
                        }
                        else {
                            swal({ title: "Alert", text: "Posting Failed", width: 300, height: 200 });
                            $scope.ClearInsert();
                            $scope.Authorizeuser();
                        }
                    });

                }
                else if ((msg_flag1 == false) && (status == "Save")) {
                    msg_flag1 = false;
                    if (str) {
                        swal({ title: "Alert", text: "Record Inserted Successfully\nProvision No:-" + $scope.prvno, width: 300, height: 200 });
                        $scope.ClearInsert();
                        $scope.Authorizeuser();
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not inserted", width: 300, height: 200 });
                    }

                }

                else if ((msg_flag1 == false) && (status == "Verify")) {
                    msg_flag1 = false;
                    if (str) {
                        swal({ title: "Alert", text: "Record Verified Successfully\nProvision No:-" + $scope.prvno, width: 300, height: 200 });
                        $scope.ClearInsert();
                        $scope.Authorizeuser();
                    }
                    else {
                        swal({ title: "Alert", text: "Verification Failed", width: 300, height: 200 });
                    }

                }

                $scope.ClearInsert();
                $scope.Authorizeuser();
                $scope.cancel();

            }

            $scope.InsertPDCBills = function () {

                var senddata = [];
                var data = [];
                var j = 1;
                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    data = {
                        pb_comp_code: companyCode,
                        pb_sl_ldgr_code: $scope.Bankdetails[i].sllc_ldgr_code,
                        pb_sl_acno: $scope.Bankdetails[i].slma_acno,
                        pb_dept_no: $scope.Bankdetails[i].coad_dept_no,
                        pb_doc_type: "PP",
                        pb_our_doc_no: "",
                        pb_bank_from: $scope.Bankdetails[i].slma_pty_bank_id,
                        pb_bank_slno: $scope.Bankdetails[i].bankslno,
                        pb_cheque_no: $scope.Bankdetails[i].check_no,
                        pb_due_date: $scope.Bankdetails[i].ChequeDate,
                        pb_amount: $scope.Bankdetails[i].gldd_doc_amount,
                        pb_pstng_no: "",
                        pb_narrative: $scope.Bankdetails[i].gldd_doc_narr,
                        pb_bank_to: "",
                        pb_rec_serial: j,
                        pb_discd: "O",
                        pb_calendar: "A"
                    }
                    j++;
                    senddata.push(data);

                }

                $http.post(ENV.apiUrl + "api/PDCBill/Insert_PdcBill", senddata).then(function (msg) {
                    $scope.msg1 = msg.data;

                });

            }
            $scope.Prepare = function () {
                try {
                    if ($scope.Bankdetails.length > 0) {
                        $scope.flag1 = false;
                        if ($scope.temp.PostDate == null || $scope.temp.PostDate == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Select Posting Date", showCloseButton: true, width: 380, });

                        }
                        else if ($scope.temp.BankReceiveableDetails == null || $scope.temp.BankReceiveableDetails == undefined || payable_acc_code == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Define Receivable account", showCloseButton: true, width: 380, });

                        }


                        else {

                            $scope.preparebtn = true;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                            status = "Save";
                            //$scope.InsertPDCBills();
                            $scope.Insert_temp_docs();
                            $scope.Ac_code = false;
                            $scope.Ac_code_main = true;
                        }
                    }
                    else {
                        swal({ title: "Alert", text: $scope.insertmsg.strMessage, showCloseButton: true, width: 380, });
                    }
                }


                catch (e) {

                }


            }

            $scope.Verify = function () {
                try {
                    if ($scope.Bankdetails.length > 0) {
                        $scope.flag1 = false;
                        if ($scope.temp.PostDate == null || $scope.temp.PostDate == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Select Posting Date", showCloseButton: true, width: 380, });
                        }
                        else if ($scope.temp.BankReceiveableDetails == null || $scope.temp.BankReceiveableDetails == undefined || payable_acc_code == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Define Receivable account", showCloseButton: true, width: 380, });

                        }

                        else {
                            $scope.preparebtn = true;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                            status = "Verify";
                            $scope.Insert_temp_docs();
                            
                            $scope.Ac_code = false;
                            $scope.Ac_code_main = true;
                        }
                    }
                    else {
                        swal({ title: "Alert", text: $scope.insertmsg.strMessage, showCloseButton: true, width: 380, });
                    }


                } catch (e) {

                }

            }

            $scope.Authorize = function () {
                try {
                    if ($scope.Bankdetails.length > 0) {
                        $scope.flag1 = false;
                        if ($scope.temp.PostDate == null || $scope.temp.PostDate == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Select Posting Date", showCloseButton: true, width: 380, });

                        }
                        else if ($scope.temp.BankReceiveableDetails == null || $scope.temp.BankReceiveableDetails == undefined || payable_acc_code == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Define Receivable account", showCloseButton: true, width: 380, });

                        }

                        else {
                            $scope.preparebtn = true;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                            status = "Authorize";
                           
                            $scope.Insert_temp_docs();
                            $scope.Ac_code = false;
                            $scope.Ac_code_main = true;

                        }



                    }
                    else {
                        swal({ title: "Alert", text: $scope.insertmsg.strMessage, showCloseButton: true, width: 380, });

                    }


                } catch (e) {

                }
            }

            $scope.cancel = function () {

                $scope.Add = true;
                $scope.noofchequereadonly = false;
                $scope.updatebtn = false;
                $scope.edt = [];
                $scope.edt1 = [];
                $scope.edt2 = [];
                $scope.GetAllGLAcc(payable_acc_code, companyCode);
                $("#cmb_acc_Code").select2("val", "");

                $scope.Ac_code = false;
                $scope.Ac_code_main = true;
                $scope.ldgrcode = false;
            }
            $scope.ClearInsert = function () {
                debugger;
                $scope.Bankdetails = [];
                $scope.total = "";
                $scope.temp1 = {
                    gltd_doc_narr1: "",
                    gltd_doc_remark: ""
                }
            }
            setTimeout(function () {
                $("#cmb_acc_Code").select2();

            }, 100);



            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.onlyNumbers = function (event) {
                debugger;
                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $scope.summ = function () {

                $scope.total = 0;

                for (var i = 0; i < $scope.Bankdetails.length; i++) {

                    $scope.total = $scope.total + parseInt($scope.Bankdetails[i].gldd_doc_amount);

                }

            }

            $scope.checkduplicatecheque = function (str) {
                debugger;


                //var checkno = $('#chno').val();
                //var bankId = $('#bankId').val();
                var countno = 0;
                var f = false;



                console.log(DuplicateBankDetails);

                for (var j = 0; j < $scope.Bankdetails.length; j++) {
                    if ($scope.Bankdetails[j].check_no == str.check_no && $scope.Bankdetails[j].slma_pty_bank_id == str.slma_pty_bank_id) {
                        countno++;

                    }
                }
                if (countno > 1) {
                    // $('#chno').css.({ backgroundColor: "#FFF", border: "5px solid #ccc" })
                    //$('#chno').css('background-color', '#FFF');
                    //document.getElementById("chno").style.backgroundColor = "#ff0000";
                    $scope.Bankdetails = angular.copy(DuplicateBankDetails);
                    swal({ title: "Alert", text: "Check no and bank Code is already Present", showCloseButton: true, width: 380, });

                }
                else {
                    $scope.Bankdetails.push({
                        'gldd_comp_code': str.gldd_comp_code,
                        'sllc_ldgr_code': str.sllc_ldgr_code,
                        'slma_acno': str.slma_acno,
                        'coad_pty_full_name': str.coad_pty_full_name,
                        'coad_dept_no': str.coad_dept_no,
                        'bankslno': str.bankslno,
                        'check_no': str.check_no,
                        'ChequeDate': str.ChequeDate,
                        'coad_dept_name': str.coad_dept_name,
                        'gldd_doc_narr': str.coad_dept_name,
                        'gldd_doc_amount': str.gldd_doc_amount,
                        'slma_pty_bank_id': str.slma_pty_bank_id,
                        'noOfCheck': str.noOfCheck,
                    });
                    $scope.Bankdetails.splice(j, 1);
                    DuplicateBankDetails = angular.copy($scope.Bankdetails);

                }

            }


            //Add Button Code......
            $scope.AddintoGrid = function (myForm) {
                debugger;
                if (myForm) {
                    $scope.flag1 = false;
                    if ($scope.edt.gldd_acct_code == undefined || $scope.edt.gldd_acct_code == "") {
                        $scope.flag1 = true;
                        swal({ title: "Alert", text: "Please select Account name", showCloseButton: true, width: 380, });

                    }
                    else if ($scope.edt.gldd_doc_amount == undefined || $scope.edt.gldd_doc_amount == "") {
                        $scope.flag1 = true;
                        swal({ title: "Alert", text: "Please Enter Amount", showCloseButton: true, width: 380, });
                    }
                    else if ($scope.edt.slma_pty_bank_id == undefined || $scope.edt.slma_pty_bank_id == "") {
                        $scope.flag1 = true;
                        swal({ title: "Alert", text: "Please select bank name", showCloseButton: true, width: 380, });

                    }
                    else if ($scope.edt2 == undefined || $scope.edt2.chequeno1 == "") {
                        $scope.flag1 = true;
                        swal({ title: "Alert", text: "Please Enter Cheque Number", showCloseButton: true, width: 380, });
                    }
                    else if ($scope.temp1.ChequeDate == null || $scope.temp1.ChequeDate == "") {
                        $scope.flag1 = true;
                        swal({ title: "Alert", text: "Please Enter Cheque Date", showCloseButton: true, width: 380, });
                    }
                    else if ($scope.edt.NoOfCheque == null || $scope.edt.NoOfCheque == "") {
                        $scope.flag1 = true;
                        swal({ title: "Alert", text: "Please Enter Number of Cheque", showCloseButton: true, width: 380, });
                    }
                    else {
                        $scope.flag1 = false;
                    }

                    if (!$scope.flag1) {
                        for (var i = 0; i < $scope.edt.NoOfCheque; i++) {
                            var f = false;
                            var ldgr_code = "";

                            var nextMonth = new Date($scope.temp1.ChequeDate);
                            nextMonth.setMonth(nextMonth.getMonth() + i);
                            var d = nextMonth.getDate();
                            var m = nextMonth.getMonth() + 1;
                            if (m < 10) {
                                m = '0' + m;
                            }
                            var y = nextMonth.getFullYear();
                            var bankname = document.getElementById("bankName");
                            var selectbankname = bankname.options[bankname.selectedIndex].text;

                            var terminal = document.getElementById("cmb_acc_Code");
                            $scope.selectedText = terminal.options[terminal.selectedIndex].text;
                            if ($scope.edt.gldd_doc_narr == undefined) {
                                $scope.edt.gldd_doc_narr = "";
                            }
                            $scope.gldd_doc_narr1 = $scope.edt.gldd_doc_narr + " - " + $scope.edt2.chequeno1 + " - " + y + '-' + m + '-' + d;
                            if ($scope.edt.sllc_ldgr_code == undefined) {
                                ldgr_code = '00';
                            }
                            else {
                                ldgr_code = $scope.edt.sllc_ldgr_code;
                            }
                            for (var j = 0; j < $scope.Bankdetails.length; j++) {
                                if ($scope.Bankdetails[j].check_no == $scope.edt2.chequeno1 && $scope.Bankdetails[j].slma_pty_bank_id == $scope.edt.slma_pty_bank_id) {
                                    f = true;
                                }
                            }
                            if (f) {
                                $scope.edt2.chequeno1 = parseInt($scope.edt2.chequeno1) + 1;
                                i--;
                            }
                            else {
                                $scope.Bankdetails.push({
                                    'gldd_comp_code': companyCode,
                                    'sllc_ldgr_code': ldgr_code,
                                    'slma_acno': $scope.edt.gldd_acct_code,
                                    'coad_pty_full_name': $scope.selectedText,
                                    'coad_dept_no': $scope.edt1.coad_dept_no,
                                    'bankslno': bankslno,
                                    //  'check_no_old': $scope.edt2.chequeno1,
                                    'check_no': $scope.edt2.chequeno1,
                                    'ChequeDate': y + '-' + m + '-' + d,
                                    'coad_dept_name': selectbankname,
                                    'gldd_doc_narr': $scope.gldd_doc_narr1,
                                    'gldd_doc_amount': $scope.edt.gldd_doc_amount,
                                    'slma_pty_bank_id': $scope.edt.slma_pty_bank_id,
                                    'noOfCheck': $scope.edt.NoOfCheque,

                                });
                                $scope.edt2.chequeno1 = parseFloat($scope.edt2.chequeno1) + 1;
                            }



                        }
                        DuplicateBankDetails = angular.copy($scope.Bankdetails);

                        $scope.total = 0;

                        for (var i = 0; i < $scope.Bankdetails.length; i++) {

                            $scope.total = parseFloat($scope.total) + parseFloat($scope.Bankdetails[i].gldd_doc_amount);

                        }

                        $scope.edt = [];
                        $scope.edt1 = [];
                        $scope.edt2 = [];
                        $scope.getSLAccNo($scope.edt.sllc_ldgr_code);
                        $("#cmb_acc_Code").select2("val", "");

                    }
                }


            }


            $scope.editupdate = function (str) {
                debugger;
                $scope.Ac_code = true;
                $scope.Ac_code_main = false;
                var data = angular.copy(str);
                DuplicateEditBankDetails[0] = data;
                sano = "";
                chno = "";
                acno = "";
                sano = data.slma_acno;
                chno = data.check_no;
                amt = data.gldd_doc_amount;
                chdate = data.ChequeDate;
                cmbvalue = data.slma_acno;
                acno = data.slma_acno;
                $scope.noofchequereadonly = true;
                $scope.Add = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.ldgrcode = true;
                $scope.edt1 = {
                    coad_dept_no: data.coad_dept_no,

                }

                $scope.temp1 = {
                    ChequeDate: data.ChequeDate,
                }
                $scope.edt =
                {
                    // gldd_acct_code: data.slma_acno,
                    accCode: data.coad_pty_full_name,
                    accountDescription: data.coad_pty_full_name,
                    sllc_ldgr_code: data.sllc_ldgr_code,
                    gldd_doc_amount: data.gldd_doc_amount,
                    slma_pty_bank_id: data.slma_pty_bank_id,
                    gldd_doc_narr: data.gldd_doc_narr,
                };

                $scope.edt2 = {
                    chequeno1: data.check_no
                }


                //if (data.sllc_ldgr_code != "00") {
                //    $("#cmb_acc_Code").select2("val", "");
                //    //$scope.getSLAccNo(data.sllc_ldgr_code);
                //}
                //else {
                //    $("#cmb_acc_Code").select2("val", data.slma_acno);
                //}
                debugger;
                accountDescription = $scope.edt.accCode;
                $scope.Ac_code = true;
                $scope.Ac_code_main = false;

            }

            //$scope.Update = function () {
            //    debugger;
            //    $scope.noofchequereadonly = false;

            //    for (var i = 0; i < $scope.Bankdetails.length; i++) {
            //        if ($scope.Bankdetails[i].check_no == chno && $scope.Bankdetails[i].slma_acno == sano && $scope.Bankdetails[i].gldd_doc_amount == amt && $scope.Bankdetails[i].ChequeDate == chdate) {
            //            cmbvalue = "";
            //            sano = "";
            //            chno = "";
            //            amt = "";
            //            chdate = "";

            //            var ldgr_code = "";
            //            var nextMonth = new Date($scope.temp1.ChequeDate);
            //            nextMonth.setMonth(nextMonth.getMonth() + i);
            //            var d = nextMonth.getDate();
            //            var m = nextMonth.getMonth() + 1;
            //            if (m < 10) {
            //                m = '0' + m;
            //            }
            //            var y = nextMonth.getFullYear();
            //            var bankname = document.getElementById("bankName");
            //            var selectbankname = bankname.options[bankname.selectedIndex].text;

            //            //var terminal = document.getElementById("cmb_acc_Code");
            //            //$scope.selectedText = terminal.options[terminal.selectedIndex].text;
            //            if ($scope.edt.gldd_doc_narr == undefined) {
            //                $scope.edt.gldd_doc_narr = "";
            //            }
            //            $scope.gldd_doc_narr1 = $scope.edt.gldd_doc_narr + " - " + $scope.edt2.chequeno1 + " - " + y + '-' + m + '-' + d;
            //            if ($scope.edt.sllc_ldgr_code == undefined) {
            //                ldgr_code = '00';
            //            }
            //            else {
            //                ldgr_code = $scope.edt.sllc_ldgr_code;
            //            }

            //            $scope.Bankdetails.push({
            //                'gldd_comp_code': companyCode,
            //                'sllc_ldgr_code': ldgr_code,
            //                'slma_acno': acno,
            //                'coad_pty_full_name': $scope.selectedText,
            //                'coad_dept_no': $scope.edt1.coad_dept_no,
            //                'bankslno': bankslno,
            //                'check_no': $scope.edt2.chequeno1,
            //                'ChequeDate': y + '-' + m + '-' + d,
            //                'coad_dept_name': selectbankname,
            //                'gldd_doc_narr': $scope.gldd_doc_narr1,
            //                'gldd_doc_amount': $scope.edt.gldd_doc_amount,
            //                'slma_pty_bank_id': $scope.edt.slma_pty_bank_id,
            //                'noOfCheck': $scope.edt.NoOfCheque,
            //            });
            //            $scope.Bankdetails.splice(i, 1);


            //        }
            //        $scope.total = 0;

            //        for (var i = 0; i < $scope.Bankdetails.length; i++) {

            //            $scope.total = $scope.total + parseInt($scope.Bankdetails[i].gldd_doc_amount);

            //        }
            //        $scope.updatebtn = false;
            //        $scope.Add = true;
            //        acno = '';

            //        $scope.edt = [];
            //        $scope.edt1 = [];
            //        $scope.edt2 = [];
            //        $scope.getSLAccNo($scope.edt.sllc_ldgr_code);
            //        $("#cmb_acc_Code").select2("val", "");
            //    }
            //}

            $scope.Update = function () {
                debugger;
                if (myForm) {
                    var ff = false;
                    debugger;
                    if (DuplicateEditBankDetails[0].slma_pty_bank_id == $scope.edt.slma_pty_bank_id && DuplicateEditBankDetails[0].check_no == $scope.edt2.chequeno1) {
                        ff = false;
                    }
                    else {
                        for (var i = 0; i < $scope.Bankdetails.length; i++) {
                            if ($scope.Bankdetails[i].slma_pty_bank_id == $scope.edt.slma_pty_bank_id && $scope.Bankdetails[i].check_no == $scope.edt2.chequeno1) {
                                ff = true;
                            }
                        }
                    }
                    if (!ff) {
                        $scope.noofchequereadonly = false;
                        for (var i = 0; i < $scope.Bankdetails.length; i++) {
                            if ($scope.Bankdetails[i].check_no == chno && $scope.Bankdetails[i].slma_acno == sano && $scope.Bankdetails[i].gldd_doc_amount == amt && $scope.Bankdetails[i].ChequeDate == chdate) {
                                cmbvalue = "";
                                sano = "";
                                chno = "";
                                amt = "";
                                chdate = "";

                                var ldgr_code = "";
                                var nextMonth = new Date($scope.temp1.ChequeDate);
                                nextMonth.setMonth(nextMonth.getMonth() + i);
                                var d = nextMonth.getDate();
                                var m = nextMonth.getMonth() + 1;
                                if (m < 10) {
                                    m = '0' + m;
                                }
                                var y = nextMonth.getFullYear();
                                var bankname = document.getElementById("bankName");
                                var selectbankname = bankname.options[bankname.selectedIndex].text;

                                //var terminal = document.getElementById("cmb_acc_Code");
                                //$scope.selectedText = terminal.options[terminal.selectedIndex].text;
                                if ($scope.edt.gldd_doc_narr == undefined) {
                                    $scope.edt.gldd_doc_narr = "";
                                }
                                $scope.gldd_doc_narr1 = $scope.edt.gldd_doc_narr + " - " + $scope.edt2.chequeno1 + " - " + y + '-' + m + '-' + d;
                                if ($scope.edt.sllc_ldgr_code == undefined) {
                                    ldgr_code = '00';
                                }
                                else {
                                    ldgr_code = $scope.edt.sllc_ldgr_code;
                                }

                                $scope.Bankdetails.push({
                                    'gldd_comp_code': companyCode,
                                    'sllc_ldgr_code': ldgr_code,
                                    'slma_acno': acno,
                                    'coad_pty_full_name': $scope.selectedText,
                                    'coad_dept_no': $scope.edt1.coad_dept_no,
                                    'bankslno': bankslno,
                                    'check_no': $scope.edt2.chequeno1,
                                    'ChequeDate': y + '-' + m + '-' + d,
                                    'coad_dept_name': selectbankname,
                                    'gldd_doc_narr': $scope.gldd_doc_narr1,
                                    'gldd_doc_amount': $scope.edt.gldd_doc_amount,
                                    'slma_pty_bank_id': $scope.edt.slma_pty_bank_id,
                                    'noOfCheck': $scope.edt.NoOfCheque,
                                });
                                $scope.Bankdetails.splice(i, 1);


                            }
                        }
                        debugger;
                        $scope.total = 0;

                        for (var i = 0; i < $scope.Bankdetails.length; i++) {

                            $scope.total = parseFloat($scope.total) + parseFloat($scope.Bankdetails[i].gldd_doc_amount);

                        }
                        $scope.updatebtn = false;
                        $scope.Add = true;
                        acno = '';

                        $scope.edt = [];
                        $scope.edt1 = [];
                        $scope.edt2 = [];
                        $scope.getSLAccNo($scope.edt.sllc_ldgr_code);
                        $("#cmb_acc_Code").select2("val", "");

                    }

                    else {
                        swal({ title: "Alert", text: "Check no Already Present", width: 300, height: 200 });
                    }
                }
                $scope.Ac_code = false;
                $scope.Ac_code_main = true;
                $scope.ldgrcode = false;

            }


            $scope.Delete = function (str) {

                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    if ($scope.Bankdetails[i].check_no == str.check_no && $scope.Bankdetails[i].slma_acno == str.slma_acno && $scope.Bankdetails[i].gldd_doc_amount == str.gldd_doc_amount && $scope.Bankdetails[i].ChequeDate == str.ChequeDate) {
                        $scope.Bankdetails.splice(i, 1);
                        break;
                    }
                }
                DuplicateBankDetails = angular.copy($scope.Bankdetails);
                $scope.total = 0;

                for (var i = 0; i < $scope.Bankdetails.length; i++) {

                    $scope.total = parseFloat($scope.total) + parseFloat($scope.Bankdetails[i].gldd_doc_amount);

                }
            }


        }])
}

)();
