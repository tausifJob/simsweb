﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var comp_code;
    var finance_year;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('FinancialDocumentCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;

            var user = $rootScope.globals.currentUser.username;
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            comp_code = $scope.finnDetail.company;
            finance_year = $scope.finnDetail.year;


            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/FinancialDoc/getDocumentType").then(function (docName_Data) {
                debugger;
                $scope.doc_Name = docName_Data.data;
                console.log($scope.doc_Name);
            });

           //Select Data SHOW

            $http.get(ENV.apiUrl + "api/FinancialDoc/getAllFinancialDocument").then(function (res1) {
                $scope.fin_doc = res1.data;
                $scope.totalItems = $scope.fin_doc.length;
                $scope.todos = $scope.fin_doc;
                $scope.makeTodos();

            });


            //$http.get(ENV.apiUrl + "api/GoalTarget/getAcademicYear").then(function (Acyear) {
            //    $scope.Acc_year = Acyear.data;
            //    console.log($scope.Acc_year);
            //});
            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.fin_doc, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.fin_doc;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.gldc_doc_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.gldc_doc_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_doc_srno == toSearch) ? true : false;
            }

            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "/" + month + "/" + day;
            }


            //NEW BUTTON
            $scope.New = function () {

                debugger;
              
                $scope.disabled = false;
                $scope.gtcreadonly = true;
                $scope.gtdreadonly = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = "";

            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger
                if (Myform) {
                    var data = $scope.temp;
                    data.opr = 'I';
                    data.gldc_year = finance_year;
                    data.gldc_comp_code = comp_code;

                    datasend.push(data)
                    $http.post(ENV.apiUrl + "api/FinancialDoc/CUDFinantialDocument", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Inserted Successfully", showCloseButton: true, width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record not Inserted", showCloseButton: true, width: 300, height: 200 });
                        }
                        $http.get(ENV.apiUrl + "api/FinancialDoc/getAllFinancialDocument").then(function (res1) {
                            $scope.fin_doc = res1.data;
                            $scope.totalItems = $scope.fin_doc.length;
                            $scope.todos = $scope.fin_doc;
                            $scope.makeTodos();
                            $scope.table = true;
                            $scope.display = false;
                        });

                    });
                    datasend = [];
                }
            }
            
            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger;
                $scope.gtcreadonly = true;
                $scope.gtdreadonly = true;
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.temp = str;

                //$scope.getDocumentN(str.sims_doc_mod_code);


                $scope.temp = {
                    gldc_doc_code: str.gldc_doc_code
                   , gldc_doc_name: str.gldc_doc_name
                   , gldc_doc_type: str.gldc_doc_type
                   , gldc_next_srl_no: str.gldc_next_srl_no
                   , gldc_next_prv_no: str.gldc_next_prv_no

                };
            }


            //DATA UPADATE
            var dataupdate = [];
            $scope.update = function () {
                debugger;

                    var data = $scope.temp;
                    data.opr = "U";
                    data.gldc_year = finance_year;
                    data.gldc_comp_code = comp_code;

                    dataupdate.push(data);
                    $http.post(ENV.apiUrl + "api/FinancialDoc/CUDFinantialDocument", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", showCloseButton: true, width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", showCloseButton: true, width: 300, height: 200 });
                        }
                        $http.get(ENV.apiUrl + "api/FinancialDoc/getAllFinancialDocument").then(function (res1) {
                            $scope.fin_doc = res1.data;
                            $scope.totalItems = $scope.fin_doc.length;
                            $scope.todos = $scope.fin_doc;
                            $scope.makeTodos();
                            $scope.table = true;
                            $scope.display = false;
                        });

                    });
                    dataupdate = [];
                }
            
           


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });


        }])

})();
