﻿(function () {
    'use strict';
    var modulecode = [];
    var del = [];
    var main;
    var deletefin = [];
    var flag;
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CompanyMasterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
          
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.div_hide = false;
            $scope.display = false;
            $scope.grid = true;
            $scope.save1 = false;//
            $scope.value = false;
            $scope.itemsPerPage = "5";
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/finance/getAllCompanyMaster").then(function (res) {
                $scope.obj = res.data;
                for (var i = 0; i < $scope.obj.length; i++) {
                    $scope.obj[i].icon = "fa fa-plus-circle";
                }               
                console.log($scope.obj);
                $scope.totalItems = $scope.obj.length;
                $scope.todos = $scope.obj;
                $scope.makeTodos();
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            });

            
            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                console.log($scope.filteredTodos);
            };

            $scope.size = function (str) {
                console.log(str);
                $scope.itemsPerPage = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }
          

            $http.get(ENV.apiUrl + "api/finance/getComCode").then(function (res) {
                $scope.compcode = res.data;
            });
           
            $scope.currency = function (contrycode) {
               
                $http.get(ENV.apiUrl + "api/finance/getCurrencydesc?contrycode=" + contrycode).then(function (res) {
                    $scope.currencyDesc = res.data;
                });
            }
                     
            $scope.edit = function (str) {
                $scope.value = true;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edt = str;
                console.log($scope.edt);
                $scope.currency(str.comp_cntry_code);
            }
            
              
            $http.get(ENV.apiUrl + "api/finance/getAllCountryName").then(function (res) {
                $scope.countryNames = res.data;
              
            });

            $scope.Update = function (isvalidate) {
                if (isvalidate) {
                    $scope.edt.opr = "U";
                    data.push($scope.edt)
                    console.log(data);
                    $http.post(ENV.apiUrl + "api/finance/CUDCompanyMaster", data).then(function (msg) {
                        if (msg.data) {
                            $scope.grid = true;
                            $scope.display = false;
                            data = [];
                            swal('', 'Record Updated Successfully');

                            $scope.maindata();
                        } else {

                            swal('', "Record Not Updated");
                        }
                        $scope.display = false;
                        $scope.grid = true;
                    });
                }
            }
            var data = [];

            $scope.Save = function (isvalidate) {
                if (isvalidate) {
                    $scope.edt.opr = "I"
                    data.push($scope.edt);
                    console.log(data);
                    $http.post(ENV.apiUrl + "api/finance/CUDCompanyMaster", data).then(function (msg) {
                        if (msg.data) {
                            data = [];
                            swal('', 'Record Inserted Successfully');

                            $scope.maindata();
                        } else {
                            data = [];
                            swal('', "Record Not Inserted");
                        }
                        $scope.display = false;
                        $scope.grid = true;
                    });
                }

            }

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.value = false;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.maindata();
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            //$scope.Delete = function () {
            //    $scope.row1 = '';
            //    //for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //    //    var v = document.getElementById(i);
            //    //    if (v.checked == true) {
            //    //        $scope.filteredTodos[i].opr = "D";
            //    //        del.push($scope.filteredTodos[i]);
            //    //    }
            //    //}
            //    //$http.post(ENV.apiUrl + "api/finance/CUDCompanyMaster", del).then(function (msg) {
            //    //    if (msg.data) {
            //    //        del = [];
            //    //        swal('', 'Record Deleted Successfully');
            //    //             $scope.maindata();
            //    //        $scope.currentPage = 0;
            //    //    } else {
            //    //        del = [];
            //    //        swal('', 'Record Not Deleted');
            //    //    }
            //    //})
            //}
                                   
            $scope.maindata = function () {
                $http.get(ENV.apiUrl + "api/finance/getAllCompanyMaster").then(function (res) {
                    $scope.obj = res.data;
                    $scope.div_hide = false;
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.save1 = false;
                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.makeTodos();
                    for (var i = 0; i < $scope.obj.length; i++) {

                        $scope.obj[i].icon = "fa fa-plus-circle";

                    }
                });
            }
        
            $scope.multipledelete = function () {
               
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                   
                    var v = document.getElementById($scope.filteredTodos[i].comp_code+i);
                    $scope.color = '';
                if (main.checked == true) {
                    v.checked = true;
                    $scope.color = '#edefef';
                    $('tr').addClass("row_selected");
                }
                else {
                        v.checked = false;
                        main.checked = false;
                        $('tr').removeClass("row_selected");
                        $scope.color = '';
                    }
                }
                
            }

            $scope.delete_onebyone = function () {
              
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }
            }


            $scope.Delete = function () {
                debugger;
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].comp_code + i);
                    if (v.checked == true) {
                        $scope.filteredTodos[i].opr = "D";
                        deletefin.push($scope.filteredTodos[i]);
                        $scope.flag = true;
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            debugger;
                            $http.post(ENV.apiUrl + "api/finance/CUDCompanyMaster", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm)
                                    {
                                        if (isConfirm)
                                        {
                                            $scope.maindata();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                        
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.maindata();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                    $scope.multipledelete();
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].comp_code+i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    //$scope.row1 = '';
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }



            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
            var dom;
            var count = 0;
        

            $scope.searched = function (valLists, toSearch) {



                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.comp_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.comp_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.comp_short_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }
            $scope.flag = true;

            $scope.expand = function (info, $event) {
                console.log(info)
                console.log($event);
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                         "<tr><td class='semi-bold'>" + "COUNTRY CODE" + " </td><td class='semi-bold'>" + "CURRENCY CODE" + "</td> <td class='semi-bold'>" + "CURRENCY DECIMAL" + " </td><td class='semi-bold'>" + "DEPT MAX" + "</td><td class='semi-bold'>" + "DEPT MIN" + "</td><td class='semi-bold'>" + "ADDRESS LINE1" + "</td></tr>" +
                         "<tr><td class='v-align-middle'>" + (info.comp_cntry_name) + " </td><td>" + (info.comp_curcy_name) + "</td><td class='v-align-middle'>" + (info.comp_curcy_dec) + " </td><td>" + (info.comp_max_dept) + "</td><td>" + (info.comp_min_dept) + "</td><td>" + (info.comp_addr_line1) + "</td> </tr>" +

                         "<tr> <td class='semi-bold'>" + "ADDRESS LINE2" + " </td><td class='semi-bold'>" + "ADDRESS LINE3" + "</td> <td class='semi-bold'>" + "TELEPHONE NO" + "</td><td class='semi-bold'>" + "FAX NO" + " </td>" +
                        "<td class='semi-bold'>" + "EMAIL" + "</td><td class='semi-bold'>" + "WEB SITE" + " </td></tr>" +

                          "<tr><td>" + (info.comp_addr_line2) + " </td><td>" + (info.comp_addr_line3) + "</td><td>" + (info.comp_tel_no) + "</td><td>" + (info.comp_fax_no) + "</td>" +
                        "<td>" + (info.comp_email) + "</td><td>" + (info.comp_web_site) + " </td></tr>" +

                        " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);

                    console.log(dom);
                    $scope.flag = false;
                } else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.obj.length; i++) {

                        $scope.obj[i].icon = "fa fa-plus-circle";

                    }
                    $scope.flag = true;
                }
            };

        }])
})();