﻿(function () {
    'use strict';
    var main;
    var prdocdata = [];
    var data1 = [];
    var data = [];
    //var totalDebit=0;
    //var totalcredit=0;

    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('VoucherReversalCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.edt = "";
            $scope.operation = true;
            $scope.table1 = true;
            $scope.save1 = true;
            $scope.Add = true;
            $scope.update = false;
            $scope.Update = false;
            var user = $rootScope.globals.currentUser.username;

            $scope.btn_reverse = false;
            $scope.btn_verify = true;

            //$scope.schoolUrl = window.location.href;
            //var conStr = $scope.schoolUrl.substring(0, $scope.schoolUrl.indexOf('#'));
            //console.log(conStr);

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
                $("#fixedtable1").tableHeadFixer({ 'top': 1 });
            }, 100);


            $scope.SearchVouReverData = [];
            $scope.ref_docprvno = '';
            $scope.ref_final_doc_no = '';
            $scope.new_prov_data= '';
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $http.get(ENV.apiUrl + "api/VoucherReversal/getDocCodeJV?comp_code=1").then(function (res) {
                $scope.getDocCode = res.data;
            });

            $http.get(ENV.apiUrl + "api/VoucherReversal/GetNextfins_doc_prvno").then(function (res) {
                $scope.getnextprovno_rv = res.data;
            });

            $scope.ContraEntry = function (vcrrev) {
                $('#myModal_ContraEntry').modal('show');
               
                debugger
                $scope.btn_reverse = false;
                $scope.btn_verify = true;

                $http.post(ENV.apiUrl + "api/VoucherReversal/Search_vouchers_temp_docs?doccode=" + vcrrev.gltd_doc_code +
                   "&provdoc_no=" + vcrrev.gltd_prov_doc_no).then(function (Search_vouchers_temp_docs_Data) {
                       debugger;
                       $scope.ref_docprvno = vcrrev.gltd_prov_doc_no;
                       $scope.ref_final_doc_no = vcrrev.gltd_final_doc_no;

                       $scope.SearchVouReverData = Search_vouchers_temp_docs_Data.data;
                       console.log($scope.SearchVouReverData);
                       $scope.edt = {
                           gltd_postdate: vcrrev.gltd_postdate,
                            gltd_docdate: yyyy + '-' + mm + '-' + dd,
                           //gltd_docdate: vcrrev.gltd_docdate,
                           gltd_doc_narr: vcrrev.gltd_doc_narr,
                           gltd_remarks: vcrrev.gltd_remarks,
                           gltd_doc_code: vcrrev.gltd_doc_code
                       }
                       $scope.totalDebit = 0;
                       $scope.totalcredit = 0;
                       for (var i = 0; i < $scope.SearchVouReverData.length; i++) {
                           $scope.totalDebit = parseInt($scope.totalDebit) + parseInt($scope.SearchVouReverData[i].gldd_doc_amount_debit);
                           $scope.totalcredit = parseInt($scope.totalcredit) + parseInt($scope.SearchVouReverData[i].gldd_doc_amount_credit);
                       }
                       $scope.amountdifference = $scope.totalcredit - $scope.totalDebit;
                   });
            }


            $scope.Show = function () {
                debugger
                //if (doc_prov_no == undefined)
                //    doc_prov_no = 0;
                //from_date = $scope.edt.from_date;
                //to_date = $scope.edt.to_date;
                $http.get(ENV.apiUrl + "api/VoucherReversal/getAllRecordsVoucherReversal?doc_code=" + $scope.edt.gltd_doc_code + "&doc_prov_no=" + $scope.edt.gltd_doc_proven_no +
                    "&from_date=" + $scope.edt.from_date + "&to_date=" + $scope.edt.to_date).then(function (VouRever_Data) {
                        $scope.VouReverData = VouRever_Data.data;
                        console.log($scope.VouReverData);
                    });
            }

            $scope.RevertVoucher = function () {
                if ($scope.totalDebit == $scope.totalcredit) {
                    debugger;
                    var data = $scope.edt;
                    data.gldd_doc_code = 'RV';
                    //data.gltd_prov_doc_no = '';
                    data.gltd_prov_doc_no = $scope.getnextprovno_rv;
                    $scope.new_prov_data = data.gltd_prov_doc_no;
                    data.gltd_doc_date = $scope.edt.gltd_docdate;
                    data.gltd_post_date = $scope.edt.gltd_postdate;
                    data.gltd_doc_narr = $scope.edt.gltd_doc_narr;
                    data.gltd_remarks = $scope.edt.gltd_remarks;
                    data.gltd_prepare_user = '';
                    data.gltd_prepare_user = $rootScope.globals.currentUser.username;
                    data.gltd_reference_doc_code = $scope.gltd_doc_code;
                    data.gltd_reference_final_doc_no = $scope.ref_final_doc_no;

                    data.detailsdata = $scope.SearchVouReverData;
                    $http.post(ENV.apiUrl + "api/VoucherReversal/InsertFins_temp_docs", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.btn_reverse = true;
                                    $scope.btn_verify = false;
                                }
                            });
                        }
                        else {
                            swal({ title: "Alert", text: "Record not Updated", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.btn_reverse = false;
                                    $scope.btn_verify = true;
                                }
                            });
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Debit and Credit amount do not match.Cannot Revert Voucher.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.btn_reverse = false;
                            $scope.btn_verify = true;
                        }
                    });
                }
            }

            $scope.close = function () {
                debugger;
                $scope.edt.gltd_doc_code = '';
                $scope.edt.gltd_doc_proven_no = '';
                $scope.edt.from_date = '';
                $scope.edt.to_date = '';

                $scope.Show();
            }

            $scope.Verify = function () {
                debugger;
                var data = $scope.edt;
                data.gldd_doc_code = 'RV';
                data.gltd_prov_doc_no = $scope.new_prov_data;//$scope.edt.gltd_prov_doc_no;
                data.gltd_prepare_user = $rootScope.globals.currentUser.username;

                $http.post(ENV.apiUrl + "api/VoucherReversal/UpdateFins_temp_docs_current_status_sendtoverify", data).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {

                        swal({ title: "Alert", text: "Voucher Sent to Verification", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                $scope.btn_reverse = false;
                                $scope.btn_verify = true;
                            }
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Record not updated.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                $scope.btn_reverse = false;
                                $scope.btn_verify = true;
                            }
                        });
                    }
                });
            }
        }]
        )
})();
