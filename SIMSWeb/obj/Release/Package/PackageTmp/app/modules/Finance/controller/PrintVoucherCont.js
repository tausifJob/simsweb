﻿
(function () {
    'use strict';
    var chksearch;
    var main1;
    var prdocdata = [];
    var data1 = [];
    var data = [];
    var doc_no;
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('PrintVoucherCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {
            $scope.pagesize = '5';

            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            console.log($scope.finnDetail)

            debugger;
            $http.get(ENV.apiUrl + "api/PrintVoucher/getPurchasedeatils").then(function (result) {
                $scope.total_counts = result.data;
                console.log($scope.total_counts);
            });

            $scope.search_textbox = false;
            $scope.disabled_fromdate = false;

            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.countData = [
              { val: 5, data: 5 },
              { val: 10, data: 10 },
              { val: 15, data: 15 },

            ]
            $scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');

            $scope.edt =
                          {
                              from_date: $scope.ddMMyyyy,
                              to_date: $scope.ddMMyyyy,
                          }

            $scope.size = function (str) {
                if (str == 5 || str == 10 || str == 15) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();

            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.createdate = function (date) {
                if ($scope.edt.from_date != '') {
                    if ($scope.edt.from_date > $scope.edt.to_date) {
                        swal({ title: 'Please Select future Date', width: 380, height: 100 });
                        $scope.edt.to_date = '';
                    }
                }
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.Show = function (from_date, to_date, search, comp_code) {

                if ($scope.edt.search == undefined || $scope.edt.search == "") {
                    $scope.edt.search = '';
                }
                $scope.table1 = true;
                $scope.filteredTodos = [];
                $scope.pagesize = '5';
                $scope.currentPage = 1;
                $scope.numPerPage = 5;
                $http.get(ENV.apiUrl + "api/PrintVoucher/getAllVoucherForPrint?from_date=" + $scope.edt.from_date + "&to_date=" + $scope.edt.to_date + "&search=" + $scope.edt.search + "&comp_code=" + $scope.finnDetail.company).then(function (AllVoucherForPrint_Data) {
                    $scope.table1 = true;
                    $scope.VoucherForPrint_Data = AllVoucherForPrint_Data.data;
                    if ($scope.VoucherForPrint_Data.length > 0) {
                        $scope.pager = true;

                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.VoucherForPrint_Data.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.VoucherForPrint_Data.length, data: 'All' })
                        }

                        $scope.totalItems = $scope.VoucherForPrint_Data.length;
                        $scope.todos = $scope.VoucherForPrint_Data;
                        $scope.makeTodos();
                        $scope.table1 = true;
                        $scope.page_index = true;
                        $scope.currentPage = 1;
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                        $scope.page_index = false;
                        $scope.pager = false;
                        $scope.filteredTodos = [];
                    }
                });
            }
           
            $scope.Report = function (str) {
                debugger
               // $http.get(ENV.apiUrl + "api/StudentFee/GetReceipt").then(function (res) {
                   // $scope.reportparameter = res.data;
                    var data = {
                        location: $scope.total_counts[0].fins_appl_form_field_value1,
                        parameter: {
                           
                        doc_code:str.gltd_doc_code,
                        doc_no:str.gltd_final_doc_no

                        },
                        state: 'main.FINR15',
                        ready: function () {
                            this.refreshReport();
                        },
                    }
                
                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                    $state.go('main.ReportCardParameter')
               // });
            }

            $scope.check_searchnull = function () {
                chksearch = document.getElementById("chkmarks1");

                if (chksearch.checked == true) {
                    $scope.search_textbox = true;
                }
                else {
                    $scope.search_textbox = false;
                }
            }

            $scope.check_fromdate = function () {
                main1 = document.getElementById("chkmarks");

                if (main1.checked == true) {
                    $scope.edt.from_date = '';
                    $scope.disabled_fromdate = true;
                }
                else {
                    $scope.disabled_fromdate = false;
                    $scope.edt =
                          {
                              from_date: $scope.ddMMyyyy,
                              to_date: $scope.ddMMyyyy,
                          }
                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                debugger;
                $('input[type="text"]', $(this).parent()).focus();
            });
        }])
})();
