﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CreateEditFinancialPeriodCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
     
            $scope.display = false;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.grid = true;
            $scope.FinPeriod_data = [];
            $scope.edit_data = false;
            $scope.comp_edit = false;
            var comn_code;
            var data1 = [];
           // var date3;
            console.log($rootScope.globals.currentUser.username);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetFinancialPeriod").then(function (res) {
                $scope.FinPeriod_data = res.data;
                $scope.totalItems = $scope.FinPeriod_data.length;
                $scope.todos = $scope.FinPeriod_data;
                $scope.makeTodos();
                $scope.grid = true;
            });

            $scope.makeTodos = function ()
            {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.New = function ()
            {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
                $scope.comp_edit = false;
                $scope.edit_data = true;

                if ($rootScope.globals.currentUser.username != null)
                {
                    $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
                        $scope.comp_data = res.data;
                        console.log($scope.comp_data);
                    });
                }
                else
                {
                    $rootScope.globals.currentUser.username = null;
                }

            }


            $scope.edit = function (str)
            {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edt = str;
                $scope.comp_edit = true;
                $scope.edit_data = true;
                $scope.edt.prd_st_dt = convertdate($scope.edt.prd_st_dt);
                console.log($scope.edt);
                $scope.edt.prd_end_dt = convertdate($scope.edt.prd_end_dt);

                if ($rootScope.globals.currentUser.username != null)
                {
                    $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
                        $scope.comp_data = res.data;
                    });
                }
                else
                {
                    $rootScope.globals.currentUser.username = null;
                }
            }

            function convertdate(dt)
            {
                if (dt != null)
                {
                    var d1 = new Date(dt);
                    var month = d1.getMonth() + 1;
                    var day = d1.getDate();
                    if (month < 10)
                        month = "0" + month;
                    if (day < 10)
                        day = "0" + day;
                    var d = d1.getFullYear() + "-" + (month) + "-" + (day);
                    console.log(d);
                    $scope.convertdated = d;
                    return d;
                }
            }

            $scope.createdate = function (date)
            {
                var d1 = new Date(date);
                var month = d1.getMonth() + 1;
                var day = d1.getDate();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                var d = d1.getFullYear() + "-" + (month) + "-" + (day);

                $scope.edt.prd_st_dt = d;

                if (date != null)
                {
                    if (day != '01')
                    {
                        swal({ title: "Alert", text: "Start Date Will be First of This Month", imageUrl: "assets/img/check.png", });
                    }

                    var nextYear = new Date(date);
                    nextYear.setDate(nextYear.getDate() - 1);

                    var year1 = new Date(date).getFullYear() + 1;
                    var month1 = nextYear.getMonth() + 1;
                    var day1 = nextYear.getDate();
                    if (month1 < 10)
                        month1 = "0" + month1;
                    if (day1 < 10)
                        day1 = "0" + day1;
                    $scope.edt.prd_end_dt = year1 + "-" + month1 + "-" + day1;
                }
            }
                
            $scope.Save = function (isvalidate)
            {
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false)
                    {
                        var d1 = new Date($scope.edt.prd_st_dt);
                        var year = d1.getFullYear();

                        $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getCheckFinancialPeriod?comp_code=" + $scope.edt.dept_comp_code + "&year=" + year).then(function (res) {
                            $scope.chkfinperiod = res.data;
                            console.log($scope.chkfinperiod);

                            if ($scope.chkfinperiod == '0') {
                                swal({ title: "Alert", text: "Record Not Exist in Financial Year", imageUrl: "assets/img/check.png", });
                                $scope.edt = "";
                            }
                            if ($scope.chkfinperiod == '2') {
                                swal({ title: "Alert", text: "Record Already Exist For this Financial Year", imageUrl: "assets/img/check.png", });
                                $scope.edt = "";
                            }
                            if ($scope.chkfinperiod == '1') {
                                var data = ({
                                    dept_comp_code: $scope.edt.dept_comp_code,
                                    prd_year: year,
                                    prd_st_dt: $scope.edt.prd_st_dt,
                                    prd_end_dt: $scope.edt.prd_end_dt,
                                    status: $scope.edt.status,
                                    opr: 'I'
                                });
                                data1.push(data);

                                $http.post(ENV.apiUrl + "api/common/FinancialPeriod/CUDFinancialPeriod", data1).then(function (res) {
                                    $scope.display = true;
                                    $scope.msg1 = res.data;
                                    if ($scope.msg1 == true) {
                                        swal({ title: "Alert", text: "Financial Period Data Added Successfully", imageUrl: "assets/img/check.png", },
                                        function () {
                                            $scope.getgrid();
                                        });
                                    }
                                    else {
                                        swal({ title: "Alert", text: "Financial Period Data Not Added Successfully", imageUrl: "assets/img/notification-alert.png", });
                                    }
                                });

                            }
                        });
                    }
                    else
                    {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetFinancialPeriod").then(function (res) {
                    $scope.FinPeriod_data = res.data;
                    $scope.totalItems = $scope.FinPeriod_data.length;
                    $scope.todos = $scope.FinPeriod_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                    $scope.display = false;
                });
            }


            $scope.Update = function (isvalidate)
            {
                var data1 = [];
                if (isvalidate)
                {
                    var d1 = new Date($scope.edt.prd_st_dt);
                    var year = d1.getFullYear();

                    var data = ({
                        dept_comp_code: $scope.edt.dept_comp_code,
                        prd_year: year,
                        prd_st_dt: $scope.edt.prd_st_dt,
                        prd_end_dt: $scope.edt.prd_end_dt,
                        status: $scope.edt.status,
                        opr: 'U'
                    });
                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/FinancialPeriod/CUDFinancialPeriod",data1).then(function (res) {
                        $scope.display = true;
                        $scope.grid = false;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true)
                        {
                            swal({ title: "Alert", text: "Financial Period Data Updated Successfully", imageUrl: "assets/img/check.png", },
                            function () {
                                $scope.getgrid();
                            });
                        }
                        else
                        {
                            swal({ title: "Alert", text: "Financial Period Data Not Updated Successfully", imageUrl: "assets/img/notification-alert.png", });
                        }
                    });
                }
            }

            $scope.cancel = function ()
            {
                $scope.grid = true;
                $scope.display = false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }
        }])
})();