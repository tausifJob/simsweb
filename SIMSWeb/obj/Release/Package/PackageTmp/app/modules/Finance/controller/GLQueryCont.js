﻿(function () {
    'use strict';
    var main, temp;
    var data1 = [];
    var data2 = [];
    var block1 = [];
    var status = "";
    var msg_flag1 = false;
    var prdocdata = [];
    var acconcode = "";
    var cmbvalue = "";
    var comp_code = "1";
    var data = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('GLQueryCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '5';
            var user = $rootScope.globals.currentUser.username;
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            console.log($scope.finnDetail)
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            $scope.block1 = {};

            setTimeout(function () {
                $("#cmb_acc_Code3").select2();

            }, 100);


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
                $("#Table1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


            $http.get(ENV.apiUrl + "api/JVCreation/getDocCodeJV?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                $scope.geDocCode_jv = res.data;
            });

            $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code).then(function (docstatus) {
                debugger;
                $scope.getAllAccNos = docstatus.data;
            });

            $scope.FetchOPBal = function (str) {
                //Code sgr
                debugger
                for (var i = 0; i < $scope.getAllAccNos.length; i++) {
                    if ($scope.getAllAccNos[i].gldd_acct_code == $scope.block1.gldd_acct_code) {

                        $scope.block1.sltr_yob_amt = $scope.getAllAccNos[i].sltr_yob_amt;
                    }
                }
                if ($scope.block1.sltr_yob_amt > 0)
                    $scope.opbal = $scope.block1.sltr_yob_amt + ' Dr';
                else if ($scope.block1.sltr_yob_amt < 0)
                    $scope.opbal = Math.abs($scope.block1.sltr_yob_amt) + ' Cr';
                else
                    $scope.opbal = $scope.block1.sltr_yob_amt;


            }

            $scope.getViewDetails = function () {
                debugger;


                var data = {
                    gltr_comp_code: comp_code,
                    gltr_doc_code: ($scope.block1['gltd_doc_code'] != undefined || $scope.block1['gltd_doc_code'] != "") ? $scope.block1['gltd_doc_code'] : null,
                    gltr_acct_code: ($scope.block1.gldd_acct_code != undefined || $scope.block1.gldd_acct_code != "") ? $scope.block1.gldd_acct_code : null,
                    gltr_pstng_date: ($scope.block1.from_date != undefined || $scope.block1.from_date != "") ? $scope.block1.from_date : null,
                    gltr_doc_date: ($scope.block1.to_date != undefined || $scope.block1.to_date != "") ? $scope.block1.to_date : null,

                }
                var close_amt;

                $http.post(ENV.apiUrl + "api/GLSLQuery/list_All_GL_Query", data).then(function (res1) {
                    $scope.trans_data = res1.data;
                    $scope.length = $scope.trans_data.length - 1;

                    close_amt = parseFloat($scope.trans_data[$scope.length].gltr_total_dr_amount) - parseFloat($scope.trans_data[$scope.length].gltr_total_cr_amount);

                    if (close_amt > 0)
                        $scope.closebal = close_amt + ' Dr';
                    else if (close_amt < 0)
                        $scope.closebal = math.abs(close_amt) + ' Cr';
                    else
                        $scope.closebal = close_amt;


                });
            }




        }]
        )
})();
