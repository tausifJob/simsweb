﻿
(function () {
    'use strict';

    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CreateEditAccountCodeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.grid = true;
            $scope.save = true;
            var main = '';
            var user = $rootScope.globals.currentUser.username.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            console.log("Hello");


            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $http.get(ENV.apiUrl + "api/CreateEditAccount/getAllFinsAccountCodes?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                $scope.obj = res.data;
                $scope.totalItems = $scope.obj.length;
                $scope.todos = $scope.obj;
                $scope.makeTodos();
            });

            $http.get(ENV.apiUrl + "api/CreateEditAccount/getScheduleCode?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (ScheduleCode) {
                $scope.Schedule_Code = ScheduleCode.data;
            });

            $http.get(ENV.apiUrl + "api/CreateEditAccount/getAllAccountTypes?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (AllAccountTypes) {
                $scope.All_Account_Types = AllAccountTypes.data;
            });

            $http.get(ENV.apiUrl + "api/CreateEditAccount/getAllParentAccounts?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (AllParentAccounts) {
                $scope.All_Parent_Accounts = AllParentAccounts.data;


            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.Reset = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
                $scope.dis_acct_code = false;
            }

            $scope.check = function () {
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].glac_acct_code;
                    var v = document.getElementById(t);
                    if (main.checked == true) {
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }

                    else {
                        v.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.check1 = function (str) {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.dis_acct_code = false;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            var data = [];

            $scope.Save = function (isvalid) {
                if (isvalid) {

                    debugger;
                    if ($scope.edt.glac_acct_code != "" && $scope.edt.glac_acct_code != undefined) {
                        $scope.save = true;
                        $scope.edt.opr = "I";
                        $scope.edt.comp_code = comp_code;
                        $scope.edt.glac_year = finance_year;

                        data.push($scope.edt);

                        $http.post(ENV.apiUrl + "api/CreateEditAccount/CUDAccountCodeDetails", data).then(function (msg) {
                            $scope.display = true;
                            data = [];
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal('', 'Account Created Successfully');

                                $http.get(ENV.apiUrl + "api/CreateEditAccount/getAllFinsAccountCodes?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                                    $scope.obj = res.data;
                                    $scope.totalItems = $scope.obj.length;
                                    $scope.todos = $scope.obj;
                                    $scope.makeTodos();
                                });

                            }
                            else {
                                swal('', 'Account Not Created');
                            }
                        });
                    }
                }
                else {

                    swal('', 'Please Insert Code');
                }
            }

            var deletedata = [];

            $scope.Delete = function () {
                var flag = false;
                $scope.save = true;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].glac_acct_code;
                    var v = document.getElementById(t);
                    if (v.checked == true) {
                        flag = true;
                        $scope.filteredTodos[i].opr = "D";
                        $scope.filteredTodos[i].comp_code = comp_code;
                        $scope.filteredTodos[i].glac_year = finance_year;

                        deletedata.push($scope.filteredTodos[i]);
                    }
                }

                if (flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/CreateEditAccount/CUDAccountCodeDetails", deletedata).then(function (msg) {
                                $scope.display = true;
                                deletedata = [];
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal('', 'Account Deleted Successfully');
                                    $http.get(ENV.apiUrl + "api/CreateEditAccount/getAllFinsAccountCodes?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                                        $scope.obj = res.data;
                                        $scope.totalItems = $scope.obj.length;
                                        $scope.todos = $scope.obj;
                                        $scope.makeTodos();
                                    });
                                }
                                else {
                                    swal({ text: 'Account Not Deleted', width: 380 });
                                }
                            });
                        }
                    });

                }

                else {

                    swal({ text: 'Select At Least One Record To Delete', width: 380 });
                }

            }

            $scope.edit = function (str) {
                $scope.save = false;
                $scope.dis_acct_code = true;
                $scope.edt = {
                    glac_acct_code: str.glac_acct_code,
                    glac_name: str.glac_name,
                    glac_status: str.glac_status,
                    glac_type_code: str.glac_type1,
                    glac_parent_acct_code: str.glac_parent_acct1,
                    glac_sched_code: str.glac_sched_code1,
                }

                console.log($scope.edt);
            }

            $scope.Update = function () {
                $scope.edt.opr = "U";
                $scope.edt.comp_code = comp_code;
                $scope.edt.glac_year = finance_year;

                data.push($scope.edt);

                $http.post(ENV.apiUrl + "api/CreateEditAccount/CUDAccountCodeDetails", data).then(function (msg) {
                    $scope.display = true;
                    data = [];
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal('', 'Account Data Updated Successfully');
                        
                        $http.get(ENV.apiUrl + "api/CreateEditAccount/getAllFinsAccountCodes?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                            $scope.obj = res.data;
                            $scope.totalItems = $scope.obj.length;
                            $scope.todos = $scope.obj;
                            $scope.makeTodos();
                        });

                        $scope.Reset();
                    }
                    else {
                        swal('', 'Account Data Not Updated');
                    }
                });
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.glac_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.glac_acct_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.glac_sched_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }


        }])
})();