﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SLQueryCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.edt = "";
            $scope.operation = true;
            $scope.table1 = false;
            $scope.addbtn = true;
            $scope.btnDelete = false;
            $scope.users = [];
            var comp_code = "1";
            $scope.chkcost_Ac = false;
            $scope.data = [];
            $scope.Bankdetails = "";
            $scope.Bankdetails = [];
            $scope.chkcost = false;
            $scope.cost_combo = false;
            $scope.crAmount = 0;
            $scope.TotalAmount = 0;
            $scope.drAmount = 0;
            $scope.edt5 = [];
            debugger
            var user = $rootScope.globals.currentUser.username.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=Single").then(function (res) {
                $scope.final_doc_url = res.data;
                console.log($scope.final_doc_url);
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $http.get(ENV.apiUrl + "api/BankPayment/getDocCode?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {

                $scope.geDocCode = res.data;
                for (var i = 0; i < $scope.geDocCode.length; i++) {
                    if ($scope.geDocCode[i].gltd_doc_code == "BP") {
                        $scope.getDocCode = $scope.geDocCode[i].gltd_doc_code;
                    }
                }
                $scope.temp3 = {
                    gltd_doc_code: $scope.getDocCode
                }
                $http.get(ENV.apiUrl + "api/BankPayment/GetAllCashBankGLMasterAccountNo?cmp_cd=" + comp_code + "&financialyear=" + finance_year + "&doc_cd=" + $scope.getDocCode + "&username=" + user).then(function (docstatus2) {
                    debugger
                    $scope.bankname = docstatus2.data
                    $scope.edt5['master_acno'] = docstatus2.data[0];
                    $scope.getAccountName(docstatus2.data[0].master_ac_cdname)

                    //    $scope.edt5={

                    //        master_acno: $scope.bankname[0].master_acno,
                    //        master_ac_cdname: $scope.bankname[0].master_ac_cdname
                    //}
                    //    $scope.getAccountName($scope.edt5.master_acno);
                });
            });

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckPrintReport = function () {


                var data = {
                    location: $scope.final_doc_url,
                    parameter: {
                        doc_code: $scope.temp3.gltd_doc_code,
                        doc_no: $scope.Docno,

                    },

                    state: 'main.Fin141'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')


            }
            $scope.New = function () {
                $scope.operation = true;
                $scope.addbtn = true;
                $scope.updatebtn = false;
                $scope.Delete = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

          
            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = yyyy + '-' + mm + '-' + dd;
            $scope.edate = yyyy + '-' + mm + '-' + dd;
            $scope.edt4 = {
                DocDate: yyyy + '-' + mm + '-' + dd,
                PostDate: yyyy + '-' + mm + '-' + dd,
                ChequeDate: yyyy + '-' + mm + '-' + dd,
            }

            $scope.getSLAccNo = function (slcode) {

                $scope.getAllAccNos = [];
                var acconcode = $scope.getAllAccNos.gldd_acct_code;
                if (slcode == "00") {
                    $scope.GetAllGLAcc(acconcode, comp_code);
                }
                else {

                    $http.get(ENV.apiUrl + "api/JVCreation/GetSLAccNumber?ldgr_code=" + slcode + "&cmp_cd=" + comp_code + "&year=" + finance_year).then(function (docstatus3) {
                        // $scope.sllc_ldgr_code = slcode;
                        $scope.getAllAccNos = docstatus3.data;
                        //$scope.slacno = docstatus3.data;
                    });

                    
                }
                $("#cmb_acc_Code3").select2("val", cmbvalue);
            }

            setTimeout(function () {
                $("#cmb_acc_Code3").select2();

            }, 100);

          
            $http.get(ENV.apiUrl + "api/JVCreation/getDocCodeJV?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                $scope.geDocCode_jv = res.data;
            });


            $scope.getDepartcode = function (ledgercode, accountcode) {
                debugger
               
               
                $http.get(ENV.apiUrl + "api/GLSLQuery/getdataAccountChange?compcode=" + comp_code + "&financial_year=" + finance_year + "&ledger_code=" + ledgercode + "&slma_acno=" + accountcode).then(function (res) {
                    $scope.accountData = res.data;
                    console.log($scope.accountData);
                });



            }


            $scope.Show = function (ledgercode, accountcode, doc_code, from_date, todate) {
                debugger;


                if (ledgercode != undefined && accountcode != undefined) {

                    debugger;
                    $http.get(ENV.apiUrl + "api/GLSLQuery/getdataAllShow?compcode=" + comp_code + "&ledger_code=" + ledgercode + "&slma_acno=" + accountcode + "&doc_code=" + doc_code + "&from_date=" + from_date + "&to_date=" + todate).then(function (res) {
                        $scope.accountDatashow = res.data;
                        debugger
                        if ($scope.accountDatashow.length > 0) {
                            $scope.table1 = true;
                            $scope.drAmount = 0;
                            $scope.crAmount = 0;
                            $scope.TotalAmount = 0;
                            for (var i = 0; i < $scope.accountDatashow.length; i++) {
                                $scope.drAmount = parseFloat($scope.drAmount) + parseFloat($scope.accountDatashow[i].sltr_dr_amount);
                                $scope.crAmount = parseFloat($scope.crAmount) + parseFloat($scope.accountDatashow[i].sltr_cr_amount);
                                $scope.TotalAmount = parseFloat($scope.TotalAmount) + parseFloat($scope.accountDatashow[i].pb_amount);
                            }

                            if ($scope.drAmount == "NaN") {
                                $scope.drAmount = 0;
                            }
                            else if ($scope.crAmount == "NaN") {
                                $scope.crAmount = 0;
                            }
                            else if ($scope.TotalAmount == "NaN") {
                                $scope.TotalAmount = 0;
                            }
                        }
                        else {
                            $scope.table1 = false;
                            swal({ title: "Alert", text: "Data Not Found", showCloseButton: true, width: 300, height: 200 });
                            $scope.drAmount = 0;
                            $scope.crAmount = 0;
                            $scope.TotalAmount = 0;
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select LedgerCode and Account Number", showCloseButton: true, width: 300, height: 200 });
                }
            }
            //Cheque Detail
            $scope.Cheque_Details = function (ledgercode, accountcode)
            {
                if (ledgercode != undefined && accountcode != undefined) {
                    debugger;
                    $http.get(ENV.apiUrl + "api/GLSLQuery/getChequeDetails?compcode=" + comp_code + "&ledger_code=" + ledgercode + "&slma_acno=" + accountcode).then(function (res) {
                        $scope.ChequeDatashow = res.data;
                        console.log($scope.ChequeDatashow);
                    })
                    $('#MyModal2').modal('show');
                }
                else {
                        swal({ title: "Alert", text: "Please Select LedgerCode and Account Number", showCloseButton: true, width: 300, height: 200 });
                }
            }

            //Bill Details
            $scope.Bill_Details = function (ledgercode, accountcode) {
                debugger;
                if (ledgercode != undefined && accountcode != undefined) {
                    $http.get(ENV.apiUrl + "api/GLSLQuery/getBillDetails?compcode=" + comp_code + "&ledger_code=" + ledgercode + "&slma_acno=" + accountcode).then(function (res) {
                        $scope.billDatashow = res.data;
                        console.log($scope.billDatashow);
                    })
                    $('#MyModal3').modal('show');
                }
                else {
                    swal({ title: "Alert", text: "Please Select LedgerCode and Account Number", showCloseButton: true, width: 300, height: 200 });
                }
            }


            //Fill Combo Ledger
            $http.get(ENV.apiUrl + "api/BankPayment/GetLedgerNumber?financialyear=" + finance_year + "&comp_code=" + comp_code).then(function (docstatus2) {
                $scope.LdgrCode = docstatus2.data;
                console.log($scope.LdgrCode);
            });




          
        }]
        )
})();
