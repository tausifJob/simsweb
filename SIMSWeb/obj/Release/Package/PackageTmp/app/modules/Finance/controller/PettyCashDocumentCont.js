﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('PettyCashDocumentCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.temp = [];
            $scope.edt = [];
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            debugger
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            console.log($scope.finnDetail)
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
            var user = $rootScope.globals.currentUser.username;



            $http.get(ENV.apiUrl + "api/JVCreation/getDocCodeJV_DPS?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                debugger
                $scope.geDocCode_jv = res.data;
            });

            $http.get(ENV.apiUrl + "api/BankMaster/getAllPettyCashDocs").then(function (res) {
                debugger
                $scope.getPettyCashDetails = res.data;
                $scope.totalItems = $scope.getPettyCashDetails.length;
                $scope.todos = $scope.getPettyCashDetails;
                $scope.makeTodos();

            });

            $scope.Imbursement=function(str)
            {
                debugger;
                //if (str == 'Imb_true')
                //{
                //    $scope.edt=
                //    {'glpdc_type':true}
                //}
                //else {
                //    $scope.edt =
                //   { 'glpdc_type': true}
                //}
            }
            

            //$http.get(ENV.apiUrl + "api/GoalTarget/getAcademicYear").then(function (Acyear) {
            //    $scope.Acc_year = Acyear.data;
            //    console.log($scope.Acc_year);
            //});
            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                main.checked = false;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Bankmaster, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Bankmaster;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.pb_bank_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.pb_bank_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.pb_srl_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.pb_gl_acno.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.pb_gl_acno == toSearch) ? true : false;
            }

            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "/" + month + "/" + day;
            }


            //NEW BUTTON
            $scope.New = function () {

                $scope.doc_code = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;

                $scope.temp =
                     { 'glpdc_year': $scope.finnDetail.year }
            }

            //EnableDesable

            $scope.setvisible = function () {
                debugger;
                $scope.ldgno = true;
                $scope.slno = true;
            }


            $scope.getSelectData=function()
            {
                 $http.get(ENV.apiUrl + "api/BankMaster/getAllPettyCashDocs").then(function (res) {
                     debugger
                     $scope.getPettyCashDetails = res.data;
                     $scope.totalItems = $scope.getPettyCashDetails.length;
                     $scope.todos = $scope.getPettyCashDetails;
                     $scope.makeTodos();
                 });
            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger;
                if (Myform) {
                    var data =
                        {
                            'glpdc_comp_code':comp_code,
                            'glpdc_year': $scope.finnDetail.year,
                            'glpdc_doc_code': $scope.temp.gltd_doc_code,
                            'glpdc_type': $scope.edt.glpdc_type,
                            opr : 'I'
                         }
                          
                             dataforSave.push(data);
                             $http.post(ENV.apiUrl + "api/BankMaster/CUDPettyCashDocument", dataforSave).then(function (msg) {
                                 $scope.msg1 = msg.data;
                                 if ($scope.msg1 == true) {
                                     swal({ title: "Alert", text: "Record Inserted Successfully", showCloseButton: true, width: 300, height: 200 });
                                     $scope.getSelectData();
                                 }
                                 else {
                                     swal({ title: "Alert", text: "Record Already Present", showCloseButton: true, width: 300, height: 200 });
                                 }
                                
                                 $scope.getSelectData();
                             });
                        datasend = [];
                        $scope.display = false;
                        $scope.table = true;
                }
            }

            //Date FORMAT
            $scope.showdate = function (date, name1) {
                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "-" + month + "-" + day;
            }
            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }
            //DATA EDIT
            $scope.edit = function (str) {
                debugger;

                $scope.Academic_year = true;
                $scope.doc_code = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
               
                $scope.temp = {
                        glpdc_year: str.glpdc_year
                     , gltd_doc_code: str.glpdc_doc_code
                    
                };
                $scope.edt=
                    {
                      glpdc_type: str.glpdc_type
                    }

            }


            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                debugger
                if (Myform) {
                    var data =
                        {
                            'glpdc_comp_code': comp_code,
                            'glpdc_year': $scope.finnDetail.year,
                            'glpdc_doc_code': $scope.temp.gltd_doc_code,
                            'glpdc_type': $scope.edt.glpdc_type,
                            opr: 'I'
                        }
                    dataforUpdate.push(data);
                    //dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/BankMaster/CUDPettyCashDocument", dataforUpdate).then(function (msg) {

                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", showCloseButton: true, width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Same signature record already present", showCloseButton: true, width: 300, height: 200 });
                        }
                        $http.get(ENV.apiUrl + "api/BankMaster/getAllPettyCashDocs").then(function (res) {
                            debugger
                            $scope.getPettyCashDetails = res.data;
                            $scope.totalItems = $scope.getPettyCashDetails.length;
                            $scope.todos = $scope.getPettyCashDetails;
                            $scope.makeTodos();
                        });
                    });
                    data2 = [];
                    $scope.display = false;
                    $scope.table = true;
                }
            }

            //DELETE RECORD
            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].glpdc_doc_code);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].glpdc_doc_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }




            $scope.OkDelete = function () {
                debugger;
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i+ $scope.filteredTodos[i].glpdc_doc_code);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'glpdc_comp_code': $scope.filteredTodos[i].glpdc_comp_code,
                            'glpdc_year': $scope.filteredTodos[i].glpdc_year,
                            'glpdc_doc_code': $scope.filteredTodos[i].glpdc_doc_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/BankMaster/CUDPettyCashDocument", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BankMaster/getAllPettyCashDocs").then(function (res) {
                                                debugger
                                                $scope.getPettyCashDetails = res.data;
                                                $scope.totalItems = $scope.getPettyCashDetails.length;
                                                $scope.todos = $scope.getPettyCashDetails;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BankMaster/getAllPettyCashDocs").then(function (res) {
                                                debugger
                                                $scope.getPettyCashDetails = res.data;
                                                $scope.totalItems = $scope.getPettyCashDetails.length;
                                                $scope.todos = $scope.getPettyCashDetails;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i +$scope.filteredTodos[i].glpdc_doc_code);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");

                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });






        }])

})();
