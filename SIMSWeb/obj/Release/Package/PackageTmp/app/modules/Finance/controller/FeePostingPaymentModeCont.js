﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main;
    var finanacecode = [];
    var deletefin = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('FeePostingPaymentModeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/FeePosting/getFeePosting").then(function (res1) {
                $scope.feepost = res1.data;
                $scope.totalItems = $scope.feepost.length;
                $scope.todos = $scope.feepost;
                $scope.makeTodos();

            });

            //Fill Combo PaymentMode
            $http.get(ENV.apiUrl + "api/FeePosting/GetPaymentMode").then(function (paymode) {
                debugger;
                $scope.pay_mode = paymode.data;
                console.log($scope.docstatus1);
            });



            //Fill Combo GLAccountNo
            $http.get(ENV.apiUrl + "api/FeePosting/GetGLAccountNumber").then(function (docstatus1) {
                $scope.GlACNO = docstatus1.data;
                console.log($scope.docstatus1);
            });


            //Currcode
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (currname) {
                $scope.Cname = currname.data;
                console.log($scope.docstatus1);
            });

            //Fill Combo AcademicYear
            $scope.getacyr = function (str) {

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                })
            }


            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.feepost, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.feepost;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.fins_fee_payment_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.fins_fee_acno.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "/" + month + "/" + day;
            }


            //NEW BUTTON
            $scope.New = function () {

                $scope.disabled = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = "";
                $scope.temp = {};
                $scope.temp['fins_fee_posting_status'] = true;
                $scope.currcode1 = false;
                $scope.acdyear1 = false;
                $scope.Ptype1 = false;
            }

            //EnableDesable

            $scope.setvisible = function () {
                debugger;
                $scope.ldgno = true;
                $scope.slno = true;
            }


            //DATA SAVE INSERT
            var dataforSave = [];
            $scope.savedata = function (Myform) {
                debugger;
                dataforSave = [];
                data = [];
                debugger;
                if (Myform) {
                    var data = $scope.temp;
                    data.opr = 'I';

                    //data.pb_comp_code = '2016';
                    dataforSave.push(data);
                    $http.post(ENV.apiUrl + "api/FeePosting/CUDFeePostingPaymentMode", dataforSave).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: $scope.msg1.strMessage, width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: $scope.msg1.strMessage, width: 300, height: 200 });
                        }

                        $http.get(ENV.apiUrl + "api/FeePosting/getFeePosting").then(function (res1) {
                            $scope.feepost = res1.data;
                            $scope.totalItems = $scope.feepost.length;
                            $scope.todos = $scope.feepost;
                            $scope.makeTodos();
                            $scope.table = true;
                            $scope.display = false;
                        });

                    });
                    dataforSave = [];

                }
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger;
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;

                // $scope.temp = str;
                var fins_fee_acno_old = 'fins_fee_acno_old';

                $scope.temp = {
                    sims_cur_code: str.fins_fee_cur_code

                  , sims_academic_year: str.fins_fee_academic_year

                  , sims_appl_parameter: str.fins_fee_payment_type

                  , acno: str.fins_fee_acno

                 , fins_fee_posting_status: str.fins_fee_posting_status

                   , fins_fee_acno_old: str.fins_fee_acno
                };
                $scope.getacyr();

                $scope.currcode1 = true;
                $scope.acdyear1 = true;
                $scope.Ptype1 = true;

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function () {
                var data = $scope.temp;

                debugger;
                data = {
                    opr: "U",
                    sims_cur_code: $scope.temp.sims_cur_code,
                    sims_academic_year: $scope.temp.sims_academic_year,
                    sims_appl_parameter: $scope.temp.sims_appl_parameter,
                    acno: $scope.temp.acno,
                    fins_fee_acno_old: $scope.temp.fins_fee_acno_old,
                    fins_fee_posting_status: $scope.temp.fins_fee_posting_status


                };
                dataforUpdate.push(data);
                //dataupdate.push(data);

                $http.post(ENV.apiUrl + "api/FeePosting/CUDFeePostingPaymentMode", dataforUpdate).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: $scope.msg1.strMessage, width: 300, height: 200 });
                    }
                    else {
                        swal({ title: "Alert", text: $scope.msg1.strMessage, width: 300, height: 200 });
                    }
                    $http.get(ENV.apiUrl + "api/FeePosting/getFeePosting").then(function (res1) {
                        $scope.feepost = res1.data;
                        $scope.totalItems = $scope.feepost.length;
                        $scope.todos = $scope.feepost;
                        $scope.makeTodos();
                        $scope.table = true;
                        $scope.display = false;
                    });

                });
                dataforUpdate = [];


            }

            //DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].fins_fee_cur_name + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].fins_fee_cur_name + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            //Delete

            //$scope.OkDelete = function () {
            //    deletefin = [];
            //    swal ( {
            //        title: '',
            //        text: "Are you sure you want to Delete?",
            //        showCloseButton: true,
            //        showCancelButton: true,
            //        confirmButtonText: 'Yes',
            //        width: 380,
            //        cancelButtonText: 'No',
            //    },
            //   function (isConfirm) {
            //       if (isConfirm) {
            //           debugger;
            //           // $scope.temp.pb_comp_code = "1";
            //           for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //               var v = document.getElementById(i);
            //               if (v.checked == true) {
            //                   var deletemodulecode = ({
            //                       'sims_cur_code': $scope.filteredTodos[i].fins_fee_cur_code,
            //                       'sims_academic_year': $scope.filteredTodos[i].fins_fee_academic_year,
            //                       'sims_appl_parameter': $scope.filteredTodos[i].fins_fee_payment_type,
            //                       'acno': $scope.filteredTodos[i].fins_fee_acno,
            //                       opr: 'D'
            //                   });
            //                   deletefin.push(deletemodulecode);
            //               }
            //           }
            //           $http.post(ENV.apiUrl + "api/FeePosting/CUDFeePostingPaymentMode", deletefin).then(function (msg) {
            //               $scope.msg1 = msg.data;
            //               if ($scope.msg1 == true) {
            //                   swal({ title: "", text: "Record Deleted Successfully", showCloseButton: true, width: 380,});
            //                   $http.get(ENV.apiUrl + "api/FeePosting/getFeePosting").then(function (res1) {
            //                       $scope.feepost = res1.data;
            //                       $scope.totalItems = $scope.feepost.length;
            //                       $scope.todos = $scope.feepost;
            //                       $scope.makeTodos();
            //                       $scope.table = true;
            //                       $scope.display = false;
            //                   });
            //               }
            //               else {
            //                      swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380,});
            //                      main.checked = false;
            //               }
            //           });
            //       } 
            //    deletefin = [];
            //   });
            //}

            $scope.OkDelete = function () {
                debugger;
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].fins_fee_cur_name + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_cur_code': $scope.filteredTodos[i].fins_fee_cur_code,
                            'sims_academic_year': $scope.filteredTodos[i].fins_fee_academic_year,
                            'sims_appl_parameter': $scope.filteredTodos[i].fins_fee_payment_type,
                            'acno': $scope.filteredTodos[i].fins_fee_acno,
                            'fins_fee_posting_status': $scope.filteredTodos[i].fins_fee_posting_status,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger
                            $http.post(ENV.apiUrl + "api/FeePosting/CUDFeePostingPaymentMode", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: $scope.msg1.strMessage, width: 300, height: 200 }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/FeePosting/getFeePosting").then(function (res1) {
                                                $scope.feepost = res1.data;
                                                $scope.totalItems = $scope.feepost.length;
                                                $scope.todos = $scope.feepost;
                                                $scope.makeTodos();
                                                $scope.table = true;
                                                $scope.display = false;
                                            });

                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: $scope.msg1.strMessage, width: 300, height: 200 }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/FeePosting/getFeePosting").then(function (res1) {
                                                $scope.feepost = res1.data;
                                                $scope.totalItems = $scope.feepost.length;
                                                $scope.todos = $scope.feepost;
                                                $scope.makeTodos();
                                                $scope.table = true;
                                                $scope.display = false;
                                            });

                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].fins_fee_cur_name + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;

            }




            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });


        }])

})();
