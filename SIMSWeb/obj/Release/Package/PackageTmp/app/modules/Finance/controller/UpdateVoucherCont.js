﻿(function () {
    'use strict';
    var main;
    var prdocdata = [];
    var data1 = [];
    var data = [];
    var gltd_doc_narr1 = null;
    var gltd_remarks1 = null;
    var flag = false;
    var comp_code;
    var finance_year;
    // var totalDebit = 0;
    // var totalcredit = 0;
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('UpdateVoucherCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.edt = "";
            $scope.operation = true;
            $scope.table1 = true;
            $scope.save1 = true;
            $scope.Add = true;
            $scope.norecord = false;
            debugger;
            //var comp_code = "1";
            //var finance_year = "2016";
            var user = $rootScope.globals.currentUser.username;
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
            $scope.comcode = comp_code;
            $scope.Update = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/UpdateVoucher/getDocCodeJV?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                debugger
                $scope.getDocCode = res.data;
                $scope.doCode = true;

            });

            $scope.getprovno = function () {
                debugger
                $http.get(ENV.apiUrl + "api/UpdateVoucher/getProvisionNo?doccode=" + $scope.edt.gltd_doc_code + "&comp_code=" + comp_code).then(function (pronum) {

                    $scope.prnumber = pronum.data;
                    // $scope.doCode = true;
                });
            }

            //$scope.getprovno = function () {
            //    debugger
            //    $http.get(ENV.apiUrl + "api/JVCreation/getAllRecordWithDocCodeAndProvisionNo?doccode=" + $scope.edt.gltd_doc_code).then(function (pronum) {

            //        $scope.prnumber = pronum.data;
            //        // $scope.doCode = true;
            //    });
            //}

            $scope.Show = function (doccode, prnumber, comp_code) {
                if ($scope.edt.gltd_doc_code == undefined || $scope.edt.gltd_doc_code == "") {
                    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please select Document Code", showCloseButton: true, width: 380, });

                }
                else if ($scope.edt.gltd_prov_doc_no == undefined || $scope.edt.gltd_prov_doc_no == "") {
                    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please select Provision Number", showCloseButton: true, width: 380, });
                }
                else {

                    $http.get(ENV.apiUrl + "api/UpdateVoucher/getAllRecords?doccode=" + $scope.edt.gltd_doc_code +
                        "&prnumber=" + $scope.edt.gltd_prov_doc_no + "&comp_code=" + $scope.comcode).then(function (prdoc_data) {
                            debugger;
                            $scope.prdocdata = prdoc_data.data;
                            console.log($scope.prdocdata);

                            $scope.totalDebit = 0;
                            $scope.totalcredit = 0;
                            for (var i = 0; i < $scope.prdocdata.length; i++) {
                                debugger;
                                if (i == 0) {
                                    $scope.gltd_doc_narr1 = $scope.prdocdata[i].gltd_doc_narr;
                                    $scope.gltd_remarks1 = $scope.prdocdata[i].gltd_remarks;
                                    $scope.edt.gltd_doc_date = $scope.prdocdata[i].gltd_doc_date;
                                    $scope.edt.gltd_post_date = $scope.prdocdata[i].gltd_post_date;
                                }
                                $scope.edt.gltd_doc_narr = $scope.gltd_doc_narr1;
                                $scope.edt.gltd_remarks = $scope.gltd_remarks1;
                                $scope.totalDebit = $scope.totalDebit + parseFloat($scope.prdocdata[i].gldd_doc_amount_debit);
                                //$scope.totalDebit = parseInt($scope.totalDebit) + parseInt($scope.prdocdata[i].gldd_doc_amount_debit);
                                console.log($scope.gldd_doc_amount_debit);
                                $scope.totalcredit = parseFloat($scope.totalcredit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_credit);
                                console.log($scope.gldd_doc_amount_credit);

                                // $scope.grandtotal = parseInt($scope.grandtotal) + parseInt($scope.AdmissionGradeFeesData[i].sims_grade_fee_amount);

                              
                            }

                        });


                }
            }


            $scope.Ok = function () {
                debugger;
                var data = [];
                $scope.flag = false;
                var datasend = [];
                for (var i = 0; i < $scope.prdocdata.length; i++) {
                    if (i == 0) {
                        if ($scope.edt.gltd_doc_narr == 'undefined' || $scope.edt.gltd_remarks == 'undefined') {
                            $scope.edt.gltd_doc_narr = "";
                            $scope.edt.gltd_remarks = "";
                        }
                        if ($scope.gltd_doc_narr1 != $scope.edt.gltd_doc_narr || $scope.gltd_remarks1 != $scope.edt.gltd_remarks) {
                            $scope.flag = true;
                        }
                    }

                    if ($scope.flag) {
                        $scope.data = ({
                            gldd_comp_code: $scope.prdocdata[i].gldd_comp_code,
                            gldd_doc_code: $scope.prdocdata[i].gldd_doc_code,
                            gltd_prov_doc_no: $scope.prdocdata[i].gltd_prov_doc_no,
                            gltd_final_doc_no: $scope.prdocdata[i].gltd_final_doc_no,
                            gltd_doc_narr: $scope.edt.gltd_doc_narr,
                            //gldd_doc_narr: $scope.prdocdata[i].gldd_doc_narr,
                            gltd_remarks: $scope.edt.gltd_remarks,
                            gldd_line_no: $scope.prdocdata[i].gldd_line_no,
                            opr: 'A',
                            opr_upd: 'TDC',
                        });
                    }
                    else {
                        $scope.data = ({
                            gldd_comp_code: $scope.prdocdata[i].gldd_comp_code,
                            gldd_doc_code: $scope.prdocdata[i].gldd_doc_code,
                            gltd_prov_doc_no: $scope.prdocdata[i].gltd_prov_doc_no,
                            gltd_final_doc_no: $scope.prdocdata[i].gltd_final_doc_no,
                            gltd_doc_narr: $scope.prdocdata[i].gldd_doc_narr,
                           // gldd_doc_narr: $scope.prdocdata[i].gldd_doc_narr,
                            gltd_remarks: $scope.prdocdata[i].gltd_remarks,
                            gldd_line_no: $scope.prdocdata[i].gldd_line_no,
                            opr: 'A',
                            opr_upd: 'TDL',
                        });
                    }

                    datasend.push($scope.data);
                }
               
                $http.post(ENV.apiUrl + "api/UpdateVoucher/UpdateVoucherCUD",datasend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: 'Alert', text: 'Record Updated Successfully', showCloseButton: true, width: 380 });
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Updated", showCloseButton: true, width: 380 });
                    }

                });

            }

            $http.get(ENV.apiUrl + "api/BankPayment/getInsertMesage").then(function (res) {
                debugger;
                $scope.insertmsg = res.data;
            });


        }]
        )
})();
