﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var temp;
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('UserAccountCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            
            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //Combobox Dependency

            $scope.getApplication = function (modcode) {
                $scope.app_Name = [];
                $http.get(ENV.apiUrl + "api/UserAudit/getApplicationName?modcode=" + modcode).then(function (appName) {
                    $scope.app_Name = appName.data;
                    console.log($scope.app_Name);
                });
            }

            //Fill Combo ComapanyName
            $http.get(ENV.apiUrl + "api/common/GetComapanyName").then(function (compname) {

                $scope.Cname = compname.data;
                console.log($scope.docstatus1);
            });
            //GetYear
            $http.get(ENV.apiUrl + "api/common/GetYear").then(function (compyear) {

                $scope.year = compyear.data;
                console.log($scope.docstatus1);
            });

            //GetDepartment

            $scope.Getdepartment = function () {

                $http.get(ENV.apiUrl + "api/UserControl/GetDepartment?comp_code=" + $scope.temp.comp_code).then(function(compdept) {

                    $scope.dept = compdept.data;
                    console.log($scope.docstatus1);
                });
            }
            //GetDocument
            $http.get(ENV.apiUrl + "api/UserControl/GetDocument").then(function (compdoc) {

                $scope.document = compdoc.data;
                console.log($scope.docstatus1);
            });

            //GetUserCode
            $http.get(ENV.apiUrl + "api/UserControl/GetUSerCode").then(function (compUcode) {

                $scope.Usercode = compUcode.data;
                console.log($scope.docstatus1);
            });
       
            //GetAccountCode

            $scope.GetAccountCode = function () {

                $http.get(ENV.apiUrl + "api/UserControl/GetAccountCode?comp_code=" + $scope.temp.comp_code + "&year=" + $scope.temp.fins_finance_year + "&dept_code=" + $scope.temp.gdua_dept_no).then(function (compAccCode) {

                    $scope.Accountcode = compAccCode.data;
                    console.log($scope.docstatus1);
                });

            }

           

            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/UserControl/getAllUserControl").then(function (res1) {
                $scope.UserData = res1.data;
                $scope.totalItems = $scope.UserData.length;
                $scope.todos = $scope.UserData;
                $scope.makeTodos();

            });




            //$http.get(ENV.apiUrl + "api/GoalTarget/getAcademicYear").then(function (Acyear) {
            //    $scope.Acc_year = Acyear.data;
            //    console.log($scope.Acc_year);
            //});
            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                  //  main.checked = false;
                });
                
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.UserData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.UserData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
              
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.gdua_dept_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                       item.gdua_doc_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                       item.acno == toSearch) ? true : false;
              //  main.checked = false;
            }

            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "/" + month + "/" + day;
            }


            //NEW BUTTON
            $scope.New = function () {
              
                $scope.disabled = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.compname1 = false;
                $scope.year1 = false;
                $scope.dept1 = false;
                $scope.document1 = false;

                $scope.temp = "";
                $scope.temp = {};
                $scope.temp['gdua_status'] = true;
             //   

            }

            //EnableDesable

            $scope.setvisible = function () {
                
                $scope.ldgno = true;
                $scope.slno = true;
            }


            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                if (Myform) {
                        var data = $scope.temp;
                        data.opr = 'I';
                        //data.pb_comp_code = '2016';
                        dataforSave.push(data);
                        $http.post(ENV.apiUrl + "api/UserControl/CUDUserAccount", dataforSave).then(function (msg) {
                            $scope.msg1 = msg.data;


                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Bank code allready present", width: 300, height: 200 });
                            }

                            $http.get(ENV.apiUrl + "api/UserControl/getAllUserControl").then(function (res1) {
                                $scope.UserData = res1.data;
                                $scope.totalItems = $scope.UserData.length;
                                $scope.todos = $scope.UserData;
                                $scope.makeTodos();

                          
                                $scope.table = true;
                                $scope.display = false;
                            });

                        });
                        datasend = [];
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();
                    }
                }
            

            //Date FORMAT
            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "-" + month + "-" + day;
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {

                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
               
                // $scope.temp = str;
                $scope.temp = {
                  comp_code: str.gdua_comp_code
                , fins_finance_year: str.gdua_year
                , gdua_dept_no: str.gdua_dept_no
                , gdua_doc_code: str.gdua_doc_code
                , gdua_acct_code: str.gdua_acct_code
                , gdua_doc_user_code: str.gdua_doc_user_code
                , gdua_status: str.gdua_status
                };

                //For Dependency
                $scope.GetAccountCode();
                $scope.Getdepartment();

                $scope.compname1 = true;
                $scope.year1 = true;
                $scope.dept1 = true;
                $scope.document1 = true;

            }


            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function () {
                debugger;
                var data = $scope.temp;
                data.opr = "U";
                dataforUpdate.push(data);
                
                $http.post(ENV.apiUrl + "api/UserControl/CUDUserAccount", dataforUpdate).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                    }

                    $http.get(ENV.apiUrl + "api/UserControl/getAllUserControl").then(function (res1) {
                        $scope.UserData = res1.data;
                        $scope.totalItems = $scope.UserData.length;
                        $scope.todos = $scope.UserData;
                        $scope.makeTodos();

                    });

                });
                dataforUpdate = [];
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }


            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            //DELETE

            //$scope.OkDelete = function () {
            //    deletefin = [];
            //    swal ( {
            //        title: "Are you sure?", text: "to Delete Record", width: 300, height: 200, showCancelButton: true,
            //        confirmButtonColor: "#DD6B55", confirmButtonText: "Yes", closeOnConfirm: false
            //    },
            //    function (isConfirm) {
            //        if (isConfirm) {
            //            // $scope.temp.pb_comp_code = "1";
            //            for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //                var v = document.getElementById(i);
            //                if (v.checked == true) {
            //                    var deletemodulecode = ({
            //                        'gdua_comp_code': $scope.filteredTodos[i].gdua_comp_code,
            //                        'gdua_year': $scope.filteredTodos[i].gdua_year,
            //                        'gdua_dept_no': $scope.filteredTodos[i].gdua_dept_no,
            //                        'gdua_doc_code': $scope.filteredTodos[i].gdua_doc_code,
            //                        'gdua_acct_code': $scope.filteredTodos[i].gdua_acct_code,
            //                        'gdua_doc_user_code': $scope.filteredTodos[i].gdua_doc_user_code,
            //                        opr: 'D'
            //                    });
            //                    deletefin.push(deletemodulecode);
            //                }
            //            }
            //            $http.post(ENV.apiUrl + "api/UserControl/CUDUserAccount", deletefin).then(function (msg) {
            //                $scope.msg1 = msg.data;
            //                if ($scope.msg1 == true) {
            //                    swal({ title: "Alert", text: "Record Deleted Successfully", width: 300, height: 200 });
            //                    $http.get(ENV.apiUrl + "api/UserControl/getAllUserControl").then(function (res1) {
            //                        $scope.UserData = res1.data;
            //                        $scope.totalItems = $scope.UserData.length;
            //                        $scope.todos = $scope.UserData;
            //                        $scope.makeTodos();
            //                    });
            //                }
            //                else {
            //                    swal({ title: "Alert", text: "Record Not Deleted", width: 300, height: 200 });
            //                    main.checked = false;
            //                }
            //            });
            //        }
            //    deletefin = [];
            //    });
            //}


            $scope.OkDelete = function () {
                debugger;
                var data1 = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'gdua_comp_code': $scope.filteredTodos[i].gdua_comp_code,
                            'gdua_year': $scope.filteredTodos[i].gdua_year,
                            'gdua_dept_no': $scope.filteredTodos[i].gdua_dept_no,
                            'gdua_doc_code': $scope.filteredTodos[i].gdua_doc_code,
                            'gdua_acct_code': $scope.filteredTodos[i].gdua_acct_code,
                            'gdua_doc_user_code': $scope.filteredTodos[i].gdua_doc_user_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/UserControl/CUDUserAccount", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/UserControl/getAllUserControl").then(function (res1) {
                                                $scope.UserData = res1.data;
                                                $scope.totalItems = $scope.UserData.length;
                                                $scope.todos = $scope.UserData;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/UserControl/getAllUserControl").then(function (res1) {
                                                $scope.UserData = res1.data;
                                                $scope.totalItems = $scope.UserData.length;
                                                $scope.todos = $scope.UserData;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                data1 = [];
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

        }])

})();
