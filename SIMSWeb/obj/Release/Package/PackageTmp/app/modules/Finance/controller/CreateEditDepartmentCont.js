﻿(function () {
    'use strict';
    var obj1, obj2, main;
    var opr = '';
    var departmentcode = [];
    var formdata = new FormData();
    var data1 = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CreateEditDepartmentCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.DepartmentDetail = true;
            $scope.editmode = false;
            $scope.edt = "";
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);
            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.check_all();
            }

            $http.get(ENV.apiUrl + "api/CreateEditDepartment/getDepartType").then(function (res) {
                $scope.DepartType = res.data;
            });


            $http.get(ENV.apiUrl + "api/CreateEditDepartment/getCompany").then(function (res) {
                $scope.getCompany = res.data;
            });

            $http.get(ENV.apiUrl + "api/CreateEditDepartment/getDepartmentDetail").then(function (Depart_Data) {
                debugger;
                $scope.DepartData = Depart_Data.data;
                $scope.totalItems = $scope.DepartData.length;
                $scope.todos = $scope.DepartData;
                $scope.makeTodos();

                console.log($scope.DepartData);
            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.DepartData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.DepartData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.codp_dept_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.codp_dept_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.codp_dept_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.codp_short_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.codp_short_name == toSearch) ? true : false;
            }

            $scope.cancel = function () {
                $scope.DepartmentDetail = true;
                $scope.DepartmentOperation = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
            }

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.editmode = false;
                opr = 'S';
                $scope.readonly = false;
                $scope.DepartmentDetail = false;
                $scope.DepartmentOperation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = {};
                // $scope.edt['sims_board_status'] = true;
            }

            $scope.Save = function (myForm) {
                debugger
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'I';
                    $scope.exist = false;
                    data1.push(data);
                    for (var i = 0; i < $scope.DepartData.length; i++) {
                        if ($scope.DepartData[i].codp_dept_no == data.codp_dept_no && $scope.DepartData[i].codp_dept_name == data.codp_dept_name && $scope.DepartData[i].codp_short_name == data.codp_short_name) {
                            $scope.exist = true;
                        }

                    }
                    if ($scope.exist) {
                        swal({ title: "Alert", text: "Record Already Present", width: 300, height: 200 });
                    }

                    else {

                        $http.post(ENV.apiUrl + "api/CreateEditDepartment/DepartmentDetailCUD?simsobj=", data1).then(function (msg) {

                            $scope.DepartmentOperation = false;
                            $http.get(ENV.apiUrl + "api/CreateEditDepartment/getDepartmentDetail").then(function (Depart_Data) {
                                $scope.DepartData = Depart_Data.data;
                                $scope.totalItems = $scope.DepartData.length;
                                $scope.todos = $scope.DepartData;
                                $scope.makeTodos();
                                $scope.msg1 = msg.data;

                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                                }
                                else {
                                    swal({ title: "Alert", text: "Department Number Already Present", width: 300, height: 200 });
                                }

                            });

                        });

                        data1 = [];
                        $scope.DepartmentDetail = true;
                        $scope.DepartmentOperation = false;

                    }

                }
            }

            $scope.up = function (str) {
                opr = 'U';
                //$scope.edt = str;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.DepartmentDetail = false;
                $scope.DepartmentOperation = true;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.edt = {
                    codp_comp_code: str.codp_comp_code
                         , codp_dept_no: str.codp_dept_no
                         , codp_dept_name: str.codp_dept_name
                         , codp_short_name: str.codp_short_name
                         , codp_dept_type: str.codp_dept_type
                         , codp_year: str.codp_year
                         , codp_status: str.codp_status

                };
            }

            $scope.Update = function (myForm) {
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'U';

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/CreateEditDepartment/DepartmentDetailCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.DepartmentOperation = false;
                        $http.get(ENV.apiUrl + "api/CreateEditDepartment/getDepartmentDetail").then(function (Depart_Data) {
                            $scope.DepartData = Depart_Data.data;
                            $scope.totalItems = $scope.DepartData.length;
                            $scope.todos = $scope.DepartData;
                            $scope.makeTodos();

                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                            }
                        });

                    })
                    $scope.DepartmentOperation = false;
                    $scope.DepartmentDetail = true;
                    data1 = [];
                }
            }

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].codp_dept_no);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].codp_dept_no);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.check_once = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }

            }

            $scope.deleterecord = function () {
                debugger
                var departmentcode = [];
                var data1 = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i + $scope.filteredTodos[i].codp_dept_no);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'codp_dept_no': $scope.filteredTodos[i].codp_dept_no,
                            opr: 'D'
                        });
                        departmentcode.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/CreateEditDepartment/DepartmentDetailCUD", departmentcode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/CreateEditDepartment/getDepartmentDetail").then(function (getDepartment_Data) {
                                                $scope.getDepartmentData = getDepartment_Data.data;
                                                $scope.totalItems = $scope.getDepartmentData.length;
                                                $scope.todos = $scope.getDepartmentData;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Already mapped!", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/CreateEditDepartment/getDepartmentDetail").then(function (getDepartment_Data) {
                                                $scope.getDepartmentData = getDepartment_Data.data;
                                                $scope.totalItems = $scope.getDepartmentData.length;
                                                $scope.todos = $scope.getDepartmentData;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i + $scope.filteredTodos[i].codp_dept_no);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                data1 = [];
            }

        }]);

})();