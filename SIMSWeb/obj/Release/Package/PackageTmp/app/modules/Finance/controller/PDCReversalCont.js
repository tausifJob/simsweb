﻿(function () {
    'use strict';
    var main;
    var prdocdata = [];
    var data1 = [];
    var data = [], temp_doc=[];
    //var totalDebit=0;
    //var totalcredit=0;

    var simsController = angular.module('sims.module.Finance');
    simsController.controller('PDCReversalCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.edt = [];
            $scope.block1 = {};
            $scope.operation = true;
            $scope.table1 = true;
            $scope.save1 = true;
            $scope.Add = true;
            $scope.update = false;
            $scope.Update = false;
            var user = $rootScope.globals.currentUser.username;

            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            console.log($scope.finnDetail)
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            $scope.btn_reverse = false;
            //$scope.btn_verify = true;
            $scope.prdocdata = [];
            $scope.temp_doc = [];
            //$scope.schoolUrl = window.location.href;
            //var conStr = $scope.schoolUrl.substring(0, $scope.schoolUrl.indexOf('#'));
            //console.log(conStr);

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
                $("#fixedtable1").tableHeadFixer({ 'top': 1 });
            }, 100);


            $scope.SearchVouReverData = [];
            $scope.ref_docprvno = '';
            $scope.ref_final_doc_no = '';
            $scope.new_prov_data = '';
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $http.get(ENV.apiUrl + "api/VoucherReversal/getDocCodeJV?comp_code=1").then(function (res) {
                $scope.getDocCode = res.data;
            });

           
            $scope.ContraEntry = function (vcrrev) {
                $('#myModal_ContraEntry').modal('show');

                debugger
                $scope.btn_reverse = false;
                //$scope.btn_verify = true;
                $scope.ref_docprvno = '';
                $scope.ref_final_doc_no = '';
                $scope.lbl_recur = '';

                $http.get(ENV.apiUrl + "api/VoucherReversal/Get_vouchers_data?doc_cd=" + vcrrev.gltd_doc_code + "&final_doc_no=" + vcrrev.gltd_final_doc_no + "&usrname=" + user +
                   "&comp_code=" + comp_code + "&fyear=" + finance_year).then(function (voucher_data) {
                       debugger;
                       $scope.ref_docprvno = vcrrev.gltd_prov_doc_no;
                       $scope.ref_final_doc_no = vcrrev.gltd_final_doc_no;
                       $scope.lbl_recur = 'RV';

                       $scope.lbl_rev_doc = vcrrev.gltd_doc_code + '-' + vcrrev.gltd_final_doc_no;

                       $scope.SearchVouReverData = voucher_data.data;
                       //console.log($scope.SearchVouReverData);
                       $scope.edt = {
                           gltd_postdate: vcrrev.gltd_post_date,
                           gltd_docdate: yyyy + '-' + mm + '-' + dd,
                           //gltd_docdate: vcrrev.gltd_docdate,
                           gltd_doc_narr: vcrrev.gltd_doc_narr,
                           gltd_remarks: vcrrev.gltd_remarks,
                           gltd_doc_code: vcrrev.gltd_doc_code
                       }
                       $scope.totalDebit = 0;
                       $scope.totalcredit = 0;
                       for (var i = 0; i < $scope.SearchVouReverData.length; i++) {
                           $scope.totalDebit = parseInt($scope.totalDebit) + parseInt($scope.SearchVouReverData[i].gldd_doc_amount_debit);
                           $scope.totalcredit = parseInt($scope.totalcredit) + parseInt($scope.SearchVouReverData[i].gldd_doc_amount_credit);
                       }
                       $scope.amountdifference = $scope.totalcredit - $scope.totalDebit;
                   });
            }


            $scope.Show = function () {
                debugger
                //if (doc_prov_no == undefined)
                //    doc_prov_no = 0;
                //from_date = $scope.edt.from_date;
                //to_date = $scope.edt.to_date;
                $http.get(ENV.apiUrl + "api/VoucherReversal/getAllPDCvouchers?comp_cd=" + comp_code + "&doc_cd=" + $scope.edt.gltd_doc_code + "&final_doc_no=" + $scope.block1.gltd_final_doc_no +
                    "&from_date=" + $scope.block1.from_date + "&to_date=" + $scope.block1.to_date + "&cheque_no=" + $scope.block1.gltd_cheque_no).then(function (VouRever_Data) {
                        $scope.verify_data = VouRever_Data.data;
                        //console.log($scope.verify_data);
                    });
            }

            $scope.RevertVoucher = function () {
                if ($scope.totalDebit == $scope.totalcredit) {
                    debugger;
                    var data = {};
                    data.gltd_comp_code = comp_code;
                    data.gltd_doc_code = $scope.lbl_recur;
                    data.gltd_doc_date = $scope.edt.gltd_docdate;
                    data.gltd_post_date = $scope.edt.gltd_postdate;
                    data.gltd_doc_narr = $scope.edt.gltd_doc_narr;
                    data.gltd_remarks = $scope.edt.gltd_remarks;
                    data.gltd_prepare_user =user;
                    data.gltd_reference_doc_code = $scope.edt.gltd_doc_code;
                    data.gltd_reference_final_doc_no = $scope.ref_final_doc_no;

                    data.detailsdata = $scope.SearchVouReverData;
                    $http.post(ENV.apiUrl + "api/VoucherReversal/Insert_ReverseVoucher", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Voucher Reversed Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.btn_reverse = true;
                                    //$scope.btn_verify = false;
                                }
                            });
                        }
                        else {
                            swal({ title: "Alert", text: "Voucher Not Reversed", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.btn_reverse = false;
                                    //$scope.btn_verify = true;
                                }
                            });
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Debit and Credit amount do not match.Cannot Revert Voucher.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.btn_reverse = false;
                            //$scope.btn_verify = true;
                        }
                    });
                }
            }

            $scope.close = function () {
                debugger;
                $scope.edt = [];
                
                $scope.Show();
            }

            $scope.Verify = function () {
                debugger;
                var data = $scope.edt;
                data.gldd_doc_code = 'RV';
                data.gltd_prov_doc_no = $scope.new_prov_data;//$scope.edt.gltd_prov_doc_no;
                data.gltd_prepare_user = $rootScope.globals.currentUser.username;

                $http.post(ENV.apiUrl + "api/VoucherReversal/UpdateFins_temp_docs_current_status_sendtoverify", data).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {

                        swal({ title: "Alert", text: "Voucher Sent to Verification", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                $scope.btn_reverse = false;
                                //$scope.btn_verify = true;
                            }
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Record not updated.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                $scope.btn_reverse = false;
                                //$scope.btn_verify = true;
                            }
                        });
                    }
                });
            }


            $scope.Fetchdata = function (cmp_cd, docd, final_doc_no) {
                $scope.docCode1 = docd;
                $scope.provNo1 = final_doc_no;
                debugger;
                $('#myModal4').modal('show')
                //$scope.prdocdata = [];

                $http.get(ENV.apiUrl + "api/JVCreation/getAllRecords?doccode=" + docd + "&prnumber=" + final_doc_no + "&comp_code=" + cmp_cd).then(function (prdoc_data) {

                    $scope.prdocdata = prdoc_data.data;
                    $scope.block1 = [];
                    $scope.temp_doc = [];
                    $scope.totalDebit = 0;
                    $scope.totalcredit = 0;

                    for (var i = 0; i < $scope.prdocdata.length; i++) {

                        if (i == 0) {
                            $scope.temp_doc.gltd_comp_code = $scope.prdocdata[i].gltd_comp_code;
                            $scope.temp_doc.gltd_doc_code = $scope.prdocdata[i].gltd_doc_code;

                            $scope.temp_doc.gltd_doc_narr = $scope.prdocdata[i].gltd_doc_narr;
                            $scope.temp_doc.gltd_remarks = $scope.prdocdata[i].gltd_remarks;
                            $scope.temp_doc.gltd_doc_date = $scope.prdocdata[i].gltd_doc_date;
                            $scope.temp_doc.gltd_post_date = $scope.prdocdata[i].gltd_post_date;
                            $scope.temp_doc.gltd_prov_doc_no = $scope.prdocdata[i].gltd_prov_doc_no;
                            $scope.temp_doc.gltd_final_doc_no = final_doc_no;

                            $scope.temp_doc.gldu_authorize = $scope.prdocdata[i].gldu_authorize;
                            $scope.temp_doc.gldu_verify = $scope.prdocdata[i].gldu_verify;
                            $scope.temp_doc.gltd_authorize_date = $scope.prdocdata[i].gltd_authorize_date;
                            $scope.temp_doc.gltd_verify_date = $scope.prdocdata[i].gltd_verify_date;

                        }
                        $scope.cost = $scope.cost || ($scope.prdocdata[i].gldd_cost_center_code != '' || $scope.prdocdata[i].gldd_cost_center_code != "");
                        $scope.totalDebit = parseFloat($scope.totalDebit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_debit);
                        $scope.totalcredit = parseFloat($scope.totalcredit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_credit);

                        //console.log($scope.gldd_doc_amount_debit);
                        //console.log($scope.gldd_doc_amount_credit);


                    }

                });

            }

            $scope.maxmize = function () {
                $('#jv_Voucher').css({ 'height': '100%', 'width': '100%' })
                $scope.windowflg = true;
            }

            $scope.manimize = function () {
                $('#jv_Voucher').css({ 'height': '600px', 'width': '900px' })
                $scope.windowflg = false;

            }


        }]
        )
})();
