﻿(function () {
    'use strict';
    var obj1, temp, opr, comp, doc_cd_final_doc_no;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('DepreciationJVPostCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = true;
            $scope.table = false;
            var values = [];
            var doc_no;
            var doc_code;
            $scope.btn_report = false;
            var username = $rootScope.globals.currentUser.username;
            debugger;
            var dateyear;
            var date = new Date();
            dateyear = $filter('date')(new Date(), 'yyyy/MM/dd');
            var year = dateyear.split("/")[0];

            //$scope['year_no'] = year;
            $scope.company_code='1';
           
            
            $http.get(ENV.apiUrl + "api/DepreciationJVPost/getPeriodsData?fins_year=" + year).then(function (periodsName) {
                 $scope.periods_Name = periodsName.data;
                 console.log($scope.periods_Name);
             });

            debugger
            $http.get(ENV.apiUrl + "api/DepreciationJVPost/getCurrentYear").then(function (a_year) {
                debugger
                $scope.year_no = a_year.data[0].year1;
                console.log($scope.periods_Name);
            });


           
            debugger
            var datasend = [];
            $scope.calculateDep = function (year, fins_financial_period_no,company_code, username) {
               
                $http.get(ENV.apiUrl + "api/DepreciationJVPost/getDepreciationJVPosting?year=" + year + "&fins_financial_period_no=" + $scope.temp.fins_financial_period_no + "&username=" + $rootScope.globals.currentUser.username + "&company_code=" + $scope.company_code).then(function (getData) {
                    $scope.get_Data = getData.data;
                    debugger
                    swal({ text: $scope.get_Data, width: 400, height: 300 });
                   

                    var Data_Doc = $scope.get_Data;
                    values = Data_Doc.split("-");

                    doc_no   = values[0];
                    doc_code = values[1];

                    debugger;
                    $http.get(ENV.apiUrl + "api/JVCreation/Getdoc_cd_prov_no?doc_prov_no=" + doc_code + doc_no + "&comp_code=" + $scope.company_code).then(function (final_doc_no) {
                        $scope.doc_cd_final_doc_no = final_doc_no.data;
                        debugger;
                        //var data = {
                        //    location: $scope.final_doc_url,
                        //    parameter: { doc: $scope.doc_cd_final_doc_no },
                        //    state: 'main.Fin150'
                        //}

                    });

                    if(doc_no > 0)
                    {
                        $scope.btn_report = true;
                    }

                });
            }


            $scope.Report=function()
            {
                var data = {
                    location: 'Finn.FINR10Final',
                    parameter: { doc: $scope.doc_cd_final_doc_no },
                    state: 'main.fin017'
                }

                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')
            }



            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = 'fa fa-plus-circle';
                }
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.achieveData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.achieveData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_appl_form_field_value1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_sip_achievement_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_sip_achievement_point == toSearch) ? true : false;
            }

            $scope.New = function () {

                $scope.disabled = false;
                $scope.readonly = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = "";
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

           

            $scope.Clear = function () { $scope.temp = ""; }
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.sims_sip_achievement_type = "";
                $scope.temp.sims_sip_achievement_desc = "";
                $scope.temp.sims_sip_achievement_point = "";
                $scope.temp.sims_sip_achievement_status = "";
            }

            $scope.edit = function (str) {

                $scope.disabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;

                $scope.temp = {
                    sims_sip_achievement_type: str.sims_sip_achievement_type
                   , sims_sip_achievement_code: str.sims_sip_achievement_code
                   , sims_sip_achievement_desc: str.sims_sip_achievement_desc
                   , sims_sip_achievement_status: str.sims_sip_achievement_status
                   , sims_sip_achievement_point: str.sims_sip_achievement_point
                   , sims_appl_form_field_value1: str.sims_appl_form_field_value1

                };
            }

            var dataupdate = [];
            $scope.update = function (Myform) {
                if (Myform) {
                    var data = {
                        sims_sip_achievement_type: $scope.temp.sims_sip_achievement_type
                       , sims_sip_achievement_code: $scope.temp.sims_sip_achievement_code
                       , sims_sip_achievement_desc: $scope.temp.sims_sip_achievement_desc
                       , sims_sip_achievement_status: $scope.temp.sims_sip_achievement_status
                       , sims_sip_achievement_point: $scope.temp.sims_sip_achievement_point
                       , sims_appl_form_field_value1: $scope.temp.sims_appl_form_field_value1
                       , opr: 'U'
                    };
                    dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/Achievement/CUDAchievement", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", showCloseButton: true, width: 380, });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", showCloseButton: true, width: 380, });
                        }

                        $http.get(ENV.apiUrl + "api/Achievement/getAllAchievement").then(function (res1) {
                            $scope.achieveData = res1.data;
                            $scope.totalItems = $scope.achieveData.length;
                            $scope.todos = $scope.achieveData;
                            $scope.makeTodos();
                        });
                        main = document.getElementById('mainchk');
                        if (main.checked == true) {
                            main.checked = false;
                            $scope.row1 = '';
                        }
                        $scope.currentPage = true;
                    });
                    dataupdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_sip_achievement_code': $scope.filteredTodos[i].sims_sip_achievement_code,
                            'opr': "D"
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/Achievement/CUDAchievement", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Achievement/getAllAchievement").then(function (res1) {
                                                $scope.achieveData = res1.data;
                                                $scope.totalItems = $scope.achieveData.length;
                                                $scope.todos = $scope.achieveData;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted ", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Achievement/getAllAchievement").then(function (res1) {
                                                $scope.achieveData = res1.data;
                                                $scope.totalItems = $scope.achieveData.length;
                                                $scope.todos = $scope.achieveData;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }

            var dom; $scope.flag = true;
            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                         "<tr><td class='semi-bold'>" + "Quantity" + "</td> <td class='semi-bold'>" + "Invoice Amount" + " </td><td class='semi-bold'>" + "Book Value" + "</td><td class='semi-bold'>" + "Cum Depreciation" + "</td>" +
                          "<td class='semi-bold'>" + "Yearly Depr" + "</td> <td class='semi-bold'>" + "Monthly Depr" + " </td><td class='semi-bold'>" + "Sales Value" + "</td><td class='semi-bold'>" + "Replacement Cost" + "</td>" +
                          "<td class='semi-bold'>" + "Depr Percent" + "</td> <td class='semi-bold'>" + "Used Item" + " </td><td class='semi-bold'>" + "Status" + "</td><td class='semi-bold'>" + "Status Name" + "</td>" +
                        "</tr>" +

                          "<tr><td>" + (info.gam_quantity) + "</td> <td>" + (info.gam_invoice_amount) + " </td><td>" + (info.gam_book_value) + "</td><td>" + (info.gam_cum_deprn) + "</td>" +
                           "<td>" + (info.gam_ytd_deprn) + "</td> <td>" + (info.gam_mth_deprn) + " </td><td>" + (info.gam_sale_value) + "</td><td>" + (info.gam_repl_cost) + "</td>" +
                             "<td>" + (info.gam_deprn_percent) + "</td> <td>" + (info.gam_used_on_item) + " </td><td>" + (info.gam_status) + "</td><td>" + (info.gam_status_name) + "</td>" +
                        "</tr>" +

                        " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom); $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }

            };

        }])

})();
