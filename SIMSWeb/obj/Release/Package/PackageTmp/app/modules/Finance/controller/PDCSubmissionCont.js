﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var is_receipt, payment_mode, from_dt, to_dt, reference_no, bankcode;

    var finanacecode = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PDCSubmissionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.tot_btn = true;
            $scope.Maintabledata = false;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.records = false;
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            var user = $rootScope.globals.currentUser.username;
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            console.log($scope.finnDetail)
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            var acconcode = '';

            $scope.block1 = {
                receipt_date: 'receipt',
                cheque: 'cheque',
            }

            $(function () {
                $('#Select4').multipleSelect({
                    width: '100%'
                });
            });

            $scope.visible_textbox = function () {
                $scope.records = true;
            }

            //Fill ComboBox Status
            $http.get(ENV.apiUrl + "api/Financialyear/getDocumentStatus").then(function (docstatus) {

                $scope.docstatus_Data = docstatus.data;
                console.log($scope.docstatus);
            });


            //Fill BAnk Name
            $http.get(ENV.apiUrl + "api/PDCSubmission/getBankName").then(function (docstatus1) {
                $scope.bank_name = docstatus1.data;

                setTimeout(function () {
                    $('#Select4').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);

            });

            $http.get(ENV.apiUrl + "api/PDCSubmission/GetCreatedBy_user").then(function (data1) {
                $scope.user_list = data1.data;
                console.log($scope.data1);
            });

            //Fill department
            $http.get(ENV.apiUrl + "api/PDCSubmission/getDepartment").then(function (docstatus2) {
                $scope.Department = docstatus2.data;
                console.log($scope.docstatus2);
            });


            //Fill Ledger
            //$http.get(ENV.apiUrl + "api/PDCSubmission/getLedgerName").then(function (docstatus3) {
            //    $scope.ledger = docstatus3.data;
            //    console.log($scope.docstatus3);
            //});
            $http.get(ENV.apiUrl + "api/JVCreation/GetLedgerNumber").then(function (docstatus2) {
                $scope.LdgrCode = docstatus2.data;
                console.log($scope.LdgrCode);
            });


            $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code).then(function (docstatus) {

                $scope.getAllAccNos = docstatus.data;
            });

            $scope.getSLAccNo = function () {

                debugger;
                var acconcode = $scope.getAllAccNos.gldd_acct_code;
                if ($scope.block1.gldd_ledger_code != "00") {

                    $http.get(ENV.apiUrl + "api/JVCreation/GetSLAccNumber?ldgr_code=" + $scope.block1.gldd_ledger_code + "&cmp_cd=" + comp_code + "&year=" + finance_year).then(function (docstatus3) {
                        // $scope.sllc_ldgr_code = slcode;
                        $scope.getAllAccNos = docstatus3.data;
                        //$scope.slacno = docstatus3.data;
                    });
                }
                else {
                    $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code).then(function (docstatus) {

                        $scope.getAllAccNos = docstatus.data;
                    });
                }


            }

            //Account number


            //Select Data SHOW
            var showdetails = [], bankcode1;
            $scope.getViewDetails = function () {
                debugger;

                if ($scope.block1.cheque == 'cheque') {
                    payment_mode = 'Ch';
                }
                else {
                    payment_mode = 'Dd';

                }
                if ($scope.block1.receipt_date == 'receipt') {
                    is_receipt = 'R';
                }
                else {
                    is_receipt = 'C';

                }

                bankcode = [];
                if ($scope.block1.bank_code.length == 0 || $scope.block1 == undefined) {
                    bankcode = '';
                }
                else {
                    bankcode1 = $scope.block1.bank_code;
                    bankcode = bankcode + ',' + bankcode1;;
                }

                var data = {
                    ps_comp_code: comp_code,
                    ps_dept_no: $scope.block1.dept_no,
                    ps_ldgr_code: $scope.block1.gldd_ledger_code,
                    ps_sl_acno: $scope.block1.gldd_acct_code,
                    ps_discd: 'O',
                    pc_calendar: null,
                    ps_bank_code: bankcode,
                    filterText: $scope.block1.filter_range,
                    filterRange: $scope.block1.filter_data,
                    createdBY: $scope.block1.createdby_usr,

                    recFromDate: ($scope.block1.receipt_date) ? $scope.block1.from_date : null,
                    recToDate: ($scope.block1.receipt_date) ? $scope.block1.to_date : null,

                    ps_due_dateStr: ($scope.block1.cheque_date) ? $scope.block1.from_date : null,
                    subMissionDate: ($scope.block1.cheque_date) ? $scope.block1.to_date : null,
                    pc_ref_no: null,
                    pc_payment_mode: payment_mode,

                }
                debugger;


                $http.post(ENV.apiUrl + "api/PDCSubmission/AllPDCSubmission_new_data", data).then(function (res1) {
                    $timeout(function () {
                        $("#fixTable").tableHeadFixer({ 'top': 1 });
                    }, 100);
                    // j.ps_cheque_no_check

                    if (res1.data.length > 0) {
                        $scope.pdcSubmissionList = res1.data;
                        $scope.pdc_details = res1.data;
                        $scope.totalItems = $scope.pdc_details.length;
                        $scope.todos = $scope.pdc_details;
                        $scope.makeTodos();

                        $scope.checkprintbtn = true;

                        $scope.Maintabledata = true;

                        debugger;
                        $scope.total = 0;
                        for (var i = 0; i < res1.data.length; i++) {
                            $scope.total = $scope.total + res1.data[i].ps_amount;
                        }
                    }
                    else {
                        swal({ text: "No Data Found...!", timer: 50000 });
                        $scope.pdcSubmissionList = [];
                        $scope.total = 0;
                    }


                });


                showdetails = []


            }


            $scope.SearcModelDetails = function (dept_no, ldgr_code, slma_acno, from_date, to_date) {
                debugger;
                if (dept_no == undefined)
                    dept_no = '';
                if (ldgr_code == undefined)
                    ldgr_code = '';
                if (slma_acno == undefined)
                    slma_acno = '';
                if (from_date == undefined)
                    from_date = '';
                if (to_date == undefined)
                    to_date = '';

                $http.get(ENV.apiUrl + "api/PDCSubmission/getAllPDCSubmissions?dept_no=" + dept_no + "&ldgr_code=" + ldgr_code + "&slma_acno=" + slma_acno + "&from_date=" + from_date + "&to_date=" + to_date).then(function (getAllPDCSubmissions_Data) {
                    $scope.AllPDCSubmissions = getAllPDCSubmissions_Data.data;
                });
            }

            //$http.get(ENV.apiUrl + "api/GoalTarget/getAcademicYear").then(function (Acyear) {
            //    $scope.Acc_year = Acyear.data;
            //    console.log($scope.Acc_year);
            //});
            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);


            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.fin_doc, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.fin_doc;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_financial_year.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_financial_year_status.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_doc_srno == toSearch) ? true : false;
            }

            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "/" + month + "/" + day;
            }


            //NEW BUTTON
            $scope.New = function () {

                $scope.disabled = false;
                $scope.gtcreadonly = true;
                $scope.gtdreadonly = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;

                $scope.cmbstatus = true;
                $scope.txtyear = false;

                $scope.temp = "";

            }

            var cntr = 0;
            $scope.selectonebyone = function (str) {
                debugger;
                var d = document.getElementById(str);
                if (d.checked == true) {
                    cntr = cntr + 1;
                }
                else { cntr = cntr - 1; }
            }


            $scope.clear = function () {
                $scope.records = false;

                $scope.temp = [];
            }

            //DATA SAVE INSERT for cheque Transfer
            var datasend = [];
            $scope.savedata = function () {//Myform

                if ($scope.block1.cheque == 'cheque') {
                    payment_mode = 'Ch';
                }
                else {
                    payment_mode = 'Dd';

                }
               
                // if (Myform) {
                debugger;
                //  var data = $scope.temp;
                //data.opr = 'I';
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var e = document.getElementById(i + i);
                    if (e.checked == true) {
                        $scope.filteredTodos[i].cnt = 1;
                        $scope.filteredTodos.opr = 'I';
                        $scope.filteredTodos[i].pc_payment_mode = payment_mode;
                        datasend.push($scope.filteredTodos[i]);
                    }
                }
                if (datasend.length > 0) {
                    $http.post(ENV.apiUrl + "api/PDCSubmission/PDC_Submission_or_ReSubmission", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        //swal({ text: $scope.msg1.strMessage, timer: 5000 });
                        swal({ title: "Alert", text: $scope.msg1.strMessage, imageUrl: "assets/img/check.png", });
                        $scope.checkprintbtn = false;

                        reference_no = $scope.msg1.systemMessage;
                    });
                }
                else {
                    swal({ title: "Alert", text: 'Please Select Cheque', });

                }

                // getViewDetails();
                datasend = [];
            }


            var cntr1 = 0;
            $scope.selectmodeldata = function (str1) {
                debugger;
                var d = document.getElementById(str1);
                if (d.checked == true) {
                    cntr1 = cntr1 + 1;
                }
                else { cntr1 = cntr1 - 1; }

            }

            //DATA SAVE for Cheque Revert
            var datasend = [];
            $scope.selectmodeldatasave = function () {
                debugger;
                for (var i = 0; i < $scope.AllPDCSubmissions.length; i++) {
                    var e = document.getElementById(i);
                    if (e.checked == true) {
                        $scope.AllPDCSubmissions[i].cnt = 3;
                        $scope.AllPDCSubmissions.opr = 'I';
                        datasend.push($scope.AllPDCSubmissions[i]);
                    }
                }
                if (datasend.length > 0) {
                    $http.post(ENV.apiUrl + "api/PDCSubmission/PDC_Submission_or_ReSubmission1", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        swal({ title: "Alert", text: $scope.msg1.strMessage, imageUrl: "assets/img/check.png", });


                    });
                }
                else {
                    swal({ title: "Alert", text: 'Please Select Cheque', });

                }

                datasend = [];

            }



            //Pull Data
            $scope.pulldata = function () {
                debugger;



                if ($scope.block1.from_date == undefined && $scope.block1.to_date == undefined) {
                    swal({ text: "please select from date and to date", timer: 5000 });
                }
                else {
                    if ($scope.block1.cheque == 'cheque') {
                        payment_mode = 'Ch';
                    }
                    else {
                        payment_mode = 'Dd';

                    }
                    if ($scope.block1.receipt_date == 'receipt') {
                        is_receipt = 'R';
                    }
                    else {
                        is_receipt = 'C';

                    }

                    var data = {
                        ps_comp_code: comp_code,
                        ps_ldgr_code: is_receipt,
                        recFromDate: $scope.block1.from_date,
                        recToDate: $scope.block1.to_date,
                        pc_payment_mode: payment_mode,

                    }


                    $http.post(ENV.apiUrl + "api/PDCSubmission/PDC_Pool_Submission_data", data).then(function (res1) {
                        $scope.msg1 = res1.data;

                        swal({ text: $scope.msg1.strMessage, timer: 50000 });

                    });




                }
            }

            $scope.chkrevert = function () {
                $('#MyModal').modal('show');
                $scope.display = true;

            }


            $scope.CheckPrintReport = function () {
                debugger;

                var data = {
                    location: 'Finn.FINR23',
                    parameter: {
                        doc_no: reference_no,

                    },

                    state: 'main.Fin102'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')


            }

            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                    var v = document.getElementById(i + i);
                    if (main.checked == true) {
                        v.checked = true;
                        $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                    }
                    else {
                        v.checked = false;
                        main.checked = false;
                        $scope.pdcSubmissionList[i].row_color = ''
                    }
                }

            }

            $scope.ShowFilterText = function (str) {
                if (str == "") {
                    $scope.filterVisible = false;
                }
                else {

                    $scope.filterVisible = true;
                }
            }

            $scope.FilterCode = function () {
                debugger;
                var splitno = [];
                if ($scope.temp.chk_previous_clear == true) {
                    main = document.getElementById('mainchk');
                    for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {

                        var v = document.getElementById(i + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $scope.pdcSubmissionList[i].row_color = 'white';

                    }
                    if ($scope.edt.SelectOption == 'Select Records') {
                        debugger;

                        for (var i = 0; i < $scope.edt.txt_filter_range; i++) {
                            var v = document.getElementById(i + i);
                            v.checked = true;
                            $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                        }



                    }

                    else if ($scope.edt.SelectOption == 'Cheque Amount between') {


                        if ($scope.edt.txt_filter_range != undefined) {
                            splitno = $scope.edt.txt_filter_range.split('-');
                        }
                        if (splitno.length >= 2) {

                            for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                                if ($scope.pdcSubmissionList[i].ps_amount >= splitno[0] && $scope.pdcSubmissionList[i].ps_amount <= splitno[1]) {

                                    var v = document.getElementById(i + i);
                                    v.checked = true;
                                    $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                                }
                            }
                        }
                    }
                    else if ($scope.edt.SelectOption == 'Cheque Series between') {


                        if ($scope.edt.txt_filter_range != undefined) {
                            debugger;
                            splitno = $scope.edt.txt_filter_range.split('-');
                        }
                        if (splitno.length >= 2) {
                            $scope.minchkno = 0;
                            $scope.maxchkno = 0;
                            for (var j = 0; j < splitno.length; j++) {
                                if (j == 0) {
                                    $scope.minchkno = parseInt(splitno[0]);
                                }
                                else {
                                    $scope.maxchkno = parseInt(splitno[1]);
                                }

                            }

                            for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                                debugger;
                                if ($scope.pdcSubmissionList[i].ps_cheque_no >= $scope.minchkno && $scope.pdcSubmissionList[i].ps_cheque_no <= $scope.maxchkno) {

                                    var v = document.getElementById(i + i);
                                    v.checked = true;
                                    $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                                }
                            }
                        }

                    }
                }

                else {

                    if ($scope.edt.SelectOption == 'Select Records') {

                        if ($scope.pdcSubmissionList.length >= $scope.edt.txt_filter_range) {
                            for (var i = 0; i < $scope.edt.txt_filter_range; i++) {
                                var v = document.getElementById(i + i);
                                v.checked = true;
                                $scope.pdcSubmissionList[i].row_color = '#ffffcc'

                            }
                        }
                    }

                    else if ($scope.edt.SelectOption == 'Cheque Amount between') {


                        if ($scope.edt.txt_filter_range != undefined) {
                            splitno = $scope.edt.txt_filter_range.split('-');
                        }
                        if (splitno.length >= 2) {

                            for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                                if ($scope.pdcSubmissionList[i].ps_amount >= splitno[0] && $scope.pdcSubmissionList[i].ps_amount <= splitno[1]) {

                                    var v = document.getElementById(i + i);
                                    v.checked = true;
                                    $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                                }
                            }
                        }
                    }
                    else if ($scope.edt.SelectOption == 'Cheque Series between') {


                        if ($scope.edt.txt_filter_range != undefined) {
                            splitno = $scope.edt.txt_filter_range.split('-');
                        }
                        if (splitno.length >= 2) {

                            $scope.minchkno = 0;
                            $scope.maxchkno = 0;
                            for (var j = 0; j < splitno.length; j++) {
                                if (j == 0) {
                                    $scope.minchkno = parseInt(splitno[0]);
                                }
                                else {
                                    $scope.maxchkno = parseInt(splitno[1]);
                                }

                            }

                            for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                                debugger;
                                if ($scope.pdcSubmissionList[i].ps_cheque_no >= $scope.minchkno && $scope.pdcSubmissionList[i].ps_cheque_no <= $scope.maxchkno) {

                                    var v = document.getElementById(i + i);
                                    v.checked = true;
                                    $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                                }
                            }

                            //for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                            //    if ($scope.pdcSubmissionList[i].ps_cheque_no >= splitno[0] && $scope.pdcSubmissionList[i].ps_cheque_no <= splitno[1]) {

                            //        var v = document.getElementById(i);
                            //        v.checked = true;
                            //        $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                            //    }
                            //}
                        }

                    }

                }
            }


            //Date FORMAT
            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "-" + month + "-" + day;
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
            }

            //DATA EDIT
            $scope.edit = function (str) {
                $scope.gtcreadonly = true;
                $scope.gtdreadonly = true;
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.txtyear = true;
                $scope.cmbstatus = false;
                $scope.temp = str;


            }


            //DATA UPADATE
            var dataupdate = [];
            $scope.update = function () {

                var data = $scope.temp;
                data.opr = "U";

                dataupdate.push(data);

                $http.post(ENV.apiUrl + "api/Financialyear/CUDFinantialYear", data).then(function (msg) {

                    $scope.msg1 = msg.data;
                    swal({ text: $scope.msg1.strMessage, timer: 5000 });

                    $http.get(ENV.apiUrl + "api/Financialyear/getAllFinancialYear").then(function (res1) {
                        $scope.fin_doc = res1.data;
                        $scope.totalItems = $scope.fin_doc.length;
                        $scope.todos = $scope.fin_doc;
                        $scope.makeTodos();
                        $scope.table = true;
                        $scope.display = false;
                    });

                });
                dataupdate = [];

            }


            //DELETE RECORD
            //$scope.CheckAllChecked = function () {
            //    main = document.getElementById('mainchk');

            //    if (main.checked == true) {
            //        for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //            var t = $scope.filteredTodos[i].sims_financial_year;
            //            var v = document.getElementById(t);
            //            v.checked = true;
            //            finanacecode = finanacecode + $scope.filteredTodos[i].sims_financial_year + ',';
            //            $scope.row1 = 'row_selected';
            //        }
            //    }
            //    else {

            //        for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //            var t = $scope.filteredTodos[i].sims_financial_year;
            //            var v = document.getElementById(t);
            //            v.checked = false;
            //            main.checked = false;
            //            $scope.row1 = '';
            //        }
            //    }

            //}

            $scope.checkonebyonedelete = function () {

                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
            }


            $scope.OkDelete = function () {
                finanacecode = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_financial_year;
                    var v = document.getElementById(t);

                    if (v.checked == true)
                        finanacecode = finanacecode + $scope.filteredTodos[i].sims_financial_year + ',';

                }
                var deletemodulecode = ({

                    'sims_financial_year': finanacecode,
                    'opr': "D"
                });

                $http.post(ENV.apiUrl + "api/Financialyear/CUDFinantialYear", deletemodulecode).then(function (msg) {
                    $scope.msg1 = msg.data;
                    swal({ text: $scope.msg1.strMessage, timer: 5000 });

                    $http.get(ENV.apiUrl + "api/Financialyear/getAllFinancialYear").then(function (res1) {
                        $scope.fin_doc = res1.data;
                        $scope.totalItems = $scope.fin_doc.length;
                        $scope.todos = $scope.fin_doc;
                        $scope.makeTodos();
                        $scope.table = true;
                        $scope.display = false;
                    });

                });
                deletefin = [];

            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            //Selection Procedure
            $scope.Getdates = function (str) {



                for (var i = 0; i < $scope.docyear_Data1.length; i++) {
                    if ($scope.docyear_Data1[i].sims_academic_year == str) {
                        $scope.temp = {
                            sims_financial_year_start_date: $scope.docyear_Data1[i].sims_financial_year_start_date,
                            sims_financial_year_end_date: $scope.docyear_Data1[i].sims_financial_year_end_date,
                            sims_financial_year: $scope.docyear_Data1[i].sims_academic_year,


                        };
                    }
                }



            }

        }])

})();