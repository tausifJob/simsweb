﻿(function () {
    'use strict';
    var main, temp;
    var data1 = [];
    var data2 = [];
    var data3 = [];
    var Bankdetails = [];
    var status = "";
    var msg_flag1 = false;
    var acconcode = "";
    var prvno = "";
    var cmbvalue = "";
    var data = [];
    var cost = [];
    var chk;
    var coad_pty_short_name = [];
    var simsController = angular.module('sims.module.Finance');


    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CashPaymentCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.edt = "";
            $scope.operation = true;
            $scope.table1 = true;
            $scope.Add = true;
            $scope.btnDelete = false;

            $scope.users = [];
            $scope.data = [];
            $scope.Bankdetails = "";
            $scope.Bankdetails = [];
            $scope.chkcost_Ac = false;
            $scope.chkcost = false;
            $scope.cost_combo = false;
            $scope.edt5 = [];
            var user = $rootScope.globals.currentUser.username.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=Single").then(function (res) {
                $scope.final_doc_url = res.data;
                console.log($scope.final_doc_url);
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.totalAmountCount = function () {
               
                $scope.total = 0;

                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    $scope.total = parseFloat($scope.total) + parseFloat($scope.Bankdetails[i].gldd_doc_amount);
                }
            }

            $scope.CheckPrintReport = function () {
               

                var data = {
                    location: $scope.final_doc_url,
                    parameter: {
                        doc_code: $scope.temp.gltd_doc_code,
                        doc_no: $scope.Docno,

                    },

                    state: 'main.Fin152'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')


            }
            $http.get(ENV.apiUrl + "api/CashPayment/getDocCode?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {

                $scope.geDocCode = res.data;
                for (var i = 0; i < $scope.geDocCode.length; i++) {
                    if ($scope.geDocCode[i].gltd_doc_code == "CP") {
                        $scope.getDocCode = $scope.geDocCode[i].gltd_doc_code;
                    }
                }
                $scope.temp = {
                    gltd_doc_code: $scope.getDocCode
                }
                $http.get(ENV.apiUrl + "api/CashPayment/GetAllCashBankGLMasterAccountNo?cmp_cd=" + comp_code + "&financialyear=" + finance_year + "&doc_cd=" + $scope.getDocCode + "&username=" + user).then(function (docstatus2) {
                   
                    $scope.bankname = docstatus2.data
                    $scope.edt5['master_acno'] = docstatus2.data[0];
                    $scope.getAccountName(docstatus2.data[0].master_ac_cdname)

                    //    $scope.edt5={

                    //        master_acno: $scope.bankname[0].master_acno,
                    //        master_ac_cdname: $scope.bankname[0].master_ac_cdname
                    //}
                    //    $scope.getAccountName($scope.edt5.master_acno);
                });

            });

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.New = function () {
                $scope.operation = true;
                $scope.Add = true;
                $scope.updatebtn = false;
                $scope.Delete = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $http.get(ENV.apiUrl + "api/CashPayment/GetNextfins_doc_prvno?cmp_cd=" + comp_code + "&financialyear=" + finance_year + "&doc_cd=" + 'BP').then(function (provdata) {

                $scope.provenCode = provdata.data;
                //for (var i = 0; i < $scope.provenCode.length; i++) {
                //    $scope.gldc_next_prv_no = $scope.provenCode[i].gldc_next_prv_no,
                //      console.log($scope.provenCode);
                //}
            });

            $scope.cancel = function () {
                $scope.Add = true;
                $scope.updatebtn = false;
                $scope.edt = "";
                $scope.edt1 = "";
                $("#cmb_acc_Code3").select2("val", "");
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = yyyy + '-' + mm + '-' + dd;
            $scope.edate = yyyy + '-' + mm + '-' + dd;
            $scope.edt4 = {
                DocDate: yyyy + '-' + mm + '-' + dd,
                PostDate: yyyy + '-' + mm + '-' + dd,
                ChequeDate: yyyy + '-' + mm + '-' + dd,
            }

            $scope.getDepartcode = function (str) {

               
                $http.get(ENV.apiUrl + "api/BankReceipt/getCostCenter?aacno=" + str).then(function (cost_center) {
                   
                    $scope.costcenter = cost_center.data;
                    if ($scope.costcenter.length > 0) {
                        $scope.chkcost = true;
                    }
                    else {
                        $scope.chkcost = false;
                    }
                    console.log($scope.costcenter);
                });

                for (var i = 0; $scope.getAllAccNos.length; i++) {
                    if ($scope.getAllAccNos[i].gldd_acct_code == str) {
                        $scope.edt1 = {
                            coad_dept_no: $scope.getAllAccNos[i].gldd_dept_code,
                            gldd_acct_code: $scope.getAllAccNos[i].gldd_acct_code,
                            codp_dept_name: $scope.getAllAccNos[i].gldd_dept_name,
                            coad_pty_short_name: $scope.getAllAccNos[i].gldd_acct_name,
                        }
                    }
                }


            }


            $scope.cost_center = function (str) {
               
                chk = str;
                if (str == true) {
                    $scope.cost_combo = true;
                }
                else {
                    $scope.cost_combo = false;
                }

            }

            $scope.cost_center_Account = function (str) {
                chk = str;
                debugger;
                if (str == true) {
                    $scope.cost_combo_AC = true;
                }
                else {
                    $scope.cost_combo_AC = false;
                }

            }

            //Fill Combo Ledger
            $http.get(ENV.apiUrl + "api/CashPayment/GetLedgerNumber?financialyear="+finance_year + "&comp_code="+comp_code).then(function (docstatus2) {
                $scope.LdgrCode = docstatus2.data;
                console.log($scope.LdgrCode);
            });



            $scope.GetAllGLAcc = function (acconcode, cmpnycode) {
                ;
                $http.get(ENV.apiUrl + "api/CashPayment/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code).then(function (docstatus) {

                    $scope.getAllAccNos = docstatus.data;
                });
            }

            $http.get(ENV.apiUrl + "api/CashPayment/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code).then(function (docstatus) {
                ;
                $scope.getAllAccNos = docstatus.data;
            });

            //Fill Combo SLACCNO
            $scope.getSLAccNo = function (slcode) {

                $scope.getAllAccNos = [];
                var acconcode = $scope.getAllAccNos.gldd_acct_code;
                if (slcode == "00") {
                    $scope.GetAllGLAcc(acconcode, comp_code);
                }
                else {

                    $http.get(ENV.apiUrl + "api/CashPayment/GetSLAccNumber?pbslcode=" + slcode + "&cmp_cd=" + comp_code).then(function (docstatus3) {
                        // $scope.sllc_ldgr_code = slcode;
                        $scope.getAllAccNos = docstatus3.data;
                        //$scope.slacno = docstatus3.data;
                    });
                }
                $("#cmb_acc_Code3").select2("val", cmbvalue);
            }

            $http.get(ENV.apiUrl + "api/CashPayment/getBankPayment").then(function (BankPayment_Data) {

                $scope.BankPaymentData = BankPayment_Data.data;
                $scope.totalItems = $scope.BankPaymentData.length;
                $scope.todos = $scope.BankPaymentData;
                $scope.makeTodos();
                console.log($scope.BankPaymentData);

            });

            setTimeout(function () {
                $("#cmb_acc_Code3").select2();

            }, 100);

            $scope.summ = function () {

                $scope.total = 0;

                for (var i = 0; i < $scope.Bankdetails.length; i++) {

                    $scope.total = $scope.total + parseInt($scope.Bankdetails[i].gldd_doc_amount);
                    console.log($scope.total);
                }

            }

            $scope.datagrid = function (myForm) {

                $scope.chkcost = false;
                $scope.cost_combo = false;
                $scope.temp.cost_chk = false;

                if ($scope.edt.gldd_acct_code == undefined || $scope.edt.gldd_acct_code == "") {
                    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please select Account name", showCloseButton: true, width: 380, });

                }
                else if ($scope.edt.gldd_doc_amount == undefined || $scope.edt.gldd_doc_amount == "") {
                    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please Enter Amount", showCloseButton: true, width: 380, });
                }
                else {

                    var ledcode = "";
                    if (myForm) {

                        if ($scope.edt.sllc_ldgr_code == undefined) {
                            ledcode = '00';
                        }
                        else {
                            ledcode = $scope.edt.sllc_ldgr_code;
                        }

                        var costcenter1 = document.getElementById("costCenter")
                        var costcentername = costcenter1.options[costcenter1.selectedIndex].text;


                        var terminal = document.getElementById("cmb_acc_Code3");
                        $scope.selectedText = terminal.options[terminal.selectedIndex].text;
                        $scope.Bankdetails.push({
                            'gldd_comp_code': comp_code,
                            'sllc_ldgr_code': ledcode,
                            'slma_acno': $scope.edt1.gldd_acct_code,
                            'coad_pty_full_name': $scope.selectedText,
                            'coad_dept_no': $scope.edt1.coad_dept_no,
                            'gldd_party_ref_date': $scope.edt.gldd_party_ref_date,
                            'gldd_doc_narr': $scope.edt.gldd_doc_narr,
                            'gldd_doc_amount': $scope.edt.gldd_doc_amount,
                            'glco_cost_centre_code': $scope.edt.glco_cost_centre_code,
                            'coce_cost_centre_name1': costcentername,
                            'gldd_fc_amount': 0.00

                        });
                        $scope.total = 0;
                        $scope.fctotal = 0;

                        for (var i = 0; i < $scope.Bankdetails.length; i++) {

                            $scope.total = parseFloat($scope.total) + parseFloat($scope.Bankdetails[i].gldd_doc_amount);
                            console.log($scope.total);
                            // $scope.fctotal = $scope.fctotal + parseInt($scope.Bankdetails[i].gldd_fc_amount);
                            $scope.fctotal = parseFloat($scope.fctotal) + parseFloat($scope.Bankdetails[i].gldd_fc_amount);
                            console.log($scope.fctotal);
                        }
                        //$scope.btnDelete = true;


                        //$scope.fctotal = 0;

                        //for (var i = 0; i < $scope.Bankdetails.length; i++) {
                        //    $scope.fctotal = $scope.fctotal + parseInt($scope.Bankdetails[i].gldd_fc_amount);
                        //    console.log($scope.fctotal);
                        //}
                        $scope.edt = "";
                        $scope.edt1 = "";
                        $scope.edt2 = "";
                        $("#cmb_acc_Code3").select2("val", "");
                        // $scope.edt.slma_acno = '';
                        $scope.myForm.$setPristine();
                        $scope.myForm.$setUntouched();
                        Bankdetails = [];
                        //$scope.selectedText = "";
                    }
                    else {
                        swal({ title: "Alert", text: "This Account Name Already Present", width: 380 });
                    }
                }
            };

            $scope.up = function (str) {
               
                $scope.Add = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                //$scope.edt = str;
                //var terminal1 = document.getElementById("cmb_acc_Code3");
                //$scope.selectedText = terminal1.options[terminal1.selectedIndex].text;

                $scope.edt =
                {
                    // slma_acno:str.edt.slma_acno,
                    coad_dept_no: str.coad_dept_no,
                    // gldd_acct_code: str.gldd_acct_code,
                    sllc_ldgr_code: str.sllc_ldgr_code,
                    gldd_party_ref_date: str.gldd_party_ref_date,
                    gldd_doc_amount: str.gldd_doc_amount,
                    gldd_doc_narr: str.gldd_doc_narr,
                    coad_pty_full_name: str.slma_acno,
                    gldd_acct_code: str.slma_acno,

                    //   selectedText:select2("val", str.gldd_acct_code)

                };
                cmbvalue = str.slma_acno;
                $scope.getSLAccNo($scope.edt.sllc_ldgr_code);
                //$scope.getDepartcode($scope.edt.gldd_acct_code);
                //setTimeout(function () { $("#cmb_acc_Code3").select2("val", $scope.tes); }, 1000);
                $scope.edt1 =
              {
                  coad_dept_no: str.coad_dept_no,
                  gldd_acct_code: str.slma_acno,
              };
            }

            $scope.Update = function () {
               
                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    //  $scope.data2.push($scope.Bankdetails);
                    if ($scope.Bankdetails.slma_acno == "")

                    { }
                    else {
                        var terminal = document.getElementById("cmb_acc_Code3");
                        var selectedText = terminal.options[terminal.selectedIndex].text;
                        //$scope.Bankdetails.pop({
                        //    'gldd_comp_code': comp_code,
                        //    'sllc_ldgr_code': $scope.edt.sllc_ldgr_code,
                        //    'slma_acno': $scope.edt1.gldd_acct_code,
                        //    'coad_pty_full_name': selectedText,
                        //    'coad_dept_no': $scope.edt1.coad_dept_no,
                        //    'gldd_party_ref_date': $scope.edt.gldd_party_ref_date,
                        //    'gldd_doc_narr': $scope.edt.gldd_doc_narr,
                        //    'gldd_doc_amount': $scope.edt.gldd_doc_amount,
                        //    'gldd_fc_amount': 0.00

                        //});
                        $scope.Bankdetails.push({
                            'gldd_comp_code': comp_code,
                            'sllc_ldgr_code': $scope.edt.sllc_ldgr_code,
                            'slma_acno': $scope.edt1.gldd_acct_code,
                            'coad_pty_full_name': selectedText,
                            'coad_dept_no': $scope.edt1.coad_dept_no,
                            'gldd_party_ref_date': $scope.edt.gldd_party_ref_date,
                            'gldd_doc_narr': $scope.edt.gldd_doc_narr,
                            'gldd_doc_amount': $scope.edt.gldd_doc_amount,
                            'gldd_fc_amount': 0.00

                        });
                        swal({ title: "Alert", text: "Record Updated", width: 300, height: 200 });
                        $scope.total = 0;
                        $scope.fctotal = 0;

                        for (var i = 0; i < $scope.Bankdetails.length; i++) {

                            $scope.total = $scope.total + parseInt($scope.Bankdetails[i].gldd_doc_amount);
                            $scope.fctotal = $scope.fctotal + parseInt($scope.Bankdetails[i].gldd_fc_amount);
                            console.log($scope.total);
                            console.log($scope.fctotal);
                        }
                        //$scope.btnDelete = true;




                        //for (var i = 0; i < $scope.Bankdetails.length; i++) {

                        //    $scope.fctotal = $scope.fctotal + parseInt($scope.Bankdetails[i].gldd_fc_amount);
                        //    console.log($scope.fctotal);
                        //}
                        $scope.edt = "";
                        $scope.edt1 = "";
                        $scope.edt2 = "";
                        $scope.updatebtn = false;
                        $scope.Add = true;
                        $("#cmb_acc_Code3").select2("val", "");
                    };

                }
            }

            $scope.coad_pty_full_name = $scope.selectedText;
            console.log($scope.coad_pty_full_name);

            $scope.Delete = function (str) {
                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    if ($scope.Bankdetails[i].slma_acno == str.slma_acno && $scope.Bankdetails[i].gldd_doc_amount == str.gldd_doc_amount) {
                        $scope.Bankdetails.splice(i, 1);
                        break;
                    }
                }
                $scope.total = 0;
                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    $scope.total = parseFloat($scope.total) + parseFloat($scope.Bankdetails[i].gldd_doc_amount);
                    console.log($scope.total);
                }
            }

            $scope.getAccountName = function (str) {
                debugger
                cost = str.master_acno;
                var cost_center = cost;

                $http.get(ENV.apiUrl + "api/BankReceipt/getCostCenter?aacno=" + cost_center).then(function (cost_center) {
                    debugger
                    $scope.costcenter_account = cost_center.data;
                    if ($scope.costcenter_account != null || $scope.costcenter_account != undefined) {
                        $scope.chkcost_Ac = true;
                    }
                    console.log($scope.costcenter_account);
                });

                for (var i = 0; i < $scope.bankname.length; i++) {
                    if ($scope.bankname[i].master_acno == str.master_acno) {
                        acconcode = $scope.bankname[i].master_acno;
                        $scope.edt3 = {
                            master_ac_cdname: $scope.bankname[i].master_ac_cdname,
                        }
                    }

                }
            }

            $http.get(ENV.apiUrl + "api/BankPayment/GetVerifyAuthorizeUsers?username=" + user).then(function (users_list) {
               
                $scope.users = users_list.data;
                console.log($scope.users);
                if ($scope.users.length != 0) {
                   
                    if ($scope.users[0] == user && $scope.users[1] == user) {
                        $scope.preparebtn = false;
                        $scope.verifybtn = false;
                        $scope.authorizebtn = false;
                    }
                    else if ($scope.users[0] == user && ($scope.users[1] == "" || $scope.users[1] != user)) {
                        $scope.verifybtn = false;
                        $scope.preparebtn = false;
                        $scope.authorizebtn = true;
                    }
                    else {
                        $scope.preparebtn = false;
                        $scope.verifybtn = true;
                        $scope.authorizebtn = true;
                    }
                }
            });
            $scope.Authorizeuser = function () {
                $http.get(ENV.apiUrl + "api/CashPayment/GetVerifyAuthorizeUsers?username=" + user).then(function (users_list) {
                   
                    $scope.users = users_list.data;
                    console.log($scope.users);
                    if ($scope.users.length != 0) {
                       
                        if ($scope.users[0] == user && $scope.users[1] == user) {
                            $scope.preparebtn = false;
                            $scope.verifybtn = false;
                            $scope.authorizebtn = false;
                        }
                        else if ($scope.users[0] == user && $scope.users[0] != user) {
                            $scope.verifybtn = false;
                            $scope.preparebtn = true;
                            $scope.authorizebtn = true;
                        }
                        else if ($scope.users[0] != user && $scope.users[0] != user) {
                            $scope.verifybtn = true;
                            $scope.preparebtn = false;
                            $scope.authorizebtn = true;
                        }
                    }
                });
            }

            $scope.Insert_temp_docs = function () {
               
                var datasend = [];
                if ($scope.temp1 == undefined) {
                    $scope.temp1 = "";
                }
                var verifyUser, verifyDate, authorize_user, authorize_date;
                if (status == "Verify") {
                    verifyUser = user;
                    verifyDate = $scope.edt4.DocDate;
                }
                else if (status == "Authorize") {
                    verifyUser = user;
                    verifyDate = $scope.edt4.DocDate;
                    authorize_user = "";
                    authorize_date = "";
                }
                else {
                    verifyUser = "";
                    verifyDate = "";
                    authorize_user = "";
                    authorize_date = "";

                }
                var data = {
                    gltd_comp_code: comp_code,
                    gltd_prepare_user: user,
                    gltd_doc_code: $scope.temp.gltd_doc_code,
                    gltd_doc_date: $scope.edt4.DocDate,
                    gltd_cur_status: status,
                    gltd_post_date: $scope.edt4.PostDate,
                    gltd_doc_narr: $scope.temp1.gltd_doc_narr1,
                    gltd_remarks: $scope.temp1.gltd_doc_remark,
                    gltd_prepare_date: $scope.edt4.DocDate,
                    gltd_final_doc_no: "0",
                    gltd_verify_user: verifyUser,
                    gltd_verify_date: verifyDate,
                    gltd_authorize_user: authorize_user,
                    gltd_authorize_date: authorize_date,
                    gltd_paid_to: $scope.temp1.gltd_paid_to,
                    gltd_cheque_no: $scope.temp.gltd_cheque_No
                }
                datasend.push(data);

                $http.post(ENV.apiUrl + "api/CashPayment/Insert_Fins_temp_docs", datasend).then(function (msg) {
                    $scope.prvno = msg.data;

                    if ($scope.prvno != "" && $scope.prvno != null) {
                        $scope.Insert_Fins_temp_doc_details();
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not inserted", width: 300, height: 200 });
                    }
                });

            }

            $scope.Insert_Fins_temp_doc_details = function () {
               
                var dataSend = [];
                var j = 1;

                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    var data = {
                        gldd_doc_narr: $scope.Bankdetails[i].gldd_doc_narr,
                        gldd_dept_code: $scope.Bankdetails[i].coad_dept_no,
                        gldd_ledger_code: $scope.Bankdetails[i].sllc_ldgr_code,
                        gldd_acct_code: $scope.Bankdetails[i].slma_acno,
                        gldd_doc_amount: parseFloat($scope.Bankdetails[i].gldd_doc_amount),
                        gldd_cost_center_code: $scope.Bankdetails[i].glco_cost_centre_code,
                        gldd_comp_code: comp_code,
                        gldd_doc_code: $scope.temp.gltd_doc_code,
                        gldd_prov_doc_no: $scope.prvno,
                        gldd_final_doc_no: "0",
                        gldd_line_no: j,

                    }
                    j++;
                    dataSend.push(data);

                }
                var data = {
                    gldd_comp_code: comp_code,
                    gldd_doc_code: $scope.temp.gltd_doc_code,
                    gldd_prov_doc_no: $scope.prvno,
                    gldd_final_doc_no: "0",
                    gldd_line_no: j,
                    gldd_ledger_code: "00",
                    gldd_acct_code: $scope.edt5.master_acno.master_acno,
                    gldd_doc_amount: ("-" + $scope.total),
                    gldd_fc_amount_debit: 0,
                    gldd_fc_code: "",
                    gldd_fc_rate: 0,
                    gldd_doc_narr: $scope.temp1.gltd_doc_narr1,
                    gldd_cost_center_code: $scope.edt5.glco_cost_centre_code,
                    gldd_dept_code: "",
                    gldd_party_ref_no: "",
                    gldd_party_ref_date: $scope.gldd_party_ref_date,

                }
                dataSend.push(data);
                $http.post(ENV.apiUrl + "api/CashPayment/Insert_Fins_temp_doc_details", dataSend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if (status == "Authorize") {
                        msg_flag1 = true;
                        $scope.Authorize_Insert_temp_doc_details($scope.msg1);

                    }
                    else if (status == "Save") {
                        msg_flag1 = false;
                        $scope.Authorize_Insert_temp_doc_details($scope.msg1);

                    }
                    else if (status == "Verify") {
                        msg_flag1 = false;
                        $scope.Authorize_Insert_temp_doc_details($scope.msg1);

                    }
                    if ($scope.msg1 == true) {

                    }
                    else {
                        swal({ title: "Alert", text: "Record Not inserted", width: 300, height: 200 });
                    }
                });

            }


            $scope.Authorize_Insert_temp_doc_details = function (str) {
               
                if (msg_flag1) {
                    msg_flag1 = false;
                    var auth_date = $scope.edt4.DocDate;
                    $http.post(ENV.apiUrl + "api/CashPayment/Authorize_Posting?comp_code=" + comp_code + "&doc_code=" + $scope.temp.gltd_doc_code + "&prv_no=" + $scope.prvno + "&auth_user=" + user + "&auth_date=" + auth_date).then(function (Auth) {
                       
                        $scope.Docno = Auth.data;
                        if ($scope.Docno != "") {
                            swal({ title: "Alert", text: "Posted Successfully\nFinal Doc No is=" + $scope.Docno, width: 300, height: 200 });
                            $scope.checkprintbtn = true;
                            $scope.ClearInsert();
                            $scope.Authorizeuser();
                            $scope.cancel();
                        }
                        else {
                            swal({ title: "Alert", text: "Posting Failed", width: 300, height: 200 });
                        }
                    });

                }
                else if ((msg_flag1 == false) && (status == "Save")) {
                    msg_flag1 = false;
                    if (str) {
                        swal({ title: "Alert", text: "Record Inserted Successfully\nProvision No:-" + $scope.temp.gltd_doc_code + "--" + $scope.prvno, width: 300, height: 200 });
                        $scope.ClearInsert();
                        $scope.Authorizeuser();
                        $scope.cancel();
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not inserted", width: 300, height: 200 });
                    }
                }

                else if ((msg_flag1 == false) && (status == "Verify")) {
                    msg_flag1 = false;
                    if (str) {
                        swal({ title: "Alert", text: "Record Verified Successfully\nProvision No:-" + $scope.temp.gltd_doc_code + "--" + $scope.prvno, width: 300, height: 200 });
                        $scope.ClearInsert();
                        $scope.Authorizeuser();
                        $scope.cancel();
                    }
                    else {
                        swal({ title: "Alert", text: "Verification Failed", width: 300, height: 200 });
                    }
                }

                $scope.ClearInsert();
                $scope.Authorizeuser();
            }

            $scope.Prepare = function () {
                $scope.chkcost_Ac = false;
                $scope.cost_combo_AC = false;
                try {
                    if ($scope.Bankdetails.length > 0) {
                        $scope.flag1 = false;
                        if ($scope.edt4.PostDate == null || $scope.edt4.PostDate == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Select Posting Date", showCloseButton: true, width: 380, });
                        }

                        else {

                            $scope.preparebtn = true;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                            status = "Save";
                            $scope.Insert_temp_docs();

                        }
                    }
                    else {
                        swal({ title: "Alert", text: $scope.insertmsg.strMessage, showCloseButton: true, width: 380, });
                    }
                }


                catch (e) {

                }


            }

            $scope.Verify = function () {
                try {
                    if ($scope.Bankdetails.length > 0) {
                        $scope.flag1 = false;
                        if ($scope.edt4.PostDate == null || $scope.edt4.PostDate == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Select Posting Date", showCloseButton: true, width: 380, });

                        }

                        else {
                            $scope.preparebtn = true;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                            status = "Verify";
                            $scope.Insert_temp_docs();


                        }


                    }
                    else {
                        swal({ title: "Alert", text: $scope.insertmsg.strMessage, showCloseButton: true, width: 380, });
                    }

                } catch (e) {

                }


            }

            $scope.Authorize = function () {
                try {
                    if ($scope.Bankdetails.length > 0) {
                        $scope.flag1 = false;
                        if ($scope.edt4.PostDate == null || $scope.edt4.PostDate == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Select Posting Date", showCloseButton: true, width: 380, });

                        }

                        else {
                            $scope.preparebtn = true;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                            status = "Authorize";
                            $scope.Insert_temp_docs();
                        }
                    }
                    else {
                        swal({ title: "Alert", text: $scope.insertmsg.strMessage, showCloseButton: true, width: 380, });
                    }

                } catch (e) {

                }
            }

            $scope.ClearInsert = function () {
                $scope.Bankdetails = [];
                $scope.total = "";
                $scope.temp1 = {
                    gltd_doc_narr1: "",
                    gltd_doc_remark: ""
                }
            }

            $scope.onlyNumbers = function (event) {
               
                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $http.get(ENV.apiUrl + "api/BankPayment/getInsertMesage").then(function (res) {
               
                $scope.insertmsg = res.data;
            });

        }]
        )
})();
