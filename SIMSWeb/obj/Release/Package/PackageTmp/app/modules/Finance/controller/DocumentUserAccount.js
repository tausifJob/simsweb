﻿(function () {
    'use strict';
    var modulecode = [];
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('DocumentUserAccountCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {


            $scope.edt = {};
            $scope.itemsPerPage = '5';
            $http.get(ENV.apiUrl + "api/finance/getDocUserAccount").then(function (res) {
                $scope.obj = res.data;
                console.log($scope.obj);
                $scope.div_hide = false;
                $scope.display = false;
                $scope.grid = true;
                $scope.save1 = false;
                $scope.select = true;
                $scope.editTxt = true;
                $scope.accedtbtn = false;
                $scope.totalItems = $scope.obj.length;
                $scope.todos = $scope.obj;
                $scope.makeTodos();
            });

            $scope.onKeyPress = function ($event) {
                if ($event.keyCode == 13) {
                    $scope.Save();
                    // console.log("enetr");
                }
            };
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.multipledelete();
                }


                console.log($scope.filteredTodos);
            };

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();

            }

            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;


            };

            $scope.company = function () {
                $http.get(ENV.apiUrl + "api/finance/getComCode").then(function (res) {
                    $scope.compcode = res.data;
                    console.log($scope.compcode);

                    if (res.data.length > 0) {
                        $scope.edt['gdua_comp_code'] = $scope.compcode[0].gdua_comp_code;
                        $scope.year($scope.edt.gdua_comp_code);
                    }

                });

            }

            $scope.company();

            $scope.year = function (comp_code) {
                $http.get(ENV.apiUrl + "api/finance/getYear?comp=" + comp_code).then(function (res) {
                    $scope.years = res.data;
                    console.log($scope.years);

                    if (res.data.length > 0) {
                        $scope.edt['gdua_year'] = $scope.years[0].gdua_year;
                        $scope.dept($scope.compcode[0].gdua_comp_code, $scope.years[0].gdua_year);
                    }
                });
            }

            $scope.dept = function (comp_code, year) {
                $http.get(ENV.apiUrl + "api/finance/getDepCode?comp=" + comp_code + "&year=" + year).then(function (res) {
                    $scope.depts = res.data;
                    if (res.data.length > 0) {
                        $scope.edt['gdua_dept_no'] = $scope.depts[0].gdua_dept_no;
                        $scope.accountcode(comp_code, year, $scope.depts[0].gdua_dept_no);
                    }

                });
                $http.get(ENV.apiUrl + "api/finance/getDocCode?comp=" + comp_code + "&year=" + year).then(function (res) {
                    $scope.docs = res.data;
                    if (res.data.length > 0) {
                        $scope.edt['gdua_doc_code'] = $scope.docs[0].gdua_doc_code;
                    }

                });
            }

            setTimeout(function () {
                $("#cmb_ac_code").select2();
                $("#cmb_ac_code").select2("val", '');
            }, 100);

            $scope.accountcode = function (comp_code, year, deptno) {
                $http.get(ENV.apiUrl + "api/finance/getAcctCode?comp=" + comp_code + "&year=" + year + "&deptno=" + deptno).then(function (res) {
                    $scope.accounts = res.data;
                    setTimeout(function () {
                        $("#cmb_ac_code").select2("val", $scope.edt.gdua_acct_code);
                    }, 100);
                });
            }

            $http.get(ENV.apiUrl + "api/finance/getDocUserCode").then(function (res) {
                $scope.docsusercodes = res.data;
                setTimeout(function () {


                    $("#grade_module").jqxListBox(
                            { source: $scope.docsusercodes, displayMember: 'user_name', valueMember: "comn_user_name", width: '100%', height: 250, theme: 'energyblue', multiple: true }
                     );
                }, 100)
            });

            $('#grade_module').bind('select', function (event) {
                var args = event.args;
            });

            $('#grade_module').bind('unselect', function (event) {
                var args = event.args;
            });


            $scope.selectAll = function () {
                if ($scope.edt.select) {
                    $("#grade_module").jqxListBox('clearSelection');
                    for (var i = 0; i < $scope.docsusercodes.length; i++) {
                        $("#grade_module").jqxListBox('selectIndex', i);
                    }
                } else {
                    $("#grade_module").jqxListBox('clearSelection');
                }
            }

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.accedtbtn = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.dropshow = true;
                $scope.update = true;
                $scope.edt = str;

            }

            $scope.showdate = function (date, name1) {
                var date = new Date(date);
                var month = (date.getMonth() + 1);
                var day = date.getDate();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                $scope.edt[name1] = date.getFullYear() + '-' + (month) + '-' + (day)

            }

            $scope.Update = function () {

                $scope.edt['opr'] = "U";
                //data.push($scope.edt)
                //console.log(data);
                $http.post(ENV.apiUrl + "api/finance/CUDDocUserAccountDetails", $scope.edt).then(function (msg) {
                    if (msg.data) {
                        $scope.grid = true;
                        $scope.display = false;

                        swal({ text: 'Record Updated Successfully', width: 300, showCloseButton: true });

                        $scope.maindata();
                    } else {

                        swal({ text: "Record Not Updated", width: 300, showCloseButton: true });
                    }
                    $scope.display = false;
                    $scope.grid = true;
                });
            }
            function filterArray(arr) {
                var len = arr.length;
                for (var i = len; i >= 0; --i) {
                    if (angular.isFunction(arr[i])) {
                        arr.splice(i, 1);
                    }
                }
            }

            var data = [];
            $scope.savedisable = false;
            $scope.Save = function (isvalid) {
                if (isvalid) {
                    var items = $("#grade_module").jqxListBox('getSelectedItems');
                    filterArray(items);
                    if (items.length > 0) {
                        $scope.edt.str_Selected = "";
                        for (var i = 0; i < items.length; i++) {
                            $scope.edt.str_Selected += items[i].originalItem.comn_user_name;
                            if (i < items.length - 1)
                                $scope.edt.str_Selected += ",";
                        }
                        $scope.edt['gdua_doc_user_code'] = $scope.edt.str_Selected;
                        console.log($scope.edt['gdua_doc_user_code']);
                        if (!jQuery.isEmptyObject($scope.edt)) {
                            //                            $('#loader').modal('show');
                            $scope.edt['opr'] = "I"
                            $http.post(ENV.apiUrl + "api/finance/CUDDocUserAccountDetails", $scope.edt).then(function (msg) {
                                //                              $('#loader').modal('hide');

                                if (msg.data) {
                                    data = [];
                                    swal({ text: 'Record Inserted Successfully', width: 300, showCloseButton: true });

                                    $scope.maindata();
                                } else {
                                    data = [];
                                    swal({ text: "Record Not Inserted", width: 300, showCloseButton: true });
                                }
                                $scope.display = false;
                                $scope.grid = true;
                            });
                        } else {
                            swal({ text: 'Fill Fields To Insert Record', width: 300, showCloseButton: true });
                        }
                    } else {
                        swal({ text: 'Please select username', width: 300, showCloseButton: true });
                        // console.log("Please select username")
                    }
                }
            }

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.dropshow = false;
                $scope.update = false;
                $scope.company();
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.edt['select'] = false;
                setTimeout(function () {
                    $("#cmb_ac_code").select2("val", '');
                }, 100);
                $("#grade_module").jqxListBox('clearSelection');
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.edt = {};
            }

            $scope.Delete = function () {

                $scope.flag = false;
                del = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].comp_name + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        $scope.filteredTodos[i]['opr'] = "D";
                        del.push($scope.filteredTodos[i]);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/finance/DeUserAccountDetails", del).then(function (msg) {
                                if (msg.data) {
                                    del = [];
                                    swal({ title: "Message", text: "Record Delete Successfully", width: 300, showCloseButton: true });
                                    $scope.maindata();

                                }

                                else {

                                    swal({ title: "Message", text: "Record Not Delete", width: 300, showCloseButton: true });
                                    $scope.multipledelete();
                                }
                            })
                        } else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            $scope.multipledelete();
                        }
                    })
                } else {
                    swal({ title: "Message", text: "Select atleast one record to delete", width: 300, showCloseButton: true });

                }
            }

            $scope.maindata = function () {
                $http.get(ENV.apiUrl + "api/finance/getDocUserAccount").then(function (res) {
                    $scope.obj = res.data;
                    $scope.div_hide = false;
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.save1 = false;
                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.makeTodos();
                });
            }

            $scope.multipledelete = function () {

                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    var v = document.getElementById($scope.filteredTodos[i].comp_name + i);
                    if (main.checked == true) {
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                    else {
                        v.checked = false;
                        main.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.delete_onebyone = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
            var dom;
            var count = 0;

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {

                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.codp_dept_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.gldc_doc_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.comn_user_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.glma_acct_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);



        }]);



})();