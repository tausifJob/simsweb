﻿(function () {
    'use strict';
    var opr = '';
    var main;
    var data1 = [];
    var simsController = angular.module('sims.module.Finance');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SubledgerMatchingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.operation = true;
            $scope.table1 = false;
            $scope.editmode = false;
            $scope.edt = "";
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            console.log($scope.finnDetail)
         
            $http.get(ENV.apiUrl + "api/SubledgerMatchingDetails/GetLedgercode").then(function (GetLedgercode_Data) {
                $scope.LdgrCode = GetLedgercode_Data.data;
            });

            $http.get(ENV.apiUrl + "api/SubledgerMatchingDetails/getDocCode?sltr_comp_code=" + $scope.finnDetail.company).then(function (getDocCode_Data) {
                $scope.DocCode_Data = getDocCode_Data.data;
            });

            $scope.getSLAccNo = function (sltr_comp_code, sltr_ldgr_code) {
              
              $http.get(ENV.apiUrl + "api/SubledgerMatchingDetails/GetSLAccNumber?sltr_comp_code=" + $scope.finnDetail.company + "&sltr_ldgr_code=" + $scope.edt.sltr_ldgr_code).then(function (GetSLAccNumber_Data) {
                    $scope.getAllAccNos = GetSLAccNumber_Data.data;
              });
             // $("#cmb_acc_Code1").select2("val", cmbvalue);
            }

            setTimeout(function () {
                $("#cmb_acc_Code1").select2();

            }, 100);

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.Cancel = function () {
                $scope.table1 = false;
                $scope.operation = true;
                $("#cmb_acc_Code1").select2("val", "");
                $scope.edt = {
                    sltr_ldgr_code: '',
                    sltr_slmast_acno: '',
                    sltr_doc_code: '',
                    sltr_pstng_date: '',
                    sltr_doc_date: '',
                }
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Reset = function () {
                $("#cmb_acc_Code1").select2("val", "");
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.edt = {
                    sltr_ldgr_code: '',
                    sltr_slmast_acno: '',
                    sltr_doc_code: '',
                    sltr_pstng_date: '',
                    sltr_doc_date: '',
                }
            }

            $scope.Show_Data = function (myForm) {
                if (myForm){
                $http.get(ENV.apiUrl + "api/SubledgerMatchingDetails/gettransactiondetails?sltr_comp_code=" + $scope.finnDetail.company + "&sltr_ldgr_code=" + $scope.edt.sltr_ldgr_code + "&sltr_slmast_acno=" + $scope.edt.sltr_slmast_acno + "&sltr_doc_code=" + $scope.edt.sltr_doc_code + "&sltr_pstng_date=" + $scope.edt.sltr_pstng_date + "&sltr_doc_date=" + $scope.edt.sltr_doc_date).then(function (GetSLtransactionDetails_Data) {
                    $scope.SLtransactionDetails_Data = GetSLtransactionDetails_Data.data;
                    $scope.totalItems = $scope.SLtransactionDetails_Data.length;
                    $scope.todos = $scope.SLtransactionDetails_Data;
                    $scope.makeTodos();
                    console.log($scope.SLtransactionDetails_Data);
                    if ($scope.SLtransactionDetails_Data < 1) {

                        swal({ title: "Alert", text: "Sorry, There Is No Record Found..!", width: 300, height: 200 });
                        $scope.table1 = false;
                        $scope.operation = true;
                    }
                    else {
                        $scope.table1 = true;
                        $scope.operation = true;

                    }
                });
                }
            }

            $scope.checkonebyoneupdate = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                       
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.Submit = function () {
                data1 = [];

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        data1.push($scope.filteredTodos[i]);
                    }
                    else {

                        data1.push($scope.filteredTodos[i]);
                    }
                }
               
                $http.post(ENV.apiUrl + "api/SubledgerMatchingDetails/CUDtransactiondetails", data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                    }
                    $http.get(ENV.apiUrl + "api/SubledgerMatchingDetails/gettransactiondetails?sltr_comp_code=" + $scope.finnDetail.company + "&sltr_ldgr_code=" + $scope.edt.sltr_ldgr_code + "&sltr_slmast_acno=" + $scope.edt.sltr_slmast_acno + "&sltr_doc_code=" + $scope.edt.sltr_doc_code + "&sltr_pstng_date=" + $scope.edt.sltr_pstng_date + "&sltr_doc_date=" + $scope.edt.sltr_doc_date).then(function (GetSLtransactionDetails_Data) {
                        $scope.SLtransactionDetails_Data = GetSLtransactionDetails_Data.data;
                        $scope.totalItems = $scope.SLtransactionDetails_Data.length;
                        $scope.todos = $scope.SLtransactionDetails_Data;
                        $scope.makeTodos();
                    });
                });
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.SLtransactionDetails_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.SLtransactionDetails_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                     item.codp_dept_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.gldc_doc_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sllc_ldgr_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.coad_pty_short_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                     item.sltr_dept_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sltr_doc_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sltr_ldgr_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sltr_slmast_acno.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                     item.sltr_tran_amt_credit.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sltr_tran_amt_debite.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                     item.sltr_pstng_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sltr_comp_code == toSearch) ? true : false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = yyyy + '-' + mm + '-' + dd;
            $scope.edate = yyyy + '-' + mm + '-' + dd;
            $scope.edt = {
                sltr_pstng_date: yyyy + '-' + mm + '-' + dd,
               // sltr_doc_date: yyyy + '-' + mm + '-' + dd,
            }

            $scope.ChkDate = function (noissueafter) {
              
                //var date = new Date();
                //$scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy/MM/dd');
                //var year1 = noissueafter.split("-")[0];
                //var month1 = noissueafter.split("-")[1];
                //var day1 = noissueafter.split("-")[2];
                //var new_end_date = year1 + "-" + month1 + "-" + day1;

                if ($scope.edt.sltr_doc_date < $scope.edt.sltr_pstng_date) {
                    //$rootScope.strMessage = "Please Select Today or Next Date";
                    //$('#message').modal('show');
                    swal({ title: "Alert", text: "Please Select future Date", showCloseButton: true, width: 380, });
                    $scope.edt.sltr_doc_date = "";
                }

            }

            $scope.myFunct = function (keyEvent) {
              
                if (keyEvent.which == 13)
                    $scope.edt.sltr_pstng_date = yyyy + '-' + mm + '-' + dd;
            }

            $scope.myFunct1 = function (keyEvent) {
                
                if (keyEvent.which == 13)
                    $scope.edt.sltr_doc_date = yyyy + '-' + mm + '-' + dd;
            }



          
        }])
})();
