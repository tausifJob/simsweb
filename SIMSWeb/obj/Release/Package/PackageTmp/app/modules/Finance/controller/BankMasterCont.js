﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('BankMasterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            debugger
            $http.get(ENV.apiUrl + "api/BankMaster/getAllBankMaster").then(function (res1) {
                debugger;
                $scope.Bankmaster = res1.data;
                $scope.totalItems = $scope.Bankmaster.length;
                $scope.todos = $scope.Bankmaster;
                $scope.makeTodos();

            });

            //Combobox Dependency

            $scope.getApplication = function (modcode) {
                $scope.app_Name = [];
                $http.get(ENV.apiUrl + "api/UserAudit/getApplicationName?modcode=" + modcode).then(function (appName) {
                    $scope.app_Name = appName.data;
                    console.log($scope.app_Name);
                });
            }


            ////Fill ComboBox Status
            //$http.get(ENV.apiUrl + "api/Financialyear/getDocumentStatus").then(function (docstatus) {

            //    $scope.docstatus_Data = docstatus.data;
            //    console.log($scope.docstatus);
            //});


            //Fill Combo GLAccountNo
            $http.get(ENV.apiUrl + "api/BankMaster/GetGLAccountNumber").then(function (docstatus1) {

                $scope.GlACNO = docstatus1.data;
                console.log($scope.docstatus1);
            });

            //Fill Combo Ledger
            $http.get(ENV.apiUrl + "api/BankMaster/GetLedgerNumber").then(function (docstatus2) {
                debugger;
                $scope.LdgrCode = docstatus2.data;
                console.log($scope.docstatus2);
            });


            //Fill Combo SLACCNO
            $scope.getSLAccNo = function (slcode) {
                $scope.glaccno = true;
                $http.get(ENV.apiUrl + "api/BankMaster/GetSLAccNumber?pbslcode=" + slcode).then(function (docstatus3) {
                    debugger;
                    $scope.slacno = docstatus3.data;
                    console.log($scope.docstatus3);
                });
            }

            //Select Data SHOW

           




            //$http.get(ENV.apiUrl + "api/GoalTarget/getAcademicYear").then(function (Acyear) {
            //    $scope.Acc_year = Acyear.data;
            //    console.log($scope.Acc_year);
            //});
            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                main.checked = false;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Bankmaster, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Bankmaster;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.pb_bank_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.pb_bank_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.pb_srl_no == toSearch) ? true : false;
            }

            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "/" + month + "/" + day;
            }


            //NEW BUTTON
            $scope.New = function () {

                $scope.disabled = false;
                $scope.gtcreadonly = true;
                $scope.readonlybankname = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.ldgno = false;
                $scope.slno = false;
                $scope.cmbstatus = true;
                $scope.txtyear = false;
                $scope.glaccno = false;
                $scope.temp = "";
                $scope.gtdreadonlyBnkcode = false;

            }

            //EnableDesable

            $scope.setvisible = function () {
                debugger;
                $scope.ldgno = true;
                $scope.slno = true;
            }


            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger;
                if (Myform) {

                    if ($scope.temp.acno == undefined && $scope.temp.slma_acno == undefined) {
                        //alert('Please Select atlist one Account Number')
                        swal({ title: "Alert", text: "Please Select atlist one Account Number", showCloseButton: true, width: 300, height: 200 });
                    }
                    else {
                        var data = $scope.temp;
                        data.opr = 'I';
                        dataforSave.push(data);
                        $http.post(ENV.apiUrl + "api/BankMaster/CUDBankMaster", dataforSave).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", showCloseButton: true, width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Bank code allready present", showCloseButton: true, width: 300, height: 200 });
                            }

                            $http.get(ENV.apiUrl + "api/BankMaster/getAllBankMaster").then(function (res1) {

                                $scope.Bankmaster = res1.data;
                                $scope.totalItems = $scope.Bankmaster.length;
                                $scope.todos = $scope.Bankmaster;
                                $scope.makeTodos();
                                $scope.table = true;
                                $scope.display = false;
                            });

                        });
                        datasend = [];
                    }
                }
            }

            //Date FORMAT
            $scope.showdate = function (date, name1) {
                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "-" + month + "-" + day;
            }
            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }
            //DATA EDIT
            $scope.edit = function (str) {
                debugger;

                $scope.gtcreadonly = true;
                $scope.readonlybankname = false;
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.txtyear = true;
                $scope.cmbstatus = false;
                // $scope.temp = str;
                $scope.ldgno = true;
                $scope.slno = true;
                $scope.gtdreadonlyBnkcode = true;
                $scope.pb_gl_acno = str.pb_gl_acno;


                $scope.temp = {
                    pb_bank_code: str.pb_bank_code
                , pb_bank_name: str.pb_bank_name
                , acno: str.pb_gl_acno
                , slma_acno: str.pb_sl_acno
                , sllc_ldgr_code: str.pb_sl_code
                , pb_srl_no: str.pb_srl_no
                , pb_posting_flag: str.pb_posting_flag
                };


            }


            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                if (Myform) {

                    var data = $scope.temp;
                    data.opr = "U";
                    dataforUpdate.push(data);
                    //dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/BankMaster/CUDBankMaster", dataforUpdate).then(function (msg) {

                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", showCloseButton: true, width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", showCloseButton: true, width: 300, height: 200 });
                        }

                        $http.get(ENV.apiUrl + "api/BankMaster/getAllBankMaster").then(function (res1) {
                            $scope.Bankmaster = res1.data;
                            $scope.totalItems = $scope.Bankmaster.length;
                            $scope.todos = $scope.Bankmaster;
                            $scope.makeTodos();
                            $scope.table = true;
                            $scope.display = false;
                        });

                    });
                    data2 = [];

                }
            }

            //DELETE RECORD
            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }




            $scope.OkDelete = function () {
                debugger;
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'pb_bank_code': $scope.filteredTodos[i].pb_bank_code,
                            'pb_comp_code': $scope.filteredTodos[i].pb_comp_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/BankMaster/CUDBankMaster", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BankMaster/getAllBankMaster").then(function (res1) {
                                                $scope.Bankmaster = res1.data;
                                                $scope.totalItems = $scope.Bankmaster.length;
                                                $scope.todos = $scope.Bankmaster;
                                                $scope.makeTodos();
                                                $scope.table = true;
                                                $scope.display = false;
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BankMaster/getAllBankMaster").then(function (res1) {
                                                $scope.Bankmaster = res1.data;
                                                $scope.totalItems = $scope.Bankmaster.length;
                                                $scope.todos = $scope.Bankmaster;
                                                $scope.makeTodos();
                                                $scope.table = true;
                                                $scope.display = false;
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");

                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });






        }])

})();
