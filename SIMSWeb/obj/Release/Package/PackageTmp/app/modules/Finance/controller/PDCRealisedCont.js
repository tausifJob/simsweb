﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var payment_mode, from_dt, to_dt, reference_no, bankcode;
    var acconcode = '';
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PDCRealisedCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.tot_btn = true;
            $scope.Maintabledata = false;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.records = false;
            //$scope.cmbstatus = true;
            //$scope.checked = false;

            var user = $rootScope.globals.currentUser.username;
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            console.log($scope.finnDetail)
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;



            //$scope.block1.cheque =true;
            $scope.block1 = {
                cheque: 'cheque'
            }

            $scope.visible_textbox = function () {
                debugger;
                $scope.records = true;
            }

            //Fill ComboBox Status
            $http.get(ENV.apiUrl + "api/Financialyear/getDocumentStatus").then(function (docstatus) {

                $scope.docstatus_Data = docstatus.data;
                console.log($scope.docstatus);
            });


            //Fill BAnk Name
            $http.get(ENV.apiUrl + "api/PDCSubmission/getBankName").then(function (docstatus1) {
                $scope.bank_name = docstatus1.data;
                console.log($scope.docstatus1);
            });

            //Fill department
            $http.get(ENV.apiUrl + "api/PDCSubmission/getDepartment").then(function (docstatus2) {
                $scope.Department = docstatus2.data;
                console.log($scope.docstatus2);
            });


            //Fill Ledger
            $http.get(ENV.apiUrl + "api/JVCreation/GetLedgerNumber").then(function (docstatus2) {
                $scope.LdgrCode = docstatus2.data;
                console.log($scope.LdgrCode);
            });

            $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code).then(function (docstatus) {

                $scope.getAllAccNos = docstatus.data;
            });

            $scope.getSLAccNo = function () {

                debugger;
                var acconcode = $scope.getAllAccNos.gldd_acct_code;
                if ($scope.block1.gldd_ledger_code != "00") {

                    $http.get(ENV.apiUrl + "api/JVCreation/GetSLAccNumber?ldgr_code=" + $scope.block1.gldd_ledger_code + "&cmp_cd=" + comp_code + "&year=" + finance_year).then(function (docstatus3) {
                        // $scope.sllc_ldgr_code = slcode;
                        $scope.getAllAccNos = docstatus3.data;
                        //$scope.slacno = docstatus3.data;
                    });
                }
                else {
                    $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code).then(function (docstatus) {

                        $scope.getAllAccNos = docstatus.data;
                    });
                }


            }

            //Account number


            //Select Data SHOW
            var showdetails = [];
            $scope.getViewDetails = function () {
                debugger;
                if ($scope.block1.cheque == 'cheque') {
                    payment_mode = 'Ch';
                }
                else {
                    payment_mode = 'Dd';
                }

                //bankcode = [];
                //if ($scope.block1.bank_code.length == 0 || $scope.block1 == undefined) {
                //    bankcode = '';
                //}
                //else {
                //    bankcode1 = $scope.block1.bank_code;
                //    bankcode = bankcode + ',' + bankcode1;;
                //}

                var data = {
                    ps_comp_code: comp_code,
                    ps_dept_no: $scope.block1.dept_no,
                    ps_ldgr_code: $scope.block1.gldd_ledger_code,
                    ps_sl_acno: $scope.block1.gldd_acct_code,
                    ps_discd: 'I',
                    pc_calendar: 'R',
                    ps_bank_code: $scope.block1.bank_code,
                    filterText: $scope.block1.filter_range,
                    filterRange: $scope.block1.filter_data,
                    //recFromDate: ($scope.block1.receipt_date) ? $scope.block1.from_date : null,
                    //recToDate: ($scope.block1.receipt_date) ? $scope.block1.to_date : null,

                    ps_due_dateStr: $scope.block1.from_date,
                    subMissionDate: $scope.block1.to_date,
                    pc_ref_no: $scope.block1.submission_no,
                    pc_payment_mode: payment_mode,

                }
                debugger;


                $http.post(ENV.apiUrl + "api/PDCSubmission/AllPDCSubmission_new_data", data).then(function (res1) {
                    $scope.pdcSubmissionList = res1.data;
                    $scope.pdc_details = res1.data;
                    $scope.totalItems = $scope.pdc_details.length;
                    $scope.todos = $scope.pdc_details;
                    $scope.makeTodos();
                    $scope.Maintabledata = true;

                    debugger;
                    $scope.total = 0;
                    for (var i = 0; i < res1.data.length; i++) {
                        $scope.total = $scope.total + res1.data[i].ps_amount;
                    }

                });


                showdetails = []


            }



            //$http.get(ENV.apiUrl + "api/GoalTarget/getAcademicYear").then(function (Acyear) {
            //    $scope.Acc_year = Acyear.data;
            //    console.log($scope.Acc_year);
            //});

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.fin_doc, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.fin_doc;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_financial_year.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_financial_year_status.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_doc_srno == toSearch) ? true : false;
            }

            var cntr = 0;
            $scope.selectonebyone = function (str) {
                //debugger;
                //var d=document.getElementById(str);
                if (str == true) {
                    cntr = cntr + 1;
                }
                else { cntr = cntr - 1; }
            }



            //DATA SAVE INSERT for cheque Transfer
            var datasend = [];
            $scope.savedata = function () {//Myform


                if ($scope.block1.cheque == 'cheque') {
                    payment_mode = 'Ch';
                }
                else  {
                    payment_mode = 'Dd';
                }
                // if (Myform) {
                if ($scope.block1.pdc_clearance_date != undefined) {
                    debugger;
                    //  var data = $scope.temp;
                    //data.opr = 'I';
                    for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {

                        if ($scope.pdcSubmissionList[i].ps_cheque_no_check == true) {

                            $scope.pdcSubmissionList[i].ps_approv_date = $scope.block1.pdc_clearance_date;
                            $scope.pdcSubmissionList[i].ps_ldgr_code_name = $rootScope.globals.currentUser.username;
                            //$scope.filteredTodos[i].cnt = 2;
                            $scope.pdcSubmissionList[i].pc_payment_mode = payment_mode;
                            $scope.pdcSubmissionList.opr = 'I';
                            datasend.push($scope.pdcSubmissionList[i]);
                        }
                    }
                    $http.post(ENV.apiUrl + "api/PDCSubmission/Finn102_PDC_Realise", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        swal({ text: $scope.msg1.strMessage, timer: 50000 });
                        //swal({ title: "Alert", text: $scope.msg1.strMessage, imageUrl: "assets/img/check.png", });

                    });
                    // getViewDetails();
                }
                else {
                    swal({ text: "please select Cheque Clearance Date", timer: 5000 });
                }
                datasend = [];
            }

            //Date FORMAT
            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "-" + month + "-" + day;
            }


            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                    if (main.checked == true) {
                        $scope.pdcSubmissionList[i].ps_cheque_no_check = true;
                        $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                    }
                    else {
                        $scope.pdcSubmissionList[i].ps_cheque_no_check = false;
                        $scope.pdcSubmissionList[i].row_color = ''
                    }
                }

            }

            $scope.ShowFilterText = function (str) {
                if (str == "") {
                    $scope.filterVisible = false;
                }
                else {

                    $scope.filterVisible = true;
                }
            }

            $scope.FilterCode = function () {
                debugger;
                var splitno = [];
                if ($scope.temp.chk_previous_clear == true) {
                    main = document.getElementById('mainchk');
                    for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {

                        var v = document.getElementById(i + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $scope.pdcSubmissionList[i].row_color = 'white';

                    }
                    if ($scope.edt.SelectOption == 'Select Records') {
                        debugger;

                        for (var i = 0; i < $scope.edt.txt_filter_range; i++) {
                            var v = document.getElementById(i + i);
                            v.checked = true;
                            $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                        }
                    }

                    else if ($scope.edt.SelectOption == 'Cheque Amount between') {


                        if ($scope.edt.txt_filter_range != undefined) {
                            splitno = $scope.edt.txt_filter_range.split('-');
                        }
                        if (splitno.length >= 2) {

                            for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                                if ($scope.pdcSubmissionList[i].ps_amount >= splitno[0] && $scope.pdcSubmissionList[i].ps_amount <= splitno[1]) {

                                    var v = document.getElementById(i + i);
                                    v.checked = true;
                                    $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                                }
                            }
                        }
                    }
                    else if ($scope.edt.SelectOption == 'Cheque Series between') {


                        if ($scope.edt.txt_filter_range != undefined) {
                            debugger;
                            splitno = $scope.edt.txt_filter_range.split('-');
                        }
                        if (splitno.length >= 2) {
                            $scope.minchkno = 0;
                            $scope.maxchkno = 0;
                            for (var j = 0; j < splitno.length; j++) {
                                if (j == 0) {
                                    $scope.minchkno = parseInt(splitno[0]);
                                }
                                else {
                                    $scope.maxchkno = parseInt(splitno[1]);
                                }

                            }

                            for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                                debugger;
                                if ($scope.pdcSubmissionList[i].ps_cheque_no >= $scope.minchkno && $scope.pdcSubmissionList[i].ps_cheque_no <= $scope.maxchkno) {

                                    var v = document.getElementById(i + i);
                                    v.checked = true;
                                    $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                                }
                            }
                        }

                    }
                }

                else {

                    if ($scope.edt.SelectOption == 'Select Records') {

                        if ($scope.pdcSubmissionList.length >= $scope.edt.txt_filter_range) {
                            for (var i = 0; i < $scope.edt.txt_filter_range; i++) {
                                var v = document.getElementById(i + i);
                                v.checked = true;
                                $scope.pdcSubmissionList[i].row_color = '#ffffcc'

                            }
                        }
                    }

                    else if ($scope.edt.SelectOption == 'Cheque Amount between') {


                        if ($scope.edt.txt_filter_range != undefined) {
                            splitno = $scope.edt.txt_filter_range.split('-');
                        }
                        if (splitno.length >= 2) {

                            for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                                if ($scope.pdcSubmissionList[i].ps_amount >= splitno[0] && $scope.pdcSubmissionList[i].ps_amount <= splitno[1]) {

                                    var v = document.getElementById(i + i);
                                    v.checked = true;
                                    $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                                }
                            }
                        }
                    }
                    else if ($scope.edt.SelectOption == 'Cheque Series between') {


                        if ($scope.edt.txt_filter_range != undefined) {
                            splitno = $scope.edt.txt_filter_range.split('-');
                        }
                        if (splitno.length >= 2) {

                            $scope.minchkno = 0;
                            $scope.maxchkno = 0;
                            for (var j = 0; j < splitno.length; j++) {
                                if (j == 0) {
                                    $scope.minchkno = parseInt(splitno[0]);
                                }
                                else {
                                    $scope.maxchkno = parseInt(splitno[1]);
                                }

                            }

                            for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                                debugger;
                                if ($scope.pdcSubmissionList[i].ps_cheque_no >= $scope.minchkno && $scope.pdcSubmissionList[i].ps_cheque_no <= $scope.maxchkno) {

                                    var v = document.getElementById(i + i);
                                    v.checked = true;
                                    $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                                }
                            }

                            //for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                            //    if ($scope.pdcSubmissionList[i].ps_cheque_no >= splitno[0] && $scope.pdcSubmissionList[i].ps_cheque_no <= splitno[1]) {

                            //        var v = document.getElementById(i);
                            //        v.checked = true;
                            //        $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                            //    }
                            //}
                        }

                    }

                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            //Selection Procedure
            $scope.Getdates = function (str) {



                for (var i = 0; i < $scope.docyear_Data1.length; i++) {
                    if ($scope.docyear_Data1[i].sims_academic_year == str) {
                        $scope.temp = {
                            sims_financial_year_start_date: $scope.docyear_Data1[i].sims_financial_year_start_date,
                            sims_financial_year_end_date: $scope.docyear_Data1[i].sims_financial_year_end_date,
                            sims_financial_year: $scope.docyear_Data1[i].sims_academic_year,


                        };
                    }
                }



            }

        }])

})();