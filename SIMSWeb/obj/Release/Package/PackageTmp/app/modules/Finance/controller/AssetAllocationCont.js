﻿
(function () {
    'use strict';
    var data1 = [];
    var dataforInsert = [];
    var dataforUpdate = [];
    var temp, main;
    var deleteReportCard = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AssetAllocationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.edt = [];
            $scope.Expand1 = true
            $scope.Expand = true;
            $scope.Reset = true;
            $scope.savebtn = false;
            $scope.giveallocation1 = false;
            $scope.searched_data = false;
            $rootScope.visible_stud = true;
            $rootScope.visible_parent = true;
            $rootScope.visible_search_parent = true;
            $rootScope.visible_teacher = true;
            $rootScope.visible_User = true;
            $rootScope.visible_Employee = true;
            var user = $rootScope.globals.currentUser.username.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
            $rootScope.chkMulti = false;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $http.get(ENV.apiUrl + "api/AssetAllocation/Get_Asset_Type_Finn218?comp_code=" + comp_code).then(function (res) {

                $scope.assettype = res.data;
            })

            $http.get(ENV.apiUrl + "api/AssetAllocation/Get_Asset_Location_Finn218?comp_code=" + comp_code).then(function (res) {

                $scope.assetlocation = res.data;
            })

            $http.get(ENV.apiUrl + "api/AssetAllocation/Get_Asset_Department_Finn218?financialyear=" + finance_year + "&comp_code=" + comp_code).then(function (res) {
                $scope.assetdept = res.data;
            })

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 8;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                console.log("begin=" + begin); console.log("end=" + end);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.Reset1 = function () {
                $scope.datatable = false;
                $scope.search_data_model = [];
                $scope.edt.marks = '';
                $scope.edt['edt.markupdate'] = false;
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.StudenteData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.StudenteData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in 2 fields */
                return (item.sims_student_passport_full_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_student_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_student_enroll_number == toSearch) ? true : false;
            }

            $scope.ShowRecord = function () {
                if ($scope.edt.gal_asst_type == undefined || $scope.edt.gal_asst_type == "") {
                    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please select asset type", showCloseButton: true, width: 380, });
                }
                else if ($scope.edt.gam_location == undefined || $scope.edt.gam_location == "") {
                    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please select location", showCloseButton: true, width: 380, });
                }
                else if ($scope.edt.codp_dept_no == undefined || $scope.edt.codp_dept_no == "") {
                    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please select Department Name", showCloseButton: true, width: 380, });
                }

                else {

                    $http.get(ENV.apiUrl + "api/AssetAllocation/Get_AllAsset_Item_Finn218?asset_type=" + $scope.edt.gal_asst_type +
                                           "&location=" + $scope.edt.gam_location + "&dept_no=" + $scope.edt.codp_dept_no).then(function (res) {
                                               $scope.allasset = res.data;
                                               $scope.totalItems = $scope.allasset.length;
                                               $scope.todos = $scope.allasset;
                                               $scope.makeTodos();
                                               $scope.searched_data = true;
                                           });
                }
            }

            $scope.Reset = function () {
                $scope.edt.gal_asst_type = "";
                $scope.edt.gam_location = "";
                $scope.edt.codp_dept_no = "";
                $scope.searched_data = false;
            }

            $scope.check_once = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.Save = function () {
                dataforInsert = [];
                //var emp_code= $scope.edt.emp_code;
                $scope.flag = false;
                if ($scope.obj.emp_code == undefined || $scope.obj.emp_code == "") {
                    //    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please Enter Or Search User Code", showCloseButton: true, width: 380, });
                }
                else {
                    for (var i = 0; i < $scope.allasset.length; i++) {
                        var v = document.getElementById(i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var data = ({
                                gam_item_no: $scope.allasset[i].gam_item_no
                                 , gam_desc_1: $scope.allasset[i].gam_desc_1
                                , emp_code: $scope.allasset[i].emp_code
                                , fal_from_date: $scope.allasset[i].fal_from_date
                                , fal_upto_date: $scope.allasset[i].fal_upto_date
                                , gam_status: $scope.allasset[i].gam_status
                            });
                            dataforInsert.push(data);
                        }
                    }
                    if ($scope.flag) {
                        $http.post(ENV.apiUrl + "api/AssetAllocation/Insert_Asset_Location", dataforInsert).then(function (msg) {
                            debugger
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 380 });
                                dataforInsert = [];
                                $scope.ShowRecord();
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Inserted", width: 380 });
                            }
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                    }
                }
            }

            //$scope.Search = function () {

            //    $('#SearchEmpList').modal('show');
            //}

            $scope.Search = function (str) {
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                //$('#stdSearch').modal('show');
                $scope.obj = str;
            }

            $scope.$on('global_cancel', function (str) {

                console.log($scope.SelectedUserLst);
                if ($scope.SelectedUserLst.length > 0) {
                    $scope.obj['em_first_name'] = $scope.SelectedUserLst[0].empName;
                    $scope.obj['emp_code'] = $scope.SelectedUserLst[0].em_number;
                }
                // $scope.getstudentList();
            });

            $scope.SearchEmployee = function () {
                $scope.searchemp = true;
                $http.post(ENV.apiUrl + "api/EmpDocumentUp/searchEmployeeList").then(function (Employee_Data) {
                    $scope.EmployeeData = Employee_Data.data;
                    $scope.totalItems = $scope.EmployeeData.length;
                    $scope.todos = $scope.EmployeeData;
                    $scope.makeTodos();

                    console.log($scope.EmployeeData);
                });

                $scope.Add = function (str) {
                    $scope.edt["em_first_name"] = str.em_first_name;
                    $scope.edt["emp_code"] = str.em_number;
                }
            }

            $http.post(ENV.apiUrl + "api/AssetAllocation/searchEmployeeList").then(function (Employee_Data) {
                $scope.EmployeeData = Employee_Data.data;
                console.log($scope.EmployeeData);
            });


        }])
})();