﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Security');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('UserGroupCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.libmemcode = true;
            $scope.temp = {};
            $scope.temp['sims_library_membership_type_status'] = true;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/UserGroup/getUserGroup").then(function (res1) {
                debugger;
                $scope.user_grp = res1.data;
                $scope.totalItems = $scope.user_grp.length;
                $scope.todos = $scope.user_grp;
                $scope.makeTodos();
            });


            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.user_grp, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.user_grp;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                return (item.comn_user_group_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.comn_user_group_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {

                $scope.disabled = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.divcode_readonly = false;
                $scope.temp = "";
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.cancel_btn = false;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.libmemcode = true;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            //DATA EDIT
            $scope.edit = function (str) {
                $scope.table = false;
                $scope.temp = angular.copy(str);
                $scope.divcode_readonly = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.cancel_btn = true;
                $scope.libmemcode = true;
            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger;
                if (Myform) {

                    var data = $scope.temp;
                    data.opr = 'I';

                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/UserGroup/CUDUserGroup", datasend).then(function (msg) {
                        datasend = [];
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();
                        $scope.temp["comn_user_group_name"] = "";
                        $scope.temp["comn_user_group_code"] = "";
                        $scope.msg1 = msg.data;

                        $http.get(ENV.apiUrl + "api/UserGroup/getUserGroup").then(function (res1) {
                            $scope.user_grp = res1.data;
                            $scope.totalItems = $scope.user_grp.length;
                            $scope.todos = $scope.user_grp;
                            $scope.makeTodos();
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "User Group Code alreday present", width: 300, height: 200 });
                            }

                        });

                    });
                }

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                if (Myform) {
                    debugger;
                    var data = $scope.temp;
                    data.opr = "U";
                    dataforUpdate.push(data);
                    //dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/UserGroup/CUDUserGroup", dataforUpdate).then(function (msg) {
                        dataforUpdate = [];
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();
                        $scope.temp["comn_user_group_code"] = "";
                        $scope.temp["comn_user_group_name"] = "";
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                        //   swal({ text: $scope.msg1.strMessage, timer: 5000 });

                        $http.get(ENV.apiUrl + "api/UserGroup/getUserGroup").then(function (res1) {
                            $scope.user_grp = res1.data;
                            $scope.totalItems = $scope.user_grp.length;
                            $scope.todos = $scope.user_grp;
                            $scope.makeTodos();
                        });

                    });
                    $scope.table = true;
                    //$scope.table = true;
                    $scope.display = false;
                    $scope.cancel_btn = false;
                    $scope.save_btn = true;
                    $scope.Update_btn = false;
                    $scope.libmemcode = true;
                }
            }


            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                debugger;
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'comn_user_group_code': $scope.filteredTodos[i].comn_user_group_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/UserGroup/CUDUserGroup", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/UserGroup/getUserGroup").then(function (res1) {
                                                $scope.user_grp = res1.data;
                                                $scope.totalItems = $scope.user_grp.length;
                                                $scope.todos = $scope.user_grp;
                                                $scope.makeTodos();
                                            });

                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted ", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/UserGroup/getUserGroup").then(function (res1) {
                                                $scope.user_grp = res1.data;
                                                $scope.totalItems = $scope.user_grp.length;
                                                $scope.todos = $scope.user_grp;
                                                $scope.makeTodos();
                                            });

                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });


        }])

})();







