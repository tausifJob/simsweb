﻿
(function () {
    'use strict';

    var simsController = angular.module('sims.module.main');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('MainController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', 'AuthenticationService', '$interval', 'Idle', 'Keepalive', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, AuthenticationService, $interval, Idle, Keepalive) {
            var user = $rootScope.globals.currentUser.username;
            $rootScope.isCondensed = false;
            $scope.lang = "en";
            $scope.new_std = "";
            $rootScope.strMessage = "";
            $scope.SchoolLogo = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/SchoolLogo/';
            $rootScope.module = '';
            $rootScope.location = '';
            $scope.SelectedUserLst = [];
            $scope.global_Search = {}
            $scope.global_search_result = [];

            if ($http.defaults.headers.common['schoolId'] == 'qfis' || $http.defaults.headers.common['schoolId'] == 'pearl' || $http.defaults.headers.common['schoolId'] == 'dpsmis') {
                $scope.languageicon = true;
            }
            debugger
            //if ($http.defaults.headers.common['schoolId'] == 'adisw') {
            //    $scope.feeDetailsshow1 = true;
            //}


            $scope.redirect = function (i) {
                console.log(i);
                $rootScope.module = i;
                $state.go('main.UserMenu');
                $rootScope.$broadcast('module-chage', { data: $rootScope.module });
            }

            //var dataAlert = {
            //    fromdate:'',
            //    todate: '',
            //    usercode: $rootScope.globals.currentUser.username,
            //    alert_status: '',
            //    module_code: '',
            //}

            //$http.post(ENV.apiUrl + "api/common/Alert/AlertDetails", dataAlert).then(function (AlertDetails) {
            //    //$scope.AlertDetails = AlertDetails.data;
            //    $scope.alert_count = AlertDetails.data.length;
            //});
            $http.get(ENV.apiUrl + "api/common/Alert/getUserCount?user=" + $rootScope.globals.currentUser.username).then(function (AlertDetails) {
                $scope.AlertDetails = AlertDetails.data;
                //$scope.alert_count = AlertDetails.data.length;
                debugger;
                $scope.alert_count = $scope.AlertDetails[0].unread_status;


            });


            $scope.propertyName = null;
            $scope.reverse = false;
            // propertyName = null; reverse = false
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };


            $scope.RedirectCommunicationPage = function () {
                $state.go('main.communication')
            }

            $scope.goAlert = function () {
                $state.go('main.Sim166')
            }


            $scope.total_counts = 0;

            $scope.getTotal_Communication_count = function () {
                $http.get(ENV.apiUrl + "api/Communication/getPortalCommunicationCounts?user_code_from=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.total_counts = res.data;
                });
            }
            $scope.getTotal_Communication_count();
            $scope.lms_redirct = function (str) {
                if (str == 'gblsea') {

                    $scope.searchGlobalClick();


                }

                if ($http.defaults.headers.common['schoolId'] == 'imert') {
                    if (str == 'Lms001' || str == '2') {
                        window.open('http://imert.learntron.net/learntron/index.html', "_new");

                    }
                }


                if ($http.defaults.headers.common['schoolId'] == 'adis') {
                    if (str == '2')   //Online Access
                    {
                        window.open('http://oa.mograsys.com/adis/OnlineAdmission.html#?flag=true', "_new");
                    }

                    if (str == 'Lms001') {

                        $http.get(ENV.apiUrl + "api/common/getUserDetails_pass?uname=" + $rootScope.globals.currentUser.username).then(function (res) {
                            // $scope.obj = res.data;
                            debugger
                            console.log(res.data.table[0].comn_user_password);
                            window.open("http://lms.mograsys.com/mdladis/autologin.php" + "?u=" + $rootScope.globals.currentUser.username + "&p=" + res.data.table[0].comn_user_password, "_new");
                            //  $state.go('main.dashboard');

                            //$('#load').show();

                            //$scope.uri = "http://www.google.com/";
                            //$scope.url = encodeURI($scope.uri);
                            //$("#load").attr("src", $sce.trustAsResourceUrl($scope.url));

                        });


                    }
                }

                if ($http.defaults.headers.common['schoolId'] == 'adisw') {


                    if (str == 'Lms001') {

                        $http.get(ENV.apiUrl + "api/common/getUserDetails_pass?uname=" + $rootScope.globals.currentUser.username).then(function (res) {
                            // $scope.obj = res.data;
                            debugger
                            console.log(res.data.table[0].comn_user_password);
                            window.open("http://lms.mograsys.com/mdladisw/autologin.php" + "?u=" + $rootScope.globals.currentUser.username + "&p=" + res.data.table[0].comn_user_password, "_new");
                            //  $state.go('main.dashboard');

                            //$('#load').show();

                            //$scope.uri = "http://www.google.com/";
                            //$scope.url = encodeURI($scope.uri);
                            //$("#load").attr("src", $sce.trustAsResourceUrl($scope.url));

                        });


                    }
                }

                if ($http.defaults.headers.common['schoolId'] == 'gsis') {
                    if (str == 'Lms001')   //Online Access
                    {
                        window.open('http://lms.mograsys.com/mdlgsis/', "_new");
                    }
                }
                if (str == 'Lms003')   //Online Access
                {
                    window.open('http://10.10.200.3:4000/learntron', "_new");
                }
                if (str == 'Lms002') //local
                {
                    window.open('http://212.77.213.147:4000/learntron', "_new");
                }
            }
            $scope.dashboard_load = function () {
                $state.go('main.EmpDshBrd')
            }

            $scope.timeline_load = function () {
                $state.go('main.TimeLine')
            }


            $http.get(ENV.apiUrl + "api/common/GetSystemAlerts").then(function (res) {

            });


            $http.get(ENV.apiUrl + "api/common/getUserAllDetails?uname=" + $rootScope.globals.currentUser.username).then(function (res) {
                //console.log(res.data);
                $scope.uimage = 'http://api.mograsys.com/APIERP/Content/' + $http.defaults.headers.common['schoolId'] + '/'
                // console.log('dse')

                if (res.data.group_Code == '06') {
                    $scope.flg_ck_group = true;
                }
                else
                    $scope.flg_ck_group = false;
                // console.log(res.data)
                $scope.user_details = res.data;
            });

            //$scope.changePassword = function () {
            //    var data = {
            //        OldPasscode: $scope.temp.OldPasscode,
            //        NewPasscode: $scope.temp.NewPasscode,
            //        UserName: $rootScope.globals.currentUser.username
            //    }
            //    $scope.temp.UserName = $rootScope.globals.currentUser.username;
            //    $http.post(ENV.apiUrl + "api/common/ResetPassword?str=" + JSON.stringify(data)).then(function (res) {
            //        $scope.display = true;
            //        $scope.obj = res.data;
            //        console.log($rootScope.globals.currentUser.username);
            //        AuthenticationService.ClearCredentials();
            //        $state.go("login");
            //    });
            //    console.log("change");
            //}

            $scope.changePassword = function () {
                debugger
                var data =
                    {
                        OldPasscode: $scope.temp.OldPasscode,
                        NewPasscode: $scope.temp.NewPasscode,
                        UserName: $rootScope.globals.currentUser.username
                    }
                var NewPasscode = $scope.temp.NewPasscode;
                var confirmpass = $scope.temp.cnfrmPass;

                if (NewPasscode == confirmpass) {
                    $scope.temp.UserName = $rootScope.globals.currentUser.username;
                    $http.post(ENV.apiUrl + "api/common/ResetPassword?str=" + JSON.stringify(data)).then(function (res) {
                        $scope.msg1 = res.data;
                        debugger;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Password Changes Sussessfully", showCloseButton: true, width: 300, height: 200 });
                            $('#top').modal('hide');
                            $(".modal-backdrop").removeClass("modal-backdrop");

                            $scope.display = true;
                            $scope.obj = res.data;
                            console.log($rootScope.globals.currentUser.username);
                            AuthenticationService.ClearCredentials();
                            setTimeout(function () { $state.go("login"); }, 1500)
                            $scope.temp["NewPasscode"] = "";
                            $scope.temp["cnfrmPass"] = "";
                            $scope.temp["OldPasscode"] = "";
                        }
                        else {
                            swal({ title: "Alert", text: "Plese Enter Correct Old Password", showCloseButton: true, width: 300, height: 200 });
                            $('#top').modal('show');
                            $scope.temp["OldPasscode"] = "";

                        }

                    });
                }

                else {
                    swal({ title: "Alert", text: "New Password And Confirm Password Doesn`t Match ", showCloseButton: true, width: 300, height: 200 });
                    $('#top').modal('show');
                    $scope.temp["cnfrmPass"] = "";
                }


                //console.log("change");
            }


            $(window).bind('keydown', function (event) {

                if (event.keyCode == '13' && $scope.global_Search_show) {

                    if ($scope.gbl_tab == 'tab1') {
                        $scope.Global_Search_by_student();
                    }
                    else if ($scope.gbl_tab == 'tab2') {
                        $scope.Global_Search_by_parent();
                    }
                    else if ($scope.gbl_tab == 'tab3') {
                        $scope.Global_Search_SearchParentSectionWise();
                    }
                    else if ($scope.gbl_tab == 'tab4') {
                        $scope.Global_Search_by_Teacher();
                    }
                    else if ($scope.gbl_tab == 'tab5') {
                        $scope.Global_Search_by_User();
                    }
                    else if ($scope.gbl_tab == 'tab6') {
                        $scope.Global_Search_by_employee();
                    }
                    else if ($scope.gbl_tab == 'tab7') {
                        $scope.Global_Search_by_Uassignedstudent();
                    }
                    else if ($scope.gbl_tab == 'tab8') {
                        $scope.Global_Search_by_parent_unassigned();
                    }
                }
            });



            $scope.navigate = function (str) {
                console.log(str);
                switch (str) {
                    case 1:
                        if ($http.defaults.headers.common['schoolId'] == 'oes')
                            $state.go("main.UpdOES");
                        else
                            $state.go("main.UpdateEmployee");
                        break;
                    case 2:
                        $state.go("main.UpdateEmployee");
                        break;

                    case 3:
                        $('#ResetPasswordModal').modal('show');
                        break;
                }
            }


            $scope.url = ENV.siteUrl;

            console.log("In Main Page::" + $rootScope.globals.currentUser.username);

            $scope.started = false;


            $scope.$on('IdleStart', function () {

                $scope.Fidle = false;



                //$('#idlepop').modal('show');
                $('#idlepop').modal({ backdrop: 'static', keyboard: false });


            });

            $scope.$on('IdleEnd', function () {
                //closeModals();
                $scope.Fidle = false;

                $('#idlepop').modal('hide');
            });

            $scope.$on('IdleTimeout', function () {
                //closeModals();
                //$('#timeoutpop').modal('hide');

                $scope.Fidle = true;

                //    $('#idlepop').modal('show');

                $('#idlepop').modal({ backdrop: 'static', keyboard: false });




            });


            // finance Company
          

            $scope.financeWindow = function () {
                $scope.fin = [];
                $http.get(ENV.apiUrl + "api/PDCSubmission/GetComp_Name?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.cmbCompany = res.data;
                    if (res.data.length > 1) {
                        $scope.fin.comp_code = res.data[0].comp_code;
                        $http.get(ENV.apiUrl + "api/PDCSubmission/GetFinancial_year?comp_code=" + $scope.fin.comp_code).then(function (res) {
                            $scope.cmbYear = res.data;
                            if (res.data.length > 0) {
                                $scope.fin.financial_year = res.data[0].financial_year;
                            }
                        });
                    }


                });

                //$('#myModal3').modal('show');

                $('#myModal3').modal({ backdrop: 'static', keyboard: false });

            }

            $scope.company_change = function (str) {
                $http.get(ENV.apiUrl + "api/PDCSubmission/GetFinancial_year?comp_code=" + str).then(function (res) {
                    $scope.cmbYear = res.data;
                });
            }

            $scope.submit = function () {
                if ($scope.fin.comp_code != undefined) {
                    if ($scope.fin.financial_year != undefined) {
                        $http.post(ENV.apiUrl + "api/PDCSubmission/UpdateFinancial_year?comp_cd=" + $scope.fin.comp_code + "&year=" + $scope.fin.financial_year).then(function (res) {
                            if (res.data) {
                                var data = {
                                    year: $scope.fin.financial_year,
                                    company: $scope.fin.comp_code,
                                    companyame: ''
                                }
                                window.localStorage["Finn_comp"] = JSON.stringify(data)

                                $scope.companyName = $("#finCompany").find("option:selected").text();
                                //$.cookie("finnComapny", $scope.fin.comp_code);
                                //$.cookie("finnYear", $scope.fin.financial_year);
                                $('#myModal3').modal('hide');
                            }
                            else {
                            }
                        });
                    }
                }
            }



            $scope.changeColor = function (str, i) {
                $scope.test = '#mod' + i
                $scope.sd = $scope.obj2[i].module_color;
                $($scope.test).css({ 'background-color': $scope.sd })
            }

            $scope.resetColor = function (i) {
                $scope.test = '#mod' + i
                $scope.sd = $scope.obj2[i].module_color;
                $($scope.test).css({ 'background-color': '' })
            }

            $scope.idSelectedMenu = null;
            $scope.setSelected = function (idSelectedMenu) {
                $scope.idSelectedMenu = idSelectedMenu;
                console.log(idSelectedMenu);
            }

            $scope.start = function () {
                //   closeModals();
                Idle.watch();
                $scope.started = true;
            };

            $scope.stop = function () {
                // closeModals();
                Idle.unwatch();
                $scope.started = false;

            };


            setTimeout(function () {
                $("#Applsource").select2();

            }, 100);

            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {

                //if (isMobile.any()) {
                $rootScope.mobStyle = { 'display': 'block' };
                //   $rootScope.moblie_logout = { 'display': 'block' };
                $('.user-info-wrapper').css({ 'display': 'block' });
                $('.mobstud').css({ 'display': 'block' });
                $scope.mobleView = true;
            }
            else {
                $rootScope.mobStyle = { 'display': 'none' };
                // $rootScope.moblie_logout = { 'display': 'none' };
                $('.user-info-wrapper').css({ 'display': 'none' });
                $('.mobstud').css({ 'display': 'none' });
                $scope.mobleView = false;

            }

            $scope.menutoggle = function () {


                if ($scope.mobile == true) {

                    $('body').addClass('breakpoint-768 pace-done open-menu-left');
                    $('#main-menu').addClass('page-sidebar visible');
                    $scope.mobile = false;
                }
                else {
                    $('body').removeClass('breakpoint-768 pace-done open-menu-left');
                    $('body').addClass('breakpoint-768 pace-done ');
                    $('#main-menu').removeClass('page-sidebar visible');
                    $('#main-menu').addClass('page-sidebar');
                    $scope.mobile = true;

                }
            }


            //$('body').addClass('grey condense-menu');
            //$('#main-menu').addClass('mini');
            //$('.page-content').addClass('condensed');

            //$rootScope.isCondensed = true;

            //$("#main-menu").mouseover(function () {
            //    // $("#log").append("<div>Handler for .mouseover() called.</div>");
            //    $('body').removeClass('grey condense-menu');
            //    $('#main-menu').removeClass('mini');
            //    $('.page-content').removeClass('condensed');
            //    $rootScope.isCondensed = false;
            //});


            //$("#main-menu").mouseout(function () {
            //    $('body').addClass('grey condense-menu');
            //    $('#main-menu').addClass('mini');
            //    $('.page-content').addClass('condensed');

            //    $rootScope.isCondensed = true;
            //});

            $scope.submnuClick = function () {
                $('body').removeClass('breakpoint-768 pace-done open-menu-left');
                $('body').addClass('breakpoint-768 pace-done ');
                $('#main-menu').removeClass('page-sidebar visible');
                $('#main-menu').addClass('page-sidebar');
            }

            $http.get(ENV.apiUrl + "api/common/Get_Application?s=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.appl_items = res.data;
                // console.log($scope.appl_items);
            });

            $scope.getDateTime = function () {
                var now = new Date();
                var year = now.getFullYear();
                var month = now.getMonth() + 1;
                var day = now.getDate();
                var hour = now.getHours();
                var minute = now.getMinutes();
                var second = now.getSeconds();
                if (month.toString().length == 1) {
                    var month = '0' + month;
                }
                if (day.toString().length == 1) {
                    var day = '0' + day;
                }
                if (hour.toString().length == 1) {
                    var hour = '0' + hour;
                }
                if (minute.toString().length == 1) {
                    var minute = '0' + minute;
                }
                if (second.toString().length == 1) {
                    var second = '0' + second;
                }
                var dateTime = year + '/' + month + '/' + day + ' ' + hour + ':' + minute + ':' + second;
                return dateTime;
            }



            $scope.application_change = function (str1, index) {

                try {
                    var index = $("select[name='Applsource'] option:selected").index();
                    console.log(index)

                    var state = 'main.' + str1;
                    console.log($scope.appl_items[index]);
                    $scope.location = $scope.appl_items[index].location;
                    $rootScope.location = $scope.location;

                    var mo = '#m' + $scope.appl_items[index].comn_mod_code;
                    var mod = '#mod' + $scope.appl_items[index].comn_mod_code;
                    if ($scope.mod_old != undefined) {
                        $(('#mod' + $scope.mod_old)).css({ 'display': 'none' });

                    }
                    //$(mod).focus(function () { console.log( 'focus')})
                    console.log('applode');

                    console.log(str1);
                    var v = document.getElementById(str1);

                    console.log(v);

                    $(mod).css({ 'display': 'block' });

                    $scope.mod_old = $scope.appl_items[index].comn_mod_code;

                    // console.log($scope.appl_items[index].comn_mod_code)
                    //jQuery(mod).find('start ng-scope').replaceWith('start ng-scope open');
                    //$(mo).click();
                    $scope.idSelectedMenu = str1;

                    //var s = '#' + str1;
                    //$('#Per305').click();

                    console.log(str1);
                }
                catch (ex) {
                }

                if ($scope.appl_items[index].comn_appl_type == 'R') {
                    setTimeout(function () { $state.go('main.ReportCard'); }, 100);


                    $state.go('main');
                    //$state.go('main.ReportCard');

                }
                else {
                    if ($scope.fin == undefined) {
                        if ($scope.appl_items[index].comn_mod_code == '005') {
                            $http.get(ENV.apiUrl + "api/PDCSubmission/GetComp_Name?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                                $scope.cmbCompany = res.data;
                                if (res.data.length == 1) {
                                    $scope.companyName = res.data[0].comp_name;

                                    $scope.finnComp = res.data[0].comp_code;

                                    $http.get(ENV.apiUrl + "api/PDCSubmission/GetFinancial_year?comp_code=" + $scope.finnComp).then(function (res) {

                                        if (res.data.length > 0) {

                                            console.log('Finn');

                                            var data = {
                                                year: res.data[0].financial_year,
                                                company: $scope.finnComp,
                                                companyame: $scope.companyName
                                            }
                                            window.localStorage["Finn_comp"] = JSON.stringify(data)

                                        }
                                    });

                                    //  AuthenticationService.SetCompany(res.data[0].comp_code, '2017');



                                }
                                else if (res.data.length > 1) {
                                    $scope.financeWindow();
                                }


                            });
                        }
                    }

                    try {
                        $state.go(state);
                    }
                    catch (ex) { }
                }

                try {
                    if (!$rootScope.isCondensed) {
                        v.scrollIntoView();
                    }
                }
                catch (e) { }
                if ($scope.oldobj == undefined) {


                    $scope.oldobj = {
                        opr: 'I',
                        comn_audit_user_code: $rootScope.globals.currentUser.username,
                        comn_appl_name_en: str1,
                        comn_audit_start_time: $scope.getDateTime(),
                        //comn_audit_end_time: '',
                    }
                    $http.post(ENV.apiUrl + "api/common/InsertUserAudit", $scope.oldobj).then(function (res) {

                    });
                }
                else {

                    var edate = $scope.getDateTime()
                    var data = {
                        opr: 'I',
                        comn_audit_user_code: $rootScope.globals.currentUser.username,
                        comn_appl_name_en: str1,
                        comn_audit_start_time: edate,
                        //comn_audit_end_time: '',
                    }
                    //$scope.oldobj['comn_audit_start_time'] = $scope.getDateTime()

                    $http.post(ENV.apiUrl + "api/common/InsertUserAudit", data).then(function (res) {


                        $scope.oldobj['comn_audit_end_time'] = edate
                        $scope.oldobj['opr'] = 'J'

                        $http.post(ENV.apiUrl + "api/common/InsertUserAudit", $scope.oldobj).then(function (res) {

                            $scope.oldobj['comn_audit_start_time'] = edate
                            $scope.oldobj['comn_appl_name_en'] = str1

                        });

                    });

                }


                //$("#m000").click(function (event) {
                //    // alert(event.target.id);
                //});

                $scope.lms_redirct(str1);
                window.localStorage.removeItem("ReportProvNumber");

            }

            $scope.$on('circular-created')
            {
                console.log('callled root');
                console.log('circular-created root');
                $http.get(ENV.apiUrl + "api/common/Circular/getUserDetails?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.circularobj = res.data;
                    $scope.newsobj = res.data;
                });
            }
            $rootScope.$on('circular-created')
            {
                console.log('callled root');
                console.log('circular-created root');
                $http.get(ENV.apiUrl + "api/common/Circular/getUserDetails?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.circularobj = res.data;
                    $scope.newsobj = res.data;
                });
            }

            $scope.timeInMs = 5000;

            var countUp = function () {
                $http.get(ENV.apiUrl + "api/common/Circular/getUserDetails?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.circularobj = res.data;
                    $scope.newsobj = res.data;
                });
                //$timeout(countUp, 5000);
            }
            //$timeout(countUp, 5000);

            $scope.$on('module-chage', function () {
                console.log('New Info');
                console.log($rootScope.module);
                $http.get(ENV.apiUrl + "api/common/GetMenu?user_code=" + user + "&module_code=" + $rootScope.module.module_code).then(function (menuData) {
                    $scope.menuData = menuData.data;
                    $scope.tags = $scope.menuData[0];
                    $scope.applications = $scope.menuData[1];
                });
            })

            function callAtInterval() {
                //console.log("Interval occurred");
                //For Circular Count
                $http.get(ENV.apiUrl + "api/common/Circular/getUserDetails?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.circularobj = res.data;
                    $scope.newsobj = res.data;
                    //console.log($scope.circularobj.userCircularCount);
                });

                //For News Count
                //$http.get(ENV.apiUrl + "api/common/News/getUserDetails?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                //  $scope.newsobj = res.data;
                // console.log($scope.newsobj.userNewsCount);
                // });

            }

            // $interval(callAtInterval, 300000);




            $scope.condenseMenu = function () {
                debugger
                if ($rootScope.isCondensed) {
                    $('body').removeClass('grey condense-menu');
                    $('#main-menu').removeClass('mini');
                    $('.page-content').removeClass('condensed');
                    $rootScope.isCondensed = false;
                } else {
                    $('body').addClass('grey condense-menu');
                    $('#main-menu').addClass('mini');
                    $('.page-content').addClass('condensed');
                    $rootScope.isCondensed = true;
                }
            };

            if (!$rootScope.globals)
                $rootScope.globals = {};
            $rootScope.globals.studentsLoaded = false;


            //For Circular Count
            $http.get(ENV.apiUrl + "api/common/Circular/getUserDetails?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.circularobj = res.data;
                console.log($scope.circularobj.userCircularCount);
            });

            //For News Count
            $http.get(ENV.apiUrl + "api/common/News/getUserDetails?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.newsobj = res.data;
                console.log($scope.newsobj.userNewsCount);
            });

            $scope.RedirectPage = function () {
                console.log("main.cir");
                $state.go("main.cir");
            }

            $scope.RedirectNewsPage = function () {
                console.log("main.news");
                $state.go("main.news");
            }

            $scope.Click = function (str) {
                console.log(str);

                $scope.new_std = str;
            }

            //var handleSidenarAndContentHeight = function () {
            //    var content = $('.page-content');
            //    var sidebar = $('.page-sidebar');

            //    if (!content.attr("data-height")) {
            //        content.attr("data-height", content.height());
            //    }

            //    if (sidebar.height() > content.height()) {
            //        content.css("min-height", sidebar.height() + 120);
            //    } else {
            //        content.css("min-height", content.attr("data-height"));
            //    }
            //};

            //Auto close open menus in Condensed menu

            //if ($('.page-sidebar').hasClass('mini')) {
            //    var elem = jQuery('.page-sidebar ul');
            //    elem.children('li.open').children('a').children('.arrow').removeClass('open');
            //    elem.children('li.open').children('a').children('.arrow').removeClass('active');
            //    elem.children('li.open').children('.sub-menu').slideUp(200);
            //    elem.children('li').removeClass('open');
            //}

            $http.get(ENV.apiUrl + "api/common/getSchoolDetails").then(function (res) {
                $scope.obj1 = res.data;
                $scope.SchoolLogo = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/SchoolLogo/';

                $rootScope.globals.currentSchool = $scope.obj1;
                $rootScope.globals.currentSchool.lic_website_url = $scope.obj1.lic_website_url;
                // $scope.$broadcast('school_change', $rootScope.globals.currentSchool.lic_website_url);

            });

            console.log($scope.uname = $rootScope.globals.currentUser.username);

            $http.get(ENV.apiUrl + "api/common/getAllModules?username=" + $scope.uname).then(function (res2) {
                $scope.display = true;
                $scope.obj2 = res2.data;
                $scope.traslateEn = angular.copy(res2.data);

                console.log($scope.obj2);
                var data = $scope.obj2;
                setTimeout(function () {
                    jQuery('.page-sidebar li > a').on('click', function (e) {
                        if ($(this).next().hasClass('sub-menu') === false) {
                            return;
                        }
                        var parent = $(this).parent().parent();


                        parent.children('li.open').children('a').children('.arrow').removeClass('open');
                        parent.children('li.open').children('a').children('.arrow').removeClass('active');
                        parent.children('li.open').children('.sub-menu').slideUp(200);
                        parent.children('li').removeClass('open');
                        //parent.children('li').removeClass('active');

                        var sub = jQuery(this).next();
                        if (sub.is(":visible")) {
                            jQuery('.arrow', jQuery(this)).removeClass("open");
                            jQuery(this).parent().removeClass("active");
                            sub.slideUp(200, function () {
                                //handleSidenarAndContentHeight();
                            });
                        } else {
                            jQuery('.arrow', jQuery(this)).addClass("open");
                            jQuery(this).parent().addClass("open");
                            sub.slideDown(200, function () {
                                // handleSidenarAndContentHeight();
                            });
                        }

                        e.preventDefault();
                    });
                }, 800);

                // $scope.routepage($scope.obj2);
            });

            //$scope.routepage = (function (data) {
            //    angular.forEach(data, function (value, key) {
            //        if (value.comn_appl_code) {
            //            console.log(value);
            //            $stateProviderRef.state('main.' + value.comn_appl_code, {
            //                url: value.comn_url,
            //                templateUrl: value.comn_appl_location,
            //                controller: value.comn_controller
            //            });
            //        }
            //        if ((value.comn_type && typeof value.comn_type !== "string")) {
            //            $scope.routepage(value.comn_type);
            //        } else if (value.comn_appl) {
            //            $scope.routepage(value.comn_appl);
            //        }
            //    });
            //});

            $scope.arr = function () {

                $('.horizontal-menu .bar-inner > ul > li').on('click', function () {
                    //debugger;
                    $(this).toggleClass('open').siblings().removeClass('open');

                });
                $('.content').on('click', function () {

                    $('.horizontal-menu .bar-inner > ul > li').removeClass('open');
                });
            };

            $scope.load_page = function (path, obj, mod_code) {
                if (obj.comn_appl_type == 'R') {

                    $scope.location = obj.comn_appl_location;
                    $rootScope.location = $scope.location;
                    setTimeout(function () { $state.go('main.ReportCard'); }, 100);

                    $state.go('main');
                }

                else {

                    if ($scope.fin == undefined) {
                        if (mod_code == '005') {
                            $http.get(ENV.apiUrl + "api/PDCSubmission/GetComp_Name?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                                $scope.cmbCompany = res.data;
                                if (res.data.length == 1) {
                                    $scope.companyName = res.data[0].comp_name;

                                    $scope.finnComp = res.data[0].comp_code;

                                    $http.get(ENV.apiUrl + "api/PDCSubmission/GetFinancial_year?comp_code=" + $scope.finnComp).then(function (res) {

                                        if (res.data.length > 0) {

                                            console.log('Finn');

                                            var data = {
                                                year: res.data[0].financial_year,
                                                company: $scope.finnComp,
                                                companyame: $scope.companyName
                                            }
                                            window.localStorage["Finn_comp"] = JSON.stringify(data)


                                            // $.cookie("finnComapny", $scope.finnComp);
                                            // window.localStorage("finnComapny", $scope.finnComp);

                                            //$.cookie("finnYear", res.data[0].financial_year);

                                            //if ($.cookie("finnComapny") != null) {

                                            //    console.log($.cookie("finnComapny"));
                                            //}

                                        }
                                    });

                                    //  AuthenticationService.SetCompany(res.data[0].comp_code, '2017');



                                }
                                else if (res.data.length > 1) {
                                    $scope.financeWindow();
                                }


                            });
                        }
                    }

                    $scope.lms_redirct(path);

                    console.log('In Load');
                    console.log("main." + path);

                    var state = 'main.' + path;

                    $state.go(state);
                    console.log('After State.Go In Load');
                }




                if ($scope.oldobj == undefined) {


                    $scope.oldobj = {
                        opr: 'I',
                        comn_audit_user_code: $rootScope.globals.currentUser.username,
                        comn_appl_name_en: path,
                        comn_audit_start_time: $scope.getDateTime(),
                        //comn_audit_end_time: '',
                    }
                    $http.post(ENV.apiUrl + "api/common/InsertUserAudit", $scope.oldobj).then(function (res) {

                    });
                }
                else {

                    var edate = $scope.getDateTime()
                    var data = {
                        opr: 'I',
                        comn_audit_user_code: $rootScope.globals.currentUser.username,
                        comn_appl_name_en: path,
                        comn_audit_start_time: edate,
                        //comn_audit_end_time: '',
                    }
                    //$scope.oldobj['comn_audit_start_time'] = $scope.getDateTime()

                    $http.post(ENV.apiUrl + "api/common/InsertUserAudit", data).then(function (res) {


                        $scope.oldobj['comn_audit_end_time'] = edate
                        $scope.oldobj['opr'] = 'J'

                        $http.post(ENV.apiUrl + "api/common/InsertUserAudit", $scope.oldobj).then(function (res) {

                            $scope.oldobj['comn_audit_start_time'] = edate
                            $scope.oldobj['comn_appl_name_en'] = path

                        });

                    });

                }

                window.localStorage.removeItem("ReportProvNumber");

                $("#Applsource").select2("val", "");

            }


            $scope.parentDetailsClick = function (enrollno, parentid) {
                debugger;

                $http.get(ENV.apiUrl + "api/StudetProfile/getParentDetails?enroll_no=" + enrollno).then(function (ParentDetails) {
                    debugger;
                    $scope.Parent_Details = ParentDetails.data;
                    $scope.parent_info = $scope.Parent_Details[0];

                    $scope.student_father_image = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.parent_info.sims_admisison_father_image;
                    $scope.student_mother_image = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.parent_info.sims_admisison_mother_image;
                    $scope.student_guardian_image1 = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.parent_info.sims_admisison_guardian_image;
                    $scope.p = {
                        parentImage: $scope.student_father_image,
                    }
                    $scope.getstatefather($scope.parent_info.sims_admission_father_country_code);
                    $scope.getcityname($scope.parent_info.sims_admission_father_state);

                    $scope.getmothercityname($scope.parent_info.sims_admission_mother_state);
                    $scope.getstatemother($scope.parent_info.sims_admission_mother_country_code);

                    $scope.getguardiancityname($scope.parent_info.sims_admission_guardian_state);
                    $scope.getstateguardian($scope.parent_info.sims_admission_guardian_country_code);
                    $scope.isparentschoolemployee($scope.parent_info.sims_parent_is_employment_status);




                });

                $http.get(ENV.apiUrl + "api/StudetProfile/getStudentList?parent_no=" + parentid).then(function (Childresult) {
                    debugger;
                    $scope.ChildDetails = Childresult.data;
                    if ($scope.ChildDetails.length > 0) {
                        $scope.ChildDetailsshow = true;
                        $scope.ChildDetailshide = false;
                    }
                    else {
                        $scope.ChildDetailsshow = false;
                        $scope.ChildDetailshide = true;
                    }
                });
                $scope.CurrentParentImage = function (parentimg) {
                    $scope.p.parentImage = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + parentimg;

                }
                $('#ParentProfileModal').modal('show');

            }

            $scope.language = function (str) {
                console.log(str);
                var lang;
                if (str == 'en') {

                    console.log("English");
                    lang = "en";
                    $rootScope.langDirection = lang == "ar" ? "rtl" : "ltr";
                    gettextCatalog.setCurrentLanguage("en");
                    $rootScope.$broadcast('lagnuage_chaged', "en");
                    $http.get(ENV.apiUrl + "api/common/Get_Application?s=" + $rootScope.globals.currentUser.username).then(function (res) {
                        $scope.appl_items = res.data;
                        // console.log($scope.appl_items);
                    });


                    $scope.display = true;
                    $scope.obj2 = $scope.traslateEn
                    console.log($scope.obj2);
                    var data = $scope.obj2;
                    setTimeout(function () {
                        jQuery('.page-sidebar li > a').on('click', function (e) {
                            if ($(this).next().hasClass('sub-menu') === false) {
                                return;
                            }
                            var parent = $(this).parent().parent();


                            parent.children('li.open').children('a').children('.arrow').removeClass('open');
                            parent.children('li.open').children('a').children('.arrow').removeClass('active');
                            parent.children('li.open').children('.sub-menu').slideUp(200);
                            parent.children('li').removeClass('open');
                            //parent.children('li').removeClass('active');

                            var sub = jQuery(this).next();
                            if (sub.is(":visible")) {
                                jQuery('.arrow', jQuery(this)).removeClass("open");
                                jQuery(this).parent().removeClass("active");
                                sub.slideUp(200, function () {
                                    //handleSidenarAndContentHeight();
                                });
                            } else {
                                jQuery('.arrow', jQuery(this)).addClass("open");
                                jQuery(this).parent().addClass("open");
                                sub.slideDown(200, function () {
                                    // handleSidenarAndContentHeight();
                                });
                            }

                            e.preventDefault();
                        });
                    }, 800);


                }

                else if (str == 'ar') {
                    lang = "ar";
                    $rootScope.locale = $rootScope.locales["ar"];
                    $rootScope.langDirection = lang == "ar" ? "rtl" : "ltr";
                    gettextCatalog.setCurrentLanguage("ar");
                    $rootScope.$broadcast('lagnuage_chaged', "ar");

                    console.log("Arabic");
                    $scope.arabic_chat = { "display": "none" }


                    $http.get(ENV.apiUrl + "api/common/Get_ApplicationArabic?s=" + $rootScope.globals.currentUser.username).then(function (res) {
                        $scope.appl_items = res.data;
                        // console.log($scope.appl_items);
                    });


                    if ($scope.traslateAr == undefined) {
                        $http.get(ENV.apiUrl + "api/common/getAllModulesArabic?username=" + $scope.uname).then(function (res2) {
                            $scope.display = true;
                            $scope.obj2 = res2.data;
                            $scope.traslateAr = angular.copy(res2.data)
                            console.log($scope.obj2);
                            var data = $scope.obj2;
                            setTimeout(function () {
                                jQuery('.page-sidebar li > a').on('click', function (e) {
                                    if ($(this).next().hasClass('sub-menu') === false) {
                                        return;
                                    }
                                    var parent = $(this).parent().parent();


                                    parent.children('li.open').children('a').children('.arrow').removeClass('open');
                                    parent.children('li.open').children('a').children('.arrow').removeClass('active');
                                    parent.children('li.open').children('.sub-menu').slideUp(200);
                                    parent.children('li').removeClass('open');
                                    //parent.children('li').removeClass('active');

                                    var sub = jQuery(this).next();
                                    if (sub.is(":visible")) {
                                        jQuery('.arrow', jQuery(this)).removeClass("open");
                                        jQuery(this).parent().removeClass("active");
                                        sub.slideUp(200, function () {
                                            //handleSidenarAndContentHeight();
                                        });
                                    } else {
                                        jQuery('.arrow', jQuery(this)).addClass("open");
                                        jQuery(this).parent().addClass("open");
                                        sub.slideDown(200, function () {
                                            // handleSidenarAndContentHeight();
                                        });
                                    }

                                    e.preventDefault();
                                });
                            }, 800);

                            // $scope.routepage($scope.obj2);
                        });

                    }
                    else {

                        $scope.display = true;
                        $scope.obj2 = $scope.traslateAr;

                        console.log($scope.obj2);
                        var data = $scope.obj2;
                        setTimeout(function () {
                            jQuery('.page-sidebar li > a').on('click', function (e) {
                                if ($(this).next().hasClass('sub-menu') === false) {
                                    return;
                                }
                                var parent = $(this).parent().parent();


                                parent.children('li.open').children('a').children('.arrow').removeClass('open');
                                parent.children('li.open').children('a').children('.arrow').removeClass('active');
                                parent.children('li.open').children('.sub-menu').slideUp(200);
                                parent.children('li').removeClass('open');
                                //parent.children('li').removeClass('active');

                                var sub = jQuery(this).next();
                                if (sub.is(":visible")) {
                                    jQuery('.arrow', jQuery(this)).removeClass("open");
                                    jQuery(this).parent().removeClass("active");
                                    sub.slideUp(200, function () {
                                        //handleSidenarAndContentHeight();
                                    });
                                } else {
                                    jQuery('.arrow', jQuery(this)).addClass("open");
                                    jQuery(this).parent().addClass("open");
                                    sub.slideDown(200, function () {
                                        // handleSidenarAndContentHeight();
                                    });
                                }

                                e.preventDefault();
                            });
                        }, 800);

                        // $scope.routepage($scope.obj2);


                    }





                }
                //$scope.$apply(); //this triggers a $digest
            }

            //$scope.language = function (str) {
            //    console.log(str);
            //    var lang;
            //    if (str == 'en') {

            //        console.log("English");
            //        lang = "en";
            //        $rootScope.langDirection = lang == "ar" ? "rtl" : "ltr";
            //    }

            //    else if (str == 'ar') {
            //        lang = "ar";
            //        $rootScope.locale = $rootScope.locales["ar"];
            //        $rootScope.langDirection = lang == "ar" ? "rtl" : "ltr";

            //        console.log("Arabic");
            //    }
            //    //$scope.$apply(); //this triggers a $digest
            //}


            // for global Search

            //pager

            $scope.itemsPerPage = 10;
            $scope.currentPage = 0;
            $scope.global_search_result = [];
            $scope.perPageRecords = [];


            $scope.startPage = function () {
                $scope.currentPage = 0;
            }

            $scope.lastPage = function () {
                $scope.currentPage = $scope.pageCount();
            }

            $scope.range = function () {
                var rangeSize = 15;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;


            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
                $scope.selectedAll = false;
                angular.forEach($scope.perPageRecords, function (info, key) {
                    info.user_chk = false;
                });
                $scope.singlePage = $scope.currentPage + 1;
                var begin = (($scope.singlePage - 1) * $scope.itemsPerPage);
                var end = parseInt(begin) + parseInt($scope.itemsPerPage);
                $scope.perPageRecords = $scope.global_search_result.slice(begin, end);
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.global_search_result.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;

                // to select single page records
                $scope.selectedAll = false;
                angular.forEach($scope.perPageRecords, function (info, key) {
                    info.user_chk = false;
                });
                $scope.singlePage = n + 1;
                var begin = (($scope.singlePage - 1) * $scope.itemsPerPage);
                var end = parseInt(begin) + parseInt($scope.itemsPerPage);
                $scope.perPageRecords = $scope.global_search_result.slice(begin, end);

            };

            $scope.getPerPageRecords = function () {
                $scope.singlePage = 1;
                var begin = (($scope.singlePage - 1) * $scope.itemsPerPage);
                var end = parseInt(begin) + parseInt($scope.itemsPerPage);
                $scope.perPageRecords = $scope.global_search_result.slice(begin, end);
            }


            $http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (res) {
                $scope.cmbCur = res.data;
                if ($scope.cmbCur.length > 0) {
                    $scope.global_Search['global_curriculum_code'] = $scope.cmbCur[0].sims_cur_code;
                    $scope.getAcadm_yr($scope.cmbCur[0].sims_cur_code)

                }
            })

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;

                console.log($scope.ComboBoxValues);
            });

            $http.get(ENV.apiUrl + "api/EmployeeReport/GetDepartmentName").then(function (res) {
                $scope.cmbDepartment = res.data;
            })


            $http.get(ENV.apiUrl + "api/EmployeeReport/GetCompanyNameForShift").then(function (res) {
                //   $scope.cmbCompany = res.data[0];

                if (res.data.length > 0) {
                    $http.get(ENV.apiUrl + "api/EmployeeReport/GetDesignation?com_code=" + res.data[0].sh_company_code).then(function (res) {
                        $scope.cmbDesignation = res.data;
                    })
                }
            })


            $http.get(ENV.apiUrl + "api/common/getVehicleCode").then(function (get_VehicleCode) {
                $scope.VehicleCode = get_VehicleCode.data;
            })


            $scope.academic_change = function (acd) {

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.global_Search.global_curriculum_code + "&academic_year=" + acd).then(function (res) {
                    $scope.All_grade_names = res.data;
                })
            }

            $scope.getallsectioncode = function (gr_code) {

                $http.get(ENV.apiUrl + "api/common/SectionSubject/getSectionNames?curcode=" + $scope.global_Search.global_curriculum_code + "&academicyear=" + $scope.global_Search.global_acdm_yr + "&gradecode=" + gr_code).then(function (res) {
                    $scope.All_Section_names = res.data;
                    $('#example-enableFiltering-includeSelectAllOption').multiselect({
                        includeSelectAllOption: true,
                        enableFiltering: true
                    });
                });
            }

            $scope.getAcadm_yr = function (cur_code) {
                $scope.curriculum_code = cur_code;
                $http.get(ENV.apiUrl + "api/attendance/getAttendanceyear?cur_code=" + cur_code).then(function (res) {
                    $scope.Acdm_year = res.data; console.log($scope.Acdm_year);

                    if ($scope.cmbCur.length > 0) {
                        if ($http.defaults.headers.common['schoolId'] == 'adis') {
                            $scope.global_Search['global_acdm_yr'] = res.data[0].sims_academic_year;
                            $scope.academic_change(res.data[0].sims_academic_year);
                        }
                        else {
                            if (res.data[1].sims_academic_year == undefined || res.data[1].sims_academic_year == null || res.data[1].sims_academic_year == "") {
                                $scope.global_Search['global_acdm_yr'] = res.data[0].sims_academic_year;
                                $scope.academic_change(res.data[0].sims_academic_year);
                            }
                            else {
                                $scope.global_Search['global_acdm_yr'] = res.data[1].sims_academic_year
                                $scope.academic_change(res.data[1].sims_academic_year);
                            }
                        }
                    }
                });
            }

            $scope.modal_cancel = function () {
                //  $scope.All_grade_names = '';
                //$scope.All_Nationality_names = '';
                //  $scope.Cur_Names = '';
                $scope.glbl_obj = {};
                $scope.global_Search = {};
                $scope.global_search_result = [];
                $scope.gbl_tab = '';
                $scope.global_Search_show = false;

                debugger
                $scope.$broadcast('global_cancel', $scope.SelectedUserLst);
            }

            $scope.Global_Search_modal = function () {

                $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getAllCurName").then(function (res) {
                    $scope.Cur_Names = res.data;
                });


                $('#Global_Search_Modal').modal({ backdrop: "static" });

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getAllGradeName").then(function (res) {
                    $scope.All_grade_names = res.data;
                    console.log(res.data)
                });
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getAllNationality").then(function (res) {
                    $scope.All_Nationality_names = res.data;
                    console.log(res.data)

                });

            }


            $scope.Global_Search_by_Uassignedstudent = function () {
                //debugger

                $scope.global_Uassigned_student_table = false;
                $scope.busy = true;
                if ($scope.global_Search == "") {
                    $scope.glbl_obj = $scope.global_Search;
                }
                else {
                    $scope.glbl_obj = {
                        s_cur_code: $scope.global_Search.global_curriculum_code,
                        search_std_enroll_no: $scope.global_Search.glbl_enroll_num,
                        search_std_name: $scope.global_Search.glbl_name,
                        search_std_grade_name: $scope.global_Search.gradecode,
                        search_std_section_name: $scope.global_Search.sectioncode,
                        search_std_passport_no: $scope.global_Search.glbl_pass_num,
                        search_std_family_name: $scope.global_Search.glbl_family_name,
                        sims_academic_year: $scope.global_Search.global_acdm_yr,
                        sims_nationality_name_en: $scope.global_Search.nationalitycode,
                        std_national_id: $scope.global_Search.nationalityID
                    }
                }
                //myarray = []; $scope.global_search_result = "";

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudentUnassigned?data=" + JSON.stringify($scope.glbl_obj)).then(function (res) {
                    $scope.global_search_result = res.data;
                    console.log($scope.global_search_result);
                    $scope.global_Uassigned_student_table = true;
                    $scope.busy = false;
                    if ($scope.global_search_result.length == 0) {
                        swal({ title: 'Alert', text: "No record(s) found.", showCloseButton: true, width: 450, height: 200 });
                    }
                });


            }

            $scope.vieprofile = function (emp) {
                debugger
                $('#EmployeeProfileModal').modal('show');
                $http.get(ENV.apiUrl + "api/common/getEmployeeProfileView?em_login_code=" + emp.em_login_code).then(function (getEmployeeProfileView_Data) {
                    $scope.EmployeeProfileView = getEmployeeProfileView_Data.data;
                    console.log($scope.EmployeeProfileView);
                    for (var i = 0; i < $scope.EmployeeProfileView.length; i++) {
                        debugger;
                        $scope.emp_image_profile = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/' + $scope.EmployeeProfileView[i].em_img;
                        //$scope.imageUrl = ENV.apiUrl + 'Content/sjs/Images/EmployeeImages/' + $scope.EmployeeProfileView[i].em_img;
                        // $scope.imageUrl = ENV.siteUrl + 'Images/EmployeeImages/' + $scope.EmployeeProfileView[i].em_img;
                        $scope.em_full_name = $scope.EmployeeProfileView[i].em_full_name;
                        $scope.em_name_ot = $scope.EmployeeProfileView[i].em_name_ot;
                        $scope.em_family_name = $scope.EmployeeProfileView[i].em_family_name;
                        $scope.em_sex = $scope.EmployeeProfileView[i].em_sex;
                        $scope.em_marital_status = $scope.EmployeeProfileView[i].em_marital_status;
                        $scope.em_nation_name = $scope.EmployeeProfileView[i].em_nation_name;
                        $scope.em_last_login = $scope.EmployeeProfileView[i].em_last_login;
                        $scope.em_date_of_birth = $scope.EmployeeProfileView[i].em_date_of_birth;
                        $scope.em_handicap_status = $scope.EmployeeProfileView[i].em_handicap_status;
                        $scope.em_religion_name = $scope.EmployeeProfileView[i].em_religion_name;
                        $scope.em_salutation = $scope.EmployeeProfileView[i].em_salutation;
                        $scope.em_Salutation_Code = $scope.EmployeeProfileView[i].em_Salutation_Code;


                        //Passport & Contact_Details
                        $scope.em_passport_no = $scope.EmployeeProfileView[i].em_passport_no;
                        $scope.em_passport_issue_date = $scope.EmployeeProfileView[i].em_passport_issue_date;
                        $scope.em_passport_exp_date = $scope.EmployeeProfileView[i].em_passport_exp_date;
                        $scope.em_passport_issuing_authority = $scope.EmployeeProfileView[i].em_passport_issuing_authority;

                        $scope.em_emergency_contact_name1 = $scope.EmployeeProfileView[i].em_emergency_contact_name1;
                        $scope.em_emergency_contact_number1 = $scope.EmployeeProfileView[i].em_emergency_contact_number1;
                        $scope.em_emergency_contact_name2 = $scope.EmployeeProfileView[i].em_emergency_contact_name2;
                        $scope.em_emergency_contact_number2 = $scope.EmployeeProfileView[i].em_emergency_contact_number2;

                        //Visa & NationalID_Details
                        $scope.em_visa_no = $scope.EmployeeProfileView[i].em_visa_no;
                        $scope.em_visa_type = $scope.EmployeeProfileView[i].em_visa_type;
                        $scope.em_visa_issuing_authority = $scope.EmployeeProfileView[i].em_visa_issuing_authority;
                        $scope.em_visa_issuing_place = $scope.EmployeeProfileView[i].em_visa_issuing_place;
                        $scope.em_visa_issue_date = $scope.EmployeeProfileView[i].em_visa_issue_date;
                        $scope.em_visa_exp_date = $scope.EmployeeProfileView[i].em_visa_exp_date;

                        $scope.em_national_id = $scope.EmployeeProfileView[i].em_national_id;
                        $scope.em_national_id_issue_date = $scope.EmployeeProfileView[i].em_national_id_issue_date;
                        $scope.em_national_id_expiry_date = $scope.EmployeeProfileView[i].em_national_id_expiry_date;

                        //Company_Details
                        $scope.em_company_name = $scope.EmployeeProfileView[i].em_company_name;
                        $scope.em_dept_name = $scope.EmployeeProfileView[i].em_dept_name;
                        $scope.em_desg_name = $scope.EmployeeProfileView[i].em_desg_name;
                        $scope.em_joining_ref = $scope.EmployeeProfileView[i].em_joining_ref;
                        $scope.em_grade_name = $scope.EmployeeProfileView[i].em_grade_name;
                        $scope.em_grade_effect_from = $scope.EmployeeProfileView[i].em_grade_effect_from;
                        $scope.em_date_of_join = $scope.EmployeeProfileView[i].em_date_of_join;
                        $scope.em_agreement_start_date = $scope.EmployeeProfileView[i].em_agreement_start_date;
                        $scope.em_agreement_exp_date = $scope.EmployeeProfileView[i].em_agreement_exp_date;
                        $scope.em_punching_id = $scope.EmployeeProfileView[i].em_punching_id;
                        $scope.em_staff_type = $scope.EmployeeProfileView[i].em_staff_type;
                        $scope.em_service_status = $scope.EmployeeProfileView[i].em_service_status;

                        //Bank_Details
                        $scope.em_bank_name = $scope.EmployeeProfileView[i].em_bank_name;
                        $scope.em_gpf_ac_no = $scope.EmployeeProfileView[i].em_gpf_ac_no;
                        $scope.em_gosi_ac_no = $scope.EmployeeProfileView[i].em_gosi_ac_no;
                        $scope.em_labour_card_no = $scope.EmployeeProfileView[i].em_labour_card_no;
                        $scope.em_bank_ac_no = $scope.EmployeeProfileView[i].em_bank_ac_no;
                        $scope.em_bank_swift_code = $scope.EmployeeProfileView[i].em_bank_swift_code;
                        $scope.em_gosi_start_date = $scope.EmployeeProfileView[i].em_gosi_start_date;
                        $scope.em_iban_no = $scope.EmployeeProfileView[i].em_iban_no;


                        $scope.em_summary_address = $scope.EmployeeProfileView[i].em_summary_address;
                        $scope.em_country = $scope.EmployeeProfileView[i].em_country;
                        $scope.em_state = $scope.EmployeeProfileView[i].em_state;
                        $scope.em_city = $scope.EmployeeProfileView[i].em_city;
                        $scope.em_po_box = $scope.EmployeeProfileView[i].em_po_box;
                        $scope.em_phone = $scope.EmployeeProfileView[i].em_phone;
                        $scope.em_mobile = $scope.EmployeeProfileView[i].em_mobile;
                        $scope.em_iban_no = $scope.EmployeeProfileView[i].em_iban_no;
                        $scope.em_email = $scope.EmployeeProfileView[i].em_email;

                    }
                });

                $http.get(ENV.apiUrl + "api/common/getEmployeeAttendanceDetails?em_login_code=" + emp.em_login_code).then(function (getEmployeeAttendanceDetails_Data) {
                    $scope.EmployeeAttendanceDetails = getEmployeeAttendanceDetails_Data.data;
                    console.log($scope.EmployeeAttendanceDetails);

                    if ($scope.EmployeeAttendanceDetails < 1) {
                        $scope.NodataAttendance = true;

                    }
                    else {

                        $scope.AttendanceData = true;
                    }

                });


                $http.get(ENV.apiUrl + "api/common/getEmployeeLeavesDetails?el_number=" + emp.em_login_code).then(function (getEmployeeLeavesDetails_Data) {
                    $scope.EmployeeLeavesDetails = getEmployeeLeavesDetails_Data.data;
                    console.log($scope.EmployeeLeavesDetails);

                    if ($scope.EmployeeLeavesDetails < 1) {
                        $scope.NodataLeave = true;

                    }
                    else {

                        $scope.LeaveData = true;
                    }
                });

                $http.get(ENV.apiUrl + "api/common/getEmployeeClassDetails?em_login_code=" + emp.em_login_code).then(function (getEmployeeClassDetails_Data) {
                    $scope.EmployeeClassDetails = getEmployeeClassDetails_Data.data;
                    console.log($scope.EmployeeClassDetails);

                    if ($scope.EmployeeClassDetails < 1) {
                        $scope.NodataClass = true;

                    }
                    else {

                        $scope.ClassData = true;
                    }
                });
            }


            $(function () {
                $('#cmb_gradeparent').multipleSelect({
                    width: '100%',
                    //placeholder: "Select Grade"
                });

                $('#cmb_sectionparent').multipleSelect({
                    width: '100%',
                    //placeholder: "Select Section"
                });


            });

            $http.get(ENV.apiUrl + "api/common/GlobalSearch/getAllGradeName").then(function (res) {
                $scope.All_grade_nameParent = res.data;

                setTimeout(function () {
                    $('#cmb_gradeparent').change(function () {
                        console.log($(this).val());
                        //section = $(this).val();
                    }).multipleSelect({
                        multiple: true,
                        width: '100%'
                    });
                    //$("#cmb_section").multipleSelect("checkAll");
                }, 1000);



            });


            $scope.getallsectioncodeparent = function (str) {

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/Get_Section_CodebyAll?grade_code=" + str).then(function (res) {
                    $scope.All_Section_namesParent = res.data;

                    setTimeout(function () {
                        $('#cmb_sectionparent').change(function () {
                            console.log($(this).val());
                            //section = $(this).val();
                        }).multipleSelect({
                            multiple: true,
                            width: '100%'
                        });
                        //$("#cmb_section").multipleSelect("checkAll");
                    }, 1000);

                });
            }



            $scope.Global_Search_by_parent_unassigned = function () {

                $scope.global_unassigned_parent_table = false;
                $scope.busy = true;

                $scope.searchbyparent = {
                    search_std_grade_name: $scope.global_Search.gradecode2,
                    search_std_section_name: $scope.global_Search.sectioncode2,
                    search_parent_id: $scope.global_Search.search_parent_id,
                    search_parent_name: $scope.global_Search.search_parent_name,
                    search_parent_email_id: $scope.global_Search.search_parent_email_id,
                    search_parent_mobile_no: $scope.global_Search.search_parent_mobile_no
                }

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchParentUassigned?data=" + JSON.stringify($scope.searchbyparent)).then(function (res) {
                    $scope.global_search_result = res.data;
                    $scope.global_unassigned_parent_table = true;
                    $scope.busy = false;
                    if ($scope.global_search_result.length == 0) {
                        swal({ title: 'Alert', text: "No record(s) found.", showCloseButton: true, width: 450, height: 200 });
                    }
                    console.log($scope.global_search_result);
                });
            }


            $scope.Global_Search_by_student = function () {
                //debugger

                $scope.global_student_table = false;
                $scope.busy = true;
                if ($scope.global_Search == "") {
                    $scope.glbl_obj = $scope.global_Search;
                }
                else {
                    $scope.glbl_obj = {
                        s_cur_code: $scope.global_Search.global_curriculum_code,
                        search_std_enroll_no: $scope.global_Search.glbl_enroll_num,
                        search_std_name: $scope.global_Search.glbl_name,
                        search_std_middle_name: $scope.global_Search.glbl_middle_name,
                        search_std_last_name: $scope.global_Search.glbl_last_name,
                        search_std_grade_name: $scope.global_Search.gradecode,
                        search_std_section_name: $scope.global_Search.sectioncode,
                        search_std_passport_no: $scope.global_Search.glbl_pass_num,
                        search_std_family_name: $scope.global_Search.glbl_family_name,
                        sims_academic_year: $scope.global_Search.global_acdm_yr,
                        sims_nationality_name_en: $scope.global_Search.nationalitycode,
                        std_national_id: $scope.global_Search.nationalityID,
                        search_parent_emp_id: $scope.global_Search.search_parent_emp_id,
                        sims_student_ea_number: $scope.global_Search.sims_student_ea_number,
                        search_std_transport_bus: $scope.global_Search.search_std_transport_bus


                    }
                }
                //myarray = []; $scope.global_search_result = "";

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.glbl_obj)).then(function (res) {
                    $scope.global_search_result = res.data; console.log($scope.global_search_result);
                    if ($scope.global_search_result.length > 0) {
                        $scope.currentPage = 0;
                        $scope.global_student_table = true;
                    }
                    else if ($scope.global_search_result.length == 0) {
                        swal({ title: 'Alert', text: "No record(s) found.", showCloseButton: true, width: 450, height: 200 });

                    }
                    $scope.busy = false;
                    $scope.getPerPageRecords();
                });


            }

            $scope.Global_Search_by_parent = function () {

                $scope.global_parent_table = false;
                $scope.busy = true;

                $scope.searchbyparent = {
                    search_std_grade_name: $scope.global_Search.gradecode2,
                    search_std_section_name: $scope.global_Search.sectioncode2,
                    search_parent_id: $scope.global_Search.search_parent_id,
                    search_parent_name: $scope.global_Search.search_parent_name,
                    search_parent_email_id: $scope.global_Search.search_parent_email_id,
                    search_parent_mobile_no: $scope.global_Search.search_parent_mobile_no,
                    search_parent_mother_name: $scope.global_Search.search_parent_mother_name,
                    search_parent_mother_email_id: $scope.global_Search.search_parent_mother_email_id,
                    search_parent_mother_mobile_no: $scope.global_Search.search_parent_mother_mobile_no
                }

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchParent?data=" + JSON.stringify($scope.searchbyparent)).then(function (res) {
                    if (res.data.length > 0) {
                        $scope.currentPage = 0;
                        $scope.global_parent_table = true;
                        $scope.global_search_result = res.data;
                    }
                    else {
                        $scope.global_search_result = [];

                        swal({ title: 'Alert', text: "No record(s) found.", showCloseButton: true, width: 450, height: 200 });
                    }

                    $scope.busy = false;
                    console.log($scope.global_search_result);
                });
            }

            $scope.Global_Search_SearchParentSectionWise = function () {
                $scope.global_parent_table = false;
                $scope.busy = true;

                $scope.searchbyparent = {
                    search_std_grade_name: $scope.global_Search.gradecode2,
                    search_std_section_name: $scope.global_Search.sectioncode2,
                }

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchParentSectionWise?grade=" + $scope.global_Search.gradecode2 + "&section=" + $scope.global_Search.sectioncode2).then(function (res) {
                    $scope.global_search_result = res.data;
                    $scope.global_parent_table = true;
                    $scope.busy = false;
                    if ($scope.global_search_result.length == 0) {
                        swal({ title: 'Alert', text: "No record(s) found.", showCloseButton: true, width: 450, height: 200 });
                    }
                });
            }

            $scope.Global_Search_by_Teacher = function () {
                $scope.global_teacher_table = false;
                $scope.busy = true;

                $scope.searchbyparent = {
                    search_teacher_id: $scope.global_Search.search_teacher_id,
                    search_teacher_name: $scope.global_Search.search_teacher_name
                }

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchTeacher?data=" + JSON.stringify($scope.searchbyparent)).then(function (res) {
                    $scope.currentPage = 0;
                    if (res.data.length > 0)
                        $scope.global_search_result = res.data;

                    else {
                        $scope.global_search_result = [];

                        swal({ title: 'Alert', text: "No record(s) found.", showCloseButton: true, width: 450, height: 200 });
                    }
                    $scope.global_teacher_table = true;
                    $scope.busy = false;
                    console.log($scope.global_search_result);
                    $scope.getPerPageRecords();
                });
            }

            $scope.Global_Search_by_User = function () {
                $scope.global_search_result = [];
                $scope.currentPage = 0;

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchUser?data=" + JSON.stringify($scope.temp1)).then(function (users) {


                    if (users.data.length > 0) {
                        $scope.global_search_result = users.data;
                    }
                    else {
                        $scope.global_search_result = [];

                        swal({ title: 'Alert', text: "No record(s) found.", showCloseButton: true, width: 450, height: 200 });
                    }

                    console.log(users.data);
                    $scope.table = true;
                    $scope.busy = false;
                    $scope.getPerPageRecords();
                })
            }

            $scope.Global_Search_by_employee = function () {
                $scope.NodataEmployee = false;
                $http.post(ENV.apiUrl + "api/common/GlobalSearch/SearchEmployee?data=" + JSON.stringify($scope.temp)).then(function (SearchEmployee_Data) {
                    $scope.global_search_result = SearchEmployee_Data.data;
                    console.log($scope.global_search_result);
                    $scope.EmployeeTable = true;

                    if (SearchEmployee_Data.data.length > 0) {
                        //   $scope.EmployeeTable = true;
                    }
                    else {
                        $scope.global_search_result = [];
                        swal({ title: 'Alert', text: "No record(s) found.", showCloseButton: true, width: 450, height: 200 });
                        $scope.NodataEmployee = true;
                        //  $scope.EmployeeTable = false;
                    }

                    $scope.getPerPageRecords();
                });

            }

            $scope.addUser = function (obj, flg, tab) {
                //$scope.chkMulti
                debugger
                obj['tab'] = tab;
                if (flg) {
                    if ($scope.chkMulti) {
                        $scope.SelectedUserLst.push(obj);
                    }
                    else {
                        if ($scope.old_obj != undefined) {
                            $scope.SelectedUserLst.pop($scope.old_obj);
                            // $scope.SelectedUserLst.pop($scope.old_obj);
                            $scope.old_obj.user_chk = false;
                            $scope.SelectedUserLst.push(obj);
                        }
                        else {
                            $scope.SelectedUserLst.push(obj);
                        }

                    }
                    $scope.old_obj = obj;
                    if ($scope.SelectedUserLst.length == $scope.perPageRecords.length) {
                        $scope.selectedAll = true;
                    }
                }
                else {
                    $scope.SelectedUserLst.pop(obj);
                    $scope.selectedAll = false;
                }

            }

            $scope.isSelectAll = function () {
                $scope.SelectedUserLst = [];
                if ($scope.selectedAll) {
                    $scope.selectedAll = true;
                    for (var i = 0; i < $scope.perPageRecords.length; i++) {
                        $scope.SelectedUserLst.push($scope.perPageRecords[i]);
                    }
                    // $scope.row1 = 'row_selected';
                }
                else {
                    $scope.selectedAll = false;
                    $scope.SelectedUserLst = [];
                }

                angular.forEach($scope.perPageRecords, function (item) {
                    item.user_chk = $scope.selectedAll;
                });

            }



            $scope.markall = function (flg, tab) {
                debugger
                for (var i = 0; i < $scope.global_search_result.length; i++) {
                    if (flg) {
                        $scope.global_search_result[i].user_chk = true;
                        $scope.global_search_result[i]['tab'] = tab;
                        $scope.SelectedUserLst.push($scope.global_search_result[i])
                    }
                    else {
                        $scope.global_search_result[i].user_chk = false;
                        $scope.SelectedUserLst = [];
                    }
                }
            }

            //$scope.$on('global_Search_click', function (str) {

            //   
            //    if ($scope.search_visibility != undefined) {
            //        $scope.visible_stud = $scope.search_visibility.visible_stud;
            //        $scope.visible_parent = $scope.search_visibility.visible_parent;
            //        $scope.visible_search_parent = $scope.search_visibility.visible_search_parent;
            //        $scope.visible_teacher = $scope.search_visibility.visible_teacher;
            //        $scope.visible_User = $scope.search_visibility.visible_User;
            //        $scope.visible_Employee = $scope.search_visibility.visible_Employee;

            //    }
            //})


            $scope.global_Search_click = function () {
                debugger
                $scope.temp1 = {};
                $scope.SelectedUserLst = [];
                if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                    if ($rootScope.globals.currentUser.username == 'admin') {
                        $scope.visible_stud = $rootScope.visible_stud;
                        $scope.visible_parent = $rootScope.visible_parent;
                        $scope.visible_search_parent = $rootScope.visible_search_parent
                        $scope.visible_teacher = $rootScope.visible_teacher;
                        $scope.visible_User = $rootScope.visible_User;
                        $scope.visible_Employee = $rootScope.visible_Employee;
                        $scope.chkMulti = $rootScope.chkMulti;
                        $scope.visible_UnassignedStudent = $rootScope.visible_UnassignedStudent;
                        $scope.visible_UnassignedParent = $rootScope.visible_UnassignedParent;
                    }
                    else {
                        $scope.visible_stud = $rootScope.visible_stud;
                        $scope.visible_parent = $rootScope.visible_parent;
                        $scope.visible_search_parent = false;
                        $scope.visible_teacher = $rootScope.visible_teacher;
                        $scope.visible_User = $rootScope.visible_User;
                        $scope.visible_Employee = $rootScope.visible_Employee;
                        $scope.chkMulti = $rootScope.chkMulti;
                        $scope.visible_UnassignedStudent = false;
                        $scope.visible_UnassignedParent = false;
                    }
                }
                else {
                    $scope.visible_stud = $rootScope.visible_stud;
                    $scope.visible_parent = $rootScope.visible_parent;
                    $scope.visible_search_parent = $rootScope.visible_search_parent
                    $scope.visible_teacher = $rootScope.visible_teacher;
                    $scope.visible_User = $rootScope.visible_User;
                    $scope.visible_Employee = $rootScope.visible_Employee;
                    $scope.chkMulti = $rootScope.chkMulti;
                    $scope.visible_UnassignedStudent = $rootScope.visible_UnassignedStudent;
                    $scope.visible_UnassignedParent = $rootScope.visible_UnassignedParent;
                }
                //   $("#tab1").css({ display: "none" });
                //  $("#tab2").css({ display: "none" });
                // $("#tab3").css({ display: "none" });
                // $("#tab4").css({ display: "none" });
                // $("#tab5").css({ display: "none" });
                // $("#tab6").css({ display: "none" });
                $scope.global_Search_show = true;
                $scope.selectedAll = false;
                $scope.markall1 = false;
                //$("#Global_Search_Modal").draggable({
                //    handle: ".modal-header"
                //});
                $('#tab1').removeClass('active');
                $('#tab2').removeClass('active');
                $('#tab3').removeClass('active');
                $('#tab4').removeClass('active');
                $('#tab5').removeClass('active');
                $('#tab6').removeClass('active');

                $('#tab7').removeClass('active');
                $('#tab8').removeClass('active');

                $('#mtab1').addClass('active');
                $('#mtab2').removeClass('active');
                $('#mtab3').removeClass('active');
                $('#mtab4').removeClass('active');
                $('#mtab5').removeClass('active');
                $('#mtab6').removeClass('active');
                $('#mtab7').removeClass('active');
                $('#mtab8').removeClass('active');

                if ($rootScope.visible_stud) {
                    //$("#tab1").css({ display: "block" });
                    $('#tab1').addClass('active');
                    $scope.gbl_tab = 'tab1'
                }
                else if ($scope.visible_parent) {
                    //$("#tab2").css({ display: "block" });
                    $('#tab2').addClass('active');
                    $scope.gbl_tab = 'tab2'

                }

                else if ($scope.visible_search_parent) {
                    //$("#tab3").css({ display: "block" });
                    $('#tab3').addClass('active');
                    $scope.gbl_tab = 'tab3'

                }

                else if ($scope.visible_teacher) {
                    //$("#tab4").css({ display: "block" });
                    $('#tab4').addClass('active');
                    $scope.gbl_tab = 'tab4'

                }

                else if ($scope.visible_User) {
                    //  $("#tab5").css({ display: "block" });
                    $('#tab5').addClass('active');
                    $scope.gbl_tab = 'tab5'

                }

                else if ($scope.visible_Employee) {
                    // $("#tab6").css({ display: "block" });
                    $('#tab6').addClass('active');
                    $scope.gbl_tab = 'tab6'

                }

                else if ($scope.visible_UnassignedStudent) {
                    //  $("#tab5").css({ display: "block" });
                    $('#tab7').addClass('active');
                    $scope.gbl_tab = 'tab7'

                }

                else if ($scope.visible_UnassignedParent) {
                    // $("#tab6").css({ display: "block" });
                    $('#tab8').addClass('active');
                    $scope.gbl_tab = 'tab8'

                }

                $scope.glbl_obj = '';
                $scope.global_Search = {};
                $scope.global_search_result = [];
                $scope.temp = '';
                $scope.EmployeeDetails = '';
                try {
                    $('#cmb_gradeparent').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_sectionparent').multipleSelect('uncheckAll');
                }
                catch (e) {

                }


                $http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (res) {
                    $scope.cmbCur = res.data;
                    if ($scope.cmbCur.length > 0) {
                        $scope.global_Search['global_curriculum_code'] = $scope.cmbCur[0].sims_cur_code;

                        $scope.getAcadm_yr($scope.cmbCur[0].sims_cur_code)
                    }
                })

            }
            $scope.firstTime = true;
            $scope.searchGlobalClick = function () {
                // if ($scope.firstTime) {
                $('#tab1').addClass('active');
                $('#tab2').removeClass('active');
                $('#tab3').removeClass('active');
                $('#tab4').removeClass('active');
                $('#tab5').removeClass('active');
                $('#tab6').removeClass('active');
                $('#tab7').removeClass('active');
                $('#tab8').removeClass('active');

                $('#mtab1').addClass('active');
                $('#mtab2').removeClass('active');
                $('#mtab3').removeClass('active');
                $('#mtab4').removeClass('active');
                $('#mtab5').removeClass('active');
                $('#mtab6').removeClass('active');
                $('#mtab7').removeClass('active');
                $('#mtab8').removeClass('active');


                //    $scope.firstTime = false;
                $scope.gbl_tab = 'tab1';
                //  }




                $scope.global_Search_show = true;
                if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                    if ($rootScope.globals.currentUser.username == 'admin') {
                        $scope.visible_stud = true;
                        $scope.visible_parent = true;
                        $scope.visible_search_parent = false;
                        $scope.visible_teacher = true;
                        $scope.visible_User = true;
                        $scope.visible_Employee = true;
                        $scope.visible_UnassignedStudent = true;
                        $scope.visible_UnassignedParent = true;
                    }
                    else {
                        $scope.visible_stud = true;
                        $scope.visible_parent = true;
                        $scope.visible_search_parent = false;
                        $scope.visible_teacher = true;
                        $scope.visible_User = true;
                        $scope.visible_Employee = false;
                        $scope.visible_UnassignedStudent = false;
                        $scope.visible_UnassignedParent = false;
                    }
                }
                else {
                    $scope.visible_stud = true;
                    $scope.visible_parent = true;
                    $scope.visible_search_parent = false;
                    $scope.visible_teacher = true;
                    $scope.visible_User = true;
                    $scope.visible_Employee = true;
                    $scope.visible_UnassignedStudent = true;
                    $scope.visible_UnassignedParent = true;
                }

                $scope.chkMulti = false;
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                //$("#Global_Search_Modal").draggable({
                //    handle: ".modal-header"
                //});
                $http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (res) {
                    $scope.cmbCur = res.data;
                    if ($scope.cmbCur.length > 0) {
                        $scope.global_Search['global_curriculum_code'] = $scope.cmbCur[0].sims_cur_code;

                        $scope.getAcadm_yr($scope.cmbCur[0].sims_cur_code)
                    }
                })


            }


            $scope.menu_click = function (str, tab) {
                $scope.gbl_tab = tab;
                $scope.SelectedUserLst = [];
                $scope.glbl_obj = '';
                $scope.global_Search = {};
                $scope.global_search_result = '';
                $scope.temp = {};
                try {
                    $('#cmb_gradeparent').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_sectionparent').multipleSelect('uncheckAll');
                }
                catch (e) {

                }

                $http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (res) {
                    $scope.cmbCur = res.data;
                    if ($scope.cmbCur.length > 0) {
                        $scope.global_Search['global_curriculum_code'] = $scope.cmbCur[0].sims_cur_code;
                        $scope.getAcadm_yr($scope.cmbCur[0].sims_cur_code)

                    }
                })

            }

            ///Student Profile

            $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/';
            $scope.pdfUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/Docs/timetable/';

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
                $("#fixTable2").tableHeadFixer({ 'top': 1 });
                $("#fixTable5").tableHeadFixer({ 'top': 1 });
                $("#fixTable6").tableHeadFixer({ 'top': 1 });
                $("#fixTable3").tableHeadFixer({ 'top': 1 });
                $("#fixTable4").tableHeadFixer({ 'top': 1 });
                $("#fixTable7").tableHeadFixer({ 'top': 1 });
                $("#example_wrapper3").scrollbar();
                $("#example_wrapper4").scrollbar();
                $("#example_wrapper5").scrollbar();
                $("#example_wrapper6").scrollbar();
                $("#example_wrapper7").scrollbar();
            }, 100);


            $scope.studentProfile = function (enrollNo, parentId, Gradecode, SectionCode, curcode) {

                $scope.temp6 = {
                    enrollNo: enrollNo,
                }
                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentGradeSection?enroll_no=" + enrollNo).then(function (res1) {
                    $scope.studentGrade = res1.data;
                    if ($scope.studentGrade.length > 0) {

                        $scope.edt1 = {
                            sims_grade_name: $scope.studentGrade[0].sims_grade_name,
                            sims_section_name: $scope.studentGrade[0].sims_section_name,
                        }
                    }
                });

                $http.get(ENV.apiUrl + "api/StudetProfile/getParentDetails?enroll_no=" + enrollNo).then(function (ParentDetails) {

                    $scope.Parent_Details = ParentDetails.data;
                    $scope.parent_info = $scope.Parent_Details[0];

                    $scope.student_father_image = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.parent_info.sims_admisison_father_image;
                    $scope.student_mother_image = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.parent_info.sims_admisison_mother_image;
                    $scope.student_guardian_image1 = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.parent_info.sims_admisison_guardian_image;

                    $scope.getstatefather($scope.parent_info.sims_admission_father_country_code);
                    $scope.getcityname($scope.parent_info.sims_admission_father_state);

                    $scope.getmothercityname($scope.parent_info.sims_admission_mother_state);
                    $scope.getstatemother($scope.parent_info.sims_admission_mother_country_code);

                    $scope.getguardiancityname($scope.parent_info.sims_admission_guardian_state);
                    $scope.getstateguardian($scope.parent_info.sims_admission_guardian_country_code);
                    $scope.isparentschoolemployee($scope.parent_info.sims_parent_is_employment_status);





                });

                $http.get(ENV.apiUrl + "api/StudetProfile/getStaffchilddetail?enroll_no=" + enrollNo).then(function (Staffchil) {
                    $scope.Staffchilddata = Staffchil.data;
                    if ($scope.Staffchilddata.length > 0) {
                        $scope.staffchildshow = true;
                        $scope.temp4 = {
                            comp_name: $scope.Staffchilddata[0].comp_name,
                            em_login_code: $scope.Staffchilddata[0].em_login_code,
                            employeeName: $scope.Staffchilddata[0].employeeName,
                            codp_dept_name: $scope.Staffchilddata[0].codp_dept_name,
                            dg_desc: $scope.Staffchilddata[0].dg_desc,
                        }
                    }
                    else {
                        $scope.staffchildshow = false;
                    }
                });

                $http.get(ENV.apiUrl + "api/StudetProfile/getLeaveApplicationDetails?enroll_no=" + enrollNo).then(function (leaveAppl) {
                    debugger
                    if ($http.defaults.headers.common['schoolId'] == 'adisw') {
                        $scope.feeDetailsshow1 = true;
                        $("#fee_details").css({ display: "none" });
                        // $('#fee_details').addClass
                    }

                    $scope.Leaveapplication = leaveAppl.data;
                    if ($scope.Leaveapplication.length > 0) {
                        $scope.LeaveApplicationshow = true;
                        $scope.LeaveApplicationhide = false;
                    }
                    else {
                        $scope.LeaveApplicationhide = true;
                        $scope.LeaveApplicationshow = false;
                    }
                });

                $http.get(ENV.apiUrl + "api/StudetProfile/getSubjectDetails?enroll_no=" + enrollNo + "&grade_cd=" + Gradecode + "&section_cd=" + SectionCode).then(function (subject) {

                    $scope.subjectDetails = subject.data;
                    if ($scope.subjectDetails.length > 0) {
                        $scope.subjectDatashow = true;
                        $scope.subjectdetailhide = false;
                    }
                    else {
                        $scope.subjectdetailhide = true;
                        $scope.subjectDatashow = false;
                    }
                });

                $http.get(ENV.apiUrl + "api/StudetProfile/Get_SimsActiveForm").then(function (res1) {

                    $scope.activeForms = res1.data;
                    if ($scope.activeForms != undefined || $scope.activeForms != null) {

                        for (var i = 0; i < $scope.activeForms.length; i++) {

                            if ($scope.activeForms[i].sims_active_form == "student" && $scope.activeForms[i].sims_active_form_status == "Y") {
                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentDetails?student_enroll=" + enrollNo + "&parent_no=" + parentId).then(function (res) {
                                    debugger;
                                    $scope.studentProfileshow = true;
                                    $scope.academicYearshow = true;
                                    $scope.StudetProfile = res.data;
                                    if ($scope.StudetProfile.length > 0) {
                                        $scope.temp = {
                                            student_full_name: $scope.StudetProfile[0].student_full_name,
                                            sims_student_family_name_en: $scope.StudetProfile[0].sims_student_family_name_en,
                                            sims_student_image: $scope.StudetProfile[0].sims_student_image,

                                            sims_student_family_name_ot: $scope.StudetProfile[0].sims_student_family_name_ot,
                                            sims_student_nickname: $scope.StudetProfile[0].sims_student_nickname,
                                            student_dob: $scope.StudetProfile[0].sims_student_dob,
                                            sims_student_nationality_code: $scope.StudetProfile[0].sims_student_nationality_code,
                                            sims_student_gender: $scope.StudetProfile[0].sims_student_gender,
                                            sims_student_birth_country_code: $scope.StudetProfile[0].sims_student_birth_country_code,
                                            sims_student_religion_code: $scope.StudetProfile[0].sims_student_religion_code,
                                            sims_parent_father_summary_address: $scope.StudetProfile[0].sims_parent_father_summary_address,
                                            sims_parent_country_code: $scope.StudetProfile[0].sims_parent_country_code,
                                            sims_parent_father_city: $scope.StudetProfile[0].sims_parent_father_city,
                                            sims_parent_father_phone: $scope.StudetProfile[0].sims_parent_father_phone,
                                            sims_parent_father_email: $scope.StudetProfile[0].sims_parent_father_email,
                                            sims_parent_father_state: $scope.StudetProfile[0].sims_parent_father_state,
                                            sims_parent_father_po_box: $scope.StudetProfile[0].sims_parent_father_po_box,
                                            sims_parent_father_mobile: $scope.StudetProfile[0].sims_parent_father_mobile,
                                            sims_parent_father_fax: $scope.StudetProfile[0].sims_parent_father_fax,
                                            sims_student_emergency_contact_name1: $scope.StudetProfile[0].sims_student_emergency_contact_name1,
                                            sims_student_emergency_contact_name2: $scope.StudetProfile[0].sims_student_emergency_contact_name2,
                                            sims_student_emergency_contact_number1: $scope.StudetProfile[0].sims_student_emergency_contact_number1,
                                            sims_student_emergency_contact_number2: $scope.StudetProfile[0].sims_student_emergency_contact_number2,

                                            sims_student_passport_number: $scope.StudetProfile[0].sims_student_passport_number,
                                            sims_student_passport_issue_date: $scope.StudetProfile[0].sims_student_passport_issue_date,
                                            sims_student_passport_expiry_date: $scope.StudetProfile[0].sims_student_passport_expiry_date,
                                            sims_student_passport_issuing_authority: $scope.StudetProfile[0].sims_student_passport_issuing_authority,
                                            sims_student_passport_issue_place: $scope.StudetProfile[0].sims_student_passport_issue_place,

                                            sims_student_visa_number: $scope.StudetProfile[0].sims_student_visa_number,
                                            sims_student_visa_type: $scope.StudetProfile[0].sims_student_visa_type,
                                            sims_student_visa_issuing_authority: $scope.StudetProfile[0].sims_student_visa_issuing_authority,

                                            sims_student_visa_issuing_place: $scope.StudetProfile[0].sims_student_visa_issuing_place,
                                            sims_student_visa_expiry_date: $scope.StudetProfile[0].sims_student_visa_expiry_date,
                                            sims_student_visa_issue_date: $scope.StudetProfile[0].sims_student_visa_issue_date,
                                            sims_student_national_id: $scope.StudetProfile[0].sims_student_national_id,
                                            sims_student_national_id_issue_date: $scope.StudetProfile[0].sims_student_national_id_issue_date,
                                            sims_student_national_id_expiry_date: $scope.StudetProfile[0].sims_student_national_id_expiry_date,

                                            sims_student_current_school_code: $scope.StudetProfile[0].sims_student_current_school_code,
                                            sims_student_ea_number: $scope.StudetProfile[0].sims_student_ea_number,
                                            sims_student_commence_date: $scope.StudetProfile[0].sims_student_commence_date,
                                            sims_student_date: $scope.StudetProfile[0].sims_student_date,
                                            sims_student_academic_status: $scope.StudetProfile[0].sims_student_academic_status,
                                            sims_student_house: $scope.StudetProfile[0].sims_student_house,
                                            sims_student_class_rank: $scope.StudetProfile[0].sims_student_class_rank,
                                            sims_student_transport_status: $scope.StudetProfile[0].sims_student_transport_status,
                                            sims_student_main_language: $scope.StudetProfile[0].sims_student_main_language,
                                            sims_student_other_language: $scope.StudetProfile[0].sims_student_other_language,
                                            sims_student_last_login: $scope.StudetProfile[0].sims_student_last_login,
                                            sims_student_remark: $scope.StudetProfile[0].sims_student_remark,

                                        }
                                    }

                                });
                            }
                            else if ($scope.activeForms[i].sims_active_form == "medical" && $scope.activeForms[i].sims_active_form_status == "Y") {
                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentMedicalDetails?enroll_no=" + enrollNo).then(function (res) {
                                    $scope.MedicalData = res.data;

                                    $scope.medicalDetailsshow = true;
                                    if ($scope.MedicalData.length > 0) {
                                        $scope.medicalDetailsshowData = true;
                                        $scope.medicalDetailshideData = false;

                                        $scope.edt = {
                                            health_card_number: $scope.MedicalData[0].health_card_number,
                                            health_card_issue_date: $scope.MedicalData[0].health_card_issue_date,
                                            health_card_expiry_date: $scope.MedicalData[0].health_card_expiry_date,
                                            health_card_issuing_authority: $scope.MedicalData[0].health_card_issuing_authority,
                                            student_blood_group_code: $scope.MedicalData[0].student_blood_group_code,
                                            student_health_restriction_status: $scope.MedicalData[0].student_health_restriction_status,
                                            student_disability_status: $scope.MedicalData[0].student_disability_status,
                                            student_medication_status: $scope.MedicalData[0].student_medication_status,
                                            health_other_status: $scope.MedicalData[0].health_other_status,
                                            health_hearing_status: $scope.MedicalData[0].health_hearing_status,
                                            health_vision_status: $scope.MedicalData[0].health_vision_status,

                                            regular_doctor_name: $scope.MedicalData[0].regular_doctor_name,
                                            regular_hospital_name: $scope.MedicalData[0].regular_hospital_name,
                                            regular_doctor_phone: $scope.MedicalData[0].regular_doctor_phone,
                                            regular_hospital_phone: $scope.MedicalData[0].regular_hospital_phone,

                                            student_height: $scope.MedicalData[0].student_height,
                                            student_wieght: $scope.MedicalData[0].student_wieght,
                                            student_teeth: $scope.MedicalData[0].student_teeth,


                                        }
                                    }
                                    else {
                                        $scope.medicalDetailshideData = true;
                                        $scope.medicalDetailsshowData = false;
                                    }

                                });

                            }

                            else if ($scope.activeForms[i].sims_active_form == "fees" && $scope.activeForms[i].sims_active_form_status == "Y") {
                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_FeeDetails?enroll_no=" + enrollNo).then(function (res) {
                                    $scope.feeDetailsshow = true;

                                    $scope.FeeDetails = res.data;
                                    if ($scope.FeeDetails.length > 0) {
                                        $scope.feeDetailsData = true;
                                        $scope.feeDetailshide = false;
                                    }

                                    else {
                                        $scope.feeDetailshide = true;
                                        $scope.feeDetailsData = false;
                                    }


                                });

                            }

                            else if ($scope.activeForms[i].sims_active_form == "incidence" && $scope.activeForms[i].sims_active_form_status == "Y") {
                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_SimsIncidence?enroll_no=" + enrollNo).then(function (res) {

                                    $scope.IncidanceDetails = res.data;

                                    $scope.incidenceDetailsshow = true;
                                    if ($scope.IncidanceDetails.length > 0) {
                                        $scope.incidenceDetailsData = true;
                                        $scope.incidenceDetailshide = false;
                                    }
                                    else {
                                        $scope.incidenceDetailshide = true;
                                        $scope.incidenceDetailsData = false;
                                    }

                                });

                            }

                            else if ($scope.activeForms[i].sims_active_form == "library" && $scope.activeForms[i].sims_active_form_status == "Y") {
                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_LibraryTransaction?enroll_no=" + enrollNo).then(function (res) {

                                    $scope.libraryDetailsshow = true;

                                    $scope.LibraryDetails = res.data;
                                    if ($scope.LibraryDetails.length > 0) {
                                        $scope.libraryDetailsData = true;
                                        $scope.libraryDetailshide = false;
                                    }
                                    else {
                                        $scope.libraryDetailshide = true;
                                        $scope.libraryDetailsData = false;
                                    }



                                });

                            }

                            else if ($scope.activeForms[i].sims_active_form == "attendance" && $scope.activeForms[i].sims_active_form_status == "Y") {
                                debugger;
                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentAttendancechartview?enroll_no=" + enrollNo).then(function (res) {
                                    $scope.attendanceDetailsshow = true;
                                    $scope.AttendanceDetails = res.data;
                                    if ($scope.AttendanceDetails.length > 0) {
                                        $scope.attendanceDetailsData = true;
                                        $scope.attendanceDetailshide = false;
                                    }
                                    else {
                                        $scope.attendanceDetailshide = true;
                                        $scope.attendanceDetailsshow = false;
                                    }


                                    $scope.totalpresent = 0;
                                    $scope.totalApsent = 0;
                                    $scope.totalDays = 0;
                                    for (var k = 0; k < $scope.AttendanceDetails.length; k++) {
                                        $scope.totalpresent = $scope.totalpresent + $scope.AttendanceDetails[k].sims_attendance_cd_p;
                                        $scope.totalApsent = $scope.totalApsent + $scope.AttendanceDetails[k].sims_attendance_cd_a;
                                        $scope.totalDays = $scope.totalDays + $scope.AttendanceDetails[k].sims_attendance_totaldays;
                                    }
                                    $scope.edt2 = {
                                        sims_attendance_cd_p: $scope.totalpresent,
                                        sims_attendance_cd_a: $scope.totalApsent,
                                    }

                                });

                            }
                            else if ($scope.activeForms[i].sims_active_form == "result" && $scope.activeForms[i].sims_active_form_status == "Y") {
                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentResult?enroll_no=" + enrollNo + "&grade_cd=" + Gradecode + "&section_cd=" + SectionCode).then(function (res) {
                                    $scope.ResultDetails = res.data;

                                    $scope.resultDetailsshow = true;
                                    $scope.resultDetailshide = false;
                                    if ($scope.ResultDetails.length > 0) {
                                        $scope.resultDetailsData = true;
                                    }
                                    else {
                                        $scope.resultDetailshide = true;
                                        $scope.resultDetailsshow = false;
                                    }


                                });

                            }

                            else if ($scope.activeForms[i].sims_active_form == "timetable" && $scope.activeForms[i].sims_active_form_status == "Y") {
                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_TimetableFileName?grade_cd=" + Gradecode + "&section_cd=" + SectionCode).then(function (res) {

                                    $scope.TimeTableDetails = res.data;
                                    $scope.timeTableDetailsshow = true;
                                    if ($scope.TimeTableDetails.length > 0) {
                                        $scope.timeTableDetailsData = true;
                                        $scope.timeTableDetailshide = false;
                                        //$scope.edt3 = {
                                        //    pdffile: $scope.TimeTableDetails,
                                        //}
                                        $scope.edt3 = {
                                            pdffile: $scope.pdfUrl + $scope.TimeTableDetails
                                        }
                                        $("#pdfFileE").attr('src', $scope.edt3.pdffile);
                                    }
                                    else {
                                        $scope.timeTableDetailshide = true;
                                        $scope.timeTableDetailsData = false;
                                        $("#pdfFileE").attr('src','');
                                    }


                                });

                            }

                            else if ($scope.activeForms[i].sims_active_form == "transport" && $scope.activeForms[i].sims_active_form_status == "Y") {
                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_TransportRouteStudent?enroll_no=" + enrollNo).then(function (res) {
                                    debugger;
                                    $scope.TransportDetails = res.data;
                                    $scope.tansportDetailsshow = true;
                                    if ($scope.TransportDetails.length > 0) {
                                        $scope.tansportDetailsshowData = true;
                                        $scope.tansportDetailshideData = false;

                                        $scope.temp1 = {
                                            sims_transport_route_code_name: $scope.MedicalData[0].sims_transport_route_code_name,
                                            sims_transport_route_direction: $scope.MedicalData[0].sims_transport_route_direction,
                                            sims_transport_effective_from: $scope.MedicalData[0].sims_transport_effective_from,
                                            sims_transport_effective_upto: $scope.MedicalData[0].sims_transport_effective_upto,
                                            sims_transport_pickup_stop_name: $scope.MedicalData[0].sims_transport_pickup_stop_name,
                                            sims_transport_drop_stop_name: $scope.MedicalData[0].sims_transport_drop_stop_name,

                                        }
                                    }

                                    else {
                                        $scope.tansportDetailsshowData = false;
                                        $scope.tansportDetailshideData = true;
                                    }


                                });

                            }

                            else if ($scope.activeForms[i].sims_active_form == "sibling" && $scope.activeForms[i].sims_active_form_status == "Y") {
                                $http.get(ENV.apiUrl + "api/StudetProfile/Get_SiblingDetails?enroll_no=" + enrollNo).then(function (res) {

                                    $scope.SiblingDetails = res.data;
                                    if ($scope.SiblingDetails.length > 0) {
                                        $scope.temp2 = {
                                            siblingCount: $scope.SiblingDetails.length,

                                        }
                                    }
                                    else {
                                        $scope.temp2 = {
                                            siblingCount: 0,
                                        }



                                    }

                                });

                            }
                        }
                    }

                });

                $('#StudentProfileModal').modal('show');



            }

            $scope.check_clickdataP = function (atte) {
                debugger;
                var enrollno = null;
                var f = atte.sims_attendance_cd_p;
                for (var i = 0; i < $scope.global_search_result.length; i++) {
                    enrollno = $scope.global_search_result[i].s_enroll_no;
                }

                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentAttendancedayview?enroll_no=" + enrollno + "&status=" + 'P' + "&month=" + atte.sims_month_no).then(function (res) {
                    $scope.all_data_stud_perticular = res.data;
                    $scope.all_data_emp_perticular1 = res.data;
                    if ($scope.all_data_emp_perticular1.length > 0) {
                        $('#MyModal42').modal('show');
                        $scope.tabledata = true;
                        $scope.tansportDetailsshow = true;
                        //$scope.tansportDetailsshow

                    }
                    else {
                        swal({ title: "Alert", text: "Data not Availiable", width: 300, height: 200 });
                    }

                });
            }

            $scope.check_clickdataa = function (atte) {
                debugger;
                var enrollno = null;
                var f = atte.sims_attendance_cd_p;
                for (var i = 0; i < $scope.global_search_result.length; i++) {
                    enrollno = $scope.global_search_result[i].s_enroll_no;
                }

                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentAttendancedayview?enroll_no=" + enrollno + "&status=" + 'A' + "&month=" + atte.sims_month_no).then(function (res) {
                    $scope.all_data_stud_perticular = res.data;
                    $scope.all_data_emp_perticular1 = res.data;
                    if ($scope.all_data_emp_perticular1.length > 0) {
                        $('#MyModal42').modal('show');
                        $scope.tabledata = true;
                        $scope.tansportDetailsshow = true;
                        //$scope.tansportDetailsshow

                    }
                    else {
                        swal({ title: "Alert", text: "Data not Availiable", width: 300, height: 200 });
                    }

                });
            }

            $scope.check_clickdataae = function (atte) {
                debugger;
                var enrollno = null;
                var f = atte.sims_attendance_cd_p;
                for (var i = 0; i < $scope.global_search_result.length; i++) {
                    enrollno = $scope.global_search_result[i].s_enroll_no;
                }

                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentAttendancedayview?enroll_no=" + enrollno + "&status=" + 'AE' + "&month=" + atte.sims_month_no).then(function (res) {
                    $scope.all_data_stud_perticular = res.data;
                    $scope.all_data_emp_perticular1 = res.data;
                    if ($scope.all_data_emp_perticular1.length > 0) {
                        $('#MyModal42').modal('show');
                        $scope.tabledata = true;
                        $scope.tansportDetailsshow = true;
                        //$scope.tansportDetailsshow

                    }
                    else {
                        swal({ title: "Alert", text: "Data not Availiable", width: 300, height: 200 });
                    }

                });
            }

            $scope.check_clickdatat = function (atte) {
                debugger;
                var enrollno = null;
                var f = atte.sims_attendance_cd_p;
                for (var i = 0; i < $scope.global_search_result.length; i++) {
                    enrollno = $scope.global_search_result[i].s_enroll_no;
                }

                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentAttendancedayview?enroll_no=" + enrollno + "&status=" + 'T' + "&month=" + atte.sims_month_no).then(function (res) {
                    $scope.all_data_stud_perticular = res.data;
                    $scope.all_data_emp_perticular1 = res.data;
                    if ($scope.all_data_emp_perticular1.length > 0) {
                        $('#MyModal42').modal('show');
                        $scope.tabledata = true;
                        $scope.tansportDetailsshow = true;
                        //$scope.tansportDetailsshow

                    }
                    else {
                        swal({ title: "Alert", text: "Data not Availiable", width: 300, height: 200 });
                    }

                });
            }

            $scope.check_clickdatate = function (atte) {
                debugger;
                var enrollno = null;
                var f = atte.sims_attendance_cd_p;
                for (var i = 0; i < $scope.global_search_result.length; i++) {
                    enrollno = $scope.global_search_result[i].s_enroll_no;
                }

                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentAttendancedayview?enroll_no=" + enrollno + "&status=" + 'TE' + "&month=" + atte.sims_month_no).then(function (res) {
                    $scope.all_data_stud_perticular = res.data;
                    $scope.all_data_emp_perticular1 = res.data;
                    if ($scope.all_data_emp_perticular1.length > 0) {
                        $('#MyModal42').modal('show');
                        $scope.tabledata = true;
                        $scope.tansportDetailsshow = true;
                        //$scope.tansportDetailsshow

                    }
                    else {
                        swal({ title: "Alert", text: "Data not Availiable", width: 300, height: 200 });
                    }

                });
            }

            $scope.check_clickdataum = function (atte) {
                debugger;
                var enrollno = null;
                var f = atte.sims_attendance_cd_p;
                for (var i = 0; i < $scope.global_search_result.length; i++) {
                    enrollno = $scope.global_search_result[i].s_enroll_no;
                }

                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentAttendancedayview?enroll_no=" + enrollno + "&status=" + 'UM' + "&month=" + atte.sims_month_no).then(function (res) {
                    $scope.all_data_stud_perticular = res.data;
                    $scope.all_data_emp_perticular1 = res.data;
                    if ($scope.all_data_emp_perticular1.length > 0) {
                        $('#MyModal42').modal('show');
                        $scope.tabledata = true;
                        $scope.tansportDetailsshow = true;
                        //$scope.tansportDetailsshow

                    }
                    else {
                        swal({ title: "Alert", text: "Data not Availiable", width: 300, height: 200 });
                    }

                });
            }

            $scope.check_clickdataw = function (atte) {
                debugger;
                var enrollno = null;
                var f = atte.sims_attendance_cd_p;
                for (var i = 0; i < $scope.global_search_result.length; i++) {
                    enrollno = $scope.global_search_result[i].s_enroll_no;
                }

                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentAttendancedayview?enroll_no=" + enrollno + "&status=" + 'W' + "&month=" + atte.sims_month_no).then(function (res) {
                    $scope.all_data_stud_perticular = res.data;
                    $scope.all_data_emp_perticular1 = res.data;
                    if ($scope.all_data_emp_perticular1.length > 0) {
                        $('#MyModal42').modal('show');
                        $scope.tabledata = true;
                        $scope.tansportDetailsshow = true;
                        //$scope.tansportDetailsshow

                    }
                    else {
                        swal({ title: "Alert", text: "Data not Availiable", width: 300, height: 200 });
                    }

                });
            }

            $scope.check_clickdatah = function (atte) {
                debugger;
                var enrollno = null;
                var f = atte.sims_attendance_cd_p;
                for (var i = 0; i < $scope.global_search_result.length; i++) {
                    enrollno = $scope.global_search_result[i].s_enroll_no;
                }

                $http.get(ENV.apiUrl + "api/StudetProfile/Get_StudentAttendancedayview?enroll_no=" + enrollno + "&status=" + 'H' + "&month=" + atte.sims_month_no).then(function (res) {
                    $scope.all_data_stud_perticular = res.data;
                    $scope.all_data_emp_perticular1 = res.data;
                    if ($scope.all_data_emp_perticular1.length > 0) {
                        $('#MyModal42').modal('show');
                        $scope.tabledata = true;
                        $scope.tansportDetailsshow = true;
                        //$scope.tansportDetailsshow

                    }
                    else {
                        swal({ title: "Alert", text: "Data not Availiable", width: 300, height: 200 });
                    }

                });
            }

            $scope.closemymodule2 = function () {
                $('#MyModal42').modal('hide');
                $scope.attendanceDetailsshow = true;
            }


            //$scope.check_clickdata = function () {
            //    $('#MyModal2').modal('show');
            //}

            $timeout(function () {
                $("#printdata").tableHeadFixer({ 'top': 1 });
            }, 100);






            // for employee_login details
            $scope.LoginDetails = function () {
                $('#employeeProfile').modal('show');
                debugger;
                $scope.profile_details = true;
                $scope.NodataAttendance = false;
                $scope.NodataLeave = false;
                $scope.NodataClass = false;
                $http.get(ENV.apiUrl + "api/common/getEmployeeProfileView?em_login_code=" + user + "&em_number=" + user).then(function (getEmployeeProfileView_Data) {
                    $scope.EmployeeProfileView = getEmployeeProfileView_Data.data;
                    console.log($scope.EmployeeProfileView);
                    for (var i = 0; i < $scope.EmployeeProfileView.length; i++) {


                        //   $scope.imageUrl = ENV.apiUrl + 'Content/sjs/Images/EmployeeImages/' + $scope.EmployeeProfileView[i].em_img;
                        $scope.imageUrl = ENV.siteUrl + 'Images/EmployeeImages/' + $scope.EmployeeProfileView[i].em_img;
                        $scope.em_full_name = $scope.EmployeeProfileView[i].em_full_name;
                        $scope.em_name_ot = $scope.EmployeeProfileView[i].em_name_ot;
                        $scope.em_family_name = $scope.EmployeeProfileView[i].em_family_name;
                        $scope.em_sex = $scope.EmployeeProfileView[i].em_sex;
                        $scope.em_marital_status = $scope.EmployeeProfileView[i].em_marital_status;
                        $scope.em_nation_name = $scope.EmployeeProfileView[i].em_nation_name;
                        $scope.em_last_login = $scope.EmployeeProfileView[i].em_last_login;
                        $scope.em_date_of_birth = $scope.EmployeeProfileView[i].em_date_of_birth;
                        $scope.em_handicap_status = $scope.EmployeeProfileView[i].em_handicap_status;
                        $scope.em_religion_name = $scope.EmployeeProfileView[i].em_religion_name;
                        $scope.em_salutation = $scope.EmployeeProfileView[i].em_salutation;
                        $scope.em_Salutation_Code = $scope.EmployeeProfileView[i].em_Salutation_Code;


                        //Passport & Contact_Details
                        $scope.em_passport_no = $scope.EmployeeProfileView[i].em_passport_no;
                        $scope.em_passport_issue_date = $scope.EmployeeProfileView[i].em_passport_issue_date;
                        $scope.em_passport_exp_date = $scope.EmployeeProfileView[i].em_passport_exp_date;
                        $scope.em_passport_issuing_authority = $scope.EmployeeProfileView[i].em_passport_issuing_authority;

                        $scope.em_emergency_contact_name1 = $scope.EmployeeProfileView[i].em_emergency_contact_name1;
                        $scope.em_emergency_contact_number1 = $scope.EmployeeProfileView[i].em_emergency_contact_number1;
                        $scope.em_emergency_contact_name2 = $scope.EmployeeProfileView[i].em_emergency_contact_name2;
                        $scope.em_emergency_contact_number2 = $scope.EmployeeProfileView[i].em_emergency_contact_number2;

                        //Visa & NationalID_Details
                        $scope.em_visa_no = $scope.EmployeeProfileView[i].em_visa_no;
                        $scope.em_visa_type = $scope.EmployeeProfileView[i].em_visa_type;
                        $scope.em_visa_issuing_authority = $scope.EmployeeProfileView[i].em_visa_issuing_authority;
                        $scope.em_visa_issuing_place = $scope.EmployeeProfileView[i].em_visa_issuing_place;
                        $scope.em_visa_issue_date = $scope.EmployeeProfileView[i].em_visa_issue_date;
                        $scope.em_visa_exp_date = $scope.EmployeeProfileView[i].em_visa_exp_date;

                        $scope.em_national_id = $scope.EmployeeProfileView[i].em_national_id;
                        $scope.em_national_id_issue_date = $scope.EmployeeProfileView[i].em_national_id_issue_date;
                        $scope.em_national_id_expiry_date = $scope.EmployeeProfileView[i].em_national_id_expiry_date;

                        //Company_Details
                        $scope.em_company_name = $scope.EmployeeProfileView[i].em_company_name;
                        $scope.em_dept_name = $scope.EmployeeProfileView[i].em_dept_name;
                        $scope.em_desg_name = $scope.EmployeeProfileView[i].em_desg_name;
                        $scope.em_joining_ref = $scope.EmployeeProfileView[i].em_joining_ref;
                        $scope.em_grade_name = $scope.EmployeeProfileView[i].em_grade_name;
                        $scope.em_grade_effect_from = $scope.EmployeeProfileView[i].em_grade_effect_from;
                        $scope.em_date_of_join = $scope.EmployeeProfileView[i].em_date_of_join;
                        $scope.em_agreement_start_date = $scope.EmployeeProfileView[i].em_agreement_start_date;
                        $scope.em_agreement_exp_date = $scope.EmployeeProfileView[i].em_agreement_exp_date;
                        $scope.em_punching_id = $scope.EmployeeProfileView[i].em_punching_id;
                        $scope.em_staff_type = $scope.EmployeeProfileView[i].em_staff_type;
                        $scope.em_service_status = $scope.EmployeeProfileView[i].em_service_status;

                        //Bank_Details
                        $scope.em_bank_name = $scope.EmployeeProfileView[i].em_bank_name;
                        $scope.em_gpf_ac_no = $scope.EmployeeProfileView[i].em_gpf_ac_no;
                        $scope.em_gosi_ac_no = $scope.EmployeeProfileView[i].em_gosi_ac_no;
                        $scope.em_labour_card_no = $scope.EmployeeProfileView[i].em_labour_card_no;
                        $scope.em_bank_ac_no = $scope.EmployeeProfileView[i].em_bank_ac_no;
                        $scope.em_bank_swift_code = $scope.EmployeeProfileView[i].em_bank_swift_code;
                        $scope.em_gosi_start_date = $scope.EmployeeProfileView[i].em_gosi_start_date;
                        $scope.em_iban_no = $scope.EmployeeProfileView[i].em_iban_no;


                        $scope.em_summary_address = $scope.EmployeeProfileView[i].em_summary_address;
                        $scope.em_country = $scope.EmployeeProfileView[i].em_country;
                        $scope.em_state = $scope.EmployeeProfileView[i].em_state;
                        $scope.em_city = $scope.EmployeeProfileView[i].em_city;
                        $scope.em_po_box = $scope.EmployeeProfileView[i].em_po_box;
                        $scope.em_phone = $scope.EmployeeProfileView[i].em_phone;
                        $scope.em_mobile = $scope.EmployeeProfileView[i].em_mobile;
                        $scope.em_iban_no = $scope.EmployeeProfileView[i].em_iban_no;
                        $scope.em_email = $scope.EmployeeProfileView[i].em_email;

                    }
                });

                $http.get(ENV.apiUrl + "api/common/getEmployeeAttendanceDetails?em_login_code=" + user).then(function (getEmployeeAttendanceDetails_Data) {
                    $scope.EmployeeAttendanceDetails = getEmployeeAttendanceDetails_Data.data;
                    console.log($scope.EmployeeAttendanceDetails);

                    if ($scope.EmployeeAttendanceDetails < 1) {
                        $scope.NodataAttendance = true;

                    }
                    else {

                        $scope.AttendanceData = true;
                    }

                });


                $http.get(ENV.apiUrl + "api/common/getEmployeeLeavesDetails?el_number=" + user).then(function (getEmployeeLeavesDetails_Data) {
                    $scope.EmployeeLeavesDetails = getEmployeeLeavesDetails_Data.data;
                    console.log($scope.EmployeeLeavesDetails);

                    if ($scope.EmployeeLeavesDetails < 1) {
                        $scope.NodataLeave = true;

                    }
                    else {

                        $scope.LeaveData = true;
                    }
                });

                $http.get(ENV.apiUrl + "api/common/getEmployeeClassDetails?em_login_code=" + user).then(function (getEmployeeClassDetails_Data) {
                    $scope.EmployeeClassDetails = getEmployeeClassDetails_Data.data;
                    console.log($scope.EmployeeClassDetails);

                    if ($scope.EmployeeClassDetails < 1) {
                        $scope.NodataClass = true;

                    }
                    else {

                        $scope.ClassData = true;
                    }
                });
            }


            $scope.logout = function () {
                AuthenticationService.ClearCredentials();
                $state.go("login");

                //window.localStorage["Finn_comp"]
                window.localStorage.removeItem("ReportProvNumber");
            }




            $scope.menuoptions = [
                  { uiSref: "main.Sim771", name: gettextCatalog.getString('Weekly Assessment Report'), iconCls: "fa fa-dashboard" },
           { uiSref: "main.Dasadi", name: gettextCatalog.getString('Admission Dashboard'), iconCls: "fa fa-dashboard" },
            { uiSref: "main.Sim770", name: gettextCatalog.getString('Weekly Assessment'), iconCls: "fa fa-dashboard" },
            { uiSref: "main.InvSaleDoc", name: gettextCatalog.getString('Sales Document List'), iconCls: "fa fa-dashboard" },
               { uiSref: "main.EmLPrl", name: gettextCatalog.getString('Employee Master'), iconCls: "fa fa-dashboard" },
            { uiSref: "main.FeePPM", name: gettextCatalog.getString('Fee Posting Payment Mode-Invs'), iconCls: "fa fa-dashboard" },

            { uiSref: "main.EmpGrt", name: gettextCatalog.getString('Employee Gratuity View'), iconCls: "fa fa-dashboard" },
            { uiSref: "main.UAdisw", name: gettextCatalog.getString('Update Employee'), iconCls: "fa fa-dashboard" },

            { uiSref: "main.UpEmpM", name: gettextCatalog.getString('Update Employee'), iconCls: "fa fa-dashboard" },
            { uiSref: "main.EmLgAD", name: gettextCatalog.getString('Employee Long Form'), iconCls: "fa fa-dashboard" },
            { uiSref: "main.CdEmpW", name: gettextCatalog.getString('Employee Short Form'), iconCls: "fa fa-dashboard" },



            { uiSref: "main.TrnEMP", name: gettextCatalog.getString('Transfer Employee'), iconCls: "fa fa-dashboard" },
            { uiSref: "main.EmpDps", name: gettextCatalog.getString('Create Edit Employee'), iconCls: "fa fa-dashboard" },


            { uiSref: "main.Sim615", name: gettextCatalog.getString('Fee Receipt'), iconCls: "fa fa-dashboard" },
            { uiSref: "main.EmpOES", name: gettextCatalog.getString('Employee Master'), iconCls: "fa fa-dashboard" },
            { uiSref: "main.SubTrt", name: gettextCatalog.getString('Trait Master'), iconCls: "fa fa-dashboard" },
            { uiSref: "main.StuTrt", name: gettextCatalog.getString('Student Trait'), iconCls: "fa fa-dashboard" },

            { uiSref: "main.UpdOES", name: gettextCatalog.getString('Update Employee'), iconCls: "fa fa-dashboard" },
            { uiSref: "main.Sm043C", name: gettextCatalog.getString('Cancel Fee Receipt'), iconCls: "fa fa-dashboard" },


                   { uiSref: "main.Com007", name: gettextCatalog.getString('User Audit'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.EmployeeShortForm", name: gettextCatalog.getString('Employee Short Form'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Fin017", name: gettextCatalog.getString('Fixed Asset Depreciation Calculation'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Per256", name: gettextCatalog.getString('Qualification Master'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.stdEA", name: gettextCatalog.getString('Update Student EA Number'), iconCls: "fa fa-dashboard" },

                   { uiSref: "main.stdImg", name: gettextCatalog.getString('Update Student Image'), iconCls: "fa fa-dashboard" },

                   { uiSref: "main.Inv148", name: gettextCatalog.getString('Inter Department Transfer'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.InvRes", name: gettextCatalog.getString('Cancel Sales Receipt'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Inv149", name: gettextCatalog.getString('Approved IDT'), iconCls: "fa fa-dashboard" },

                   { uiSref: "main.Per324", name: gettextCatalog.getString('Gratuity Calculation'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Per323", name: gettextCatalog.getString('Gratuity Criteria'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Per306", name: gettextCatalog.getString('Mark Employee Resignation'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.Per046", name: gettextCatalog.getString('Pays Grade'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Fin17A", name: gettextCatalog.getString('Depreciation JV Post'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Cls10T", name: gettextCatalog.getString('ClassList Teacher View'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Admissiondashboard", name: gettextCatalog.getString('Admission Dashboard'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.Admcan", name: gettextCatalog.getString('ADM Cancellation'), iconCls: "fa fa-dashboard" },

               { uiSref: "main.Com052", name: gettextCatalog.getString('Prospect Admission'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.ProspectAdmission", name: gettextCatalog.getString('Prospect Admission'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.ProspectDashboard", name: gettextCatalog.getString('Prospect Dashboard'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.sim041", name: gettextCatalog.getString('SMTP'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Sim43b", name: gettextCatalog.getString('Student Fee Refund'), iconCls: "fa fa-dashboard" },
                    // { uiSref: "main.Sim020", name: gettextCatalog.getString('EmployeeDocumentMaster'), iconCls: "fa fa-dashboard" },
                   //{ uiSref: "main.Pers312", name: gettextCatalog.getString('EmployeeDocumentMaster'), iconCls: "fa fa-dashboard" },
                       { uiSref: "main.Per610", name: gettextCatalog.getString('EmployeeDocumentDetails'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Fin099", name: gettextCatalog.getString('PDC Receipt(Bank Receipt)'), iconCls: "fa fa-dashboard" },
                   //{ uiSref: "main.Fin212", name: gettextCatalog.getString('PDC Bills'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.Sim531", name: gettextCatalog.getString('Student Paying Agent'), iconCls: "fa fa-dashboard" },
                     { uiSref: "main.Sims532", name: gettextCatalog.getString('Student Paying Agent Mapping'), iconCls: "fa fa-dashboard" },
                       { uiSref: "main.LPOGRN", name: gettextCatalog.getString('LPO GRN'), iconCls: "fa fa-dashboard" },
                         { uiSref: "main.BankReconciliation", name: gettextCatalog.getString('Bank Reconciliation'), iconCls: "fa fa-dashboard" },
                           { uiSref: "main.Pe078V", name: gettextCatalog.getString('View PaySlip'), iconCls: "fa fa-dashboard" },
                       { uiSref: "main.SimFee", name: gettextCatalog.getString('Fees Split'), iconCls: "fa fa-dashboard" },
                         { uiSref: "main.Fin123", name: gettextCatalog.getString('SL Query'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Fin054", name: gettextCatalog.getString('Document User'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Per78B", name: gettextCatalog.getString('Update Delete Paysheet'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Per326", name: gettextCatalog.getString('Update Payroll Status'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.InvLPO", name: gettextCatalog.getString('Local Purchase Order'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Inv006", name: gettextCatalog.getString('Supplier Master'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Inv042", name: gettextCatalog.getString('Bank'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Inv052", name: gettextCatalog.getString('Section'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Inv062", name: gettextCatalog.getString('Location'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Inv064", name: gettextCatalog.getString('Trade Term'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Sim201", name: gettextCatalog.getString('Student Device'), iconCls: "fa fa-dashboard" },
                      { uiSref: "main.Email", name: gettextCatalog.getString('Email'), iconCls: "fa fa-dashboard" },
                         { uiSref: "main.Sim092", name: gettextCatalog.getString('PortalReference'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.sim139", name: gettextCatalog.getString('ReportCardAllocation'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Sim43a", name: gettextCatalog.getString('Fee Refund'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim143", name: gettextCatalog.getString('Co-Scolastic'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim072", name: gettextCatalog.getString('Concession'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Comn050", name: gettextCatalog.getString('Alert Transaction'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Fin155", name: gettextCatalog.getString('Fee Posting'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Fin021", name: gettextCatalog.getString('Asset Master'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.Fin006", name: gettextCatalog.getString('Asset Literal'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Fin055", name: gettextCatalog.getString('Document master'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.per059", name: gettextCatalog.getString('Company master'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Fin069", name: gettextCatalog.getString('LedgerControl'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Fin016", name: gettextCatalog.getString('Subledger Master'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Fin013", name: gettextCatalog.getString('Create/Edit GL Period Master'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Fin012", name: gettextCatalog.getString('Create/Edit Financial Period'), iconCls: "fa fa-dashboard" },

                 { uiSref: "main.Per300", name: gettextCatalog.getString('Contract'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Fin151", name: gettextCatalog.getString('Contract'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Fin001", name: gettextCatalog.getString('Contract'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Per060", name: gettextCatalog.getString('Contract'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Per260", name: gettextCatalog.getString('Employee Leave Assign'), iconCls: "fa fa-dashboard" },

                 { uiSref: "main.Sim518", name: gettextCatalog.getString('Attendance Rule'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Per308", name: gettextCatalog.getString('Active User'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Per104", name: gettextCatalog.getString('Employee Shift'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Com004", name: gettextCatalog.getString('Common Sequence'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim165", name: gettextCatalog.getString('Moderator User'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Sim164", name: gettextCatalog.getString('Moderator'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Sim186", name: gettextCatalog.getString('Head Teacher Mapping'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Sim187", name: gettextCatalog.getString('Supervisor Mapping'), iconCls: "fa fa-dashboard" },

                  { uiSref: "main.Sim505", name: gettextCatalog.getString('Certificate TC Parameter'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Sim681", name: gettextCatalog.getString('Certificate TC Parameter'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Sim684", name: gettextCatalog.getString('TC Approve'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Sim108", name: gettextCatalog.getString('Request Certificate'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Sim012", name: gettextCatalog.getString('admission Criteria'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Sim038", name: gettextCatalog.getString('Section Screening'), iconCls: "fa fa-dashboard" },

                  { uiSref: "main.Sim025", name: gettextCatalog.getString('Calendar Exception'), iconCls: "fa fa-dashboard" },

                  { uiSref: "main.Sim156", name: gettextCatalog.getString('Attendance Code'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Sim500", name: gettextCatalog.getString('class Wise Attendance'), iconCls: "fa fa-dashboard" },

                 { uiSref: "main.Sim158", name: gettextCatalog.getString('Homeroom/Homeroom Batch'), iconCls: "fa fa-dashboard" },


                { uiSref: "main.Sim161", name: gettextCatalog.getString('Homeroom Teacher'), iconCls: "fa fa-dashboard" },

                  { uiSref: "main.Sim160", name: gettextCatalog.getString('Homeroom Student'), iconCls: "fa fa-dashboard" },

            { uiSref: "main.Sim040", name: gettextCatalog.getString('Create Edit Sibling'), iconCls: "fa fa-dashboard" },
              { uiSref: "main.Fin102", name: gettextCatalog.getString('PDC Submission'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Fin201", name: gettextCatalog.getString('UpdateVoucher'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim155", name: gettextCatalog.getString('Student Attendance'), iconCls: "fa fa-dashboard" },

                 { uiSref: "main.TimeLine", name: gettextCatalog.getString('TimeLine'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.TimeLineAlert", name: gettextCatalog.getString('TimeLineAlert'), iconCls: "fa fa-dashboard" },
                      { uiSref: "main.Inv100", name: gettextCatalog.getString('Bin'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Inv126", name: gettextCatalog.getString('SalesType'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Inv128", name: gettextCatalog.getString('SaleTypeMarkUp'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Inv046", name: gettextCatalog.getString('AdjustmentReason'), iconCls: "fa fa-dashboard" },

                  { uiSref: "main.Sim060", name: gettextCatalog.getString('Bell Section Subject Teacher'), iconCls: "fa fa-dashboard" },

                  { uiSref: "main.Sim174", name: gettextCatalog.getString('Student Attendance Monthly'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim173", name: gettextCatalog.getString('Employee Teacher Creation'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Comn005", name: gettextCatalog.getString('Create Edit User'), iconCls: "fa fa-dashboard" },

                 { uiSref: "main.Sim502", name: gettextCatalog.getString('Define Student Fee'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim178", name: gettextCatalog.getString('Copy Section Fee'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Sim044", name: gettextCatalog.getString('Update Student Section'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim022", name: gettextCatalog.getString('Fee Type'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim021", name: gettextCatalog.getString('Fee Category'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim045", name: gettextCatalog.getString('Subject Master'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim046", name: gettextCatalog.getString('Subject Group'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim031", name: gettextCatalog.getString('Parameter Master'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim003", name: gettextCatalog.getString('Curriculum'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim001", name: gettextCatalog.getString('Academic Year'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.SimSht", name: gettextCatalog.getString('Admission Short'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim024", name: gettextCatalog.getString('Grade'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Sim008", name: gettextCatalog.getString('Nationality'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim009", name: gettextCatalog.getString('User Role'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim036", name: gettextCatalog.getString('Section Fee'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim017", name: gettextCatalog.getString('Student Fee Concession'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim018", name: gettextCatalog.getString('Student Fee Concession Transaction'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim035", name: gettextCatalog.getString('Section'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Sim004", name: gettextCatalog.getString('Grade Level'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim005", name: gettextCatalog.getString('Curriculum Level'), iconCls: "fa fa-dashboard" },

                { uiSref: "main.Sim059", name: gettextCatalog.getString('Section Term'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Com003", name: gettextCatalog.getString('Module Master'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Com001", name: gettextCatalog.getString('Common Application'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.RoleApplication", name: gettextCatalog.getString('Role Application'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.Sims044", name: gettextCatalog.getString('Update Student Section'), iconCls: "fa fa-dashboard" },
                      { uiSref: "main.ExRefM", name: gettextCatalog.getString('Exam Reference Material'), iconCls: "fa fa-dashboard" },

                     { uiSref: "main.Sim089", name: gettextCatalog.getString('Student Section Subject'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Mog001", name: gettextCatalog.getString('School Licence'), iconCls: "fa fa-dashboard" },

                { uiSref: "main.EmpSht", name: gettextCatalog.getString('Create Edit Employee'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Per099", name: gettextCatalog.getString('Employee Master'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim042", name: gettextCatalog.getString('Student Database'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim680", name: gettextCatalog.getString('Student Database'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim043", name: gettextCatalog.getString('Studentt Fee'), iconCls: "fa fa-dashboard" },

                { uiSref: "main.Sim039", name: gettextCatalog.getString('Section Subject'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim179", name: gettextCatalog.getString('Class Teacher Mapping'), iconCls: "fa fa-dashboard" },

                 { uiSref: "main.Sim015", name: gettextCatalog.getString('School Calender'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim540", name: gettextCatalog.getString('Staff Calender'), iconCls: "fa fa-dashboard" },

                 { uiSref: "main.Per072", name: gettextCatalog.getString('Payroll Employee'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Per258", name: gettextCatalog.getString('Qualification Employee'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Per234", name: gettextCatalog.getString('Designation'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Per229", name: gettextCatalog.getString('Destination'), iconCls: "fa fa-dashboard" },

                 { uiSref: "main.Sim510", name: gettextCatalog.getString('SearchUser'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim533", name: gettextCatalog.getString('FeeTerm'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim168", name: gettextCatalog.getString('HouseAllocation'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim029", name: gettextCatalog.getString('House'), iconCls: "fa fa-dashboard" },
                 //{ uiSref: "main.Sim086", name: gettextCatalog.getString('TransportVehicleDetails'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim065", name: gettextCatalog.getString('BellDetails'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim064", name: gettextCatalog.getString('BellDay'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim063", name: gettextCatalog.getString('BellSlotRoom'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim062", name: gettextCatalog.getString('BellSlotGroup'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim061", name: gettextCatalog.getString('Bell'), iconCls: "fa fa-dashboard" },

                 { uiSref: "main.Sim083", name: gettextCatalog.getString('TransportRoute'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim084", name: gettextCatalog.getString('TransportStop'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim085", name: gettextCatalog.getString('TransportRouteStop'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim086", name: gettextCatalog.getString('TransportVehicle'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim087", name: gettextCatalog.getString('TransportCaretaker'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim088", name: gettextCatalog.getString('TransportDriver'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim081", name: gettextCatalog.getString('TransportRouteStudent'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim079", name: gettextCatalog.getString('TransportFees'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim519", name: gettextCatalog.getString('ApproveStudentTransport'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim525", name: gettextCatalog.getString('TransportCancelUpdate'), iconCls: "fa fa-dashboard" },

                 { uiSref: "main.Sim162", name: gettextCatalog.getString('Building'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim163", name: gettextCatalog.getString('Location'), iconCls: "fa fa-dashboard" },

                 { uiSref: "main.Per058", name: gettextCatalog.getString('PayCode'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Per244", name: gettextCatalog.getString('CommonCode'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Per109", name: gettextCatalog.getString('PaysGradeChange'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Per109", name: gettextCatalog.getString('PaysAttendanceCode'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Per137", name: gettextCatalog.getString('OvertimeHour'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Per138", name: gettextCatalog.getString('OvertimeRate'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Per135", name: gettextCatalog.getString('ManagerConfirmation'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Per132", name: gettextCatalog.getString('LoanConfirmation'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Per134", name: gettextCatalog.getString('LoanRegister'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Per131", name: gettextCatalog.getString('LoanCode'), iconCls: "fa fa-dashboard" },
                 //**************************Common*****************************
                 { uiSref: "main.Com012", name: gettextCatalog.getString('Exceptions View'), iconCls: "fa fa-dashboard" },

                 { uiSref: "main.Per326", name: gettextCatalog.getString('Employee Attendance Daily'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Per322", name: gettextCatalog.getString('Attendance Machine'), iconCls: "fa fa-dashboard" },

                 { uiSref: "main.Per130", name: gettextCatalog.getString('LoanAmount'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Fin108", name: gettextCatalog.getString('PaymentTerm'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Fin118", name: gettextCatalog.getString('Schedule'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Fin143", name: gettextCatalog.getString('SubledgerMatching'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim007", name: gettextCatalog.getString('Language'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim006", name: gettextCatalog.getString('Ethnicity'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim002", name: gettextCatalog.getString('Country'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim057", name: gettextCatalog.getString('Region'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim055", name: gettextCatalog.getString('State'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim056", name: gettextCatalog.getString('CityMaster'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sim182", name: gettextCatalog.getString('CouncilMaster'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Sim189", name: gettextCatalog.getString('CouncilStudentMapping'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.EmpDshBrd", name: gettextCatalog.getString('Employee Dashboard'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Per235", name: gettextCatalog.getString('Pays Parameter'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim099", name: gettextCatalog.getString('Medical Student Susceptibility'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Fin149", name: gettextCatalog.getString('Finn Parameter'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Fin149", name: gettextCatalog.getString('Finn Parameter'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Fin149", name: gettextCatalog.getString('Finn Parameter'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.CostCentre", name: gettextCatalog.getString('Cost Centre'), iconCls: "fa fa-dashboard" },

                 { uiSref: "main.sim600", name: gettextCatalog.getString('Agenda Approval'), iconCls: "fa fa-dashboard" },

                  { uiSref: "main.PP003", name: gettextCatalog.getString('GradeBook'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Sim539", name: gettextCatalog.getString('Agenda View'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim195", name: gettextCatalog.getString('Agenda Configuration'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.Sims020", name: gettextCatalog.getString('EmployeeDocumentMaster'), iconCls: "fa fa-dashboard" },

                { uiSref: "main.Sim019", name: gettextCatalog.getString('Document Details'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.UpdateEmployee", name: gettextCatalog.getString('Update Employee'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Ucw240", name: gettextCatalog.getString('Achievemnet'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Ucw242", name: gettextCatalog.getString('Goal Target'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Ucw243", name: gettextCatalog.getString('Goal Target KPI'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Ucw244", name: gettextCatalog.getString('Goal TargetKPI Measure'), iconCls: "fa fa-dashboard" },
                 //{ uiSref: "main.Sim086", name: gettextCatalog.getString('TransportVehicleDetails'), iconCls: "fa fa-dashboard" },

                  /////
                { uiSref: "main.Sim149", name: gettextCatalog.getString('Incidence'), iconCls: "fa fa-rocket" },
                { uiSref: "main.Sim150", name: gettextCatalog.getString('Incidence Action'), iconCls: "fa fa-rocket" },
                { uiSref: "main.Sim151", name: gettextCatalog.getString('Action Category'), iconCls: "fa fa-rocket" },
                { uiSref: "main.Sim152", name: gettextCatalog.getString('Consequence'), iconCls: "fa fa-rocket" },
                { uiSref: "main.Sim153", name: gettextCatalog.getString('Timeline'), iconCls: "fa fa-rocket" },
                { uiSref: "main.Sim154", name: gettextCatalog.getString('Warning'), iconCls: "fa fa-rocket" },
                  { uiSref: "main.Email", name: gettextCatalog.getString('Email'), iconCls: "fa fa-dashboard" },
                  //////////
                 { uiSref: "main.Per145", name: gettextCatalog.getString('Shift Template'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Per092", name: gettextCatalog.getString('Shift Master'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.PerAF", name: gettextCatalog.getString('Air Fare'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.PerAFE", name: gettextCatalog.getString('Air Fare For Employee'), iconCls: "fa fa-dashboard" },

                { uiSref: "main.Per309", name: gettextCatalog.getString('InActive User'), iconCls: "fa fa-dashboard" },

                { uiSref: "main.Sim526", name: gettextCatalog.getString('UploadDocument'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim183", name: gettextCatalog.getString('Board'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Pers116", name: gettextCatalog.getString('LeaveType'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Pers116", name: gettextCatalog.getString('EmployeeImageView'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim185", name: gettextCatalog.getString('StudentBoardExam'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim184", name: gettextCatalog.getString('BoardExam'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim175", name: gettextCatalog.getString('StudentImageView'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Pers021", name: gettextCatalog.getString('EmployeeGrade'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Per254", name: gettextCatalog.getString('DefineCategory'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Pers061", name: gettextCatalog.getString('OtherLoan'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Pers090", name: gettextCatalog.getString('SettlementType'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Pers312", name: gettextCatalog.getString('EmployeeDocumentUpload'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Fin116", name: gettextCatalog.getString('ScheduleGroupDetails'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Fin117", name: gettextCatalog.getString('ScheduleGroup'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Fin010", name: gettextCatalog.getString('CreateEditDepartment'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim508", name: gettextCatalog.getString('TeacherAssignmentUpload'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim509", name: gettextCatalog.getString('TeacherAssignmentView'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim685", name: gettextCatalog.getString('AddExamPaper'), iconCls: "fa fa-dashboard" },

                //LIBRARY

                { uiSref: "main.Sim115", name: gettextCatalog.getString('Library Attribute'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim117", name: gettextCatalog.getString('Library Catalogue Attribute'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim119", name: gettextCatalog.getString('Library Catalogue'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim118", name: gettextCatalog.getString('Library Category'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim120", name: gettextCatalog.getString('Library Fee'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim131", name: gettextCatalog.getString('Library Subcategory'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim116", name: gettextCatalog.getString('Library Bin'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim121", name: gettextCatalog.getString('Library Item master'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim122", name: gettextCatalog.getString('Library Item Style'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim130", name: gettextCatalog.getString('Library Status'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim134", name: gettextCatalog.getString('Library Subscription Type'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim137", name: gettextCatalog.getString('Library Transaction Type'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim128", name: gettextCatalog.getString('Library Item Request'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim129", name: gettextCatalog.getString('Library Request Details'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim126", name: gettextCatalog.getString('LibraryMembershipTypeCont'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim127", name: gettextCatalog.getString('Library Privilege'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim123", name: gettextCatalog.getString('Library Location'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim503", name: gettextCatalog.getString('Renew Library'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim503", name: gettextCatalog.getString('Library Transaction Detail'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim671", name: gettextCatalog.getString('Library Employee Membership'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim682", name: gettextCatalog.getString('Library Item Search '), iconCls: "fa fa-dashboard" },

                { uiSref: "main.Sim155H", name: gettextCatalog.getString('Homeroom Attendance'), iconCls: "fa fa-dashboard" },


                { uiSref: "main.Fin060", name: gettextCatalog.getString('Financal Document'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim515", name: gettextCatalog.getString('Financal Year'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Fin138", name: gettextCatalog.getString('Bank Master'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Fin141", name: gettextCatalog.getString('Bank Payment'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Fin152", name: gettextCatalog.getString('Cash Payment'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.Fin153", name: gettextCatalog.getString('Cash Receipt'), iconCls: "fa fa-dashboard" },
                     { uiSref: "main.Fin145", name: gettextCatalog.getString('Bank Receipt'), iconCls: "fa fa-dashboard" },

                { uiSref: "main.Fin011", name: gettextCatalog.getString('Credit/Edit Division'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Fin205", name: gettextCatalog.getString('User Account'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Fin207", name: gettextCatalog.getString('Fee Posting Payment Mode'), iconCls: "fa fa-dashboard" },

                { uiSref: "main.Sim098", name: gettextCatalog.getString('Student Health'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Per302", name: gettextCatalog.getString('Sip Evaluation Schedule'), iconCls: "fa fa-dashboard" },

                   { uiSref: "main.Per305", name: gettextCatalog.getString('Goal Target KPI Measure Achievement'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Fin206", name: gettextCatalog.getString('Document User Account'), iconCls: "fa fa-rocket" },
                { uiSref: "main.Fin043", name: gettextCatalog.getString('Currency Master'), iconCls: "fa fa-rocket" },
                { uiSref: "main.Fin003", name: gettextCatalog.getString('Company Master'), iconCls: "fa fa-rocket" },

                { uiSref: "main.Inv001", name: gettextCatalog.getString('Unit Of Measurement'), iconCls: "fa fa-rocket" },
                 { uiSref: "main.Inv002", name: gettextCatalog.getString('UOM Conversions'), iconCls: "fa fa-rocket" },
                  { uiSref: "main.Invs009", name: gettextCatalog.getString('Supplier Description Types'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Invs010", name: gettextCatalog.getString('Supplier Description'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Inv011", name: gettextCatalog.getString('Supplier Groups'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Inv118", name: gettextCatalog.getString('Departments'), iconCls: "fa fa-dashboard" },
                //////////Ganesh

                 { uiSref: "main.Per313", name: gettextCatalog.getString('Employee Attendance'), iconCls: "fa fa-dashboard" },

                  { uiSref: "main.Sim563", name: gettextCatalog.getString('Admission Fee'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Sim521", name: gettextCatalog.getString('Admission Fees'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Sim565", name: gettextCatalog.getString('Prospect Fees'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Sim105", name: gettextCatalog.getString('Issue Certificate'), iconCls: "fa fa-dashboard" },

                  { uiSref: "main.UserMenu", name: gettextCatalog.getString('Menu'), iconCls: "fa fa-dashboard" },

                  //Inventory
                   { uiSref: "main.Inv021", name: gettextCatalog.getString('ItemMaster'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Inv143", name: gettextCatalog.getString('InterestType'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Inv027", name: gettextCatalog.getString('PurchaseExpensesType'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Inv134", name: gettextCatalog.getString('CustomExpenses'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Inv133", name: gettextCatalog.getString('CustomClearance'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Inv132", name: gettextCatalog.getString('CreateCostingSheet'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Invs005", name: gettextCatalog.getString('Delivery Reasons'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Inv124", name: gettextCatalog.getString('INVS Parameter'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Invs063", name: gettextCatalog.getString('Product Code'), iconCls: "fa fa-dashboard" },

                    { uiSref: "main.Inv044", name: gettextCatalog.getString('Create Order'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.Inv058", name: gettextCatalog.getString('Request Details'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.Inv131", name: gettextCatalog.getString('ROL Settings'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.Inv018", name: gettextCatalog.getString('Create Company'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.Inv020", name: gettextCatalog.getString('Company Parameter'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.Inv071", name: gettextCatalog.getString('Document Types'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.communication", name: gettextCatalog.getString('Communication'), iconCls: "fa fa-dashboard" },
                     { uiSref: "main.Inv065", name: gettextCatalog.getString('Item Assembly'), iconCls: "fa fa-dashboard" },

                 //Security
                  { uiSref: "main.Com008", name: gettextCatalog.getString('User Group Master'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.Com009", name: gettextCatalog.getString('User Role'), iconCls: "fa fa-dashboard" },
                  { uiSref: "main.sim543", name: gettextCatalog.getString('Report Card Comment'), iconCls: "fa fa-dashboard" },
                   { uiSref: "main.Fin218", name: gettextCatalog.getString('AssetAllocation'), iconCls: "fa fa-dashboard" },
                     { uiSref: "main.Fin220", name: gettextCatalog.getString('Gl Cost Centre Mapping'), iconCls: "fa fa-dashboard" },
                     { uiSref: "main.LateFeeRules", name: gettextCatalog.getString('Late Fee Rules'), iconCls: "fa fa-dashboard" },
                     { uiSref: "main.StudentLateFee", name: gettextCatalog.getString('Student Late Fee Mapping'), iconCls: "fa fa-dashboard" },
                     { uiSref: "main.LateFeeFormula", name: gettextCatalog.getString('Late Fee Formula'), iconCls: "fa fa-dashboard" },

                     //My Change
                    { uiSref: "main.Sim176", name: gettextCatalog.getString('Student Report'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.sim215", name: gettextCatalog.getString('Manual Fee Posting'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.Inv047", name: gettextCatalog.getString('Stock Adjustment'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.Sim561", name: gettextCatalog.getString('Student Document List'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.Sim668", name: gettextCatalog.getString('Tc Certificate Requiest'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.SimIEM", name: gettextCatalog.getString('Email Invoice'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.SimSEL", name: gettextCatalog.getString('Absent Students Notification'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.Sim669", name: gettextCatalog.getString('Suppiler Location'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.Per319", name: gettextCatalog.getString('Vacancy Master'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.Per331", name: gettextCatalog.getString('Vacancy View'), iconCls: "fa fa-dashboard" },
                    { uiSref: "main.Sim101", name: gettextCatalog.getString('Medical Visit'), iconCls: "fa fa-dashboard" },
                    //{ uiSref: "main.Fin226", name: gettextCatalog.getString('Print Vouchers'), iconCls: "fa fa-dashboard" },
                     { uiSref: "main.Per335", name: gettextCatalog.getString('Employee Report Oes'), iconCls: "fa fa-dashboard" },
                     { uiSref: "main.FINR15", name: gettextCatalog.getString('PrintVoucher'), iconCls: "fa fa-dashboard" },
                     { uiSref: "main.Inv175", name: gettextCatalog.getString('Request Approve'), iconCls: "fa fa-dashboard" },
                     { uiSref: "main.Fin234", name: gettextCatalog.getString('Vehicle Expense'), iconCls: "fa fa-dashboard" },
                   //END

                       { uiSref: "main.Sim613", name: gettextCatalog.getString('Honour'), iconCls: "fa fa-dashboard" },
                               { uiSref: "main.Inv200", name: gettextCatalog.getString('Vehicle Consumption Details'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim614", name: gettextCatalog.getString('UserHonour'), iconCls: "fa fa-dashboard" },
                //Cancel Admission 
                { uiSref: "main.Sim619", name: gettextCatalog.getString('TC To Cancel Admission'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim620", name: gettextCatalog.getString('Cancel Admission From TC'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim622", name: gettextCatalog.getString('Cancel Admission Clear Fees'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim623", name: gettextCatalog.getString('TC With Clear Fees'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim624", name: gettextCatalog.getString('Cancel Admission Approval'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim625", name: gettextCatalog.getString('Cancel Admission To TC'), iconCls: "fa fa-dashboard" },
                { uiSref: "main.Sim522", name: gettextCatalog.getString('TC From Cancel Admission'), iconCls: "fa fa-dashboard" },
                 { uiSref: "main.522", name: gettextCatalog.getString('TC From Cancel Admission'), iconCls: "fa fa-dashboard" },
                  //Re-Admission 
                  { uiSref: "main.Sim670", name: gettextCatalog.getString('Re-Admission'), iconCls: "fa fa-dashboard" },
            ];



            /* User Notifications */

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": true,
                "progressBar": true,
                "positionClass": "toast-top-right",
                "preventDuplicates": true,
                "onclick": null,
                "showDuration": "10",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            $scope.temp_notifications = [];
            $scope.notifications = [];


            //var countUp = function () {
            //    $http.get(ENV.apiUrl + "api/common/GetUserNotifications?user_code=" + $rootScope.globals.currentUser.username).then(function (notifications) {
            //        if ($scope.temp_notifications.length == 0) {
            //            $scope.notifications = notifications.data;
            //            $scope.temp_notifications = $scope.notifications;
            //        }
            //        else {
            //            $scope.result = notifications.data.filter(function (item1) {
            //                for (var i in $scope.temp_notifications) {
            //                    if (item1.common_notification_subject === $scope.temp_notifications[i].common_notification_subject &&
            //                        item1.common_notification_type === $scope.temp_notifications[i].common_notification_type)
            //                    { return false; }
            //                };
            //                return true;
            //            });
            //            //console.log($scope.result.length,'Result');
            //            //console.log(notifications.data.length, 'Notifications');
            //            for (var i = 0; i < $scope.result.length; i++) {
            //                var x = $scope.result[i].common_notification_subject + ' Date :' + $scope.result[i].common_notification_date;
            //                toastr["info"]($scope.result[i].common_notification_subject, $scope.result[i].common_notification_type);
            //                $scope.temp_notifications.push($scope.result[i]);
            //            }
            //        }
            //        $timeout(countUp, 5000);
            //    });
            //}
            //$timeout(countUp, 6000);


            //$http.get(ENV.apiUrl + "api/common/GetUserNotifications?user_code=" + $rootScope.globals.currentUser.username).then(function (notifications) {
            //    if ($scope.temp_notifications.length == 0) {
            //        $scope.notifications = notifications.data;
            //        $scope.temp_notifications = $scope.notifications;
            //    }
            //    for (var i = 0; i < $scope.temp_notifications.length; i++) {

            //        var x = $scope.temp_notifications[i].common_notification_subject + ' Date :' + $scope.temp_notifications[i].common_notification_date;
            //        if ($scope.temp_notifications[i].common_notification_type == 'ALERT')
            //            toastr["warning"](x, $scope.temp_notifications[i].common_notification_type);
            //        else
            //            toastr["info"](x, $scope.temp_notifications[i].common_notification_type);
            //    }
            //});

            //$state.go("main.FeeType");

            $timeout(function () {
                $("#main-menu-wrapper").scrollbar();
            }, 100);

        }])

      .config(function (IdleProvider, KeepaliveProvider) {
          // configure Idle settings
          IdleProvider.idle(1800); // in seconds
          IdleProvider.timeout(10); // in seconds
          KeepaliveProvider.interval(2); // in seconds
      })

        .config(['TitleProvider', function (TitleProvider) {
            TitleProvider.enabled(false); // it is enabled by default
        }])

    .run(function (Idle) {
        // start watching when the app runs. also starts the Keepalive service by default.
        Idle.watch();
    });



    //simsController.filter('sumByKey', function () {
    //    return function (data, key) {
    //        if (typeof (data) === 'undefined' || typeof (key) === 'undefined') {
    //            return 0;
    //        }

    //        var sum = 0;
    //        for (var i = data.length - 1; i >= 0; i--) {
    //            sum += parseInt(data[i][key]);
    //        }

    //        return sum;
    //    };
    //});

})();