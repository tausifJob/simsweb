﻿(function () {
    'use strict';
    var obj1, temp, opr, comp, edt;
    var main, deletefin = [];
    var deletecode = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TcToCancelAdmCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.search = true;
            var dataforSave = [];
            $scope.reset = true;
            $scope.display = true;
            $scope.table = true;
            $scope.tcdetails = true;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.divcode_readonly = true;
            $scope.temp = [];

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100)

            $timeout(function () {
                $("#fixTable2").tableHeadFixer({ 'top': 1 });
            }, 100)
            var user = $rootScope.globals.currentUser.username;

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            var dt = new Date();
            $scope.temp.tC_Date = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();

            $(function () {
                $('#cmb_Subjects').multipleSelect({
                    width: '100%'
                });
            });

            //Navigated from Cancel Admission 
            //debugger;
            //console.log($stateParams.edt);
            //var cadmin = $stateParams.edt;
            //var opr = 'P';
            //$http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Student_details?enroll_No=" + cadmin.enroll + "&opr=" + opr).then(function (certificate) {

            //  $scope.savedata = certificate.data;
            //    $scope.temp = $scope.savedata[0];
            //    var dt = new Date();
            //    $scope.temp['tC_Date'] = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
            //});

            //$http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Stud_subject?enroll_No=" + cadmin.enroll).then(function (subject) {
            //    $scope.subject_studied = subject.data;
            //    console.log($scope.subject_studied);
            //    setTimeout(function () {
            //        $('#cmb_Subjects').change(function () {
            //            console.log($(this).val());
            //        }).multipleSelect({
            //            width: '100%'
            //        });
            //    }, 1000);
            //});

            //Select Data SHOW (Combo Box)

            $http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_certificate_no").then(function (certificate) {
                $scope.certificate_data = certificate.data;
                $scope.edt = {
                    sims_certificate_number: $scope.certificate_data[0].sims_certificate_number
                };
            });

            $http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Stream").then(function (get_stream) {
                $scope.stream_data = get_stream.data;
                $scope.edts = {
                    stream_code: $scope.stream_data[0].stream_code
                };
            });

            $scope.reset = function () {

                //$scope.edt = "";
                //$scope.edts = "";
                $scope.Update_btn = false;
                $scope.save_btn = true;
                var dt = new Date();
                $scope.temp['tC_Date'] = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
                //$scope.edt.sims_certificate_number = "";
                $scope.temp.sims_certificate_enroll_number = "";
                $scope.temp.stud_name = "";
                $scope.temp.nationality = "";
                $scope.temp.father_name = "";
                $scope.temp.religion = "";
                $scope.temp.date_of_birth = "";
                $scope.temp.date_of_admission = "";
                $scope.temp.dobw = "";
                $scope.temp.present_class = "";
                $scope.temp.present_sec = "";
                $scope.temp.admitted_class = "";
                $scope.temp.result_end_acad_yr = "";
                $scope.temp.promoted_class = "";
                $scope.temp.promoted_acad_yr = "";
                $scope.temp.admitted_class = "";
                $scope.temp.sims_certificate_subject_studied = [];
                $scope.temp.subject_studied = [];
                $scope.temp.present_days = "";
                $scope.temp.total_days = "";
                $scope.temp.credits = "";
                $scope.temp.sims_certificate_general_conduct = "";
                $scope.temp.sims_certificate_remark = "";
                $scope.temp.registrar = "";
                $scope.temp.principle = "";
                $scope.temp.sims_certificate_registration_register_no = "";
                $scope.temp.sims_certificate_registration_serial_no = "";
                $scope.temp.sims_certificate_date_of_leaving = "";
                $scope.temp.sims_certificate_reason_of_leaving = "";
                $scope.temp.sims_certificate_academic_progress = "";
                $scope.temp.sims_certificate_fee_paid = "";
                $scope.temp.sims_certificate_sc_st_status = "";
                setTimeout(function () {
                    debugger;
                    $('#cmb_Subjects').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            }

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;
            });

            $scope.stud_details = function () {

                $scope.Update_btn = false;
                $scope.save_btn = true;

                if ($scope.temp.sims_certificate_enroll_number == null || $scope.temp.sims_certificate_enroll_number == "") {

                    $scope.searchtable = false;
                    $('#myModal2').modal('show');

                }
                else {
                    var opr = 'P';
                    //$http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Student_details?enroll_no=" + $scope.temp.sims_certificate_enroll_number + "&opr=" + opr).then(function (Student_Details) {
                    //    $scope.msg = Student_Details.data;                       
                    //        $scope.savedata = Student_Details.data;
                    //        $scope.temp = $scope.savedata[0];
                    //        console.log($scope.temp);                        
                    //});

                    $http.post(ENV.apiUrl + "api/CertificateTCParameter/Check_Tc_details?userid=" + $scope.temp.sims_certificate_enroll_number).then(function (check) {

                        $scope.msg = check.data;
                        //$scope.msg1 = $scope.msg;
                        if ($scope.msg == true) {
                            swal('', 'TC Already Issued for this Student');
                        }
                        else {

                            $http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Student_details?enroll_No=" + $scope.temp.sims_certificate_enroll_number + "&opr=" + opr).then(function (certificate) {

                                $scope.savedata = certificate.data;
                                console.log($scope.savedata);
                                $scope.temp = $scope.savedata[0];
                                var dt = new Date();
                                $scope.temp['tC_Date'] = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
                            });

                            //$scope.temp.sims_certificate_enroll_number = info.s_enroll_no;
                            $http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Stud_subject?enroll_No=" + $scope.temp.sims_certificate_enroll_number).then(function (subject) {
                                $scope.subject_studied = subject.data;
                                console.log($scope.subject_studied);
                                setTimeout(function () {
                                    debugger;
                                    $('#cmb_Subjects').change(function () {
                                        console.log($(this).val());
                                    }).multipleSelect({
                                        width: '100%'
                                    });
                                }, 1000);
                            });

                        }
                        //flag = false;

                    });


                }
            }

            $scope.searchstudent = function () {

                $scope.busy = true;
                $scope.searchtable = false;

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.edt)).then(function (Allstudent) {

                    $scope.student = Allstudent.data;

                    $scope.busy = false;

                    $scope.searchtable = true;
                });
            }

            $scope.edit = function (info) {

                $http.post(ENV.apiUrl + "api/CertificateTCParameter/Check_Tc_details?userid=" + info.s_enroll_no).then(function (certi) {

                    $scope.msg = certi.data;
                    //$scope.msg1 = $scope.msg;
                    if ($scope.msg == true) {
                        swal('', 'TC Already Issued for this Student');
                    }
                    else {
                        var opr = 'P';
                        $http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Student_details?enroll_No=" + info.s_enroll_no + "&opr=" + opr).then(function (certificate) {
                            debugger
                            $scope.savedata = certificate.data;
                            console.log($scope.savedata);
                            $scope.temp = $scope.savedata[0];
                            var dt = new Date();
                            $scope.temp['tC_Date'] = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();

                        });

                        $scope.temp.sims_certificate_enroll_number = info.s_enroll_no;
                        $http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Stud_subject?enroll_No=" + $scope.temp.sims_certificate_enroll_number).then(function (subject1) {

                            $scope.subject_studied = subject1.data;
                            console.log($scope.subject_studied);
                            setTimeout(function () {
                                $('#cmb_Subjects').change(function () {
                                    console.log($(this).val());
                                }).multipleSelect({
                                    width: '100%'
                                });
                            }, 1000);
                        });

                    }
                    //flag = false;
                });

                $scope.stud_details();

            }

            $scope.getsectioncode = function (cur, year, grade) {

                $http.get(ENV.apiUrl + "api/common/SectionTerm/getAllSections?cur_code=" + cur + "&ac_year=" + year + "&g_code=" + grade).then(function (res1) {
                    $scope.sections = res1.data;
                });
            }

            //Ceritficate Search Submit Button(Modal 1)
            $scope.submit = function () {

                for (var j = 0; j < $scope.filteredTodos.length; j++) {
                    var v = document.getElementById($scope.filteredTodos[j].sims_certificate_number + j);
                    if (v.checked == true) {
                        $scope.savedata = $scope.filteredTodos[j];
                        $scope.temp = $scope.savedata;
                    }
                }


                $http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Stud_subject?enroll_No=" + $scope.temp.sims_certificate_enroll_number).then(function (subj) {
                    debugger
                    $scope.subject_studied = subj.data;
                    console.log($scope.subject_studied);
                    setTimeout(function () {
                        debugger;
                        $('#cmb_Subjects').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);


                    var add = [];

                    setTimeout(function () {
                        //add.push('08')

                        $scope.test = $scope.savedata.sims_subject_code.substring(0, $scope.savedata.sims_subject_code.length - 1);
                        $scope.cls = $scope.test.split(",");
                        for (var i = 0; i < $scope.cls.length; i++) {
                            add.push($scope.cls[i])

                        }
                        try {
                            $("#cmb_Subjects").multipleSelect("setSelects", add);
                        }
                        catch (e) {

                        }
                    }, 1000);


                });
                $('#myModal1').modal('hide');
                $scope.Update_btn = true;
                $scope.save_btn = false;

            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.display = true;
                $scope.table = true;
            }

            $scope.tcdetails = function () {

                $scope.certificate = true;
                $('#myModal1').modal('show');

                $http.get(ENV.apiUrl + "api/CertificateTCParameter/Get_Search_Tc_details").then(function (res1) {

                    $scope.CreDiv = res1.data;
                    $scope.totalItems = $scope.CreDiv.length;
                    $scope.todos = $scope.CreDiv;
                    $scope.makeTodos();
                    console.log(res1.data);
                });

                $scope.size = function (str) {
                    console.log(str);
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                }

                $scope.index = function (str) {
                    $scope.pageindex = str;
                    $scope.currentPage = str;
                    console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                    main.checked = false;
                    $scope.CheckAllChecked();
                }

                $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

                $scope.makeTodos = function () {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }

                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                };

                $scope.searched = function (valLists, toSearch) {
                    return _.filter(valLists,

                    function (i) {
                        /* Search Text in all  fields */
                        return searchUtil(i, toSearch);
                    });
                };

                //Search
                $scope.search = function () {
                    $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                    $scope.totalItems = $scope.todos.length;
                    $scope.currentPage = '1';
                    if ($scope.searchText == '') {
                        $scope.todos = $scope.CreDiv;
                    }
                    $scope.makeTodos();
                }

                function searchUtil(item, toSearch) {
                    /* Search Text in all 3 fields */
                    return (item.stud_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                            item.sims_certificate_enroll_number == toSearch) ? true : false;
                }

            }

            //DATA SAVE INSERT
            var datasend = [];
            var subject_code = [];
            $scope.SaveData = function () {
                debugger

                // if (Myform) {                
                var subject = $scope.temp.sims_certificate_subject_studied;
                subject_code = subject_code + ',' + subject;
                var str2 = subject_code.substr(subject_code.indexOf(',') + 1);
                var data = {
                    'opr': 'I',
                    'sims_certificate_number': $scope.edt.sims_certificate_number,
                    'sims_certificate_enroll_number': $scope.temp.sims_certificate_enroll_number,
                    'sims_certificate_date_of_leaving': $scope.temp.sims_certificate_date_of_leaving,
                    'sims_certificate_reason_of_leaving': $scope.temp.sims_certificate_reason_of_leaving,
                    'sims_certificate_general_conduct': $scope.temp.sims_certificate_general_conduct,
                    'sims_certificate_academic_progress': $scope.temp.sims_certificate_academic_progress,
                    'sims_certificate_subject_studied': str2,
                    'sims_certificate_remark': $scope.temp.sims_certificate_remark,
                    'sims_certificate_attendance_remark': $scope.temp.sims_certificate_attendance_remark,
                    'sims_certificate_qualified_for_promotion': $scope.temp.sims_certificate_qualified_for_promotion,
                    'sims_certificate_date_of_issue': $scope.temp.tC_Date,
                    'sims_certificate_fee_paid': $scope.temp.sims_certificate_fee_paid,
                    'sims_certificate_sc_st_status': $scope.temp.sims_certificate_sc_st_status,
                    'sims_certificate_field1': $scope.temp.sims_certificate_field1,
                    'sims_certificate_field2': $scope.temp.sims_certificate_field2,
                    'sims_certificate_field3': $scope.temp.sims_certificate_field3,
                    'sims_certificate_field4': $scope.temp.sims_certificate_field4,
                    'sims_certificate_field5': $scope.temp.sims_certificate_field5,
                    'sims_certificate_registration_register_no': $scope.temp.sims_certificate_registration_register_no,
                    'sims_certificate_registration_serial_no': $scope.temp.sims_certificate_registration_serial_no,
                    'sims_certificate_result_register_no': $scope.temp.sims_certificate_result_register_no,
                    'sims_certificate_result_serial_no': $scope.temp.sims_certificate_result_serial_no,
                    'stream': $scope.edts.stream_code,
                    'Credits': $scope.temp.credits,
                }
                datasend.push(data);
                $scope.temp.opr = 'I';
                $scope.temp.sims_certificate_subject_studied = str2;
                $scope.temp.sims_certificate_number = $scope.edt.sims_certificate_number;
                $state.go('main.Sim620', { tcsend: $scope.temp });
                //$http.post(ENV.apiUrl + "api/CertificateTCParameter/CUDCertificate_Tc_Parameter", datasend).then(function (msg) {
                //    $scope.msg1 = msg.data;

                //    if ($scope.msg1 == true) {
                //        swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                //        $scope.Update_btn = false;
                //        $scope.save_btn = true;
                //        $scope.currentPage = true;
                //        $scope.reset();
                //    }
                //    else {
                //        swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                //        $scope.Update_btn = false;
                //        $scope.save_btn = true;
                //        $scope.currentPage = true;
                //    }
                //});
                datasend = [];
                $scope.table = true;
                $scope.display = true;

                //}               
            }



            //DATA UPDATE
            var dataforUpdate = [];
            $scope.update = function () {
                debugger
                //if (Myform) {
                //var data = $scope.temp;
                //data.opr = "U";
                //dataforUpdate.push(data);
                var subject = $scope.temp.sims_certificate_subject_studied;
                subject_code = subject_code + ',' + subject;
                var str2 = subject_code.substr(subject_code.indexOf(',') + 1);

                var data = {
                    'opr': 'U',
                    'sims_certificate_number': $scope.edt.sims_certificate_number,
                    'sims_certificate_enroll_number': $scope.temp.sims_certificate_enroll_number,
                    'sims_certificate_date_of_leaving': $scope.temp.sims_certificate_date_of_leaving,
                    'sims_certificate_reason_of_leaving': $scope.temp.sims_certificate_reason_of_leaving,
                    'sims_certificate_general_conduct': $scope.temp.sims_certificate_general_conduct,
                    'sims_certificate_academic_progress': $scope.temp.sims_certificate_academic_progress,
                    'sims_certificate_subject_studied': str2,
                    'sims_certificate_remark': $scope.temp.sims_certificate_remark,
                    'sims_certificate_attendance_remark': $scope.temp.sims_certificate_attendance_remark,
                    'sims_certificate_qualified_for_promotion': $scope.temp.sims_certificate_qualified_for_promotion,
                    // 'sims_certificate_date_of_issue': $scope.temp.tC_Date,
                    'sims_certificate_fee_paid': $scope.temp.sims_certificate_fee_paid,
                    'sims_certificate_sc_st_status': $scope.temp.sims_certificate_sc_st_status,
                    'sims_certificate_field1': $scope.temp.sims_certificate_field1,
                    'sims_certificate_field2': $scope.temp.sims_certificate_field2,
                    'sims_certificate_field3': $scope.temp.sims_certificate_field3,
                    'sims_certificate_field4': $scope.temp.sims_certificate_field4,
                    'sims_certificate_field5': $scope.temp.sims_certificate_field5,
                    'sims_certificate_registration_register_no': $scope.temp.sims_certificate_registration_register_no,
                    'sims_certificate_registration_serial_no': $scope.temp.sims_certificate_registration_serial_no,
                    'sims_certificate_result_register_no': $scope.temp.sims_certificate_result_register_no,
                    'sims_certificate_result_serial_no': $scope.temp.sims_certificate_result_serial_no,
                    'stream': $scope.edts.stream_code,
                    'Credits': $scope.temp.credits,
                }

                dataforUpdate.push(data);
                $http.post(ENV.apiUrl + "api/CertificateTCParameter/CUDCertificate_Tc_Parameter", dataforUpdate).then(function (msg) {

                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Record Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            debugger
                            if (isConfirm) {
                                debugger
                                $scope.Update_btn = false;
                                $scope.save_btn = true;
                                $scope.currentPage = true;
                                $scope.reset();
                            }
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Updated", showCloseButton: true, width: 380, })
                        $scope.Update_btn = false;
                        $scope.save_btn = true;
                        $scope.currentPage = true;
                    }
                });
                dataforUpdate = [];
                $scope.table = true;
                $scope.display = true;
                //}
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_certificate_number + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_certificate_number + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function (info) {
                debugger
                $('tr').removeClass("row_selected");
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_certificate_number + i);
                    if (v.checked == true) {
                        $scope.filteredTodos[i].ischange = true;
                    }
                    else {
                        $scope.filteredTodos[i].ischange = false;
                    }
                }

                $("input[type='radio']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('$index');
                if (main.checked == true) {

                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                    $('tr').removeClass("row_selected");
                }
            }

        }])
})();