﻿(function () {
    'use strict';
    var medical_info;
    var parent_info; var temp, photo_filename;

    var del = [];
    var main;

    var sims_father_image, sims_mother_image, sims_guardian_image, student_image;

    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

    //StudentDatabaseCont

    simsController.controller('ParentProfileCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.Restriction = true;
            $scope.demo = true;
            $scope.table1 = true;
            $scope.hide = true;
            $scope.EmploymentNo = true;
            $scope.editmode = false;
            var formdata = new FormData();
            $scope.read1 = true;
            $scope.read2 = true;
            $scope.read3 = true;
            $scope.read4 = true;
            $scope.read5 = true;
           

            $scope.enrollno = "P10022";

            $scope.parent_info = [];
            $scope.sims_admisison_father_image = '';
            $scope.sims_admisison_mother_image = '';
            $scope.sims_admisison_guardian_image = '';


            var v = document.getElementById('cmb_company_emp');
            //v.disabled = true;
         

            

            $timeout(function () {
                $("#example_wrapper").scrollbar();
                $("#example_wrapper1").scrollbar();
                //$(".scroll-wrapper").css({ 'height': '300px' });
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);


            $scope.studentProfile = function (enrollNo, parentId, Gradecode, SectionCode) {
                debugger;
                $scope.enrollno = "P10022";
                $scope.temp = {
                    enrollNo: enrollNo,
                }
                

                $http.get(ENV.apiUrl + "api/studentdatabase/getParentDetails?enroll_no=" + $scope.enrollno).then(function (ParentDetails) {
                    $scope.Parent_Details = ParentDetails.data;
                    $scope.parent_info = $scope.Parent_Details[0];


                    $scope.getstatefather($scope.parent_info.sims_admission_father_country_code);
                    $scope.getcityname($scope.parent_info.sims_admission_father_state);

                    $scope.getmothercityname($scope.parent_info.sims_admission_mother_state);
                    $scope.getstatemother($scope.parent_info.sims_admission_mother_country_code);

                    $scope.getguardiancityname($scope.parent_info.sims_admission_guardian_state);
                    $scope.getstateguardian($scope.parent_info.sims_admission_guardian_country_code);
                    $scope.isparentschoolemployee($scope.parent_info.sims_parent_is_employment_status);



                    $scope.student_father_image = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.parent_info.sims_admisison_father_image;
                    $scope.student_mother_image = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.parent_info.sims_admisison_mother_image;
                    $scope.student_guardian_image1 = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + $scope.parent_info.sims_admisison_guardian_image;

                });

                $('#StudentProfileModal').modal('show');

            }

            $scope.modal_cancel = function () {
                $('#StudentProfileModal').modal('hide');
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $scope.IsCompanyEmployee = function () {

                var c = document.getElementById('chk_company_emp');
                if (c.checked == true) {
                    var v = document.getElementById('cmb_company_emp');
                    v.disabled = false;
                }
                else {
                    var v = document.getElementById('cmb_company_emp');
                    v.disabled = true;

                    var cmb = document.getElementById('')
                }

            }

            $scope.isparentschoolemployee = function (isparent_school_employee) {
                if (isparent_school_employee) {
                    $scope.hide = false;
                    $scope.EmploymentNo = false
                }
                else {

                    $scope.hide = true;
                    $scope.EmploymentNo = true;
                }
            }

            $scope.createdate = function (exp_date_pass, issue_date_pass, name) {


                var month1 = exp_date_pass.split("/")[0];
                var day1 = exp_date_pass.split("/")[1];
                var year1 = exp_date_pass.split("/")[2];
                var new_exp_date_pass = year1 + "/" + month1 + "/" + day1;

                var year = issue_date_pass.split("/")[0];
                var month = issue_date_pass.split("/")[1];
                var day = issue_date_pass.split("/")[2];
                var new_issue_date_pass = year + "/" + month + "/" + day;

                if (new_exp_date_pass < new_issue_date_pass) {

                    swal({ text: "Please Select Future Date", width: 300, showCloseButton: true });

                    $scope.student_info[name] = '';

                }
                else {


                    $scope.student_info[name] = new_exp_date_pass;

                }
            }

            $scope.createdate1 = function (exp_date_pass, issue_date_pass, name1) {

                debugger;
                var month1 = exp_date_pass.split("/")[0];
                var day1 = exp_date_pass.split("/")[1];
                var year1 = exp_date_pass.split("/")[2];
                var new_exp_date_pass = year1 + "/" + month1 + "/" + day1;

                var year = issue_date_pass.split("/")[0];
                var month = issue_date_pass.split("/")[1];
                var day = issue_date_pass.split("/")[2];
                var new_issue_date_pass = year + "/" + month + "/" + day;

                if (new_exp_date_pass < new_issue_date_pass) {

                    $rootScope.strMessage = "Please Select Future Date";
                    $('#message').modal('show');

                    $scope.parent_info[name1] = '';
                }
                else {

                    $scope.parent_info[name1] = new_exp_date_pass;
                }
            }

            $scope.showdate = function (date, name) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                $scope.student_info[name] = year + "/" + month + "/" + day;


            }

            $scope.showdate1 = function (date, name1) {
                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                $scope.parent_info[name1] = year + "/" + month + "/" + day;

            }

            $scope.showdatemedical = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.medical_info[name1] = year + "/" + month + "/" + day;
            }

            $scope.Healthdatecheck1 = function (exp_date_pass, issue_date_pass, name1) {


                var month1 = exp_date_pass.split("/")[0];
                var day1 = exp_date_pass.split("/")[1];
                var year1 = exp_date_pass.split("/")[2];
                var new_exp_date_pass = year1 + "/" + month1 + "/" + day1;

                var year = issue_date_pass.split("/")[0];
                var month = issue_date_pass.split("/")[1];
                var day = issue_date_pass.split("/")[2];
                var new_issue_date_pass = year + "/" + month + "/" + day;

                if (new_exp_date_pass < new_issue_date_pass) {

                    $rootScope.strMessage = "Please Select Future Date";
                    $('#message').modal('show');

                    $scope.parent_info[name1] = '';
                }
                else {



                    $scope.parent_info[name1] = new_exp_date_pass;
                }
            }


            $scope.getsectioncode = function (cur, year, grade) {

                $http.get(ENV.apiUrl + "api/common/SectionTerm/getAllSections?cur_code=" + cur + "&ac_year=" + year + "&g_code=" + grade).then(function (res1) {
                    $scope.sections = res1.data;
                });


            }

            $scope.getTheFiles = function ($files) {

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };

            $scope.file_mother = function (element, name) {

                var str = '';
                str = 'M_' + $scope.parent_info.sims_parent_number;

                var photofile = element.files[0];
                photo_filename = (photofile.type);
                $scope.parent_info[name] = str + '.' + photo_filename.split("/")[1];
                sims_mother_image = photo_filename.replace(photo_filename, str);

                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.mother_photo = e.target.result;
                        //$scope.temp.em_img = element.files[0].name;
                        //console.log($scope.temp.em_img);
                    });
                };
                reader.readAsDataURL(photofile);

                var request = {
                    method: 'POST',
                    url: ENV.apiUrl + '/api/file/upload?filename=' + sims_mother_image.split(".")[0] + '&location=ParentImages',
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http(request)
                       .success(function (d) {

                       }, function () {
                           alert("Err");
                       });
            };

            $scope.file_father = function (element, name) {

                var str = '';
                str = 'F_' + $scope.parent_info.sims_parent_number;


                var photofile = element.files[0];
                photo_filename = (photofile.type);
                $scope.parent_info[name] = str + '.' + photo_filename.split("/")[1];
                sims_father_image = photo_filename.replace(photo_filename, str);

                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.father_photo = e.target.result;
                        //$scope.temp.em_img = element.files[0].name;
                        //console.log($scope.temp.em_img);
                    });
                };
                reader.readAsDataURL(photofile);

                var request = {
                    method: 'POST',
                    url: ENV.apiUrl + '/api/file/upload?filename=' + sims_father_image.split(".")[0] + '&location=ParentImages',
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http(request)
                       .success(function (d) {
                       }, function () {
                           alert("Err");
                       });
            };

            $scope.getTheFiles_mother = function ($files) {

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };

            $scope.getTheFiles_father = function ($files) {

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });

            }

            $scope.file_guardian = function (element, name) {

                var str = '';
                str = 'G_' + $scope.parent_info.sims_parent_number;

                var photofile = element.files[0];
                photo_filename = (photofile.type);
                $scope.parent_info[name] = str + '.' + photo_filename.split("/")[1];
                sims_guardian_image = photo_filename.replace(photo_filename, str);

                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.guardian_photo = e.target.result;
                        //$scope.temp.em_img = element.files[0].name;
                        //console.log($scope.temp.em_img);
                    });
                };
                reader.readAsDataURL(photofile);

                var request = {
                    method: 'POST',
                    url: ENV.apiUrl + '/api/file/upload?filename=' + sims_guardian_image.split(".")[0] + '&location=ParentImages',
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http(request)
                       .success(function (d) {
                           mother = d;
                       }, function () {
                           alert("Err");
                       });

            };

            $scope.getTheFiles_guardian = function ($files) {

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };


            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


            $(document).keydown(function (e) {
                // ESCAPE key pressed
                if (e.keyCode == 27) {
                    $('#myModal').modal('hide');
                }
            });

        }]);
})();



