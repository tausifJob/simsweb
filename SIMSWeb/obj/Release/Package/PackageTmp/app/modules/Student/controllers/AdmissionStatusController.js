﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AdmissionStatusController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
          // Pre-Required Functions and Variables
                $('*[data-datepicker="true"] input[type="text"]').datepicker(
                {
                    todayBtn: true,
                    orientation: "top left",
                    autoclose: true,
                    todayHighlight: true,
                    format: 'yyyy-mm-dd'
                });

       
            /* PAGER FUNCTIONS*/
            $scope.edt = [];
            $http.get(ENV.apiUrl + "api/common/Admission/GetadmCurName").then(function (admissionCur) {
                $scope.admissionCur = admissionCur.data;
            });

            $http.get(ENV.apiUrl + "api/common/Admission/Getadmstatus").then(function (admissionStatus) {
                $scope.admissionStatus = admissionStatus.data;
                setTimeout(function () {
                    $('#cmd_status').change(function () {
                       
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });

            $scope.cur_change=function()
            {
                $http.get(ENV.apiUrl + "api/common/Admission/GetadmAcademicYears?cur_code=" + $scope.edt.sims_cur_code).then(function (admissionYear) {
                    $scope.admissionYear = admissionYear.data;
                });
            }
            $scope.acdm_yr_change = function ()
            {
                $http.get(ENV.apiUrl + "api/common/Admission/GetadmGrades?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year).then(function (admissionGrade) {
                    $scope.admissionGrade = admissionGrade.data;
                    setTimeout(function () {
                        $('#cmb_grade').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }
            $scope.btnPreview_click = function () {

                $scope.grades = '';
                $scope.admstatus = '';

                for (var i = 0; i < $scope.edt.sims_application_status_code.length; i++)
                    $scope.admstatus = $scope.admstatus + ($scope.edt.sims_application_status_code[i]) + ',';

                for (var i = 0; i < $scope.edt.sims_grade_code.length; i++)
                    $scope.grades = $scope.grades + ($scope.edt.sims_grade_code[i]) + ',';


                $scope.edt1 = {};

                $scope.edt1.sims_cur_code = $scope.edt.sims_cur_code;
                $scope.edt1.sims_academic_year = $scope.edt.sims_academic_year;
                $scope.edt1.sims_grade_code = $scope.grades;
                $scope.edt1.sims_application_status_code = $scope.admstatus;
                if ($scope.edt.sims_start_date != undefined && $scope.edt.sims_start_date != '')
                    $scope.edt1.sims_start_date = $scope.edt.sims_start_date;
                else
                    $scope.edt1.sims_start_date = undefined;

                if ($scope.edt.sims_end_date != undefined && $scope.edt.sims_end_date != '')
                    $scope.edt1.sims_end_date = $scope.edt.sims_end_date;
                else
                    $scope.edt1.sims_end_date = undefined;

                if ($scope.edt.admission_no != undefined)
                    $scope.edt1.admission_no = $scope.edt.admission_no;
                else
                    $scope.edt1.admission_no = undefined;



                $http.post(ENV.apiUrl + "api/common/Admission/AdmissionData", $scope.edt1).then(function (admissions) {
                    $scope.admissions = admissions.data;
                    console.log($scope.admissions);
                });
            }
            $scope.printData = function (info) {
                var data = {
                    location: 'Sims.SIMR41',
                    parameter: {
                        admission_no: info.admission_no,
                    },
                    state: 'main.SIMR41',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')
            }

            $scope.reset = function () {
                $scope.admissions = [];
                $scope.edt.sims_cur_code = '';
                $scope.edt.sims_academic_year = '';
                $scope.acdm_yr_change();
                $scope.edt.sims_start_date = '';
                $scope.edt.sims_end_date = '';
                $scope.edt.admission_no = '';
            }

            $(function () {
                $('#cmb_grade').multipleSelect({ width: '100%' });
                $('#cmd_status').multipleSelect({ width: '100%' });
            });
        //Events End
        }])
})();



