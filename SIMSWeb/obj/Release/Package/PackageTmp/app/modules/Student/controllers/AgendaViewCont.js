﻿(function () {
    'use strict';
    var obj1, temp, opr, comp, flag, current_date;
    var main, deletefin = [], temp = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AgendaViewCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.Update_btn = true;
            $scope.display = true;
            $scope.table = true;
            $scope.agenda_grid = false;
            $scope.PageSize_Search = false;
            $scope.pageSize_txt = false;
            $scope.search_txt = false;

            var username = $rootScope.globals.currentUser.username;

            $scope.CurrentDate = new Date();
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            var yyyy = today.getFullYear();
            current_date = yyyy + '-' + mm + '-' + dd;


            console.log("Current Date");
            console.log(current_date);
            // var cur_year = yyyy;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#test").tableHeadFixer({ 'top': 1 });
            }, 100);
            $scope.downloaddoc = function (str) {
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Agenda/' + str;
                window.open($scope.url);
            }

            $scope.compareDate = function () {
                if ($scope.temp.sims_agenda_sdate > $scope.temp.sims_agenda_edate) {
                    swal({ title: "Alert", text: "End date should be greater than Start date", showCloseButton: true, width: 300, height: 200 });
                    $scope.temp.sims_agenda_edate = "";
                }
            }

            $scope.clear = function () {
                $scope.temp = '';
                $scope.filteredTodos = [];
                $scope.agenda_grid = false;
                $scope.PageSize_Search = false;
                $scope.pageSize_txt = false;
                $scope.search_txt = false;
            }

            $http.get(ENV.apiUrl + "api/Agenda/getAll_Agenda_Details").then(function (AllagendaData) {
                $scope.agenda_Details = AllagendaData.data;
                $scope.totalItems = $scope.agenda_Details.length;
                $scope.todos = $scope.agenda_Details;
                $scope.makeTodos();
                console.log($scope.agenda_Details);
            });

            $scope.printDiv = function (div) {

                var docHead = document.head.outerHTML;
                var printContents = document.getElementById(div).outerHTML;
                var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

                var newWin = window.open("", "_blank", winAttr);
                var writeDoc = newWin.document;
                writeDoc.open();
                writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                writeDoc.close();
                newWin.focus();
            }

            $scope.exportAction = function () {
                $('#test').tableExport({ type: 'pdf', pdfFontSize: '7', escape: 'false' });
            }

            $scope.exportData = function () {
                swal({
                    title: '',
                    text: "Are you sure you want to Save?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {//exportable
                        var blob = new Blob([document.getElementById('example_wrapper').innerHTML], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                        });
                        saveAs(blob, "Report.xls");
                    }
                });
            };

            $scope.expand = function (j, $event) {
                //console.log(j);
                if (j.sims_isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.sims_isexpanded = "grid";
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.sims_isexpanded = "none";
                }
            }




            $scope.agenda_Details_fun = function (acYear, sdt, edt, gCode, sCode, subCode) {

                if (acYear === "" || acYear === null || typeof acYear === "undefined" && gCode === "" || gCode === null || typeof gCode === "undefined"
                    && gCode === "" || sCode === null || typeof sCode === "undefined" && sdt === "" || sdt === null || typeof sdt === "undefined"
                    && edt === "" || edt === null || typeof edt === "undefined") {
                    swal({ title: "Alert", text: "Please Select Proper Input & Date", width: 300, height: 200 });
                }
                else {
                    $http.get(ENV.apiUrl + "api/Agenda/getAgendaDetails?sims_acaedmic_year=" + acYear + "&sdate=" + sdt + "&edate=" + edt + "&sims_grade_code=" + gCode + "&sims_section_code=" + sCode + "&sims_subject_code=" + subCode + "&em_login_code=" + username).then(function (agendaDetails) {
                        $scope.agenda_Details = agendaDetails.data;
                        console.log($scope.agenda_Details);
                        if ($scope.agenda_Details.length > 0) {
                            $scope.totalItems = $scope.agenda_Details.length;
                            $scope.todos = $scope.agenda_Details;
                            $scope.makeTodos();
                            $scope.PageSize_Search = true;
                            $scope.pageSize_txt = true;
                            $scope.search_txt = true;
                            $scope.agenda_grid = true;
                        }
                        else {
                            swal({ title: "Alert", text: "No Records Found", width: 300, height: 200 });
                        }
                    });
                }

            }


            $scope.show_Description = function (str, str1) {
                $('#agenda_desc').modal('show');
                debugger;
                $http.get(ENV.apiUrl + "api/Agenda/getAgendaDesc?sims_agenda_number=" + str + "&sims_agenda_name=" + str1).then(function (agendaDesc) {
                    $scope.agenda_Desc = agendaDesc.data;
                });
            }


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.temp = {
                    'sims_cur_code': $scope.curriculum[0].sims_cur_code
                }
                $scope.getAccYear($scope.temp.sims_cur_code);
            });

            $http.get(ENV.apiUrl + "api/Agenda/getSubject").then(function (Allsubjects) {
                $scope.All_subjects = Allsubjects.data;
            });

            $scope.getAccYear = function (cur_code) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = {
                        'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                        'sims_academic_year': $scope.Acc_year[0].sims_academic_year,
                    }
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                });
            }

            debugger;
            if (username == "admin" || username == "padmin") {
                $scope.getGrade = function (curCode, accYear) {
                    $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                        $scope.Grade_code = Gradecode.data;
                    });
                }
                $scope.getSection = function (curCode, gradeCode, accYear) {
                    $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                        $scope.Section_code = Sectioncode.data;
                    });
                }
            }
            else {
                $scope.getGrade = function (curCode, accYear) {
                    $http.get(ENV.apiUrl + "api/Agenda/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear + "&emp_login_code=" + username).then(function (Gradecode) {
                        $scope.Grade_code = Gradecode.data;
                    });
                }

                $scope.getSection = function (curCode, gradeCode, accYear) {
                    $http.get(ENV.apiUrl + "api/Agenda/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear + "&emp_login_code=" + username).then(function (Sectioncode) {
                        $scope.Section_code = Sectioncode.data;
                    });
                }
            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    $scope.filteredTodos[i].sims_icon = 'fa fa-plus-circle';
                }
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.agenda_Details, $scope.searchText);
                $scope.totalItems = $scope.agenda_Details.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.agenda_Details;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_agenda_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_subject_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_agenda_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_agenda_doc == toSearch) ? true : false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd",
            });
            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
        }])



})();
