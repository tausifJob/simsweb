﻿(function () {
    'use strict';
    var del = [];
    var main, grade;
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SectionScreeningCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.sectionScreening = [];
            $scope.grid1 = true;
            $scope.grid = true;
            $scope.display = false;


            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.temp = {
                    'sims_cur_code': $scope.curriculum[0].sims_cur_code
                }
                $scope.getAccYear($scope.temp.sims_cur_code);
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = {
                        'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                        'sims_academic_year': $scope.Acc_year[0].sims_academic_year
                    }
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                });
            }

            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
            }

            $http.get(ENV.apiUrl + "api/common/SectionScreening/Get_All_Section_Screening").then(function (res1) {
                $scope.obj1 = res1.data;
                $scope.totalItems = $scope.obj1.length;
                $scope.todos = $scope.obj1;
                $scope.makeTodos();
            });

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj1, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj1;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.screening_grade_code_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.screening_criteria_code_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.desg_company_code == toSearch) ? true : false;
            }

            $scope.edt={};

            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.obj2 = res.data;

                $scope.edt['screening_cur_code'] = $scope.obj2[0].sims_attendance_cur_code;
                $scope.getAcdyr($scope.obj2[0].sims_attendance_cur_code);
            });

            // $http.get(ENV.apiUrl + "api/common/getAllScreeningGradeCode?cur_code=" + $scope.edt.screening_cur_code + "&acad_yr=" + $scope.edt.screening_academic_year).then(function (res) {

            $http.get(ENV.apiUrl + "api/common/getAllGrades").then(function (res) {
                $scope.allgrade = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/SectionScreening/getAllCriteriaNames").then(function (res) {
                $scope.critype = res.data;
            });

            $scope.getAcdyr = function (crcode) {
                //debugger;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + crcode).then(function (res) {
                    $scope.acad = res.data;
                    $scope.edt['screening_academic_year'] = $scope.acad[0].sims_academic_year;

                    $scope.GetGrade(crcode, $scope.acad[0].sims_academic_year);
                    
                });
            }

            $scope.GetGrade = function (cur, acad_yr) {

                if (cur != null && acad_yr != null) {
                    $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + cur + "&academic_year=" + acad_yr).then(function (res) {
                        // $http.get(ENV.apiUrl + "api/common/getAllScreeningGradeCode?cur_code=" + cur + "&acad_yr=" + acad_yr).then(function (res) {
                        $scope.grade = res.data;
                      
                        setTimeout(function () {
                            $('#cmb_grade').change(function () {
                                console.log($(this).val());
                            }).multipleSelect({
                                width: '100%'
                            });
                        }, 1000);
                    });


                }
            }

            $scope.GetSection = function (cur, acad_yr, grade) {
                console.log(grade);
                if (cur != null && acad_yr != null && grade != null) {
                    $http.get(ENV.apiUrl + "api/common/SectionScreening/getAllSection?cur_code=" + cur + "&ac_year=" + acad_yr + "&grade=" + grade).then(function (res) {

                        $scope.section = res.data;
                        console.log($scope.section);
                        setTimeout(function () {
                            $('#cmb_section').change(function () {
                                console.log($(this).val());
                            }).multipleSelect({
                                width: '100%'
                            });
                        }, 1000);
                    });
                }
            }

            $scope.clear = function () {
                $scope.temp.sims_cur_code = "";
                $scope.temp.sims_academic_year = "";
                $scope.temp.sims_grade_code = "";
                $scope.sectionScreening = "";
                $scope.grid = false;
            }

            $scope.getscreening = function (curcode, acyear, gradecode) {


                $http.get(ENV.apiUrl + "api/common/SectionScreening/Get_Section_Screening?cur_code=" + curcode + "&academic_year=" + acyear + "&grade_code=" + gradecode).then(function (res) {
                    $scope.display = false;
                    $scope.sectionScreening = res.data;

                    if ($scope.sectionScreening == 0) {
                        swal({ text: "Record not found", width: 300, height: 200 });
                    }


                });

            }

            $scope.edit = function (str) {

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str.screening_cur_code).then(function (res) {
                    $scope.acad = res.data;
                    $scope.edt['screening_academic_year'] = str.screening_academic_year

                });
                
                $scope.display = true;
                $scope.grid1 = false;
                $scope.grid = false;
                $scope.btn_save1 = false;
                $scope.btn_update1 = true;
                $scope.delete1 = false;

                $scope.select_grade = false;
                $scope.txt_grade = true;

                console.log(str);
                $scope.edt = str;
                $scope.sec_screen = true;

                grade = $scope.edt.screening_grade_code;
                //console.log(grade);
               

            }

            var datasend = [];
            $scope.Save = function (isvalidate) {

                if (isvalidate) {
                    if ($scope.btn_update1 == false) {
                        var data = $scope.edt;
                        // var str_section;
                        var grade_code;

                        var grade = $scope.edt.screening_grade_code;
                        grade_code = grade_code + ',' + grade;
                        var str2 = grade_code.substr(grade_code.indexOf(',') + 1);

                        data.screening_grade_code = str2;
                        //data.screening_grade_sec = str1;
                        data.opr = "Y";
                        datasend.push(data);

                        $http.post(ENV.apiUrl + "api/common/SectionScreening/CUDSection_Screening", datasend).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 380, showCloseButton: true });
                            }
                            else {
                                swal({ title: "Alert", text: "Selected Grade, Section And Criteria is Already Defined", width: 380, showCloseButton: true });
                            }

                            $http.get(ENV.apiUrl + "api/common/SectionScreening/Get_All_Section_Screening").then(function (res1) {
                                $scope.obj1 = res1.data;
                                $scope.totalItems = $scope.obj1.length;
                                $scope.todos = $scope.obj1;
                                $scope.makeTodos();
                            });

                            $scope.grid1 = true;
                            $scope.grid = true;
                            $scope.display = false;
                        });

                        datasend = [];
                    }
                    else {
                        console.log('ddd');
                        $scope.Update(isvalidate);
                    }

                }

            }


            var dataupdate = [];
            $scope.Update = function (isvalidate) {
                debugger;
                if (isvalidate) {
                    var data = $scope.edt;
                    //data.screening_grade_code = grade;
                    data.opr = "U";
                    dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/common/SectionScreening/CUDSection_Screening", dataupdate).then(function (res) {
                        debugger;
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", width: 380, showCloseButton: true });
                        }
                        else {
                            swal({ text: "Record Not Updated", width: 380, showCloseButton: true });
                        }
                        $http.get(ENV.apiUrl + "api/common/SectionScreening/Get_All_Section_Screening").then(function (res1) {
                            $scope.obj1 = res1.data;
                            $scope.totalItems = $scope.obj1.length;
                            $scope.todos = $scope.obj1;
                            $scope.makeTodos();
                        });

                        $scope.grid1 = true;
                        $scope.grid = true;
                        $scope.display = false;
                        //var grade = $scope.temp.sims_grade_code
                        //$scope.getscreening(grade);
                    });
                    dataupdate = [];
                }
            }

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.grid1 = false;
                $scope.btn_save1 = true;
                $scope.btn_update1 = false;
                $scope.delete1 = false;
                $scope.edt['screening_grade_code'] = "";
                $scope.edt['screening_criteria_code'] = "";
                $scope.edt['screening_rating'] = "";
                $scope.edt['screening_marks'] = "";
                $scope.select_grade = true;
                $scope.txt_grade = false;
                $scope.sec_screen = false;

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.grid1 = true;
            }


            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-"+i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById("test-" + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'screening_cur_code': $scope.filteredTodos[i].screening_cur_code,
                            'screening_academic_year': $scope.filteredTodos[i].screening_academic_year,
                            'screening_grade_code': $scope.filteredTodos[i].screening_grade_code,
                            'screening_criteria_code': $scope.filteredTodos[i].screening_criteria_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/common/SectionScreening/CUDSection_Screening?data=", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/common/SectionScreening/Get_All_Section_Screening").then(function (res1) {
                                                $scope.obj1 = res1.data;
                                                $scope.totalItems = $scope.obj1.length;
                                                $scope.todos = $scope.obj1;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted ", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {

                                            $http.get(ENV.apiUrl + "api/common/SectionScreening/Get_All_Section_Screening").then(function (res1) {
                                                $scope.obj1 = res1.data;
                                                $scope.totalItems = $scope.obj1.length;
                                                $scope.todos = $scope.obj1;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById("test-" + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }




        }])
})();