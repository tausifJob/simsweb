﻿(function () {
    'use strict';
    var del = [], feedetails = [];

    var main;
    var date1, date3, date4;

    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CancelAdmissionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.grid = true;
            $scope.save1 = true;
            $scope.edit_code = true;
            $scope.edt = { feeDet: '' };

            $scope.username = $rootScope.globals.currentUser.username;
            // console.log($scope.username);

            $rootScope.visible_stud = true;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = false;
            $rootScope.chkMulti = false;

            $scope.$on('global_cancel', function (str) {
                console.log($scope.SelectedUserLst);
                $scope.edit_code = false;
                $scope.DataEnroll();

            });

            $scope.getstudentView = function () {
                $scope.edt = '';
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (res2) {
                $scope.display = true;
                $scope.ComboBoxValues = res2.data;
                console.log($scope.ComboBoxValues);
            });

            $scope.SearchStudentWindow = function () {
                $scope.searchtable = false;
                $scope.student = '';
                $('#MyStudentModal').modal('show');
                $scope.temp = '';
            }

            $scope.SearchSudent = function () {
                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    main.checked = false;
                }

                $scope.searchtable = false;
                $scope.busy = true;
             

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.temp)).then(function (Allstudent) {
                    debugger
                    $scope.student = Allstudent.data;
                    $scope.searchtable = true;
                    $scope.busy = false;
                });
                $scope.edit_code = false;
            }

            $scope.DataEnroll = function ()
            {
                for (var i = 0; i < $scope.SelectedUserLst.length; i++) {
                    // var t = $scope.SelectedUserLst[i].s_enroll_no;
                    //var v = document.getElementById(t + i);
                    // if (v.checked == true)
                    if ($scope.SelectedUserLst[i].user_chk == true) {
                        var student = $scope.SelectedUserLst[i].s_enroll_no;
                        $scope.cur_code = $scope.SelectedUserLst[i].s_cur_code;
                        $scope.acad_yr = $scope.SelectedUserLst[i].sims_academic_year;

                        $http.post(ENV.apiUrl + "api/common/CancelAdmission/CancelAdmission?enroll_no=" + $scope.SelectedUserLst[i].s_enroll_no + "&cur_code=" + $scope.SelectedUserLst[i].s_cur_code + "&acad_yr=" + $scope.SelectedUserLst[i].sims_academic_year).then(function (res) {
                            $scope.edt = res.data;
                            console.log($scope.edt);
                        });

                    }
                }
                $scope.student = [];
            }

            //$scope.DataEnroll = function () {
            //    for (var i = 0; i < $scope.student.length; i++) {
            //        var t = $scope.student[i].s_enroll_no;
            //        var v = document.getElementById(t + i);
            //        if (v.checked == true) {
            //            var student = $scope.student[i].s_enroll_no;

            //            $http.post(ENV.apiUrl + "api/common/CancelAdmission/CancelAdmission?enroll_no=" + $scope.student[i].s_enroll_no + "&cur_code=" + $scope.student[i].s_cur_code + "&acad_yr=" + $scope.student[i].sims_academic_year).then(function (res) {
            //                $scope.edt = res.data;
            //                console.log($scope.edt);
            //            });

            //        }
            //    }
            //    $scope.student = [];
            //}

            $scope.OkCancelAdmission = function () {
                feedetails = [];
                for (var i = 0; i < $scope.edt.feeDet.length; i++) {
                    var t = $scope.edt.feeDet[i].feeCode;
                    var v = document.getElementById(t);

                    if (v.checked == true) {
                        feedetails = feedetails + $scope.edt.feeDet[i].feeCode + ',';
                    }
                }

                //var v = document.getElementById('txt_enroll_no');
                //$scope.edt.enroll = v.value;
                var data = $scope.edt;
                data.cur_code = $scope.edt.studentDet.curr_name;
                data.academic_year = $scope.edt.studentDet.academic_year;
                data.fee_category_code = feedetails;
                data.fees_paid_status = '1';
                data.opr = "P";

                console.log(data);

                $http.post(ENV.apiUrl + "api/common/CancelAdmission/CancelAdmissionProceed", data).then(function (res) {
                    $scope.obj1 = res.data;
                    swal({ title: "Alert", text: "Admission Cancelled", showCloseButton: true, width: 380, });
                    $scope.cancel();
                });
                // $scope.edt = [];

            }

            $scope.cancel = function () {
                $scope.edt = [];
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Save = function (isvalidate) {
                //if ($scope.edt.reason != '')
                if ($scope.edt.reason == null) {
                    if (isvalidate) {
                        swal({
                            title: "Alert",
                            text: "Do You Want To Continue Cancellation Process Without Reason",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $scope.OkCancelAdmission();

                            }
                        });
                    }
                }
                else {
                    if (isvalidate) {
                        $scope.OkCancelAdmission();
                    }
                }
            }

            $scope.OkCanceladm = function () {
                $scope.OkCancelAdmission();
            }

            $scope.check1 = function (Fee) {
                console.log(Fee);
                var v = document.getElementById(Fee.feeCode);

                if (v.checked == true) {
                    feedetails = feedetails + Fee.feeCode + ','
                    console.log(feedetails);
                    $scope.row1 = '';
                }
                else {
                    v.checked = false;
                    console.log(feedetails);
                }
            }

            function convertdate(dt) {
                if (dt != null) {
                    var d1 = new Date(dt);
                    var month = d1.getMonth() + 1;
                    var day = d1.getDate();
                    if (month < 10)
                        month = "0" + month;
                    if (day < 10)
                        day = "0" + day;
                    var d = d1.getFullYear() + "-" + (month) + "-" + (day);
                    return d;
                }
            }
            $scope.flag = true;
            $scope.getCollapse = function () {
                // $('.nav-tabs a[href="#collapseOne"]').tab('show');
                // $('#fee').removeClass('collapsed');
                //debugger;
                //if ($scope.flag==true) {
                //    $('#fee').removeClass('collapsed');
                //    $scope.flag = false
                // }
                //else {
                //    $('#fee').addClass('collapsed');

                //}
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


        }])
})();