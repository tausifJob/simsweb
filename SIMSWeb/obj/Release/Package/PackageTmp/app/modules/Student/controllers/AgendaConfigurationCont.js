﻿(function () {
    'use strict';
    var temp, photo_filename;
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

    simsController.controller('AgendaConfigurationCont',
       ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
           $scope.Restriction = true;
           $scope.table1 = true;
           $scope.hide = true;
           $scope.EmploymentNo = true;
           $scope.editmode = false;
           var formdata = new FormData();

           //All ComboBoxes
           $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
               $scope.ComboBoxValues = AllComboBoxValues.data;
               $scope.temp = {
                   sims_academic_year: $scope.ComboBoxValues[0].sims_acadmic_year,
                   s_cur_code: $scope.ComboBoxValues[0].sims_cur_code,
               };
               console.log($scope.ComboBoxValues);
           });

           //Agenda Type
           $http.get(ENV.apiUrl + "api/AgendaConfiguration/GetAgendatype").then(function (Type) {
               $scope.agenda = Type.data;
               console.log($scope.agenda);
           });

           //Clear
           $scope.clear = function () {
               $scope.temp = '';
           }

           /////Supervisor Selection
           function SUDetails(obj) {
               debugger
               if ($scope.temp == undefined || $scope.temp.s_cur_code == undefined || $scope.temp.s_cur_code == null || $scope.temp.sims_academic_year == undefined || $scope.temp.sims_academic_year == null) {
                   swal('', 'Please Select Curriculum And Academic Year');
                   $scope.searchtable = false;
                   return;
               }

               $http.get(ENV.apiUrl + "api/AgendaConfiguration/getSupervisorGradeSectionSubject?data=" + JSON.stringify($scope.temp)).then(function (supervisordata) {

                   $scope[obj] = supervisordata.data;
                   console.log($scope.supervisor_data);
               });
           }
           $scope.hide1 = function () {
               $scope.supervisor = true;
               $scope.hod_show = false;
               $scope.subjectteacher = false;
               $scope.classteacher = false;
               $scope.hod_data = '';
               $scope.subjectteach_data = '';
               $scope.classteach_data = '';

               $scope.Getdetails = function () {
                   debugger
                   if ($scope.temp == undefined || $scope.temp.s_cur_code == undefined || $scope.temp.s_cur_code == null || $scope.temp.sims_academic_year == undefined || $scope.temp.sims_academic_year == null) {
                       swal('', 'Please Select Curriculum And Academic Year');
                       $scope.searchtable = false;
                       return;
                   }

                   $http.get(ENV.apiUrl + "api/AgendaConfiguration/getSupervisorGradeSectionSubject?data=" + JSON.stringify($scope.temp)).then(function (supervisordata) {

                       $scope.supervisor_data = supervisordata.data;
                       console.log($scope.supervisor_data);
                   });
               }
           }
           $scope.hide1();
           $scope.checkAllGrade = function (sup) {
               for (var i = 0; i < sup.sup_grade.length; i++) {
                   sup.sup_grade[i]['isSelected'] = sup.isSelected;
                   $scope.checkAllSection(sup.sup_grade[i]);
               }
           }

           $scope.checkAllSection = function (grd) {
               for (var i = 0; i < grd.sup_sect.length; i++) {
                   grd.sup_sect[i]['isSelected'] = grd.isSelected;
                   $scope.checkAllSubject(grd.sup_sect[i]);
               }
           }

           $scope.checkAllSubject = function (sec) {
               for (var i = 0; i < sec.sup_subject.length; i++) {
                   sec.sup_subject[i]['isSelected'] = sec.isSelected;
               }
           }

           /////HOD Selection
           function HODetails(obj) {
               debugger
               if ($scope.temp == undefined || $scope.temp.s_cur_code == undefined || $scope.temp.s_cur_code == null || $scope.temp.sims_academic_year == undefined || $scope.temp.sims_academic_year == null) {
                   swal('', 'Please Select Curriculum And Academic Year');
                   $scope.searchtable = false;
                   return;
               }

               $http.get(ENV.apiUrl + "api/AgendaConfiguration/getHODGradeSectionSubject?data=" + JSON.stringify($scope.temp)).then(function (hod) {
                   debugger
                   $scope[obj] = hod.data;
                   console.log($scope.hod_data);
               });
           }
           $scope.hide2 = function () {
               debugger
               $scope.supervisor = false;
               $scope.hod_show = true;
               $scope.subjectteacher = false;
               $scope.classteacher = false;
               $scope.supervisor_data = '';
               $scope.subjectteach_data = '';
               $scope.classteach_data = '';

               $scope.Getdetails = function () {
                   debugger
                   if ($scope.temp == undefined || $scope.temp.s_cur_code == undefined || $scope.temp.s_cur_code == null || $scope.temp.sims_academic_year == undefined || $scope.temp.sims_academic_year == null) {
                       swal('', 'Please Select Curriculum And Academic Year');
                       $scope.searchtable = false;
                       return;
                   }

                   $http.get(ENV.apiUrl + "api/AgendaConfiguration/getHODGradeSectionSubject?data=" + JSON.stringify($scope.temp)).then(function (hod) {
                       debugger
                       $scope.hod_data = hod.data;
                       console.log($scope.hod_data);
                   });
               }
           }
           $scope.checkAllHodGrade = function (sup) {
               for (var i = 0; i < sup.sup_grade.length; i++) {
                   sup.sup_grade[i]['isSelected'] = sup.isSelected;
                   $scope.checkAllHodSection(sup.sup_grade[i]);
               }
           }

           $scope.checkAllHodSection = function (grd) {
               for (var i = 0; i < grd.sup_sect.length; i++) {
                   grd.sup_sect[i]['isSelected'] = grd.isSelected;
                   $scope.checkAllHodSubject(grd.sup_sect[i]);
               }
           }

           $scope.checkAllHodSubject = function (sec) {
               for (var i = 0; i < sec.sup_subject.length; i++) {
                   sec.sup_subject[i]['isSelected'] = sec.isSelected;
               }
           }

           /////Subject Teacher Selection 
           function STDetails(obj) {
               if ($scope.temp == undefined || $scope.temp.s_cur_code == undefined || $scope.temp.s_cur_code == null || $scope.temp.sims_academic_year == undefined || $scope.temp.sims_academic_year == null) {
                   swal('', 'Please Select Curriculum And Academic Year');
                   $scope.searchtable = false;
                   return;
               }

               $http.get(ENV.apiUrl + "api/AgendaConfiguration/getSubjectTeacherGradeSectionSubject?data=" + JSON.stringify($scope.temp)).then(function (subjdata) {
                   debugger
                   $scope[obj] = subjdata.data;
                   console.log($scope.subjectteach_data);
               });
           }

           $scope.hide3 = function () {
               $scope.supervisor = false;
               $scope.hod_show = false;
               $scope.subjectteacher = true;
               $scope.classteacher = false;
               $scope.supervisor_data = '';
               $scope.hod_data = '';
               $scope.classteach_data = '';


               $scope.Getdetails = function () {

                   if ($scope.temp == undefined || $scope.temp.s_cur_code == undefined || $scope.temp.s_cur_code == null || $scope.temp.sims_academic_year == undefined || $scope.temp.sims_academic_year == null) {
                       swal('', 'Please Select Curriculum And Academic Year');
                       $scope.searchtable = false;
                       return;
                   }

                   $http.get(ENV.apiUrl + "api/AgendaConfiguration/getSubjectTeacherGradeSectionSubject?data=" + JSON.stringify($scope.temp)).then(function (subjdata) {
                       debugger
                       $scope.subjectteach_data = subjdata.data;
                       console.log($scope.subjectteach_data);
                   });
               }
           }

           $scope.checkAllSubjectTeacherGrade = function (sup) {
               for (var i = 0; i < sup.sup_grade.length; i++) {
                   sup.sup_grade[i]['isSelected'] = sup.isSelected;
                   $scope.checkAllSubjectTeacherSection(sup.sup_grade[i]);
               }
           }

           $scope.checkAllSubjectTeacherSection = function (grd) {
               for (var i = 0; i < grd.sup_sect.length; i++) {
                   grd.sup_sect[i]['isSelected'] = grd.isSelected;
                   $scope.checkAllSubjectTeacherSubject(grd.sup_sect[i]);
               }
           }

           $scope.checkAllSubjectTeacherSubject = function (sec) {
               for (var i = 0; i < sec.sup_subject.length; i++) {
                   sec.sup_subject[i]['isSelected'] = sec.isSelected;
               }
           }

           /////Class Teacher Selection   
           function CTDetails(obj) {
               debugger
               if ($scope.temp == undefined || $scope.temp.s_cur_code == undefined || $scope.temp.s_cur_code == null || $scope.temp.sims_academic_year == undefined || $scope.temp.sims_academic_year == null) {
                   swal('', 'Please Select Curriculum And Academic Year');
                   $scope.searchtable = false;
                   return;
               }

               $http.get(ENV.apiUrl + "api/AgendaConfiguration/getClassTeacherGradeSectionSubject?data=" + JSON.stringify($scope.temp)).then(function (Classdata) {

                   $scope[obj] = Classdata.data;
                   console.log($scope.classteach_data);
               });
           }
           $scope.hide4 = function () {
               debugger
               $scope.supervisor = false;
               $scope.hod_show = false;
               $scope.subjectteacher = false;
               $scope.classteacher = true;
               $scope.supervisor_data = '';
               $scope.subjectteach_data = '';
               $scope.hod_data = '';

               $scope.Getdetails = function () {
                   debugger
                   if ($scope.temp == undefined || $scope.temp.s_cur_code == undefined || $scope.temp.s_cur_code == null || $scope.temp.sims_academic_year == undefined || $scope.temp.sims_academic_year == null) {
                       swal('', 'Please Select Curriculum And Academic Year');
                       $scope.searchtable = false;
                       return;
                   }

                   $http.get(ENV.apiUrl + "api/AgendaConfiguration/getClassTeacherGradeSectionSubject?data=" + JSON.stringify($scope.temp)).then(function (Classdata) {

                       $scope.classteach_data = Classdata.data;
                       console.log($scope.classteach_data);
                   });
               }
           }
           $scope.checkAllClassTeacherGrade = function (sup) {
               for (var i = 0; i < sup.sup_grade.length; i++) {
                   sup.sup_grade[i]['isSelected'] = sup.isSelected;
                   $scope.checkAllClassTeacherSection(sup.sup_grade[i]);
               }
           }

           $scope.checkAllClassTeacherSection = function (grd) {
               for (var i = 0; i < grd.sup_sect.length; i++) {
                   grd.sup_sect[i]['isSelected'] = grd.isSelected;
                   $scope.checkAllClassTeacherSubject(grd.sup_sect[i]);
               }
           }

           $scope.checkAllClassTeacherSubject = function (sec) {
               for (var i = 0; i < sec.sup_subject.length; i++) {
                   sec.sup_subject[i]['isSelected'] = sec.isSelected;
               }
           }

           /////Save Records
           var subdata = [];
           $scope.Save = function () {
               if ($scope.temp.sims_agenda_type == null || $scope.temp.sims_agenda_type == undefined) {
                   swal('', 'Please Select Agenda Type');
                   return;
               }

               ////////Save Supervisor
               if ($scope.supervisor == true) {
                   for (var i = 0; i < $scope.supervisor_data.length; i++) {
                       if ($scope.supervisor_data[i].isSelected == true) {
                           for (var j = 0; j < $scope.supervisor_data[i].sup_grade.length; j++) {
                               for (var k = 0; k < $scope.supervisor_data[i].sup_grade[j].sup_sect.length; k++) {
                                   for (var l = 0; l < $scope.supervisor_data[i].sup_grade[j].sup_sect[k].sup_subject.length; l++) {
                                       var subject = ({
                                           'sims_cur_code': $scope.supervisor_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_cur_code,
                                           'sims_academic_year': $scope.supervisor_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_academic_year,
                                           'sims_grade_code': $scope.supervisor_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_grade_code,
                                           'sims_section_code': $scope.supervisor_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_section_code,
                                           'sims_subject_code': $scope.supervisor_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_subject_code,
                                           'sims_employee_code': $scope.supervisor_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_supervisor_code,
                                           'sims_agenda_assign_type': $scope.temp.sims_agenda_type,
                                       });
                                       subdata.push(subject);
                                   }
                               }
                           }
                       }
                       //else {
                       //    swal('','Please Select Records');
                       //}
                   }
                   $http.post(ENV.apiUrl + "api/AgendaConfiguration/InsertAgendaAccess", subdata).then(function (msg) {
                       $scope.msg1 = msg.data;
                       if ($scope.msg1 == true) {
                           swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 }).then(function (isConfirm) {
                               if (isConfirm) {
                                   SUDetails('supervisor_data');
                               }
                           });
                       }
                       else {
                           swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                       }
                   });
                   subdata = [];

               }

                   ////////Save HOD
               else if ($scope.hod_show == true) {
                   for (var i = 0; i < $scope.hod_data.length; i++) {
                       if ($scope.hod_data[i].isSelected == true) {
                           for (var j = 0; j < $scope.hod_data[i].sup_grade.length; j++) {
                               for (var k = 0; k < $scope.hod_data[i].sup_grade[j].sup_sect.length; k++) {
                                   for (var l = 0; l < $scope.hod_data[i].sup_grade[j].sup_sect[k].sup_subject.length; l++) {
                                       var subject = ({
                                           'sims_cur_code': $scope.hod_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_cur_code,
                                           'sims_academic_year': $scope.hod_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_academic_year,
                                           'sims_grade_code': $scope.hod_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_grade_code,
                                           'sims_section_code': $scope.hod_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_section_code,
                                           'sims_subject_code': $scope.hod_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_subject_code,
                                           'sims_employee_code': $scope.hod_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_head_teacher_code,
                                           'sims_agenda_assign_type': $scope.temp.sims_agenda_type,
                                       });
                                       subdata.push(subject);
                                   }
                               }
                           }
                       }
                       //else {
                       //    swal('', 'Please Select Records');
                       //}
                   }
                   $http.post(ENV.apiUrl + "api/AgendaConfiguration/InsertAgendaAccess", subdata).then(function (msg) {
                       $scope.msg1 = msg.data;
                       if ($scope.msg1 == true) {
                           swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 }).then(function (isConfirm) {
                               if (isConfirm) {
                                   HODetails('hod_data');
                               }
                           });
                       }
                       else {
                           swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                       }
                   });
                   subdata = [];
               }

                   ////////Save Subject Teacher
               else if ($scope.subjectteacher == true) {
                   for (var i = 0; i < $scope.subjectteach_data.length; i++) {
                       if ($scope.subjectteach_data[i].isSelected == true) {
                           for (var j = 0; j < $scope.subjectteach_data[i].sup_grade.length; j++) {
                               for (var k = 0; k < $scope.subjectteach_data[i].sup_grade[j].sup_sect.length; k++) {
                                   for (var l = 0; l < $scope.subjectteach_data[i].sup_grade[j].sup_sect[k].sup_subject.length; l++) {
                                       var subject = ({
                                           'sims_cur_code': $scope.subjectteach_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_cur_code,
                                           'sims_academic_year': $scope.subjectteach_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_academic_year,
                                           'sims_grade_code': $scope.subjectteach_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_grade_code,
                                           'sims_section_code': $scope.subjectteach_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_section_code,
                                           'sims_subject_code': $scope.subjectteach_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_subject_code,
                                           'sims_employee_code': $scope.subjectteach_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_bell_teacher_code,
                                           'sims_agenda_assign_type': $scope.temp.sims_agenda_type,
                                       });
                                       subdata.push(subject);
                                   }
                               }
                           }
                       }
                       //else {
                       //    swal('', 'Please Select Records');
                       //}
                   }
                   $http.post(ENV.apiUrl + "api/AgendaConfiguration/InsertAgendaAccess", subdata).then(function (msg) {
                       $scope.msg1 = msg.data;
                       if ($scope.msg1 == true) {
                           swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 }).then(function (isConfirm) {
                               if (isConfirm) {
                                   STDetails('subjectteach_data');
                               }
                           });
                       }
                       else {
                           swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                       }
                   });
                   subdata = [];
               }

                   ////////Save Class Teacher
               else if ($scope.classteacher == true) {
                   for (var i = 0; i < $scope.classteach_data.length; i++) {
                       if ($scope.classteach_data[i].isSelected == true) {
                           for (var j = 0; j < $scope.classteach_data[i].sup_grade.length; j++) {
                               for (var k = 0; k < $scope.classteach_data[i].sup_grade[j].sup_sect.length; k++) {
                                   for (var l = 0; l < $scope.classteach_data[i].sup_grade[j].sup_sect[k].sup_subject.length; l++) {
                                       var subject = ({
                                           'sims_cur_code': $scope.classteach_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_cur_code,
                                           'sims_academic_year': $scope.classteach_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_academic_year,
                                           'sims_grade_code': $scope.classteach_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_grade_code,
                                           'sims_section_code': $scope.classteach_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_section_code,
                                           'sims_subject_code': $scope.classteach_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_subject_code,
                                           'sims_employee_code': $scope.classteach_data[i].sup_grade[j].sup_sect[k].sup_subject[l].sims_class_teacher_code,
                                           'sims_agenda_assign_type': $scope.temp.sims_agenda_type,
                                       });
                                       subdata.push(subject);
                                   }
                               }
                           }
                       }
                       //else {
                       //    swal('', 'Please Select Records');
                       //}
                   }
                   $http.post(ENV.apiUrl + "api/AgendaConfiguration/InsertAgendaAccess", subdata).then(function (msg) {
                       $scope.msg1 = msg.data;
                       if ($scope.msg1 == true) {
                           swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 }).then(function (isConfirm) {
                               if (isConfirm) {
                                   CTDetails('classteach_data');
                               }
                           });
                       }
                       else {
                           swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                       }
                   });
                   subdata = [];
               }
           }

       }]);
})();




