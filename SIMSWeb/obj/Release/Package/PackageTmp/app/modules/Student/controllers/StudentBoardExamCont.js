﻿(function () {
    'use strict';
    var selected_enroll_number = [];
    var registration_num = [];
    var main;
    var categorycode = [];
    var data1 = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StudentBoardExamCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.examstudentdata = false;
            // $scope.searchStudent = true;
            $scope.grid = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.enrollno = true;
            $scope.enroll_number = [];
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;
            var sims_student_enrolls = [];
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $http.get(ENV.apiUrl + "api/student/studentexam/getBoard").then(function (res) {
                $scope.BoardData = res.data;
                console.log($scope.BoardData);

            });

            $scope.getboardcode = function (boardcode) {
                debugger
                $http.get(ENV.apiUrl + "api/student/studentexam/getBoardExam?board_code=" + boardcode).then(function (res) {
                    $scope.BoardExamData = res.data;
                    console.log($scope.BoardExamData);
                });
            }

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;
                console.log($scope.ComboBoxValues);
            });

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                console.log($scope.curriculum);

            });

            $scope.getacyr = function (str) {
                console.log(str);
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    console.log($scope.Academic_year);
                })
            }

            $scope.edit = function (str) {
                debugger
                console.log(str);
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.readonly = true;
                $scope.searchStudent = false;
                $scope.cur = true;
                $scope.sttt = true;
                $scope.year = true;
                $scope.examboard = true;
                $scope.boardname = true;
                $scope.getacyr(str.sims_cur_name);
                $scope.enrollno = false;
                $scope.ClearCheckBox();
                $scope.enroll_number = [];
                $scope.edt = {
                    studentName: str.studentName
                             , sims_board_exam_enroll_number: str.sims_board_exam_enroll_number
                             , sims_cur_code: str.sims_cur_code
                             , sims_academic_year: str.sims_academic_year
                            , sims_board_code: str.sims_board_code
                            , sims_board_exam_code: str.sims_board_exam_code
                            , sims_board_exam_registration_no: str.sims_board_exam_registration_no
                            , sims_board_exam_status: str.sims_board_exam_status

                };
            }

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.searchStudent = true;
                $scope.cur = false;
                $scope.sttt = false;
                $scope.year = false;
                $scope.examboard = false;
                $scope.boardname = false;
                $scope.edt = "";
                $scope.enrollno = true;
                $scope.ClearCheckBox();
                $scope.enroll_number = [];
                $scope.MyForm.$setPristine();
                $scope.MyForm.$setUntouched();
            }

            $scope.Show = function () {
                $scope.examstudentdata = true;
                $scope.ImageView = false;

                $http.get(ENV.apiUrl + "api/student/studentexam/getStudentsExamDetails?curcode=" + $scope.edt.sims_cur_code +
                "&boardcode=" + $scope.edt.sims_board_code +
                "&academicyear=" + $scope.edt.sims_academic_year +
                "&boardexamcode=" + $scope.edt.sims_board_exam_name).then(function (Student_Data) {

                    $scope.StudenteData = Student_Data.data;
                    $scope.totalItems = $scope.StudenteData.length;
                    $scope.todos = $scope.StudenteData;
                    $scope.makeTodos();

                    console.log($scope.StudenteData);
                    if (Student_Data.data.length > 0) { }
                    else {
                        $scope.ImageView = true;
                    }
                });
            }

            $scope.Save = function (isvalid) {
                if (isvalid) {
                    debugger
                    if ($scope.enroll_number.length > 0) {

                        for (var i = 0; i < $scope.enroll_number.length; i++) {

                            debugger;
                            //   $scope.edt.sims_student_passport_first_name_en = sims_student_enrolls;
                            var data = {

                                sims_cur_code: $scope.edt.sims_cur_code,
                                sims_academic_year: $scope.edt.sims_academic_year,
                                sims_grade_code: $scope.enroll_number[i].grade_code,
                                sims_section_code: $scope.enroll_number[i].section_code,
                                sims_board_code: $scope.edt.sims_board_code,
                                sims_board_exam_code: $scope.edt.sims_board_exam_code,
                                sims_board_exam_enroll_number: $scope.enroll_number[i].s_enroll_no,
                                sims_board_exam_registration_no: $scope.enroll_number[i].registration_num,
                                sims_board_exam_student_fee_paid_status: 'P',
                                sims_board_exam_status: $scope.edt.sims_board_exam_status,
                                opr: 'I'
                            }

                            data1.push(data);
                            $http.post(ENV.apiUrl + "api/student/studentexam/StudentBoardCUD", data1).then(function (MSG1) {
                                $scope.msg = MSG1.data;
                                $scope.grid = true;
                                $scope.display = false;
                                if ($scope.msg == true) {
                                    swal({ title: "Alert", text: "Student Exam Created Successfully", showCloseButton: true, width: 380 });
                                    $scope.checkonebyonedelete();
                                }
                                else {
                                    swal({ title: "Alert", text: "This Record Alraedy Exists...", showCloseButton: true, width: 380 });
                                }

                                $http.get(ENV.apiUrl + "api/student/studentexam/getStudentsExamDetails?curcode=" + $scope.edt.sims_cur_code +
                          "&boardcode=" + $scope.edt.sims_board_code +
                          "&academicyear=" + $scope.edt.sims_academic_year +
                          "&boardexamcode=" + $scope.edt.sims_board_exam_name).then(function (Student_Data) {

                              $scope.StudenteData = Student_Data.data;
                              $scope.totalItems = $scope.StudenteData.length;
                              $scope.todos = $scope.StudenteData;
                              $scope.makeTodos();
                          });


                            });
                        }
                        $scope.enroll_number = [];
                        sims_student_enrolls = [];
                        data1 = [];
                    }
                    else {

                        swal('', 'Select Atleast One Student To Insert Record');
                    }
                }
            }

            $scope.Update = function () {
                debugger
                //  if (myForm) {
                var data = $scope.edt;
                data.opr = 'U';
                data1.push(data);
                $http.post(ENV.apiUrl + "api/student/studentexam/StudentBoardCUD", data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    $scope.BoardData1 = false;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Record Updated Successfully", showCloseButton: true, width: 300, height: 200 });
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Updated", showCloseButton: true, width: 300, height: 200 });
                    }
                    $http.get(ENV.apiUrl + "api/student/studentexam/getStudentsExamDetails?curcode=" + $scope.edt.sims_cur_code +
                      "&boardcode=" + $scope.edt.sims_board_code +
                      "&academicyear=" + $scope.edt.sims_academic_year +
                      "&boardexamcode=" + $scope.edt.sims_board_exam_name).then(function (Student_Data) {

                          $scope.StudenteData = Student_Data.data;
                          $scope.totalItems = $scope.StudenteData.length;
                          $scope.todos = $scope.StudenteData;
                          $scope.makeTodos();
                      });

                })
                $scope.display = false;
                $scope.grid = true;
            }
            //  }
            $scope.Delete = function () {
                Student_bacth_code = [];
                $scope.enroll_number = [];

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_batch_enroll_number;
                    var v = document.getElementById(t);

                    if (v.checked == true)

                        Student_bacth_code = Student_bacth_code + $scope.filteredTodos[i].sims_batch_enroll_number + ',';
                }

                var deletebacthcode = ({
                    'sims_batch_enroll_number': Student_bacth_code,
                    'opr': 'D'
                });


                main.checked = false;
                $scope.check();
                Student_bacth_code = [];
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.DeleteRecord = function () {
                debugger;
                var data1 = [];
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_board_exam_enroll_number': $scope.filteredTodos[i].sims_board_exam_enroll_number,
                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger;
                            $http.post(ENV.apiUrl + "api/student/studentexam/StudentBoardCUD", deleteleave).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/student/studentexam/getStudentsExamDetails?curcode=" + $scope.edt.sims_cur_code +
                                                  "&boardcode=" + $scope.edt.sims_board_code +
                                                  "&academicyear=" + $scope.edt.sims_academic_year +
                                                  "&boardexamcode=" + $scope.edt.sims_board_exam_name).then(function (Student_Data) {
                                                      $scope.StudenteData = Student_Data.data;
                                                      $scope.totalItems = $scope.StudenteData.length;
                                                      $scope.todos = $scope.StudenteData;
                                                      $scope.makeTodos();
                                                  });
                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                                else {
                                    //if(){}
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/student/studentexam/getStudentsExamDetails?curcode=" + $scope.edt.sims_cur_code +
                                                   "&boardcode=" + $scope.edt.sims_board_code +
                                                   "&academicyear=" + $scope.edt.sims_academic_year +
                                                   "&boardexamcode=" + $scope.edt.sims_board_exam_name).then(function (Student_Data) {
                                                       $scope.StudenteData = Student_Data.data;
                                                       $scope.totalItems = $scope.StudenteData.length;
                                                       $scope.todos = $scope.StudenteData;
                                                       $scope.makeTodos();
                                                   });
                                        }

                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                data1 = [];
                $scope.row1 = '';
                main.checked = false;
                $scope.currentPage = str;
            }

            $scope.getGrid = function () {


            }

            $scope.cancel = function () {
                $scope.display = false;
                $scope.grid = true;
                $scope.MyForm.$setPristine();
                $scope.MyForm.$setUntouched();
            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.SearchStudentWindow = function () {
                $scope.searchtable = false;
                $scope.student = '';
                $('#MyModal').modal('show');
                $scope.temp = '';
            }

            $scope.SearchSudent = function () {

                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    main.checked = false;
                }


                $scope.searchtable = false;
                $scope.busy = true;
                $scope.sibling_result
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.temp)).then(function (Allstudent) {

                    $scope.student = Allstudent.data;
                    $scope.searchtable = true;
                    $scope.busy = false;


                });
            }

            $scope.searched = function (valLists, toSearch) {

                debugger;

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_student_passport_first_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.Reset = function () {
                $scope.temp = '';
                $scope.searchtable = false;
            }

            $scope.ClearCheckBox = function () {

                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].sims_batch_enroll_number;
                        var v = document.getElementById(t);
                        v.checked = false;
                        Student_bacth_code = [];
                        $scope.row1 = '';

                    }
                    main.checked = false;
                }



            }

            $scope.RemoveEnrollMentNo = function ($event, index, str) {

                str.splice(index, 1);

            }

            $scope.MultipleStudentSelect = function () {

                $scope.enroll_number = [];
                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.student.length; i++) {
                        var t = $scope.student[i].s_enroll_no;
                        var v = document.getElementById(t + i);
                        v.checked = true;

                        $scope.enroll_number.push($scope.student[i]);
                    }
                }
                else {
                    for (var i = 0; i < $scope.student.length; i++) {
                        var t = $scope.student[i].s_enroll_no;
                        var v = document.getElementById(t + i);
                        v.checked = false;
                        $scope.enroll_number = [];
                    }
                }


            }

            $scope.DataEnroll = function () {
                debugger
                for (var i = 0; i < $scope.student.length; i++) {
                    var t = $scope.student[i].s_enroll_no;
                    var v = document.getElementById(t + i);
                    if (v.checked == true)
                        $scope.enroll_number.push($scope.student[i]);
                }
                $scope.student = [];
            }

        }])
})();