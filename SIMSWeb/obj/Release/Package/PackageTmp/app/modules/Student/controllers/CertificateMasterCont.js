﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var user_type = 'S'
    var issueno;

    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CertificateMasterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            debugger;
            var username = $rootScope.globals.currentUser.username;
            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();

            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/CertificateMaster/getCertificate").then(function (res1) {
                $scope.CertificateData = res1.data;
                $scope.totalItems = $scope.CertificateData.length;
                $scope.todos = $scope.CertificateData;
                $scope.makeTodos();

            });

          
            $scope.getgrid = function () {
                debugger;

                $http.get(ENV.apiUrl + "api/CertificateMaster/getCertificate").then(function (res1) {
                    $scope.CertificateData = res1.data;
                    $scope.totalItems = $scope.CertificateData.length;
                    $scope.todos = $scope.CertificateData;
                    $scope.makeTodos();

                });

            }

            $scope.getDataById = function (str) {
                debugger;

                $http.get(ENV.apiUrl + "api/CertificateMaster/getCertificateByNo?certificate_no=" + str).then(function (res1) {
                    $scope.CertificateDataById = res1.data;
                    $('#text-editor').data('wysihtml5').editor.setValue($scope.CertificateDataById.sims_certificate_header1);
                    $('#text-editor1').data('wysihtml5').editor.setValue($scope.CertificateDataById.sims_certificate_header2);
                    $('#text-editor2').data('wysihtml5').editor.setValue($scope.CertificateDataById.sims_certificate_header3);

                    $('#body1').data('wysihtml5').editor.setValue($scope.CertificateDataById.sims_certificate_body1);
                    $('#body2').data('wysihtml5').editor.setValue($scope.CertificateDataById.sims_certificate_body2);
                    $('#body3').data('wysihtml5').editor.setValue($scope.CertificateDataById.sims_certificate_body3);
                    $('#body4').data('wysihtml5').editor.setValue($scope.CertificateDataById.sims_certificate_body4);
                    $('#body5').data('wysihtml5').editor.setValue($scope.CertificateDataById.sims_certificate_body5);

                    $('#Footer1').data('wysihtml5').editor.setValue($scope.CertificateDataById.sims_certificate_footer1);
                    $('#Footer2').data('wysihtml5').editor.setValue($scope.CertificateDataById.sims_certificate_footer2);
                    $('#Footer3').data('wysihtml5').editor.setValue($scope.CertificateDataById.sims_certificate_footer3);

                    $('#Signatory1').data('wysihtml5').editor.setValue($scope.CertificateDataById.sims_certificate_signatory1);
                    $('#Signatory2').data('wysihtml5').editor.setValue($scope.CertificateDataById.sims_certificate_signatory2);
                    $('#Signatory3').data('wysihtml5').editor.setValue($scope.CertificateDataById.sims_certificate_signatory3);

                    $scope.edt = {
                        header4: $scope.CertificateDataById.sims_certificate_header4,
                        header5: $scope.CertificateDataById.sims_certificate_header5,

                        body6: $scope.CertificateDataById.sims_certificate_body6,
                        body7: $scope.CertificateDataById.sims_certificate_body7,
                        body8: $scope.CertificateDataById.sims_certificate_body8,
                        body9: $scope.CertificateDataById.sims_certificate_body9,
                        body10: $scope.CertificateDataById.sims_certificate_body10,
                        body11: $scope.CertificateDataById.sims_certificate_body11,
                        body12: $scope.CertificateDataById.sims_certificate_body12,
                        body13: $scope.CertificateDataById.sims_certificate_body13,
                        body14: $scope.CertificateDataById.sims_certificate_body14,
                        body15: $scope.CertificateDataById.sims_certificate_body15,

                        footer4: $scope.CertificateDataById.sims_certificate_footer4,
                        footer5: $scope.CertificateDataById.sims_certificate_footer5,

                        signatory4: $scope.CertificateDataById.sims_certificate_signatory4,
                        signatory5: $scope.CertificateDataById.sims_certificate_signatory5,



                    }

                    $scope.temp = {
                        sims_certificate_number: $scope.CertificateDataById.sims_certificate_number,
                        sims_cur_code: $scope.CertificateDataById.sims_cur_code,
                        sims_certificate_name: $scope.CertificateDataById.sims_certificate_name,
                        sims_certificate_desc: $scope.CertificateDataById.sims_certificate_desc,
                        sims_certificate_watermark: $scope.CertificateDataById.sims_certificate_watermark,
                        status: $scope.CertificateDataById.sims_certificate_status,

                    };
                    $scope.table = false;
                    $scope.display = true;
                    $scope.save_btn = false;
                    $scope.Update_btn = true;

                });

            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
                //$scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;
           
            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Bind Combo

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCurriculum) {
                $scope.Curriculum = getCurriculum.data;
                console.log($scope.Curriculum);
            });

            //HTML5 editor
            $('#text-editor').wysihtml5();
            $('#text-editor1').wysihtml5();
            $('#text-editor2').wysihtml5();
            $('#body1').wysihtml5();
            $('#body2').wysihtml5();
            $('#body3').wysihtml5();
            $('#body4').wysihtml5();
            $('#body5').wysihtml5();
            $('#Footer1').wysihtml5();
            $('#Footer2').wysihtml5();
            $('#Footer3').wysihtml5();
            $('#Signatory1').wysihtml5();
            $('#Signatory2').wysihtml5();
            $('#Signatory3').wysihtml5();

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.CertificateData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CertificateData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sup_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sdt_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sd_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {

                $scope.disabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                
                $scope.temp = [];
                $scope.edt = [];
               
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $('#tab1').addClass('active');
                $('#step1').addClass('active');
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = [];
                $scope.edt = [];
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $('#tab1').removeClass('active');
                $('#tab2').removeClass('active');
                $('#tab3').removeClass('active');
                $('#tab4').removeClass('active');
                $('#step1').removeClass('active');
                $('#step2').removeClass('active');
                $('#step3').removeClass('active');
                $('#step4').removeClass('active');
            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger;
                var certificateno = str.sims_certificate_number;
                $scope.getDataById(certificateno);
                $('#tab1').addClass('active');
                $('#step1').addClass('active');
               
               

            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger;
                if (Myform) {
                    datasend = [];
                    data = [];
                  
                    var data = {
                        sims_certificate_number: $scope.temp.sims_certificate_number,
                        sims_cur_code: $scope.temp.sims_cur_code,
                        sims_certificate_name: $scope.temp.sims_certificate_name,
                        sims_certificate_desc: $scope.temp.sims_certificate_desc,
                        sims_certificate_watermark: $scope.temp.sims_certificate_watermark,
                        sims_certificate_status: $scope.temp.status,
                        sims_certificate_header1: $('#text-editor').val(),
                        sims_certificate_header2: $('#text-editor1').val(),
                        sims_certificate_header3: $('#text-editor2').val(),
                        sims_certificate_header4: $scope.edt.header4,
                        sims_certificate_header5: $scope.edt.header5,
                        sims_certificate_body1: $('#body1').val(),
                        sims_certificate_body2: $('#body2').val(),
                        sims_certificate_body3: $('#body3').val(),
                        sims_certificate_body4: $('#body4').val(),
                        sims_certificate_body5: $('#body5').val(),
                        sims_certificate_body6: $scope.edt.body6,
                        sims_certificate_body7:  $scope.edt.body7,
                        sims_certificate_body8:  $scope.edt.body8,
                        sims_certificate_body9:  $scope.edt.body9,
                        sims_certificate_body10: $scope.edt.body10,
                        sims_certificate_body11: $scope.edt.body11,
                        sims_certificate_body12: $scope.edt.body12,
                        sims_certificate_body13: $scope.edt.body13,
                        sims_certificate_body14: $scope.edt.body14,
                        sims_certificate_body15: $scope.edt.body15,

                        sims_certificate_footer1: $('#Footer1').val(),
                        sims_certificate_footer2: $('#Footer2').val(),
                        sims_certificate_footer3: $('#Footer3').val(),
                        sims_certificate_footer4: $scope.edt.footer4,
                        sims_certificate_footer5: $scope.edt.footer5,

                        sims_certificate_signatory1: $('#Signatory1').val(),
                        sims_certificate_signatory2: $('#Signatory2').val(),
                        sims_certificate_signatory3: $('#Signatory3').val(),
                        sims_certificate_signatory4: $scope.edt.signatory4,
                        sims_certificate_signatory5: $scope.edt.signatory5,
                    }                            
                    data.opr = 'I';                         
                    datasend.push(data);                  

                    $http.post(ENV.apiUrl + "api/CertificateMaster/CUDCertificate", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Inserted Successfully", width: 380 });
                            $scope.getgrid();
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Inserted", width: 380 });
                        }
                        $scope.getgrid()
                    });

                    $scope.table = true;
                    $scope.display = false;
                }

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                debugger;
                if (Myform) {
                    dataforUpdate = [];
                   
                    var data = {
                        sims_certificate_number: $scope.temp.sims_certificate_number,
                        sims_cur_code: $scope.temp.sims_cur_code,
                        sims_certificate_name: $scope.temp.sims_certificate_name,
                        sims_certificate_desc: $scope.temp.sims_certificate_desc,
                        sims_certificate_watermark: $scope.temp.sims_certificate_watermark,
                        sims_certificate_status: $scope.temp.status,
                        sims_certificate_header1: $('#text-editor').val(),
                        sims_certificate_header2: $('#text-editor1').val(),
                        sims_certificate_header3: $('#text-editor2').val(),
                        sims_certificate_header4: $scope.edt.header4,
                        sims_certificate_header5: $scope.edt.header5,
                        sims_certificate_body1: $('#body1').val(),
                        sims_certificate_body2: $('#body2').val(),
                        sims_certificate_body3: $('#body3').val(),
                        sims_certificate_body4: $('#body4').val(),
                        sims_certificate_body5: $('#body5').val(),
                        sims_certificate_body6: $scope.edt.body6,
                        sims_certificate_body7: $scope.edt.body7,
                        sims_certificate_body8: $scope.edt.body8,
                        sims_certificate_body9: $scope.edt.body9,
                        sims_certificate_body10: $scope.edt.body10,
                        sims_certificate_body11: $scope.edt.body11,
                        sims_certificate_body12: $scope.edt.body12,
                        sims_certificate_body13: $scope.edt.body13,
                        sims_certificate_body14: $scope.edt.body14,
                        sims_certificate_body15: $scope.edt.body15,

                        sims_certificate_footer1: $('#Footer1').val(),
                        sims_certificate_footer2: $('#Footer2').val(),
                        sims_certificate_footer3: $('#Footer3').val(),
                        sims_certificate_footer4: $scope.edt.footer4,
                        sims_certificate_footer5: $scope.edt.footer5,

                        sims_certificate_signatory1: $('#Signatory1').val(),
                        sims_certificate_signatory2: $('#Signatory2').val(),
                        sims_certificate_signatory3: $('#Signatory3').val(),
                        sims_certificate_signatory4: $scope.edt.signatory4,
                        sims_certificate_signatory5: $scope.edt.signatory5,
                    }
                    data.opr = "U";
                    dataforUpdate.push(data);
                    //dataupdate.push(data);
                    $http.post(ENV.apiUrl + "api/CertificateMaster/CUDCertificate", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 380 });
                            $scope.BindGrid(user_type);
                            $scope.getgrid();
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 380 });
                            $scope.BindGrid(user_type);
                        }

                    });
                    dataforUpdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].sims_certificate_number);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].sims_certificate_number);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            //Image Upload

            var formdata = new FormData();

            $scope.getTheFiles = function ($files) {
                $scope.filesize = true;

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);

                    var i = 0;
                    if ($files[i].size > 800000) {
                        $scope.filesize = false;
                        $scope.edt.photoStatus = false;

                        swal({ title: "Alert", text: "File Should Not Exceed 800Kb.", imageUrl: "assets/img/notification-alert.png", });

                    }
                    else {

                    }

                });
            };

            $scope.file_changed = function (element) {
                var cname = $scope.temp.sims_certificate_name;
                if (cname == undefined || cname == '') {
                    swal({ title: "Alert", text: "Enter Certificate Name", width: 380 });
                }
                else {
                    debugger;
                    var date = new Date();
                    var d = date.getDate();
                    var m = date.getMonth() + 1; //January is 0!
                    var y = date.getFullYear();
                    var tm = date.getMinutes();
                    var s = date.getSeconds();
                    var hr = date.getHours();
                    var ms = date.getMilliseconds();



                    console.log(t);
                    $scope.ImgLoaded = true;
                    var photofile = element.files[0];
                    $scope.photo_filename = (photofile.type);
                    var t = $scope.photo_filename.split("/")[1];
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $scope.$apply(function () {
                            $scope.prev_img = e.target.result;

                            $scope.temp.sims_certificate_watermark = cname + '-' + y + '' + m + '' + d + '' + hr + '' + tm + '' + s + '' + ms + "." + t;

                        });
                    };
                    reader.readAsDataURL(photofile);
                }
            };

            $scope.uploadImgClickF = function () {
                $scope.uploadImgClickFShow = true;
            }

            $scope.uploadImgCancel = function () {
                $scope.uploadImgClickFShow = false;
            }

            $scope.UploadImageModal = function (str) {
                $scope.admissionno = str.admis_num;
                $scope.criteriacode = str.sims_criteria_code;
                $scope.filename = str.admis_num + '_' + str.sims_criteria_code + '_' + str.count_rec;
                $scope.checkupload = str.if_uploaded;
                $('#myModal').modal('show');
            }

            $scope.Upload = function () {
                debugger;
                var data = [];
                var t = $scope.photo_filename.split("/")[1];
                console.log(t);
                data = {
                    sims_admission_doc_path: $scope.filename + "." + t,
                    admis_num: $scope.admissionno,
                    sims_criteria_code: $scope.criteriacode,
                    opr: "U"
                }

                console.log(data);

                $http.post(ENV.apiUrl + "api/AdmissionDoc/CUDUploadDocument", data).then(function (msg) {
                    $scope.msg1 = msg.data;

                    debugger
                    $('#myModal').modal('hide');
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "File Upload Successfully", width: 300, height: 200 });

                    }


                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + '/api/file/upload?filename=' + data.sims_admission_doc_path + "&location=" + "StudentImages",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    $http(request)
                  .success(function (d) {
                  },
                  function () {
                      alert("Err");
                  });

                },
                    function () {
                        $('#ErrorMessage').modal({ backdrop: "static" });


                    });

            }


            //Delete
            $scope.Delete = function (str) {
                debugger;
                var deletecode = [];
                $scope.flag = false;
                if (str == undefined) {
                    user_type = 'S';
                }
                else {
                    user_type = str;
                }
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    //var t = $scope.filteredTodos[i].uom_code;
                    var v = document.getElementById(i + $scope.filteredTodos[i].sims_certificate_number);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sims_certificate_issue_number': $scope.filteredTodos[i].sims_certificate_issue_number,
                            'opr1': user_type,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {

                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/CertificateMaster/CUDCertificate", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.BindGrid(user_type);

                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;

                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Already Mapped.Can't be Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.BindGrid(user_type);
                                        }
                                    });
                                }

                            });
                        }
                        else {

                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i + $scope.filteredTodos[i].sims_certificate_number);
                                //var t = $scope.filteredTodos[i].sims_fee_category;
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }


                            }

                        }

                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
               
                $scope.currentPage = str;
            }

            $scope.preview = function () {
                debugger;
                var data1 = {
                    sims_certificate_number: $scope.temp.sims_certificate_number,
                    sims_cur_code: $scope.temp.sims_cur_code,
                    sims_certificate_name: $scope.temp.sims_certificate_name,
                    sims_certificate_desc: $scope.temp.sims_certificate_desc,
                    sims_certificate_watermark: $scope.temp.sims_certificate_watermark,
                    sims_certificate_status: $scope.temp.status,
                    sims_certificate_header1: $('#text-editor').val(),
                    sims_certificate_header2: $('#text-editor1').val(),
                    sims_certificate_header3: $('#text-editor2').val(),
                    sims_certificate_header4: $scope.edt.header4,
                    sims_certificate_header5: $scope.edt.header5,
                    sims_certificate_body1: $('#body1').val(),
                    sims_certificate_body2: $('#body2').val(),
                    sims_certificate_body3: $('#body3').val(),
                    sims_certificate_body4: $('#body4').val(),
                    sims_certificate_body5: $('#body5').val(),
                    sims_certificate_body6: $scope.edt.body6,
                    sims_certificate_body7: $scope.edt.body7,
                    sims_certificate_body8: $scope.edt.body8,
                    sims_certificate_body9: $scope.edt.body9,
                    sims_certificate_body10: $scope.edt.body10,
                    sims_certificate_body11: $scope.edt.body11,
                    sims_certificate_body12: $scope.edt.body12,
                    sims_certificate_body13: $scope.edt.body13,
                    sims_certificate_body14: $scope.edt.body14,
                    sims_certificate_body15: $scope.edt.body15,

                    sims_certificate_footer1: $('#Footer1').val(),
                    sims_certificate_footer2: $('#Footer2').val(),
                    sims_certificate_footer3: $('#Footer3').val(),
                    sims_certificate_footer4: $scope.edt.footer4,
                    sims_certificate_footer5: $scope.edt.footer5,

                    sims_certificate_signatory1: $('#Signatory1').val(),
                    sims_certificate_signatory2: $('#Signatory2').val(),
                    sims_certificate_signatory3: $('#Signatory3').val(),
                    sims_certificate_signatory4: $scope.edt.signatory4,
                    sims_certificate_signatory5: $scope.edt.signatory5,
                }
                debugger;
                
                $scope.preview1 = data1;
              
                $('#myModal').modal('show');
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                formate: "yyyy-mm-dd"

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.next1 = function () {
                debugger
                $('#tab2').addClass('active');
                $('#tab1').removeClass('active');
                $('#step2').addClass('active');
                $('#step1').removeClass('active');
            }

            $scope.next2 = function () {

                $('#tab3').addClass('active');
                $('#tab2').removeClass('active');
                $('#step3').addClass('active');
                $('#step2').removeClass('active');
            }

            $scope.next3 = function () {

                $('#tab4').addClass('active');
                $('#tab3').removeClass('active');
                $('#step4').addClass('active');
                $('#step3').removeClass('active');
            }

            $scope.prev1 = function () {
                $('#tab1').addClass('active');
                $('#tab2').removeClass('active');
                $('#step1').addClass('active');
                $('#step2').removeClass('active');
            }

            $scope.prev2 = function () {
                $('#tab2').addClass('active');
                $('#tab3').removeClass('active');
                $('#step2').addClass('active');
                $('#step3').removeClass('active');
            }
        }])

})();
