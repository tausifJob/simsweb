﻿
(function () {
    'use strict';
    var demo = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('HouseAllocationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '5';
            $scope.busyindicator = true;
            $scope.not_student = false;
            $scope.btn_submit = false

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                console.log($scope.curriculum);
                $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                $scope.getAccYear($scope.edt.sims_cur_code);
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.edt.sims_cur_code).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;

                    $scope.edt = {
                        sims_cur_code: $scope.curriculum[0].sims_cur_code,
                        sims_academic_year: $scope.Acc_year[0].sims_academic_year
                    };
                    $scope.getGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                });
            }

            $scope.getGrade = function (curCode, accYear) {

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;

                    //$scope.edt = {
                    //    sims_cur_code: $scope.curriculum[0].sims_cur_code,
                    //    sims_academic_year: $scope.Acc_year[0].sims_academic_year,
                    //    sims_grade_code: $scope.Grade_code[0].sims_grade_code
                    //};
                    //$scope.getSection($scope.edt.sims_cur_code, $scope.edt.sims_academic_year,$scope.edt.sims_grade_code);
                });
            }

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;

                    //$scope.edt = {
                    //    sims_cur_code: $scope.curriculum[0].sims_cur_code,
                    //    sims_academic_year: $scope.Acc_year[0].sims_academic_year,
                    //    sims_grade_code: $scope.Grade_code[0].sims_grade_code,
                    //    sims_section_code: $scope.Section_code[0].sims_section_code
                    //};
                });
            }

            $scope.Show_Data = function (curcode, academicyear, gradecode, section) {
                $scope.btn_submit = true;
                var demo1 = [];
                $scope.busyindicator = false;
                $scope.info1 = [];
                $scope.table = false;
                $scope.subject = false;
                debugger;
                if (curcode != "" && academicyear != "" &&
                    gradecode != "" && section != "")
                    if (curcode != undefined && academicyear != undefined &&
                        gradecode != undefined && section != undefined) {

                        $http.get(ENV.apiUrl + "api/HouseAllocation/getStudentNameHA?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_code).then(function (allSectionStudent) {
                            $scope.Studentdata = allSectionStudent.data;
                            if ($scope.Studentdata.length > 0) {
                                $scope.house_name = $scope.Studentdata[0].house_lst;
                                $scope.busyindicator = true;
                                $scope.table = true;

                            }
                            else {
                                $scope.busyindicator = true;
                                $scope.btn_submit = false;
                                $scope.table = false;
                                swal({ text: 'Record Not Found', width: 300, showCloseButton: true });
                            }
                        });

                    }
                    else {
                        $scope.busyindicator = true;
                        $scope.btn_submit = false;
                        $scope.table = false;
                        swal({ text: 'Please select Grade and Section', width: 300, showCloseButton: true });
                    }
            }

            $scope.Submit = function () {
                var data = [];

                for (var i = 0; i < $scope.Studentdata.length; i++) {
                    for (var j = 0; j < $scope.Studentdata[i].house_lst.length; j++) {

                        if ($scope.Studentdata[i].house_lst[j].sims_status == true) {
                            var objhouse = {
                                house_code: $scope.Studentdata[i].house_lst[j].house_code,
                                enroll: $scope.Studentdata[i].enroll,
                            }
                            data.push(objhouse);
                        }
                    }
                }

                $http.post(ENV.apiUrl + "api/HouseAllocation/HouseAllOcCUD", data).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "House assigned successfully", showCloseButton: true, width: 380, });
                    }
                    else {
                        swal({ text: "House not assigned", showCloseButton: true, width: 380, });
                    }
                });
                $scope.table = false;
            }

            $scope.check_all = function (str) {
                for (var i = 0; i < $scope.Studentdata.length; i++) {
                    for (var j = 0; j < $scope.Studentdata[i].house_lst.length; j++) {
                        if ($scope.Studentdata[i].house_lst[j].house_code == str) {
                            $scope.Studentdata[i].house_lst[j].sims_status = true;
                        }
                        else {
                            $scope.Studentdata[i].house_lst[j].sims_status = false;
                        }
                    }
                }
            }

            $scope.checkboxCheck = function (str, no) {
                for (var i = 0; i < $scope.Studentdata.length; i++) {
                    if ($scope.Studentdata[i].enroll == no) {
                        for (var j = 0; j < $scope.Studentdata[i].house_lst.length; j++) {
                            if ($scope.Studentdata[i].house_lst[j].house_code == str) {
                                $scope.Studentdata[i].house_lst[j].sims_status = true;
                            }
                            else {
                                $scope.Studentdata[i].house_lst[j].sims_status = false;
                            }
                        }
                    }
                }
            }

            $scope.Reset = function () {
                $scope.table = false;
                $scope.btn_submit = false;
                $scope.edt.sims_grade_code = '';
                $scope.edt.sims_section_code = '';
                //$scope.edt = {
                //    sims_grade_code: '',
                //    sims_section_name: ''
                //}
            }
        }])
})();