﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ProspectDashboardCont',
        ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.display = true;
            $scope.grid = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.pagesize1 = "5";
            $scope.pageindex1 = "1";
            $scope.flag = true;
            var str, cnt;
            $scope.dash = [];
            $scope.view = [];
            var admdetails = [];
            $scope.btn_edit = true;
            var del = [];
            var t = false;
            $scope.comm_btn = true;
            $scope.pros_btn = true;
            $scope.display1 = false;
            $scope.grid1 = true;
            var main, section = "", fee_category = "";
            $scope.filesize = true;
            var date = new Date();
            $scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.once = true;
            var param = $stateParams.Class;
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;
            $scope.filteredTodos1 = [], $scope.currentPage1 = 1, $scope.numPerPage1 = 5, $scope.maxSize1 = 5;

            $timeout(function () {
                $("#fixedtable,#fixedtable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            if (param != undefined) {
                $scope.edt =
                    {
                        curr_code: param.curr_code,
                        academic_year: param.academic_year,
                        grade_code: param.grade_code,
                        sims_appl_parameter_reg: param.sims_appl_parameter_reg
                    };
            }

            $scope.Cancel = function () {
                $scope.view = "";
            }

            $scope.link = function (str) {
                $state.go("main.ProspectAdmission", { Pros_num: str, Class: $scope.edt });
            }

            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.obj2 = res.data;
                $scope.edt['curr_code'] = $scope.obj2[0].sims_attendance_cur_code;
                $scope.curr_code = $scope.obj2[0].sims_attendance_cur_code;
                $scope.getCur($scope.edt.curr_code);
            });

            $scope.getCur = function (cur_code) {
                // $scope.edt.sims_appl_parameter_reg = "";

                $http.get(ENV.apiUrl + "api/common/getAllAcademicYearDash?cur_code=" + cur_code).then(function (res) {
                    $scope.obj1 = res.data;
                    $scope.edt['academic_year'] = $scope.obj1[0].sims_academic_year;
                    $scope.acad_yr = $scope.obj1[0].sims_academic_year;
                    $scope.GetGrade(cur_code, $scope.acad_yr, $scope.edt.grade_code, $scope.edt.sims_appl_parameter_reg);
                });

                $scope.GetInfo(cur_code, $scope.acad_yr, $scope.edt.grade_code, $scope.edt.sims_appl_parameter_reg);
            }

            $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/getRegistration").then(function (res) {
                $scope.reg = res.data;
                console.log($scope.reg);
            });

            //Get Dashboard Details

            $scope.GetInfo = function (cur_code, AcadmicYear, gradeCode, reg)
            {
                
                $scope.filteredTodos = [];
                if (reg == '1')//W
                {
                    $http.get(ENV.apiUrl + "api/common/ProspectDashboard/GetProspectDetail?curr_code=" + cur_code + "&AcadmicYear=" + AcadmicYear + "&gradeCode=" + gradeCode + "&admission_status=A").then(function (res) {
                        $scope.dash = res.data;
                        $scope.totalItems = $scope.dash.length;
                        $scope.todos = $scope.dash;
                        $scope.makeTodos();
                        $scope.grid = true;
                    });
                }
                if (reg == '2')//R
                {
                    $http.get(ENV.apiUrl + "api/common/ProspectDashboard/GetProspectDetail?curr_code=" + cur_code + "&AcadmicYear=" + AcadmicYear + "&gradeCode=" + gradeCode + "&admission_status=R").then(function (res) {
                        $scope.dash = res.data;
                        $scope.totalItems = $scope.dash.length;
                        $scope.todos = $scope.dash;
                        $scope.makeTodos();
                        $scope.grid = true;

                    });
                }
            }

            $scope.GetGrade = function (cur, acad_yr, grade, reg) {
                //$scope.edt.grade_code = "";
                // $scope.edt.sims_appl_parameter_reg = "";
                reg = "";
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + cur + "&academic_year=" + acad_yr).then(function (res) {
                    $scope.obj3 = res.data;
                    //$scope.edt['grade_code'] = $scope.obj3[0].sims_grade_code;
                    //$scope.grade = $scope.obj3[0].sims_grade_code;
                    //$scope.GetGradechage($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.edt.sims_appl_parameter_reg);
                });

                $scope.GetInfo(cur, acad_yr, grade, reg);
            }

            $scope.GetGrade(param.curr_code, param.academic_year);

            $scope.GetGradechage = function (cur, acad_yr, grade, reg) {
                $scope.edt.sims_appl_parameter_reg = "";
                reg = "";

                $scope.GetInfo(cur, acad_yr, grade, reg);
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.dash, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.dash;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.pros_num.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.stud_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.pros_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.fatherName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                       item.grade_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.gender.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.birth_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.nation.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.sibling_enroll.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_admission_quota_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.GetInfo(param.curr_code, param.academic_year, param.grade_code, param.sims_appl_parameter_reg);

            $scope.check1 = function (dash) {
                console.log(dash);
                admdetails = dash.pros_num;
                var v = document.getElementById(dash.pros_num);
                //if (dash.pros_num1 == true)
                if (v.checked == true) {
                    $scope.comm_btn = false;
                    $scope.pros_btn = false;
                }
                else {
                    $scope.comm_btn = true;
                    $scope.pros_btn = true;
                }
                // if (dash.curr_code == '' || dash.academic_year == '' || dash.grade_code == '')
                // {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        // if (dash.curr_code == '' || dash.academic_year == '' || dash.grade_code == '')
                        // {
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                        // }
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }

                });
                // }

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.check = function () {
                main = document.getElementById('mainchk');
                del = [];
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].pros_num);

                        //if ($scope.filteredTodos[i].curr_code == '' || $scope.filteredTodos[i].academic_year == '' || $scope.filteredTodos[i].grade_code == '')
                        //{
                        //    v.checked = false;
                        //    var index = del.indexOf($scope.filteredTodos[i].pros_num);

                        //    if (index > -1)
                        //    {
                        //        del.splice(index, 3);
                        //    }
                        //}
                        //else
                        //{
                        v.checked = true;
                        $scope.comm_btn = false;
                        del.push($scope.filteredTodos[i].pros_num);
                        $('tr').addClass("row_selected");
                        // }
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].pros_num;
                        var v = document.getElementById(t);
                        v.checked = false;
                        del.pop(t);
                        // $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }
            }

            //Promote the Admissions
            $scope.getPromote = function () {
                admdetails = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    // var t = $scope.filteredTodos[i].pros_num;
                    //var v = document.getElementById(t);

                    if ($scope.filteredTodos[i].pros_num1 == true) {
                        admdetails = admdetails + $scope.filteredTodos[i].pros_num + ',';
                    }
                }
                if (admdetails.length == 0) {
                    swal({ title: "Alert", text: "Nothing Selected", showCloseButton: true, width: 380, });
                }
                else {

                    var data = $scope.edt;
                    data.pros_num = admdetails;
                    console.log(data);
                    if (admdetails.length > 0) {
                        $http.post(ENV.apiUrl + "api/common/ProspectDashboard/ApproveMultiple", data).then(function (res) {
                            $scope.promote = res.data;
                            console.log($scope.promote);
                            $scope.GetInfo($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.edt.sims_appl_parameter_reg);
                        });
                    }
                }

            }

            $scope.MoveToAdmission = function (str) {
                var data = $scope.edt;
                data.pros_num = str.pros_num;
                console.log(data);

                swal({
                    title: '',
                    text: "Are you sure you want Move to Admission ?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        $http.post(ENV.apiUrl + "api/common/ProspectDashboard/ApproveMultiple", data).then(function (res) {
                            $scope.promote = res.data;
                            console.log($scope.promote);
                            $scope.GetInfo($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.edt.sims_appl_parameter_reg);
                        });
                    }
                });

            }

            //Reject the Admissions
            $scope.getReject = function () {
                admdetails = [];

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    //var t = $scope.filteredTodos[i].pros_num;
                    // var v = document.getElementById(t);

                    //if (v.checked == true)
                    if ($scope.filteredTodos[i].pros_num1 == true) {
                        admdetails = admdetails + $scope.filteredTodos[i].pros_num + ',';
                    }
                }
                if (admdetails.length == 0) {
                    swal({ title: "Alert", text: "Please select atleast 1 applicant", showCloseButton: true, width: 380, });
                    //$('#message').modal({ backdrop: 'static', keyboard: false });
                }
                else {
                    swal({
                        title: '',
                        text: "The selected applications will be rejected",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.OkRejectadm();
                        }
                    });
                }
            }

            $scope.OkRejectadm = function () {
                var data = $scope.edt;
                data.pros_num = admdetails;

                $http.post(ENV.apiUrl + "api/common/ProspectDashboard/AdmissionStatusOpr", data).then(function (res) {
                    $scope.display = true;
                    $scope.msg1 = res.data;
                    console.log($scope.msg1);

                    if ($scope.msg1 > 0) {
                        $scope.GetInfo($scope.edt.curr_code, $scope.edt.academic_year, $scope.edt.grade_code, $scope.edt.sims_appl_parameter_reg);
                    }
                });
            }

            $scope.getcommunication = function (pros_No) {
                $scope.prospectNo = pros_No;
                $('#commnModal').modal({ backdrop: 'static', keyboard: true });

                $http.get(ENV.apiUrl + "api/common/ProspectDashboard/GetCommMethods").then(function (res) {
                    $scope.method_data = res.data;
                });

                $http.get(ENV.apiUrl + "api/common/ProspectDashboard/getCommunication?pros_no=" + $scope.prospectNo).then(function (res) {
                    $scope.comm_data = res.data;
                    $scope.totalItems1 = $scope.comm_data.length;
                    $scope.todos1 = $scope.comm_data;
                    $scope.makeTodos1();
                    $scope.grid1 = true;
                    $scope.display1 = false;
                });

            }

            $scope.makeTodos1 = function () {
                var rem = parseInt($scope.totalItems1 % $scope.numPerPage1);
                if (rem == '0') {
                    $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1);
                }
                else {
                    $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1) + 1;
                }
                var begin1 = (($scope.currentPage1 - 1) * $scope.numPerPage1);
                var end1 = parseInt(begin1) + parseInt($scope.numPerPage1);

                $scope.filteredTodos1 = $scope.todos1.slice(begin1, end1);
            };

            $scope.size1 = function (str) {
                console.log(str);
                $scope.pagesize1 = str;
                $scope.currentPage1 = 1;
                $scope.numPerPage1 = str;
                console.log("numPerPage=" + $scope.numPerPage1);
                $scope.makeTodos1();
            }

            $scope.index1 = function (str) {
                $scope.pageindex1 = str;
                $scope.currentPage1 = str;
                console.log("currentPage1=" + $scope.currentPage1);
                $scope.makeTodos1();
            }

            $scope.New = function () {
                $scope.display1 = true;
                $scope.grid1 = false;
                $scope.temp = "";
            }

            $scope.commcancel_btn = function () {
                $scope.grid1 = true;
                $scope.display1 = false;
                $scope.myForm1.$setPristine();
                $scope.myForm1.$setUntouched();
            }

            $scope.commOk_btn = function (isvalidate) {
                var commdata1 = [];
                var commdata = [];

                if (isvalidate) {
                    var commdata = ({
                        pros_num: $scope.prospectNo,
                        comm_method: $scope.temp.comm_method,
                        comm_date: $scope.temp.comm_date,
                        comm_desc: $scope.temp.comm_desc,
                        enq_rem: $scope.temp.enq_rem,
                        status: $scope.temp.status,
                        opr: 'I'
                    });

                    commdata1.push(commdata);

                    $http.post(ENV.apiUrl + "api/common/ProspectDashboard/CUDCommunication", commdata1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Prospect Communication Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getcommunication($scope.prospectNo);
                                }
                            });
                        }
                        else {
                            swal({ title: "Alert", text: "Prospect Communication Not Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getcommunication($scope.prospectNo);
                                }
                            });
                        }
                    });

                    $scope.myForm1.$setPristine();
                    $scope.myForm1.$setUntouched();
                }
            }

            $scope.searched1 = function (valLists, toSearch) {

                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil1(i, toSearch);
                });
            };

            $scope.search1 = function () {
                $scope.todos1 = $scope.searched1($scope.comm_data, $scope.searchText1);
                $scope.totalItems1 = $scope.todos1.length;
                $scope.currentPage1 = '1';
                if ($scope.searchText1 == '') {
                    $scope.todos1 = $scope.comm_data;
                }
                $scope.makeTodos1();
            }

            function searchUtil1(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.comm_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.comm_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.getCommunicate = function () {
                admdetails = [];
                var adm_data = [];


                for (var i = 0; i < $scope.filteredTodos.length; i++)
                {
                    //var v = document.getElementById($scope.filteredTodos[i].pros_num);

                    //if ($scope.filteredTodos[i].pros_num1 == true)
                    //{
                    var t = $scope.filteredTodos[i].pros_num;
                    var v = document.getElementById(t);

                    if (v.checked == true)
                    {
                        adm_data.push($scope.filteredTodos[i].pros_num);
                        admdetails = admdetails + $scope.filteredTodos[i].pros_num + ',';

                    }
                }

                $scope.email = [];
                //if (adm_data.length <= 1)
                //{
                    $('#commnDetailsModal').modal({ backdrop: 'static', keyboard: true });

                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetAdmissionTemplates").then(function (res) {
                        $scope.template_data = res.data;
                        for (var i = 0; i < res.data.length; i++) {
                            if ($scope.template_data[i].sims_msg_subject == 'DPS-MIS Admission Department') {
                                $scope.email['sims_msg_subject'] = $scope.template_data[i].sims_msg_subject;
                            }
                        }
                        $scope.getbody($scope.email['sims_msg_subject']);
                    });

                    $http.get(ENV.apiUrl + "api/common/ProspectDashboard/GetAdmission_EmailIds?pros_nos=" + admdetails).then(function (res) {
                        $scope.emailId_data = res.data;
                    });


               // }
                //else
                //{
                //    swal({ title: "Alert", text: "Should Not be communicate with Multiple Prospect No.", showCloseButton: true, width: 380, });
                //    admdetails = [];
                //    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                //        // var v = document.getElementById($scope.filteredTodos[i].pros_num);
                //        $scope.filteredTodos[i].pros_num1 = false;

                //    }
                //    $scope.comm_btn = true;
                //    $('tr').removeClass("row_selected");
                //}
            }

            $('#text-editor').wysihtml5();

            $scope.getbody = function (msg_type) {
                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetAdmissionTemplatesBody?template_subject=" + msg_type).then(function (res) {
                    $scope.tempBody_data = res.data;
                    var body = $scope.tempBody_data.sims_msg_body;
                    console.log($scope.tempBody_data.sims_msg_sr_no);
                    var v = document.getElementById('text-editor');
                    v.value = body;
                    $scope.email.msgbody = v.value;
                    $scope.flag = false;
                    $('#text-editor').data("wysihtml5").editor.setValue($scope.email.msgbody);

                    $http.get(ENV.apiUrl + "api/common/Email/GetcheckEmailProfile?sr_no=" + $scope.tempBody_data.sims_msg_sr_no).then(function (res) {
                        $scope.emailProfile_data = res.data;
                        console.log($scope.emailProfile_data);
                    });
                });

            }

            $scope.sendMail = function () {
                $scope.email_exists = false;
                var data1 = [];
                var lst_cc = [];

                if ($scope.email.sims_msg_subject != "" || $scope.email.sims_msg_subject != "Template") {
                    $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetAdmissionTemplatesBody?template_subject=" + $scope.email.sims_msg_subject).then(function (res) {
                        $scope.TemplatesBody_data = res.data;

                        $scope.sims_msg_body = $scope.TemplatesBody_data.sims_msg_body;
                        $scope.sims_msg_signature = $scope.TemplatesBody_data.sims_msg_signature;

                        var msgbody = $('#text-editor').val();


                        for (var i = 0; i < $scope.emailId_data.length; i++) {
                            if ($scope.emailId_data[i].chk_email == true) {
                                var pros_number = admdetails;
                                var pro_no = pros_number.indexOf(',')
                                var pro = pros_number.substr(0, pro_no);
                                debugger;
                                var data =
                                   ({
                                       emailsendto: $scope.emailId_data[i].emailid,
                                       body: msgbody,
                                       subject: $scope.email.sims_msg_subject,
                                       comm_desc: msgbody,
                                       pros_num: pro,
                                       enrollnumber: pro,
                                       comm_method: 'E',
                                       comm_date: $scope.ddMMyyyy,
                                       sender_emailid: $scope.emailProfile_data,
                                       sims_recepient_id: '0'
                                   });

                                data1.push(data);

                                $scope.email_exists = true;
                                console.log(data1);
                            }
                            //else
                            //{
                            // swal({ title: "Alert", text: "Unable to Send Mail", showCloseButton: true, width: 380, });
                            // }
                        }

                        //if ($scope.email_exists == false)
                        //{
                        //    swal({ title: "Alert", text: "Unable to Send Mail", showCloseButton: true, width: 380, });
                        //}

                        if ($scope.email.ccTo === "undefined") {
                        }
                        else {
                            var v = [];
                            var s = $scope.email.ccTo;
                            if (s != null) {
                                v = s.split(',');
                            }

                            var pros_number = admdetails;
                            var pro_no = pros_number.indexOf(',');
                            var pro = pros_number.substr(0, pro_no);

                            for (var i = 0; i < v.length; i++) {
                                var pros_number = admdetails.split(',');
                                var data =
                                  ({
                                      emailsendto: v[i],
                                      body: msgbody,
                                      subject: $scope.email.sims_msg_subject,
                                      comm_desc: msgbody,
                                      pros_num: pro,
                                      enrollnumber: '',
                                      comm_method: 'E',
                                      comm_date: $scope.ddMMyyyy,
                                      sender_emailid: $scope.emailProfile_data,
                                      sims_recepient_id:'1'
                                  });

                                data1.push(data);
                            }

                            var data2 =
                                {
                                    attFilename: ''
                                }
                            lst_cc.push(data2);
                        }

                        console.log(data1);

                        $http.post(ENV.apiUrl + "api/common/ProspectDashboard/CUDCommunication", data1).then(function (res) {
                            $scope.Proscomm_data = res.data;
                            if ($scope.Proscomm_data == true) {
                                $http.post(ENV.apiUrl + "api/common/Email/ScheduleMails_prospect?filenames=" + JSON.stringify(lst_cc), data1).then(function (res) {
                                    $scope.ScheduleMails_data = res.data;
                                    if ($scope.ScheduleMails_data == true) {
                                        swal({ title: "Alert", text: "Mail Send Successfully", showCloseButton: true, width: 380, });//.then(function (isConfirm) {
                                        //if (isConfirm) {
                                        $scope.commnModaldisplay();
                                        //    }
                                        //});
                                    }
                                    else {
                                        $scope.commnModaldisplay();
                                    }
                                    $('#commnDetailsModal').modal('hide');
                                });
                            }
                        });
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Template to Send Mail.", showCloseButton: true, width: 380, });
                }
            }

            $scope.commnModaldisplay = function () {
                $scope.email = [];
                $scope.emailId_data = [];
                $('#commnDetailsModal').modal('hide');
                //$scope.div_Communication = false;
                $('#text-editor').data("wysihtml5").editor.clear();

            }

            $scope.Printdashboard = function (pros_no) {
                var data = {
                    location: 'Sims.SIMR41DPSMIS',
                    parameter: { prospect_no: pros_no },
                    state: 'main.Sim567'
                }

                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter');
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
        }])


})();