﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var srno;
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AdmissionFeeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            
            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
           
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/AdmissionFee/getAllAdmissionFee").then(function (res1) {
                
                $scope.AdmissionFeeData = res1.data;
                $scope.totalItems = $scope.AdmissionFeeData.length;
                $scope.todos = $scope.AdmissionFeeData;
                $scope.makeTodos();

            });

            //Bind Combo
            $http.get(ENV.apiUrl + "api/AdmissionFee/getAllAccountName").then(function (res1) {
               
                $scope.AllAccountName = res1.data;
                console.log($scope.AllAccountName);
            });
            $http.get(ENV.apiUrl + "api/AdmissionFee/getAllFeeType").then(function (FeeType) {
                debugger;
                $scope.FeeTypeData = FeeType.data;               
                console.log($scope.FeeTypeData);
            });
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCurriculum) {
                $scope.Curriculum = getCurriculum.data;
                //$scope.temp['sims_cur_code'] = $scope.Curriculum[0].sims_cur_code;
                //$scope.getacademicYear($scope.Curriculum[0].sims_cur_code);
                console.log($scope.Curriculum);
            });

            $scope.getacademicYear = function (str) {
                $scope.curriculum_code = str;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (getAcademicYear) {
                    $scope.AcademicYear = getAcademicYear.data;
                    //$scope.temp['sims_academic_year'] = $scope.AcademicYear[0].sims_academic_year;
                   // $scope.getGrade($scope.Curriculum[0].sims_cur_code,$scope.AcademicYear[0].sims_academic_year)
                    console.log($scope.AcademicYear);
                })
            }


            $scope.getGrade = function (str1, str2) {

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str1 + "&academic_year=" + str2).then(function (getAllGrades) {
                    $scope.AllGrades = getAllGrades.data;
                   // $scope.temp['sims_grade_code'] = $scope.AllGrades[0].sims_grade_code;
                    console.log($scope.AllGrades);
                })
            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                //$scope.CheckAllChecked();
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.AdmissionFeeData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.AdmissionFeeData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_grade_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_fee_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||                       
                        item.sims_rev_acc_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_cash_acc_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_fee_amount == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {
                
                $scope.disabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = [];
                $scope.temp.sims_fee_amount = "";
             
                $scope.edt = {
                    copy1: 'A',
                }
                $scope.temp = {
                    status: true,
                    AdjustfeecodeStatus:true,
                }
                
                
                $scope.readonlyAdjustableFeeType = false;
                $scope.CurriculumReadonly = false;
                $scope.AcademicYearReadonly = false;
                $scope.GradeReadonly = false;
                $scope.sims_fee_name1Readonly = false;
               // $scope.temp['sims_cur_code'] = $scope.Curriculum[0].sims_cur_code;
                //$scope.getacademicYear($scope.Curriculum[0].sims_cur_code);
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = [];
                $scope.table = true;
                $scope.display = false;           
                $scope.edt = [];
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {
                $scope.Feecount(str);
                debugger;
                $scope.table = false;
                $scope.display = true;
                $scope.readonlyAdjustableFeeType = true;
                $scope.CurriculumReadonly = true;
                $scope.AcademicYearReadonly = true;
                $scope.GradeReadonly = true;
                $scope.sims_fee_name1Readonly = true;
                $scope.save_btn = false;
                $scope.FeeAmountReadonly = false;
                $scope.Update_btn = true;
                $scope.sdttypeReadonly = true;
                if (str.sims_is_fee_adjustable == false) {
                    $scope.readonlyAdjustableFeeType = true;
                }
                else {
                    $scope.readonlyAdjustableFeeType = false;
                }
                $scope.temp = {
                    status:str.sims_fee_status,
                    AdjustfeecodeStatus:str.sims_is_fee_adjustable,
                    sims_cur_code:str.sims_cur_code,
                    sims_academic_year:str.sims_academic_year,
                    sims_grade_code:str.sims_grade_code,
                    sims_fee_code:str.sims_fee_code,
                    sims_fee_amount:str.sims_fee_amount,
                    sims_rev_acc_no:str.sims_rev_acc_no,
                   
                };
                $scope.edt = {
                    sims_cash_acc_no:str.sims_cash_acc_no,
                    sims_fee_code:str.sims_adjustable_fee_type,
                    copy1:str.is_admission_or_prospect_fee,
                };
                srno = str.sr_no;
                $scope.getacademicYear(str.sims_cur_code);
                $scope.getGrade(str.sims_cur_code, str.sims_academic_year)
            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger;
                $scope.flag1=false;
                if (Myform) {
                    if ($scope.temp.AdjustfeecodeStatus) {
                        if ($scope.edt.sims_fee_code == undefined || $scope.edt.sims_fee_code == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Select Adjustable Fee Type", showCloseButton: true, width: 380, });
                        }
                    }
                    if (!$scope.flag1) {
                        if (!$scope.temp.AdjustfeecodeStatus) {
                            $scope.edt.sims_fee_code = $scope.temp.sims_fee_code;
                        }
                        datasend = [];
                        data = [];
                        // var data = $scope.temp;
                        var data = {
                            sims_fee_status: $scope.temp.status,
                            sims_is_fee_adjustable: $scope.temp.AdjustfeecodeStatus,
                            sims_cur_code: $scope.temp.sims_cur_code,
                            sims_academic_year: $scope.temp.sims_academic_year,
                            sims_grade_code: $scope.temp.sims_grade_code,
                            sims_fee_code: $scope.temp.sims_fee_code,
                            sims_fee_amount: $scope.temp.sims_fee_amount,
                            sims_rev_acc_no: $scope.temp.sims_rev_acc_no,
                            sims_cash_acc_no: $scope.edt.sims_cash_acc_no,
                            sims_adjustable_fee_type: $scope.edt.sims_fee_code,
                            is_admission_or_prospect_fee: $scope.edt.copy1,
                           
                        }
                        data.opr = 'I';
                        data.opr_upd = "IAF";
                        datasend.push(data);

                        $http.post(ENV.apiUrl + "api/AdmissionFee/CUDAdmissionFee", datasend).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 380 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Already Exist", width: 380 });
                            }
                            $scope.getgrid();
                        });
                        $scope.table = true;
                        $scope.display = false;
                    }
                    

                }

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                $scope.flag1 = false;
                if (Myform) {
                    if ($scope.temp.AdjustfeecodeStatus) {
                        if ($scope.edt.sims_fee_code == undefined || $scope.edt.sims_fee_code == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Select Adjustable Fee Type", showCloseButton: true, width: 380, });
                        }
                    }
                    if (!$scope.flag1) {
                        if (!$scope.temp.AdjustfeecodeStatus) {
                            $scope.edt.sims_fee_code = $scope.temp.sims_fee_code;
                        }
                        dataforUpdate = [];
                        var data = {
                            sims_fee_status: $scope.temp.status,
                            sims_is_fee_adjustable: $scope.temp.AdjustfeecodeStatus,

                            sims_cur_code: $scope.temp.sims_cur_code,
                            sims_academic_year: $scope.temp.sims_academic_year,
                            sims_grade_code: $scope.temp.sims_grade_code,
                            sims_fee_code: $scope.temp.sims_fee_code,
                            sims_fee_amount: $scope.temp.sims_fee_amount,
                            sims_rev_acc_no: $scope.temp.sims_rev_acc_no,
                            sims_cash_acc_no: $scope.edt.sims_cash_acc_no,
                            sims_adjustable_fee_type: $scope.edt.sims_fee_code,
                            is_admission_or_prospect_fee: $scope.edt.copy1,
                            sr_no: srno,
                        }
                        data.opr = "U";
                        data.opr_upd = "UAF";
                        dataforUpdate.push(data);
                        //dataupdate.push(data);
                        $http.post(ENV.apiUrl + "api/AdmissionFee/CUDAdmissionFee", dataforUpdate).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Updated Successfully", width: 380 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Updated", width: 380 });
                            }
                            $scope.getgrid();
                        });
                        dataforUpdate = [];
                        $scope.table = true;
                        $scope.display = false;
                    }
                }
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/AdmissionFee/getAllAdmissionFee").then(function (res1) {

                    $scope.AdmissionFeeData = res1.data;
                    $scope.totalItems = $scope.AdmissionFeeData.length;
                    $scope.todos = $scope.AdmissionFeeData;
                    $scope.makeTodos();

                });
            }

            $scope.Delete = function () {
                debugger;
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    //var t = $scope.filteredTodos[i].uom_code;
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sr_no': $scope.filteredTodos[i].sr_no,
                            'opr': 'D',
                           'opr_upd':"DAF"
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {
                    
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/AdmissionFee/CUDAdmissionFee", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;



                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Already Mapped.Can't be Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }

                            });
                        }
                        else {

                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                //var t = $scope.filteredTodos[i].sims_fee_category;
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");


                                }


                            }

                        }
                       
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                
                $scope.currentPage = str;
            }
            $scope.Feecount = function (str) {
                debugger;
                datasend = [];
                data = [];
                // var data = $scope.temp;
                var data = {                  
                    sims_cur_code:str.sims_cur_code,
                    sims_academic_year:str.sims_academic_year,
                    sims_grade_code:str.sims_grade_code,
                    sims_fee_code:str.sims_fee_code,      

                }
                data.opr = 'S';
                data.opr_upd = "CCF";
              


                $http.post(ENV.apiUrl + "api/AdmissionFee/FeeCount", data).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1>0) {                        
                        $scope.FeeAmountReadonly = true;
                    }
                    else {
                        $scope.FeeAmountReadonly = false;
                    }
                   
                });

            }
            $scope.feestatus = function (str) {
               
                if (str) {
                    $scope.readonlyAdjustableFeeType =false;
                }
                else {
                    $scope.readonlyAdjustableFeeType = true;
                }
            }

           
            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });



        }])

})();
