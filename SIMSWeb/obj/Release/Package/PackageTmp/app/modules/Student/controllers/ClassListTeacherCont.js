﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ClassListTeacherCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = true;

            $scope.table_data = false;

            
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (cur_code) {
                $scope.temp = {};
                $scope.cur_data = cur_code.data;
                $scope.temp["sims_cur_code"] = $scope.cur_data[0].sims_cur_code;
                $scope.academic_year();
            });
          
            $scope.academic_year=function()
            {
                 debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.cur_data[0].sims_cur_code).then(function (a_year) {
                             debugger
                             $scope.aca_year = a_year.data;
                             $scope.temp["sims_academic_year"] = $scope.aca_year[0].sims_academic_year;
                             $scope.get_grades($scope.cur_data[0].sims_cur_code, $scope.aca_year[0].sims_academic_year);

               });
            }

            $http.get(ENV.apiUrl + "api/Classteacher/getCurrentYear").then(function (a_year) {
                
                $scope.year_no = a_year.data[0].year1;
                console.log($scope.periods_Name);
            });

            $scope.get_grades=function(str1,str2)
            {
                debugger
                $http.get(ENV.apiUrl + "api/Classteacher/getclasslistgrades?cur=" + str1 + "&a_year=" + str2 + "&login_code=" + $rootScope.globals.currentUser.username).then(function (grades) {
                    debugger
                    $scope.grades_data = grades.data;
                    console.log($scope.grades_data);
                });
            }

           


            $scope.pas_ssection=function(cur,year,grade)
            {
                debugger;
                $http.get(ENV.apiUrl + "api/Classteacher/getclasslistsection?cur=" + cur + "&a_year=" + year + "&grades=" + grade + "&login_code=" + $rootScope.globals.currentUser.username).then(function (section) {
                    debugger
                    $scope.section_data = section.data;
                    console.log($scope.section_data);
                });

            }

            $scope.Fetch=function(cur,year,grade,section)
            {
                debugger;
                $scope.table_data = false;
                $http.get(ENV.apiUrl + "api/Classteacher/getallData?cur=" + cur + "&a_year=" + year + "&grades=" + grade + "&section=" + section + "&login_code=" + $rootScope.globals.currentUser.username).then(function (fetch_data) {
                    debugger
                    $scope.get_all_data = fetch_data.data;
                    console.log($scope.section_data);
                });

            }

            $scope.Export=function()
            {
                debugger
                    var check = true;
                    debugger;

                    if (check == true) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " Class Teacher.xls ?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 390,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                debugger;
                                var blob = new Blob([document.getElementById('printdata').innerHTML], {
                                    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                                });
                                saveAs(blob, $scope.obj1.lic_school_name + " Class Teacher" + ".xls");


                            }
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Report Not Save", showCloseButton: true, width: 380, })
                    }

                
            }

        }])

})();
