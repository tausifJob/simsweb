﻿/// <reference path="../views/StudentDocumentList.html" />
(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, modulecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('AbsentStudentsNotificationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.table = false;
            $scope.table1 = false;
            $scope.pagesize = "5";
            $scope.pageindex = 0;
            console.clear();
            $timeout(function () {
                $("#example").tableHeadFixer({ 'top': 1 });
            }, 100);

            var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.dt = {
                sims_from_date: dateyear,
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {

                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                $scope.getAccYear($scope.edt.sims_cur_code);
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                    $scope.getGrade(curCode, $scope.temp.sims_academic_year);
                });
            }

            $scope.getGrade = function (curCode, accYear) {

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
            }

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }

            $scope.Show_Data = function (str, str1) {
                debugger;
                if ((str != undefined && str1 != undefined) || $scope.edt.sims_cur_code != "" && $scope.temp.sims_academic_year != "") {
                    $http.get(ENV.apiUrl + "api/StudentReport/getAbsentStudentList?cur_name=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grd_code=" + $scope.edt.sims_grade_code + "&sec_code=" + $scope.edt.sims_section_code + "&student_en=" + $scope.edt.sims_student_enroll_number + "&select_date=" + $scope.dt.sims_from_date).then(function (res) {
                        $scope.studlist = res.data;
                        $scope.totalItems = $scope.studlist.length;
                        $scope.todos = $scope.studlist;
                        $scope.makeTodos();
                        if ($scope.studlist.length == 0) {
                            swal({ title: 'Alert', text: "Please Try Again Record Not Found...", showCloseButton: true, width: 450, height: 200 });
                        }
                        else {
                            $scope.table = true;
                            $scope.table1 = true;
                        }
                    })
                }

                else {
                    swal({ title: 'Alert', text: "Please Select Curriculum & Academic Year Then Try Again...", showCloseButton: true, width: 450, height: 200 });
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {

                $scope.todos = $scope.searched($scope.studlist, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.studlist;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                return (item.student_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_attendance_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_grade_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_section_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sagar == toSearch) ? true : false;
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyoneselect = function () {

                $("input[type='checkbox']").change(function (e) {

                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.Submit = function () {

                if ($scope.edt1.sims_send_email != undefined || $scope.edt1.sims_send_alert != undefined || $scope.edt1.sims_send_sms != undefined) {
                    debugger;
                    if ($scope.edt1.sims_send_email == true && $scope.edt1.sims_send_email != undefined) {
                        var Savedata = [];
                        for (var i = 0; i < $scope.filteredTodos.length; i++) {
                            var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                            if (v.checked == true) {
                                $scope.flag = true;
                                var alertsave = ({
                                    'sims_cur_code': $scope.edt.sims_cur_code,
                                    'sims_academic_year': $scope.temp.sims_academic_year,
                                    'sims_grade_name': $scope.filteredTodos[i].sims_grade_name,
                                    'sims_section_name': $scope.filteredTodos[i].sims_section_name,
                                    'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                                    'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                                    'sims_enroll_number': $scope.filteredTodos[i].sims_enroll_number,
                                    'student_full_name': $scope.filteredTodos[i].student_full_name,
                                    'sims_parent_number': $scope.filteredTodos[i].sims_parent_number,
                                    'sims_contact_person': $scope.filteredTodos[i].sims_contact_person,
                                    'sims_from_date': $scope.dt.sims_from_date,
                                    'sims_email_id': $scope.filteredTodos[i].sims_email_id,
                                    opr: 'R'
                                });
                                Savedata.push(alertsave);
                            }
                        }

                        $http.post(ENV.apiUrl + "api/StudentReport/CUDAbsentStudentList", Savedata).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1.strMessage != undefined) {
                                if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                    swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                                    $scope.Reset();
                                    $scope.currentPage = true;
                                }
                            }

                        });
                    }


                    if ($scope.edt1.sims_send_alert == true && $scope.edt1.sims_send_alert != undefined) {
                        var Savedata = [];
                        for (var i = 0; i < $scope.filteredTodos.length; i++) {
                            var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                            if (v.checked == true) {
                                $scope.flag = true;
                                var alertsave = ({
                                    'sims_cur_code': $scope.edt.sims_cur_code,
                                    'sims_academic_year': $scope.temp.sims_academic_year,
                                    'sims_grade_name': $scope.filteredTodos[i].sims_grade_name,
                                    'sims_section_name': $scope.filteredTodos[i].sims_section_name,
                                    'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                                    'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                                    'sims_enroll_number': $scope.filteredTodos[i].sims_enroll_number,
                                    'student_full_name': $scope.filteredTodos[i].student_full_name,
                                    'sims_parent_number': $scope.filteredTodos[i].sims_parent_number,
                                    'sims_from_date': $scope.dt.sims_from_date,
                                    opr: 'I'
                                });
                                Savedata.push(alertsave);
                            }
                        }

                        $http.post(ENV.apiUrl + "api/StudentReport/CUDAbsentStudentList", Savedata).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1.strMessage != undefined) {
                                if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                    swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                                    $scope.Reset();
                                    $scope.currentPage = true;
                                }
                            }

                        });
                    }

                    if ($scope.edt1.sims_send_sms == true && $scope.edt1.sims_send_sms != undefined) {
                        var Savedata = [];
                        for (var i = 0; i < $scope.filteredTodos.length; i++) {
                            var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                            if (v.checked == true) {
                                $scope.flag = true;
                                var alertsave = ({
                                    'sims_cur_code': $scope.edt.sims_cur_code,
                                    'sims_academic_year': $scope.temp.sims_academic_year,
                                    'sims_grade_name': $scope.filteredTodos[i].sims_grade_name,
                                    'sims_section_name': $scope.filteredTodos[i].sims_section_name,
                                    'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                                    'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                                    'sims_enroll_number': $scope.filteredTodos[i].sims_enroll_number,
                                    'student_full_name': $scope.filteredTodos[i].student_full_name,
                                    'sims_parent_number': $scope.filteredTodos[i].sims_parent_number,
                                    'sims_contact_number': $scope.filteredTodos[i].sims_contact_number,
                                    'sims_from_date': $scope.dt.sims_from_date,
                                    opr: 'M'
                                });
                                Savedata.push(alertsave);
                            }
                        }

                        $http.post(ENV.apiUrl + "api/StudentReport/CUDAbsentStudentList", Savedata).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1.strMessage != undefined) {
                                if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                    swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                                    $scope.Reset();
                                    $scope.currentPage = true;
                                }
                            }

                        });
                    }
                }

                else {
                    swal({ title: 'Alert', text: "Please Select Any One CheckBox To Perform Opration...", showCloseButton: true, width: 450, height: 200 });
                }
            }

            $scope.Reset = function () {
                $scope.edt = {
                    sims_cur_code: '',
                    sims_grade_code: '',
                    sims_section_code: '',
                    sims_student_enroll_number: '',
                }
                $scope.temp = {
                    sims_academic_year: '',
                }
                $scope.dt = {
                    sims_from_date: dateyear,
                }
                $scope.table = false;
                $scope.table1 = false;
            }

            $scope.Clear = function () {
                $scope.edt1 = {
                    sims_send_email: false,
                    sims_send_alert: false,
                    sims_send_sms: false,
                }
            }

        }])
})();
