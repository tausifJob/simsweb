﻿(function () {
    'use strict';
    var opr = '';
    var itemcode = [];
    var main;
    var data1 = [];
    var data = [];
    var itemset;
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AdmisisonQuotaCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            itemset = document.getElementById("chk_itemset");
            $scope.pagesize = '10';
            $scope.table1 = false;
            $scope.operation = false;
            $scope.editmode = false;
            $scope.global_search = true;
            $scope.temp1 = {};
            var username = $rootScope.globals.currentUser.username;
            $scope.CheckItemSet = function () {
                debugger
                itemset = document.getElementById("chk_itemset");
                if (itemset.checked == true) {
                    $scope.supdis = true;
                    $scope.edt = {};
                    $scope.edt =
                        {
                            sg_name: $scope.SupplierGroupNamewithouitem_data[0].sg_name,
                            im_model_name: 'NA'
                        }

                    $scope.edt['im_assembly_ind'] = true;
                }
                else {
                    $scope.supdis = false;
                    $scope.edt = {};
                    $scope.edt['im_assembly_ind'] = false;

                }
            }


            $http.get(ENV.apiUrl + "api/Fee/SFS/getQuotaCur").then(function (QuotaCur) {
                $scope.QuotaCur = QuotaCur.data;
                $scope.temp1['sims_q_cur_code'] = $scope.QuotaCur[0].sims_q_cur_code;
                $scope.cur_change();
            });

            $scope.total_quota_strength = 0; $scope.total_recommended_seats = 0; $scope.balance_recommended_seats = 0;

            $scope.cur_change = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/getQuotaAcademic?cur_code=" + $scope.temp1.sims_q_cur_code).then(function (QuotaAcademic) {
                    $scope.QuotaAcademic = QuotaAcademic.data;
                    $scope.temp1['sims_q_academic_year'] = $scope.QuotaAcademic[0].sims_q_academic_year;
                    $scope.academic_change();
                });

            }

            $scope.academic_change = function () {
                $scope.temp1.sims_q_user_name = username;//Change;
                $http.get(ENV.apiUrl + "api/Fee/SFS/getQuotaForUser?cur_code=" + $scope.temp1.sims_q_cur_code + "&academic_year=" + $scope.temp1.sims_q_academic_year + "&user_name=" + $scope.temp1.sims_q_user_name).then(function (Quota) {
                    $scope.Quota = Quota.data;
                   
                    $scope.temp1['sims_quota_id'] = $scope.Quota[0];
                    $scope.quota_change($scope.temp1.sims_quota_id, $scope.temp1);
                });

            }

            $scope.sims_quota_id=''
            $scope.gobqId = '';

            $scope.quota_change = function (qid, obj) {
                debugger;
                $scope.total_quota_strength = 0; $scope.total_recommended_seats = 0; $scope.balance_recommended_seats = 0;
                if(qid!=undefined)
                    $scope.gobqId = qid;
                console.log('Qc');
                $scope.total_quota_strength = $scope.gobqId.total_quota_strength; $scope.total_recommended_seats = $scope.gobqId.total_recommended_seats;
                $scope.balance_recommended_seats = $scope.gobqId.balance_recommended_seats;
                $scope.sims_quota_id = $scope.gobqId.sims_quota_id;
                debugger;
                console.log(obj);
                $scope.temp1.sims_q_user_name = username;//change
                $http.get(ENV.apiUrl + "api/Fee/SFS/getQuotaGrades?cur_code=" + $scope.temp1.sims_q_cur_code + "&academic_year=" + $scope.temp1.sims_q_academic_year + "&user_name=" + $scope.temp1.sims_q_user_name + "&quotaid=" + $scope.gobqId.sims_quota_id).then(function (QuotaGrade) {
                    $scope.QuotaGrade = QuotaGrade.data;
                    console.log(QuotaGrade.data);
                });

            }

            $scope.Show_Data = function () {

                //var data = $scope.temp1;
                if ($scope.search_param == undefined || $scope.search_param == 'undefined')
                    $scope.search_param='';
                if($scope.sims_q_grade_code==undefined ||  $scope.sims_q_grade_code=='undefined')
                    $scope.sims_q_grade_code='';

                  
                $http.get(ENV.apiUrl + "api/Fee/SFS/getQuotaAdmissions?cur_code=" + $scope.temp1.sims_q_cur_code + "&academic_year=" + $scope.temp1.sims_q_academic_year + "&user_name=" + $scope.temp1.sims_q_user_name + "&quotaid=" + $scope.sims_quota_id + "&grade_code=" + $scope.sims_q_grade_code + "&search=" + $scope.search_param).then(function (admissions) {
                    $scope.ItemMasterDetail = admissions.data;
                    console.log(admissions.data);
                    $scope.totalItems = $scope.ItemMasterDetail.length;
                    $scope.todos = $scope.ItemMasterDetail;
                    $scope.makeTodos();
                    if ($scope.ItemMasterDetail < 1) {
                        swal({ title: "Alert", text: "Sorry, There Is No Data Found..!", width: 300, height: 200 });

                    }
                   
                });
            }

            $scope.show_recommended_amds = function () {
                if ($scope.search_param == undefined || $scope.search_param == 'undefined')
                    $scope.search_param = '';
                if ($scope.sims_q_grade_code == undefined || $scope.sims_q_grade_code == 'undefined')
                    $scope.sims_q_grade_code = '';


                $http.get(ENV.apiUrl + "api/Fee/SFS/getRecommedAdmissions?cur_code=" + $scope.temp1.sims_q_cur_code + "&academic_year=" + $scope.temp1.sims_q_academic_year + "&user_name=" + $scope.temp1.sims_q_user_name ).then(function (rcadmissions) {
                    $scope.rcadmissions= rcadmissions.data;
                    if ($scope.rcadmissions < 1) {
                        swal({ title: "Alert", text: "Sorry, There Is No Data Found..!", width: 300, height: 200 });
                    }
                    else
                    {
                        $('#MyModal').modal({ backdrop: 'static', keyboard: true });
                    }
                });
            }

            $scope.UnRecommendAdmissions = function (info) {

                $http.post(ENV.apiUrl + "api/Fee/SFS/UnRecommendAdmissions?cur_code=" + $scope.temp1.sims_q_cur_code + "&academic_year=" + $scope.temp1.sims_q_academic_year + "&user_name=" + $scope.temp1.sims_q_user_name + "&admissionsno=" + info.sims_admission_number).then(function (urcadmissions) {
                    if (urcadmissions.data == true) {
                        swal({ title: "Alert", text: "Admission removed from Recommendation.", width: 300, height: 200 });
                    }
                    $scope.show_recommended_amds();
                    $scope.academic_change();
                    $scope.quota_change();
                });
            }
            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage);
                $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['check_all'] = false;
                $scope.row1 = '';
            }

            $scope.save = function () {
                $scope.data = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].sims_admission_recommend_status == true) {
                        $scope.data.push($scope.filteredTodos[i]);
                    }
                }
                if ($scope.balance_recommended_seats < $scope.data.length) {
                    swal({ title: "Alert", text: "You can only Recommend " + $scope.balance_recommended_seats + " Admissions.", width: 300, height: 200 });
                }
                else {
                    if ($scope.data.length > 0) {
                        $http.post(ENV.apiUrl + "api/Fee/SFS/QuotaAdmissions?cur_code=" + $scope.temp1.sims_q_cur_code + "&academic_year=" + $scope.temp1.sims_q_academic_year + "&user_name=" + $scope.temp1.sims_q_user_name + "&quotaid=" + $scope.sims_quota_id, $scope.data).then(function (res) {
                            $scope.res = res.data;
                            if ($scope.res == true) {
                                swal({ title: "Alert", text: "Your Admissions are recommended successfully.", width: 300, height: 200 });
                                $scope.Reset();
                            }
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Please select at least one Admissions.", width: 300, height: 200 });

                    }
                    
                }
            }
            
            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.global_search = true;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.search_param = '';
                $scope.edt = {

                    sg_name: "",
                    im_item_code: "",
                    im_desc: "",
                    im_model_name: "",
                    dep_code: "",
                    sec_code: "",
                    pc_code: "",
                    sup_code: "",
                    uom_code: "",
                    uom_code_has: "",
                    im_supl_catalog_price: "",
                    im_supl_price_date: "",
                    im_supl_min_qty: "",
                    im_supl_buying_price: "",
                    im_supl_buying_price_old: "",
                    im_sell_min_qty: "",
                    im_min_level: "",
                    im_max_level: "",
                    im_reorder_level: "",
                    im_economic_order_qty: "",
                    im_rso_qty: "",
                    im_average_month_demand: "",
                    im_malc_rate: "",
                    im_malc_rate_old: "",
                    im_mark_up: "",
                    im_sell_price: "",
                    im_sell_price_old: "",
                    im_sellprice_special: "",
                    im_sellprice_freeze_ind: "",
                    im_approximate_price: "",
                    im_estimated_price: "",
                    im_creation_date: "",
                    im_stock_check_date: "",
                    im_no_of_packing: "",
                    im_last_receipt_date: "",
                    im_last_issue_date: "",
                    im_last_supp_code: "",
                    im_approximate_lead_time: "",
                    im_manufact_serial_no: "",
                    im_last_supp_cur: "",
                    im_trade_cat: "",
                    im_one_time_flag: "",
                    im_agency_flag: "",
                    im_assembly_ind: "",
                    im_proprietary_flag: "",
                    im_reusable_flag: "",
                    im_supersed_ind: "",
                    im_obsolete_excess_ind: "",
                    im_invest_flag: ""
                };
            }

            $scope.Reset = function () {
                $scope.search_param = '';
                $scope.total_quota_strength = 0; $scope.total_recommended_seats = 0; $scope.balance_recommended_seats = 0;

                $scope.table1 = false;
                $scope.temp1 = {
                    im_inv_no: '',
                    im_desc: '',
                    im_item_code: '',
                    dep_code: '',
                    sec_code: '',
                    sup_code: '',
                    sg_name: '',
                    pc_code: ''
                }

                $scope.filteredTodos = [];

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);
                main = document.getElementById('mainchk');
                main.checked = false;
              
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_admission_number );
                        $scope.filteredTodos[i].sims_admission_recommend_status = true;
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_admission_number );
                       $scope.filteredTodos[i].sims_admission_recommend_status = false;
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function (info) {
             

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                       
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                      
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            
       
            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.ItemMasterDetail, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.ItemMasterDetail;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */

                return (
                     item.sg_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.im_item_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.im_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.dep_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                     item.sec_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.pc_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sup_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.uom_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.im_min_level.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                     item.im_inv_no == toSearch) ? true : false;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


        }])
})();





