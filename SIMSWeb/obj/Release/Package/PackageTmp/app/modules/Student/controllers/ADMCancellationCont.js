﻿(function () {
    'use strict';
    var appcode = [];
    var getdata = [];
    var senddata = [], demo = [], demo1 = [];
    var main;
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ADMCancellationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.busyindicator = true;
            $scope.submit2 = false;
            $scope.submit1 = true;
            $scope.chkbox = false;
            $scope.edt = [];
            $scope.grades_data = "";
            var g = false;

           

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                debugger;
                $scope.curriculum = res.data;
                $scope.getsections();
                //$scope.edt["sims_cur_code"] = $scope.curriculum[0].sims_cur_code;
                //$scope.getacyr($scope.curriculum[0].sims_cur_code);
            })

            $scope.getsections = function () {
                
                $scope.chkbox = true;
                
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (res1) {
                    console.log(res1.data);
                    
                    $scope.grades_data = "";
                    $scope.grades_data = res1.data;
                    $("#grade_module").jqxListBox(
                           { source: '', displayMember: '', valueMember: "", width: '100%', height: 300, theme: 'energyblue', multiple: true }

                       );

                    $("#grade_module").jqxListBox(
                            { source: $scope.grades_data, displayMember: 'sims_grade_name', valueMember: "sims_grade_code", width: '100%', height: 300, theme: 'energyblue', multiple: true }

                        );
                })

            };

            $scope.getacyr = function (str) {

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                  //  $scope.edt["sims_academic_year"] = $scope.Academic_year[0].sims_academic_year;
                    $scope.getsections();
                });
            }

            $('#grade_module').bind('select', function (event) {
                var args = event.args;
               // var items = $('#grade_module').jqxListBox('getItems', args.index);
                var items = $("#grade_module").jqxListBox('getSelectedItems');

                if (items.length > 0) {
                    $scope.edt.sims_grade_list = "";
                    for (var i = 0; i < items.length; i++) {
                        $scope.edt.sims_grade_list += items[i].originalItem.sims_grade_code;
                        if (i < items.length - 1) $scope.edt.sims_grade_list += ",";
                    }
                    //alert(labels);
                }

            });

            var datasend = [];
            $scope.savedata = function () {
                if ($scope.edt.empName != undefined) {

                    $scope.chkbox = false;
                    debugger;
                    var data = $scope.edt;
                    data.opr = 'I';
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/common/CUDCancelAdmission", datasend).then(function (msg) {
                        datasend = [];
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            $scope.edt["sims_cur_code"] = '';
                            $scope.edt["sims_academic_year"] = '';
                            $scope.edt["empName"] = '';
                            $scope.edt["sims_fee_clr_status"] = '',
                            $scope.edt["sims_finn_clr_status"] = '',
                            $scope.edt["sims_inv_clr_status"] = '',
                            $scope.edt["sims_inci_clr_status"] = '',
                            $scope.edt["sims_lib_clr_status"] = '',
                            $scope.edt["sims_trans_clr_status"] = '',
                            $scope.edt["sims_acad_clr_status"] = '',
                            $scope.edt["sims_admin_clr_status"] = '',
                            $scope.edt["sims_other1_clr_status"] = '',
                            $scope.edt["sims_other2_clr_status"] = '',
                            $scope.edt["sims_other3_clr_status"] = '',
                            $scope.edt["sims_clr_emp_status"] = '',
                             $scope.edt["sims_clr_emp_status"] = '',

                            $scope.grade_s = '';

                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Employee", width: 300, height: 200 });
                }
                   
            }

            $scope.Submit2=function()
            {
                debugger;
                
                $scope.chkbox = false;
                var data = $scope.edt;
                datasend.push(data);
                $http.post(ENV.apiUrl + "api/common/CUDCancelAdmissionshow", datasend).then(function (msg) {
                    datasend = [];
                    debugger;
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                        $scope.edt["sims_cur_code"] = '';
                        $scope.edt["sims_academic_year"] = '';
                        $scope.edt["empName"] = '';
                        $scope.edt["sims_fee_clr_status"]='',
                        $scope.edt["sims_finn_clr_status"]='',
                        $scope.edt["sims_inv_clr_status"]='',
                        $scope.edt["sims_inci_clr_status"]='',
                        $scope.edt["sims_lib_clr_status"]='',
                        $scope.edt["sims_trans_clr_status"]='',
                        $scope.edt["sims_acad_clr_status"]='',
                        $scope.edt["sims_admin_clr_status"]='',
                        $scope.edt["sims_other1_clr_status"]='',
                        $scope.edt["sims_other2_clr_status"]='',
                        $scope.edt["sims_other3_clr_status"]='',
                        $scope.edt["sims_clr_emp_status"] = '',
                         $scope.edt["sims_clr_emp_status"] = '',
                         $("#grade_module").jqxListBox('selectItem', "");
                        $scope.grade_s = '';
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                    }
                });
            }
            $scope.Fetch=function()
            {
                debugger;
                if ($scope.edt.empName != undefined && $scope.edt.sims_academic_year != undefined && $scope.edt.sims_cur_code != undefined && $scope.edt.empName !="") {
                    $scope.submit2 = true;
                    $scope.submit1 = false;
                    $scope.chkbox = true;
                    $http.get(ENV.apiUrl + "api/common/getAllADMData?Empnum=" + $scope.em_number).then(function (res1) {
                        $scope.CreDiv = res1.data;
                        console.log($scope.CreDiv);
                        debugger;
                        $scope.edt = {
                            'sims_sr_no': $scope.CreDiv[0].sims_sr_no,
                            'sims_acad_clr_status': $scope.CreDiv[0].sims_acad_clr_status,
                            'sims_admin_clr_status': $scope.CreDiv[0].sims_admin_clr_status,
                            'sims_clr_emp_status': $scope.CreDiv[0].sims_clr_emp_status,
                            'sims_fee_clr_status': $scope.CreDiv[0].sims_fee_clr_status,
                            'sims_finn_clr_status': $scope.CreDiv[0].sims_finn_clr_status,
                            'sims_inci_clr_status': $scope.CreDiv[0].sims_inci_clr_status,
                            'sims_inv_clr_status': $scope.CreDiv[0].sims_inv_clr_status,
                            'sims_lib_clr_status': $scope.CreDiv[0].sims_lib_clr_status,
                            'sims_other1_clr_status': $scope.CreDiv[0].sims_other1_clr_status,
                            'sims_other2_clr_status': $scope.CreDiv[0].sims_other2_clr_status,
                            'sims_other3_clr_status': $scope.CreDiv[0].sims_other3_clr_status,
                            'sims_status': $scope.CreDiv[0].sims_status,
                            'sims_trans_clr_status': $scope.CreDiv[0].sims_trans_clr_status,
                            'sims_clr_super_user_flag':$scope.CreDiv[0].sims_clr_super_user_flag,
                            //'grades'   grades
                            'sims_status': $scope.CreDiv[0].sims_status,
                            'sims_academic_year': $scope.CreDiv[0].sims_academic_year,
                            'sims_cur_code': $scope.CreDiv[0].sims_cur_code,
                            'empName': $scope.CreDiv[0].sims_clr_emp_id

                        }
                        $scope.grade_s = $scope.CreDiv[0].grades;
                        $scope.cur_code = $scope.CreDiv[0].sims_cur_code;


                        if ($scope.grade_s != undefined) {
                            var d = $scope.grade_s.split(',');
                            for (var i = 0; i < d.length - 1; i++) {
                                var jqItem = $("#grade_module").jqxListBox('getItemByValue', d[i]);
                                $("#grade_module").jqxListBox('selectItem', jqItem);
                            }
                        }
                       
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Employee,Curriculum,Academic year", width: 300, height: 200 });
                }
            }
                       $rootScope.visible_stud = false;
                       $rootScope.visible_parent = false;
                       $rootScope.visible_search_parent = false;
                       $rootScope.visible_teacher = false;
                       $rootScope.visible_User = false;
                       $rootScope.visible_Employee = true;
                       $rootScope.chkMulti = false;
                       $scope.Search = function () {
                       $scope.global_Search_click();

                $('#Global_Search_Modal').modal({ backdrop: "static" });
                //  $scope.display = true;
                //$scope.SelectedUserLst = [];
            }

            $scope.$on('global_cancel', function () {
                debugger;

                if ($scope.SelectedUserLst.length > 0) {

                    debugger
                    $scope.edt =
                        {
                            empName: $scope.SelectedUserLst[0].empName,
                            em_number: $scope.SelectedUserLst[0].em_number
                        }
                }
                $scope.em_number = $scope.edt.em_number;
               
            });


         


        }]);
})();



