﻿(function () {
    'use strict';
    // var main, del = '', delvar = [],catdetails = [];
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AdmissionCriteriaCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.critobj = [];
            var data1 = [];
            var deletecode = [];

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $http.get(ENV.apiUrl + "api/common/AdmissionCriteria/getAdmissionCriteria").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.critobj = res.data;
                $scope.totalItems = $scope.critobj.length;
                $scope.todos = $scope.critobj;
                $scope.makeTodos();

            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $http.get(ENV.apiUrl + "api/common/AdmissionCriteria/getCriteriaType").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.critype = res.data;
                console.log($scope.critype);
            });

            $scope.New = function () {
                var autoid;
                $scope.display = true;
                $scope.grid = false;
                $scope.btn_save1 = true;
                $scope.btn_update1 = false;
                $scope.delete1 = false;
                $scope.edt = [];

                $http.get(ENV.apiUrl + "api/common/AdmissionCriteria/getAutoCriteriaCode").then(function (res) {
                    autoid = res.data;
                    // var v = document.getElementById('txt_criteria_code');
                    // v.value = $scope.autoid[0].sims_criteria_code;
                    $scope.edt['sims_criteria_code'] = autoid;
                });
            }

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.btn_save1 = false;
                $scope.btn_update1 = true;
                $scope.delete1 = false;
                console.log(str);
                //$scope.edt = str;
                $scope.edt =
                    {
                        sims_criteria_code: str.sims_criteria_code,
                        sims_criteria_name_en: str.sims_criteria_name_en,
                        sims_criteria_name_ot: str.sims_criteria_name_ot,
                        sims_criteria_mandatory: str.sims_criteria_mandatory,
                        sims_criteria_type: str.sims_criteria_type,
                        sims_criteria_status: str.sims_criteria_status,
                    }
            }

            $scope.Save = function (isvalidate) {
                var data1 = [];

                if (isvalidate) {
                    if ($scope.btn_save1 == true) {
                        var data = ({
                            sims_criteria_code: $scope.edt.sims_criteria_code,
                            sims_criteria_name_en: $scope.edt.sims_criteria_name_en,
                            sims_criteria_name_ot: $scope.edt.sims_criteria_name_ot,
                            sims_criteria_mandatory: $scope.edt.sims_criteria_mandatory,
                            sims_criteria_type: $scope.edt.sims_criteria_type,
                            sims_criteria_status: $scope.edt.sims_criteria_status,
                            opr: 'I'
                        });

                        data1.push(data);

                        $http.post(ENV.apiUrl + "api/common/AdmissionCriteria/CUDAdmissionCriteria", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Admission Criteria  Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });

                            }
                            else {
                                swal({ title: "Alert", text: "Admission Criteria Not Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $scope.currentPage = 1;
                $http.get(ENV.apiUrl + "api/common/AdmissionCriteria/getAdmissionCriteria").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.critobj = res.data;
                    $scope.totalItems = $scope.critobj.length;
                    $scope.todos = $scope.critobj;
                    $scope.makeTodos();
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

            }

            $scope.Update = function (isvalidate) {
                var data1 = [];

                if (isvalidate) {
                    var data = ({
                        sims_criteria_code: $scope.edt.sims_criteria_code,
                        sims_criteria_name_en: $scope.edt.sims_criteria_name_en,
                        sims_criteria_name_ot: $scope.edt.sims_criteria_name_ot,
                        sims_criteria_mandatory: $scope.edt.sims_criteria_mandatory,
                        sims_criteria_type: $scope.edt.sims_criteria_type,
                        sims_criteria_status: $scope.edt.sims_criteria_status,
                        opr: 'U'
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/AdmissionCriteria/CUDAdmissionCriteria", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Admission Criteria Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });

                        }
                        else {
                            swal({ title: "Alert", text: "Admission Criteria Not Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                    });
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        // var v = document.getElementById(i);
                        var t = $scope.filteredTodos[i].sims_criteria_code;
                        var v = document.getElementById(t+i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        // var v = document.getElementById(i);
                        var t = $scope.filteredTodos[i].sims_criteria_code;
                        var v = document.getElementById(t+i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.Delete = function () {
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++)
                {
                    var t = $scope.filteredTodos[i].sims_criteria_code;
                    var v = document.getElementById(t+i);

                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sims_criteria_code': $scope.filteredTodos[i].sims_criteria_code,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/common/AdmissionCriteria/CUDAdmissionCriteria", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Admission Criteria Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });

                                }
                                else {
                                    swal({ title: "Alert", text: "Admission Criteria Not Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var t = $scope.filteredTodos[i].sims_criteria_code;
                                var v = document.getElementById(t+i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.critobj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.critobj;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_criteria_code+i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_criteria_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_criteria_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            var dom;
            var count = 0;

            $scope.expand = function (info, $event) {
                console.log(info);
                $(dom).remove();

                dom = $("<tr><td class='details' colspan='11'>" +
                    "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                    "<tbody>" +
                     "<tr><td class='semi-bold'>&nbsp;</td><td class='semi-bold'></td><td class='semi-bold'></td> <td class='semi-bold'>" + gettextCatalog.getString('French Short Name') + "</td> <td class='semi-bold'>" + gettextCatalog.getString('Regional Short Name') + " </td><td class='semi-bold'>" + gettextCatalog.getString('French Full Name') + "</td><td class='semi-bold'>" + gettextCatalog.getString('Regional Full Name') + "</td>" +
                    "</tr>" +

                      "<tr><td class='semi-bold'>&nbsp;</td><td class='semi-bold'>&nbsp;</td><td class='semi-bold'>&nbsp;</td><td>" + (info.curriculum_short_name_fr) + "</td> <td>" + (info.curriculum_short_name_ot) + " </td><td>" + (info.curriculum_full_name_fr) + "</td><td>" + (info.curriculum_full_name_ot) + "</td>" +
                    "</tr>" +

                    " </table></td></tr>")

                $($event.currentTarget).parents("tr").after(dom);


            };

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

        }])
})();