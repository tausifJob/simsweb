﻿(function () {
    'use strict';
    var del = [], feedetails = [];

    var main;
    var date1, date3, date4;

    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CancelAdmToTCCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$window', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $window, ENV) {
            $scope.display = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.grid = true;
            $scope.save1 = true;
            $scope.edit_code = true;
            $scope.edt = { feeDet: '' };

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (res2) {
                $scope.display = true;
                $scope.ComboBoxValues = res2.data;
                console.log($scope.ComboBoxValues);
            });

            $scope.SearchStudentWindow = function () {
                $scope.searchtable = false;
                $scope.student = '';
                $('#MyStudentModal').modal('show');
                $scope.temp = '';
            }

            $scope.SearchSudent = function () {
                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    main.checked = false;
                }

                $scope.searchtable = false;
                $scope.busy = true;
                $scope.sibling_result

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.temp)).then(function (Allstudent) {
                    $scope.student = Allstudent.data;
                    $scope.searchtable = true;
                    $scope.busy = false;
                });
                $scope.edit_code = false;
            }

            $scope.DataEnroll = function () {
                for (var i = 0; i < $scope.student.length; i++) {
                    var t = $scope.student[i].s_enroll_no;
                    var v = document.getElementById(t + i);
                    if (v.checked == true) {
                        var student = $scope.student[i].s_enroll_no;
                        $scope.cur_code = $scope.student[i].s_cur_code;
                        $scope.acad_yr = $scope.student[i].sims_academic_year;

                        $http.post(ENV.apiUrl + "api/common/CancelAdmission/CancelAdmission?enroll_no=" + $scope.student[i].s_enroll_no + "&cur_code=" + $scope.student[i].s_cur_code + "&acad_yr=" + $scope.student[i].sims_academic_year).then(function (res) {
                            $scope.edt = res.data;
                            console.log($scope.edt);
                        });

                    }
                }
                $scope.student = [];
            }

            $scope.OkCancelAdmission = function () {
                debugger
                feedetails = [];
                if (!jQuery.isEmptyObject($scope.edt.feeDet)) {
                    for (var i = 0; i < $scope.edt.feeDet.length; i++) {
                        var t = $scope.edt.feeDet[i].feeCode;
                        var v = document.getElementById(t);

                        if (v.checked == true) {
                            feedetails = feedetails + $scope.edt.feeDet[i].feeCode + ',';
                        }
                    }
                }

                //var v = document.getElementById('txt_enroll_no');
                //$scope.edt.enroll = v.value;
                var data = $scope.edt;
                if (!jQuery.isEmptyObject($scope.edt.studentDet)) {
                    data.cur_code = $scope.edt.studentDet.curr_name;
                    data.academic_year = $scope.edt.studentDet.academic_year;
                }
                //else
                //{
                //    data.cur_code = $scope.cur_code;
                //    data.academic_year = $scope.acad_yr;
                //}
                data.fee_category_code = feedetails;
                data.fees_paid_status = '1';
                data.opr = "P";

                console.log(data);

                $http.post(ENV.apiUrl + "api/common/CancelAdmission/CancelAdmissionProceed", data).then(function (res) {
                    $scope.obj1 = res.data;
                    if ($scope.obj1.length > 0) {
                        swal({ title: "Alert", text: "Admission Cancelled and TC Requested Successfully", showCloseButton: true, width: 380, });
                    }
                    $scope.cancel();
                });
                // $scope.edt = [];

            }

            $scope.cancel = function () {
                $scope.edt = [];
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }



            $scope.Save = function (isvalidate) {
                debugger
                //if ($scope.edt.reason != '')
                if ($scope.edt.reason == null) {
                    if (isvalidate) {
                        swal({
                            title: "Alert",
                            text: "Do You Want To Continue Cancellation Process Without Reason",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $state.go('main.Sim626', { edt: $scope.edt });
                                //$scope.OkCancelAdmission();                                   
                            }
                        });
                    }
                }
                else {
                    if (isvalidate) {
                        $state.go('main.Sim626', { edt: $scope.edt });
                        //$scope.OkCancelAdmission();
                    }
                }
            }

            $scope.OkCanceladm = function () {
                $scope.OkCancelAdmission();
            }

            $scope.check1 = function (Fee) {
                console.log(Fee);
                var v = document.getElementById(Fee.feeCode);

                if (v.checked == true) {
                    feedetails = feedetails + Fee.feeCode + ','
                    console.log(feedetails);
                    $scope.row1 = '';
                }
                else {
                    v.checked = false;
                    console.log(feedetails);
                }
            }

            function convertdate(dt) {
                if (dt != null) {
                    var d1 = new Date(dt);
                    var month = d1.getMonth() + 1;
                    var day = d1.getDate();
                    if (month < 10)
                        month = "0" + month;
                    if (day < 10)
                        day = "0" + day;
                    var d = d1.getFullYear() + "-" + (month) + "-" + (day);
                    return d;
                }
            }
            $scope.flag = true;
            $scope.getCollapse = function () {
                // $('.nav-tabs a[href="#collapseOne"]').tab('show');
                // $('#fee').removeClass('collapsed');
                //debugger;
                //if ($scope.flag==true) {
                //    $('#fee').removeClass('collapsed');
                //    $scope.flag = false
                // }
                //else {
                //    $('#fee').addClass('collapsed');

                //}
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }])
})();