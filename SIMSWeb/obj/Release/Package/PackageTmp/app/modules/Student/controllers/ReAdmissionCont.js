﻿(function () {
    'use strict';
    var obj1, temp, opr, comp, edt;
    var main, deletefin = [];
    var deletecode = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ReAdmissionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.search = true;
            var dataforSave = [];
            $scope.reset = true;
            $scope.display = true;
            $scope.save_btn = true;
            $scope.txt_read = true;
            $scope.temp = [];

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100)

            var user = $rootScope.globals.currentUser.username;

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage);
                $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                //main.checked = false;
                //$scope.CheckAllChecked();
            }

            $scope.student = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                debugger
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.student = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.student, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    debugger
                    $scope.totalItems = $scope.student1.length;
                    $scope.todos = $scope.student1;
                }
                $scope.makeTodos();
                // main.checked = false;
                // $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_enroll_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.stud_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_cur_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_grade_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_section_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_academic_year == toSearch) ? true : false;
            }

            $scope.reset = function () {
                $scope.temp = [];
                $scope.edt = [];
                $scope.savedata = [];
                $scope.temp.sims_enroll_no = '';
                $scope.temp.sims_cur_name = '';
                $scope.temp.sims_academic_year = '';
                $scope.temp.sims_grade_name = '';
                $scope.temp.sims_section_name = '';
                $scope.temp.stud_full_name = '';
                $scope.edt.s_cur_code = '';
                $scope.edt.sims_academic_year = '';
                $scope.edt.search_std_grade_name = '';
                $scope.edt.search_std_section_name = '';
            }

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;
            });

            $scope.getsectioncode = function (cur, year, grade) {

                $http.get(ENV.apiUrl + "api/common/SectionTerm/getAllSections?cur_code=" + cur + "&ac_year=" + year + "&g_code=" + grade).then(function (res1) {
                    $scope.sections = res1.data;
                });
            }

            //Stud details
            $scope.access_nos = function (event) {
                debugger
                //savedata = [];
                if (event.key == "Tab" || event.key == "Enter") {
                    if ($scope.temp.sims_enroll_no == null || $scope.temp.sims_enroll_no == "") {
                        swal('', 'Please Enter Enroll No.')
                        return;
                    }
                    $http.get(ENV.apiUrl + "api/ReAdmission/Get_studentlist?enroll_no=" + $scope.temp.sims_enroll_no).then(function (Allstudent) {
                        debugger
                        if (Allstudent.data.length == 0) {
                            swal('', 'No Record Found');
                            return;
                        }
                        $scope.savedata = Allstudent.data;
                        $scope.temp = $scope.savedata[0];
                        console.log($scope.savedata)
                    });
                }
            }

            $scope.stud_details = function () {
                debugger;
                if ($scope.temp.sims_enroll_no == null || $scope.temp.sims_enroll_no == "") {
                    $('#myModal1').modal('show');
                    $http.get(ENV.apiUrl + "api/ReAdmission/Get_studentlist?enroll_no=" + $scope.temp.sims_enroll_no).then(function (Allstudent) {
                        $scope.student = Allstudent.data;
                        $scope.student1 = Allstudent.data;
                        $scope.totalItems = $scope.student.length;
                        $scope.todos = $scope.student;
                        $scope.makeTodos();
                    });
                    $scope.list = true;
                }
                else {
                    $http.get(ENV.apiUrl + "api/ReAdmission/Get_studentlist?enroll_no=" + $scope.temp.sims_enroll_no).then(function (Allstudent) {
                        debugger
                        if (Allstudent.data.length == 0) {
                            swal('', 'No Record Found');
                            return;
                        }
                        $scope.savedata = Allstudent.data;
                        $scope.temp = $scope.savedata[0];
                        console.log($scope.savedata)
                    });
                }
            }



            $scope.checkonebyonedelete = function (info) {

                for (var i = 0; i < $scope.student.length; i++) {
                    var d = document.getElementById(i);
                    if (d.checked == true) {
                        $scope.student[i].ischange = true;
                    }
                    else {
                        $scope.student[i].ischange = false;
                    }
                }

                $("input[type='radio']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('$index');
                if (main.checked == true) {

                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.submit = function () {
                for (var j = 0; j < $scope.student.length; j++) {
                    var v = document.getElementById(j + $scope.student[j].sims_enroll_no);
                    if (v.checked == true) {
                        $scope.savedata = $scope.student[j];
                        $scope.temp = $scope.savedata;
                    }
                }
                $('#myModal1').modal('hide');
            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.SaveData = function () {
                debugger
                if ($scope.temp.sims_enroll_no == null || $scope.temp.sims_enroll_no == "") {
                    swal('', 'Please Enter Enroll No.')
                    return;
                }
                if ($scope.edt == undefined || $scope.edt.s_cur_code == undefined || $scope.edt.s_cur_code == null || $scope.edt.sims_academic_year == undefined || $scope.edt.sims_academic_year == null) {
                    swal('', 'Please Select Curriculum And Academic Year');
                    return;
                }
                if ($scope.edt == undefined || $scope.edt.search_std_grade_name == undefined || $scope.edt.search_std_grade_name == null || $scope.edt.search_std_section_name == undefined || $scope.edt.search_std_section_name == null) {
                    swal('', 'Please Select Grade And Section');
                    return;
                }
                var data = {
                    'opr': 'V',
                    'sims_enroll_no': $scope.temp.sims_enroll_no,
                    'sims_cur_code': $scope.temp.sims_cur_code,
                    'sims_academic_year': $scope.temp.sims_academic_year,
                    'sims_grade_code': $scope.temp.sims_grade_code,
                    'sims_section_code': $scope.temp.sims_section_code,
                    'sims_cur_code_new': $scope.edt.s_cur_code,
                    'sims_academic_year_new': $scope.edt.sims_academic_year,
                    'sims_grade_code_new': $scope.edt.search_std_grade_name,
                    'sims_section_code_new': $scope.edt.search_std_section_name,
                }
                datasend.push(data);
                $http.post(ENV.apiUrl + "api/ReAdmission/CUDRe_admission", datasend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Re-Admission is successfully done", width: 300, height: 200 });
                        $scope.reset();
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                        $scope.currentPage = true;
                    }
                });
                datasend = [];
                $scope.table = true;
                $scope.display = true;
            }
        }])
})();