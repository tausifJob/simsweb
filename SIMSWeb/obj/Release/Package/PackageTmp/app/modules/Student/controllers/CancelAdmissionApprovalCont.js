﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Student');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CancelAdmissionApprovalCont',
           ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

               console.clear();

               $scope.pagesize = "5";
               $scope.pageindex = "1";

               // $scope.global_Search_click1 = function () {
               $rootScope.visible_stud = true;
               //$rootScope.visible_parent = false;
               //$rootScope.visible_search_parent = false;
               //$rootScope.visible_teacher = false;
               //$rootScope.visible_User = true;
               //$rootScope.visible_Employee = false;
               $rootScope.chkMulti = false;
               $scope.username = $rootScope.globals.currentUser.username;

               var dt = new Date();
               var date = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();

               $timeout(function () {
                   $("#table").tableHeadFixer({ 'top': 1 });
               }, 100);

               $timeout(function () {
                   $("#fixTable").tableHeadFixer({ 'top': 1 });
               }, 100);

               var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd');
               $scope.dt = {
                   sims_tc_certificate_request_date: dateyear,
               }

               $scope.size = function (str) {
                   $scope.pagesize = str;
                   $scope.currentPage = 1;
                   $scope.numPerPage = str;
                   console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
               }

               $scope.index = function (str) {
                   $scope.pageindex = str;
                   $scope.currentPage = str;
                   console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                   main.checked = false;
                   $scope.CheckAllChecked();
               }

               $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

               $scope.makeTodos = function () {
                   var rem = parseInt($scope.totalItems % $scope.numPerPage);
                   if (rem == '0') {
                       $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                   }
                   else {
                       $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                   }

                   var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                   var end = parseInt(begin) + parseInt($scope.numPerPage);

                   console.log("begin=" + begin); console.log("end=" + end);

                   $scope.filteredTodos = $scope.todos.slice(begin, end);
               };

               $('*[data-datepicker="true"] input[type="text"]').datepicker({
                   todayBtn: true,
                   orientation: "top left",
                   autoclose: true,
                   todayHighlight: true,
                   format: 'yyyy-mm-dd'
               });

               $scope.showdata = function () {
                   debugger
                   $http.get(ENV.apiUrl + "api/CancelAdmissionApproval/getTcCertificateRequest?user=" + $scope.username).then(function (res1) {
                       $scope.obj = res1.data;
                       $scope.totalItems = $scope.obj.length;
                       $scope.todos = $scope.obj;
                       $scope.makeTodos();
                       if ($scope.obj.length == 0) {
                           swal({ title: 'Alert', text: "No Record Found...", showCloseButton: true, width: 450, height: 200 });
                           $scope.tablehide = false;

                       }
                       else {
                           $scope.tablehide = true;
                       }
                   })
               }

               $scope.showdata();


               $scope.Show = function () {
                   $http.get(ENV.apiUrl + "api/StudentReport/getTcCertificateRequiestDate?date_wisesearch=" + $scope.dt.sims_tc_certificate_request_date).then(function (res1) {
                       $scope.obj = res1.data;
                       $scope.totalItems = $scope.obj.length;
                       $scope.todos = $scope.obj;
                       $scope.makeTodos();
                       if ($scope.obj.length == 0) {
                           swal({ title: 'Alert', text: "No Record Found...", showCloseButton: true, width: 450, height: 200 });
                           $scope.tablehide = false;

                       }
                       else {
                           $scope.tablehide = true;
                       }
                   })
               }

               $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                   $('input[type="text"]', $(this).parent()).focus();
               });

               $scope.SearchEnroll = function () {
                   $scope.global_Search_click();
                   $('#Global_Search_Modal').modal({ backdrop: "static" });
               }

               $scope.$on('global_cancel', function () {
                   debugger
                   if ($scope.SelectedUserLst.length > 0) {
                       $scope.edt =
                           {
                               sims_enroll_number: $scope.SelectedUserLst[0].s_enroll_no,
                               sims_student_name: $scope.SelectedUserLst[0].name,
                           }
                   }
               });

               $scope.Save = function (MyForm) {
                   debugger;
                   if (MyForm) {
                       var user = $rootScope.globals.currentUser.username;
                       var Savedata = [];
                       var obj = ({
                           'sims_enroll_number': $scope.edt.sims_enroll_number,
                           'sims_tc_certificate_reason': $scope.edt.sims_tc_certificate_reason,
                           'sims_tc_certificate_requested_by': user,
                           'sims_tc_certificate_request_date': $scope.dt.sims_tc_certificate_request_date,
                           opr: 'I',
                       });

                       Savedata.push(obj);

                       $http.post(ENV.apiUrl + "api/StudentReport/CUDTccertificaterequiest", Savedata).then(function (msg) {

                           $scope.msg1 = msg.data;
                           if ($scope.msg1.strMessage != undefined) {
                               if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                   swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                                   $scope.showdata();
                                   $scope.currentPage = true;
                               }
                           }
                           else {
                               swal({ title: "Alert", text: "Record Not Inserted...", showCloseButton: true, width: 380, });
                           }
                       });

                   }
               }

               $scope.searched = function (valLists, toSearch) {
                   return _.filter(valLists,
                   function (i) {
                       return searchUtil(i, toSearch);
                   });
               };

               $scope.search = function () {
                   $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                   $scope.totalItems = $scope.todos.length;
                   $scope.currentPage = '1';
                   if ($scope.searchText == '') {
                       $scope.todos = $scope.obj;
                   }
                   $scope.makeTodos();
               }

               function searchUtil(item, toSearch) {
                   return (item.sims_tc_certificate_request_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                          item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                          item.sims_tc_certificate_request_status.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                          item.sager == toSearch) ? true : false;
               }

               $scope.clear = function () {
                   $scope.edt = {
                       sims_enroll_number: '',
                       sims_tc_certificate_reason: '',
                   }
                   $scope.searchText = '';
               }

               $scope.Reset = function () {
                   debugger;
                   $scope.clear();
                   $scope.tablehide = false;
               }

               //Pending, Cancelled and Approved

               $scope.cancel_adm = function (info) {
                   debugger
                   $state.go('main.CancelAdmissionClearFees', { data: info });
               }
               $scope.viewDetails = function (clear) {
                   debugger;
                   console.log($scope.clear)
                   //$state.go('main.CancelAdmissionClearFees', { data: $scope.info });
                   $scope.enroll = clear.sims_enroll_number
                   $('#myModal1').modal('show');
                   $http.post(ENV.apiUrl + "api/common/CancelAdmission/CancelAdmission?enroll_no=" + clear.sims_enroll_number + "&cur_code=" + clear.sims_tc_certificate_cur_code + "&acad_yr=" + clear.sims_tc_certificate_academic_year).then(function (res) {
                       debugger
                       $scope.edt = res.data;
                       $scope.clearance = true;
                       console.log($scope.edt);
                   });

                   $scope.chk = [];
                   $http.get(ENV.apiUrl + "api/CancelAdmissionApproval/getApprovalDept?user=" + $scope.username).then(function (check) {
                       debugger;
                       $scope.chk = check.data;
                       $scope.chkshow = $scope.chk[0];
                   })

                   $http.post(ENV.apiUrl + "api/CancelAdmissionApproval/AdmCheckedDept?enroll=" + $scope.enroll).then(function (chkdept) {
                       debugger;
                       $scope.edts = chkdept.data[0];
                       //$scope.chkshow = $scope.chk[0];                                    
                   })
               }

               $scope.check1 = function () {
                   debugger
                   $scope.fee['isSelected'] = fee.isSelected;
                   $scope.fee.isChecked = true;

                   //if (fee.isSelected = true) {
                   //    var fee_user = $scope.username;
                   //}
               }
               $scope.check2 = function () {
                   incidence['isSelected'] = incidence.isSelected;
               }
               $scope.check3 = function () {
                   library['isSelected'] = library.isSelected;
               }
               $scope.check4 = function () {
                   exam['isSelected'] = exam.isSelected;
               }
               $scope.check5 = function () {
                   inventory['isSelected'] = inventory.isSelected;
               }

               $scope.submit = function () {
                   debugger

                   console.log($scope.enroll);
                   $scope.chk = [];
                   $http.get(ENV.apiUrl + "api/CancelAdmissionApproval/getApprovalDept?user=" + $scope.username).then(function (check) {
                       debugger;
                       $scope.chk = check.data;
                       $scope.chkshow = $scope.chk[0];
                   })

                   if ($scope.chkshow.sims_acad_clr_status == true) {
                       var acad_user = $scope.username;
                       var approve_date1 = date;
                   }
                   if ($scope.chkshow.sims_fee_clr_status == true) {
                       var fee_user = $scope.username;
                       var approve_date2 = date;
                   }
                   if ($scope.chkshow.sims_finn_clr_status == true) {
                       var finn_user = $scope.username;
                       var approve_date3 = date;
                   }
                   if ($scope.chkshow.sims_inv_clr_status == true) {
                       var inv_user = $scope.username;
                       var approve_date4 = date;
                   }
                   if ($scope.chkshow.sims_inci_clr_status == true) {
                       var inci_user = $scope.username;
                       var approve_date5 = date;
                   }
                   if ($scope.chkshow.sims_lib_clr_status == true) {
                       var lib_user = $scope.username;
                       var approve_date6 = date;
                   }
                   if ($scope.chkshow.sims_trans_clr_status == true) {
                       var trans_user = $scope.username;
                       var approve_date7 = date;
                   }
                   if ($scope.chkshow.sims_admin_clr_status == true) {
                       var admin_user = $scope.username;
                       var approve_date8 = date;
                   }
                   if ($scope.chkshow.sims_other1_clr_status == true) {
                       var other1_user = $scope.username;
                       var approve_date9 = date;
                   }
                   if ($scope.chkshow.sims_other2_clr_status == true) {
                       var other2_user = $scope.username;
                       var approve_date10 = date;
                   }
                   if ($scope.chkshow.sims_other3_clr_status == true) {
                       var other3_user = $scope.username;
                       var approve_date11 = date;
                   }
                   var data = [];
                   var senddata = {
                       'opr': 'I',
                       'sims_enroll_number': $scope.enroll,
                       'sims_fee_clr_status': $scope.edts.sims_fee_clr_status,
                       'sims_fee_clr_remark': $scope.sims_fee_clr_remark,
                       'sims_fee_clr_by': fee_user,
                       'sims_fee_clr_date': approve_date2,
                       'sims_finn_clr_status': $scope.edts.sims_finn_clr_status,
                       'sims_finn_clr_remark': $scope.sims_finn_clr_remark,
                       'sims_finn_clr_by': finn_user,
                       'sims_finn_clr_date': approve_date3,
                       'sims_inv_clr_status': $scope.edts.sims_inv_clr_status,
                       'sims_inv_clr_remark': $scope.sims_inv_clr_remark,
                       'sims_inv_clr_by': inv_user,
                       'sims_inv_clr_date': approve_date4,
                       'sims_inci_clr_status': $scope.edts.sims_inci_clr_status,
                       'sims_inci_clr_remark': $scope.sims_inci_clr_remark,
                       'sims_inci_clr_by': inci_user,
                       'sims_inci_clr_date': approve_date5,
                       'sims_lib_clr_status': $scope.edts.sims_lib_clr_status,
                       'sims_lib_clr_remark': $scope.sims_lib_clr_remark,
                       'sims_lib_clr_by': lib_user,
                       'sims_lib_clr_date': approve_date6,
                       'sims_trans_clr_status': $scope.edts.sims_trans_clr_status,
                       'sims_trans_clr_remark': $scope.sims_trans_clr_remark,
                       'sims_trans_clr_by': trans_user,
                       'sims_trans_clr_date': approve_date7,
                       'sims_acad_clr_status': $scope.edts.sims_acad_clr_status,
                       'sims_acad_clr_remark': $scope.sims_acad_clr_remark,
                       'sims_acad_clr_by': acad_user,
                       'sims_acad_clr_date': approve_date1,
                       'sims_admin_clr_status': $scope.edts.sims_admin_clr_status,
                       'sims_admin_clr_remark': $scope.sims_admin_clr_remark,
                       'sims_admin_clr_by': admin_user,
                       'sims_admin_clr_date': approve_date8,
                       'sims_other1_clr_status': $scope.edts.sims_other1_clr_status,
                       'sims_other1_clr_remark': $scope.sims_other1_clr_remark,
                       'sims_other1_clr_by': other1_user,
                       'sims_other1_clr_date': approve_date9,
                       'sims_other2_clr_status': $scope.edts.sims_other2_clr_status,
                       'sims_other2_clr_remark': $scope.sims_other2_clr_remark,
                       'sims_other2_clr_by': other2_user,
                       'sims_other2_clr_date': approve_date10,
                       'sims_other3_clr_status': $scope.edts.sims_other3_clr_status,
                       'sims_other3_clr_remark': $scope.sims_other3_clr_remark,
                       'sims_other3_clr_by': other3_user,
                       'sims_other3_clr_date': approve_date11,
                       'sims_clr_status': $scope.edts.sims_clr_status
                   }
                   data.push(senddata);

                   $http.post(ENV.apiUrl + "api/CancelAdmissionApproval/InsertClearStatus", data).then(function (msg) {
                       debugger
                       $scope.msg1 = msg.data;
                       if ($scope.msg1 == true) {
                           $('#myModal1').modal('hide');
                           swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 }).then(function (isConfirm) {
                               $scope.showdata();
                           });
                       }
                       else {
                           swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                       }
                   });
                   data = [];
               }

               $scope.allow = function () {
                   debugger
                   if ($scope.edts.sims_clr_status == true) {
                       swal('', 'Clearance will be directly given if you select this and later you wont be able to approve other departments.');
                   }
               }

               $scope.modal_cancel = function () {
                   //$scope.j = '';
                   $scope.clear = '';
                   $scope.showdata();
               }
           }]);
})();