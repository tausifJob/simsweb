﻿(function () {
    'use strict';
    var opr = '';
    var belldetailscode = [], belldetailsbellcode = [];
    var main;
    var data1 = [];
    var simsController = angular.module('sims.module.Scheduling');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('BellDetailsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';

            delete $scope.edt;

            $scope.table1 = true;
            $http.get(ENV.apiUrl + "api/BellDetails/getBellDetails").then(function (getBellDetails_Data) {
                $scope.BellDetails_data = getBellDetails_Data.data;
                $scope.totalItems = $scope.BellDetails_data.length;
                $scope.todos = $scope.BellDetails_data;
                $scope.makeTodos();
            });

            $http.get(ENV.apiUrl + "api/common/getAllAcademicYear").then(function (get_AllAcademicYear) {
                $scope.ActiveYear = get_AllAcademicYear.data;
            });

            $scope.getBellDesc = function () {

                $http.get(ENV.apiUrl + "api/BellDay/getBellDescByAcy?Acayear=" + $scope.edt.sims_bell_academic_year).then(function (getBellDescByAcy_Data) {
                    $scope.BellDescByAcy = getBellDescByAcy_Data.data;
                });

            }

            $scope.getbellday = function () {
                $http.get(ENV.apiUrl + "api/BellDetails/getBellDayCode?bellCode=" + $scope.edt.sims_bell_code + "&bellyear=" + $scope.edt.sims_bell_academic_year).then(function (BellDay_Code) {
                    $scope.BellDayCode = BellDay_Code.data;
                })
            }

            $scope.operation = false;
            $scope.editmode = false;

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
                $scope.CheckAllChecked();
            }


            $scope.New = function () {

                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';

                $scope.editmode = false;

                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = {};
                $scope.edt['sims_bell_status'] = true;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

            }

            $scope.up = function (str) {

                opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.operation = true;

                $scope.edt = {
                    sims_bell_academic_year: str.sims_bell_academic_year,
                    sims_academic_year_description: str.sims_academic_year_description,
                    sims_bell_code: str.sims_bell_code,
                    sims_bell_day_code: str.sims_bell_day_code,
                    sims_bell_slot_code: str.sims_bell_slot_code,
                    sims_bell_break: str.sims_bell_break,
                    sims_bell_start_time: str.sims_bell_start_time,
                    sims_bell_end_time: str.sims_bell_end_time,
                    sims_bell_slot_desc: str.sims_bell_slot_desc,

                    sims_bell_status: str.sims_bell_status,
                    sims_bell_desc: str.sims_bell_desc,
                    sims_bell_day_desc: str.sims_bell_day_desc

                }

            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Save = function (myForm) {
                debugger
                if (myForm) {
                    data1 = [];
                    var data = $scope.edt;
                    data.opr = 'I';
                    $scope.exist = false;
                    for (var i = 0; i < $scope.BellDetails_data.length; i++) {
                        if ($scope.BellDetails_data[i].sims_bell_slot_code == data.sims_bell_slot_code &&
                            $scope.BellDetails_data[i].sims_bell_academic_year == data.sims_bell_academic_year &&
                            $scope.BellDetails_data[i].sims_bell_code == data.sims_bell_code &&
                            $scope.BellDetails_data[i].sims_bell_day_code == data.sims_bell_day_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ title: "Alert", text: "Record Already Exists", width: 300, height: 200 });
                    }
                    else {
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/BellDetails/BellDetailsCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                            }
                            $http.get(ENV.apiUrl + "api/BellDetails/getBellDetails").then(function (getBellDetails_Data) {
                                $scope.BellDetails_data = getBellDetails_Data.data;
                                $scope.totalItems = $scope.BellDetails_data.length;
                                $scope.todos = $scope.BellDetails_data;
                                $scope.makeTodos();
                            });

                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                }
            }

            $scope.Update = function (myForm) {
                if (myForm) {
                    data1 = [];
                    var data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/BellDetails/BellDetailsCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                        $http.get(ENV.apiUrl + "api/BellDetails/getBellDetails").then(function (getBellDetails_Data) {
                            $scope.BellDetails_data = getBellDetails_Data.data;
                            $scope.totalItems = $scope.BellDetails_data.length;
                            $scope.todos = $scope.BellDetails_data;
                            $scope.makeTodos();
                        });
                    });
                    $scope.operation = false;
                    $scope.table1 = true;
                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                debugger
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_bell_slot_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_bell_slot_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.deleterecord = function () {
                belldetailscode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_bell_slot_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletebelldaycode = ({
                            'sims_bell_academic_year': $scope.filteredTodos[i].sims_bell_academic_year,
                            'sims_bell_day_code': $scope.filteredTodos[i].sims_bell_day_code,
                            'sims_bell_code': $scope.filteredTodos[i].sims_bell_code,
                            'sims_bell_slot_code': $scope.filteredTodos[i].sims_bell_slot_code,
                            opr: 'D'
                        });
                        belldetailscode.push(deletebelldaycode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger
                            $http.post(ENV.apiUrl + "api/BellDetails/BellDetailsCUD", belldetailscode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BellDetails/getBellDetails").then(function (getBellDetails_Data) {
                                                $scope.BellDetails_data = getBellDetails_Data.data;
                                                $scope.totalItems = $scope.BellDetails_data.length;
                                                $scope.todos = $scope.BellDetails_data;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BellDetails/getBellDetails").then(function (getBellDetails_Data) {
                                                $scope.BellDetails_data = getBellDetails_Data.data;
                                                $scope.totalItems = $scope.BellDetails_data.length;
                                                $scope.todos = $scope.BellDetails_data;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_bell_slot_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.BellDetails_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.BellDetails_data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                     item.sims_bell_academic_year.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_bell_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_bell_day_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                     item.sims_bell_start_time.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_bell_end_time.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_bell_slot_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                    item.sims_bell_slot_code == toSearch) ? true : false;
            }

            $('.clockpicker').clockpicker({
                autoclose: true
            });



        }])
})();
