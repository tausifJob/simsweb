﻿(function () {
    'use strict';
    var opr = '';
    var bellslotcode = [];
    var main;
    var data1 = [];
    var simsController = angular.module('sims.module.Scheduling');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('BellSlotGroupCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);
            $scope.table1 = true;
            $http.get(ENV.apiUrl + "api/BellSlotGroup/getBellslot").then(function (BellSlotG_Data) {
                $scope.BellSlotGData = BellSlotG_Data.data;
                $scope.totalItems = $scope.BellSlotGData.length;
                $scope.todos = $scope.BellSlotGData;
                $scope.makeTodos();

                console.log($scope.BellSlotGData);
            });

            $scope.operation = false;
            $scope.editmode = false;

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }
            $scope.edt = "";

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.newmode = true;
                $scope.check = true;

                $scope.editmode = false;

                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = {};
                //$scope.edt['sims_bell_slot_group_status'] = true;
            }

            $scope.up = function (str) {

                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.edt = {
                    sims_bell_slot_group: str.sims_bell_slot_group,
                    sims_bell_slot_group_desc: str.sims_bell_slot_group_desc,
                    sims_bell_slot_group_status: str.sims_bell_slot_group_status

                }
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.edt = {
                    sims_bell_slot_group: "",
                    sims_bell_slot_group_desc: "",
                    sims_bell_slot_group_status: ""
                }
            }

            $scope.Save = function (myForm) {
                if (myForm) {
                    data1 = [];
                    var data = $scope.edt;
                    data.opr = 'I';
                    $scope.exist = false;
                    for (var i = 0; i < $scope.BellSlotGData.length; i++) {
                        if ($scope.BellSlotGData[i].sims_bell_slot_group == data.sims_bell_slot_group) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ title: "Alert", text: "Record Already Exists", width: 300, height: 200 });
                    }
                    else {
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/BellSlotGroup/BellSlotCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                            }
                            $http.get(ENV.apiUrl + "api/BellSlotGroup/getBellslot").then(function (BellSlotG_Data) {
                                $scope.BellSlotGData = BellSlotG_Data.data;
                                $scope.totalItems = $scope.BellSlotGData.length;
                                $scope.todos = $scope.BellSlotGData;
                                $scope.makeTodos();
                            });
                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                }
            }

            $scope.Update = function (myForm) {

                if (myForm) {
                    data1 = [];
                    var data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/BellSlotGroup/BellSlotCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                        $http.get(ENV.apiUrl + "api/BellSlotGroup/getBellslot").then(function (BellSlotG_Data) {
                            $scope.BellSlotGData = BellSlotG_Data.data;
                            $scope.totalItems = $scope.BellSlotGData.length;
                            $scope.todos = $scope.BellSlotGData;
                            $scope.makeTodos();
                        });
                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                }
            }
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {

                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.deleterecord = function () {
                bellslotcode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletebelldaycode = ({
                            'sims_bell_slot_group': $scope.filteredTodos[i].sims_bell_slot_group,
                            opr: 'D'
                        });
                        bellslotcode.push(deletebelldaycode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger
                            $http.post(ENV.apiUrl + "api/BellSlotGroup/BellSlotCUD", bellslotcode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BellSlotGroup/getBellslot").then(function (BellSlotG_Data) {
                                                $scope.BellSlotGData = BellSlotG_Data.data;
                                                $scope.totalItems = $scope.BellSlotGData.length;
                                                $scope.todos = $scope.BellSlotGData;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BellSlotGroup/getBellslot").then(function (BellSlotG_Data) {
                                                $scope.BellSlotGData = BellSlotG_Data.data;
                                                $scope.totalItems = $scope.BellSlotGData.length;
                                                $scope.todos = $scope.BellSlotGData;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
                main.checked = false;
                $scope.CheckAllChecked();
            }



            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.BellSlotGData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.BellSlotGData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                         item.sims_bell_slot_group_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.sims_bell_slot_group == toSearch) ? true : false;
            }


        }])
})();
