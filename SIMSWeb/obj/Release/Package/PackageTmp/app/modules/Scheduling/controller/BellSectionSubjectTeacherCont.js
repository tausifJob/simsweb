﻿
(function () {
    'use strict';
    var subject1 = [], subject, Employee_code = [], subject_code = [], bell_code = [], lesson_type = [], group_order = [];
    var le = 0;
    var main, strMessage;

    var simsController = angular.module('sims.module.Scheduling');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('BellSectionSubjectTeacherCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ "top": 1 });
            }, 100);
            $scope.Grid = true;

            $scope.table = false;
            $scope.insert = true;
            $scope.busyindicator = true;
            $scope.edt = {};
            $scope.getacyr = function (str) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.getAcademicYear = Academicyear.data;
                    $scope.edt['academic_year'] = $scope.getAcademicYear[0].sims_academic_year;
                });
            }



            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;
                $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code
                $scope.getacyr($scope.curriculum[0].sims_cur_code)

            });


            $http.get(ENV.apiUrl + "api/bellsectionsubject/GetAllTeacherName").then(function (GetAllTeacherName) {
                $scope.GetAllTeacherName = GetAllTeacherName.data;
            });

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;
            });

            $scope.Show_Data = function () {
                $scope.expanded = true;
                $scope.busyindicator = false;

                $http.get(ENV.apiUrl + "api/bellsectionsubject/GetSectionSubjectTeacher?data=" + JSON.stringify($scope.edt)).then(function (AllTeacher_Name) {

                    $scope.All_Teacher_Name1 = AllTeacher_Name.data;

                    for (var i = 0; i < $scope.All_Teacher_Name1.length; i++) {
                        if ($scope.All_Teacher_Name1[i].sims_bell_code == "" && $scope.All_Teacher_Name1[i].sims_bell_desc == "" && $scope.All_Teacher_Name1[i].sims_bell_group_order == "" && $scope.All_Teacher_Name1[i].sims_bell_slot_group == "" && $scope.All_Teacher_Name1[i].sims_bell_slot_group_desc == "" && $scope.All_Teacher_Name1[i].sims_bell_slot_room == "" && $scope.All_Teacher_Name1[i].sims_bell_slot_room_desc == "" && $scope.All_Teacher_Name1[i].sims_gb_teacher_code_name == "" && $scope.All_Teacher_Name1[i].sims_teacher_code == "") {
                            $scope.All_Teacher_Name1[i].color = '#e8f7c2';
                        }
                    }

                    $scope.All_Teacher_Name = $scope.All_Teacher_Name1;
                    var subject_names1 = [];

                    $http.get(ENV.apiUrl + "api/common/SectionSubject/getSubjectByIndex?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.grade_code + "&academicyear=" + $scope.edt.academic_year + "&sectioncode=" + $scope.edt.section_code).then(function (allSection_Subject) {

                        $scope.SectionSubject5 = allSection_Subject.data;
                        $scope.busyindicator = true;
                        $scope.table = true;
                        for (var i = 0; i < $scope.SectionSubject5.length; i++) {

                            if ($scope.SectionSubject5[i].sims_subject_status == true) {

                                var data = ({
                                    'sims_subject_code': $scope.SectionSubject5[i].sims_subject_code,
                                    'sims_subject_name': $scope.SectionSubject5[i].sims_subject_name
                                })
                                subject_names1.push(data);
                            }
                        }
                        $scope.subject_names = subject_names1;
                    });
                });

            }

            $scope.SaveBellData = function (isvalid, str) {


                if (isvalid) {
                    var data;
                    if ($scope.temp !== undefined) {
                        data = {
                            'academic_year': $scope.edt.academic_year,
                            'sims_bell_desc': str.sims_bell_desc,
                            'sims_grade_code': $scope.edt.grade_code,
                            'sims_section_code': $scope.edt.section_code,
                            'sims_subject_code': str.sims_subject_code,
                            'sims_gb_teacher_code': str.sims_gb_teacher_code_name,
                            'sims_bell_lecutre_count': str.sims_bell_lecutre_count,
                            'sims_bell_lecture_practical_count': str.sims_bell_lecture_practical_count,
                            'sims_bell_slot_group_code': str.sims_bell_slot_group_code,
                            'sims_bell_slot_room': str.sims_bell_slot_room,
                            'sims_bell_group_order': str.sims_bell_group_order,
                            'sims_bell_lecture_per_week': str.sims_bell_lecture_per_week,
                            'sims_bell_slot_per_lecture': str.sims_bell_slot_per_lecture,
                            'sims_bell_practical_per_week': str.sims_bell_practical_per_week,
                            'sims_bell_slot_per_practical': str.sims_bell_slot_per_practical,
                            'sims_lesson_type': str.sims_lesson_type,
                            'opr': 'I'
                        }
                        $http.post(ENV.apiUrl + "api/bellsectionsubject/InsertSectionSubjectTeacher", data).then(function (msg) {

                            $scope.data = '';
                            $scope.msg1 = msg.data;

                            if ($scope.msg1 == true) {
                                swal({ text: 'Information Inserted Successfully', width: 300, showCloseButton: true });

                                $scope.table = true;
                                $scope.Grid = true;
                                $scope.insert = true;
                                $scope.temp = [];
                                $scope.Show_Data();
                            }

                            else {

                                swal({ text: 'Information Not Inserted', width: 300, showCloseButton: true });

                            }

                            $scope.temp = '';
                            $scope.table = true;
                        });

                    }
                    else {
                        swal({ text: 'Please Fill Field(S) To Insert Record', width: 300, showCloseButton: true });
                        $scope.table = true;
                    }
                }
                $scope.data = '';
            }

            $scope.UpdateBellData = function () {


                debugger;

                $scope.teacherupdate = false;

                var Sdata = [{}];
                for (var i = 0; i < $scope.All_Teacher_Name.length; i++) {
                    if ($scope.All_Teacher_Name[i].sims_teacher_code == teacher_code1) {
                        if (teacher_code1 != '' && $scope.All_Teacher_Name[i].ischange == true) {


                            $scope.teacherupdate = true;
                            var le = Sdata.length;
                            Sdata[le] = {
                                'academic_year': $scope.edt.academic_year,
                                'sims_bell_desc': $scope.All_Teacher_Name[i].sims_bell_code,
                                'sims_grade_code': $scope.edt.grade_code,
                                'sims_section_code': $scope.edt.section_code,
                                'sims_subject_code': $scope.All_Teacher_Name[i].sims_subject_code,
                                'sims_gb_teacher_code': $scope.All_Teacher_Name[i].sims_gb_teacher_code_name,
                                'sims_bell_lecutre_count': $scope.All_Teacher_Name[i].sims_bell_lecutre_count,
                                'sims_bell_lecture_practical_count': $scope.All_Teacher_Name[i].sims_bell_lecture_practical_count,
                                'sims_bell_slot_group_code': $scope.All_Teacher_Name[i].sims_bell_slot_group,
                                'sims_bell_slot_room': $scope.All_Teacher_Name[i].sims_bell_slot_room,

                                'sims_bell_group_order': $scope.All_Teacher_Name[i].sims_bell_group_order,
                                'sims_bell_lecture_per_week': $scope.All_Teacher_Name[i].sims_bell_lecture_per_week,
                                'sims_bell_slot_per_lecture': $scope.All_Teacher_Name[i].sims_bell_slot_per_lecture,
                                'sims_bell_practical_per_week': $scope.All_Teacher_Name[i].sims_bell_practical_per_week,
                                'sims_bell_slot_per_practical': $scope.All_Teacher_Name[i].sims_bell_slot_per_practical,
                                'sims_lesson_type': $scope.All_Teacher_Name[i].sims_lesson_type1,
                                'sims_old_teacher_code': $scope.All_Teacher_Name[i].sims_teacher_code,
                                'opr': 'U'
                            }
                        }
                    }
                }
                if ($scope.teacherupdate == false) {
                    for (var i = 0; i < $scope.All_Teacher_Name.length; i++) {

                        if ($scope.All_Teacher_Name[i].ischange == true) {
                            if ($scope.All_Teacher_Name[i].sims_bell_group_order == "" || $scope.All_Teacher_Name[i].sims_bell_group_order == undefined || $scope.All_Teacher_Name[i].sims_bell_group_order == null) {

                                $scope.All_Teacher_Name[i].sims_bell_group_order = '1';
                            }
                            var le = Sdata.length;
                            Sdata[le] = {
                                'academic_year': $scope.edt.academic_year,
                                'sims_bell_desc': $scope.All_Teacher_Name[i].sims_bell_code,
                                'sims_grade_code': $scope.edt.grade_code,
                                'sims_section_code': $scope.edt.section_code,
                                'sims_subject_code': $scope.All_Teacher_Name[i].sims_subject_code,
                                'sims_gb_teacher_code': $scope.All_Teacher_Name[i].sims_gb_teacher_code_name,
                                'sims_bell_lecutre_count': $scope.All_Teacher_Name[i].sims_bell_lecutre_count,
                                'sims_bell_lecture_practical_count': $scope.All_Teacher_Name[i].sims_bell_lecture_practical_count,
                                'sims_bell_slot_group_code': $scope.All_Teacher_Name[i].sims_bell_slot_group,
                                'sims_bell_slot_room': $scope.All_Teacher_Name[i].sims_bell_slot_room,

                                'sims_bell_group_order': $scope.All_Teacher_Name[i].sims_bell_group_order,
                                'sims_bell_lecture_per_week': $scope.All_Teacher_Name[i].sims_bell_lecture_per_week,
                                'sims_bell_slot_per_lecture': $scope.All_Teacher_Name[i].sims_bell_slot_per_lecture,
                                'sims_bell_practical_per_week': $scope.All_Teacher_Name[i].sims_bell_practical_per_week,
                                'sims_bell_slot_per_practical': $scope.All_Teacher_Name[i].sims_bell_slot_per_practical,
                                'sims_lesson_type': $scope.All_Teacher_Name[i].sims_lesson_type1,
                                'sims_old_teacher_code': $scope.All_Teacher_Name[i].sims_teacher_code,
                                'opr': 'U'
                            }
                        }
                    }
                }


                Sdata.splice(0, 1);
                var data = Sdata;

                if (data.length > 0) {
                    $http.post(ENV.apiUrl + "api/bellsectionsubject/UpdateSectionSubjectTeacher?simsobj=", data).then(function (msg) {

                        $scope.busyindicator = false;
                        $scope.table = false;

                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: 'Information Updated Successfully', width: 300, showCloseButton: true });
                            $scope.teacher_code = '';
                            $scope.Show_Data();
                            $scope.busyindicator = true;
                            $scope.table = true;
                            //swal({text: 'Information Updated Successfully', timer: 2000, showConfirmButton: false });
                        }

                        else {

                            swal({ text: 'Information Not Updated', width: 300, showCloseButton: true });
                            $scope.Show_Data();
                            $scope.table = true;
                            $scope.busyindicator = true;
                        }
                    });
                }
                else {
                    swal({ text: 'Please Change At Least One Field To Update', width: 380, showCloseButton: true });
                    $scope.table = true;
                    $scope.busyindicator = true;
                }
            }

            $scope.CheckClassTeacherExist = function (str) {

                str.ischange = true;

                var ischeck = str.sims_section_code;

                var data = {
                    'sims_cur_code': $scope.temp.sims_cur_code,
                    'academic_year': $scope.temp.academic_year,
                    'sims_grade_code': str.sims_grade_code,
                    'sims_section_code': str.sims_section_code,
                    'opr': 'C'

                };

                $http.post(ENV.apiUrl + "api/classteachermapping/CheckExist?simsobj=", data).then(function (isexistteacher) {

                    $scope.isexist_teacher = isexistteacher.data;

                    for (var i = 0; i < $scope.isexist_teacher.length; i++) {


                        if (str.sims_section_code == $scope.isexist_teacher[i].sims_section_code && str.sims_teacher_code == $scope.isexist_teacher[i].sims_teacher_code) {
                            str.sims_section_code = $scope.isexist_teacher[i].sims_section_code;
                        }

                        else if (str.sims_section_code == $scope.isexist_teacher[i].sims_section_code) {

                            for (var i = 0; i < $scope.isexist_teacher.length; i++) {

                                str.sims_section_code = '';
                                strMessage = $scope.isexist_teacher[i].sims_grade_name + '    ' + 'Of' + '    ' + $scope.isexist_teacher[i].sims_section_name + '      ' + 'This Class Already Assigned To' + '    ' + $scope.isexist_teacher[i].sims_teacher_name;
                                swal('', strMessage)
                                break;

                            }
                        }

                        else {

                            le = sectionlist.length;

                            if (le == 0) {
                                str.sims_section_code = str.sims_section_code;
                                sectionlist = [{ section: str.sims_section_code, teacher_code: str.sims_teacher_code }];
                            }

                            else if (le > 0) {
                                $scope.flag = true;
                                for (var n = 0; n < sectionlist.length; n++) {
                                    if (str.sims_section_code == sectionlist[n].section && str.sims_teacher_code == sectionlist[n].teacher_code) {
                                        str.sims_section_code = sectionlist[n].section;
                                    }
                                    else {
                                        $scope.flag = false;
                                        for (var j = 0; j < $scope.All_Teacher_Name.length; j++) {
                                            if (str.sims_section_code == $scope.All_Teacher_Name[j].sims_section_code) {
                                                str.sims_section_code = '';
                                                strMessage = 'This Class Already Assigned To' + '    ' + $scope.All_Teacher_Name[j].sims_teacher_name;
                                                swal('', strMessage);

                                                for (var s = 0; s < sectionlist.length; s++) {
                                                    if (str.sims_teacher_code == sectionlist[s].teacher_code) {
                                                        var index = sectionlist.indexOf(str.sims_section_code);
                                                        sectionlist.splice(index, 1);
                                                        $scope.All_Teacher_Name[j].sims_section_code = '';
                                                        break;
                                                    }
                                                }
                                                break;
                                            }

                                        }

                                    }

                                }
                                if ($scope.flag == true) {
                                    str.sims_section_code == str.sims_section_code;
                                    sectionlist = [{ section: str.sims_section_code }];
                                }

                            }


                        }
                    }

                });

            }

            $scope.CheckOnebyOneDelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });
            }

            $scope.DeleteTeacherData = function () {
                for (var i = 0; i < $scope.All_Teacher_Name.length; i++) {
                    var t = document.getElementById($scope.All_Teacher_Name[i].sims_teacher_code + $scope.All_Teacher_Name[i].sims_subject_code)
                    if (t.checked == true) {
                        Employee_code = Employee_code + $scope.All_Teacher_Name[i].sims_teacher_code + ',';
                        subject_code = subject_code + $scope.All_Teacher_Name[i].sims_subject_code + ',';
                        bell_code = bell_code + $scope.All_Teacher_Name[i].sims_bell_code + ',';
                        lesson_type = lesson_type + $scope.All_Teacher_Name[i].sims_lession_type1 + ',';
                        group_order = group_order + $scope.All_Teacher_Name[i].sims_bell_group_order + ',';

                    }
                }
                var Employee_code_list = ({
                    'sims_cur_code': $scope.edt.sims_cur_code,
                    'sims_grade_code': $scope.edt.grade_code,
                    'sims_section_code': $scope.edt.section_code,
                    'academic_year': $scope.edt.academic_year,
                    'sims_gb_teacher_code': Employee_code,
                    'sims_subject_code': subject_code,
                    'sims_bell_desc': bell_code,
                    'sims_lesson_type': lesson_type,
                    'sims_bell_group_order': group_order,
                    'opr': 'D'
                });



                $http.post(ENV.apiUrl + "api/bellsectionsubject/SectionSubjectTeacherDelete?simsobj=", Employee_code_list).then(function (msg) {

                    Employee_code = [];
                    subject_code = [], bell_code = [], lesson_type = [];
                    group_order = [];
                    $scope.msg1 = msg.data;
                    Employee_code_list = {};
                    if ($scope.msg1 == true) {
                        swal({ text: 'Information Deleted Successfully', width: 300, showCloseButton: true });
                        $scope.Show_Data();
                    }
                    else {
                        swal({ text: 'Information Not Deleted', width: 300, showCloseButton: true });
                    }
                });
            }

            $scope.Check = function (teacher) {
                teacher.ischange = true;
            }

            $scope.showcmb = function (str) {
                for (var i = 0; i < $scope.All_Teacher_Name.length; i++) {
                    if (str == $scope.All_Teacher_Name[i].sims_subject_name)
                        $scope[str] = true;
                }
            }

            var teacher_code1 = '';

            $scope.CheckTeacher = function (str) {
                teacher_code1 = str.sims_teacher_code;
                $scope.Check(str);
            }

            $scope.CheckteacherAssigned = function (teacher_name1, subject_name) {

                //for (var i = 0; i < $scope.GetAllTeacherName.length; i++)
                //    if ($scope.GetAllTeacherName[i].sims_gb_teacher_code_name == teacher_name1) {
                //        $scope.teacher_code = $scope.GetAllTeacherName[i].sims_teacher_code;
                //    }


                for (var i = 0; i < $scope.All_Teacher_Name.length; i++) {
                    if ($scope.All_Teacher_Name[i].sims_teacher_code == teacher_name1 && $scope.All_Teacher_Name[i].sims_subject_code == subject_name) {
                        swal('', 'Please Select Another Teacher, This Teacher  already Assiged Same Subject ' + '   ' + $scope.All_Teacher_Name[i].sims_subject_name);
                        $scope.temp.sims_gb_teacher_code = '';
                    }
                }
                $scope.new = true;
            }

            $scope.AddNewData = function (info) {
                //$scope.NewData = true;
                $scope.Grid = true;
                $scope.table = false;
                $scope.copyobject = angular.copy(info);

                $scope.temp = [];

                $scope.copyobject.sims_bell_code = '';
                $scope.copyobject.sims_bell_desc = '';

                $scope.copyobject.sims_bell_slot_room = '';
                $scope.copyobject.sims_bell_slot_room_desc = '';
                $scope.copyobject.sims_gb_teacher_code_name = '';
                $scope.copyobject.sims_grade_section = '';
                $scope.copyobject.sims_lession_type1 = '';
                $scope.copyobject.sims_lesson_type = '';
                $scope.copyobject.sims_pre_status = '';
                $scope.copyobject.sims_section_code
                $scope.copyobject.sims_sub_status
                $scope.copyobject.sims_subject_code
                $scope.copyobject.sims_subject_name
                $scope.copyobject.sims_teacher_code = '';

                $scope.temp.push($scope.copyobject);
                $scope.temp['sims_subject_code'] = $scope.copyobject.sims_subject_code
                $scope.temp['sims_bell_group_order'] = 0;
                $scope.temp['sims_bell_lecture_per_week'] = 0;
                $scope.temp['sims_bell_lecture_practical_count'] = 0;
                $scope.temp['sims_bell_lecutre_count'] = 0;
                $scope.temp['sims_bell_practical_per_week'] = 0;
                $scope.temp['sims_bell_slot_group'] = 0;
                $scope.temp['sims_bell_slot_group_desc'] = 0;
                $scope.temp['sims_bell_slot_per_lecture'] = 0;
                $scope.temp['sims_bell_slot_per_practical'] = 0;
                $scope.insert = false;
            }

            $scope.Cancel = function () {
                $scope.Grid = true;
                $scope.insert = true;
                $scope.temp = [];
                $scope.table = true;
            }


        }])
})();


