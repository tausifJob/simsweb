﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CurriculumCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.curriculum_data = [];
            $scope.edit_code = false;
            var data1 = [];
            var deletecode = [];


            $timeout(function () {
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $http.get(ENV.apiUrl + "api/common/Curriculum/getCurriculum").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.curriculum_data = res.data;
                $scope.totalItems = $scope.curriculum_data.length;
                $scope.todos = $scope.curriculum_data;
                $scope.makeTodos();

                for (var i = 0; i < $scope.totalItems; i++) {
                    $scope.curriculum_data[i].icon = "fa fa-plus-circle";
                }
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edt =
                    {
                        curriculum_code: str.curriculum_code,
                        curriculum_short_name: str.curriculum_short_name,
                        curriculum_short_name_ar: str.curriculum_short_name_ar,
                        curriculum_short_name_fr: str.curriculum_short_name_fr,
                        curriculum_short_name_ot: str.curriculum_short_name_ot,
                        curriculum_full_name: str.curriculum_full_name,
                        curriculum_full_name_ar: str.curriculum_full_name_ar,
                        curriculum_full_name_fr: str.curriculum_full_name_fr,
                        curriculum_full_name_ot: str.curriculum_full_name_ot,
                        curriculum_status: str.curriculum_status,
                    }
                $scope.edit_code = true;
            }

            $scope.Save = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = ({
                            curriculum_code: $scope.edt.curriculum_code,
                            curriculum_short_name: $scope.edt.curriculum_short_name,
                            curriculum_short_name_ar: $scope.edt.curriculum_short_name_ar,
                            curriculum_short_name_fr: $scope.edt.curriculum_short_name_fr,
                            curriculum_short_name_ot: $scope.edt.curriculum_short_name_ot,
                            curriculum_full_name: $scope.edt.curriculum_full_name,
                            curriculum_full_name_ar: $scope.edt.curriculum_full_name_ar,
                            curriculum_full_name_fr: $scope.edt.curriculum_full_name_fr,
                            curriculum_full_name_ot: $scope.edt.curriculum_full_name_ot,
                            curriculum_status: $scope.edt.curriculum_status,
                            opr: 'I'
                        });

                        data1.push(data);

                        $http.post(ENV.apiUrl + "api/common/Curriculum/CUDInsertCurriculum", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Curriculum  Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal({ title: "Alert", text: "Curriculum Record Already Exists", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/common/Curriculum/getCurriculum").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.curriculum_data = res.data;
                    $scope.totalItems = $scope.curriculum_data.length;
                    $scope.todos = $scope.curriculum_data;
                    $scope.makeTodos();

                    for (var i = 0; i < $scope.totalItems; i++) {
                        $scope.curriculum_data[i].icon = "fa fa-plus-circle";
                    }

                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    var data = ({
                        curriculum_code: $scope.edt.curriculum_code,
                        curriculum_short_name: $scope.edt.curriculum_short_name,
                        curriculum_short_name_ar: $scope.edt.curriculum_short_name_ar,
                        curriculum_short_name_fr: $scope.edt.curriculum_short_name_fr,
                        curriculum_short_name_ot: $scope.edt.curriculum_short_name_ot,
                        curriculum_full_name: $scope.edt.curriculum_full_name,
                        curriculum_full_name_ar: $scope.edt.curriculum_full_name_ar,
                        curriculum_full_name_fr: $scope.edt.curriculum_full_name_fr,
                        curriculum_full_name_ot: $scope.edt.curriculum_full_name_ot,
                        curriculum_status: $scope.edt.curriculum_status,
                        opr: 'U'
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/Curriculum/CUDCurriculum", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Curriculum Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    //, imageUrl: "assets/img/check.png", 
                                    $scope.getgrid();

                                }
                            });
                        }
                        else {
                            swal({ title: "Alert", text: "Curriculum  Not Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                // imageUrl: "assets/img/notification-alert.png",
                                if (isConfirm) {
                                    //, imageUrl: "assets/img/check.png", 
                                    $scope.getgrid();
                                }
                            });
                        }
                    });
                }
            }

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
                $scope.edit_code = false;
                $scope.edt=
                    {
                        curriculum_status:true
                    }
            }

            $scope.cancel = function () {
                $scope.edt = "";
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Delete = function () {

                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].curriculum_code;
                    var v = document.getElementById(t);

                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'curriculum_code': $scope.filteredTodos[i].curriculum_code,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {


                            $http.post(ENV.apiUrl + "api/common/Curriculum/CUDCurriculum", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Curriculum  Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        //, imageUrl: "assets/img/check.png",
                                        if (isConfirm) {
                                            //, imageUrl: "assets/img/check.png", 
                                            $scope.getgrid();
                                        }

                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Curriculum  Not Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        //, imageUrl: "assets/img/notification-alert.png",
                                        if (isConfirm) {
                                            //, imageUrl: "assets/img/check.png", 
                                            $scope.getgrid();
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var t = $scope.filteredTodos[i].curriculum_code;
                                var v = document.getElementById(t);

                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }

            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        // var v = document.getElementById(i);
                        var t = $scope.filteredTodos[i].curriculum_code;
                        var v = document.getElementById(t);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].curriculum_code;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        //$scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        //$scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    //main.checked = false;
                    //$scope.color = '#edefef';
                    //$scope.row1 = '';

                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
                //else
                //{
                //    $scope.color = '#fff';
                //    $scope.row1 = '';
                //}
            }


            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            var dom;
            var count = 0;

            $scope.flag = true;
            $scope.icon = "fa fa-plus-circle";

            $scope.expand = function (info, $event) {
                console.log(info);
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                    "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                    "<tbody>" +
                     "<tr> <td class='semi-bold'>" + gettextCatalog.getString('French Short Name') + "</td> <td class='semi-bold'>" + gettextCatalog.getString('Regional Short Name') + " </td><td class='semi-bold'>" + gettextCatalog.getString('French Full Name') + "</td><td class='semi-bold'>" + gettextCatalog.getString('Regional Full Name') + "</td>" +
                    "</tr>" +

                      "<tr><td>" + (info.curriculum_short_name_fr) + "</td> <td>" + (info.curriculum_short_name_ot) + " </td><td>" + (info.curriculum_full_name_fr) + "</td><td>" + (info.curriculum_full_name_ot) + "</td>" +
                    "</tr>" +

                    " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);

                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    info.icon = "fa fa-plus-circle";
                    $scope.flag = true;
                }
            };

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.curriculum_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.curriculum_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].curriculum_code;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }

                }


            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.curriculum_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.curriculum_short_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  || item.curriculum_full_name == toSearch) ? true : false;
            }

        }])


})();