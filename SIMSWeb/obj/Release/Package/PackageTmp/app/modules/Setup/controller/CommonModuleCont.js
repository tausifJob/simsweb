﻿(function () {
    'use strict';

    var main, modulecode = [];
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CommonModuleCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.pagesize = "5";

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $http.get(ENV.apiUrl + "api/ModuleMaster/getModulesByIndex").then(function (res) {
                $scope.display = false;
                $scope.obj = res.data;
                for (var i = 0; i < $scope.obj.length; i++) {

                    $scope.obj[i].icon = "fa fa-plus-circle";

                }

                $scope.totalItems = $scope.obj.length;
                $scope.todos = $scope.obj;


                $scope.makeTodos();
                $scope.grid = true;
            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
                
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

            };

            $scope.edit = function (str) {

                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                var data = angular.copy(str);
                $scope.edt = data;

                if (main.checked == true) {

                    for (var i = 0; i < $scope.obj.length; i++) {
                        var t = $scope.obj[i].module_code; console.log(t);
                        var v = document.getElementById(t);
                        v.checked = false;

                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
               $scope.makeTodos();
              
               
            }

            $scope.createdate = function (date) {

                var date = new Date(date);
                var month = (date.getMonth() + 1);
                var day = date.getDate();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                var d = date.getFullYear() + '-' + (month) + '-' + (day);
                $scope.edt.module_create_date = d;
            }

            $scope.SaveData = function (isvalid) {

                if (isvalid) {

                    var data = $scope.edt;

                    $http.post(ENV.apiUrl + "api/ModuleMaster/CUDinsertModule", data).then(function (msg) {

                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: 'Record Inserted Successfully', width: 380, showCloseButton: true });

                            $http.get(ENV.apiUrl + "api/ModuleMaster/getModulesByIndex").then(function (res) {
                                $scope.display = false;
                                $scope.obj = res.data;
                                for (var i = 0; i < $scope.obj.length; i++) {
                                    $scope.obj[i].icon = "fa fa-plus-circle";

                                }

                                $scope.totalItems = $scope.obj.length;
                                $scope.todos = $scope.obj;


                                $scope.makeTodos();
                                $scope.grid = true;
                            });
                        }
                        else {
                            $scope.makeTodos();
                            swal({ text: 'Record Not Inserted', width: 380, showCloseButton: true });

                        }

                    })

                }
              

            }

            $scope.Update = function () {
                var data = $scope.edt;
                $http.post(ENV.apiUrl + "api/ModuleMaster/CUDupdateModule", data).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ text: 'Record Updated Successfully', width: 380, showCloseButton: true });

                        $http.get(ENV.apiUrl + "api/ModuleMaster/getModulesByIndex").then(function (res) {
                            $scope.display = false;
                            $scope.obj = res.data;
                            for (var i = 0; i < $scope.obj.length; i++) {
                                $scope.obj[i].icon = "fa fa-plus-circle";
                            }
                            $scope.totalItems = $scope.obj.length;
                            $scope.todos = $scope.obj;
                            $scope.makeTodos();
                            $scope.grid = true;
                        });
                    }
                    else {
                        $scope.makeTodos();
                        swal({ text: 'Record Not Updated', width: 380, showCloseButton: true });

                    }
                })
            }

            var DeleteData = '';

            $scope.Delete = function () {
               
               var  flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t =document.getElementById($scope.filteredTodos[i].module_code);

                    if (t.checked == true) {
                        flag = true;
                        DeleteData = DeleteData + $scope.filteredTodos[i].module_code + ',';
                    }
                }

                if (flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            data = { module_code: DeleteData };
                            $http.post(ENV.apiUrl + "api/ModuleMaster/CUDDELETEModule", data).then(function (msg) {
                                $scope.msg1 = msg.data;
                                DeleteData = '';
                                swal({ text: $scope.msg1.strMessage, width: 380, showCloseButton: true });

                                $http.get(ENV.apiUrl + "api/ModuleMaster/getModulesByIndex").then(function (res) {
                                    $scope.display = false;
                                    $scope.obj = res.data;
                                    for (var i = 0; i < $scope.obj.length; i++) {
                                        $scope.obj[i].icon = "fa fa-plus-circle";
                                    }
                                    $scope.totalItems = $scope.obj.length;
                                    $scope.todos = $scope.obj;
                                    $scope.makeTodos();
                                    $scope.grid = true;
                                });

                            })

                        }
                        else {
                            
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            $scope.CheckAllChecked();
                        }
                    });
                }
                else {
                    DeleteData = '';
                    swal({ text: 'Select At Least One Record To Delete', width: 380, showCloseButton: true });
                }

            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
            }

            $scope.New = function () {
                $scope.grid = false;
                $scope.display = true;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
            }

            $scope.CheckAllChecked = function () {
                modulecode = [];
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].module_code;
                    var v = document.getElementById(t);
                    if (main.checked == true) {
                        $scope.row1 = 'row_selected';
                        v.checked = true;
                        modulecode = modulecode + $scope.filteredTodos[i].module_code + ','
                        $scope.color = '#edefef';
                    }
                    else {
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        modulecode = [];
                        $scope.color = '#edefef';
                    }
                }
            }

            $scope.setClickedRow = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }
            }

            var dom;
            var count = 0;
            $scope.flag = true;

            $scope.expand = function (info, $event) {

                debugger;

                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0' width='100%' style='border-color:red;border:solid;border-width:02px'>" +
                        "<tbody>" +
                         "<tr><td class='semi-bold'>" + "Arabic" + "</td> <td class='semi-bold'>" + "Vresion" + " </td><td class='semi-bold' colspan='2'>" + "Description" + "</td><td class='semi-bold'>" + "French" + "</td><td class='semi-bold' colspan='2'>" + "location" + " </td>" +
                        "<td class='semi-bold'>" + "Regional" + "</td><td class='semi-bold'>" + "Keywords" + "</td></tr>" +
                          "<tr><td>" + (info.module_name_ar) + "</td> <td>" + (info.module_version) + " </td><td colspan='2'>" + (info.module_desc) + "</td><td>" + (info.module_name_fr) + "</td><td colspan='2'>" + (info.module_location) + "</td>" +
                        "<td>" + (info.module_name_ot) + "</td><td>" + (info.module_keywords) + "</td></tr>" +
                        " </table></td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                } else {

                    debugger;

                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        info.icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }
            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                //$scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.module_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.module_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  || item.module_short_desc == toSearch) ? true : false;
            }

        }]
        )
})();