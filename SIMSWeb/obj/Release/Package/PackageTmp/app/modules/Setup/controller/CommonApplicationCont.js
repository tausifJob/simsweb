﻿(function () {
    'use strict';
    var appcode = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CommonApplicationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
         
            $scope.display = false;
            $scope.grid = true;
            $scope.obj4 = [];
            $http.get(ENV.apiUrl + "api/common/ApplicationMaster/getApplicationsbyIndex").then(function (res) {
               
                $scope.obj4 = res.data;
                $scope.totalItems = $scope.obj4.length;
                $scope.todos = $scope.obj4;
                $scope.makeTodos();

            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }

            };

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                var data = angular.copy(str);
                $scope.edt = data;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }
                $scope.CheckAllChecked();
            }

            $http.get(ENV.apiUrl + "api/common/ApplicationMaster/getModules").then(function (res) {
                
                $scope.obj = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/ApplicationMaster/getAppModes").then(function (res) {
               
                $scope.obj1 = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/ApplicationMaster/getAppTypes").then(function (res) {
               
                $scope.obj2 = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/ApplicationMaster/getAppTags").then(function (res) {
               
                $scope.obj3 = res.data;
            });

            $scope.size = function (str) {
              
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; 
                $scope.makeTodos();
            }

            $scope.Delete = function () {

                $scope.flag = false;

                appcode = '';

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].comn_appl_code+ $scope.filteredTodos[i].comn_app_mode);
                    if (v.checked == true)
                        $scope.flag = true;
                    appcode = appcode + $scope.filteredTodos[i].comn_appl_code + ',';
                }

                var deleteappcode = ({
                    'comn_appl_code': appcode,
                    'opr': 'D'
                });

                if ($scope.flag == true) {
                    swal({
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {


                          

                            $http.post(ENV.apiUrl + "api/common/ApplicationMaster/CUDupdateInsertApplications", deleteappcode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                swal({ text: $scope.msg1.strMessage, width: 380, showCloseButton: true });

                                $http.get(ENV.apiUrl + "api/common/ApplicationMaster/getApplicationsbyIndex").then(function (res) {
                                    $scope.display = false;
                                    $scope.grid = true;
                                    $scope.obj4 = res.data;
                                    $scope.totalItems = $scope.obj4.length;
                                    $scope.todos = $scope.obj4;
                                    console.log($scope.totalItems);
                                    $scope.makeTodos();

                                    main = document.getElementById('mainchk');
                                    if (main.checked == true) {
                                        main.checked = false;
                                    }

                                    $scope.CheckAllChecked();
                                });

                            });

                        } else {

                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            $scope.CheckAllChecked();
                        }
                    })
                }
                else {

                    swal({ text: 'Select At Least One Record To Delete', width: 380, showCloseButton: true });
                }


            }

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }

                $scope.CheckAllChecked();
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;

            }

            $scope.SaveData = function (isvalid) {

                if (isvalid) {

                    var data = $scope.edt;
                    data.opr = 'I';
                    $http.post(ENV.apiUrl + "api/common/ApplicationMaster/CUDupdateInsertApplications", data).then(function (msg) {

                        $scope.msg1 = msg.data;
                        swal({ text: $scope.msg1.strMessage, width: 350, showCloseButton: true });

                        $http.get(ENV.apiUrl + "api/common/ApplicationMaster/getApplicationsbyIndex").then(function (res) {
                            $scope.display = false;
                            $scope.grid = true;
                            $scope.obj4 = res.data;
                            $scope.totalItems = $scope.obj4.length;
                            $scope.todos = $scope.obj4;
                            console.log($scope.totalItems);
                            $scope.makeTodos();

                        });

                    })
                }
            }

            $scope.Update = function () {

                var data = $scope.edt;
                data.opr = 'U';
                $http.post(ENV.apiUrl + "api/common/ApplicationMaster/CUDupdateInsertApplications", data).then(function (msg) {
                    $scope.msg1 = msg.data;
                    swal({ text: $scope.msg1.strMessage, width: 350, showCloseButton: true });

                    $http.get(ENV.apiUrl + "api/common/ApplicationMaster/getApplicationsbyIndex").then(function (res) {
                        $scope.display = false;
                        $scope.grid = true;
                        $scope.obj4 = res.data;
                        $scope.totalItems = $scope.obj4.length;
                        $scope.todos = $scope.obj4;
                        console.log($scope.totalItems);
                        $scope.makeTodos();

                    });

                })
            }

            $scope.CheckAllChecked = function () {

               
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].comn_appl_code + $scope.filteredTodos[i].comn_app_mode);
                if (main.checked == true) {
                        v.checked = true;
                     
                        $('tr').addClass("row_selected");
                }
                else {
                        v.checked = false;
                        main.checked = false;
                        $('tr').removeClass("row_selected");
                      
                    }
                }
                
            }

            $scope.checkonebyonedelete = function () {


                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }
            }



            var dom;
            $scope.flag = true;

            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                    "<table class='inner-table'  width='100%' cellpadding='5' cellspacing='0' style='border-color:red;border:solid;border-width:02px'>" +
                    "<tbody>" +

                    "<tr><td class='semi-bold'>" + "NAME(Arabic)" + "</td><td class='semi-bold'>" + "NAME(French)" + "</td><td class='semi-bold'>" + "NAME(Other)" + "</td><td class='semi-bold'>" + "KEYWORD" + "</td><td class='semi-bold'>" + "VERSION" + "</td> </tr>" +
                        "<tr><td>" + (info.comn_appl_name_ar) + "</td><td>" + (info.comn_appl_name_fr) + "</td><td>" + (info.comn_appl_name_ot) + "</td><td>" + (info.comn_appl_keywords) + "</td><td>" + (info.comn_appl_version) + "</td></tr>" +

                      "<tr><td class='semi-bold'>" + "LOCATION" + "</td><td class='semi-bold'>" + "DISPALY ORDER" + "</td><td class='semi-bold'>" + "APPLICATOIN TAG" + "</td> <td class='semi-bold'colsapn='2'>" + "LOGIC" + "</td><td></td></tr>" +

                       "<tr><td>" + (info.comn_appl_location) + "</td><td>" + (info.comn_appl_display_order) + "</td><td>" + (info.comn_appl_tag) + "</td><td>" + (info.comn_appl_logic) + "</td><td></td></tr>" +
                    "</tbody>" +
                    " </table></td></tr>")

                $($event.currentTarget).parents("tr").after(dom);
                $scope.flag = false;
            } else {
                $('#innerRow').css({ 'display': 'none' });
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
                $scope.flag = true;
            }
            };


            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj4, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj4;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.comn_appl_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.comn_appl_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }


           

        }]);
})();



