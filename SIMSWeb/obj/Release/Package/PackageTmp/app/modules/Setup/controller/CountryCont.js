﻿(function () {
    'use strict';
    var countrycode = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    var data1 = [];
    simsController.controller('CountryCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            //$scope.operation = true;
            $scope.table1 = true;
            $scope.editmode = false;

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                $scope.nullcheck();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                $scope.nullcheck();
            }
            $scope.edt = "";

            $http.get(ENV.apiUrl + "api/Countrymaster/getCountrydata").then(function (get_Country) {
                $scope.Country_Data = get_Country.data;
                $scope.totalItems = $scope.Country_Data.length;
                $scope.todos = $scope.Country_Data;
                $scope.makeTodos();
                console.log($scope.Country_Data);
            })

            $http.get(ENV.apiUrl + "api/common/getAllRegionName").then(function (res) {
                $scope.RegionName = res.data;
            })
            $scope.getdesc = function (str1) {
                debugger
                for (var i = 0 ; i < $scope.CurrencyCode.length; i++) {
                    if ($scope.CurrencyCode[i].sims_currency_code == str1)
                        $scope.edt['sims_currency_dec'] = $scope.CurrencyCode[i].sims_currency_dec;

                }
                //$scope.sims_currency_dec= str1.sims_currency_dec;
            }

            $http.get(ENV.apiUrl + "api/common/getAllCurrencyCode").then(function (res) {
                $scope.CurrencyCode = res.data;
            })


            $scope.New = function () {
                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.editmode = false;
                $scope.opr = 'S';
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = {};
                $scope.edt['sims_status'] = true;
                $scope.temp = '';
                $scope.nullcheck();
            }

            $scope.up = function (str) {
                $scope.opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.operation = true;
                //    $scope.edt = str;
                $scope.edt = {
                    sims_country_code: str.sims_country_code,
                    sims_country_name_en: str.sims_country_name_en,
                    sims_country_name_ar: str.sims_country_name_ar,
                    sims_country_name_fr: str.sims_country_name_fr,
                    sims_country_name_ot: str.sims_country_name_ot,
                    sims_continent: str.sims_continent,

                    sims_currency_code: str.sims_currency_code,
                    sims_region_code: str.sims_region_code,
                    sims_status: str.sims_status
                };
                //  $scope.edt['sims_currency_code'] = str.sims_currency_dec;
                $scope.getdesc(str.sims_currency_code);
                $scope.nullcheck();
            }

            $scope.cancel = function () {
                $scope.edt.sims_country_code = "";
                $scope.table1 = true;
                $scope.operation = false;

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Save = function (myForm) {
                if (myForm) {

                    var data = $scope.edt;
                    data.opr = 'I';
                    $scope.edt = "";
                    $scope.exist = false;
                    for (var i = 0; i < $scope.Country_Data.length; i++) {
                        if ($scope.Country_Data[i].sims_country_code == data.sims_country_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ title: "Alert", text: "Record Already Exists", width: 300, height: 200 });
                    }
                    else {
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/Countrymaster/CUDCountrydata", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            $scope.operation = false;
                            $http.get(ENV.apiUrl + "api/Countrymaster/getCountrydata").then(function (get_Country) {
                                $scope.Country_Data = get_Country.data;
                                $scope.totalItems = $scope.Country_Data.length;
                                $scope.todos = $scope.Country_Data;
                                $scope.makeTodos();
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                                }
                            });
                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                    data1 = [];
                }
            }

            $scope.Update = function (myForm) {
                if (myForm) {
                    debugger
                    var data = $scope.edt;
                    data.opr = 'U';
                    $scope.edt = "";
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/Countrymaster/CUDCountrydata", data1).then(function (msg) {
                        $scope.msg1 = msg.data;

                        $http.get(ENV.apiUrl + "api/Countrymaster/getCountrydata").then(function (get_Country) {
                            $scope.Country_Data = get_Country.data;
                            $scope.totalItems = $scope.Country_Data.length;
                            $scope.todos = $scope.Country_Data;
                            $scope.makeTodos();

                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                            }

                        });

                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                    data1 = [];
                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_country_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_country_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.deleterecord = function () {

                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_country_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletelocacode = ({
                            'sims_country_code': $scope.filteredTodos[i].sims_country_code,
                            opr: 'D'
                        });
                        countrycode.push(deletelocacode);
                    }
                }
                debugger
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/Countrymaster/CUDCountrydata", countrycode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Countrymaster/getCountrydata").then(function (get_Country) {
                                                $scope.Country_Data = get_Country.data;
                                                $scope.totalItems = $scope.Country_Data.length;
                                                $scope.todos = $scope.Country_Data;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Countrymaster/getCountrydata").then(function (get_Country) {
                                                $scope.Country_Data = get_Country.data;
                                                $scope.totalItems = $scope.Country_Data.length;
                                                $scope.todos = $scope.Country_Data;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_country_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                data1 = [];
                $scope.currentPage = true;
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Country_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Country_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_country_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_continent.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_region_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_currency_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                       // item.sims_transport_amount ||
                        item.sims_country_code == toSearch) ? true : false;

            }

            $scope.nullcheck = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].sims_country_code;
                        var v = document.getElementById(t + i);
                        v.checked = false;
                        $scope.row1 = '';
                    }
                }
                main.checked = false;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

        }]
        )
})();