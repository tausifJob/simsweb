﻿(function () {
    'use strict';
    var obj1, obj2, temp, main;
    var opr = '';
    var portalcode = [];

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TimeLineCont',
         ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
             $scope.pagesize = '5';
             $scope.color_code = '';
             $scope.table1 = true;
             $scope.editmode = false;
             var formdata = new FormData();
             var data1 = [];
             $scope.url = ENV.siteUrl;

             //$scope.UserAuditfun = function (modcode, appcode, sdt, edt) {
             //    debugger;
             //    sdt = $scope.temp.comn_audit_start_time;
             //    edt = $scope.temp.comn_audit_end_time;
                    
             //}

             $scope.appl_load=function(str)
             {
                
                 $state.go("main." + str);
             }
             $http.get(ENV.apiUrl + "api/TimeLine/getUserAudit?uname=" + $rootScope.globals.currentUser.username).then(function (auditDetails) {
                 $scope.audit_Details = auditDetails.data;
                
             });


             
           
    }])
})();
