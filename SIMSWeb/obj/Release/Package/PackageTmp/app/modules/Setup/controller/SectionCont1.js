﻿(function () {
    'use strict';

    var main,temp, del = [];

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SectionCont1',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            //$scope.display = false;
            //$scope.eDetail = false;
           var sendata = []
            $scope.sms = true;
            $scope.loading = false;
            $scope.pagesize = "5";
                           
            $scope.dis1 = true;
            $scope.dis2 = false;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);


            $http.get(ENV.apiUrl + "api/Section/Master/getGender").then(function (getGender) {
                $scope.getGender = getGender.data;

            });

                           
            $scope.showSections = function (crcode, acyr, grcode) {

                $scope.loading = true;
                $http.get(ENV.apiUrl + "api/Section/Master/getSections?curCode=" + crcode + "&academicYear=" + acyr + "&grade_code=" + grcode).then(function (res) {
                    $scope.sectionsobj = res.data;
                    $scope.table = true;
                    $scope.loading = false;
                    $scope.totalItems = $scope.sectionsobj.length;
                    $scope.todos = $scope.sectionsobj;
                    $scope.makeTodos();
                }, function () {
                    $scope.loading = true;
                    alert("Failed to get Data");
                });
            }

            $scope.check = function () {
                main = document.getElementById('mainchk');

                debugger;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_section_code + $scope.filteredTodos[i]._sims_section_name);
                    if (main.checked == true) {
                        v.checked = true;
                              
                        $('tr').addClass("row_selected");
                        }
                    else {
                        v.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }
                              
            }

            $scope.CheckAny = function (sims_section_code) {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }
                               

            }

            $scope.Update = function (str) {
                $scope.temp =angular.copy(str);
                $scope.update_btn = true; $scope.new_funs = false;
                $scope.dis1 = false; $scope.dis2 = true;
                $scope.select_list = false;
                $scope.update_flag = true;
             
                $scope.grd_name = $('#Select5 option:selected').text();
                $http.get(ENV.apiUrl + "api/Section/Master/getPromoteCode").then(function (res) {
                    $scope.PromoteCode = res.data;
                   
                });


                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }
                $scope.check();
            }

            $scope.getPromoteCode = function (grd_Code) {
                $scope.grd_name = $('#Select5 option:selected').text();
                $http.get(ENV.apiUrl + "api/Section/Master/getPromoteCode?grd_code=" + grd_Code + "&grd_name=" + $scope.grd_name).then(function (res) {
                    $scope.PromoteCode = res.data;
                    
                });
            }

            $scope.save = function (str) {

                if (str) {
                    var data = $scope.temp;
                    data.opr = "I";
                    //  debugger;
                    sendata.push(data);
                    $http.post(ENV.apiUrl + "api/Section/Master/CUDSection", sendata).then(function (res) {
                        $scope.sectionsobj = res.data;
                        sendata = [];
                        if ($scope.sectionsobj == true) {
                            sendata = [];
                            $scope.dis1 = true;
                            $scope.dis2 = false;
                            swal({text:'section Inserted Successfully', width: 380 });
                            $scope.showSections($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.edt.sims_grade_code);

                        }
                        else {
                            $scope.showSections($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.edt.sims_grade_code);
                            swal({text:'Section Not Inserted', width: 380 });
                        }

                    });

                }
            }

            $scope.Update_data = function () {

                var Update_data = $scope.temp;
                Update_data.opr = 'U';
                Update_data.sims_cur_code = $scope.edt.sims_cur_code;
                Update_data.sims_academic_year = $scope.edt.sims_academic_year;
                Update_data.grade_code = $scope.edt.sims_grade_code;
                              
                sendata.push(Update_data);

                $http.post(ENV.apiUrl + "api/Section/Master/CUDSection", sendata).then(function (res) {
                    $scope.sectionsobj = res.data;
                    sendata = [];

                    if ($scope.sectionsobj==true)
                    {
                        $scope.dis1 = true;
                        $scope.dis2 = false;
                        swal({ text: 'Section Updated Successfully', width: 380 });
                        $scope.showSections($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.edt.sims_grade_code);

                    }
                    else
                    {
                        swal({ text: 'Section Not Updated', width: 380 });
                        $scope.showSections($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.edt.sims_grade_code);
                      
                    }
                                       
                                   
                })
            }

            $scope.Delete_Records = function () {
                var data = {};
               sendata = [];
                var section_name = '';
                $scope.nodelete = false;
                $scope.sims_statusfordelete = false;
                var flag = false;

                var deletecount = 0;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_section_code + $scope.filteredTodos[i]._sims_section_name);
                    if (v.checked == true) {
                        if ($scope.filteredTodos[i].count == '0') {
                            flag = true;
                             data= {
                                sims_cur_code: $scope.edt.sims_cur_code,
                                sims_academic_year: $scope.edt.sims_academic_year,
                                grade_code: $scope.edt.sims_grade_code,
                                sims_section_code: $scope.filteredTodos[i].sims_section_code,
                                opr: "D"
                            };
                            sendata.push(data);
                        }
                        else {
                            debugger
                            flag = false;
                            deletecount = parseInt(deletecount) + 1;
                            section_name = section_name + $scope.filteredTodos[i]._sims_section_name + ' ,';
                            $scope.nodelete = true;
                        }
                    }
                }
                    if ($scope.nodelete==true)
                    {
                   flag = false;
                   $scope.sims_statusfordelete = true;
                   if (deletecount == 1) {
                       swal({ text: section_name + ' ' + 'This Section Is Mapped In Student Section Or Section Subject', width: 380, showCloseButton: true });
                   }
                   else {
                       swal({ text: section_name + ' ' + 'This Sections Are Mapped In Student Section Or Section Subject', width: 380, showCloseButton: true });
                   }

                 
                    main = document.getElementById('mainchk');
                    main.checked = false;
                    $scope.check();
                    }

                
                if (flag == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/Section/Master/CUDSection", sendata).then(function (res) {
                                $scope.sectionsobj = res.data;
                                sendata = [];
                                if ($scope.sectionsobj == true) {
                                    $scope.dis1 = true;
                                    $scope.dis2 = false;
                                    swal({ text: 'Section Deleted Successfully', width: 350, showCancelButton: true, });
                                    $scope.showSections($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.edt.sims_grade_code);
                                }
                                else {
                                    $scope.showSections($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.edt.sims_grade_code);
                                    swal({ text: 'Section Not Deleted', width: 350, showCancelButton: true, });
                                }

                            })
                        }
                        else {

                            main = document.getElementById('mainchk');
                            if (main.checked == true)
                            {
                                main.checked = false;
                            }
                            $scope.check();

                        }
                    });
                }
                else {
                    if ($scope.sims_statusfordelete != true) {
                        swal({ text: 'Select At Least One Record To Delete', width: 320, showCancelButton: true, });
                    }
                }
            }

            $scope.can = function () {
                $scope.dis1 = true;
                $scope.dis2 = false;
                $scope.temp = "";
                del = [];
            }

            $scope.New = function () {
                $scope.temp = "";
                $scope.update_flag = false; $scope.new_funs = true;
                $scope.dis1 = false; $scope.dis2 = true;
                $scope.select_list = true; $scope.update_btn = false;

                $scope.temp = {
                     sims_cur_code: $scope.edt.sims_cur_code,
                     sims_academic_year: $scope.edt.sims_academic_year,
                     grade_code: $scope.edt.sims_grade_code,
                     sims_status:true
                };

                $http.get(ENV.apiUrl + "api/Section/Master/getPromoteCode").then(function (res) {
                    $scope.PromoteCode = res.data;
                                   
                });
            }

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
               
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();

               
            }

            var dom;
            $scope.flag = true;
            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='9'>" +
                    "<table class='inner-table' cellpadding='5' cellspacing='0' style='width:100%;border:solid;border-width:02px'>" +
                    "<tbody>" +
                    "<tr><td class='semi-bold' colsapn='3'>" + "Name (Arabic)" + "</td> " + "<td class='semi-bold'colsapn='3'>" + "Name (French)" +
                    "</td>" + "<td class='semi-bold'colsapn='3'>" + "Name (Regional)" + "</tr>" +
                    "<tr> <td colsapn='3'>" + (info.sims_section_name_ar) + " </td>" + "<td colsapn='3'>" + (info.sims_section_name_fr) + " </td>" + "<td colsapn='3'>" + (info.sims_section_name_ot) + " </td> </tr>" +
                    //"<tr><td class='semi-bold'>" + "Regional:" + "</td> <td colsapn='2'>" + (info.module_name_ot) + " </td>" + "<td class='semi-bold' colsapn='2'>" + "Keywords:" + "<td colsapn='1'>" + (info.module_keywords) + " </td></tr>" +
                    "</tbody>" +
                    " </table></td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }

            };

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.Homeroom_MultipleDelete();
                }
            };

            $scope.searched = function (valLists, toSearch) {

             

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.sectionsobj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.sectionsobj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item._sims_section_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_section_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }
          

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;

                $scope.edt = {
                    sims_cur_code: $scope.curriculum[0].sims_cur_code
                };
                $scope.temp = {
                    sims_cur_code:$scope.curriculum[0].sims_cur_code
                };

                $scope.getacyr($scope.temp.sims_cur_code);
            });

            $scope.getacyr = function (str) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    $scope.edt = {
                        sims_academic_year:$scope.Academic_year[0].sims_academic_year, 
                            sims_cur_code :str
                    };
                    $scope.temp = {
                        sims_cur_code:str,
                        sims_academic_year:$scope.Academic_year[0].sims_academic_year 
                    };
                    $scope.getsections($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);

                });
            }

            $scope.getsections = function (str,str1) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str + "&academic_year=" + str1).then(function (res) {
                    $scope.grade = res.data;

                    console.log($scope.grade);
                   
                })
               
            };
           
        }]);
})();

