﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ModeratorUserCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.grid = true;
            $scope.moderatorUser_data = [];
            $scope.user_result = [];
            var data1 = [];
            var deletecode = [];
            $scope.edit_data = false;

            $scope.itemsPerPage1 = '5';
            $scope.currentPage1 = 0;

            $(document).keydown(function (e) {
                if (e.keyCode == 27) {
                    // window.close();
                    $('.nav-tabs a[href="#User_information"]').tab('hide')
                    $('#MyModal').modal({ backdrop: 'static', keyboard: false });
                }
            });

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.size1 = function (pagesize) {
                $scope.itemsPerPage1 = pagesize;
            }

            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage1;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;


            };

            $scope.prevPage = function () {
                if ($scope.currentPage1 > 0) {
                    $scope.currentPage1--;
                }
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage1 === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.user_result.length / $scope.itemsPerPage1) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage1 < $scope.pageCount()) {
                    $scope.currentPage1++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage1 === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage1 = n;
            };

            $http.get(ENV.apiUrl + "api/common/ModeratorUser/getModeratorUser").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.moderatorUser_data = res.data;
                $scope.totalItems = $scope.moderatorUser_data.length;
                $scope.todos = $scope.moderatorUser_data;
                $scope.makeTodos();
                $scope.grid = true;

                for (var i = 0; i < $scope.totalItems; i++) {
                    $scope.moderatorUser_data[i].icon = "fa fa-plus-circle";
                }
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };



            $http.get(ENV.apiUrl + "api/common/ModeratorUser/getAllModeratorNames").then(function (res) {
                $scope.ModeratorNames = res.data;
                console.log($scope.ModeratorNames);
            });

            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.cur_data = res.data;
                console.log($scope.cur_data);
            });

            $scope.GetAacd_yr = function (cur) {
                if (cur != undefined) {
                    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur).then(function (res) {
                        $scope.acad = res.data;
                        console.log($scope.acad);
                    });
                }
            }

            $scope.GetGrade = function (cur, acad_yr) {
                console.log(cur, acad_yr);
                $http.get(ENV.apiUrl + "api/common/ModeratorUser/getAllGrades?cur_code=" + cur + "&ac_year=" + acad_yr).then(function (res) {
                    $scope.grade = res.data;
                    console.log($scope.grade);
                });
            }

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                console.log(str);
                $scope.edt = str;
                $scope.edit_data = true;
                $scope.GetAacd_yr($scope.edt.sims_cur_code);
                $scope.GetGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
            }

            $scope.Save = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = ({
                            sims_cur_code: $scope.edt.sims_cur_code,
                            sims_academic_year: $scope.edt.sims_academic_year,
                            sims_grade_code: $scope.edt.sims_grade_code,
                            sims_comm_code: $scope.edt.sims_comm_code,
                            sims_comm_user_name: $scope.edt.sims_comm_user_name,
                            sims_comm_user_code_status: $scope.edt.sims_comm_user_code_status,
                            sims_comm_email_user_code: $scope.edt.sims_comm_email_user_code,
                            sims_comm_email_user_code_status: $scope.edt.sims_comm_email_user_code_status,
                            sims_comm_sms_user_code: $scope.edt.sims_comm_sms_user_code,
                            sims_comm_sms_user_code_status: $scope.edt.sims_comm_sms_user_code_status,
                            sims_comm_bcc_user_code: $scope.edt.sims_comm_bcc_user_code,
                            sims_comm_bcc_user_code_status: $scope.edt.sims_comm_bcc_user_code_status,
                            sims_comm_bcc_email_user_code: $scope.edt.sims_comm_bcc_email_user_code,
                            sims_comm_bcc_email_user_code_status: $scope.edt.sims_comm_bcc_email_user_code_status,
                            sims_comm_bcc_sms_user_code: $scope.edt.sims_comm_bcc_sms_user_code,
                            sims_comm_bcc_sms_user_code_status: $scope.edt.sims_comm_bcc_sms_user_code_status,
                            opr: 'I'
                        });

                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/common/ModeratorUser/CUDModeratorUser", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Modeartor User Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });

                            }
                            else {
                                swal({ title: "Alert", text: "Modeartor User Not Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $scope.currentPage1 = 0;
                $http.get(ENV.apiUrl + "api/common/ModeratorUser/getModeratorUser").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.moderatorUser_data = res.data;
                    $scope.totalItems = $scope.moderatorUser_data.length;
                    $scope.todos = $scope.moderatorUser_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                    for (var i = 0; i < $scope.totalItems; i++) {
                        $scope.moderatorUser_data[i].icon = "fa fa-plus-circle";
                    }
                });

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

            }

            $scope.Update = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    var data = ({
                        sims_cur_code: $scope.edt.sims_cur_code,
                        sims_academic_year: $scope.edt.sims_academic_year,
                        sims_grade_code: $scope.edt.sims_grade_code,
                        sims_comm_code: $scope.edt.sims_comm_code,
                        sims_comm_user_name: $scope.edt.sims_comm_user_name,
                        sims_comm_user_code_status: $scope.edt.sims_comm_user_code_status,
                        sims_comm_email_user_code: $scope.edt.sims_comm_email_user_code,
                        sims_comm_email_user_code_status: $scope.edt.sims_comm_email_user_code_status,
                        sims_comm_sms_user_code: $scope.edt.sims_comm_sms_user_code,
                        sims_comm_sms_user_code_status: $scope.edt.sims_comm_sms_user_code_status,
                        sims_comm_bcc_user_code: $scope.edt.sims_comm_bcc_user_code,
                        sims_comm_bcc_user_code_status: $scope.edt.sims_comm_bcc_user_code_status,
                        sims_comm_bcc_email_user_code: $scope.edt.sims_comm_bcc_email_user_code,
                        sims_comm_bcc_email_user_code_status: $scope.edt.sims_comm_bcc_email_user_code_status,
                        sims_comm_bcc_sms_user_code: $scope.edt.sims_comm_bcc_sms_user_code,
                        sims_comm_bcc_sms_user_code_status: $scope.edt.sims_comm_bcc_sms_user_code_status,
                        opr: 'U'
                    });
                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/ModeratorUser/CUDModeratorUser", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Modeartor User Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });

                        }
                        else {
                            swal({ title: "Alert", text: "Modeartor User Not Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                    });
                }
            }

            $scope.Delete = function () {
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    var v = document.getElementById(i);

                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sims_cur_code': $scope.filteredTodos[i].sims_cur_code,
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            'sims_comm_code': $scope.filteredTodos[i].sims_comm_code,
                            'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                            'sims_comm_user_name': $scope.filteredTodos[i].sims_comm_user_name,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/common/ModeratorUser/CUDModeratorUser", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Modeartor User Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });

                                }
                                else {
                                    swal({ title: "Alert", text: "Modeartor User Not Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);

                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
            }

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = [];
                $scope.edit_data = false;
            }


            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }


            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }


            $scope.SearchUserModal = function () {
                $scope.Usertable = false;
                $scope.buttons = false;
                $scope.busy = false;

                $scope.pp = '';
                $scope.user_result = '';
                $('.nav-tabs a[href="#User_information"]').tab('show')
                $('#MyModal').modal({ backdrop: 'static', keyboard: false });
                // $('#MyModal').modal('show');
            }

            $scope.SearchUser = function () {
                $scope.Usertable = false;
                $scope.busy = true;
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchUser?data=" + JSON.stringify($scope.pp)).then(function (AllUser) {
                    $scope.user_result = AllUser.data;
                    $scope.Usertable = true;
                    $scope.busy = false;
                });
                $scope.currentPage1 = 0;

            }

            $scope.getUserdata = function () {
                for (var i = 0; i < $scope.user_result.length; i++) {
                    if ($scope.user_result[i].user_code1 == true) {
                        $scope.edt.sims_comm_user_name = $scope.user_result[i].user_name;

                    }
                }
            }


            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.moderatorUser_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.moderatorUser_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_comm_code_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_comm_user_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            var dom;
            var count = 0;
            $scope.flag = true;
            $scope.icon = "fa fa-plus-circle";

            $scope.expand = function (info, $event) {
                console.log(info);
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";

                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                         "<tr> <td class='semi-bold'>" + "User Id(Email)" + "</td> <td class='semi-bold'>" + "Status" + " </td><td class='semi-bold'>" + "User Id(SMS)" + "</td><td class='semi-bold'>" + "Status" + "</td><td class='semi-bold'>" + "User Id(Bcc Portal)" + "</td> <td class='semi-bold'>" + "Status" + " </td><td class='semi-bold'>" + "User Id(Bcc Email)" + "</td><td class='semi-bold'>" + "Status" + "</td><td class='semi-bold'>" + "User Id(Bcc SMS)" + "</td> <td class='semi-bold'>" + "Status" + " </td>" +
                        "</tr>" +
                          "<tr><td>" + (info.sims_comm_email_user_code) + "</td><td>" + (info.sims_comm_email_user_code_status) + " </td><td>" + (info.sims_comm_sms_user_code) + "</td><td>" + (info.sims_comm_sms_user_code_status) + "</td><td>" + (info.sims_comm_bcc_user_code) + "</td><td>" + (info.sims_comm_bcc_user_code_status) + " </td><td>" + (info.sims_comm_bcc_email_user_code) + "</td><td>" + (info.sims_comm_bcc_email_user_code_status) + "</td><td>" + (info.sims_comm_bcc_sms_user_code) + "</td><td>" + (info.sims_comm_bcc_sms_user_code_status) + "</td>" +
                        "</tr>" +
                        " </table></td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    info.icon = "fa fa-plus-circle";
                    $scope.flag = true;
                }
            };

            $scope.ex = function (str) {
                console.log("hello");
            }

        }])
})();