﻿(function () {
    'use strict';
    var del = [];
    var main, todos = [], Allusers = [];
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CreateEditUserCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'filterFilter', 'ENV', '$window', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, filterFilter, ENV, $window) {
            $scope.display = false;
            $scope.grid = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            var data = "";
            var totalItems;
            var Allusers;
            $scope.busy = false;
            $scope.table = false;
            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2015&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;


            });

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.UserSearch = function () {
                $scope.busy = true;
                $scope.table = false;
                $scope.filteredTodos = "";

                $http.get(ENV.apiUrl + "api/common/User/getUsers?data=" + JSON.stringify($scope.temp1)).then(function (users) {
                    $scope.Allusers = users.data;
                    $scope.table = true;
                    if (users.data.length > 0) {
                        $scope.totalItems = $scope.Allusers.length;
                        $scope.todos = $scope.Allusers;
                        $scope.makeTodos();

                    }
                    else {

                        swal({ text: "Sorry Search Failed Enter Proper Filed(S)", width: 300, showCloseButton: true });
                    }
                    $scope.busy = false;
                })
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseFloat($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseFloat(begin) + parseFloat($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

            };

            $scope.edit = function (str) {
                $scope.editmode = true;
                $scope.display = true;
                $scope.grid = false;
                $scope.btn_save = false;
                $scope.btn_update = true;
                $scope.delete1 = false;
                $scope.readonly = true;
                $scope.edt = {
                    comn_user_group_name: str.comn_user_group_name,
                    comn_user_email: str.comn_user_email,
                    comn_user_status_name: str.comn_user_status_name,
                    comn_user_date_created: str.comn_user_date_created,
                    comn_user_expiry_date: str.comn_user_expiry_date,
                    comn_user_alias: str.comn_user_alias,
                    comn_user_code: str.comn_user_code,
                    comn_user_name: str.comn_user_name,
                    comn_user_captcha_status: str.comn_user_captcha_status,
                    comn_user_phone_number: str.comn_user_phone_number,
                    comn_user_secret_question: str.comn_user_secret_question,
                    comn_user_secret_answer: str.comn_user_secret_answer,
                    comn_user_remark: str.comn_user_remark
                };

            }

            $scope.New = function () {
                $scope.user_status = '';
                $scope.display = true;
                $scope.grid = false;
                $scope.btn_save = true;
                $scope.btn_update = false;

                $scope.edt = "";
                $scope.editmode = true;

                $scope.newmode = true;
                $scope.editonly = false;
                $scope.readonly = false;


                var now = new Date();
                var month = (now.getMonth() + 1);
                var day = now.getDate();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                $scope.edt = { comn_user_date_created: now.getFullYear() + '-' + month + '-' + day }

                $scope.edt['comn_user_captcha_status'] = true;
                $scope.edt['comn_user_status_name'] = 'Active';
            }

            $scope.cancel = function () {

                $scope.grid = true;
                $scope.display = false;


            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.SaveData = function (isvalid) {
                $scope.display = false;
                $scope.grid = true;
                var username1 = $rootScope.globals.currentUser;
                var data = $scope.edt;
                data.comn_user_name = username1.username;
                if (isvalid) {
                    $http.post(ENV.apiUrl + "api/common/User/CUDUser", data).then(function (MSG) {
                        $scope.msg1 = MSG.data;
                        if ($scope.msg1 == true) {

                            swal({ text: 'User Created Successfully', width: 300, showCloseButton: true });

                            $http.get(ENV.apiUrl + "api/common/User/getUsers?data=" + JSON.stringify($scope.temp1)).then(function (users) {
                                $scope.Allusers = users.data;
                                $scope.table = true;
                                $scope.totalItems = $scope.Allusers.length;
                                $scope.todos = $scope.Allusers;
                                $scope.makeTodos();
                               
                                $scope.newmode = false;
                                $scope.editonly = false;
                            })
                        }
                        else {
                            swal({ text: 'User Not Created', width: 300, showCloseButton: true });

                        }


                    });
                }

                else {
                    var Text = 'Plesase Fill Fields';
                    swal({ text: Text, width: 300, height: 300 });
                }



            }

            $scope.update = function () {
                $scope.grid = true;
                $scope.display = false;

                var data1 = $scope.edt;
                $http.post(ENV.apiUrl + "api/common/User/UpdateUser", data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {

                        swal({ text: 'User Updated Successfully', width: 300, showCloseButton: true });

                        $http.get(ENV.apiUrl + "api/common/User/getUsers?data=" + JSON.stringify($scope.temp1)).then(function (users) {
                            $scope.Allusers = users.data;
                            $scope.table = true;
                          
                            $scope.newmode = false;
                            $scope.editonly = false;
                            $scope.totalItems = $scope.Allusers.length;
                            $scope.todos = $scope.Allusers;
                            $scope.makeTodos();

                        });
                    }
                    else {
                        swal({ text: 'User Not Updated', width: 300, showCloseButton: true });

                    }
                });
            }

            $scope.reset = function () {
                $scope.edt = "";

                $scope.temp1 = '';

            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Allusers, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Allusers;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.comn_user_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.comn_user_email.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  || item.comn_user_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.comn_user_alias.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.CheckDate = function (exp_date, create_date1, name) {
                if (exp_date < create_date1) {

                    swal('', 'Please Select Future Date')

                    $scope.edt[name] = '';
                }
                else {

                    $scope.edt[name] = exp_date;
                }
            }

        }])
})();

