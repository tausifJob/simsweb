﻿
(function () {
    'use strict';
    var Homeroom_code = [], HomeroomBatch_code = [], HomeroomBatch_code1=[];
    var main, main2;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('HomeRoomCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.homeroom_display = false;
            $scope.pagesize = '5';
            $scope.pagesize1 = '5';
            $scope.pageindex = "1";
            $scope.homeroom_grid = true;
            $scope.homeroom = true;
           
            $http.get(ENV.apiUrl + "api/homeroom/getAllHomeroom").then(function (getAllHomeroom) {
                $scope.AllHomeroom = getAllHomeroom.data;
                $scope.totalItems = $scope.AllHomeroom.length;
                $scope.todos = $scope.AllHomeroom;
                $scope.makeTodos();

            })

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
               
            });

            $scope.getacyr = function (str) {
               
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                   
                })
            }

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }
                $scope.Homeroom_MultipleDelete();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }
                $scope.Homeroom_MultipleDelete();
            }

            $scope.Homeroom_edit = function (str) {
                $scope.homeroom_display = true;
                $scope.homeroom_grid = false;
                $scope.btn_save = false;
                $scope.btn_update = true;
                $scope.btn_delete = false; 
                var data = angular.copy(str);
                $scope.edt = data;
             
                $scope.getacyr(str.sims_cur_code);
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }
                $scope.Homeroom_MultipleDelete();

               
            }

            $scope.Homeroom_Update = function () {

                var data = $scope.edt;
                data.opr = 'U';

                $http.post(ENV.apiUrl + "api/homeroom/CUDHomeroom", data).then(function (msg) {
                    $scope.msg1 = msg.data;
                    $scope.homeroom_display = false;
                    $scope.homeroom_grid = true;
                    if ($scope.msg1 == true) {

                        swal({
                            text: 'Information Updated Successfully',
                            width: 380
                            , showCloseButton: true });
                        $http.get(ENV.apiUrl + "api/homeroom/getAllHomeroom").then(function (getAllHomeroom) {
                            $scope.AllHomeroom = getAllHomeroom.data;
                            $scope.totalItems = $scope.AllHomeroom.length;
                            $scope.todos = $scope.AllHomeroom;
                            $scope.makeTodos();

                        })
                    }
                    else {

                        swal({
                            text: 'Information Not Updated',
                            width: 380, showCloseButton: true
                        });
                    }
                    
                })
            }

            $scope.Homeroom_New = function () {
                $scope.homeroom_display = true;
                $scope.homeroom_grid = false;
                $scope.btn_save = true;
                $scope.btn_update = false;
                $scope.btn_delete = false;
                $scope.edt = "";

                $scope.edt = { sims_homeroom_name: '',sims_homeroom_location_details:'' };
                $http.get(ENV.apiUrl + "api/homeroom/getAutoGenerate_HomeroomCode").then(function (getAutoGenerate_HomeroomCode) {
                    $scope.AutoGenerate_HomeroomCode = getAutoGenerate_HomeroomCode.data;
                    $scope.edt = { sims_homeroom_code: $scope.AutoGenerate_HomeroomCode };
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }
                $scope.Homeroom_MultipleDelete();


            }

            $scope.Homeroom_Delete = function () {
                
                Homeroom_code = [];
                var flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_homeroom_code;
                    var v = document.getElementById(t);

                    if (v.checked == true) {
                        flag = true;
                        Homeroom_code = Homeroom_code + $scope.filteredTodos[i].sims_homeroom_code + ',';
                    }
                }

                var homeroomcode = ({
                    'sims_homeroom_code': Homeroom_code,
                    'opr': 'D'
                });

                if (flag == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/homeroom/CUDHomeroom", homeroomcode).then(function (msg) {
                                $scope.msg1 = msg.data;

                                $scope.homeroom_display = false;
                                $scope.homeroom_grid = true;

                                if ($scope.msg1 == true) {

                                    swal({ text: 'Information Deleted Successfully', width: 380, showCloseButton: true });
                                    $http.get(ENV.apiUrl + "api/homeroom/getAllHomeroom").then(function (getAllHomeroom) {
                                        $scope.AllHomeroom = getAllHomeroom.data;
                                        $scope.row1 = "";
                                        $scope.currentPage = 1;
                                        $scope.totalItems = $scope.AllHomeroom.length;
                                        $scope.todos = $scope.AllHomeroom;
                                        $scope.makeTodos();
                                    });
                                }
                                else {

                                    swal({ text: 'Information Not Deleted', width: 380, showCloseButton: true });
                                    main = document.getElementById('mainchk');
                                    if (main.checked == true) {
                                        main.checked = false;
                                    }
                                    $scope.Homeroom_MultipleDelete();
                                }

                               
                            })
                        }
                        else {

                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            $scope.Homeroom_MultipleDelete();
                        }
                    })
                }
                else {
                    swal({ text: 'Select At Least One Record To Delete', width: 380 });
                }
            }

            $scope.cancel = function () {
                $scope.homeroom_grid = true;
                $scope.homeroom_display = false;
            }

            $scope.Homeroom_MultipleDelete = function () {

                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_homeroom_code);
                    if (main.checked == true) {
                        v.checked = true;
                        $scope.filteredTodos[i].addClass = '#ffffcc';

                    }
                    else {
                        for (var i = 0; i < $scope.filteredTodos.length; i++) {
                            var t = $scope.filteredTodos[i].sims_homeroom_code;
                            var v = document.getElementById(t);
                            v.checked = false;
                            main.checked = false;
                            Homeroom_code = [];
                            $scope.filteredTodos[i].addClass = '#fff';
                            $('tr').removeClass("row_selected");
                        }

                    }
                }

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    for (var j = 0; j < $scope.Sims_Homeroom_Batch1.length; j++) {

                        var v = document.getElementById($scope.filteredTodos[i].sims_homeroom_code);
                        if ($scope.filteredTodos[i].sims_homeroom_code == $scope.Sims_Homeroom_Batch1[j].sims_homeroom_code) {
                            v.checked = false;
                            $scope.filteredTodos[i].addClass = '#fff';
                            $('tr').removeClass("row_selected");
                            break;
                        }
                    }
                }
            }

            $scope.Homeroom_Delete_Onebyone = function (str) {

                for (var i = 0; i < $scope.Sims_Homeroom_Batch1.length; i++) {
                        var v = document.getElementById(str);
                        if (str == $scope.Sims_Homeroom_Batch1[i].sims_homeroom_code) {
                            v.checked = false;
                            swal({ text: 'This Homeroome Already Mapped Can Not Be Delete', width: 320, showCloseButton: true });
                            break;
                        }
                        else {
                            $("input[type='checkbox']").change(function (e) {
                                if ($(this).is(":checked")) { //If the checkbox is checked
                                    $(this).closest('tr').addClass("row_selected");
                                    //Add class on checkbox checked
                                } else {
                                    $(this).closest('tr').removeClass("row_selected");
                                    //Remove class on checkbox uncheck
                                }
                            });

                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                        }
                }

                
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(str);
                        if ($scope.filteredTodos[i].sims_homeroom_code == str) {
                            if( v.checked == false)
                            $scope.filteredTodos[i].addClass = '#fff';
                            $('tr').removeClass("row_selected");
                            break;
                        }
                }
            }

            $scope.Homeroom_SaveData = function (isvalid) {


                if (isvalid) {

                    var data = $scope.edt;
                    data.opr = 'I';

                    $http.post(ENV.apiUrl + "api/homeroom/CUDHomeroom", data).then(function (msg) {
                        $scope.msg1 = msg.data;

                        $scope.homeroom_display = false;
                        $scope.homeroom_grid = true;

                        if ($scope.msg1 == true) {

                            swal({
                                text: 'Information Inserted Successfully',
                                width: 380, showCloseButton: true
                            });
                            $http.get(ENV.apiUrl + "api/homeroom/getAllHomeroom").then(function (getAllHomeroom) {
                                $scope.AllHomeroom = getAllHomeroom.data;
                                $scope.totalItems = $scope.AllHomeroom.length;
                                $scope.todos = $scope.AllHomeroom;

                                $scope.makeTodos();

                            });
                        }
                        else {

                            swal({ text: 'Information Not Inserted', width: 380, showCloseButton: true });

                        }

                        
                    })

                }

            }

            $scope.HomeRoom = function () {
                $scope.homeroom = true;
                $scope.homeroom_display = false;
                $scope.homeroom_grid = true;

                $scope.homeroombacth = false;
                $scope.homeroombacth_display = false;
                $scope.homeroombacth_grid = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.Homeroom_MultipleDelete();
                }
            };
          
            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.AllHomeroom, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.AllHomeroom;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_homeroom_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_homeroom_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            /**************************************HomeroomBatch Form Code*************************************************/

            $http.get(ENV.apiUrl + "api/homeroom/getSims_HomeroomBatch").then(function (Sims_HomeroomBatch) {
                $scope.Sims_Homeroom_Batch1 = Sims_HomeroomBatch.data;
                $scope.totalItems1 = $scope.Sims_Homeroom_Batch1.length;
                $scope.todos1 = $scope.Sims_Homeroom_Batch1;
                $scope.makeTodos1();
                
            });

            $scope.cancel1 = function () {
              
                $scope.homeroombacth_display = false;
                $scope.homeroombacth_grid = true;

               
            }

            $scope.HomeRoomBatch = function () {
                $scope.homeroom = false;
                $scope.homeroom_display = false;
                $scope.homeroom_grid = false;

                $scope.homeroombacth = true;
                $scope.homeroombacth_display = false;
                $scope.homeroombacth_grid = true;

                $http.get(ENV.apiUrl + "api/homeroom/GetAllHomeroomName").then(function (GetAllHomeroomName) {
                    $scope.Homeroomnames = GetAllHomeroomName.data;

                });

                //$http.get(ENV.apiUrl + "api/homeroom/getSims_HomeroomBatch").then(function (Sims_HomeroomBatch) {
                //    $scope.Sims_Homeroom_Batch = Sims_HomeroomBatch.data;
                   
                //    $scope.totalItems1 = $scope.Sims_Homeroom_Batch.length;
                //    $scope.todos1 = $scope.Sims_Homeroom_Batch;
                //    $scope.makeTodos1();
                //});
                
            }

            $scope.HomeroomBatch_New = function () {
                $scope.homeroombacth_display = true;
                $scope.homeroombacth_grid = false;
                $scope.btn_save = true;
                $scope.btn_update = false;
                $scope.edt = "";

                main2 = document.getElementById('mainchk1');
                if (main2.checked == true) {
                    main2.checked = false;
                }

                $scope.edt={sims_batch_name:''}

                $http.get(ENV.apiUrl + "api/homeroom/getAutoGenerate_HomeroombatchCode").then(function (AutoGenerate_HomeroombatchCode) {
                    $scope.AutoGenerate_Homeroombatch_Code = AutoGenerate_HomeroombatchCode.data;
                    $scope.edt = { sims_batch_code: $scope.AutoGenerate_Homeroombatch_Code };
                });

                $scope.HomeroomBatch_MultipleDelete();

            }

            $scope.HomeroomBatch_edit = function (str) {

                $scope.homeroombacth_display = true;
                $scope.homeroombacth_grid = false;
                $scope.btn_save = false;
                $scope.btn_update = true;
                $scope.btn_delete = false; 
                var data = angular.copy(str);
                $scope.edt = data;

                main2 = document.getElementById('mainchk1');
                if (main2.checked == true) {
                    main2.checked = false;
                }

                $scope.getacyr(str.sims_cur_code);
                $scope.HomeroomBatch_MultipleDelete();

            }

            $scope.HomeroomBatch_SaveData = function (isvalid) {

                if (isvalid) {
                    var data = $scope.edt;
                    data.opr = 'I';


                    $http.post(ENV.apiUrl + "api/homeroom/CUDRHomeroomBatch", data).then(function (MSG) {
                        $scope.homeroombacth_display = false;
                        $scope.homeroombacth_grid = true;
                        $scope.msg1 = MSG.data;


                        if ($scope.msg1 == true) {

                            swal({ text: 'Information Inserted Successfully', width: 380, showCloseButton: true });
                            $http.get(ENV.apiUrl + "api/homeroom/getSims_HomeroomBatch").then(function (Sims_HomeroomBatch) {
                                $scope.Sims_Homeroom_Batch = Sims_HomeroomBatch.data;
                                $scope.totalItems1 = $scope.Sims_Homeroom_Batch.length;
                                $scope.todos1 = $scope.Sims_Homeroom_Batch;
                                $scope.row2 = '';
                                $scope.makeTodos1();
                            });
                        }
                        else {
                            swal({ text: 'Information Not Inserted', width: 380, showCloseButton: true });
                        }
                    });
                }
            }

            $scope.HomeroomBatch_Update = function () {

                var data = $scope.edt;
                data.opr = 'U';
               
                $http.post(ENV.apiUrl + "api/homeroom/CUDRHomeroomBatch",data).then(function (MSG) {
                    $scope.homeroombacth_display = false;
                    $scope.homeroombacth_grid = true;
                    $scope.msg1 = MSG.data;

                    if ($scope.msg1 == true) {

                        swal({ text: 'Information Updated Successfully', width: 380, showCloseButton: true });
                        $http.get(ENV.apiUrl + "api/homeroom/getSims_HomeroomBatch").then(function (Sims_HomeroomBatch) {
                            $scope.Sims_Homeroom_Batch = Sims_HomeroomBatch.data;
                            $scope.totalItems1 = $scope.Sims_Homeroom_Batch.length;
                            $scope.todos1 = $scope.Sims_Homeroom_Batch;
                            $scope.row2 = '';
                            $scope.makeTodos1();
                        });
                    }
                    else {

                        swal({ text: 'Information Not Updated', width: 380, showCloseButton: true });

                    }

                   

                });
            }
            
            $scope.HomeroomBatch_Delete = function () {
                var flag = false;
                HomeroomBatch_code = [];
                var code = [];
                var HomeroomBatch_code1 = [];
                for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                    var t = $scope.filteredTodos1[i].sims_batch_code;
                    var v = document.getElementById(t+i);

                    if (v.checked == true) {
                        flag = true;
                        HomeroomBatch_code1 = {
                            'sims_cur_code': $scope.filteredTodos1[i].sims_cur_code,
                            'sims_academic_year': $scope.filteredTodos1[i].sims_academic_year,
                            'sims_batch_name': $scope.filteredTodos1[i].sims_batch_name,
                            'sims_homeroom_code': $scope.filteredTodos1[i].sims_homeroom_code,
                            'sims_batch_code': $scope.filteredTodos1[i].sims_batch_code,
                            'opr': 'D'
                        };

                        code.push(HomeroomBatch_code1);
                    }
                    
                }
                if (flag == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/homeroom/CUDRHomeroomBatchDelete", code).then(function (MSG) {

                                $scope.homeroombacth_display = false;
                                $scope.homeroombacth_grid = true;
                                $scope.msg1 = MSG.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: 'Information Deleted Successfully', width: 380, showCloseButton: true });
                                    $http.get(ENV.apiUrl + "api/homeroom/getSims_HomeroomBatch").then(function (Sims_HomeroomBatch) {
                                        $scope.Sims_Homeroom_Batch = Sims_HomeroomBatch.data;
                                        $scope.totalItems1 = $scope.Sims_Homeroom_Batch.length;
                                        $scope.todos1 = $scope.Sims_Homeroom_Batch;
                                        $scope.row2 = '';
                                        $scope.makeTodos1();
                                    });

                                }
                                else {

                                    swal({ text: 'Information Not Deleted', width: 380, showCloseButton: true });

                                    main2 = document.getElementById('mainchk1');
                                    if (main2.checked == true) {
                                        main2.checked = false;
                                    }
                                    $scope.HomeroomBatch_MultipleDelete();
                                }

                                
                            });
                        }
                        else {

                            main2 = document.getElementById('mainchk1');
                            if (main2.checked == true) {
                                main2.checked = false;
                            }
                            $scope.HomeroomBatch_MultipleDelete();
                        }
                    })
                }
                else {
                    swal({ text: 'Select At Least One Record To Delete', width: 380, showCloseButton: true });
                }
            }

            $scope.HomeroomBatch_MultipleDelete = function () {
                main2 = document.getElementById('mainchk1');
                for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                    var t = $scope.filteredTodos1[i].sims_batch_code;
                    var v = document.getElementById(t+i);
                if (main2.checked == true) {
                        v.checked = true;
                        HomeroomBatch_code = HomeroomBatch_code + $scope.filteredTodos1[i].sims_batch_code + ',';
                        $('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                }
                else {
                        v.checked = false;
                        main2.checked = false;
                        HomeroomBatch_code = [];
                        $('tr').removeClass("row_selected");
                       
                    }

                }
                
            }

            $scope.HomeroomBatch_Delete_Onebyone = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main2 = document.getElementById('mainchk1');
                if (main2.checked == true) {
                    main2.checked = false;
                }
            }

            $scope.filteredTodos1 = [], $scope.currentPage1 = 1, $scope.numPerPage1 = 5, $scope.maxSize1 = 5;

            $scope.makeTodos1 = function () {
                var rem = parseInt($scope.totalItems1 % $scope.numPerPage1);
                if (rem == '0') {
                    $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1);
                }
                else {
                    $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1) + 1;
                }
                var begin = (($scope.currentPage1 - 1) * $scope.numPerPage1);
                var end = parseInt(begin) + parseInt($scope.numPerPage1);
                $scope.filteredTodos1 = $scope.todos1.slice(begin, end);

                main2 = document.getElementById('mainchk1');
                if (main2.checked == true) {
                    main2.checked = false;
                    
                }
                $scope.HomeroomBatch_MultipleDelete();
            };

            $scope.size1 = function (str) {
                $scope.pagesize1 = str;
                $scope.currentPage1 = 1;
                $scope.numPerPage1 = str;
              
                main2 = document.getElementById('mainchk1');
                if (main2.checked == true) {
                    main2.checked = false;
                  
                    $scope.color = '#edefef';
                }
                $scope.makeTodos1();
            }

            $scope.index1 = function (str) {

                main2 = document.getElementById('mainchk1');
                if (main2.checked == true) {
                    main2.checked = false;
                    $scope.row2 = '';
                    $scope.color = '#edefef';
                }

                $scope.pageindex1 = str;
                $scope.currentPage1 = str;
                $scope.makeTodos1();

               
            }

            $scope.searched1 = function (valLists, toSearch) {

               
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil1(i, toSearch);
                });
            };

            $scope.search1 = function () {
                $scope.todos1 = $scope.searched1($scope.Sims_Homeroom_Batch, $scope.searchText);
                $scope.totalItems1 = $scope.todos1.length;
                $scope.currentPage1 = '1';
                if ($scope.searchText == '') {
                    $scope.todos1 = $scope.Sims_Homeroom_Batch;
                }
                $scope.makeTodos1();
            }

            function searchUtil1(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_batch_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_batch_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

        }])
})();