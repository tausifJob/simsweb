﻿(function () {
    'use strict';
    var obj1;
    var temp, opr = "";
    var obj2;
    var temp;
    var main, del = [];
    var data1 = [];
    var simsController = angular.module('sims.module.Setup');

    simsController.controller('NationalityController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'filterFilter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog,
            $http, filterFilter, ENV, $filter) {
            $scope.dis1 = true;
            $scope.cmsl = true;
            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.msg = false; $scope.operation = false; $scope.table = true;
            $scope.Sbtn = false; $scope.Nbtn = true; $scope.update = false; $scope.Update_btn = false;
            //$scope.roles.comn_role_name = 'myRole';
            //$http.get(ENV.apiUrl + "api/common/Nationality/getAllSimsNationality").then(function (res1) {
            //    $scope.display = true;
            //    $scope.dis1 = true;
            //    $scope.obj1 = res1.data;
            //    $scope.totalItems = $scope.obj1.length;
            //    console.log($scope.totalItems);
            //    $scope.todos = $scope.obj1;
            //    console.log($scope.totalItems);
            //    $scope.makeTodos();
            //});

            $http.get(ENV.apiUrl + "api/common/Nationality/getAllCountries").then(function (res) {
                $scope.country = res.data;
            })
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);
            $http.get(ENV.apiUrl + "api/common/Nationality/getAllCountries").then(function (res1) {
                $scope.obj2 = res1.data;
                console.log($scope.obj2);
            });

            $http.get(ENV.apiUrl + "api/common/Nationality/getAllNationalityDetails").then(function (SGD_Data) {
                $scope.SGDExamData = SGD_Data.data;
                $scope.totalItems = $scope.SGDExamData.length;
                $scope.todos = $scope.SGDExamData;
                $scope.makeTodos();
                console.log($scope.SGDExamData);

            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.check_once = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }

            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.SGDExamData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.SGDExamData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                debugger
                /* Search Text in all 3 fields */
                return (item.sims_country_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.nationality_name_eng.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.nationality_name_ar.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.nationality_name_fr.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.nationality_name_ot.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.nationality_code == toSearch) ? true : false;
            }

            $scope.update_click = function (str) {
                debugger;
                $scope.operation = true;
                $scope.table = false;
                $scope.Update_btn = true;
                $scope.Save_btn = false;
                $scope.readonly = true;
                $scope.temp = {
                    nationality_code: str.nationality_code
                        , sims_country_code: str.sims_country_code
                        , sims_country_name_en: str.sims_country_name_en
                        , nationality_name_eng: str.nationality_name_eng
                        , nationality_name_ar: str.nationality_name_ar
                        , nationality_name_fr: str.nationality_name_fr
                        , nationality_name_ot: str.nationality_name_ot
                        , nationality_status: str.nationality_status



                };

            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.check_all();
            }
            $scope.New = function (str) {
                $scope.temp = "";
                $scope.opr = 'I';
                $scope.operation = true;
                $scope.table = false;
                $scope.Update_btn = false;
                $scope.Save_btn = true;
                $scope.readonly = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            $scope.can = function () {
                $scope.operation = false;
                $scope.table = true;
                $scope.temp = "";
                $scope.opr = "";
            }

            $scope.Save = function (Myform) {
                if (Myform) {
                    var data = $scope.temp;
                    data.opr = 'I';
                    $scope.exist = false;
                    data1.push(data);
                    for (var i = 0; i < $scope.SGDExamData.length; i++) {
                        if ($scope.SGDExamData[i].nationality_code == data.nationality_code && $scope.SGDExamData[i].sims_country_code == data.sims_country_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ title: "Alert", text: "Record Already exists", width: 300, height: 200 });
                    }

                    else {

                        $http.post(ENV.apiUrl + "api/common/Nationality/NationalityCUD?simsobj=", data1).then(function (msg) {

                            $scope.ScheduleGroupDetailOperation = false;
                            $http.get(ENV.apiUrl + "api/common/Nationality/getAllNationalityDetails").then(function (SGD_Data) {
                                $scope.SGDData = SGD_Data.data;
                                $scope.totalItems = $scope.SGDData.length;
                                $scope.todos = $scope.SGDData;
                                $scope.makeTodos();
                                $scope.msg1 = msg.data;

                                if ($scope.msg1 == true) {

                                    swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });

                                    $http.get(ENV.apiUrl + "api/common/Nationality/getAllNationalityDetails").then(function (SGD_Data) {
                                        $scope.SGDExamData = SGD_Data.data;
                                        $scope.totalItems = $scope.SGDExamData.length;
                                        $scope.todos = $scope.SGDExamData;
                                        $scope.makeTodos();
                                        console.log($scope.SGDExamData);

                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                                }

                            });

                        });

                        $scope.table = true;
                        $scope.operation = false;
                    }
                    data1 = [];
                }
            }

            $scope.Update = function () {

                debugger;
                var data = $scope.temp;
                data.opr = 'U';
                $scope.temp = "";
                data1.push(data);
                $http.post(ENV.apiUrl + "api/common/Nationality/NationalityCUD", data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    $scope.ScheduleGroupDetailOperation = false;
                    $http.get(ENV.apiUrl + "api/common/Nationality/getAllNationalityDetails").then(function (SGD_Data) {
                        $scope.SGDData = SGD_Data.data;
                        $scope.totalItems = $scope.SGDData.length;
                        $scope.todos = $scope.SGDData;

                        $scope.makeTodos();

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });

                            $http.get(ENV.apiUrl + "api/common/Nationality/getAllNationalityDetails").then(function (SGD_Data) {
                                $scope.SGDExamData = SGD_Data.data;
                                $scope.totalItems = $scope.SGDExamData.length;
                                $scope.todos = $scope.SGDExamData;
                                $scope.makeTodos();
                                console.log($scope.SGDExamData);

                            });

                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                    });

                })
                $scope.table = true;
                $scope.operation = false;
                data1 = [];
            }


            $scope.Delete = function () {
                debugger;
                var data1 = [];
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById("test-" + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'nationality_code': $scope.filteredTodos[i].nationality_code,
                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/common/Nationality/NationalityCUD", deleteleave).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/common/Nationality/getAllNationalityDetails").then(function (getLeaveDetail_data) {
                                                $scope.LeaveData = getLeaveDetail_data.data;
                                                $scope.totalItems = $scope.LeaveData.length;
                                                $scope.todos = $scope.LeaveData;
                                                $scope.makeTodos();
                                            });
                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/common/Nationality/getAllNationalityDetails").then(function (getLeaveDetail_data) {
                                                $scope.LeaveData = getLeaveDetail_data.data;
                                                $scope.totalItems = $scope.LeaveData.length;
                                                $scope.todos = $scope.LeaveData;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById("test-" + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.row1 = '';
                main.checked = false;
                $scope.currentPage = str;
            }


        }])
})();
