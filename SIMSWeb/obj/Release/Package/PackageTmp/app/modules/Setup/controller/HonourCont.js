﻿(function () {
    'use strict';
    var obj1, obj2, temp, main;
    var opr = '';
    var categorycode = [];

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('HonourCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.HonourDetail = true;
            $scope.editmode = false;
            $scope.edt = {};
            $scope.pager = true;
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;
            var data1 = [];

            $http.get(ENV.apiUrl + "api/Honour/getHonourDetail").then(function (honour_data) {

                $scope.honourdata = honour_data.data;
                $scope.totalItems = $scope.honourdata.length;
                $scope.todos = $scope.honourdata;
                $scope.makeTodos();
                console.log($scope.honourdata);
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.honourdata;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.honourdata, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.honourdata;
                }

                $scope.makeTodos();
                $scope.check_all();
                main.checked = false;
            }

            function searchUtil(item, toSearch) {
                debugger
                /* Search Text in all 3 fields */
                return (
                        item.sims_honour_cur_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.year_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_honour_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_honour_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_honour_criteria_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_honoursims_honour_min_criteria_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_honour_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_honour_type == toSearch) ? true : false;
            }

            $scope.cancel = function () {
                $scope.HonourDetail = true;
                $scope.HonourOperation = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.newmode = true;

                $scope.editmode = false;
                opr = 'S';
                $scope.readonly = false;
                $scope.HonourDetail = false;
                $scope.HonourOperation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;

                $scope.edt = {};
                $scope.edt['sims_honour_type'] = '01';
                $scope.edt['sims_honour_criteria_type'] = '02';
                $http.get(ENV.apiUrl + "api/Honour/getMaxValue").then(function (max_val) {
                    debugger
                    $scope.max_val = max_val.data;
                    $scope.edt.sims_honour_code = $scope.max_val[0].max_num;

                });
                $scope.edt.sims_honour_status = true;
            }

            $scope.data = function (str) {
                debugger;
                if (str > 100) {
                    swal({ title: "Alert", text: "Value not exceed than 100", showCloseButton: true, width: 380 });
                    $scope.edt.sims_honour_min_criteria = '';
                }

            }

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {

                $scope.curriculum = AllCurr.data;

                $scope.temp = { s_cur_code: $scope.curriculum[0].sims_cur_code };
                $scope.getAccYear();
            });

            $scope.getAccYear = function (curCode) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.temp.s_cur_code).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = {
                        s_cur_code: $scope.curriculum[0].sims_cur_code,
                        sims_academic_year: $scope.Acc_year[0].sims_academic_year
                    }
                    //$scope.getGrade();
                });

            }

            $scope.Save = function (myForm) {
                debugger;
                if (myForm) {
                    var data = {
                        sims_honour_cur_code: $scope.temp.s_cur_code
                        , sims_academic_year: $scope.temp.sims_academic_year
                         , sims_honour_code: $scope.edt.sims_honour_code
                         , sims_honour_desc: $scope.edt.sims_honour_desc
                            , sims_honour_type: $scope.edt.sims_honour_type
                            , sims_honour_criteria_type: $scope.edt.sims_honour_criteria_type
                             , sims_honour_min_criteria: $scope.edt.sims_honour_min_criteria
                         , sims_honour_status: $scope.edt.sims_honour_status
                    }
                    data.opr = 'I';
                    $scope.exist = false;
                    data1.push(data);
                    //for (var i = 0; i < $scope.honourdata.length; i++) {
                    //    if ($scope.honourdata[i].sims_honour_type == data.sims_honour_type) {
                    //        $scope.exist = true;
                    //    }
                    //}
                    //if ($scope.exist) {
                    //    swal({ title: "Alert", text: "Record Already exists", width: 300, height: 200 });
                    //}

                    //else {

                    $http.post(ENV.apiUrl + "api/Honour/HonourCUD?simsobj=", data1).then(function (msg) {

                        $scope.HonourOperation = false;
                        $http.get(ENV.apiUrl + "api/Honour/getHonourDetail").then(function (honour_data) {
                            $scope.honourdata = honour_data.data;
                            $scope.totalItems = $scope.honourdata.length;
                            $scope.todos = $scope.honourdata;
                            $scope.makeTodos();
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {

                                swal({ title: "Alert", text: "Record Inserted Successfully", showCloseButton: true, width: 380 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Inserted", showCloseButton: true, width: 380 });
                            }

                        });

                    });

                    $scope.HonourDetail = true;
                    $scope.HonourOperation = false;
                }
                data1 = [];
                //}
            }

            $scope.up = function (str) {
                debugger
                opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.HonourDetail = false;
                $scope.HonourOperation = true;
                var abc = str.sims_honour_min_criteria
                var st = []
                st = abc.split(".");
                $scope.edt = {
                    sims_honour_cur_code: str.s_cur_code
                        , sims_academic_year: str.sims_academic_year
                         , sims_honour_code: str.sims_honour_code
                         , sims_honour_desc: str.sims_honour_desc
                            , sims_honour_type: str.sims_honour_type
                            , sims_honour_criteria_type: str.sims_honour_criteria_type
                             , sims_honour_min_criteria: st[0]
                         , sims_honour_status: str.sims_honour_status
                };
            }

            $scope.Update = function () {

                debugger;
                var data = $scope.edt;
                data.opr = 'U';
                $scope.edt = "";
                data1.push(data);
                $http.post(ENV.apiUrl + "api/Honour/HonourCUD", data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    $scope.HonourOperation = false;
                    $http.get(ENV.apiUrl + "api/Honour/getHonourDetail").then(function (honour_data) {
                        $scope.honourdata = honour_data.data;
                        $scope.totalItems = $scope.honourdata.length;
                        $scope.todos = $scope.honourdata;
                        $scope.makeTodos();

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", showCloseButton: true, width: 380 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", showCloseButton: true, width: 380 });
                        }
                    });

                })
                $scope.HonourOperation = false;
                $scope.HonourDetail = true;
                data1 = [];
            }

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.check_once = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.deleterecord = function () {
                debugger;
                var data1 = [];
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_honour_code': $scope.filteredTodos[i].sims_honour_code,
                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger
                            $http.post(ENV.apiUrl + "api/Honour/HonourCUD", deleteleave).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Honour/getHonourDetail").then(function (honour_data) {
                                                $scope.honourdata = honour_data.data;
                                                $scope.totalItems = $scope.honourdata.length;
                                                $scope.todos = $scope.honourdata;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                                else {
                                    debugger
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Honour/getHonourDetail").then(function (honour_data) {
                                                $scope.honourdata = honour_data.data;
                                                $scope.totalItems = $scope.honourdata.length;
                                                $scope.todos = $scope.honourdata;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            debugger
                            main = document.getElementById('all_chk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
                $scope.row1 = '';
            }

        }]);

})();