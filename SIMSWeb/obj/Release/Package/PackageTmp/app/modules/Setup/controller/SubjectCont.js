﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Setup');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SubjectCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            //$scope.pagesize = "5";
            //$scope.pageindex = "1";

            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.sub_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.edit_code = false;

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $http.get(ENV.apiUrl + "api/common/Subject/getSubject").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.sub_data = res.data;
                $scope.totalItems = $scope.sub_data.length;
                $scope.todos = $scope.sub_data;
                $scope.makeTodos();

                for (var i = 0; i < $scope.totalItems; i++) {
                    $scope.sub_data[i].icon = "fa fa-plus-circle";
                }

            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.obj2 = res.data;
                //console.log($scope.obj2);
                // $scope.Getinfo($scope.edt.sims_attendance_cur_code);
            });

            $scope.Getinfo = function (cur) {

                if (cur != '') {
                    $http.get(ENV.apiUrl + "api/common/Subject/getSubjectGroup?curCode=" + cur).then(function (res) {
                        $scope.subgrp = res.data;
                        console.log($scope.obj1);
                    });

                    $http.get(ENV.apiUrl + "api/common/Subject/getSubjectType?curCode=" + cur).then(function (res) {
                        $scope.obj3 = res.data;
                        console.log($scope.obj3);
                    });

                }

            }



            $http.get(ENV.apiUrl + "api/common/Subject/getCountry").then(function (res) {
                $scope.obj4 = res.data;
                console.log($scope.obj4);
            });

            $scope.Save = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        $http.post(ENV.apiUrl + "api/common/Subject/CheckSubject?SubjectCode=" + $scope.edt.subject_code + "&cur_code=" + $scope.edt.sims_attendance_cur_code).then(function (res) {
                            $scope.msg1 = res.data;
                            console.log($scope.msg1);

                            if ($scope.msg1.status == true) {
                                //  $('#myModal').modal('show');
                                swal({ title: "Alert", text: "Subject Already Exists", showCloseButton: true, width: 380, });
                                $scope.edt = "";

                            }
                            else {
                                var data = ({
                                    sims_attendance_cur_code: $scope.edt.sims_attendance_cur_code,
                                    subject_code: $scope.edt.subject_code,
                                    subject_group_code: $scope.edt.subject_group_code,
                                    subject_name_en: $scope.edt.subject_name_en,
                                    subject_name_ar: $scope.edt.subject_name_ar,
                                    subject_name_fr: $scope.edt.subject_name_fr,
                                    subject_name_ot: $scope.edt.subject_name_ot,
                                    subject_name_abbr: $scope.edt.subject_name_abbr,
                                    subject_moe_code: $scope.edt.subject_moe_code,
                                    subject_status: $scope.edt.subject_status,
                                    subject_type: $scope.edt.subject_type,
                                    subject_country_code: $scope.edt.subject_country_code,
                                    opr: 'I'
                                });

                                data1.push(data);
                                $http.post(ENV.apiUrl + "api/common/Subject/updateInsertDeleteSubject", data1).then(function (res) {
                                    $scope.display = true;
                                    $scope.msg1 = res.data;
                                    if ($scope.msg1 == true) {
                                        swal({ title: "Alert", text: "Subject Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else {
                                        swal({ title: "Alert", text: "Subject Not Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/common/Subject/getSubject").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.sub_data = res.data;
                    $scope.totalItems = $scope.sub_data.length;
                    $scope.todos = $scope.sub_data;
                    $scope.makeTodos();
                    for (var i = 0; i < $scope.totalItems; i++) {
                        $scope.sub_data[i].icon = "fa fa-plus-circle";
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    var data = ({
                        sims_attendance_cur_code: $scope.edt.sims_attendance_cur_code,
                        subject_code: $scope.edt.subject_code,
                        subject_group_code: $scope.edt.subject_group_code,
                        subject_name_en: $scope.edt.subject_name_en,
                        subject_name_ar: $scope.edt.subject_name_ar,
                        subject_name_fr: $scope.edt.subject_name_fr,
                        subject_name_ot: $scope.edt.subject_name_ot,
                        subject_name_abbr: $scope.edt.subject_name_abbr,
                        subject_moe_code: $scope.edt.subject_moe_code,
                        subject_status: $scope.edt.subject_status,
                        subject_type: $scope.edt.subject_type,
                        subject_country_code: $scope.edt.subject_country_code,
                        opr: 'U'
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/Subject/updateInsertDeleteSubject", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Subject Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });

                        }
                        else {
                            swal({ title: "Alert", text: "Subject Not Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                    });
                }
            }

            $scope.Delete = function () {

                var deletecode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    var v = document.getElementById($scope.filteredTodos[i].subject_code + i);

                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'subject_code': $scope.filteredTodos[i].subject_code,
                            'sims_attendance_cur_code': $scope.filteredTodos[i].sims_attendance_cur_code,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {


                            $http.post(ENV.apiUrl + "api/common/Subject/updateInsertDeleteSubject", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Subject Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });

                                }
                                else {
                                    swal({ title: "Alert", text: "Subject Not Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].subject_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                            $scope.row1 = '';
                        }

                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].subject_code + i);
                        v.checked = true;
                        if (v.checked == true) {
                            $('tr').addClass("row_selected");
                        }
                        //$scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].subject_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.New = function () {
                var autoid;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = [];

                $scope.edit_code = false;
                $scope.edt = {
                    subject_status: true
                }


                $http.get(ENV.apiUrl + "api/common/Subject/getAutoGeneratesubjectCode").then(function (res) {
                    autoid = res.data;
                    console.log(autoid);
                    $scope.edt['subject_code'] = autoid;
                });
            }

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edt =
                    {
                        sims_attendance_cur_code: str.sims_attendance_cur_code,
                        subject_code: str.subject_code,
                        subject_group_code: str.subject_group_code,
                        subject_name_en: str.subject_name_en,
                        subject_name_ar: str.subject_name_ar,
                        subject_name_fr: str.subject_name_fr,
                        subject_name_ot: str.subject_name_ot,
                        subject_name_abbr: str.subject_name_abbr,
                        subject_moe_code: str.subject_moe_code,
                        subject_status: str.subject_status,
                        subject_type: str.subject_type,
                        subject_country_code: str.subject_country_code,
                    }

                $scope.edit_code = true;


                $scope.Getinfo($scope.edt.sims_attendance_cur_code);
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.sub_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.sub_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].subject_code+i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.subject_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.subject_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.subject_name_abbr.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;

            }

            var dom;
            $scope.flag = true;
            $scope.icon = "fa fa-plus-circle";

            $scope.expand = function (info, $event) {
                console.log(info);

                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                        //"<tr><td class='semi-bold'>" + "In Arabic" + "</td>" + "<td class='semi-bold'colsapn='2'>" + "In French" + "" + "<td class='semi-bold'colsapn='2'>" + "In Regional" + "<td class='semi-bold'>" + "Short Name" + "</td></tr>" +
                        //  "<tr><td>" + (info.subject_name_ar) + "</td> " + "<td colsapn='2'>" + (info.subject_name_fr) + "" + "<td colsapn='2'>" + (info.subject_name_ot) + "<td>" + (info.subject_name_abbr) + "</td></tr>" +
                        //  "<tr><td class='semi-bold'colsapn='2'>" + "MOE Code" + "<td class='semi-bold'>" + "Subject Type" + "</td> " + "<td class='semi-bold'colsapn='2'>" + "Country Name" + "</td></tr>" +
                        //   "<tr><td colsapn='2'>" + (info.subject_moe_code) + "<td>" + (info.subject_type_name) + "</td>" + "<td colsapn='2'>" + (info.subject_country_code_name) + "</td></tr>" +

                         "<tr><td class='semi-bold'>" + "In Arabic" + "</td>" + "<td class='semi-bold'colsapn='2'>" + "In French" + "" + "<td class='semi-bold'colsapn='2'>" + "In Regional" + "<td class='semi-bold'>" + "Short Name" + "</td><td class='semi-bold'colsapn='2'>" + "MOE Code" + "<td class='semi-bold'>" + "Subject Type" + "</td> " + "<td class='semi-bold'colsapn='2'>" + "Country Name" + "</td></tr>" +
                          "<tr><td>" + (info.subject_name_ar) + "</td> " + "<td colsapn='2'>" + (info.subject_name_fr) + "" + "<td colsapn='2'>" + (info.subject_name_ot) + "<td>" + (info.subject_name_abbr) + "</td><td colsapn='2'>" + (info.subject_moe_code) + "<td>" + (info.subject_type_name) + "</td>" + "<td colsapn='2'>" + (info.subject_country_code_name) + "</td></tr>" +
                        "</tbody>" +
                        " </table></td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    info.icon = "fa fa-plus-circle";
                    $scope.flag = true;
                }
            };

            $scope.ex = function (str) {
                console.log("hello");
            }

        }])
})();