﻿(function () {
    'use strict';
    var data1 = [];
    // var opr = '';
    var councilcode = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CouncilMasterCont',
         ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
             $scope.pagesize = '5';
             //$scope.operation = true;
             $scope.table1 = true;
             $scope.editmode = false;
             $scope.edt = "";

             $scope.display = false;
             $scope.grid = true;

             $timeout(function () {
                 $("#fixTable").tableHeadFixer({ 'top': 1 });
             }, 100);

             $timeout(function () {
                 $("#fixTable1").tableHeadFixer({ 'top': 1 });

             }, 100);

             $scope.size = function (str) {
                 console.log(str);
                 $scope.pagesize = str;
                 $scope.currentPage = 1;
                 $scope.numPerPage = str;
                 console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
             }

             $scope.index = function (str) {
                 $scope.pageindex = str;
                 $scope.currentPage = str;
                 console.log("currentPage=" + $scope.currentPage);
                 $scope.makeTodos();
                 main.checked = false;
                 $scope.check_all();
             }

             $http.get(ENV.apiUrl + "api/CouncilMaster/getCouncil").then(function (get_CouncilM) {
                 $scope.CouncilMData = get_CouncilM.data;
                 $scope.totalItems = $scope.CouncilMData.length;
                 $scope.todos = $scope.CouncilMData;
                 $scope.makeTodos();
                 console.log($scope.CouncilMData);
             })

             $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                 $scope.curriculum = AllCurr.data;
             });

             $scope.getAccYear = function (curCode) {
                 $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                     $scope.Acc_year = Acyear.data;
                 });
             }

             //$http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCurriculum) {
             //    debugger;
             //    $scope.Curriculum = getCurriculum.data;               
             //    console.log($scope.Curriculum);
             //});

             //$scope.getacademicYear = function (str) {
             //    debugger;
             //    $scope.curriculum_code = str;
             //    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (getAcademicYear) {
             //        $scope.AcademicYear = getAcademicYear.data;
             //        //$scope.temp['sims_academic_year'] = $scope.AcademicYear[0].sims_academic_year;
             //        // $scope.getGrade($scope.Curriculum[0].sims_cur_code,$scope.AcademicYear[0].sims_academic_year)
             //        console.log($scope.AcademicYear);
             //    })
             //}


             //$http.get(ENV.apiUrl + "api/CouncilMaster/getCurriculum").then(function (res) {
             //    debugger
             //    $scope.curriculum = res.data;
             //    console.log($scope.curriculum);
             //})


             $http.get(ENV.apiUrl + "api/CouncilMaster/getCurriculum").then(function (res) {

                 $scope.year = res.data;
                 console.log($scope.year);
             })

             $scope.getacyr = function (str) {

                 $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                     $scope.Academic_year = Academicyear.data;
                 })
             }

             $scope.New = function () {

                 $scope.newmode = true;
                 $scope.check = true;
                 $scope.editmode = false;
                 $scope.opr = 'S';
                 $scope.readonly = false;
                 $scope.table1 = false;
                 $scope.operation = true;
                 $scope.savebtn = true;
                 $scope.updatebtn = false;
                 $scope.edt = "";
                 $scope.myForm.$setPristine();
                 $scope.myForm.$setUntouched();

             }

             $scope.up = function (str) {
                 $scope.opr = 'U';
                 $scope.editmode = true;
                 $scope.newmode = false;
                 $scope.savebtn = false;
                 $scope.updatebtn = true;
                 $scope.readonly = true;
                 $scope.table1 = false;
                 $scope.operation = true;
                 $scope.edt = {
                     sims_cur_code: str.sims_cur_code
                               , sims_academic_year: str.sims_academic_year
                               , sims_council_code: str.sims_council_code
                               , sims_council_name: str.sims_council_name
                               , sims_council_status: str.sims_council_status

                 };
                 $scope.getAccYear(edt.sims_cur_code);
             }

             $scope.cancel = function () {
                 $scope.table1 = true;
                 $scope.operation = false;
                 $scope.myForm.$setPristine();
                 $scope.myForm.$setUntouched();
                 $scope.edt.sims_cur_code = "";
                 $scope.edt.sims_council_name = "";
                 $scope.edt.sims_council_code = "";
                 $scope.edt.sims_academic_year = "";
                 $scope.edt.sims_council_status = "";
                 $scope.myForm.$setPristine();
                 $scope.myForm.$setUntouched();


             }

             $scope.edit = function (str) {
                 debugger;
                 //$scope.readonly = true;
                 //$scope.disabled = true;
                 $scope.table1 = false;
                 $scope.operation = true;
                 $scope.savebtn = false;
                 $scope.updatebtn = true;
                 //$scope.temp = str;

                 $scope.getDocumentN(str.sims_council_code);
                 $scope.getacyr(str.sims_academic_year);

                 $scope.edt = {
                     sims_cur_code: str.sims_cur_code
                 , sims_academic_year: str.sims_academic_year
                 , sims_council_code: str.sims_council_code
                 , sims_council_name: str.sims_council_name
                 , sims_council_status: str.sims_council_status

                 };

             }

             $scope.Save = function (myForm) {
                 debugger
                 if (myForm) {
                     data1 = [];
                     var data = $scope.edt;
                     data.opr = 'I';
                     $scope.edt = "";
                     $scope.exist = false;
                     for (var i = 0; i < $scope.CouncilMData.length; i++) {
                         if ($scope.CouncilMData[i].sims_council_code == data.sims_council_code && $scope.CouncilMData[i].sims_academic_year == data.sims_academic_year) {
                             $scope.exist = true;
                         }

                     }
                     if ($scope.exist) {
                         swal({ title: "Alert", text: "Record Already exists", width: 300, height: 200 });
                     }
                     else {
                         data1.push(data);
                         $http.post(ENV.apiUrl + "api/CouncilMaster/CUDCouncil", data1).then(function (msg) {
                             $scope.msg1 = msg.data;
                             debugger
                             // swal({ text: $scope.msg1.strMessage, timer: 5000 });
                             $scope.operation = false;
                             $http.get(ENV.apiUrl + "api/CouncilMaster/getCouncil").then(function (get_CouncilM) {
                                 $scope.CouncilMData = get_CouncilM.data;
                                 $scope.totalItems = $scope.CouncilMData.length;
                                 $scope.todos = $scope.CouncilMData;
                                 $scope.makeTodos();

                                 $scope.msg1 = msg.data;

                                 if ($scope.msg1 == true) {
                                     swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                                 }
                                 else {
                                     swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                                 }
                             });

                         });
                     }
                     $scope.table1 = true;
                     $scope.operation = false;

                 }
             }

             var dataforUpdate = [];
             $scope.Update = function (Myform) {
                 if (Myform) {
                     debugger;
                     dataforUpdate = [];
                     var data = $scope.edt;
                     data.opr = "U";
                     dataforUpdate.push(data);
                     //dataupdate.push(data);
                     $http.post(ENV.apiUrl + "api/CouncilMaster/CUDCouncil", dataforUpdate).then(function (msg) {
                         $scope.msg1 = msg.data;
                         if ($scope.msg1 == true) {
                             swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                         }
                         else {
                             swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                         }

                         $http.get(ENV.apiUrl + "api/CouncilMaster/getCouncil").then(function (get_CouncilM) {
                             $scope.CouncilMData = get_CouncilM.data;
                             $scope.totalItems = $scope.CouncilMData.length;
                             $scope.todos = $scope.CouncilMData;
                             $scope.makeTodos();
                         });

                     });

                     $scope.operation = false;
                     $scope.table1 = true;
                 }
             }


             $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

             $scope.makeTodos = function () {
                 var rem = parseInt($scope.totalItems % $scope.numPerPage);
                 if (rem == '0') {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                 }
                 else {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                 }
                 var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                 var end = parseInt(begin) + parseInt($scope.numPerPage);

                 console.log("begin=" + begin); console.log("end=" + end);

                 $scope.filteredTodos = $scope.todos.slice(begin, end);
             };

             $scope.checkonebyonedelete = function () {
                 $("input[type='checkbox']").change(function (e) {
                     if ($(this).is(":checked")) {
                         $(this).closest('tr').addClass("row_selected");
                         $scope.color = '#edefef';
                     } else {
                         $(this).closest('tr').removeClass("row_selected");
                         $scope.color = '#edefef';
                     }
                 });

                 main = document.getElementById('mainchk');
                 if (main.checked == true) {
                     main.checked = false;
                     $("input[type='checkbox']").change(function (e) {
                         if ($(this).is(":checked")) {
                             $(this).closest('tr').addClass("row_selected");
                         }
                         else {
                             $(this).closest('tr').removeClass("row_selected");
                         }
                     });
                 }
             }

             $scope.CheckAllChecked = function () {
                 debugger;
                 main = document.getElementById('mainchk');
                 if (main.checked == true) {
                     for (var i = 0; i < $scope.filteredTodos.length; i++) {
                         var v = document.getElementById(i + $scope.filteredTodos[i].sims_council_code);
                         v.checked = true;
                         $scope.row1 = 'row_selected';
                         $('tr').addClass("row_selected");
                     }
                 }
                 else {

                     for (var i = 0; i < $scope.filteredTodos.length; i++) {
                         var v = document.getElementById(i + $scope.filteredTodos[i].sims_council_code);
                         v.checked = false;
                         main.checked = false;
                         $scope.row1 = '';
                     }
                 }

             }

             $scope.deleterecord = function () {
                 var deletefin = [];
                 $scope.flag = false;
                 for (var i = 0; i < $scope.filteredTodos.length; i++) {
                     var v = document.getElementById(i + $scope.filteredTodos[i].sims_council_code);
                     if (v.checked == true) {
                         $scope.flag = true;
                         var deletemodulecode = ({
                             'sims_council_code': $scope.filteredTodos[i].sims_council_code,
                             opr: 'D'
                         });
                         deletefin.push(deletemodulecode);
                     }
                 }
                 if ($scope.flag) {
                     swal({
                         title: '',
                         text: "Are you sure you want to Delete?",
                         showCloseButton: true,
                         showCancelButton: true,
                         confirmButtonText: 'Yes',
                         width: 380,
                         cancelButtonText: 'No',

                     }).then(function (isConfirm) {
                         if (isConfirm) {
                             debugger;
                             $http.post(ENV.apiUrl + "api/CouncilMaster/CUDCouncil", deletefin).then(function (msg) {
                                 $scope.msg1 = msg.data;
                                 if ($scope.msg1 == true) {
                                     swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                         if (isConfirm) {

                                             $http.get(ENV.apiUrl + "api/CouncilMaster/getCouncil").then(function (get_CouncilM) {
                                                 $scope.CouncilMData = get_CouncilM.data;
                                                 $scope.totalItems = $scope.CouncilMData.length;
                                                 $scope.todos = $scope.CouncilMData;
                                                 $scope.makeTodos();
                                                 console.log($scope.CouncilMData);
                                             })
                                             main = document.getElementById('mainchk');
                                             if (main.checked == true) {
                                                 main.checked = false;
                                                 {
                                                     $scope.row1 = '';
                                                 }
                                             }
                                         }

                                     });
                                 }
                                 else {
                                     swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                         if (isConfirm) {
                                             $http.get(ENV.apiUrl + "api/CouncilMaster/getCouncil").then(function (get_CouncilM) {
                                                 $scope.CouncilMData = get_CouncilM.data;
                                                 $scope.totalItems = $scope.CouncilMData.length;
                                                 $scope.todos = $scope.CouncilMData;
                                                 $scope.makeTodos();
                                                 console.log($scope.CouncilMData);
                                             })
                                             main = document.getElementById('mainchk');
                                             if (main.checked == true) {
                                                 main.checked = false;
                                                 {
                                                     $scope.row1 = '';
                                                 }
                                             }
                                         }
                                     });
                                 }

                             });
                         }
                         else {
                             debugger
                             for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                 var v = document.getElementById(i + $scope.filteredTodos[i].sims_council_code);
                                 if (v.checked == true) {
                                     v.checked = false;
                                     main.checked = false;
                                     $('tr').removeClass("row_selected");
                                     $scope.row1 = '';

                                 }
                             }
                         }
                     });
                 }
                 else {
                     swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                 }
                 $scope.currentPage = true;
             }



             $scope.searched = function (valLists, toSearch) {
                 return _.filter(valLists,
                 function (i) {
                     /* Search Text in all  fields */
                     return searchUtil(i, toSearch);
                 });
             };

             $scope.search = function () {
                 $scope.todos = $scope.searched($scope.CouncilMData, $scope.searchText);
                 $scope.totalItems = $scope.todos.length;
                 $scope.currentPage = '1';
                 if ($scope.searchText == '') {
                     $scope.todos = $scope.CouncilMData;
                 }
                 $scope.makeTodos();
             }

             function searchUtil(item, toSearch) {
                 /* Search Text in all 3 fields */
                 return (
                      item.sims_council_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.sims_council_code == toSearch) ? true : false;
             }

             //$scope.searched = function (valLists, toSearch) {
             //    return _.filter(valLists,

             //    function (i) {
             //        /* Search Text in all  fields */
             //        return searchUtil(i, toSearch);
             //    });
             //};

             //$scope.search = function () {
             //    $scope.todos = $scope.searched($scope.smtpdata, $scope.searchText);
             //    $scope.totalItems = $scope.todos.length;
             //    $scope.currentPage = '1';
             //    if ($scope.searchText == '') {
             //        $scope.todos = $scope.CouncilMData;
             //    }
             //    $scope.makeTodos();
             //}

             //function searchUtil(item, toSearch) {
             //    /* Search Text in all 3 fields */
             //    //return (item.sims_smtp_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_smtp_username.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
             //    //  || item.sims_mod_code == toSearch) ? true : false;

             //    return (item.sims_council_code == toSearch) ? true : false;

             //}

         }]
         )
})();
