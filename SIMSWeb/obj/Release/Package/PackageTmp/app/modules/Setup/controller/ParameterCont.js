﻿(function () {
    'use strict';
    var del = [];
    var oldvalue = [], appl_form_field, appl_parameter, appl_form_field_value1;
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ParameterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.grid = true;
            var data1 = [];
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.parameter_data = [];
            $scope.edit_code = false;

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $http.get(ENV.apiUrl + "api/common/ParameterMaster/getParameterbyIndex").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.parameter_data = res.data;
                $scope.totalItems = $scope.parameter_data.length;
                $scope.todos = $scope.parameter_data;
                $scope.makeTodos();
                for (var i = 0; i < $scope.totalItems; i++) {
                    $scope.parameter_data[i].icon = "fa fa-plus-circle";
                }
            });


            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $http.get(ENV.apiUrl + "api/common/ApplicationMaster/getModules").then(function (res) {
                $scope.display = true;
                $scope.obj = res.data;
            });


            $scope.Getinfo1 = function (modulecode) {
                console.log(modulecode);

                if (modulecode != undefined) {
                    $http.get(ENV.apiUrl + "api/common/ParameterMaster/getApplication?modCode=" + modulecode).then(function (res) {
                        $scope.display = true;
                        $scope.appl_data = res.data;
                    });
                }
            }

            $scope.edit = function (str) {
                $scope.edit_code = true;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                // $scope.edt = str;

                $scope.edt =
                    {
                        comn_appl_mod_code: str.comn_appl_mod_code,
                        comn_application_code: str.comn_application_code,
                        sims_appl_form_field: str.sims_appl_form_field,
                        sims_appl_parameter: str.sims_appl_parameter,
                        sims_appl_form_field_value1: str.sims_appl_form_field_value1,
                        sims_appl_form_field_value2: str.sims_appl_form_field_value2,
                        sims_appl_form_field_value3: str.sims_appl_form_field_value3,
                        sims_appl_form_field_value4: str.sims_appl_form_field_value4,
                        appl_form_field: str.appl_form_field,
                        appl_form_field_value1: str.appl_form_field_value1,
                        appl_parameter: str.appl_parameter,
                    }
                $scope.Getinfo1(str.comn_appl_mod_code);

                console.log($scope.edt.sims_appl_form_field);
                appl_form_field = $scope.edt.sims_appl_form_field;

                //$scope.oldvalue.push({appl_form_field:$scope.edt.sims_appl_form_field, appl_parameter: $scope.edt.sims_appl_parameter, appl_form_field_value1: $scope.edt.sims_appl_form_field_value1 });
                console.log($scope.edt);


            }




            $scope.New = function () {
                $scope.edit_code = false;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }


            $scope.Save = function (isvalidate) {
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = ({
                            comn_appl_mod_code: $scope.edt.comn_appl_mod_code,
                            comn_application_code: $scope.edt.comn_application_code,
                            sims_appl_form_field: $scope.edt.sims_appl_form_field,
                            sims_appl_parameter: $scope.edt.sims_appl_parameter,
                            sims_appl_form_field_value1: $scope.edt.sims_appl_form_field_value1,
                            sims_appl_form_field_value2: $scope.edt.sims_appl_form_field_value2,
                            sims_appl_form_field_value3: $scope.edt.sims_appl_form_field_value3,
                            sims_appl_form_field_value4: $scope.edt.sims_appl_form_field_value4,
                            opr: 'I'
                        });

                        data1.push(data);

                        $http.post(ENV.apiUrl + "api/common/ParameterMaster/updateInsertDeleteParameter", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Parameter Data Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal({ title: "Alert", text: "Parameter Data Already Exists", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/common/ParameterMaster/getParameterbyIndex").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.parameter_data = res.data;
                    $scope.totalItems = $scope.parameter_data.length;
                    $scope.todos = $scope.parameter_data;
                    $scope.makeTodos();
                    for (var i = 0; i < $scope.totalItems; i++) {
                        $scope.parameter_data[i].icon = "fa fa-plus-circle";
                    }
                });

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {
                if (isvalidate) {
                    console.log(oldvalue);
                    // var data = $scope.edt;
                    //data.opr = "U";

                    var data = ({
                        comn_appl_mod_code: $scope.edt.comn_appl_mod_code,
                        comn_application_code: $scope.edt.comn_application_code,
                        sims_appl_form_field: $scope.edt.sims_appl_form_field,
                        sims_appl_parameter: $scope.edt.sims_appl_parameter,
                        sims_appl_form_field_value1: $scope.edt.sims_appl_form_field_value1,
                        sims_appl_form_field_value2: $scope.edt.sims_appl_form_field_value2,
                        sims_appl_form_field_value3: $scope.edt.sims_appl_form_field_value3,
                        sims_appl_form_field_value4: $scope.edt.sims_appl_form_field_value4,
                        appl_form_field: $scope.edt.appl_form_field,
                        appl_form_field_value1: $scope.edt.appl_form_field_value1,
                        appl_parameter: $scope.edt.appl_parameter,
                        opr: 'U'
                    });
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/common/ParameterMaster/updateParameterMaster", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Parameter Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal({ title: "Alert", text: "Parameter Not Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                    });
                }
            }

            $scope.Delete = function () {
                console.log($scope.del1);
                var data = $scope.del1;
                data.opr = "D";

                $http.post(ENV.apiUrl + "api/common/ParameterMaster/updateInsertDeleteParameter", data).then(function (res) {
                    $scope.msg1 = res.data;
                    $rootScope.strMessage = $scope.msg1.strMessage;
                    console.log($scope.msg1);
                    $('#message').modal('show');

                    $http.get(ENV.apiUrl + "api/common/ParameterMaster/getParameterbyIndex").then(function (res) {
                        $scope.display = false;
                        $scope.grid = true;
                        $scope.parameter_data = res.data;
                    });
                })
                // }
            }

            var dom;
            $scope.flag = true;
            $scope.icon = "fa fa-plus-circle";

            $scope.expand = function (info, $event) {
                console.log(info);
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                        "<tr> <td class='semi-bold'>" + gettextCatalog.getString('Form Field Value 1') + "</td> <td class='semi-bold'>" + gettextCatalog.getString('Form Field Value 2') + " </td><td class='semi-bold'>" + gettextCatalog.getString('Form Field Value 3') + "</td><td class='semi-bold'>" + gettextCatalog.getString('Form Field Value 4') + "</td>" +
                            "</tr>" +
                              "<tr><td>" + (info.sims_appl_form_field_value1) + "</td> <td>" + (info.sims_appl_form_field_value2) + " </td><td>" + (info.sims_appl_form_field_value3) + "</td><td>" + (info.sims_appl_form_field_value4) + "</td>" +
                            "</tr>" +

                        "</tbody>" +
                        " </table></td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    info.icon = "fa fa-plus-circle";
                    $scope.flag = true;
                }
            };


            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.parameter_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.parameter_data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_mod_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_appl_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  || item.sims_appl_form_field.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

        }])
})();