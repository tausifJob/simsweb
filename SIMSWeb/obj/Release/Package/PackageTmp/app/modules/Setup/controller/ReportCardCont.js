﻿(function () {
    'use strict';
    var citycode = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    var data1 = [];
    simsController.controller('ReportCardCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
          
            debugger
            var s
        
            if ($http.defaults.headers.common['schoolId'] == 'abps')

                s = "SimsReports." + $rootScope.location + ",SimsReportsAbps";



            else if ($http.defaults.headers.common['schoolId'] == 'dpsmis')
                s = "SimsReports." + $rootScope.location + ",SimsReports";

            else if ($http.defaults.headers.common['schoolId'] == 'ajn')
                s = "SimsReports." + $rootScope.location + ",SimsReportsAjn";

            else if ($http.defaults.headers.common['schoolId'] == 'kkn')
                s = "SimsReports." + $rootScope.location + ",SimsReportsKkn";

            else if ($http.defaults.headers.common['schoolId'] == 'bluestar')
                s = "SimsReports." + $rootScope.location + ",SimsReportsBluestar";

            else if ($http.defaults.headers.common['schoolId'] == 'ihis')
                s = "SimsReports." + $rootScope.location + ",SimsReportsIhis";


            else if ($http.defaults.headers.common['schoolId'] == 'egn')
                s = "SimsReports." + $rootScope.location + ",SimsReportsEgn";

            else if ($http.defaults.headers.common['schoolId'] == 'demo')
                s = "SimsReports." + $rootScope.location + ",SimsReportsDemo";

            else if ($http.defaults.headers.common['schoolId'] == 'tsis')
                s = "SimsReports." + $rootScope.location + ",SimsReportsTsis";

           else if ($http.defaults.headers.common['schoolId'] == 'qfis')
               s = "SimsReports." + $rootScope.location + ",SimsReportsQfis";

           else if ($http.defaults.headers.common['schoolId'] == 'sisqatar')
               s = "SimsReports." + $rootScope.location + ",SimsReportsSisqatar";

           else if ($http.defaults.headers.common['schoolId'] == 'eiama')
               s = "SimsReports." + $rootScope.location + ",SimsReportsEiama";

           else if ($http.defaults.headers.common['schoolId'] == 'eiagps')
               s = "SimsReports." + $rootScope.location + ",SimsReportsEiagps";

           else if ($http.defaults.headers.common['schoolId'] == 'eiand')
               s = "SimsReports." + $rootScope.location + ",SimsReportsEiand";

           else if ($http.defaults.headers.common['schoolId'] == 'eiam')
               s = "SimsReports." + $rootScope.location + ",SimsReportsEiam";

           else if ($http.defaults.headers.common['schoolId'] == 'zps')
               s = "SimsReports." + $rootScope.location + ",SimsReportsZps";

           else if ($http.defaults.headers.common['schoolId'] == 'sms2')
               s = "SimsReports." + $rootScope.location + ",SimsReportsSms2";

           else if ($http.defaults.headers.common['schoolId'] == 'sms')
               s = "SimsReports." + $rootScope.location + ",SimsReportsSms2";

           else if ($http.defaults.headers.common['schoolId'] == 'imert')
               s = "SimsReports." + $rootScope.location + ",SimsReportsImert";



           else if ($http.defaults.headers.common['schoolId'] == 'amps')
               s = "SimsReports." + $rootScope.location + ",SimsReportsAmps";

           else if ($http.defaults.headers.common['schoolId'] == 'azps')
               s = "SimsReports." + $rootScope.location + ",SimsReportsAzps";


           else if ($http.defaults.headers.common['schoolId'] == 'brs')
               s = "SimsReports." + $rootScope.location + ",SimsReportsBrs";


           else if ($http.defaults.headers.common['schoolId'] == 'etaaltwar')
               s = "SimsReports." + $rootScope.location + ",SimsReportsEta";


           else if ($http.defaults.headers.common['schoolId'] == 'deps')
               s = "SimsReports." + $rootScope.location + ",SimsReportsDeps";


         
          else if ($http.defaults.headers.common['schoolId'] == 'ajb')
              s = "SimsReports." + $rootScope.location + ",SimsReportsAjb";

          else if ($http.defaults.headers.common['schoolId'] == 'aji')
              s = "SimsReports." + $rootScope.location + ",SimsReportsAji";

          else if ($http.defaults.headers.common['schoolId'] == 'adisw')
              s = "SimsReports." + $rootScope.location + ",SimsReportsAdisw";

          else if ($http.defaults.headers.common['schoolId'] == 'adis')
              s = "SimsReports." + $rootScope.location + ",SimsReportsAdis";

          else if ($http.defaults.headers.common['schoolId'] == 'oes')
              s = "SimsReports." + $rootScope.location + ",SimsReportsOes";

          else if ($http.defaults.headers.common['schoolId'] == 'sn')
              s = "SimsReports." + $rootScope.location + ",SimsReportsSn";

            else if($http.defaults.headers.common['schoolId'] == 'sis')
                s = "SimsReports." + $rootScope.location + ",SimsReportsSis";
          
            else if ($http.defaults.headers.common['schoolId'] == 'nis')
                s = "SimsReports." + $rootScope.location + ",SimsReportsNis";

            else if ($http.defaults.headers.common['schoolId'] == 'pearl')
                s = "SimsReports." + $rootScope.location + ",SimsReportsPearl";

            else if ($http.defaults.headers.common['schoolId'] == 'mhm')
                s = "SimsReports." + $rootScope.location + ",SimsReportsMhm";

            else if ($http.defaults.headers.common['schoolId'] == 'gsis')
                s = "SimsReports." + $rootScope.location + ",SimsReportsGsis";

            else if ($http.defaults.headers.common['schoolId'] == 'sbis')
                s = "SimsReports." + $rootScope.location + ",SimsReportsSbis";

            else if ($http.defaults.headers.common['schoolId'] == 'misqatar')
                s = "SimsReports." + $rootScope.location + ",SimsReportsMisqatar";


            $("#reportViewer1")
                           .telerik_ReportViewer({
                               serviceUrl: ENV.apiUrl + "api/reports/",
                               viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                               scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                               // Zoom in and out the report using the scale
                               // 1.0 is equal to 100%, i.e. the original size of the report
                               scale: 1.0,
                               ready: function () {
                                   //this.refreshReport();
                               },
                               reportSource: {
                                   report:s ,
                                   //parameters: {
                                   //    //Date: new Date(),
                                   //    acad_year: '2016',
                                   //    cur_code:'01',
                                   //    grade_code: '07',
                                   //    section_code: '24',

                                   //    serch_student: '59401'
                                   //},
                               } 
                           });


       


            var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
            reportViewer.reportSource({
                report:s ,
                //parameters: {
                //    //Date: new Date(),
                //    acad_year: '2016',
                //    cur_code:'01',
                //    grade_code: '07',
                //    section_code: '24',

                //    serch_student: '59401'
                //},
            });

            setInterval(function () {

                $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                //$('.list k-widget k-listview').css({ 'style': 'height: 100px' })
                $('.list').attr('style', 'max-height: 150px;overflow-y: auto;overflow-x: hidden;');

                //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });

            }, 100);
           

            $timeout(function () {
                
                $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                $('.list').attr('style', 'max-height: 150px;overflow-y: auto;overflow-x: hidden;');
                
                //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });


            }, 100)
          


        }])
})();