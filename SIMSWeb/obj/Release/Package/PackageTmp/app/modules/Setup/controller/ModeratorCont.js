﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ModeratorCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.grid = true;
            $scope.edit_data = false;
            $scope.moderator_data = [];
            var data1 = [];
            var deletecode = [];

            $timeout(function () {
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $http.get(ENV.apiUrl + "api/common/Moderator/getModerator").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.moderator_data = res.data;
                $scope.totalItems = $scope.moderator_data.length;
                $scope.todos = $scope.moderator_data;
                $scope.makeTodos();
                $scope.grid = true;


            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edit_data = true;
                $scope.edt =
                    {
                        sims_attendance_cur_code: str.sims_attendance_cur_code,
                        sims_academic_year: str.sims_academic_year,
                        sims_comm_code: str.sims_comm_code,
                        sims_comm_code_name: str.sims_comm_code_name,
                        sims_email_flag: str.sims_email_flag,
                        sims_sms_flag: str.sims_sms_flag,
                        sims_bcc_flag: str.sims_bcc_flag,
                        sims_bcc_email_flag: str.sims_bcc_email_flag,
                        sims_bcc_sms_flag: str.sims_bcc_sms_flag,
                        sims_comm_status: str.sims_comm_status,
                    }
                $scope.GetAacd_yr($scope.edt.sims_attendance_cur_code);
            }

            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.cur_data = res.data;
                console.log($scope.cur_data);
            });

            $scope.GetAacd_yr = function (cur) {
                if (cur != undefined) {
                    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur).then(function (res) {
                        $scope.acad = res.data;
                        console.log($scope.acad);
                    });
                }
            }

            $scope.New = function () {
                var autoid;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edit_data = false;
                $scope.edt = [];
                $http.get(ENV.apiUrl + "api/common/Moderator/getAutoGenerateModeratorCommCode").then(function (res) {
                    autoid = res.data;
                    $scope.edt['sims_comm_code'] = autoid;
                });



            }

            $scope.Save = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = ({

                            sims_attendance_cur_code: $scope.edt.sims_attendance_cur_code,
                            sims_academic_year: $scope.edt.sims_academic_year,
                            sims_comm_code: $scope.edt.sims_comm_code,
                            sims_comm_code_name: $scope.edt.sims_comm_code_name,
                            sims_email_flag: $scope.edt.sims_email_flag,
                            sims_sms_flag: $scope.edt.sims_sms_flag,
                            sims_bcc_flag: $scope.edt.sims_bcc_flag,
                            sims_bcc_email_flag: $scope.edt.sims_bcc_email_flag,
                            sims_bcc_sms_flag: $scope.edt.sims_bcc_sms_flag,
                            sims_comm_status: $scope.edt.sims_comm_status,
                            opr: 'I'
                        });

                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/common/Moderator/CUDModerator", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            console.log($scope.msg1);
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Modeartor Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal({ title: "Alert", text: "Modeartor Not Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/common/Moderator/getModerator").then(function (res) {
                    $scope.display = false;
                    $scope.moderator_data = res.data;
                    $scope.totalItems = $scope.moderator_data.length;
                    $scope.todos = $scope.moderator_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }


            $scope.Update = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    var data = ({

                        sims_attendance_cur_code: $scope.edt.sims_attendance_cur_code,
                        sims_academic_year: $scope.edt.sims_academic_year,
                        sims_comm_code: $scope.edt.sims_comm_code,
                        sims_comm_code_name: $scope.edt.sims_comm_code_name,
                        sims_email_flag: $scope.edt.sims_email_flag,
                        sims_sms_flag: $scope.edt.sims_sms_flag,
                        sims_bcc_flag: $scope.edt.sims_bcc_flag,
                        sims_bcc_email_flag: $scope.edt.sims_bcc_email_flag,
                        sims_bcc_sms_flag: $scope.edt.sims_bcc_sms_flag,
                        sims_comm_status: $scope.edt.sims_comm_status,
                        opr: 'U'
                    });

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/common/Moderator/CUDModerator", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Modeartor Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });

                        }
                        else {
                            swal({ title: "Alert", text: "Modeartor Not Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                    });
                }
            }


            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Delete = function () {
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    var v = document.getElementById(i);

                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sims_attendance_cur_code': $scope.filteredTodos[i].sims_attendance_cur_code,
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            'sims_comm_code': $scope.filteredTodos[i].sims_comm_code,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {


                            $http.post(ENV.apiUrl + "api/common/Moderator/CUDModerator", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Modeartor Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Modeartor Not Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);

                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
            }


            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }


            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }


            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.moderator_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.moderator_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_comm_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_comm_code_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }


            var dom;
            $scope.expand = function (info, $event) {
                console.log(info);
                $(dom).remove();
                dom = $("<tr><td class='details' colspan='11'>" +
                    "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                    "<tbody>" +
                      "<tr><td class='semi-bold'>&nbsp;</td><td class='semi-bold'></td><td class='semi-bold'></td> <td class='semi-bold'>" + "Email" + "</td><td class='semi-bold'>&nbsp;</td> <td class='semi-bold'>" + "Sms" + " </td><td class='semi-bold'>&nbsp;</td><td class='semi-bold'>" + "Bcc" + "</td><td class='semi-bold'>&nbsp;</td><td class='semi-bold'>" + "Bcc Email" + "</td><td class='semi-bold'>&nbsp;</td><td class='semi-bold'>" + "Bcc Sms" + "</td>" +
                        "</tr>" +
                          "<tr><td class='semi-bold'>&nbsp;</td><td class='semi-bold'>&nbsp;</td><td class='semi-bold'>&nbsp;</td><td>" + (info.sims_email_flag) + "</td> <td class='semi-bold'>&nbsp;</td><td>" + (info.sims_sms_flag) + " </td><td class='semi-bold'>&nbsp;</td><td>" + (info.sims_bcc_flag) + "</td><td class='semi-bold'>&nbsp;</td><td>" + (info.sims_bcc_email_flag) + "</td><td class='semi-bold'>&nbsp;</td><td>" + (info.sims_bcc_sms_flag) + "</td>" +
                        "</tr>" +

                    "</tbody>" +
                    " </table></td></tr>")
                $($event.currentTarget).parents("tr").after(dom);
            };



        }])
})();