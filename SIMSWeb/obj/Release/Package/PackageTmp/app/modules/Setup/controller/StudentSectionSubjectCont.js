﻿(function () {
    'use strict';
    var appcode = [];
    var getdata = [];
    var senddata = [], demo = [], demo1 = [];
    var main;
    var simsController = angular.module('sims.module.Setup');
    simsController.controller('StudentSectionSubjectCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.edt = {};
            //$scope.edt['sims_subject_code'] = '';
            $scope.edt['sims_subject_code'] = [];
            $scope.simsArray = [];
            var user = $rootScope.globals.currentUser.username;
            $scope.busyindicator = true;
            $scope.propertyName = null;
            $scope.reverse = false;
            // propertyName = null; reverse = false
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            // Change the selector if needed
            var $table = $('table.scroll'),
                $bodyCells = $table.find('tbody tr:first').children(),
                colWidth;

            // Adjust the width of thead cells when window resizes
            $(window).resize(function () {
                // Get the tbody columns width array
                colWidth = $bodyCells.map(function () {
                    return $(this).width();
                }).get();

                // Set the width of thead columns
                $table.find('thead tr').children().each(function (i, v) {
                    $(v).width(colWidth[i]);
                });
            }).resize(); // Trigger resize handler


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;

                $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;

                $scope.getacyr($scope.curriculum[0].sims_cur_code);
            });

            $(function () {
                $('#cmb_grade').multipleSelect({
                    width: '100%'
                });
            });

            $scope.getacyr = function (str) {
                $scope.cur_code = str;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    $scope.edt['sims_academic_year'] = $scope.Academic_year[0].sims_academic_year;
                    $scope.getsections(str, $scope.Academic_year[0].sims_academic_year);
                })
            }

            $scope.getsection = function (str, str1, str2) {
                $http.get(ENV.apiUrl + "api/Gradebook/GetRCSection?curcode=" + str + "&ayear=" + str1 + "&gradecode=" + str2 + "&teachercode=" + user).then(function (Allsection) {
                    $scope.section1 = Allsection.data;

                })
            };

            $scope.getsections = function (str, str1) {
                $http.get(ENV.apiUrl + "api/Gradebook/GetRCGrade?curcode=" + str + "&ayear=" + str1 + "&teachercode=" + user).then(function (res) {
                    $scope.grade = res.data;


                })
            };

            $scope.get = function (section) {

                $http.get(ENV.apiUrl + "api/StudentSectionSubject/getSectionsubject_name?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&login_user=" + user).then(function (getSectionsubject_name) {
                    $scope.cmb_subject_name = getSectionsubject_name.data;
                    setTimeout(function () {
                        $('#cmb_grade').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }

            $scope.Show_Data = function () {

                $scope.busyindicator = false;
                $scope.info1 = [];
                var demo = [];
                var demo1 = [];
                $scope.secData = [];
                $scope.table = false;
                $scope.subject = false;
                $scope.showSelectedSubject = false;
                if ($scope.edt.sims_subject_code == undefined) {
                    // $scope.edt.sims_subject_code = '';
                    $scope.edt.sims_subject_code = '';
                }
                //$scope.edt.sims_subject_code = [];
                var sims_subject_code = "";
                for (var i = 0; i < $scope.edt.sims_subject_code.length; i++) {
                    sims_subject_code = sims_subject_code + $scope.edt.sims_subject_code[i] + ',';
                }

                $http.get(ENV.apiUrl + "api/StudentSectionSubject/getStudentName?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&subcode=" + sims_subject_code).then(function (allSectionStudent) {
                    $scope.SectionStudent = allSectionStudent.data;
                    $scope.busyindicator = true;

                    if (allSectionStudent.data.length > 0) {
                        //for (var j = 0; j < $scope.SectionStudent[0].sublist.length; j++) {                                                           
                        //    demo = {
                        //        'sims_subject_name_en': $scope.SectionStudent[0].sublist[j].sims_subject_name_en,
                        //        'sims_subject_code': $scope.SectionStudent[0].sublist[j].sims_subject_code
                        //    }
                        //    demo1.push(demo);                                                                              
                        //}
                        for (var j = 0; j < $scope.SectionStudent[0].sublist.length; j++) {
                            for (var i = 0; i < $scope.edt.sims_subject_code.length; i++) {
                                console.log($scope.edt.sims_subject_code[i]);
                                if ($scope.SectionStudent[0].sublist[j].sims_subject_code == $scope.edt.sims_subject_code[i]) {

                                    demo = {
                                        'sims_subject_name_en': $scope.SectionStudent[0].sublist[j].sims_subject_name_en,
                                        'sims_subject_code': $scope.SectionStudent[0].sublist[j].sims_subject_code
                                    }
                                    demo1.push(demo);

                                }
                            }
                        }

                        for (var k = 0; k < $scope.SectionStudent.length; k++) {

                            for (var j = 0; j < $scope.SectionStudent[k].sublist.length; j++) {
                                $scope.flg = false
                                for (var i = 0; i < $scope.edt.sims_subject_code.length; i++) {
                                    if ($scope.edt.sims_subject_code[i] == $scope.SectionStudent[k].sublist[j].sims_subject_code) {
                                        $scope.flg = true;
                                        break;
                                    }
                                }
                                $scope.SectionStudent[k].sublist[j]['is_visible'] = $scope.flg;

                            }
                        }

                        $scope.info1 = demo1;
                        $timeout(function () {
                            $("#fixTable").tableHeadFixer({ 'top': 1 });
                        }, 100);

                        $scope.table = true;

                    }
                    else {
                        $scope.subject = true;
                    }
                });

            }

            var subject1;
            $scope.skipdSubject = [];

            $scope.Check_Single_Subject = function (enroll, subject) {
                console.log(subject, enroll);
                enroll.ischange = true;
                if (subject.sims_status == false) {
                    $http.get(ENV.apiUrl + "api/StudentSectionSubject/getcheckSectionsubject_name?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&enroll_number=" + enroll.sims_student_enroll_number + "&subject_code=" + subject.sims_subject_code).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1.length > 0) {
                            swal({
                                text: 'This Subject Having Mark In Gradebook ,Are You Want Be Remove Subject?',
                                showCloseButton: true,
                                showCancelButton: true,
                                confirmButtonText: 'Yes',
                                width: 380,
                                cancelButtonText: 'No',
                            }).then(function (isConfirm) {
                                if (isConfirm) {
                                    subject.sims_status = false;
                                }
                                else {
                                    subject.sims_status = true;
                                    var v = document.getElementById(subject.sims_subject_code + enroll.sims_student_enroll_number);
                                    v.checked = true;
                                }
                            })

                        }
                    })
                }
            }

            //$scope.Select_ALL_Subject = function (str) {
            //    debugger
            //    var id = '';
            //    var subject_code = '';
            //    var student_enroll = ''
            //    $scope.ischecked = '';
            //    var check = document.getElementById(str);
            //    $scope.ischecked = str;
            //    $scope.tem = {};
            //    if (check.checked == false) {

            //        for (var i = 0; i < $scope.SectionStudent.length; i++) {
            //            student_enroll = student_enroll + $scope.SectionStudent[i].sims_student_enroll_number + ',';
            //            subject_code = str;
            //        }
            //        $http.get(ENV.apiUrl + "api/StudentSectionSubject/getcheckSectionsubject_name?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&enroll_number=" + student_enroll + "&subject_code=" + subject_code).then(function (msg) {
            //            $scope.msg1 = msg.data;
            //            if ($scope.msg1.length > 0) {
            //                $('#MyModal').modal({ backdrop: "static" });
            //            } else {

            //                for (var i = 0; i < $scope.SectionStudent.length; i++) {
            //                    $scope.SectionStudent[i].sublist[check.tabIndex].sims_status = check.checked;
            //                    $scope.SectionStudent[i].ischange = true;
            //                }
            //            }
            //        })
            //    }
            //    else {
            //        for (var i = 0; i < $scope.SectionStudent.length; i++) {
            //            $scope.SectionStudent[i].sublist[check.tabIndex].sims_status = check.checked;
            //            $scope.SectionStudent[i].ischange = true;
            //        }
            //    }
            //}



            $scope.Select_ALL_Subject = function (str, obj, index, chk) {
                debugger;
                var check = document.getElementById(str);
                var subject_code = '';
                var student_enroll = ''
                $scope.ischecked = '';

                for (var i = 0; i < $scope.SectionStudent.length; i++) {
                    for (var j = 0; j < $scope.SectionStudent[i].sublist.length; j++) {
                        if ($scope.SectionStudent[i].sublist[j].sims_subject_code == obj.sims_subject_code)
                            $scope.SectionStudent[i].sublist[j]['sims_status'] = chk;
                        $scope.SectionStudent[i]['ischange'] = chk;
                    }
                }
                if (chk == false) {
                    for (var i = 0; i < $scope.SectionStudent.length; i++) {
                        student_enroll = student_enroll + $scope.SectionStudent[i].sims_student_enroll_number + ',';
                        subject_code = str;
                    }
                    $http.get(ENV.apiUrl + "api/StudentSectionSubject/getcheckSectionsubject_name?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&enroll_number=" + student_enroll + "&subject_code=" + subject_code).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1.length > 0) {
                            $('#MyModal').modal({ backdrop: "static" });
                        } else {

                            for (var i = 0; i < $scope.SectionStudent.length; i++) {
                                $scope.SectionStudent[i].sublist[check.tabIndex].sims_status = chk;
                                $scope.SectionStudent[i].ischange = true;
                            }
                        }
                    })
                }
            }

            $scope.btn_Ok_Click = function () {

                var check = document.getElementById($scope.ischecked);
                for (var i = 0; i < $scope.SectionStudent.length; i++) {
                    $scope.SectionStudent[i].sublist[check.tabIndex].sims_status = false;
                    $scope.SectionStudent[i].ischange = true;
                }
            }

            $scope.btn_cancel_Click = function () {

                var check = document.getElementById($scope.ischecked);
                for (var i = 0; i < $scope.SectionStudent.length; i++) {
                    $scope.SectionStudent[i].sublist[check.tabIndex].sims_status = false;
                    $scope.SectionStudent[i].ischange = true;
                }

                for (var i = 0; i < $scope.SectionStudent.length; i++) {
                    var check = document.getElementById($scope.ischecked);
                    for (var j = 0; j < $scope.msg1.length; j++) {
                        if ($scope.SectionStudent[i].sims_student_enroll_number == $scope.msg1[j].sims_student_enroll_number) {
                            $scope.SectionStudent[i].sublist[check.tabIndex].sims_status = true;
                            $scope.SectionStudent[i].ischange = true;
                        }

                    }
                }

            }

            $scope.Select_Student_ALL_Subject = function (str) {
                var id = '';
                var check = document.getElementById(str);
                for (var i = 0; i < $scope.SectionStudent.length; i++) {
                    if ($scope.SectionStudent[i].sims_student_enroll_number == str) {
                        for (var j = 0; j < $scope.SectionStudent[i].sublist.length; j++) {
                            $scope.SectionStudent[i].sublist[j].sims_status = check.checked;
                            $scope.SectionStudent[i].ischange = true;
                        }
                    }
                }
            }

            $scope.Submit = function () {
                //$scope.busyindicator = true;
                //$scope.table = true;
                $scope.subject = false;
                getdata = [];
                var Sdata = [{}];
                for (var i = 0; i < $scope.SectionStudent.length; i++) {

                    if ($scope.SectionStudent[i].ischange == true) {
                        var le = Sdata.length;
                        Sdata[le] = {
                            'sims_student_enroll_number': $scope.SectionStudent[i].sims_student_enroll_number,
                            'sims_cur_code': $scope.SectionStudent[i].sims_cur_code,
                            'sims_academic_year': $scope.SectionStudent[i].sims_academic_year,
                            'sims_grade_code': $scope.SectionStudent[i].sims_grade_code,
                            'sims_section_code': $scope.SectionStudent[i].sims_section_code,
                            'sims_subject_name_en': ''
                        };
                        var sbcode = '';
                        var sbcode1 = '';
                        for (var j = 0; j < $scope.SectionStudent[i].sublist.length; j++) {
                            debugger
                            if ($scope.SectionStudent[i].sublist[j].sims_status == true) {
                                sbcode = sbcode + $scope.SectionStudent[i].sublist[j].sims_subject_code + ',';
                            }
                            else {
                                //sbcode1 = sbcode1 + $scope.SectionStudent[i].sublist[j].sims_subject_code + ',';
                                sbcode1 = sbcode1 + $scope.SectionStudent[i].sublist[j].sims_subject_code + ',';
                            }
                        }

                        Sdata[le].sims_subject_name_en = sbcode;
                        Sdata[le].sims_subject_code = sbcode1;
                    }
                }

                Sdata.splice(0, 1);
                var data = Sdata;
                if (Sdata.length != 0) {
                    $http.post(ENV.apiUrl + "api/StudentSectionSubject/insert_student_section_subject", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        Sdata = [{}];
                        data = [];
                        swal({ text: $scope.msg1.strMessage, width: 380 });
                        //$scope.busyindicator = false;
                        $http.get(ENV.apiUrl + "api/StudentSectionSubject/getStudentName?curcode=" + $scope.cur_code + "&gradecode=" + $scope.grade_code2 + "&academicyear=" + $scope.year + "&section=" + $scope.section).then(function (allSectionStudent) {
                            //$scope.table = false;
                            $scope.SectionStudent = allSectionStudent.data;
                            demo1 = [];
                            if (allSectionStudent.data.length > 0) {
                                //for (var j = 0; j < $scope.SectionStudent[0].sublist.length; j++) {
                                //    demo = {
                                //        'sims_subject_name_en': $scope.SectionStudent[0].sublist[j].sims_subject_name_en,
                                //        'sims_subject_code': $scope.SectionStudent[0].sublist[j].sims_subject_code
                                //    }
                                //    demo1.push(demo);
                                //}
                                for (var j = 0; j < $scope.SectionStudent[0].sublist.length; j++) {
                                    for (var i = 0; i < $scope.edt.sims_subject_code.length; i++) {
                                        console.log($scope.edt.sims_subject_code[i]);
                                        if ($scope.SectionStudent[0].sublist[j].sims_subject_code == $scope.edt.sims_subject_code[i]) {
                                            demo = {
                                                'sims_subject_name_en': $scope.SectionStudent[0].sublist[j].sims_subject_name_en,
                                                'sims_subject_code': $scope.SectionStudent[0].sublist[j].sims_subject_code
                                            }
                                            demo1.push(demo);
                                        }
                                    }
                                }
                                $scope.info1 = demo1;
                                $timeout(function () {
                                    $("#fixTable").tableHeadFixer({ 'top': 1 });
                                }, 100);
                                //$scope.table = false;
                            }
                            else {
                                $scope.subject = true;
                            }
                        });
                    });
                    senddata = [];
                    //$scope.table = false;

                    // subject1.checked = false;
                }
                else {
                    swal({ text: $scope.msg1.strMessage, width: 380 });
                }
            }





        }]);

})();



