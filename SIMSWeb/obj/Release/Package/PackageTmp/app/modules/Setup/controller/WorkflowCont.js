﻿(function () {
    'use strict';
    var main = '';
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('WorkflowCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.removedEmpl = [];
            $scope.display = false;
            $scope.busyindicator = false;
            $scope.grid = true;
            $scope.color = '#edefef';
            $scope.rgvtbl = false;
            $scope.btnhide = true;
            $scope.Employee_detials = [];
            $scope.edt = '';
            $scope.firstview = true;
            $scope.secondview = false;
            


            $http.get(ENV.apiUrl + "api/Workflow/GetMod").then(function (GetMod) {
                $scope.GetMod = GetMod.data;
            });

            $http.get(ENV.apiUrl + "api/Workflow/GetWorkFlowType").then(function (GetWorkFlowType) {
                $scope.GetWorkFlowType = GetWorkFlowType.data;
            });


            $scope.select_mod = function (str) {
                $http.get(ENV.apiUrl + "api/Workflow/GetAppl?mod_code=" + str).then(function (GetAppl) {
                    $scope.GetAppl = GetAppl.data;
                });
            }


            $scope.select_appl = function () {
                $http.get(ENV.apiUrl + "api/Workflow/GetDept").then(function (GetDept) {
                    $scope.GetDept = GetDept.data;
                });

            }

            $scope.select_dep = function (str) {
                $scope.Employee_detials = [];
                $scope.insertbtn = false;
                $scope.updatebtn = false;
                $http.get(ENV.apiUrl + "api/Workflow/GetEmpWorkflowDetails?mod_code=" + $scope.edt.sims_workflow_mod_code + "&appl_code=" + $scope.edt.sims_workflow_appl_code + "&dept_code=" + str).then(function (GetEmpWorkflowDetails) {
                    $scope.GetEmpWorkflowDetails = GetEmpWorkflowDetails.data;
                    console.log($scope.GetEmpWorkflowDetails);
                    debugger;
                    $scope.edt.dept_code =str;
                    $scope.edt.sims_workflow_type = $scope.GetEmpWorkflowDetails[0].sims_workflow_type ;
                    $scope.edt.sims_worfflow_description=$scope.GetEmpWorkflowDetails[0].sims_worfflow_description;
                    for (var i = 0; i < $scope.GetEmpWorkflowDetails.length; i++) {

                        var p = {
                            sims_workflow_det_empno: $scope.GetEmpWorkflowDetails[i].sims_workflow_det_empno,
                            sims_workflow_employee_name: $scope.GetEmpWorkflowDetails[i].sims_workflow_employee_name,
                            sims_workflow_type: $scope.GetEmpWorkflowDetails[i].sims_workflow_type,
                            sims_worfflow_description: $scope.GetEmpWorkflowDetails[i].sims_worfflow_description,
                            sims_workflow: $scope.GetEmpWorkflowDetails[i].sims_workflow,
                            sims_workflow_srno: $scope.GetEmpWorkflowDetails[i].sims_workflow_srno,
                            sims_workflow_employee_code: $scope.GetEmpWorkflowDetails[i].sims_workflow_employee_code,
                            sims_workflow_order: $scope.GetEmpWorkflowDetails[i].sims_workflow_order,
                            sims_workflow_effectivity: $scope.GetEmpWorkflowDetails[i].sims_workflow_effectivity
                        }
                        $scope.Employee_detials.push(p);
                        
                    }
                    if ($scope.Employee_detials.length > 0) {
                        $scope.rgvtbl = true;
                        $scope.insertbtn = false;
                        $scope.updatebtn = true;
                    }
                    else {
                        $scope.insertbtn = false;
                        $scope.updatebtn = false;
                    }

                });
              
                if ($scope.Employee_detials.length == 0) {
                    
                    $scope.edt.sims_workflow_type = '';
                    $scope.edt.sims_worfflow_description = '';
                }
            }

            $scope.Add_employee = function () {
                $scope.GetEmployee = [];
                
                $http.get(ENV.apiUrl + "api/Workflow/GetDesg").then(function (GetDesg) {
                    $scope.GetDesg = GetDesg.data;
                    $scope.edt.dg_code = '';
                    $scope.edt.emp_code = '';
                });
                $scope.firstview = false;
                $scope.secondview = true;

            }

            $scope.Show_employee = function () {

                $scope.busy = true;
                $http.get(ENV.apiUrl + "api/Workflow/GetEmployee?empid=" + $scope.edt.emp_code + "&desg=" + $scope.edt.dg_code).then(function (GetEmployee) {
                    $scope.GetEmployee = GetEmployee.data;
                    $scope.busy = false;
                    $scope.rgvtbl1 = true;
                });
                $scope.busy = false;

            }

            $scope.Save_all = function () {
                debugger;
                if ($scope.edt.sims_workflow_mod_code == undefined || $scope.edt.sims_workflow_appl_code == undefined || $scope.edt.dept_code == undefined || $scope.edt.sims_workflow_type == undefined
                    || $scope.edt.sims_workflow_mod_code == '' || $scope.edt.sims_workflow_appl_code == '' || $scope.edt.dept_code == '' || $scope.edt.sims_workflow_type == '')
                {
                    swal('', 'All fields are mandatory.');
                }
                else
                {
                    var d = {
                        sims_worfflow_description: $scope.edt.sims_worfflow_description,
                        sims_workflow_mod_code: $scope.edt.sims_workflow_mod_code,
                        sims_workflow_appl_code: $scope.edt.sims_workflow_appl_code,
                        sims_workflow_type: $scope.edt.sims_workflow_type,
                        dept_code: $scope.edt.dept_code,
                    }
                    console.log(d);
                    debugger;
                    $http.post(ENV.apiUrl + "api/Workflow/InsertSimsWorkflow", d).then(function (msg1) {
                        $scope.msg = msg1.data;
                        if ($scope.msg) {
                            var datall = [];
                            for (var i = 0; i < $scope.Employee_detials.length; i++) {
                                var v = {
                                    sims_workflow_employee_code: $scope.Employee_detials[i].sims_workflow_employee_code,
                                    sims_workflow_order: $scope.Employee_detials[i].sims_workflow_order,
                                    sims_workflow_effectivity: $scope.Employee_detials[i].sims_workflow_effectivity,
                                    sims_workflow_surpassable: $scope.Employee_detials[i].sims_workflow_surpassable
                                }
                                datall.push(v);
                            }
                        }

                        $http.post(ENV.apiUrl + "api/Workflow/InsertSimsWorkflowemp", datall).then(function (msg2) {
                            $scope.msg2 = msg2.data;
                            if ($scope.msg2) {
                                swal('', 'Record Inserted Successfully.');
                            }
                            $scope.reset_form();
                        });
                    });
                }

                
            }

            $scope.Update_all = function () {
                if ($scope.edt.sims_workflow_mod_code == undefined || $scope.edt.sims_workflow_appl_code == undefined || $scope.edt.dept_code == undefined || $scope.edt.sims_workflow_type == undefined
                    || $scope.edt.sims_workflow_mod_code == '' || $scope.edt.sims_workflow_appl_code == '' || $scope.edt.dept_code == '' || $scope.edt.sims_workflow_type == '') {
                    swal('', 'All fields are mandatory.');
                }
                else {
                    var m = {
                        sims_workflow: $scope.GetEmpWorkflowDetails[0].sims_workflow,
                        sims_worfflow_description: $scope.edt.sims_worfflow_description,
                        sims_workflow_type: $scope.edt.sims_workflow_type,
                        sims_workflow_srno: $scope.GetEmpWorkflowDetails[0].sims_workflow_srno
                    }



                    var srno = $scope.GetEmpWorkflowDetails[0].sims_workflow_srno;
                    $http.post(ENV.apiUrl + "api/Workflow/UpdateSimsWorkflow", m).then(function (ms) {
                        $scope.ms = ms.data;
                        var datav = [];

                        if ($scope.ms) {

                            for (var i = 0; i < $scope.Employee_detials.length; i++) {
                                var q = '';
                                if ($scope.Employee_detials[i].sims_workflow_det_empno == undefined)
                                    q = '';
                                else
                                    q = $scope.Employee_detials[i].sims_workflow_det_empno;
                                var t = {
                                    sims_workflow_det_empno: q,
                                    sims_workflow_srno: srno,
                                    sims_workflow_employee_code: $scope.Employee_detials[i].sims_workflow_employee_code,
                                    sims_workflow_order: $scope.Employee_detials[i].sims_workflow_order,
                                    sims_workflow_effectivity: $scope.Employee_detials[i].sims_workflow_effectivity,
                                    sims_workflow_surpassable: 'Y'
                                }
                                datav.push(t);
                            }
                            debugger;
                            var deleteemp = [];
                            if ($scope.removedEmpl.length > 0) {
                               
                                for (var i = 0; i < $scope.removedEmpl.length; i++) {
                                    var a = {
                                        sims_workflow_det_empno: $scope.removedEmpl[i].sims_workflow_det_empno,
                                        sims_workflow_employee_code: $scope.removedEmpl[i].sims_workflow_employee_code
                                    }
                                    deleteemp.push(a);
                                }


                                $http.post(ENV.apiUrl + "api/Workflow/UDeleteSimsWorkflowemp", deleteemp).then(function (ds) {
                                    $scope.ds = ds.data;
                                    $scope.removedEmpl = [];
                                });
                            }
                            $http.post(ENV.apiUrl + "api/Workflow/UpdateSimsWorkflowemp", datav).then(function (ms1) {
                                $scope.ms1 = ms1.data;
                                if ($scope.ms1)
                                    swal('', 'Record updated Successfully.');
                                $scope.reset_form();
                            });
                        }
                    });
                }

            }

            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.get_employeedetails = function () {

                $scope.firstview = true;
                $scope.secondview = false;

                var temp = [];
                for (var i = 0; i < $scope.GetEmployee.length; i++) {
                    if ($scope.GetEmployee[i].select_status == true) {

                        var data = {
                            sims_workflow_employee_code: $scope.GetEmployee[i].sims_workflow_employee_code,
                            sims_workflow_employee_name: $scope.GetEmployee[i].sims_workflow_employee_name
                            //sims_workflow_order: $scope.Employee_detials.length + 1,
                            //sims_workflow_effectivity:1,
                            //sims_workflow_surpassable:'Y',
                        }
                        temp.push(data);
                    }
                }
                debugger;
                if ($scope.Employee_detials.length > 0) {
                    var temp1 = [];
                    var temp1 = $scope.Employee_detials;

                    for (var i = 0; i < temp.length; i++) {

                        for (var j = 0; j < $scope.Employee_detials.length; j++) {
                            if ($scope.Employee_detials[j].sims_workflow_employee_code == temp[i].sims_workflow_employee_code) {

                                temp.splice(i, 1);

                            }
                        }
                    }

                    for (var t = 0; t < temp.length; t++) {
                        var data1 = {
                            sims_workflow_employee_code: temp[t].sims_workflow_employee_code,
                            sims_workflow_employee_name: temp[t].sims_workflow_employee_name,
                            sims_workflow_order: $scope.Employee_detials.length + 1,
                            sims_workflow_effectivity: 1,
                            sims_workflow_surpassable: 'Y',
                        }

                        $scope.Employee_detials.push(data1);
                    }




                }
                else {
                    for (var i = 0; i < $scope.GetEmployee.length; i++) {
                        if ($scope.GetEmployee[i].select_status == true) {

                            var data = {
                                sims_workflow_employee_code: $scope.GetEmployee[i].sims_workflow_employee_code,
                                sims_workflow_employee_name: $scope.GetEmployee[i].sims_workflow_employee_name,
                                sims_workflow_order: $scope.Employee_detials.length + 1,
                                sims_workflow_effectivity: 1,
                                sims_workflow_surpassable: 'Y',
                            }
                            $scope.Employee_detials.push(data);
                        }
                    }
                }
                debugger;
                if ($scope.Employee_detials.length > 0) {
                    $scope.rgvtbl = true;
                    if ($scope.updatebtn == true)
                        $scope.insertbtn = false;
                    else
                        $scope.insertbtn = true;
                }


              
              


            }

            $scope.close_employee = function () {
                $scope.firstview = true;
                $scope.secondview = false;
            }

            $scope.changeno = function (obj) {

                console.log(obj)
                $scope.maindata = obj;

                if ($scope.maindata.sims_workflow_effectivity >= 365 || $scope.maindata.sims_workflow_effectivity <= 0) {
                    swal('', 'Please enter 0 to 365 days.');
                    $scope.maindata.sims_workflow_effectivity = 1;
                }

            }

            $scope.onlyNumbers = function (event, leave_max) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }

                }

                event.preventDefault();


            };

            $scope.reset_form = function () {
                $scope.edt = '';
                $scope.Employee_detials = [];
                $scope.insertbtn = false;
                $scope.updatebtn = false;
                $scope.rgvtbl = false;
                $scope.rgvtbl1 = false;
                $scope.removedEmpl = [];

            }

  
            $scope.removedEmpl = [];

            $scope.deletedoc = function (sp) {

                debugger;
                for (var j = 0; j < $scope.Employee_detials.length; j++) {
                    if ($scope.Employee_detials[j].sims_workflow_employee_code == sp) {
                        if ($scope.Employee_detials[j].sims_workflow_det_empno != undefined || $scope.Employee_detials[j].sims_workflow_det_empno != '') {
                            $scope.removedEmpl.push($scope.Employee_detials[j]);
                        }
                        $scope.Employee_detials.splice(j, 1);
                    }
                }
                var temp = [];
                temp = $scope.Employee_detials;
                $scope.Employee_detials = [];
                for (var i = 0; i < temp.length; i++) {
                    var c;
                    if (temp[i].sims_workflow == undefined) {
                        c = {
                            sims_workflow_effectivity: temp[i].sims_workflow_effectivity
                            , sims_workflow_employee_code: temp[i].sims_workflow_employee_code
                        , sims_workflow_employee_name: temp[i].sims_workflow_employee_name
                        , sims_workflow_order: i + 1
                        , sims_workflow_surpassable: temp[i].sims_workflow_surpassable
                        }
                    }
                    else {
                        c = {
                            sims_worfflow_description: temp[i].sims_worfflow_description
                       , sims_workflow: temp[i].sims_workflow
                       , sims_workflow_det_empno: temp[i].sims_workflow_det_empno
                       , sims_workflow_effectivity: temp[i].sims_workflow_effectivity
                       , sims_workflow_employee_code: temp[i].sims_workflow_employee_code
                       , sims_workflow_employee_name: temp[i].sims_workflow_employee_name
                       , sims_workflow_order: i + 1
                       , sims_workflow_srno: temp[i].sims_workflow_srno
                       , sims_workflow_type: temp[i].sims_workflow_type
                        }
                    }
                    $scope.Employee_detials.push(c);
                }


                console.log($scope.Employee_detials);
            }





        }])
})();