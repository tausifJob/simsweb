﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    var main, del = [];
    simsController.controller('CurriculumLevelController',
       ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', 'filterFilter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout,
           gettextCatalog, filterFilter, $http, ENV, $filter) {
           $scope.display = false;
           $scope.pagesize = "5";
           $scope.pageindex = "1";

           $scope.levelcodeRonly = false;
           $http.get(ENV.apiUrl + "api/common/CurLevel/getCurlevel").then(function (res) {
               $scope.display = false;
               $scope.grid = true;
               $scope.obj = res.data;
               console.log($scope.obj);
               $scope.totalItems = $scope.obj.length;
               $scope.todos = $scope.obj;
               console.log($scope.totalItems);
               $scope.makeTodos();
           });
           $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

           $scope.makeTodos = function () {

               var rem = parseInt($scope.totalItems % $scope.numPerPage);
               if (rem == '0') {
                   $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
               }
               else {
                   $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
               }


               var begin = (($scope.currentPage - 1) * $scope.numPerPage);
               var end = parseInt(begin) + parseInt($scope.numPerPage);

               console.log("begin=" + begin); console.log("end=" + end);

               $scope.filteredTodos = $scope.todos.slice(begin, end);
           };
           $http.get(ENV.apiUrl + "api/common/CurLevel/getCuriculum").then(function (res) {
               $scope.cur = res.data;
           });

           $scope.CheckAll = function () {
               main = document.getElementById('chk_min');
               del = [];
               if (main.checked == true) {
                   // $scope.obj1 = res.data;
                   for (var i = 0; i < $scope.filteredTodos.length; i++) {
                       var t = $scope.filteredTodos[i].level_code;
                       var v = document.getElementById(t);
                       v.checked = true;
                       //  debugger;
                       // $scope.deleteobj[i].nationality_code = t;
                       del.push(t);
                   }
               }
               else {
                   // $scope.obj1 = res.data;
                   for (var i = 0; i < $scope.filteredTodos.length; i++) {
                       var t = $scope.filteredTodos[i].level_code;
                       var v = document.getElementById(t);
                       v.checked = false;
                   }
               }
               console.log(del);
           }

           $scope.CheckAny = function (level_code) {

               var v = document.getElementById(level_code);
               if (v.checked == true) {
                   console.log(level_code);
                   del.push(level_code);
                   console.log(del);
               }
               else {
                   console.log(level_code);
                   v.checked = false;

                   var index = del.indexOf(level_code);
                   console.log(index);

                   if (index > -1) {
                       del.splice(index, 1);
                   }
                   //del.pop({id:sims_concession_number});
                   console.log(del);
               }

           }

           $scope.size = function (str) {
               console.log(str);
               $scope.pagesize = str;
               $scope.currentPage = 1;
               //main.checked = false;
               $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
           }

           $scope.index = function (str) {
               // $scope.pageindex = str;
               $scope.pageindex = str;
               //   main.checked = false;
               $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
           }

           $scope.searched = function (valLists, toSearch) {
               // debugger;
               return _.filter(valLists,

               function (i) {
                   /* Search Text in all  fields */
                   return searchUtil(i, toSearch);
               });
           };

           $scope.search = function () {
               $scope.todos = $scope.searched($scope.obj, $scope.searchText);
               $scope.totalItems = $scope.todos.length;
               $scope.currentPage = '1';
               if ($scope.searchText == '') {
                   $scope.todos = $scope.obj;
               }
               $scope.makeTodos();
           }

           function searchUtil(item, toSearch) {
               /* Search Text in all 3 fields */
               return (item.level_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.level_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                  item.level_cur_code_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.level_cur_code_name == toSearch) ? true : false;
           }

           $scope.edit = function (str) {
               $scope.levelcodeRonly = true;
               $scope.display = true; $scope.update_flag = true;
               $scope.grid = false; $scope.btn_save = false; $scope.btn_update = true;
              // $scope.temp = str;
               $scope.temp = {
                   level_cur_code: str.level_cur_code
                              , sims_academic_year: str.sims_academic_year
                              , level_code: str.level_code
                              , level_name_en: str.level_name_en
                              , level_name_fr: str.level_name_fr
                              , level_name_ar: str.level_name_ar
                              , level_name_ot: str.level_name_ot
                              , level_status: str.level_status

               };

           }

           $scope.Save = function (isvalid) {
               if (isvalid) {
                   if ($scope.update_flag == true) {
                       $scope.Update();
                   }
                   else {
                       var data = $scope.temp;
                       data.opr = 'I';
                       $http.post(ENV.apiUrl + "api/common/CurLevel/CUDCurLevel", data).then(function (res) {
                           $scope.msg = res.data;
                         
                           if ($scope.msg.strMessage == '0' || $scope.msg.strMessage == '3') {
                               $('#CurLevelCodeError').modal({ backdrop: "static" });
                           }
                           else {
                               $('#SucessMessage').modal({ backdrop: "static" });
                               $http.get(ENV.apiUrl + "api/common/CurLevel/getCurlevel").then(function (res) {
                                   $scope.display = false;
                                   $scope.grid = true;
                                   $scope.obj = res.data;
                                   console.log($scope.obj);
                                   $scope.totalItems = $scope.obj.length;
                                   $scope.todos = $scope.obj;
                                   console.log($scope.totalItems);
                                   $scope.makeTodos();
                               }, function () {
                                   $scope.loading = true;
                                   alert("Failed to get Daata");
                               });
                               $scope.msg = true; $scope.dis2 = false; $scope.dis1 = true;
                           }
                       }, function () {
                           $scope.show_ins_table_loading = false;
                           $('#ErrorMessage').modal({ backdrop: "static" });
                       });
                   }
               }
           }

           $scope.Update = function () {
               var data = $scope.temp;
               data.opr = 'U';
               $http.post(ENV.apiUrl + "api/common/CurLevel/CUDCurLevel", data).then(function (res) {
                   $scope.up = res.data;
                   $('#Success_Update').modal({ backdrop: "static" });
                   $http.get(ENV.apiUrl + "api/common/CurLevel/getCurlevel").then(function (res) {
                       $scope.display = false;
                       $scope.grid = true;
                       $scope.obj = res.data;
                       console.log($scope.obj);
                       $scope.totalItems = $scope.obj.length;
                       $scope.todos = $scope.obj;
                       console.log($scope.totalItems);
                       $scope.makeTodos();
                   }, function () {
                       $scope.loading = true;
                       alert("Failed to get Daata");
                   });
                   $scope.msg = true; $scope.dis2 = false; $scope.dis1 = true;

               }, function () {
                   $scope.show_ins_table_loading = false;
                   $('#ErrorMessage').modal({ backdrop: "static" });
               });
           }

           $scope.Delete = function () {

               if (del.length != 0) {
                   $http.post(ENV.apiUrl + "api/common/CurLevel/DCurLevel?objlist=" + JSON.stringify(del)).then(function (res) {
                       $scope.ins = res.data;
                       del = [];
                       if ($scope.ins.strMessage == "1") {
                           $('#Success_Delete').modal({ backdrop: "static" });

                           $http.get(ENV.apiUrl + "api/common/CurLevel/getCurlevel").then(function (res) {
                               $scope.display = false;
                               $scope.grid = true;
                               $scope.obj = res.data;
                               console.log($scope.obj);
                               $scope.totalItems = $scope.obj.length;
                               $scope.todos = $scope.obj;
                               console.log($scope.totalItems);
                               $scope.makeTodos();
                           }, function () {
                               $scope.loading = true;
                               alert("Failed to get Daata");
                           });
                       }
                       else {
                           $('#Error_Delete').modal({ backdrop: "static" });
                       }
                       $scope.msg = true; $scope.dis2 = false; $scope.dis1 = true;
                       // console.log($scope.sectionsobj);
                   }, function () {
                       $scope.show_ins_table_loading = false;
                       $('#ErrorMessage').modal({ backdrop: "static" });
                   });
               }
               else {
                   alert("Select Record");
               }
           }

           $scope.New = function () {
               $scope.temp = "";
               $scope.levelcodeRonly = false;
               $scope.grid = false; $scope.update_flag = false;
               $scope.display = true; $scope.btn_save = true; $scope.btn_update = false;
           }

           $scope.cancel = function () {
               $scope.grid = true;
               $scope.display = false;
               $scope.update1 = false;
               $scope.save1 = false;
               $scope.Myform.$setPristine();
               $scope.Myform.$setUntouched();
           }


           $('*[data-datepicker="true"] input[type="text"]').datepicker({
               todayBtn: true,
               orientation: "top left",
               autoclose: true,
               todayHighlight: true
           });

           $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
               $('input[type="text"]', $(this).parent()).focus();
           });



       }]);
})();