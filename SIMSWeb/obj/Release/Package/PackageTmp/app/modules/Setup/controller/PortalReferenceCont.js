﻿(function () {
    'use strict';
    var obj1, obj2, temp, main;
    var opr = '';
    var portalcode = [];

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('PortalReferenceCont',
         ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
             $scope.pagesize = '5';
             $scope.table1 = true;
             $scope.lbl_filename = false;
             $scope.busy = false;
             $scope.editmode = false;
             var formdata = new FormData();
             var dataSave = [];
             var dataUpdate = [];
             $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

             $scope.size = function (str) {
                 console.log(str);
                 $scope.pagesize = str;
                 $scope.currentPage = 1;
                 $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
             }

             $scope.index = function (str) {
                 $scope.pageindex = str;
                 $scope.currentPage = str;
                 console.log("currentPage=" + $scope.currentPage);
                 $scope.makeTodos();
                 main.checked = false;
                 $scope.row1 = '';
             }

             $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                 $scope.curriculum = res.data;
             })

             $scope.New = function () {
                 $scope.lbl_filename = false;
                 $scope.myForm.$setPristine();
                 $scope.myForm.$setUntouched();
                 $scope.newmode = true;
                 $scope.edt = '';
                 $scope.editmode = false;
                 opr = 'S';
                 $scope.readonly = false;
                 $scope.table1 = false;
                 $scope.operation = true;
                 $scope.savebtn = true;
                 $scope.updatebtn = false;
                 //$scope.edt['sims_reference_status'] = true;
             }

             $scope.up = function (str) {
                 opr = 'U';
                 $scope.editmode = true;
                 $scope.newmode = false;
                 $scope.savebtn = false;
                 $scope.updatebtn = true;
                 $scope.readonly = true;
                 $scope.table1 = false;
                 $scope.operation = true;
                 $scope.lbl_filename = true;
                 //$scope.edt = str;
                 $scope.edt = {
                     sims_reference_number: str.sims_reference_number,
                     sims_cur_code: str.sims_cur_code,
                     sims_reference_name: str.sims_reference_name,
                     sims_reference_filename: str.sims_reference_filename,
                     sims_reference_desc: str.sims_reference_desc,
                     sims_reference_publish_date: str.sims_reference_publish_date,
                     sims_reference_expiry_date: str.sims_reference_expiry_date,
                     sims_reference_status: str.sims_reference_status,
                     sims_learn_arabic_status: str.sims_learn_arabic_status,
                     sims_cur_name: str.sims_cur_name
                 }

             }

             $http.get(ENV.apiUrl + "api/portalrefer/getPortalDetail").then(function (Portal_Data) {
                 $scope.PortalData = Portal_Data.data;
                 $scope.totalItems = $scope.PortalData.length;
                 $scope.todos = $scope.PortalData;
                 $scope.makeTodos();
             });

             $scope.cancel = function () {
                 $scope.table1 = true;
                 $scope.operation = false;
                 $scope.edt = "";
                 $scope.myForm.$setPristine();
             }

             $scope.Save = function (myForm) {
                 debugger
                 $('#loader0712').modal({ backdrop: 'static', keyboard: false });
                 
                 if (myForm) {
                     var request = {
                         method: 'POST',
                         url: ENV.apiUrl + 'api/file/uploadDocument?filename=' + $scope.edt.sims_reference_name + "&location=" + "Docs/portalReference/",
                         data: formdata,
                         headers: {
                             'Content-Type': undefined
                         }
                     };

                     $http(request).success(function (d) {

                         debugger
                         var flag = false;
                         var data = $scope.edt;
                         data.opr = 'I';
                         data.sims_reference_filename = $scope.edt.sims_reference_name;
                         data.sims_reference_filename = d;
                         data.sims_timetable_filename = d;
                         if (data.sims_timetable_filename == "false") {
                             swal({ title: "Alert", text: "Please Select File....", width: 300, height: 200 });
                             $('#loader0712').modal('hide');
                         }
                         else {
                             for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                 if ($scope.filteredTodos[i].sims_reference_number == data.sims_reference_number) {
                                     flag = true;
                                 }
                             }
                             if (flag) {
                                 swal({ title: "Alert", text: "Reference number Already exists", width: 300, height: 200 });
                                 $state.go($state.current, {}, { reload: true });
                                 $('#loader0712').modal('hide');
                             }
                             else {
                                 dataSave.push(data);
                                 $http.post(ENV.apiUrl + "api/portalrefer/PortalCUD?simsobj=", dataSave).then(function (msg) {
                                     $scope.msg1 = msg.data;
                                     if ($scope.msg1 == true) {
                                         swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                                         $('#loader0712').modal('hide');
                                       
                                        
                                     }
                                     else {
                                         swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                                         $('#loader0712').modal('hide');
                                     }

                                     $http.get(ENV.apiUrl + "api/portalrefer/getPortalDetail").then(function (Portal_Data) {
                                         $scope.PortalData = Portal_Data.data;
                                         $scope.totalItems = $scope.PortalData.length;
                                         $scope.todos = $scope.PortalData;
                                         $scope.makeTodos();
                                         $('#loader0712').modal('hide');
                                     });
                                 });
                             }
                         }
                     });
                     dataSave = [];
                     $scope.table1 = true;
                     $scope.operation = false;

                 }

             }

             $scope.Update = function () {
                 $scope.busy = true;
                 var request = {
                     method: 'POST',
                     url: ENV.apiUrl + 'api/file/uploadDocument?filename=' + $scope.edt.sims_reference_name + "&location=" + "Docs/portalReference/",
                     data: formdata,
                     headers: {
                         'Content-Type': undefined
                     }
                 };
                 $http(request).success(function (d) {
                     var data = $scope.edt;
                     data.opr = 'U';
                     data.sims_reference_filename = $scope.edt.sims_reference_name;
                     data.sims_reference_filename = d;
                     data.sims_timetable_filename = d;
                     if (data.sims_timetable_filename == "false") {
                         swal({ title: "Alert", text: "Please Select File....", width: 300, height: 200 });
                         $('#loader0712').modal('hide');
                     }
                     else {
                         dataUpdate.push(data);
                         $http.post(ENV.apiUrl + "api/portalrefer/PortalCUD?simsobj=", dataUpdate).then(function (msg) {
                             $scope.msg1 = msg.data;
                             $scope.operation = false;
                             $http.get(ENV.apiUrl + "api/portalrefer/getPortalDetail").then(function (Portal_Data) {
                                 $scope.PortalData = Portal_Data.data;
                                 $scope.totalItems = $scope.PortalData.length;
                                 $scope.todos = $scope.PortalData;
                                 formdata = new FormData();
                                 $scope.makeTodos();

                                 if ($scope.msg1 == true) {
                                     swal({ text: "Record Updated Successfully", width: 300, height: 200 });
                                     $('#loader0712').modal('hide');
                                 }
                                 else {
                                     swal({ text: "Record Not Updated", width: 300, height: 200 });
                                     $('#loader0712').modal('hide');
                                 }
                             });

                         })
                     }
                 });
                 $scope.operation = false;
                 $scope.table1 = true;
             }

             $scope.makeTodos = function () {
                 var rem = parseInt($scope.totalItems % $scope.numPerPage);
                 if (rem == '0') {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                 }
                 else {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                 }
                 var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                 var end = parseInt(begin) + parseInt($scope.numPerPage);

                 console.log("begin=" + begin); console.log("end=" + end);

                 $scope.filteredTodos = $scope.todos.slice(begin, end);
             };

             $scope.getTheFiles = function ($files) {
                 $scope.filesize = true;
                 $scope.lbl_filename = false;

                 angular.forEach($files, function (value, key) {
                     formdata.append(key, value);

                     var i = 0;
                     if ($files[i].size > 2097152) {
                         $scope.filesize = false;
                         $scope.edt.photoStatus = false;

                         swal({ title: "Alert", text: "File Should Not Exceed 2MB.", imageUrl: "assets/img/notification-alert.png", });

                     }
                     else {

                     }

                 });
             };

             $scope.file_changed = function (element, str) {
                 $scope.lbl_filename = false;
                 var photofile = element.files[0];

                 $scope.photo_filename = (photofile.name);

                 $scope.edt1 = { sims_timetable_filename: $scope.photo_filename }
                 $.extend($scope.edt, $scope.edt1)
                 $scope.photo_filename = (photofile.type);
                 var reader = new FileReader();
                 reader.onload = function (e) {
                     $scope.$apply(function () {
                         $scope.prev_img = e.target.result;

                     });
                 };
                 reader.readAsDataURL(photofile);

                 if (element.files[0].size < 2097152) {

                 }
             };

             $(function () {

                 $('table tbody tr:even').addClass('even_row');
                 $("#all_chk").change(function () {
                     $('input:checkbox').prop('checked', this.checked);
                     $('tr').toggleClass("selected_row", this.checked)
                 });

                 $('table tbody :checkbox').change(function (event) {
                     $(this).closest('tr').toggleClass("selected_row", this.checked);
                 });
                 $('table tbody tr').click(function (event) {
                     if (event.target.type !== 'checkbox') {
                         $(':checkbox', this).trigger('click');
                     }
                     $("input[type='checkbox']").not('#all_chk').change(function (e) {
                         if ($(this).is(":checked")) {
                             $(this).closest('tr').addClass("selected_row");
                         } else {
                             $(this).closest('tr').removeClass("selected_row");
                         }
                     });
                 });

             });

             $timeout(function () {
                 $("#fixTable").tableHeadFixer({ 'top': 1 });
             }, 100);

             $timeout(function () {
                 $("#fixTable1").tableHeadFixer({ 'top': 1 });

             }, 100);

             $scope.check_all = function () {
                 main = document.getElementById('all_chk');
                 if (main.checked == true) {
                     for (var i = 0; i < $scope.filteredTodos.length; i++) {
                         var v = document.getElementById(i + $scope.filteredTodos[i].sims_reference_number);
                         v.checked = true;
                         $scope.row1 = 'row_selected';
                         $('tr').addClass("row_selected");
                     }
                 }
                 else {
                     for (var i = 0; i < $scope.filteredTodos.length; i++) {
                         var v = document.getElementById(i + $scope.filteredTodos[i].sims_reference_number);
                         v.checked = false;
                         main.checked = false;
                         $scope.row1 = '';

                     }
                 }

             }

             $scope.check_once = function (info) {
                 $("input[type='checkbox']").change(function (e) {
                     if ($(this).is(":checked")) { //If the checkbox is checked
                         $(this).closest('tr').addClass("row_selected");
                         //Add class on checkbox checked
                         $scope.color = '#edefef';
                     }
                     else {
                         $(this).closest('tr').removeClass("row_selected");
                         //Remove class on checkbox uncheck
                         $scope.color = '#edefef';
                     }
                 });

                 main = document.getElementById('all_chk');
                 if (main.checked == true) {
                     main.checked = false;
                     $("input[type='checkbox']").change(function (e) {
                         if ($(this).is(":checked")) {
                             $(this).closest('tr').addClass("row_selected");
                         }
                         else {
                             $(this).closest('tr').removeClass("row_selected");
                         }
                     });
                 }

             }

             //$scope.check_all = function () {
             //    main = document.getElementById('all_chk');
             //    if (main.checked == true) {
             //        for (var i = 0; i < $scope.filteredTodos.length; i++) {
             //            var v = document.getElementById(i);
             //            v.checked = true;
             //            $scope.row1 = 'row_selected';
             //            $('tr').addClass("row_selected");
             //        }
             //    }
             //    else {
             //        for (var i = 0; i < $scope.filteredTodos.length; i++) {
             //            var v = document.getElementById(i);
             //            v.checked = false;
             //            main.checked = false;
             //            $scope.row1 = '';
             //        }
             //    }
             //}

             //$scope.check_once = function () {
             //    $("input[type='checkbox']").change(function (e) {
             //        if ($(this).is(":checked")) {
             //            $(this).closest('tr').addClass("row_selected");
             //            $scope.color = '#edefef';
             //        } else {
             //            $(this).closest('tr').removeClass("row_selected");
             //            $scope.color = '#edefef';
             //        }
             //    });

             //    main = document.getElementById('all_chk');
             //    if (main.checked == true) {
             //        main.checked = false;
             //        $("input[type='checkbox']").change(function (e) {
             //            if ($(this).is(":checked")) {
             //                $(this).closest('tr').addClass("row_selected");
             //            }
             //            else {
             //                $(this).closest('tr').removeClass("row_selected");
             //            }
             //        });
             //    }

             //}


             $scope.deleterecord = function () {
                 debugger;
                 $scope.flag = false;
                 var deleteleave = [];
                 for (var i = 0; i < $scope.filteredTodos.length; i++) {
                     var v = document.getElementById(i + $scope.filteredTodos[i].sims_reference_number);
                     if (v.checked == true) {
                         $scope.flag = true;
                         var deletemodulecode = ({
                             'sims_reference_number': $scope.filteredTodos[i].sims_reference_number,
                             opr: 'D'
                         });
                         deleteleave.push(deletemodulecode);
                     }
                 }
                 if ($scope.flag) {
                     swal({
                         title: '',
                         text: "Are you sure you want to Delete?",
                         showCloseButton: true,
                         confirmButtonText: 'Yes',
                         showCancelButton: true,
                         width: 380,
                         cancelButtonText: 'No',

                     }).then(function (isConfirm) {
                         if (isConfirm) {
                             $http.post(ENV.apiUrl + "api/portalrefer/PortalCUD", deleteleave).then(function (msg) {
                                 $scope.msg1 = msg.data;
                                 if ($scope.msg1 == true) {
                                     swal({ text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                         if (isConfirm) {
                                             $http.get(ENV.apiUrl + "api/portalrefer/getPortalDetail").then(function (portal_Data) {
                                                 $scope.AttendanceCode_Data = portal_Data.data;
                                                 $scope.totalItems = $scope.AttendanceCode_Data.length;
                                                 $scope.todos = $scope.AttendanceCode_Data;
                                                 $scope.makeTodos();
                                             });
                                         }

                                         $scope.currentPage = true;
                                     });
                                 }
                                 else {
                                     swal({ text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                         if (isConfirm) {
                                             $http.get(ENV.apiUrl + "api/portalrefer/getPortalDetail").then(function (portal_Data) {
                                                 $scope.AttendanceCode_Data = portal_Data.data;
                                                 $scope.totalItems = $scope.AttendanceCode_Data.length;
                                                 $scope.todos = $scope.AttendanceCode_Data;
                                                 $scope.makeTodos();
                                                 main.checked = false;
                                                 $('tr').removeClass("row_selected");
                                             });
                                         }
                                     });
                                 }
                             });
                         }
                         else {
                             main = document.getElementById('all_chk');
                             if (main.checked == true) {
                                 main.checked = false;
                             }
                             for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                 var v = document.getElementById(i + $scope.filteredTodos[i].sims_reference_number);
                                 if (v.checked == true) {
                                     v.checked = false;
                                     main.checked = false;
                                     $('tr').removeClass("row_selected");
                                 }
                             }
                         }
                     });
                 }
                 else {
                     swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                 }

                 $scope.currentPage = str;
             }

             $scope.searched = function (valLists, toSearch) {
                 return _.filter(valLists, function (i) {
                     /* Search Text in all  fields */
                     return searchUtil(i, toSearch);
                 });
             };

             $scope.search = function () {
                 $scope.todos = $scope.searched($scope.PortalData, $scope.searchText);
                 $scope.totalItems = $scope.todos.length;
                 $scope.currentPage = '1';
                 if ($scope.searchText == '') {
                     $scope.todos = $scope.PortalData;
                 }
                 $scope.makeTodos();
             }

             function searchUtil(item, toSearch) {
                 /* Search Text in all 3 fields */
                 return (item.sims_reference_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_reference_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_reference_filename.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_reference_publish_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_reference_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_reference_number == toSearch) ? true : false;
             }

             $('*[data-datepicker="true"] input[type="text"]').datepicker({
                 todayBtn: true,
                 orientation: "top left",
                 autoclose: true,
                 todayHighlight: true
             });

             $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                 $('input[type="text"]', $(this).parent()).focus();
             });
             $scope.createdate = function (end_date, start_date, name) {


                 var month1 = end_date.split("/")[0];
                 var day1 = end_date.split("/")[1];
                 var year1 = end_date.split("/")[2];
                 var new_end_date = year1 + "/" + month1 + "/" + day1;
                 //var new_end_date = day1 + "/" + month1 + "/" + year1;

                 var year = start_date.split("/")[0];
                 var month = start_date.split("/")[1];
                 var day = start_date.split("/")[2];
                 var new_start_date = year + "/" + month + "/" + day;
                 // var new_start_date = day + "/" + month + "/" + year;

                 if (new_end_date < new_start_date) {

                     $rootScope.strMessage = "Please Select Future Date";
                     $('#message').modal('show');

                     $scope.edt[name] = '';
                 }
                 else {

                     $scope.edt[name] = new_end_date;
                 }
             }

             $scope.showdate = function (date, name) {

                 var month = date.split("/")[0];
                 var day = date.split("/")[1];
                 var year = date.split("/")[2];
                 $scope.edt[name] = year + "/" + month + "/" + day;


             }

             simsController.directive('ngFiles', ['$parse', function ($parse) {

                 function fn_link(scope, element, attrs) {
                     var onChange = $parse(attrs.ngFiles);
                     element.on('change', function (event) {
                         onChange(scope, { $files: event.target.files });
                     });
                 };

                 return {
                     link: fn_link
                 }
             }])


         }])
})();
