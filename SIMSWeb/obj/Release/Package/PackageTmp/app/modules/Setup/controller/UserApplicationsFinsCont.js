﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('UserApplicationsFinsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {


            $scope.Allusers = [];
            $scope.busy = false;

            $scope.table = false;
            $scope.newmode = false;
            $scope.editonly = false;
            $scope.edt = { comn_user_date_created: new Date() }
            $http.get(ENV.apiUrl + "api/common/UserApplicationsFinsController/GetCompany").then(function (group) {
                $scope.Company = group.data;
                console.log($scope.Company);
                switch ($scope.Company.length) {
                    case 1:
                        $scope.flg_comp1 = true;
                        $scope.flg_comp2 = false;
                        $scope.flg_comp3 = false;
                        break;
                    case 2:
                        $scope.flg_comp1 = true;
                        $scope.flg_comp2 = true;
                        $scope.flg_comp3 = false;
                        break;
                    case 3:
                        $scope.flg_comp1 = true;
                        $scope.flg_comp2 = true;
                        $scope.flg_comp3 = true;
                        break;


                }
               

            });



            

            //child





            var theme = '';
            $scope.copy = 'Update';
            $scope.FlagUpdate = true;
            $scope.chkcopy = true;



            $http.get(ENV.apiUrl + "api/common/UserApplicationsController/GetRoles").then(function (res) {

                $scope.role = res.data;


            });

            $scope.Submit_click = function () {
             
                $http.get(ENV.apiUrl + "api/common/UserApplicationsFinsController/GetCompanyStatusWithAppl?userName=" + $scope.userName).then(function (res) {

                    $scope.UserApplicationList = res.data;

                    console.log(res.data);

                });

            }


            $scope.company_click = function (flg, index) {
                debugger
              //  if (index == 1) {
                    for (var i = 0; i < $scope.UserApplicationList.length; i++) {

                        $scope.UserApplicationList[i]['comp_status_' + index] = flg;
                    }
               // }
                

            }

            

       

            $scope.radioclick = function (str) {
                $scope.clear();
                // console.log(str)
                if (str == 'Update') {
                   


                }
                else {
                  

                }

            }


          
            $scope.itemsPerPage = "5";
            $scope.currentPage = 0;
            $scope.items = [];

            $scope.size = function (pagesize) {
                $scope.itemsPerPage = pagesize;

            }

            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;

            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.Allusers.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;
            };

            $scope.serachUser = function () {
                $scope.temp1 = undefined;
                $scope.Allusers = [];
                $scope.copysearch = false;
               // $('#searchUser').modal('show');
              

                $rootScope.visible_stud = false;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = true;
                $rootScope.visible_Employee = false;
                if ($scope.copy == 'Update')

                    $rootScope.chkMulti = false;
                else
                    $rootScope.chkMulti = true;

                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });





            }



            $scope.$on('global_cancel', function (str) {
                delvar = [];
                var selectedUserNames = [];


                for (var i = 0; i < $scope.SelectedUserLst.length; i++) {

                    //if ($scope.Allusers[i].user_code1 == true) {
                    delvar.push($scope.SelectedUserLst[i])
                    selectedUserNames.push($scope.SelectedUserLst[i].user_name)

                    //}

                }
                console.log(delvar)
                var data = [];
                if ($scope.copy == 'Update') {

                    if (delvar.length > 0) {
                        data.push(delvar[0].user_name);


                        $scope.userName = delvar[0].user_name;

                    }
                }

                else {


                }

                //     $('#searchUser').modal('hide');

            }

          );

            $scope.cancel = function () {
                delvar = [];
                var selectedUserNames = [];


                for (var i = 0; i < $scope.Allusers.length; i++) {

                    if ($scope.Allusers[i].user_code1 == true) {
                        delvar.push($scope.Allusers[i])
                        selectedUserNames.push($scope.Allusers[i].user_name)

                    }

                }
                console.log(delvar)
                var data = [];
                if ($scope.copy == 'Update') {
                   
                    if (delvar.length > 0) {
                        data.push(delvar[0].user_name);

                        
                        $scope.userName = delvar[0].user_name;
                      
                    }
                }

                else {

                   
                }

                $('#searchUser').modal('hide');

            }
            var main, del = '', delvar = [];
            $scope.chk = function () {

                main = document.getElementById('chk_min');
                del = [];
                if (main.checked == true) {
                    for (var i = 0; i < $scope.Allusers.length; i++) {

                        $scope.Allusers[i].user_code1 = true;

                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.Allusers.length; i++) {
                        $scope.Allusers[i].user_code1 = false;
                        main.checked = false;


                        $scope.row1 = '';
                    }
                }


            }


            $scope.Delete1 = function () {
                $scope.Allusers = [];
                $scope.currentPage = 0;

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchUser?data=" + JSON.stringify($scope.temp1)).then(function (users) {
                    $scope.Allusers = users.data;

                    $scope.table = true;


                    $scope.busy = false;
                })


                //del = '';

                //for (var i = 0; i < $scope.Allusers.length; i++) {

                //    if ($scope.Allusers[i].user_code1 == true) {
                //        delvar.push($scope.Allusers[i])
                //    }

                //}
                //console.log(delvar)
                //var data = { user_code: del };


            }


            //$http.get(ENV.apiUrl + "api/common/UserApplicationsController/GetAssignRoles?userName=1mp49").then(function (res) {

            //    $scope.UserApplicationList = res.data;
            //    console.log($scope.items);

            //});




            $scope.statusCheck = function (str) {
                if (str) {
                    // $scope.obj1 = res.data;
                    for (var i = 0; i < $scope.UserApplicationList.length; i++) {
                        $scope.UserApplicationList[i].comn_user_read = true;
                        $scope.UserApplicationList[i].comn_user_insert = true;
                        $scope.UserApplicationList[i].comn_user_update = true;
                        $scope.UserApplicationList[i].comn_user_delete = true;
                        $scope.UserApplicationList[i].comn_user_massupdate = true;
                        $scope.UserApplicationList[i].comn_user_appl_status = true;

                    }
                }
                else {

                    for (var i = 0; i < $scope.UserApplicationList.length; i++) {
                        $scope.UserApplicationList[i].comn_user_read = false;
                        $scope.UserApplicationList[i].comn_user_insert = false;
                        $scope.UserApplicationList[i].comn_user_update = false;
                        $scope.UserApplicationList[i].comn_user_delete = false;
                        $scope.UserApplicationList[i].comn_user_massupdate = false;
                        $scope.UserApplicationList[i].comn_user_appl_status = false;


                    }
                }
            }

            $scope.ReadCheck = function (str, name) {
                if (str) {
                    // $scope.obj1 = res.data;
                    for (var i = 0; i < $scope.UserApplicationList.length; i++) {
                        if (name == 'Read')
                            $scope.UserApplicationList[i].comn_user_read = true;
                        if (name == 'Insert')
                            $scope.UserApplicationList[i].comn_user_insert = true;
                        if (name == 'Update')
                            $scope.UserApplicationList[i].comn_user_update = true;
                        if (name == 'Delete')
                            $scope.UserApplicationList[i].comn_user_delete = true;
                        if (name == 'Mass_Update')
                            $scope.UserApplicationList[i].comn_user_massupdate = true;


                    }
                }
                else {

                    for (var i = 0; i < $scope.UserApplicationList.length; i++) {
                        if (name == 'Read')
                            $scope.UserApplicationList[i].comn_user_read = false;
                        if (name == 'Insert')
                            $scope.UserApplicationList[i].comn_user_insert = false;
                        if (name == 'Update')
                            $scope.UserApplicationList[i].comn_user_update = false;
                        if (name == 'Delete')
                            $scope.UserApplicationList[i].comn_user_delete = false;
                        if (name == 'Mass_Update')
                            $scope.UserApplicationList[i].comn_user_massupdate = false;


                    }
                }
            }


            $scope.rowchk = function (index, status) {
                console.log(index);

                if (status) {
                    $scope.UserApplicationList[index].comn_user_read = true;
                    $scope.UserApplicationList[index].comn_user_insert = true;
                    $scope.UserApplicationList[index].comn_user_update = true;
                    $scope.UserApplicationList[index].comn_user_delete = true;
                    $scope.UserApplicationList[index].comn_user_massupdate = true;

                }
                else {

                    $scope.UserApplicationList[index].comn_user_read = false;
                    $scope.UserApplicationList[index].comn_user_insert = false;
                    $scope.UserApplicationList[index].comn_user_update = false;
                    $scope.UserApplicationList[index].comn_user_delete = false;
                    $scope.UserApplicationList[index].comn_user_massupdate = false;


                }


            }

            

            $scope.save = function () {
                debugger;
                if ($scope.copy == 'Update') {

                    if ($scope.Company.length == 1) {

                        var data = $scope.UserApplicationList;
                        for (var i = 0; i < $scope.UserApplicationList.length; i++) {


                            data[i].comp_code = $scope.Company[0].fins_comp_code;
                            data[i].username = $scope.userName;
                            data[i].comn_user_appl_fins_comp_status = $scope.UserApplicationList[i].comp_status_1;

                        }

                        $http.post(ENV.apiUrl + "api/common/UserApplicationsFinsController/InsertApplications", data).then(function (res) {

                            if (res.data) {
                                swal({ title: "Alert", text: "Record Updated Successfully", imageUrl: "assets/img/check.png", });
                                $scope.clear();
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Updated ", imageUrl: "assets/img/notification-alert.png", });
                            }

                        });

                    }

                    if ($scope.Company.length == 2) {

                        var data = $scope.UserApplicationList;
                        for (var i = 0; i < $scope.UserApplicationList.length; i++) {


                            data[i].comp_code = $scope.Company[0].fins_comp_code;
                            data[i].username = $scope.userName;
                            data[i].comn_user_appl_fins_comp_status = $scope.UserApplicationList[i].comp_status_1;

                        }

                        $http.post(ENV.apiUrl + "api/common/UserApplicationsFinsController/InsertApplications", data).then(function (res) {


                        });

                        var data = $scope.UserApplicationList;
                        for (var i = 0; i < $scope.UserApplicationList.length; i++) {


                            data[i].comp_code = $scope.Company[1].fins_comp_code;
                            data[i].username = $scope.userName;
                            data[i].comn_user_appl_fins_comp_status = $scope.UserApplicationList[i].comp_status_2;

                        }

                        $http.post(ENV.apiUrl + "api/common/UserApplicationsFinsController/InsertApplications", data).then(function (res) {
                            if (res.data) {
                                swal({ title: "Alert", text: "Record Updated Successfully", imageUrl: "assets/img/check.png", });
                                $scope.clear();
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Updated ", imageUrl: "assets/img/notification-alert.png", });
                            }


                        });
                    
                    }

                    if ($scope.Company.length == 3) {
                        $('#loader').modal({ backdrop: 'static', keyboard: false });

                        var data = angular.copy($scope.UserApplicationList);

                        var data1 = angular.copy($scope.UserApplicationList);
                        var data2 = angular.copy($scope.UserApplicationList);


                        for (var i = 0; i < $scope.UserApplicationList.length; i++) {


                            data[i].comp_code = $scope.Company[0].fins_comp_code;
                            data[i].username = $scope.userName;
                            data[i].comn_user_appl_fins_comp_status = $scope.UserApplicationList[i].comp_status_1;

                            data1[i].comp_code = $scope.Company[1].fins_comp_code;
                            data1[i].username = $scope.userName;
                            data1[i].comn_user_appl_fins_comp_status = $scope.UserApplicationList[i].comp_status_2;


                            data2[i].comp_code = $scope.Company[2].fins_comp_code;
                            data2[i].username = $scope.userName;
                            data2[i].comn_user_appl_fins_comp_status = $scope.UserApplicationList[i].comp_status_3;



                        }

                        $http.post(ENV.apiUrl + "api/common/UserApplicationsFinsController/InsertApplications", data).then(function (res) {

                            $http.post(ENV.apiUrl + "api/common/UserApplicationsFinsController/InsertApplications", data1).then(function (res) {

                                $http.post(ENV.apiUrl + "api/common/UserApplicationsFinsController/InsertApplications", data2).then(function (res) {
                                    $('#loader').modal('hide');
                                    $(".modal-backdrop").removeClass("modal-backdrop");
                                    swal({ title: "Alert", text: "Record Updated Successfully", imageUrl: "assets/img/check.png", });
                                    $scope.clear();

                                });
                            });

                        });


                    }

                }
                else {


                }


            }
            $scope.clear = function () {
                $scope.copy_from_user = undefined;
                $scope.UserApplicationList = undefined;
                $scope.test_comn_role_code = undefined;
                $("#Selected_Role").jqxListBox('clear');
                $("#Selected_User").jqxListBox('clear');
                $scope.Allusers = [];

                for (var i = 0; i < $scope.Company.length; i++) {
                    $scope.Company[i]['comp_status'] = false;
                }

            }
            $scope.copyCheck = function (str) {
                if (str) {

                    $scope.FlagUpdate = false;

                }


                else {
                    $scope.FlagUpdate = true;
                }

            }



        }])
})();
