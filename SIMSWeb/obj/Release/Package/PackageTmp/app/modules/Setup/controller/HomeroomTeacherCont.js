﻿(function () {
    'use strict';
    var teacher_code = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('HomeroomTeacherCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.grid = true;
            $scope.pagesize = "5";
            $scope.pageindex = "1";

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/homeroom/getTeacher").then(function (getTeacher) {
                $scope.getTeacher = getTeacher.data;

            });

            $http.get(ENV.apiUrl + "api/homeroom/getAllHomeroomBatchName").then(function (AllHomeroomBatchName) {
                $scope.AllHomeroom_BatchName = AllHomeroomBatchName.data;
                console.log($scope.AllHomeroom_BatchName);

            });

            $http.get(ENV.apiUrl + "api/Homeroom/getHomeroomTeacher").then(function (res1) {
                if (res1.data !== null) {
                    $scope.obj = res1.data;
                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.makeTodos();
                }
                else {
                    swal({ text: 'Sorry Data Not Found', width: 380, showCloseButton: true });
                }

            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);


            };

            $http.get(ENV.apiUrl + "api/Homeroom/GetAllHomeroomName").then(function (AllHomeroomName) {
                $scope.Homeroomnames = AllHomeroomName.data;
                console.log($scope.Homeroomnames);

            });

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                console.log($scope.curriculum);

            });

            $scope.getacyr = function (str) {
                console.log(str);
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    console.log($scope.Academic_year);
                })
            }

            $scope.SearchTeacherCode = function () {
                $scope.searchtable = false;
                $scope.TeacherName = false;
                $scope.temp = '';
                $('#MyModal').modal('show');

            }

            $scope.SearchAllTeacher = function () {
                main = document.getElementById('mainchk1');
                if (main.cheked = true)
                    main.checked = false;
                $scope.busy = true;

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchTeacher?data=" + JSON.stringify($scope.temp)).then(function (SearchTeacher) {
                    $scope.Search_Teacher_result = SearchTeacher.data;
                    $scope.teacher_table = true;
                    $scope.busy = false;
                    $scope.searchtable = true;
                });
            }

            var old_teacher_code = '';

            $scope.edit = function (str) {

                old_teacher_code = str.sims_homeroom_teacher_code;
                $scope.dis = true;
                $scope.TeacherName = true;
                $scope.teacher_search = false;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edt = str;
                $scope.teacher_table = false;
                $scope.searchtable = true;
                ClareCheckboxex();
                $scope.getacyr(str.sims_cur_code);
                $scope.edt.sims_homeroom_teacher_code1 = str.sims_homeroom_teacher_name;
            }

            $scope.New = function (MyForm) {
                $scope.dis = false;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
                $scope.teacher_table = false;
                $scope.teacher_search = true;
                $scope.teacher_number = [];
                $scope.TeacherName = false;
             
                $scope.MyForm.$setPristine();
               
            }

            $scope.Delete = function () {
                var flag = false;

                $scope.teacher_number = [];

                teacher_code = [];

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_homeroom_teacher_code;
                    var v = document.getElementById(t);
                    if (v.checked == true) {
                        flag = true;

                        teacher_code = teacher_code + $scope.filteredTodos[i].sims_homeroom_teacher_code + ',';
                    }
                }
                var deleteteacher_code = ({
                    'sims_homeroom_teacher_code': teacher_code,
                    'opr': 'D'
                });

                if (flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/Homeroom/UHomeroomTeacher", deleteteacher_code).then(function (MSG) {

                                $scope.msg = MSG.data;
                                $scope.display = false;
                                $scope.grid = true;
                                if ($scope.msg == true) {
                                    swal({ title: "", text: "Homeroom Deleted Successfully", showCloseButton: true, width: 380, });
                                    $http.get(ENV.apiUrl + "api/Homeroom/getHomeroomTeacher").then(function (res1) {
                                        if (res1.data !== null) {

                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                            }
                                            $scope.row1 = "";
                                            $scope.currentPage = 1
                                            $scope.obj = res1.data;
                                            $scope.totalItems = $scope.obj.length;
                                            $scope.todos = $scope.obj;
                                            $scope.makeTodos();
                                        }
                                        else {
                                            swal({ text: "Data Not Fount", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "", text: "Homeroom Not Deleted ", showCloseButton: true, width: 380, });
                                    main = document.getElementById('mainchk');
                                    if (main.checked == true) {
                                        main.checked = false;
                                        $scope.row1 = '';
                                    }
                                    $scope.CheckAllChecked();
                                }
                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                                $scope.row1 = '';
                            }
                            $scope.CheckAllChecked();
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                teacher_code = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_homeroom_teacher_code);
                    if (main.checked == true) {
                        v.checked = true;
                        teacher_code = teacher_code + $scope.filteredTodos[i].sims_homeroom_teacher_code + ',';
                        $('tr').addClass("row_selected");
                    }
                    else {
                        main.checked = false;
                        v.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }
            }

            $scope.checkonebyonedelete = function (str) {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }

            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;

            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                ClareCheckboxex();
                $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                ClareCheckboxex();
                $scope.makeTodos();
            }

            $scope.Save = function (isvalid) {

                debugger;

                var sims_teacher_code = [];
                if (isvalid) {
                    if ($scope.teacher_number.length > 0) {
                        for (var i = 0; i < $scope.teacher_number.length; i++) {
                            sims_teacher_code = sims_teacher_code + $scope.teacher_number[i].teacher_id + ','
                        }

                        $scope.edt.sims_homeroom_teacher_code1 = sims_teacher_code;

                        $http.post(ENV.apiUrl + "api/Homeroom/CHomeroomTeacher", $scope.edt).then(function (MSG) {
                            $scope.display = false;
                            $scope.grid = true;
                            $scope.msg = MSG.data;

                            $http.get(ENV.apiUrl + "api/Homeroom/getHomeroomTeacher").then(function (res1) {

                                if (res1.data !== null) {
                                    $scope.row1 = "";
                                    $scope.obj = res1.data;
                                    $scope.totalItems = $scope.obj.length;
                                    $scope.todos = $scope.obj;
                                    $scope.currentPage = 1;
                                    $scope.makeTodos();

                                }
                                else {
                                    swal({ text: 'Sorry Data Not Found', width: 380, showCloseButton: true });
                                }

                            });

                            if ($scope.msg == true) {

                                swal({ text: 'Homeroom Mapped Successfully', width: 380, showCloseButton: true });

                            }
                            else {
                                swal({ text: 'Homeroom Not Mapped', width: 380, showCloseButton: true });
                            }
                        });
                        main.checked = false;
                        $scope.check();
                      
                        $scope.teacher_number = [];
                    }
                    else {
                        swal('', 'Select Atleast One Teacher To Insert Record');
                    }
                }
            }

            $scope.Update = function () {
                $scope.teacher = true;

                $scope.teacher_number = [];

                var data = {
                    sims_cur_code: $scope.edt.sims_cur_code,
                    sims_academic_year: $scope.edt.sims_academic_year,
                    sims_homeroom_teacher_code: $scope.edt.sims_homeroom_teacher_code,
                    sims_status: $scope.edt.sims_status,
                    sims_homeroom_code: $scope.edt.sims_homeroom_code,
                    old_sims_homeroom_teacher_code: old_teacher_code,
                    opr: 'U'
                }

                $http.post(ENV.apiUrl + "api/Homeroom/UHomeroomTeacher", data).then(function (MSG) {

                    $scope.msg = MSG.data;
                    $scope.display = false;
                    $scope.grid = true;

                    $http.get(ENV.apiUrl + "api/Homeroom/getHomeroomTeacher").then(function (res1) {

                        if (res1.data !== null) {
                            $scope.obj = res1.data;

                            $scope.totalItems = $scope.obj.length;
                            $scope.todos = $scope.obj;
                            $scope.makeTodos();

                        }
                        else {
                            swal('', 'Sorry Data Not Found');
                        }

                    });

                    if ($scope.msg == true) {

                        swal({ text: 'Information Updated Successfully', width: 380, showCloseButton: true });

                    }
                    else {

                        swal({ text: 'Information Not Updated', width: 380, showCloseButton: true });

                    }


                });
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }
                $scope.check();

            }

            $scope.teacher_number = [];

            function ClareCheckboxex() {

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                  
                }

                $scope.CheckAllChecked();

            }

            $scope.RemoveTeacherNo = function ($event, index, str) {

                str.splice(index, 1)
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_homeroom_teacher_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            var teacher_code_multiple = [];

            $scope.MultipleTeacherSelection = function () {

                $scope.teacher_number = [];
                main = document.getElementById('mainchk1');
                teacher_code_multiple = [];
                for (var i = 0; i < $scope.Search_Teacher_result.length; i++) {
                    var t = $scope.Search_Teacher_result[i].user_name;
                    var v = document.getElementById(t + i);
                if (main.checked == true) {
                        v.checked = true;
                        teacher_code_multiple.push($scope.Search_Teacher_result[i]);
                        $scope.teacher_number.push($scope.Search_Teacher_result[i]);
                }
                else {
                        v.checked = false;
                        $scope.teacher_number = [];
                        teacher_code_multiple = [];

                    }
                }


            }

            $scope.Datateachercode = function () {

                for (var i = 0; i < $scope.Search_Teacher_result.length; i++) {
                    var t = $scope.Search_Teacher_result[i].user_name;
                    var v = document.getElementById(t + i);

                    if (v.checked == true)

                        $scope.teacher_number.push($scope.Search_Teacher_result[i]);
                }
            }
        }])
})();