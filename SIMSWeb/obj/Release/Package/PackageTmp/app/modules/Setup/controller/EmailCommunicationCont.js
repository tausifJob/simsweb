﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmailCommunicationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.eDetail = false;
            $scope.email = true;

            $scope.itemsPerPage = 10;
            $scope.currentPage = 0;
            $scope.items = [];
            //for (var i = 0; i < 50; i++) {
            //    $scope.items.push({ id: i, name: "name " + i, description: "description " + i });
            //}


            $scope.size = function (pagesize) {
                $scope.itemsPerPage = pagesize;

            }


            debugger;
            $http.get(ENV.apiUrl + "api/EmailTransaction/getSelectInbox").then(function (res) {
                $scope.display = true;
                $scope.items = res.data;
                console.log($scope.items);
            });


            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;
            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.items.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;
            };



            

            $scope.getEmail = function (email_no) {

                        $scope.display = true;
                        $scope.obj1 = email_no;
                       // console.log(res.data);
                        $scope.eDetail = true;
                        $scope.email = false;
            }
            //$scope.get = function (email_no) {
            //    debugger;
            //    console.log(email_no);
            //    $http.get(ENV.apiUrl + "api/EmailTransaction/getfullmessage?email_no=" + email_no).then(function (res) {
            //        $scope.display = true;
            //        $scope.obj1 = res.data;
            //        console.log(res.data);
            //        $scope.eDetail = true;
            //        $scope.email = false;
            //        //console.log($scope.visible);
            //        //console.log($scope.visible[0]);

            //    });


            //}

            $scope.back = function () {
                $scope.eDetail = false;
                $scope.email = true;
            }


        }]);
})();

