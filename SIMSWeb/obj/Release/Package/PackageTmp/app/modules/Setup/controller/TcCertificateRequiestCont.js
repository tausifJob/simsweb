﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TcCertificateRequiestCont',
          ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

              console.clear();

              $scope.pagesize = "5";
              $scope.pageindex = "1";

              // $scope.global_Search_click1 = function () {
              $rootScope.visible_stud = true;
              //$rootScope.visible_parent = false;
              //$rootScope.visible_search_parent = false;
              //$rootScope.visible_teacher = false;
              //$rootScope.visible_User = true;
              //$rootScope.visible_Employee = false;
              $rootScope.chkMulti = false;

              $timeout(function () {
                  $("#table").tableHeadFixer({ 'top': 1 });
              }, 100);

              var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd');
              $scope.dt = {
                  sims_tc_certificate_request_date: dateyear,
              }

              $scope.size = function (str) {
                  $scope.pagesize = str;
                  $scope.currentPage = 1;
                  $scope.numPerPage = str;
                  console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
              }

              $scope.index = function (str) {
                  $scope.pageindex = str;
                  $scope.currentPage = str;
                  console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                  main.checked = false;
                  $scope.CheckAllChecked();
              }

              $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

              $scope.makeTodos = function () {
                  var rem = parseInt($scope.totalItems % $scope.numPerPage);
                  if (rem == '0') {
                      $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                  }
                  else {
                      $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                  }

                  var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                  var end = parseInt(begin) + parseInt($scope.numPerPage);

                  console.log("begin=" + begin); console.log("end=" + end);

                  $scope.filteredTodos = $scope.todos.slice(begin, end);
              };

              $('*[data-datepicker="true"] input[type="text"]').datepicker({
                  todayBtn: true,
                  orientation: "top left",
                  autoclose: true,
                  todayHighlight: true,
                  format: 'yyyy-mm-dd'
              });

              $scope.showdata = function () {
                  $http.get(ENV.apiUrl + "api/StudentReport/getTcCertificateRequiest").then(function (res1) {
                      $scope.obj = res1.data;
                      $scope.totalItems = $scope.obj.length;
                      $scope.todos = $scope.obj;
                      $scope.makeTodos();
                      if ($scope.obj.length == 0) {
                          //swal({ title: 'Alert', text: "No Record Found...", showCloseButton: true, width: 450, height: 200 });
                          $scope.tablehide = false;

                      }
                      else {
                          $scope.tablehide = true;
                      }
                  })
              }

              $scope.showdata();


              $scope.Show = function () {
                  $http.get(ENV.apiUrl + "api/StudentReport/getTcCertificateRequiestDate?date_wisesearch=" + $scope.dt.sims_tc_certificate_request_date).then(function (res1) {
                      $scope.obj = res1.data;
                      $scope.totalItems = $scope.obj.length;
                      $scope.todos = $scope.obj;
                      $scope.makeTodos();
                      if ($scope.obj.length == 0) {
                          swal({ title: 'Alert', text: "No Record Found...", showCloseButton: true, width: 450, height: 200 });
                          $scope.tablehide = false;

                      }
                      else {
                          $scope.tablehide = true;
                      }
                  })
              }

              $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                  $('input[type="text"]', $(this).parent()).focus();
              });

              $scope.SearchEnroll = function () {
                  $scope.global_Search_click();
                  $('#Global_Search_Modal').modal({ backdrop: "static" });
              }

              $scope.$on('global_cancel', function () {
                  if ($scope.SelectedUserLst.length > 0) {
                      $scope.edt =
                          {
                              sims_enroll_number: $scope.SelectedUserLst[0].s_enroll_no,
                              sims_student_name: $scope.SelectedUserLst[0].name,
                              sims_cur_code: $scope.SelectedUserLst[0].s_cur_code,
                              sims_academic_year: $scope.SelectedUserLst[0].sims_academic_year,
                          }
                  }
              });

              $scope.Save = function (MyForm) {
                  debugger;
                  if (MyForm) {
                      $http.post(ENV.apiUrl + "api/StudentReport/GetTCappliedStatus?enroll=" + $scope.edt.sims_enroll_number + "&curr=" + $scope.edt.sims_tc_certificate_cur_code).then(function (check) {

                          $scope.msg = check.data;
                          //$scope.msg1 = $scope.msg;
                          if ($scope.msg == true) {
                              swal('', 'Certificate Already Issued for this Student');
                              return;
                          }
                      });
                      var user = $rootScope.globals.currentUser.username;
                      var Savedata = [];
                      var obj = ({
                          'sims_enroll_number': $scope.edt.sims_enroll_number,
                          'sims_tc_certificate_reason': $scope.edt.sims_tc_certificate_reason,
                          'sims_tc_certificate_requested_by': user,
                          'sims_tc_certificate_request_date': $scope.dt.sims_tc_certificate_request_date,
                          'sims_cur_code': $scope.edt.sims_cur_code,
                          'sims_academic_year': $scope.edt.sims_academic_year,
                          opr: 'I',
                      });

                      Savedata.push(obj);

                      $http.post(ENV.apiUrl + "api/StudentReport/CUDTccertificaterequiest", Savedata).then(function (msg) {

                          $scope.msg1 = msg.data;
                          if ($scope.msg1.strMessage != undefined) {
                              if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                  swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                                  $scope.showdata();
                                  $scope.Reset();
                                  $scope.currentPage = true;
                              }
                          }
                          else {
                              swal({ title: "Alert", text: "Record Not Inserted...", showCloseButton: true, width: 380, });
                          }
                      });
                  }
              }

              $scope.searched = function (valLists, toSearch) {
                  return _.filter(valLists,
                  function (i) {
                      return searchUtil(i, toSearch);
                  });
              };

              $scope.search = function () {
                  $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                  $scope.totalItems = $scope.todos.length;
                  $scope.currentPage = '1';
                  if ($scope.searchText == '') {
                      $scope.todos = $scope.obj;
                  }
                  $scope.makeTodos();
              }

              function searchUtil(item, toSearch) {
                  return (item.sims_tc_certificate_request_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_tc_certificate_request_status.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sager == toSearch) ? true : false;
              }

              $scope.clear = function () {
                  $scope.edt = {
                      sims_enroll_number: '',
                      sims_tc_certificate_reason: '',
                  }
                  $scope.edt = '';
                  $scope.searchText = '';
              }

              $scope.Reset = function () {
                  debugger;
                  $scope.clear();
                  $scope.tablehide = true;
              }

              //Pending, Cancelled and Approved

              $scope.cancel_adm = function (info) {
                  debugger
                  $state.go('main.Sim622', { data: info });
              }
              $scope.viewDetails = function (clear) {
                  debugger;
                  console.log($scope.clear)
                  //$state.go('main.CancelAdmissionClearFees', { data: $scope.info });

                  $('#myModal1').modal('show');
                  $http.get(ENV.apiUrl + "api/StudentReport/GetStatusforStudent?enroll_no=" + clear.sims_enroll_number).then(function (res) {
                      debugger
                      $scope.clear = res.data;
                      $scope.totalItems = $scope.clear.length;
                      $scope.todos = $scope.clear;
                      $scope.makeTodos();
                      $scope.clearance = true;
                  });
              }

              $scope.modal_cancel = function () {
                  //$scope.j = '';
                  $scope.clear = '';
                  $scope.showdata();
              }
          }]);
})();

