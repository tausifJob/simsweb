﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SubjectGroupCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.subgrp_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.edit_code = false;

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.obj2 = res.data;
                console.log($scope.obj2);
            });

            $http.get(ENV.apiUrl + "api/common/SubjectGroup/getSubjectGroup").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.subgrp_data = res.data;
                $scope.totalItems = $scope.subgrp_data.length;
                $scope.todos = $scope.subgrp_data;
                $scope.makeTodos();
                $scope.grid = true;
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edit_code = true;
                $scope.edt =
                    {
                        sims_attendance_cur_code: str.sims_attendance_cur_code,
                        subject_group_code: str.subject_group_code,
                        subject_group_name_eng: str.subject_group_name_eng,
                        subject_group_name_ar: str.subject_group_name_ar,
                        subject_group_name_fr: str.subject_group_name_fr,
                        subject_group_name_ot: str.subject_group_name_ot,
                    }

                var v = document.getElementById('txt_grp_code');
                v.disabled = true;
            }

            $scope.Save = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        $http.post(ENV.apiUrl + "api/common/SubjectGroup/CheckSubjectGroup?SubjectGroup=" + $scope.edt.subject_group_code + "&cur_code=" + $scope.edt.sims_attendance_cur_code).then(function (res) {
                            $scope.msg1 = res.data;
                            console.log($scope.msg1);
                            if ($scope.msg1.status == true) {
                                swal({ title: "Alert", text: "Subject Group Already Exists", showCloseButton: true, width: 380, });
                                $scope.edt = "";
                            }
                            else {
                                var data = ({
                                    sims_attendance_cur_code: $scope.edt.sims_attendance_cur_code,
                                    subject_group_code: $scope.edt.subject_group_code,
                                    subject_group_name_eng: $scope.edt.subject_group_name_eng,
                                    subject_group_name_ar: $scope.edt.subject_group_name_ar,
                                    subject_group_name_fr: $scope.edt.subject_group_name_fr,
                                    subject_group_name_ot: $scope.edt.subject_group_name_ot,
                                    opr: 'I'
                                });

                                data1.push(data);


                                $http.post(ENV.apiUrl + "api/common/SubjectGroup/CUDSubjectGroup", data1).then(function (res) {
                                    $scope.display = true;
                                    $scope.msg1 = res.data;
                                    if ($scope.msg1 == true) {
                                        swal({ title: "Alert", text: "Subject Group Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else {
                                        swal({ title: "Alert", text: "Subject Group Not Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                });
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }

                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/common/SubjectGroup/getSubjectGroup").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.subgrp_data = res.data;
                    $scope.totalItems = $scope.subgrp_data.length;
                    $scope.todos = $scope.subgrp_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    var data = ({
                        sims_attendance_cur_code: $scope.edt.sims_attendance_cur_code,
                        subject_group_code: $scope.edt.subject_group_code,
                        subject_group_name_eng: $scope.edt.subject_group_name_eng,
                        subject_group_name_ar: $scope.edt.subject_group_name_ar,
                        subject_group_name_fr: $scope.edt.subject_group_name_fr,
                        subject_group_name_ot: $scope.edt.subject_group_name_ot,
                        opr: 'U'
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/SubjectGroup/CUDSubjectGroup", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Subject Group Data Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal({ title: "Alert", text: "Subject Data Not Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                    });
                }
            }

            $scope.Delete = function () {
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i + $scope.filteredTodos[i].subject_group_code);

                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sims_attendance_cur_code': $scope.filteredTodos[i].sims_attendance_cur_code,
                            'subject_group_code': $scope.filteredTodos[i].subject_group_code,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/common/SubjectGroup/CUDSubjectGroup", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Subject Group Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Already Mapped.Can't be Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i + $scope.filteredTodos[i].subject_group_code);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }

            }

            $scope.New = function () {
                $scope.edit_code = false;
                var autoid;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = [];
                //var v = document.getElementById('txt_grp_code');
                // v.disabled = false;

                $http.get(ENV.apiUrl + "api/common/SubjectGroup/getAutoGeneratesubjectgrpCode").then(function (res) {
                    autoid = res.data;
                    $scope.edt['subject_group_code'] = autoid;
                });
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].subject_group_code);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i+ $scope.filteredTodos[i].subject_group_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.subgrp_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.subgrp_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].subject_group_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.subject_group_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.subject_group_name_eng.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

        }])
})();