﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('RoleApplicationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            //          var countries = [
            //{ "name": "Afghanistan", "code": "AF" },
            //{ "name": "Albania", "code": "AF" },
            //{ "name": "Algeria", "code": "AF" },
            //{ "name": "Zimbabwe", "code": "ZW" }
            //          ];

            //          // Dynamically generated list of matched countries

            //          var matches = [
            //            { "name": "Albania" },
            //            { "name": "Ghana" },
            //            { "name": "Zimbabwe", "code": "ZW" }
            //          ];

            // Rejecting the countries that have "some" (at least one) match in the other list.

            $scope.compareArrays = function (countries, matches) {
                //function compareArrays(countries, matches) {
                return _.reject(countries, function (country) {
                    return _.some(matches, function (match) {
                        return country.app_Code === match.app_Code;
                    });
                });
            };

            //var s= $scope.compareArrays(countries, matches)

            //console.log(s);
            var theme = '';
            var apcode = [];

            console.log($rootScope.globals.currentUser.username);
            $http.get(ENV.apiUrl + "api/common/ApplicationRoleMapping/getAllRoleCode").then(function (res) {

                $scope.role = res.data;
                console.log($scope.role);

            });


            $http.get(ENV.apiUrl + "api/common/ApplicationRoleMapping/getAllModules").then(function (res) {

                $scope.items = res.data;
                console.log($scope.items);

                $("#module").jqxListBox({ source: res.data, width: '100%', height: 300, theme: 'energyblue' });

            });



            $scope.role_click = function (str) {
                apcode = [];
                $http.get(ENV.apiUrl + "api/common/ApplicationRoleMapping/getUserApplicationsNew?roleCode=" + str).then(function (res) {



                    if (res.data.length > 0) {
                        $scope.appl = res.data;
                        $scope.old_app = angular.copy(res.data)
                    }
                    else {
                        $scope.appl = undefined;
                    }

                    $("#SelectedApplication").jqxListBox({ allowDrop: true, allowDrag: true, displayMember: 'app_nm', valueMember: 'app_Code', source: $scope.appl, width: '100%', height: 300, theme: 'energyblue' });

                    var items = $("#SelectedApplication").jqxListBox('getItems');
                    for (var i = 0; i < items.length; i++) {
                        apcode.push(items[i].value);

                        console.log(items[i].label);
                    }
                });

            }
            $scope.module_click = function (str) {


            }


            $scope.save = function () {
                $('#loader').modal({ backdrop: 'static', keyboard: false });

                var insert = [];
                var remove = [];
                console.log('save');
                $scope.new_app = [];
                var items = $("#SelectedApplication").jqxListBox('getItems');
                for (var i = 0; i < items.length; i++) {
                    insert.push(items[i].value);
                    //var value = items[i].value;
                    console.log(items[i].label);
                    var data = {
                        app_nm: items[i].label,
                        app_Code: items[i].value
                    }
                    $scope.new_app.push(data);
                }
                debugger;
                var s = $scope.compareArrays($scope.old_app, $scope.new_app)
                console.log('diff')
                if (s.length > 0) {
                    console.log(s)
                    for (var i = 0; i < s.length; i++) {
                        remove.push(s[i].app_Code)
                    }
                }
                else {
                    remove = [];
                }
                var selectItem = $("#Application").jqxListBox('getSelectedItem');
                //  console.log(selectItem.value);

                for (var i = 0; i < $scope.role.length; i++) {
                    if ($scope.comn_role_code == $scope.role[i].comn_role_code) {
                        $scope.role_code = $scope.role[i].comn_role_code_o;
                        break;
                    }
                }


                var data3 = {
                    rolenm: $scope.role_code,
                    app_Code: remove,
                    opr: 'D'
                }

                $http.post(ENV.apiUrl + "api/common/ApplicationRoleMapping/InsertComn_role_App_new", data3).then(function (res) {

                    var data1 = {
                        rolenm: $scope.role_code,
                        app_Code: insert,
                        opr: 'I'
                    }
                    $http.post(ENV.apiUrl + "api/common/ApplicationRoleMapping/InsertComn_role_App_new", data1).then(function (res) {
                        //$scope.showMessage = true;
                        //$scope.messageClass = "alert-success";
                        if (res.data) {
                            $('#loader').modal('hide');
                            $(".modal-backdrop").removeClass("modal-backdrop");
                            swal({ title: "Alert", text: "Record Updated Sucessfully.." });
                        }
                        else {
                            $('#loader').modal('hide');
                            $(".modal-backdrop").removeClass("modal-backdrop");

                            swal({ title: "Alert", text: "Record  Updated Sucessfully." });

                        }

                        $("#SelectedApplication").jqxListBox('clear');
                        $("#Application").jqxListBox('clear');
                        $scope.comn_role_code = '0';
                    });


                });




            }


            $('#module').bind('select', function (event) {

                var args = event.args;
                var item = $('#module').jqxListBox('getItem', args.index);

                if (item != null) {
                    var datasrc = [];
                    debugger
                    console.log(item.label);
                    $http.get(ENV.apiUrl + "api/common/ApplicationRoleMapping/getAllApplicationsUser?flag=false&modNames=" + item.label + "&rolename=" + $scope.comn_role_code + "&username=" + $rootScope.globals.currentUser.username).then(function (res) {
                        if (res.data.length > 0) {
                            datasrc = res.data;
                            console.log(datasrc);
                        }
                        else {
                            datasrc = undefined;
                        }
                        $("#Application").jqxListBox({ allowDrop: true, allowDrag: true, source: datasrc, displayMember: 'app_nm', valueMember: 'app_Code', width: '100%', height: 300, theme: 'energyblue' });

                        //$("#listbox").jqxListBox({ multiple: true, displayMember: 'name', width: 200, height: 200, theme: theme });
                        //$("#listbox").jqxListBox({ source: dataAdapter });

                    });
                }
            });


            var data1 = [


            ];
            $("#Application").jqxListBox({ allowDrop: true, allowDrag: true, source: data1, width: '100%', height: 300, theme: 'energyblue' });

            $("#SelectedApplication").jqxListBox({ allowDrop: true, allowDrag: true, source: data1, width: '100%', height: 300, theme: 'energyblue' });

        }]);
})();