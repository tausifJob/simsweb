﻿(function () {
    'use strict';
    var main;
    var opr;
    var data1 = [];
    var deleteleave = [];
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmailCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.Email = true;
            $scope.EMailOperation = false;
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.check_all();
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                console.log("begin=" + begin); console.log("end=" + end);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $('#text-editor').wysihtml5();

            $scope.New = function () {

                $scope.newmode = true;
                $scope.editmode = false;
                opr = 'S';
                $scope.readonly = false;
                $scope.Email = false;
                $scope.EMailOperation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                //$scope.edt.sims_msg_subject = "";
                //$scope.edt.sims_msg_signature = "";
                //$scope.edt.sims_msg_status = "";
                $('#text-editor').data("wysihtml5").editor.clear();
            }

            $scope.cancel = function () {
                $scope.Email = true;
                $scope.EMailOperation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $('#text-editor').data("wysihtml5").editor.clear();
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.EmailData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.EmailData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.check_all();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                    item.sims_msg_subject.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_msg_body.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_msg_signature.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_msg_signature == toSearch) ? true : false;
            }

            $http.get(ENV.apiUrl + "api/emailtemplate/getEmailDetail").then(function (Email_Data) {
                debugger;
                $scope.EmailData = Email_Data.data;
                $scope.totalItems = $scope.EmailData.length;
                $scope.todos = $scope.EmailData;
                $scope.makeTodos();
                console.log($scope.EmailData);
            });

            $http.get(ENV.apiUrl + "api/emailtemplate/getApplicationCode").then(function (Application_Data) {
                $scope.ApplicationData = Application_Data.data;
            });
            $scope.ApplicationCode = function () {
                $http.get(ENV.apiUrl + "api/emailtemplate/getApplicationCode").then(function (Application_Data) {
                    $scope.ApplicationData = Application_Data.data;
                });
            }

            $scope.getModName = function (str) {
                debugger
                $http.get(ENV.apiUrl + "api/emailtemplate/getModuleCode?modecode=" + str).then(function (mname) {
                    $scope.moduleName = mname.data;
                });
            }

            //$scope.getAllModName = function () {
            //    debugger
            //    $http.get(ENV.apiUrl + "api/emailtemplate/getAllModuleCode").then(function (mname) {
            //        $scope.moduleName = mname.data;
            //    });
            //}

            $scope.Save = function (myForm) {
                if (myForm) {
                    debugger

                    var sims_body = $('#text-editor').val();
                    var data = {
                        comn_mod_code: $scope.edt.comn_mod_code,
                        comn_appl_code: $scope.edt.comn_appl_code,
                        sims_msg_type: $scope.edt.sims_msg_type,
                        sims_msg_body: sims_body,
                        sims_msg_subject: $scope.edt.sims_msg_subject,
                        sims_msg_signature: $scope.edt.sims_msg_signature,
                        sims_msg_status: $scope.edt.sims_msg_status,
                        opr: 'I',
                    }

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/emailtemplate/EmailCUD?simsobj=", data1).then(function (msg) {
                        $scope.BoardData1 = false;
                        $http.get(ENV.apiUrl + "api/emailtemplate/getEmailDetail").then(function (Email_Data) {
                            $scope.EmailData = Email_Data.data;
                            $scope.totalItems = $scope.EmailData.length;
                            $scope.todos = $scope.EmailData;
                            $scope.makeTodos();
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                            }
                        });
                    });
                    $scope.Email = true;
                    $scope.EMailOperation = false;
                    data1 = [];
                }
            }

            $scope.up = function (str) {
                debugger;
                // $scope.getAllModName();
                $scope.ApplicationCode();
                $scope.getModName(str.sims_msg_mod_code);
                opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.Email = false;
                $scope.EMailOperation = true;
                $scope.edt = {

                    comn_mod_code: str.sims_msg_mod_code
                            , sims_msg_sr_no: str.sims_msg_sr_no
                            , comn_appl_code: str.sims_msg_appl_code
                             , sims_msg_type: str.sims_msg_type
                              , sims_msg_subject: str.sims_msg_subject
                              , sims_msg_signature: str.sims_msg_signature
                              , sims_msg_status: str.sims_msg_status
                };

                $('#text-editor').data('wysihtml5').editor.setValue(str.sims_msg_body)
            }

            $scope.Update = function (myForm) {
                if (myForm) {
                    debugger
                    var sims_body = $('#text-editor').val();
                    var data = {
                        sims_msg_body: sims_body,
                        sims_msg_mod_code: $scope.edt.comn_mod_code,
                        sims_msg_sr_no: $scope.edt.sims_msg_sr_no,
                        comn_appl_code: $scope.edt.comn_appl_code,
                        sims_msg_type: $scope.edt.sims_msg_type,
                        sims_msg_subject: $scope.edt.sims_msg_subject,
                        sims_msg_signature: $scope.edt.sims_msg_signature,
                        sims_msg_status: $scope.edt.sims_msg_status,
                        opr: 'U',
                    }
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/emailtemplate/EmailCUD?simsobj=", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.BoardData1 = false;
                        $http.get(ENV.apiUrl + "api/emailtemplate/getEmailDetail").then(function (Email_Data) {
                            $scope.EmailData = Email_Data.data;
                            $scope.totalItems = $scope.EmailData.length;
                            $scope.todos = $scope.EmailData;
                            $scope.makeTodos();
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                            }
                        });

                    })
                    $scope.Email = true;
                    $scope.EMailOperation = false;
                    data1 = [];
                }
            }

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_msg_sr_no + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_msg_sr_no + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.check_once = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }

            $scope.deleterecord = function () {
                debugger;
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_msg_sr_no + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_msg_sr_no': $scope.filteredTodos[i].sims_msg_sr_no,
                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        confirmButtonText: 'Yes',
                        showCancelButton: true,
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/emailtemplate/EmailCUD", deleteleave).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/emailtemplate/getEmailDetail").then(function (Email_Data) {
                                                $scope.EmailData = Email_Data.data;
                                                $scope.totalItems = $scope.EmailData.length;
                                                $scope.todos = $scope.EmailData;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/emailtemplate/getEmailDetail").then(function (Email_Data) {
                                                $scope.EmailData = Email_Data.data;
                                                $scope.totalItems = $scope.EmailData.length;
                                                $scope.todos = $scope.EmailData;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                        }
                                    });
                                }
                            });
                        }
                        else {
                            main = document.getElementById('all_chk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_msg_sr_no + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }


        }]
        )
})();
