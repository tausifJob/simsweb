﻿(function () {
    'use strict';
    var formdata = new FormData();
    var imagename = '';
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    var st = [];
    simsController.controller('UserHonourCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = "5";
            //$scope.pageindex = "1";
            $scope.pagesize1 = "5";
            $scope.pageindex1 = "1";
            $scope.student = true;
            $scope.pager = true;
            $scope.pager1 = true;
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/';

            $(function () {
                $('#cmb_section_code').multipleSelect({
                    width: '100%'
                });
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                1
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.StudenteData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.size1 = function (str) {
                if (str == "All") {
                    $scope.currentPage1 = '1';
                    $scope.filteredTodos1 = $scope.EmployeeData;
                    $scope.pager1 = false;
                }
                else {
                    $scope.pager1 = true;
                    $scope.pagesize1 = str;
                    $scope.currentPage1 = 1;
                    $scope.numPerPage1 = str;
                    $scope.makeTodos1();
                }
            }

            $scope.index1 = function (str) {
                $scope.pageindex1 = str;
                $scope.currentPage1 = str; console.log("currentPage1=" + $scope.currentPage1); $scope.makeTodos1();
            }

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;
                $scope.temp = { s_cur_code: $scope.curriculum[0].sims_cur_code };
                $scope.getAccYear();
            })

            $scope.getAccYear = function (curCode) {

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.temp.s_cur_code).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = {
                        s_cur_code: $scope.curriculum[0].sims_cur_code,
                        sims_academic_year: $scope.Acc_year[0].sims_academic_year
                    }
                    $scope.getGrade();
                });

            }

            $(function () {
                $('#cmb_grade_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.getGrade = function (cur_code, academic_year) {

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.s_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (res) {

                    $scope.grade = res.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $scope.getsection = function (cur_code, grade_code, academic_year) {


                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.temp.s_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Allsection) {

                    $scope.section1 = Allsection.data;
                    setTimeout(function () {

                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                })
            };

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;
            $scope.filteredTodos1 = [], $scope.currentPage1 = 1, $scope.numPerPage1 = 5, $scope.maxSize1 = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.makeTodos1 = function () {
                var rem1 = parseInt($scope.totalItems1 % $scope.numPerPage1);
                if (rem1 == '0') {
                    $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1);
                }
                else {
                    $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1) + 1;
                }
                var begin1 = (($scope.currentPage1 - 1) * $scope.numPerPage1);
                var end1= parseInt(begin1) + parseInt($scope.numPerPage1);

                console.log("begin1=" + begin1); console.log("end1=" + end1);

                $scope.filteredTodos1 = $scope.todos1.slice(begin1, end1);
            };

            $scope.Show_Data = function () {
                $scope.student_data = true;
                $scope.ImageView = false;
                $http.get(ENV.apiUrl + "api/UserHonour/getStudentsDetails?curcode=" + $scope.temp.s_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.temp.sims_academic_year + "&section=" + $scope.edt.sims_section_name).then(function (Student_Data) {
                    $scope.StudenteData = Student_Data.data;
                    $scope.totalItems = $scope.StudenteData.length;
                    $scope.todos = $scope.StudenteData;
                    $scope.makeTodos();
               
                    $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/';
                        if (Student_Data.data.length > 0){ }
                        else {
                            $scope.ImageView = true;
                        }
                    
                });
            }

            //for student
            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.StudenteData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.StudenteData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {

                /* Search Text in 2 fields */
                return (item.fullname.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_honor_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.gradesection.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.gradesection == toSearch) ? true : false;
            }

            //for employee
            $scope.searched1 = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil1(i, toSearch);
                });
            };

            $scope.search1 = function () {
                debugger
                $scope.todos1 = $scope.searched1($scope.EmployeeData, $scope.searchText);
                $scope.totalItems1 = $scope.todos1.length;
                $scope.currentPage1 = '1';
                if ($scope.searchText == '') {
                    $scope.todos1 = $scope.EmployeeData;
                }
                $scope.makeTodos1();
            }

            function searchUtil1(item, toSearch) {

                /* Search Text in 2 fields */
                return (item.emp_Name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.codp_dept_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.dg_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.dg_desc == toSearch) ? true : false;
            }

            //student data
            $scope.Reset = function () {
                $scope.student_data = false;
                $scope.StudenteData = [];
                $scope.edt.codp_dept_no = "";
                $scope.edt.dg_code = "";
                $("#cmb_grade_code").multipleSelect('clearSelection');
            }

            //employee data
            $scope.Reset1 = function () {
                $scope.edt.codp_dept_no = "";
                $scope.edt.dg_code = "";
                $scope.employee_data = false;
                $scope.EmployeeData = [];
            }

            $scope.Student = function () {
                $scope.student = true;
                $scope.employee_data = false;
                $scope.EmployeeData = [];
                $scope.employee = false;
                $scope.edt.codp_dept_no = "";
                $scope.edt.dg_code = "";
            }

            $scope.Employee = function () {
                $scope.employee = true;
                $scope.student_data = [];
                $scope.student_data = false;
                $scope.student = false;
            }

            $scope.Show_Data_Employee = function () {
                if ($scope.edt.codp_dept_no == undefined || $scope.edt.codp_dept_no == "") {
                    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please select Department", showCloseButton: true, width: 380, });
                }
                else if ($scope.edt.dg_code == undefined || $scope.edt.dg_code == "") {
                    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please select Designation", showCloseButton: true, width: 380, });
                }
                else {
                    debugger
                    $scope.employee_data = true;
                    $scope.ImageView = false;
                    $http.get(ENV.apiUrl + "api/UserHonour/getEmployeeDetails?dgcode=" + $scope.edt.dg_code + "&deptcode=" + $scope.edt.codp_dept_no + "&comp_code=" + comp_code + "&year=" + finance_year).then(function (employee_Data) {
                        $scope.EmployeeData = employee_Data.data;
                        $scope.totalItems1 = $scope.EmployeeData.length;
                        $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/';
                        $scope.todos1 = $scope.EmployeeData;
                        $scope.makeTodos1();
                        console.log($scope.EmployeeData);

                        if (employee_Data.data.length > 0) { }
                        else {
                            $scope.ImageView = true;
                        }
                    });
                }
            }

            $http.get(ENV.apiUrl + "api/UserHonour/getDepartmentDetails").then(function (res) {
                $scope.dept = res.data;
            })

            $http.get(ENV.apiUrl + "api/UserHonour/getDesginationDetails").then(function (res) {
                $scope.desg = res.data;
            })


        }])
})();
