﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    var main, del = [];
    simsController.controller('GradeLevelCont',
       ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
           $scope.operation = false;
           $scope.pagesize = "5";
           $scope.pageindex = "1";
           $scope.grid = true;

           $http.get(ENV.apiUrl + "api/CurLevelGrade/getCurLevelGrade").then(function (res) {
               $scope.obj = res.data;
               $scope.totalItems = $scope.obj.length;
               $scope.todos = $scope.obj;
               console.log($scope.totalItems);
               $scope.makeTodos();
               $scope.operation = false;
               $scope.grid = true;
           });

           $scope.currGradeLevelDisplay = function () {
               $http.get(ENV.apiUrl + "api/CurLevelGrade/getCurLevelGrade").then(function (res) {
                   $scope.obj = res.data;
                   $scope.totalItems = $scope.obj.length;
                   $scope.todos = $scope.obj;
                   console.log($scope.totalItems);
                   $scope.makeTodos();
                   $scope.operation = false;
                   $scope.grid = true;
               });
           }

           $timeout(function () {
               $("#fixTable").tableHeadFixer({ 'top': 1 });
           }, 100);

           $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

           $scope.makeTodos = function () {
               var rem = parseInt($scope.totalItems % $scope.numPerPage);
               if (rem == '0') {
                   $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
               }
               else {
                   $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
               }

               var begin = (($scope.currentPage - 1) * $scope.numPerPage);
               var end = parseInt(begin) + parseInt($scope.numPerPage);

               console.log("begin=" + begin); console.log("end=" + end);

               $scope.filteredTodos = $scope.todos.slice(begin, end);
           };

           $scope.searched = function (valLists, toSearch) {
               return _.filter(valLists,

               function (i) {
                   /* Search Text in all  fields */
                   return searchUtil(i, toSearch);
               });
           };

           $scope.search = function () {
               $scope.todos = $scope.searched($scope.obj, $scope.searchText);
               $scope.totalItems = $scope.todos.length;
               $scope.currentPage = '1';
               if ($scope.searchText == '') {
                   $scope.todos = $scope.obj;
               }
               $scope.makeTodos();
               main.checked = false;
               $scope.CheckAllChecked();
           }

           function searchUtil(item, toSearch) {
               /* Search Text in all 3 fields */
               return (item.level_grade_cur_short_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.level_grade_grade_code_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.level_grade_level_code_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.level_grade_grade_code_name == toSearch) ? true : false;
           }

           $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
               $scope.cur_obj = res.data;
           });

           $scope.getLevelNames = function (cur_code) {
               $http.get(ENV.apiUrl + "api/CurLevelGrade/getCuriculumLevel?cur_code=" + cur_code).then(function (res) {
                   $scope.level_names_obj = res.data;
               });

           }

           $scope.getAcdmYr = function (cur_code) {
               $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code).then(function (res) {
                   $scope.Acdm_yr_obj = res.data;
               });
           }

           $scope.getGradeNames = function (cur_code, acdm_yr) {
               $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + cur_code + "&academic_year=" + acdm_yr).then(function (res) {
                   $scope.gradeNames_obj = res.data;
               });
           }

           $scope.size = function (str) {
               console.log(str);
               $scope.pagesize = str;
               $scope.currentPage = 1;
               $scope.numPerPage = str;
               console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
           }

           $scope.index = function (str) {
               $scope.pageindex = str;
               $scope.currentPage = str;
               console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
               main.checked = false;
               $scope.CheckAllChecked();
           }

           $scope.CheckAllChecked = function () {
               main = document.getElementById('mainchk');
               if (main.checked == true) {
                   for (var i = 0; i < $scope.filteredTodos.length; i++) {
                       var v = document.getElementById($scope.filteredTodos[i].level_grade_grade_code_name+i);
                       v.checked = true;
                       $scope.row1 = 'row_selected';
                       $('tr').addClass("row_selected");
                   }
               }
               else {

                   for (var i = 0; i < $scope.filteredTodos.length; i++) {
                       var v = document.getElementById($scope.filteredTodos[i].level_grade_grade_code_name + i);
                       v.checked = false;
                       main.checked = false;
                       $scope.row1 = '';
                   }
               }

           }

           $scope.checkonebyonedelete = function () {

               $("input[type='checkbox']").change(function (e) {
                   if ($(this).is(":checked")) {
                       $(this).closest('tr').addClass("row_selected");
                       $scope.color = '#edefef';
                   } else {
                       $(this).closest('tr').removeClass("row_selected");
                       $scope.color = '#edefef';
                   }
               });

               main = document.getElementById('mainchk');
               if (main.checked == true) {
                   main.checked = false;
                   $("input[type='checkbox']").change(function (e) {
                       if ($(this).is(":checked")) {
                           $(this).closest('tr').addClass("row_selected");
                       }
                       else {
                           $(this).closest('tr').removeClass("row_selected");
                       }
                   });
               }
           }

           $scope.OkDelete = function () {
            
               var deletefin = [];
               $scope.flag = false;
               for (var i = 0; i < $scope.filteredTodos.length; i++) {
                   var v = document.getElementById($scope.filteredTodos[i].level_grade_grade_code_name + i);
                   if (v.checked == true) {
                       $scope.flag = true;
                       var deletemodulecode = ({
                           'sims_cur_code': $scope.filteredTodos[i].level_grade_cur_code,
                           'sims_cur_level_code': $scope.filteredTodos[i].level_grade_level_code,
                           'sims_academic_year': $scope.filteredTodos[i].level_grade_academic_year,
                           'sims_grade_code': $scope.filteredTodos[i].level_grade_grade_code,
                           'opr': "D"
                       });
                       deletefin.push(deletemodulecode);
                   }
               }
               if ($scope.flag) {
                   swal({
                       title: '',
                       text: "Are you sure you want to Delete?",
                       showCloseButton: true,
                       showCancelButton: true,
                       confirmButtonText: 'Yes',
                       width: 380,
                       cancelButtonText: 'No',

                   }).then(function (isConfirm) {
                       if (isConfirm) {
                           $http.post(ENV.apiUrl + "api/CurLevelGrade/CUDCurGradeLevel", deletefin).then(function (res) {
                               $scope.msg1 = res.data;
                               if ($scope.msg1 == true) {
                                   swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                       if (isConfirm) {
                                           $scope.currGradeLevelDisplay();
                                           main = document.getElementById('mainchk');
                                           if (main.checked == true) {
                                               main.checked = false;
                                               $scope.row1 = '';
                                           }
                                           $scope.currentPage = true;
                                       }
                                   });
                               }
                               else {
                                   swal({ title: "Alert", text: "Record Not Deleted ", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                       if (isConfirm) {
                                           $scope.currGradeLevelDisplay();
                                           main = document.getElementById('mainchk');
                                           if (main.checked == true) {
                                               main.checked = false;
                                               $scope.row1 = '';
                                           }
                                           $scope.currentPage = true;
                                       }
                                   });
                               }

                           });
                       }
                       else {
                           main = document.getElementById('mainchk');
                           if (main.checked == true) {
                               main.checked = false;
                           }
                           for (var i = 0; i < $scope.filteredTodos.length; i++) {
                               var v = document.getElementById($scope.filteredTodos[i].level_grade_grade_code_name + i);
                               if (v.checked == true) {
                                   v.checked = false;
                                   $('tr').removeClass("row_selected");
                               }
                           }
                       }
                   });
               }
               else {
                   swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
               }
               $scope.currentPage = str;
           }

           var datasend = [];
           $scope.save = function (isvalid) {
               if (isvalid) {
                   debugger;
                   var f = false;
                   for (var k = 0; k < $scope.obj.length; k++) {
                       if ($scope.obj[k].level_grade_cur_code == $scope.temp.sims_cur_code && $scope.obj[k].level_grade_academic_year == $scope.temp.sims_academic_year &&
                            $scope.obj[k].level_grade_level_code == $scope.temp.sims_cur_level_code &&
                            $scope.obj[k].level_grade_grade_code == $scope.temp.sims_grade_code) {
                           swal({ title: "Alert", text: "Record Already Present", showCloseButton: true, width: 380, });
                           f = true;
                           break;
                       }
                   }
                   if (!f) {
                       var data = $scope.temp;
                       data.opr = 'I';
                       datasend.push(data);

                       $http.post(ENV.apiUrl + "api/CurLevelGrade/CUDCurGradeLevel", datasend).then(function (res) {
                           $scope.msg1 = res.data;

                           if ($scope.msg1 == true) {
                               swal({ title: "Alert", text: "Record Inserted Successfully", showCloseButton: true, width: 380, });
                           }
                           else {
                               swal({ title: "Alert", text: "Record Not Inserted", showCloseButton: true, width: 380, });
                           }
                           $scope.currGradeLevelDisplay();
                           main = document.getElementById('mainchk');
                           if (main.checked == true) {
                               main.checked = false;
                               $scope.row1 = '';
                           }
                           $scope.currentPage = true;
                       });
                       datasend = [];
                       $scope.operation = false;
                       $scope.grid = true;
                   }
                   else {
                       swal({ title: "Alert", text: "Record Already Present", showCloseButton: true, width: 380, });
                   }
               }
           }

           $scope.New = function () {
               $scope.grid = false;
               $scope.operation = true;
               $scope.temp = "";
               $scope.Myform.$setPristine();
               $scope.Myform.$setUntouched();
           }

           $scope.cancel = function () {
               $scope.grid = true;
               $scope.operation = false;
               $scope.temp = "";
               $scope.Myform.$setPristine();
               $scope.Myform.$setUntouched();
           }

       }]);
})();