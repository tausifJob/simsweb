﻿
(function () {
    'use strict';

    var main;
    var subject_code = [], subject_code1 = [], sib_enroll_code = [];;

    var cur_code;
    var section_code, sectioncodemodal1;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CreateEditSiblingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.parent = false;
            $scope.student = false;
            $scope.buttons = false;

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;

            });

            $scope.SearchSudentModal = function () {

                $scope.searchtable = false;
                $scope.buttons = false;

                $scope.busy = false;


                $scope.temp = '';

                $scope.sibling_result = '';
                $('.nav-tabs a[href="#Student_Details"]').tab('show')
                $scope.active = 'active';
                $('#MyModal').modal('show');

                $scope.searchtable = false;
            };

            $scope.SearchParentModal = function () {
                $scope.parenttable = false;
                $scope.buttons = false;
                $scope.busy = false;

                $scope.edt = '';
                $scope.parent_result = '';

                $('.nav-tabs a[href="#Parent_information"]').tab('show')

                $('#MyModal').modal('show');
            }

            var checkglobalserch;
            $scope.GlobalSearch = function (str) {
              
                checkglobalserch = str;
                if (str == "P") {
                    $rootScope.visible_stud = false;
                    $rootScope.visible_parent=true;
                    $rootScope.visible_search_parent = false;
                    $rootScope.visible_teacher=false;
                    $rootScope.visible_User=false;
                    $rootScope.visible_Employee=false;
                    $rootScope.chkMulti = false;
                    $rootScope.visible_UnassignedStudent = false;
                    $rootScope.visible_UnassignedParent = true;
                    $scope.global_Search_click();
                    $('#Global_Search_Modal').modal({ backdrop: "static" });
                }
                else {
                    $rootScope.visible_UnassignedStudent = true;
                    $rootScope.visible_stud = true;
                    $rootScope.visible_parent = false;
                    $rootScope.visible_search_parent = false;
                    $rootScope.visible_teacher = false;
                    $rootScope.visible_User = false;
                    $rootScope.visible_Employee = false;
                    $rootScope.chkMulti = true;
                    $rootScope.visible_UnassignedParent = false;
                    $scope.global_Search_click();
                    $('#Global_Search_Modal').modal({ backdrop: "static" });

                }
              
            }

            $scope.$on('global_cancel', function (str) {
                debugger;
                console.log($scope.SelectedUserLst);
                if ($scope.SelectedUserLst.length > 0) {
                    if (checkglobalserch == "P") {
                        if ($scope.SelectedUserLst[0].tab == "tab8") {
                            $scope.SerchUnmappingParent($scope.SelectedUserLst);
                            $scope.sibling_result = '';
                            $scope.student = false;

                        }
                        else {
                           
                            $scope.SearchParentChild($scope.SelectedUserLst);
                        }
                    }
                    else {
                        if ($scope.SelectedUserLst[0].tab == "tab7") {
                            $scope.SerchUnmapingStudent($scope.SelectedUserLst);
                        }
                        else {
                            $scope.SearchStudentSibling($scope.SelectedUserLst);
                        }
                    }
                   // $scope.obj['em_first_name'] = $scope.SelectedUserLst[0].empName;
                    //$scope.obj['emp_code'] = $scope.SelectedUserLst[0].em_number;
                }
                // $scope.getstudentList();
            });

            $scope.SearchParent = function () {

                $scope.parenttable = false;
                $scope.busy = true;
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchParent?data=" + JSON.stringify($scope.edt)).then(function (Allparent) {

                    $scope.parent_result = Allparent.data;
                    $scope.parenttable = true;
                    $scope.busy = false;

                });

            }

            $scope.SearchSudent = function () {
                $scope.searchtable = false;
                $scope.busy = true;
                $scope.sibling_result
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.temp)).then(function (Allstudent) {

                    $scope.student = Allstudent.data;
                    $scope.searchtable = true;
                    $scope.busy = false;
                    $scope.parenttable = true;


                });
            }

            $scope.SearchStudentSibling = function (str) {
              
                var enrollmentno = '';
                $scope.sibling_result = '';
                $scope.unmapStudentresultData = '';
                $scope.UnMappedStudent = '';
                $scope.student_name = str[0].name;
                for (var i = 0; i < str.length; i++) {
                    enrollmentno += str[i].s_enroll_no + ',';
                }
                $scope.busy = true;

                $http.get(ENV.apiUrl + "api/createeditsibling/GetSims040?sib_enroll=" + enrollmentno).then(function (siblingresult) {
                    $scope.sibling_result_new = siblingresult.data;

                    if ($scope.sibling_result_new.length > 0) {
                        $scope.sibling_result = $scope.sibling_result_new;

                        for (var i = 0; i < $scope.sibling_result.length; i++) {
                            $scope.sibling_result[i].icon = "fa fa-plus-circle";
                        }
                        $scope.busy = false;
                        //$scope.parent = true;
                        $scope.student = true;
                        $scope.unmapedstudent = false;
                        $scope.buttons = true;

                    }
                    else {
                        swal({ text: 'Record Not Found', width: 380, showCloseButton: true });
                        $scope.student_name = '';
                        $scope.busy = false;
                        $scope.buttons = false;
                        $scope.unmapedstudent = false;
                        $scope.busy = false;
                        $scope.student = false;
                    }
                });
            }

            $scope.SerchUnmapingStudent = function (str) {
               
                var enrollmentno = '';
                $scope.sibling_result = '';
               
                $scope.student_name = str[0].name;
                for (var i = 0; i < str.length; i++) {
                    enrollmentno += str[i].s_enroll_no + ',';
                }
                $scope.busy = true;

                $http.get(ENV.apiUrl + "api/createeditsibling/GetunmappingStudent?studentid=" + enrollmentno).then(function (unmapStudentresult) {
                    $scope.unmapStudentresultData = unmapStudentresult.data;

                    if ($scope.unmapStudentresultData.length > 0) {
                        $scope.UnMappedStudent = $scope.unmapStudentresultData;

                        for (var i = 0; i < $scope.UnMappedStudent.length; i++) {
                            $scope.UnMappedStudent[i].icon = "fa fa-plus-circle";
                        }
                        $scope.busy = false;
                        $scope.unmapedstudent = true;
                        $scope.student = false;
                        $scope.buttons = true;

                    }
                    else {
                        swal({ text: 'Record Not Found', width: 380, showCloseButton: true });
                        $scope.student_name = '';
                        $scope.busy = false;
                        $scope.unmapedstudent = false;
                        $scope.buttons = false;
                        $scope.busy = false;
                        $scope.student = false;
                    }
                });
            }

            $scope.SearchParentChild = function (str) {
               
                var enrollmentno = '';
                $scope.busy = true;
                $scope.sibling_result1 = '';
                $scope.UnmapedparentResultData = '';
                $scope.UnMappedParent = '';
                $scope.parent_name = str[0].s_parent_name;
                for (var i = 0; i < str.length; i++) {
                    enrollmentno += str[i].s_enroll_no + ',';
                }
                $http.get(ENV.apiUrl + "api/createeditsibling/GetSims040?sib_enroll=" + enrollmentno).then(function (siblingresult) {
                    $scope.sibling_result_new1 = siblingresult.data;
                    if ($scope.sibling_result_new1.length > 0) {
                        $scope.sibling_result1 = $scope.sibling_result_new1;

                        for (var i = 0; i < $scope.sibling_result1.length; i++) {
                            $scope.sibling_result1[i].icon = "fa fa-plus-circle";
                        }

                        $scope.parent = true;
                        $scope.buttons = true;
                        $scope.Unmapedparent = false;
                        $scope.busy = false;
                    }
                    else {

                        swal({ text: 'Search Failed', width: 380, showCloseButton: true });
                        $scope.parent_name = '';
                        $scope.parent = false;
                        $scope.buttons = false;
                        $scope.Unmapedparent = false;
                        $scope.busy = false;
                    }
                })
            }

            $scope.SerchUnmappingParent = function (str) {
                debugger;
                var enrollmentno = '';
                $scope.busy = true;
                $scope.sibling_result = '';
                $scope.sibling_result1 = '';
                for (var i = 0; i < str.length; i++) {
                    enrollmentno = str[i].s_parent_id + ',';
                }
                $scope.parent_name = str[0].s_parent_name;
                $http.get(ENV.apiUrl + "api/createeditsibling/Getunmappingparent?parentid=" + enrollmentno).then(function (UnmapedparentResult) {
                   
                    $scope.UnmapedparentResultData = UnmapedparentResult.data;
                    if ($scope.UnmapedparentResultData.length > 0) {
                        $scope.UnMappedParent = $scope.UnmapedparentResultData;

                        for (var i = 0; i < $scope.UnMappedParent.length; i++) {
                            $scope.UnMappedParent[i].icon = "fa fa-plus-circle";
                        }

                        $scope.Unmapedparent = true;
                        $scope.parent = false;
                        $scope.buttons = true;
                        $scope.busy = false;
                    }
                    else {

                        swal({ text: 'No Record Found', width: 380, showCloseButton: true });
                        $scope.parent_name = '';
                        $scope.parent = false;
                        $scope.Unmapedparent = false;
                        $scope.buttons = false;
                        $scope.busy = false;
                    }
                })
            }

            $scope.Save = function () {
              
                sib_enroll_code = [];
                $scope.parent_name = '';
                $scope.student_name = '';
                for (var i = 0; i < $scope.sibling_result.length; i++) {
                    var t = $scope.sibling_result[i].sib_enroll;
                    var v = document.getElementById(t);

                    if (v.checked == true)

                        sib_enroll_code = sib_enroll_code + $scope.sibling_result[i].sib_enroll + ',';
                }

                var sibling_parent_code = $scope.sibling_result[0].parent_number;

                var data = ({
                    'sibling_student_enrollNo': sib_enroll_code,
                    'sibling_parent_code': sibling_parent_code,
                });

                $http.post(ENV.apiUrl + "api/createeditsibling/Sims040Insert", data).then(function (msg) {

                    var str = { s_enroll_no: $scope.sibling_result[0].sib_enroll }
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: 'Information Inserted Successfully', width: 300, showCloseButton: true });
                        main = document.getElementById('mainchk');
                        if (main.checked == true) {
                            main.checked = false;
                            $scope.row1 = '';
                        }
                        $scope.sibling_result = [];
                        $scope.SearchParentChild(str);
                    }
                    else {
                        swal({ text: 'Information Not Inserted', width: 300, showCloseButton: true });
                        main = document.getElementById('mainchk');
                        if (main.checked == true) {
                            main.checked = false;
                            $scope.row1 = '';
                            $scope.SearchParentChild(str);
                        }
                        $scope.sibling_result = '';
                    }


                });
            }

            $scope.ClearAll = function () {
                $scope.parent_name = '';
                $scope.student_name = '';
                $scope.sibling_result1 = [];
                $scope.sibling_result = [];
                $scope.UnMappedStudent = [];
                $scope.UnMappedParent = [];
                $scope.parent = false;
                $scope.buttons = false;
                $scope.Unmapedparent = false;
                $scope.unmapedstudent = false;

                $scope.student = false;

            }

            $scope.Update = function () {
               
                var flag1 = false;
                sib_enroll_code = [];
                if ($scope.sibling_result.length > 0) {
                    $scope.parent = true;
                    //$scope.student = false;
                    //$scope.buttons = false;

                    for (var i = 0; i < $scope.sibling_result.length; i++) {
                        var t = $scope.sibling_result[i].sib_enroll;
                        var v = document.getElementById(t);

                        if (v.checked == true) {
                            flag1 = true;
                            sib_enroll_code = sib_enroll_code + $scope.sibling_result[i].sib_enroll + ',';
                        }
                    }

                    var sibling_parent_code = $scope.sibling_result[0].parent_number;
                    var strNewParentID = $scope.sibling_result1[0].parent_number;
                    var data = ({
                        'sibling_student_enrollNo': sib_enroll_code,
                        'sims_sibling_old_parent_number': sibling_parent_code,
                        'sims_sibling_parent_number': strNewParentID
                    });

                    if (flag1) {
                        $http.post(ENV.apiUrl + "api/createeditsibling/Sims040Update", data).then(function (msg) {

                            var str = { s_enroll_no: $scope.sibling_result[0].sib_enroll }
                            $scope.msg1 = msg.data;

                            if ($scope.msg1 == true) {
                                swal({ text: 'Information Inserted Successfully', width: 300, showCloseButton: true });
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                    $scope.row1 = '';
                                }
                                $scope.sibling_result = [];
                                $scope.SearchParentChild(str);
                            }
                            else {
                                swal({ text: 'Information Not Inserted', width: 300, showCloseButton: true });
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                    $scope.row1 = '';
                                    $scope.SearchParentChild(str);
                                }
                                $scope.UnMappedStudent = '';
                                $scope.sibling_result1 = '';
                                $scope.sibling_result = '';
                                $scope.UnMappedParent = '';
                                $scope.parent_name = '';
                                $scope.student_name = '';
                            }
                        });
                    }
                    else {

                        swal({ text: 'please Select Record', width: 300, showCloseButton: true });
                    }
                }
                else if ($scope.sibling_result1.length > 0 && $scope.UnMappedStudent.length>0) {
                    for (var i = 0; i < $scope.UnMappedStudent.length; i++) {
                        var t = $scope.UnMappedStudent[i].sib_enroll + i;
                        var v = document.getElementById(t);

                        if (v.checked == true) {
                            flag1 = true;
                            sib_enroll_code = sib_enroll_code + $scope.UnMappedStudent[i].sib_enroll + ',';
                        }
                    }


                    var strNewParentID = $scope.sibling_result1[0].parent_number;
                    var data = ({
                        'sibling_student_enrollNo': sib_enroll_code,
                        'sims_sibling_parent_number': strNewParentID
                    });

                    if (flag1) {
                        $http.post(ENV.apiUrl + "api/createeditsibling/mappedStudenttomappedparent", data).then(function (msg) {
                            debugger;
                            // var str = { s_enroll_no: $scope.UnMappedStudent[0].sib_enroll }
                            $scope.msg1 = msg.data;

                            if ($scope.msg1 == true) {
                                swal({ text: 'Information Inserted Successfully', width: 300, showCloseButton: true });
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                    $scope.row1 = '';
                                }


                            }
                            else {
                                swal({ text: 'Information Not Inserted', width: 300, showCloseButton: true });
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                    $scope.row1 = '';

                                }
                                $$scope.UnMappedStudent = '';
                                $scope.sibling_result1 = '';
                                $scope.sibling_result = '';
                                $scope.UnMappedParent = '';
                                $scope.parent_name = '';
                                $scope.student_name = '';
                               
                            }
                        });
                    }
                    else {

                        swal({ text: 'please Select Record', width: 300, showCloseButton: true });
                    }
                }
                else {


                    for (var i = 0; i < $scope.UnMappedStudent.length; i++) {
                        var t = $scope.UnMappedStudent[i].sib_enroll + i;
                        var v = document.getElementById(t);

                        if (v.checked == true) {
                            flag1 = true;
                            sib_enroll_code = sib_enroll_code + $scope.UnMappedStudent[i].sib_enroll + ',';
                        }
                    }


                    var strNewParentID = $scope.UnMappedParent[0].parent_number;
                    var data = ({
                        'sibling_student_enrollNo': sib_enroll_code,
                        'sims_sibling_parent_number': strNewParentID
                    });

                    if (flag1) {
                        $http.post(ENV.apiUrl + "api/createeditsibling/mappedStudenttounmappedparent", data).then(function (msg) {

                            var str = { s_enroll_no: $scope.UnMappedStudent[0].sib_enroll }
                            $scope.msg1 = msg.data;

                            if ($scope.msg1 == true) {
                                swal({ text: 'Information Inserted Successfully', width: 300, showCloseButton: true });
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                    $scope.row1 = '';
                                }


                            }
                            else {
                                swal({ text: 'Information Not Inserted', width: 300, showCloseButton: true });
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                    $scope.row1 = '';

                                }
                               
                            }
                            $scope.UnMappedStudent = '';
                            $scope.sibling_result1 = '';
                            $scope.sibling_result = '';
                            $scope.UnMappedParent = '';
                            $scope.parent_name = '';
                            $scope.student_name = '';
                        });
                    }
                    else {

                        swal({ text: 'please Select Record', width: 300, showCloseButton: true });
                    }

                }
               // $scope.ClearAll();

            }

            $scope.Delete = function () {
                $scope.student_name = '';

                sib_enroll_code = [];

                for (var i = 0; i < $scope.sibling_result.length; i++) {
                    var t = $scope.sibling_result[i].sib_enroll;
                    var v = document.getElementById(t);

                    if (v.checked == true)

                        sib_enroll_code = sib_enroll_code + $scope.sibling_result[i].sib_enroll + ',';
                }

                var sibling_parent_code = $scope.sibling_result[0].parent_number;

                var data = ({
                    'sibling_student_enrollNo': sib_enroll_code,
                    'sims_sibling_parent_number': sibling_parent_code
                });

                $http.post(ENV.apiUrl + "api/createeditsibling/Sims040Delete", data).then(function (msg) {

                    $scope.msg1 = msg.data;
                    var student_name = '';

                    for (var i = 0; i < $scope.sibling_result.length; i++) {
                        sib_enroll_code = $scope.sibling_result[i].sib_enroll;
                        student_name = $scope.sibling_result[i].s_sname_in_english;
                    }

                    $scope.student_name = student_name;
                    if ($scope.msg1 == true) {

                        swal({ text: 'Information Deleted Successfully', width: 380, showCloseButton: true });
                        main = document.getElementById('mainchk');

                        if (main.checked == true) {
                            main.checked = false;
                            $scope.row1 = '';
                        }
                        var str = { s_enroll_no: sib_enroll_code }
                        $scope.SearchStudentSibling(str);
                        $scope.student = true;
                        $scope.buttons = true;
                    }
                    else {
                        swal({ text: 'Information Not Deleted', width: 380, showCloseButton: true });
                        main = document.getElementById('mainchk');
                        if (main.checked == true) {
                            main.checked = false;
                            $scope.row1 = '';
                        }
                    }
                });

            }
            var dom1;
            $scope.flag = true;

            $scope.expand = function (info, $event) {

                console.log(info);

                setTimeout(function () {
                    $scope.img_url = $scope.obj1.lic_website_url + '/Images/StudentImage/'
                    console.log($scope.img_url);
                }, 3000);

                if ($scope.flag == true) {
                    var sims_student_image = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/' + info.sib_photo;
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                    "<table class='inner-table' width='100%' cellpadding='5' cellspacing='0' style='border-color:red;border:solid;border-width:02px'>" +
                    "<tbody>" +
                    "<tr><td class='semi-bold' style='text-align: left; vertical-align: middle' rowspan='5'>" + "<img src='" + sims_student_image + "' style='width:100px;height:100px'>" + "</td> <td class='semi-bold'>" + "SIBLING NAME" + " </td>" + "<td>" + (info.sib_name) + "</td></tr>" +
                    "<tr><td class='semi-bold'>" + "CLASS" + " </td><td colsapn='1'>" + (info.sib_class) + " </td></tr>" +
                    "<tr><td class='semi-bold'>" + "PARENT NAME" + "</td> <td>" + (info.parent_name) + " </td></tr>" +
                    "<tr><td class='semi-bold'>" + "ADMISSION DATE" + "</td> <td>" + (info.sib_admiss_date) + " </td></tr>" +
                    "<tr><td class='semi-bold'>" + "GENDER" + "</td> <td>" + (info.sib_gender) + " </td></tr>" +
                    "</tbody>" +
                        "</table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {

                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.sibling_result.length; i++) {
                        $scope.sibling_result[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }
            };

            var dom;
            $scope.flag1 = true;

            $scope.expandUnmappedParent = function (info, $event) {

                if ($scope.flag1 == true) {
                    var sims_Parent_image = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/ParentImages/' + info.sims_parent_father_img;
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                    "<table class='inner-table' width='100%' cellpadding='5' cellspacing='0' style='border-color:red;border:solid;border-width:02px'>" +
                    "<tbody>" +
                    "<tr><td class='semi-bold' style='text-align: left; vertical-align: middle' rowspan='5'>" + "<img src='" + sims_Parent_image + "' style='width:100px;height:100px'>" + "</td> <td class='semi-bold'>" + "Natinality" + " </td>" + "<td>" + (info.sims_parent_father_nationality1_code) + "</td></tr>" +
                    "<tr><td class='semi-bold'>" + "Address Summary" + " </td><td colsapn='1'>" + (info.sims_parent_father_summary_address) + " </td></tr>" +

                    "<tr><td class='semi-bold'>" + "Contry" + "</td> <td>" + (info.sims_parent_father_country_code) + " </td></tr>" +
                    "<tr><td class='semi-bold'>" + "Passport No" + "</td> <td>" + (info.sims_parent_father_passport_number) + " </td></tr>" +
                    "</tbody>" +
                        "</table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag1 = false;
                }
                else {

                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.UnMappedParent.length; i++) {
                        $scope.UnMappedParent[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag1 = true;
                }

            };

            $scope.expandUnmappedStudent = function (info, $event) {


                if ($scope.flag == true) {
                    var sims_student_image = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/' + info.sib_photo;
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                    "<table class='inner-table' width='100%' cellpadding='5' cellspacing='0' style='border-color:red;border:solid;border-width:02px'>" +
                    "<tbody>" +
                    "<tr><td class='semi-bold' style='text-align: left; vertical-align: middle' rowspan='5'>" + "<img src='" + sims_student_image + "' style='width:100px;height:100px'>" + "</td> <td class='semi-bold'>" + "SIBLING NAME" + " </td>" + "<td>" + (info.sib_name) + "</td></tr>" +
                    "<tr><td class='semi-bold'>" + "CLASS" + " </td><td colsapn='1'>" + (info.sib_class) + " </td></tr>" +
                    "<tr><td class='semi-bold'>" + "ADMISSION DATE" + "</td> <td>" + (info.sib_admiss_date) + " </td></tr>" +
                    "<tr><td class='semi-bold'>" + "GENDER" + "</td> <td>" + (info.sib_gender) + " </td></tr>" +
                    "</tbody>" +
                        "</table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {

                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.UnMappedStudent.length; i++) {
                        $scope.UnMappedStudent[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }
            };

            $scope.expand1 = function (info, $event) {

                if ($scope.flag1 == true) {
                    var sims_student_image = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/' + info.sib_photo;
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                    "<table class='inner-table' width='100%' cellpadding='5' cellspacing='0' style='border-color:red;border:solid;border-width:02px'>" +
                    "<tbody>" +
                    "<tr><td class='semi-bold' style='text-align: left; vertical-align: middle' rowspan='5'>" + "<img src='" + sims_student_image + "' style='width:100px;height:100px'>" + "</td> <td class='semi-bold'>" + "SIBLING NAME" + " </td>" + "<td>" + (info.sib_name) + "</td></tr>" +
                    "<tr><td class='semi-bold'>" + "CLASS" + " </td><td colsapn='1'>" + (info.sib_class) + " </td></tr>" +
                    "<tr><td class='semi-bold'>" + "PARENT NAME" + "</td> <td>" + (info.parent_name) + " </td></tr>" +
                    "<tr><td class='semi-bold'>" + "ADMISSION DATE" + "</td> <td>" + (info.sib_admiss_date) + " </td></tr>" +
                    "<tr><td class='semi-bold'>" + "GENDER" + "</td> <td>" + (info.sib_gender) + " </td></tr>" +
                    "</tbody>" +
                        "</table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag1 = false;
                }
                else {

                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.sibling_result1.length; i++) {
                        $scope.sibling_result1[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag1 = true;
                }

            };

            $scope.CheckAllChecked = function () {

                sib_enroll_code = [];
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.sibling_result.length; i++) {
                        var t = $scope.sibling_result[i].sib_enroll;
                        var v = document.getElementById(t);
                        v.checked = true;
                        sib_enroll_code = sib_enroll_code + $scope.sibling_result[i].sib_enroll + ','
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.sibling_result.length; i++) {
                        var t = $scope.sibling_result[i].sib_enroll;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        sib_enroll_code = [];
                    }
                }
                console.log(sib_enroll_code);
            }

            $scope.CheckOnebyOneDelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
            }

            $scope.Cancel = function () {

                $scope.row1 = '';

                sib_enroll_code = [];

                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.sibling_result.length; i++) {
                        var t = $scope.sibling_result[i].sib_enroll;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
                else {

                    for (var i = 0; i < $scope.sibling_result.length; i++) {
                        var t = $scope.sibling_result[i].sib_enroll;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $timeout(function () {
                $("#Table3").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#Table4").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.reset = function () {

                $scope.temp = '';
                $scope.edt = '';
            }
        }])
})();