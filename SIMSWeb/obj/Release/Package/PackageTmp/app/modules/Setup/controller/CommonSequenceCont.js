﻿(function () {
    'use strict';
    var del = [];
    var oldvalue = [], appl_form_field, appl_parameter, appl_form_field_value1;
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CommonSequenceCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.grid = true;
            $scope.edit_data = false;
            $scope.sequencedata = [];
            var data1 = [];
            var deletecode = [];

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $http.get(ENV.apiUrl + "api/common/common_seq/getcomn004_show").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.sequencedata = res.data;
                $scope.totalItems = $scope.sequencedata.length;
                $scope.todos = $scope.sequencedata;
                $scope.makeTodos();

                for (var i = 0; i < $scope.totalItems; i++) {
                    $scope.sequencedata[i].icon = "fa fa-plus-circle";
                }
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $http.get(ENV.apiUrl + "api/common/ApplicationMaster/getModules").then(function (res) {
                $scope.module_data = res.data;
                console.log($scope.module_data);
            });

            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.cur_data = res.data;
                console.log($scope.cur_data);
            });

            //$scope.GetAacd_yr = function (cur) {
            //    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur).then(function (res) {
            //        $scope.acad = res.data;
            //        console.log($scope.acad);
            //    });
            //}

            $scope.GetAacd_yr = function (cur)
            {
                $http.get(ENV.apiUrl + "api/common/common_seq/getAllAcademicYear?cur=" + cur).then(function (res) {
                    $scope.acad = res.data;
                    console.log($scope.acad);
                });
            }

            $scope.GetApplication = function (modulecode) {
                $scope.appl_data = null;

                if (modulecode != undefined) {
                    $http.get(ENV.apiUrl + "api/common/ParameterMaster/getApplication?modCode=" + modulecode).then(function (res) {
                        $scope.appl_data = res.data;
                        console.log($scope.appl_data);
                    });
                }
            }


            $scope.edit = function (str)
            {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edit_data = true;
                $scope.edt =
                    {
                        comn_application_code: str.comn_application_code,
                        comn_mod_code: str.comn_mod_code,
                        comn_squence_code: str.comn_squence_code,
                        comn_description: str.comn_description,
                        comm_cur_code: str.comm_cur_code,
                        comn_academic_year: str.comn_academic_year,
                        comn_character_count: str.comn_character_count,
                        comn_character_squence: str.comn_character_squence,
                        comm_number_count: str.comm_number_count,
                        comn_number_squence: str.comn_number_squence,
                        comn_parameter_ref: str.comn_parameter_ref,
                    }
                $scope.GetApplication(str.comn_mod_code);
                $scope.GetAacd_yr(str.comm_cur_code);
            }


            $scope.New = function () {
                $scope.edit_data = false;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
            }

            $scope.Save = function (isvalidate) {
                var data1 = [];

                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = ({
                            comn_application_code: $scope.edt.comn_application_code,
                            comn_mod_code: $scope.edt.comn_mod_code,
                            comn_squence_code: $scope.edt.comn_squence_code,
                            comn_description: $scope.edt.comn_description,
                            comm_cur_code: $scope.edt.comm_cur_code,
                            comn_academic_year: $scope.edt.comn_academic_year,
                            comn_character_count: $scope.edt.comn_character_count,
                            comn_character_squence: $scope.edt.comn_character_squence,
                            comm_number_count: $scope.edt.comm_number_count,
                            comn_number_squence: $scope.edt.comn_number_squence,
                            comn_parameter_ref: $scope.edt.comn_parameter_ref,
                            opr: 'I'
                        });

                        data1.push(data);

                        $http.post(ENV.apiUrl + "api/common/common_seq/comn_comn004insert", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Common Sequence Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal({ title: "Alert", text: "Common Sequence Not Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.Update = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    var data = ({
                        comn_application_code: $scope.edt.comn_application_code,
                        comn_mod_code: $scope.edt.comn_mod_code,
                        comn_squence_code: $scope.edt.comn_squence_code,
                        comn_description: $scope.edt.comn_description,
                        comm_cur_code: $scope.edt.comm_cur_code,
                        comn_academic_year: $scope.edt.comn_academic_year,
                        comn_character_count: $scope.edt.comn_character_count,
                        comn_character_squence: $scope.edt.comn_character_squence,
                        comm_number_count: $scope.edt.comm_number_count,
                        comn_number_squence: $scope.edt.comn_number_squence,
                        comn_parameter_ref: $scope.edt.comn_parameter_ref,
                        opr: 'U'
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/common_seq/comn_comn004Update", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Common Sequence Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal({ title: "Alert", text: "Common Sequence Not Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                    });
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/common/common_seq/getcomn004_show").then(function (res) {
                    $scope.display = false;
                    $scope.sequencedata = res.data;
                    $scope.totalItems = $scope.sequencedata.length;
                    $scope.todos = $scope.sequencedata;
                    $scope.makeTodos();
                    $scope.grid = true;


                    for (var i = 0; i < $scope.totalItems; i++) {
                        $scope.sequencedata[i].icon = "fa fa-plus-circle";
                    }
                });
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.appl_data = null;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.sequencedata, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.sequencedata;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.comn_squence_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.comn_description.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            var dom;
            $scope.flag = true;
            $scope.icon = "fa fa-plus-circle";

            $scope.expand = function (info, $event) {
                console.log(info);
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                        "<tr> <td class='semi-bold'>" + gettextCatalog.getString('Character Count') + "</td> <td class='semi-bold'>" + gettextCatalog.getString('Character Sequence') + " </td><td class='semi-bold'>" + gettextCatalog.getString('Number Count') + "</td><td class='semi-bold'>" + gettextCatalog.getString('Number Sequence') + "</td>" +
                            "</tr>" +
                              "<tr><td>" + (info.comn_character_count) + "</td> <td>" + (info.comn_character_squence) + " </td><td>" + (info.comm_number_count) + "</td><td>" + (info.comn_number_squence) + "</td>" +
                            "</tr>" +

                        "</tbody>" +
                        " </table></td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    info.icon = "fa fa-plus-circle";
                    $scope.flag = true;
                }
            };



        }])
})();