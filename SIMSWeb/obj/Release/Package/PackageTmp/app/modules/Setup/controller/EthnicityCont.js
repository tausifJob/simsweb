﻿(function () {
    'use strict';
    var ethnicitycode = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    var data1 = [];
    simsController.controller('EthnicityCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            //$scope.operation = true;
            $scope.table1 = true;
            $scope.editmode = false;

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }


            $scope.edt = "";

            $http.get(ENV.apiUrl + "api/Ethnicity/getEthnicitydata").then(function (get_Ethnicity) {
                $scope.Ethnicity_Data = get_Ethnicity.data;
                $scope.totalItems = $scope.Ethnicity_Data.length;
                $scope.todos = $scope.Ethnicity_Data;
                $scope.makeTodos();
                console.log($scope.Ethnicity_Data);
            })


            $scope.New = function () {
                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.editmode = false;
                $scope.opr = 'S';
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = {};
                $scope.edt['sims_ethnicity_status'] = true;
                $scope.temp = '';
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.up = function (str) {
                $scope.opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.edt = {
                    sims_ethnicity_code: str.sims_ethnicity_code,
                    sims_ethnicity_name_en: str.sims_ethnicity_name_en,
                    sims_ethnicity_name_ar: str.sims_ethnicity_name_ar,
                    sims_ethnicity_name_fr: str.sims_ethnicity_name_fr,
                    sims_ethnicity_name_ot: str.sims_ethnicity_name_ot,
                    sims_ethnicity_status: str.sims_ethnicity_status,
                };



            }

            $scope.cancel = function () {
                $scope.edt.sims_ethnicity_code = "";
                $scope.table1 = true;
                $scope.operation = false;

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Save = function (myForm) {
                if (myForm) {
                    data1 = [];
                    var data = $scope.edt;
                    data.opr = 'I';
                    $scope.exist = false;
                    for (var i = 0; i < $scope.Ethnicity_Data.length; i++) {
                        if ($scope.Ethnicity_Data[i].sims_ethnicity_code == data.sims_ethnicity_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ title: "Alert", text: "Record Already Exists", width: 300, height: 200 });
                    }
                    else {
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/Ethnicity/CUDEthnicitydata", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            $scope.operation = false;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                            }
                            $http.get(ENV.apiUrl + "api/Ethnicity/getEthnicitydata").then(function (get_Ethnicity) {
                                $scope.Ethnicity_Data = get_Ethnicity.data;
                                $scope.totalItems = $scope.Ethnicity_Data.length;
                                $scope.todos = $scope.Ethnicity_Data;
                                $scope.makeTodos();
                                $scope.msg1 = msg.data;

                            });
                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                    
                }
            }

            $scope.Update = function (myForm) {
                if (myForm) {
                    debugger
                    data1 = [];
                    var data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/Ethnicity/CUDEthnicitydata", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                        $scope.operation = false;
                        $http.get(ENV.apiUrl + "api/Ethnicity/getEthnicitydata").then(function (get_Ethnicity) {
                            $scope.Ethnicity_Data = get_Ethnicity.data;
                            $scope.totalItems = $scope.Ethnicity_Data.length;
                            $scope.todos = $scope.Ethnicity_Data;
                            $scope.makeTodos();

                        });

                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                   
                }
            }
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;
            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };


            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_ethnicity_code + i);
                    if (main.checked == true) {
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }

                    else {
                        v.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }


            $scope.deleterecord = function () {
                var data1 = [];
                ethnicitycode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_ethnicity_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletelocacode = ({
                            'sims_ethnicity_code': $scope.filteredTodos[i].sims_ethnicity_code,
                            opr: 'D'
                        });
                        ethnicitycode.push(deletelocacode);
                    }
                }
                debugger
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/Ethnicity/CUDEthnicitydata", ethnicitycode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Ethnicity/getEthnicitydata").then(function (get_Ethnicity) {
                                                $scope.Ethnicity_Data = get_Ethnicity.data;
                                                $scope.totalItems = $scope.Ethnicity_Data.length;
                                                $scope.todos = $scope.Ethnicity_Data;
                                                $scope.makeTodos();
                                            });
                                        }

                                        $scope.currentPage = true;
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Ethnicity/getEthnicitydata").then(function (get_Ethnicity) {
                                                $scope.Ethnicity_Data = get_Ethnicity.data;
                                                $scope.totalItems = $scope.Ethnicity_Data.length;
                                                $scope.todos = $scope.Ethnicity_Data;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.row1 = '';
                main.checked = false;
                $scope.currentPage = true;
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Ethnicity_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Ethnicity_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                    item.sims_ethnicity_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_ethnicity_name_ar.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_ethnicity_code == toSearch) ? true : false;
            }

          

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

        }]
        )
})();