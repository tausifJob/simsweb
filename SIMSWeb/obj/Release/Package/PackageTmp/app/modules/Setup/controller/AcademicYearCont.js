﻿(function () {
    'use strict';
    var del = [];
    var main;
    var date1;
    var date3;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AcademicYearCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.acadyr_data = [];
            $scope.edit_code = false;
            var data1 = [];
            var deletecode = [];

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;


            //$http.get(ENV.apiUrl + "api/common/AcademicYear/getCuriculum").then(function (res) {
            //    $scope.obj2 = res.data;
            //    console.log($scope.obj2);
            //});

            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
                $scope.obj2 = res.data;
                console.log($scope.obj2);
            });

            $http.get(ENV.apiUrl + "api/common/AcademicYear/getAcademicStatus").then(function (res) {
                $scope.obj3 = res.data;
                console.log($scope.obj3);
            });

            $http.get(ENV.apiUrl + "api/common/AcademicYear/getAcademicYear").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.acadyr_data = res.data;
                $scope.totalItems = $scope.acadyr_data.length;
                $scope.todos = $scope.acadyr_data;
                $scope.makeTodos();
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edt =
                    {
                        sims_cur_code: str.sims_cur_code,
                        sims_academic_year: str.sims_academic_year,
                        sims_academic_year_start_date: str.sims_academic_year_start_date,
                        sims_academic_year_end_date: str.sims_academic_year_end_date,
                        sims_academic_year_status: str.sims_academic_year_status,
                        sims_academic_year_desc: str.sims_academic_year_desc,
                    }

                $scope.edit_code = true;

                //var date = $scope.edt.sims_academic_year_start_date;
                //var month = date.split("-")[1];
                //var day = date.split("-")[0];
                //var year = date.split("-")[2];
                //date1 = year + "-" + month + "-" + day;
                //console.log(date1);
                //$scope.edt.sims_academic_year_start_date = date1;
                //var v = document.getElementById('dp_start_date');
                //v.value = $scope.edt.sims_academic_year_start_date;

                //var date2 = $scope.edt.sims_academic_year_end_date;
                //var month = date2.split("-")[1];
                //var day = date2.split("-")[0];
                //var year = date2.split("-")[2];
                //date3 = year + "-" + month + "-" + day;
                //console.log(date3);
                //$scope.edt.sims_academic_year_end_date = date3;
                //var v = document.getElementById('dp_endDate');
                //v.value = $scope.edt.sims_academic_year_end_date;
            }

            // $scope.createdate = function (date)
            // {
            //console.log(date);
            //var month = date.split("/")[0];
            //var day = date.split("/")[1];
            //var year = date.split("/")[2];
            //var date1 = year + "-" + month + "-" + day;
            //console.log(date1);
            //  $scope.edt.sims_academic_year_start_date = convertdate(date);
            // }

            // $scope.createdate1 = function (date)
            // {
            //console.log(date);
            //var month = date.split("/")[0];
            //var day = date.split("/")[1];
            //var year = date.split("/")[2];
            //var date1 = year + "-" + month + "-" + day;
            //console.log(date1);
            //  $scope.edt.sims_academic_year_end_date = convertdate(date);
            // }

            //function convertdate(dt) {
            //    if (dt != null) {
            //        var d1 = new Date(dt);
            //        var month = d1.getMonth() + 1;
            //        var day = d1.getDate();
            //        if (month < 10)
            //            month = "0" + month;
            //        if (day < 10)
            //            day = "0" + day;
            //        var d = d1.getFullYear() + "-" + (month) + "-" + (day);
            //        console.log(d);
            //        $scope.convertdated = d;
            //        return d;
            //    }
            //}

            $scope.Save = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = ({
                            sims_cur_code: $scope.edt.sims_cur_code,
                            sims_academic_year: $scope.edt.sims_academic_year,
                            sims_academic_year_start_date: $scope.edt.sims_academic_year_start_date,
                            sims_academic_year_end_date: $scope.edt.sims_academic_year_end_date,
                            sims_academic_year_status: $scope.edt.sims_academic_year_status,
                            sims_academic_year_desc: $scope.edt.sims_academic_year_desc,
                            opr: 'I'
                        });

                        data1.push(data);

                        $http.post(ENV.apiUrl + "api/common/AcademicYear/CUDInsertAcademicYear", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Academic Year Added Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal({ title: "Alert", text: "Academic Year Record Already Exists", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $scope.currentPage = 1;
                $http.get(ENV.apiUrl + "api/common/AcademicYear/getAcademicYear").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.acadyr_data = res.data;
                    $scope.totalItems = $scope.acadyr_data.length;
                    $scope.todos = $scope.acadyr_data;
                    $scope.makeTodos();
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    var data = ({
                        sims_cur_code: $scope.edt.sims_cur_code,
                        sims_academic_year: $scope.edt.sims_academic_year,
                        sims_academic_year_start_date: $scope.edt.sims_academic_year_start_date,
                        sims_academic_year_end_date: $scope.edt.sims_academic_year_end_date,
                        sims_academic_year_status: $scope.edt.sims_academic_year_status,
                        sims_academic_year_desc: $scope.edt.sims_academic_year_desc,
                        opr: 'U'
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/AcademicYear/CUDAcademicYear", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Academic Year Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal({ title: "Alert", text: "Academic Year Not Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                    });
                }
            }

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
                $scope.edit_code = false;
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_academic_year + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_academic_year + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }

            $scope.Delete = function () {
                var deletecode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    //var v = document.getElementById(i);
                    var v = document.getElementById($scope.filteredTodos[i].sims_academic_year + i);

                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            'sims_cur_code': $scope.filteredTodos[i].sims_cur_code,
                            'sims_academic_year_start_date': $scope.filteredTodos[i].sims_academic_year_start_date,
                            'sims_academic_year_end_date': $scope.filteredTodos[i].sims_academic_year_end_date,
                            'sims_academic_year_status': $scope.filteredTodos[i].sims_academic_year_status,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {


                            $http.post(ENV.apiUrl + "api/common/AcademicYear/CUDAcademicYear", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Academic Year Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Already Mapped.Can't be Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                            });


                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                //var v = document.getElementById(i);
                                var v = document.getElementById($scope.filteredTodos[i].sims_academic_year + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }

                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }

            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.acadyr_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.acadyr_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        // var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_academic_year + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }


            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_academic_year == toSearch) ? true : false;
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
        }])
})();