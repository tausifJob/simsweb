﻿/// <reference path="ReportCardParameterCont.js" />
(function () {
    'use strict';
    var citycode = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    var data1 = [];
    simsController.controller('ReportCardParameterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
         
            //var data = {
            //    location: 'Sims.SIMR51',
            //    parameter: {fee_rec_no:'12.0'},
            //    state:'main.Sim043'
            //}

            //window.localStorage["ReportDetails"] = JSON.stringify(data)
            debugger
            try{
                $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                $scope.location = $scope.rpt.location;
                $scope.parameter = $scope.rpt.parameter;
                $scope.state = $scope.rpt.state;
            }
            catch (ex) {
            }
            var s;

          if ($http.defaults.headers.common['schoolId'] == 'abps')

              s = "SimsReports." + $scope.location + ",SimsReportsAbps";

          else if ($http.defaults.headers.common['schoolId'] == 'dpsmis')
              s = "SimsReports." + $scope.location + ",SimsReports";

          else if ($http.defaults.headers.common['schoolId'] == 'ajn')
              s = "SimsReports." + $scope.location + ",SimsReportsAjn";


          else if ($http.defaults.headers.common['schoolId'] == 'kkn')
              s = "SimsReports." + $scope.location + ",SimsReportsKkn";

          else if ($http.defaults.headers.common['schoolId'] == 'bluestar')
              s = "SimsReports." + $scope.location + ",SimsReportsBluestar";
          else if ($http.defaults.headers.common['schoolId'] == 'sisqatar')
              s = "SimsReports." + $scope.location + ",SimsReportsSisqatar";

          else if ($http.defaults.headers.common['schoolId'] == 'ihis')
              s = "SimsReports." + $scope.location + ",SimsReportsIhis";

          else if ($http.defaults.headers.common['schoolId'] == 'egn')
              s = "SimsReports." + $scope.location + ",SimsReportsEgn";

          else if ($http.defaults.headers.common['schoolId'] == 'demo')
              s = "SimsReports." + $scope.location + ",SimsReportsDemo";

          else if ($http.defaults.headers.common['schoolId'] == 'tsis')
              s = "SimsReports." + $scope.location + ",SimsReportsTsis";

          else if ($http.defaults.headers.common['schoolId'] == 'eiama')
              s = "SimsReports." + $scope.location + ",SimsReportsEiama";

          else if ($http.defaults.headers.common['schoolId'] == 'eiagps')
              s = "SimsReports." + $scope.location + ",SimsReportsEiagps";

          else if ($http.defaults.headers.common['schoolId'] == 'eiand')
              s = "SimsReports." + $scope.location + ",SimsReportsEiand";

          else if ($http.defaults.headers.common['schoolId'] == 'eiam')
              s = "SimsReports." + $scope.location + ",SimsReportsEiam";

          else if ($http.defaults.headers.common['schoolId'] == 'zps')
              s = "SimsReports." + $scope.location + ",SimsReportsZps";

          else if ($http.defaults.headers.common['schoolId'] == 'sms2')
              s = "SimsReports." + $scope.location + ",SimsReportsSms2";

          else if ($http.defaults.headers.common['schoolId'] == 'sms')
              s = "SimsReports." + $scope.location + ",SimsReportsSms2";

          else if ($http.defaults.headers.common['schoolId'] == 'imert')
              s = "SimsReports." + $scope.location + ",SimsReportsImert";


          else if ($http.defaults.headers.common['schoolId'] == 'brs')
              s = "SimsReports." + $scope.location + ",SimsReportsBrs";


            else if ($http.defaults.headers.common['schoolId'] == 'qfis')
                s = "SimsReports." +  $scope.location + ",SimsReportsQfis";

            else if ($http.defaults.headers.common['schoolId'] == 'deps')
                s = "SimsReports." +  $scope.location + ",SimsReportsDeps";


            else if ($http.defaults.headers.common['schoolId'] == 'sis')
                s = "SimsReports." +  $scope.location + ",SimsReportsSis";

            else if ($http.defaults.headers.common['schoolId'] == 'ajb')
                s = "SimsReports." +  $scope.location + ",SimsReportsAjb";

            else if ($http.defaults.headers.common['schoolId'] == 'aji')
                s = "SimsReports." +  $scope.location + ",SimsReportsAji";

            else if ($http.defaults.headers.common['schoolId'] == 'adisw')
                s = "SimsReports." +  $scope.location + ",SimsReportsAdisw";

            else if ($http.defaults.headers.common['schoolId'] == 'adis')
                s = "SimsReports." +  $scope.location + ",SimsReportsAdis";

            else if ($http.defaults.headers.common['schoolId'] == 'oes')
                s = "SimsReports." +  $scope.location + ",SimsReportsOes";

            else if ($http.defaults.headers.common['schoolId'] == 'sn')
                s = "SimsReports." +  $scope.location + ",SimsReportsSn";

        
            else if ($http.defaults.headers.common['schoolId'] == 'etaaltwar')
                s = "SimsReports." + $scope.location + ",SimsReportsEta";

            else if ($http.defaults.headers.common['schoolId'] == 'nis')
                s = "SimsReports." +  $scope.location + ",SimsReportsNis";

            else if ($http.defaults.headers.common['schoolId'] == 'pearl')
                s = "SimsReports." +  $scope.location + ",SimsReportsPearl";

            else if ($http.defaults.headers.common['schoolId'] == 'mhm')
                s = "SimsReports." +  $scope.location + ",SimsReportsMhm";

            else if ($http.defaults.headers.common['schoolId'] == 'gsis')
                s = "SimsReports." + $scope.location + ",SimsReportsGsis";

            else if ($http.defaults.headers.common['schoolId'] == 'sbis')
                s = "SimsReports." + $scope.location + ",SimsReportsSbis";

            else if ($http.defaults.headers.common['schoolId'] == 'misqatar')
                s = "SimsReports." + $scope.location + ",SimsReportsMisqatar";


          //  window.localStorage["Finn_comp"] = JSON.stringify(data)

            $scope.parameters = {}
           
            $("#reportViewer1")
                           .telerik_ReportViewer({
                               serviceUrl: ENV.apiUrl + "api/reports/",
                               viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                               scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                               // Zoom in and out the report using the scale
                               // 1.0 is equal to 100%, i.e. the original size of the report
                               scale: 1.0,
                               ready: function () {
                                   //this.refreshReport();
                               }
                              
                           });


       


            var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
            reportViewer.reportSource({
                report:s,
                parameters: $scope.parameter,
            });

            setInterval(function () {

                $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });

            }, 1000);


            $timeout(function () {
                $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });


            }, 100)


            $scope.back = function () {
                $state.go($scope.state);
            }


        }])
})();