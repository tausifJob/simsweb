﻿(function () {
    'use strict';
   
    var data1 = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CouncilStudentMappingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
         //   $scope.getroutecodename(str.sims_academic_year);
            $scope.busyindicator = true;
           

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                
                });
            }

            $scope.gethouse = function (cur_code, academic_year) {
             
                $http.get(ENV.apiUrl + "api/CouncilStudentMapping/getAllHouseNameByCur?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (get_HouseName) {
                    debugger
                    $scope.HouseName = get_HouseName.data;
                })
            }
           
            $scope.getGrade = function (curCode, accYear) {
                $scope.Grade_code = [];
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    console.log($scope.Grade_code);
                });
            }

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $scope.Section_code = [];
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.temp.sims_cur_code + "&grade_code=" + $scope.temp.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    console.log($scope.Section_code);
                });
            }

                $scope.Show_Data = function () {
                    $scope.NodataCouncil = false;
                    $http.get(ENV.apiUrl + "api/CouncilStudentMapping/getAllCouncilDetails?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&house_cd=" + $scope.edt.sims_house_code).then(function (getAllCouncilDetails_Data) {
                        $scope.AllCouncilDetails_Data = getAllCouncilDetails_Data.data;
                        $scope.CouncilRecord = true;
                        if (getAllCouncilDetails_Data.data.length > 0) {
                            $scope.submit_btn = true;
                    }
                    else{
                        $scope.NodataCouncil = true;
                        $scope.CouncilRecord = false;
                        $scope.submit_btn = false;
                    }
                    });
                }

                var data="";
                $scope.SearchStudent = function (info) 
                {
                    $scope.set = info;
                    
                    data=info;
                    $('#MyModal').modal('show');
                    $scope.temp = "";
                    $scope.searchtable = false;
                   // $scope.EmployeeTable = false;
                }

                $scope.StudentDetails = function () {
                    $scope.NodataStudent = false;
                    $http.get(ENV.apiUrl + "api/CouncilStudentMapping/getAllStudentsDetail?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&sec_code=" + $scope.temp.sims_section_code + "&house_cd=" + $scope.edt.sims_house_code).then(function (getAllStudentsDetail) {
                        $scope.StudentsDetail = getAllStudentsDetail.data;
                        $scope.searchtable = true;

                        if (getAllStudentsDetail.data.length > 0) { }
                        else {
                            $scope.NodataStudent = true;
                            $scope.searchtable = false;
                        }

                    });
                    
                }

                $scope.EmployeeDetails = function ()
                {
                    $scope.NodataEmployee = false;
                    $http.post(ENV.apiUrl + "api/CouncilStudentMapping/SearchEmployee?data=" + JSON.stringify($scope.temp)).then(function (SearchEmployee_Data) {
                        $scope.EmployeeDetails = SearchEmployee_Data.data;
                        console.log($scope.EmployeeDetails);
                        $scope.EmployeeTable = true;

                        if (SearchEmployee_Data.data.length > 0) {
                         //   $scope.EmployeeTable = true;
                        }
                        else {
                            $scope.NodataEmployee = true;
                          //  $scope.EmployeeTable = false;
                        }
                    });
                }
                
                $scope.StudAdd = function (obj) {
                    for (var i=0; i < $scope.AllCouncilDetails_Data.length; i++)
                    {
                        if ($scope.AllCouncilDetails_Data[i].sims_council_code == data.sims_council_code) 
                        {
                            if ($scope.set.sims_emp_name == '') {
                                $scope.AllCouncilDetails_Data[i].sims_user_login_code = obj.sims_student_enroll_number
                                $scope.AllCouncilDetails_Data[i].sims_stud_name = obj.student_name
                            }
                            else
                            {
                                $scope.AllCouncilDetails_Data[i].sims_emp_name = ''
                                $scope.AllCouncilDetails_Data[i].sims_user_login_code = obj.sims_student_enroll_number
                                $scope.AllCouncilDetails_Data[i].sims_stud_name = obj.student_name
                            }

                            $scope.AllCouncilDetails_Data[i].sims_council_name=data.sims_council_name,
                            $scope.AllCouncilDetails_Data[i].sims_house_council_enroll_start_date=data.sims_house_council_enroll_start_date,
                            $scope.AllCouncilDetails_Data[i].sims_council_code=data.sims_council_code
                        //    $scope.AllCouncilDetails_Data[i].sims_house_code = obj.sims_student_house

                        }
                    }
                }

                $scope.EmployeeAdd = function (emp) {

                    for (var i = 0; i < $scope.AllCouncilDetails_Data.length; i++) {
                        if ($scope.AllCouncilDetails_Data[i].sims_council_code == data.sims_council_code)
                            {
                            if ($scope.set.sims_stud_name == '') {
                                $scope.AllCouncilDetails_Data[i].sims_user_login_code = emp.em_login_code
                                $scope.AllCouncilDetails_Data[i].sims_emp_name = emp.empName

                            }
                            else {
                                $scope.AllCouncilDetails_Data[i].sims_stud_name = ''
                                $scope.AllCouncilDetails_Data[i].sims_user_login_code = emp.em_login_code
                                $scope.AllCouncilDetails_Data[i].sims_emp_name = emp.empName
                            }
                            $scope.AllCouncilDetails_Data[i].sims_council_name = data.sims_council_name,
                            $scope.AllCouncilDetails_Data[i].sims_house_council_enroll_start_date = data.sims_house_council_enroll_start_date,
                            $scope.AllCouncilDetails_Data[i].sims_council_code = data.sims_council_code
                          //  $scope.AllCouncilDetails_Data[i].sims_house_code = obj.sims_student_house
                        }
                    }
                }
              
                $scope.Submit = function () {
                   var data1 = [];
                            for (var i = 0; i < $scope.AllCouncilDetails_Data.length; i++) {
                                if ($scope.AllCouncilDetails_Data[i].sims_council_code == data.sims_council_code)
                                    {
                                data = {
                                    sims_cur_code: $scope.edt.sims_cur_code,
                                    sims_academic_year: $scope.edt.sims_academic_year,
                                    sims_user_login_code: $scope.AllCouncilDetails_Data[i].sims_user_login_code,
                                    sims_house_code: $scope.edt.sims_house_code,
                                    sims_council_code: $scope.AllCouncilDetails_Data[i].sims_council_code,
                                    sims_house_council_enroll_start_date: $scope.AllCouncilDetails_Data[i].sims_house_council_enroll_start_date,
                                    opr: 'T',
                                };
                                }
                            }
                         
                            data1.push(data);
                            $http.post(ENV.apiUrl + "api/CouncilStudentMapping/CouncilStudentCUD", data1).then(function (msg) {
                                $scope.msg1 = msg.data;

                                if ($scope.msg1 == true) {

                                    swal({ title: "Alert", text: "Council Assigned Successfully", width: 300, height: 200 });

                                $http.get(ENV.apiUrl + "api/CouncilStudentMapping/getAllCouncilDetails").then(function (getAllCouncilDetails_Data) {
                                    $scope.AllCouncilDetails_Data = getAllCouncilDetails_Data.data;
                                });
                                }
                                    else {

                                        swal({ title: "Alert", text: "Council Not Assigned", width: 300, height: 200 });
                                    }

                              
                            });
                            $scope.CouncilRecord = false;
                            $scope.submit_btn = false;
                            $scope.edt = "";
                            $scope.data1 = [];
                }

                $timeout(function () {
                    $("#fixTable").tableHeadFixer({ 'top': 1 });
                }, 100);

                $timeout(function () {
                    $("#fixTable1").tableHeadFixer({ 'top': 1 });
                }, 100);
            
        }]);
})();



