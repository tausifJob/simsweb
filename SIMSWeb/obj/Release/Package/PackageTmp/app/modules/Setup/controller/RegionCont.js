﻿(function () {
    'use strict';
    var regioncode = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    var data1 = [];
    simsController.controller('RegionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            //$scope.operation = true;
            $scope.table1 = true;
            $scope.editmode = false;

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }
            $scope.edt = "";

            $http.get(ENV.apiUrl + "api/Region/getRegion").then(function (get_Region) {
                $scope.RegionData = get_Region.data;
                $scope.totalItems = $scope.RegionData.length;
                $scope.todos = $scope.RegionData;
                $scope.makeTodos();
            })



            $scope.New = function () {
                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.editmode = false;
                $scope.opr = 'S';
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = {};
                $scope.edt['sims_status'] = true;
                $scope.temp = '';
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.up = function (str) {
                $scope.opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.edt = {
                    sims_region_code: str.sims_region_code,
                    sims_region_name_en: str.sims_region_name_en,
                    sims_region_name_ar: str.sims_region_name_ar,
                    sims_region_name_fr: str.sims_region_name_fr,
                    sims_region_name_ot: str.sims_region_name_ot,
                    sims_status: str.sims_status
                }

            }

            $scope.cancel = function () {

                $scope.table1 = true;
                $scope.operation = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Save = function (myForm) {
                if (myForm) {
                    data1 = [];
                    var data = $scope.edt;
                    data.opr = 'I';
                    $scope.exist = false;
                    for (var i = 0; i < $scope.RegionData.length; i++) {
                        if ($scope.RegionData[i].sims_region_code == data.sims_region_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ title: "Alert", text: "Record Already Exists", width: 300, height: 200 });
                    }
                    else {
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/Region/CUDgetRegion", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                            }
                            $scope.operation = false;
                            $http.get(ENV.apiUrl + "api/Region/getRegion").then(function (get_Region) {
                                $scope.RegionData = get_Region.data;
                                $scope.totalItems = $scope.RegionData.length;
                                $scope.todos = $scope.RegionData;
                                $scope.makeTodos();

                            });
                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                    data1 = [];
                }
            }

            $scope.Update = function (myForm) {
                debugger
                if (myForm) {
                    data1=[];
                    var data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/Region/CUDgetRegion", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                        $scope.operation = false;
                        $http.get(ENV.apiUrl + "api/Region/getRegion").then(function (get_Region) {
                            $scope.RegionData = get_Region.data;
                            $scope.totalItems = $scope.RegionData.length;
                            $scope.todos = $scope.RegionData;
                            $scope.makeTodos();
                        });

                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                }
            }
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;
            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_region_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_region_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.deleterecord = function () {
                debugger;
                regioncode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_region_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletelocacode = ({
                            'sims_region_code': $scope.filteredTodos[i].sims_region_code,
                            opr: 'D'
                        });
                        regioncode.push(deletelocacode);
                    }
                }
                debugger
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger
                            $http.post(ENV.apiUrl + "api/Region/CUDgetRegion", regioncode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Region/getRegion").then(function (get_Region) {
                                                $scope.RegionData = get_Region.data;
                                                $scope.totalItems = $scope.RegionData.length;
                                                $scope.todos = $scope.RegionData;
                                                $scope.makeTodos();

                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record is Mapped, CanNot Delete", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Region/getRegion").then(function (get_Region) {
                                                $scope.RegionData = get_Region.data;
                                                $scope.totalItems = $scope.RegionData.length;
                                                $scope.todos = $scope.RegionData;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_region_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
              
                $scope.currentPage = true;
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.RegionData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.RegionData;
                }
                $scope.makeTodos();
            }


            function searchUtil(item, toSearch) {
                return (
                     item.sims_region_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_region_name_ar.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_region_name_fr.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_region_name_ot.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_region_code == toSearch) ? true : false;


            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


        }]
        )
})();