﻿(function () {
    'use strict';
    angular.module('sims.module.Setup', [
        'sims',
        'bw.paging',
        'gettext'
    ]);
})();