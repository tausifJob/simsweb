﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('MySearchCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
           
            $rootScope.visible_stud = false;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = true;

            $rootScope.chkMulti = true;

            $scope.edt = {};

            $scope.New = function () {
                $state.go('main.Sim510');
            }

            $http.get(ENV.apiUrl + "api/mysearch/GetgradesAll").then(function (res) {
                debugger;
                $scope.obj_grade = res.data
                console.log($scope.obj_grade);
            });

            $scope.UserSearch = function () {
                debugger;
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }

            $scope.$on('global_cancel', function (str) {
                debugger;
                console.log($scope.SelectedUserLst);
                if ($scope.SelectedUserLst.length > 0) {
                    ////arr_files.push($scope.SelectedUserLst);
                    //$scope.arr_files.push({
                    //    name: $scope.SelectedUserLst.empName,
                    //    logincode: $scope.SelectedUserLst.em_login_code
                    //});
                }
            });
           
            $scope.getsections = function (gradecode) {
                $http.get(ENV.apiUrl + "api/mysearch/getSearchGradesSections?gradecode=" + gradecode).then(function (res) {
                    $scope.o_section = res.data;
                });
            }            

            $http.get(ENV.apiUrl + "api/mysearch/getSearchReligion").then(function (res) {
                $scope.o_religion = res.data;
            });

            $http.get(ENV.apiUrl + "api/mysearch/getSearchNationality").then(function (res) {
                $scope.o_nationality = res.data;
            });

            $http.get(ENV.apiUrl + "api/mysearch/getSearchSubject").then(function (res) {
                $scope.ob_subject = res.data;
            });

            $scope.Save = function () {

                if ($scope.searchdesc != undefined) {

                    $scope.edt = {

                        sims_search_desc: $scope.searchdesc,
                        sims_search_short_name: $scope.searchdesc,
                        sims_search_status: 'A'
                    };

                    var data = $scope.edt;

                    $http.post(ENV.apiUrl + "api/mysearch/CMySearch", data).then(function (res) {
                        debugger;
                        $scope.msg1 = res.data;
                        $scope.search_query = $scope.datafield;
                        $scope.condition='';
                        //if (cur_shortname != undefined)
                        //    $scope.condition = $scope.condition+

                        swal({ title: "Alert", text: "Record inserted Successfully.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {

                            }
                        });

                        $scope.search_condition = '';
                        //if ($scope.msg1.strMessage != '') {
                        //    $http.post(ENV.apiUrl + "api/mysearch/CMySearchUsers?sims_search_code=" + $scope.msg1.strMessage + "&users=" + $scope.users + "&sims_search_query=" + $scope.search_query + "&sims_search_condition=" + $scope.search_condition).then(function (res) {

                        //        swal({ title: "Alert", text: "Record inserted Successfully.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        //            if (isConfirm) {

                        //            }
                        //        });
                        //    });
                        //}
                    });
                }
            }

        }])    
})();