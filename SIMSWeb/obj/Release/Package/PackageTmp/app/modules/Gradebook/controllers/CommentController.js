﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CommentController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$location', '$anchorScroll', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $location, $anchorScroll) {
           
            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1, 'z-index': 1 });
            }, 100);

            $scope.edt =
               {
                   sims_admission_cur_code: '',
                   sims_academic_year: '',
                   sims_grade_code: '',
                   sims_section_code: '',
                   sims_subject_code: '',
                   sims_term_code: '',
                   sims_report_card_level: '',
                   sims_report_card: '',
                   sims_enroll_number: ''
               };
            $scope.Expand = true;
            $scope.busy = false;
            $scope.gradebook_admin = false;
            $scope.add_new_comment = true;
            $scope.insert_flag = false;
            $scope.saveEnabled = true;
            $scope.new_comment_text = true;

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.cur_data = res.data;
                $scope.edt['sims_cur_code'] = res.data[0].sims_cur_code;
                $scope.cur_code_change();
            });


            //$http.get(ENV.apiUrl + "api/Gradebook/GetRCCuriculam").then(function (res) {
            //    $scope.cur_data = res.data;
            //    $scope.edt['sims_cur_code'] = res.data[0].sims_cur_code;
            //    $scope.cur_code_change();
            //});

            $http.get(ENV.apiUrl + "api/Gradebook/GetRCCommentType").then(function (CommentType) {
                $scope.CommentType = CommentType.data;
            });

            //$http.get(ENV.apiUrl + "api/Gradebook/GetAdminFlag?user_name=admin").then(function (gradebook_admin) {
            //    if (gradebook_admin.data == 'Y')
            //        $scope.gradebook_admin = true;

            //});

            $http.get(ENV.apiUrl + "api/Gradebook/getCheckedUser?user_code=" + user + "&co_scholastic=" + 'N').then(function (CheckUserData) {
                $scope.CheckUser_Data = CheckUserData.data;

                if ($scope.CheckUser_Data == true) {
                    $scope.gradebook_admin = true;
                }
                

            })

            $scope.cur_code_change = function () {

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + newval).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                });




                //$http.get(ENV.apiUrl + "api/Gradebook/GetRCAcademicYear?cur_code=" + $scope.edt.sims_cur_code).then(function (AcademicYears) {
                //    $scope.AcademicYears = AcademicYears.data;
                //    for (var i = 0; i < $scope.AcademicYears.length; i++) {
                //        if ($scope.AcademicYears[i].sims_academic_year_status == 'C') {
                //            //   $scope.edt['sims_cur_code'] = cur_values;
                //            $scope.edt['sims_academic_year'] = $scope.AcademicYears[i].sims_academic_year;
                //        }
                //    }
                //    $scope.academic_year_change($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                //});
            }

            $scope.academic_year_change = function (sims_cur_code, sims_academic_year) {


                $http.get(ENV.apiUrl + "api/Gradebook/getTermforGradebook?cur_code=" + $scope.code + "&academic_year=" + oldval).then(function (Getsims550_TermsAcademicYear) {
                    $scope.TermsAcademicYear = Getsims550_TermsAcademicYear.data;

                });



                //$http.get(ENV.apiUrl + "api/Gradebook/GetRCTerm?cur=" + sims_cur_code + "&aca=" + sims_academic_year).then(function (terms) {
                //    $scope.terms = terms.data;
                //    $scope.edt['sims_term_code'] = terms.data[0].sims_term_code;
                //});
                $http.get(ENV.apiUrl + "api/Gradebook/GetRCGrade?cur=" + sims_cur_code + "&aca=" + sims_academic_year+"&user=admin").then(function (grades) {
                    $scope.grades = grades.data;
                });
            }

            $scope.grade_change = function (sims_cur_code, sims_academic_year,sims_grade_code) {
                $http.get(ENV.apiUrl + "api/Gradebook/GetRCSection?cur=" + sims_cur_code + "&aca=" + sims_academic_year + "&grd=" + sims_grade_code + "&user=admin").then(function (sections) {
                    $scope.sections = sections.data;
                });
            }

            $scope.section_change = function (sims_cur_code, sims_academic_year, sims_grade_code, sims_section_code) {
                $http.get(ENV.apiUrl + "api/Gradebook/GetRCStudentForComments?cur=" + sims_cur_code + "&aca=" + sims_academic_year + "&grd=" + sims_grade_code + "&sec=" + sims_section_code).then(function (students) {
                    $scope.students = students.data;
                    console.log($scope.students);
                });


                $http.post(ENV.apiUrl + "api/Gradebook/AllGradeSection_subject", report1).then(function (allSection_Subject) {
                    $scope.subject_names1 = allSection_Subject.data;

                });


                //$http.get(ENV.apiUrl + "api/Gradebook/GetRCSubjects?cur=" + sims_cur_code + "&aca=" + sims_academic_year + "&grd=" + sims_grade_code + "&sec=" + sims_section_code + "&user=admin").then(function (subjects) {
                //    $scope.subjects = subjects.data;

                //});
                $http.get(ENV.apiUrl + "api/Gradebook/GetRCReportCardLevel?cur=" + sims_cur_code + "&aca=" + sims_academic_year + "&grd=" + sims_grade_code + "&sec=" + sims_section_code).then(function (levels) {
                    $scope.levels = levels.data;

                });
            }

            $scope.level_change = function (sims_cur_code, sims_academic_year, sims_grade_code, sims_section_code, levelcode) {
                $http.get(ENV.apiUrl + "api/Gradebook/GetRCReportCardNames?cur=" + sims_cur_code + "&aca=" + sims_academic_year + "&grd=" + sims_grade_code + "&sec=" + sims_section_code + "&levelcode=" + levelcode).then(function (report_cards) {
                    $scope.report_cards = report_cards.data;
                    $scope.edt['sims_report_card'] = report_cards.data[0].sims_report_card;
                });
            }

            $scope.preview = function () {
                $scope.busy = true;
                $scope.edt.sims_admission_cur_code = $scope.edt.sims_cur_code;
                $scope.edt.sims_enroll_number = '';
                $scope.add_new_comment = true;
                $scope.enroll = '';
                $scope.new_comment_text = true;
                $scope.new_message = '';

                if ($scope.edt.sims_comment_type != 'S')
                    $scope.edt.sims_subject_code = '';

                $http.post(ENV.apiUrl + "api/Gradebook/RCCommentsforStudent", $scope.edt).then(function (comments_Data) {
                    $scope.comments_Data = comments_Data.data;
                    $scope.busy = false;
                });
            }

            $scope.enroll = '';

            $scope.getCommentsStd = function (enroll) {
                $scope.enroll = enroll;
                $scope.edt.sims_admission_cur_code = $scope.edt.sims_cur_code;
                $scope.edt.sims_enroll_number = enroll;
                $scope.busy = true;
                $scope.saveEnabled = false;
                $scope.new_message = '';

                $http.post(ENV.apiUrl + "api/Gradebook/RCCommentsforStudent", $scope.edt).then(function (comments_Data) {
                    $scope.comments_Data = comments_Data.data;
                    $scope.Expand = false;
                    $scope.busy = false;
                    $scope.add_new_comment = true;
                    

                });
            }

            $scope.editcomment = function (info) {
                $scope.insert_flag = false;
                $scope.new_message = info.sims_comment;
                $scope.saveEnabled = false;
                $scope.new_comment_text = false;
                $scope.current_comment = info;
            }
            $scope.current_header = '';
            $scope.current_comment = '';

            $scope.edit = function (comments, $index) {
                $scope.add_new_comment = false;
                $scope.current_header = comments;
                $scope.current_comment = comments;
            }

            $scope.add_new_header = function () {
                $scope.insert_flag = true;
                $scope.CommentTypeFlag = 'H';
                $scope.saveEnabled = false;
                $scope.new_message = '';
                $scope.new_comment_text = false;
                $scope.current_header = '';
            }
            
            $scope.btnadd_new_comment = function () {
                $scope.insert_flag = true;
                $scope.CommentTypeFlag = 'C';
                $scope.saveEnabled = false;
                $scope.new_message = '';
                $scope.new_comment_text = false;
            }

            $scope.save = function () {
                if ($scope.enroll == '') {
                    swal(
                             {
                                 showCloseButton: true,
                                 text: 'Please Select Enroll Number',
                                 width: 350,
                                 showCloseButon: true
                             });
                }
                else {
                    if ($scope.insert_flag == true) {
                        $scope.edt.sims_student_comment_header_id = $scope.current_header.sims_student_comment_header_id;
                        $scope.edt.sims_comment_type = $scope.CommentTypeFlag;
                        $scope.edt.sims_comment_header_desc = $scope.new_message;
                        $http.post(ENV.apiUrl + "api/Gradebook/RCNewInsertCommentforStudent", $scope.edt).then(function (res) {
                            if (res.data == true) {
                                swal(
                               {
                                   showCloseButton: true,
                                   text: 'Comment Data Added Successfully',
                                   width: 350,
                                   showCloseButon: true
                               });
                             //   $scope.getCommentsStd();
                            }
                        });

                    }
                    else {
                        debugger;
                        for (var x = 0; x < $scope.comments_Data.length; x++) {
                            for (var j = 0; j < $scope.comments_Data[x].coments_lst.length; j++) {
                                var co = $scope.comments_Data[x].coments_lst[j];
                                if ($scope.current_comment.sims_student_comment_header_id == co.sims_student_comment_header_id && $scope.current_comment.sims_comment_code == co.sims_comment_code) {
                                    co.sims_comment = $scope.new_message;
                                    break;
                                }
                            }
                        }
                        $http.post(ENV.apiUrl + "api/Gradebook/RCInsertHeaderforStudent", $scope.comments_Data).then(function (res) {
                            if (res.data == true) {
                                swal(
                              {
                                  showCloseButton: true,
                                  text: 'Comment Data Updated Successfully',
                                  width: 350,
                                  showCloseButon: true
                              });
                                //$scope.getCommentsStd();
                            }
                        });
                    }
                }
              

            }

            $scope.reset = function () {

                $scope.edt.sims_grade_code='';
                $scope.edt.sims_section_code='';
                $scope.edt.sims_subject_code='';
                $scope.edt.sims_term_code='';
                $scope.edt.sims_report_card_level = '';
                $scope.edt.sims_report_card = '';
                $scope.edt.sims_enroll_number = '';
                $scope.current_header = '';
                $scope.current_comment = '';
                $scope.Expand = true;
                $scope.busy = false;
                $scope.add_new_comment = true;
                $scope.insert_flag = false;
                $scope.saveEnabled = true;
                $scope.new_comment_text = true;
                $scope.comments_Data = [];
                $scope.students = [];
            }
            /* End Filter */
            //Events End
        }])

       
})();
