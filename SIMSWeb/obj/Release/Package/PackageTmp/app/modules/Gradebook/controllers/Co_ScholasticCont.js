﻿(function () {
    'use strict';
    var teacher_code = [];
    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('Co_ScholasticCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.disable = false;
            $scope.Co_Scholastic = true;
            $scope.names = true;
            $scope.stud = {};
            $scope.copys = {};
            debugger;
            var user = ($rootScope.globals.currentUser.username + '').toUpperCase();

            $http.get(ENV.apiUrl + "api/Gradebook/getCheckedUser?user_code=" + user + "&co_scholastic=" + 'Y').then(function (CheckUserData) {
                $scope.CheckUser_Data = CheckUserData.data;

                if ($scope.CheckUser_Data == true) {
                    $scope.deletebuttonshow = true;
                    $scope.deletebuttonshow1 = true;
                    $scope.names = true;
                    $scope.teacherhide = false;
                    $scope.headcolor = '1';
                }
                else {

                    $scope.deletebuttonshow = false;
                    $scope.teacherhide = true;
                    $scope.Co_Scholastic = false;
                    $scope.Attributes = false;
                    $scope.Indicator = false;
                    $scope.Indicatorgrade = true;
                    $scope.names = true;
                    $scope.deletebuttonshow1 = true;
                    $scope.Indicatorgrade1 = false;
                    $scope.Indicatorgrade2 = false;
                    $scope.headcolor = '4';
                }

            })

            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1 });
                $("#Table2").tableHeadFixer({ 'top': 1 });
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#Table3").tableHeadFixer({ 'top': 1 });
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
                $("#table4").tableHeadFixer({ 'top': 1 });
                $("#table5").tableHeadFixer({ 'top': 1 });
                $("#Table4").tableHeadFixer({ 'top': 1 });
                $("#Table6").tableHeadFixer({ 'top': 1 });
                $("#Table8").tableHeadFixer({ 'top': 1 });
                $("#Table9").tableHeadFixer({ 'top': 1 });
                $("#Table10").tableHeadFixer({ 'top': 1 });
                $("#Table4").tableHeadFixer({ 'top': 1 });
                $("#Table6").tableHeadFixer({ 'top': 1 });
                $("#Table7").tableHeadFixer({ 'top': 1 });

                $("#scroll-wrapper").css({ 'height': '600px' });
                $("#menu-wrapper1").scrollbar();
            }, 100);

            $scope.temp = {};
            //for collapse main menu
            $('body').addClass('grey condense-menu');
            $('#main-menu').addClass('mini');
            $('.page-content').addClass('condensed');

            getallco_scholasticgroup(user);

            $http.get(ENV.apiUrl + "api/Gradebook/GetTerm").then(function (GetTerm) {
                $scope.TermsAcademicYear = GetTerm.data;
            });

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;
                $scope.temp['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.Getyear($scope.temp.sims_cur_code);

            });

            $scope.Getyear = function () {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.temp.sims_cur_code).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    $scope.temp['sims_academic_year'] = $scope.Academic_year[0].sims_academic_year;
                    $scope.temp['sims_co_scholastic_group_status'] = true;
                    $scope.GetGradecodes();
                });
            }

            teachers();
            getgradesectiondata();

            $scope.Attri = { sims_co_scholastic_attribute_status: true };
            $scope.Indic = { sims_co_scholastic_discriptive_indicator_status: true };

            function teachers() {
                $http.get(ENV.apiUrl + "api/Gradebook/GetAllTeacher").then(function (GetAllTeacher) {

                    $scope.GetAllTeacher = GetAllTeacher.data;
                });
            }

            $http.get(ENV.apiUrl + "api/Gradebook/GetScoreTypes_co_scholastic").then(function (GetScoreTypes) {
                $scope.Get_Score_Types = GetScoreTypes.data;
            });

            $http.get(ENV.apiUrl + "api/Gradebook/getGradeScaleForcoscholasticgroup").then(function (All_Grade) {
                $scope.All_SCALE_GRADE = All_Grade.data;

            });

            function getgradesectiondata() {
                $scope.GetGradeData = [];
                $http.get(ENV.apiUrl + "api/Gradebook/GetGradeData").then(function (GetGradeData) {
                    $scope.GetGradeData = GetGradeData.data;
                });
            }

            $scope.Co_ScholasticShow = function (color) {
                $scope.headcolor = color;
                $scope.Co_Scholastic = true;
                $scope.Attributes = false;
                $scope.Indicator = false;
                $scope.disable = false;
                $scope.NewInsert = true;
                $scope.temp = '';
                $scope.Indicatorgrade = false;
                $scope.names = true;
                $scope.pearlIndicatorgrade1 = false;
                $scope.Indicatorgrade1 = false;
                $scope.Indicatorgrade2 = false;
                $scope.GradeSchemeShow = false;
                $scope.copyco_scholastic = false;
                $scope.temp = {
                    sims_academic_year: $scope.Academic_year[0].sims_academic_year,
                    sims_cur_code: $scope.curriculum[0].sims_cur_code,
                    sims_co_scholastic_group_status: true
                }

                getallco_scholasticgroup(user);
            }

            $scope.Co_ScholasticAttributeShow = function (color) {
                $scope.headcolor = color;
                $scope.Co_Scholastic = false;
                $scope.Attributes = true;
                $scope.Indicator = false;
                $scope.disable = false;
                $scope.Attri = "";
                $scope.pearlIndicatorgrade1 = false;

                $scope.Attri = { sims_co_scholastic_attribute_status: true };
                $scope.NewInsert = true;
                $scope.term = false;
                $scope.Indicatorgrade = false;
                $scope.names = true;
                $scope.Indicatorgrade1 = false;
                $scope.Indicatorgrade2 = false;
                $scope.GradeSchemeShow = false;
                $scope.copyco_scholastic = false;

            }

            $scope.Co_ScholasticIndicatorShow = function (color) {
                $scope.headcolor = color;
                $scope.Co_Scholastic = false;
                $scope.Attributes = false;
                $scope.Indicator = true;
                $scope.Indic = "";
                $scope.pearlIndicatorgrade1 = false;

                $scope.Indic = { sims_co_scholastic_discriptive_indicator_status: true };
                $scope.NewInsert = true;
                $scope.term = false;
                $scope.Indicatorgrade = false;
                $scope.names = true;
                $scope.Indicatorgrade1 = false;
                $scope.Indicatorgrade2 = false;
                $scope.GradeSchemeShow = false;
                $scope.copyco_scholastic = false;

            }

            $scope.Co_ScholasticIndicatorgradeShow = function (color) {
                $scope.headcolor = color;
                $scope.Co_Scholastic = false;
                $scope.Attributes = false;
                $scope.Indicator = false;
                $scope.Indic = "";
                $scope.term = false;
                $scope.Indicatorgrade = true;
                $scope.names = true;
                $scope.Indicatorgrade1 = false;
                $scope.Indicatorgrade2 = false;
                $scope.GradeSchemeShow = false;
                $scope.copyco_scholastic = false;
                $scope.pearlIndicatorgrade1 = false;


            }

            $scope.Co_ScholasticSchemaGradeShow = function (color) {
                $scope.headcolor = color;
                $scope.gradescale = '';
                $scope.Co_Scholastic = false;
                $scope.Attributes = false;
                $scope.Indicator = false;
                $scope.Indic = "";
                $scope.term = false;
                $scope.Indicatorgrade = false;
                $scope.names = false;
                $scope.Indicatorgrade1 = false;
                $scope.Indicatorgrade2 = false;
                $scope.GradeSchemeShow = true;
                $scope.copyco_scholastic = false;
                $scope.pearlIndicatorgrade1 = false;

            }

            $scope.headcolor = '1';
            $scope.Co_ScholasticCopyShow = function (color) {
                $scope.headcolor = color;
                $scope.Co_Scholastic = false;
                $scope.Attributes = false;
                $scope.Indicator = false;
                $scope.term = false;
                $scope.Indicatorgrade = false;
                $scope.names = false;
                $scope.Indicatorgrade1 = false;
                $scope.Indicatorgrade2 = false;
                $scope.GradeSchemeShow = false;
                $scope.copyco_scholastic = true;
                $scope.GradeSchemeShow = false;

                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                    $scope.curriculum1 = res.data;
                    $scope.copys['cur_code'] = $scope.curriculum[0].sims_cur_code;
                    $scope.Getyear1($scope.copys.cur_code);

                });

                $scope.Getco_scholastic_for_copy();

            }

            $scope.AllGradeSectionSelect = function () {
                var main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.GetGradeData.length; i++) {
                    var t = $scope.GetGradeData[i].gradeCode;
                    var v = document.getElementById(t + i)
                    if (main.checked == true) {
                        v.checked = true;
                        for (var j = 0; j < $scope.GetGradeData[i].gradesectionlist.length; j++) {
                            var k = document.getElementById($scope.GetGradeData[i].gradesectionlist[j].section_code + t);
                            if (v.checked == true) {
                                k.checked = true;
                            } else {
                                k.checked = false;
                            }
                        }
                    }
                    else {
                        v.checked = false;
                        for (var j = 0; j < $scope.GetGradeData[i].gradesectionlist.length; j++) {
                            var k = document.getElementById($scope.GetGradeData[i].gradesectionlist[j].section_code + t);
                            if (v.checked == false) {
                                k.checked = false;
                            }
                        }
                    }
                }
            }

            $scope.SelectOneByOneGrade = function () {
                for (var i = 0; i < $scope.GetGradeData.length; i++) {
                    var v = document.getElementById($scope.GetGradeData[i].gradeCode + i)
                    for (var j = 0; j < $scope.GetGradeData[i].gradesectionlist.length; j++) {
                        var t = document.getElementById($scope.GetGradeData[i].gradesectionlist[j].section_code + $scope.GetGradeData[i].gradeCode)
                        if (v.checked == true) {
                            t.checked = true;
                        }
                        else {

                            t.checked = false;
                        }
                    }
                }
            }

            $scope.SelectOnyByOneSection = function (str) {

                var v = document.getElementById(str);
                if (v.checked == true) {
                    v.checked = false;
                }
            }

            /**************************************************************Start GradeBook Co_Scholastic Group*******************************************************************/

            $scope.Expand1 = true;

            $scope.NewInsert = true;

            $scope.btn_Co_ScholasticSave_Click = function (isvalid) {

                if (isvalid) {
                    var sectionlist = "";
                    var grade_code = '', sectioncode = '';
                    for (var i = 0; i < $scope.GetGradeData.length; i++) {
                        for (var j = 0; j < $scope.GetGradeData[i].gradesectionlist.length; j++) {
                            var val = document.getElementById($scope.GetGradeData[i].gradesectionlist[j].section_code + $scope.GetGradeData[i].gradeCode)
                            if (val.checked == true) {
                                sectionlist = sectionlist + $scope.GetGradeData[i].gradeCode + $scope.GetGradeData[i].gradesectionlist[j].section_code + ',';
                            }
                        }
                    }
                    var data = {
                        sims_academic_year: $scope.temp.sims_academic_year
                    , sims_cur_code: $scope.temp.sims_cur_code
                    , sims_grade_code: ""
                    , sims_section_code: ""
                    , sims_co_scholastic_group_code: $scope.temp.sims_co_scholastic_group_code
                    , sims_co_scholastic_group_name: $scope.temp.sims_co_scholastic_group_name
                    , sims_co_scholastic_group_name_ot: $scope.temp.sims_co_scholastic_group_name_ot
                    , sims_co_scholastic_group_description: $scope.temp.sims_co_scholastic_group_description
                    , sims_co_scholastic_group_heading: $scope.temp.sims_co_scholastic_group_heading
                    , sims_co_scholastic_group_note: $scope.temp.sims_co_scholastic_group_note
                    , sims_co_scholastic_group_assessment_count: $scope.temp.sims_co_scholastic_group_assessment_count
                    , sims_co_scholastic_group_display_order: $scope.temp.sims_co_scholastic_group_display_order
                    , sims_co_scolastic_employee_code: user
                    , sims_co_scolastic_grading_scale: $scope.temp.sims_co_scolastic_grading_scale
                    , sims_co_scolastic_Score_type: $scope.temp.sims_co_scolastic_Score_type
                    , sims_co_scholastic_group_status: $scope.temp.sims_co_scholastic_group_status
                    , sims_grd_sec_list: sectionlist
                    , sims_co_scholastic_teacherList: ''
                    , opr: 'I'
                    };
                    $http.post(ENV.apiUrl + "api/Gradebook/CoScholGroupDataOpr", data).then(function (msg) {
                        $scope.MSG1 = msg.data;

                        if ($scope.MSG1 == true) {

                            $scope.temp = '';
                            getgradesectiondata();
                            getallco_scholasticgroup(user);
                            swal({ text: 'Group Created Successfully', width: 320, showCloseButton: true });
                        }
                        else {
                            swal({ text: 'Group Not Created', width: 300, showCloseButton: true });
                        }
                    });
                }
            }

            function getallco_scholasticgroup(code) {
                var gradesection = [];
                var gradesection1 = [];
                $scope.NewInsert = true;

                $scope.busy2 = true;
                $http.get(ENV.apiUrl + "api/Gradebook/GetAllCoScholData?employee_code=" + code).then(function (getAllCoScholData) {
                    $scope.get_AllCoScholData = getAllCoScholData.data;
                    $scope.busy2 = false;
                    for (var i = 0; i < $scope.get_AllCoScholData.length; i++) {
                        if ($scope.get_AllCoScholData[i].attrlist == undefined) {
                            $scope.get_AllCoScholData[i].attrlist = [];
                        }
                        for (var j = 0; j < $scope.get_AllCoScholData[i].attrlist.length; j++) {
                            if ($scope.get_AllCoScholData[i].attrlist[j].indlist == undefined) {
                                $scope.get_AllCoScholData[i].attrlist[j].indlist = [];
                            }
                            $scope.get_AllCoScholData[i].attrlist[j].ischecked1 = false;
                            $scope.get_AllCoScholData[i].cnt = 0;
                        }
                    }
                    $scope.Attri = { sims_co_scholastic_attribute_status: true };
                    $scope.Indic = { sims_co_scholastic_discriptive_indicator_status: true };

                    $scope.names = true;

                });
            }

            $scope.edit = function (str, index) {

                $scope.scale_code = str.sims_co_scolastic_grading_scale;

                $scope.cur_gb_name = str.sims_co_scholastic_group_code + str.sims_grade_code + str.sims_section_code;
                $scope.Expand = $scope.cur_gb_name;
                $scope.assignmentcolor = '';
                $scope.catcolor = '';

                $scope.expandedd = false;
                var arr = [];
                if ($scope.Co_Scholastic == true || $scope.Indicator == true || $scope.Attributes == true) {
                    str.active = true;
                    $scope.temp = angular.copy(str);
                    $scope.Co_Scholastic = true;
                    $scope.NewInsert = false;
                    $scope.Attributes = false;
                    $scope.Indicator = false;

                    $http.get(ENV.apiUrl + "api/Gradebook/getAllCoScholTeacherName?group_code=" + $scope.temp.sims_co_scholastic_group_code + "&grade_code=" + str.sims_grade_code + "&section_code=" + str.sims_section_code).then(function (getAllCoScholTeacherName) {
                        $scope.getAllCoScholTeacherName = getAllCoScholTeacherName.data;

                        for (var i = 0; i < $scope.GetAllTeacher.length; i++) {
                            $scope.GetAllTeacher[i].teacher_freeze_status = false;
                            for (var j = 0; j < $scope.getAllCoScholTeacherName.length; j++) {
                                if ($scope.GetAllTeacher[i].sims_employee_code == $scope.getAllCoScholTeacherName[j].sims_employee_code) {
                                    $scope.GetAllTeacher[i].check = true;
                                    $scope.GetAllTeacher[i].teacher_freeze_status = $scope.getAllCoScholTeacherName[j].teacher_freeze_status;
                                    break;
                                }
                                else {
                                    $scope.GetAllTeacher[i].check = false;
                                }
                            }
                        }

                    });
                }
            }

            $scope.NewGradeBookCo_scholasticNames = function () {
                $scope.NewInsert = true;
                $scope.temp = "";
                $scope.temp = {
                    sims_academic_year: $scope.Academic_year[0].sims_academic_year,
                    sims_cur_code: $scope.curriculum[0].sims_cur_code,
                    sims_co_scholastic_group_status: true
                }

            }

            $scope.btn_Co_ScholasticCancel_click = function () {
                $scope.NewInsert = true;
                $scope.temp = "";
                $scope.temp = {
                    sims_academic_year: $scope.Academic_year[0].sims_academic_year,
                    sims_cur_code: $scope.curriculum[0].sims_cur_code,
                    sims_co_scholastic_group_status: true
                }
            }

            $scope.TeacherCheckAllChecked = function () {

                var main = document.getElementById('mainchk1');
                for (var i = 0; i < $scope.GetAllTeacher.length; i++) {
                    var t = document.getElementById($scope.GetAllTeacher[i].sims_employee_code)

                    if (main.checked == true) {
                        t.checked = true;
                    }
                    else {
                        t.checked = false;
                    }
                }
            }

            $scope.btn_Co_ScholasticUpdate_Click = function () {

                var teacherlist = "";

                for (var i = 0; i < $scope.GetAllTeacher.length; i++) {
                    if ($scope.GetAllTeacher[i].check == true) {
                        teacherlist = teacherlist + $scope.GetAllTeacher[i].sims_employee_code + ',';
                    }
                }

                var name = $scope.temp.sims_co_scholastic_group_dispaly_name;

                var data = {
                    sims_academic_year: $scope.temp.sims_academic_year
                , sims_cur_code: $scope.temp.sims_cur_code
                , sims_grade_code: $scope.temp.sims_grade_code
                , sims_section_code: $scope.temp.sims_section_code
                , sims_co_scholastic_group_code: $scope.temp.sims_co_scholastic_group_code
                , sims_co_scholastic_group_name: name
                , sims_co_scholastic_group_name_ot: $scope.temp.sims_co_scholastic_group_name_ot
                , sims_co_scholastic_group_description: $scope.temp.sims_co_scholastic_group_description
                , sims_co_scholastic_group_heading: $scope.temp.sims_co_scholastic_group_heading
                , sims_co_scholastic_group_note: $scope.temp.sims_co_scholastic_group_note
                , sims_co_scholastic_group_assessment_count: $scope.temp.sims_co_scholastic_group_assessment_count
                , sims_co_scholastic_group_display_order: $scope.temp.sims_co_scholastic_group_display_order
                , sims_co_scolastic_employee_code: user
                , sims_co_scolastic_grading_scale: $scope.temp.sims_co_scolastic_grading_scale
                , sims_co_scolastic_Score_type: $scope.temp.sims_co_scolastic_Score_type
                , sims_co_scholastic_group_status: $scope.temp.sims_co_scholastic_group_status
                , sims_grd_sec_list: ''
                , sims_co_scholastic_teacherList: teacherlist
                , opr: 'U'
                };
                $http.post(ENV.apiUrl + "api/Gradebook/CoScholGroupDataOpr", data).then(function (msg) {
                    $scope.MSG1 = msg.data;

                    if ($scope.MSG1 == true) {

                        var teacherdataobject = [];
                        for (var i = 0; i < $scope.GetAllTeacher.length; i++) {
                            if ($scope.GetAllTeacher[i].teacher_freeze_status == true) {
                                var data = {};
                                data.sims_academic_year = $scope.temp.sims_academic_year;
                                data.sims_cur_code = $scope.temp.sims_cur_code
                                data.sims_co_scholastic_group_code = $scope.temp.sims_co_scholastic_group_code
                                data.sims_grade_code = $scope.temp.sims_grade_code;
                                data.sims_section_code = $scope.temp.sims_section_code;
                                data.sims_co_scolastic_employee_code = $scope.GetAllTeacher[i].sims_employee_code
                                data.teacher_freeze_status = $scope.GetAllTeacher[i].teacher_freeze_status
                                teacherdataobject.push(data);
                            }

                            if ($scope.GetAllTeacher[i].ischange == true && $scope.GetAllTeacher[i].teacher_freeze_status == false) {
                                var data = {};
                                data.sims_academic_year = $scope.temp.sims_academic_year;
                                data.sims_cur_code = $scope.temp.sims_cur_code
                                data.sims_co_scholastic_group_code = $scope.temp.sims_co_scholastic_group_code
                                data.sims_grade_code = $scope.temp.sims_grade_code;
                                data.sims_section_code = $scope.temp.sims_section_code;
                                data.sims_co_scolastic_employee_code = $scope.GetAllTeacher[i].sims_employee_code
                                data.teacher_freeze_status = $scope.GetAllTeacher[i].teacher_freeze_status
                                teacherdataobject.push(data);
                            }

                        }
                        if (teacherdataobject.length > 0) {
                            $http.post(ENV.apiUrl + "api/Gradebook/CoScholGroupDataOprTeacherFreeze", teacherdataobject).then(function (msg) {
                                $scope.MSG2 = msg.data;

                                $scope.temp = '';
                                getgradesectiondata();
                                getallco_scholasticgroup(user);
                                teacherlist = "";
                                teachers();
                                swal({ text: 'Group Updated Successfully', width: 320, showCloseButton: true });

                            });
                        }
                        else {
                            $scope.temp = '';
                            getgradesectiondata();
                            getallco_scholasticgroup(user);
                            teacherlist = "";
                            teachers();
                            swal({ text: 'Group Updated Successfully', width: 320, showCloseButton: true });
                        }
                    }

                    else {
                        swal({ text: 'Group Not Updated', width: 320, showCloseButton: true });
                    }
                });
            }

            $scope.checkteacherIsfreezeorNot = function (info) {
                info.ischange = true;
            }

            $scope.SelectAllAttribute = function (info) {

                for (var j = 0; j < info.attrlist.length; j++) {
                    if (info.ischecked2 == true) {
                        info.attrlist[j].ischecked1 = true;
                        info.cnt = parseInt(info.cnt) + 1;
                    }
                    else {
                        info.attrlist[j].ischecked1 = false;
                        info.cnt = 0;
                    }
                }

            }

            $scope.SelectOneByOneAttribute = function (str, info, info1) {

                var cnt1 = parseInt(str.attrlist.length);

                if (info1.ischecked1 == true) {
                    str.cnt = parseInt(str.cnt) + 1;
                    if (cnt1 == str.cnt) {
                        str.ischecked2 = true;
                    }

                    else {
                        str.ischecked2 = false;
                    }
                }
                else {
                    str.cnt = parseInt(str.cnt) - 1;
                    str.ischecked2 = false;
                    info1.ischecked1 = false;
                }
            }

            /**************************************************************End GradeBook Co_Scholastic Group*******************************************************************/

            /**************************************************************Start GradeBook Attribute*******************************************************************/

            $scope.btn_AttributeSave_Click = function (isvalid) {
                if (isvalid) {
                    $scope.insert = false;
                    var DatasendObject = [];
                    for (var i = 0; i < $scope.get_AllCoScholData.length; i++) {
                        if ($scope.get_AllCoScholData[i].ischecked2 == true) {
                            $scope.insert = true;
                            var data = {
                                sims_academic_year: $scope.get_AllCoScholData[i].sims_academic_year
                              , sims_cur_code: $scope.get_AllCoScholData[i].sims_cur_code
                              , sims_grade_code: $scope.get_AllCoScholData[i].sims_grade_code
                              , sims_section_code: $scope.get_AllCoScholData[i].sims_section_code
                              , sims_co_scholastic_group_code: $scope.get_AllCoScholData[i].sims_co_scholastic_group_code
                              , sims_co_scholastic_attribute_code: $scope.Attri.sims_co_scholastic_attribute_code
                              , sims_co_scholastic_attribute_name: $scope.Attri.sims_co_scholastic_attribute_name
                              , sims_co_scholastic_attribute_name_ot: $scope.Attri.sims_co_scholastic_attribute_name_ot
                              , sims_co_scholastic_attribute_display_order: $scope.Attri.sims_co_scholastic_attribute_display_order
                              , sims_co_scholastic_attribute_status: $scope.Attri.sims_co_scholastic_attribute_status
                                , sims_co_scholastic_group_grade_scale_code: $scope.Attri.sims_co_scholastic_group_grade_scale_code
                              , opr: 'I'
                            };

                            DatasendObject.push(data);
                        }
                    }
                    if ($scope.insert != false) {
                        $http.post(ENV.apiUrl + "api/Gradebook/CoScholAttrDataOpr", DatasendObject).then(function (msg) {
                            $scope.MSG1 = msg.data;

                            if ($scope.MSG1 == true) {

                                $scope.A = '';
                                getgradesectiondata();
                                getallco_scholasticgroup(user);
                                swal({ text: 'Group Attribute Created Successfully', width: 320, showCloseButton: true });
                            }
                            else {
                                swal({ text: 'Group Attribute Not Created', width: 320, showCloseButton: true });
                            }
                        });
                    }
                    else {
                        swal({ text: 'Select At Least One Group  To Create Attribute', width: 320, showCloseButton: true });
                    }
                }
            }

            $scope.EditAttribute = function (info) {

                if (info.sims_co_scholastic_group_grade_scale_code != "") {
                    $scope.scale_code = info.sims_co_scholastic_group_grade_scale_code;
                }

                $scope.catcolor = info.sims_co_scholastic_attribute_code + info.sims_co_scholastic_group_code + info.sims_section_code;
                $scope.cur_gb_name = info.sims_co_scholastic_group_code + info.sims_grade_code + info.sims_section_code;
                $scope.assignmentcolor = '';

                $scope.disable = true;
                $scope.NewInsert = false;
                if ($scope.Co_Scholastic == true || $scope.Attributes == true || $scope.Indicator == true) {
                    $scope.headcolor = '2';
                    $scope.Co_Scholastic = false;
                    $scope.Attributes = true;
                    $scope.Indicator = false;
                    $scope.Attri = info;

                    $scope.Attri['Group_name'] = info.sims_grade_name + '-' + '(' + info.sims_section_name + ')' + '-' + info.sims_co_scholastic_group_name;
                }

            }

            $scope.btn_AttributeUpdate_Click = function () {

                var DatasendObject = [];

                var data = {

                    sims_academic_year: $scope.Attri.sims_academic_year
                    , sims_cur_code: $scope.Attri.sims_cur_code
                    , sims_grade_code: $scope.Attri.sims_grade_code
                    , sims_section_code: $scope.Attri.sims_section_code
                    , sims_co_scholastic_group_code: $scope.Attri.sims_co_scholastic_group_code
                    , sims_co_scholastic_attribute_code: $scope.Attri.sims_co_scholastic_attribute_code
                    , sims_co_scholastic_attribute_name: $scope.Attri.sims_co_scholastic_attribute_name
                    , sims_co_scholastic_attribute_name_ot: $scope.Attri.sims_co_scholastic_attribute_name_ot
                    , sims_co_scholastic_attribute_display_order: $scope.Attri.sims_co_scholastic_attribute_display_order
                    , sims_co_scholastic_attribute_status: $scope.Attri.sims_co_scholastic_attribute_status
                     , sims_co_scholastic_group_grade_scale_code: $scope.Attri.sims_co_scholastic_group_grade_scale_code
                    , opr: 'U'
                };
                DatasendObject.push(data)

                $http.post(ENV.apiUrl + "api/Gradebook/CoScholAttrDataOpr", DatasendObject).then(function (msg) {
                    $scope.MSG1 = msg.data;

                    if ($scope.MSG1 == true) {
                        $scope.Attri = "";
                        $scope.NewInsert = true;
                        $scope.disable = false;
                        getgradesectiondata();
                        getallco_scholasticgroup(user);
                        swal({ text: 'Group Attribute Updated Successfully', width: 320, showCloseButton: true });
                    }
                    else {
                        swal({ text: 'Group Attribute Not Updated', width: 320, showCloseButton: true });
                    }
                });
            }

            $scope.btn_AttributeCancel_Click = function () {

                $scope.disable = false;
                $scope.NewInsert = true;
                $scope.Attributes = true;
                $scope.Attri = "";
                $scope.Attri = { sims_co_scholastic_attribute_status: true };

            }

            /**************************************************************End GradeBook Attribute*******************************************************************/

            /**************************************************************Start GradeBook Indicator*******************************************************************/

            $scope.term = false;

            $scope.EditIndicator = function (info) {
                $scope.Indicatorgrade2 = false;
                $scope.catcolor = info.sims_co_scholastic_attribute_code + info.sims_co_scholastic_group_code + info.sims_section_code;
                $scope.assignmentcolor = info.sims_co_scholastic_attribute_code + info.sims_co_scholastic_group_code + info.sims_co_scholastic_discriptive_indicator_code;
                $scope.cur_gb_name = info.sims_co_scholastic_group_code + info.sims_grade_code + info.sims_section_code;

                if ($scope.Co_Scholastic == true || $scope.Attributes == true || $scope.Indicator == true || $scope.Indicatorgrade != true) {
                    $scope.headcolor = '3';
                    $scope.NewInsert = false;
                    $scope.NewInsert1 = true;
                    $scope.Co_Scholastic = false;
                    $scope.Attributes = false;
                    $scope.Indicator = true;
                    $scope.Indic = info;
                    $scope.term = true;
                    $scope.getattributenames(info.sims_section_code);

                    $http.post(ENV.apiUrl + "api/Gradebook/ScholStudentDataOpr", info).then(function (ScholStudentDataOpr) {
                        $scope.Schol_Student_DataOpr = ScholStudentDataOpr.data;
                        for (var i = 0; i < $scope.Schol_Student_DataOpr.length; i++) {
                            $scope.temlist = $scope.Schol_Student_DataOpr[i].termlist;
                        }
                    });

                    $scope.Indic['Group_name'] = $scope.Attri['Group_name'] = info.sims_grade_name + '-' + '(' + info.sims_section_name + ')' + '-' + info.sims_co_scholastic_group_name;;
                    $scope.Indic['sims_co_scholastic_attribute_name'] = info.sims_co_scholastic_attribute_dispaly_name;


                }
                else {
                    $scope.headcolor = '4';
                    if ($http.defaults.headers.common['schoolId'] == 'pearl') {
                        $scope.pearlIndicatorgrade1 = true;
                        $scope.Indicatorgrade1 = false;
                    }
                    else {
                        $scope.pearlIndicatorgrade1 = false;
                        $scope.Indicatorgrade1 = true;
                    }


                    var data = {
                        Stud_AcademicYear: info.sims_academic_year,
                        Stud_CurrCode: info.sims_cur_code,
                        Stud_GradeCode: info.sims_grade_code,
                        Stud_SectionCode: info.sims_section_code,
                        Group_No: info.sims_co_scholastic_group_code,
                        AttribNo: info.sims_co_scholastic_attribute_code,
                        IndCode: info.sims_co_scholastic_discriptive_indicator_code,
                        Stud_indic_teacher_code: user,
                        sims_term_code: info.sims_term_code,
                        opr: 'CT'
                    };

                    $scope.stud = info;

                    $http.post(ENV.apiUrl + "api/Gradebook/CoScholTeacher", data).then(function (GetCoScholTeacher) {
                        $scope.GetCoScholTeacher = GetCoScholTeacher.data;

                        if (user != 'admin') {
                            $scope.stud['SubTeacherEmpId'] = user;
                        }


                        //if (user != 'admin' && $scope.GetCoScholTeacher.length==0) {
                        //    $http.post(ENV.apiUrl + "api/Gradebook/CheckCoScholTeacher", data).then(function (checkCoScholTeacher) {
                        //        $scope.checkCoScholTeacher = checkCoScholTeacher.data;
                        //        if ($scope.checkCoScholTeacher == false) {
                        //            swal({ text: 'This Teacher Have Not Student In Co-Sholastic Indicator,You Want To Assign Student?', 
                        //                showCloseButton: true,
                        //                showCancelButton: true,
                        //                confirmButtonText: 'Yes',
                        //                width: 380,
                        //                cancelButtonText: 'No',
                        //            }).then(function (isConfirm) {
                        //                if (isConfirm) {

                        //                    $http.post(ENV.apiUrl + "api/Gradebook/UnsertStudentCo_Sholastic_Indicator", data).then(function (res) {
                        //                        $scope.msg = res.data;
                        //                    });
                        //                }
                        //            })
                        //        }

                        //    });
                        //}
                    });

                }
            }

            $scope.getattributenames = function (str) {
                $scope.attributenames = [];
                for (var i = 0; i < $scope.get_AllCoScholData.length; i++) {
                    if ($scope.get_AllCoScholData[i].sims_section_code == str) {
                        for (var j = 0; j < $scope.get_AllCoScholData[i].attrlist.length; j++) {
                            $scope.attributenames.push($scope.get_AllCoScholData[i].attrlist[j]);
                        }
                    }
                }
            }

            $scope.btn_AttributeIndicatorSave_Click = function (isvalid) {

                if (isvalid) {

                    var DataSendObject = [];
                    $scope.insert = false;

                    for (var i = 0; i < $scope.get_AllCoScholData.length; i++)
                        for (var j = 0; j < $scope.get_AllCoScholData[i].attrlist.length; j++) {
                            if ($scope.get_AllCoScholData[i].attrlist[j].ischecked1 == true) {

                                $scope.insert = true;

                                var data = {
                                    sims_academic_year: $scope.get_AllCoScholData[i].attrlist[j].sims_academic_year
                            , sims_cur_code: $scope.get_AllCoScholData[i].attrlist[j].sims_cur_code
                            , sims_grade_code: $scope.get_AllCoScholData[i].attrlist[j].sims_grade_code
                            , sims_section_code: $scope.get_AllCoScholData[i].attrlist[j].sims_section_code
                            , sims_co_scholastic_group_code: $scope.get_AllCoScholData[i].attrlist[j].sims_co_scholastic_group_code
                            , sims_co_scholastic_attribute_code: $scope.get_AllCoScholData[i].attrlist[j].sims_co_scholastic_attribute_code
                            , sims_co_scholastic_discriptive_indicator_code: $scope.Indic.sims_co_scholastic_discriptive_indicator_code
                            , sims_co_scholastic_discriptive_indicator_name: $scope.Indic.sims_co_scholastic_discriptive_indicator_name
                            , sims_co_scholastic_discriptive_indicator_name_ot: $scope.Indic.sims_co_scholastic_discriptive_indicator_name_ot
                            , sims_co_scholastic_discriptive_indicator_status: $scope.Indic.sims_co_scholastic_discriptive_indicator_status
                            , sims_term_code: $scope.Indic.sims_term_code
                            , opr: 'I'
                                };

                                DataSendObject.push(data);
                            }
                        }
                    if ($scope.insert != false) {
                        $http.post(ENV.apiUrl + "api/Gradebook/CoScholDescInd", DataSendObject).then(function (msg) {
                            $scope.MSG1 = msg.data;

                            if ($scope.MSG1 == true) {

                                $scope.Indic = '';
                                getgradesectiondata();
                                getallco_scholasticgroup(user);
                                swal({ text: 'Attribute Indicator Created Successfully', width: 380, showCloseButton: true });
                            }
                            else {
                                swal({ text: 'Attribute Indicator Not Created', width: 350, showCloseButton: true });
                            }
                        });
                    }
                    else {

                        swal({ text: 'Select At Least On Attribute To Created Indicator', width: 320, showCloseButton: true });
                    }
                }
            }

            $scope.btn_AttributeIndicatorUpdate_Click = function () {

                var datasend = []; var StudentList1 = ''; termdata = ''; var Teacher_code = '';
                for (var i = 0; i < $scope.Schol_Student_DataOpr.length; i++) {
                    for (var j = 0; j < $scope.Schol_Student_DataOpr[i].termlist.length; j++) {
                        if ($scope.Schol_Student_DataOpr[i].termlist[j].ischange == true && $scope.Schol_Student_DataOpr[i].termlist[j].studFlag == true) {
                            var data = {
                                sims_academic_year: $scope.Indic.sims_academic_year
                        , sims_cur_code: $scope.Indic.sims_cur_code
                        , sims_grade_code: $scope.Indic.sims_grade_code
                        , sims_section_code: $scope.Indic.sims_section_code
                        , sims_co_scholastic_group_code: $scope.Indic.sims_co_scholastic_group_code
                        , sims_co_scholastic_attribute_code: $scope.Indic.sims_co_scholastic_attribute_code
                        , sims_co_scholastic_discriptive_indicator_code: $scope.Indic.sims_co_scholastic_discriptive_indicator_code
                        , sims_co_scholastic_discriptive_indicator_name: $scope.Indic.sims_co_scholastic_discriptive_indicator_name
                        , sims_co_scholastic_discriptive_indicator_name_ot: $scope.Indic.sims_co_scholastic_discriptive_indicator_name_ot
                        , sims_co_scholastic_discriptive_indicator_status: $scope.Indic.sims_co_scholastic_discriptive_indicator_status
                        , sims_term_code: $scope.Schol_Student_DataOpr[i].termlist[j].sims_term_code
                        , StudentList: $scope.Schol_Student_DataOpr[i].stud_Enroll
                        , opr: 'A'
                            };

                            datasend.push(data);
                        }

                        else if ($scope.Schol_Student_DataOpr[i].termlist[j].ischange == true && $scope.Schol_Student_DataOpr[i].termlist[j].studFlag == false) {
                            var data = {
                                sims_academic_year: $scope.Indic.sims_academic_year
                        , sims_cur_code: $scope.Indic.sims_cur_code
                        , sims_grade_code: $scope.Indic.sims_grade_code
                        , sims_section_code: $scope.Indic.sims_section_code
                        , sims_co_scholastic_group_code: $scope.Indic.sims_co_scholastic_group_code
                        , sims_co_scholastic_attribute_code: $scope.Indic.sims_co_scholastic_attribute_code
                        , sims_co_scholastic_discriptive_indicator_code: $scope.Indic.sims_co_scholastic_discriptive_indicator_code
                        , sims_co_scholastic_discriptive_indicator_name: $scope.Indic.sims_co_scholastic_discriptive_indicator_name
                        , sims_co_scholastic_discriptive_indicator_name_ot: $scope.Indic.sims_co_scholastic_discriptive_indicator_name_ot
                        , sims_co_scholastic_discriptive_indicator_status: $scope.Indic.sims_co_scholastic_discriptive_indicator_status
                        , sims_term_code: $scope.Schol_Student_DataOpr[i].termlist[j].sims_term_code
                        , StudentList: $scope.Schol_Student_DataOpr[i].stud_Enroll
                        , sims_co_scolastic_employee_code: $scope.Schol_Student_DataOpr[i].termlist[j].stud_indic_teacher_code
                        , opr: 'R'
                            };

                            datasend.push(data);
                        }
                    }
                }
                if (datasend.length != 0) {
                    $http.post(ENV.apiUrl + "api/Gradebook/CoScholDescIndInsert", datasend).then(function (msg) {
                        $scope.MSG1 = msg.data;
                        if ($scope.MSG1 == true) {
                            $scope.term = false;
                            $scope.Indic = '';
                            getgradesectiondata();
                            getallco_scholasticgroup(user);
                            swal({ text: 'Indicator Assigned  Successfully', width: 380, showCloseButton: true });
                        }
                        else {
                            swal({ text: 'Indicator Not Assigned', width: 320, showCloseButton: true });
                        }
                    });
                }
                else {

                    var data = {
                        sims_academic_year: $scope.Indic.sims_academic_year
                        , sims_cur_code: $scope.Indic.sims_cur_code
                        , sims_grade_code: $scope.Indic.sims_grade_code
                        , sims_section_code: $scope.Indic.sims_section_code
                        , sims_co_scholastic_group_code: $scope.Indic.sims_co_scholastic_group_code
                        , sims_co_scholastic_attribute_code: $scope.Indic.sims_co_scholastic_attribute_code
                        , sims_co_scholastic_discriptive_indicator_code: $scope.Indic.sims_co_scholastic_discriptive_indicator_code
                        , sims_co_scholastic_discriptive_indicator_name: $scope.Indic.sims_co_scholastic_discriptive_indicator_name
                        , sims_co_scholastic_discriptive_indicator_name_ot: $scope.Indic.sims_co_scholastic_discriptive_indicator_name_ot
                        , sims_co_scholastic_discriptive_indicator_status: $scope.Indic.sims_co_scholastic_discriptive_indicator_status
                        , opr: 'U'
                    };

                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/Gradebook/CoScholDescIndInsert", datasend).then(function (msg) {
                        $scope.MSG1 = msg.data;
                        if ($scope.MSG1 == true) {
                            $scope.term = false;
                            $scope.Indic = '';
                            getgradesectiondata();
                            getallco_scholasticgroup(user);
                            swal({ text: 'Indicator Updated  Successfully', width: 380, showCloseButton: true });
                        }
                        else {
                            swal({ text: 'Indicator Not Updated', width: 320, showCloseButton: true });
                        }
                    });

                }
            }

            var termdata = "";

            $scope.btn_AttributeIndicatorCancel_Click = function () {
                $scope.term = false;
                $scope.NewInsert = true;
                $scope.Co_Scholastic = false;
                $scope.Attributes = false;
                $scope.Indicator = true;
                $scope.Indic = "";
                $scope.Indic = { sims_co_scholastic_discriptive_indicator_status: true };
            }

            $scope.selectAllTermCode = function (str, str1) {

                var check = document.getElementById(str);
                for (var i = 0; i < $scope.Schol_Student_DataOpr.length; i++) {
                    $scope.Schol_Student_DataOpr[i].termlist[check.tabIndex].studFlag = check.checked;
                    $scope.Schol_Student_DataOpr[i].termlist[check.tabIndex].ischange = check.checked;
                }

                $scope.NewInsert1 = false;
                $scope.NewInsert = false;

            }

            $scope.SelectOneByOneTerm = function (str, str1, info) {

                var v = document.getElementById(str1);
                for (var i = 0; i < info.termlist.length; i++) {
                    if (info.termlist[i].sims_term_code == str) {
                        info.ischange = true;
                        info.termlist[i].ischange = true;

                    }
                    //else {
                    //    if (info.termlist[i].sims_term_code == str) {
                    //        info.ischange = false;
                    //        info.termlist[i].ischange = false;
                    //    }
                    //}
                }
            }

            $scope.btn_AttributeIndicatorDelete_Click = function () {

                var datasend = [];

                for (var i = 0; i < $scope.Schol_Student_DataOpr.length; i++) {
                    for (var j = 0; j < $scope.Schol_Student_DataOpr[i].termlist.length; j++) {
                        if ($scope.Schol_Student_DataOpr[i].termlist[j].ischange == false && $scope.Schol_Student_DataOpr[i].termlist[j].studFlag == false) {

                            var data = {
                                sims_academic_year: $scope.Indic.sims_academic_year
                            , sims_cur_code: $scope.Indic.sims_cur_code
                            , sims_grade_code: $scope.Indic.sims_grade_code
                            , sims_section_code: $scope.Indic.sims_section_code
                            , sims_co_scholastic_group_code: $scope.Indic.sims_co_scholastic_group_code
                            , sims_co_scholastic_attribute_code: $scope.Indic.sims_co_scholastic_attribute_code
                            , sims_co_scholastic_discriptive_indicator_code: $scope.Indic.sims_co_scholastic_discriptive_indicator_code
                            , sims_term_code: $scope.Schol_Student_DataOpr[i].termlist[j].sims_term_code
                            , StudentList: $scope.Schol_Student_DataOpr[i].stud_Enroll
                            , sims_co_scolastic_employee_code: $scope.Schol_Student_DataOpr[i].termlist[j].stud_indic_teacher_code
                            , opr: 'E'
                            };

                            datasend.push(data);

                        }
                    }
                }


                $http.post(ENV.apiUrl + "api/Gradebook/CoScholDescIndstudent_delete", datasend).then(function (CoScholDescIndstudent_delete) {

                    $scope.CoScholDescIndstudent_delete = CoScholDescIndstudent_delete.data;

                    if ($scope.CoScholDescIndstudent_delete == true) {
                        $scope.term = false;
                        $scope.Indic = "";
                        $scope.NewInsert1 = false;
                        $scope.NewInsert = true;
                        getgradesectiondata();

                        getallco_scholasticgroup(user);
                        swal({ text: 'Indicator Student Removed Successfully', width: 320, showCloseButton: true });
                    }
                    else {
                        swal({ text: 'Indicator Student Not Removed', width: 320, showCloseButton: true });
                    }
                });

            }

            /**************************************************************End GradeBook Indicator*******************************************************************/

            /**************************************************************Start GradeBook Indicator Grade*******************************************************************/

            $http.get(ENV.apiUrl + "api/Gradebook/GetAllIndicatorNames?employee_code=" + user).then(function (GetAllIndicatorNames) {
                $scope.GetAllIndicatorNames = GetAllIndicatorNames.data;
            })

            $scope.btn_ShowIndicatorTeacherAndStudent_Click = function () {

                var teacher_code = '';
                if ($scope.deletebuttonshow == true) {
                    teacher_code = $scope.stud.SubTeacherEmpId;
                }
                else {
                    teacher_code = user;
                }

                var data = {
                    Stud_AcademicYear: $scope.stud.sims_academic_year,
                    Stud_CurrCode: $scope.stud.sims_cur_code,
                    Stud_GradeCode: $scope.stud.sims_grade_code,
                    Stud_SectionCode: $scope.stud.sims_section_code,
                    Group_No: $scope.stud.sims_co_scholastic_group_code,
                    AttribNo: $scope.stud.sims_co_scholastic_attribute_code,
                    IndCode: $scope.stud.sims_co_scholastic_discriptive_indicator_code,
                    sims_term_code: $scope.stud.sims_term_code,
                    SubTeacherEmpId: teacher_code
                };

                $scope.GetScholStudents = [];
                $scope.GetScholStudents1 = [];
                $scope.busy1 = true;
                $scope.Indicatorgrade2 = false;
                $http.post(ENV.apiUrl + "api/Gradebook/ScholStudents", data).then(function (GetScholStudents) {
                    $scope.GetScholStudents1 = GetScholStudents.data;

                    if ($scope.GetScholStudents1.length > 0) {
                        $scope.GetScholStudents = angular.copy(GetScholStudents.data);
                        for (var i = 0; i < $scope.GetCoScholTeacher.length; i++) {
                            for (var j = 0; j < $scope.GetScholStudents1.length; j++) {
                                if ($scope.stud.SubTeacherEmpId == $scope.GetCoScholTeacher[i].subTeacherEmpId) {
                                    $scope.GetScholStudents[j].isfreeze = $scope.GetCoScholTeacher[i].teacherStatus;
                                }
                            }
                        }
                        $scope.busy1 = false;
                        $scope.Indicatorgrade2 = true;
                        $scope.Group_Name = $scope.GetScholStudents[0].group_Name;
                        $scope.attribName = $scope.GetScholStudents[0].attribName;
                        $scope.ind_Name = $scope.GetScholStudents[0].ind_Name;

                    }
                    else {
                        $scope.busy1 = false;
                        $scope.Indicatorgrade2 = false;
                        swal({ text: 'Data Not Found', width: 300, showCloseButton: true });
                    }


                })

                $http.post(ENV.apiUrl + "api/Gradebook/ScholGrades?ScholGradeCode=" + $scope.scale_code).then(function (ScholGrades) {
                    $scope.ScholGrades = ScholGrades.data;

                })
            }

            $scope.btn_ShowIndicatorTeacherAndStudent_Click1 = function () {
                $scope.Indicatorgrade2 = false;
                $scope.busy = true;
                var data = {
                    Stud_AcademicYear: $scope.stud.sims_academic_year,
                    Stud_CurrCode: $scope.stud.sims_cur_code,
                    Stud_GradeCode: $scope.stud.sims_grade_code,
                    Stud_SectionCode: $scope.stud.sims_section_code,
                    Group_No: $scope.stud.sims_co_scholastic_group_code,
                    AttribNo: $scope.stud.sims_co_scholastic_attribute_code,
                    IndCode: $scope.stud.sims_co_scholastic_discriptive_indicator_code,
                    sims_term_code: $scope.stud.sims_term_code,
                    SubTeacherEmpId: $scope.stud.SubTeacherEmpId
                };

                $http.post(ENV.apiUrl + "api/Gradebook/ScholStudentspearl", data).then(function (GetScholStudents) {
                    $scope.GetScholStudents1 = GetScholStudents.data;
                    if ($scope.GetScholStudents1.length != 0) {
                        $scope.busy = false;
                        if ($scope.GetScholStudents1[0].stud_Enroll == '1') {
                            $scope.datadeleteobject = GetScholStudents.data;
                            $("#Div14").modal({ backdrop: false });
                        }
                        else {
                            $scope.busy = false;
                            $scope.GetScholStudents = angular.copy(GetScholStudents.data);
                            for (var i = 0; i < $scope.GetCoScholTeacher.length; i++) {
                                for (var j = 0; j < $scope.GetScholStudents.length; j++) {
                                    if ($scope.stud.SubTeacherEmpId == $scope.GetCoScholTeacher[i].subTeacherEmpId) {
                                        $scope.GetScholStudents[j].isfreeze = $scope.GetCoScholTeacher[i].teacherStatus;
                                    }
                                }
                            }
                            $scope.Indicatorgrade2 = true;
                            $scope.Group_Name = $scope.GetScholStudents[0].group_Name;
                            $scope.attribName = $scope.GetScholStudents[0].attribName;
                            $scope.ind_Name = $scope.GetScholStudents[0].ind_Name;

                            $http.post(ENV.apiUrl + "api/Gradebook/ScholGrades?ScholGradeCode=" + $scope.scale_code).then(function (ScholGrades) {
                                $scope.ScholGrades = ScholGrades.data;

                            });
                        }

                    }
                    else {
                        $scope.busy = false;
                        swal({ text: 'Data Not Found', width: 320, showCloseButton: true });
                    }
                })
            }

            $scope.AssignGrade = function (gradeno, info) {

                debugger;

                for (var i = 0; i < $scope.ScholGrades.length; i++) {
                    if (gradeno == $scope.ScholGrades[i].grade_Code) {
                        info.grade_Point = $scope.ScholGrades[i].grade_Point_high;
                        info.grade_Desc = $scope.ScholGrades[i].grade_Description;
                        info.ischecked = true;
                    }
                }
            }

            $scope.ApplyDefualtValues = function () {

                for (var i = 0; i < $scope.ScholGrades.length; i++) {
                    if ($scope.val.grade_Code == $scope.ScholGrades[i].grade_Code) {

                        for (var j = 0; j < $scope.GetScholStudents.length; j++) {
                            var t = document.getElementById('Overwrite');
                            if (t.checked == false) {
                                if ($scope.GetScholStudents[j].grade_Description == '' || $scope.GetScholStudents[j].grade_Point == '') {
                                    $scope.GetScholStudents[j].grade_Point = $scope.ScholGrades[i].grade_Point_high;
                                    $scope.GetScholStudents[j].grade_Desc = $scope.ScholGrades[i].grade_Description;
                                    $scope.GetScholStudents[j].grade_C = $scope.val.grade_Code;
                                    $scope.GetScholStudents[j].ischecked = true;
                                }
                            }
                            else {
                                $scope.GetScholStudents[j].grade_Point = $scope.ScholGrades[i].grade_Point_high;
                                $scope.GetScholStudents[j].grade_Desc = $scope.ScholGrades[i].grade_Description;
                                $scope.GetScholStudents[j].grade_C = $scope.val.grade_Code;
                                $scope.GetScholStudents[j].ischecked = true;
                            }
                        }
                    }
                }

            }

            $scope.val = {};

            $scope.btn_SetAllDefaultvalues_Click = function () {
                var t = document.getElementById('Overwrite');
                if (t.checked == true) {
                    t.checked = false;
                }
                $scope.val['grade_Code'] = '';
                $scope.GetScholStudents = angular.copy($scope.GetScholStudents1);

            }

            var DataSendObject = [];

            $scope.btn_SaveGradeBookCo_ScholasticData_Click = function () {
                $scope.insert = false;

                for (var j = 0; j < $scope.GetScholStudents.length; j++) {

                    if ($scope.GetScholStudents[j].ischecked == true) {
                        $scope.insert = true;
                        $scope.GetScholStudents[j].sims_term_code = $scope.stud.sims_term_code;
                        $scope.GetScholStudents[j].SubTeacherEmpId = $scope.stud.SubTeacherEmpId;
                        $scope.GetScholStudents[j].opr = "G"
                        DataSendObject.push($scope.GetScholStudents[j]);

                    }
                }
                if ($scope.insert == true) {
                    $http.post(ENV.apiUrl + "api/Gradebook/AssignScholGrade", DataSendObject).then(function (AssignScholGrade) {
                        $scope.msg = AssignScholGrade.data;

                        if ($scope.msg == true) {
                            swal({
                                text: 'Grade Or Mark Assigned Successfully',
                                width: 300,
                                showCloseButton: true
                            });
                            $scope.btn_ShowIndicatorTeacherAndStudent_Click();

                        }
                        else {
                            swal({
                                text: 'Grade Or Mark Not Assigned',
                                width: 300,
                                showCloseButton: true
                            });
                        }
                    })
                }
                else {
                    swal({ text: 'Change At Least One Student Record To Update Mark', width: 320, showCloseButton: true });
                }


            }

            $scope.pearlbtn_SaveGradeBookCo_ScholasticData_Click = function () {
                $scope.insert = false;

                for (var j = 0; j < $scope.GetScholStudents.length; j++) {

                    if ($scope.GetScholStudents[j].ischecked == true) {
                        $scope.insert = true;
                        $scope.GetScholStudents[j].sims_term_code = $scope.stud.sims_term_code;
                        $scope.GetScholStudents[j].SubTeacherEmpId = $scope.stud.SubTeacherEmpId;
                        $scope.GetScholStudents[j].opr = "G"
                        DataSendObject.push($scope.GetScholStudents[j]);

                    }
                }
                if ($scope.insert == true) {
                    $http.post(ENV.apiUrl + "api/Gradebook/AssignScholGrade", DataSendObject).then(function (AssignScholGrade) {
                        $scope.msg = AssignScholGrade.data;

                        if ($scope.msg == true) {
                            swal({
                                text: 'Grade Or Mark Assigned Successfully',
                                width: 300,
                                showCloseButton: true
                            });
                            $scope.btn_ShowIndicatorTeacherAndStudent_Click1();

                        }
                        else {
                            swal({
                                text: 'Grade Or Mark Not Assigned',
                                width: 300,
                                showCloseButton: true
                            });
                        }
                    })
                }
                else {
                    swal({ text: 'Change At Least One Student Record To Update Mark', width: 320, showCloseButton: true });
                }


            }

            $scope.CheckMark = function (info) {

                $scope.mark = angular.copy(info.grade_Point);

                for (var i = 0; i < $scope.ScholGrades.length; i++) {
                    if (info.grade_Scale_C == $scope.ScholGrades[i].gradegruopCode && parseInt($scope.mark) <= parseInt($scope.ScholGrades[i].grade_Point_high) && parseInt($scope.mark) >= parseInt($scope.ScholGrades[i].grade_Point_low)) {
                        info.grade_C = $scope.ScholGrades[i].grade_Code;
                        info.grade_Desc = $scope.ScholGrades[i].grade_Description;
                        info.grade_Point = $scope.mark;

                        info.ischecked = true;
                        break;
                    }
                    else {
                        info.grade_C = '';
                        info.grade_Desc = '';
                        info.grade_Point = '';
                    }
                }
            }

            $scope.btn_Ok_Click = function () {

                $scope.GetScholStudents = [];

                $scope.Indicatorgrade2 = false;
                $scope.busy = true;
                var data = {
                    Stud_AcademicYear: $scope.stud.sims_academic_year,
                    Stud_CurrCode: $scope.stud.sims_cur_code,
                    Stud_GradeCode: $scope.stud.sims_grade_code,
                    Stud_SectionCode: $scope.stud.sims_section_code,
                    Group_No: $scope.stud.sims_co_scholastic_group_code,
                    AttribNo: $scope.stud.sims_co_scholastic_attribute_code,
                    IndCode: $scope.stud.sims_co_scholastic_discriptive_indicator_code,
                    sims_term_code: $scope.stud.sims_term_code,
                    Stud_indic_teacher_code: $scope.stud.SubTeacherEmpId
                };

                $http.post(ENV.apiUrl + "api/Gradebook/UnsertStudentCo_Sholastic_Indicator", data).then(function (GetScholStudents2) {
                    $scope.GetScholStudents2 = GetScholStudents2.data;

                    if ($scope.GetScholStudents2.length > 0) {
                        $scope.GetScholStudents = angular.copy($scope.GetScholStudents2);
                        $scope.pearlIndicatorgrade1 = true;
                        $scope.Indicatorgrade2 = true;
                        for (var i = 0; i < $scope.GetCoScholTeacher.length; i++) {
                            for (var j = 0; j < $scope.GetScholStudents.length; j++) {
                                if ($scope.stud.SubTeacherEmpId == $scope.GetCoScholTeacher[i].subTeacherEmpId) {
                                    $scope.GetScholStudents[j].isfreeze = $scope.GetCoScholTeacher[i].teacherStatus;
                                }
                            }
                        }
                        $scope.Group_Name = $scope.GetScholStudents[0].group_Name;
                        $scope.attribName = $scope.GetScholStudents[0].attribName;
                        $scope.ind_Name = $scope.GetScholStudents[0].ind_Name;
                        $scope.busy = false;
                        $http.post(ENV.apiUrl + "api/Gradebook/ScholGrades?ScholGradeCode=" + $scope.scale_code).then(function (ScholGrades) {
                            $scope.ScholGrades = ScholGrades.data;

                        });

                    }
                    else {
                        $scope.busy = false;
                        swal({ text: 'Student not inserted', width: 320, showCloseButton: true });
                    }
                });

            }

            $scope.pearlbtn_SetAllDefaultvalues_Click = function () {
                $scope.busy = false;
            }
            /**************************************************************End GradeBook Indicator Grade*******************************************************************/

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            /**************************************************************Start Co_scholastic GradeSchema*******************************************************************/

            $scope.btn_insert = true;

            $scope.gradescalebtn = true;

            $scope.GetGradecodes = function () {

                $http.get(ENV.apiUrl + "api/Gradebook/GetAllgradeScaleforco_scholastic?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (GetAllgrade_Scale) {
                    $scope.GetAllgradeScale = GetAllgrade_Scale.data;

                    var gradelist = [];

                    for (var i = 0; i < $scope.GetAllgradeScale.length; i++) {
                        $scope.GetAllgradeScale[i].ischecked = false;
                        if ($scope.GetAllgradeScale[i].gradelst.length == 0) {
                            $scope.GetAllgradeScale[i].gradelst = [];
                        }
                        for (var j = 0; j < $scope.GetAllgradeScale[i].gradelst.length; j++) {
                            $scope.GetAllgradeScale[i].gradelst[j].ischecked = false;
                            gradelist.push($scope.GetAllgradeScale[i].gradelst[j]);
                        }
                    }

                    $scope.co_scholasticgradelistlateruse = angular.copy(gradelist);
                    $scope.co_scholasticgradelist = angular.copy(gradelist);

                });


            }

            $scope.ShowGradeWiseGradescale = function (info) {

                info.ischange = true;
                $scope.btn_insert = false;
                $scope.gradgroup = angular.copy(info);
                $scope.co_scholasticgradelist = [];
                var gardescaledata = [];
                $scope.gradescale = '';
                for (var i = 0; i < $scope.co_scholasticgradelistlateruse.length; i++) {
                    if ($scope.co_scholasticgradelistlateruse[i].gradegruopCode == info.gradeGroupCode) {
                        gardescaledata.push(angular.copy($scope.co_scholasticgradelistlateruse[i]));
                    }
                }
                $scope.co_scholasticgradelist = gardescaledata;
            }

            $scope.gradescale = {};

            $scope.RowUpdateGradeScale = function (info) {
                $scope.gradescalebtn = false;
                info.ischange = true;
                $scope.gradescale = angular.copy(info);

            }

            $scope.btn_GradeScaleCancel_click = function () {
                $scope.gradescale = "";
            }

            $scope.MultipleSelectScaleGrades = function () {

                var main = document.getElementById('MultipleSelect');

                for (var i = 0; i < $scope.co_scholasticgradelist.length; i++) {

                    if (main.checked == true) {

                        $scope.co_scholasticgradelist[i].ischecked = true;

                    }
                    else {

                        $scope.co_scholasticgradelist[i].ischecked = false;

                    }
                }
            }

            $scope.MultipleSelectGrades = function () {

                var main = document.getElementById('SelectGrades');
                for (var i = 0; i < $scope.GetAllgradeScale.length; i++) {
                    if (main.checked == true) {
                        $scope.GetAllgradeScale[i].ischecked = true;

                    }
                    else {
                        $scope.GetAllgradeScale[i].ischecked = false;

                    }
                }

            }

            $scope.btn_GradeScaleInsert_click = function () {
                var datasendobject = [];
                for (var i = 0; i < $scope.GetAllgradeScale.length; i++) {
                    if ($scope.GetAllgradeScale[i].ischecked == true) {

                        var data = {
                            opr: "I"
                            , Curr_Code: $scope.GetAllgradeScale[0].curr_Code
                            , Academic_Year: $scope.GetAllgradeScale[0].academic_Year
                            , GradegruopCode: $scope.GetAllgradeScale[i].gradeGroupCode
                            , Grade_Name: $scope.gradescale.grade_Name
                            , Grade_Point_low: $scope.gradescale.grade_Point_low
                            , Grade_Point_high: $scope.gradescale.grade_Point_high
                            , Grade_Description: $scope.gradescale.grade_Description
                            , Status: true
                        }
                        datasendobject.push(data);
                    }
                }

                if (datasendobject.length > 0) {
                    $http.post(ENV.apiUrl + "api/Gradebook/CUDgradeforco_scholastic", datasendobject).then(function (MSG) {
                        $scope.msg = MSG.data;
                        if ($scope.msg == true) {
                            swal({ text: 'Grade Inserted Successfully', width: 320, showCloseButton: true });
                            $scope.GetGradecodes();
                        }
                        else {
                            swal({ text: 'Grade Not Inserted', width: 320, showCloseButton: true });
                        }
                    });

                }
                else {
                    swal({ text: 'Select At Least One Grade Scale To Insert Grade', width: 320, showCloseButton: true });
                }
            }

            $scope.btn_GradeScaleUpdate_click = function () {

                var datasendobject = [];
                for (var i = 0; i < $scope.co_scholasticgradelist.length; i++) {
                    if ($scope.co_scholasticgradelist[i].ischange == true) {
                        $scope.co_scholasticgradelist[i].opr = 'U';
                        $scope.co_scholasticgradelist[i].Grade_Name = $scope.gradescale.grade_Name;
                        $scope.co_scholasticgradelist[i].Grade_Point_low = $scope.gradescale.grade_Point_low;
                        $scope.co_scholasticgradelist[i].Grade_Point_high = $scope.gradescale.grade_Point_high;
                        $scope.co_scholasticgradelist[i].Grade_Description = $scope.gradescale.grade_Description;
                        $scope.co_scholasticgradelist[i].Status = $scope.gradescale.status
                        datasendobject.push($scope.co_scholasticgradelist[i]);
                    }
                }

                if (datasendobject.length > 0) {
                    $http.post(ENV.apiUrl + "api/Gradebook/CUDgradeforco_scholastic", datasendobject).then(function (MSG) {
                        $scope.msg = MSG.data;
                        if ($scope.msg == true) {
                            swal({ text: 'Grade Updated Successfully', width: 320, showCloseButton: true });
                            $scope.btn_insert = true;
                            $scope.GetGradecodes();
                        }
                        else {
                            swal({ text: 'Grade Not Updated', width: 320, showCloseButton: true });
                        }
                    });

                }
                else {
                    swal({ text: 'Change At Least One Field To Update', width: 320, showCloseButton: true });
                }
            }

            $scope.btn_GradeScaleDelete_click = function () {

                var datasendobject = [];
                for (var i = 0; i < $scope.co_scholasticgradelist.length; i++) {
                    if ($scope.co_scholasticgradelist[i].ischecked == true) {
                        $scope.co_scholasticgradelist[i].opr = 'D';
                        datasendobject.push($scope.co_scholasticgradelist[i]);
                    }
                }

                if (datasendobject.length > 0) {
                    $http.post(ENV.apiUrl + "api/Gradebook/CUDgradeforco_scholastic", datasendobject).then(function (MSG) {
                        $scope.msg = MSG.data;
                        if ($scope.msg == true) {
                            swal({ text: 'Grade Deleted Successfully', width: 320, showCloseButton: true });
                            $scope.GetGradecodes();
                        }
                        else {
                            swal({ text: 'Grade Not Deleted', width: 320, showCloseButton: true });
                        }
                    });

                }
                else {
                    swal({ text: 'Select At Least One Grade To Delete', width: 320, showCloseButton: true });
                }
            }

            $scope.btn_GruopGradeInsert_click = function () {

                var datasendobject = [];

                var data = {
                    opr: "I"
                    , GradeGroupDesc: $scope.gradgroup.gradeGroupDesc
                    , Curr_Code: $scope.curriculum[0].sims_cur_code
                    , Academic_Year: $scope.Academic_year[0].sims_academic_year
                    , grade_status: $scope.gradgroup.grade_status
                }

                datasendobject.push(data);

                if (datasendobject.length > 0) {

                    $http.post(ENV.apiUrl + "api/Gradebook/CUDgradeScaleforco_scholastic", datasendobject).then(function (MSG) {
                        $scope.msg = MSG.data;
                        if ($scope.msg == true) {
                            swal({ text: 'Grade Gruop Inserted Successfully', width: 320, showCloseButton: true });
                            $scope.GetGradecodes();
                        }
                        else {
                            swal({ text: 'Grade Gruop Not Inserted', width: 320, showCloseButton: true });
                        }
                    });
                }
                else {

                    swal({ text: 'Fill Field To Insert Grade Group', width: 320, showCloseButton: true });
                }
            }

            $scope.btn_GruopGradeUpdate_click = function () {

                var datasendobject = [];
                for (var i = 0; i < $scope.GetAllgradeScale.length; i++) {
                    if ($scope.GetAllgradeScale[i].ischange == true) {
                        var data = {
                            opr: "U"
                            , GradeGroupDesc: $scope.gradgroup.gradeGroupDesc
                            , Curr_Code: $scope.GetAllgradeScale[i].curr_Code
                            , Academic_Year: $scope.GetAllgradeScale[i].academic_Year
                            , GradeGroupCode: $scope.GetAllgradeScale[i].gradeGroupCode
                            , grade_status: $scope.gradgroup.grade_status
                        }
                    }
                }
                datasendobject.push(data);

                if (datasendobject.length > 0) {

                    $http.post(ENV.apiUrl + "api/Gradebook/CUDgradeScaleforco_scholastic", datasendobject).then(function (MSG) {
                        $scope.msg = MSG.data;
                        if ($scope.msg == true) {
                            swal({ text: 'Grade Gruop Updated Successfully', width: 320, showCloseButton: true });
                            $scope.GetGradecodes();
                        }
                        else {
                            swal({ text: 'Grade Gruop Not Updated', width: 320, showCloseButton: true });
                        }
                    });
                }
                else {

                    swal({ text: 'Change At Least One Field To Update Grade Group', width: 350, showCloseButton: true });
                }
            }

            $scope.btn_GruopGradeDelete_click = function () {

                var datasendobject = [];
                for (var i = 0; i < $scope.GetAllgradeScale.length; i++) {
                    if ($scope.GetAllgradeScale[i].ischecked == true) {
                        $scope.GetAllgradeScale[i].opr = "D";
                        datasendobject.push($scope.GetAllgradeScale[i]);
                    }
                }


                if (datasendobject.length > 0) {

                    $http.post(ENV.apiUrl + "api/Gradebook/CUDgradeScaleforco_scholastic", datasendobject).then(function (MSG) {
                        $scope.msg = MSG.data;
                        if ($scope.msg == true) {
                            swal({ text: 'Grade Gruop Deleted Successfully', width: 320, showCloseButton: true });
                            $scope.GetGradecodes();
                        }
                        else {
                            swal({ text: 'Grade Gruop Not Deleted', width: 320, showCloseButton: true });
                        }
                    });
                }
                else {

                    swal({ text: 'Select At Least One Grade Group To Delete', width: 350, showCloseButton: true });
                }
            }

            $scope.btn_GruopGradeCancel_click = function () {
                $scope.btn_insert = true;
                $scope.gradgroup.gradeGroupDesc = '';
            }

            /**************************************************************End Co_scholastic GradeSchema*******************************************************************/

            /**************************************************************Start Co_scholastic Copy Code*******************************************************************/

            $scope.copyExpand = false;

            $scope.Getyear1 = function (newval) {

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + newval).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    $scope.copys['academic_year'] = $scope.Academic_year[0].sims_academic_year;
                    //$scope.Getterm1($scope.Academic_year[0].sims_academic_year);
                    $scope.getsections1(newval, $scope.Academic_year[0].sims_academic_year);
                });
            }

            //$scope.Getterm1 = function (oldval) {

            //    $http.get(ENV.apiUrl + "api/Gradebook/getTermforGradebook?cur_code=" + $scope.copys.cur_code + "&academic_year=" + oldval).then(function (Getsims550_TermsAcademicYear) {
            //        $scope.TermsAcademicYear = Getsims550_TermsAcademicYear.data;



            //    });
            //}

            $scope.getsection1 = function (str, str1, str2) {
                $http.get(ENV.apiUrl + "api/Gradebook/GetRCSection?curcode=" + str + "&ayear=" + str1 + "&gradecode=" + str2 + "&teachercode=" + user).then(function (Allsection) {
                    $scope.getSectionFromGrade = Allsection.data;
                })
            };

            $scope.getsections1 = function (str, str1) {

                $http.get(ENV.apiUrl + "api/Gradebook/GetRCGrade?curcode=" + str + "&ayear=" + str1 + "&teachercode=" + user).then(function (res) {
                    $scope.getAllGrades = res.data;
                    $scope.getsection1(str, str1, $scope.getAllGrades[0].sims_grade_code);
                })
            };

            $scope.Getco_scholastic_for_copy = function () {

                if ($scope.copys.sims_grade_code == undefined) {
                    $scope.copys.sims_grade_code = 'null';
                }

                $scope.Indihow = false;
                $scope.Attributeshow = false;
                $scope.groupshow = true;

                $http.get(ENV.apiUrl + "api/Gradebook/getCo_ScholasticForCopy?cur_code=" + $scope.copys.cur_code + "&academic_year=" + $scope.copys.academic_year + "&grade_code=" + $scope.copys.sims_grade_code).then(function (Co_Scholastic_copy_data) {
                    $scope.Co_Scholastic_copy_data = Co_Scholastic_copy_data.data;
                });

                $http.get(ENV.apiUrl + "api/Gradebook/GetGradeData").then(function (GetGradeData1) {
                    $scope.GetGradeData1 = GetGradeData1.data;
                });


            }

            $scope.Getco_scholastic_Attribute_for_copy = function () {

                $scope.Indihow = false;
                $scope.Attributeshow = true;
                $scope.groupshow = false;

                if ($scope.copys.sims_grade_code == undefined) {
                    $scope.copys.sims_grade_code = 'null';
                }

                $http.get(ENV.apiUrl + "api/Gradebook/getCo_ScholasticForCopy?cur_code=" + $scope.copys.cur_code + "&academic_year=" + $scope.copys.academic_year + "&grade_code=" + $scope.copys.sims_grade_code).then(function (Co_Scholastic_att1_data) {
                    $scope.Co_Scholastic_att1_data = Co_Scholastic_att1_data.data;
                })

                $http.get(ENV.apiUrl + "api/Gradebook/getCo_ScholasticForCopy?cur_code=" + $scope.copys.cur_code + "&academic_year=" + $scope.copys.academic_year + "&grade_code=" + null).then(function (Co_Scholastic_att_data) {
                    $scope.Co_Scholastic_att_data = Co_Scholastic_att_data.data;
                })

            }

            $scope.Getco_scholastic_Indicator_for_copy = function () {

                $scope.Indihow = true;
                $scope.Attributeshow = false;
                $scope.groupshow = false;

                if ($scope.copys.sims_grade_code == undefined) {
                    $scope.copys.sims_grade_code = 'null';
                }

                $http.get(ENV.apiUrl + "api/Gradebook/getCo_ScholasticForCopy?cur_code=" + $scope.copys.cur_code + "&academic_year=" + $scope.copys.academic_year + "&grade_code=" + $scope.copys.sims_grade_code).then(function (Co_Scholastic_att2_data) {
                    $scope.Co_Scholastic_att2_data = Co_Scholastic_att2_data.data;
                })

                $http.get(ENV.apiUrl + "api/Gradebook/getCo_ScholasticForCopy?cur_code=" + $scope.copys.cur_code + "&academic_year=" + $scope.copys.academic_year + "&grade_code=" + null).then(function (Co_Scholastic_att3_data) {
                    $scope.Co_Scholastic_att3_data = Co_Scholastic_att3_data.data;
                })

            }

            $scope.Click_On_Group_Copy_Button = function () {

                var copygroupobject = [];

                for (var i = 0; i < $scope.Co_Scholastic_copy_data.length; i++) {
                    if ($scope.Co_Scholastic_copy_data[i].ischecked2 == true) {
                        for (var j = 0; j < $scope.GetGradeData1.length; j++) {
                            for (var k = 0; k < $scope.GetGradeData1[j].gradesectionlist.length; k++) {
                                if ($scope.GetGradeData1[j].gradesectionlist[k].ischecked1 == true) {
                                    var data = {};

                                    data.CO_SCHO_CUR = $scope.Co_Scholastic_copy_data[i].sims_cur_code;
                                    data.CO_SCHO_ACADEMIC_YEAR = $scope.Co_Scholastic_copy_data[i].sims_academic_year;
                                    data.CO_SCHO_GRADE_CODE = $scope.Co_Scholastic_copy_data[i].sims_grade_code;
                                    data.CO_SCHO_SECTION_CODE = $scope.Co_Scholastic_copy_data[i].sims_section_code;
                                    data.CO_SCHO_GROUP_CODE = $scope.Co_Scholastic_copy_data[i].sims_co_scholastic_group_code;
                                    data.CO_SCHO_TO_CUR = $scope.Co_Scholastic_copy_data[i].sims_cur_code;
                                    data.CO_SCHO_TO_ACADEMIC_YEAR = $scope.Co_Scholastic_copy_data[i].sims_academic_year;
                                    data.CO_SCHO_TO_GRADE_CODE = $scope.GetGradeData1[j].gradeCode;
                                    data.CO_SCHO_TO_SECTION_CODE = $scope.GetGradeData1[j].gradesectionlist[k].section_code;
                                    data.CO_SCHO_TO_IsAttribute = $scope.groupIsAttribute == true ? 'Y' : 'N';
                                    data.CO_SCHO_TO_IsIndicator = $scope.groupIsIndicator == true ? 'Y' : 'N';
                                    copygroupobject.push(data);
                                }
                            }
                        }
                    }

                }

                if (copygroupobject.length > 0) {

                    $http.post(ENV.apiUrl + "api/Gradebook/ScholasticGroupCopy", copygroupobject).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: 'Group Created Successfully', width: 320, showCloseButton: true });
                            $scope.groupIsAttribute = false;
                            $scope.groupIsIndicator = false;
                            $scope.Getco_scholastic_for_copy();
                        }
                        else {
                            swal({ text: 'Group Not Created', width: 320, showCloseButton: true });

                        }
                    })
                }
            }

            $scope.Click_On_Attribute_Copy_Button = function () {

                var copyattributeobject = [];

                for (var i = 0; i < $scope.Co_Scholastic_att1_data.length; i++) {

                    if ($scope.Co_Scholastic_att1_data[i].attrlist == undefined) {
                        $scope.Co_Scholastic_att1_data[i].attrlist = [];
                    }

                    for (var k = 0; k < $scope.Co_Scholastic_att1_data[i].attrlist.length; k++) {
                        if ($scope.Co_Scholastic_att1_data[i].attrlist[k].ischecked1 == true) {

                            for (var j = 0; j < $scope.Co_Scholastic_att_data.length; j++) {
                                if ($scope.Co_Scholastic_att_data[j].ischecked2 == true) {
                                    var data = {};

                                    data.CO_SCHO_CUR = $scope.Co_Scholastic_att1_data[i].attrlist[k].sims_cur_code;
                                    data.CO_SCHO_ACADEMIC_YEAR = $scope.Co_Scholastic_att1_data[i].attrlist[k].sims_academic_year;
                                    data.CO_SCHO_GRADE_CODE = $scope.Co_Scholastic_att1_data[i].attrlist[k].sims_grade_code;
                                    data.CO_SCHO_SECTION_CODE = $scope.Co_Scholastic_att1_data[i].attrlist[k].sims_section_code;
                                    data.CO_SCHO_GROUP_CODE = $scope.Co_Scholastic_att1_data[i].attrlist[k].sims_co_scholastic_group_code;
                                    data.CO_SCHO_GROUP_ATT_CODE = $scope.Co_Scholastic_att1_data[i].attrlist[k].sims_co_scholastic_attribute_code

                                    data.CO_SCHO_TO_CUR = $scope.Co_Scholastic_att_data[j].sims_cur_code;
                                    data.CO_SCHO_TO_ACADEMIC_YEAR = $scope.Co_Scholastic_att_data[j].sims_academic_year;
                                    data.CO_SCHO_TO_GRADE_CODE = $scope.Co_Scholastic_att_data[j].sims_grade_code;
                                    data.CO_SCHO_TO_SECTION_CODE = $scope.Co_Scholastic_att_data[j].sims_section_code;
                                    data.CO_SCHO_TO_GROUP_CODE = $scope.Co_Scholastic_att_data[j].sims_co_scholastic_group_code;
                                    data.CO_SCHO_TO_IsIndicator = $scope.AttriIsIndicator == true ? 'Y' : 'N';
                                    copyattributeobject.push(data);
                                }
                            }
                        }

                    }

                    if (copyattributeobject.length > 0) {

                        $http.post(ENV.apiUrl + "api/Gradebook/ScholasticAttributeCopy", copyattributeobject).then(function (msg) {
                            $scope.msg1 = msg.data;

                            if ($scope.msg1 == true) {
                                swal({ text: 'Attribute Insrted Successfully', width: 320, showCloseButton: true });
                                $scope.AttriIsIndicator = false;
                                $scope.Getco_scholastic_for_copy();
                            }
                            else {
                                swal({ text: 'Attribute Not Insrted', width: 320, showCloseButton: true });

                            }
                        })
                    }

                }
            }

            $scope.Click_On_Indicator_Copy_Button = function () {

                var copyindicatorobject = [];
                for (var i = 0; i < $scope.Co_Scholastic_att3_data.length; i++) {

                    if ($scope.Co_Scholastic_att3_data[i].attrlist == undefined) {
                        $scope.Co_Scholastic_att3_data[i].attrlist = [];
                    }

                    for (var k = 0; k < $scope.Co_Scholastic_att3_data[i].attrlist.length; k++) {

                        if ($scope.Co_Scholastic_att3_data[i].attrlist[k].indlist == undefined) {
                            $scope.Co_Scholastic_att3_data[i].attrlist[k].indlist = [];
                        }
                        for (var m = 0; m < $scope.Co_Scholastic_att3_data[i].attrlist[k].indlist.length; m++) {

                            if ($scope.Co_Scholastic_att3_data[i].attrlist[k].indlist[m].ischecked1 == true) {

                                for (var j = 0; j < $scope.Co_Scholastic_att2_data.length; j++) {

                                    if ($scope.Co_Scholastic_att2_data[j].attrlist == undefined) {
                                        $scope.Co_Scholastic_att2_data[j].attrlist = [];
                                    }

                                    for (var p = 0; p < $scope.Co_Scholastic_att2_data[j].attrlist.length; p++) {
                                        if ($scope.Co_Scholastic_att2_data[j].attrlist[p].ischecked1 == true) {
                                            var data = {};

                                            data.CO_SCHO_CUR = $scope.Co_Scholastic_att3_data[i].attrlist[k].indlist[m].sims_cur_code;
                                            data.CO_SCHO_ACADEMIC_YEAR = $scope.Co_Scholastic_att3_data[i].attrlist[k].indlist[m].sims_academic_year;
                                            data.CO_SCHO_GRADE_CODE = $scope.Co_Scholastic_att3_data[i].attrlist[k].indlist[m].sims_grade_code;
                                            data.CO_SCHO_SECTION_CODE = $scope.Co_Scholastic_att3_data[i].attrlist[k].indlist[m].sims_section_code;
                                            data.CO_SCHO_GROUP_CODE = $scope.Co_Scholastic_att3_data[i].attrlist[k].indlist[m].sims_co_scholastic_group_code;
                                            data.CO_SCHO_GROUP_ATT_CODE = $scope.Co_Scholastic_att3_data[i].attrlist[k].indlist[m].sims_co_scholastic_attribute_code;
                                            data.CO_SCHO_GROUP_ATT_INDI_CODE = $scope.Co_Scholastic_att3_data[i].attrlist[k].indlist[m].sims_co_scholastic_discriptive_indicator_code

                                            data.CO_SCHO_TO_CUR = $scope.Co_Scholastic_att2_data[j].attrlist[p].sims_cur_code;
                                            data.CO_SCHO_TO_ACADEMIC_YEAR = $scope.Co_Scholastic_att2_data[j].attrlist[p].sims_academic_year;
                                            data.CO_SCHO_TO_GRADE_CODE = $scope.Co_Scholastic_att2_data[j].attrlist[p].sims_grade_code;
                                            data.CO_SCHO_TO_SECTION_CODE = $scope.Co_Scholastic_att2_data[j].attrlist[p].sims_section_code;
                                            data.CO_SCHO_TO_GROUP_CODE = $scope.Co_Scholastic_att2_data[j].attrlist[p].sims_co_scholastic_group_code;
                                            data.CO_SCHO_TO_GROUP_ATT_CODE = $scope.Co_Scholastic_att2_data[j].attrlist[p].sims_co_scholastic_attribute_code;

                                            copyindicatorobject.push(data);
                                        }
                                    }
                                }
                            }

                        }
                    }

                }

                if (copyindicatorobject.length > 0) {

                    $http.post(ENV.apiUrl + "api/Gradebook/ScholasticIndicatorCopy", copyindicatorobject).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: 'Indicator Insrted Successfully', width: 320, showCloseButton: true });
                            $scope.Getco_scholastic_for_copy();
                        }
                        else {
                            swal({ text: 'Indicator Not Insrted', width: 320, showCloseButton: true });
                        }
                    })
                }
            }


            /**************************************************************End Co_scholastic Copy Code*******************************************************************/

        }]);
})();




