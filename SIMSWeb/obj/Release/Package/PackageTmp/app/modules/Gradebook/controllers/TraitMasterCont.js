﻿(function () {
    'use strict';
    var main = '';
    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

    simsController.controller('TraitMasterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.busyindicator = false;
            $scope.grid = true;
            $scope.color = '#edefef';
            $scope.subjectReadonly = true;

            var uname = $rootScope.globals.currentUser.username;

            $('#cmd_subject').change(function () {
                console.log($(this).val());
            }).multipleSelect({
                width: '100%'
            });
          
         
            $("#cmd_subject").multipleSelect("disable");

            $scope.select_sub = function () {
                $("#cmd_subject").multipleSelect("enable");
            }


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.edt = {
                    'sims_cur_code': $scope.curriculum[0].sims_cur_code
                }
                $scope.getAccYear($scope.edt.sims_cur_code);
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.edt = {
                        'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                        'sims_academic_year': $scope.Acc_year[0].sims_academic_year
                    }
                    $scope.getGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                });
            }

            //$scope.getGrade = function (curCode, accYear) {
            //    $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Gradecode) {
            //        $scope.Grade_code = Gradecode.data;
            //    });
            //}

            //$scope.getSection = function (curCode, gradeCode, accYear) {
            //    $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
            //        $scope.Section_code = Sectioncode.data;
            //    });
            //}

            $scope.getGrade = function (curCode, accYear) {
              
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $(function () {
                $('#cmb_grade_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.getSection = function (curCode, gradeCode, accYear) {
              
                $http.get(ENV.apiUrl + "api/Trait/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $(function () {
                $('#cmb_section_code').multipleSelect({
                    width: '100%'
                });
            });
           
            $http.get(ENV.apiUrl + "api/common/getAllTerms").then(function (Allterms) {
                $scope.Term_data = Allterms.data;
            });

            $http.get(ENV.apiUrl + "api/Trait/getSubjectsTraits").then(function (Alltrait) {
                $scope.SubTrait_data = Alltrait.data;
            });

            //$http.get(ENV.apiUrl + "api/Trait/getSubjects").then(function (Allsub) {
            //    $scope.Subj_data = Allsub.data;
            //});
           
            $http.get(ENV.apiUrl + "api/Trait/getSubjects").then(function (getSubjectsdata) {
                $scope.getSubjects_data = getSubjectsdata.data;
                    setTimeout(function () {
                        $('#cmd_subject').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });

            $(function () {
                $('#cmd_subject').multipleSelect({
                    width: '100%'
                });
            });


           

            $scope.file_doc = [];
            $scope.AddTrait = function (str, str1) {
                debugger;
                var t = {
                    sims_trait_name: str,
                    trait_status: str1
                }
                $scope.file_doc.push(t);
            }
            $scope.RemoveEnrollMentNo = function ($event, index, str) {
                str.splice(index, 1);
            }
            

            $scope.SaveData = function () {//Myform
                debugger;
                var datasend = [];
                // if (Myform) {
                var section;
                var subject;
               
                for (var i = 0; i < $scope.edt.sims_section_code.length; i++) {
                    section = $scope.edt.sims_section_code + ',';
                }

                for (var i = 0; i < $scope.edt.sims_subject_code.length; i++) {
                    subject = $scope.edt.sims_subject_code + ',';
                }

                    for (var i = 0; i < $scope.file_doc.length; i++) {
                        var data = {
                            sims_cur_code: $scope.edt.sims_cur_code,
                            sims_academic_year: $scope.edt.sims_academic_year,
                            sims_grade_code: '',
                            sims_section_code: section,
                            sims_term_code: $scope.edt.sims_term_code,
                            sims_subject_code: subject,
                            sims_trait_name: $scope.file_doc[i].sims_trait_name,
                            trait_status: $scope.file_doc[i].trait_status
                        }
                        datasend.push(data);
                    }

                    $http.post(ENV.apiUrl + "api/Trait/CUDTrait", datasend).then(function (res) {
                        $scope.AssignmentData = res.data;
                        if ($scope.AssignmentData == true) {
                            swal({ text: "Record Inserted Successfully", width: 380, showCloseButton: true });
                            $scope.edt = "";
                            $scope.file_doc = "";
                            $scope.edt.sims_grade_code = "";
                            $scope.edt.sims_section_code = "";
                            $scope.edt.sims_subject_code = "";
                        }
                        else {
                            swal({ text: "Record Not Inserted", width: 380, showCloseButton: true });
                        }

                    });
                    $scope.file_doc = [];
                //}
            }
        

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }])
})();