﻿(function () {
    'use strict';
    angular.module('sims.module.Gradebook', [
        'sims',
         'bw.paging',
        'gettext'
    ]);
})();