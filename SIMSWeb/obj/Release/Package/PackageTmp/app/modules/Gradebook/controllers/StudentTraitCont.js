﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StudentTraitCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.search_btn = true;
            $scope.clr_btn = true;
            $scope.Update_btn = false;
            $scope.newdisplay = false;
            $scope.table = true;
            $scope.grid = false;
            $scope.Comment_entry = false;
            $scope.no_entry = true;
            $scope.update_btn_disable = true;
            var fetchedRecords = [];


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.temp = {
                    'sims_cur_code': $scope.curriculum[0].sims_cur_code
                }
                $scope.getAccYear($scope.temp.sims_cur_code);
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = {
                        'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                        'sims_academic_year': $scope.Acc_year[0].sims_academic_year
                    }
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                });
            }

            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/StudentTrait/getAllGrades?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
            }

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/StudentTrait/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }

            $scope.getTerm = function (curCode, accYear, gradeCode, sectionCode) {
                $http.get(ENV.apiUrl + "api/StudentTrait/getAllTerms?cur_code=" + curCode + "&academic_year=" + accYear + "&grade_code=" + gradeCode + "&section_code=" + sectionCode).then(function (Allterms) {
                    $scope.Term_data = Allterms.data;
                });
            }


            $scope.getAll_students_traits = function (grade, section, term, trait) {
                $scope.stud_enroll_name_grid = true;
                $scope.trait_grid = true;
                $scope.update_btn_disable = true;
                $http.get(ENV.apiUrl + "api/StudentTrait/getAllStudentTrait?sims_grade_code=" + grade + "&sims_section_code=" + section +
                    "&sims_term_code=" + term + "&sims_trait_code=" + trait).then(function (AllStudentTrait) {
                        $scope.AllStudent_Trait_data = AllStudentTrait.data;
                        $timeout(function () {
                            $("#fixTable1").tableHeadFixer({ 'left': 1 });
                        }, 100);
                    });
            }

            $scope.getTraits = function (grade, section, term) {

                $http.get(ENV.apiUrl + "api/StudentTrait/getTrait?sims_grade_code=" + grade + "&sims_section_code=" + section + "&sims_term_code=" + term).then(function (Alltrait) {
                    $scope.Trait_data = Alltrait.data;
                    setTimeout(function () {
                        $('#cmb_trait_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $(function () {
                $('#cmb_trait_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.countData = [
                    { val: 5, data: 5 },
                    { val: 10, data: 10 },
                    { val: 15, data: 15 },
            ]

            $scope.size = function (str) {
                if (str == 5 || str == 10 || str == 15) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    $scope.filteredTodos[i].icon = 'fa fa-plus-circle';
                }


            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                debugger;
                $scope.StudEA_Details = $scope.searched($scope.StudEA_Details, $scope.searchText);
                if ($scope.searchText != '') {
                    $scope.StudEA_Details = $scope.StudEA_Details;
                }
                else { $scope.StudEA_Details = $scope.fetchedRecords; }

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_enrollment_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_student_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_student_ea_number == toSearch) ? true : false;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);
            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.newdisplay = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.dest_code = "";
                $scope.temp.country_code = "";
                $scope.temp.dest_name = "";
            }

            $scope.clear = function () {

                $scope.temp.sims_grade_code = '';
                $scope.temp.sims_section_code = '';
                $scope.temp.sims_term_code = '';
                $scope.AllStudent_Trait_data = [];
                $scope.studtraitlst = [];
                $scope.trait_grid = false;
                $scope.stud_enroll_name_grid = false;
                $scope.updt_btn = false;
                $scope.grid = true;
                $scope.table = true;

                try {
                    $('#cmb_trait_code').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_trait_code').multipleSelect('uncheckAll');
                }
                catch (e) {

                }
            }

            $scope.change_comment = function (ea) {
                $scope.update_btn_disable = false;
                ea.isChange = true;
            }

            $scope.show_comment_entry = function (stud, index) {
                $scope.updt_btn = true;
                $scope.studtraitlst = stud.studtraitlst;
                if ($scope.old_index != undefined)
                    $scope.AllStudent_Trait_data[parseInt($scope.old_index)]['back'] = '';
                stud['back'] = '#faf0e6'
                $scope.old_index = index;


            }

            $scope.update = function (Myform) {

                if (Myform == false) {
                    swal({ text: "Please Enter Valid data to Update", width: 380, showCloseButton: true });
                }

                if (Myform) {


                    var dataupdate = [];
                    var data = {};
                    for (var k = 0; k < $scope.AllStudent_Trait_data.length; k++) {
                        for (var a = 0; a < $scope.AllStudent_Trait_data[k].studtraitlst.length; a++) {
                            if ($scope.AllStudent_Trait_data[k].studtraitlst[a].isChange == true) {
                                data =
                               {
                                   sims_cur_code: $scope.temp.sims_cur_code,
                                   sims_academic_year: $scope.temp.sims_academic_year,
                                   sims_term_code: $scope.temp.sims_term_code,
                                   sims_grade_code: $scope.temp.sims_grade_code,
                                   sims_section_code: $scope.temp.sims_section_code,
                                   sims_enroll_number: $scope.AllStudent_Trait_data[k].sims_enroll_number,
                                   sims_trait_code: $scope.AllStudent_Trait_data[k].studtraitlst[a].sims_trait_code,
                                   sims_trait_comment1: $scope.AllStudent_Trait_data[k].studtraitlst[a].sims_trait_comment1,
                                   sims_trait_comment2: $scope.AllStudent_Trait_data[k].studtraitlst[a].sims_trait_comment2,
                                   sims_trait_comment3: $scope.AllStudent_Trait_data[k].studtraitlst[a].sims_trait_comment3,
                                   sims_trait_comment4: $scope.AllStudent_Trait_data[k].studtraitlst[a].sims_trait_comment4,
                                   opr: 'I',
                               }
                                dataupdate.push(data);
                            }
                        }
                    }


                    if (dataupdate.length > 0) {
                        $http.post(ENV.apiUrl + "api/StudentTrait/CUStudentTrait", dataupdate).then(function (msg) {
                            $scope.msg1 = msg.data;

                            if ($scope.msg1 == true) {
                                swal({ text: "Record Updated Successfully", width: 300, showCloseButton: true });
                            }
                            else {
                                swal({ text: "Record Not Updated", width: 300, showCloseButton: true });
                            }

                        })
                        $scope.grid = true;
                        $scope.table = true;
                        $scope.newdisplay = false;
                        dataupdate = [];

                    }
                    //else {
                    //    swal({ text: "Change Atleast one Record to Update", width: 380, showCloseButton: true });
                    //}
                }
            }


        }])
})();
