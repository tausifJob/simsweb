﻿(function () {
    'use strict';
    var teacher_code = [];
    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ResultGenerationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = true;
            $scope.busyindicator = false;
            $scope.show_ins_table_loading = false;
            $(function () {
                $('#cmb_grade_code').multipleSelect({ width: '100%' });
                $('#cmb_section_code').multipleSelect({ width: '100%' });
            });

            $scope.grade_changed = function () {
                $scope.IP['sims_grade_code'] = $scope.IP['sims_grade_code_arr'] + "";
                $http.post(ENV.apiUrl + "api/ResultGeneration/sims_Sec", $scope.IP).then(function (res) {
                    $scope.sec_data = res.data.table;
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                        $("#cmb_section_code").multipleSelect("checkAll");
                    }, 1000);

                });
            }

            $scope.academic_changed = function () {
                $http.post(ENV.apiUrl + "api/ResultGeneration/sims_Grd", $scope.IP).then(function (res) {
                    $scope.grd_data = res.data.table;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            $scope.grade_changed();
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);


                    //if ($scope.grd_data.length > 0) {
                    //    $scope.IP['sims_grade_code'] = $scope.grd_data[0].sims_grade_code;
                    //    $scope.grade_changed();
                    //}
                });
                $http.post(ENV.apiUrl + "api/ResultGeneration/sims_Term", $scope.IP).then(function (res) {
                    $scope.term_data = res.data.table;
                    if ($scope.term_data.length > 0) {
                        $scope.IP['sims_term_code'] = $scope.term_data[0].sims_term_code;
                    }
                });
            }

            $scope.cur_changed = function () {
                $http.post(ENV.apiUrl + "api/ResultGeneration/sims_Academic", $scope.IP).then(function (res) {
                    $scope.acad_data = res.data.table;
                    if ($scope.acad_data.length > 0) {
                        $scope.IP['sims_academic_year'] = $scope.acad_data[0].sims_academic_year;
                        $scope.academic_changed();
                    }
                });
            }

            $http.post(ENV.apiUrl + "api/ResultGeneration/sims_Cur").then(function (res) {
                $scope.curr_data = res.data.table;
                if ($scope.curr_data.length > 0) {
                    $scope.IP = {
                        sims_cur_code: $scope.curr_data[0].sims_cur_code
                    }
                }
                $scope.cur_changed();
            });

            $scope.generate = function () {
                $scope.busyindicator = true;
                $scope.show_ins_table_loading = true;
                $scope.IP['sims_section_code'] = $scope.IP['sims_section_code_arr'] + "";
                $http.post(ENV.apiUrl + "api/ResultGeneration/sims_Generate", $scope.IP).success(function (data, status, headers, config) {
                    var msg = data.table[0];
                    console.log(msg)
                    swal({ title: "Alert", text: msg['msg'], showCloseButton: true, width: 450, });
                    $scope.busyindicator = false;
                    $scope.show_ins_table_loading = false;
                }).error(function (data, status, headers, config) {
                    var msg = data.table[0];
                    console.log(msg)
                    swal({ title: "Alert", text: msg, showCloseButton: true, width: 450, });
                    $scope.busyindicator = false;
                    $scope.show_ins_table_loading = false;
                });
            }
        }]);
})();