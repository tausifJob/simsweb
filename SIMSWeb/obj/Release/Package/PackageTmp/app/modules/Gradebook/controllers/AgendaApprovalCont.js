﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AgendaApprovalCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$location', '$anchorScroll', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $location, $anchorScroll) {
            // Pre-Required Functions and Variables
            var uname = $rootScope.globals.currentUser.username;


            /* Start Filter */
            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1, 'z-index': 1 });
            }, 100);

            var subdata = [];

            $http.get(ENV.apiUrl + "api/AgendaApprovalController/GetCur").then(function (GetCur) {
                $scope.GetCur = GetCur.data;
                $scope.rgvtbl = true;
                $scope.edt = { sims_cur_code: $scope.GetCur[0].sims_cur_code }
                $scope.select_cur($scope.GetCur[0].sims_cur_code);
                $scope.rgvtbl = false;
            });

            $scope.select_cur = function (cur) {
                $http.get(ENV.apiUrl + "api/AgendaApprovalController/GetAca?cur=" + cur).then(function (GetAca) {
                    $scope.GetAca = GetAca.data;
                    $scope.edt['sims_acaedmic_year'] = $scope.GetAca[0].sims_acaedmic_year;
                    $scope.select_aca($scope.GetAca[0].sims_acaedmic_year)
                });
            }

            $scope.select_aca = function (aca) {

                $http.get(ENV.apiUrl + "api/AgendaApprovalController/Getgrd?cur=" + $scope.edt.sims_cur_code + "&aca=" + aca + "&user=" + uname).then(function (Getgrd) {
                    $scope.Getgrd = Getgrd.data;

                    setTimeout(function () {
                        $('#cmb_grade').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });


            }

            //////////////////////////////////////////////////////////////Single Days   
            $scope.select_grd = function (grd) {
                $scope.AgendaDetails = '';

                $http.get(ENV.apiUrl + "api/AgendaApprovalController/Getsec?cur=" + $scope.edt.sims_cur_code + "&aca=" + $scope.edt.sims_acaedmic_year + "&grd=" + grd + "&user=" + uname).then(function (Getsec) {
                    $scope.Getsec = Getsec.data;
                });

            }

            $scope.select_sec = function (sec) {
                $scope.AgendaDetails = '';
                $http.get(ENV.apiUrl + "api/AgendaApprovalController/GetSubject?cur=" + $scope.edt.sims_cur_code + "&aca=" + $scope.edt.sims_acaedmic_year + "&grd=" + $scope.edt.sims_grade_code + "&sec=" + $scope.edt.sims_section_code + "&user=" + uname).then(function (GetSubject) {
                    $scope.GetSubject = GetSubject.data;
                });

            }


            $scope.Show_data = function () {
                if ($scope.edt.sims_cur_code == undefined || $scope.edt.sims_cur_code == '' || $scope.edt.sims_acaedmic_year == undefined || $scope.edt.sims_acaedmic_year == '')
                    //|| $scope.edt.sims_section_code == undefined || $scope.edt.sims_section_code == '' || $scope.edt.sims_subject_code == undefined || $scope.edt.sims_subject_code == '')
                    //|| $scope.edt.from_date == undefined || $scope.edt.from_date == ''
                    //|| $scope.edt.to_date == undefined || $scope.edt.to_date == '') {
                {
                    swal('', 'All Fields Are Mandatory.');
                }
                else {
                    if ($scope.edt.sims_grade_code == undefined)
                        $scope.edt.sims_grade_code = '';
                    //$scope.busy = true;
                    //$scope.rgvtbl = false;
                    var data = {
                        sims_cur_code: $scope.edt.sims_cur_code
                           , sims_acaedmic_year: $scope.edt.sims_acaedmic_year
                           , sims_grade_code: $scope.edt.sims_grade_code
                           , sims_section_code: $scope.edt.sims_section_code
                           , sims_subject_code: $scope.edt.sims_subject_code
                           , from_date: $scope.edt.from_date
                           , to_date: $scope.edt.to_date
                           , user_code: uname
                    };
                    $http.post(ENV.apiUrl + "api/AgendaApprovalController/listAgendaDetails", data).then(function (AgendaDetails) {
                        debugger;
                        $scope.AgendaDetails = AgendaDetails.data;

                        $scope.allItems = AgendaDetails.data;

                        console.log($scope.AgendaDetails);
                        $scope.edt.btn_save_vis = true;
                        $scope.edt.btn_rej_vis = true;

                        // $scope.busy = false;
                        //$scope.rgvtbl = true;
                    });


                }

            }


            $scope.reject_details = function () {
                debugger;
                for (var i = 0; i < $scope.AgendaDetails.length; i++) {
                    for (var j = 0; j < $scope.AgendaDetails[i].sub_list.length; j++) {
                        for (var k = 0; k < $scope.AgendaDetails[i].sub_list[j].agendalist.length; k++) {
                            //for (var l = 0; l < $scope.AgendaDetails[i].sub_list[j].agendalist[k].sup_subject.length; l++) {
                            if ($scope.AgendaDetails[i].sub_list[j].agendalist[k].isselected == true) {
                                debugger;
                                var subject = ({
                                    'sims_cur_code': $scope.AgendaDetails[i].sub_list[j].agendalist[k].sims_cur_code,
                                    'sims_acaedmic_year': $scope.AgendaDetails[i].sub_list[j].agendalist[k].sims_acaedmic_year,
                                    'sims_grade_code': $scope.AgendaDetails[i].sub_list[j].agendalist[k].sims_grade_code,
                                    'sims_section_code': $scope.AgendaDetails[i].sub_list[j].agendalist[k].sims_section_code,
                                    'sims_subject_code': $scope.AgendaDetails[i].sub_list[j].agendalist[k].sims_subject_code,
                                    'sims_agenda_number': $scope.AgendaDetails[i].sub_list[j].agendalist[k].sims_agenda_number,
                                });
                                subdata.push(subject);
                            }
                            //}
                        }
                    }
                }

                $http.post(ENV.apiUrl + "api/AgendaApprovalController/RejectAgenda", subdata).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Agenda Rejected Successfully", width: 300, height: 200 }).then(function (isConfirm) {
                            //if (isConfirm) {
                            //    HODetails('hod_data');
                            //}
                            $scope.Show_data();
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Agenda Not Rejected...!", width: 300, height: 200 });
                    }
                });
            }

            $scope.downloaddoc = function (str) {
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Agenda/' + str;
                window.open($scope.url);
            }


            $scope.update_details = function () {
                debugger;
                for (var i = 0; i < $scope.AgendaDetails.length; i++) {
                    for (var j = 0; j < $scope.AgendaDetails[i].sub_list.length; j++) {
                        for (var k = 0; k < $scope.AgendaDetails[i].sub_list[j].agendalist.length; k++) {
                            //for (var l = 0; l < $scope.AgendaDetails[i].sub_list[j].agendalist[k].sup_subject.length; l++) {
                            if ($scope.AgendaDetails[i].sub_list[j].agendalist[k].isselected == true) {
                                debugger;
                                var subject = ({
                                    'sims_cur_code': $scope.AgendaDetails[i].sub_list[j].agendalist[k].sims_cur_code,
                                    'sims_acaedmic_year': $scope.AgendaDetails[i].sub_list[j].agendalist[k].sims_acaedmic_year,
                                    'sims_grade_code': $scope.AgendaDetails[i].sub_list[j].agendalist[k].sims_grade_code,
                                    'sims_section_code': $scope.AgendaDetails[i].sub_list[j].agendalist[k].sims_section_code,
                                    'sims_subject_code': $scope.AgendaDetails[i].sub_list[j].agendalist[k].sims_subject_code,
                                    'sims_agenda_number': $scope.AgendaDetails[i].sub_list[j].agendalist[k].sims_agenda_number,
                                });
                                subdata.push(subject);
                            }
                            //}
                        }
                    }
                }

                $http.post(ENV.apiUrl + "api/AgendaApprovalController/UpdateAgendaStatus", subdata).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Agenda Approved Successfully", width: 300, height: 200 }).then(function (isConfirm) {
                            //if (isConfirm) {
                            //    HODetails('hod_data');
                            //}
                            $scope.Show_data();
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Agenda Not Approved...!", width: 300, height: 200 });
                    }
                });
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


            $scope.reset_data = function () {
                $scope.GetCur = '';
                $scope.GetAca = '';
                $scope.Getgrd = '';
                $scope.Getsec = '';
                $scope.GetSubject = '';
                $scope.edt.from_date = '';
                $scope.edt.to_date = '';
                $scope.AgendaDetails = '';
                $http.get(ENV.apiUrl + "api/AgendaApprovalController/GetCur").then(function (GetCur) {
                    $scope.GetCur = GetCur.data;

                    $scope.edt = { sims_cur_code: $scope.GetCur[0].sims_cur_code }
                    $scope.select_cur($scope.GetCur[0].sims_cur_code)


                });

            }
            /* End Filter */
            //Events End
        }])


})();
