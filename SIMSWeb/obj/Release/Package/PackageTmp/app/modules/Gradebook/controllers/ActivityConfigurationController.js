﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Gradebook');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ActivityConfigurationController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            $('*[data-datepicker="true"] input[type="text"]').datepicker(
                         {
                             todayBtn: true,
                             orientation: "top left",
                             autoclose: true,
                             todayHighlight: true,
                             format: 'yyyy-mm-dd'
                         });

            $scope.condenseMenu();
            $scope.expanded = true;
            $scope.expand_func = function () {
                if ($scope.expanded)
                    $scope.expanded = !$scope.expanded;
                
            }
            
            /* PAGER FUNCTIONS*/
            $scope.edt = [];
            $scope.var1 = "Test 12";
            $scope.temp_tab1 = { 'sims_cur_code':'','sims_academic_year':'','sims_term_code':'','sims_grade_code': '','sims_section_code':'','sims_subject_code':'','sims_config_desc':'','sims_type_code':'','sims_type_value':'','sims_activity_status':'' };
            $scope.temp_tab2 = { 'sims_grade_code': '', 'sims_section_code': '', 'sims_subject_code': '', 'sims_activity_name': '', 'sims_activity_desc': '', 'narr_grade_group_code': '', 'sims_activity_max_pnt': '0', 'sims_indicator_min_pnt': '0', 'sims_activity_min_pnt': '0', 'sims_activity_status': '', 'sims_config_sr_no': '' };
            $scope.temp_tab3 = { 'sims_indicator_name': '','sims_indicator_desc':'','sims_activity_code': '','sims_config_sr_no':'', 'sims_activity_min_pnt': '0','sims_indicator_max_pnt':'0', 'sims_indicator_status': '','narr_grade_group_code':''};
            $scope.temp_tab5 = {};
            $scope.acti_indicator = '';
            /*ACTIVITY CONFIG CONTROLLER-END */
            $(function () {
                $('#cmb_sections').multipleSelect({ width: '100%' });
                $('#cmd_subjects').multipleSelect({ width: '100%' });
                $('#cmb_tab2_sections').multipleSelect({ width: '100%' });
                $('#cmb_tab2_subjects').multipleSelect({ width: '100%' });
            });

            $scope.global_temp = [];

            $scope.cur_change = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/StudentAssessment/GetActAcademic?cur_code=" + $scope.global_temp.sims_cur_code).then(function (activityacde) {
                    $scope.activityacde = activityacde.data;
                    for (var i = 0; i < $scope.activityacde.length; i++) {
                        if ($scope.activityacde[i].sims_academic_status == 'C') {
                            $scope.global_temp['sims_academic_year'] = $scope.activityacde[i].sims_academic_year;
                            break;
                        }
                    }
                    $scope.year_change();
                });
            }

            $http.get(ENV.apiUrl + "api/StudentAssessment/GetActCurName").then(function (activityCur) {
                $scope.activityCur = activityCur.data;
                $scope.global_temp['sims_cur_code'] = activityCur.data[0].sims_cur_code;
                $scope.cur_change();
            });

           

            $scope.year_change = function () {
               
                $http.get(ENV.apiUrl + "api/StudentAssessment/GetActGroups?cur_code=" + $scope.global_temp.sims_cur_code + "&acdemic_year=" + $scope.global_temp.sims_academic_year).then(function (activityGroups) {
                    $scope.activityGroups = activityGroups.data;
                });

                $http.get(ENV.apiUrl + "api/StudentAssessment/GetActTerms?cur_code=" + $scope.global_temp.sims_cur_code + "&acdemic_year=" + $scope.global_temp.sims_academic_year).then(function (activityTerms) {
                    $scope.activityTerms = activityTerms.data;

                });
            }

            $scope.tab1_grade_change = function () {
                $http.get(ENV.apiUrl + "api/StudentAssessment/GetActSections?cur_code=" + $scope.global_temp.sims_cur_code + "&acdemic_year=" + $scope.global_temp.sims_academic_year + "&grade=" + $scope.temp_tab1.sims_grade_code).then(function (activitySections) {
                    $scope.activitySections = activitySections.data;
                    setTimeout(function () {
                        $('#cmb_sections').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }
          
            $scope.tab2_grade_change = function () {
                $http.get(ENV.apiUrl + "api/StudentAssessment/GetActSections?cur_code=" + $scope.global_temp.sims_cur_code + "&acdemic_year=" + $scope.global_temp.sims_academic_year + "&grade=" + $scope.temp_tab2.sims_grade_code).then(function (activitySections) {
                    $scope.tab2_activitySections = activitySections.data;
                    setTimeout(function () {
                        $('#cmb_tab2_sections').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

         
            /*Activity Type- Tab 1*/
            $http.get(ENV.apiUrl + "api/StudentAssessment/GetActTypes").then(function (activityTypes) {
                $scope.activityTypes = activityTypes.data;
            });

            $scope.Expand = true, $scope.Expand_act = true, $scope.Expand_indi = true;

            $scope.getAll_activities = function () {
                $http.get(ENV.apiUrl + "api/StudentAssessment/GetAllActivitiesConfig?cur_code=" + $scope.global_temp.sims_cur_code + "&acdemic_year=" + $scope.global_temp.sims_academic_year + "&term=" + $scope.global_temp.sims_term_code+ "&section=" + $scope.global_temp.sims_activity_group_code).then(function (allactivity) {
                    $scope.allactivity = allactivity.data;
                    console.log($scope.allactivity);
                });
                
            }

            $scope.tab2_section_change = function () {
                $scope.gradesec = '';
                if ($scope.temp_tab2.sims_section_code != undefined || $scope.temp_tab2.sims_section_code != null) {
                    for (var i = 0; i < $scope.temp_tab2.sims_section_code.length; i++)
                        $scope.gradesec = $scope.gradesec + ($scope.temp_tab2.sims_section_code[i]) + ',';
                }
                console.log($scope.gradesec);
                $http.get(ENV.apiUrl + "api/StudentAssessment/GetAllActivitiesConfigCopy?cur_code=" + $scope.global_temp.sims_cur_code + "&acdemic_year=" + $scope.global_temp.sims_academic_year + "&term=" + $scope.global_temp.sims_term_code + "&sections=" + $scope.gradesec).then(function (allactivity) {
                    $scope.allactivityForCopy = allactivity.data;
                    setTimeout(function () {
                        $('#cmb_tab2_subjects').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                    console.log($scope.allactivityForCopy);
                });
            }

            /*GET ALL ACTIVITY CONFIG AND SUBJECTS*/
            $scope.bnt_preview_click = function () {
                console.log($scope.global_temp);
                $scope.getAll_activities();
                $scope.indi_opr = "I";
                /*INDICATOR GROUP*/
                $http.get(ENV.apiUrl + "api/StudentAssessment/GetIndicatorGroups?cur_code=" + $scope.global_temp.sims_cur_code + "&acdemic_year=" + $scope.global_temp.sims_academic_year + "&group_code=" + $scope.global_temp.sims_activity_group_code).then(function (indicatorgroups) {
                    $scope.indicatorgroups = indicatorgroups.data;
                });

                $http.post(ENV.apiUrl + "api/StudentAssessment/ActSectionsSubjects?cur_code=" + $scope.global_temp.sims_cur_code + "&acdemic_year=" + $scope.global_temp.sims_academic_year + "&act_group_code=" + $scope.global_temp.sims_activity_group_code).then(function (activitySectionsSubjects) {
                    $scope.activitySectionsSubjects = activitySectionsSubjects.data;
                    setTimeout(function () {
                        $('#cmd_subjects').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }

            $scope.createactivity = function () {
               
                $scope.temp_tab1.sims_subject_code_lst = '';
                for (var i = 0; i < $scope.temp_tab1.sims_subject_code.length; i++)
                    $scope.temp_tab1.sims_subject_code_lst = $scope.temp_tab1.sims_subject_code_lst + ($scope.temp_tab1.sims_subject_code[i]) + ',';
                
                $scope.temp_tab1['sims_cur_code'] = $scope.global_temp.sims_cur_code;
                $scope.temp_tab1['sims_academic_year'] = $scope.global_temp.sims_academic_year;
                $scope.temp_tab1['sims_term_code'] = $scope.global_temp.sims_term_code;
                $scope.temp_tab1['sims_section_code_lst'] = $scope.grade_section;
                console.log($scope.temp_tab1);
                $http.post(ENV.apiUrl + "api/StudentAssessment/defineactivityConfig?act_group_code=" + $scope.global_temp.sims_activity_group_code, $scope.temp_tab1).then(function (defineActRes) {
                    $scope.defineActRes = defineActRes.data;
                    console.log($scope.defineActRes);
                    if ($scope.defineActRes == true) {
                        swal(
                        {
                            showCloseButton: true,
                            text: 'Activity Configuration Defined Successfully.',
                            width: 350,
                            showCloseButon: true
                        });
                        $scope.temp_tab1 = '';
                        
                        $scope.activitySectionsSubjects = '';
                        $scope.getAll_activities();
                        $scope.reset_tab1();
                    }
                });
                
            }

            $scope.editActivity = false;

            $scope.reset_tab1 = function () {
                $scope.editActivity = false;
                $scope.temp_tab1['sims_grade_code'] = '';
                $scope.temp_tab1['sims_section_code'] = '';
                $scope.temp_tab1['sims_subject_code'] = '';
                $scope.temp_tab1['sims_config_desc'] = '';
                $scope.temp_tab1['sims_type_code'] = '';
                $scope.temp_tab1['sims_type_value'] = '';
                $scope.temp_tab1['sims_activity_status'] = false;
                $scope.temp_tab1['sims_subject_code'] = '';
              
                setTimeout(function () {
                    $('#cmd_subjects').change(function () {

                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
                console.log($scope.temp_tab1);
            }
            $scope.reset_tab2 = function () {
                $scope.temp_tab2 = '';
                $scope.frmactivity.$setPristine();
                $scope.frmactivity.$setUntouched();
            }
            $scope.reset_tab3 = function () {
                $scope.temp_tab3 = { 'sims_indicator_name': '', 'sims_indicator_desc': '', 'sims_activity_min_pnt': '0','sims_indicator_max_pnt':'0', 'sims_indicator_status': '', 'narr_grade_group_code': '' };
                $scope.frmindicator.$setPristine();
                $scope.frmindicator.$setUntouched();
            }
            /*EDIT CONFIG*/
            $scope.selected_group = '';

            $scope.editconfig = function (obj, inx) {
                $scope.selected_group = obj;
                $scope.temp_tab2 = { 'sims_grade_code': '', 'sims_section_code': '', 'sims_subject_code': '', 'sims_activity_name': '', 'sims_activity_desc': '', 'narr_grade_group_code': '', 'sims_activity_max_pnt': '0', 'sims_activity_min_pnt': '0', 'sims_activity_status': '', 'sims_config_sr_no': '' };
                $scope.temp_tab3 = { 'sims_indicator_name': '', 'sims_indicator_desc': '', 'sims_activity_code': '', 'sims_config_sr_no': '', 'sims_indicator_min_pnt':'0','sims_activity_min_pnt': '0', 'sims_indicator_max_pnt': '0', 'sims_indicator_status': '', 'narr_grade_group_code': '' };

                /*Loading On Demand*/
                $scope.activity_config_desc = obj.sims_acativity_config_name;
                $http.post(ENV.apiUrl + "api/StudentAssessment/AllSubActivities", obj).then(function (activities) {
                    for (var x = 0; x < $scope.allactivity.length; x++) {
                        if ($scope.allactivity[x].sims_config_sr_no == obj.sims_config_sr_no && $scope.allactivity[x].sims_subject_code == obj.sims_subject_code) {
                            $scope.allactivity[x].lst_activity = activities.data;
                            break;
                        }
                    }
                });
                $scope.acti_indicator = '';
                $scope.editActivity = true;
                $scope.temp_tab1['sims_cur_code'] = obj.sims_cur_code;
                $scope.temp_tab1['sims_academic_year'] = obj.sims_academic_year;
                $scope.temp_tab1['sims_term_code'] = obj.sims_term_code;
                $scope.temp_tab1['sims_acativity_config_name'] = obj.sims_acativity_config_name;
                $scope.temp_tab1['sims_grade_code'] = obj.sims_grade_code;
                $scope.temp_tab1['sims_config_sr_no'] = obj.sims_config_sr_no;
                $scope.temp_tab1['sims_subject_name'] = obj.sims_subject_name;
                $scope.temp_tab1['sims_subject_code'] = obj.sims_subject_code;
                $scope.temp_tab1['sims_config_desc'] = obj.sims_config_desc;
                $scope.temp_tab1['sims_type_code'] = obj.sims_type_code;
                $scope.temp_tab1['sims_type_value'] = obj.sims_type_value;
                $scope.temp_tab1['sims_activity_status'] = obj.sims_activity_status;
             
            }
            /*Activity Type- Tab 1-END*/

            /*CREATE Activity Type- Tab 2*/
            $http.get(ENV.apiUrl + "api/StudentAssessment/GetnarrtiveGrades").then(function (narrtiveGrades) {
                $scope.narrtiveGrades = narrtiveGrades.data;
                $scope.narrtiveGrades_tab3 = narrtiveGrades.data;

            });
            $scope.act_opr = "I";
            $scope.indi_opr = "I";
            $scope.indi_comm_opr = "I";

            $scope.createactivity_tab2 = function (isvalid) {
                /// COPY
                $scope.temp_tab3 = '';
                if (isvalid) {
                    $scope.temp_tab2['sims_cur_code'] = $scope.selected_group.sims_cur_code;
                    $scope.temp_tab2['sims_academic_year'] = $scope.selected_group.sims_academic_year;
                    $scope.temp_tab2['sims_term_code'] = $scope.selected_group.sims_term_code;
                    $scope.temp_tab2['sims_subject_code'] = $scope.selected_group.sims_subject_code;
                    $scope.temp_tab2['sims_config_sr_no'] = $scope.selected_group.sims_config_sr_no;
                    $http.post(ENV.apiUrl + "api/StudentAssessment/defineactivity?opr_param=" + $scope.act_opr, $scope.temp_tab2).then(function (defineActRes) {
                        $scope.defineActRes = defineActRes.data;
                        console.log('RESULT');
                        console.log($scope.defineActRes);
                        if ($scope.defineActRes == true) {
                            if ($scope.act_opr == "I") {
                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'Activity Defined Successfully.',
                                    width: 350,
                                    showCloseButon: true
                                });
                            }
                            else if ($scope.act_opr == "U") {
                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'Activity Updated Successfully.',
                                    width: 350,
                                    showCloseButon: true
                                });
                            }
                        }
                        else {
                            swal(
                            {
                                showCloseButton: true,
                                text: 'Error in Defining Activity.',
                                width: 350,
                                showCloseButon: true
                            });
                        }
                        $scope.temp_tab2 = '';
                        $scope.getAll_activities();
                    });
                    $scope.frmactivity.$setPristine();
                    $scope.frmactivity.$setUntouched();
                }
            }
            /*CREATE Activity Type- Tab 2-END*/
            
            $scope.activity_config_desc = '';

            $scope.Activity_edit = function (act_obj, main_act_c, ind) {
                console.log('ACTIVITY OBJ', act_obj)
                console.log('MAIN OBJ', main_act_c)
                $scope.temp_tab3 = '';
             
                $scope.temp_tab2 = act_obj;
                $scope.activity_config_desc = main_act_c.sims_acativity_config_name;
                $scope.acti_indicator = main_act_c.sims_acativity_config_name + ' [ ' + act_obj.sims_activity_desc + ' ]';
                console.log(main_act_c);
                console.log(act_obj);
                $scope.act_opr = "U";
            }

            $scope.createindicator = function (isvalid) {
                console.log('createindicator ', isvalid);
                if (isvalid) {
                    if ($scope.indi_opr != "U") {
                        $scope.temp_tab3['sims_config_sr_no'] = $scope.temp_tab2.sims_config_sr_no;
                        $scope.temp_tab3['sims_activity_sr_no'] = $scope.temp_tab2.sims_activity_sr_no;
                    }
                    $scope.temp_tab3['sims_subject_code'] = $scope.temp_tab2.sims_subject_code;
                    $http.post(ENV.apiUrl + "api/StudentAssessment/defineindicator?opr_param=" + $scope.indi_opr, $scope.temp_tab3).then(function (defineIndRes) {
                        $scope.defineIndRes = defineIndRes.data;
                        console.log($scope.defineActRes);
                        if ($scope.defineIndRes == true) {
                            swal(
                            {
                                showCloseButton: true,
                                text: 'Indicator Defined Successfully.',
                                width: 350,
                                showCloseButon: true
                            });
                            $scope.temp_tab3 = '';
                            $scope.getAll_activities();
                            //   $scope.reset_tab1();
                        }
                    });
                    $scope.frmindicator.$setPristine();
                    $scope.frmindicator.$setUntouched();
                }
            }
            
            $scope.Indicator_edit = function (ind, act, act_c, $index) {
                console.log('INDICATOR', ind);
                console.log('INDICATOR', act);
                $scope.temp_tab3 = ind;
                $scope.indi_opr = "U";
                $scope.temp_tab5['sims_indicator_group_code'] = '';
                $scope.Indicator_comments = '';
                $scope.indicator_group_change();
            }
            
            $scope.indicator_group_change = function () {
                $scope.temp_tab5['sims_comment_desc'] = '';
                $scope.temp_tab5['sims_comment_desc_ar'] = '';
                $scope.temp_tab5['sims_indicator_comment_status'] = '';
                $scope.indi_comm_opr = "I";
                $scope.temp_tab5['sims_indicator_code'] = $scope.temp_tab3.sims_config_sr_no;
                $scope.temp_tab5['sims_activity_code'] = $scope.temp_tab3.sims_activity_sr_no;
                $http.post(ENV.apiUrl + "api/StudentAssessment/indicatorComment", $scope.temp_tab5).then(function (Indicator_comments) {
                    $scope.Indicator_comments = Indicator_comments.data;
                });
            }

            /*COMMENT DEFINE*/
            $scope.createcomment_tab5 = function (isvalid) {
                console.log('createcomment_tab5',isvalid);
                if (isvalid) {
                    $scope.temp_tab5['sims_indicator_code'] = $scope.temp_tab3.sims_config_sr_no;
                    $scope.temp_tab5['sims_activity_code'] = $scope.temp_tab3.sims_activity_sr_no;
                    $http.post(ENV.apiUrl + "api/StudentAssessment/defineindicatorComment?opr_param=" + $scope.indi_comm_opr, $scope.temp_tab5).then(function (defineIndCommRes) {
                        $scope.defineIndCommRes = defineIndCommRes.data;
                        console.log($scope.defineActRes);
                        if ($scope.defineIndCommRes == true) {
                            if ($scope.indi_comm_opr == "I") {
                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'Comment Defined Successfully.',
                                    width: 350,
                                    showCloseButon: true
                                });
                            }
                            else if ($scope.indi_comm_opr == "U") {
                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'Comment Updated Successfully.',
                                    width: 350,
                                    showCloseButon: true
                                });
                            }
                            else if ($scope.indi_comm_opr == "D") {
                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'Comment Deleted Successfully.',
                                    width: 350,
                                    showCloseButon: true
                                });
                            }
                            $scope.indicator_group_change();
                            $scope.temp_tab5['sims_comment_desc'] = '';
                            $scope.temp_tab5['sims_comment_desc_ar'] = '';
                            $scope.temp_tab5['sims_indicator_comment_status'] = '';
                            $scope.indi_comm_opr = "I";
                        }
                    });
                    $scope.frmcomment.$setPristine();
                    $scope.frmcomment.$setUntouched();
                }
            }
            /*EDIT COMMENT*/
            $scope.editcomment = function (comment) {
                $scope.temp_tab5 = comment;
                $scope.temp_tab5.sims_indicator_group_code = comment.sims_indicator_group_code;
                $scope.indi_comm_opr = "U";

            }
            $scope.deletecomment = function (comment) {
                $scope.indi_comm_opr = "D";
                $scope.temp_tab5 = comment;
                $scope.createcomment_tab5(true);
            }
            $scope.reset_tab5 = function () {
                $scope.temp_tab5['sims_comment_desc'] = '';
                $scope.temp_tab5['sims_comment_desc_ar'] = '';
                $scope.temp_tab5['sims_indicator_comment_status'] = '';
                $scope.temp_tab5['sims_indicator_group_code'] = '';
                $scope.indi_comm_opr = "I";
                $scope.indicator_group_change();
                $scope.frmcomment.$setPristine();
                $scope.frmcomment.$setUntouched();
            }

        }])

})();



