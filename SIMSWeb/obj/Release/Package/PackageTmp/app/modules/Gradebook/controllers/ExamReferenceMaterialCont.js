﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Gradebook');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('ExamReferenceMaterialCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.search_btn = true;
            $scope.clr_btn = true;
            $scope.Update_btn = false;
            $scope.newdisplay = false;
            $scope.table = true;
            $scope.table1 = true;
            $scope.display = false;
            $scope.grid = false;
            var fetchedRecords = [];

            //var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd');
            //$scope.temp = {
            //    gb_ref_publish_date: dateyear,
            //    gb_ref_expiry_date: dateyear,
            //}
            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };
            var arr_files = [];
            var arr_files1 = [];
            $scope.uploading_doc1 = true;
            $scope.images = [];
            $scope.images1 = [];

            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };
            var file_name = '';
            var file_name_name = '';
            $scope.file_doc = [];

            $scope.file_changed = function (element) {

                file_name = '';
                file_name_name = '';
                var v = new Date();
                if ($scope.file_doc == undefined) {
                    $scope.file_doc = '';
                }

                if ($scope.file_doc.length > 4) {
                    swal('', 'Upload maximum 5 files.');

                }
                else {

                    $scope.photofile = element.files[0];
                    file_name_name = $scope.photofile.name;
                    var len = 0;
                    len = file_name_name.split('.');
                    file_name = file_name_name.split('.')[0];
                    var fortype = file_name_name.split('.')[len.length - 1];

                    $scope.uploading_doc1 = false;
                    $scope.photo_filename = ($scope.photofile.type);

                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $scope.$apply(function () {
                        })
                    };
                    reader.readAsDataURL($scope.photofile);
                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + 'api/file/upload?filename=' + file_name + '&location=Docs/Student',
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    $http(request).success(function (d) {

                        var t = {
                            gb_ref_file_name: d,
                            //gb_ref_display_name: file_name,
                        }
                        $scope.file_doc.push(t);
                        $scope.uploading_doc1 = true;
                    });

                }

            }

            $scope.chkDate = function (publishDate, expiryDate) {
                if (publishDate > expiryDate) {
                    swal({ text: "Expiry date must greater than Publish date", width: 380, showCloseButton: true })
                    $scope.temp['gb_ref_expiry_date'] = "";
                }
            }

            $scope.RemoveEnrollMentNo = function ($event, index, str) {
                str.splice(index, 1);
            }

            $scope.downloaddoc1 = function (str) {
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/Docs/Student/' + str;
                window.open($scope.url);
            }


            $http.get(ENV.apiUrl + "api/ExamRef/getAllExamRefMaterial").then(function (allExamRefMtl) {
                $scope.allExam_Ref_Mtl = allExamRefMtl.data;
                $scope.totalItems = $scope.allExam_Ref_Mtl.length;
                $scope.todos = $scope.allExam_Ref_Mtl;
                $scope.makeTodos();
            });

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.allExam_Ref_Mtl, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.allExam_Ref_Mtl;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_grade_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    || item.sims_subject_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_gb_ref_name == toSearch) ? true : false;
            }

            $scope.getGradeBooks = function (str, str1) {
                $http.get(ENV.apiUrl + "api/ExamRef/getGradebooks?subject_code=" + str + "&grade_code=" + str1).then(function (getAllGradebooks) {
                    $scope.get_All_Gradebooks_detials = getAllGradebooks.data;
                });
            }

            $scope.getGbCat = function (gr_code, gb_num) {
                $http.get(ENV.apiUrl + "api/ExamRef/getGradebookCategory?grade_code=" + gr_code + "&gb_number=" + gb_num).then(function (getAllGradebookCategory) {
                    $scope.get_Gradebook_Category = getAllGradebookCategory.data;
                });
            }

            $scope.getGbAssMnt = function (gr_code, gb_num, gb_cat) {
                $http.get(ENV.apiUrl + "api/ExamRef/getGradebookAssignment?grade_code=" + gr_code + "&gb_number=" + gb_num + "&gb_cat_code=" + gb_cat).then(function (getAllGradebookAssignment) {
                    $scope.getAll_Gradebook_Assignment = getAllGradebookAssignment.data;
                });
            }

            $scope.New = function () {

                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                    $scope.curriculum = AllCurr.data;
                    $scope.temp = {
                        'sims_cur_code': $scope.curriculum[0].sims_cur_code
                    }
                    $scope.getAccYear($scope.temp.sims_cur_code);
                });
                $scope.file_up = true;
                $scope.file_up_grid = true;
                $scope.file_up_grid_buttons = true;
                $scope.first_col_md = true;
                $scope.second_col_md = true;
                $scope.reset_for_new = true;
                $scope.buttons_for_update = false;

                $scope.disabled = false;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = "";
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            $scope.edit = function (str) {
                $scope.file_up = false;
                $scope.file_up_grid = false;
                $scope.file_up_grid_buttons = false;
                $scope.first_col_md = false;
                $scope.second_col_md = false;
                $scope.reset_for_new = false;
                $scope.buttons_for_update = true;
                $scope.display = true;
                $scope.table1 = false;
                $scope.grid = false;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.edit_data = true;
                $scope.edit_code = true;
                debugger;
                $scope.temp =
                    {
                        sims_gb_ref_number: str.sims_gb_ref_number,
                        gb_ref_line_number: str.sims_gb_ref_line_number,
                        gb_ref_name: str.sims_gb_ref_name,
                        gb_ref_desc: str.sims_gb_ref_desc,
                        gb_ref_publish_date: str.sims_gb_ref_publish_date,
                        gb_ref_expiry_date: str.sims_gb_ref_expiry_date,
                        gb_status: str.sims_gb_ref_status,
                    }
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table1 = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.desg_company_code = "";
                $scope.temp.desg_desc = "";
                $scope.temp.desg_communication_status = "";

            }

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = {
                        'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                        'sims_academic_year': $scope.Acc_year[0].sims_academic_year,
                        'gb_ref_publish_date': dateyear,
                        'gb_ref_expiry_date': dateyear,
                    }
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                });
            }

            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
            }

            $scope.getSection = function (curCode, gradeCode, accYear) {

                $http.get(ENV.apiUrl + "api/ExamRef/getSubjects?sims_grade_code=" + gradeCode).then(function (getAllSubjects) {
                    $scope.get_All_Subjects = getAllSubjects.data;
                });


                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.temp.sims_cur_code + "&grade_code=" + $scope.temp.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $(function () {
                $('#cmb_section_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.SaveData = function (Myform) {
                debugger;
                var section = [];
                for (var i = 0; i < $scope.temp.sims_section_code.length; i++) {
                    var data = {
                        'sims_section_code': $scope.temp.sims_section_code[i]
                    }
                    section.push(data);
                }
                var datasend1 = [];
                var datasend = [];
                var datasend2 = [];
                if (Myform) {


                    for (var j = 0; j < section.length; j++) {

                        var data1 = {
                            sims_cur_code: $scope.temp.sims_cur_code,
                            sims_academic_year: $scope.temp.sims_academic_year,
                            sims_grade_code: $scope.temp.sims_grade_code,
                            sims_section_code: section[j].sims_section_code,
                            sims_gb_number: $scope.temp.sims_gb_number,
                            sims_subject_code: $scope.temp.sims_subject_code,
                            sims_gb_cat_code: $scope.temp.sims_gb_cat_code,
                            sims_gb_cat_assign_number: $scope.temp.sims_gb_cat_assign_number,
                            gb_ref_name: $scope.temp.gb_ref_name,
                            gb_ref_desc: $scope.temp.gb_ref_desc,
                            gb_status: $scope.temp.gb_status,
                            gb_ref_publish_date: $scope.temp.gb_ref_publish_date,
                            gb_ref_expiry_date: $scope.temp.gb_ref_expiry_date,
                        };
                        datasend1.push(data1);
                    }


                    for (var i = 0; i < $scope.file_doc.length; i++) {
                        var data = {
                            gb_ref_file_name: $scope.file_doc[i].gb_ref_file_name,
                            gb_ref_publish_date: $scope.temp.gb_ref_publish_date,
                            gb_ref_expiry_date: $scope.temp.gb_ref_expiry_date,
                            gb_ref_display_name: $scope.file_doc[i].gb_ref_display_name
                        }
                        datasend.push(data);
                    }



                    $http.post(ENV.apiUrl + "api/ExamRef/CUDExamRefMaterial?data=" + JSON.stringify(datasend), datasend1).then(function (res) {
                        $scope.AssignmentData = res.data;
                        if ($scope.AssignmentData == true) {
                            swal({ text: "Record Inserted Successfully", width: 380, showCloseButton: true });
                            $state.go($state.current, {}, { reload: true });
                            $scope.temp = "";
                            $scope.file_doc = "";
                        }
                        else {
                            swal({ text: "Record Not Inserted", width: 380, showCloseButton: true });
                        }

                    });

                }
            }

            $scope.Update = function () {
                var data1 = [];


                var data = ({
                    sims_gb_ref_number: $scope.temp.sims_gb_ref_number,
                    sims_gb_ref_line_number: $scope.temp.gb_ref_line_number,
                    gb_ref_name: $scope.temp.gb_ref_name,
                    gb_ref_desc: $scope.temp.gb_ref_desc,
                    gb_ref_publish_date: $scope.temp.gb_ref_publish_date,
                    gb_ref_expiry_date: $scope.temp.gb_ref_expiry_date,
                    sims_gb_ref_status: $scope.temp.gb_status,
                    opr: 'U'
                });

                data1.push(data);

                $http.post(ENV.apiUrl + "api/ExamRef/UpExamRefMaterial", data1).then(function (res) {
                    $scope.display = true;
                    $scope.msg1 = res.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Exam Reference Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                $http.get(ENV.apiUrl + "api/ExamRef/getAllExamRefMaterial").then(function (allExamRefMtl) {
                                    $scope.allExam_Ref_Mtl = allExamRefMtl.data;
                                    $scope.totalItems = $scope.allExam_Ref_Mtl.length;
                                    $scope.todos = $scope.allExam_Ref_Mtl;
                                    $scope.makeTodos();
                                });

                            }
                        });
                        data1 = [];
                        $scope.table1 = true;
                        $scope.display = false;
                    }
                    else {
                        swal({ title: "Alert", text: "Exam Reference Not Updated", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                $http.get(ENV.apiUrl + "api/ExamRef/getAllExamRefMaterial").then(function (allExamRefMtl) {
                                    $scope.allExam_Ref_Mtl = allExamRefMtl.data;
                                    $scope.totalItems = $scope.allExam_Ref_Mtl.length;
                                    $scope.todos = $scope.allExam_Ref_Mtl;
                                    $scope.makeTodos();
                                });

                            }
                        });
                    }
                    data1 = [];
                    $scope.table1 = true;
                    $scope.display = false;
                });
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.reset = function () {
                $scope.temp = "";
                $scope.file_doc = "";
                $scope.table = true;
                $scope.newdisplay = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.dest_code = "";
                $scope.temp.country_code = "";
                $scope.temp.dest_name = "";
            }

            $scope.clear = function () {
                $scope.temp = '';
                $scope.grid = true;
                $scope.table = true;
            }


            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_grade_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_grade_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_grade_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_gb_ref_number': $scope.filteredTodos[i].sims_gb_ref_number,
                            'sims_gb_ref_line_number': $scope.filteredTodos[i].sims_gb_ref_line_number,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/ExamRef/DelExamRefMaterial?data=", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/ExamRef/getAllExamRefMaterial").then(function (allExamRefMtl) {
                                                $scope.allExam_Ref_Mtl = allExamRefMtl.data;
                                                $scope.totalItems = $scope.allExam_Ref_Mtl.length;
                                                $scope.todos = $scope.allExam_Ref_Mtl;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted ", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {

                                            $http.get(ENV.apiUrl + "api/ExamRef/getAllExamRefMaterial").then(function (allExamRefMtl) {
                                                $scope.allExam_Ref_Mtl = allExamRefMtl.data;
                                                $scope.totalItems = $scope.allExam_Ref_Mtl.length;
                                                $scope.todos = $scope.allExam_Ref_Mtl;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_grade_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }


            simsController.directive('ngFiles', ['$parse', function ($parse) {

                function fn_link(scope, element, attrs) {
                    var onChange = $parse(attrs.ngFiles);
                    element.on('change', function (event) {
                        onChange(scope, { $files: event.target.files });
                    });
                };

                return {
                    link: fn_link
                }
            }])

        }])
})();
