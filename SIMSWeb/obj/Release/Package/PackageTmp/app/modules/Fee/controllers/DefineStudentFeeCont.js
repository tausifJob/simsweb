﻿
(function () {
    'use strict';
    // getStudentFeeDetails
    // getStudentDetails
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('DefineStudentFeeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.temp = [];

            $http.get(ENV.apiUrl + "api/DefineStudentFee/GetPageLoadValues").then(function (res) {

                $scope.PageLoadValues_obj = res.data;

                $scope.opration = false;
                $scope.display = true;
                //  console.log($scope.PageLoadValues_obj);
                $scope.curriculum = $scope.PageLoadValues_obj.curriculum_Names;
                $scope.category_names = $scope.PageLoadValues_obj.fee_Catagory;
                //$scope.All_fee_type = $scope.PageLoadValues_obj.fee_Description;

                $scope.AllFeeCodeType = $scope.PageLoadValues_obj.fee_code_type;
                $scope.All_freq = $scope.PageLoadValues_obj.fee_Frequency;

            })

            $http.get(ENV.apiUrl + "api/DefineStudentFee/Gettableheaderfees").then(function (Gettableheaderfees) {
                $scope.All_fee_type_obj = Gettableheaderfees.data;
            });


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.countData = [
                  { val: 5, data: 5 },
                  { val: 10, data: 10 },
                  { val: 15, data: 15 },

            ]

            $scope.size = function (str) {
                if (str == 5 || str == 10 || str == 15) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }

                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCurriculum) {

                $scope.Curriculum = getCurriculum.data;
                $scope.temp['sims_cur_code'] = $scope.Curriculum[0].sims_cur_code
                $scope.getacademicYear($scope.Curriculum[0].sims_cur_code);
            });

            $scope.getacademicYear = function (str) {

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (getAcademicYear) {
                    $scope.AcademicYear = getAcademicYear.data;
                    $scope.temp['sims_academic_year'] = $scope.AcademicYear[0].sims_academic_year
                    $scope.getGrade($scope.Curriculum[0].sims_cur_code, $scope.AcademicYear[0].sims_academic_year);
                })
            }

            $scope.getGrade = function (str1, str2) {


                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str1 + "&academic_year=" + str2).then(function (getAllGrades) {
                    $scope.AllGrades = getAllGrades.data;

                })
            }

            $scope.searchStudent = function () {
                $rootScope.visible_UnassignedStudent = true;
                $rootScope.visible_stud = true;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = false;
                $rootScope.visible_Employee = false;
                $rootScope.chkMulti = false;
                $rootScope.visible_UnassignedParent = false;
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });

            }

            $scope.$on('global_cancel', function (str) {
                ;
                console.log($scope.SelectedUserLst);
                if ($scope.SelectedUserLst.length > 0) {
                    $scope.temp.enrollmentNo = $scope.SelectedUserLst[0].s_enroll_no;
                    // $scope.obj['em_first_name'] = $scope.SelectedUserLst[0].empName;
                    //$scope.obj['emp_code'] = $scope.SelectedUserLst[0].em_number;
                }
                // $scope.getstudentList();
            });

            $scope.getacyr = function (str) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;

                })
            }

            $scope.CalculateAmount = function (fee_frequency) {


                var decimal = ($scope.studentDetails_obj[0].sims_fee_amount.toString().split(".")[1]);
                if (decimal == undefined) {
                    decimal = '00'
                }
                var amount = Math.abs(($scope.studentDetails_obj[0].sims_fee_amount));
                var temp_value1 = 0;
                temp_value1 = (parseInt(Math.sign(amount / 3) * Math.ceil(Math.abs(amount / 3) / 5) * 5));

                if (fee_frequency == 'O' || fee_frequency == 'Y') {
                    for (var i = 1; i <= 12; i++) {
                        $scope.studentDetails_obj[0]["sims_fee_period" + i] = 0;
                    }

                    for (var i = 1; i <= 12; i++) {
                        if ($scope.studentDetails_obj[0].sims_month_code == i) {
                            $scope.studentDetails_obj[0]["sims_fee_period" + i] = amount;
                        }
                    }
                }

                if (fee_frequency == 'T') {
                    $scope.Gettermmonthcodes = {};
                    $scope.Gettermmonthcodes.data = [];

                    $http.get(ENV.apiUrl + "api/DefineStudentFee/Gettermmonthcodes?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gettermmonthcodes) {
                        $scope.Gettermmonthcodes.data = Gettermmonthcodes.data;
                        if (Gettermmonthcodes.data.length > 0) {
                            try {
                                var tempU = 0;
                                var temp_value = parseInt(amount / $scope.Gettermmonthcodes.data.length);
                                tempU = amount % $scope.Gettermmonthcodes.data.length;
                                for (var i = 0; i < $scope.Gettermmonthcodes.data.length; i++) {
                                    $scope.Gettermmonthcodes.data[i]['amt'] = temp_value + tempU;
                                    tempU = 0;
                                }
                                console.log($scope.Gettermmonthcodes);
                                for (i = 1; i <= 12; i++) {
                                    $scope.studentDetails_obj[0]["sims_fee_period" + i] = 0;
                                }
                                for (var i = 0; i < $scope.Gettermmonthcodes.data.length; i++) {
                                    $scope.studentDetails_obj[0]["sims_fee_period" + $scope.Gettermmonthcodes.data[i].sims_month_code] = $scope.Gettermmonthcodes.data[i].amt;
                                }
                            }
                            catch (e) {
                            }
                        }
                        else {
                            swal({ text: 'Sectoin Term Not Defined', width: 320, showCloseButton: true });
                        }
                    });
                }

                if (fee_frequency == 'M') {

                    var temp_value = parseInt(amount / 10);
                    var rem_amt = parseInt(amount - (temp_value * 10));

                    $scope.studentDetails_obj[0]["sims_fee_period1"] = (temp_value); $scope.studentDetails_obj[0]["sims_fee_period2"] = (temp_value); $scope.studentDetails_obj[0]["sims_fee_period3"] = (temp_value) + '.' + decimal; $scope.studentDetails_obj[0]["sims_fee_period4"] = temp_value;
                    $scope.studentDetails_obj[0]["sims_fee_period5"] = temp_value; $scope.studentDetails_obj[0]["sims_fee_period6"] = 0; $scope.studentDetails_obj[0]["sims_fee_period7"] = 0; $scope.studentDetails_obj[0]["sims_fee_period8"] = temp_value;
                    $scope.studentDetails_obj[0]["sims_fee_period9"] = temp_value; $scope.studentDetails_obj[0]["sims_fee_period10"] = temp_value; $scope.studentDetails_obj[0]["sims_fee_period11"] = temp_value; $scope.studentDetails_obj[0]["sims_fee_period12"] = temp_value + rem_amt;

                }

            }

            $scope.getStudentFeeDetails = function (sims_cur_code, sims_academic_year, sims_grade_code, sims_section_code, enrollmentNo) {

                if (sims_cur_code == undefined || sims_cur_code == "")
                    sims_cur_code = '';
                if (sims_academic_year == undefined || sims_academic_year == "")
                    sims_academic_year = '';
                if (sims_grade_code == undefined || sims_grade_code == "")
                    sims_grade_code = '';
                if (sims_section_code == undefined || sims_section_code == "")
                    sims_section_code = '';
                if (enrollmentNo == undefined || enrollmentNo == "")
                    enrollmentNo = '';

                $scope.loading = true;

                $http.get(ENV.apiUrl + "api/DefineStudentFee/getStudentFeeDetails?cur_name=" + sims_cur_code + "&academic_year=" + sims_academic_year + "&grade_name=" + sims_grade_code
                    + "&section_name=" + sims_section_code + "&enrollmentNo=" + enrollmentNo).then(function (res) {
                        $scope.All_Student_Fee_Details_obj = res.data;

                        if ($scope.All_Student_Fee_Details_obj.length > 0) {
                            //  $scope.All_fee_type_obj = $scope.All_Student_Fee_Details_obj[0].sublist;
                            $scope.table = true;
                        }
                        else {
                            $scope.table = false;
                            swal({ text: "Record Not Found", width: 350, showCloseButton: true });
                        }
                    });
            }

            $scope.getsection = function (str, str1, str2) {

                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + str + "&grade_code=" + str1 + "&academic_year=" + str2).then(function (Allsection) {
                    $scope.getSectionFromGrade = Allsection.data;
                    setTimeout(function () {
                        $('#app_on1').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                })
            };

            $scope.getStudentDetails = function (info, sub) {

                $scope.fee_code_Desc = sub.sims_fee_code_description;

                $scope.studentname = info.studentName;


                var data = sub;
                data.sims_enroll_number = info.sims_enroll_number;
                data.studentName = info.studentName;
                data.sims_fee_academic_year = info.sims_fee_academic_year;
                //data.sims_fee_category = info.sims_fee_category;
                data.sims_fee_cur_code = info.sims_fee_cur_code;
                data.sims_fee_grade_code = info.sims_fee_grade_code;
                data.sims_fee_section_code = info.sims_fee_section_code;
                data.concession = info.concession;

                // data.sims_fee_code = sub.sims_fee_code;
                //console.log(data);
                var temp = data;
                $scope.studentDetails_obj = [];
                $http.get(ENV.apiUrl + "api/DefineStudentFee/getStudentDetails?data=" + JSON.stringify(data)).then(function (res) {

                    $scope.studentDetails_obj = res.data;
                    $scope.loading = false;
                    if ($scope.studentDetails_obj.length > 0) {
                        value = parseFloat($scope.studentDetails_obj[0].sims_fee_amount);
                        $('#myModal').modal({ backdrop: "static" });


                        $timeout(function () {
                            $("#fixTable1").tableHeadFixer({ 'top': 1 });
                        }, 100);
                    }
                });


            }

            $scope.updateStudentData = function () {

                var total_amount = 0;

                for (var i = 1; i <= 12; i++) {
                    total_amount = parseFloat(total_amount) + parseFloat($scope.studentDetails_obj[0]["sims_fee_period" + i])
                }
                if (total_amount == parseFloat($scope.studentDetails_obj[0].sims_fee_amount)) {

                    $http.post(ENV.apiUrl + "api/DefineStudentFee/updatstudentdetails?update_data=" + JSON.stringify($scope.studentDetails_obj[0])).then(function (res) {
                        $scope.result = res.data;
                        if ($scope.result == true) {
                            swal({ text: "Student fee defined successfully", width: 350, showCloseButton: true });
                            $('#myModal').modal('hide');
                            $scope.getStudentFeeDetails($scope.temp.sims_cur_code, $scope.temp.sims_academic_year, $scope.temp.sims_grade_code, $scope.temp.sims_section_code, $scope.temp.enrollmentNo);
                        }
                        else {
                            swal({ text: "Student fee not defined", width: 350, showCloseButton: true });
                        }

                    });
                }
                else {
                    swal({ text: "Total amount should match with total amount defined for periods", width: 380, showCloseButton: true });
                }
            }

            var data_assign;
            $scope.isAssign_fee = false;

            $scope.Can_Assign = function () {
                $scope.isAssign_fee = false;
                var v1 = document.getElementById(data_assign.sims_enroll_number + data_assign.sims_fee_code);
                v1.checked = false;
                data_assign.sims_fee_status = false;
                data_assign = "";
            }

            $scope.Can_UnAssign = function () {
                temp.sims_fee_status = true;
            }

            var temp;
            $scope.Assign_fee_to_student = function (info, sub) {


                if (sub.sims_fee_status == true) {

                    data_assign = sub;
                    data_assign.sims_enroll_number = info.sims_enroll_number;
                    data_assign.sims_fee_academic_year = info.sims_fee_academic_year;
                    data_assign.sims_fee_category = info.sims_fee_category;
                    data_assign.sims_fee_cur_code = info.sims_fee_cur_code;
                    data_assign.sims_fee_grade_code = info.sims_fee_grade_code;
                    data_assign.sims_fee_section_code = info.sims_fee_section_code;

                    //swal({
                    //    title: '',
                    //    text: "Are you sure you want to Assign Fee?",
                    //    showCloseButton: true,
                    //    showCancelButton: true,
                    //    confirmButtonText: 'Yes',
                    //    width: 380,
                    //    cancelButtonText: 'No',

                    //}).then(function (isConfirm) {
                    //    if (isConfirm) {

                    $scope.isAssign_fee = true;
                    $http.post(ENV.apiUrl + "api/DefineStudentFee/Assign_fee_to_student?data=" + JSON.stringify(data_assign)).then(function (res) {
                        $scope.resul = res.data;
                        if ($scope.resul == true) {
                            swal({ text: "Fee type Mapped to student Successfully", width: 350, showCloseButton: true });
                        }
                        else {
                            swal({ text: "Fee type can not Mapped to Student", width: 350, showCloseButton: true });
                        }

                        $scope.isAssign_fee = false;

                        data_assign = "";
                        $scope.getStudentFeeDetails($scope.temp.sims_cur_code, $scope.temp.sims_academic_year, $scope.temp.sims_grade_code, $scope.temp.sims_section_code, $scope.temp.enrollmentNo);
                    });

                    //    }
                    //})
                }
                if (sub.sims_fee_status == false) {

                    temp = sub;
                    temp.sims_enroll_number = info.sims_enroll_number;
                    temp.sims_fee_academic_year = info.sims_fee_academic_year;
                    temp.sims_fee_category = info.sims_fee_category;
                    temp.sims_fee_cur_code = info.sims_fee_cur_code;
                    temp.sims_fee_grade_code = info.sims_fee_grade_code;
                    temp.sims_fee_section_code = info.sims_fee_section_code;
                    //   temp.sims_fee_status = false;
                    //swal({
                    //    title: '',
                    //    text: "Are you sure you want to Unassign Fee?",
                    //    showCloseButton: true,
                    //    showCancelButton: true,
                    //    confirmButtonText: 'Yes',
                    //    width: 380,
                    //    cancelButtonText: 'No',

                    //}).then(function (isConfirm) {
                    //    if (isConfirm) {

                    $http.post(ENV.apiUrl + "api/DefineStudentFee/UnAssignStudentFee?update_data=" + JSON.stringify(temp)).then(function (res) {
                        $scope.result = res.data;
                        $scope.loading = true;

                        swal({ text: "Fee  updated Successfully", width: 350, showCloseButton: true });



                        $scope.getStudentFeeDetails($scope.temp.sims_cur_code, $scope.temp.sims_academic_year, $scope.temp.sims_grade_code, $scope.temp.sims_section_code, $scope.temp.enrollmentNo);
                    });
                    //    }
                    //})
                }

            }
            var value = '';

            $scope.checkevalues = function () {

                value = parseFloat($scope.studentDetails_obj[0].sims_fee_period1) + parseFloat($scope.studentDetails_obj[0].sims_fee_period2) + parseFloat($scope.studentDetails_obj[0].sims_fee_period3) + parseFloat($scope.studentDetails_obj[0].sims_fee_period4) + parseFloat($scope.studentDetails_obj[0].sims_fee_period5) + parseFloat($scope.studentDetails_obj[0].sims_fee_period6) + parseFloat($scope.studentDetails_obj[0].sims_fee_period7) + parseFloat($scope.studentDetails_obj[0].sims_fee_period8) + parseFloat($scope.studentDetails_obj[0].sims_fee_period9) + parseFloat($scope.studentDetails_obj[0].sims_fee_period10) + parseFloat($scope.studentDetails_obj[0].sims_fee_period11) + parseFloat($scope.studentDetails_obj[0].sims_fee_period12);
                if (value != parseFloat($scope.studentDetails_obj[0].sims_fee_amount)) {
                    $scope.studentDetails_obj[0].sims_fee_amount = value;
                }
            }

        }]);
})();

