﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('FeeTransPayingAgent',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.maxLength = 0;
            $scope.SelectedFees = [];
            $scope.FeeDet = [];
            $scope.show_det = false;
            $scope.sel = {};
            $scope.IP = {};
            $scope.SUM = 0;
            $scope.IP.StudFeeTermCode = '';
            $scope.IP.PACODE = '';
            $scope.IP.StudEnroll = '';

            $http.post(ENV.apiUrl + "api/StudentFee/d5b1d259f086f320d2f5479209eb338a6ce47bc0").then(function (res) {
                $scope.sel.pas = res.data.table;
            });

            $http.post(ENV.apiUrl + "api/StudentFee/da7b7454507b2a8fa08803a7cb06f938758d109d").then(function (res) {
                $scope.sel.terms = res.data.table;
            });

            $scope.search = function () {
                $scope.clicked = true;
                $scope.SUM = 0;
                $scope.receiptDate = new Date().getFullYear() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getDate();
                $scope.IP.StudFeeTermCode = $scope.s.StudFeeTermCode.termCode;
                $scope.IP.PACODE = $scope.s.PACODE.pcode;
                $scope.IP.StudEnroll = $scope.s.StudEnroll;
                $scope.StudFeeDetails = [];
                $scope.SelectedFees = [];
                $http.get(ENV.apiUrl + "api/StudentFee/GetStudentPAFee?data=" + JSON.stringify($scope.IP)).then(function (res) {
                    $scope.StudFeeDetails = res.data.table;
                    console.log($scope.StudFeeDetails);
                });
                $scope.show_det = $scope.SelectedFees.length > 0;
            }

            //$scope.search();

            $scope.OnFeeAdd = function (item) {
                if (item.master) {
                    console.log(item);
                    var ob = {};
                    var ID = $scope.maxLength++;
                    item.ID = ID;
                    $scope.clicked = false;
                    ob.pno = item.periodNo;
                    ob.feeNumber = item.fn;
                    ob.ID = ID;
                    ob.FD = item.studFeeDesc;
                    ob.enroll = item.enroll;
                    ob.bal_Fee = item.bal_Fee;
                    ob.studCurr = item.cur_code;
                    ob.studAcademic = item.ayear;
                    ob.term_code = item.term_code
                    ob.studGrade = item.grade_code;
                    ob.studSection = item.section_code;
                    ob.p_number = item.p_number;
                    ob.ppacode = item.pacode;
                    ob.sr = $scope.SelectedFees.length + 1;
                    ob.FeeCat = item.sims_fee_category;
                    ob.FeeCode = item.fc;
                    ob.flag = false;
                    $scope.SelectedFees.push(ob);
                    var v = true;
                    for (var i = 0; i < $scope.StudFeeDetails.length; i++) {
                        var item = $scope.StudFeeDetails[i];
                        v = v && item.master;
                    }
                    $scope.selAll = v;
                }
                else {
                    $scope.selAll = false;
                    for (var i = 0; i < $scope.SelectedFees.length; i++) {
                        if ($scope.SelectedFees[i].ID == item.ID) {
                            $scope.SelectedFees.splice(i, 1);
                            break;
                        }
                    }
                }
                $scope.totalFeeamt();
                $scope.show_det = $scope.SelectedFees.length > 0;
            }

            $scope.OnAllFeeAdd = function () {
                if ($scope.selAll) {
                    for (var i = 0; i < $scope.StudFeeDetails.length; i++) {
                        var item = $scope.StudFeeDetails[i];
                        if (!item.master) {
                            item.master = true;
                            var ob = {};
                            var ID = $scope.maxLength++;
                            item.ID = ID;
                            $scope.clicked = false;
                            ob.pno = item.periodNo;
                            ob.feeNumber = item.fn;
                            ob.ID = ID;
                            ob.bal_Fee = item.bal_Fee;
                            ob.FD = item.studFeeDesc;
                            ob.enroll = item.enroll;
                            ob.studCurr = item.cur_code;
                            ob.studAcademic = item.ayear;
                            ob.term_code = item.term_code
                            ob.studGrade = item.grade_code;
                            ob.studSection = item.section_code;
                            ob.p_number = item.p_number;
                            ob.ppacode = item.pacode;
                            ob.sr = $scope.SelectedFees.length + 1;
                            ob.FeeCat = item.sims_fee_category;
                            ob.FeeCode = item.fc;
                            ob.flag = false;
                            $scope.SelectedFees.push(ob);
                        }
                    }
                }
                else {
                    for (var i = 0; i < $scope.StudFeeDetails.length; i++) {
                        var item = $scope.StudFeeDetails[i];
                        item.master = false;
                    }
                    $scope.SelectedFees = [];
                }
                $scope.totalFeeamt();
                $scope.show_det = $scope.SelectedFees.length > 0;
            }

            $scope.submit = function () {
                $scope.clicked = true;
                var ar = [];
                for (var i = 0; i < $scope.SelectedFees.length; i++) {
                    if ($scope.SelectedFees[i].flag == false) {
                        var ob = {};
                        ob.P_NUMBER = $scope.SelectedFees[i].p_number;
                        ob.P_code = $scope.SelectedFees[i].ppacode;
                        ob.ReceiptDate = $scope.receiptDate;
                        ob.Remark = $scope.SelectedFees[i].Remark;
                        ob.ENROLL = $scope.SelectedFees[i].enroll;
                        ob.CURR = $scope.SelectedFees[i].studCurr;
                        ob.ACADEMIC = $scope.SelectedFees[i].studAcademic;
                        ob.GRADE = $scope.SelectedFees[i].studGrade;
                        ob.SECTION = $scope.SelectedFees[i].studSection;
                        ob.q = getFeeCode(ob.ENROLL);
                        ob.FEE_CATEGORY = $scope.SelectedFees[i].FeeCat;
                        ob.FEE_Per = $scope.SelectedFees[i].pno;
                        ob.FEE_AMT = '0';
                        ob.UserNm = $rootScope.globals.currentUser.username;
                        ob.T_code = $scope.SelectedFees[i].term_code;
                        ar.push(ob);
                    }
                }
                if (ar.length == 0) {
                    alert("Nothing Selected");
                    return;
                }
                $http.post(ENV.apiUrl + "api/StudentFee/a0f1490a20d0211c997b44bc357e1972deab8ae3", ar).then(function (resl) {
                    $scope.Result = resl.data;
                    $scope.SelectedFees = [];
                    $scope.search();
                    $scope.clicked = false;
                    $rootScope.strMessage = 'Fee Collected Successfully';
                    $('#message').modal('show');
                });
            }

            function getFeeCode(enroll) {
                var ret = '';
                for (var i = 0; i < $scope.SelectedFees.length; i++) {
                    if ($scope.SelectedFees[i].flag == false && $scope.SelectedFees[i].enroll == enroll) {
                        ret = ret + $scope.SelectedFees[i].FeeCode + $scope.SelectedFees[i].feeNumber + ',';
                        $scope.SelectedFees[i].flag = true;
                    }
                }
                return ret;
            }

            function checkClicked() {
                $scope.clicked = !$scope.SelectedFees.length > 0;
            }

            $scope.totalFeeamt = function () {
                if (typeof ($scope.SelectedFees) === 'undefined') {
                    return 0;
                }
                var sum = 0;
                for (var i = $scope.SelectedFees.length - 1; i >= 0; i--) {
                    sum += parseFloat($scope.SelectedFees[i].bal_Fee);
                }
                $scope.SUM = sum.toFixed(2);
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }]);
})();