﻿(function () {
    'use strict';
    var del = [], feecodes = [], All_Fee_codesMul = [];
    var main, temp, opr;
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('LateFeeRuleDefineController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            //$scope.display = false; $scope.table = true; $scope.operation = false;
            $scope.pagesize = "5";
            $scope.table = true;
            $scope.readonlydiscount = true;
            $scope.dis = true;
            $scope.operation = false;
            data();
            $scope.temp = { sims_cur_code: '' };
            function data() {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetLateRules").then(function (res) {
                    $scope.display = false;
                    $scope.obj = res.data;
                   
                    if ($scope.obj.length != 0) {
                        $scope.table = true;
                        $scope.display = true;
                        $scope.operation = false;
                        $scope.totalItems = $scope.obj.length;
                        $scope.todos = $scope.obj;
                        $scope.makeTodos();

                    }
                    else {
                        $scope.table = true;
                        $scope.display = false;
                        $scope.operation = false;
                        swal({ text: 'Sorry Data Not Found', width: 380, showCloseButton: true });
                    }


                });
            }

            $scope.getCur = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetRuleAllCurName").then(function (CurName) {
                    $scope.CurName = CurName.data;
                    $scope.temp = { sims_cur_code: $scope.CurName[0].sims_cur_code };
                    $scope.getAcademicyear();
                });
            }
            $scope.getAcademicyear = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetRuleAllAcademicYears?cur_code=" + $scope.temp.sims_cur_code).then(function (res) {
                    $scope.Acdm_Year = res.data;
                    $scope.temp = { sims_academic_year: $scope.Acdm_Year[0].sims_academic_year, sims_cur_code: $scope.temp.sims_cur_code }
                    $scope.getTerms();
                });
            }
            //Fee Types
          

            $scope.getFeeTypes = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetLateRulesFeeType").then(function (feeTypes) {
                    $scope.feeTypes = feeTypes.data;
                });
            }
            $scope.getCur();

            $scope.getFeeTypes();

            $scope.getTerms = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetLateRulesTerm?cur=" + $scope.temp.sims_cur_code + "&aca=" + $scope.temp.sims_academic_year).then(function (terms) {
                    $scope.terms = terms.data;
                    console.log(terms.data);
                });
            }

            $scope.selectedUserIds = [];

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.CheckMultiple();
                }
            };

            $scope.checkAll = function () {

                main = document.getElementById('mainchk');
              
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_late_rule_code + i;
                    var v = document.getElementById(t);
                    if (main.checked == true) {
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                    else {
                        v.checked = false;
                        $scope.row1 = ''
                        $('tr').removeClass("row_selected");
                    }
                }
            }

            $scope.checkAny = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }

            }

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }
            }

            $scope.Cancel = function () {
                $scope.table = true;
                $scope.temp = "";
                $scope.readonly = false;
                $scope.operation = false;

            }

            $scope.edit = function (str) {
                $scope.readonly1 = true;
                $scope.fee_code = '';
                $scope.updisabled = true;
                $scope.readonlydiscount = false;
                $scope.table = false;
                $scope.operation = true;
                $scope.readonly = false;
                $scope.Save_btn = false;
                $scope.Update_btn = true;
                var data = angular.copy(str);
                $scope.temp = data;
                for (var i = 0; i < $scope.feeTypes.length; i++) {
                    $scope.feeTypes[i].sims_late_rule_details_status = false;
                }
                for (var i = 0; i < $scope.feeTypes.length; i++) {
                    for (var j = 0; j < $scope.temp.details.length; j++) {
                        if ($scope.feeTypes[i].sims_fee_code == $scope.temp.details[j].sims_fee_code)
                            $scope.feeTypes[i].sims_late_rule_details_status = true;
                    }
                }
                $scope.opr = 'U';
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }
                $scope.checkAll();
            }

            $scope.Delete = function () {

                var datasend = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_late_rule_code + i);
                    if (v.checked == true) {
                        datasend.push($scope.filteredTodos[i]);
                        $scope.flag = true;
                    }
                }
               
                //$http.post(ENV.apiUrl + "api/Fee/SFS/CURDdeleterules", datasend).then(function (res) {
                //    $scope.CUDobj = res.data;
                //    swal({ text: 'Rules Deleted Successfully', width: 380, showCloseButton: true });
                //    $scope.currentPage = 1;
                //    data();
                //});
                if ($scope.flag) {
                    swal({
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/Fee/SFS/deleterules", datasend).then(function (res) {
                                $scope.CUDobj = res.data;
                                swal({ text: 'Rules Deleted Successfully', width: 380, showCloseButton: true });
                                $scope.currentPage = 1;
                                data();
                            })
                        }
                        else {

                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                                $scope.row1 = '';
                            }
                            $scope.checkAll();
                        }
                    })
                }
                else {
                    swal({ text: 'Select At Least One Record To Delete', width: 380, showCloseButton: true });
                }
            }

            $scope.New = function () {
                $scope.updisabled = false;
                $scope.tenantForm.$setPristine();
                $scope.fee_code = '';
                $scope.table = false;
                $scope.operation = true;
                $scope.Save_btn = true;
                $scope.Update_btn = false;
                $scope.opr = 'I';
                $scope.readonly = true;
                $scope.readonlydiscount = true;
                $scope.readonly1 = true;
                $scope.CUDobj = false;
                $scope.getCur();
                $scope.getFeeTypes();

            }

            $scope.selectfeecodes = function (fee_Codes) {
                $scope.studfeecode = fee_Codes;
            }

            $scope.SaveData = function (isvalid) {
                var status = false;
                var selected_fee_codes = '';
                if (isvalid) {
                    var selected_fee_codes = '';
                    for (var i = 0; i < $scope.feeTypes.length; i++) {
                        var d = document.getElementById($scope.feeTypes[i].sims_fee_code)
                        if (d.checked == true)
                            selected_fee_codes += $scope.feeTypes[i].sims_fee_code + ",";
                    }
                    $scope.temp.sims_fee_code_list = selected_fee_codes;

                    if (selected_fee_codes != '') {
                        if ($scope.opr == 'I') {
                            $http.post(ENV.apiUrl + "api/Fee/SFS/CURDrules", $scope.temp).then(function (res) {
                                swal({ text: 'Rule Defined Successfully', width: 380, showCloseButton: true });
                                data();
                                $scope.Cancel();
                            });
                        }
                        else if ($scope.opr == 'U') {
                            $http.post(ENV.apiUrl + "api/Fee/SFS/updaterules", $scope.temp).then(function (res) {
                                swal({ text: 'Rule Updated Successfully', width: 380, showCloseButton: true });
                                data();
                                $scope.Cancel();
                            });
                        }
                    }
                    else {
                        swal({ text: 'Please select At least One fee type.', width: 380, showCloseButton: true });
                    }//Insert Close
                }
            }

            $scope.ischecked = function (str) {
                $scope.fee_code = '';
                var code = document.getElementById(str);
                if (code.checked = true) {
                    $scope.sims_fee_code = str;
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_late_rule_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_late_rule_description.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

        
          

            
        }]
        )
})();