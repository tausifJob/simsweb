﻿(function () {
    'use strict';
    var del = [], search_obj;
    var main, fee_type;
    var grade_code;
    var cur_code;
    var section_code;
    var length_of_search_result;
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SectionFeeController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$compile', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $compile) {
            $scope.display = false;
            $scope.table1 = false;
            $scope.pagesize = "5";
            $scope.pageindex = "1";

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;
                $scope.temp = { s_cur_code: $scope.curriculum[0].sims_cur_code };
                $scope.getAccYear();
            })

            $scope.getAccYear = function (curCode) {

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.temp.s_cur_code).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = {
                        s_cur_code: $scope.temp.s_cur_code,
                        sims_academic_year: $scope.Acc_year[0].sims_academic_year
                    }

                    $scope.getGrade($scope.temp.s_cur_code, $scope.temp.sims_academic_year);
                });

            }

            $scope.getGrade = function (str1, str2) {


                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str1 + "&academic_year=" + str2).then(function (getAllGrades) {
                    $scope.grade = getAllGrades.data;
                    console.log($scope.grade);
                   
                })
            }

            $http.get(ENV.apiUrl + "api/common/SectionFee/GetPageLoadValues").then(function (res) {
                $scope.PageLoadValues_obj = res.data;
                $scope.table = true;
                $scope.opration = false;
                $scope.display = true;
                console.log($scope.PageLoadValues_obj);
                $scope.curriculum1 = $scope.PageLoadValues_obj.curriculum_Names;
                $scope.category_names = $scope.PageLoadValues_obj.fee_Catagory;
                $scope.All_fee_type = $scope.PageLoadValues_obj.fee_Description;
                $scope.AllFeeType = $scope.PageLoadValues_obj.fee_Description;
                $scope.AllFeeCodeType = $scope.PageLoadValues_obj.fee_code_type;
                $scope.All_freq = $scope.PageLoadValues_obj.fee_Frequency;
            })

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            var amount = '6';
            var temp_value = (parseInt(Math.sign(amount / 10) * Math.ceil(Math.abs(amount / 10) / 5) * 5));

            $scope.filterValue = function ($event) {
                if (isNaN(String.fromCharCode($event.keyCode))) {
                    $event.preventDefault();
                }
            };

            $scope.CalculateAmount = function (fee_frequency) {

                var decimal = ($scope.search_obj.section_fee_amount.toString().split(".")[1]);
                if (decimal == undefined) {
                    decimal = '00'
                }
                var amount = Math.abs(($scope.search_obj.section_fee_amount));
                var temp_value1 = 0;
                temp_value1 = (parseInt(Math.sign(amount / 3) * Math.ceil(Math.abs(amount / 3) / 5) * 5));

                if (fee_frequency == 'O' || fee_frequency == 'Y') {

                    for (var i = 1; i <= 12; i++) {
                        $scope.search_obj["section_fee_period" + i] = 0;
                    }

                    for (var i = 1; i <= 12; i++) {

                        if ($scope.search_obj.sims_month_code == i) {
                            $scope.search_obj["section_fee_period" + i] = amount;
                        }
                    }

                }

                if (fee_frequency == 'T') {
                    $scope.Gettermmonthcodes = {};
                    $scope.Gettermmonthcodes.data = [];

                    $http.get(ENV.apiUrl + "api/DefineStudentFee/Gettermmonthcodes?cur_code=" + $scope.temp.s_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gettermmonthcodes) {
                        $scope.Gettermmonthcodes.data = Gettermmonthcodes.data;
                        if (Gettermmonthcodes.data.length > 0) {
                            try {
                                var tempU = 0;
                                var temp_value = parseInt(amount / $scope.Gettermmonthcodes.data.length);
                                tempU = amount % $scope.Gettermmonthcodes.data.length;
                                for (var i = 0; i < $scope.Gettermmonthcodes.data.length; i++) {
                                    $scope.Gettermmonthcodes.data[i]['amt'] = temp_value + tempU;
                                    tempU = 0;
                                }
                                console.log($scope.Gettermmonthcodes);
                                for (i = 1; i <= 12; i++) {
                                    $scope.search_obj["section_fee_period" + i] = 0;
                                }
                                for (var i = 0; i < $scope.Gettermmonthcodes.data.length; i++) {
                                    $scope.search_obj["section_fee_period" + $scope.Gettermmonthcodes.data[i].sims_month_code] = $scope.Gettermmonthcodes.data[i].amt;
                                }
                            }
                            catch (e) {
                            }
                        }

                        else {
                            swal({ text: 'Sectoin Term Not Defined', width: 320, showCloseButton: true });
                        }
                    });

                }

                if (fee_frequency == 'M') {
                    var temp_value = parseInt(amount / 10);
                    var rem_amt = parseInt(amount - (temp_value * 10));

                    $scope.search_obj["section_fee_period1"] = (temp_value); $scope.search_obj["section_fee_period2"] = (temp_value); $scope.search_obj["section_fee_period3"] = (temp_value) + '.' + decimal; $scope.search_obj["section_fee_period4"] = temp_value;
                    $scope.search_obj["section_fee_period5"] = temp_value; $scope.search_obj["section_fee_period6"] = '0'; $scope.search_obj["section_fee_period7"] = '0'; $scope.search_obj["section_fee_period8"] = temp_value;
                    $scope.search_obj["section_fee_period9"] = temp_value; $scope.search_obj["section_fee_period10"] = temp_value; $scope.search_obj["section_fee_period11"] = temp_value; $scope.search_obj["section_fee_period12"] = temp_value + rem_amt;

                }

            }

            $scope.size = function (str) {
                // console.log(str);
                $scope.pagesize = str;
                debugger;
                $http.get(ENV.apiUrl + "api/common/SectionFee/getSectionFee?cur_name=" + $scope.temp.s_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grade_name=" + $scope.search_obj.sims_grade_code
                   + "&section_name=" + $scope.search_obj.sims_section_code + "&fee_category=" + $scope.search_obj.section_fee_category + "&pageIndex=" + $scope.pageindex + "&PageSize=" + $scope.pagesize).then(function (res) {
                       $scope.searhcResult = res.data;



                   })
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $http.get(ENV.apiUrl + "api/common/SectionFee/getSectionFee?cur_name=" + $scope.temp.s_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grade_name=" + $scope.search_obj.sims_grade_code
                   + "&section_name=" + $scope.search_obj.sims_section_code + "&fee_category=" + $scope.search_obj.section_fee_category + "&pageIndex=" + $scope.pageindex + "&PageSize=" + $scope.pagesize).then(function (res) {
                       $scope.searhcResult = res.data;
                       console.log($scope.searhcResult);
                   })
            }

            $scope.getacyr = function (str) {
                debugger;
                // $scope.cur_code = str;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    
                })
            }

            $scope.getsection = function (str,str1,str2) {

                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + str + "&grade_code=" + str1 + "&academic_year=" + str2).then(function (Allsection) {
                    $scope.All_Section_names = Allsection.data;
                    //   $scope.sims_section_name = $scope.section1[0].sims_section_code;
                    //  $scope.Show_Data();

                })
            };

            $scope.getAllSectionFees = function (isvalid) {
                if (isvalid) {
                    var len;
                    $scope.table1 = false;
                    $scope.busyindicator = true;
                    $scope.show_Update_table = false;

                    $http.get(ENV.apiUrl + "api/common/SectionFee/getSectionFee?cur_name=" + $scope.temp.s_cur_code + "&academic_year=" + $scope.temp.sims_academic_year
                        + "&grade_name=" + $scope.sims_grade_code + "&section_name=" + $scope.sims_section_code
                        + "&fee_category=" + $scope.section_fee_category).then(function (res) {

                            $scope.searhcResult = res.data;
                            $scope.len = $scope.searhcResult.length;
                            $scope.show_Update_table = true;
                            $scope.show_Insert_table = false;
                            $scope.show_ins_table = false;

                            if ($scope.searhcResult.length > 0) {
                                $scope.table1 = true;
                                $scope.busyindicator = false;
                            }
                            else {
                                $scope.table1 = false;
                                $scope.busyindicator = false;

                                swal({ text: 'Fee Not Found', width: 320, showCloseButton: true });
                            }
                            if ($scope.len == 0) {
                                $scope.get();
                            }
                        });
                }
            }

            $scope.New_table = function () {
                $scope.show_Insert_table = false;
                $scope.show_Update_table = false;
                $scope.show_ins_table = true;
                $scope.table1 = false;
                /*
                $http.get(ENV.apiUrl + "api/common/SectionFee/getAllFeeType").then(function (res) {
                    $scope.All_fee_type = res.data; //console.log($scope.All_Section_names);
                });
                $http.get(ENV.apiUrl + "api/common/SectionFee/getAllFeeCodeType").then(function (res) {
                    $scope.AllFeeCodeType = res.data; //console.log($scope.All_Section_names);
                });
                $http.get(ENV.apiUrl + "api/common/SectionFee/getAllFeeFrequency").then(function (res) {
                    $scope.All_freq = res.data; //console.log($scope.All_Section_names);
                });
                */
            }

            $scope.get = function () {
                $scope.show_Insert_table = true;
                $scope.show_Update_table = false;
                /*
                $http.get(ENV.apiUrl + "api/common/SectionFee/getAllFeeType").then(function (res) {
                    $scope.AllFeeType = res.data;
                    console.log($scope.AllFeeType);
                    });
                    */
            }

            $scope.demo = function (Str) {

                console.log($scope.sims_cur_code, $scope.sims_academic_year, $scope.section_fee_category,
                $scope.sims_grade_code, $scope.sims_section_code);


                var flag = false;
                var insertdata = Str;
                $scope.Sumflag = true;
                for (var i = 0; i < insertdata.length; i++) {
                    insertdata[i].section_fee_cur_code_name = $scope.sims_cur_code;
                    insertdata[i].section_fee_academic_year = $scope.sims_academic_year;
                    insertdata[i].section_fee_grade_name = $scope.sims_grade_code;
                    insertdata[i].section_fee_section_name = $scope.sims_section_code;
                    insertdata[i].section_fee_category_name = $scope.section_fee_category;
                    if (insertdata[i].section_fee_amount) {
                        var totalsum = (parseFloat(insertdata[i].section_fee_period1) + parseFloat(insertdata[i].section_fee_period2) + parseFloat(insertdata[i].section_fee_period3) + parseFloat(insertdata[i].section_fee_period4) + parseFloat(insertdata[i].section_fee_period5) + parseFloat(insertdata[i].section_fee_period6) + parseFloat(insertdata[i].section_fee_period7) + parseFloat(insertdata[i].section_fee_period8) + parseFloat(insertdata[i].section_fee_period9) + parseFloat(insertdata[i].section_fee_period10) + parseFloat(insertdata[i].section_fee_period11) + parseFloat(insertdata[i].section_fee_period12));
                        debugger;
                        if (totalsum == insertdata[i].section_fee_amount) {
                            $scope.flag = true;
                            $scope.Sumflag = true;
                        }
                        else {
                            $scope.Sumflag = false;
                            $scope.errormessage_at = insertdata[i].section_fee_code_desctiption;
                            break;
                        }
                    }
                    else {
                        $scope.flag = true;
                    }
                }
                console.log(insertdata);
                //debugger;
                if ($scope.flag == true && $scope.Sumflag == true) {
                    $scope.show_ins_table = false;
                    $scope.show_ins_table_loading = true;
                    $http.post(ENV.apiUrl + "api/common/SectionFee/InsertSectionFeeDetails", insertdata).then(function (res) {
                        $scope.msg = res.data;
                        console.log($scope.msg);
                        $scope.show_ins_table_loading = false;
                        if ($scope.msg == true) {
                            swal({ text: 'Section Fee Details Added Sucessfully', width: 380, showCloseButton: true });
                        }
                        else {
                            swal({ text: "Internal server Error", width: 300, showCloseButton: true });


                        }
                    }, function () {
                        $scope.show_ins_table_loading = false;
                        swal({ text: 'Error In Adding Section Fee Details', width: 380, showCloseButton: true });
                    });
                }
                else {
                    swal({ text: 'Fee Amount Mismatch for' + $scope.errormessage_at, width: 380, showCloseButton: true });

                }

            }

            $scope.Update = function (str) {
                $scope.search_obj = str;
                $scope.display = true; $scope.table = false; $scope.readonly = true; $scope.opration = true;
                $scope.Update_btn = true; $scope.Save_btn = false; $scope.opr = 'U';
            }

            $scope.btn_insert_cancel_click = function () {
                $scope.show_Insert_table = true;
                $scope.show_Update_table = false;
                $scope.show_ins_table = false;

            }

            $scope.UpdateData = function () {

                var update_data = {};

                var sum = (parseFloat($scope.search_obj.section_fee_period1) + parseFloat($scope.search_obj.section_fee_period2) + parseFloat($scope.search_obj.section_fee_period3) + parseFloat($scope.search_obj.section_fee_period4) + parseFloat($scope.search_obj.section_fee_period5) + parseFloat($scope.search_obj.section_fee_period6) + parseFloat($scope.search_obj.section_fee_period7) + parseFloat($scope.search_obj.section_fee_period8) + parseFloat($scope.search_obj.section_fee_period9) + parseFloat($scope.search_obj.section_fee_period10) + parseFloat($scope.search_obj.section_fee_period11) + parseFloat($scope.search_obj.section_fee_period12));

                if (sum == $scope.search_obj.section_fee_amount) {

                    if ($scope.search_obj.section_fee_installment_mode == false)
                    { $scope.search_obj.section_fee_installment_min_amount = '0' }

                    update_data = $scope.search_obj;

                    $http.post(ENV.apiUrl + "api/common/SectionFee/CUDSectionFeeDetailsnew", update_data).then(function (res) {
                        $scope.result = res.data;
                        if ($scope.result == true) {
                            $scope.table = true; $scope.opration = false;
                            swal({ text: 'Section Fee  Updated Sucessfully', width: 380, showCloseButton: true });
                            $scope.getAllSectionFees();
                        }
                        else {
                            // alert("Internal server Error");
                            swal({ text: "Section Fee  Not Updated", width: 300, showCloseButton: true });

                        }
                    });

                }
                else {

                    swal({ text: "Total amount should match with total amount defined for periods", width: 380, showCloseButton: true });
                }

                //$('#message').modal('show');
            }

            $scope.cancel = function () {
                $scope.table = true; $scope.opration = false;
            }

            $scope.New = function () {
                $scope.display = false;
                $scope.table = false;
                $scope.opration = true;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
            }

            var dom;

            $scope.CalculateAmountInsert = function (fee_frequency) {
                //  console.log(fee_frequency);
                var mydata = fee_frequency;
            }

            var dom;

            $scope.flag = true;

            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    console.log(info);
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";

                    $scope.readonly = true;
                    dom = $("<tr id='innerRow'><td class='details' colspan='13'>" +

                        "<table class='inner-table' cellpadding='5' cellspacing='0' style='width:100%;border:solid;border-width:1px;border-color:black'>" +
                        "<tbody>" +

                         "<tr style='background-color: #edefef'><td class='semi-bold'>" + "January" + "</td><td class='semi-bold'>" + "February" + "</td> <td class='semi-bold'>" + "March" + " </td><td class='semi-bold'>" + "April" + "</td><td class='semi-bold'>" + "May" + "</td> <td class='semi-bold'>" + "June" + "</td><td class='semi-bold'>" + "July" + "</td><td class='semi-bold'>" + "August" + "</td><td class='semi-bold'>" + "september" + "</td><td class='semi-bold'>" + "Octomber" + "</td><td class='semi-bold'>" + "November" + "</td><td class='semi-bold'>" + "December" + "</td>" +
                       "</tr>" +

                         "<tr><td>" +
                         '<label type="text" style="width:70px" class="form-control input-sm" >' + info.section_fee_period1 + '</label>' + "</td> <td>" +
                        '<label type="text" style="width:70px" class="form-control input-sm">' + info.section_fee_period2 + '</label>' + "</td><td>" +
                         '<label type="text" style="width:70px" class="form-control input-sm" >' + info.section_fee_period3 + '</label>' + "</td> <td>" +
                         '<label type="text" style="width:70px" class="form-control input-sm" >' + info.section_fee_period4 + '</label>' + "</td><td>" +
                         '<label type="text" style="width:70px" class="form-control input-sm" >' + info.section_fee_period5 + '</label>' + "</td><td>" +
                         '<label type="text" style="width:70px" class="form-control input-sm" >' + info.section_fee_period6 + '</label>' + "</td><td>" +
                         '<label type="text" style="width:70px" class="form-control input-sm" >' + info.section_fee_period7 + '</label>' + "</td><td>" +
                         '<label type="text" style="width:70px" class="form-control input-sm" >' + info.section_fee_period8 + '</label>' + "</td>" +
                         "<td>" + '<label type="text" style="width:70px" class="form-control input-sm" >' + info.section_fee_period9 + '</label>' +
                         " </td><td>" + '<label type="text" style="width:70px" class="form-control input-sm">' + info.section_fee_period10 + '</label>' +
                         "</td><td>" + '<label type="text" style="width:70px" class="form-control input-sm" >' + info.section_fee_period11 + '</label>' +
                         "</td><td>" + '<label type="text" style="width:70px" class="form-control input-sm" >' + info.section_fee_period12 + '</label>' + "</td>"
                        + " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;

                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.searhcResult.length; i++) {
                        $scope.searhcResult[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }


            };

            $scope.focusCallback = function ($event) {
                if ($event.target === null) {
                    return;
                }
                //  var target = $event.target || $event.srcElement;
                //target.style.visibility = 'hidden';
                $scope.targetField = $event.target;
                $scope.dom_flag = true;

            };

            $scope.DomFlag = function () {
                $scope.dom_flag = true;
            }

            $scope.Section_fee_periods_expand_plus_click = function (info, $event) {
                debugger;
                $(dom).remove();

                var scope = angular.element($scope.targetField).scope();
                scope.$parent.fee_type = info;

                /*  for (var i = 1; i <= 12; i++) {
                      scope.$parent.fee_type["section_fee_period" + i] = info["section_fee_period" + i];
                  }*/

                dom = $compile(document.getElementById("templateFeesExpand").innerText)(scope);

                $($event.currentTarget).parents("tr").after(dom);
                console.log($scope.AllFeeType);
            }

            $scope.Section_fee_periods_expand = function (info, $event) {
                for (var i = 1; i <= 12; i++) {
                    info["section_fee_period" + i] = '0';
                }

                $(dom).remove();
                if (info.section_fee_amount) {
                    $scope.calculator(info);
                }
                $scope.readonly = true;
                var scope = angular.element($scope.targetField).scope();
                for (var i = 1; i <= 12; i++) {
                    scope.$parent.fee_type["section_fee_period" + i] = info["section_fee_period" + i];
                }
                dom = $compile(document.getElementById("templateFeesExpand").innerText)(scope);


                if ($scope.dom_flag == true) {
                    $($scope.targetField).parents("tr").after(dom);
                    $scope.addfun_flag = true;
                    $scope.dom_flag = false;
                }
                else {
                    $($event.currentTarget).parents("tr").after(dom);
                }
                console.log($scope.AllFeeType);
            };

            $scope.calculator = function (info, $event) {
                //  debugger;
                var decimal = (info.section_fee_amount.split(".")[1]);
                if (decimal == undefined) {
                    decimal = '00'
                }
                var amount = Math.abs((info.section_fee_amount));
                var temp_value1 = 0;
                temp_value1 = (parseInt(Math.sign(amount / 3) * Math.ceil(Math.abs(amount / 3) / 5) * 5));

                if (info.section_fee_frequency == 'O' || info.section_fee_frequency == 'Y') {
                    info["section_fee_period1"] = amount; info["section_fee_period2"] = '0'; info["section_fee_period3"] = '0'; info["section_fee_period4"] = '0';
                    info["section_fee_period5"] = '0'; info["section_fee_period6"] = '0'; info["section_fee_period7"] = '0'; info["section_fee_period8"] = '0';
                    info["section_fee_period9"] = '0'; info["section_fee_period10"] = '0'; info["section_fee_period11"] = '0'; info["section_fee_period12"] = '0';
                }

                if (info.section_fee_frequency == 'T') {
                    var temp_value2 = temp_value1;
                    var temp_value3 = (parseInt(amount - (temp_value1 * 2)));
                    if (amount < 6) { temp_value1 = amount; temp_value2 = '0'; temp_value3 = '0'; }
                    if (temp_value3 < 0) {
                        temp_value2 = parseFloat(temp_value2) + parseFloat(temp_value3); if (temp_value2 < 0) { temp_value2 = '0'; }
                        temp_value3 = '0';
                    }
                    info["section_fee_period1"] = temp_value1;
                    info["section_fee_period2"] = temp_value2;
                    info["section_fee_period3"] = temp_value3 + '.' + decimal;
                    info["section_fee_period4"] = '0';
                    info["section_fee_period5"] = '0'; info["section_fee_period6"] = '0'; info["section_fee_period7"] = '0'; info["section_fee_period8"] = '0';
                    info["section_fee_period9"] = '0'; info["section_fee_period10"] = '0'; info["section_fee_period11"] = '0'; info["section_fee_period12"] = '0';
                }

                if (info.section_fee_frequency == 'M') {
                    var temp_value = parseInt(amount / 10);
                    var rem_amt = parseInt(amount - (temp_value * 10));

                    info["section_fee_period1"] = (temp_value); info["section_fee_period2"] = (temp_value); info["section_fee_period3"] = (temp_value) + '.' + decimal; info["section_fee_period4"] = temp_value;
                    info["section_fee_period5"] = temp_value; info["section_fee_period6"] = '0'; info["section_fee_period7"] = '0'; info["section_fee_period8"] = temp_value;
                    info["section_fee_period9"] = temp_value; info["section_fee_period10"] = temp_value; info["section_fee_period11"] = temp_value; info["section_fee_period12"] = temp_value + rem_amt;

                }
            }

            $scope.Reset = function () {

                $scope.sims_section_code = '';
                $scope.sims_grade_code = '';
                $scope.section_fee_category = '';
            }
        }])


})();


