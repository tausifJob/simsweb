﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, main1, deletefin = [];
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('ManualInventoryFeePostingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.pagesize = '';
            $scope.pageindex = 1;
            $scope.table = true;

            console.clear();
            var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.dt = {
                sims_from_date: dateyear,
                sims_to_date: dateyear,
            }

            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodosP = [];
            $scope.filteredTodosR = [];
            $scope.studentnameP = [];
            $scope.studentnameR = [];

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                if ($scope.studentnameP.length > 0)
                    $scope.filteredTodosP = $scope.todos.slice(begin, end);
                else if ($scope.studentnameR.length > 0)
                    $scope.filteredTodosR = $scope.todos.slice(begin, end);
            };

            $http.get(ENV.apiUrl + "api/ManualFeePosting/getCompanyName").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                $scope.getAccYear();
            });

            $scope.getAccYear = function (curCode) {

                $http.get(ENV.apiUrl + "api/ManualFeePosting/getAcademicYear?curCode=" + $scope.edt.sims_cur_code).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.edt = {
                        sims_cur_code: $scope.curriculum[0].sims_cur_code,
                        sims_academic_year: $scope.Acc_year[0].sims_academic_year
                    }
                    $scope.dt = {
                        sims_from_date: dateyear,
                        sims_to_date: dateyear,
                    }
                    $scope.Show_Data();
                });
            }

            $scope.Show = function () {

                if ($scope.filteredTodosP.length > 0) {
                    $scope.table1 = true;
                    $scope.table3 = true;
                    $scope.table2 = false;
                    $scope.table4 = false;
                    $('#loader0712').modal('hide');
                    main.checked = false;
                }
                else if ($scope.filteredTodosR.length > 0) {
                    $scope.table2 = true;
                    $scope.table4 = true;
                    $scope.table1 = false;
                    $scope.table3 = false;
                    $('#loader0712').modal('hide');
                    main1.checked = false;
                }
            }

            $scope.HideShow = function () {
                if ($scope.filteredTodosP.length > 0) {
                    $scope.table1 = false;
                    $scope.table3 = false;
                }
                else if ($scope.filteredTodosR.length > 0) {
                    $scope.table2 = false;
                    $scope.table4 = false;
                }
            }

            $scope.Show_Data = function () {
                debugger;

                $('#loader0712').modal({ backdrop: 'static', keyboard: false });

                if ($scope.filteredTodosR.length > 0) {
                    $scope.studentnameR.length = 0;
                    $scope.filteredTodosR.length = 0;
                }

                if ($scope.edt.sims_cur_code != undefined && $scope.edt.sims_academic_year != undefined) {

                    $http.get(ENV.apiUrl + "api/ManualFeePosting/getRecordPostinginventory?cur_name=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&from_date=" + $scope.dt.sims_from_date + "&to_date=" + $scope.dt.sims_to_date).then(function (res) {

                        $scope.studentnameP = res.data;
                        if ($scope.studentnameP.length == 0) {
                            $('#loader0712').modal('hide');
                            swal({ title: 'Alert', text: "Record Not Found For This Date...", showCloseButton: true, width: 450, height: 200 });
                            $scope.HideShow();
                        }

                        else {

                            $scope.feetemp = [];
                            $scope.PAcols = [];
                            var fc = false;

                            for (var i = 0; i < $scope.studentnameP.length; i++) {
                                if (!$scope.PAcols.includes($scope.studentnameP[i].dd_fee_payment_mode)) {
                                    $scope.PAcols.push($scope.studentnameP[i].dd_fee_payment_mode);
                                }
                            }

                            for (var i = 0; i < $scope.studentnameP.length; i++) {
                                var x = {};
                                for (var j = 0; j < $scope.PAcols.length; j++) {
                                    x[$scope.PAcols[j]] = 0;
                                }

                                x[$scope.studentnameP[i].dd_fee_payment_mode] = parseFloat($scope.studentnameP[i].fee_paid);
                                var ob =
                                {
                                    'sims_fee_code_description': $scope.studentnameP[i].sims_fee_code_description,
                                    'expected_fee': $scope.studentnameP[i].expected_fee,
                                    'discount': $scope.studentnameP[i].discount,
                                    'fee_paid': $scope.studentnameP[i].fee_paid,
                                    'total': $scope.studentnameP[i].total,
                                    'dd_fee_payment_mode': $scope.studentnameP[i].dd_fee_payment_mode,
                                    'fee_status_desc': $scope.studentnameP[i].fee_status_desc,
                                    'doc_date': $scope.studentnameP[i].doc_date,
                                    'sims_enroll_number': $scope.studentnameP[i].sims_enroll_number,
                                    'PAvals': {},
                                    'Paid': 0
                                }

                                ob.sum = new function (PAvals) {
                                    var sm = 0;
                                    for (var item in PAvals) {
                                        sm = parseFloat(item) + parseFloat(item);
                                    }
                                    return sm.toFixed();
                                }

                                ob.PAvals = x;
                                var fo = false;
                                for (var j = 0; j < $scope.feetemp.length; j++) {
                                    fo = false;
                                    if ($scope.feetemp[j].doc_date == $scope.studentnameP[i].doc_date && $scope.feetemp[j].sims_fee_code_description == $scope.studentnameP[i].sims_fee_code_description) {
                                        $scope.feetemp[j].discount = parseFloat($scope.feetemp[j].discount) + parseFloat($scope.studentnameP[i].discount);
                                        $scope.feetemp[j].expected_fee = parseFloat($scope.feetemp[j].expected_fee) + parseFloat($scope.studentnameP[i].expected_fee);
                                        $scope.feetemp[j][$scope.studentnameP[i].dd_fee_payment_mode] = parseFloat($scope.feetemp[j][$scope.studentnameP[i].dd_fee_payment_mode]) + parseFloat($scope.studentnameP[i].fee_paid);
                                        $scope.feetemp[j].total = parseFloat($scope.feetemp[j].total) + parseFloat($scope.studentnameP[i].total);
                                        $scope.feetemp[j].PAvals[$scope.studentnameP[i].dd_fee_payment_mode] = parseFloat($scope.feetemp[j].PAvals[$scope.studentnameP[i].dd_fee_payment_mode]);// + parseFloat($scope.studentnameP[i].fee_paid);
                                        fo = true;
                                    }
                                }

                                if (fo == false) {
                                    ob.PAvals[$scope.studentnameP[i].dd_fee_payment_mode] = parseFloat($scope.studentnameP[i].fee_paid);
                                    $scope.feetemp.push(ob);
                                }
                            }

                            for (var i = 0; i < $scope.feetemp.length; i++) {
                                var sm = 0;
                                for (var item in $scope.feetemp[i].PAvals) {
                                    sm = parseFloat(sm) + parseFloat($scope.feetemp[i].PAvals[item]);
                                }
                                $scope.feetemp[i].Paid = sm;
                            }

                            $scope.datatotal = [];


                            $scope.allDt = [];
                            for (var d = 0; d < $scope.feetemp.length; d++) {
                                var dt = $scope.feetemp[d].doc_date;
                                if ($scope.allDt[dt] == undefined) {
                                    $scope.allDt[dt] = getRecDateWise(dt);
                                    getRecDateWisetotal(dt);
                                }
                            }

                            $scope.finalData = [];
                            for (var item in $scope.allDt) {
                                var ob = { 'dt': item, 'PCL': $scope.allDt[item][0].PAcols, 'arr': $scope.allDt[item] };
                                $scope.finalData.push(ob);
                            }


                            var no = $scope.finalData.length;
                            $scope.pagesize = no;
                            $scope.numPerPage = no;
                            $scope.maxSize = no;
                            $scope.totalItems = $scope.finalData.length;
                            $scope.todos = $scope.finalData;
                            $scope.makeTodos();
                            $scope.Show();
                        }
                    })
                }

                else {
                    $('#loader0712').modal('hide');
                    swal({ title: 'Alert', text: "Please Select Curriculum & Academic Year Or Select Date Then Try Again...", showCloseButton: true, width: 450, height: 200 });
                    $scope.HideShow();
                }

            }

            function DeleteColumns(arr) {
                var l = arr.length;
                var temp = [];
                for (var i = 0; i < l; i++) {
                    for (var v in arr[i].PAvals) {
                        var ob = { 'mode': v, 'value': 0 };
                        temp.push(ob);
                    }
                    break;
                }
                for (var i = 0; i < l; i++) {
                    for (var t = 0; t < temp.length; t++) {
                        temp[t].value = parseFloat(temp[t].value) + parseFloat(arr[i].PAvals[temp[t].mode]);
                    }
                }
                var finalPA = {};
                for (var x = 0; x < temp.length; x++) {
                    if (temp[x].value != 0) {
                        finalPA[temp[x].mode] = temp[x].value;
                    }
                }
                for (var i = 0; i < l; i++) {
                    var n = {};
                    var he = [];
                    for (var x in finalPA) {
                        n[x] = arr[i].PAvals[x];
                        he.push(x);
                    }
                    arr[i].PAvals = n;
                    arr[i].PAcols = he;
                }
            }

            function getRecDateWise(dt) {
                var a = [];
                for (var i = 0; i < $scope.feetemp.length; i++) {
                    if ($scope.feetemp[i].doc_date == dt) {
                        a.push($scope.feetemp[i]);
                    }
                }
                DeleteColumns(a);
                return a;
            }

            function getRecDateWisetotal(dt) {
                var p = 0; var e = 0; var ds = 0; var pdv = 0; var tpd = 0;
                for (var i = 0; i < $scope.feetemp.length; i++) {
                    if ($scope.feetemp[i].doc_date == dt) {
                        p = p + parseFloat($scope.feetemp[i].expected_fee);
                        e = e + parseFloat($scope.feetemp[i].fee_paid);
                        ds = ds + parseFloat($scope.feetemp[i].discount);
                    }
                }
                $scope.datatotal.push({ 'date': dt, 'paid': e, 'expected': p, 'discount': ds });
            }

            $scope.footTotal = function (arrdt) {
                var ret = {};
                console.log(arrdt);
                var cnt = arrdt.length;
                for (var i = 0; i < cnt; i++) {
                    for (var x in arrdt[i].PAvals) {
                        if (ret[x] == undefined)
                            ret[x] = 0;
                        //ret[x] = ret[x]+(ret[x] == undefined ? 0 : parseFloat(arrdt[i].PAvals[x]));
                        ret[x] = ret[x] + (parseFloat(arrdt[i].PAvals[x]));
                    }
                }
                return ret;
            }

            $scope.Post_Fee = function () {
                $('#loader0712').modal({ backdrop: 'static', keyboard: false });
                var Savedata = [];
                var feepost = [];
                for (var i = 0; i < $scope.finalData.length; i++) {
                    var v = document.getElementById($scope.finalData[i].dt + i);
                    if (v.checked == true) {
                        feepost = {
                            //'doc_date': $scope.finalData[i].dt,
                            'doc_date': $scope.finalData[i].dt, //+ ',' + $scope.finalData[i].dt,
                            'sims_academic_year': $scope.edt.sims_academic_year,
                        }
                        Savedata.push(feepost);


                    }
                }

                $http.post(ENV.apiUrl + "api/ManualFeePosting/CUDfeepostinginventory", Savedata).then(function (msg) {

                    $scope.msg1 = msg.data;
                    if ($scope.msg1.strMessage != undefined) {
                        if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                            //swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                            //swal({ title: "Alert", text: "Please Select Atleast Record...", showCloseButton: true, width: 380, });
                            swal({ title: "Alert", text: "Posted Successfully...", width: 380, height: 200 });

                        }
                    }
                    else {
                        swal({ title: "Alert", text: "Not Posted...", showCloseButton: true, width: 380, });
                    }
                    $scope.Show_Data();
                });
            }

            //$scope.searched = function (valLists, toSearch) {
            //    return _.filter(valLists,
            //    function (i) {
            //        /* Search Text in all  fields */
            //        return searchUtil(i, toSearch);
            //    });
            //};

            //$scope.search = function () {
            //    $scope.todos = $scope.searched($scope.studentnameP, $scope.searchText);
            //    $scope.totalItems = $scope.todos.length;
            //    $scope.currentPage = '1';
            //    if ($scope.searchText == '') {
            //        $scope.todos = $scope.studentnameP;
            //    }
            //    $scope.makeTodos();
            //    main.checked = false;
            //    $scope.CheckAllChecked();
            //}

            //function searchUtil(item, toSearch) {
            //    /* Search Text in all 3 fields */
            //    return (item.dt.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sager == toSearch) ? true : false;
            //}

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.finalData.length; i++) {
                        var v = document.getElementById($scope.finalData[i].dt + i);
                        //var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }

                else {

                    for (var i = 0; i < $scope.finalData.length; i++) {
                        var v = document.getElementById($scope.finalData[i].dt + i);
                        //var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyoneselect = function () {

                $("input[type='checkbox']").change(function (e) {

                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });



            /////////////////////////////////////Reversal Table Code.............................................

            $scope.Show_Data_R = function () {
                $('#loader0712').modal({ backdrop: 'static', keyboard: false });
                if ($scope.filteredTodosP.length > 0) {
                    $scope.studentnameP.length = 0;
                    $scope.filteredTodosP.length = 0;
                }

                //if (cur_code != undefined && ac_year != undefined) {
                if ($scope.edt.sims_cur_code != undefined && $scope.edt.sims_academic_year != undefined) {
                    $http.get(ENV.apiUrl + "api/ManualFeePosting/getRecordReversalinventory?cur_name=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&from_date=" + $scope.dt.sims_from_date + "&to_date=" + $scope.dt.sims_to_date).then(function (res) {

                        $scope.studentnameR = res.data;

                        if ($scope.studentnameR.length == 0) {
                            $('#loader0712').modal('hide');
                            swal({ title: 'Alert', text: "Please Try Again Record Not Found...", showCloseButton: true, width: 450, height: 200 });
                        }

                        else {

                            $scope.feetempR = [];
                            $scope.PAcolsR = [];
                            var fcR = false;

                            for (var i = 0; i < $scope.studentnameR.length; i++) {
                                if (!$scope.PAcolsR.includes($scope.studentnameR[i].dd_fee_payment_mode)) {
                                    $scope.PAcolsR.push($scope.studentnameR[i].dd_fee_payment_mode);
                                }
                            }

                            for (var i = 0; i < $scope.studentnameR.length; i++) {
                                var xR = {};
                                for (var j = 0; j < $scope.PAcolsR.length; j++) {
                                    xR[$scope.PAcolsR[j]] = 0;
                                }
                                xR[$scope.studentnameR[i].dd_fee_payment_mode] = parseFloat($scope.studentnameR[i].fee_paid);
                                var obR = {
                                    'sims_fee_code_description': $scope.studentnameR[i].sims_fee_code_description,
                                    'expected_fee': $scope.studentnameR[i].expected_fee,
                                    'discount': $scope.studentnameR[i].discount,
                                    'fee_paid': $scope.studentnameR[i].fee_paid,
                                    'total': $scope.studentnameR[i].total,
                                    'dd_fee_payment_mode': $scope.studentnameR[i].dd_fee_payment_mode,
                                    'fee_status_desc': $scope.studentnameR[i].fee_status_desc,
                                    'doc_date': $scope.studentnameR[i].doc_date,
                                    'sims_enroll_numberR': $scope.studentnameR[i].sims_enroll_numberR,
                                    'PAvalsR': {},
                                    'Paid': 0
                                }
                                obR.sum = new function (PAvalsR) {
                                    var smR = 0;
                                    for (var item in PAvalsR) {
                                        smR = parseFloat(item) + parseFloat(item);
                                    }
                                    return smR.toFixed();
                                }


                                obR.PAvalsR = xR;

                                var foR = false;
                                for (var j = 0; j < $scope.feetempR.length; j++) {
                                    foR = false;
                                    if ($scope.feetempR[j].doc_date == $scope.studentnameR[i].doc_date && $scope.feetempR[j].sims_fee_code_description == $scope.studentnameR[i].sims_fee_code_description) {
                                        $scope.feetempR[j].discount = parseFloat($scope.feetempR[j].discount) + parseFloat($scope.studentnameR[i].discount);
                                        $scope.feetempR[j].expected_fee = parseFloat($scope.feetempR[j].expected_fee) + parseFloat($scope.studentnameR[i].expected_fee);
                                        $scope.feetempR[j][$scope.studentnameR[i].dd_fee_payment_mode] = parseFloat($scope.feetempR[j][$scope.studentnameR[i].dd_fee_payment_mode]) + parseFloat($scope.studentnameR[i].fee_paid);
                                        $scope.feetempR[j].total = parseFloat($scope.feetempR[j].total) + parseFloat($scope.studentnameR[i].total);
                                        $scope.feetempR[j].PAvalsR[$scope.studentnameR[i].dd_fee_payment_mode] = parseFloat($scope.feetempR[j].PAvalsR[$scope.studentnameR[i].dd_fee_payment_mode]);// + parseFloat($scope.studentnameR[i].fee_paid);
                                        foR = true;
                                    }
                                }
                                if (foR == false) {
                                    obR.PAvalsR[$scope.studentnameR[i].dd_fee_payment_mode] = parseFloat($scope.studentnameR[i].fee_paid);
                                    $scope.feetempR.push(obR);
                                }
                            }

                            for (var i = 0; i < $scope.feetempR.length; i++) {
                                var smR = 0;
                                for (var item in $scope.feetempR[i].PAvalsR) {
                                    smR = parseFloat(smR) + parseFloat($scope.feetempR[i].PAvalsR[item]);
                                }
                                $scope.feetempR[i].Paid = smR;
                            }
                            $scope.datatotalR = [];
                            $scope.allDtR = [];
                            for (var d = 0; d < $scope.feetempR.length; d++) {
                                var dtR = $scope.feetempR[d].doc_date;
                                if ($scope.allDtR[dtR] == undefined) {
                                    $scope.allDtR[dtR] = getRecDateWiseR(dtR);
                                    getRecDateWisetotalR(dtR);
                                }
                            }

                            $scope.finalDataR = [];
                            for (var item in $scope.allDtR) {
                                var obR = { 'dtR': item, 'PCLR': $scope.allDtR[item][0].PAcolsR, 'arrR': $scope.allDtR[item] };
                                $scope.finalDataR.push(obR);
                            }

                            var noR = $scope.finalDataR.length;
                            $scope.pagesize = noR;
                            $scope.numPerPage = noR;
                            $scope.maxSize = noR;
                            $scope.totalItems = $scope.finalDataR.length;
                            $scope.todos = $scope.finalDataR;
                            $scope.makeTodos();
                            $scope.Show();
                        }
                    })
                }

                else {
                    $('#loader0712').modal('hide');
                    swal({ title: 'Alert', text: "Please Select Curriculum & Academic Year Or Select Date Then Try Again...", showCloseButton: true, width: 450, height: 200 });
                    $scope.HideShow();
                }
            }

            function DeleteColumnsR(arrR) {
                var lR = arrR.length;
                var tempR = [];
                for (var i = 0; i < lR; i++) {
                    for (var v in arrR[i].PAvalsR) {
                        var obR = { 'mode': v, 'value': 0 };
                        tempR.push(obR);
                    }
                    break;
                }
                for (var i = 0; i < lR; i++) {
                    for (var tR = 0; tR < tempR.length; tR++) {
                        tempR[tR].value = parseFloat(tempR[tR].value) + parseFloat(arrR[i].PAvalsR[tempR[tR].mode]);
                    }
                }
                var finalPAR = {};
                for (var x = 0; x < tempR.length; x++) {
                    if (tempR[x].value != 0) {
                        finalPAR[tempR[x].mode] = tempR[x].value;
                    }
                }
                for (var i = 0; i < lR; i++) {
                    var nR = {};
                    var heR = [];
                    for (var xR in finalPAR) {
                        nR[xR] = arrR[i].PAvalsR[xR];
                        heR.push(xR);
                    }
                    arrR[i].PAvalsR = nR;
                    arrR[i].PAcolsR = heR;
                }
            }

            function getRecDateWiseR(dtR) {
                var aR = [];
                for (var i = 0; i < $scope.feetempR.length; i++) {
                    if ($scope.feetempR[i].doc_date == dtR) {
                        aR.push($scope.feetempR[i]);
                    }
                }
                DeleteColumnsR(aR);
                return aR;
            }

            function getRecDateWisetotalR(dtR) {
                var p = 0; var e = 0; var ds = 0;
                for (var i = 0; i < $scope.feetempR.length; i++) {
                    if ($scope.feetempR[i].doc_date == dtR) {
                        p = p + parseFloat($scope.feetempR[i].expected_fee);
                        e = e + parseFloat($scope.feetempR[i].fee_paid);
                        ds = ds + parseFloat($scope.feetempR[i].discount);
                    }
                }
                $scope.datatotalR.push({ 'date': dtR, 'paid': e, 'expected': p, 'discount': ds });
            }

            $scope.footTotalR = function (arrdtR) {
                var retR = {};
                console.log(arrdtR);
                var cntR = arrdtR.length;
                for (var iR = 0; iR < cntR; iR++) {
                    for (var xR in arrdtR[iR].PAvalsR) {
                        if (retR[xR] == undefined)
                            retR[xR] = 0;
                        retR[xR] = retR[xR] + (parseFloat(arrdtR[iR].PAvalsR[xR]));
                    }
                }
                return retR;
            }

            $scope.CheckAllCheckedR = function () {
                main1 = document.getElementById('key1');
                if (main1.checked == true) {
                    for (var i = 0; i < $scope.filteredTodosR.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodosR.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main1.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyoneselectR = function () {

                $("input[type='checkbox']").change(function (e) {

                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main1 = document.getElementById('mainchk1');
                if (main1.checked == true) {
                    main1.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            //$scope.searchedR = function (valLists, toSearch) {
            //    return _.filter(valLists,
            //    function (i) {
            //        /* Search Text in all  fields */
            //        return searchUtilR(i, toSearch);
            //    });
            //};

            //$scope.searchR = function () {
            //    $scope.todos = $scope.searchedR($scope.studentnameR, $scope.searchTextR);
            //    $scope.totalItems = $scope.todos.length;
            //    $scope.currentPage = '1';
            //    if ($scope.searchTextR == '') {
            //        $scope.todos = $scope.studentnameR;
            //    }
            //    $scope.makeTodos();
            //    main1.checked = false;
            //    $scope.CheckAllCheckedR();
            //}

            //function searchUtilR(item, toSearch) {
            //    /* Search Text in all 3 fields */
            //    return (item.dt.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sager == toSearch) ? true : false;
            //}

            $scope.Reversal_Fee = function () {


                $('#loader0712').modal({ backdrop: 'static', keyboard: false });
                var SavedataR = [];
                var feepostR = [];
                for (var i = 0; i < $scope.finalDataR.length; i++) {
                    //var v = document.getElementById(i);
                    var v = document.getElementById($scope.finalDataR[i].dtR + i);
                    if (v.checked == true) {
                        feepostR = {
                            'doc_date': $scope.finalDataR[i].dtR,
                            'sims_academic_year': $scope.edt.sims_academic_year,
                        }
                        SavedataR.push(feepostR);
                    }
                }

                $http.post(ENV.apiUrl + "api/ManualFeePosting/CUDfeereversalinventory", SavedataR).then(function (msg) {

                    $scope.msg1 = msg.data;
                    if ($scope.msg1.strMessage != undefined) {
                        if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                            //swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                            swal({ title: "Alert", text: "Fee Reversal Successfully...", width: 380, height: 200 });
                        }
                    }
                    else {
                        swal({ title: "Alert", text: "Fee Not Reversal...", showCloseButton: true, width: 380, });
                    }
                    $scope.Show_Data_R();
                });
            }

            $scope.Clear_Data = function () {

                $scope.studentnameP = [];
                $scope.studentnameR = [];

                $scope.edt = {
                    sims_academic_year: '',
                    sims_from_date: '',
                }

                $scope.dt = {
                    sims_to_date: '',
                    sims_cur_code: '',
                }

                if ($scope.filteredTodosP.length > 0 || $scope.filteredTodosR.length > 0) {
                    $scope.table1 = false;
                    $scope.table3 = false;
                    $scope.table2 = false;
                    $scope.table4 = false;
                    $scope.filteredTodosP.length = 0;
                    $scope.filteredTodosR.length = 0;
                }

            }

        }
        ])
})();