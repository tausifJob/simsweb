﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('SimsInvoiceEmailCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.table = false;


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                $scope.getAccYear($scope.edt.sims_cur_code);
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                    $scope.getGrade(curCode, $scope.temp.sims_academic_year);
                });
            }

            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
            }

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }

            $scope.Show_Data = function (str, str1, start_date, end_date) {
                if (str != undefined && str1 != undefined) {
                    $http.get(ENV.apiUrl + "api/StudentReport/getStudentEmailInvoice?cur_name=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grd_code=" + $scope.edt.sims_grade_code + "&sec_code=" + $scope.edt.sims_section_code + "&user_code=" + $scope.edt.sims_student_enroll_number + "&start_date=" + $scope.edt.sims_from_date + "&end_date=" + $scope.edt.sims_to_date).then(function (res) {
                        $scope.studlist = res.data;
                        $scope.totalItems = $scope.studlist.length;
                        $scope.todos = $scope.studlist;
                        $scope.makeTodos();
                        console.log($scope.studlist);
                        if ($scope.studlist.length == 0) {
                            swal({ title: 'Alert', text: "Please Try Again Record Not Found...", showCloseButton: true, width: 450, height: 200 });
                            $scope.table1 = false;
                        }
                        else {
                            $scope.table1 = true;
                        }
                    })

                }
                else {
                    swal({ title: 'Alert', text: "Please Select Curriculum & Academic Year Then Try Again...", showCloseButton: true, width: 450, height: 200 });
                }
            }

            $scope.View = function (str) {
                var data = {
                    location: 'Sims.SIMR60',
                    parameter: {
                        sims_invoce_mode: str.sims_invoce_mode,
                        sims_period_code: str.sims_period_code,
                    },
                    state: 'main.SimIEM',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')
            }

            $scope.SendEmail = function (gtr) {
                $http.get(ENV.apiUrl + "api/StudentReport/sendemailinvoice?cur_name=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grd_code=" + grt.sims_grade_code + "&sec_code=" + grt.sims_section_code + "&enroll=" + gtr.sims_student_enroll_number + "&invoice_no=" + gtr.in_no + "&invoice_mode=" + grt.sims_invoce_mode + "&invoice_period=" + grt.sims_period_code + "&emailids=" + grt.father_email).then(function (res) {
                    //(          c.sims_cur_code,                                c.sims_academic_year,                   c.sims_grade_code,                   c.sims_section_code,                 c.sims_enroll_number,                             c.in_no,                       c.sims_invoce_mode,                         c.sims_period_code,                   c.father_email);
                    swal({ title: "Alert", text: "Email Sent Successfully...", showCloseButton: true, width: 380, });
                })
            }


            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {

                $scope.todos = $scope.searched($scope.studlist, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.studlist;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_parent_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.in_no == toSearch) ? true : false;

            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $scope.Check_Date = function (fromdate, todate) {
                if (fromdate > todate) {
                    swal({ title: 'Alert', text: "Please Select Future Date...", showCloseButton: true, width: 450, height: 200 });
                    $scope.edt.sims_to_date = '';
                }

            }

        }])
})();