﻿(function () {
    'use strict';
    var feecode = [], code = '';
    var main;
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    var data1 = [];
    simsController.controller('FeeTermCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            //$scope.operation = true;
            $scope.table1 = true;
            $scope.editmode = false;

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }
            $scope.edt = "";

            $http.get(ENV.apiUrl + "api/FeeTerm/getFeeTerm").then(function (Fee_Data) {
                $scope.FeeData = Fee_Data.data;
                $scope.totalItems = $scope.FeeData.length;
                $scope.todos = $scope.FeeData;
                $scope.makeTodos();
                console.log($scope.FeeData);

            })

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;
            })

            $scope.getacyr = function (str) {
                $scope.cur_code = str;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                })
            }

            $scope.Alltermcode = function () {
                $http.get(ENV.apiUrl + "api/FeeTerm/getAllTerms?curCode=" + JSON.stringify($scope.temp)).then(function (AllTerms) {
                    $scope.All_Terms = AllTerms.data;
                })

            }

            $scope.showdata = function (str) {
                debugger
                $scope.code = str;
                for (var i = 0; i < $scope.All_Terms.length; i++) {
                    if ($scope.All_Terms[i].sims_term_code == str) {
                        $scope.edt = {
                            sims_fee_term_code: $scope.All_Terms[i].sims_fee_term_code,
                            sims_term_status: $scope.All_Terms[i].sims_term_status,
                            sims_term_desc_en: $scope.All_Terms[i].sims_term_desc_en,
                            sims_term_desc_ar: $scope.All_Terms[i].sims_term_desc_ar,
                            sims_term_desc_ot: $scope.All_Terms[i].sims_term_desc_ot,
                            sims_term_desc_fr: $scope.All_Terms[i].sims_term_desc_fr,
                            sims_term_start_date: $scope.All_Terms[i].sims_term_start_date,
                            sims_term_end_date: $scope.All_Terms[i].sims_term_end_date
                        }
                    }
                }
            }


            $scope.New = function () {

                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.editmode = false;
                $scope.opr = 'S';
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = {};
                $scope.edt['sims_term_status'] = true;
                $scope.temp = '';
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.up = function (str) {
                $scope.opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.operation = true;
                debugger
                $scope.edt = {

                    sims_cur_code: str.sims_cur_code,
                    sims_term_cur_short_name_en: str.sims_term_cur_short_name_en,
                    sims_academic_year: str.sims_academic_year,
                    sims_academic_year_description: str.sims_academic_year_description,
                    sims_term_code: str.sims_term_code,

                    sims_term_desc_ar: str.sims_term_desc_ar,
                    sims_term_desc_en: str.sims_term_desc_en,
                    sims_term_desc_fr: str.sims_term_desc_fr,

                    sims_term_desc_ot: str.sims_term_desc_ot,
                    sims_term_end_date: str.sims_term_end_date,
                    sims_term_start_date: str.sims_term_start_date,
                    sims_term_status: str.sims_term_status
                };
                $scope.showdata(str.sims_term_code);

            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

            }

            $scope.Save = function (myForm) {
                if (myForm) {
                    var data = {
                        sims_cur_code: $scope.temp.sims_cur_code,
                        sims_academic_year: $scope.temp.sims_academic_year,
                        sims_term_code: $scope.temp.sims_term_code,
                        sims_term_desc_en: $scope.edt.sims_term_desc_en,
                        sims_term_desc_ar: $scope.edt.sims_term_desc_ar,
                        sims_term_desc_fr: $scope.edt.sims_term_desc_ot,
                        sims_term_desc_ot: $scope.edt.sims_term_desc_fr,
                        sims_term_start_date: $scope.edt.sims_term_start_date,
                        sims_term_end_date: $scope.edt.sims_term_end_date,
                        sims_term_status: $scope.edt.sims_term_status,
                        opr: 'I'
                    }

                    $scope.exist = false;
                    for (var i = 0; i < $scope.FeeData.length; i++) {
                        if ($scope.FeeData[i].sims_cur_code == data.sims_cur_code && $scope.FeeData[i].sims_academic_year == data.sims_academic_year && $scope.FeeData[i].sims_term_code == data.sims_term_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ title: "Alert", text: "Record Already exists", width: 300, height: 200 });
                    }
                    else {
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/FeeTerm/FeeTermCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                            }
                            $scope.operation = false;
                            $http.get(ENV.apiUrl + "api/FeeTerm/getFeeTerm").then(function (Fee_Data) {
                                $scope.FeeData = Fee_Data.data;
                                $scope.totalItems = $scope.FeeData.length;
                                $scope.todos = $scope.FeeData;
                                $scope.makeTodos();

                            });

                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                    data1 = [];
                }
            }

            $scope.Update = function () {

                debugger
                var data = $scope.edt;
                data.opr = 'U';
                data1.push(data);
                $http.post(ENV.apiUrl + "api/FeeTerm/FeeTermCUD", data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                    }
                    $scope.operation = false;
                    $http.get(ENV.apiUrl + "api/FeeTerm/getFeeTerm").then(function (Fee_Data) {
                        $scope.FeeData = Fee_Data.data;
                        $scope.totalItems = $scope.FeeData.length;
                        $scope.todos = $scope.FeeData;
                        $scope.makeTodos();
                    });
                })
                $scope.operation = false;
                $scope.table1 = true;
                data1 = [];

            }


            $scope.deleterecord = function () {
                var data1 = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    //var v = document.getElementById(i);
                    var v = document.getElementById($scope.filteredTodos[i].sims_term_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletelocacode = ({
                            'sims_term_code': $scope.filteredTodos[i].sims_term_code,
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            'sims_cur_code': $scope.filteredTodos[i].sims_cur_code,
                            opr: 'D'
                        });
                        feecode.push(deletelocacode);
                    }
                }
                debugger
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/FeeTerm/FeeTermCUD", feecode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/FeeTerm/getFeeTerm").then(function (Fee_Data) {
                                                $scope.FeeData = Fee_Data.data;
                                                $scope.totalItems = $scope.FeeData.length;
                                                $scope.todos = $scope.FeeData;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/FeeTerm/getFeeTerm").then(function (Fee_Data) {
                                                $scope.FeeData = Fee_Data.data;
                                                $scope.totalItems = $scope.FeeData.length;
                                                $scope.todos = $scope.FeeData;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                //var v = document.getElementById(i);
                                var v = document.getElementById($scope.filteredTodos[i].sims_term_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                data1 = [];
                $scope.currentPage = true;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;
            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_term_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_term_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });



            $scope.createdate = function (end_date, start_date, name) {

                debugger;
                var month1 = end_date.split("/")[0];
                var day1 = end_date.split("/")[1];
                var year1 = end_date.split("/")[2];
                var new_end_date = year1 + "/" + month1 + "/" + day1;

                var year = start_date.split("/")[0];
                var month = start_date.split("/")[1];
                var day = start_date.split("/")[2];
                var new_start_date = year + "/" + month + "/" + day;

                if (new_end_date < new_start_date) {

                    $rootScope.strMessage = "Please Select Future Date";
                    $('#message').modal('show');

                    $scope.edt[name] = '';
                }
                else {

                    $scope.edt[name] = new_end_date;
                }
            }

            $scope.showdate = function (date, name) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                $scope.edt[name] = year + "/" + month + "/" + day;
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.FeeData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.FeeData;
                }
                $scope.makeTodos();
            }


            function searchUtil(item, toSearch) {
                return (
                     item.sims_term_desc_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_term_desc_fr.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_term_desc_ar.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_term_desc_ot.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_term_code == toSearch) ? true : false;


            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);



        }]
        )
})();