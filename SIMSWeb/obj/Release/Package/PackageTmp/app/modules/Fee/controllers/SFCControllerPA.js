﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SFCControllerPA',
        ['$scope', '$state', '$rootScope', '$timeout', '$stateParams', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout,$stateParams, gettextCatalog, $http, ENV) {
          // Pre-Required Functions and Variables
          // Start

            $('*[data-datepicker="true"] input[type="text"]').datepicker(
                {
                    todayBtn: true,
                    orientation: "top left",
                    autoclose: true,
                    todayHighlight: true,
                    format: 'yyyy-mm-dd'
            });
            var op = $stateParams.sel;
            if (op != undefined) {
                try {
                    $scope.edt = {};
                    $scope.edt.sims_cur_code=op.cur;
                    $scope.edt.sims_academic_year=op.ac;
                    $scope.edt.sims_grade_code=op.gr;
                    $scope.edt.sims_section_code=op.sc;
                    $scope.edt.search_txt = op.st;
                    _onFetchData();
                } catch (e) {

                }
            }
            $scope.callDet = function (info) {
                $state.go('main.PATrans', { 'sel': $scope.sel, 'IP': { 'StudCurr': info.std_fee_cur_code, 'StudAcademic': info.std_fee_academic_year, 'StudEnroll': info.std_fee_enroll_number } });
            }

            //params: { 'StudCurr': '', 'StudAcademic': '', 'StudEnroll': '' }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
            $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '', sims_search_code: '' };
            /*FILTER*/
            $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllCurName").then(function (CurData) {
                $scope.CurData = CurData.data;
                $scope.edt['sims_cur_code'] = CurData.data[0].sims_cur_code;
                $scope.cur_code_change(CurData.data[0].sims_cur_code);
          
            });
            $http.get(ENV.apiUrl + "api/Fee/SFS/getSeachOptions").then(function (searchoptions) {
                $scope.searchoptions = searchoptions.data;

                for (var i = 0; i < $scope.searchoptions.length; i++) {
                    if ($scope.searchoptions[i].sims_search_code_status == 'Y') {
                        $scope.edt['sims_search_code'] = $scope.searchoptions[i].sims_search_code;
                    }
                }
            });

            $scope.currentYear_status = 'C';
            $scope.cur_code_change = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllAcademicYears?cur_code=" + $scope.edt.sims_cur_code).then(function (AcademicYears) {
                    $scope.AcademicYears = AcademicYears.data;
                    for (var i = 0; i < $scope.AcademicYears.length; i++) {
                        if ($scope.AcademicYears[i].sims_academic_year_status == 'C') {
                            //   $scope.edt['sims_cur_code'] = cur_values;
                            $scope.edt['sims_academic_year'] = $scope.AcademicYears[i].sims_academic_year;
                        }
                    }
                    $scope.academic_year_change();
                });
                
            }
            $scope.academic_year_change = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year).then(function (Grades) {
                    $scope.Grades = Grades.data;
                });

            }
            $scope.grade_change = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllSections?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year + "&g_code=" + $scope.edt.sims_grade_code).then(function (sections) {
                    $scope.sections = sections.data;
                    console.log($scope.sections);

                });

            }
            
            
            /* PAGER FUNCTIONS*/
           
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 9, $scope.maxSize = 7, $scope.currentPage_ind = 0;

            $scope.makeTodos = function () {
                $scope.currentPage_ind = 0;
                
                if ((Math.round($scope.totalItems / $scope.numPerPage) * $scope.numPerPage) > $scope.totalItems)
                    $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage);
                else
                    $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage) + 1;

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.currentPage_ind = str;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage_ind = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                console.log("PageNumber=" + $scope.numPerPage);
                

                $scope.makeTodos();
            }

            /* PAGER FUNCTIONS*/
            
            function _onFetchData() {
                if ($scope.edt.search_txt == undefined)
                    $scope.edt.search_txt = '';
                $scope.sel = { 'cur': $scope.edt.sims_cur_code, 'ac': $scope.edt.sims_academic_year, 'gr': $scope.edt.sims_grade_code, 'sc': $scope.edt.sims_section_code, 'st': $scope.edt.search_txt ,'search_flag':'3'}
                $http.get(ENV.apiUrl + "api/Fee/SFS/getStudent_FeesPA?minval=0&maxval=0&cc=" + $scope.edt.sims_cur_code + "&ay=" + $scope.edt.sims_academic_year + "&gc=" + $scope.edt.sims_grade_code + "&sc=" + $scope.edt.sims_section_code + "&search_stud=" + $scope.edt.search_txt + "&search_flag=" + $scope.edt.sims_search_code).then(function (feesData) {
                    $scope.feesData = feesData.data;
                    $scope.totalItems = $scope.feesData.length;
                    $scope.todos = $scope.feesData;
                    $scope.makeTodos();

                });
            }
            $scope.fetchDatakeyPress = function ($event) {
                if ($event.keyCode == 13) {
                    _onFetchData();
                }
            }
            $scope.btnPreview_click = function () {
                _onFetchData();
            }
            
        //Events End
        }])
})();

