﻿
(function () {
    'use strict';

    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('Sims043RefundCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {


            $scope.grid = true;
            var user = $rootScope.globals.currentUser.username;
            $scope.SelectedPaymentModeObject = [];
            $scope.pagesize = '5';
            $scope.addfee = false;
            $scope.grand_total_amount1 = '0.00';
            $scope.grand_total_amount = '0.00';
            $scope.temp = { fee_concession_number: '0' };
            $scope.other_fee_amount1 = '0.00';
            $scope.grand_total_amount = '0.00';
            $scope.other_fee_amount = '0.00';
            $scope.other_fee_amount1 = '0.00';
            $scope.doc_discount_amount = '0.00';
            $scope.doc_discount_amount1 = '0.00';
            $scope.fee_concession_percentage = '0.00';
            var now = new Date();
            var month = (now.getMonth() + 1);
            var day = now.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            $scope.payment_date = month + '/' + day + '/' + now.getFullYear();

            $scope.agent2 = true;
            var data = '';
            var SelectedAmount = '0.00';

            var dataset = [];
            var selectedData = '';
            $scope.paymentmode = 'Cash';
            $scope.SelectedFeeObject = [];
            var datatoAddfee = '';
            $scope.show = true;
            var count3 = 0;
            var Transaction_id = 'N/A';
            $scope.SelectedOtherfeeObject = [];

            //var t = document.getElementsByName('optionyes');
            //for (var j = 0; j < t.length; j++) {
            //    if (t[j].checked === true) {
            //        $scope.paymentmode = t[j].defaultValue;
            //    }
            //}

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;

                $scope.sims_cur_code = $scope.curriculum[0].sims_cur_code;
                $scope.getacyr();
            })

            $http.get(ENV.apiUrl + "api/studentfeerefund/GetSimsSearchOptions").then(function (res) {
                $scope.search_by_data = res.data;
                $scope.search_by_code = $scope.search_by_data[0].search_by_code;
            })

            $http.get(ENV.apiUrl + "api/studentfeerefund/GetSimsPaymentMode").then(function (GetSimsPaymentMode) {
                $scope.Get_SimsPayment_Mode = GetSimsPaymentMode.data;
            })

            $scope.getacyr = function () {

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.sims_cur_code).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    $scope.sims_academic_year = $scope.Academic_year[0].sims_academic_year;
                    $scope.getsections();
                })
            }

            $scope.getsections = function () {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.sims_cur_code + "&academic_year=" + $scope.sims_academic_year).then(function (res) {
                    $scope.grade = res.data;
                    //  $scope.sims_grade_code = $scope.grade[0].sims_grade_code;
                    $scope.getsection();
                })
            };

            $scope.getsection = function () {

                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.sims_cur_code + "&grade_code=" + $scope.sims_grade_code + "&academic_year=" + $scope.sims_academic_year).then(function (Allsection) {
                    $scope.section1 = Allsection.data;
                    //   $scope.sims_section_name = $scope.section1[0].sims_section_code;
                    //  $scope.Show_Data();

                })
            };


            //$scope.get = function () {
            //    $scope.Show_Data();

            //}

            $scope.showtables = function () {
                if ($scope.temp.strExcess == true) {

                    $scope.addfee = false;
                } else {
                    $scope.addfee = true;

                }
            }

            $scope.btn_Cancel_Click = function () {

                $scope.grid = true;
                $scope.addfee = false;
                $scope.discounttable = false;
                $scope.SelectedOtherfee = false;
                $scope.SelectedFeeObject = [];
                $scope.SelectedPaymentModeObject = [];
                $scope.GetStudentRefundFeeNumberDetails = [];
                $scope.SelectedOtherfeeObject = [];
                $scope.temp.fee_concession_number = '';
                $scope.fee_concession_percentage = 0;
                $scope.doc_discount_amount = 0;
                $scope.grand_total_amount = 0;
            }

            $scope.Show_Data = function () {

                $scope.maintable = false;
                if ($scope.sims_cur_code == undefined) {

                    $scope.sims_cur_code = null;
                }
                if ($scope.sims_grade_code == undefined) {

                    $scope.sims_grade_code = null;
                }
                if ($scope.sims_academic_year == undefined) {

                    $scope.sims_academic_year = null;
                }
                if ($scope.sims_section_name == undefined) {

                    $scope.sims_section_name = null;
                }
                if ($scope.search_flag == undefined) {

                    $scope.search_flag = null;
                }
                $scope.busyindicator = true;

                if ($scope.search_text == undefined || $scope.search_text == '') {
                    $scope.search_text = null;
                }

                $http.get(ENV.apiUrl + "api/studentfeerefund/GetAllStudentFee?minval=" + null + "&maxval=" + null + "&cc=" + $scope.sims_cur_code + "&ay=" + $scope.sims_academic_year + "&gc=" + $scope.sims_grade_code + "&sc=" + $scope.sims_section_name + "&search_stud=" + $scope.search_text + "&search_flag=" + $scope.search_by_code).then(function (GetAllStudentFee) {
                    $scope.GetAllStudentFee = GetAllStudentFee.data;
                    if ($scope.GetAllStudentFee.length != 0) {
                        $scope.currentPage = '1';
                        $scope.maintable = true;
                        $scope.busyindicator = false;
                        $scope.totalItems = $scope.GetAllStudentFee.length;
                        $scope.todos = $scope.GetAllStudentFee;
                        $scope.makeTodos();
                    }
                    else {
                        $scope.busyindicator = false;
                        $scope.maintable = false;
                        swal({ text: 'Data Not Found', width: 300, showCloseButton: true });
                    }
                    $scope.busyindicator = false;
                });
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseFloat($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseFloat(begin) + parseFloat($scope.numPerPage);



                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {

                if (str == 'All') {

                    $scope.totalItems = $scope.GetAllStudentFee.length;
                    $scope.todos = $scope.GetAllStudentFee;
                    $scope.filteredTodos = $scope.GetAllStudentFee;
                    $scope.numPerPage = $scope.GetAllStudentFee.length;
                    $scope.maxSize = $scope.GetAllStudentFee.length
                    $scope.makeTodos();
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#Table3").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.GetStudentFeeForRefund = function (str) {

                $scope.grand_total_amount1 = '0.00';
                $scope.grand_total_amount = '0.00';
                $scope.temp = { fee_concession_number: '0' };
                $scope.other_fee_amount1 = '0.00';
                $scope.grand_total_amount = '0.00';
                $scope.other_fee_amount = '0.00';
                $scope.other_fee_amount1 = '0.00';
                $scope.doc_discount_amount = '0.00';
                $scope.doc_discount_amount1 = '0.00';
                $scope.fee_concession_percentage = '0.00';
                SelectedAmount = '0.00'


                var data = [];
                dataset = [];
                $scope.GetStudentRefundFeeNumberDetails1 = [];
                $http.get(ENV.apiUrl + "api/studentfeerefund/GetAgentList?enroll_number=" + str.std_fee_enroll_number + "&a_year=" + str.std_fee_academic_year).then(function (GetAgentList) {
                    $scope.GetAgentList = GetAgentList.data;

                });

                $http.get(ENV.apiUrl + "api/studentfeerefund/GetStudentRefundFeeNumberDetails?enroll=" + str.std_fee_enroll_number + "&cur_code=" + str.std_fee_cur_code + "&aca_year=" + str.std_fee_academic_year).then(function (GetStudentRefundFeeNumberDetails) {
                    $scope.GetStudentRefundFeeNumberDetails1 = GetStudentRefundFeeNumberDetails.data;

                    if ($scope.GetStudentRefundFeeNumberDetails1.length != 0) {

                        $scope.Student_Name = str.std_fee_student_name;
                        $scope.Student_Enroll = str.std_fee_enroll_number;
                        $scope.Student_Class = str.std_fee_grade_name + '(' + str.std_fee_section_name + ')';

                        $scope.grid = false;
                        for (var i = 0; i < $scope.GetStudentRefundFeeNumberDetails1.length; i++) {
                            $('tr').removeClass("row_selected");
                            dataset.push($scope.GetStudentRefundFeeNumberDetails1[i]);
                        }

                        var check = document.getElementById("mainchk");

                        if (check.checked == true) {
                            check.checked = false;
                        }

                        $scope.GetStudentRefundFeeNumberDetails = dataset;
                        data = {
                            std_fee_cur_code: $scope.GetStudentRefundFeeNumberDetails[0].std_fee_cur_code
                        , std_fee_academic_year: $scope.GetStudentRefundFeeNumberDetails[0].std_fee_academic_year
                        , std_fee_grade_code: $scope.GetStudentRefundFeeNumberDetails[0].std_fee_grade_code
                        , std_fee_section_code: $scope.GetStudentRefundFeeNumberDetails[0].std_fee_section_code
                        , FeeEnrollNo: $scope.GetStudentRefundFeeNumberDetails[0].std_fee_enroll_number

                        };

                        $scope.sims_academic_year = data.std_fee_academic_year;
                        $scope.sims_cur_code = data.std_fee_cur_code;
                        $scope.sims_grade_code = data.std_fee_grade_code;
                        $scope.sims_section_name = data.std_fee_section_code;
                        $scope.getacyr();


                        $http.post(ENV.apiUrl + "api/studentfeerefund/AllGetConcessionFee", data).then(function (AllGetConcessionFee) {
                            $scope.AllGetConcessionFee = AllGetConcessionFee.data;



                        });
                        $scope.GetDiscountFeeTypes = [];
                        $http.post(ENV.apiUrl + "api/studentfeerefund/FeeDiscountTypes", data).then(function (GetDiscountFeeTypes) {
                            $scope.GetDiscountFeeTypes = GetDiscountFeeTypes.data;
                        });

                        $scope.paymentmode = $scope.Get_SimsPayment_Mode[0].paymentMode;

                    }
                    else {
                        $scope.grid = true;
                        swal({ text: 'Refund Fee Data Not Found', width: 320, showCloseButton: true });
                    }
                });
            }

            $http.get(ENV.apiUrl + "api/studentfeerefund/GetAllBankNames").then(function (GetAllBankNames) {
                $scope.AllBankNames = GetAllBankNames.data;
            });

            $scope.Check_pament_mode = function (info) {
                if (info == 'Ch') {
                    $scope.cheque_info = false;
                }
                else {
                    $scope.cheque_info = true;
                    $scope.temp2 = '';
                }
            }

            $scope.btn_StudentSearch_Click = function () {
                $scope.searchtable = false;
                $scope.temp = '';
                $('#myModal').modal('show');
            }

            /************************************************* Add Multiple Fee To Refund****************************************************/
            $scope.AddMultipleFee = function () {

                $scope.addfee = true;

                $scope.SelectedPaymentModeObject = [];
                $scope.SelectedFeeObject = [];

                if ($scope.SelectedFeeObject.length <= 0) {
                    SelectedAmount = 0.00;
                }
                var check = document.getElementById("mainchk");
                for (var i = 0; i < $scope.GetStudentRefundFeeNumberDetails.length; i++) {
                    var v = document.getElementById($scope.GetStudentRefundFeeNumberDetails[i].feeTypeGroup);
                    if (check.checked == true) {

                        v.checked = true;
                        $('tr').addClass("row_selected");
                        $scope.GetStudentRefundFeeNumberDetails[i].Cheque_NO = 'N/A';
                        $scope.GetStudentRefundFeeNumberDetails[i].Payment_mode = $scope.paymentmode;
                        $scope.GetStudentRefundFeeNumberDetails[i].primary_no = $scope.GetStudentRefundFeeNumberDetails[i].feeTypeGroup + $scope.GetStudentRefundFeeNumberDetails[i].std_fee_child_period_No + $scope.GetStudentRefundFeeNumberDetails[i].std_fee_number + $scope.GetStudentRefundFeeNumberDetails[i].std_fee_enroll_number;
                        SelectedAmount = parseFloat(SelectedAmount) + parseFloat($scope.GetStudentRefundFeeNumberDetails[i].std_fee_child_paying_amount_temp);
                        $scope.grand_total_amount = parseFloat(SelectedAmount).toFixed(2);

                        var adddata = {};

                        adddata.Transaction_id = 'N/A';
                        adddata.Cheque_NO = $scope.GetStudentRefundFeeNumberDetails[i].Cheque_NO = 'N/A';
                        adddata.primary_no = $scope.GetStudentRefundFeeNumberDetails[i].feeTypeGroup + $scope.GetStudentRefundFeeNumberDetails[i].std_fee_child_period_No + $scope.GetStudentRefundFeeNumberDetails[i].std_fee_number + $scope.GetStudentRefundFeeNumberDetails[i].std_fee_enroll_number;
                        adddata.Payment_mode = $scope.GetStudentRefundFeeNumberDetails[i].Payment_mode = $scope.paymentmode;
                        adddata.std_fee_child_paying_amount_temp = $scope.GetStudentRefundFeeNumberDetails[i].std_fee_child_paying_amount_temp;
                        adddata.std_fee_enroll_number = $scope.GetStudentRefundFeeNumberDetails[i].std_fee_enroll_number;
                        adddata.std_fee_type = $scope.GetStudentRefundFeeNumberDetails[i].std_fee_type;
                        adddata.feeTypeGroup = $scope.GetStudentRefundFeeNumberDetails[i].feeTypeGroup;
                        adddata.std_fee_other_paying_amount = $scope.GetStudentRefundFeeNumberDetails[i].std_fee_other_paying_amount;
                        adddata.std_fee_Period = $scope.GetStudentRefundFeeNumberDetails[i].std_fee_child_period;
                        adddata.std_fee_child_period_No = $scope.GetStudentRefundFeeNumberDetails[i].std_fee_child_period_No;
                        adddata.std_fee_code = $scope.GetStudentRefundFeeNumberDetails[i].std_fee_code;
                        adddata.std_fee_concession_amount = $scope.GetStudentRefundFeeNumberDetails[i].std_fee_concession_amount;
                        adddata.std_fee_concession_amount_used = $scope.GetStudentRefundFeeNumberDetails[i].std_fee_concession_amount_used;
                        adddata.std_fee_discount_percent = $scope.GetStudentRefundFeeNumberDetails[i].std_fee_discount_percent;
                        adddata.std_fee_grade_code = $scope.GetStudentRefundFeeNumberDetails[i].std_fee_grade_code;
                        adddata.std_fee_id = $scope.GetStudentRefundFeeNumberDetails[i].std_fee_id;
                        adddata.std_fee_number = $scope.GetStudentRefundFeeNumberDetails[i].std_fee_number;
                        adddata.std_fee_section_code = $scope.GetStudentRefundFeeNumberDetails[i].std_fee_section_code;
                        adddata.stud_sib_select_status = $scope.GetStudentRefundFeeNumberDetails[i].stud_sib_select_status;
                        adddata.std_fee_cur_code = $scope.GetStudentRefundFeeNumberDetails[i].std_fee_cur_code;
                        adddata.Bank_name = 'Select Name';
                        adddata.Amount = $scope.GetStudentRefundFeeNumberDetails[i].std_fee_child_paying_amount_temp;
                        adddata.Chk_amount = '0.00';
                        adddata.sims_fee_collection_mode = $scope.GetStudentRefundFeeNumberDetails[i].sims_fee_collection_mode;


                        if (adddata.Payment_mode == 'Cash') {
                            adddata.Bank_name = 'N/A';
                            adddata.Transaction_id = 'N/A';
                            adddata.Cheque_NO = 'N/A';
                        }

                        $scope.SelectedPaymentModeObject.push(adddata);
                        $scope.SelectedFeeObject.push(adddata);



                    }
                    else {
                        $scope.addfee = false;
                        SelectedAmount = 0.00;
                        v.checked = false;
                        $('tr').removeClass("row_selected");
                        $scope.SelectedFeeObject = [];
                        $scope.SelectedPaymentModeObject = [];
                        $scope.grand_total_amount = '';
                    }
                }

            }

            $scope.MultipleSelectFee = function () {

                if ($scope.paymentmode == 'Agent') {
                    if ($scope.slma_ldgrctl_code != undefined && $scope.slma_ldgrctl_code != '') {
                        $scope.AddMultipleFee();
                    }
                    else {
                        swal({ text: 'Select Agent Name', width: 300, showCloseButton: true });
                        var main = document.getElementById('mainchk');
                        if (main.checked == true) {
                            main.checked = false;
                        }
                    }
                }
                else {
                    $scope.AddMultipleFee();
                }
            }

            /************************************************* Add Single Fee To Refund****************************************************/
            $scope.AddSelectOneByOneFee = function (info) {



                var adddata = {};
                if ($scope.paymentmode == '') {
                    $scope.paymentmode = $scope.paymentmode;
                    info.primary_no = info.feeTypeGroup + info.std_fee_child_period_No + info.std_fee_number + info.std_fee_enroll_number;
                }
                info.primary_no = info.feeTypeGroup + info.std_fee_child_period_No + info.std_fee_number + info.std_fee_enroll_number;
                $scope.addfee = true;

                var d = document.getElementById(info.feeTypeGroup);
                if (d.checked == true) {

                    SelectedAmount = parseFloat(SelectedAmount) + parseFloat(info.std_fee_child_paying_amount_temp);
                    $scope.grand_total_amount = parseFloat(SelectedAmount).toFixed(2);
                    $scope.grand_total_amount1 = parseFloat(SelectedAmount).toFixed(2);
                    adddata.Cheque_NO = info.Cheque_NO = 'N/A';
                    adddata.Transaction_id = 'N/A';
                    adddata.primary_no = info.feeTypeGroup + info.std_fee_child_period_No + info.std_fee_number + info.std_fee_enroll_number;
                    adddata.Payment_mode = info.Payment_mode = $scope.paymentmode;
                    adddata.std_fee_child_paying_amount_temp = info.std_fee_child_paying_amount_temp;
                    adddata.std_fee_enroll_number = info.std_fee_enroll_number;
                    adddata.std_fee_type = info.std_fee_type;
                    adddata.feeTypeGroup = info.feeTypeGroup;
                    adddata.std_fee_other_paying_amount = info.std_fee_other_paying_amount;
                    adddata.std_fee_Period = info.std_fee_child_period;
                    adddata.sims_fee_collection_mode = info.sims_fee_collection_mode;
                    adddata.std_fee_child_period_No = info.std_fee_child_period_No;
                    adddata.std_fee_code = info.std_fee_code;
                    adddata.std_fee_concession_amount = info.std_fee_concession_amount;
                    adddata.std_fee_concession_amount_used = info.std_fee_concession_amount_used;
                    adddata.std_fee_discount_percent = info.std_fee_discount_percent;
                    adddata.std_fee_grade_code = info.std_fee_grade_code;
                    adddata.std_fee_id = info.std_fee_id;
                    adddata.std_fee_number = info.std_fee_number;
                    adddata.stud_sib_select_status = info.stud_sib_select_status;
                    adddata.Bank_name = 'select name';
                    adddata.Amount = info.std_fee_child_paying_amount_temp;
                    adddata.Chk_amount = '0.00';

                    //if ($scope.fee_concession_percentage > 0) {
                    //    $scope.grand_total_amount1 = '0.00';
                    //    adddata.std_fee_other_paying_amount = Math.round(parseFloat($scope.fee_concession_percentage / 100) * parseFloat(adddata.std_fee_child_paying_amount_temp)).toFixed(2);
                    //    adddata.Chk_amount = Math.round(adddata.std_fee_other_paying_amount).toFixed(2);

                    //    $scope.doc_discount_amount = Math.round(parseFloat($scope.doc_discount_amount) + parseFloat(adddata.Chk_amount)).toFixed(2);
                    //    adddata.discount_flag = true;
                    //    $scope.grand_total_amount1 = Math.round(parseFloat(SelectedAmount) - parseFloat($scope.doc_discount_amount)).toFixed(2);
                    //}

                    $scope.SelectedPaymentModeObject.push(adddata);
                    $scope.SelectedFeeObject.push(adddata);


                }
                else {

                    SelectedAmount = parseFloat(SelectedAmount) - parseFloat(info.std_fee_child_paying_amount_temp);
                    $scope.grand_total_amount = parseFloat(SelectedAmount).toFixed(2);
                    info.std_fee_child_paying_amount_temp = info.std_orignal_exp_amount;
                    for (var k = 0; k < $scope.SelectedPaymentModeObject.length; k++) {
                        if (info.primary_no == $scope.SelectedPaymentModeObject[k].primary_no) {
                            $scope.SelectedPaymentModeObject.splice(k, 1);
                        }
                        var main = document.getElementById('mainchk');
                        if (main.checked == true) {
                            main.checked = false;
                        }
                    }

                    for (var k = 0; k < $scope.SelectedPaymentModeObject.length; k++) {
                        if (info.primary_no == $scope.SelectedPaymentModeObject[k].primary_no) {
                            $scope.SelectedPaymentModeObject.splice(k, 1);
                        }

                    }

                    for (var m = 0; m < $scope.SelectedFeeObject.length; m++) {
                        if (info.primary_no == $scope.SelectedFeeObject[m].primary_no) {
                            $scope.SelectedFeeObject.splice(m, 1);
                        }
                    }
                    for (var m = 0; m < $scope.SelectedFeeObject.length; m++) {
                        if (info.primary_no == $scope.SelectedFeeObject[m].primary_no) {
                            $scope.SelectedFeeObject.splice(m, 1);
                        }
                    }
                }
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                var flag = false;

                for (var i = 0; i < $scope.GetStudentRefundFeeNumberDetails.length; i++) {
                    var t = document.getElementById($scope.GetStudentRefundFeeNumberDetails[i].feeTypeGroup);
                    if (t.checked == true) {
                        flag = true;
                    }
                }
                if (flag != true && $scope.SelectedOtherfeeObject.length <= 0) {
                    $scope.SelectedFeeObject = [];
                    $scope.SelectedPaymentModeObject = [];
                    $scope.addfee = false;
                    $scope.SelectedDiscountfeeObject = [];
                    $scope.discounttable = false;
                    $scope.doc_discount_amount = '0.00';
                    $scope.grand_total_amount = '0.00';
                    $scope.grand_total_amount1 = '0.00';
                    $scope.fee_concession_percentage = '0.00';
                    $scope.fee_concession_number = '0';

                }

                $scope.btn_Adddiscount_click();
            }

            $scope.SelectOneByOneFee = function (info) {

                if ($scope.paymentmode == 'Agent') {
                    var d = document.getElementById(info.feeTypeGroup);
                    if ($scope.slma_ldgrctl_code != undefined && $scope.slma_ldgrctl_code != '') {
                        $scope.AddSelectOneByOneFee(info);
                    }
                    else {
                        swal({ text: 'Select Agent Name', width: 300, showCloseButton: true });
                        var main = document.getElementById('mainchk');
                        if (main.checked == true) {
                            main.checked = false;
                        }
                        d.checked = false;
                    }
                }
                else {
                    $scope.AddSelectOneByOneFee(info);
                }
            }

            /************************************************* If PayMent Mode Is Agent Check****************************************************/

            $scope.SelectPayMentAgent = function (str) {

                //var t = document.getElementsByName('optionyes');
                //for (var j = 0; j < t.length; j++) {
                //    if (t[j].checked === true) {
                //        $scope.paymentmode = t[j].defaultValue;
                //    }
                //}

                if (str == 'Agent') {
                    $scope.agent2 = false;
                }
                else {
                    $scope.agent2 = true;
                }

            }

            /************************************************* Remove Selected Payment Mode****************************************************/

            $scope.RemoveSelectedPaymentMode = function ($event, index, str) {

                var flag = false;
                for (var i = 0; i < $scope.GetStudentRefundFeeNumberDetails.length; i++) {
                    var t = document.getElementById($scope.GetStudentRefundFeeNumberDetails[i].feeTypeGroup);
                    if (t.checked == true && $scope.GetStudentRefundFeeNumberDetails[i].feeTypeGroup == str[0].feeTypeGroup && $scope.GetStudentRefundFeeNumberDetails[i].std_fee_type == str[0].std_fee_type && $scope.GetStudentRefundFeeNumberDetails[i].std_fee_child_period == str[0].std_fee_Period) {
                        flag = true;
                    }
                }
                if (flag != true) {
                    str.splice(index, 1)
                }
            }

            $scope.Ischecked = function (info) {
                for (var i = 0; i < $scope.GetStudentDetailsRefound.length; i++) {
                    var amount = info.total_amount + '.00';
                    if (amount1 < amount && $scope.GetStudentDetailsRefound[i].enroll_number == info.enroll_number && $scope.GetStudentDetailsRefound[i].fee_code == info.fee_code) {
                        info.total_amount = amount1;
                        swal({ text: 'Sum Of Total Amount And Used Amount Should Not Greater Than Fee Amount', width: 380, showCloseButton: true });
                    }
                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.btn_Reset_Click = function () {
                $scope.temp3 = '';
                $scope.temp2 = '';
                $scope.payment_date = '';

            }

            $scope.SelectBankName = function () {
                $scope.show = true;
            }

            /*************************************************Split Fee****************************************************/

            var mainamount = '';
            var mainpaymentmode = '';
            $scope.btn_Split_Click = function (info) {

                mainpaymentmode = info.Payment_mode;
                mainamount = info.std_fee_child_paying_amount_temp;
                $scope.collection_type = info.sims_fee_collection_mode;

                $scope.AddnewPaymentdetails = [];
                var payment_date = 'payment_date';
                $scope.edt = info;
                $scope.edt1 = {
                    std_fee_child_paying_amount_temp2: info.std_fee_child_paying_amount_temp,
                    std_fee_child_paying_amount_temp1: info.std_fee_child_paying_amount_temp,
                    payment_date: $scope.payment_date,
                    payment_mode: info.Payment_mode
                };

                $('#MyModal').modal('show');
            }

            /*************************************************Child Window Code****************************************************/

            $scope.btn_AddnewPaymentdetails_Click = function () {


                if ($scope.edt1.std_fee_child_paying_amount_temp1 == $scope.edt1.std_fee_child_paying_amount_temp2) {
                    swal({ text: 'No Remaining Amount', width: 380, showCloseButton: true })
                }
                else {

                    $scope.cash_modes = $scope.Get_SimsPayment_Mode;

                    // $scope.cash_modes = [{
                    //     name: 'Bank Transfer',
                    //     code: 'Bank_transfer'
                    // },
                    // {
                    //     name: 'Demand Draft',
                    //     code: 'Demand_Draft'
                    // },
                    // {
                    //     name: 'Agent',
                    //     code: 'Agent'
                    // },
                    //{
                    //    name: 'Concession',
                    //    code: 'Concession'
                    //},
                    // {
                    //     name: 'Cash',
                    //     code: 'Cash'
                    // },
                    //  {
                    //      name: 'Cheque',
                    //      code: 'Cheque'
                    //  },
                    //  {
                    //      name: 'Credit Card',
                    //      code: 'Credit Card'
                    //  },

                    // ]

                    $scope.edt.std_fee_child_paying_amount_temp3 = parseFloat($scope.edt1.std_fee_child_paying_amount_temp1) - ($scope.edt1.std_fee_child_paying_amount_temp2);
                    $scope.AddnewPaymentdetails = [];
                    $scope.AddnewPaymentdetails.push($scope.edt)
                }
            }

            $scope.btn_AddnewPaymentdetails_update_Click = function () {



                var selectedData1 = [];

                var now = new Date();
                var month = (now.getMonth() + 1);
                var day = now.getDate();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                $scope.payment_date1 = now.getFullYear() + '/' + month + '/' + day;
                if ($scope.AddnewPaymentdetails.length > 0) {

                    for (var i = 0; i < $scope.SelectedPaymentModeObject.length; i++) {
                        if ($scope.SelectedPaymentModeObject[i].primary_no == $scope.AddnewPaymentdetails[0].primary_no) {

                            selectedData1 = {};
                            selectedData1.primary_no = $scope.SelectedPaymentModeObject[i].primary_no;
                            selectedData1.Payment_mode = $scope.AddnewPaymentdetails[0].payment_mode;
                            selectedData1.std_fee_type = $scope.AddnewPaymentdetails[0].std_fee_type;
                            selectedData1.Cheque_NO = $scope.AddnewPaymentdetails[0].Cheque_NO;
                            selectedData1.Bank_name = $scope.AddnewPaymentdetails[0].Bank_name;
                            selectedData1.Cheque_DD_Date = $scope.payment_date1;
                            selectedData1.Transaction_id = $scope.AddnewPaymentdetails[0].Cheque_NO;
                            selectedData1.std_fee_type = $scope.AddnewPaymentdetails[0].std_fee_type;
                            selectedData1.Amount = $scope.SelectedPaymentModeObject[i].std_fee_child_paying_amount_temp = $scope.AddnewPaymentdetails[0].std_fee_child_paying_amount_temp3;
                            selectedData1.Chk_amount = '0'
                            selectedData1.std_fee_child_period_No = $scope.AddnewPaymentdetails[0].std_fee_child_period_No;
                            selectedData1.std_fee_code = $scope.AddnewPaymentdetails[0].std_fee_code;
                            selectedData1.std_fee_concession_amount = $scope.AddnewPaymentdetails[0].std_fee_concession_amount;
                            selectedData1.std_fee_concession_amount_used = $scope.AddnewPaymentdetails[0].std_fee_concession_amount_used;
                            selectedData1.std_fee_discount_percent = $scope.AddnewPaymentdetails[0].std_fee_discount_percent;
                            selectedData1.std_fee_grade_code = $scope.AddnewPaymentdetails[0].std_fee_grade_code;
                            selectedData1.std_fee_id = $scope.AddnewPaymentdetails[0].std_fee_id;
                            selectedData1.std_fee_number = $scope.AddnewPaymentdetails[0].std_fee_number;
                            selectedData1.std_fee_section_code = $scope.AddnewPaymentdetails[0].std_fee_section_code;
                            selectedData1.stud_sib_select_status = $scope.AddnewPaymentdetails[0].stud_sib_select_status;
                            selectedData1.std_fee_cur_code = $scope.AddnewPaymentdetails[0].std_fee_cur_code;
                            selectedData1.sims_fee_collection_mode = $scope.AddnewPaymentdetails[0].sims_fee_collection_mode;
                            $scope.SelectedPaymentModeObject.push(selectedData1);
                            break;
                        }
                    }

                    for (var i = 0; i < $scope.SelectedPaymentModeObject.length; i++) {
                        if ($scope.SelectedPaymentModeObject[i].primary_no == $scope.AddnewPaymentdetails[0].primary_no) {
                            $scope.SelectedPaymentModeObject[i].Amount = parseFloat($scope.SelectedPaymentModeObject[i].Amount) - parseFloat($scope.AddnewPaymentdetails[0].std_fee_child_paying_amount_temp3);
                            break;
                        }
                    }
                    for (var j = 0; j < $scope.SelectedFeeObject.length; j++) {

                        if ($scope.SelectedFeeObject[j].primary_no == $scope.AddnewPaymentdetails[0].primary_no) {
                            selectedData = {};
                            selectedData.primary_no = $scope.AddnewPaymentdetails[0].primary_no;
                            selectedData.std_fee_enroll_number = $scope.AddnewPaymentdetails[0].std_fee_enroll_number;
                            selectedData.std_fee_type = $scope.AddnewPaymentdetails[0].std_fee_type;
                            selectedData.Payment_mode = $scope.AddnewPaymentdetails[0].payment_mode;
                            selectedData.Cheque_NO = $scope.AddnewPaymentdetails[0].Cheque_NO;
                            selectedData.std_fee_child_paying_amount_temp = $scope.AddnewPaymentdetails[0].std_fee_child_paying_amount_temp3;
                            selectedData.std_fee_other_paying_amount = $scope.AddnewPaymentdetails[0].std_fee_other_paying_amount;
                            selectedData.Bank_name = $scope.AddnewPaymentdetails[0].Bank_name;
                            selectedData.Transaction_id = $scope.AddnewPaymentdetails[0].Cheque_NO;
                            selectedData.Cheque_DD_Date = $scope.payment_date1;
                            selectedData.std_fee_child_period_No = $scope.AddnewPaymentdetails[0].std_fee_child_period_No;
                            selectedData.std_fee_code = $scope.AddnewPaymentdetails[0].std_fee_code;
                            selectedData.std_fee_concession_amount = $scope.AddnewPaymentdetails[0].std_fee_concession_amount;
                            selectedData.std_fee_concession_amount_used = $scope.AddnewPaymentdetails[0].std_fee_concession_amount_used;
                            selectedData.std_fee_discount_percent = $scope.AddnewPaymentdetails[0].std_fee_discount_percent;
                            selectedData.std_fee_grade_code = $scope.AddnewPaymentdetails[0].std_fee_grade_code;
                            selectedData.std_fee_id = $scope.AddnewPaymentdetails[0].std_fee_id;
                            selectedData.std_fee_number = $scope.AddnewPaymentdetails[0].std_fee_number;
                            selectedData.std_fee_section_code = $scope.AddnewPaymentdetails[0].std_fee_section_code;
                            selectedData.stud_sib_select_status = $scope.AddnewPaymentdetails[0].stud_sib_select_status;
                            selectedData.std_fee_cur_code = $scope.AddnewPaymentdetails[0].std_fee_cur_code;
                            selectedData.sims_fee_collection_mode = $scope.AddnewPaymentdetails[0].sims_fee_collection_mode;
                            $scope.SelectedFeeObject.push(selectedData);

                            if ($scope.SelectedFeeObject[j].Payment_mode == 'Cash' || $scope.AddnewPaymentdetails[0].payment_mode == 'Bank Transfer') {
                                $scope.AddnewPaymentdetails[0].Cheque_NO = 'N/A';
                            }
                            break;
                        }
                        else {

                            selectedData = {};
                            selectedData.primary_no = $scope.AddnewPaymentdetails[0].primary_no;
                            selectedData.std_fee_enroll_number = $scope.AddnewPaymentdetails[0].std_fee_enroll_number;
                            selectedData.std_fee_type = $scope.AddnewPaymentdetails[0].std_fee_type;
                            selectedData.Payment_mode = $scope.AddnewPaymentdetails[0].payment_mode;
                            selectedData.Cheque_NO = $scope.AddnewPaymentdetails[0].Cheque_NO;
                            selectedData.std_fee_child_paying_amount_temp = $scope.AddnewPaymentdetails[0].std_fee_child_paying_amount_temp3;
                            selectedData.std_fee_other_paying_amount = $scope.AddnewPaymentdetails[0].std_fee_other_paying_amount;
                            selectedData.Bank_name = $scope.AddnewPaymentdetails[0].Bank_name;
                            selectedData.Transaction_id = $scope.AddnewPaymentdetails[0].Cheque_NO;
                            selectedData.Cheque_DD_Date = $scope.payment_date1;
                            selectedData.std_fee_child_period_No = $scope.AddnewPaymentdetails[0].std_fee_child_period_No;
                            selectedData.std_fee_code = $scope.AddnewPaymentdetails[0].std_fee_code;
                            selectedData.std_fee_concession_amount = $scope.AddnewPaymentdetails[0].std_fee_concession_amount;
                            selectedData.std_fee_concession_amount_used = $scope.AddnewPaymentdetails[0].std_fee_concession_amount_used;
                            selectedData.std_fee_discount_percent = $scope.AddnewPaymentdetails[0].std_fee_discount_percent;
                            selectedData.std_fee_grade_code = $scope.AddnewPaymentdetails[0].std_fee_grade_code;
                            selectedData.std_fee_id = $scope.AddnewPaymentdetails[0].std_fee_id;
                            selectedData.std_fee_number = $scope.AddnewPaymentdetails[0].std_fee_number;
                            selectedData.std_fee_section_code = $scope.AddnewPaymentdetails[0].std_fee_section_code;
                            selectedData.stud_sib_select_status = $scope.AddnewPaymentdetails[0].stud_sib_select_status;
                            selectedData.std_fee_cur_code = $scope.AddnewPaymentdetails[0].std_fee_cur_code;

                            selectedData.sims_fee_collection_mode = $scope.AddnewPaymentdetails[0].sims_fee_collection_mode;

                            $scope.SelectedFeeObject.push(selectedData);
                            break;
                        }
                    }

                    for (var i = 0; i < $scope.SelectedPaymentModeObject.length; i++) {
                        if ($scope.SelectedPaymentModeObject[i].Payment_mode == 'Cash' || $scope.SelectedPaymentModeObject[i].Payment_mode == 'Bank Transfer') {
                            $scope.SelectedPaymentModeObject[i].Cheque_NO = 'N/A';
                        }
                    }

                    for (var i = 0; i < $scope.SelectedPaymentModeObject.length; i++) {
                        if ($scope.SelectedPaymentModeObject[i].Payment_mode == 'Agent' || $scope.SelectedPaymentModeObject[i].Payment_mode == 'Credit Card' || $scope.SelectedPaymentModeObject[i].Payment_mode == 'Cheque' || $scope.SelectedPaymentModeObject[i].Payment_mode == 'Demand Draft') {
                            $scope.SelectedPaymentModeObject[i].Transaction_id = 'N/A';
                        }
                    }

                    for (var i = 0; i < $scope.SelectedFeeObject.length; i++) {
                        if ($scope.SelectedFeeObject[i].Payment_mode == 'Cash' || $scope.SelectedFeeObject[i].Payment_mode == 'Bank Transfer') {
                            $scope.SelectedFeeObject[i].Cheque_NO = 'N/A';
                        }

                        if ($scope.SelectedFeeObject[i].primary_no == $scope.AddnewPaymentdetails[0].primary_no) {
                            $scope.SelectedFeeObject[i].std_fee_child_paying_amount_temp = parseFloat(mainamount) - parseFloat($scope.AddnewPaymentdetails[0].std_fee_child_paying_amount_temp3);
                            break;
                        }
                    }
                }
                $scope.btn_Adddiscount_click();

            }

            $scope.RemoveSelectedPaymentMode_new = function ($event, index, str) {

                str.splice(index, 1);
                $scope.edt1 = {
                    std_fee_child_paying_amount_temp2: $scope.edt.std_fee_child_paying_amount_temp,
                    std_fee_child_paying_amount_temp1: $scope.edt.std_fee_child_paying_amount_temp
                };


            }

            /*************************************************Add Other Fees Code****************************************************/

            $scope.btn_otherFeeAdd_Click = function () {

                var flag = false;
                var Bank_name = '';
                var t = document.getElementById('cmb_std_fee_discount').value;
                for (var i = 0; i < $scope.SelectedOtherfeeObject.length; i++) {
                    if (t == $scope.SelectedOtherfeeObject[i].std_fee_type) {
                        flag = true;
                    }
                }
                if (t != undefined && t != '' && $scope.other_fee_amount != undefined && $scope.other_fee_amount != '') {
                    if (flag != true) {
                        count3 = count3 + 1;
                        $scope.SelectedOtherfee = true;

                        var Cheque_NO = '';
                        if ($scope.paymentmode == 'Cash' || $scope.paymentmode == 'Bank Transfer') {
                            Cheque_NO = 'N/A';
                            Bank_name = 'N/A';
                        }
                        $scope.addfee = true;
                        SelectedAmount = parseFloat(SelectedAmount) + parseFloat($scope.other_fee_amount);
                        $scope.grand_total_amount = parseFloat(SelectedAmount).toFixed(2);

                        var countamount = 0;
                        countamount = parseFloat(countamount) + parseFloat($scope.other_fee_amount);
                        $scope.other_fee_amount1 = countamount;
                        var selectedData = {};
                        selectedData.primary_no = count3
                        selectedData.Payment_mode = $scope.paymentmode;
                        selectedData.Cheque_NO = 'N/A';
                        selectedData.Bank_name = 'select name';
                        selectedData.Cheque_DD_Date = $scope.payment_date;
                        selectedData.Transaction_id = 'N/A';
                        selectedData.Amount = $scope.other_fee_amount;
                        selectedData.Chk_amount = '0';
                        selectedData.std_fee_type = $scope.std_fee_discount_type;
                        selectedData.feeTypeGroup = '';
                        selectedData.std_fee_Period = '';
                        selectedData.sims_fee_collection_mode = 'M';
                        //selectedData.isOtherFee = true;
                        $scope.SelectedPaymentModeObject.push(selectedData);
                        $scope.grand_total_amount1 = parseFloat($scope.grand_total_amount).toFixed(2);
                        var data = {};
                        data.primary_no = count3;
                        data.std_fee_enroll_number = $scope.GetStudentRefundFeeNumberDetails[0].std_fee_enroll_number;
                        data.std_fee_type = $scope.std_fee_discount_type;
                        data.Payment_mode = $scope.paymentmode;
                        data.Cheque_NO = Cheque_NO;
                        data.std_fee_child_paying_amount_temp = $scope.other_fee_amount;
                        data.std_fee_other_paying_amount = 0;
                        data.Cheque_DD_Date = $scope.payment_date;
                        data.Bank_name = Bank_name;
                        data.sims_fee_collection_mode = 'M';
                        //data.isOtherFee = true;
                        data.std_fee_Period = '';
                        $scope.SelectedFeeObject.push(data);
                        $scope.SelectedOtherfeeObject.push(data);
                    }
                    else {
                        swal({ text: 'This Fee Type Is Already Used Select Another Fee To Insert', width: 380, showCloseButton: true });
                    }


                }
                else {
                    swal({ text: 'Fee Type Is Empty Or Other Fee Amount Is Empty', width: 380, showCloseButton: true });
                }
                $scope.other_fee_amount = '0.00';
                $scope.std_fee_discount_type = '';
                $scope.btn_Adddiscount_click();
            }

            $scope.RemoveSelectedOtherfeeObject = function ($event, index, str, info) {

                var countamount = '';
                countamount = parseFloat(countamount) - parseFloat(info.std_fee_child_paying_amount_temp);
                $scope.other_fee_amount1 = countamount;

                count3 = count3 - 1;

                $scope.other = '';
                str.splice(index, 1);

                for (var i = 0; i < $scope.SelectedPaymentModeObject.length; i++) {
                    if ($scope.SelectedPaymentModeObject[i].primary_no == info.primary_no) {
                        $scope.SelectedPaymentModeObject.splice(i, 1);
                    }
                }

                for (var j = 0; j < $scope.SelectedFeeObject.length; j++) {
                    if ($scope.SelectedFeeObject[j].primary_no == info.primary_no) {
                        $scope.SelectedFeeObject.splice(j, 1);
                    }
                }

                if ($scope.SelectedOtherfeeObject.length <= 0) {
                    $scope.SelectedOtherfee = false;

                }

                if ($scope.SelectedPaymentModeObject.length <= 0) {
                    $scope.SelectedOtherfee = false;
                    $scope.addfee = false;
                }

                SelectedAmount = parseFloat(SelectedAmount) - parseFloat(info.std_fee_child_paying_amount_temp);
                $scope.grand_total_amount = parseFloat(SelectedAmount).toFixed(2);

                var flag = false;
                for (var i = 0; i < $scope.GetStudentRefundFeeNumberDetails.length; i++) {
                    var v = document.getElementById($scope.GetStudentRefundFeeNumberDetails[i].feeTypeGroup);
                    if (v.checked == true) {
                        flag = true;
                    }
                }

                var check = document.getElementById('mainchk');

                if (str.length <= 0 && check.checked != true && flag != true) {
                    $scope.addfee = false;
                    $scope.SelectedFeeObject = [];
                    $scope.SelectedPaymentModeObject = [];

                    $scope.grand_total_amount = '0.00';
                }
                $scope.other_fee_amount1 = '0.00';
                $scope.btn_Adddiscount_click();
            }

            /*************************************************Set Cheque DD No****************************************************/
            $scope.SetChequeNO_DD_No = function (str) {

                for (var i = 0; i < $scope.SelectedFeeObject.length; i++) {
                    if (str.Payment_mode == $scope.SelectedFeeObject[i].Payment_mode && str.std_fee_type == $scope.SelectedFeeObject[i].std_fee_type) {
                        $scope.SelectedFeeObject[i].Cheque_NO = str.Cheque_NO;
                    }
                }
            }

            $scope.SetTransactionId = function (str) {


                for (var i = 0; i < $scope.SelectedFeeObject.length; i++) {
                    if (str.Payment_mode == 'Bank Transfer' && str.std_fee_type == $scope.SelectedFeeObject[i].std_fee_type) {

                        $scope.SelectedFeeObject[i].Transaction_id = str.Transaction_id;
                    }
                    else {
                        for (var i = 0; i < $scope.SelectedFeeObject.length; i++) {
                            if (str.Payment_mode == $scope.SelectedFeeObject[i].Payment_mode && str.std_fee_type == $scope.SelectedFeeObject[i].std_fee_type && str.Payment_mode != 'Bank Transfer') {
                                $scope.SelectedFeeObject[i].Cheque_NO = str.Cheque_NO;
                            }
                        }

                    }
                }

            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent('#MyModal')).focus();
            });

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.GetAllStudentFee, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.GetAllStudentFee;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.std_fee_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.std_fee_student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.btn_StudentRefund_Click = function () {

                $scope.collection_type = $scope.SelectedPaymentModeObject[0].sims_fee_collection_mode;

                var datasend = [];


                if ($scope.SelectedFeeObject.length != 0) {

                    if ($scope.grand_total_amount1 == undefined) {
                        $scope.grand_total_amount1 = 0.00;
                    }
                    if ($scope.doc_discount_amount == undefined) {
                        $scope.doc_discount_amount = 0.00;
                    }
                    if ($scope.doc_narration == undefined) {
                        $scope.doc_narration = null;
                    }

                    var data = {};
                    var datasendobject = [];
                    data.doc_date = $scope.payment_date;
                    data.doc_total_amount = $scope.grand_total_amount;
                    data.doc_enroll_no = $scope.Student_Enroll;
                    data.doc_other_charge_desc = 0;
                    data.doc_other_charge_amount = $scope.grand_total_amount1;
                    data.doc_discount_pct = null;
                    data.doc_discount_amount = $scope.doc_discount_amount
                    data.doc_narration = $scope.doc_narration;
                    data.std_fee_student_name = user;
                    data.std_fee_academic_year = $scope.sims_academic_year;

                    $http.post(ENV.apiUrl + "api/studentfeerefund/RefundInsertToFee_Document", data).then(function (RefundInsertToFee_Document) {
                        $scope.RefundInsertToFee_Document = RefundInsertToFee_Document.data;

                        if ($scope.RefundInsertToFee_Document != '') {

                            for (var i = 0; i < $scope.SelectedFeeObject.length; i++) {
                                for (var j = 0; j < $scope.SelectedPaymentModeObject.length; j++) {

                                    if ($scope.SelectedPaymentModeObject[j].primary_no == $scope.SelectedFeeObject[i].primary_no && $scope.SelectedPaymentModeObject[j].Payment_mode == $scope.SelectedFeeObject[i].Payment_mode) {
                                        var data1 = {};

                                        data1.doc_no = $scope.RefundInsertToFee_Document;
                                        data1.dd_fee_number = $scope.SelectedFeeObject[i].std_fee_number;
                                        data1.PayFeeCode = $scope.SelectedFeeObject[i].std_fee_code;
                                        data1.FeeEnrollNo = $scope.Student_Enroll;
                                        data1.PayDisPeriodNo = $scope.SelectedFeeObject[i].std_fee_child_period_No;
                                        data1.PayAmount = $scope.SelectedFeeObject[i].std_fee_child_paying_amount_temp;
                                        data1.PayDisAmount = $scope.SelectedFeeObject[i].std_fee_concession_amount;
                                        data1.paymentMode = $scope.SelectedFeeObject[i].Payment_mode;
                                        data1.chequeDDNo = $scope.SelectedFeeObject[i].Cheque_NO;
                                        if ($scope.SelectedPaymentModeObject[j].Cheque_DD_Date == 'NaN/NaN/NaN' || $scope.SelectedPaymentModeObject[j].Cheque_DD_Date == 'N/A') {
                                            data1.chequeDDDate = 'N/A';
                                        }
                                        else {
                                            data1.chequeDDDate = $scope.SelectedPaymentModeObject[j].Cheque_DD_Date;

                                        }
                                        data1.BankName = $scope.SelectedPaymentModeObject[j].Bank_name;
                                        data1.TransactionID = $scope.SelectedFeeObject[i].Transaction_id;
                                        data1.std_fee_cur_code = $scope.sims_cur_code;
                                        data1.std_fee_academic_year = $scope.sims_academic_year;
                                        data1.std_fee_grade_code = $scope.sims_grade_code;
                                        data1.std_fee_section_code = $scope.sims_section_name;
                                        data1.sims_fee_collection_mode = $scope.SelectedFeeObject[i].sims_fee_collection_mode;

                                        data1.slma_ldgrctl_code = null;
                                        data1.slma_acno = null;
                                        data1.slma_ldgrctl_year = null;
                                        data1.isOtherFee = false;

                                        data1.doc_paying_agent_transaction_lineno = null;
                                        data1.doc_paying_agent_transaction_no = null;
                                        data1.dd_fee_credit_card_code = null;
                                        data1.dd_fee_credit_card_auth_code = null;

                                        if (data1.paymentMode == 'Demand Draft' || data1.paymentMode == 'Cheque') {
                                            data1.TransactionID = 'N/A';
                                        }

                                        else if (data1.paymentMode == 'Bank Transfer') {
                                            data1.chequeDDNo = 'N/A';
                                        }

                                        else if (data1.paymentMode == 'Cash') {
                                            data1.chequeDDNo = 'N/A';
                                            data1.TransactionID = 'N/A';
                                            data1.chequeDDDate = 'N/A';
                                        }
                                        else if (data1.paymentMode == 'Agent') {
                                            data1.chequeDDNo = 'N/A';
                                            data1.TransactionID = 'N/A';
                                            data1.doc_paying_agent_transaction_lineno = i;
                                            data1.doc_paying_agent_transaction_no = $scope.SelectedPaymentModeObject[j].Cheque_NO;
                                        }
                                        else if (data1.paymentMode == 'Credit Card') {
                                            data1.chequeDDNo = 'N/A';
                                            data1.TransactionID = 'N/A';
                                            data1.dd_fee_credit_card_code = $scope.SelectedFeeObject[i].Cheque_NO;
                                            data1.dd_fee_credit_card_auth_code = $scope.SelectedPaymentModeObject[j].Transaction_id;
                                        }
                                        if ($scope.temp.strExcess == true) {
                                            data1.isecxess_fee = true;
                                            data1.paymentMode = "AD";
                                        }
                                        else {
                                            data1.isecxess_fee = false;
                                        }

                                        if ($scope.SelectedFeeObject[i].discount_flag = true) {
                                            data1.PayDisAmount = $scope.doc_discount_amount;
                                        }

                                        datasend.push(data1)
                                    }
                                }
                            }

                            $http.post(ENV.apiUrl + "api/studentfeerefund/RefundInsertToFee_Document_Details", datasend).then(function (RefundInsertToFee_Document_Details) {
                                $scope.RefundInsertToFee_Document_Details = RefundInsertToFee_Document_Details.data;

                                datasend = [];
                                if ($scope.RefundInsertToFee_Document_Details == true) {

                                    $scope.SelectedFeeObject = [];
                                    $scope.SelectedPaymentModeObject = [];
                                    $scope.GetStudentRefundFeeNumberDetails = [];
                                    $scope.SelectedDiscountfeeObject = [];

                                    $scope.addfee = false;
                                    $scope.SelectedOtherfee = false;
                                    $scope.discounttable = false;
                                    $scope.grid = true;
                                    $scope.grand_total_amount1 = '0.00';
                                    $scope.grand_total_amount = '0.00';
                                    $scope.temp = { fee_concession_number: '0' };
                                    $scope.other_fee_amount1 = '0.00';
                                    $scope.grand_total_amount = '0.00';
                                    $scope.other_fee_amount = '0.00';
                                    $scope.other_fee_amount1 = '0.00';
                                    $scope.doc_discount_amount = '0.00';
                                    $scope.doc_discount_amount1 = '0.00';
                                    $scope.fee_concession_percentage = '0.00';
                                    SelectedAmount = '0.00'

                                    $scope.Show_Data();
                                    swal({ text: 'Fee Refund  Successfully', width: 350, showCloseButton: true });
                                }
                                else {

                                    swal({ text: 'Fee Not Refund', width: 310, showCloseButton: true });
                                }
                            })
                        }
                        else {
                            swal({ text: 'Recoed Not Inserted In Fee Document', width: 380, showCloseButton: true });
                        }
                    });
                }
                else {

                    swal({ text: 'Select At Least One Fee To Refund', width: 380, showCloseButton: true });
                }

            }

            $scope.btn_Adddiscount_click = function () {

                $scope.grand_total_amount1 = $scope.grand_total_amount = Math.round($scope.grand_total_amount).toFixed(2);

                $scope.SelectedDiscountfeeObject = [];
                if ($scope.fee_concession_percentage > 0) {
                    var concession_type = '';
                    var Discount_amount = 0.00;
                    var Discount_amount1 = 0.00;
                    var concession_type_desc = '';
                    for (var j = 0; j < $scope.AllGetConcessionFee.length; j++) {
                        if ($scope.AllGetConcessionFee[j].fee_concession_number == $scope.temp.fee_concession_number)
                            concession_type = $scope.AllGetConcessionFee[j].sims_concession_discount_type;
                        concession_type_desc = $scope.AllGetConcessionFee[j].fee_concession_description;
                    }

                    if (concession_type == 'P') {

                        for (var i = 0; i < $scope.SelectedFeeObject.length; i++) {
                            var mainamount_per = $scope.SelectedFeeObject[i].std_fee_child_paying_amount_temp;
                            var percenatge_amount = parseFloat($scope.fee_concession_percentage / 100) * parseFloat($scope.SelectedFeeObject[i].std_fee_child_paying_amount_temp);
                            $scope.SelectedFeeObject[i].std_fee_other_paying_amount = Math.round(percenatge_amount).toFixed(2);
                            Discount_amount1 = Math.round(parseFloat(Discount_amount1) + parseFloat($scope.SelectedFeeObject[i].std_fee_other_paying_amount));

                            $scope.doc_discount_amount = Math.round(Discount_amount1).toFixed(2);
                            $scope.grand_total_amount = Math.round(($scope.grand_total_amount) + parseFloat(mainamount_per)).toFixed(2);
                            $scope.grand_total_amount1 = Math.round(parseFloat($scope.grand_total_amount) - (parseFloat($scope.doc_discount_amount))).toFixed(2);

                            $scope.SelectedFeeObject[i].discount_flag = true;
                        }

                        for (var i = 0; i < $scope.SelectedPaymentModeObject.length; i++) {
                            var mainamount_per = $scope.SelectedPaymentModeObject[i].Amount;
                            var percenatge_amount = parseFloat($scope.fee_concession_percentage / 100) * parseFloat($scope.SelectedPaymentModeObject[i].Amount);
                            $scope.SelectedPaymentModeObject[i].Chk_amount = Math.round(percenatge_amount).toFixed(2);
                            $scope.SelectedPaymentModeObject[i].discount_flag = true;
                        }


                    }
                    else {

                        for (var i = 0; i < $scope.SelectedFeeObject.length; i++) {
                            var mainamount_per = $scope.SelectedFeeObject[i].std_fee_child_paying_amount_temp;
                            $scope.SelectedFeeObject[i].std_fee_other_paying_amount = parseFloat($scope.SelectedFeeObject[i].std_fee_other_paying_amount) - parseFloat($scope.fee_concession_percentage)
                            Discount_amount1 = Math.round(parseFloat(Discount_amount1) + parseFloat($scope.SelectedFeeObject[i].std_fee_other_paying_amount));
                            $scope.SelectedFeeObject[i].discount_flag = true;

                            $scope.doc_discount_amount = Math.round(Discount_amount1).toFixed(2);
                            $scope.grand_total_amount = Math.round(($scope.grand_total_amount) + parseFloat(mainamount_per)).toFixed(2);
                            $scope.grand_total_amount1 = Math.round(parseFloat($scope.grand_total_amount) - (parseFloat($scope.doc_discount_amount))).toFixed(2);
                        }
                        for (var i = 0; i < $scope.SelectedPaymentModeObject.length; i++) {
                            $scope.SelectedPaymentModeObject[i].Chk_amount = parseFloat($scope.SelectedPaymentModeObject[i].std_fee_other_paying_amount) - parseFloat($scope.fee_concession_percentage)
                            $scope.doc_discount_amount = Math.round($scope.SelectedPaymentModeObject[i].Chk_amount);
                            $scope.SelectedPaymentModeObject[i].discount_flag = true;
                        }
                    }

                    $scope.discounttable = true;
                    var selecteddiscount = {};
                    selecteddiscount.concession_type_desc = concession_type_desc;
                    selecteddiscount.Discount_amount_for_table = $scope.fee_concession_percentage;
                    $scope.SelectedDiscountfeeObject.push(selecteddiscount);
                }

            }

            $scope.getConcession_amount = function () {


                if ($scope.temp.fee_concession_number == '0') {
                    $scope.fee_concession_percentage = '0.00';
                }
                else {
                    for (var j = 0; j < $scope.AllGetConcessionFee.length; j++) {
                        if ($scope.AllGetConcessionFee[j].fee_concession_number == $scope.temp.fee_concession_number)
                            $scope.fee_concession_percentage = $scope.AllGetConcessionFee[j].sims_concession_discount_value;
                    }
                }

            }

            $scope.SetDiscountValue = function () {

                if ($scope.fee_concession_percentage > 100) {
                    $scope.fee_concession_percentage = '';
                }

            }

            $scope.RemoveSelectedDiscountfeeObject = function ($event, index, str, info) {



                str.splice(index, 1);

                for (var i = 0; i < $scope.SelectedFeeObject.length; i++) {
                    $scope.SelectedFeeObject[i].std_fee_other_paying_amount = '0.00';
                    $scope.SelectedFeeObject[i].discount_flag = false;
                }

                for (var i = 0; i < $scope.SelectedPaymentModeObject.length; i++) {
                    $scope.grand_total_amount1 = '0.00';
                    $scope.SelectedPaymentModeObject[i].Chk_amount = '0.00';
                    $scope.SelectedPaymentModeObject[i].discount_flag = false;
                    $scope.grand_total_amount1 = parseFloat($scope.grand_total_amount).toFixed(2);

                }

                $scope.doc_discount_amount = '0.00';
                $scope.temp.fee_concession_number = '0';
                $scope.fee_concession_percentage = '0.00';
                $scope.discounttable = false;
                $scope.SelectedDiscountfeeObject = [];
            }
        }])
})();
