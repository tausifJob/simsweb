﻿(function () {
    'use strict';
    var main;
    var prdocdata = [];
    var data1 = [];
    var data = [];
    var doc_no;
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('FeeReceiptCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {
            $scope.pagesize = '5';
            $scope.pager = true;
            $scope.pagggg = false;
            $scope.feeReceiptTable = true;
            $scope.showEditForm = false;
            $scope.Update_btn = false;
            $scope.d = {};
            $scope.dd = [];
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/StudentFee/GetPaymentMode").then(function (res) {
                $scope.PaymentModes = res.data;
            });


            $scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.edt = { from_date: $scope.ddMMyyyy, to_date: $scope.ddMMyyyy, }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.size = function (str) {

                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.FeeReceiptData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.createdate = function (date) {

                if ($scope.edt.from_date != '') {
                    if ($scope.edt.from_date > $scope.edt.to_date) {
                        swal({ title: 'Please Select future Date', width: 380, height: 100 });
                        $scope.edt.to_date = '';
                    }
                }
            }

            $scope.paginationview1 = document.getElementById("paginationview");

            $scope.gotomain = function () {
                $scope.feeReceiptTable = true;
                $scope.showEditForm = false;
                $scope.Update_btn = false;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.Show = function (from_date, to_date, search) {
                debugger
                if (from_date == undefined || from_date == "") {
                    from_date = '';
                }
                else {
                    var fdate = from_date.split("-")[0];
                    var fmoth = from_date.split("-")[1];
                    var fyear = from_date.split("-")[2];

                }
                if ($scope.edt.search == undefined || $scope.edt.search == "") {
                    $scope.edt.search = '';
                }



                var tdate = to_date.split("-")[0];
                var tmoth = to_date.split("-")[1];
                var tyear = to_date.split("-")[2];
                from_date = fyear + "-" + fmoth + "-" + fdate;
                to_date = tyear + "-" + tmoth + "-" + tdate;
                //if ($scope.edt.from_date == undefined || $scope.edt.from_date == "") {
                //    $scope.flag1 = true;
                //    swal({ title: "Alert", text: "Please select From Date", showCloseButton: true, width: 380, });

                //}
                //else if ($scope.edt.to_date == undefined || $scope.edt.to_date == "") {
                //    $scope.flag1 = true;
                //    swal({ title: "Alert", text: "Please select To Date", showCloseButton: true, width: 380, });
                //}
                //else {

                if ($scope.edt.search == undefined || $scope.edt.search == "") {
                    $scope.edt.search = '';
                }
                $scope.table1 = true;
                $scope.pagggg = true;
                $scope.ImageView = false;
                $scope.filteredTodos = [];
                $http.get(ENV.apiUrl + "api/FeeReceipt/getAllRecordsFeeReceipt?from_date=" + $scope.edt.from_date + "&to_date=" + $scope.edt.to_date + "&search=" + $scope.edt.search).then(function (FeeReceipt_Data) {
                    $scope.FeeReceiptData = FeeReceipt_Data.data;
                    $scope.totalItems = $scope.FeeReceiptData.length;
                    $scope.todos = $scope.FeeReceiptData;
                    $scope.makeTodos();
                    console.log($scope.FeeReceiptData);
                    if (FeeReceipt_Data.data.length > 0) { }
                    else {
                        $scope.ImageView = true;
                        $scope.pager = false;
                    }
                });
                //}
            }

            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#searchbyallfield").attr("disabled", "disabled");
                        $scope.edt.search = '';
                    } else {

                        $("#searchbyallfield").removeAttr("disabled");
                        $("#searchbyallfield").focus();
                    }
                });
            });

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.edt["from_date"] = '';
                        $scope.startDate = ''
                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.edt = {
                            from_date: dd + '-' + mm + '-' + yyyy,
                            to_date: dd + '-' + mm + '-' + yyyy,
                        }

                    }
                });
            });

            $scope.Report = function (str) {
                $http.get(ENV.apiUrl + "api/StudentFee/GetReceipt").then(function (res) {
                    $scope.reportparameter = res.data;
                    var data = {
                        location: res.data,
                        parameter: {
                            fee_rec_no: str.doc_no,
                        },
                        state: 'main.Sim615',
                        ready: function () {
                            this.refreshReport();
                        },
                    }
                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                    $state.go('main.ReportCardParameter')
                });

            }

            $scope.edit = function (obj) {
                var temp = {};
                $scope.feeReceiptTable = false;;
                $scope.showEditForm = true;
                $scope.Update_btn = true;
                console.log(obj);
                temp.doc_no = obj.doc_no;
                $scope.d = {};
                $scope.dd = [];
                temp.doc_academic_year = obj.doc_academic_year;
                $http.post(ENV.apiUrl + "api/FeeReceipt/DocDetailsGet", temp).then(function (res) {
                    $scope.d = res.data.table[0];
                    $scope.dd = res.data.table1;
                    console.log($scope.dd);
                });
            }

            $scope.Cancel = function () {
                $scope.feeReceiptTable = true;;
                $scope.showEditForm = false;
                $scope.Update_btn = false;
            }

            $scope.update = function () {
                function ud() {
                    var ob = {
                        val: 0,
                        cur_code: 0,
                        academic_year: '',
                        doc_no: '',
                        doc_line_no: '',
                        fee_num: '',
                        p_code: '',
                        pm: '',
                        ch_no: '',
                        ch_date: '',
                        ch_rem: '',
                        bk: '',
                        enroll: ''
                    }
                    return ob;
                }
                var temp = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.dd.length; i++) {
                    var o = ud();
                    o.val = r;
                    o.doc_no = $scope.dd[i].doc_no;
                    o.doc_line_no = $scope.dd[i].dd_line_no;
                    o.academic_year = $scope.dd[i].doc_academic_year;
                    o.fee_num = $scope.dd[i].dd_fee_number;
                    o.p_code = $scope.dd[i].dd_fee_period_code;
                    o.pm = $scope.dd[i].dd_fee_payment_mode;
                    o.ch_no = $scope.dd[i].dd_fee_cheque_number;
                    o.ch_date = $scope.dd[i].dd_fee_cheque_date;
                    o.ch_rem = $scope.d.doc_narration;
                    o.enroll = $scope.dd[i].enroll_number;
                    temp.push(o);
                }
                if (temp.length == 0) {
                    alert("Nothing Selected");
                    return;
                }

                $http.post(ENV.apiUrl + "api/FeeReceipt/UpdateDoc", temp).then(function (res) {
                    console.log(res);
                    $scope.gotomain();
                    swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });
                });
            }
        }]
        )
})();