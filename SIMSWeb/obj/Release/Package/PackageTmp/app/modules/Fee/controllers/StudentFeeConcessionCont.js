﻿(function () {
    'use strict';
    var del = [], feecodes = [], All_Fee_codesMul = [];
    var main, temp, opr;
    var simsController = angular.module('sims.module.Fee');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StudentFeeConcessionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            //$scope.display = false; $scope.table = true; $scope.operation = false;
            $scope.pagesize = "5";
            $scope.readonlydiscount = true;
            $scope.dis = true;
            $scope.operation = false;
            data();

            function data() {

                $http.get(ENV.apiUrl + "api/common/Concession/getAllSimsConcession").then(function (res) {
                    $scope.display = false;
                    $scope.obj = res.data;
                    if ($scope.obj.length != 0) {
                        $scope.table = true;
                        $scope.display = true;
                        $scope.operation = false;
                        $scope.totalItems = $scope.obj.length;
                        $scope.todos = $scope.obj;
                        $scope.makeTodos();

                    }
                    else {
                        $scope.table = true;
                        $scope.display = false;
                        $scope.operation = false;
                        swal({ text: 'Sorry Data Not Found', width: 380, showCloseButton: true });
                    }


                });
            }

            $http.get(ENV.apiUrl + "api/common/Concession/getAcadamicYear").then(function (res) {
                $scope.Acdm_Year = res.data;
            });

            $scope.someMethod = function () {
                $http.get(ENV.apiUrl + "api/common/Concession/getAllFeeCode").then(function (res) {
                    $scope.All_Fee_codes = res.data;
                });
            }

            $http.get(ENV.apiUrl + "api/common/Concession/getConcessionDiscountType1").then(function (res) {
                $scope.concession_discount_type = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/Concession/getConcessionApplicableOn").then(function (res) {
                $scope.concession_applicable_on = res.data;

            });

            $http.get(ENV.apiUrl + "api/common/Concession/getConcessionApplicableTo").then(function (res) {
                $scope.concession_applicable_to = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/Concession/getConcessionType").then(function (res) {
                $scope.concession_types = res.data;
            });


            $scope.selectedUserIds = [];

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.checkAll();
                }
                main.checked = false;
                $scope.checkAll();
            };

            $scope.checkAll = function () {

                main = document.getElementById('mainchk');

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_concession_number + i;
                    var v = document.getElementById(t);
                    if (main.checked == true) {
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                    else {
                        v.checked = false;
                        $scope.row1 = ''
                        $('tr').removeClass("row_selected");
                    }

                }
            }

            $scope.checkAny = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }

            }

            $scope.size = function (str) {

                if (str == 'All') {

                    $scope.filteredTodos = $scope.obj;
                    $scope.numPerPage = $scope.obj.length,
                    $scope.maxSize = $scope.obj.length;
                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.makeTodos();
                }
                else {


                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();

               
            }

            $scope.Cancel = function () {
                $scope.table = true;
                $scope.temp = "";
                $scope.readonly = false;
                $scope.operation = false;

            }

            $scope.edit = function (str) {

                $scope.readonly1 = true;
                $scope.fee_code = '';
                $scope.updisabled = true;
                $scope.readonlydiscount = false;

                $scope.table = false;
                $scope.operation = true;
                $scope.readonly = false;
                $scope.Save_btn = false;
                $scope.Update_btn = true;

                var data = angular.copy(str);
                $scope.temp = data;
                $scope.opr = 'U';


                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }
                $scope.checkAll();
                $http.get(ENV.apiUrl + "api/common/Concession/getcheckSimsConcession?concession_number=" + $scope.temp.sims_concession_number).then(function (res) {
                    $scope.CUDobj = res.data;
                    $scope.readonlydiscount = $scope.CUDobj;

                });

                $scope.All_Fee_codes = [];
                $scope.All_Fee_codes1 = [];


                if (str.sims_concession_applicable_on == 'Regular Fees') {

                    
                    $scope.chk_status = [];
                    $http.get(ENV.apiUrl + "api/common/Concession/getcheckstatus?conce_no=" + str.sims_concession_number).then(function (res) {
                        $scope.chk_status = res.data;
                        if ($scope.chk_status[0].sims_status == false) {
                            
                            $http.get(ENV.apiUrl + "api/common/Concession/getAllFeeCode").then(function (res) {
                                $scope.All_Fee_codes1 = res.data;

                                for (var i = 0; i < $scope.obj.length; i++) {
                                    for (var j = 0; j < $scope.All_Fee_codes1.length; j++) {
                                        for (var k = 0; k < $scope.obj[i].list.length; k++) {
                                            if (str.sims_concession_number == $scope.obj[i].sims_concession_number && $scope.All_Fee_codes1[j].sims_concession_fee_code == $scope.obj[i].list[k].sims_concession_fee_code1) {
                                                $scope.All_Fee_codes1[j].status = true;
                                                $scope.All_Fee_codes1[j].disabled1 = false;
                                            }
                                        }
                                    }
                                }

                                $scope.All_Fee_codes = $scope.All_Fee_codes1;
                            });
                        }
                        else {
                            $http.get(ENV.apiUrl + "api/common/Concession/getAllFeeCode").then(function (res) {
                                $scope.All_Fee_codes1 = res.data;
                                for (var i = 0; i < $scope.obj.length; i++) {
                                    for (var j = 0; j < $scope.All_Fee_codes1.length; j++) {
                                        for (var k = 0; k < $scope.obj[i].list.length; k++) {
                                            if (str.sims_concession_number == $scope.obj[i].sims_concession_number && $scope.All_Fee_codes1[j].sims_concession_fee_code == $scope.obj[i].list[k].sims_concession_fee_code1) {
                                                $scope.All_Fee_codes1[j].status = true;
                                                $scope.All_Fee_codes1[j].disabled1 = true;
                                            }
                                        }
                                    }
                                }

                                $scope.All_Fee_codes = $scope.All_Fee_codes1;
                            });
                        }

                    });
                }

            }

            $scope.Delete = function () {

                var sims_concession_number = '';
                var simsconcession_fee_code = '';
                var datasend = [];
                var data1 = '';
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_concession_number + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        data1 = {
                            sims_concession_number: $scope.filteredTodos[i].sims_concession_number,
                            sims_concession_fee_code: $scope.filteredTodos[i].sims_concession_fee_code
                        }
                        datasend.push(data1);
                    }
                }

                if ($scope.flag) {
                    swal({
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/common/Concession/SimsConcessionDelete", datasend).then(function (res) {
                                $scope.CUDobj = res.data;
                                swal({ text: $scope.CUDobj.strMessage, width: 380, showCloseButton: true });
                                $scope.currentPage = 1;
                                data();

                            })
                        }
                        else {

                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                                $scope.row1 = '';
                            }
                            $scope.checkAll();
                        }
                    })
                }
                else {
                    swal({ text: 'Select At Least One Record To Delete', width: 380, showCloseButton: true });
                }
            }

            $scope.New = function () {
                $scope.updisabled = false;
                $scope.tenantForm.$setPristine();
                $scope.fee_code = '';
                $http.get(ENV.apiUrl + "api/common/Concession/getComnSequenceCode").then(function (res) {
                    //   $scope.sendobject = { sims_concession_number: res.data }
                    $scope.Com_seq_code = res.data;
                    $scope.temp = { sims_concession_number: $scope.Com_seq_code, sims_concession_status: true };
                });

                $scope.table = false;
                $scope.operation = true;
                $scope.Save_btn = true;
                $scope.Update_btn = false;
                $scope.opr = 'I';
                $scope.readonly = true;
                $scope.readonlydiscount = true;
                $scope.readonly1 = true;

                $scope.CUDobj = false;


            }

            $scope.selectfeecodes = function (fee_Codes) {
                $scope.studfeecode = fee_Codes;
            }

            $scope.SaveData = function (isvalid) {
                var status = true;

                if (isvalid) {
                    var datasend = [];
                    if ($scope.opr == 'I') {

                        if ($scope.temp.sims_concession_applicable_on != 'Total Fees') {
                            for (var i = 0; i < $scope.All_Fee_codes.length; i++) {
                                var d = document.getElementById($scope.All_Fee_codes[i].sims_concession_fee_code)
                                if (d.checked == true) {
                                    $scope.sendobject = {
                                        opr: $scope.opr = 'I',
                                        sims_concession_number: $scope.Com_seq_code,
                                        sims_concession_description: $scope.temp.sims_concession_description,
                                        sims_concession_type: $scope.temp.sims_concession_type,
                                        sims_concession_discount_type: $scope.temp.sims_concession_discount_type,
                                        sims_concession_discount_value: $scope.temp.sims_concession_discount_value,
                                        sims_concession_applicable_on: $scope.temp.sims_concession_applicable_on,
                                        sims_concession_corporate_billing: $scope.temp.sims_concession_corporate_billing,
                                        sims_concession_academic_year: $scope.temp.sims_concession_academic_year,
                                        sims_concession_applicable_to: $scope.temp.sims_concession_applicable_to,
                                        sims_concession_onward_child: $scope.temp.sims_concession_onward_child,
                                        sims_concession_status: $scope.temp.sims_concession_status,
                                        sims_concession_fee_code: $scope.All_Fee_codes[i].sims_concession_fee_code
                                    }
                                    datasend.push($scope.sendobject);
                                }
                            }
                        }

                        else {
                            $scope.sendobject = {
                                opr: $scope.opr = 'I',
                                sims_concession_number: $scope.Com_seq_code,
                                sims_concession_description: $scope.temp.sims_concession_description,
                                sims_concession_type: $scope.temp.sims_concession_type,
                                sims_concession_discount_type: $scope.temp.sims_concession_discount_type,
                                sims_concession_discount_value: $scope.temp.sims_concession_discount_value,
                                sims_concession_applicable_on: $scope.temp.sims_concession_applicable_on,
                                sims_concession_corporate_billing: $scope.temp.sims_concession_corporate_billing,
                                sims_concession_academic_year: $scope.temp.sims_concession_academic_year,
                                sims_concession_applicable_to: $scope.temp.sims_concession_applicable_to,
                                sims_concession_onward_child: $scope.temp.sims_concession_onward_child,
                                sims_concession_status: $scope.temp.sims_concession_status
                            }
                            datasend.push($scope.sendobject);
                        }
                    }
                    if ($scope.opr == 'U') {


                        debugger;

                        if ($scope.temp.sims_concession_applicable_on != 'Total Fees') {
                            for (var i = 0; i < $scope.All_Fee_codes.length; i++) {
                                if ($scope.All_Fee_codes[i].status == false) {
                                    status = false;
                                }
                            }



                            if (status == true) {
                                for (var i = 0; i < $scope.All_Fee_codes.length; i++) {
                                    var d = document.getElementById($scope.All_Fee_codes[i].sims_concession_fee_code)
                                    if ($scope.All_Fee_codes[i].status == true) {
                                        $scope.sendobject = {
                                            opr: $scope.opr = 'U',
                                            sims_concession_number: $scope.temp.sims_concession_number,
                                            sims_concession_description: $scope.temp.sims_concession_description,
                                            sims_concession_type: $scope.temp.sims_concession_type,
                                            sims_concession_discount_type: $scope.temp.sims_concession_discount_type,
                                            sims_concession_discount_value: $scope.temp.sims_concession_discount_value,
                                            sims_concession_applicable_on: $scope.temp.sims_concession_applicable_on,
                                            sims_concession_corporate_billing: $scope.temp.sims_concession_corporate_billing,
                                            sims_concession_academic_year: $scope.temp.sims_concession_academic_year,
                                            sims_concession_applicable_to: $scope.temp.sims_concession_applicable_to,
                                            sims_concession_onward_child: $scope.temp.sims_concession_onward_child,
                                            sims_concession_status: $scope.temp.sims_concession_status,
                                            sims_concession_fee_code: $scope.All_Fee_codes[i].sims_concession_fee_code
                                        }
                                        datasend.push($scope.sendobject);
                                    }
                                }
                            }
                            else {

                                debugger;

                                for (var i = 0; i < $scope.All_Fee_codes.length; i++) {
                                    var d = document.getElementById($scope.All_Fee_codes[i].sims_concession_fee_code)
                                    if ($scope.All_Fee_codes[i].status == false) {
                                        $scope.sendobject = {
                                            opr: $scope.opr = 'D',
                                            sims_concession_number: $scope.temp.sims_concession_number,
                                            sims_concession_description: $scope.temp.sims_concession_description,
                                            sims_concession_type: $scope.temp.sims_concession_type,
                                            sims_concession_discount_type: $scope.temp.sims_concession_discount_type,
                                            sims_concession_discount_value: $scope.temp.sims_concession_discount_value,
                                            sims_concession_applicable_on: $scope.temp.sims_concession_applicable_on,
                                            sims_concession_corporate_billing: $scope.temp.sims_concession_corporate_billing,
                                            sims_concession_academic_year: $scope.temp.sims_concession_academic_year,
                                            sims_concession_applicable_to: $scope.temp.sims_concession_applicable_to,
                                            sims_concession_onward_child: $scope.temp.sims_concession_onward_child,
                                            sims_concession_status: $scope.temp.sims_concession_status,
                                            sims_concession_fee_code: $scope.All_Fee_codes[i].sims_concession_fee_code
                                        }
                                        datasend.push($scope.sendobject);
                                    }
                                }

                            }

                        }

                        else {
                            $scope.sendobject = {
                                opr: $scope.opr = 'U',
                                sims_concession_number: $scope.temp.sims_concession_number,
                                sims_concession_description: $scope.temp.sims_concession_description,
                                sims_concession_type: $scope.temp.sims_concession_type,
                                sims_concession_discount_type: $scope.temp.sims_concession_discount_type,
                                sims_concession_discount_value: $scope.temp.sims_concession_discount_value,
                                sims_concession_applicable_on: $scope.temp.sims_concession_applicable_on,
                                sims_concession_corporate_billing: $scope.temp.sims_concession_corporate_billing,
                                sims_concession_academic_year: $scope.temp.sims_concession_academic_year,
                                sims_concession_applicable_to: $scope.temp.sims_concession_applicable_to,
                                sims_concession_onward_child: $scope.temp.sims_concession_onward_child,
                                sims_concession_status: $scope.temp.sims_concession_status,
                                sims_concession_fee_code: $scope.fee_code
                            }
                            datasend.push($scope.sendobject);
                        }
                    }
                    $http.post(ENV.apiUrl + "api/common/Concession/CUSimsConcession", datasend).then(function (res) {
                        $scope.CUDobj = res.data;

                        swal({ text: $scope.CUDobj.strMessage, width: 380, showCloseButton: true });

                        data();
                        $scope.readonlydiscount = true;
                    });
                }
            }

            $scope.ischecked = function (str) {


                $scope.fee_code = '';
                var code = document.getElementById(str);
                if (code.checked = true) {
                    $scope.fee_code = str;
                }

            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_concession_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_concession_description.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.Showsims_concession_applicable_to = function (str) {
                if (str == 'Onward Child') {
                    $scope.dis = false;
                }
                else {
                    $scope.dis = true;
                }
            }

            $scope.ShowsimsDiscount_Type = function (str1) {
                if ($scope.updisabled == true) {
                    $scope.temp[str1] = '';
                }

                $scope.readonlydiscount = false;
            }

            $scope.Checkvalues = function (str) {

                if ($scope.temp.sims_concession_discount_type == 'Percentage ') {
                    if (str > 100) {
                        $scope.temp.sims_concession_discount_value = '';
                    }
                }

            }

        }]
        )
})();