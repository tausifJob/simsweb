﻿(function () {
    'use strict';
    var main;
    var prdocdata = [];
    var data1 = [];
    var data = [];
    var doc_no;
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('FeeCollectionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            var user = $rootScope.globals.currentUser.username;
            $scope.table1 = true;
            $scope.busy = false;
            $scope.rgvtbl = false;
            $scope.edt = {};
            $http.post(ENV.apiUrl + "api/FeeReceipt/Cashier").then(function (Cashier) {
                $scope.Cashier = Cashier.data;
            });

            $http.post(ENV.apiUrl + "api/FeeReceipt/PaymentMode").then(function (PaymentMode) {
                $scope.PaymentMode = PaymentMode.data;
            });

            $http.post(ENV.apiUrl + "api/FeeReceipt/FeeType").then(function (FeeType) {
                $scope.FT = FeeType.data;
            });
            debugger;
            
            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.Show_data = function () {

                if ($scope.edt.from_date == undefined || $scope.edt.from_date == '' || $scope.edt.to_date == undefined || $scope.edt.to_date == '') {
                    swal('', 'From date and to date are mandatory.');
                }
                else {

                    debugger;
                    var data = {
                        from_date: $scope.edt.from_date,
                        to_date: $scope.edt.to_date,
                        cashier_code: $scope.edt.cashier_code,
                        payment_mode_code: $scope.edt.payment_mode_code,
                        enroll_number: $scope.edt.search
                    }
                    $scope.busy = true;
                    $scope.rgvtbl = false;
                    $http.post(ENV.apiUrl + "api/FeeReceipt/FeeCollection", data).then(function (FeeCollection) {
                        $scope.FeeCollection = FeeCollection.data;
                        console.log(FeeCollection.data)
                        $scope.header_data = [];
                        debugger;
                        angular.forEach($scope.FeeCollection[0], function (value, key) {
                            $scope.header_data.push({ name: key });
                        });
                        if ($scope.FeeCollection.length > 0) {
                            $scope.busy = false;
                            $scope.rgvtbl = true;
                        }
                        else {
                            $scope.busy = false;
                            $scope.rgvtbl = false;
                            swal('', 'Record not found.');
                        }

                    });
                }
            }

            $scope.Reset_data = function () {
                $scope.busy = false;
                $scope.rgvtbl = false;
                $scope.edt = {};
                
            }

            

            $scope.Report = function (str) {
                debugger;
                console.log(str['doc name']);
                var data = {
                    location: 'Sims.SIMR51FeeRec',
                    parameter: {
                        fee_rec_no: str['doc name'],
                    },
                    state: 'main.Sim700',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')
            }

        }]
        )
})();