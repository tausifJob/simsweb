﻿
(function () {
    'use strict';

    var simsController = angular.module('sims.module.Fee');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('ClassWiseFeeCollectionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '5';
            $scope.show = true;
            $scope.grid = true;
            $scope.submitbutton = true;
            var user = $rootScope.globals.currentUser.username;
            $scope.edt1 = { fee_type: '0' };
            $scope.Get_Sims550FeesDetails = [];

            var now = new Date();
            var month = (now.getMonth() + 1);
            var day = now.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;

            $scope.payment_date = month + '/' + day + '/' + now.getFullYear();

            $http.get(ENV.apiUrl + "api/studentfeerefund/GetAllBankNames").then(function (GetAllBankNames) {
                $scope.AllBankNames = GetAllBankNames.data;
            });

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;

            });

            $scope.getacyr = function () {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.sims_cur_code).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;

                });
            }

            $scope.getsections = function () {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.sims_cur_code + "&academic_year=" + $scope.sims_academic_year).then(function (res) {
                    $scope.grade = res.data;

                })

                $http.get(ENV.apiUrl + "api/Classwisefeecollection/Getsims550_TermsAcademicYear?curiculum=" + $scope.sims_cur_code + "&academicyear=" + $scope.sims_academic_year).then(function (Getsims550_TermsAcademicYear) {
                    $scope.TermsAcademicYear = Getsims550_TermsAcademicYear.data;
                })
            };

            $scope.getsection = function () {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.sims_cur_code + "&grade_code=" + $scope.sims_grade_code + "&academic_year=" + $scope.sims_academic_year).then(function (Allsection) {
                    $scope.section1 = Allsection.data;

                })
            };

            $scope.btn_Cancel_Click = function () {

                $scope.grid = true;
                $scope.addfee = false;
                $scope.discounttable = false;
                $scope.SelectedOtherfee = false;
                $scope.SelectedFeeObject = [];
                $scope.SelectedPaymentModeObject = [];
                $scope.GetStudentRefundFeeNumberDetails = [];
                $scope.SelectedOtherfeeObject = [];
                $scope.temp.fee_concession_number = '';
                $scope.fee_concession_percentage = 0;
                $scope.doc_discount_amount = 0;
                $scope.grand_total_amount = 0;
            }

            $scope.Show_Data = function () {

                $scope.submitbutton = true;
                $scope.maintable = false;

                $scope.busyindicator = true;
                $http.get(ENV.apiUrl + "api/Classwisefeecollection/Get_sims550Fees?curcode=" + $scope.sims_cur_code + "&ayear=" + $scope.sims_academic_year + "&gradecode=" + $scope.sims_grade_code + "&sectioncode=" + $scope.sims_section_name + "&termcode=" + $scope.sims_term_code).then(function (Get_sims550Fees) {
                    $scope.Get_sims550Fees = Get_sims550Fees.data;

                    if ($scope.Get_sims550Fees.length != 0) {

                        for (var i = 0; i < $scope.Get_sims550Fees.length; i++) {
                            //$scope.Get_sims550Fees[i].readonly1 = true;
                            $scope.Get_sims550Fees[i].Bank_name = 'Select Bank';

                        }

                        $scope.maintable = true;
                        $scope.busyindicator = false;
                        $scope.totalItems = $scope.Get_sims550Fees.length;
                        $scope.todos = $scope.Get_sims550Fees;
                        $scope.makeTodos();
                    }
                    else {
                        $scope.busyindicator = false;
                        $scope.maintable = false;
                        swal({ text: 'Data Not Found', width: 300, showCloseButton: true });
                    }
                    $scope.busyindicator = false;
                });
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {

                var rem = parseFloat($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseFloat(begin) + parseFloat($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].disabled2 = true;
                    $scope.filteredTodos[i].sims_iscash_mode = true;
               
                }

            };

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);
           
            $scope.Studentdata = [];

            $scope.Get_FeesDetails = function (info) {

                $scope.edt1 = '';
                $scope.insertdata = info;
                $scope.Get_Sims550FeesDetails = info;
                var data = {
                    std_fee_cur_code: info.sims_cur_code,
                    std_fee_academic_year: info.sims_academic_year,
                    std_fee_grade_code: info.sims_grade_code,
                    std_fee_section_code: info.sims_section_code,
                    FeeEnrollNo: info.sims_enroll_number
                };

                $http.post(ENV.apiUrl + "api/Classwisefeecollection/SimsFeeType1", data).then(function (GetDiscountFeeTypes) {
                    $scope.GetDiscountFeeTypes = GetDiscountFeeTypes.data;
                    $('#MyModal').modal('show');
                });
            }

            $scope.size = function (str) {
                
                if (str == 'All') {
                    $scope.currentPage = 1;

                    $scope.filteredTodos = $scope.Get_sims550Fees;

                    $scope.numPerPage  = $scope.filteredTodos.length;

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].disabled2 = true;
                        $scope.filteredTodos[i].sims_iscash_mode = true;
                    }
                    $scope.makeTodos();
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#Table3").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.btn_Addnewfeedetails_Click = function () {

                $scope.flag1 = false;

                if (($scope.edt1.fee_type != undefined && $scope.edt1.fee_type != '') && ($scope.edt1.std_fee_child_paying_amount_temp2 != undefined && $scope.edt1.std_fee_child_paying_amount_temp2 != '')) {
                    for (var i = 0; i < $scope.GetDiscountFeeTypes.length; i++) {
                        if ($scope.GetDiscountFeeTypes[i].std_fee_discount_type_code == $scope.edt1.fee_type) {
                            $scope.sims_fee_description = $scope.GetDiscountFeeTypes[i].std_fee_discount_type;
                        }
                    }

                    for (var j = 0; j < $scope.Get_Sims550FeesDetails.length; j++) {
                        if ($scope.Get_Sims550FeesDetails[j].sims_fee_description == $scope.sims_fee_description) {
                            $scope.flag1 = true;
                            break;
                        }

                    }
                    if ($scope.flag1 != true) {

                        var data = {
                            sims_academic_year: $scope.insertdata.sims_academic_year
                            , isOtherFee: true
                            , sims_cur_code: $scope.insertdata.sims_cur_code
                            , sims_fee_code: $scope.edt1.fee_type
                            , sims_grade_code: $scope.insertdata.sims_grade_code
                            , sims_section_code: $scope.insertdata.sims_section_code
                            , sims_term_code: $scope.insertdata.sims_term_code

                            , sims_enroll_number: $scope.insertdata.sims_enroll_number
                            , sims_expected_amt: parseFloat($scope.edt1.std_fee_child_paying_amount_temp2).toFixed(4)
                            , sims_fee_description: $scope.sims_fee_description
                            , sims_paid_amt: '0.0000'
                            , sims_balance_amt: parseFloat($scope.edt1.std_fee_child_paying_amount_temp2).toFixed(4)
                            , status: false
                            , disabled1: false
                        }

                        $scope.Get_Sims550FeesDetails.fee_details.push(data);
                        var maindata = [];
                        for (var i = 0; i < $scope.Get_Sims550FeesDetails.fee_details.length; i++) {
                            maindata.push($scope.Get_Sims550FeesDetails.fee_details[i]);
                        }

                        for (var j = 0; j < $scope.Get_sims550Fees.length; j++) {

                            if ($scope.Get_sims550Fees[j].sims_enroll_number == data.sims_enroll_number) {
                                $scope.Get_sims550Fees[j].sims_expected_amt = Math.round(parseInt($scope.Get_sims550Fees[j].sims_expected_amt) + parseInt($scope.edt1.std_fee_child_paying_amount_temp2)).toFixed(4);
                                $scope.Get_sims550Fees[j].sims_balance_amt = Math.round(parseInt($scope.Get_sims550Fees[j].sims_expected_amt)).toFixed(4);
                                $scope.Get_sims550Fees[j].fee_details = '';
                                $scope.Get_sims550Fees[j].fee_details = maindata;
                                break;
                            }
                        }
                    }
                    else {
                        $scope.edt1.fee_type = '0';
                        $scope.edt1.std_fee_child_paying_amount_temp2 = '';
                        swal({ text: 'This Fee Type Already Used Choose Another Fee type', width: 380 });
                    }
                }
                else {

                    swal({ text: 'Enter Amount Or Select Fee Type', width: 350 ,showCloseButton:true});
                }
            }

            $scope.btn_rmovefeedetails_Click = function ($event, index, str, info) {

                for (var j = 0; j < $scope.filteredTodos.length; j++) {
                    if ($scope.filteredTodos[j].sims_enroll_number == info.sims_enroll_number) {
                        $scope.filteredTodos[j].sims_expected_amt = Math.round(parseInt($scope.filteredTodos[j].sims_expected_amt) - parseInt(info.sims_expected_amt)).toFixed(4);
                        $scope.filteredTodos[j].sims_balance_amt = Math.round(parseInt($scope.filteredTodos[j].sims_expected_amt)).toFixed(4);
                        break;
                    }
                }
                str.fee_details.splice(index, 1);
            }

            $scope.CheckDataToInsert = function (info) {

                $scope.submitbutton = true;

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';

                        $scope.insertdata = info;
                        $scope.Get_Sims550FeesDetails = info;
                        $scope.GetDiscountFeeTypes = [];
                        var datafee = [];
                        $scope.Get_Sims550FeesDetails = [];
                        info.fee_details = [];
                        $http.post(ENV.apiUrl + "api/Classwisefeecollection/FeesDetailsGet_Sims550", info).then(function (Get_Sims550FeesDetails1) {
                            $scope.Get_Sims550FeesDetails = Get_Sims550FeesDetails1.data;
                            $scope.submitbutton = false;
                            for (var i = 0; i < $scope.Get_Sims550FeesDetails.fee_details.length; i++) {
                                datafee.push($scope.Get_Sims550FeesDetails.fee_details[i]);
                            }
                            info.fee_details = datafee;
                            info.disabled2 = false;
                            $scope.submitbutton = false;

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                if ($scope.filteredTodos[i].sims_student_check == true) {
                                    $scope.submitbutton = false;
                                    break;
                                }
                            }
                        });

                        $scope.sims_enroll_number = info.sims_enroll_number;

                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                        //info.readonly1 = true;
                       
                    }

                });
               
            }

            $scope.Savedata_function = function (isvalid) {
               
                if (isvalid) {

                    $scope.insertedcheque = true;
                    $scope.inserted = false;
                    var studentdata = [];
                    var name = '';
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        if ($scope.filteredTodos[i].sims_student_check == true) {

                            name = name + $scope.filteredTodos[i].sims_enroll_number + ',' + $scope.filteredTodos[i].sims_student_Name + '/';

                            $scope.inserted = true;
                            for (var k = 0; k < $scope.AllBankNames.length; k++) {
                                if ($scope.AllBankNames[k].bank_name == $scope.filteredTodos[i].Bank_name) {
                                    $scope.filteredTodos[i].Bank_name = $scope.AllBankNames[k].bank_code;
                                }
                            }
                            for (var j = 0; j < $scope.filteredTodos[i].fee_details.length; j++) {
                                if ($scope.filteredTodos[i].sims_iscash_mode == true) {
                                    $scope.filteredTodos[i].fee_details[j].Payment_mode = 'CA';
                                    $scope.filteredTodos[i].fee_details[j].sims_cheque_no = 'N/A';
                                    $scope.filteredTodos[i].fee_details[j].sims_cheque_date = null;
                                    $scope.filteredTodos[i].fee_details[j].sims_bank_name = null;
                                }
                                else {

                                    $scope.filteredTodos[i].fee_details[j].Payment_mode = 'CH';
                                    $scope.filteredTodos[i].fee_details[j].sims_cheque_no = $scope.filteredTodos[i].cheque_no;
                                    $scope.filteredTodos[i].fee_details[j].sims_cheque_date = $scope.filteredTodos[i].cheque_date;
                                    $scope.filteredTodos[i].fee_details[j].sims_bank_name = $scope.filteredTodos[i].Bank_name;
                                }
                            }

                            studentdata.push($scope.filteredTodos[i]);

                            for (var j = 0; j < $scope.filteredTodos[i].fee_details.length; j++) {
                                if ($scope.filteredTodos[i].sims_iscash_mode != true) {
                                    if (($scope.filteredTodos[i].fee_details[j].sims_cheque_no == '' || $scope.filteredTodos[i].fee_details[j].sims_cheque_date == '' || $scope.filteredTodos[i].fee_details[j].sims_bank_name == '') || ($scope.filteredTodos[i].fee_details[j].sims_cheque_no == undefined || $scope.filteredTodos[i].fee_details[j].sims_cheque_date == undefined || $scope.filteredTodos[i].fee_details[j].sims_bank_name == 'Select Bank')) {
                                        $scope.insertedcheque = false;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                    if ($scope.inserted == true) {
                        if ($scope.insertedcheque != false) {
                            $http.post(ENV.apiUrl + "api/Classwisefeecollection/update_sims550Fees", studentdata).then(function (update_sims550Fees) {
                                $scope.update_sims550Fees = update_sims550Fees.data;
                                $scope.studentdataobject = [];
                                $scope.studentdataobject1 = [];

                                for (var i = 0; i < $scope.update_sims550Fees.length; i++) {
                                    if ($scope.update_sims550Fees[i].inserted == true) {
                                        for (var j = 0; j < $scope.filteredTodos.length; j++) {

                                            if ($scope.filteredTodos[j].sims_enroll_number == $scope.update_sims550Fees[i].sims_enroll_number) {
                                                $scope.update_sims550Fees[i].sims_student_Name = $scope.filteredTodos[j].sims_student_Name,
                                                $scope.update_sims550Fees[i].sims_academic_year = $scope.sims_academic_year
                                                $scope.studentdataobject1.push($scope.update_sims550Fees[i]);
                                            }
                                        }

                                        $scope.studentdataobject = $scope.studentdataobject1;

                                        $('#Div1').modal('show');
                                        $scope.Show_Data();
                                    }

                                    else {
                                        swal({ text: 'Fee Not Inserted', width: 350, showCloseButton: true });
                                        for (var j = 0; j < $scope.filteredTodos.length; j++) {
                                            for (var i = 0; i < studentdata.length; i++) {
                                                if ($scope.filteredTodos[j].sims_enroll_number == studentdata[i].sims_enroll_number) {
                                                    $scope.filteredTodos[j].sims_expected_amt = Math.round(parseInt($scope.filteredTodos[j].sims_expected_amt) - parseInt(studentdata[i].sims_expected_amt)).toFixed(4);
                                                    $scope.filteredTodos[j].sims_balance_amt = Math.round(parseInt($scope.filteredTodos[j].sims_expected_amt)).toFixed(4);
                                                }
                                            }
                                        }
                                    }
                                }
                                
                            });
                        }
                        else {
                            swal({ text: 'Fill All Fields For Cheque', width: 320, showCloseButton: true });
                        }
                    }
                    else {
                        swal({ text: 'Select At Least One Student To Collect Fee', width: 350, showCloseButton: true });
                    }
                }
            }

            $scope.searched = function (valLists, toSearch) {
                    return _.filter(valLists,
                    function (i) {
                        /* Search Text in all  fields */
                        return searchUtil(i, toSearch);
                    });
                };

            $scope.search = function () {
                    $scope.todos = $scope.searched($scope.Get_sims550Fees, $scope.searchText);
                    $scope.totalItems = $scope.todos.length;;
                    $scope.currentPage = '1';
                    if ($scope.searchText == '') {
                        $scope.todos = $scope.Get_sims550Fees;
                    }

                    $scope.makeTodos();
                }

            function searchUtil(item, toSearch) {
                    /* Search Text in all 3 fields */
                    return (item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_student_Name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
                }

            $scope.Setdefaultvalues = function (info) {
                    info.Bank_name = 'Select Bank';
                    info.cheque_no = '';
                    info.cheque_date = '';
                }
            }])
        })();

