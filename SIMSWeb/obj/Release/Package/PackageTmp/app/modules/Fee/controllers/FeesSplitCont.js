﻿(function () {
    'use strict';
    var obj1, temp, opr;
    var main, del = [];
    var Record = [];
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('FeesSplitCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.pagesize = "5";
            $scope.pageindex = 0;

            $scope.operation = true;
            $scope.save_btn = false;

            // $scope.global_Search_click1 = function () {
            $rootScope.visible_stud = true;
            $rootScope.visible_parent = true;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = false;
            $rootScope.chkMulti = true;
            $scope.split_tbl1 = false;


            //     }

            $scope.edit = function (str) {
                $scope.readonly = true;
                $scope.temp = str;
                $scope.opr = 'U';
                $scope.table = false; $scope.display = true; $scope.save_btn = false; $scope.Update_btn = true;

            }

            $scope.Global_Search_modal = function () {
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }

            $http.get(ENV.apiUrl + "api/ResetPassword/GetAll_User_Status").then(function (docstatus2) {
                $scope.User_stat = docstatus2.data;
                console.log($scope.docstatus2);
            });

          


           
            $scope.$on('global_cancel', function ()
            {
                debugger;
                for (var i = 0; i < $scope.SelectedUserLst.length; i++)
                {
                            debugger
                            $scope.edt =
                                {
                                    s_parent_id: $scope.SelectedUserLst[i].s_parent_id,
                                    s_parent_name: $scope.SelectedUserLst[i].s_parent_name,
                                    s_enroll_no: $scope.SelectedUserLst[i].s_enroll_no,
                                    s_sname_in_english: $scope.SelectedUserLst[i].s_sname_in_english,
                                    s_class: $scope.SelectedUserLst[i].s_class,
                                }
                            Record.push($scope.edt);
                            $scope.Record1 = Record;
                }
                $scope.split_tbl1 = true;
                $scope.save_btn = true;
            });

            $scope.CheckAllChecked=function()
            {
                main = document.getElementById("mainchk");
                for(var i=0;i<$scope.Record1.length;i++)
                {
                    var v = document.getElementById($scope.Record1[i].s_enroll_no);
                }

            }
            $scope.submit_click = function ()
            {
                $scope.split_tbl1 = false;
                debugger
                for (var i = 0; i < $scope.Record1.length; i++)
                {
                    debugger;
                    var v = document.getElementById(i);
                    if (v.checked == true)
                    {
                        $http.get(ENV.apiUrl + "api/FeesSplit/getFeesplitEnroll?enroll=" + $scope.Record1[i].s_enroll_no).then(function (splitrecord) {
                            $scope.split_record = splitrecord.data;

                            if ($scope.split_record == true) {
                                swal({ title: "Alert", text: "Student Fee Splited Sucessfully", showCloseButton: true, width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "No fee available to split in months/No Fee Assigned to Student..!", showCloseButton: true, width: 300, height: 200 });
                            }
                        });
                    }
                }

                

            }


            $scope.Search = function () {
                $scope.global_Search_click();

                $('#Global_Search_Modal').modal({ backdrop: "static" });
                //  $scope.display = true;
                //$scope.SelectedUserLst = [];
            }

           

            var dataforUpdate = [];
            $scope.Submit = function () {
                debugger;
                var data = $scope.edt;
                data.opr = "U";
                // for (var i = 0; i < $scope.data.length; i++)
                {
                    if (data.comn_user_password == data.comn_user_Confirmpassword) {
                        dataforUpdate.push(data);
                        //dataupdate.push(data);
                        $http.post(ENV.apiUrl + "api/ResetPassword/CUDResetPassword", dataforUpdate).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Password changes successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Password Dose not Matched, Please check..", width: 350, height: 200 });
                            }
                        });
                        dataforUpdate = [];
                    }
                    else {
                        swal({ title: "Alert", text: "Password Dose not Matched, Please check..", width: 350, height: 200 });

                    }
                }

            }




            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                // format:"mm/dd/yyyy",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });



            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });



        }])



})();
