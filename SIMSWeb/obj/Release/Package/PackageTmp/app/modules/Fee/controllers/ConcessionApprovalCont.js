﻿
(function () {
    'use strict';

    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

   
    simsController.controller('ConcessionApprovalCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.table = false;
            $scope.pagesize = '5';

            $scope.temp1 = {};
            $(function () {
                $('#cmb_sections').multipleSelect({ width: '100%' });
            });
            var user = $rootScope.globals.currentUser.username;
            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;
            });


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (getCuriculum) {
                $scope.getCuriculum = getCuriculum.data;
                $scope.getAcademicyear($scope.getCuriculum[0].sims_cur_code);
            });

            $scope.getAcademicyear = function (str) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (getAcademicYear) {
                    $scope.getAcademicYear = getAcademicYear.data;

                    $scope.temp1 = {
                        sims_cur_code: $scope.getCuriculum[0].sims_cur_code,
                        sims_fee_academic_year: $scope.getAcademicYear[0].sims_academic_year
                    };
                    $scope.getConcessionTypes();

                });
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#Table3").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.btn_StudentView_Click = function () {
                $scope.busy = true;
                $scope.concession_number = '';
                console.log('OBJE', $scope.temp1);
                for (var i = 0; i < $scope.temp1.sims_concession_number.length; i++) {
                    $scope.concession_number = $scope.concession_number + $scope.temp1.sims_concession_number[i] + ",";
                }
                console.log('CONCESSION', $scope.concession_number);
                $http.post(ENV.apiUrl + "api/concession/allsims013_Students?concession_number="+$scope.concession_number, $scope.temp1).then(function (Get_sims013_Students) {
                    $scope.Get_sims013_Students = Get_sims013_Students.data;
                    $scope.filteredTodos = [];



                    if ($scope.Get_sims013_Students.length <= 0) {
                        $scope.searchtable = false;
                        $scope.busy = false;
                        swal({
                            text: 'No concessions awaiting for approval',
                            width: 300,
                            height: 300
                        });
                    }
                    else {
                        $scope.busy = false;
                        $scope.totalItems = $scope.Get_sims013_Students.length;
                        $scope.todos = $scope.Get_sims013_Students;
                        $scope.makeTodos();
                        $scope.searchtable = true;
                    }
                })
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.getConcessionTypes = function () {

                $http.get(ENV.apiUrl + "api/concession/getConcession_type?sims_fee_academic_year=" + $scope.temp1.sims_fee_academic_year).then(function (Get_Concession_type) {
                    $scope.Get_Concession_type = Get_Concession_type.data;
                   
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);

                });
            }

            $scope.btn_StudentSearch_Click = function () {

                $scope.searchtable = false;
                $scope.temp = '';

                $('#myModal').modal('show');
            }

            $scope.edit = function (str) {
                $scope.display = false;
                $scope.table = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.opration = true;
                $scope.edt = str;


                if (main.checked == true) {

                    for (var i = 0; i < $scope.obj.length; i++) {
                        var t = $scope.obj[i].sims_student_enroll_number; console.log(t);
                        var v = document.getElementById(t);
                        v.checked = false;

                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.New = function () {
                $scope.display = false;
                $scope.table = false;
                $scope.opration = true;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
            }

            var check = '';

            $scope.btn_StudentSave_Click = function () {
                var allfee = [];
                var flag = false;
                for (var j = 0; j < $scope.filteredTodos.length ; j++) {
                    var d = document.getElementById($scope.filteredTodos[j].sims_transaction_number);
                    if (d.checked == true) {
                        for (var i = 0; i < $scope.filteredTodos[j].list.length; i++) {
                            $scope.filteredTodos[j].list[i]['sims_created_by'] = user;
                            allfee.push($scope.filteredTodos[j].list[i]);
                        }
                        flag = true;
                    }
                }
               
                if (flag == true) {
                    swal({
                        text: 'Are you sure? You Want be Approved Concession',
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/concession/CUD_sims013_Students", allfee).then(function (msg) {
                                $scope.msg1 = msg.data;

                                if ($scope.msg1 == true) {
                                    allfee = [];
                                    swal({
                                        text: 'Concession Approved Successfully',
                                        width: 300,
                                        height: 300
                                    });
                                    $scope.btn_StudentView_Click();
                                }
                                else {
                                    swal({
                                        text: 'Concession Not Approved',
                                        width: 300,
                                        height: 300
                                    });
                                }
                            })
                        }
                        else {

                            check = document.getElementById("mainchk");
                            check.checked = false;
                            $scope.MultipleSelectrecords();
                        }
                    })
                }
                else {
                    swal({
                        text: 'Select Record To Apply Concession',
                        width: 320,
                        showCloseButton: true
                    });
                }
            }

            $scope.MultipleSelectrecords = function () {

                check = document.getElementById("mainchk");
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var d = document.getElementById($scope.filteredTodos[i].sims_transaction_number);

                    if (check.checked == true) {
                        d.checked = true;
                        $('tr').addClass("row_selected");
                    }
                    else {
                        d.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }
            }

            $scope.checkAny = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                var main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }

            }

            $scope.SearchSudent = function () {

                $scope.searchtable = false;
                $scope.busy = true;
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.temp)).then(function (Allstudent) {
                    $scope.student = Allstudent.data;
                    $scope.searchtable = true;
                    $scope.busy = false;

                });
            }

            var enroll = '';

            $scope.DataEnroll = function () {

                debugger;
                var sims_enroll_number = 'sims_enroll_number';
                var str = '';
                for (var i = 0; i < $scope.student.length; i++) {
                    var t = $scope.student[i].s_enroll_no;
                    var v = document.getElementById(t + i);
                    if (v.checked == true)
                        str = str + $scope.student[i].s_enroll_no + ',';
                }

                $scope.temp1[sims_enroll_number] = str;
            }

            $scope.size = function (str) {
              
                if (str == "All") {

                    $scope.filteredTodos = $scope.Get_sims013_Students;
                    $scope.todos = $scope.Get_sims013_Students;
                    $scope.totalItems = $scope.Get_sims013_Students.length;
                    $scope.numPerPage = $scope.Get_sims013_Students.length;
                    $scope.makeTodos();
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };
            $scope.searchText = '';
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Get_sims013_Students, $scope.searchText);
                $scope.totalItems = $scope.Get_sims013_Students.length;;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Get_sims013_Students;
                }

                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

        }])
})();

