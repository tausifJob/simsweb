﻿
(function () {
    'use strict';

    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('FeeRefundCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.table = false;
            $scope.fee_total_amount = 0;
            $scope.temp = {};
            $scope.temp1 = {}
            $scope.temp1['enroll_number'] = '';
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/StudentFee/GetReceipt").then(function (res) {
                rname = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;
                $scope.temp['cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.getacyr();
            })

            $scope.getacyr = function () {

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.temp.cur_code).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    $scope.temp['academic_year'] = $scope.Academic_year[0].sims_academic_year;
                    $scope.getsections();

                })
            }

            $scope.getsections = function () {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.cur_code + "&academic_year=" + $scope.temp.academic_year).then(function (res) {
                    $scope.grade = res.data;
                })
            };

            $scope.getsection = function () {

                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.temp.cur_code + "&grade_code=" + $scope.temp.grade_code + "&academic_year=" + $scope.temp.academic_year).then(function (Allsection) {
                    $scope.section1 = Allsection.data;
                })
            };

            $scope.show = true;
            $scope.cheque_info = true;
            $scope.FeeAdjustment = false;
            var now = new Date();
            var month = (now.getMonth() + 1);
            var day = now.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            $scope.payment_date = now.getFullYear() + '/' + month + '/' + day;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#Table3").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/Feerefund/Get_sims550_payment_mode").then(function (Get_sims550_payment_mode) {
                $scope.Get_sims550_payment_mode = Get_sims550_payment_mode.data;
            });

            $http.get(ENV.apiUrl + "api/Feerefund/Get_sims550_bank").then(function (Get_sims550_bank) {
                $scope.Get_sims550_bank = Get_sims550_bank.data;
            });

            $scope.Check_pament_mode = function (info) {
                if (info == 'Ch') {
                    $scope.cheque_info = false;
                }
                else {
                    $scope.cheque_info = true;
                    $scope.temp2 = '';
                }
            }

            $scope.btn_StudentSearch_Click = function () {

                if ($scope.temp.enroll_number != "") {
                    $scope.SearchSudent();
                }
                else {
                    $scope.searchtable = false;
                    $scope.temp['grade_code'] = '';
                    $scope.temp['section_code'] = '';
                    $scope.temp['enroll_number'] = '';
                    $scope.temp['parentcode'] = '';
                    $('#myModal').modal('show');
                }
            }

            $scope.edit = function (str) {
                $scope.display = false;
                $scope.table = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.opration = true;
                $scope.edt = str;
                if (main.checked == true) {

                    for (var i = 0; i < $scope.obj.length; i++) {
                        var t = $scope.obj[i].sims_student_enroll_number; console.log(t);
                        var v = document.getElementById(t);
                        v.checked = false;

                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.New = function () {
                $scope.display = false;
                $scope.table = false;
                $scope.opration = true;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
            }

            $scope.btn_StudentSave_Click = function (isvalid) {

                if (isvalid) {
                    var datasend = [];
                    if ($scope.FeeAdjustment != true) {

                        for (var i = 0; i < $scope.filteredTodos.length; i++) {
                            var data = {
                                grade_code: $scope.filteredTodos[i].grade_code,
                                section_code: $scope.filteredTodos[i].section_code,
                                academic_year: $scope.filteredTodos[i].academic_year,
                                cur_code: $scope.filteredTodos[i].cur_code,
                                enroll_number: $scope.filteredTodos[i].enroll_number,
                                fee_code: $scope.filteredTodos[i].fee_code + ',' + parseInt($scope.filteredTodos[i].total_amount) + '/',
                                payment_mode_code: $scope.temp1.payment_mode_code,
                                payment_date: $scope.payment_date,
                                remark: $scope.remark,
                                bank_code: $scope.temp2.bank_code,
                                chk_no: $scope.temp2.chk_no,
                                chk_Date: $scope.temp2.chk_Date
                            }
                        }
                        $http.post(ENV.apiUrl + "api/Feerefund/UpdateStudentDetailsRefound", data).then(function (msg) {
                            $scope.msg1 = msg.data;
                            $scope.filteredTodos = [];
                            if ($scope.msg1 != '') {

                                $scope.searchtable = false;
                                $scope.temp = '';
                                $scope.temp1 = '';
                                $scope.temp2 = '';
                                $scope.fee_total_amount = 0;
                                swal({
                                    text: 'Doc No=' + $scope.msg1 + '   ' + 'Fee Refund Successfully',
                                    width: 320,
                                    showCloseButton: true
                                }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        //  $scope.OkRejectadm();
                                        var data = {
                                            location: rname,
                                            parameter: { fee_rec_no: $scope.msg1 },
                                            state: 'main.Sim43a'
                                        }

                                        window.localStorage["ReportDetails"] = JSON.stringify(data)
                                        $state.go('main.ReportCardParameter')

                                    }
                                    else {

                                        $scope.getllFeedata();
                                    }
                                    $scope.getllFeedata();

                                });
                            }
                            else {
                                swal({
                                    text: 'Fee Not Refund',
                                    width: 320,
                                    showCloseButton: true
                                });

                            }
                        });
                    }
                    else {

                        var datasend = [];

                        var data = {};
                        var datasendobject = [];
                        data.doc_date = $scope.payment_date;
                        data.doc_total_amount = $scope.fee_total_amount;
                        data.doc_enroll_no = $scope.temp.enroll_number;
                        data.doc_other_charge_desc = 0;
                        data.doc_other_charge_amount = 0;
                        data.doc_discount_pct = null;
                        data.doc_discount_amount = 0,
                        data.doc_narration = $scope.remark;
                        data.std_fee_student_name = user;
                        data.std_fee_academic_year = $scope.temp.academic_year;

                        $http.post(ENV.apiUrl + "api/Feerefund/adjustmentRefundInsertToFee_Document", data).then(function (RefundInsertToFee_Document) {
                            $scope.RefundInsertToFee_Document = RefundInsertToFee_Document.data;

                            if ($scope.RefundInsertToFee_Document != '' && $scope.RefundInsertToFee_Document != 'false') {

                                for (var i = 0; i < $scope.Fee_Details.length; i++) {
                                    if ($scope.Fee_Details[i].status == true) {
                                        var data1 = {};

                                        data1.doc_no = $scope.RefundInsertToFee_Document;
                                        data1.dd_fee_number = $scope.Fee_Details[i].std_fee_number;
                                        data1.PayFeeCode = $scope.Fee_Details[i].std_fee_code;
                                        data1.FeeEnrollNo = $scope.temp.enroll_number;
                                        data1.PayDisPeriodNo = $scope.Fee_Details[i].std_fee_child_period_No;
                                        data1.PayAmount = $scope.Fee_Details[i].std_fee_child_exp_amount;
                                        data1.PayDisAmount = $scope.Fee_Details[i].std_fee_concession_amount;
                                        data1.paymentMode = $scope.temp1.payment_mode_code;
                                        data1.chequeDDNo = $scope.temp2.chk_no;
                                        data1.BankName = $scope.temp2.bank_code;
                                        data1.std_fee_cur_code = $scope.Fee_Details[i].std_fee_cur_code;
                                        data1.std_fee_academic_year = $scope.Fee_Details[i].std_fee_academic_year;
                                        data1.std_fee_grade_code = $scope.Fee_Details[i].std_fee_grade_code;
                                        data1.std_fee_section_code = $scope.Fee_Details[i].std_fee_section_code;
                                        data1.sims_fee_collection_mode = $scope.Fee_Details[i].sims_fee_collection_mode;

                                        data1.slma_ldgrctl_code = null;
                                        data1.slma_acno = null;
                                        data1.slma_ldgrctl_year = null;
                                        data1.isOtherFee = false;

                                        data1.doc_paying_agent_transaction_lineno = null;
                                        data1.doc_paying_agent_transaction_no = null;
                                        data1.dd_fee_credit_card_code = null;
                                        data1.dd_fee_credit_card_auth_code = null;
                                        data1.PayDisAmount = 0;


                                        if ($scope.temp1.payment_mode_code == 'Cash') {
                                            data1.TransactionID = 'N/A';
                                            data1.chequeDDNo = 'N/A';
                                            data1.chequeDDNo = 'N/A';
                                            data1.TransactionID = 'N/A';
                                            data1.chequeDDDate = 'N/A';
                                            data1.chequeDDNo = 'N/A';
                                            data1.TransactionID = 'N/A';
                                            data1.doc_paying_agent_transaction_lineno = 'N/A';
                                            data1.doc_paying_agent_transaction_no = 'N/A';
                                            data1.chequeDDNo = 'N/A';
                                            data1.TransactionID = 'N/A';
                                            data1.dd_fee_credit_card_code = 'N/A';
                                            data1.dd_fee_credit_card_auth_code = 'N/A';
                                        }
                                        else {

                                            data1.TransactionID = 'N/A';
                                            data1.chequeDDNo = 'N/A';
                                            data1.chequeDDNo = 'N/A';
                                            data1.TransactionID = 'N/A';
                                            data1.chequeDDDate = $scope.temp2.chk_Date;
                                            data1.chequeDDNo = $scope.temp2.chk_no;
                                            data1.TransactionID = 'N/A';
                                            data1.doc_paying_agent_transaction_lineno = 'N/A';
                                            data1.doc_paying_agent_transaction_no = 'N/A';
                                            data1.chequeDDNo = 'N/A';
                                            data1.TransactionID = 'N/A';
                                            data1.dd_fee_credit_card_code = 'N/A';
                                            data1.dd_fee_credit_card_auth_code = 'N/A';

                                        }

                                        datasend.push(data1)
                                    }
                                }



                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var data2 = {};
                                    data2.doc_no = $scope.RefundInsertToFee_Document;
                                    data2.std_fee_grade_code = $scope.filteredTodos[i].grade_code;
                                    data2.std_fee_section_code = $scope.filteredTodos[i].section_code;
                                    data2.std_fee_academic_year = $scope.filteredTodos[i].academic_year;
                                    data2.std_fee_cur_code = $scope.filteredTodos[i].cur_code;
                                    data2.FeeEnrollNo = $scope.filteredTodos[i].enroll_number;
                                    data2.PayFeeCode = $scope.filteredTodos[i].fee_code;
                                    data2.paymentMode = $scope.temp1.payment_mode_code;
                                    data2.payment_date = $scope.payment_date;
                                    data2.remark = $scope.remark;
                                    data2.chk_Date = $scope.temp2.chk_Date;
                                    data2.chequeDDNo = $scope.temp2.chk_no;
                                    data2.BankName = $scope.temp2.bank_code;
                                    data2.slma_ldgrctl_code = null;
                                    data2.slma_acno = null;
                                    data2.slma_ldgrctl_year = null;
                                    data2.isOtherFee = false;
                                    data2.doc_paying_agent_transaction_lineno = null;
                                    data2.doc_paying_agent_transaction_no = null;
                                    data2.dd_fee_credit_card_code = null;
                                    data2.dd_fee_credit_card_auth_code = null;
                                    data2.PayDisAmount = 0;
                                    data2.PayDisPeriodNo = datasend[0].PayDisPeriodNo
                                    data2.dd_fee_number = $scope.filteredTodos[i].std_fee_number;
                                    data2.PayAmount = '-' + $scope.fee_total_amount;
                                    data2.PayDisAmount = '0';
                                    data2.sims_fee_collection_mode = $scope.Fee_Details[0].sims_fee_collection_mode;

                                    if ($scope.temp1.payment_mode_code == 'Cash') {
                                        data2.TransactionID = 'N/A';
                                        data2.chequeDDNo = 'N/A';
                                        data2.chequeDDNo = 'N/A';
                                        data2.TransactionID = 'N/A';
                                        data2.chequeDDDate = 'N/A';
                                        data2.chequeDDNo = 'N/A';
                                        data2.TransactionID = 'N/A';
                                        data2.doc_paying_agent_transaction_lineno = 'N/A';
                                        data2.doc_paying_agent_transaction_no = 'N/A';
                                        data2.chequeDDNo = 'N/A';
                                        data2.TransactionID = 'N/A';
                                        data2.dd_fee_credit_card_code = 'N/A';
                                        data2.dd_fee_credit_card_auth_code = 'N/A';
                                    }
                                    else {

                                        data2.TransactionID = 'N/A';
                                        data2.chequeDDNo = 'N/A';
                                        data2.chequeDDNo = 'N/A';
                                        data2.TransactionID = 'N/A';
                                        data2.chequeDDDate = $scope.temp2.chk_Date;
                                        data2.chequeDDNo = $scope.temp2.chk_no;
                                        data2.TransactionID = 'N/A';
                                        data2.doc_paying_agent_transaction_lineno = 'N/A';
                                        data2.doc_paying_agent_transaction_no = 'N/A';
                                        data2.chequeDDNo = 'N/A';
                                        data2.TransactionID = 'N/A';
                                        data2.dd_fee_credit_card_code = 'N/A';
                                        data2.dd_fee_credit_card_auth_code = 'N/A';

                                    }
                                    datasend.push(data2);
                                }

                                $http.post(ENV.apiUrl + "api/Feerefund/AdjustmentRefundInsertToFee_Document_Details", datasend).then(function (RefundInsertToFee_Document_Details) {
                                    $scope.RefundInsertToFee_Document_Details = RefundInsertToFee_Document_Details.data;

                                    datasend = [];
                                    if ($scope.RefundInsertToFee_Document_Details != "") {


                                        for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                            var data = {
                                                grade_code: $scope.filteredTodos[i].grade_code,
                                                section_code: $scope.filteredTodos[i].section_code,
                                                academic_year: $scope.filteredTodos[i].academic_year,
                                                cur_code: $scope.filteredTodos[i].cur_code,
                                                enroll_number: $scope.filteredTodos[i].enroll_number,
                                                fee_code: $scope.filteredTodos[i].fee_code + ',' + parseInt($scope.fee_total_amount) + '/',
                                                payment_mode_code: $scope.temp1.payment_mode_code,
                                                payment_date: $scope.payment_date,
                                                remark: $scope.remark,
                                                bank_code: $scope.temp2.bank_code,
                                                chk_no: $scope.temp2.chk_no,
                                                chk_Date: $scope.temp2.chk_Date
                                            }
                                        }


                                        $http.post(ENV.apiUrl + "api/Feerefund/AdjustmentRefundInsertToFee", data).then(function (AdjustmentRefundInsertToFee) {
                                            $scope.AdjustmentRefundInsertToFee = AdjustmentRefundInsertToFee.data;

                                            if ($scope.AdjustmentRefundInsertToFee == true) {
                                                swal({
                                                    text: 'Fee Refund Successfully',
                                                    width: 350, showCloseButton: true
                                                }).then(function (isConfirm) {
                                                    if (isConfirm) {
                                                        var data = {
                                                            location: rname,
                                                            parameter: { fee_rec_no: $scope.RefundInsertToFee_Document_Details },
                                                            state: 'main.Sim43a'
                                                        }

                                                        window.localStorage["ReportDetails"] = JSON.stringify(data)
                                                        $state.go('main.ReportCardParameter')
                                                    }
                                                    else {

                                                        $scope.getllFeedata();
                                                    }
                                                    $scope.getllFeedata();
                                                });
                                            }
                                            else {

                                                swal({ text: 'Fee Not Refund', width: 310, showCloseButton: true });
                                            }
                                        });

                                    } else {
                                        swal({ text: 'Fee Not Refund', width: 310, showCloseButton: true });
                                    }
                                })

                            }
                            else {
                                swal({ text: 'Record Not Inserted In Fee Document', width: 380, showCloseButton: true });
                            }
                        });
                    }
                }
            }

            $scope.MultipleSelectrecords = function () {

                var check = document.getElementById("mainchk");
                for (var i = 0; i < $scope.Get_sims013_Students.length; i++) {
                    $scope.Get_sims013_Students[i].sims_status = check.checked;
                }
            }

            $scope.SearchSudent = function () {

                $scope.searchtable = false;
                $scope.busy = true;
                $http.post(ENV.apiUrl + "api/Feerefund/StudentDetailsRefound", $scope.temp).then(function (GetStudentDetailsRefound) {
                    $scope.GetStudentDetailsRefound = GetStudentDetailsRefound.data;

                    if ($scope.GetStudentDetailsRefound.length <= 0) {
                        $scope.searchtable = false;
                        $scope.busy = false;
                        swal({ text: 'Record Not Found', width: 380 });
                    }
                    else {
                        $scope.searchtable = true;
                        $scope.busy = false;
                        if ($scope.temp.enroll_number != "") {
                            if ($scope.GetStudentDetailsRefound.length > 0) {
                                $scope.StudentEnrollNumber($scope.GetStudentDetailsRefound[0]);
                            }
                        }
                    }
                });


            }

            var totalamount = '';
            var amount1 = '';

            $scope.StudentEnrollNumber = function (info) {
                $scope.filteredTodos = [];
                $scope.temp1 = angular.copy(info);
                $scope.enroll_number = angular.copy(info.enroll_number);
                $scope.filteredTodos.push(angular.copy(info));
                amount1 = angular.copy(info.total_amount);
                $scope.fee_total_amount = angular.copy(amount1);
                $scope.temp['enroll_number'] = info.enroll_number;

                $scope.getllFeedata();


            }

            $scope.getllFeedata = function () {
                $http.get(ENV.apiUrl + "api/Feerefund/getAdjustFeeDetailsStudentRefound?enroll=" + $scope.temp.enroll_number + "&cur_code=" + $scope.temp.cur_code + "&aca_year=" + $scope.temp.academic_year).then(function (AdjustFeeDetails) {
                    $scope.Fee_Details = angular.copy(AdjustFeeDetails.data);
                    $scope.Fee_Details1 = angular.copy(AdjustFeeDetails.data);

                    $('#myModal').modal('hide');

                    $scope.Checkadjustfeecheck();
                });
            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Get_sims013_Students, $scope.searchText);
                $scope.totalItems = $scope.Get_sims013_Students.length;;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Get_sims013_Students;
                }

                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_enroll_number == toSearch) ? true : false;
            }

            $scope.Ischecked = function (info) {
                for (var i = 0; i < $scope.GetStudentDetailsRefound.length; i++) {
                    var amount = info.total_amount + '.00';

                    $scope.fee_total_amount = parseInt(amount);
                    if (amount1 < amount && $scope.GetStudentDetailsRefound[i].enroll_number == info.enroll_number && $scope.GetStudentDetailsRefound[i].fee_code == info.fee_code) {
                        info.total_amount = amount1;
                        $scope.fee_total_amount = parseInt(info.total_amount);
                        swal({ text: 'Sum Of Fee Used Amount And Refund Amount Should Not Greater Than Fee Amount', width: 380, showCloseButton: true });
                    }
                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.btn_Reset_Click = function () {

                $scope.temp3 = '';
                $scope.temp2 = '';
                $scope.payment_date = '';

            }

            $scope.CheckAmount = function (info) {

                var amount = 0;
                var amount1 = 0;
                $scope.fee_total_amount = 0;
                for (var j = 0; j < $scope.filteredTodos.length; j++) {
                    amount1 = parseFloat(amount1) + parseFloat($scope.filteredTodos[j].total_amount);
                }

                for (var i = 0; i < $scope.Fee_Details.length; i++) {
                    if ($scope.Fee_Details[i].status == true) {
                        amount = parseFloat(amount) + parseFloat($scope.Fee_Details[i].std_fee_child_exp_amount);
                        $scope.fee_total_amount = parseInt(amount);
                        if (amount == 0) {
                            $scope.fee_total_amount = 0;
                        }
                    }
                    else {
                        if (amount == 0) {
                            $scope.fee_total_amount = amount1;
                        }
                    }
                }

                if (info.status == false) {
                    info.std_fee_child_exp_amount = angular.copy(info.dd_fee_amount);
                }

                if (amount > amount1) {
                    amount = 0;
                    $scope.fee_total_amount = 0;
                    $scope.fee_total_amount = amount1;
                    info.status = false;
                    for (var i = 0; i < $scope.Fee_Details.length; i++) {
                        if ($scope.Fee_Details[i].status == true) {
                            amount = parseFloat(amount) + parseFloat($scope.Fee_Details[i].std_fee_child_exp_amount);
                            $scope.fee_total_amount = parseInt(amount);
                        }
                    }
                    swal({ text: 'Selected Amount Should Be Less Than Refund Amount', width: 350, showCloseButton: true });
                }
            }

            $scope.Checkadjustfeecheck = function () {
                for (var j = 0; j < $scope.filteredTodos.length; j++) {
                    $scope.filteredTodos[j].total_amount = amount1;
                    $scope.fee_total_amount = amount1;
                }
                if ($scope.FeeAdjustment == false) {
                    for (var i = 0; i < $scope.Fee_Details.length; i++) {
                        $scope.Fee_Details[i].status = false;
                        $scope.Fee_Details = angular.copy($scope.Fee_Details1);
                    }
                    $scope.fee_total_amount = amount1;
                }
            }

        }])
})();

