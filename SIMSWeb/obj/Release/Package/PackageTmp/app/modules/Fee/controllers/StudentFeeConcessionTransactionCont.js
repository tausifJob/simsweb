﻿(function () {
    'use strict';
    var del = [], listofstudents;
    var myarray = [];
    var main, temp, global_Search, opr;
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StudentFeeConcessionTransactionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            var datasend = [];
            $scope.display = false;
            $scope.table = true;
            $scope.operation = false;
            $scope.pagesize = "5";
            $scope.studenttable = false;
            data();
            $scope.studentdata = false;
            $scope.global_Search = {};
            function data() {



                $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getAllSimsConcessionTransaction").then(function (res) {
                    $scope.display = true;
                    $scope.obj = res.data;
                    $scope.table = true;
                    datasend = [];
                    $scope.operation = false;
                    if ($scope.obj.length != 0) {
                        $scope.totalItems = $scope.obj.length;
                        $scope.todos = $scope.obj;
                        $scope.makeTodos();

                    }
                    else {
                        $scope.table = true;
                        $scope.display = false;
                        $scope.operation = false;
                        swal({ text: 'Sorry Data Not Found', width: 380, showCloseButton: true });
                    }

                });
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.checkAll();
                }
            };

            $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getAllCurName").then(function (res) {
                $scope.Cur_Names = res.data;
                $scope.global_Search['global_curriculum_code'] = $scope.Cur_Names[0].sims_cur_code;
            });

            $scope.Cancel = function () {
                $scope.table = true; $scope.temp = "";
                $scope.operation = false;

            }

            $scope.edit = function (str) {
                $scope.opr = '';
                $scope.studenttable = false;
                $scope.table = false;
                $scope.operation = true;
                $scope.opr = 'U';


                var data = angular.copy(str);
                $scope.temp = data;

                $scope.date = data.sims_created_date;

                $scope.Save_btn = false; $scope.Update_btn = true; $scope.readonly = true; $scope.multi_stud_select = false;
                del = []; myarray = []; $scope.opr = 'U';

                $scope.getAcadm_yr1(data.sims_cur_short_name);

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.checkAll();
                }
            }

            $scope.getAcadm_yr = function (cur_code) {
                $scope.curriculum_code = cur_code;
                $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getAllACAYearCurCode?curcode=" + cur_code).then(function (res) {
                    $scope.Acdm_year = res.data;
                    $scope.global_Search['global_acdm_yr'] = $scope.Acdm_year[0].sims_academic_year;
                    $scope.temp['sims_fee_academic_year'] = $scope.Acdm_year[0].sims_academic_year;
                    $scope.getconcession_description($scope.Acdm_year[0].sims_academic_year);
                });
            }

            $scope.getconcession_description = function (str) {

                $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getAllConcessionCode?academic_year=" + str).then(function (res) {
                    $scope.Concession_nums = res.data;


                    $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getAllConcessionDates?academic_year=" + str).then(function (res) {
                        $scope.dates = res.data;

                        $scope.temp['sims_concession_from_date'] = $scope.dates[0].sims_concession_from_date;
                        $scope.temp['sims_concession_to_date'] = $scope.dates[0].sims_concession_to_date;
                    });
                });
            }

            $scope.getconcession_description1 = function (str) {

                $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getAllConcessionCode?academic_year=" + str).then(function (res) {
                    $scope.Concession_nums = res.data;

                })
            };

            $scope.getAcadm_yr1 = function (cur_code) {
                $scope.curriculum_code = cur_code;
                $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getAllACAYearCurCode?curcode=" + cur_code).then(function (res) {
                    $scope.Acdm_year = res.data;
                    $scope.temp['sims_fee_academic_year'] = $scope.Acdm_year[0].sims_academic_year;
                    $scope.getconcession_description1($scope.Acdm_year[0].sims_academic_year)
                });
            }

            $scope.New = function () {
                $scope.tenantForm.$setPristine();
                $scope.opr = 'I';

                $scope.studenttable = true;
                var d = new Date();
                var year = d.getFullYear();
                var month = d.getMonth() + 1;
                if (month < 10) {
                    month = "0" + month;
                };
                var day = d.getDate();
                $scope.date = year + "/" + month + "/" + day;

                $http.get(ENV.apiUrl + "api/common/ConcessionTransaction/getComnSequenceCode").then(function (res) {
                    $scope.Transaction_Number = res.data; console.log($scope.Transaction_Number);
                    $scope.temp = {
                        sims_transaction_number: $scope.Transaction_Number,
                        sims_created_date: $scope.date,
                        sims_created_by: $rootScope.globals.currentUser.username,
                        sims_status: true,
                        sims_cur_short_name: $scope.Cur_Names[0].sims_cur_code
                    };

                });

                $scope.table = false; $scope.Save_btn = true; $scope.Update_btn = false; $scope.readonly = true; $scope.multi_stud_select = true;
                $scope.operation = true; del = []; $scope.chck_obj = "";
                $scope.getAcadm_yr($scope.Cur_Names[0].sims_cur_code);


            }

            $scope.chck_obj = [];

            $scope.Gbl_checkbox_click = function (info) {
                if (info.ischecked == true) {
                    datasend.push(info)
                    $scope.chck_obj = datasend;
                }
                else {
                    for (var i = 0; i < $scope.chck_obj.length; i++) {
                        if (info.s_enroll_no == $scope.chck_obj[i].s_enroll_no) {

                            $scope.chck_obj.splice(i, 1);
                        }
                    }
                }

            }

            $scope.glbl_remove = function (index, str) {
                str.splice(index, 1);
            }

            $scope.SaveData = function (isvalid) {

                if (isvalid) {
                    if ($scope.opr == 'U') {
                        $http.post(ENV.apiUrl + "api/common/ConcessionTransaction/UpdateConcessionTransaction?data=" + JSON.stringify($scope.temp) + "&opr=U").then(function (res) {
                            $scope.Message = res.data;
                            swal({ text: $scope.Message.strMessage, width: 320, showCloseButton: true });
                            data();
                        });
                    }
                    else {

                        if ($scope.chck_obj.length > 0) {
                            var datasendObject = [];
                            for (var i = 0; i < $scope.chck_obj.length; i++) {
                                var data1 = {

                                    sims_transaction_number: $scope.temp.sims_transaction_number
                                , sims_cur_code: $scope.temp.sims_cur_short_name
                                , sims_fee_academic_year: $scope.temp.sims_fee_academic_year
                                , sims_enroll_number: $scope.chck_obj[i].s_enroll_no
                                , sims_concession_number: $scope.temp.sims_concession_number
                                , sims_concession_from_date: $scope.temp.sims_concession_from_date
                                , sims_concession_to_date: $scope.temp.sims_concession_to_date
                                , sims_created_by: $scope.temp.sims_created_by
                                , sims_created_date: $scope.temp.sims_created_date
                                , sims_status: $scope.temp.sims_status
                                , opr: 'I'
                                }

                                datasendObject.push(data1);

                            }

                            $http.post(ENV.apiUrl + "api/common/ConcessionTransaction/CUConcessionTransaction", datasendObject).then(function (res) {
                                $scope.Message = res.data;

                                if ($scope.Message != "") {
                                    swal({ text: $scope.Message, width: 320, showCloseButton: true });
                                    data();
                                }

                            });
                        }
                        else {

                            swal({ text: 'Select At Least One Student To Insert Record', width: 380, showCloseButton: true });
                        }

                    }


                }
            }

            $scope.Global_Search_modal = function () {
                $scope.global_Search['gradecode']=''.toString();
                $scope.global_Search['sectioncode'] = '';
                $scope.global_Search['nationalitycode'] = ''; 
                $scope.global_Search['nationalityID'] = ''; 
                $scope.global_Search['glbl_enroll_num'] = '';
                $scope.global_Search['glbl_name'] = '';
                $scope.global_Search['glbl_transport_bus'] = '';
                $scope.global_Search['glbl_pass_num'] = '';
                $scope.global_Search['glbl_family_name'] = '';

                var Checkbox = document.getElementById('Checkbox1');
                if (Checkbox.checked == true) {
                    Checkbox.checked = false;
                }

                $scope.studentdata = false;
                $('#myModal').modal({ backdrop: "static" });

                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getAllGradeName").then(function (res) {
                    $scope.All_grade_names = res.data;
                });
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getAllNationality").then(function (res) {
                    $scope.All_Nationality_names = res.data;
                });

            }

            $scope.getallsectioncode = function (gr_code) {
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getAllSectionName?gradecode=" + gr_code).then(function (res) {
                    $scope.All_Section_names = res.data;
                });
            }

            $scope.Global_Search_by_student = function () {

                $scope.glbl_obj = {
                    s_cur_code: $scope.global_Search.global_curriculum_code,
                    search_std_enroll_no: $scope.global_Search.glbl_enroll_num,
                    search_std_name: $scope.global_Search.glbl_name,
                    search_std_grade_name: $scope.global_Search.gradecode,
                    search_std_section_name: $scope.global_Search.sectioncode,
                    search_std_passport_no: $scope.global_Search.glbl_pass_num,
                    search_std_family_name: $scope.global_Search.glbl_family_name,
                    sims_academic_year: $scope.global_Search.global_acdm_yr,
                    sims_nationality_name_en: $scope.global_Search.nationalitycode,
                    std_national_id: $scope.global_Search.nationalityID
                }
                myarray = []; $scope.global_search_result = "";
                $scope.busy = true;
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.glbl_obj)).then(function (res) {
                    $scope.global_search_result1 = res.data;

                    $scope.studenttable = true;
                    $scope.studentdata = true;
                    $scope.busy = false;

                    for (var i = 0; i < $scope.global_search_result1.length; i++) {
                        for (var j = 0; j < $scope.filteredTodos.length; j++) {
                            if ($scope.global_search_result1[i].s_enroll_no == $scope.filteredTodos[j].sims_enroll_number && $scope.global_Search.global_acdm_yr == $scope.temp.sims_fee_academic_year && $scope.global_Search.global_curriculum_code == $scope.temp.sims_cur_short_name && $scope.filteredTodos[j].sims_concession_number == $scope.temp.sims_concession_number) {
                                $scope.global_search_result1[i].ischecked = true;
                                $scope.global_search_result1[i].disabled1 = true;
                            }
                        }
                    }

                    $scope.global_search_result = $scope.global_search_result1;
                });
            }

            $scope.Reset = function () {

                $scope.global_Search['gradecode'] = ''.toString();
                $scope.global_Search['sectioncode'] = '';
                $scope.global_Search['nationalitycode'] = '';
                $scope.global_Search['nationalityID'] = '';
                $scope.global_Search['glbl_enroll_num'] = '';
                $scope.global_Search['glbl_name'] = '';
                $scope.global_Search['glbl_transport_bus'] = '';
                $scope.global_Search['glbl_pass_num'] = '';
                $scope.global_Search['glbl_family_name'] = '';
            }

            var dom;
            $scope.flag = true;
            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='12'>" +
                    "<table class='inner-table' width='100%' cellpadding='5' cellspacing='0' style='border-color:red;border:solid;border-width:02px'>" +
                    "<tbody>" +

                     "<tr> <td class='semi-bold'>" + "CONCESSION NUMBER" + "</td> <td class='semi-bold'>" + "CONCESSION FROM DATE" + "</td> <td class='semi-bold'>" + "CONCESSION TO DATE" + " </td><td class='semi-bold'>" + "CRATED BY" + "</td><td class='semi-bold'>"
                      + "CRATED DATE" + "</td></tr>" +

                       "<tr><td>" + (info.sims_concession_number) + "</td> <td>" + (info.sims_concession_from_date) + "</td> <td>" + (info.sims_concession_to_date) + " </td><td>" + (info.sims_created_by) + "</td><td>"
                       + (info.sims_created_date)
                       + "</td></tr>" +
                       " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }

            };

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $scope.size = function (str) {

                if (str == "All") {

                    $scope.filteredTodos = $scope.obj;
                    $scope.todos = $scope.obj;
                    $scope.totalItems = $scope.obj.length;
                    $scope.numPerPage = $scope.obj.length;
                    $scope.makeTodos();

                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }

            }

            $scope.checkAll = function () {

                main = document.getElementById('mainchk');
                del = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_transaction_number;
                    var v = document.getElementById(t);
                    if (main.checked == true) {
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                    else {
                        v.checked = false;
                        $scope.row1 = ''
                        $('tr').removeClass("row_selected");
                    }

                }
            }

            $scope.checkAny = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }

            }

            $scope.checkDate = function (from, to) {
                if (from > to) {
                    swal({ text: 'Select Future date', width: 300, showCloseButton: true });
                    $scope.temp.sims_concession_to_date = '';
                }

            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {

                console.log(item);

                return (item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.student_full_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.Delete = function () {

                var datasend = [];

                var sims_enroll_number = '';
                var sims_transaction_number = '';
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_transaction_number);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var data1 = {
                            opr: 'D',
                            sims_enroll_number: sims_enroll_number + $scope.filteredTodos[i].sims_enroll_number,
                            sims_transaction_number: sims_transaction_number + $scope.filteredTodos[i].sims_transaction_number
                        }
                        datasend.push(data1);
                    }
                }

                if ($scope.flag) {
                    swal({
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/common/ConcessionTransaction/SimsConcessionTransactionDelete", datasend).then(function (res) {
                                $scope.CUDobj = res.data;
                                swal({ text: $scope.CUDobj.strMessage, width: 380, showCloseButton: true });
                                data();
                            })
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                                $scope.row1 = '';
                            }
                            $scope.checkAll();
                        }
                    })
                }
                else {
                    swal({ text: 'Select At Least One Record To Delete', width: 380, showCloseButton: true });
                }
            }

            $scope.check = function () {

                var data2 = [];
                var data3 = [];
                var Checkbox = document.getElementById('Checkbox1');
                for (var i = 0; i < $scope.global_search_result.length; i++) {

                    var t = document.getElementById($scope.global_search_result[i].s_enroll_no);
                    if (Checkbox.checked == true) {
                        t.checked = true;
                        data2.push({ id: $scope.global_search_result[i].s_enroll_no, name: $scope.global_search_result[i].s_sname_in_english });
                        data3.push($scope.global_search_result[i].s_enroll_no);
                    }
                    else {
                        t.checked = false;
                        Checkbox.checked = false;
                        del = [];
                        myarray = [];
                    }
                }

                del = data2;
                myarray = data3;
                $scope.chck_obj = del;

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#Table3").tableHeadFixer({ 'top': 1 });
                $("#Table1").tableHeadFixer({ 'top': 1 });

            }, 100);
        }]
        )
})();