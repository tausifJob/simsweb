﻿(function () {
    'use strict';
    angular.module('sims.module.Fee', [
        'sims',
        'bw.paging',
        'gettext'
    ]);
})();