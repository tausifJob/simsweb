﻿
(function () {
    'use strict';
    var feenumber = [];
    var copy_to_sectionnumber = [];
    var main;
    var grade_code;
    var cur_code;
    var section_code1, academic_years1, fee_Category1;
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CopySectionFeeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.table = false;
            $scope.table1 = false;

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;
                $scope.opration = false;
                $scope.display = true;

            })

            $scope.getacyr = function (str) {
                $http.get(ENV.apiUrl + "api/common/SectionFees/getCurrentAdvanceAY?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    console.log($scope.Academic_year);
                })
            }

            $scope.getacyr1 = function (str) {
                $http.get(ENV.apiUrl + "api/common/SectionFees/getCurrentAdvanceAY?curcode=" + str).then(function (Academicyear1) {
                    $scope.Academic_year1 = Academicyear1.data;

                })
            }

            $scope.getgradecodes = function () {

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.section_fee_academic_year).then(function (res1) {
                    $scope.grade1 = res1.data;
                })

            }

            $scope.getgradecodes1 = function () {

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.section_fee_academic_year).then(function (res1) {

                    $scope.grade = res1.data;
                });

            }

            $http.get(ENV.apiUrl + "api/common/SectionFees/getAllFeeCategoryNames").then(function (AllFeeCategoryNames) {
                $scope.FeeCategoryNames = AllFeeCategoryNames.data;
                console.log($scope.grade);
            })

            $scope.getsection = function () {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.edt.section_fee_academic_year).then(function (res) {
                    $scope.section = res.data;

                })
            };

            $scope.alldata = function () {
                $scope.busy = true;
                $http.get(ENV.apiUrl + "api/common/SectionFees/getSectionFeeByIndex?cur_name=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.section_fee_academic_year + "&grade_name=" + $scope.edt.sims_grade_code + "&section_name=" + $scope.edt.sims_section_code + "&fee_category=" + $scope.edt.section_fee_category + "&PageIndex=1&PageSize=5").then(function (fee_list) {
                    $scope.allfee_list = fee_list.data;
                    $scope.busy = false;
                    $scope.table = true;
                });

            }

            $scope.GradeSectionName = function () {
                $scope.busy1 = true;
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.temp.sims_cur_code + "&grade_code=" + $scope.temp.sims_grade_code + "&academic_year=" + $scope.temp.section_fee_academic_year).then(function (AllGradeSectionName) {

                    var data = [];
                    var data1 = [];
                    $scope.sectiondataobj = AllGradeSectionName.data;

                    debugger;

                    for (var i = 0; i < $scope.sectiondataobj.length; i++) {
                        if ($scope.sectiondataobj[i].sims_section_code != $scope.edt.sims_section_code) {
                            data = {
                                sims_section_code: $scope.sectiondataobj[i].sims_section_code,
                                sims_section_name: $scope.sectiondataobj[i].sims_section_name
                            };
                            data1.push(data)
                        }
                    }
                    //$scope.filteredTodos = data1;
                    $scope.filteredTodos = $scope.sectiondataobj;
                    $scope.busy1 = false;
                    $scope.table1 = true;
                });


            }

            var allfee = [];

            $scope.Save = function () {

                copy_to_sectionnumber = [];
                feenumber = [];

                for (var i = 0; i < $scope.allfee_list.length; i++) {
                    var t = $scope.allfee_list[i].section_fee_code;
                    var v = document.getElementById(t);

                    if (v.checked == true)

                        feenumber = feenumber + $scope.allfee_list[i].section_fee_code + ',';
                }

                for (var j = 0; j < $scope.filteredTodos.length ; j++) {

                    var t = $scope.filteredTodos[j].sims_section_code+$scope.filteredTodos[j].sims_section_name;
                    var v = document.getElementById(t);
                    if (v.checked == true)
                        copy_to_sectionnumber = copy_to_sectionnumber + $scope.filteredTodos[j].sims_section_code + ',';
                }

                var copy_section_fee_from = ({

                    'sims_cur_code': $scope.edt.sims_cur_code,
                    'sims_academic_year': $scope.edt.section_fee_academic_year,
                    'sims_fee_cat': $scope.edt.section_fee_category,
                    'sims_grade_code': $scope.edt.sims_grade_code,
                    'sims_fee_code': feenumber,
                    'sims_section_code': $scope.edt.sims_section_code
                });

                var copy_section_fee_to = ({
                    'sims_cur_code': $scope.temp.sims_cur_code,
                    'sims_academic_year': $scope.temp.section_fee_academic_year,
                    'sims_grade_code': $scope.temp.sims_grade_code,
                    'sims_section_code': copy_to_sectionnumber

                });

                if (feenumber.length > 0 && copy_to_sectionnumber.length > 0) {

                    $http.post(ENV.apiUrl + "api/common/SectionFees/CUDSectionFeeDetails?fromstr=" + JSON.stringify(copy_section_fee_from) + "&tostr=" + JSON.stringify(copy_section_fee_to)).then(function (msg) {

                        $scope.msg1 = msg.data

                        swal({ text: $scope.msg1.strMessage, width: 380 });
                        copy_to_sectionnumber = [];
                        feenumber = [];
                        copy_to_sectionnumber = [];
                        $scope.row1 = '';
                        main.checked = false;
                        allfee = [];
                        $scope.temp = '';
                        $scope.edt = '';
                        $scope.table1 = false;
                        $scope.table = false;

                    });
                }
                else {
                    swal({ text: 'Please Secect FeeType And Section Code', width: 380 });
                }


            }

            $scope.CheckAllChecked = function () {


                main = document.getElementById('mainchk');
                feenumber = [];
                if (main.checked == true) {
                    for (var i = 0; i < $scope.allfee_list.length; i++) {
                        var t = $scope.allfee_list[i].section_fee_code;
                        var v = document.getElementById(t);
                        v.checked = true;
                        feenumber = feenumber + $scope.allfee_list[i].section_fee_code + ','
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.allfee_list.length; i++) {
                        var t = $scope.allfee_list[i].section_fee_code;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;

                        $scope.row1 = '';
                    }
                }
                console.log(feenumber);
            }

            var main1 = '';

            $scope.select_all_feenumber_to_copy = function () {

                main1 = document.getElementById('Checkbox1');
                feenumber = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_section_code + $scope.filteredTodos[i].sims_section_name;
                    var v = document.getElementById(t);
                    if (main1.checked == true) {
                        v.checked = true;
                        copy_to_sectionnumber = copy_to_sectionnumber + $scope.filteredTodos[i].sims_section_code + ','
                        $scope.row2 = 'row_selected';
                    }
                    else {
                        v.checked = false;
                        main1.checked = false;
                        $scope.row2 = '';
                    }
                }

            }
            $scope.copy_to_section_onebyone_number = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }
            }

            $scope.cancel = function () {
                $scope.table1 = false;
                $scope.table = false;
                $scope.edt = '';
                $scope.temp = '';
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);

        }])
})();

