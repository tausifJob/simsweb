﻿(function () {
    'use strict';
    var opr = '';
    var vehiclecode = [];
    var main;
    var data1 = [];

    var simsController = angular.module('sims.module.Fleet');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TransportVehicleCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.temp = "";
            $scope.table1 = true;
            $scope.operation = false;
            $scope.veh_age = true;

            $http.get(ENV.apiUrl + "api/TransportVehicle/getTransportVehicle").then(function (Vehicle_Data) {
                $scope.VehicleData = Vehicle_Data.data;
                $scope.totalItems = $scope.VehicleData.length;
                $scope.todos = $scope.VehicleData;
                $scope.makeTodos();
                console.log($scope.VehicleData);
            });

            $http.get(ENV.apiUrl + "api/common/getAllOwnership").then(function (get_Ownership) {
                $scope.Ownership = get_Ownership.data;

            })

            $http.get(ENV.apiUrl + "api/common/getAllTransmission").then(function (get_transmission) {
                $scope.transmission = get_transmission.data;

            })

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['check_all'] = false;
                $scope.row1 = '';
                // main.checked = false;
                // $scope.CheckAllChecked();
            }

            $scope.New = function () {


                $scope.check = true;

                $scope.editmode = false;
                $scope.newmode = true;
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;

                $scope.temp = {
                    sims_transport_manufacturer_name: "",
                    sims_transport_vehicle_camera_enabled: "",
                    sims_transport_vehicle_code: "",
                    sims_transport_vehicle_color: "",
                    sims_transport_vehicle_date_of_purchase: "",

                    sims_transport_vehicle_model_name: "",
                    sims_transport_vehicle_model_year: "",
                    sims_transport_vehicle_name_plate: "",
                    sims_transport_vehicle_ownership: "",
                    sims_transport_vehicle_ownership_name: "",

                    sims_transport_vehicle_power: "",
                    sims_transport_vehicle_registration_date: "",
                    sims_transport_vehicle_registration_expiry_date: "",
                    sims_transport_vehicle_registration_number: "",
                    sims_transport_vehicle_seating_capacity: "",

                    sims_transport_vehicle_security_enabled: "",
                    sims_transport_vehicle_status: "",
                    sims_transport_vehicle_transmission: "",
                    sims_transport_vehicle_transmission_name: "",

                    sims_transport_vehicle_insurance_no: "",
                    sims_transport_vehicle_insurance_issue_date: "",
                    sims_transport_vehicle_insurance_expiry_date: ""

                }

                $scope.vehicle_age = '';

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.up = function (str) {

                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.readonly = true;

                $scope.temp = {

                    sims_transport_manufacturer_name: str.sims_transport_manufacturer_name,
                    sims_transport_vehicle_camera_enabled: str.sims_transport_vehicle_camera_enabled,

                    sims_transport_vehicle_code: str.sims_transport_vehicle_code,
                    sims_transport_vehicle_color: str.sims_transport_vehicle_color,

                    sims_transport_vehicle_date_of_purchase: str.sims_transport_vehicle_date_of_purchase,
                    sims_transport_vehicle_model_name: str.sims_transport_vehicle_model_name,

                    sims_transport_vehicle_model_year: str.sims_transport_vehicle_model_year,
                    sims_transport_vehicle_name_plate: str.sims_transport_vehicle_name_plate,

                    sims_transport_vehicle_ownership: str.sims_transport_vehicle_ownership,
                    sims_transport_vehicle_ownership_name: str.sims_transport_vehicle_ownership_name,

                    sims_transport_vehicle_power: str.sims_transport_vehicle_power,


                    sims_transport_vehicle_registration_date: str.sims_transport_vehicle_registration_date,
                    sims_transport_vehicle_registration_expiry_date: str.sims_transport_vehicle_registration_expiry_date,

                    sims_transport_vehicle_registration_number: str.sims_transport_vehicle_registration_number,
                    sims_transport_vehicle_seating_capacity: str.sims_transport_vehicle_seating_capacity,

                    sims_transport_vehicle_security_enabled: str.sims_transport_vehicle_security_enabled,
                    sims_transport_vehicle_status: str.sims_transport_vehicle_status,

                    sims_transport_vehicle_transmission: str.sims_transport_vehicle_transmission,
                    sims_transport_vehicle_transmission_name: str.sims_transport_vehicle_transmission_name,
                    sims_transport_vehicle_insurance_no: str.sims_transport_vehicle_insurance_no,
                    sims_transport_vehicle_insurance_cmp: str.sims_transport_vehicle_insurance_cmp,
                    sims_transport_vehicle_insurance_issue_date: str.sims_transport_vehicle_insurance_issue_date,
                    sims_transport_vehicle_insurance_expiry_date: str.sims_transport_vehicle_insurance_expiry_date
                }
                debugger
                if (str.sims_transport_vehicle_model_year == "" || str.sims_transport_vehicle_model_year == null) {
                    $scope.vehicle_age = 0;
                }
                else {
                    $scope.vehicle_age = (2016 - str.sims_transport_vehicle_model_year);

                }
            }

            $scope.cancel = function () {

                $scope.table1 = true;
                $scope.operation = false;

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

                $scope.temp = {
                    sims_transport_manufacturer_name: "",
                    sims_transport_vehicle_camera_enabled: "",
                    sims_transport_vehicle_code: "",
                    sims_transport_vehicle_color: "",
                    sims_transport_vehicle_date_of_purchase: "",

                    sims_transport_vehicle_model_name: "",
                    sims_transport_vehicle_model_year: "",
                    sims_transport_vehicle_name_plate: "",
                    sims_transport_vehicle_ownership: "",
                    sims_transport_vehicle_ownership_name: "",

                    sims_transport_vehicle_power: "",
                    sims_transport_vehicle_registration_date: "",
                    sims_transport_vehicle_registration_expiry_date: "",
                    sims_transport_vehicle_registration_number: "",
                    sims_transport_vehicle_seating_capacity: "",

                    sims_transport_vehicle_security_enabled: "",
                    sims_transport_vehicle_status: "",
                    sims_transport_vehicle_transmission: "",
                    sims_transport_vehicle_transmission_name: "",

                    sims_transport_vehicle_insurance_no: "",
                    sims_transport_vehicle_insurance_cmp: "",
                    sims_transport_vehicle_insurance_issue_date: "",
                    sims_transport_vehicle_insurance_expiry_date: ""

                }

                $scope.vehicle_age = '';
            }

            $scope.Save = function (myForm) {
                if (myForm) {
                    data1 = [];
                    var data = $scope.temp;
                    data.opr = 'I';
                    $scope.exist = false;
                    for (var i = 0; i < $scope.VehicleData.length; i++) {
                        if ($scope.VehicleData[i].sims_transport_vehicle_code == data.sims_transport_vehicle_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ title: 'Alert', text: "Record Already Exists", width: 300, height: 200 })
                    }
                    else {
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/TransportVehicle/CUDTransportVehicle", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                            }
                            $http.get(ENV.apiUrl + "api/TransportVehicle/getTransportVehicle").then(function (Vehicle_Data) {
                                $scope.VehicleData = Vehicle_Data.data;
                                $scope.totalItems = $scope.VehicleData.length;
                                $scope.todos = $scope.VehicleData;
                                $scope.makeTodos();
                            });

                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                }
            }

            $scope.Update = function (myForm) {
                if (myForm) {
                    data1 = [];
                    var data = $scope.temp;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/TransportVehicle/CUDTransportVehicle", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                        $scope.operation = false;
                        $http.get(ENV.apiUrl + "api/TransportVehicle/getTransportVehicle").then(function (Vehicle_Data) {
                            $scope.VehicleData = Vehicle_Data.data;
                            $scope.totalItems = $scope.VehicleData.length;
                            $scope.todos = $scope.VehicleData;
                            $scope.makeTodos();
                        });
                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                debugger
            };

            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_transport_vehicle_code + i);
                    if (main.checked == true) {
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }

                    else {
                        v.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.deleterecord = function () {
                vehiclecode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_transport_vehicle_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deleteroutecode = ({
                            'sims_transport_vehicle_code': $scope.filteredTodos[i].sims_transport_vehicle_code,
                            opr: 'D'
                        });
                        vehiclecode.push(deleteroutecode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/TransportVehicle/CUDTransportVehicle", vehiclecode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/TransportVehicle/getTransportVehicle").then(function (Vehicle_Data) {
                                                $scope.VehicleData = Vehicle_Data.data;
                                                $scope.totalItems = $scope.VehicleData.length;
                                                $scope.todos = $scope.VehicleData;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/TransportVehicle/getTransportVehicle").then(function (Vehicle_Data) {
                                                $scope.VehicleData = Vehicle_Data.data;
                                                $scope.totalItems = $scope.VehicleData.length;
                                                $scope.todos = $scope.VehicleData;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                $scope.chk = {}
                                                $scope.chk['check_all'] = false;
                                                //main.checked = false;
                                                $scope.row1 = '';
                                            }
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_transport_vehicle_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }

                $scope.currentPage = true;

            }

            $scope.searchd = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                debugger
                $scope.todos = $scope.searchd($scope.VehicleData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.VehicleData;
                }
                $scope.makeTodos();

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                debugger
                return (
                         item.sims_transport_vehicle_name_plate.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.sims_transport_vehicle_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.sims_transport_vehicle_registration_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.sims_transport_vehicle_seating_capacity == toSearch) ? true : false;

            }

            $scope.getvehicle_age = function (age) {

                if (age == 0 || age == '' || age == 'undefined') {
                    $scope.vehicle_age = 0;

                }
                else if (age > 2016) {
                    swal({ title: "Alert", text: "Vehicle Model year shouldn't be greater than current year ", width: 300, height: 200 });
                    $scope.vehicle_age = 0;
                    $scope.temp.sims_transport_vehicle_model_year = 0;
                }
                else {
                    var vehicle_age = (2016 - age);
                    $scope.vehicle_age = vehicle_age;
                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.Checkdate = function () {

                if ($scope.temp.sims_transport_vehicle_registration_date < $scope.temp.sims_transport_vehicle_date_of_purchase) {
                    swal({ title: "Alert", text: "Registration Date Should be greater than Purchase Date", width: 300, height: 200 });
                    $scope.temp.sims_transport_vehicle_registration_expiry_date = '';
                }

                //if ($scope.temp.sims_transport_vehicle_registration_expiry_date < $scope.temp.sims_transport_vehicle_registration_date) {
                //    swal({ title: "Alert", text: "Expiry Date Should be greater than Registration Date", width: 300, height: 200 });
                //    $scope.temp.sims_transport_vehicle_registration_expiry_date = '';
                //}
            }

            $scope.Comparedate = function () {

                if ($scope.temp.sims_transport_vehicle_registration_expiry_date < $scope.temp.sims_transport_vehicle_registration_date) {
                    swal({ title: "Alert", text: "Expiry Date Should be greater than Registration Date", width: 300, height: 200 });
                    $scope.temp.sims_transport_vehicle_registration_expiry_date = '';
                }
            }

            $scope.CompareInsurancedate = function () {
                debugger
                if ($scope.temp.sims_transport_vehicle_insurance_expiry_date < $scope.temp.sims_transport_vehicle_insurance_issue_date) {
                    swal({ title: "Alert", text: "Insurance Expiry Date Should be greater than Insurance Date", width: 300, height: 200 });
                    $scope.temp.sims_transport_vehicle_insurance_expiry_date = '';
                }
            }

            $scope.CompareInsuranceFromdate = function () {
                debugger
                if ($scope.temp.sims_transport_vehicle_insurance_issue_date < $scope.temp.sims_transport_vehicle_registration_date) {
                    swal({ title: "Alert", text: "Insurance Date Should be greater than Registration Date", width: 300, height: 200 });
                    $scope.temp.sims_transport_vehicle_insurance_expiry_date = '';
                }
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);
        }])
})();