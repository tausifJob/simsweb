﻿(function () {
    'use strict';
    var opr = '';
    var routecode = [];
    var main;
    var data1 = [];

    var simsController = angular.module('sims.module.Fleet');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TransportRouteCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';

            $scope.temp = "";
            $scope.table1 = true;

            $scope.propertyName = null;
            $scope.reverse = false;
            // propertyName = null; reverse = false
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.countData = [
             { val: 5, data: 5 },
             { val: 10, data: 10 },
             { val: 15, data: 15 },

            ]
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;
            $scope.size = function (str) {
                if (str == 5 || str == 10 || str == 15) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $http.get(ENV.apiUrl + "api/TransportRoute/getTransportRoute").then(function (get_Route_Data) {
                $scope.operation = false;
                $scope.table1 = true;
                $scope.Route_Data = get_Route_Data.data;
                if ($scope.Route_Data.length > 0) {
                    $scope.pager = true;
                    if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.Route_Data.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.Route_Data.length, data: 'All' })
                    }

                    $scope.totalItems = $scope.Route_Data.length;
                    $scope.todos = $scope.Route_Data;
                    $scope.makeTodos();
                    $scope.table1 = true;
                }
                else {
                    swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                    $scope.filteredTodos = [];
                }

            });


            $scope.getgrid = function () {
                $scope.currentPage = 1;

                $http.get(ENV.apiUrl + "api/TransportRoute/getTransportRoute").then(function (get_Route_Data) {
                    $scope.operation = false;
                    $scope.table1 = true;
                    $scope.Route_Data = get_Route_Data.data;
                    if ($scope.Route_Data.length > 0) {
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.Route_Data.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.Route_Data.length, data: 'All' })
                        }

                        $scope.totalItems = $scope.Route_Data.length;
                        $scope.todos = $scope.Route_Data;
                        $scope.makeTodos();
                        $scope.table1 = true;
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                        $scope.filteredTodos = [];
                    }

                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
                $scope.row1 = '';
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            //$http.get(ENV.apiUrl + "api/TransportRoute/getTransportRoute").then(function (get_Route_Data) {
            //    $scope.Route_Data = get_Route_Data.data;
            //    $scope.totalItems = $scope.Route_Data.length;
            //    $scope.todos = $scope.Route_Data;
            //    $scope.makeTodos();

            //});

            $http.get(ENV.apiUrl + "api/common/getAllAcademicYearstop").then(function (get_AllAcademicYear) {
                $scope.ActiveYear = get_AllAcademicYear.data;
            });


            $http.get(ENV.apiUrl + "api/common/getRouteDirec").then(function (get_RouteDirec) {
                $scope.RouteDirec = get_RouteDirec.data;

                console.log($scope.RouteDirec);
            })

            $http.get(ENV.apiUrl + "api/common/getVehicleCode").then(function (get_VehicleCode) {
                $scope.VehicleCode = get_VehicleCode.data;

                console.log($scope.VehicleCode);
            })

            $http.get(ENV.apiUrl + "api/common/getDriverCode").then(function (get_DriverCode) {
                $scope.DriverCode = get_DriverCode.data;

                console.log($scope.DriverCode);
            })

            $http.get(ENV.apiUrl + "api/common/getCaretakerNames").then(function (get_CaretakerNames) {
                $scope.CaretakerNames = get_CaretakerNames.data;

                console.log($scope.CaretakerNames);
            })

            $scope.operation = false;
            $scope.editmode = false;

            

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
                //$('tr').removeClass("row_selected");

            }

            $scope.New = function () {
                $scope.temp = "";
                $scope.check = true;
                opr = 'S';
                $scope.editmode = false;
                $scope.newmode = true;
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.up = function (str) {
                opr = 'U';
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.readonly = true;
                $scope.temp = "";
                $scope.temp = {

                    sims_academic_year: str.sims_academic_year,
                    sims_academic_year_description: str.sims_academic_year_description,
                    sims_transport_route_code: str.sims_transport_route_code,
                    sims_transport_route_direction: str.sims_transport_route_direction,
                    sims_transport_route_direction_name: str.sims_transport_route_direction_name,
                    sims_transport_route_name: str.sims_transport_route_name,
                    sims_transport_route_short_name: str.sims_transport_route_short_name,

                    sims_transport_route_vehicle_code: str.sims_transport_route_vehicle_code,
                    sims_transport_vehicle_name_plate: str.sims_transport_vehicle_name_plate,

                    sims_transport_route_driver_code: str.sims_transport_route_driver_code,
                    sims_driver_name: str.sims_driver_name,

                    sims_transport_route_caretaker_code: str.sims_transport_route_caretaker_code,
                    sims_caretaker_name: str.sims_caretaker_name,

                    sims_transport_route_status: str.sims_transport_route_status,
                }
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
               
            }

            $scope.Save = function (myForm) {
                
                if (myForm) {
                    data1 = [];
                    var data = $scope.temp;
                    data.opr = 'I';
                    debugger
                    $scope.exist = false;
                    for (var i = 0; i < $scope.Route_Data.length; i++) {
                        if ($scope.Route_Data[i].sims_transport_route_code == data.sims_transport_route_code && $scope.Route_Data[i].sims_academic_year == data.sims_academic_year && $scope.Route_Data[i].sims_transport_route_direction == data.sims_transport_route_direction) {
                            $scope.exist = true;
                        }

                    }
                    if ($scope.exist) {
                        swal({ title: 'Alert', text: "Record Already Exists", width: 300, height: 200 });
                    }
                    else {
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/TransportRoute/CUDTransportRoute", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                            }

                            $scope.getgrid();

                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                  
                }

            }

            $scope.Update = function (myForm) {
                
                if (myForm) {
                    data1 = [];
                    var data = $scope.temp;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/TransportRoute/CUDTransportRoute", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                        $scope.operation = false;
                        $scope.getgrid();
                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                  
                }
            }

         

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_code + i);
                    if (main.checked == true) {
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }

                    else {
                        v.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            //$scope.CheckAllChecked = function () {
            //    main = document.getElementById('mainchk');
            //    if (main.checked == true) {
            //        for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //            var v = document.getElementById(i);
            //            v.checked = true;
            //            $scope.row1 = 'row_selected';
            //            $('tr').addClass("row_selected");
            //        }
            //    }
            //    else {

            //        for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //            var v = document.getElementById(i);
            //            v.checked = false;
            //            main.checked = false;
            //            $scope.row1 = '';
            //        }
            //    }

            //}
            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.deleterecord = function () {
                routecode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deleteroutecode = ({
                            'sims_transport_route_code': $scope.filteredTodos[i].sims_transport_route_code,
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            'sims_transport_route_direction': $scope.filteredTodos[i].sims_transport_route_direction,
                            opr: 'D'
                        });
                        routecode.push(deleteroutecode);
                    }
                }
                debugger
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/TransportRoute/CUDTransportRoute", routecode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_transport_route_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
            
                $scope.currentPage = true;
               
            }




            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Route_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Route_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */

                return (
                     item.sims_transport_route_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_transport_route_short_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_transport_route_direction_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_caretaker_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_transport_route_code == toSearch) ? true : false;


            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


        }])
})();





