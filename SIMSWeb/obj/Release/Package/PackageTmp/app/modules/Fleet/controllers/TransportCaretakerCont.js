﻿(function () {
    'use strict';
    var opr = '';
    var caretakercode = [];
    var main;
    var data1 = [];
    var data = [];
    var simsController = angular.module('sims.module.Fleet');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('TransportCaretakerCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.disusr = false;
            $scope.disemp = false;
            $scope.table1 = true;
            $scope.operation = false;
            $scope.editmode = false;
            $scope.caretaker_email = false;

             $scope.imageUrl = ENV.apiUrl + '/Content/' + $http.defaults.headers.common['schoolId'] + '/Images/CaretakerImage/';
            //$scope.imageUrl = ENV.apiUrl + '/Content/sjs/Images/CaretakerImage/';

            //$scope.copy = 'Employee';


             $scope.temp1 =
                    {
                        sims_user: 'emp_true'

                    };

             $scope.get_ischecked = function (str) {
                 if (str == "user_true") {
                     $scope.temp1.sims_user = 'user_true';
                     $scope.stddis = true;
                     $scope.designation = false;
                     $scope.employeecarename = false;
                     $scope.caretaker_email = true;
                     $scope.caretaker_phone_no = true;

                 }
                 else {
                     $scope.temp1.sims_user = 'emp_true';
                     $scope.stddis = false;
                     $scope.designation = true;
                     $scope.employeecarename = true;
                     $scope.caretaker_email = false;
                     $scope.caretaker_phone_no = false;

                 }
             }

            //$scope.radioclick = function (str)
            //{
            //    if (str == "true")
            //    {
            //        $scope.stddis = true;
            //        // $scope.edt = '';
            //        $scope.edt.user = 'false';
            //        console.log($scope.edt.user);
            //    }
            //    else if (str == "false")
            //    {
            //        $scope.stddis = false;
            //        $scope.edt.emp = 'true';
            //        console.log($scope.edt.emp);
            //    }
            //}

             $http.get(ENV.apiUrl + "api/TransportCaretaker/getTransportCaretaker").then(function (TransportCaretaker_Data) {
                 $scope.TransportCaretaker = TransportCaretaker_Data.data;

                 $scope.totalItems = $scope.TransportCaretaker.length;
                 $scope.todos = $scope.TransportCaretaker;
                 $scope.makeTodos();
             });


             $scope.getgrid = function () {

                 $http.get(ENV.apiUrl + "api/TransportCaretaker/getTransportCaretaker").then(function (TransportCaretaker_Data) {
                     $scope.TransportCaretaker = TransportCaretaker_Data.data;

                     $scope.totalItems = $scope.TransportCaretaker.length;
                     $scope.todos = $scope.TransportCaretaker;
                     $scope.makeTodos();
                 });
             }

             $http.get(ENV.apiUrl + "api/TransportDriver/getPaysDesignation").then(function (getPaysDesignation_Data) {
                 $scope.Designation = getPaysDesignation_Data.data;
             });

             $scope.getcaretakeremployeename = function (desg_code) {
                 $http.get(ENV.apiUrl + "api/TransportCaretaker/getAllEmpName?desg_code=" + desg_code).then(function (get_AllEmp) {
                     $scope.get_AllEmpC = get_AllEmp.data;
                 })
             }

             $scope.getcaretakername = function (str) {
                 for (var i = 0; i < $scope.get_AllEmpC.length; i++) {
                     if ($scope.get_AllEmpC[i].sims_employee_code == str)
                         $scope.edt.sims_caretaker_name = $scope.get_AllEmpC[i].sims_employee_code_name;
                 }
             }

             $scope.size = function (str) {
                 console.log(str);
                 $scope.pagesize = str;
                 $scope.currentPage = 1;
                 $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
             }

             $scope.index = function (str) {
                 $scope.pageindex = str;
                 $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                 $scope.chk = {}
                 $scope.chk['check_all'] = false;
                 $scope.row1 = '';
             }

             $scope.New = function () {


                 var img = document.getElementById("file1");
                 img = "";
                 $scope.sims_driver_img = "";
                 $scope.prev_img = "";
                 $scope.stddis = false;
                 $scope.disusr = false;
                 $scope.disemp = false;
                 $scope.designation = true;
                 $scope.employeecarename = true;

                 $scope.caretaker_email = false;
                 $scope.caretaker_phone_no = false;
                 $scope.check = true;
                 $scope.editmode = false;
                 $scope.newmode = true;
                 $scope.readonly = false;
                 $scope.table1 = false;
                 $scope.operation = true;
                 $scope.savebtn = true;
                 $scope.updatebtn = false;
                 $scope.edt = {
                     sims_employee_code: '',
                     sims_caretaker_name: '',
                     sims_caretaker_code: '',
                     sims_user_code: '',
                     sims_experience_years: '',
                     sims_caretaker_email: '',
                     comn_user_phone_number: ''
                 }
                 $scope.myForm.$setPristine();
                 $scope.myForm.$setUntouched();


                 $scope.temp1['sims_user'] = 'emp_true';

             }

             $scope.up = function (str) {

                 $scope.savebtn = false;
                 $scope.updatebtn = true;
                 $scope.table1 = false;
                 $scope.operation = true;
                 $scope.editmode = true;
                 $scope.newmode = false;
                 $scope.readonly = true;

                 $scope.sims_caretaker_img = $scope.imageUrl + str.sims_caretaker_img;


                 $scope.edt = {
                     sims_caretaker_img: str.sims_caretaker_img,
                     sims_caretaker_code: str.sims_caretaker_code,
                     sims_employee_code: str.sims_employee_code,
                     sims_employee_code_name: str.sims_employee_code_name,
                     sims_caretaker_name: str.sims_caretaker_name,
                     sims_experience_years: str.sims_experience_years,
                     sims_caretaker_status: str.sims_caretaker_status,
                     sims_caretaker_email: str.sims_caretaker_email,
                     comn_user_phone_number: str.comn_user_phone_number,
                     sims_user_code: str.sims_user_code
                 }
                 console.log($scope.edt.sims_employee_code);


                 if (str.sims_employee_code == "" || str.sims_employee_code == null) {
                     $scope.temp1 = {
                         sims_user: 'user_true'
                     }
                     $scope.disusr = true;
                     $scope.disemp = true;
                     $scope.designation = false;
                     $scope.caretaker_email = true;
                     $scope.caretaker_phone_no = true;
                     $scope.employeecarename = false;
                 }
                 else {
                     $scope.temp1 = {
                         sims_user: 'emp_true'
                     }
                     $scope.disusr = true;
                     $scope.disemp = true;
                     $scope.designation = false;
                     $scope.employeecarename = true;
                     $scope.caretaker_email = false;
                     $scope.caretaker_phone_no = false;
                 }

             }

             $scope.cancel = function () {

                 // $scope.getgrid();


                 var img = document.getElementById("file1");
                 img = "";
                 $scope.sims_driver_img = "";
                 $scope.prev_img = "";
                 $scope.table1 = true;
                 $scope.operation = false;
                 $scope.myForm.$setPristine();
                 $scope.myForm.$setUntouched();
                 $scope.edt = {
                     sims_employee_code: '',
                     sims_caretaker_name: '',
                     sims_caretaker_code: '',
                     sims_user_code: '',
                     sims_experience_years: '',
                     sims_caretaker_email: '',
                     comn_user_phone_number: ''
                 }

                 $scope.temp1 =
                     {
                         sims_user: 'emp_true'

                     };

             }

             $scope.Save = function (myForm) {

                 if (myForm) {
                     data1 = [];
                     data = [];
                     data = $scope.edt;
                     data.opr = 'I';

                     $scope.exist = false;

                     for (var i = 0; i < $scope.TransportCaretaker.length; i++) {
                         if ($scope.TransportCaretaker[i].sims_user_code == data.sims_user_code) {
                             $scope.exist = true;
                         }
                     }
                     if ($scope.exist) {
                         swal({ title: 'Alert', text: "This User Code Already Exists", showCloseButton: true, width: 300, height: 200 })
                     }

                     else {
                         data1.push(data);
                         $http.post(ENV.apiUrl + "api/TransportCaretaker/CUDTransportCareTaker", data1).then(function (msg) {
                             $scope.msg1 = msg.data;
                             if ($scope.msg1 != '' || $scope.msg1 != null) {
                                 swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 });
                             }
                             else {
                                 swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 });
                             }
                             $http.get(ENV.apiUrl + "api/TransportCaretaker/getTransportCaretaker").then(function (TransportCaretaker_Data) {
                                 $scope.TransportCaretaker = TransportCaretaker_Data.data;
                                 $scope.totalItems = $scope.TransportCaretaker.length;
                                 $scope.todos = $scope.TransportCaretaker;
                                 $scope.makeTodos();
                             });
                         });
                     }
                     $scope.table1 = true;
                     $scope.operation = false;
                 }
             }

             $scope.Update = function (myForm) {
                 debugger
                 if (myForm) {
                     data1 = [];
                     data = $scope.edt;
                     data.opr = 'U';
                     data1.push(data);
                     $http.post(ENV.apiUrl + "api/TransportCaretaker/UpdateTransportCareTaker", data1).then(function (msg) {
                         $scope.msg1 = msg.data;
                         if ($scope.msg1 == true) {
                             swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                         }
                         else {
                             swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                         }
                         $scope.operation = false;
                         $http.get(ENV.apiUrl + "api/TransportCaretaker/getTransportCaretaker").then(function (TransportCaretaker_Data) {
                             $scope.TransportCaretaker = TransportCaretaker_Data.data;
                             $scope.totalItems = $scope.TransportCaretaker.length;
                             $scope.todos = $scope.TransportCaretaker;
                             $scope.makeTodos();
                         });
                     })

                     $scope.operation = false;
                     $scope.table1 = true;
                 }
             }

             $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

             $scope.makeTodos = function () {
                 var rem = parseInt($scope.totalItems % $scope.numPerPage);
                 if (rem == '0') {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                 }
                 else {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                 }
                 var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                 var end = parseInt(begin) + parseInt($scope.numPerPage);

                 console.log("begin=" + begin); console.log("end=" + end);

                 $scope.filteredTodos = $scope.todos.slice(begin, end);
             };

             $scope.CheckAllChecked = function () {
                 main = document.getElementById('mainchk');
                 if (main.checked == true) {
                     for (var i = 0; i < $scope.filteredTodos.length; i++) {
                         var v = document.getElementById($scope.filteredTodos[i].sims_caretaker_code + i);
                         v.checked = true;
                         $scope.row1 = 'row_selected';
                         $('tr').addClass("row_selected");
                     }
                 }
                 else {

                     for (var i = 0; i < $scope.filteredTodos.length; i++) {
                         var v = document.getElementById($scope.filteredTodos[i].sims_caretaker_code + i);
                         v.checked = false;
                         main.checked = false;
                         $scope.row1 = '';
                     }
                 }

             }

             $scope.checkonebyonedelete = function () {

                 $("input[type='checkbox']").change(function (e) {
                     if ($(this).is(":checked")) {
                         $(this).closest('tr').addClass("row_selected");
                         $scope.color = '#edefef';
                     } else {
                         $(this).closest('tr').removeClass("row_selected");
                         $scope.color = '#edefef';
                     }
                 });

                 main = document.getElementById('mainchk');
                 if (main.checked == true) {
                     main.checked = false;
                     $("input[type='checkbox']").change(function (e) {
                         if ($(this).is(":checked")) {
                             $(this).closest('tr').addClass("row_selected");
                         }
                         else {
                             $(this).closest('tr').removeClass("row_selected");
                         }
                     });
                 }
             }

             $scope.deleterecord = function () {
                 caretakercode = [];
                 $scope.flag = false;
                 for (var i = 0; i < $scope.filteredTodos.length; i++) {
                     var v = document.getElementById($scope.filteredTodos[i].sims_caretaker_code + i);
                     if (v.checked == true) {
                         $scope.flag = true;
                         var deleteroutecode = ({
                             'sims_caretaker_code': $scope.filteredTodos[i].sims_caretaker_code,
                             opr: 'D'
                         });
                         caretakercode.push(deleteroutecode);
                     }
                 }
                 debugger
                 if ($scope.flag) {
                     swal({
                         title: '',
                         text: "Are you sure you want to Delete?",
                         showCloseButton: true,
                         showCancelButton: true,
                         confirmButtonText: 'Yes',
                         width: 380,
                         cancelButtonText: 'No',

                     }).then(function (isConfirm) {
                         if (isConfirm) {
                             $http.post(ENV.apiUrl + "api/TransportCaretaker/TransportCaretakerDelete", caretakercode).then(function (msg) {
                                 $scope.msg1 = msg.data;
                                 if ($scope.msg1 == true) {
                                     swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                         if (isConfirm) {
                                             $http.get(ENV.apiUrl + "api/TransportCaretaker/getTransportCaretaker").then(function (TransportCaretaker_Data) {
                                                 $scope.TransportCaretaker = TransportCaretaker_Data.data;
                                                 $scope.totalItems = $scope.TransportCaretaker.length;
                                                 $scope.todos = $scope.TransportCaretaker;
                                                 $scope.makeTodos();
                                             });
                                             main = document.getElementById('mainchk');
                                             if (main.checked == true) {
                                                 main.checked = false;
                                                 {
                                                     $scope.row1 = '';
                                                 }
                                             }
                                         }
                                     });
                                 }
                                 else {
                                     swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                         if (isConfirm) {
                                             $http.get(ENV.apiUrl + "api/TransportCaretaker/getTransportCaretaker").then(function (TransportCaretaker_Data) {
                                                 $scope.TransportCaretaker = TransportCaretaker_Data.data;
                                                 $scope.totalItems = $scope.TransportCaretaker.length;
                                                 $scope.todos = $scope.TransportCaretaker;
                                                 $scope.makeTodos();
                                             });
                                             main = document.getElementById('mainchk');
                                             if (main.checked == true) {
                                                 main.checked = false;
                                                 {
                                                     $scope.row1 = '';
                                                 }
                                             }
                                         }
                                     });
                                 }

                             });
                         }
                         else {
                             for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                 var v = document.getElementById($scope.filteredTodos[i].sims_caretaker_code + i);
                                 if (v.checked == true) {
                                     v.checked = false;
                                     $scope.row1 = '';
                                     main.checked = false;
                                     $('tr').removeClass("row_selected");
                                 }
                             }
                         }
                     });
                 }
                 else {
                     swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                 }
                 $scope.currentPage = true;
             }

             $scope.searched = function (valLists, toSearch) {
                 return _.filter(valLists,
                 function (i) {
                     /* Search Text in all  fields */
                     return searchUtil(i, toSearch);
                 });
             };

             $scope.search = function () {
                 $scope.todos = $scope.searched($scope.TransportCaretaker, $scope.searchText);
                 $scope.totalItems = $scope.todos.length;
                 $scope.currentPage = '1';
                 if ($scope.searchText == '') {
                     $scope.todos = $scope.TransportCaretaker;
                 }
                 $scope.makeTodos();
             }

             function searchUtil(item, toSearch) {
                 /* Search Text in all 3 fields */
                 return (
                      item.sims_caretaker_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.sims_caretaker_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.sims_experience_years == toSearch) ? true : false;
             }

             $timeout(function () {
                 $("#fixTable").tableHeadFixer({ 'top': 1 });
             }, 100);

             var formdata = new FormData();

             $scope.getTheFiles = function ($files) {
                 $scope.filesize = true;

                 angular.forEach($files, function (value, key) {
                     formdata.append(key, value);

                     var i = 0;
                     if ($files[i].size > 800000) {
                         $scope.filesize = false;
                         $scope.edt.photoStatus = false;
                         swal({ title: "Alert", text: "File Should Not Exceed 800Kb.", imageUrl: "assets/img/notification-alert.png", });
                     }
                     else { }
                 });
             };

             $scope.file_changed = function (element) {


                 var photofile = element.files[0];

                 $scope.photo_filename = (photofile.name);

                 $scope.edt['sims_caretaker_img'] = $scope.photo_filename;

                 $scope.photo_filename = (photofile.type);
                 var reader = new FileReader();
                 reader.onload = function (e) {
                     $scope.$apply(function () {
                         $scope.prev_img = e.target.result;

                     });
                 };
                 reader.readAsDataURL(photofile);

                 if ($scope.edt.sims_employee_code == "" || $scope.edt.sims_employee_code == null) {
                     var request = {
                         method: 'POST',
                         url: ENV.apiUrl + 'api/file/upload?filename=' + $scope.edt.sims_caretaker_name + '&location=CaretakerImage',
                         data: formdata,
                         headers: {
                             'Content-Type': undefined
                         }
                     };

                 }
                 else {
                     var request = {
                         method: 'POST',
                         url: ENV.apiUrl + 'api/file/upload?filename=' + $scope.edt.sims_employee_code + '&location=CaretakerImage',
                         data: formdata,
                         headers: {
                             'Content-Type': undefined
                         }
                     };

                 }


                 $http(request).success(function (d) {

                     $scope.edt['sims_caretaker_img'] = $scope.edt.sims_caretaker_img;
                     $scope.edt['sims_caretaker_img'] = d;
                 });


             };




        }])

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

})();





