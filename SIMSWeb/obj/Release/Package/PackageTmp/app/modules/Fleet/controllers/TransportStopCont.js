﻿(function () {
    'use strict';
    var opr = '';
    var stopcode = [];
    var main;
    var data1 = [];

    var simsController = angular.module('sims.module.Fleet');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TransportStopCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';

            $scope.edt = "";
            $scope.table1 = true;
            
            $http.get(ENV.apiUrl + "api/TransportStop/getTransportStop").then(function (Transport_Stop) {
                $scope.TransportStop = Transport_Stop.data;
                $scope.totalItems = $scope.TransportStop.length;
                $scope.todos = $scope.TransportStop;
                $scope.makeTodos();

            });
            //$scope.Grid_Data();
            //$scope.Grid_Data = function () {
            //    $http.get(ENV.apiUrl + "api/TransportStop/getTransportStop").then(function (Transport_Stop) {
            //        $scope.TransportStop = Transport_Stop.data;
            //        $scope.totalItems = $scope.TransportStop.length;
            //        $scope.todos = $scope.TransportStop;
            //        $scope.makeTodos();

            //    });
            //}
            $http.get(ENV.apiUrl + "api/common/getAllAcademicYearstop").then(function (get_AllAcademicYear) {
                $scope.ActiveYear = get_AllAcademicYear.data;
            });

            $scope.propertyName = null;
            $scope.reverse = false;
            // propertyName = null; reverse = false
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };
            $scope.operation = false;
            $scope.editmode = false;

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                $scope.CheckAllChecked();
                main.checked = false;
                $scope.row1 = '';
                $scope.CheckAllChecked();
            }

            $scope.New = function () {
                
                $scope.edt = "";
                $scope.check = true;
                opr = 'S';
                $scope.editmode = false;
                $scope.newmode = true;
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.up = function (str) {
                opr = 'U';
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.readonly = true;
                $scope.edt = "";
                $scope.edt = {

                    sims_academic_year: str.sims_academic_year,
                    sims_academic_year_description: str.sims_academic_year_description,
                    sims_transport_stop_code: str.sims_transport_stop_code,
                    sims_transport_stop_name: str.sims_transport_stop_name,
                    sims_transport_stop_landmark: str.sims_transport_stop_landmark,

                    sims_transport_stop_lat: str.sims_transport_stop_lat,
                    sims_transport_stop_long: str.sims_transport_stop_long,

                    sims_transport_stop_status: str.sims_transport_stop_status
                   
                }
            }

            $scope.cancel = function () {
                
                $scope.edt = "";
              
                $scope.table1 = true;
                $scope.operation = false;
              
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

                $scope.edt={sims_transport_stop_code:"",
                sims_transport_stop_name :"",
                sims_transport_stop_landmark : "",
                sims_transport_stop_lat: "",
                sims_transport_stop_long :""
            }
            }

            $scope.Save = function (myForm) {
                
                if (myForm) {
                    data1 = [];
                    var data = $scope.edt;
                    data.opr = 'I';

                    $scope.exist = false;
                    for (var i = 0; i < $scope.TransportStop.length; i++) {
                        if ($scope.TransportStop[i].sims_transport_stop_code == data.sims_transport_stop_code && $scope.TransportStop[i].sims_academic_year == data.sims_academic_year) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ title: 'Alert', text: "Record Already Exists", width: 300, height: 200 })
                    }
                    else {
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/TransportStop/CUDTransportStop", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Inserted", width: 300, height: 200 });
                            }
                            $http.get(ENV.apiUrl + "api/TransportStop/getTransportStop").then(function (Transport_Stop) {
                                $scope.TransportStop = Transport_Stop.data;
                                $scope.totalItems = $scope.TransportStop.length;
                                $scope.todos = $scope.TransportStop;
                                $scope.makeTodos();
                            });
                            //$scope.Grid_Data();
                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                }
            }

            $scope.Update = function (myForm) {
                
                if (myForm) {
                    data1 = [];
                    var data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/TransportStop/CUDTransportStop", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                        $scope.operation = false;
                        $http.get(ENV.apiUrl + "api/TransportStop/getTransportStop").then(function (Transport_Stop) {
                            $scope.TransportStop = Transport_Stop.data;
                            $scope.totalItems = $scope.TransportStop.length;
                            $scope.todos = $scope.TransportStop;
                            $scope.makeTodos();

                        });
                        //$scope.Grid_Data();
                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };
            
            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_transport_stop_code + i);
                    if (main.checked == true) {
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }

                    else {
                        v.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.deleterecord = function () {
                stopcode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_transport_stop_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deleteroutecode = ({
                            'sims_transport_stop_code': $scope.filteredTodos[i].sims_transport_stop_code,
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            opr: 'D'
                        });
                        stopcode.push(deleteroutecode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/TransportStop/CUDTransportStop", stopcode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/TransportStop/getTransportStop").then(function (Transport_Stop) {
                                                $scope.TransportStop = Transport_Stop.data;
                                                $scope.totalItems = $scope.TransportStop.length;
                                                $scope.todos = $scope.TransportStop;
                                                $scope.makeTodos();
                                            });
                                            //$scope.Grid_Data();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/TransportStop/getTransportStop").then(function (Transport_Stop) {
                                                $scope.TransportStop = Transport_Stop.data;
                                                $scope.totalItems = $scope.TransportStop.length;
                                                $scope.todos = $scope.TransportStop;
                                                $scope.makeTodos();
                                            });
                                            //$scope.Grid_Data();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_transport_stop_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                           
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.TransportStop, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.TransportStop;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                     item.sims_transport_stop_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_transport_stop_landmark.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_transport_stop_lat.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_transport_stop_long.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_transport_stop_code == toSearch) ? true : false;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


        }])
})();





