﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Attendance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.directive('numberOnly', function () {
        return {
            restrict: 'A',
            link: function (scope, elem, attrs) {

                elem.bind('keypress', function (e) {

                    //if (e.which >= 48 && e.which <= 57) {
                    //    var ar = scope.j.temp.split(',');
                    //    if (ar[ar.length - 1].length >= 6) {
                    //        scope.j.temp = scope.j.temp + ',';
                    //    }
                    //    scope.$apply();
                    //}
                    var ar = scope.j.temp.split(',');
                    if (ar[ar.length - 1].length >= 6) {
                        scope.j.temp = scope.j.temp + ',';
                    }
                    scope.$apply();
                    if ([8, 13, 27, 37, 38, 39, 40].indexOf(event.which) > -1) {
                        // to allow backspace, enter, escape, arrows  
                        return true;
                    }



                });

            }
        }
    });

    simsController.controller('ClassWiseAttendanceCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.attend_date = null; $scope.display = false;
            $http.get(ENV.apiUrl + "api/classwiseattendance/getAttendanceCuriculum").then(function (res) {
                $scope.cur_obj = res.data;
                $scope.display = true;
            });


            $scope.getSectionNames = function (isvalid) {

                if (isvalid) {
                    $http.get(ENV.apiUrl + "api/classwiseattendance/getSectionNamesCurCodeWise?cur_code=" + $scope.sims_cur_code).then(function (res) {
                        $scope.section_names_obj = res.data;
                    });

                }
            }

            var chckbx_checked_list = [];

            $scope.chck_section_click = function (sec_code, grade_code, index) {
                debugger;
                var v = document.getElementById(sec_code + 1);//textbox
                var s = 'sec-' + index;
                var chck_bx = document.getElementById(s);//checkbox
                if (chck_bx.checked == true) {
                    v.disabled = false;
                    chckbx_checked_list.push({
                        'sims_section_code': sec_code,
                        'sims_grade_code': grade_code,
                        'index': s
                    });
                }
                else {
                    v.disabled = true;
                    // debugger
                    var index = chckbx_checked_list.map(function (e) { return e.sims_section_code; }).indexOf(sec_code);
                    // var index = chckbx_checked_list.indexOf(sec_code);
                    if (index > -1) {
                        chckbx_checked_list.splice(index, 1);
                        v.value = "";
                    }
                }
            }

            $scope.btn_present_click = function () {
                debugger;
                $scope.loading = true;
                for (var i = 0; i < chckbx_checked_list.length; i++) {
                    var t = document.getElementById(chckbx_checked_list[i].sims_section_code + 1);
                    var f = t.value;
                    chckbx_checked_list[i]['Enrollment_list'] = t.value;
                    chckbx_checked_list[i]['attendance_code'] = 'P';
                    chckbx_checked_list[i]['attedance_date'] = $scope.attend_date;
                    chckbx_checked_list[i]['sims_cur_code'] = $scope.sims_cur_code;
                }

                //console.log(JSON.stringify(chckbx_checked_list));

                $http.post(ENV.apiUrl + "api/classwiseattendance/classWiseStudentAttendanceUpdate", JSON.stringify(chckbx_checked_list)).then(function (res) {
                    $scope.result = res.data;
                    $scope.loading = false;
                    $scope.clear();
                    debugger;
                    if ($scope.result == true) {
                        // swal("Marked", "Student Attendance Marked Successfully", "success")
                        swal({ title: "Alert", text: "Student Attendance Marked Successfully", width: 300, height: 200 });
                    }
                });
            }

            $scope.btn_absent_click = function () {
                debugger;
                $scope.loading = true;
                for (var i = 0; i < chckbx_checked_list.length; i++) {
                    var t = document.getElementById(chckbx_checked_list[i].sims_section_code + 1);
                    var f = t.value;
                    chckbx_checked_list[i]['Enrollment_list'] = t.value;
                    chckbx_checked_list[i]['attendance_code'] = 'A';
                    chckbx_checked_list[i]['attedance_date'] = $scope.attend_date;
                    chckbx_checked_list[i]['sims_cur_code'] = $scope.sims_cur_code;
                }

                // console.log(JSON.stringify(chckbx_checked_list));

                $http.post(ENV.apiUrl + "api/classwiseattendance/classWiseStudentAttendanceUpdate", JSON.stringify(chckbx_checked_list)).then(function (res) {
                    $scope.result = res.data;
                    $scope.loading = false;

                    debugger;
                    if ($scope.result == true) {
                        //swal("Marked", "Student Attendance Marked Successfully", "success")
                        swal({ title: "Alert", text: "Student Attendance Marked Successfully", width: 300, height: 200 });
                    }
                    $scope.clear();
                });
            }

            $scope.clear = function () {
                debugger;
                for (var i = 0; i < chckbx_checked_list.length; i++) {
                    var t = document.getElementById(chckbx_checked_list[i].sims_section_code + 1);
                    t.value = "";
                    t.disabled = true;
                    var s = 'sec-' + i;
                    var c = document.getElementById(chckbx_checked_list[i].index);
                    c.checked = false;
                }
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            //for collapse main menu
            $('body').addClass('grey condense-menu');
            $('#main-menu').addClass('mini');
            $('.page-content').addClass('condensed');
            $rootScope.isCondensed = true;

        }]
        )
})();