﻿(function () {
    'use strict';
    var prv;
    var nxt;
    var simsController = angular.module('sims.module.Attendance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.directive('contextMenu', ["$parse", "$q", function ($parse, $q) {

        var contextMenus = [];

        var removeContextMenus = function (level) {
            while (contextMenus.length && (!level || contextMenus.length > level)) {
                contextMenus.pop().remove();
            }
            if (contextMenus.length == 0 && $currentContextMenu) {
                $currentContextMenu.remove();
            }
        };

        var $currentContextMenu = null;

        var renderContextMenu = function ($scope, event, options, model, level) {
            if (!level) { level = 0; }
            if (!$) { var $ = angular.element; }
            $(event.currentTarget).addClass('context');
            var $contextMenu = $('<div>');
            if ($currentContextMenu) {
                $contextMenu = $currentContextMenu;
            } else {
                $currentContextMenu = $contextMenu;
            }
            $contextMenu.addClass('dropdown clearfix');
            var $ul = $('<ul>');
            $ul.addClass('dropdown-menu');
            $ul.attr({ 'role': 'menu' });
            $ul.css({
                display: 'block',
                position: 'absolute',
                left: event.pageX + 'px',
                top: event.pageY + 'px',
                "z-index": 10000
            });
            angular.forEach(options, function (item, i) {
                var $li = $('<li>');
                if (item === null) {
                    $li.addClass('divider');
                } else {
                    var nestedMenu = angular.isArray(item[1])
                      ? item[1] : angular.isArray(item[2])
                      ? item[2] : angular.isArray(item[3])
                      ? item[3] : null;
                    var $a = $('<a>');
                    $a.css("padding-right", "8px");
                    $a.attr({ tabindex: '-1' });
                    var text = typeof item[0] == 'string' ? item[0] : item[0].call($scope, $scope, event, model);
                    $q.when(text).then(function (text) {
                        $a.text(text);
                        if (nestedMenu) {
                            $a.css("cursor", "default");
                            $a.append($('<strong style="font-family:monospace;font-weight:bold;float:right;">&gt;</strong>'));
                        }
                    });
                    $li.append($a);

                    var enabled = angular.isFunction(item[2]) ? item[2].call($scope, $scope, event, model, text) : true;
                    if (enabled) {
                        var openNestedMenu = function ($event) {
                            removeContextMenus(level + 1);
                            var ev = {
                                pageX: event.pageX + $ul[0].offsetWidth - 1,
                                pageY: $ul[0].offsetTop + $li[0].offsetTop - 3
                            };
                            renderContextMenu($scope, ev, nestedMenu, model, level + 1);
                        }
                        $li.on('click', function ($event) {
                            //$event.preventDefault();
                            $scope.$apply(function () {
                                if (nestedMenu) {
                                    openNestedMenu($event);
                                } else {
                                    $(event.currentTarget).removeClass('context');
                                    removeContextMenus();
                                    item[1].call($scope, $scope, event, model);
                                }
                            });
                        });

                        $li.on('mouseover', function ($event) {
                            $scope.$apply(function () {
                                if (nestedMenu) {
                                    openNestedMenu($event);
                                }
                            });
                        });
                    } else {
                        $li.on('click', function ($event) {
                            $event.preventDefault();
                        });
                        $li.addClass('disabled');
                    }
                }
                $ul.append($li);
            });
            $contextMenu.append($ul);
            var height = Math.max(
                document.body.scrollHeight, document.documentElement.scrollHeight,
                document.body.offsetHeight, document.documentElement.offsetHeight,
                document.body.clientHeight, document.documentElement.clientHeight
            );
            $contextMenu.css({
                width: '100%',
                height: height + 'px',
                position: 'absolute',
                top: 0,
                left: 0,
                zIndex: 9999
            });
            $(document).find('body').append($contextMenu);
            $contextMenu.on("mousedown", function (e) {
                if ($(e.target).hasClass('dropdown')) {
                    $(event.currentTarget).removeClass('context');
                    removeContextMenus();
                }
            }).on('contextmenu', function (event) {
                $(event.currentTarget).removeClass('context');
                event.preventDefault();
                removeContextMenus(level);
            });
            $scope.$on("$destroy", function () {
                removeContextMenus();
            });

            contextMenus.push($ul);
        };
        return function ($scope, element, attrs) {
            element.on('contextmenu', function (event) {
                event.stopPropagation();
                $scope.$apply(function () {
                    event.preventDefault();
                    var options = $scope.$eval(attrs.contextMenu);
                    var model = $scope.$eval(attrs.model);
                    if (options instanceof Array) {
                        if (options.length === 0) { return; }
                        renderContextMenu($scope, event, options, model);
                    } else {
                        throw '"' + attrs.contextMenu + '" not an array';
                    }
                });
            });
        };
    }]);

    simsController.controller('EmployeeAttendanceMonthlyCont',
      ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

          var att_date1, att_date;
          $scope.prevDisabled = true;
          $scope.subDisabled = true;
          $scope.Atten_Codes = false;
          $scope.stud_table = true;
          $scope.data1 = [];

          //current date & user name
          var today = new Date();
          var dd = today.getDate();
          var mm = today.getMonth() + 1;
          var yyyy = today.getFullYear();
          $scope.todays_date = dd + '-' + mm + '-' + yyyy;
          $scope.todays_date1 = yyyy + '-' + '0' + mm + '-' + dd;
          $scope.ddaayy = dd;
          var Current_Username = $rootScope.globals.currentUser.username;

          $http.get(ENV.apiUrl + "api/emp/attendance/getCompanyName").then(function (comp) {
              $scope.comp_data = comp.data;
          });

          $scope.attendance_codes_obj = {};
          $http.get(ENV.apiUrl + "api/emp/attendance/getAttendanceCode").then(function (attendance_code) {
              $scope.attendance_codes_obj = attendance_code.data;
              console.log($scope.attendance_codes_obj);
          });

          $scope.get_attend_code = function (codes) {
              $scope.subDisabled = false;
              $scope.pays_attendance_color = codes.pays_attendance_color;
              $scope.pays_attendance_code = codes.pays_attendance_code;
          }

          $scope.Comp_Change = function (company_code) {
              $http.get(ENV.apiUrl + "api/emp/attendance/getDeptName?comp_code=" + company_code).then(function (dept) {
                  $scope.dept_data = dept.data;
                  $scope.Atten_Codes = true;
                  $scope.prevDisabled = false;
                  $scope.stud_table = false;
              });

              $http.get(ENV.apiUrl + "api/emp/attendance/getGradeName").then(function (grade) {
                  $scope.grade_data = grade.data;
              });

              $http.get(ENV.apiUrl + "api/emp/attendance/getStaffType").then(function (stafftype) {
                  $scope.stafftype_data = stafftype.data;
              });

              $http.get(ENV.apiUrl + "api/emp/attendance/getDesignation").then(function (designation) {
                  $scope.designation_data = designation.data;
              });

              $http.get(ENV.apiUrl + "api/emp/attendance/getSort").then(function (sort) {
                  $scope.sort_data = sort.data;

              });

              $http.get(ENV.apiUrl + "api/emp/attendance/getYear").then(function (year) {
                  $scope.year_data = year.data;
                  $scope.getMonthbyYear($scope.year_data[0].sims_academic_year);
              });

              $scope.clear = function () {
                  $scope.emp_monthlyAttendance_obj = "";
                  $scope.stud_table = false;
                  $scope.Atten_Codes = false;
                  $scope.prevDisabled = true;
                  $scope.subDisabled = true;
                  $scope.temp.employee_no = "";
                  $scope.temp.company_code = "";
                  $scope.temp.dept_code = "";
                  $scope.temp.gr_code = "";
                  $scope.temp.staff_type_code = "";
                  $scope.temp.dg_code = "";
                  $scope.temp.sims_academic_year = "";
                  $scope.temp.sims_academic_month = "";
                  $scope.temp.sort_code = "";
                  $scope.edt.att_emp_id = "";
              }

              $scope.colors = function (code) {
                  for (var x = 0; x < $scope.attendance_codes_obj.length; x++)
                      if ($scope.attendance_codes_obj[x].pays_attendance_code == code)
                          return $scope.attendance_codes_obj[x].pays_attendance_color;
              }


              $scope.getMonthbyYear = function (str) {

                  $scope.data = [];

                  var startdate = new Date();
                  var enddate = new Date();

                  for (var i = 0; i < $scope.year_data.length; i++) {
                      if ($scope.year_data[i].sims_academic_year == str) {
                          startdate = moment($scope.year_data[i].sims_academic_year_start_date).format('YYYY/MM/DD');
                          enddate = moment($scope.year_data[i].sims_academic_year_end_date).format('YYYY/MM/DD');
                      }
                  }

                  var check_date = startdate;
                  $scope.data.push({ date: moment(check_date).format('MMMM-YYYY'), dateCode: moment(check_date).format('MM') })
                  for (; new Date(check_date) < new Date(moment(enddate, "YYYY/MM/DD").add('months', -1)) ;) {

                      check_date = moment(check_date, "YYYY/MM/DD").add('months', 1);
                      check_date = moment(check_date).format('YYYY/MM/DD');
                      $scope.data.push({ date: moment(check_date).format('MMMM-YYYY'), dateCode: moment(check_date).format('MM') })
                  }

              }

              $scope.getDays_in_month = function (sims_month_name, sims_year) {

                  var month, year;
                  if (sims_month_name == "10" || sims_month_name == "11" || sims_month_name == "12") {
                      month = sims_month_name;
                  }
                  else {
                      month = sims_month_name.split(0)[1];
                  }

                  if (sims_month_name == "09" || sims_month_name == "10" || sims_month_name == "11" || sims_month_name == "12") {
                      year = parseInt(sims_year) - 1;
                  }
                  else {
                      year = sims_year;
                  }

                  var daysInMonth = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
                  var monthNames = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
                  if ((month == 2) && (year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) {
                      $scope.rowscount = 29;
                  } else {
                      $scope.rowscount = daysInMonth[month];
                  }
                  $scope.temp1 = [];
                  for (var i = 1; i <= $scope.rowscount; i++) {
                      var data = [];
                      var dayNo = (i % $scope.rowscount);
                      if (dayNo == 0) {
                          dayNo = $scope.rowscount;
                      }
                      data['id'] = dayNo;
                      $scope.temp1.push(data);
                      $scope.dayN = dayNo;
                  }
              }

              //All Employee 

              $scope.getStudentMonthlyAttendance = function (comp_code, dept_code, grade_code, stafftype_code, designation_code, att_year, month_no, employee_no, Myformvalid) {

                  if (Myformvalid) {
                      if (month_no == "09" || month_no == "10" || month_no == "11" || month_no == "12") {
                          $scope.att_date1 = parseInt(att_year) - 1;
                      }
                      else {
                          $scope.att_date1 = att_year;
                      }
                      $scope.stud_table = false;
                      $scope.day_column_obj = $scope.temp1;
                      $scope.loading = true;
                      $http.get(ENV.apiUrl + "api/emp/attendance/getAllEmployeeAttendance?comp_code=" + comp_code + "&dept_code=" + dept_code + "&grade_code=" + grade_code +
                           "&stafftype_code=" + stafftype_code + "&designation_code=" + designation_code + "&att_year=" + $scope.att_date1 + "&month_no=" + month_no + "&employee_no=" + employee_no).then(function (monthlyAttendance) {
                               $scope.emp_monthlyAttendance_obj = monthlyAttendance.data;

                               if ($scope.emp_monthlyAttendance_obj == "") {
                                   swal({ title: "Alert!", text: "Records Not Found", showCloseButton: true, width: 380, });
                                   $scope.stud_table = false;
                               }
                               else {
                                   $scope.stud_table = true;
                                   for (var i = 0; i < $scope.emp_monthlyAttendance_obj.length; i++) {
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day1_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_1_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day2_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_2_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day3_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_3_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day4_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_4_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day5_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_5_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day6_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_6_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day7_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_7_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day8_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_8_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day9_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_9_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day10_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_10_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day11_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_11_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day12_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_12_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day13_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_13_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day14_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_14_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day15_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_15_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day16_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_16_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day17_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_17_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day18_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_18_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day19_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_19_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day20_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_20_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day21_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_21_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day22_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_22_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day23_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_23_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day24_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_24_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day25_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_25_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day26_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_26_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day27_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_27_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day28_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_28_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day29_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_29_att_code);
                                       //if ((month_no == "02") && (att_year % 4 == 0) && ((att_year % 100 != 0) || (att_year % 400 == 0)))
                                       //{$scope.emp_monthlyAttendance_obj[i].attendance_day29_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_29_att_code);}
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day30_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_30_att_code);
                                       $scope.emp_monthlyAttendance_obj[i].attendance_day31_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_31_att_code);
                                   }
                               }

                               console.log($scope.emp_monthlyAttendance_obj);
                               $scope.total_students = $scope.emp_monthlyAttendance_obj.length;
                               //$scope.stud_table = true;
                               $scope.loading = false;
                               $scope.btn_mark = true;
                               var d = new Date();
                               var n = d.getDate();
                               debugger;
                               var month = month_no.split(0)[1];
                               $timeout(function () {
                                   $("#fixTable").tableHeadFixer({ "left": 2 });
                               }, 100);

                               console.log("after spilt");
                               console.log(month_no);

                               //var mon = parseInt(month);
                               var dy_str = 'pays_attendance_day_' + $scope.ddaayy + '_att_code';
                               $scope.P_cnt = 0, $scope.A_cnt = 0, $scope.AE_cnt = 0, $scope.T_cnt = 0, $scope.TE_cnt = 0, $scope.UM_cnt = 0, $scope.NA_cnt = 0;
                               for (var i = 0; i < $scope.emp_monthlyAttendance_obj.length; i++) {
                                   if ($scope.emp_monthlyAttendance_obj[i][dy_str] && $scope.emp_monthlyAttendance_obj[i].yearno == att_year && $scope.emp_monthlyAttendance_obj[i].monthYear == month && dy_str) {
                                       if ($scope.emp_monthlyAttendance_obj[i][dy_str] == 'P') {
                                           $scope.P_cnt = $scope.P_cnt + 1;
                                       }
                                       else if ($scope.emp_monthlyAttendance_obj[i][dy_str] == 'A') {
                                           $scope.A_cnt = $scope.A_cnt + 1;
                                       }
                                       else if ($scope.emp_monthlyAttendance_obj[i][dy_str] == 'AE') {
                                           $scope.AE_cnt = $scope.AE_cnt + 1;
                                       }
                                       else if ($scope.emp_monthlyAttendance_obj[i][dy_str] == 'T') {
                                           $scope.T_cnt = $scope.T_cnt + 1;
                                       }
                                       else if ($scope.emp_monthlyAttendance_obj[i][dy_str] == 'TE') {
                                           $scope.TE_cnt = $scope.TE_cnt + 1;
                                       }
                                       else if ($scope.emp_monthlyAttendance_obj[i][dy_str] == 'UM') {
                                           $scope.UM_cnt = $scope.UM_cnt + 1;
                                       }
                                       else if ($scope.emp_monthlyAttendance_obj[i][dy_str] == 'NA') {
                                           $scope.NA_cnt = $scope.NA_cnt + 1;
                                       }
                                   }
                               }
                           }, function () {
                               //swal({ title: "Alert", text: "Internal Server Error", imageUrl: "assets/img/notification-alert.png", });
                           });
                  }

              }

              //column wise attendance
              $scope.menuOptionsDayWise = [
                        ['Mark All Present', function ($itemScope) {
                            $scope.pays_attendance_code = 'P';
                            var d = $itemScope.i.id;

                            var month = '';
                            if ($itemScope.$parent.emp_monthlyAttendance_obj[0].monthYear < 10) {
                                month = '0' + $itemScope.$parent.emp_monthlyAttendance_obj[0].monthYear;
                            } else {
                                month = $itemScope.$parent.emp_monthlyAttendance_obj[0].monthYear;
                            }

                            var day = '';
                            if (d < 10) {
                                day = '0' + d;
                            }
                            else {
                                day = d;
                            }

                            var Mark_all_Stud_atte_DayWise1 = [];

                            for (var i = 0; i < $itemScope.$parent.emp_monthlyAttendance_obj.length; i++) {
                                var Mark_all_Stud_atte_DayWise = {};
                                Mark_all_Stud_atte_DayWise['opr'] = 'O';
                                Mark_all_Stud_atte_DayWise['att_emp_id'] = $itemScope.$parent.emp_monthlyAttendance_obj[i].att_emp_id;
                                Mark_all_Stud_atte_DayWise['att_date'] = $itemScope.$parent.emp_monthlyAttendance_obj[i].yearno + '-' + month + '-' + day;
                                Mark_all_Stud_atte_DayWise['att_flag'] = $scope.pays_attendance_code;
                                Mark_all_Stud_atte_DayWise1.push(Mark_all_Stud_atte_DayWise);
                            }

                            swal({
                                title: "Are you sure?", text: "You will not be able to recover this Data", showCancelButton: true,
                                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, Mark it", closeOnConfirm: false, width: 380
                            }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.loading = true;
                                    $http.post(ENV.apiUrl + "api/emp/attendance/MarkAllPreAbEmpAttendance", Mark_all_Stud_atte_DayWise1).then(function (res) {

                                        if (res.data == true) {
                                            swal({ title: "Success", text: "Attendance Marked Successfully", showCloseButton: true, width: 380 });
                                            $scope.getStudentMonthlyAttendance($scope.temp.company_code, $scope.temp.dept_code, $scope.temp.gr_code,
                                                $scope.temp.staff_type_code, $scope.temp.dg_code, $scope.temp.sims_academic_year, $scope.temp.sims_academic_month);
                                        }
                                    });
                                }
                                $scope.loading = false;
                            });

                        }],
                        null,
                        ['Mark All Absent', function ($itemScope) {
                            $scope.pays_attendance_code = 'A';
                            var d = $itemScope.i.id;

                            var month = '';
                            if ($itemScope.$parent.emp_monthlyAttendance_obj[0].monthYear < 10) {
                                month = '0' + $itemScope.$parent.emp_monthlyAttendance_obj[0].monthYear;
                            } else {
                                month = $itemScope.$parent.emp_monthlyAttendance_obj[0].monthYear;
                            }

                            var day = '';
                            if (d < 10) {
                                day = '0' + d;
                            }
                            else {
                                day = d;
                            }
                            var Mark_all_Stud_atte_DayWise1 = [];
                            for (var i = 0; i < $itemScope.$parent.emp_monthlyAttendance_obj.length; i++) {
                                var Mark_all_Stud_atte_DayWise = {};
                                Mark_all_Stud_atte_DayWise['opr'] = 'O';
                                Mark_all_Stud_atte_DayWise['att_emp_id'] = $itemScope.$parent.emp_monthlyAttendance_obj[i].att_emp_id;
                                Mark_all_Stud_atte_DayWise['att_date'] = $itemScope.$parent.emp_monthlyAttendance_obj[i].yearno + '-' + month + '-' + day;
                                Mark_all_Stud_atte_DayWise['att_flag'] = $scope.pays_attendance_code;
                                Mark_all_Stud_atte_DayWise1.push(Mark_all_Stud_atte_DayWise);
                            }

                            swal({
                                title: "Are you sure?", text: "You will not be able to recover this Data", showCancelButton: true,
                                confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, Mark it", closeOnConfirm: false, width: 380,
                            }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.loading = true;
                                    $http.post(ENV.apiUrl + "api/emp/attendance/MarkAllPreAbEmpAttendance", Mark_all_Stud_atte_DayWise1).then(function (res) {
                                        if (res.data == true) {
                                            swal({ title: "Success", text: "Attendance Marked Successfully", showCloseButton: true, width: 380 });
                                            $scope.getStudentMonthlyAttendance($scope.temp.company_code, $scope.temp.dept_code, $scope.temp.gr_code, $scope.temp.staff_type_code, $scope.temp.dg_code, $scope.temp.sims_academic_year, $scope.temp.sims_academic_month);
                                        }
                                    });
                                }
                                $scope.loading = false;
                            });
                        }],
                        null,
              ];
              //row wise 
              $scope.menuOptions = [
                      ['Mark Present For Whole Month', function ($itemScope) {
                          debugger;
                          $scope.pays_attendance_code = 'P';
                          for (var i = 0; i < $itemScope.$parent.emp_monthlyAttendance_obj.length; i++) {
                              if ($itemScope.emp == $itemScope.$parent.emp_monthlyAttendance_obj[i]) {
                                  $scope.emp_id = $itemScope.emp.att_emp_id;
                              }
                          }
                          $scope.markStudentAttendanceForMonth();
                      }],
                      null,
                      ['Mark Absent For Whole Month', function ($itemScope) {
                          $scope.pays_attendance_code = 'A';
                          for (var i = 0; i < $itemScope.$parent.emp_monthlyAttendance_obj.length; i++) {
                              if ($itemScope.emp == $itemScope.$parent.emp_monthlyAttendance_obj[i]) {
                                  $scope.emp_id = $itemScope.emp.att_emp_id;
                              }
                          }
                          $scope.markStudentAttendanceForMonth();
                      }],
                      null,
              ];

              $scope.markStudentAttendanceForMonth = function () {
                  debugger;
                  if ($scope.temp.sims_academic_month == "09" || $scope.temp.sims_academic_month == "10"
                      || $scope.temp.sims_academic_month == "11" || $scope.temp.sims_academic_month == "12") {
                      $scope.att_date1 = parseInt($scope.temp.sims_academic_year) - 1;
                  }
                  else {
                      $scope.att_date1 = $scope.temp.sims_academic_year;
                  }

                  if ($scope.temp.sims_academic_month == '01' || $scope.temp.sims_academic_month == '03' || $scope.temp.sims_academic_month == '05' || $scope.temp.sims_academic_month == '07' || $scope.temp.sims_academic_month == '08'
                      || $scope.temp.sims_academic_month == '10' || $scope.temp.sims_academic_month == '12') {
                      $scope.day = 31;
                  }
                  else if (($scope.temp.sims_academic_month == '02') && ($scope.att_date1 % 4 == 0) && (($scope.att_date1 % 100 != 0) || ($scope.att_date1 % 400 == 0))) {
                      $scope.day = 29;
                  }
                  else if ($scope.temp.sims_academic_month == '02') { $scope.day = 28; }
                  else { $scope.day = 30; }



                  var emp_mark_monthly = [];
                  for (var i = 1; i <= $scope.day; i++) {
                      if (i <= 9) {
                          var j = '0' + i;
                      }
                      else {
                          var j = i;
                      }

                      var stud_mark_monthly = {};
                      stud_mark_monthly['opr'] = 'O';
                      stud_mark_monthly['att_flag'] = $scope.pays_attendance_code;
                      stud_mark_monthly['att_emp_id'] = $scope.emp_id;
                      stud_mark_monthly['att_date'] = $scope.att_date1 + '-' + $scope.temp.sims_academic_month + '-' + j;
                      emp_mark_monthly.push(stud_mark_monthly);
                  }
                  swal({
                      title: "Are you sure?", text: "You will not be able to recover this Data", showCancelButton: true,
                      confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, Mark it", closeOnConfirm: false, width: 380
                  }).then(function (isConfirm) {
                      if (isConfirm) {
                          $scope.loading = true;
                          $http.post(ENV.apiUrl + "api/emp/attendance/MarkAllPreAbEmpAttendance", emp_mark_monthly).then(function (emp_mark_AllData) {
                              debugger;
                              if (emp_mark_AllData == true) {
                                  swal({ title: "Success", text: "Attendance Marked Successfully", showCloseButton: true, width: 380 });
                              }
                              $scope.getStudentMonthlyAttendance($scope.temp.company_code, $scope.temp.dept_code, $scope.temp.gr_code, $scope.temp.staff_type_code, $scope.temp.dg_code, $scope.att_date1, $scope.temp.sims_academic_month);
                          });

                      }

                      $scope.loading = false;
                  });


              }



              $scope.otherMenuOptions = [
                    ['Favorite Color', function ($itemScope, $event, color) {
                        alert(color);
                    }]
              ]
          }

          //single attendance mark entry

          $scope.list_student_data = []; $scope.pays_attendance_code = null; $scope.pays_attendance_color = null;

          $scope.Mark_Attendance_1 = function (emp, id, str, column_id, element) {
              console.log($scope.pays_attendance_color);
              if ($scope.pays_attendance_code != null) {
                  emp[str] = $scope.pays_attendance_color;
                  emp[id] = $scope.pays_attendance_code;
              }
          }

          $scope.Mark_Attendance = function (emp, id, str, column_id) {

              debugger;
              var d = emp[id];

              if (d == "W") {
                  if ($scope.pays_attendance_code != "A") {
                      swal({ text: 'You can assign only absent for weekend' })
                      var k = emp.att_emp_id;
                      $scope.pays_attendance_code = "W";
                      $scope.pays_attendance_color = rgb(51, 136, 85);
                  }
              }

              if (d == "H") {
                  swal({ text: 'You can not assign attendance' })
              }
              else {
                  if ($scope.pays_attendance_code != null) {
                      emp[str] = $scope.pays_attendance_color;
                      emp[id] = $scope.pays_attendance_code;
                  }
                  else {
                      swal({ title: "Alert", text: "Please Select Attendance Code", showCloseButton: true, width: 380, });
                  }
                  var d = '';
                  var data = {};
                  data['opr'] = 'I';
                  data['pays_attendance_code'] = $scope.pays_attendance_code;
                  data['att_emp_id'] = emp.att_emp_id;
                  data['pays_employee_name'] = emp.pays_employee_name;
                  data['em_company_code'] = emp.em_company_code;
                  data['em_dept_code'] = emp.em_dept_code;
                  data['em_desg_code'] = emp.em_desg_code;
                  data['em_grade_code'] = emp.em_grade_code;
                  data['em_staff_type'] = emp.em_staff_type;
                  data['monthYear'] = emp.monthYear;
                  data['yearno'] = emp.yearno;
                  data['att_date'] = emp.yearno + '-' + emp.monthYear + '-' + column_id;
                  $scope.list_student_data.push(data);

              }
          }

          $scope.Mark_All_Attendance = function () {
              debugger;
              $scope.subDisabled = false;
              if ($scope.list_student_data.length != 0) {
                  $scope.loading = true;
                  $http.post(ENV.apiUrl + "api/emp/attendance/InsertEmpAttendance", $scope.list_student_data).then(function (res) {
                      $scope.mark_result = res.data;
                      $scope.loading = false;
                      if ($scope.mark_result == true) {
                          swal({ title: "Success", text: "Employee Attendance Added Successfully", showCloseButton: true, width: 380 });
                          $scope.getStudentMonthlyAttendance($scope.temp.company_code, $scope.temp.dept_code, $scope.temp.gr_code, $scope.temp.staff_type_code, $scope.temp.dg_code, $scope.temp.sims_academic_year, $scope.temp.sims_academic_month);
                          $scope.stud_table = true;
                      }

                  }, function () {
                      //swal({ title: "Alert", text: "Internal Server Error", imageUrl: "assets/img/notification-alert.png", });
                  });
              }
              else {
                  swal({ title: "Alert", text: "Please Assign Attendance to Employee", showCloseButton: true, width: 380, });
              }
          }

          $('body').addClass('grey condense-menu');
          $('#main-menu').addClass('mini');
          $('.page-content').addClass('condensed');
          $rootScope.isCondensed = true;
      }])

})();
