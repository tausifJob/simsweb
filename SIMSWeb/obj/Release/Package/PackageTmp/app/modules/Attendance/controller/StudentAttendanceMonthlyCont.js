﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Attendance');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.directive('contextMenu', ["$parse", "$q", function ($parse, $q) {

        var contextMenus = [];

        var removeContextMenus = function (level) {
            while (contextMenus.length && (!level || contextMenus.length > level)) {
                contextMenus.pop().remove();
            }
            if (contextMenus.length == 0 && $currentContextMenu) {
                $currentContextMenu.remove();
            }
        };

        var $currentContextMenu = null;

        var renderContextMenu = function ($scope, event, options, model, level) {
            if (!level) { level = 0; }
            if (!$) { var $ = angular.element; }
            $(event.currentTarget).addClass('context');
            var $contextMenu = $('<div>');
            if ($currentContextMenu) {
                $contextMenu = $currentContextMenu;
            } else {
                $currentContextMenu = $contextMenu;
            }
            $contextMenu.addClass('dropdown clearfix');
            var $ul = $('<ul>');
            $ul.addClass('dropdown-menu');
            $ul.attr({ 'role': 'menu' });
            $ul.css({
                display: 'block',
                position: 'absolute',
                left: event.pageX + 'px',
                top: event.pageY + 'px',
                "z-index": 10000
            });
            angular.forEach(options, function (item, i) {
                var $li = $('<li>');
                if (item === null) {
                    $li.addClass('divider');
                } else {
                    var nestedMenu = angular.isArray(item[1])
                      ? item[1] : angular.isArray(item[2])
                      ? item[2] : angular.isArray(item[3])
                      ? item[3] : null;
                    var $a = $('<a>');
                    $a.css("padding-right", "8px");
                    $a.attr({ tabindex: '-1' });
                    var text = typeof item[0] == 'string' ? item[0] : item[0].call($scope, $scope, event, model);
                    $q.when(text).then(function (text) {
                        $a.text(text);
                        if (nestedMenu) {
                            $a.css("cursor", "default");
                            $a.append($('<strong style="font-family:monospace;font-weight:bold;float:right;">&gt;</strong>'));
                        }
                    });
                    $li.append($a);

                    var enabled = angular.isFunction(item[2]) ? item[2].call($scope, $scope, event, model, text) : true;
                    if (enabled) {
                        var openNestedMenu = function ($event) {
                            removeContextMenus(level + 1);
                            var ev = {
                                pageX: event.pageX + $ul[0].offsetWidth - 1,
                                pageY: $ul[0].offsetTop + $li[0].offsetTop - 3
                            };
                            renderContextMenu($scope, ev, nestedMenu, model, level + 1);
                        }
                        $li.on('click', function ($event) {
                            //$event.preventDefault();
                            $scope.$apply(function () {
                                if (nestedMenu) {
                                    openNestedMenu($event);
                                } else {
                                    $(event.currentTarget).removeClass('context');
                                    removeContextMenus();
                                    item[1].call($scope, $scope, event, model);
                                }
                            });
                        });

                        $li.on('mouseover', function ($event) {
                            $scope.$apply(function () {
                                if (nestedMenu) {
                                    openNestedMenu($event);
                                }
                            });
                        });
                    } else {
                        $li.on('click', function ($event) {
                            $event.preventDefault();
                        });
                        $li.addClass('disabled');
                    }
                }
                $ul.append($li);
            });
            $contextMenu.append($ul);
            var height = Math.max(
                document.body.scrollHeight, document.documentElement.scrollHeight,
                document.body.offsetHeight, document.documentElement.offsetHeight,
                document.body.clientHeight, document.documentElement.clientHeight
            );
            $contextMenu.css({
                width: '100%',
                height: height + 'px',
                position: 'absolute',
                top: 0,
                left: 0,
                zIndex: 9999
            });
            $(document).find('body').append($contextMenu);
            $contextMenu.on("mousedown", function (e) {
                if ($(e.target).hasClass('dropdown')) {
                    $(event.currentTarget).removeClass('context');
                    removeContextMenus();
                }
            }).on('contextmenu', function (event) {
                $(event.currentTarget).removeClass('context');
                event.preventDefault();
                removeContextMenus(level);
            });
            $scope.$on("$destroy", function () {
                removeContextMenus();
            });

            contextMenus.push($ul);
        };
        return function ($scope, element, attrs) {
            element.on('contextmenu', function (event) {
                event.stopPropagation();
                $scope.$apply(function () {
                    event.preventDefault();
                    var options = $scope.$eval(attrs.contextMenu);
                    var model = $scope.$eval(attrs.model);
                    if (options instanceof Array) {
                        if (options.length === 0) { return; }
                        renderContextMenu($scope, event, options, model);
                    } else {
                        throw '"' + attrs.contextMenu + '" not an array';
                    }
                });
            });
        };
    }]);


    simsController.controller('StudentAttendanceMonthlyCont',
      ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {




          $http.get(ENV.apiUrl + "api/attendance/getAttendancesort").then(function (sorttypes) {
              $scope.sorttypes = sorttypes.data;
          });


          $scope.markStudentAttendanceForMonth = function () {
              swal({
                  title: "Are you sure?", text: "You will not be able to recover this Data!",showCancelButton: true,
                  confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, Mark it!", closeOnConfirm: false
              }).then(function (isConfirm) {
                  if (isConfirm) {

                      var stud_mark_monthly = {};
                      stud_mark_monthly['sims_cur_code'] = $scope.single_stud_mark_atten.sims_cur_code;
                      stud_mark_monthly['sims_attendance_code'] = $scope.sims_attendance_code_m;
                      stud_mark_monthly['monthYear'] = $scope.single_stud_mark_atten.monthYear;
                      stud_mark_monthly['yearno'] = $scope.single_stud_mark_atten.yearno;
                      stud_mark_monthly['sims_academic_year'] = $scope.single_stud_mark_atten.sims_academic_year;
                      stud_mark_monthly['sims_enroll_number'] = $scope.single_stud_mark_atten.sims_enroll_number;
                      stud_mark_monthly['sims_grade_code'] = $scope.single_stud_mark_atten.sims_grade_code;
                      stud_mark_monthly['sims_section_code'] = $scope.single_stud_mark_atten.sims_section_code;
                      stud_mark_monthly['Current_Username'] = Current_Username;
                      console.log(stud_mark_monthly);
                      $http.post(ENV.apiUrl + "api/attendance/markStudentAttendanceForMonth", stud_mark_monthly).then(function (result) {
                          if (result.data == true) {
                              swal("Marked!", "Attendance Marked Successfully", "success");
                              $scope.getStudentMonthlyAttendance();
                          }
                      });
                  }
              });
          }


          $scope.menuOptionsDayWise = [
                ['Mark All Present', function ($itemScope) {
                    $('.dropdown-menu').attr('style', 'display: none');

                    debugger;
                    $scope.sims_attendance_code_m = 'P';
                    var d = $itemScope.i.day;
                    var code = $itemScope.$parent.stud_monthlyAttendance_obj[0]['attendance_day' + d + '_code']
                    if (code != 'W' && code != 'H') {

                        swal({
                            title: "Are you sure?", text: "You will not be able to recover this Data!", showCancelButton: true,
                            confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, Mark it!", closeOnConfirm: false
                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                var Mark_all_Stud_atte_DayWise = {};
                                Mark_all_Stud_atte_DayWise['sims_cur_code'] = $itemScope.$parent.stud_monthlyAttendance_obj[0].sims_cur_code;
                                Mark_all_Stud_atte_DayWise['sims_attendance_code'] = $scope.sims_attendance_code_m;
                                Mark_all_Stud_atte_DayWise['sims_academic_year'] = $itemScope.$parent.stud_monthlyAttendance_obj[0].sims_academic_year;
                                Mark_all_Stud_atte_DayWise['attednacedate'] = $itemScope.$parent.stud_monthlyAttendance_obj[0].yearno + '-' + $itemScope.$parent.stud_monthlyAttendance_obj[0].monthYear + ' -' + d;
                                Mark_all_Stud_atte_DayWise['sims_grade_code'] = $itemScope.$parent.stud_monthlyAttendance_obj[0].sims_grade_code;
                                Mark_all_Stud_atte_DayWise['sims_section_code'] = $itemScope.$parent.stud_monthlyAttendance_obj[0].sims_section_code;
                                Mark_all_Stud_atte_DayWise['Current_Username'] = Current_Username;
                                console.log(Mark_all_Stud_atte_DayWise);
                                $http.post(ENV.apiUrl + "api/attendance/markAllStudentAttendanceDayWise", Mark_all_Stud_atte_DayWise).then(function (result) {
                                    if (result.data == true) {
                                        swal("Marked!", "Attendance Marked Successfully", "success");
                                        $scope.getStudentMonthlyAttendance();
                                    }
                                });
                            }

                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Cant Mark attendance For Weekend." });

                    }

                }],
                null,
                ['Mark All Absent', function ($itemScope) {
                    $('.dropdown-menu').attr('style', 'display: none');

                    $scope.sims_attendance_code_m = 'A';
                    var d = $itemScope.i.day;
                    var code = $itemScope.$parent.stud_monthlyAttendance_obj[0]['attendance_day' + d + '_code']
                    if (code != 'W' && code != 'H') {

                        swal({
                            title: "Are you sure?", text: "You will not be able to recover this Data!", showCancelButton: true,
                            confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, Mark it!", closeOnConfirm: false
                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                var Mark_all_Stud_atte_DayWise = {};
                                Mark_all_Stud_atte_DayWise['sims_cur_code'] = $itemScope.$parent.stud_monthlyAttendance_obj[0].sims_cur_code;
                                Mark_all_Stud_atte_DayWise['sims_attendance_code'] = $scope.sims_attendance_code_m;
                                Mark_all_Stud_atte_DayWise['sims_academic_year'] = $itemScope.$parent.stud_monthlyAttendance_obj[0].sims_academic_year;
                                Mark_all_Stud_atte_DayWise['attednacedate'] = $itemScope.$parent.stud_monthlyAttendance_obj[0].yearno + '-' + $itemScope.$parent.stud_monthlyAttendance_obj[0].monthYear + ' -' + d;
                                Mark_all_Stud_atte_DayWise['sims_grade_code'] = $itemScope.$parent.stud_monthlyAttendance_obj[0].sims_grade_code;
                                Mark_all_Stud_atte_DayWise['sims_section_code'] = $itemScope.$parent.stud_monthlyAttendance_obj[0].sims_section_code;
                                Mark_all_Stud_atte_DayWise['Current_Username'] = Current_Username;
                                console.log(Mark_all_Stud_atte_DayWise);
                                $http.post(ENV.apiUrl + "api/attendance/markAllStudentAttendanceDayWise", Mark_all_Stud_atte_DayWise).then(function (result) {
                                    if (result.data == true) {
                                        swal("Marked!", "Attendance Marked Successfully", "success");
                                        $scope.getStudentMonthlyAttendance();
                                    }
                                });
                            }
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Cant Mark attendance For Weekend." });

                    }
                }],
                null,

          ];

          $scope.menuOptions = [
              ['Mark All Present', function ($itemScope) {
                  $('.dropdown-menu').attr('style', 'display: none');

                  $scope.sims_attendance_code_m = 'P';
                  $scope.single_stud_mark_atten = $itemScope.stud;
                  $scope.markStudentAttendanceForMonth();
              }],
              null,
              ['Mark All Absent', function ($itemScope) {
                  $('.dropdown-menu').attr('style', 'display: none');

                  $scope.sims_attendance_code_m = 'A';
                  $scope.single_stud_mark_atten = $itemScope.stud;
                  $scope.markStudentAttendanceForMonth();
              }],
              null,
              //['More...', [
              //    ['Alert Cost', function ($itemScope) {
              //        alert($itemScope.item.cost);
              //    }],
              //    ['Alert Player Gold', function ($itemScope) {
              //        alert($scope.player.gold);
              //    }]
              //]]
          ];
          $scope.otherMenuOptions = [
            ['Favorite Color', function ($itemScope, $event, color) {
                alert(color);
            }]
          ]


          $scope.P_cnt = 0, $scope.A_cnt = 0, $scope.AE_cnt = 0, $scope.T_cnt = 0, $scope.TE_cnt = 0, $scope.UM_cnt = 0, $scope.NA_cnt = 0, $scope.total_students = 0;

          var today = new Date();
          var dd = today.getDate();
          var mm = today.getMonth() + 1; //January is 0!

          var yyyy = today.getFullYear();
          $scope.todays_date = dd + '-' + mm + '-' + yyyy;
          $http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (curiculum) {
              $scope.Curriculum_obj = curiculum.data;
              if (curiculum.data.length > 0) {
                  $scope.cur_code = curiculum.data[0].sims_cur_code;
                  $scope.Cur_Change();
              }
          });
          var Current_Username = $rootScope.globals.currentUser.username;
          $scope.btn_mark = false;
          $scope.Cur_Change = function () {
              $http.get(ENV.apiUrl + "api/attendance/getAttendanceyear?cur_code=" + $scope.cur_code).then(function (academicyears) {
                  $scope.academicyears_obj = academicyears.data;
                  if (academicyears.data.length > 0) {
                      $scope.Acdm_yr = academicyears.data[0].sims_academic_year;
                      $scope.acdm_yr_change();
                  }

              });
              $scope.attendance_codes_obj = {};
              $http.get(ENV.apiUrl + "api/attendance/getAttendanceCode?cur_code=" + $scope.cur_code).then(function (attendance_code) {
                  $scope.attendance_codes_obj = attendance_code.data;
                  //  $scope.attendance_codes_obj.push($scope.shahaji);
                  // console.log($scope.attendance_codes_obj);
              });
          }

          $scope.acdm_yr_change = function () {
              $http.get(ENV.apiUrl + "api/attendance/getAttendancegrade?cur_code=" + $scope.cur_code + "&academic_year=" + $scope.Acdm_yr + "&userName=" + Current_Username).then(function (grades) {
                  $scope.grades_obj = grades.data;
              });

              $http.get(ENV.apiUrl + "api/attendance/getTerms?cur_code=" + $scope.cur_code + "&ayear=" + $scope.Acdm_yr).then(function (terms) {
                  $scope.terms_obj = terms.data;
              });
          }
          $scope.grade_change = function () {

              $http.get(ENV.apiUrl + "api/attendance/getAttendancesection?cur_code=" + $scope.cur_code + "&academic_year=" + $scope.Acdm_yr
                  + "&gradeCode=" + $scope.grade_code + "&userName=" + Current_Username).then(function (section) {
                      $scope.sections_names_obj = section.data;
                  });

          }
          $scope.getDays_in_month = function (sims_month_name) {
              $scope.list_student_data = [];

              var year = sims_month_name.split('-')[1];
              var month = sims_month_name.split('-')[0];
              month = new Date(Date.parse(month + " 1," + year)).getMonth() + 1;
              $scope.month_number = month;
              $scope.att_year = year;
              //   debugger
              $scope.rowscount = new Date(year, month, 0).getDate();
              var dayNameArr = new Array("SUN", "MON", "TUE", "WED", "THR", "FRD", "SAT");
              /* var daysInMonth = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
               var monthNames = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
               if ((month == 2) && (year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) {
                   $scope.rowscount = 29;
               } else {
                   $scope.rowscount = daysInMonth[month];
               }
               */
              $scope.temp = [];
              $scope.days = [];
              for (var i = 1; i <= $scope.rowscount; i++) {
                  var data = [];
                  var d = new Date(year + '-' + month + '-' + i);
                  var dayNo = (i % 7);
                  if (dayNo == 0) {
                      dayNo = 7;
                  }
                  data['id'] = i + ' ' + dayNameArr[d.getDay()];
                  $scope.temp.push(data);

                  $scope.days.push({ day: i, mon: dayNameArr[d.getDay()] })
              }
              // console.log($scope.temp);
          }

          $scope.term_change = function () {
              var starDate = '', endDate = '';
              for (var i = 0; i < $scope.terms_obj.length; i++) {
                  if ($scope.terms_obj[i].sims_term_code == $scope.term_code) {
                      starDate = $scope.terms_obj[i].sims_term_start_date;
                      endDate = $scope.terms_obj[i].sims_term_end_date;
                      break;
                  }
              }
              //var date = starDate;
              //var year = date.split("-")[0];
              //var month = date.split("-")[1];
              //var day = date.split("-")[2];

              //var starDate = year + "-" + month + "-" + day;
              //date = endDate;
              //year = date.split("-")[0];
              //month = date.split("-")[1];
              //day = date.split("-")[2];

              //endDate = year + "-" + month + "-" + day;

              $http.get(ENV.apiUrl + "api/attendance/getTermDetails?startDate=" + starDate + "&endDate=" + endDate).then(function (termsDetails) {
                  $scope.termsDetails_obj = termsDetails.data;
              });
          }
          //   swal("Event Updated Successfully!", "success")
          //   swal({ title: "Alert", text: "Event Can not Update", imageUrl: "assets/img/notification-alert.png", });
          $scope.get_attend_code = function (codes, index) {

              $scope.mnuCard = codes;
              //var s='#'+index
              //$scope.sty = { 'border-bottom': '3px solid ' + codes.sims_attendance_color };
              //$(s).css( $scope.sty);
              $scope.sims_attendance_color = codes.sims_attendance_color;
              $scope.sims_attendance_code = codes.sims_attendance_code;
          }


          $scope.stud_table = false;
          $scope.getStudentMonthlyAttendance = function () {
              console.log($scope.temp);
              $scope.day_column_obj = $scope.temp;
              $scope.stud_table = false;
              $scope.btn_mark = false;
              $scope.loading = true;
              $http.get(ENV.apiUrl + "api/attendance/getMonthlyAttendance?cur_code=" + $scope.cur_code + "&ayear=" + $scope.Acdm_yr + "&grade=" + $scope.grade_code +
                   "&section=" + $scope.section_code + "&month=" + $scope.month_number + "&year=" + $scope.att_year).then(function (monthlyAttendance) {
                       $scope.stud_monthlyAttendance_obj = monthlyAttendance.data;
                       if (monthlyAttendance.data.length <= 0)
                           swal({ title: "Alert", text: "No Attendance for this month"});


                       $scope.total_students = $scope.stud_monthlyAttendance_obj.length;
                       $scope.stud_table = true;
                       $scope.loading = false;
                       $scope.btn_mark = true;
                       var d = new Date();
                       var n = d.getDate();
                       if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                           $timeout(function () {
                               $("#fixTable").tableHeadFixer({ 'top': 1 });
                           }, 100);
                       }
                       else {
                           $timeout(function () {
                                  $("#fixTable").tableHeadFixer({ "left": 2 });
                             //  $("#fixTable").tableHeadFixer({ 'top': 1 });

                           }, 100);

                       }
                       var dy_str = 'attendance_day' + n + '_code';
                       //attendance_day18_code;

                       $scope.P_cnt = 0, $scope.A_cnt = 0, $scope.AE_cnt = 0, $scope.T_cnt = 0, $scope.TE_cnt = 0, $scope.UM_cnt = 0, $scope.NA_cnt = 0;
                       for (var i = 0; i < $scope.stud_monthlyAttendance_obj.length; i++) {
                           if ($scope.stud_monthlyAttendance_obj[i][dy_str] && $scope.stud_monthlyAttendance_obj[i].yearno == yyyy && $scope.stud_monthlyAttendance_obj[i].monthYear == mm) {
                               if ($scope.stud_monthlyAttendance_obj[i][dy_str] == 'P') {
                                   $scope.P_cnt = $scope.P_cnt + 1;
                               }
                               else if ($scope.stud_monthlyAttendance_obj[i][dy_str] == 'A') {
                                   $scope.A_cnt = $scope.A_cnt + 1;
                               }
                               else if ($scope.stud_monthlyAttendance_obj[i][dy_str] == 'AE') {
                                   $scope.AE_cnt = $scope.AE_cnt + 1;
                               }
                               else if ($scope.stud_monthlyAttendance_obj[i][dy_str] == 'T') {
                                   $scope.T_cnt = $scope.T_cnt + 1;
                               }
                               else if ($scope.stud_monthlyAttendance_obj[i][dy_str] == 'TE') {
                                   $scope.TE_cnt = $scope.TE_cnt + 1;
                               }
                               else if ($scope.stud_monthlyAttendance_obj[i][dy_str] == 'UM') {
                                   $scope.UM_cnt = $scope.UM_cnt + 1;
                               }
                               else if ($scope.stud_monthlyAttendance_obj[i][dy_str] == 'NA') {
                                   $scope.NA_cnt = $scope.NA_cnt + 1;
                               }
                           }
                       }

                       //   console.log($scope.stud_monthlyAttendance_obj);

                   }, function () {
                       swal({ title: "Alert", text: "Internal Server Error"});
                   });
          }

          $scope.list_student_data = []; $scope.sims_attendance_code = null; $scope.sims_attendance_color = null;
        
          $scope.Mark_Attendance = function (stud, date, code) {

              debugger
              //$scope.student_data = stud;
              $scope.attednacedate = $scope.att_year + '-' + $scope.month_number + '-' + date;
              if (code != 'W' && code != 'H') {
                  if ($scope.sims_attendance_code != null) {
                      //  debugger;

                      $scope.flag = true;
                      if ($scope.list_student_data.length == 0) {
                          $scope.flag = false;
                          var v = document.getElementById(stud.sims_enroll_number + date);
                          v.style.backgroundColor = $scope.sims_attendance_color;
                          v.textContent = $scope.sims_attendance_code;

                          var data = {};
                          data['sims_cur_code'] = stud.sims_cur_code;
                          data['sims_academic_year'] = stud.sims_academic_year;
                          data['sims_enroll_number'] = stud.sims_enroll_number;
                          data['sims_grade_code'] = stud.sims_grade_code;
                          data['sims_section_code'] = stud.sims_section_code;
                          data['sims_student_name'] = stud.sims_student_name;
                          data['monthYear'] = stud.monthYear;
                          data['yearno'] = stud.yearno;
                          data['attednacedate'] = $scope.att_year + '-' + $scope.month_number + '-' + date;
                          data['sims_attendance_code'] = $scope.sims_attendance_code;
                          $scope.list_student_data.push(data);
                      }
                      else {
                          for (var i = 0; i < $scope.list_student_data.length; i++) {
                              if ($scope.list_student_data[i].sims_enroll_number == stud.sims_enroll_number && $scope.list_student_data[i].attednacedate == $scope.attednacedate && $scope.list_student_data[i].sims_attendance_code) {
                                  $scope.flag = false;
                                  var v = document.getElementById(stud.sims_enroll_number + date);
                                  v.style.backgroundColor = $scope.sims_attendance_color;
                                  v.textContent = $scope.sims_attendance_code;
                                  $scope.list_student_data[i].sims_attendance_code = $scope.sims_attendance_code;
                                  // $scope.list_student_data[i].attednacedate = $scope.attednacedate;
                                  break;
                              }
                          }
                      }
                      if ($scope.flag == true) {
                          var v = document.getElementById(stud.sims_enroll_number + date);
                          v.style.backgroundColor = $scope.sims_attendance_color;
                          v.textContent = $scope.sims_attendance_code;

                          var data = {};
                          data['sims_cur_code'] = stud.sims_cur_code;
                          data['sims_academic_year'] = stud.sims_academic_year;
                          data['sims_enroll_number'] = stud.sims_enroll_number;
                          data['sims_grade_code'] = stud.sims_grade_code;
                          data['sims_section_code'] = stud.sims_section_code;
                          data['sims_student_name'] = stud.sims_student_name;
                          data['monthYear'] = stud.monthYear;
                          data['yearno'] = stud.yearno;
                          data['attednacedate'] = $scope.att_year + '-' + $scope.month_number + '-' + date;
                          data['sims_attendance_code'] = $scope.sims_attendance_code;
                          $scope.list_student_data.push(data);
                      }

                      // console.log($scope.list_student_data);
                      // console.log($scope.stud_monthlyAttendance_obj);

                  }
                  else {
                      swal({ title: "Alert", text: "Please Select Attendance Code"});
                  }
              }

              /*
              $http.post(ENV.apiUrl + "api/attendance/markStudentAttendance?cur_code=" + $scope.student_data.sims_cur_code + "&ayear=" + $scope.student_data.sims_academic_year
                  + "&grade=" + $scope.student_data.sims_grade_code + "&section=" + $scope.student_data.sims_section_code
                 + "&enrollNumber=" + $scope.student_data.sims_enroll_number + "&att_code=" + $scope.sims_attendance_code + "&attednacedate=" + $scope.attednacedate).then(function (res) {
                     $scope.result = res.data;

                     console.log($scope.result);

                   $http.get(ENV.apiUrl + "api/attendance/getMonthlyAttendance?cur_code=" + $scope.cur_code + "&ayear=" + $scope.Acdm_yr + "&grade=" + $scope.grade_code +
                   "&section=" + $scope.section_code + "&month=" + $scope.month_number + "&year=" + $scope.att_year).then(function (monthlyAttendance) {
                       $scope.stud_monthlyAttendance_obj = monthlyAttendance.data;
                       $scope.total_students = $scope.stud_monthlyAttendance_obj.length;
                       $scope.stud_table = true;
                       console.log($scope.stud_monthlyAttendance_obj);
                   });
                 });
              */
          }

          $scope.Mark_All_Attendance = function () {

              if ($scope.list_student_data.length != 0) {
                  $scope.loading = true;
                  $http.post(ENV.apiUrl + "api/attendance/markAllStudentAttendance", $scope.list_student_data).then(function (res) {
                      $scope.mark_result = res.data;
                      $scope.getStudentMonthlyAttendance();
                      $scope.loading = false;
                      if ($scope.mark_result == true) {
                          swal("Student Attendance Added Successfully!", "success")
                      }
                      //console.log($scope.mark_result);
                  }, function () {
                      swal({ title: "Alert", text: "Internal Server Error" });
                  });
              }
              else {
                  swal({ title: "Alert", text: "Please Assign Attendance to Student"});
              }
          }




          //$('body').addClass('grey condense-menu');
          //$('#main-menu').addClass('mini');
          //$('.page-content').addClass('condensed');
          //$rootScope.isCondensed = true;



      }])

})();