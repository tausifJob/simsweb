﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Attendance');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('HomeRoomAttendanceCont', ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV',
        function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.hidesummary = true;

            $('*[data-datepicker="true"] input[type="text"]').datepicker(
                {
                    todayBtn: true,
                    orientation: "top left",
                    autoclose: true,
                    todayHighlight: true,
                    format: 'yyyy-mm-dd'
                });

            $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_homeroom_code: '', sims_enroll_number: '', attDate: '' };
            $scope.rd_button = false;
            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            var username = $rootScope.globals.currentUser.username;
            $http.get(ENV.apiUrl + "api/HomeRoomAttendance/getAttendanceCuriculum").then(function (curiculum) {
                $scope.curiculum = curiculum.data;
                $scope.edt['sims_cur_code'] = curiculum.data[0].sims_cur_code;
                $scope.cur_change();
            });
            $scope.cur_change = function () {

                $http.get(ENV.apiUrl + "api/HomeRoomAttendance/getAttendanceyear?cur_code=" + $scope.edt.sims_cur_code).then(function (academicyears) {
                    $scope.academicyears = academicyears.data;
                });

               
                $http.get(ENV.apiUrl + "api/attendance/getAttendanceCode?cur_code=" + $scope.edt.sims_cur_code).then(function (attendance_code) {
                    $scope.attendance_code = attendance_code.data;
                    //$scope.rdbtn = $scope.attendance_code[0].sims_attendance_code;
                });

            }
            $scope.acdm_yr_change = function () {
                debugger
                $http.get(ENV.apiUrl + "api/HomeRoomAttendance/getHomeRoomBatch?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&userName=" + username).then(function (grades) {
                    $scope.getsec = grades.data;
                    console.log($scope.getsec);
                });

                $http.get(ENV.apiUrl + "api/HomeRoomAttendance/getTeacherSubject?academic_year=" + $scope.edt.sims_academic_year + "&username=" + username).then(function (subjects) {
                    $scope.getsub = subjects.data;
                    // console.log($scope.getsec);
                });


            }
            //$scope.grade_change = function () {

            //    $http.get(ENV.apiUrl + "api/attendance/getAttendancesection?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&gradeCode=" + $scope.edt.sims_grade_code + "&userName=" + username).then(function (section) {
            //        $scope.section = section.data;
            //    });

            //}
            $scope.getAttendance = function () {
                debugger
                $scope.rd_button = true;
                if ($scope.edt.attDate == '') {
                    swal({ text: "Please select Attendance date.", width: 400, showCloseButton: true });

                }
                else {
                    //$http.get(ENV.apiUrl + "api/attendance/getStudentAttendance?cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&attednacedate=" + $scope.edt.attDate).then(function (attendance) {
                    $http.get(ENV.apiUrl + "api/HomeRoomAttendance/getStudentAttendance?home_code=" + $scope.getsec[0].sims_homeroom_code + "&bat_code=" + $scope.getsec[0].sims_batch_code + "&ayear=" + $scope.edt.sims_academic_year + "&attedancedate=" + $scope.edt.attDate + "&subject=" + $scope.edt.sims_bell_subject_code + "&username=" + username).then(function (attendance) {
                        if (attendance.data.length <= 0) {
                            swal({ text: "No Attendance Data is found for this date.", width: 400, showCloseButton: true });
                        }
                        else {
                            $scope.attendance = attendance.data;
                            $scope.operation = true;
                            $scope.totalstudents = $scope.attendance.length;
                            $scope.hidesummary = false;
                        }
                        $scope.update();
                    });
                }
            }
            $scope.sims_attendance_code = '';
            $scope.value = '';

            $scope.attcode = function (info) {
                $scope.value = info;
                $scope.sims_attendance_code = $scope.value.sims_attendance_code;
            }

            $scope.markattendnace = function (objinp) {
                debugger
                if ($scope.sims_attendance_code != '' && ($scope.edt.attDate != '' || $scope.edt.attDate != undefined)) {
                    objinp.sims_attendance_code = $scope.value.sims_attendance_code;
                    objinp.sims_attendance_color = $scope.value.sims_attendance_color;
                    //objinp.sims_student_enroll = $scope.value.sims_student_enroll;
                    var datasend = {
                        'sims_cur_code': $scope.edt.sims_cur_code,
                        'sims_academic_year': $scope.edt.sims_academic_year,
                        'sims_attendance_code': objinp.sims_attendance_code,
                        'attednacedate': $scope.edt.attDate,
                        'sims_enroll_number': objinp.sims_student_enroll,
                    }
                    //$http.post(ENV.apiUrl + "api/HomeRoomAttendance/markAllStudAttendDay?cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&att_code=" + objinp.sims_attendance_code + "&attednacedate=" + $scope.edt.attDate).then(function (attendance) {
                    $http.post(ENV.apiUrl + "api/HomeRoomAttendance/markStudentAttendance", datasend).then(function (attendance) {

                    });
                    $scope.update();
                }
                else {
                    swal({ text: "Please select Attendance Code", width: 350, showCloseButton: true });
                }
            }

            $scope.set_Pre_Attendance_code = function (objinp) {
                debugger;
                //$scope.sims_attendance_code = 'P';
                objinp.sims_attendance_code = 'P';
                objinp.sims_attendance_color = $scope.value.sims_attendance_color;
                $scope.Mark_All_stud_Attendance();
                swal("Marked!", "", "success");
            }

            $scope.set_Abs_Attendance_code = function (objinp) {
                debugger;
                //$scope.sims_attendance_code = 'A';
                objinp.sims_attendance_code = 'A';
                objinp.sims_attendance_color = $scope.value.sims_attendance_color;
                $scope.Mark_All_stud_Attendance();
                swal("Marked!", "", "success");
            }

            var datacode = [];
            var data;
            $scope.Mark_All_stud_Attendance = function (objinp) {
                debugger;

                //if ($scope.sims_attendance_code != '' && ($scope.edt.attDate != '' || $scope.edt.attDate != undefined)) {
                //objinp.sims_attendance_code = $scope.value.sims_attendance_code;
                //objinp.sims_attendance_color = $scope.value.sims_attendance_color;
                data = {
                    'sims_cur_code': $scope.edt.sims_cur_code,
                    'sims_academic_year': $scope.edt.sims_academic_year,
                    'sims_attendance_code': $scope.edt.sims_attendance_code,
                    'attednacedate': $scope.edt.attDate,
                    'sims_batch_code': $scope.getsec[0].sims_batch_code,
                    'sims_homeroom_code': $scope.edt.sims_homeroom_code
                }

                $http.post(ENV.apiUrl + "api/HomeRoomAttendance/markAllStudAttendDay", data).then(function (result) {
                    debugger;
                    $scope.mark_all_stud_att_result = result.data;
                    if (result.data == false)
                        sweetAlert("Oops...", "Internal Server Error!", "error");
                    else
                        $scope.getAttendance(true);
                });
                updateCount();
                // }
            }


            $scope.reset = function () {
                $scope.sims_attendance_code = '';
                $scope.value = '';
                $scope.attendance = '';
                $scope.attendance_code = '';
                $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_homeroom_code: '', attDate: '' };
                $scope.totalstudents = 0;
                $scope.hidesummary = true;
                $scope.total_present = 0;
                $scope.total_absent = 0;
                $scope.rd_button = false;
                $scope.reload();
            }

            $scope.update = function () {

                $scope.total_present = 0;
                $scope.total_absent = 0;
                for (var i = 0; i < $scope.attendance.length; i++) {
                    if ($scope.attendance[i].sims_attendance_code == 'P') $scope.total_present += 1;
                    if ($scope.attendance[i].sims_attendance_code == 'A') $scope.total_absent += 1;

                }
            }
        }])
})();