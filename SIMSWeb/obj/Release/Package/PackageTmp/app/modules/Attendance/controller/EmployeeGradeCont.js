﻿(function () {
    'use strict';
    var obj1, obj2, temp, main;
    var opr = '';
    var gradecode = [];
  


    var simsController = angular.module('sims.module.Attendance');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('EmployeeGradeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5'; 
            $scope.BoardDetail = true;
            $scope.editmode = false;
            var formdata = new FormData();

            $http.get(ENV.apiUrl + "api/EmpGrade/getComapany").then(function (res) {
               
                $scope.compname = res.data;

            })
     
            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
             
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }


            $scope.cancel = function () {
                $scope.BoardDetail = true;
                $scope.BoardData1 = false;
                $scope.edt = "";
                //$scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.editmode = false;
                opr = 'S';
                $scope.readonly = false;
                $scope.BoardDetail = false;
                $scope.BoardData1 = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = {};
               
               
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.BoardData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.BoardData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.eg_grade_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.eg_grade_desc == toSearch) ? true : false;
            }

            $http.get(ENV.apiUrl + "api/EmpGrade/getEmpgradeDetail").then(function (Board_Data) {
                $scope.BoardData = Board_Data.data;
                $scope.totalItems = $scope.BoardData.length;
                $scope.todos = $scope.BoardData;
                $scope.makeTodos();

                console.log($scope.BoardData);
            });
            $http.get(ENV.apiUrl + "api/EmpGrade/getEmpgradeDetail").then(function (res) {
                $scope.BoardData1 = false;
                $scope.BoardDetail = true;
                $scope.obj = res.data;
                console.log($scope.obj);
            });


            $scope.Save = function (myForm) {

                if (myForm) {

                    var data = $scope.edt;
                    data.opr = 'I';
                   
                    $scope.exist = false;
                    debugger;
                    for (var i = 0; i < $scope.BoardData.length; i++) {
                        if ($scope.BoardData[i].eg_grade_code == data.eg_grade_code) {
                            $scope.exist = true;
                        }

                    }
                    if ($scope.exist) {
                        $rootScope.strMessage = 'Record Already exists';

                        $('#message').modal('show');
                    }

                    else {

                        $http.post(ENV.apiUrl + "api/EmpGrade/EmpGradeCUD", data).then(function (msg) {
                            $scope.msg1 = msg.data;
                            $scope.BoardData1 = false;
                            $http.get(ENV.apiUrl + "api/EmpGrade/getEmpgradeDetail").then(function (Pays_Leave) {
                                $scope.LeaveData = Pays_Leave.data;
                                $scope.totalItems = $scope.LeaveData.length;
                                $scope.todos = $scope.LeaveData;
                                $scope.makeTodos();

                                $scope.msg1 = msg.data;


                                if ($scope.msg1 == true) {

                                    $rootScope.strMessage = 'Record Inserted Successfully';

                                    $('#message').modal('show');
                                }
                                else {

                                    $rootScope.strMessage = 'Record Not Inserted';

                                    $('#message').modal('show');
                                }

                            });

                        });
                    }
                    $scope.data1 = "";
                    $scope.BoardDetail = true;
                    $scope.BoardData1 = false;



                   
                }
            }


            $scope.up = function (str) {
                opr = 'U';
                //$scope.edt = str;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.BoardDetail = false;
                $scope.BoardData1 = true;

                $scope.edt = {
                    eg_company_code: str.eg_company_code
                            , eg_grade_code: str.eg_grade_code
                            , eg_grade_desc: str.eg_grade_desc
                            , eg_min_basic: str.eg_min_basic
                            , eg_max_basic: str.eg_max_basic

                };
              
                


            }


            $scope.Update = function () {


                var data = $scope.edt;
                data.opr = 'U';
                $http.post(ENV.apiUrl + "api/EmpGrade/EmpGradeCUD", data).then(function (msg) {
                    $scope.msg1 = msg.data;
                    $scope.BoardData1 = false;
                    $http.get(ENV.apiUrl + "api/EmpGrade/getEmpgradeDetail").then(function (Pays_Leave) {
                        $scope.LeaveData = Pays_Leave.data;
                        $scope.totalItems = $scope.LeaveData.length;
                        $scope.todos = $scope.LeaveData;
                        formdata = new FormData();
                        $scope.makeTodos();

                        if ($scope.msg1 == true) {
                            $rootScope.strMessage = 'Record Updated Successfully';
                            $('#message').modal('show');
                        }
                        else {

                            $rootScope.strMessage = 'Record Not Updated';

                            $('#message').modal('show');
                        }
                    });

                })
                $scope.BoardData1 = false;
                $scope.BoardDetail = true;
            }


         
            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].eg_grade_code;

                        var v = document.getElementById(t + i);
                        v.checked = true;
                        gradecode = gradecode + $scope.filteredTodos[i].eg_grade_code + ','
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].eg_grade_code;
                        var v = document.getElementById(t + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }
            }

            $scope.check_once = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
                else {
                    main.checked = false;
                    $scope.row1 = '';
                }
            }

          

           

            $scope.deleterecord = function () {
                debugger;
                gradecode = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].eg_grade_code;
                    var v = document.getElementById(t + i);

                    if (v.checked == true)
                        gradecode = gradecode + $scope.filteredTodos[i].eg_grade_code + ',';
                }

                var deletecountrycode = ({
                    'eg_grade_code': gradecode,
                 
                    'opr': 'D'
                });

                $http.post(ENV.apiUrl + "api/EmpGrade/EmpGradeCUD", deletecountrycode).then(function (msg) {
                    $scope.msg1 = msg.data;
                    $scope.BoardDetail = true;
                    $scope.BoardData1 = false;
                    $rootScope.strMessage = $scope.msg1.strMessage;
                    $http.get(ENV.apiUrl + "api/EmpGrade/getEmpgradeDetail").then(function (get_Ethnicity) {
                        $scope.Ethnicity_Data = get_Ethnicity.data;
                        $scope.totalItems = $scope.Ethnicity_Data.length;
                        $scope.todos = $scope.Ethnicity_Data;
                        $scope.makeTodos();

                        if ($scope.msg1 == true) {
                            $rootScope.strMessage = 'Record Deleted Successfully';
                            $('#message').modal('show');
                        }
                        else {

                            $rootScope.strMessage = 'Record Not Deleted';
                            $('#message').modal('show');
                        }
                    });
                });
            }

           
        }]);

})();