﻿(function () {
    'use strict';
    var attendancecode = [];
    var curcode = [];
    var main;
    var simsController = angular.module('sims.module.Attendance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AttendanceCodeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            //$scope.operation = true;
            //$scope.table1 = true;
            $scope.editmode = false;
            var data1 = [];

            $scope.countData = [
                   { val: 5, data: 5 },
                   { val: 10, data: 10 },
                   { val: 15, data: 15 },

            ]

            $scope.size = function (str) {

                if (str == 5 || str == 10 || str == 15) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }


                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }
            $scope.edt = "";

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/AttendanceCode/getAttendanceCode").then(function (get_AttendanceCode) {
                    $scope.AttendanceCode_Data = get_AttendanceCode.data;
                    if ($scope.AttendanceCode_Data.length > 0) {
                        $scope.table1 = true;
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.AttendanceCode_Data.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.AttendanceCode_Data.length, data: 'All' })
                        }
                        $scope.totalItems = $scope.AttendanceCode_Data.length;
                        $scope.todos = $scope.AttendanceCode_Data;
                        $scope.makeTodos();

                    }
                    else {
                        $scope.table1 = false;
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                    }

                })
            }
            $http.get(ENV.apiUrl + "api/AttendanceCode/getAttendanceCode").then(function (get_AttendanceCode) {

                $scope.AttendanceCode_Data = get_AttendanceCode.data;
                if ($scope.AttendanceCode_Data.length > 0) {
                    $scope.table1 = true;
                    $scope.pager = true;
                    if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.AttendanceCode_Data.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.AttendanceCode_Data.length, data: 'All' })
                    }
                    $scope.totalItems = $scope.AttendanceCode_Data.length;
                    $scope.todos = $scope.AttendanceCode_Data;
                    $scope.makeTodos();

                }
                else {
                    $scope.table1 = false;
                    swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                }

            })

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;
            })

            $scope.clear = function () {
                $scope.edt = undefined;
            }

            $scope.New = function () {
                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.editmode = false;
                $scope.opr = 'S';
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = {};
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.temp = '';
            }

            $scope.up = function (str) {
                $scope.opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.edt = str;
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Save = function (myForm) {
                debugger;
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'I';
                    $scope.exist = false;
                    for (var i = 0; i < $scope.AttendanceCode_Data.length; i++) {
                        if ($scope.AttendanceCode_Data[i].sims_category_code == data.sims_category_code && $scope.AttendanceCode_Data[i].sims_cur_code == data.sims_cur_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ title: "Alert", text: "Record Already exists", width: 300, height: 200 });
                    }
                    else {

                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/AttendanceCode/CUDAttendanceCode", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Added Successfully", width: 320, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Added", width: 320, height: 200 });
                            }
                            $scope.operation = false;
                            $scope.getgrid();
                        });

                        // $scope.table1 = true;
                        $scope.operation = false;
                    }
                }
            }

            $scope.Update = function (myForm) {
                debugger;
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/AttendanceCode/CUDAttendanceCode", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.operation = false;
                        $http.get(ENV.apiUrl + "api/AttendanceCode/getAttendanceCode").then(function (get_AttendanceCode) {
                            $scope.AttendanceCode_Data = get_AttendanceCode.data;
                            $scope.totalItems = $scope.AttendanceCode_Data.length;
                            $scope.todos = $scope.AttendanceCode_Data;
                            $scope.makeTodos();

                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                            }
                        });

                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                }
            }
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;
            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('test-'+i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('test-' + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }

            }

            var deletecode = [];

            $scope.deleterecord = function () {
                debugger;
                var data1 = [];
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById('test-' + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_category_code': $scope.filteredTodos[i].sims_category_code,
                            'sims_cur_code': $scope.filteredTodos[i].sims_cur_code,
                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/AttendanceCode/CUDAttendanceCode", deleteleave).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/AttendanceCode/getAttendanceCode").then(function (getAttendance_data) {
                                                $scope.getAttendancedata = getAttendance_data.data;
                                                $scope.totalItems = $scope.getAttendancedata.length;
                                                $scope.todos = $scope.getAttendancedata;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    //if(){}
                                    swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/AttendanceCode/getAttendanceCode").then(function (getAttendance_data) {
                                                $scope.getAttendancedata = getAttendance_data.data;
                                                $scope.totalItems = $scope.getAttendancedata.length;
                                                $scope.todos = $scope.getAttendancedata;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                data1 = [];
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.AttendanceCode_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.AttendanceCode_Data;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_cur_full_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_attendance_description.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_attendance_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                       item.sims_category_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                       item.sims_attendance_code_numeric.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                       item.sims_attendance_short_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                       item.sims_attendance_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_attendance_code == toSearch) ? true : false;
            }


            $scope.nullcheck = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].sims_category_code;
                        var v = document.getElementById(t + i);
                        v.checked = false;
                        $scope.row1 = '';
                    }
                }
                main.checked = false;
            }

            //var dom = '';
            //$scope.expand = function (info, $event) {
            //    console.log(info);
            //    $(dom).remove();

            //    $scope.readonly = true;
            //    dom = $("<tr><td class='details' colspan='12'>" +
            //        "<table class='inner-table' cellpadding='5' cellspacing='0' style='padding-left:20px'>" +
            //        "<tbody>" +
            //         "<tr><td class='semi-bold'>" + "PRESENT VALUE" +
            //         "</td><td class='semi-bold'>" + "ABSENT VALUE" +
            //         "</td><td class='semi-bold'>" + "TARDY VALUE" +
            //         "</td><td class='semi-bold'>" + "TYPE VALUE" +
            //         "</td><td class='semi-bold'>" + "COLOR CODE" +
            //         "</td><td class='semi-bold'>" + "STATUS INACTIVE DATE" +
            //         " </td></tr>" +

            //          "<tr><td>" + info.sims_attendance_present_value +
            //          "</td><td>" + info.sims_attendance_absent_value +
            //          "</td><td>" + info.sims_attendance_tardy_value +
            //          "</td><td>" + info.sims_attendance_type_value +
            //           "</td><td>" + info.sims_attendance_color_code +
            //          "</td><td>" + info.sims_attendance_status_inactive_date + "</tr>" +

            //         " </table></td></tr>")

            //    $($event.currentTarget).parents("tr").after(dom);

            //};


            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }]
        )
})();