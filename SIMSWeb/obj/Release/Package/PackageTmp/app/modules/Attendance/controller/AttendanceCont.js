﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Attendance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AttendanceCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            var username = $rootScope.globals.currentUser.username;
            //$scope.url = $rootScope.globals.currentSchool.lic_website_url;

            $http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (curiculum) {
                $scope.curiculum = curiculum.data;

                //if ($scope.curiculum.Length() > 1)
                //    $scope.sel_cur = $scope.curiculum[2].sims_cur_code;

            });

            $http.get(ENV.apiUrl + "api/attendance/getAttendancetype").then(function (atttypes) {
                $scope.atttypes = atttypes.data;
            });

            $http.get(ENV.apiUrl + "api/attendance/getAttendancesort").then(function (sorttypes) {
                $scope.sorttypes = sorttypes.data;
            });

            $scope.type_change = function () {

                if ($scope.type_section == "1") {
                    $http.get(ENV.apiUrl + "api/attendance/getAttendancesubtype").then(function (attsubtypes) {
                        $scope.attsubtypes = attsubtypes.data;
                    });
                }
                else {
                    $scope.attsubtypes = null;
                }

            }

            $scope.cur_change = function () {

                $http.get(ENV.apiUrl + "api/attendance/getAttendanceyear?cur_code=" + $scope.sel_cur).then(function (academicyears) {
                    $scope.academicyears = academicyears.data;
                });

                $http.get(ENV.apiUrl + "api/attendance/getAttendanceCode?cur_code=" + $scope.sel_cur).then(function (attendance_code) {
                    $scope.attendance_code = attendance_code.data;
                    //$scope.rdbtn = $scope.attendance_code[0].sims_attendance_code;
                });


            }

            $scope.ay_change = function () {
                $http.get(ENV.apiUrl + "api/attendance/getAttendancegrade?cur_code=" + $scope.sel_cur + "&academic_year=" + $scope.sel_ay + "&userName=" + username).then(function (grades) {
                    $scope.grades = grades.data;
                });

            }
            $scope.grade_change = function () {

                $http.get(ENV.apiUrl + "api/attendance/getAttendancesection?cur_code=" + $scope.sel_cur + "&academic_year=" + $scope.sel_ay + "&gradeCode=" + $scope.sel_grade + "&userName=" + username).then(function (section) {
                    $scope.section = section.data;
                });

            }
            $scope.totalstudents = 0;
            $scope.totalmarkedstudents = 0;
            $scope.totalunmarkedstudents = 0;


            $scope.getAttendance = function () {
                debugger;
                var date = $scope.att_date
                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                var date1 = year + "-" + month + "-" + day;
                // $scope.mycolor = 'background-color:red';
                $http.get(ENV.apiUrl + "api/attendance/getStudentAttendance?cur_code=" + $scope.sel_cur + "&ayear=" + $scope.sel_ay + "&grade=" + $scope.sel_grade + "&section=" + $scope.sel_section + "&attednacedate=" + date1).then(function (attendance) {

                    $scope.attendance = attendance.data;
                    $scope.totalstudents = $scope.attendance.length;
                    updateCount();


                });
            }


            $scope.selectedattobj;
            $scope.setattendance = function (obj) {
                $scope.selectedattobj = obj;
            }
            $scope.markattendnace = function (objinp) {
                var date = $scope.att_date
                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                var date1 = year + "-" + month + "-" + day;

                objinp.sims_attendance_code = $scope.selectedattobj.sims_attendance_code;
                objinp.sims_attendance_color = $scope.selectedattobj.sims_attendance_color;

                $http.post(ENV.apiUrl + "api/attendance/markStudentAttendance?cur_code=" + $scope.sel_cur + "&ayear=" + $scope.sel_ay + "&grade=" + $scope.sel_grade + "&section=" + $scope.sel_section + "&enrollNumber=" + objinp.sims_student_enroll + "&att_code=" + objinp.sims_attendance_code + "&attednacedate=" + date1).then(function (attendance) {
                });

                updateCount();

            }

            function updateCount() {
                var x = 0, cnt = 0;
                for (x = 0; x < $scope.totalstudents; x++) {

                    if ($scope.attendance[x].sims_attendance_code == 'UM')
                        cnt++;
                }
                $scope.totalunmarkedstudents = cnt;
                $scope.totalmarkedstudents = $scope.totalstudents - cnt;
            }

        }]);
})();


