﻿(function () {
    'use strict';

    var FromDateDay = new Date();
    var ToDateDay = new Date();
    var main;
    var simsController = angular.module('sims.module.Attendance');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CreateAttendanceForEmployeeCont',
      ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

          $scope.prevDisabled = true;
          $scope.subDisabled = true;
          $scope.Atten_Codes = false;
          $scope.stud_table = true;
          $scope.CreateAttendancebtn = true;
          //current date & user name
          $scope.temp = [];
          var today = new Date();
          var dd = today.getDate();
          var mm = today.getMonth() + 1;
          var yyyy = today.getFullYear();
          $scope.todays_date = dd + '-' + mm + '-' + yyyy;
          $scope.todays_date1 = yyyy + '-' + '0' + mm + '-' + dd;
          $scope.ddaayy = dd;

          $timeout(function () {
              $("#fixTable").tableHeadFixer({ 'top': 1 });
          }, 100);

          var Current_Username = $rootScope.globals.currentUser.username;

          $http.get(ENV.apiUrl + "api/CreateAttendanceForEmployee/getCompanyName").then(function (comp) {
              $scope.comp_data = comp.data;
              $scope.temp['company_code'] = $scope.comp_data[0].company_code
              $scope.Comp_Change($scope.comp_data[0].company_code);
          });

          $scope.attendance_codes_obj = {};


          $scope.get_attend_code = function (codes) {
              $scope.subDisabled = false;
              $scope.pays_attendance_color = codes.pays_attendance_color;
              $scope.pays_attendance_code = codes.pays_attendance_code;
          }

          $scope.Comp_Change = function (company_code) {
              $scope.CreateAttendancebtn = true;

              $http.get(ENV.apiUrl + "api/CreateAttendanceForEmployee/getAttendanceCode").then(function (attendance_code) {
                  $scope.attendance_codes_obj = attendance_code.data;
                  console.log($scope.attendance_codes_obj);
              });

              $http.get(ENV.apiUrl + "api/CreateAttendanceForEmployee/getDeptName?comp_code=" + company_code).then(function (dept) {
                  $scope.dept_data = dept.data;
                  $scope.Atten_Codes = true;
                  $scope.prevDisabled = false;
                  $scope.stud_table = false;
              });

              $http.get(ENV.apiUrl + "api/CreateAttendanceForEmployee/getGradeName").then(function (grade) {
                  $scope.grade_data = grade.data;
              });
              $http.get(ENV.apiUrl + "api/CreateAttendanceForEmployee/getStaffType").then(function (stafftype) {
                  $scope.stafftype_data = stafftype.data;
              });

              $http.get(ENV.apiUrl + "api/CreateAttendanceForEmployee/getDesignation").then(function (designation) {
                  $scope.designation_data = designation.data;
              });

              $http.get(ENV.apiUrl + "api/CreateAttendanceForEmployee/getYear").then(function (year) {
                  debugger;
                  $scope.year_data = year.data;
                  $scope.temp['sims_academic_year'] = $scope.year_data[0].sims_academic_year
                  $scope.getMonthbyYear($scope.year_data[0].sims_academic_year);
              });

          }
          $scope.data = [];
          $scope.getMonthbyYear = function (str) {


              $scope.CreateAttendancebtn = true;
              var startdate = new Date();
              var enddate = new Date();

              for (var i = 0; i < $scope.year_data.length; i++) {
                  if ($scope.year_data[i].sims_academic_year == str) {
                      startdate = moment($scope.year_data[i].sims_academic_year_start_date).format('YYYY/MM/DD');
                      enddate = moment($scope.year_data[i].sims_academic_year_end_date).format('YYYY/MM/DD');
                  }
              }


              var check_date = startdate;

              for (; new Date(check_date) < new Date(enddate) ;) {
                  check_date = moment(check_date, "YYYY/MM/DD").add('months', 1);
                  check_date = moment(check_date).format('YYYY/MM/DD');
                  $scope.data.push({ date: moment(check_date).format('MMMM-YYYY'), dateCode: moment(check_date).format('MM') })
              }
          }

          $scope.getDayz_in_month = function (sims_month_name, sims_year) {

              $scope.CreateAttendancebtn = true;
              var month = sims_month_name; //.split(0)[1];
              if (sims_month_name < 10) {
                  var year = sims_year;
              }
              else {
                  var year = sims_year - 1;
              }
              month = new Date(Date.parse(month + " 1," + year)).getMonth() + 1;
              $scope.rowscount = new Date(year, month, 0).getDate();
              FromDateDay = year + '-' + month + '-' + '1';
              ToDateDay = year + '-' + month + '-' + $scope.rowscount;
          }

          $scope.clear = function () {
              $scope.temp = [];
              $scope.CreateAttendancebtn = true;
              $scope.CreateAttendancebtn = [];
              $scope.dept_data = [];
              $scope.grade_data = [];
              $scope.stafftype_data = [];
              $scope.designation_data = [];
              $scope.pays_attendance_code = [];
              $scope.year_data = [];
              $scope.data = [];
              $scope.table = false;
          }

          $scope.CheckAllChecked = function () {
              debugger;
              main = document.getElementById('mainchk');
              if (main.checked == true) {
                  debugger;
                  for (var i = 0; i < $scope.AttendanceInfo.length; i++) {
                      var v = document.getElementById($scope.AttendanceInfo[i].em_login_code + i);
                      v.checked = true;
                      $scope.row1 = 'row_selected';
                  }
              }
              else {
                  for (var i = 0; i < $scope.AttendanceInfo.length; i++) {
                      var v = document.getElementById($scope.AttendanceInfo[i].em_login_code + i);
                      v.checked = false;
                      main.checked = false;
                      $scope.row1 = '';
                  }
              }

          }

          $scope.checkonebyonedelete = function () {
              $("input[type='checkbox']").change(function (e) {
                  if ($(this).is(":checked")) {
                      $(this).closest('tr').addClass("row_selected");
                      $scope.color = '#edefef';
                  } else {
                      $(this).closest('tr').removeClass("row_selected");
                      $scope.color = '#edefef';
                  }
              });
              main = document.getElementById('mainchk');
              if (main.checked == true) {
                  main.checked = false;
                  $scope.color = '#edefef';
                  $scope.row1 = '';
              }
          }

          $scope.SearchEmployees = function () {
              var company_code, dept_code, grade_code, stafftype_code, designation_code, em_login_code, FromDate, ToDate;
              if ($scope.temp == undefined || $scope.temp.company_code == null) {
                  swal({ title: "Alert", text: "Please Select Company", showCloseButton: true, width: 380, });
              }
              else if ($scope.temp.sims_academic_year == undefined || $scope.temp.sims_academic_year == null) {
                  swal({ title: "Alert", text: "Please Select Year", showCloseButton: true, width: 380, });
              }
              else if ($scope.temp.sims_academic_month == undefined || $scope.temp.sims_academic_month == null) {
                  swal({ title: "Alert", text: "Please Select Month", showCloseButton: true, width: 380, });
              }
              else {
                  if ($scope.temp == undefined) {
                      $scope.temp = "";
                  }
                  if ($scope.temp.company_code == undefined) {
                      company_code = '';
                  }
                  else {
                      company_code = $scope.temp.company_code
                  }
                  if ($scope.temp.dept_code == undefined) {
                      dept_code = '';
                  }
                  else {
                      dept_code = $scope.temp.dept_code
                  }
                  if ($scope.temp.gr_code == undefined) {
                      grade_code = '';
                  }
                  else {
                      grade_code = $scope.temp.gr_code
                  }

                  if ($scope.temp.staff_type_code == undefined) {
                      stafftype_code = '';
                  }
                  else {
                      stafftype_code = $scope.temp.staff_type_code
                  }
                  if ($scope.temp.dg_code == undefined) {
                      designation_code = '';
                  }
                  else {
                      designation_code = $scope.temp.dg_code
                  }
                  if ($scope.temp.empid == undefined) {
                      em_login_code = '';
                  }
                  else {
                      em_login_code = $scope.temp.empid
                  }
                  FromDate = FromDateDay;
                  ToDate = ToDateDay;
                  $http.get(ENV.apiUrl + "api/CreateAttendanceForEmployee/getAttendanceInfoOfEmloyee?company_code=" + company_code + "&dept_code=" + dept_code + "&grade_code=" + grade_code + "&stafftype_code=" + stafftype_code + "&designation_code=" + designation_code + "&em_login_code=" + em_login_code + "&FromDateDay=" + FromDate + "&ToDateDay=" + ToDate).then(function (res) {
                      $scope.AttendanceInfo = res.data;
                      if ($scope.AttendanceInfo.length > 0) {
                          $scope.CreateAttendancebtn = false;
                          $scope.table = true;
                      }
                      else {
                          swal({ title: "Alert", text: "Data Not Found", showCloseButton: true, width: 380, });
                          $scope.CreateAttendancebtn = true;
                          $scope.table = false;
                      }
                  });
              }
          }

          $scope.CreateAttendance = function () {

              var submitcode = [];
              $scope.flag = false;

              for (var i = 0; i < $scope.AttendanceInfo.length; i++) {
                  //var t = $scope.AttendanceInfo[i].uom_code;
                  var v = document.getElementById($scope.AttendanceInfo[i].em_login_code + i);
                  if (v.checked == true) {
                      $scope.flag = true;
                      var deletemodercode = ({
                          'em_login_code': $scope.AttendanceInfo[i].em_login_code,
                          'FromDateDay': FromDateDay,
                          'ToDateDay': ToDateDay,
                          'att_absent_flag': $scope.temp.pays_attendance_code,

                      });
                      submitcode.push(deletemodercode);
                  }
              }

              if ($scope.flag) {
                  debugger;
                  swal({
                      title: '',
                      text: "Are you sure you want to Create Attendance?",
                      showCloseButton: true,
                      showCancelButton: true,
                      confirmButtonText: 'Yes',
                      width: 380,
                      cancelButtonText: 'No',

                  }).then(function (isConfirm) {
                      if (isConfirm) {

                          $http.post(ENV.apiUrl + "api/CreateAttendanceForEmployee/CreateEmployeeAttendance", submitcode).then(function (res) {
                              $scope.msg1 = res.data;
                              if ($scope.msg1 != "" && $scope.msg1 != null) {
                                  swal({ title: "Alert", text: "Employee Attendance Created Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                      if (isConfirm) {
                                          $scope.SearchEmployees();
                                          main = document.getElementById('mainchk');
                                          if (main.checked == true) {
                                              main.checked = false;
                                              $scope.row1 = '';
                                          }
                                          $scope.currentPage = true;


                                      }
                                  });
                              }
                              else {
                                  swal({ title: "Alert", text: "Employee Attendance Not Created", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                      if (isConfirm) {
                                          $scope.SearchEmployees();
                                          if (main.checked == true) {
                                              main.checked = false;
                                              $scope.row1 = '';
                                          }
                                          $scope.currentPage = true;

                                      }
                                  });
                              }

                          });
                      }
                      else {

                          main = document.getElementById('mainchk');
                          if (main.checked == true) {
                              main.checked = false;
                          }
                          for (var i = 0; i < $scope.AttendanceInfo.length; i++) {
                              var v = document.getElementById($scope.AttendanceInfo[i].em_login_code + i);
                              //var t = $scope.AttendanceInfo[i].sims_fee_category;

                              if (v.checked == true) {
                                  v.checked = false;
                                  //$scope.row1 = '';
                                  main.checked = false;
                                  $('tr').removeClass("row_selected");
                              }


                          }

                      }


                  });
              }
              else {
                  swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
              }

              $scope.currentPage = str;
          }

      }])

})();