﻿(function () {
    "use strict";
    angular.module('config', [])
   .constant('ENV', {
       name: 'production',
       apiUrl: 'http://localhost:8081/SIMSAPI/', //Always end with '/'
       siteUrl: 'http://localhost:8081/SIMSWeb/',
       reportUrl: 'http://localhost/SIMSAPI/api/reports/'

    //   apiUrl: 'http://api.mograsys.com/APIERP/', //Always end with '/'
    //siteUrl: 'http://main.mograsys.com/',
    //reportUrl: 'http://api.mograsys.com/APIERP/api/reports/'
   });
})();