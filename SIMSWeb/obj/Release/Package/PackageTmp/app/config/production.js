﻿(function () {
    "use strict";
    angular.module('config', [])
   .constant('ENV', {
       name: 'production',
       apiUrl: 'http://localhost/SIMSAPI/', //Always end with '/'
       siteUrl: 'http://localhost/SIMSWeb/',
       reportUrl: 'http://localhost/SIMSAPI/api/reports/'

       //apiUrl: 'http://api.mograsys.com/APIERP/', //Always end with '/'
       //siteUrl: 'http://sis.mograsys.com/',
       //reportUrl: 'http://api.mograsys.com/APIERP/api/reports/'
   });
})();