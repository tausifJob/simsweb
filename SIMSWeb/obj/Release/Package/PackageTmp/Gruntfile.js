// Generated on 2015-08-19 using generator-angular 0.12.1
'use strict';

// # Globbing
// for performance reasons we're only matching one level down:
// 'test/spec/{,*/}*.js'
// use this if you want to recursively match all subfolders:
// 'test/spec/**/*.js'

module.exports = function (grunt) {

    // Time how long tasks take. Can help when optimizing build times
    require('time-grunt')(grunt);

    // Automatically load required Grunt tasks
    require('jit-grunt')(grunt, {
        useminPrepare: 'grunt-usemin',
        ngtemplates: 'grunt-angular-templates',
        cdnify: 'grunt-google-cdn'
    });

    // Configurable paths for the application
    var appConfig = {
        app: 'app',
        dist: 'dist'
    };

    // Define the configuration for all the tasks
    grunt.initConfig({

        // Project settings
        yeoman: appConfig,

        // Empties folders to start fresh
        clean: {
            dist: {
                files: [{
                    dot: true,
                    src: [
                      '.tmp',
                      '<%= yeoman.dist %>/{,*/}*',
                      '!<%= yeoman.dist %>/.git{,*/}*'
                    ]
                }]
            },
            server: '.tmp'
        },

        // Renames files for browser caching purposes
        filerev: {
            dist: {
                src: [
                  '<%= yeoman.dist %>/scripts/{,*/}*.js',
                  '<%= yeoman.dist %>/styles/{,*/}*.css',
                  '<%= yeoman.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}',
                  '<%= yeoman.dist %>/styles/fonts/*'
                ]
            }
        },

        // Reads HTML for usemin blocks to enable smart builds that automatically
        // concat, minify and revision files. Creates configurations in memory so
        // additional tasks can operate on them
        useminPrepare: {
            html: 'index.html',
            options: {
                dest: '<%= yeoman.dist %>',
                flow: {
                    html: {
                        steps: {
                            js: ['concat', 'uglifyjs'],
                            css: ['cssmin']
                        },
                        post: {}
                    }
                }
            }
        },

        // Performs rewrites based on filerev and the useminPrepare configuration
        usemin: {
            html: ['<%= yeoman.dist %>/{,*/}*.html'],
        },

        processhtml: {
            build: {
                files: {
                    '<%= yeoman.dist %>/index.html': ['index.html']
                }
            },
            options: {
                
            }
        },

        imagemin: {
            dist: {
                files: [{
                    expand: true,
                    src: '<%= yeoman.dist %>/assets/theme/img/*.{png,jpg,jpeg,gif}',
                    dest: '<%= yeoman.dist %>'
                }]
            }
        },

        svgmin: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.dist %>/assets/theme/img/',
                    src: '{,*/}*.svg',
                    dest: '<%= yeoman.dist %>'
                }]
            }
        },

        htmlmin: {
            dist: {
                options: {
                    collapseWhitespace: true,
                    conservativeCollapse: true,
                    collapseBooleanAttributes: true,
                    removeCommentsFromCDATA: true
                },
                files: [{
                    expand: true,
                    cwd: '<%= yeoman.dist %>',
                    src: ['*.html'],
                    dest: '<%= yeoman.dist %>'
                }]
            }
        },

        ngtemplates: {
            dist: {
                options: {
                    module: 'bmsApp',
                    htmlmin: '<%= htmlmin.dist.options %>',
                    usemin: 'scripts/scripts.js'
                },
                cwd: '<%= yeoman.app %>',
                src: 'views/{,*/}*.html',
                dest: '.tmp/templateCache.js'
            }
        },

        // ng-annotate tries to make the code safe for minification automatically
        // by using the Angular long form for dependency injection.
        ngAnnotate: {
            dist: {
                files: [{
                    expand: true,
                    cwd: '.tmp/concat/scripts',
                    src: '*.js',
                    dest: '.tmp/concat/scripts'
                }]
            }
        },

        // Replace Google CDN references
        cdnify: {
            dist: {
                html: ['<%= yeoman.dist %>/*.html']
            }
        },

        // Copies remaining files to places other tasks can use
        copy: {
            dist: {
                files: [ {
                    expand: true,
                    dest: '<%= yeoman.dist %>',
                    src: [
                        'app/modules/**/views/*.html',
                        'assets/theme/css/ltr/**/*.css',
                        'assets/theme/css/rtl/**/*.css',
                        'assets/theme/img/**/*.{png,jpg,jpeg,gif,svg}',
                        'index.html'
                    ]
                }, {
                    expand: true,
                    cwd: "assets/theme/fonts/",
                    dest: '<%= yeoman.dist %>/fonts/',
                    src: [
                        "*.*",
                    ]
                }, {
                    expand: true,
                    cwd: "assets/libs/bower/font-awesome/fonts/",
                    dest: '<%= yeoman.dist %>/fonts/',
                    src: [
                        "*.*"
                    ]
                }]
            }
        },expand : true,

        // Run some tasks in parallel to speed up the build process
        concurrent: {
            dist: [
              'imagemin',
              'svgmin'
            ]
        }
    });

    grunt.registerTask('build', [
      'clean:dist',
      //'wiredep',
      'useminPrepare',
      'concurrent:dist',
      //'autoprefixer',
      //'ngtemplates',
      'concat',
      'ngAnnotate',
      'copy:dist',
      'cdnify',
      'cssmin',
      'uglify',
      'filerev',
      'usemin',
      'htmlmin'
    ]);

    grunt.registerTask('default', [
      //'newer:jshint',
      //'test',
      'build'
    ]);

    grunt.registerTask('cdn', [
        'clean:dist',
        'useminPrepare',
        'processhtml',
        'concat',
        'ngAnnotate',
        'copy:dist',
        //'concurrent:dist',
        'cssmin',
        'uglify',
        'usemin',
        'htmlmin'
        //'cdnify'
    ]);

};
