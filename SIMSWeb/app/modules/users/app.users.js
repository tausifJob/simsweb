﻿(function () {
    'use strict';
    angular.module('sims.module.users', [
        'sims',
        'gettext'
    ]);
})();