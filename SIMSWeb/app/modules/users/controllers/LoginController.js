﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.users');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('LoginController',
        ['$scope', '$state', '$rootScope', 'AuthenticationService', '$timeout', '$location', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, AuthenticationService, $timeout, $location, gettextCatalog, $http, ENV) {

            $scope.username = "";
            $scope.password = "";
            $scope.schoolUrl = "";
            $scope.user = {};
            $scope.signup = function () { }
            $scope.showForgetPaaword = false;
            $scope.toggleLoginScreen = function () {
                $scope.showForgetPaaword = !$scope.showForgetPaaword;
            };

            $scope.schoolUrl = window.location.href;
            var conStr = $scope.schoolUrl.substring($scope.schoolUrl.indexOf('/') + 2, $scope.schoolUrl.indexOf('.'));
            if (conStr == 'www' || conStr == 'WWW') {
                $scope.schoolUrl = window.location.href;

                var conStr = $scope.schoolUrl.split('.')[1];
                $http.defaults.headers.common['schoolId'] = conStr;

            }
            else {
                $http.defaults.headers.common['schoolId'] = 'sis';
            }

            if (conStr == 'main') {
                if (document.referrer != "") {
                    var referringURL = document.referrer;
                    $scope.schoolUrl = referringURL;
                    var conStr = $scope.schoolUrl.substring($scope.schoolUrl.indexOf('/') + 2, $scope.schoolUrl.indexOf('.'));
                    $http.defaults.headers.common['schoolId'] = conStr;


                    console.log(referringURL);
                    var local = referringURL.substring(referringURL.indexOf("?"), referringURL.length);
                    console.log(local);


                }
            }

            $http.get(ENV.apiUrl + "api/common/getSchoolDetailsNew").then(function (res) {
                $scope.display = true;
                $scope.obj1 = res.data;
                $scope.SchoolDetails = res.data;

           
            var msalconfig = {

                //clientID: "487343a6-31f8-413d-b82c-a97a2aed0348",
                //redirectUri: "https://test.mograsys.com/login"

                clientID:   $scope.SchoolDetails.lic_school_microsoft_client_id,  //"e623a34d-4e13-4f1f-a112-dde80b6abf84",
                redirectUri: $scope.SchoolDetails.lic_school_microsoft_redirect_url  //"https://" + $http.defaults.headers.common['schoolId'] + ".mograsys.com/login"
            };


            // Graph API endpoint to show user profile
            var graphApiEndpoint = "https://graph.microsoft.com/v1.0/me";

            // Graph API scope used to obtain the access token to read user profile
            var graphAPIScopes = ["https://graph.microsoft.com/user.read"];

            // Initialize application
            var userAgentApplication = new Msal.UserAgentApplication(msalconfig.clientID, null, loginCallback, {
                redirectUri: msalconfig.redirectUri
            });

            //Previous version of msal uses redirect url via a property
            if (userAgentApplication.redirectUri) {
                userAgentApplication.redirectUri = msalconfig.redirectUri;
            }

            window.onload = function () {
                // If page is refreshed, continue to display user info
                if (!userAgentApplication.isCallback(window.location.hash) && window.parent === window && !window.opener) {
                    var user = userAgentApplication.getUser();
                    if (user) {
                        callGraphApi();
                    }
                }
            }

            $scope.te_graph = function () {

              
                    var user = userAgentApplication.getUser();
                    if (!user) {
                        // If user is not signed in, then prompt user to sign in via loginRedirect.
                        // This will redirect user to the Azure Active Directory v2 Endpoint
                        userAgentApplication.loginRedirect(graphAPIScopes);
                        // The call to loginRedirect above frontloads the consent to query Graph API during the sign-in.
                        // If you want to use dynamic consent, just remove the graphAPIScopes from loginRedirect call.
                        // As such, user will be prompted to give consent when requested access to a resource that 
                        // he/she hasn't consented before. In the case of this application - 
                        // the first time the Graph API call to obtain user's profile is executed.
                    } else {
                        // If user is already signed in, display the user info
                        var userInfoElement = document.getElementById("userInfo");
                        userInfoElement.parentElement.classList.remove("hidden");
                        userInfoElement.innerHTML = JSON.stringify(user, null, 4);

                        $scope.showMessage = true;
                        $scope.messageClass = "alert alert-warning alert-dismissable";
                        $scope.message = "Athenticating please wait.";
                        $scope.show_auth = true;

                        // Show Sign-Out button
                        document.getElementById("signOutButton").classList.remove("hidden");

                        // Now Call Graph API to show the user profile information:
                        var graphCallResponseElement = document.getElementById("graphResponse");
                        graphCallResponseElement.parentElement.classList.remove("hidden");
                        graphCallResponseElement.innerText = "Calling Graph ...";

                        // In order to call the Graph API, an access token needs to be acquired.
                        // Try to acquire the token used to query Graph API silently first:
                        userAgentApplication.acquireTokenSilent(graphAPIScopes)
                            .then(function (token) {
                                //After the access token is acquired, call the Web API, sending the acquired token
                                callWebApiWithToken(graphApiEndpoint, token, graphCallResponseElement, document.getElementById("accessToken"));

                            }, function (error) {
                                // If the acquireTokenSilent() method fails, then acquire the token interactively via acquireTokenRedirect().
                                // In this case, the browser will redirect user back to the Azure Active Directory v2 Endpoint so the user 
                                // can reenter the current username/ password and/ or give consent to new permissions your application is requesting.
                                // After authentication/ authorization completes, this page will be reloaded again and callGraphApi() will be executed on page load.
                                // Then, acquireTokenSilent will then get the token silently, the Graph API call results will be made and results will be displayed in the page.
                                if (error) {
                                    userAgentApplication.acquireTokenRedirect(graphAPIScopes);
                                }
                            });
                    }
                
            }

            /**
             * Call the Microsoft Graph API and display the results on the page. Sign the user in if necessary
             */
             function callGraphApi() {
                 var user = userAgentApplication.getUser();
                 $scope.user_lst = user;
                if (!user) {
                    // If user is not signed in, then prompt user to sign in via loginRedirect.
                    // This will redirect user to the Azure Active Directory v2 Endpoint
                    userAgentApplication.loginRedirect(graphAPIScopes);
                    // The call to loginRedirect above frontloads the consent to query Graph API during the sign-in.
                    // If you want to use dynamic consent, just remove the graphAPIScopes from loginRedirect call.
                    // As such, user will be prompted to give consent when requested access to a resource that 
                    // he/she hasn't consented before. In the case of this application - 
                    // the first time the Graph API call to obtain user's profile is executed.
                } else {
                    // If user is already signed in, display the user info
                    var userInfoElement = document.getElementById("userInfo");
                    userInfoElement.parentElement.classList.remove("hidden");

                    userInfoElement.innerHTML = JSON.stringify(user, null, 4);
                    $scope.showMessage = true;
                    $scope.messageClass = "alert alert-warning alert-dismissable";
                    $scope.message = "Athenticating please wait.";
                    $scope.show_auth = true;
                    
                    // Show Sign-Out button
                    document.getElementById("signOutButton").classList.remove("hidden");

                    // Now Call Graph API to show the user profile information:
                    var graphCallResponseElement = document.getElementById("graphResponse");
                    graphCallResponseElement.parentElement.classList.remove("hidden");
                    graphCallResponseElement.innerText = "Calling Graph ...";

                    // In order to call the Graph API, an access token needs to be acquired.
                    // Try to acquire the token used to query Graph API silently first:
                    userAgentApplication.acquireTokenSilent(graphAPIScopes)
                        .then(function (token) {
                            //After the access token is acquired, call the Web API, sending the acquired token
                            callWebApiWithToken(graphApiEndpoint, token, graphCallResponseElement, document.getElementById("accessToken"));

                        }, function (error) {
                            // If the acquireTokenSilent() method fails, then acquire the token interactively via acquireTokenRedirect().
                            // In this case, the browser will redirect user back to the Azure Active Directory v2 Endpoint so the user 
                            // can reenter the current username/ password and/ or give consent to new permissions your application is requesting.
                            // After authentication/ authorization completes, this page will be reloaded again and callGraphApi() will be executed on page load.
                            // Then, acquireTokenSilent will then get the token silently, the Graph API call results will be made and results will be displayed in the page.
                            if (error) {
                                userAgentApplication.acquireTokenRedirect(graphAPIScopes);
                            }
                        });
                }
            }

            /**
             * Callback method from sign-in: if no errors, call callGraphApi() to show results.
             * @param {string} errorDesc - If error occur, the error message
             * @param {object} token - The token received from login
             * @param {object} error - The error string
             * @param {string} tokenType - The token type: For loginRedirect, tokenType = "id_token". For acquireTokenRedirect, tokenType:"access_token".
             */
            function loginCallback(errorDesc, token, error, tokenType) {
                if (errorDesc) {
                    showError(msal.authority, error, errorDesc);
                } else {
                    callGraphApi();
                }
            }

            /**
             * Show an error message in the page
             * @param {string} endpoint - the endpoint used for the error message
             * @param {string} error - Error string
             * @param {string} errorDesc - Error description
             */
            function showError(endpoint, error, errorDesc) {
                var formattedError = JSON.stringify(error, null, 4);
                if (formattedError.length < 3) {
                    formattedError = error;
                }
                document.getElementById("errorMessage").innerHTML = "An error has occurred:<br/>Endpoint: " + endpoint + "<br/>Error: " + formattedError + "<br/>" + errorDesc;
                console.error(error);
            }

            /**
             * Call a Web API using an access token.
             * @param {any} endpoint - Web API endpoint
             * @param {any} token - Access token
             * @param {object} responseElement - HTML element used to display the results
             * @param {object} showTokenElement = HTML element used to display the RAW access token
             */
            function callWebApiWithToken(endpoint, token, responseElement, showTokenElement) {
                var headers = new Headers();
                var bearer = "Bearer " + token;
                headers.append("Authorization", bearer);
                var options = {
                    method: "GET",
                    headers: headers
                };

                fetch(endpoint, options)
                    .then(function (response) {
                        var contentType = response.headers.get("content-type");
                        if (response.status === 200 && contentType && contentType.indexOf("application/json") !== -1) {
                            response.json()
                                .then(function (data) {
                                    // Display response in the page
                                    console.log(data);
                                    console.log($scope.user_lst);

                                    $http.get(ENV.apiUrl + "api/common/getuserpass?email_id=" + data.userPrincipalName).then(function (res) {
                                        console.log(res.data);
                                        if (res.data == '') {
                                            swal({ title: "Alert", text: "Your email Id is not found please contact to administrator", imageUrl: "assets/img/notification-alert.png", });
                                            $scope.show_auth = false;

                                        }
                                        else {
                                            if (res.data.length == 1) {
                                               // $scope.username =res.data[0].comn_user_name
                                                //$scope.password = res.data[0].comn_user_password
                                               $scope.Login_microsoft(res.data[0].comn_user_name, res.data[0].comn_user_password);
                                            }
                                        }

                                    });

                                    
                                    responseElement.innerHTML = JSON.stringify(data, null, 4);
                                    if (showTokenElement) {
                                        showTokenElement.parentElement.classList.remove("hidden");
                                        showTokenElement.innerHTML = token;
                                    }
                                })
                                .catch(function (error) {
                                    showError(endpoint, error);
                                });
                        } else {
                            response.json()
                                .then(function (data) {
                                    // Display response as error in the page
                                    showError(endpoint, data);
                                })
                                .catch(function (error) {
                                    showError(endpoint, error);
                                });
                        }
                    })
                    .catch(function (error) {
                        showError(endpoint, error);
                    });
            }


            /**
             * Sign-out the user
             */
            function signOut() {
                userAgentApplication.logout();
            }

            $scope.signOutnew = function () {
                userAgentApplication.logout();

            }


            });



            $scope.display_login = false;
            //if (document.referrer != "") {
            //    var referringURL = document.referrer;
            //    $scope.schoolUrl = referringURL;
            //    var conStr = $scope.schoolUrl.substring($scope.schoolUrl.indexOf('/') + 2, $scope.schoolUrl.indexOf('.'));
            //    $http.defaults.headers.common['schoolId'] = conStr;


            //    console.log(referringURL);
            //    var local = referringURL.substring(referringURL.indexOf("?"), referringURL.length);
            //    console.log(local);

            //    //location.href = "http://page.com/login" + local; 
            //} 

           // debugger
           // var encoded_string = Base64.encode('2018-02-01 10:57' + '~' + 'S591' + '~' + '36ad8c46fed42f9fa77924b4821ed2062ae8cc14');


           

            $scope.SchoolLogo = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/SchoolLogo/';


            if ($http.defaults.headers.common['schoolId'] == 'asd' || $http.defaults.headers.common['schoolId'] == 'siserp') {
                $scope.show_opac = true;
            }
            $scope.opac_link = function () {
                $state.go("login1");
            }

            $scope.cnt_attempt = 0;
            

            $scope.Login = function () {

                if ($scope.showForgetPaaword) {
                    $scope.toggleLoginScreen();
                } else {
                    if ($scope.cnt_attempt < 3) {
                        $scope.showMessage = false;
                        $http.get(ENV.apiUrl + "api/common/getlastLogin?uname=" + $scope.username).then(function (res1) {

                            $rootScope.isfistTimeLog = res1.data;
                            AuthenticationService.Login($scope.username, $scope.password, function (response) {
                                console.log(response);

                                $scope.showMessage = true;
                                if (response.success == false) {
                                    $scope.showMessage = true;
                                    $scope.messageClass = "alert alert-warning alert-dismissable";
                                    $scope.message = "Invalid username or password!!!";
                                    $scope.cnt_attempt++;
                                } else {
                                    $scope.showMessage = true;
                                    $scope.messageClass = "alert alert-success alert-dismissable";
                                    AuthenticationService.ClearCredentials();
                                    AuthenticationService.SetCredentials($scope.username, $scope.password);
                                    $scope.message = "Successfully Authenticated!!!";
                                    $timeout(function () {
                                        window.localStorage.removeItem("Finn_comp");

                                        // $state.go("main.EmpDas");
                                        $state.go("main.TimeLine");
                                    });
                                }
                            });

                        });

                    }
                    else {
                        $scope.showMessage = true;
                        $scope.messageClass = "alert alert-warning alert-dismissable";
                        $scope.message = "You tried many attempt please try again later!!!";
                    }
                }
            };



            $scope.Login_microsoft = function (user,pass) {

                if ($scope.showForgetPaaword) {
                    $scope.toggleLoginScreen();
                } else {

                    $scope.showMessage = false;
                    $http.get(ENV.apiUrl + "api/common/getlastLogin?uname=" + user).then(function (res1) {

                        $rootScope.isfistTimeLog = res1.data;
                        AuthenticationService.Login(user, pass, function (response) {
                            console.log(response);
                            $scope.showMessage = true;
                            if (response.success == false) {
                                $scope.showMessage = true;
                                $scope.messageClass = "alert alert-warning alert-dismissable";
                                $scope.message = "Invalid username or password!!!";
                            } else {
                                $scope.showMessage = true;
                                $scope.messageClass = "alert alert-success alert-dismissable";
                                AuthenticationService.ClearCredentials();
                                AuthenticationService.SetCredentials(user, pass);
                                $scope.message = "Successfully Authenticated!!!";
                                $timeout(function () {
                                    window.localStorage.removeItem("Finn_comp");

                                    window.localStorage["microsoft_flg"] = true;
                                    // $state.go("main.EmpDas");
                                    $state.go("main.TimeLine");
                                });
                            }
                        });

                    });


                }
            };


            var qs = $location.search();
            if (!angular.equals({}, qs)) {
                $scope.display_login = true;

                //$('body').css('background-image', ' !important');
               
                    
                var t = qs.q.split("-")
                if (t.length >= 2) {
                    $scope.username = t[1];
                    $scope.password =Base64.decode(t[0])  ;
                }
               // console.log($scope.username + '' + $scope.password)
                $scope.Login();
            }

            //$(window).bind('keydown', function (event) {

            //    if (event.keyCode == '13') {





            //        $scope.Login();

            //    }
            //});

            $scope.onKeyPress = function ($event) {
                if ($event.keyCode == 13) {

                    $scope.Login();
                    // console.log("enetr");
                }
            };


          

            $scope.forgotPassword_link = function () {
                $scope.email_id = false;

                $http.get(ENV.apiUrl + "api/authenticate/GetGroupsNew").then(function (res) {
                    $scope.grp = res.data;

                });
                //$('#UploadDocModal').modal({ backdrop: 'static', keyboard: false });

            }


            $scope.getemailId = function () {
                $scope.email_id = true;
            }


            $scope.Save = function () {
                //  if (isvalidate)
                if ($scope.edt.sims_email_id != undefined) {
                    var code = "";
                    $http.get(ENV.apiUrl + "api/authenticate/GetUserName?strEmailID=" + $scope.edt.sims_email_id + "&strCode=" + code + "&strgroupname=" + $scope.edt.comn_user_group_code).then(function (res) {
                        $scope.emaildata = res.data;
                        console.log($scope.emaildata);
                        if (res.data != 'false') {

                            $scope.edt.mail_success = "Your Username is :'" + $scope.emaildata + "'";
                        }
                        else {
                            $scope.edt.mail_success = "No User Name is Found!!!";
                        }

                    });
                }
                if ($scope.edt.sims_email_id == undefined) {
                    $scope.edt.mail_success = "Please Enter Email";
                }

            }

            $scope.Cancel = function () {
                $scope.edt.mail_success_lbl = "";
                $scope.edt.mail_success = "";
                $scope.edt.sims_email_id = "";

                $scope.edt.comn_user_group_code = null;
                $scope.email_id = false;
            }

            $scope.edt =
             {
                 sims_user_name_en: undefined
             }

            $scope.pwdsave = function () {
                debugger
                $scope.fbtn = true;
                if (!$scope.flgMobi) {
                    if ($scope.securitydata == $scope.edt.emailId) {
                        $scope.pwd = false;

                        // $http.get(ENV.apiUrl + "api/authenticate/getResetPasswordNew?strUsername=" + $scope.edt.sims_user_name_en).then(function (res_pwd) {
                        var data = {
                            emailsendto: $scope.securitydata,
                            sender_emailid: $scope.securitydata,
                            subject: 'Parent Password',
                            //body: 'Your Password is:' + res_pwd.data,
                            body: 'please click here for reset password http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/ppn/ChangePassword.html#?foo=' + $scope.edt.sims_user_name_en
                        }

                        $http.get(ENV.apiUrl + "api/authenticate/GetSendEmail?strUsername=" + Base64.encode($scope.edt.sims_user_name_en) + "&email=" + $scope.securitydata).then(function (res_pwd) {
                            $scope.fbtn = false;
                            $scope.pwd = true;
                            $scope.edt.pwd_success = 'Your password is sent to your email id';
                        });
                        //  });
                    }
                    else {
                        $scope.pwd = true;

                        $scope.edt.pwd_success = "Please Enter Correct Email!!!";
                        $scope.fbtn = false;

                    }
                }
                else {
                    $scope.pwd = false;

                    debugger
                    if ($scope.securitydata == $scope.edt.emailId) {

                        // $http.get(ENV.apiUrl + "api/authenticate/getResetPasswordNew?strUsername=" + $scope.edt.sims_user_name_en).then(function (res_pwd) {
                        //$scope.pwd = true;
                        //var data = {
                        //    emailsendto: $scope.securitydata,
                        //    sender_emailid: $scope.securitydata,
                        //    //subject: 'Parent Password',
                        //    //body: 'Your Password is:' + res_pwd.data,
                        //    body: 'please click here for reset password http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/ppn/ChangePassword.html#?foo=' + $scope.edt.sims_user_name_en
                        //}



                        $http.post(ENV.apiUrl + "api/authenticate/sendmobile?strUsername=" + $scope.edt.sims_user_name_en + '&mobile=' + $scope.securitydata + '&msg=' + 'please click here for reset password http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/ppn/ChangePassword.html#?foo=' + $scope.edt.sims_user_name_en, data).then(function (res_pwd) {
                            // $scope.fbtn = false;
                            $scope.pwd = true;

                            $scope.edt.pwd_success = 'Your password is sent to your mobile number';
                            $scope.fbtn = false;
                        });
                        //  });
                    }
                    else {
                        $scope.pwd = true;

                        $scope.edt.pwd_success = "Please Enter Correct Mobile!!!";
                        $scope.fbtn = false;

                    }

                }
                //if ($scope.edt.sims_user_name_en != null || $scope.edt.sims_user_name_en != undefined) {
                //    if ($scope.securitydata != null) {
                //        var str = $scope.securitydata.indexOf('~');
                //        var ques_code = $scope.securitydata.substr(str + 1);
                //    }

                //    if ($scope.edt.sims_seq_ans != undefined) {
                //        if ($scope.edt.sims_seq_ans != null || $scope.edt.sims_seq_ans != "") {
                //            $http.get(ENV.apiUrl + "api/authenticate/getResetPassword?strUsername=" + $scope.edt.sims_user_name_en + "&strSecurityCode=" + ques_code + "&strSecurityAns=" + $scope.edt.sims_seq_ans).then(function (res_pwd) {
                //                $scope.resetpwd = res_pwd.data;
                //                console.log($scope.resetpwd);
                //                if (res_pwd.data != 'false') {
                //                    $scope.edt.pwd_success = "Your New Password is:'" + $scope.resetpwd + "'";
                //                }
                //                else if (res_pwd.data == 'false') {
                //                    $scope.edt.pwd_success = "Please Fill Correct Information!!!";
                //                }
                //            });
                //        }
                //        else {
                //            $scope.edt.pwd_success = "Please Enter Security Answer!!!";
                //        }
                //    }
                //    else if ($scope.edt.sims_seq_ans == undefined) {
                //        $scope.edt.sims_seq_ans = "";
                //        $scope.edt.pwd_success = "Please Enter Security Answer!!!";
                //    }
                //}
                //else if ($scope.edt.sims_user_name_en == null || $scope.edt.sims_user_name_en == undefined) {

                //    $scope.edt.pwd_success = "Please Enter User Name!!!";
                //}
            }

            $scope.Cancel1 = function () {
                $scope.edt.sims_user_name_en = "";
                $scope.edt.pwd_success = "";
                $scope.edt.pwd_success_lbl = "";
                $scope.edt.sims_seq_ans = "";
                $scope.edt.sims_seq_que = "";

                $scope.edt.comn_user_group_code = "";
                //  $scope.edt = undefined;
                //  $scope.email_id = false;
                $('#UploadDocModal').modal('hide');

            }

            $scope.pwd = false;
            var arr;


            $scope.getsecurity = function () {

                if ($scope.edt.sims_user_name_en == null || $scope.edt.sims_user_name_en == '' || $scope.edt.sims_user_name_en == undefined) {
                    $scope.pwd = true;
                    $scope.edt.pwd_success = "Please Enter User Name!!!";

                }

                else {

                    $scope.pwd = false;

                    $http.get(ENV.apiUrl + "api/authenticate/GetEmail?strUsername=" + $scope.edt.sims_user_name_en).then(function (res) {
                        $scope.securitydata = res.data;
                        debugger;
                        console.log($scope.securitydata);
                        if ($scope.securitydata != "") {
                            // $scope.pwd = true;

                            //var str = $scope.securitydata.indexOf('~');
                            // arr = $scope.securitydata.substr(0, str);
                            //  $scope.edt.sims_seq_que = arr;
                        }
                        else if ($scope.securitydata == null || $scope.securitydata == "" || $scope.securitydata == undefined) {
                            $scope.edt.sims_seq_que = "";
                            //for Mobile
                            $scope.flgMobi = true;

                            $http.get(ENV.apiUrl + "api/authenticate/GetMobile?strUsername=" + $scope.edt.sims_user_name_en).then(function (res) {
                                debugger
                                $scope.securitydata = res.data;
                                if (res.data == "") {
                                    swal({ title: "Alert", text: "Your email and mobile number is not found please contact to administrator", imageUrl: "assets/img/notification-alert.png", });

                                }
                            });
                            // $scope.edt.pwd_success = "No Email id is Found.Check User Name!!!";
                        }

                    });
                }
                //else if ($scope.edt.sims_user_name_en == undefined) {
                //    $scope.edt.pwd_success = "Please Enter User Name!!!";
                //}
            }


          



        }]);

    var Base64 = {

        keyStr: 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=',

        encode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            do {
                chr1 = input.charCodeAt(i++);
                chr2 = input.charCodeAt(i++);
                chr3 = input.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
                enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                output = output +
                    this.keyStr.charAt(enc1) +
                    this.keyStr.charAt(enc2) +
                    this.keyStr.charAt(enc3) +
                    this.keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";
            } while (i < input.length);

            return output;
        },

        decode: function (input) {
            var output = "";
            var chr1, chr2, chr3 = "";
            var enc1, enc2, enc3, enc4 = "";
            var i = 0;

            // remove all characters that are not A-Z, a-z, 0-9, +, /, or =
            var base64test = /[^A-Za-z0-9\+\/\=]/g;
            if (base64test.exec(input)) {
                window.alert("There were invalid base64 characters in the input text.\n" +
                    "Valid base64 characters are A-Z, a-z, 0-9, '+', '/',and '='\n" +
                    "Expect errors in decoding.");
            }
            input = input.replace(/[^A-Za-z0-9\+\/\=]/g, "");

            do {
                enc1 = this.keyStr.indexOf(input.charAt(i++));
                enc2 = this.keyStr.indexOf(input.charAt(i++));
                enc3 = this.keyStr.indexOf(input.charAt(i++));
                enc4 = this.keyStr.indexOf(input.charAt(i++));

                chr1 = (enc1 << 2) | (enc2 >> 4);
                chr2 = ((enc2 & 15) << 4) | (enc3 >> 2);
                chr3 = ((enc3 & 3) << 6) | enc4;

                output = output + String.fromCharCode(chr1);

                if (enc3 != 64) {
                    output = output + String.fromCharCode(chr2);
                }
                if (enc4 != 64) {
                    output = output + String.fromCharCode(chr3);
                }

                chr1 = chr2 = chr3 = "";
                enc1 = enc2 = enc3 = enc4 = "";

            } while (i < input.length);

            return output;
        }


    };
})();