﻿(function () {
    'use strict';
    angular.module('sims.module.Workflow', [
        'sims',
        'gettext'
    ]);
})();