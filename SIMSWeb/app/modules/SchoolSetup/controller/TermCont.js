﻿(function () {
    'use strict';
    var del = [];
    var main;
    var date1, date3;
    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('TermCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.term_data = [];
            $scope.edit_code = false;
            var data1 = [];
            var deletecode = [];
            $scope.edt = {};
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {
                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });


            $timeout(function () {
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);

          

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 5;

            $scope.countData = [

                { val: 10, data: 10 },
                { val: 20, data: 20 }

            ]

            //$http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (res) {
            //    $scope.obj2 = res.data;
            //    $scope.edt['sims_cur_code']=$scope.obj2[0].sims_attendance_cur_code
            //    $scope.Getinfo($scope.obj2[0].sims_attendance_cur_code);
            //});


            function getCur(flag, comp_code) {
                if (flag) {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                        $scope.obj2 = res.data;
                        $scope.edt["sims_cur_code"] = $scope.obj2[0].sims_cur_code;
                        $scope.Getinfo($scope.obj2[0].sims_cur_code);

                    });
                }
                else {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                        $scope.obj2 = res.data;
                        $scope.edt["sims_cur_code"] = $scope.obj2[0].sims_cur_code;
                        $scope.Getinfo($scope.obj2[0].sims_cur_code);
                    });


                }

            }

            $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.global_count_comp = res.data;

                if ($scope.global_count_comp) {
                    getCur(true, $scope.user_details.comp);


                }
                else {
                    getCur(false, $scope.user_details.comp)
                }
            });




            $scope.Getinfo = function (curcode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curcode).then(function (res) {
                    $scope.acyr = res.data;
                    //$scope.edt['sims_academic_year'] = $scope.acyr[0].sims_academic_year
                });
            }

            $http.get(ENV.apiUrl + "api/term/getTermByIndex").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.term_data = res.data;
                if ($scope.term_data.length > 0)
                {
                    $scope.table = true;
                    $scope.pager = true;
                    if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.term_data.length, data: 'All' })
                    }
                    else
                    {
                        $scope.countData.push({ val: $scope.term_data.length, data: 'All' })
                    }
                    $scope.totalItems = $scope.term_data.length;
                    $scope.todos = $scope.term_data;
                    $scope.makeTodos();
                    for (var i = 0; i < $scope.totalItems; i++) {
                        $scope.term_data[i].icon = "fa fa-plus-circle";
                    }
                }
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };


            $scope.edit = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edt =
                    {
                        sims_cur_code: str.sims_cur_code,
                        sims_academic_year: str.sims_academic_year,
                        sims_term_code: str.sims_term_code,
                        sims_term_desc_en: str.sims_term_desc_en,
                        sims_term_desc_ar: str.sims_term_desc_ar,
                        sims_term_desc_fr: str.sims_term_desc_fr,
                        sims_term_desc_ot: str.sims_term_desc_ot,
                        sims_term_status: str.sims_term_status,
                        sims_term_start_date: str.sims_term_start_date,
                        sims_term_end_date:str.sims_term_end_date,
                        //sims_term_start_date: convertdate(str.sims_term_start_date),
                        //sims_term_end_date: convertdate(str.sims_term_end_date),
                    }
                //$scope.edit_code = true;
                $scope.edit_code = false;
                $scope.Getinfo($scope.edt.sims_cur_code);
            }
        }

            function convertdate(dt) {
                if (dt != null) {
                    var d1 = new Date(dt);
                    var month = d1.getMonth() + 1;
                    var day = d1.getDate();
                    if (month < 10)
                        month = "0" + month;
                    if (day < 10)
                        day = "0" + day;
                    var d = d1.getFullYear() + "-" + (month) + "-" + (day);
                  
                    $scope.convertdated = d;
                    return d;
                }
            }

            $scope.createdate = function (date) {
                $scope.edt.sims_term_start_date = convertdate(date);
            }

            $scope.createdate1 = function (date) {
                $scope.edt.sims_term_end_date = convertdate(date);
            }

            $scope.Save = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = ({
                            sims_cur_code: $scope.edt.sims_cur_code,
                            sims_academic_year: $scope.edt.sims_academic_year,
                            sims_term_code: $scope.edt.sims_term_code,
                            sims_term_desc_en: $scope.edt.sims_term_desc_en,
                            sims_term_desc_ar: $scope.edt.sims_term_desc_ar,
                            sims_term_desc_fr: $scope.edt.sims_term_desc_fr,
                            sims_term_desc_ot: $scope.edt.sims_term_desc_ot,
                            sims_term_status: $scope.edt.sims_term_status,
                            sims_term_start_date: $scope.edt.sims_term_start_date,
                            sims_term_end_date: $scope.edt.sims_term_end_date,
                            opr: 'I'
                        });

                        data1.push(data);

                        $http.post(ENV.apiUrl + "api/term/CUDInsertTerm", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Term Added Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ title: "Alert", text: "Term Already Exists. ", imageUrl: "assets/img/close.png",showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        })
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/term/getTermByIndex").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.term_data = res.data;
                    if ($scope.term_data.length > 0)
                    {
                        $scope.table = true;
                        $scope.pager = true;
                        if ($scope.countData.length > 3)
                        {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.term_data.length, data: 'All' })
                        }
                        else
                        {
                            $scope.countData.push({ val: $scope.term_data.length, data: 'All' })
                        }
                        $scope.totalItems = $scope.term_data.length;
                        $scope.todos = $scope.term_data;
                        $scope.makeTodos();
                        for (var i = 0; i < $scope.totalItems; i++) {
                            $scope.term_data[i].icon = "fa fa-plus-circle";
                        }
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    var data = ({
                        sims_cur_code: $scope.edt.sims_cur_code,
                        sims_academic_year: $scope.edt.sims_academic_year,
                        sims_term_code: $scope.edt.sims_term_code,
                        sims_term_desc_en: $scope.edt.sims_term_desc_en,
                        sims_term_desc_ar: $scope.edt.sims_term_desc_ar,
                        sims_term_desc_fr: $scope.edt.sims_term_desc_fr,
                        sims_term_desc_ot: $scope.edt.sims_term_desc_ot,
                        sims_term_status: $scope.edt.sims_term_status,
                        sims_term_start_date: $scope.edt.sims_term_start_date,
                        sims_term_end_date: $scope.edt.sims_term_end_date,
                        opr: 'U'
                    });

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/term/CUDTerm", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Term Updated Successfully",imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ title: "Alert", text: "Term Not Updated Successfully. " ,imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    })
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
               
                $scope.edit_code = false;
              
              
                $scope.edt['sims_term_code'] = '';
                $scope.edt['sims_term_desc_en'] = '';
                $scope.edt['sims_term_desc_ar'] = '';
                $scope.edt['sims_term_desc_fr'] = '';
                $scope.edt['sims_term_desc_ot'] = '';
                $scope.edt['sims_term_status'] = '';
                $scope.edt['sims_term_start_date'] = '';
                $scope.edt['sims_term_end_date'] = '';
                $scope.edt['sims_term_status'] = true;
                }
            }

            $scope.Delete = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    //var v = document.getElementById(i);
                    var v = document.getElementById($scope.filteredTodos[i].sims_academic_year + i);

                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'sims_cur_code': $scope.filteredTodos[i].sims_cur_code,
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            'sims_term_code': $scope.filteredTodos[i].sims_term_code,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/term/CUDTerm", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Term  Deleted Successfully",imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });

                                }
                                else if ($scope.msg1 == false) {
                                    swal({ title: "Alert", text: "Term Not Deleted Successfully. " ,imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                //var v = document.getElementById(i);
                                var v = document.getElementById($scope.filteredTodos[i].sims_academic_year + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }

                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png",showCloseButton: true, width: 380, });
                }
            }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_academic_year + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_academic_year + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            var dom;
            var count = 0;
            $scope.flag = true;
            $scope.icon = "fa fa-plus-circle";

            $scope.expand = function (info, $event) {
               
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +

                         "<tr> <td class='semi-bold'>" + gettextCatalog.getString('Arabic') + "</td> <td class='semi-bold'>" + gettextCatalog.getString('French') + " </td><td class='semi-bold'>" + gettextCatalog.getString('Regional') + "</td><td class='semi-bold'>" + gettextCatalog.getString('Start Date') + " </td>" +
                        "<td class='semi-bold'>" + gettextCatalog.getString('End Date') +

                          "<tr><td>" + info.sims_term_desc_ar + "</td> <td>" + info.sims_term_desc_fr + " </td><td>" + info.sims_term_desc_ot + "</td><td>" + info.sims_term_start_date + " </td>" +
                        "<td>" + info.sims_term_end_date +

                        " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);

                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    info.icon = "fa fa-plus-circle";
                    $scope.flag = true;
                }

            };

            $scope.size = function (str) {
                if (str == 5 || str == 10 || str == 15) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }
             
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;  $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.term_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.term_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        //var v = document.getElementById(i);
                        var v = document.getElementById($scope.filteredTodos[i].sims_academic_year + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_term_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_academic_year.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_term_desc_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
            //sort
            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };
        }]
        )
})();