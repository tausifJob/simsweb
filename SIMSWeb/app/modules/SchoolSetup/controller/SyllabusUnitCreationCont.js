﻿(function () {
    'use strict';
    var main;
    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SyllabusUnitCreationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var user = $rootScope.globals.currentUser.username;

            $scope.busyindicator = true;
           

            $scope.Grid = true;
            $scope.pagesize = '5';
            $scope.edt = {};
            $scope.edt["sims_syllabus_status"] = true;

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;
                $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.getacyr($scope.curriculum[0].sims_cur_code);
            });

            $scope.getacyr = function (str) {
                $scope.cur_code = str;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    $scope.edt['sims_academic_year'] = $scope.Academic_year[0].sims_academic_year;
                    $scope.getsections(str, $scope.Academic_year[0].sims_academic_year);
                })
            }

            $scope.getsection = function (str, str1, str2) {
                $http.get(ENV.apiUrl + "api/StudentSectionSubject/getsection_name?curcode=" + str + "&academicyear=" + str1 + "&gradecode=" + str2 + "&login_user=" + user).then(function (Allsection) {
                    $scope.section1 = Allsection.data;

                })
            };

            $scope.getsections = function (str, str1) {
                $http.get(ENV.apiUrl + "api/StudentSectionSubject/getgrade_name?curcode=" + str + "&academicyear=" + str1 + "&login_user=" + user).then(function (res) {
                    $scope.grade = res.data;

                });
            };

            $scope.get = function (section) {
                $http.get(ENV.apiUrl + "api/StudentSectionSubject/getSectionsubject_name?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_code + "&login_user=" + user).then(function (getSectionsubject_name) {
                    $scope.cmb_subject_name = getSectionsubject_name.data;
                });
            }

            $scope.getsyllabuscodes = function () {
                $http.post(ENV.apiUrl + "api/syllabus_lesson/syllabus_code", $scope.edt).then(function (Get_syllabus_code) {
                    $scope.syllabus_object = Get_syllabus_code.data;
                });
            }

            $http.get(ENV.apiUrl + "api/syllabus_lesson/Get_syllabus_lesson").then(function (res) {
                if (res.data !== null) {

                    $scope.obj = res.data;
                    console.log($scope.obj);
                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.makeTodos();
                }
                else {
                    swal({ text: 'Sorry Data Not Found', imageUrl: "assets/img/close.png" });
                }

            });

            $scope.SyllabusCreationShow = function (color) {

                $scope.headcolor = color;
                $scope.syllabusscreen = true;
                $scope.Unitscreen = false;
                $scope.Topicscreen = false;
                $scope.SubTopicscreen = false;

            }

            $scope.UnitCreationShow = function (color) {

                $scope.headcolor = color;
                $scope.syllabusscreen = false;
                $scope.Unitscreen = true;
                $scope.Topicscreen = false;
                $scope.SubTopicscreen = false;

            }

            $scope.TopicCreationShow = function (color) {

                $scope.headcolor = color;
                $scope.syllabusscreen = false;
                $scope.Unitscreen = false;
                $scope.Topicscreen = true;
                $scope.SubTopicscreen = false;

            }

            $scope.SubTopicCreationShow = function (color) {

                $scope.headcolor = color;
                $scope.syllabusscreen = false;
                $scope.Unitscreen = false;
                $scope.Topicscreen = false;
                $scope.SubTopicscreen = true;

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = angular.copy($scope.todos.slice(begin, end));

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
            };

            $scope.Show_Data = function () {

                $scope.busyindicator = false;

                if ($scope.edt.sims_subject_code == undefined) {
                    $scope.edt.sims_subject_code = '';
                }
                $http.get(ENV.apiUrl + "api/StudentSectionSubject/getStudentName?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_subject_code + "&subcode=" + $scope.edt.sims_subject_code).then(function (allSectionStudent) {
                    $scope.SectionStudent = allSectionStudent.data;
                    $scope.busyindicator = true;



                });

            }

            $scope.New = function () {

                main = document.getElementById('mainchk');
                main.checked = false;

                $scope.edt = {};
                $scope.savebtn = true;
                $scope.Grid = false;
                $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.getacyr($scope.curriculum[0].sims_cur_code);

                $scope.Check_Multiple_Record();
            }

            $scope.Edit = function (info) {
                $scope.edt = angular.copy(info);
                $scope.savebtn = false;
                $scope.Grid = false;
                main = document.getElementById('mainchk');
                main.checked = false;
                $scope.getsection(info.sims_cur_code, info.sims_academic_year, info.sims_grade_code);
                $scope.get(info.sims_section_code);

                $scope.Check_Multiple_Record();
            }

            $scope.Check_Single_Record = function (enroll, subject) {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }

            }

            $scope.Check_Multiple_Record = function () {
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if (main.checked == true) {
                        $scope.filteredTodos[i].sims_status = true;
                        $('tr').addClass("row_selected");
                    }
                    else {
                        $scope.filteredTodos[i].sims_status = false;
                        $('tr').removeClass("row_selected");

                    }
                }
            }

            $scope.btn_cancel_Click = function () {

                $scope.Grid = true;
                $scope.SyllabusDetails.$setUntouched();
                $scope.SyllabusDetails.$setPristine();

            }

            $scope.Submit = function (isvalid) {

                if (isvalid) {
                    var data = {};

                    data.sims_cur_code = $scope.edt.sims_cur_code;
                    data.sims_academic_year = $scope.edt.sims_academic_year;
                    data.sims_grade_code = $scope.edt.sims_grade_code;
                    data.sims_section_code = $scope.edt.sims_section_code;
                    data.sims_subject_code = $scope.edt.sims_subject_code;
                    data.sims_syllabus_code = $scope.edt.sims_syllabus_code;
                    data.sims_syllabus_description = $scope.edt.sims_syllabus_description;
                    data.sims_syllabus_start_date = $scope.edt.sims_syllabus_start_date;
                    data.sims_syllabus_end_date = $scope.edt.sims_syllabus_end_date;
                    data.sims_syllabus_remark = $scope.edt.sims_syllabus_remark;
                    data.sims_syllabus_creation_date = $scope.edt.sims_syllabus_creation_date;
                    data.sims_syllabus_created_by = $scope.edt.sims_syllabus_created_by;
                    data.sims_syllabus_status = $scope.edt.sims_syllabus_status;
                    data.opr = 'I';


                    $http.post(ENV.apiUrl + "api/syllabus_lesson/CUDsyllabuslesson", data).then(function (res) {
                        if (res.data == true) {

                            swal({ text: 'Syllabus Created Successfully',imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });

                            $http.get(ENV.apiUrl + "api/syllabus_lesson/Get_syllabus_lesson").then(function (res) {
                                if (res.data !== null) {
                                    $scope.Grid = true;
                                    $scope.obj = res.data;
                                    $scope.totalItems = $scope.obj.length;
                                    $scope.todos = $scope.obj;
                                    $scope.makeTodos();
                                }
                                else {
                                    swal({ text: 'Sorry Data Not Found',imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                                }

                            });

                        }
                        else {
                            swal({ text: 'Syllabus Not Created. ',imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                        }

                    });
                }
            }

            $scope.Update = function () {


                $scope.edt['opr'] = 'U';
                $http.post(ENV.apiUrl + "api/syllabus_lesson/CUDsyllabuslesson", $scope.edt).then(function (res) {
                    if (res.data == true) {

                        swal({ text: 'Syllabus Updated Successfully',imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });

                        $http.get(ENV.apiUrl + "api/syllabus_lesson/Get_syllabus_lesson").then(function (res) {
                            if (res.data !== null) {
                                $scope.Grid = true;
                                $scope.obj = res.data;
                                $scope.totalItems = $scope.obj.length;
                                $scope.todos = $scope.obj;
                                $scope.makeTodos();
                            }
                            else {
                                swal({ text: 'Sorry Data Not Found',imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                            }

                        });

                    }
                    else if (res.data == false) {
                            swal({ text: 'Syllabus Not Updated. ' ,imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    }
                    else {
                        swal("Error-" + res.data)
                    }
                });
            }

            $scope.DeleteRecord = function () {

                $scope.deleteobject = [];

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].sims_status == true) {
                        $scope.deleteobject.push($scope.filteredTodos[i]);
                    }
                }

                if ($scope.deleteobject.length > 0) {


                    swal({
                        text: "Are you sure? you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/syllabus_lesson/CUDeletesyllabuslesson", $scope.deleteobject).then(function (res) {
                                if (res.data == true) {
                                    swal({ text: 'Record Deleted Successfully', imageUrl: "assets/img/check.png",width: 320, showCloseButton: true });

                                    $http.get(ENV.apiUrl + "api/syllabus_lesson/Get_syllabus_lesson").then(function (res) {
                                        if (res.data !== null) {
                                            $scope.Grid = true;
                                            $scope.obj = res.data;
                                            $scope.totalItems = $scope.obj.length;
                                            $scope.todos = $scope.obj;
                                            $scope.makeTodos();
                                        }
                                        else  {
                                            swal({ text: 'Sorry Data Not Found. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                                        }

                                    });

                                }
                                else if (res.data == false) {
                                    swal({ text: 'Record Not Deleted. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                                }
                                else {
                                    swal("Error-" + res.data)
                                }
                            });
                        } else {

                            main.checked = false;
                            $scope.Check_Multiple_Record();
                        }
                    });
                }
                else {
                    swal({ text: 'Select Atleast One Record To Delete',imageUrl: "assets/img/notification-alert.png", width: 320, showCloseButton: true });
                }
            }

            $scope.size = function (str) {
                main = document.getElementById('mainchk');
                main.checked = false;
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();

            }

            $scope.index = function (str) {
                main = document.getElementById('mainchk');
                main.checked = false;
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();

            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            var dom;
            $scope.flag = true;

            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                "<table class='inner-table1' cellpadding='5' cellspacing='0'  style='width:100%;border:solid;border-width:02px'>" +
                "<tbody>" +
                "<tr> <td class='semi-bold'>" + "Created By" + "</td> <td class='semi-bold'>" + "Remark" + " </td><td class='semi-bold'>" + "Syllabus Start Date" + "</td><td class='semi-bold'>" + "Syllabus End Date" + "</td></tr>" +
                "<tr> <td>" + (info.sims_syllabus_created_by) + "</td> <td>" + (info.sims_syllabus_remark) + " </td><td>" + (info.sims_syllabus_start_date) + "</td><td>" + (info.sims_syllabus_end_date) + "</td></tr>" +

                " </table></td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    //$(dom).remove();
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }
            }

        }]);

})();



