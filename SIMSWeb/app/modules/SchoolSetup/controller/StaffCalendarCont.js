﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StaffCalenderCont',
      ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
          //  swal("Student Fee Details Updated Successfully!", "success")
          //   swal({ title: "Alert", text: "Fee frequency does not match with the periods", imageUrl: "assets/img/notification-alert.png", });
          //swal({ title: "Are you sure?", text: "You will not be able to recover this imaginary file!", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!", cancelButtonText: "No, cancel plx!", closeOnConfirm: false, closeOnCancel: false }, function (isConfirm) { if (isConfirm) { swal("Deleted!", "Your imaginary file has been deleted.", "success"); } else { swal("Cancelled", "Your imaginary file is safe :)", "error"); } });

          $scope.eventsList = [];
          $scope.display = true;
          $scope.btn_delete = false;
          $scope.check_time_flag = false;
          $scope.event_object = {};
          function editEvent(event) {
              console.log(event);
              $scope.check_time_flag = true;
              var yy = event.startDate.getFullYear(); var d = event.startDate.getDate(); var m = event.startDate.getMonth() + 1;;
              $scope.event_object['sims_calendar_date'] = yy + '-' + m + '-' + d;
              console.log($scope.event_object);
              var v = document.getElementsByName("event-start-date");
              v.value = yy + '-' + m + '-' + d;

              //$('#event-modal input[name="event-index"]').val(event ? event.id : '');
              //$('#event-modal input[name="event-name"]').val(event ? event.name : '');
              //$('#event-modal input[name="event-location"]').val(event ? event.location : '');
              //$('#event-modal input[name="event-start-date"]').val(yy+'-'+m+'-'+d);
              // $('#event-modal input[name="event-end-date"]').datepicker('update', event ? event.endDate : '');
              // $('#event-modal').modal(); 

              $('#fullCalModal').modal({ backdrop: "static" });
              $("#Checkbox2").trigger("click");
          }

          function deleteEvent(event) {
              var dataSource = $('#calendar').data('calendar').getDataSource();

              for (var i in dataSource) {
                  if (dataSource[i].id == event.id) {
                      dataSource.splice(i, 1);
                      break;
                  }
              }

              $('#calendar').data('calendar').setDataSource(dataSource);
          }

          function saveEvent() {
              var event = {
                  id: $('#event-modal input[name="event-index"]').val(),
                  name: $('#event-modal input[name="event-name"]').val(),
                  location: $('#event-modal input[name="event-location"]').val(),
                  startDate: $('#event-modal input[name="event-start-date"]').datepicker('getDate'),
                  endDate: $('#event-modal input[name="event-end-date"]').datepicker('getDate')
              }

              var dataSource = $('#calendar').data('calendar').getDataSource();

              if (event.id) {
                  for (var i in dataSource) {
                      if (dataSource[i].id == event.id) {
                          dataSource[i].name = event.name;
                          dataSource[i].location = event.location;
                          dataSource[i].startDate = event.startDate;
                          dataSource[i].endDate = event.endDate;
                      }
                  }
              }
              else {
                  var newId = 0;
                  for (var i in dataSource) {
                      if (dataSource[i].id > newId) {
                          newId = dataSource[i].id;
                      }
                  }

                  newId++;
                  event.id = newId;

                  dataSource.push(event);
              }

              $('#calendar').data('calendar').setDataSource(dataSource);
              $('#event-modal').modal('hide');
          }


          $http.get(ENV.apiUrl + "api/calender/getCalender").then(function (res) {

              $scope.display = true;
              $scope.obj = res.data;
              console.log($scope.obj);
              $scope.eventsList = [];
              //  debugger
              for (var i = 0; i < res.data.length; i++) {
                  var event = {};
                  event['id'] = res.data[i].id,
                  event['name'] = res.data[i].sims_calendar_event_short_desc,
                  // event['location'] = res.data[i].sims_calendar_event_desc,
                  event['startDate'] = new Date(res.data[i].start),
                  event['endDate'] = new Date(res.data[i].end),

                  $scope.eventsList.push(event);
              }

              console.log($scope.eventsList);
              $(function () {
                  var currentYear = new Date().getFullYear();

                  $('#calendar').calendar({
                      enableContextMenu: true,
                      enableRangeSelection: true,
                      contextMenuItems: [
                          {
                              text: 'Update',
                              click: editEvent
                          },
                          {
                              text: 'Delete',
                              click: deleteEvent
                          }
                      ],
                      selectRange: function (e) {
                          editEvent({ startDate: e.startDate, endDate: e.endDate });
                      },
                      mouseOnDay: function (e) {
                          if (e.events.length > 0) {
                              var content = '';

                              for (var i in e.events) {
                                  content += '<div class="event-tooltip-content">'
                                                  + '<div class="event-name" style="color:' + e.events[i].color + '">' + e.events[i].name + '</div>'
                                              + '</div>';
                              }

                              $(e.element).popover({
                                  trigger: 'manual',
                                  container: 'body',
                                  html: true,
                                  content: content
                              });

                              $(e.element).popover('show');
                          }
                      },
                      mouseOutDay: function (e) {
                          if (e.events.length > 0) {
                              $(e.element).popover('hide');
                          }
                      },
                      dayContextMenu: function (e) {
                          $(e.element).popover('hide');
                      },
                      dataSource: $scope.eventsList
                  });

                  $('#save-event').click(function () {
                      saveEvent();
                  });
              });

          });


          $http.get(ENV.apiUrl + "api/calender/getCategorize").then(function (res) {
              $scope.Categorize_obj = res.data;
          });

          $http.get(ENV.apiUrl + "api/calender/getTimeMarkers").then(function (res) {
              $scope.Timemarkers_obj = res.data;
          });
          $scope.update_flag = false;


          $scope.CancelClear1 = function () {
              $scope.user = angular.copy($scope.event_object)
              $scope.Myform.$setPristine();
              $scope.Myform.$setValidity();
              $scope.Myform.$setUntouched();
              console.log($scope.event_object);
          }

          $scope.TimeMarkersdemo = function (tm) {

              for (var i = 0; i < $scope.Categorize_obj.length; i++) {
                  if (tm == $scope.Categorize_obj[i].sims_appl_parameter) {
                      var c = $scope.Categorize_obj[i].color_code;
                      $('#label1').html($scope.Categorize_obj[i].sims_appl_form_field_value1);
                      var v = document.getElementById('label1');
                      v.style.backgroundColor = c;
                      v.style.color = 'black';
                      break;
                  }
              }
          }

          $scope.checktime = function (min, max) {
              console.log(min);
              console.log(max);
              $scope.check_time_flag = true;
              if (min >= max) {
                  swal({ text: "Please Select Correct Time", imageUrl: "assets/img/notification-alert.png", });
                  $scope.check_time_flag = false;
              }
          }


          $scope.AddEventByDate = function (isvalid) {

              // console.log($scope.event_object);
              if (isvalid) {
                  if ($scope.check_time_flag == true) {
                      $scope.check_time_flag = false;
                      console.log($scope.event_object);
                      $('#fullCalModal').modal('hide');
                      $http.post(ENV.apiUrl + "api/calender/AddEventByDate?data=" + JSON.stringify($scope.event_object)).then(function (res) {
                          $scope.result = res.data;
                          console.log($scope.result);

                          if ($scope.result == true) {
                              swal("Event Added Successfully!", "success")
                              $scope.eventsList = [];
                              $http.get(ENV.apiUrl + "api/calender/getCalender").then(function (res) {

                                  $scope.display = true;
                                  $scope.obj = res.data;
                                  console.log($scope.obj);
                                  $scope.eventsList = [];
                                  //  debugger
                                  for (var i = 0; i < res.data.length; i++) {
                                      var event = {};
                                      event['id'] = res.data[i].id,
                                      event['name'] = res.data[i].sims_calendar_event_short_desc,
                                      // event['location'] = res.data[i].sims_calendar_event_desc,
                                      event['startDate'] = new Date(res.data[i].start),
                                      event['endDate'] = new Date(res.data[i].end),

                                      $scope.eventsList.push(event);
                                  }

                                  console.log($scope.eventsList);
                                  $(function () {
                                      var currentYear = new Date().getFullYear();

                                      $('#calendar').calendar({
                                          enableContextMenu: true,
                                          enableRangeSelection: true,
                                          contextMenuItems: [
                                              {
                                                  text: 'Update',
                                                  click: editEvent
                                              },
                                              {
                                                  text: 'Delete',
                                                  click: deleteEvent
                                              }
                                          ],
                                          selectRange: function (e) {
                                              editEvent({ startDate: e.startDate, endDate: e.endDate });
                                          },
                                          mouseOnDay: function (e) {
                                              if (e.events.length > 0) {
                                                  var content = '';

                                                  for (var i in e.events) {
                                                      content += '<div class="event-tooltip-content">'
                                                                      + '<div class="event-name" style="color:' + e.events[i].color + '">' + e.events[i].name + '</div>'
                                                                  + '</div>';
                                                  }

                                                  $(e.element).popover({
                                                      trigger: 'manual',
                                                      container: 'body',
                                                      html: true,
                                                      content: content
                                                  });

                                                  $(e.element).popover('show');
                                              }
                                          },
                                          mouseOutDay: function (e) {
                                              if (e.events.length > 0) {
                                                  $(e.element).popover('hide');
                                              }
                                          },
                                          dayContextMenu: function (e) {
                                              $(e.element).popover('hide');
                                          },
                                          dataSource: $scope.eventsList
                                      });

                                      $('#save-event').click(function () {
                                          saveEvent();
                                      });
                                  });

                              });

                          }
                          else {
                              swal({ text: "Event Can not Add. " , imageUrl: "assets/img/notification-alert.png", });
                          }
                      }, function () {
                          swal({ text: "Internal Server Error" + $scope.result, imageUrl: "assets/img/notification-alert.png", });
                      });

                      $scope.event_object = {};
                  }
              }
          }



          $('.clockpicker').clockpicker({
              autoclose: true
          });

          $('*[data-datepicker="true"] input[type="text"]').datepicker({
              todayBtn: true,
              orientation: "top left",
              autoclose: true,
              todayHighlight: true
          });

          $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
              $('input[type="text"]', $(this).parent()).focus();
          });
      }])

})();