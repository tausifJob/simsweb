﻿(function () {
    'use strict';

    var main, temp, del = [];

    var simsController = angular.module('sims.module.SchoolSetup');
    simsController.controller('mograVersionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.dis1 = true;
            $scope.edtdetails = { comn_mod_name_e: '', comn_mod_code: '' };

            $scope.edt = { 'comn_mod_name_e': '', 'mogra_details': [] };
            
            $http.get(ENV.apiUrl + "api/Fee/SFS/GetProjectNames").then(function (prjname) {
                $scope.projectNames = prjname.data;
            });

            $scope.caption = "ADD";
            
            $scope.New = function () {
                $scope.edt = "";
                $scope.edit_code = false;
                $scope.dis1 = false;
                $scope.dis2 = true;
                $scope.opr = 'I';
                $scope.arr_versionDetails = [];
                $scope.updateItem = false;
                $scope.setCaption($scope.updateItem);
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetProjectModulesApplications").then(function (mdname) {
                    $scope.ModuleName = mdname.data;
                });
                $http.get(ENV.apiUrl + "api/Fee/SFS/Getdevelopers").then(function (developers) {
                    $scope.developers = developers.data;
                });
                
            }
            setTimeout(function () {
                $("#cmb_devl_name").select2();

            }, 100);

            $scope.depvl_name = function (str1, index) {
                try {
                    //var index = $("select[name='Applsource'] option:selected").index();
                } catch (ex) {
                }
            }

            $scope.cancel = function () {
                $scope.edt = "";
                $scope.edit_code = false;
                $scope.dis1 = true;
                $scope.dis2 = false;
                $scope.gridresult = [];

            }

            $scope.arr_versionDetails = [];

            $scope.today = (new Date());
           
            // Preview
            $scope.getPreview = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetMograVersionsDetails?project_code=" + $scope.edt1.lic_project_code).then(function (gridresult) {
                    if (gridresult.data.length > 0) {
                        $scope.gridresult = gridresult.data;
                    }
                    else {
                        $scope.gridresult = '';
                        swal({
                            text: 'No Changesets found for this Project.',
                            width: 400,
                            height: 300
                        });
                    }
                         
                });
            }

            $scope.mod_name = ''; $scope.appl_name = '';
          
            $scope.getApplications = function (obj) {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetProjectApplications?mod_code=" + obj['comn_mod_code']).then(function (appname) {
                    $scope.ApplicationName = appname.data;
                });
                $scope.edtdetails.comn_mod_name_e = obj.comn_mod_name_e;

         
            }

            $scope.SaveData = function () {
                debugger;
                $scope.edt['mogra_details'] = $scope.arr_versionDetails;
                $http.post(ENV.apiUrl + "api/Fee/SFS/CUD_SaveMograVersionsDetails?opr=" + $scope.opr, $scope.edt).then(function (res) {
                    if (res.data=='true') {
                        swal({
                            text: 'Version Information saved successfully',
                            width: 400,
                            height: 300
                        });
                    }
                    else if (res.data == 'false') {
                        swal({
                            text: 'Error while saving Version Information',
                            width: 400,
                            height: 300
                        });
                    }
                    else if (res.data == 'exists') {
                        swal({
                            text: 'Changeset No. already present in the system.',
                            width: 400,
                            height: 300
                        });
                    }
                });
                $scope.Reset();
            }

            $scope.Resetdetail = function () {
                $scope.edtdetails = { 'comn_mod_name_e': '' };
                $scope.updateItem = false;
                $scope.setCaption($scope.updateItem);
            }

            $scope.Reset = function () {
                $scope.edtdetails = [];
                $scope.arr_versionDetails = [];
                $scope.edt = "";
                $scope.edit_code = false;
                $scope.dis1 = true;
                $scope.dis2 = false;
                $scope.gridresult = [];
                $scope.edtdetails = { comn_mod_name_e: '', comn_mod_code: '' };

            }

            $scope.index = -1;
            $scope.updateItem = false;

            $scope.editItem = function (index, obj) {
                $scope.updateItem = true;
                $scope.setCaption($scope.updateItem);
                $scope.index = index;
                for (var i = 0; i < $scope.ModuleName.length; i++) {
                    if ($scope.ModuleName[i].comn_mod_code == obj.comn_mod_code) {
                        $scope.dep = $scope.ModuleName[i];
                        break;
                    }
                }
                $scope.edtdetails = obj;
            }

            $scope.AddDetails = function () {
             
                if ($scope.updateItem == false) {
                    $scope.comn_mod_name = $("#modName option:selected").text();
                    $scope.comn_appl_name = $("#appname option:selected").text();
                    $scope.edtdetails['lic_changeset_no'] = $scope.edt.lic_changeset_no;
                    $scope.edtdetails['comn_mod_code'] = $scope.edtdetails.comn_mod_code;
                    $scope.edtdetails['lic_appl_code'] = $scope.edtdetails.lic_appl_code;
                    $scope.edtdetails['comn_mod_name_e'] = $scope.comn_mod_name;
                    $scope.edtdetails['comn_appl_name'] = $scope.comn_appl_name;
                    $scope.edtdetails['lic_document'] = 'I';
                    $scope.edtdetails['lic_test_case'] = 'I';
                    $scope.edtdetails['lic_deployed'] = 'N';
                    $scope.edtdetails['is_new_flag'] = 'Y';
                    $scope.arr_versionDetails.push($scope.edtdetails);
                    $scope.edtdetails = { comn_mod_name_e: '',comn_mod_code:'' };

                }
                else {
                    if ($scope.index != -1) {
                        $scope.arr_versionDetails[$scope.index] = $scope.edtdetails;
                        $scope.index = -1;
                    }   
                }
                //$scope.edtdetails = '';
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.preview = function ()
            {
                $http.get(ENV.apiUrl + "api/Fee/SFS/SearchMograVersionsDetailsNew?ProjectName="+$scope.edt.comn_appl_project_name+"&startDate="+$scope.edt.from_date+"&endDate="+$scope.edt.to_date).then(function (res) {
                    $scope.versiondetailsall = res.data;
                });
            }

            $scope.removeItem = function (item) {
                $scope.arr_versionDetails.pop(item);
                if (item.is_new_flag == 'N') {
                    $http.post(ENV.apiUrl + "api/Fee/SFS/CUD_MograVersionsDetails", item).then(function (res) {
                        if (res.data == 'true') {
                            swal({
                                text: 'Changeset Information Deleted successfully',
                                width: 400,
                                height: 300
                            });
                        }
                        else if (res.data == 'false') {
                            swal({
                                text: 'Error while Deleting Changeset Information',
                                width: 400,
                                height: 300
                            });
                        }
                        else if (res.data == 'exists') {
                            swal({
                                text: 'Changeset No. not present in the system.',
                                width: 400,
                                height: 300
                            });
                        }
                    });
                }
            }

            $scope.getInfo = function (obj) {
                $scope.New();
                $scope.edt = obj;
                $scope.arr_versionDetails = obj.mogra_details;
                $scope.opr = 'U';
            }
           
            $scope.setCaption = function (val) {
                $scope.caption = val == true ? "UPDATE" : "ADD";
            }
        }]);
})();

