﻿
(function () {
    'use strict';
    var subject_list = [];
    var section_list = [];
    var main;
    var grade_code, SectionSubject5 = [], subject_code = [], subject_code1 = [];

    var cur_code;
    var section_code, sectioncodemodal1;
    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SectionSubjectTermMappingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.grid = true
            $scope.show = false;
            $scope.table = false;
            $scope.edt = {};
            $scope.filteredsub_array = [];

            $scope.getacyr = function (str) {

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.getAcademicYear = Academicyear.data;
                    $scope.getsections(str, $scope.getAcademicYear[0].sims_academic_year)
                });
            }

            var pre_subject_code;
            var pre_term_code;
            var pre_sub_grp_code;
            
           




            $scope.getsections = function (str, str1) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str + "&academic_year=" + str1).then(function (res) {
                    $scope.getAllGrades = res.data;
                    $scope.edt = {
                        sims_cur_code: str,
                        sims_academic_year: str1,
                        sims_grade_code: $scope.getAllGrades[0].sims_grade_code,
                    };
                    $scope.getsection(str, str1, $scope.getAllGrades[0].sims_grade_code);


                })
            };

            $scope.getsection = function (str, str1, str2) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + str + "&grade_code=" + str2 + "&academic_year=" + str1).then(function (Allsection) {
                    $scope.getSectionFromGrade = Allsection.data;
                })
            };

            //$http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
            //    $scope.curriculum = res.data;
            //    $scope.getacyr($scope.curriculum[0].sims_cur_code);
            //});

            function getCur(flag, comp_code) {
                if (flag) {


                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                        $scope.curriculum = res.data;
                        $scope.edt["sims_cur_code"] = $scope.curriculum[0].sims_cur_code;
                        $scope.getacyr($scope.curriculum[0].sims_cur_code);

                    });
                }
                else {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                        $scope.curriculum = res.data;
                        $scope.edt["sims_cur_code"] = $scope.curriculum[0].sims_cur_code;
                        $scope.getacyr($scope.curriculum[0].sims_cur_code);
                    });


                }

            }

            $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.global_count_comp = res.data;

                if ($scope.global_count_comp) {
                    getCur(true, $scope.user_details.comp);


                }
                else {
                    getCur(false, $scope.user_details.comp)
                }
            });




            //$http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
            //    $scope.ComboBoxValues = AllComboBoxValues.data;

            //});

            var SectionSubject6 = [];

            $scope.getSectionGradesWise = function () {
                debugger
                $scope.table = false;
                $scope.SectionSubject = [];
                $http.get(ENV.apiUrl + "api/term/getSectionTermSubject?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&sectioncode=" + $scope.edt.sims_section_code).then(function (allSectionSubject) {
                    $scope.SectionSubject = allSectionSubject.data;
                    $scope.edt["sims_subject_code"] = $scope.SectionSubject[1].sims_subject_code;
                        $scope.numPerPage = $scope.SectionSubject.length,
                        $scope.maxSize = $scope.SectionSubject.length;
                        $scope.totalItems = $scope.SectionSubject.length;
                        $scope.todos = $scope.SectionSubject;
                        $scope.makeTodos();
                        $scope.grid = true;
                        $scope.table = true;
                        console.log($scope.SectionSubject);
                        for (var i = 0; i < $scope.filteredTodos.length; i++) {
                            for (var j = 0; j < $scope.filteredTodos[i].term.length; j++) {
                                
                                //(trm.sims_section_subject_term_dependency_status &&  !trm.sims_subject_status)  (!trm.sims_section_subject_term_dependency_status &&  !trm.sims_subject_status) 
                                if ($scope.filteredTodos[i].term[j].sims_section_subject_term_dependency_status &&  !$scope.filteredTodos[i].term[j].sims_subject_status) {
                                    $scope.filteredTodos[i].term[j]['chkdisable'] = true;
                                    
                                }
                                if (!$scope.filteredTodos[i].term[j].sims_section_subject_term_dependency_status && !$scope.filteredTodos[i].term[j].sims_subject_status) {
                                    $scope.filteredTodos[i].term[j]['chkdisable'] = false;

                                }
                            }
                        }

                });

            }

          
            $scope.on_term_change = function (term) {
                $http.get(ENV.apiUrl + "api/term/getdependentMapping_sub?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&term_old=" + pre_term_code + "&sub_code_old=" + pre_subject_code).then(function (res) {
                    $scope.get_dependent_data_sub = res.data;
                for (var j = 0; j < $scope.filteredsub_array.length; j++) {
                        $scope.filteredsub_array[j]['sims_section_subject_term_dependency_status'] = false;

                }
                debugger
                for (var i = 0; i < $scope.get_dependent_data_sub.length; i++) {

                    for (var j = 0; j < $scope.filteredsub_array.length; j++) {
                        if ($scope.get_dependent_data_sub[i].sims_subject_dependent_subject_code == $scope.filteredsub_array[j].sims_subject_code && term == $scope.get_dependent_data_sub[i].sims_section_subject_term_dependency_code) {
                            //if ($scope.get_dependent_data_sub[i].sims_section_subject_term_dependency_status) {
                                $scope.filteredsub_array[j]['sims_section_subject_term_dependency_status'] = $scope.get_dependent_data_sub[i].sims_section_subject_term_dependency_status;
                            //}
                        }
                    }
                }

                });



            }
            
            $scope.depend = function (str,subfullarray,termfullarray) {
                //$('#t-' + str).show();
                //   $('#t-' + str).toggle();

                $scope.getAllterms = '';

                $scope.filteredsub_array = [];
                debugger
                setTimeout(function () {
                    $('#DependentModal').modal({ backdrop: 'static', keyboard: false });
                }, 500)
                $scope.subject_name = subfullarray.sims_subject_name_en;
                $scope.term_name = termfullarray.sims_term_desc_en;

                pre_subject_code = subfullarray.sims_subject_code;
                pre_term_code = termfullarray.sims_term_code;
                pre_sub_grp_code = subfullarray.sims_subject_group_code;
              

               

                for (var i = 0; i < $scope.SectionSubject.length; i++) {
                    if ($scope.SectionSubject[i].sims_subject_code != pre_subject_code) {
                        if ($scope.SectionSubject[i].sims_section_subject_term_dependency_status == true) {
                            $scope.filteredsub_array.push($scope.SectionSubject[i])
                        }
                        else {
                            $scope.filteredsub_array.push($scope.SectionSubject[i])
                        }
                    }
                    
                }

                $http.get(ENV.apiUrl + "api/term/getTermMapping?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (res) {
                    $scope.getAllterms = res.data;
                });

                console.log($scope.filteredsub_array);

            }

         
            $scope.checkonebyonedelete = function (index) {
                debugger
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById(index + "chk_term2");
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }





            $scope.Selectmultiple = function () {
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if (main.checked == true) {
                        $scope.filteredTodos[i].sims_subject_status = true;
                    }
                    else {
                        $scope.filteredTodos[i].sims_subject_status = false;
                    }
                }
            }
           
            $scope.SelectSingle = function (str,str2,index) {
                debugger
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }

                if (str.term[0]['sims_subject_status']) {
                    str.term[1]['chkdisable'] = true;
                }
                if (str.term[1]['sims_subject_status']) {
                    str.term[0]['chkdisable'] = true;
                }

                if (!str.term[0]['sims_subject_status'] && !str.term[1]['sims_subject_status']) {
                    str.term[1]['chkdisable'] = false;
                    str.term[0]['chkdisable'] = false;

                }

            }

          
            $scope.searched = function (valLists, toSearch) {


                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.SectionSubject, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.SectionSubject;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_subject_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);


               

            };

            var datasend = [];
            $scope.btn_submit = function ()
            {
                debugger;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    for (var j = 0; j < $scope.filteredTodos[i].term.length; j++) {

                   
                        //var v = document.getElementById($scope.filteredTodos[i].term[j].sims_subject_status);
                      
                        

                            var v = $scope.filteredTodos[i].term[j].sims_subject_status;
                           // if (v == true)
                            {

                                for (var k = 0; k < $scope.SectionSubject.length; k++) {

                                    //if ($scope.filteredTodos[i].sims_section_subject_term_code == $scope.filteredTodos[i].term[j].sims_term_code
                                    // && $scope.filteredTodos[i].sims_subject_code == $scope.SectionSubject[k].sims_subject_code) {

                                    //    swal({ text: "This subject Already Mapped to Semester ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                                    //}
                                    //else
                                    {

                                        $scope.flag = true;
                                        v = false;

                                        var data = {
                                            'sims_cur_code': $scope.filteredTodos[i].sims_cur_code,
                                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                                            'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                                            'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                                            'sims_subject_code': $scope.filteredTodos[i].sims_subject_code,
                                            'sims_subject_group_code': $scope.filteredTodos[i].sims_subject_group_code,
                                            'sims_section_subject_term_code': $scope.filteredTodos[i].term[j].sims_term_code,
                                            'sims_display_order': $scope.filteredTodos[i].term[j].sims_display_order,
                                            'sims_display_report_order': $scope.filteredTodos[i].term[j].sims_display_report_order,
                                            'sims_section_subject_credit_hours': $scope.filteredTodos[i].term[j].sims_section_subject_credit_hours,
                                            'sims_section_subject_term_status': $scope.filteredTodos[i].term[j].sims_subject_status,
                                            'opr': 'I'
                                        }


                                        datasend.push(data);
                                    }
                                }
                        }


                    
                    }
                        
                     
                    
                }
                
                $http.post(ENV.apiUrl + "api/term/CUDsection_subject_term", datasend).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        datasend = [];
                    }
                    else {
                        swal({ text: "allready present. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        datasend = [];
                    }



                });


            }

            var datasend1 = [];
            $scope.Save = function () {
                debugger;
               

                for (var i = 0; i < $scope.filteredsub_array.length; i++)
                {
                  //  if ($scope.filteredsub_array[i].sims_section_subject_term_dependency_status == true) {
                        var data1 = {
                            'sims_cur_code': $scope.edt.sims_cur_code,
                            'sims_academic_year': $scope.edt.sims_academic_year,
                            'sims_section_subject_term_dependency_code':$scope.edt.sims_term_code,

                            'sims_section_subject_term_code':pre_term_code,
                            'sims_subject_main_subject_code':pre_subject_code,
                            'sims_subject_group_main_subject_group_code':pre_sub_grp_code,

                            'sims_subject_dependent_subject_code':$scope.filteredsub_array[i].sims_subject_code,
                            'sims_subject_group_dependent_subject_group_code':$scope.filteredsub_array[i].sims_subject_group_code,
                            'sims_section_subject_term_dependency_status': $scope.filteredsub_array[i].sims_section_subject_term_dependency_status,
                            'opr': 'L'
                        }
                        datasend1.push(data1);
                   // }

                }

                $http.post(ENV.apiUrl + "api/term/CUDsection_subject_term_dependent", datasend1).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Dependent Subject Mapped Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        datasend1 = [];
                    }
                    else {
                        swal({ text: "allready present. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        datasend1 = [];
                    }



                });




            }


        }])
})();