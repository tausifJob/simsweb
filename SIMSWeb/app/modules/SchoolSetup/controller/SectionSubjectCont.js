﻿
(function () {
    'use strict';
    var subject_list = [];
    var section_list = [];
    var main;
    var grade_code, SectionSubject5 = [], subject_code = [], subject_code1 = [];

    var cur_code;
    var section_code, sectioncodemodal1;
    var simsController = angular.module('sims.module.SchoolSetup');
    

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SectionSubjectCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.grid = true
            $scope.show = false;
            $scope.table = false;
            $scope.pagesize = '30';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.checkbox = false;
            $scope.edt = {};
            $scope.temp = {};
            $scope.sims_view_mode = false;

            //$scope.size = function (str) {
            //    //console.log(str);
            //    //$scope.pagesize = str;
            //    //$scope.currentPage = 1;
            //    //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //    debugger;
            //    if (str == "All") {

            //        $scope.currentPage = '1';
            //        $scope.filteredTodos = $scope.todos;
            //        $scope.pager = false;
            //    }
            //    else {
            //        $scope.pager = true;
            //        $scope.pagesize = str;
            //        $scope.currentPage = 1;
            //        $scope.numPerPage = str;
            //        $scope.makeTodos();
            //    }
            //}
            //$scope.index = function (str) {
            //    $scope.pageindex = str;
            //    $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
            //    $scope.makeTodos();
            //    $scope.chk = {}
            //    $scope.chk['chk_all'] = false;
            //    $scope.row1 = '';
            //}

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 30, $scope.maxSize = 100;

            $scope.makeTodos = function () {
                debugger;
                if ($scope.pagesize == 'All') {
                    $scope.currentPage = 1;
                    $scope.showPager = false;
                    $scope.filteredTodos = $scope.report_data;
                }
                else {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                }
            };

            $scope.getacyr = function (str) {

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.getAcademicYear = Academicyear.data;
                    $scope.getsections(str, $scope.getAcademicYear[0].sims_academic_year)
                });
            }

            $scope.getsections = function (str, str1) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + str + "&academic_year=" + str1).then(function (res) {
                    $scope.getAllGrades = res.data;
                    $scope.edt = {
                        sims_cur_code: str,
                        sims_academic_year: str1,
                        sims_grade_code: $scope.getAllGrades[0].sims_grade_code,
                    };
                    $scope.getsection(str, str1, $scope.getAllGrades[0].sims_grade_code);


                })
            };

            $scope.getsection = function (str, str1, str2) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + str + "&grade_code=" + str2 + "&academic_year=" + str1).then(function (Allsection) {
                    $scope.getSectionFromGrade = Allsection.data;
                })
            };

            //$http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
            //    $scope.curriculum = res.data;
            //    $scope.getacyr($scope.curriculum[0].sims_cur_code);
            //});

            function getCur(flag, comp_code) {
                if (flag) {


                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                        $scope.curriculum = res.data;
                        $scope.edt["sims_cur_code"] = $scope.curriculum[0].sims_cur_code;
                        $scope.getacyr($scope.curriculum[0].sims_cur_code);

                    });
                }
                else {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                        $scope.curriculum = res.data;
                        $scope.edt["sims_cur_code"] = $scope.curriculum[0].sims_cur_code;
                        $scope.getacyr($scope.curriculum[0].sims_cur_code);
                    });


                }

            }

            $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.global_count_comp = res.data;

                if ($scope.global_count_comp) {
                    getCur(true, $scope.user_details.comp);


                }
                else {
                    getCur(false, $scope.user_details.comp)
                }
            });




            //$http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
            //    $scope.ComboBoxValues = AllComboBoxValues.data;

            //});

            var SectionSubject6 = [];

            $scope.getSectionGradesWise = function () {
                debugger
                $scope.table = false;
                $scope.busy = true;
                $scope.SectionSubject = [];
                $http.get(ENV.apiUrl + "api/common/SectionSubject/getSubjectByIndex?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&sectioncode=" + $scope.edt.sims_section_code).then(function (allSectionSubject) {
                    $scope.SectionSubject = allSectionSubject.data;
                    if ($scope.SectionSubject.length > 0) {
                        var count = 0;
                        for (var i = 0; i < $scope.SectionSubject.length; i++) {
                            if ($scope.SectionSubject[i].sims_subject_status == true) {
                                count = count + 1;
                            }
                            main = document.getElementById('mainchk');
                            if (count == $scope.SectionSubject.length) {
                                main.checked = true;
                            }
                            else {
                                main.checked = false;
                            }
                        }

                        $scope.numPerPage = $scope.SectionSubject.length,
                        $scope.maxSize = $scope.SectionSubject.length;
                        $scope.totalItems = $scope.SectionSubject.length;
                        $scope.todos = $scope.SectionSubject;
                        $scope.makeTodos();
                        $scope.grid = true;
                        $scope.busy = false;
                        $scope.table = true;
                        $scope.temp.sims_view_mode = false;

                    }

                    else {
                        swal({ text: 'Data Not Found', width: 300, showCloseButton: true });
                    }
                });
               // $scope.busy = false;
            }

            
            
            $scope.modalget = function () {
                debugger
                $scope.show = true;
                $scope.table = true;
                $scope.section2 = [];
                SectionSubject6 = [];

                $scope.SectionShowData = [];
                $scope.SectionSubject5 = [];
                $scope.SectionSubject2 = '';

                $http.get(ENV.apiUrl + "api/common/SectionSubject/getSectionNamesWithSubjects?curcode=" + $scope.temp.sims_cur_code + "&gradecode=" + $scope.temp.sims_grade_code + "&academicyear=" + $scope.temp.sims_academic_year + "&sectioncode=" + $scope.temp.sims_section_code).then(function (allSectionSubject2) {
                    $scope.SectionSubject5 = allSectionSubject2.data;
                    for (var i = 0; i < $scope.SectionSubject5.length; i++) {
                        if ($scope.SectionSubject5[i].sims_subject_status == true) {
                            var data = ({
                                'sims_subject_code': $scope.SectionSubject5[i].sims_subject_code,
                                'sims_subject_name': $scope.SectionSubject5[i].sims_subject_name
                            })
                            SectionSubject6.push(data);
                        }
                    }

                    $scope.SectionSubject2 = SectionSubject6;

                });

                $scope.btnadd = true;

                $http.get(ENV.apiUrl + "api/common/SectionSubject/getSectionNames?curcode=" + $scope.temp.sims_cur_code + "&gradecode=" + $scope.temp.sims_grade_code + "&academicyear=" + $scope.temp.sims_academic_year).then(function (Allsection) {
                    $scope.section2 = Allsection.data;
                    for (var i = 0; i < $scope.section2.length; i++) {
                        if ($scope.section2[i].sims_section_code != $scope.temp.sims_section_code) {
                            $scope.SectionShowData.push($scope.section2[i])
                        }
                    }
                });
            }

            $scope.showsubject = function (str) {

                $http.get(ENV.apiUrl + "api/common/SectionSubject/getSectionSubject?curcode=" + $scope.temp.sims_cur_code + "&gradecode=" + $scope.temp.sims_grade_code + "&academicyear=" + $scope.temp.sims_academic_year + "&sectioncode=" + str).then(function (allSectionSubject1) {
                    $scope.SectionSubject3 = allSectionSubject1.data;
                });
            }

            $scope.Copy_Subject = function () {

                $scope.temp = {
                    sims_cur_code: $scope.curriculum[0].sims_cur_code,
                    sims_academic_year: $scope.getAcademicYear[0].sims_academic_year,
                    sims_grade_code: $scope.getAllGrades[0].sims_grade_code,
                };


                $scope.grid = false;
                $scope.display = true;
                $scope.show = false;
            }

            $scope.Save = function () {

                subject_list = [];

                for (var i = 0; i < $scope.SectionSubject2.length; i++) {
                    if ($scope.SectionSubject2[i].ischecked == true)
                        subject_list = subject_list + $scope.SectionSubject2[i].sims_subject_code + ',';
                }
                section_list = [];
                for (var i = 0; i < $scope.SectionShowData.length; i++) {
                    if ($scope.SectionShowData[i].ischecked == true)
                        section_list = section_list + $scope.SectionShowData[i].sims_section_code + ',';
                }

                var selectedsectionlist = ({
                    'sims_subject_code': subject_list,
                    'sims_section_code': section_list,
                    'sims_grade_code': $scope.temp.sims_grade_code,
                    'sims_academic_year': $scope.temp.sims_academic_year,
                    'sims_cur_code': $scope.temp.sims_cur_code,
                    'sims_section_name':$scope.temp.sims_section_code,
                    'opr': 'E'
                });


                if (section_list.length > 0 && subject_list.length > 0) {
                    $http.post(ENV.apiUrl + "api/common/SectionSubject/CUDSectionSubjectDetails", selectedsectionlist).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.display = true;
                        var v = document.getElementById('Checkbox1');
                        if (v.checked == true) {
                            v.checked = false;
                        }
                        var v1 = document.getElementById('Checkbox2');
                        if (v1.checked == true) {
                            v1.checked = false;
                        }
                        $scope.SectionSubject2 = [];
                        $scope.section2 = [];
                        $scope.SectionShowData = [];
                        swal({ text: $scope.msg1.strMessage, width: 380 });
                        section_list = [];
                        subject_list = [];

                        $scope.SectionShowData = [];
                        $scope.SectionSubject5 = [];
                        SectionSubject6 = [];

                        $http.get(ENV.apiUrl + "api/common/SectionSubject/getSectionNamesWithSubjects?curcode=" + $scope.temp.sims_cur_code + "&gradecode=" + $scope.temp.sims_grade_code + "&academicyear=" + $scope.temp.sims_academic_year + "&sectioncode=" + $scope.temp.sims_section_code).then(function (allSectionSubject2) {
                            $scope.SectionSubject5 = allSectionSubject2.data;
                            for (var i = 0; i < $scope.SectionSubject5.length; i++) {
                                if ($scope.SectionSubject5[i].sims_subject_status == true && $scope.temp.sims_section_code == $scope.SectionSubject5[i].sims_section_code) {
                                    var data = ({
                                        'sims_subject_code': $scope.SectionSubject5[i].sims_subject_code,
                                        'sims_subject_name': $scope.SectionSubject5[i].sims_subject_name
                                    });
                                    SectionSubject6.push(data);
                                }
                            }
                            $scope.SectionSubject2 = SectionSubject6;
                        });

                        $scope.btnadd = true;

                        $http.get(ENV.apiUrl + "api/common/SectionSubject/getSectionNames?curcode=" + $scope.temp.sims_cur_code + "&gradecode=" + $scope.temp.sims_grade_code + "&academicyear=" + $scope.temp.sims_academic_year).then(function (Allsection) {
                            $scope.section2 = Allsection.data;
                            for (var i = 0; i < $scope.section2.length; i++) {
                                if ($scope.section2[i].sims_section_code != $scope.temp.sims_section_code) {
                                    $scope.SectionShowData.push($scope.section2[i])
                                }
                            }
                        });
                    });
                }
                else {
                    swal({
                        text: 'Select Subject(s) Or Select Section(s) To Copy',
                        width: 380,
                    });
                }

            }

            
            $scope.selectsubject = function (str) {

                var v = document.getElementById('Checkbox1');

                for (var i = 0; i < $scope.SectionSubject2.length; i++) {
                    if (v.checked == true) {
                        $scope.SectionSubject2[i].ischecked = true;
                    }
                    else {
                        $scope.SectionSubject2[i].ischecked = false;
                    }
                }

            }

            $scope.selectsection = function () {

                var v = document.getElementById('Checkbox2');
                for (var i = 0; i < $scope.SectionShowData.length; i++)
                {
                    if (v.checked == true) {
                        $scope.SectionShowData[i].ischecked = true;
                    }

                    else {
                        $scope.SectionShowData[i].ischecked = false;
                        
                    }
                }
            }

            $scope.cancel = function () {
                $scope.display = false;
                $scope.grid = true;
                $scope.temp = "";
            }

            $scope.Update = function (str) {
                debugger;
                var subject_insert = [];
                var subject_delete = [];
                $scope.insrterecord = false;
                $scope.deleterecord = false;


                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].sims_subject_status == true) {

                        var data = ({

                            'sims_cur_code': $scope.filteredTodos[i].sims_cur_code,
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                            'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                            'sims_subject_code': $scope.filteredTodos[i].sims_subject_code,
                            'sims_display_order': $scope.filteredTodos[i].sims_display_order,
                            'sims_subject_mdl_course_id':$scope.filteredTodos[i].sims_subject_mdl_course_id,
                            'sims_section_subject_credit_hours':$scope.filteredTodos[i].sims_section_subject_credit_hours
                        });
                        subject_insert.push(data);
                    }

                  
                    else {


                        var data = ({
                            'sims_cur_code': $scope.filteredTodos[i].sims_cur_code,
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                            'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                            'sims_subject_code': $scope.filteredTodos[i].sims_subject_code
                        });
                        subject_delete.push(data);
                    }
                }
                $http.post(ENV.apiUrl + "api/common/SectionSubject/InsertSectionSubject", subject_insert).then(function (msg) {
                    $scope.msg1 = msg.data;

                    $http.post(ENV.apiUrl + "api/common/SectionSubject/IDeleteSectionSubject", subject_delete).then(function (msg5) {
                        $scope.msg2 = msg5.data;

                        if ($scope.msg2 != "" && $scope.msg2 != undefined) {
                            swal({ text: $scope.msg2, width: 320, showCloseButton: true });
                            $scope.msg2 = "";
                        }
                        else {

                            swal({ text: 'Record Updated Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                        }


                        $scope.row1 = '';
                        main = document.getElementById('mainchk');
                        main.checked = false;
                        $scope.getSectionGradesWise();
                    })

                })

            }

            $scope.Selectmultiple = function () {
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if (main.checked == true) {
                        $scope.filteredTodos[i].sims_subject_status = true;
                    }
                    else {
                        $scope.filteredTodos[i].sims_subject_status = false;
                    }
                }
            }

            $scope.SelectSingle = function (str) {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.searched = function (valLists, toSearch) {


                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.SectionSubject, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.SectionSubject;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_subject_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

            };

            $scope.changeno = function (obj) {
                $scope.maindata = obj;
                if (obj.sims_display_order >= 99) {
                    swal('', 'Please enter 0 to 99 days.');
                    $scope.maindata['sims_display_order'] = '';
                }
            }

            $scope.onlyNumbers = function (event, leave_max) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }

                }

                event.preventDefault();


            };


        //supriya: applied one function to display only checked subject records when click on selected subject checkbox

            $scope.selectedsub = function (str) {
                debugger
                $scope.new_arr = [];
                if (str == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        if ($scope.filteredTodos[i].sims_subject_status == true) {
                            $scope.new_arr.push($scope.filteredTodos[i]);
                        }
                    }
                    $scope.filteredTodos = [];
                    $scope.filteredTodos=$scope.new_arr;
                    
                }

                else 

                {
                    $scope.getSectionGradesWise();
                  ///  $scope.busy = true;
                }
               
            }
        
           
        }])
})();