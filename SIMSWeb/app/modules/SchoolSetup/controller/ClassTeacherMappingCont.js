﻿
(function () {
    'use strict';
    var sectionlist = [];
    var le = 0;
    var main, strMessage;

    var simsController = angular.module('sims.module.SchoolSetup');
    simsController.controller('ClassTeacherMappingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.table = false;
            $scope.busyindicator = false;
            var Employee_code = '';
            $scope.temp = {};
            //$http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2017&routeCode=01").then(function (AllComboBoxValues) {
            //    $scope.ComboBoxValues = AllComboBoxValues.data;
            //    console.log($scope.ComboBoxValues);
            //});
            var ob = [];
            $scope.info = { section: [] }


            //$http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (cur_code) {
            //    $scope.temp = {};
            //    $scope.cur_data = cur_code.data;
            //    $scope.temp["sims_cur_code"] = $scope.cur_data[0].sims_cur_code;
            //    $scope.academic_year();
            //});

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
                console.log("Data:", $scope.propertyName);
                console.log("Data:", $scope.reverse);
            };


            function getCur(flag, comp_code) {
                if (flag) {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                        $scope.cur_data = res.data;
                        $scope.temp["sims_cur_code"] = $scope.cur_data[0].sims_cur_code;
                        $scope.academic_year();

                    });
                }
                else {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                        $scope.cur_data = res.data;
                        $scope.temp["sims_cur_code"] = $scope.cur_data[0].sims_cur_code;
                        $scope.academic_year();
                    });
                }

            }

            $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.count = res.data;
                if ($scope.count) {
                    getCur(true, $scope.user_details.comp);
                }
                else {
                    getCur(false, $scope.user_details.comp)
                }

            });






            $scope.academic_year = function () {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.cur_data[0].sims_cur_code).then(function (a_year) {
                    $scope.aca_year = a_year.data;
                    console.log($scope.aca_year);
                    $scope.temp["sims_academic_year"] = $scope.aca_year[0].sims_academic_year;
                    //  $scope.get_grades($scope.cur_data[0].sims_cur_code, $scope.aca_year[0].sims_academic_year);
                });
            }


            $scope.getGrades = function () {
                ob = {
                    sims_cur: $scope.temp["sims_cur_code"],
                    sims_academic_year: $scope.temp["sims_academic_year"]
                }
                $http.post(ENV.apiUrl + "api/classteachermapping/gradedetails", ob).then(function (grades) {
                    $scope.grade_data = grades.data.table;
                    $scope.section_data = grades.data.table1
                    //for (var i = 0; $scope.grade_data.length > i; i++) {                     
                    //    $scope.grade_data[i].sections=getsec($scope.grade_data[i].sims_grade_code);
                    //}

                    console.log(grades.data);
                });
            }

            function getsec(grd) {
                var sec = [];
                for (var i = 0; $scope.sec_data.length > i; i++) {
                    if ($scope.sec_data[i].sims_grade_code = grd) {
                        sec.push($scope.sec_data[i]);
                    }
                }
                return sec;
            }
            //$scope.getSections = function (curgrade) {
            //    debugger;
            //    var obj = {
            //        sims_cur:$scope.temp["sims_cur_code"],
            //        sims_academic_year:$scope.temp["sims_academic_year"],
            //        sims_grade_code:curgrade.sims_grade_code
            //    }
            //    $http.post(ENV.apiUrl + "api/classteachermapping/sectiondetails", obj).then(function (section) {
            //        $scope.section_data = section.data.table;
            //        curgrade.section_data = $scope.section_data;
            //        console.log(curgrade);

            //    });
            //}

            $scope.getteacherdata = function () {
                $scope.getGrades();
                $scope.busyindicator = true;
                $scope.table = false;
                $http.get(ENV.apiUrl + "api/classteachermapping/GetAllTeacher_Name?data=" + JSON.stringify($scope.temp)).then(function (AllTeacher_Name) {
                    $scope.All_Teacher_Name = AllTeacher_Name.data;
                    console.log($scope.All_Teacher_Name);
                    $scope.totalItems = $scope.All_Teacher_Name.length;
                    $scope.todos = $scope.All_Teacher_Name;
                    $scope.makeTodos();
                    $scope.table = true;

                    $scope.busyindicator = false;

                });

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {

                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.CheckAllChecked();
                }
            };

            $scope.SaveTeacherData = function () {
                debugger;
                var Sdata = [{}];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].ischange == true && $scope.filteredTodos[i].sims_grade_code != '' && $scope.filteredTodos[i].sims_section_code != '') {
                        var le = Sdata.length;
                        Sdata[le] = {
                            'sims_cur_code': $scope.temp.sims_cur_code,
                            'sims_academic_year': $scope.temp.sims_academic_year,
                            'sims_grade_code': $scope.filteredTodos[i].sims_grade_code,
                            'sims_section_code': $scope.filteredTodos[i].sims_section_code,
                            'sims_teacher_code': $scope.filteredTodos[i].sims_teacher_code,
                            'opr': 'I'
                        }
                    }
                }

                Sdata.splice(0, 1);
                var data = Sdata;
                if (data.length > 0) {
                    $scope.busyindicator = true;
                    $scope.table = false;
                    $http.post(ENV.apiUrl + "api/classteachermapping/InsertUpdateClassTeacher?simsobj=", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: 'Teacher Mapping Updated Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true })
                            $scope.getteacherdata();
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: 'Teacher Mapping Not Updated. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true })
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $http.get(ENV.apiUrl + "api/classteachermapping/GetAllTeacher_Name?data=" + JSON.stringify($scope.temp)).then(function (AllTeacher_Name) {

                            $scope.All_Teacher_Name = AllTeacher_Name.data;
                            //console.log($scope.All_Teacher_Name);

                            $scope.totalItems = $scope.All_Teacher_Name.length;
                            $scope.todos = $scope.All_Teacher_Name;
                            $scope.makeTodos();
                            $scope.table = true;

                            $scope.busyindicator = false;

                        });

                    });
                }
                else {
                    swal({ text: 'Please select at least one record',imageUrl: "assets/img/notification-alert.png", width: 340, showCloseButton: true })
                }
            }

            $scope.CheckClassTeacherExist = function (str) {

                debugger;
                str.ischange = true;
                var ischeck = str.sims_section_code;
                var data = {
                    'sims_cur_code': $scope.temp.sims_cur_code,
                    'sims_academic_year': $scope.temp.sims_academic_year,
                    'sims_grade_code': str.sims_grade_code,
                    'sims_section_code': str.sims_section_code,
                    'sims_teacher_code': str.sims_teacher_code,
                    'opr': 'C'

                };

                $http.post(ENV.apiUrl + "api/classteachermapping/CheckExist?simsobj=", data).then(function (isexistteacher) {

                    $scope.isexist_teacher = isexistteacher.data;

                    if ($scope.isexist_teacher.length > 0) {
                        for (var i = 0; i < $scope.isexist_teacher.length; i++) {
                            if ($scope.isexist_teacher[i].isexist == true) {
                                str.sims_section_code = '';
                                strMessage = $scope.isexist_teacher[i].sims_grade_name + '    ' + 'Of' + '    ' + $scope.isexist_teacher[i].sims_section_name + '      ' + 'This Class Already Assigned To' + '    ' + $scope.isexist_teacher[i].sims_teacher_name;
                                swal({ text: strMessage, width: 380, showCloseButton: true });
                                break;
                            }
                            else {
                                $scope.flag = false;
                                for (var j = 0; j < $scope.All_Teacher_Name.length; j++) {
                                    if (str.sims_section_code == $scope.All_Teacher_Name[j].sims_section_code && str.sims_grade_code == $scope.All_Teacher_Name[j].sims_grade_code && str.sims_teacher_code != $scope.All_Teacher_Name[j].sims_teacher_code) {
                                        str.sims_section_code = '';
                                        strMessage = 'This Class Already Assigned To' + '    ' + $scope.All_Teacher_Name[j].sims_teacher_name;
                                        swal({ text: strMessage, width: 380, showCloseButton: true });

                                        for (var s = 0; s < sectionlist.length; s++) {
                                            if (str.sims_teacher_code == sectionlist[s].teacher_code) {
                                                var index = sectionlist.indexOf(str.sims_section_code);
                                                sectionlist.splice(index, 1);
                                                $scope.All_Teacher_Name[j].sims_section_code = '';
                                                break;
                                            }
                                        }
                                        break;
                                    }

                                }

                            }
                        }
                    }

                    //for (var i = 0; i < $scope.isexist_teacher.length; i++) {

                    //    if (str.sims_section_code == $scope.isexist_teacher[i].sims_section_code && str.sims_teacher_code == $scope.isexist_teacher[i].sims_teacher_code) {
                    //        str.sims_section_code = $scope.isexist_teacher[i].sims_section_code;
                    //    }

                    //    else if (str.sims_section_code == $scope.isexist_teacher[i].sims_section_code) {

                    //        for (var i = 0; i < $scope.isexist_teacher.length; i++) {

                    //            str.sims_section_code = '';
                    //            strMessage = $scope.isexist_teacher[i].sims_grade_name + '    ' + 'Of' + '    ' + $scope.isexist_teacher[i].sims_section_name + '      ' + 'This Class Already Assigned To' + '    ' + $scope.isexist_teacher[i].sims_teacher_name;
                    //            swal({ text: strMessage, width: 380, showCloseButton: true });
                    //            break;

                    //        }
                    //    }

                    //    else {

                    //        le = sectionlist.length;

                    //        if (le == 0) {
                    //            str.sims_section_code = str.sims_section_code;
                    //            sectionlist = [{ section: str.sims_section_code, teacher_code: str.sims_teacher_code }];
                    //        }

                    //        else if (le > 0) {
                    //            $scope.flag = true;
                    //            for (var n = 0; n < sectionlist.length; n++) {
                    //                if (str.sims_section_code == sectionlist[n].section && str.sims_teacher_code == sectionlist[n].teacher_code) {
                    //                    str.sims_section_code = sectionlist[n].section;
                    //                }
                    //                else {
                    //                    $scope.flag = false;
                    //                    for (var j = 0; j < $scope.All_Teacher_Name.length; j++) {
                    //                        if (str.sims_section_code == $scope.All_Teacher_Name[j].sims_section_code) {
                    //                            str.sims_section_code = '';
                    //                            strMessage = 'This Class Already Assigned To' + '    ' + $scope.All_Teacher_Name[j].sims_teacher_name;
                    //                            swal({ text: strMessage, width: 380, showCloseButton: true });

                    //                            for (var s = 0; s < sectionlist.length; s++) {
                    //                                if (str.sims_teacher_code == sectionlist[s].teacher_code) {
                    //                                    var index = sectionlist.indexOf(str.sims_section_code);
                    //                                    sectionlist.splice(index, 1);
                    //                                    $scope.All_Teacher_Name[j].sims_section_code = '';
                    //                                    break;
                    //                                }
                    //                            }
                    //                            break;
                    //                        }

                    //                    }

                    //                }

                    //            }
                    //            if ($scope.flag == true) {
                    //                str.sims_section_code == str.sims_section_code;
                    //                sectionlist = [{ section: str.sims_section_code }];
                    //            }

                    //        }


                    //    }
                    //}

                });

            }

            $scope.CheckAllChecked = function () {

                Employee_code = [];
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_teacher_code;
                    var v = document.getElementById(t);
                    if (main.checked == true) {
                        v.checked = true;
                        Employee_code = Employee_code + $scope.filteredTodos[i].sims_teacher_code + ','
                        $scope.row1 = 'row_selected';
                    }
                    else {
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }
            }

            $scope.CheckOnebyOneDelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
            }

            $scope.DeleteTeacherData = function () {

                $scope.currentPage = 1;
                var flag = false;

                var Employee_code_list = [];



                debugger;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_teacher_code);

                    if (v.checked == true) {
                        flag = true;
                        $scope.filteredTodos[i].sims_cur_code = $scope.temp.sims_cur_code;
                        $scope.filteredTodos[i].academic_year = $scope.temp.academic_year;
                        $scope.filteredTodos[i].opr = 'D';
                        Employee_code_list.push($scope.filteredTodos[i]);
                    }
                }


                if (flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/classteachermapping/IUDnsertClassTeacher", Employee_code_list).then(function (msg) {

                                $scope.msg1 = msg.data;

                                if ($scope.msg1 == true) {

                                    swal({ text: 'Record Deleted Successfully',imageUrl: "assets/img/check.png", width: 350, showCloseButton: true });
                                    $scope.table = true;
                                    $scope.busyindicator = false;
                                    $scope.currentPage = 1;
                                    Employee_code = '';
                                    $scope.row1 = '';
                                    $scope.filteredTodos = '';
                                    $scope.getteacherdata();

                                }

                                else if ($scope.msg1 == false) {
                                    $scope.table = true;
                                    $scope.currentPage = 1;
                                    $scope.busyindicator = false;
                                    swal({ text: 'Record Not Deleted. ' , width: 350, showCloseButton: true });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });

                        }
                        else {
                            $scope.filteredTodos = [];
                            $scope.currentPage = 1;
                            $scope.table = true;
                            $scope.busyindicator = false;
                            Employee_code = '';
                            $scope.row1 = '';
                            $scope.makeTodos();
                        }

                    });
                }
                else {
                    swal({ text: 'Select At Least One Record To Delete', imageUrl: "assets/img/notification-alert.png",width: 350, showCloseButton: true });
                }
            }

            $scope.size = function (str) {

                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                //$scope.makeTodos();
                  debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.All_Teacher_Name;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {



                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.All_Teacher_Name, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.All_Teacher_Name;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_teacher_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_employee_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.Change = function (teacher) {

                teacher.ischange = true;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


        }])
})();