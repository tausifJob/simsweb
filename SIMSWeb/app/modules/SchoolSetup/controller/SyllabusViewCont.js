﻿(function () {
    'use strict';
    var main;

    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SyllabusViewCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var user = $rootScope.globals.currentUser.username;
            var schoolname = $http.defaults.headers.common['schoolId'];

            $scope.path1 = ENV.apiUrl + "/Content/" + schoolname + "/Images/Syllabus/";

            $http.get(ENV.apiUrl + "api/syllabus_lesson_PP/get_syllabus_lesson_for_PP").then(function (res) {
                    if (res.data !== null) {
                        $scope.lesson_approvaled_data = res.data;
                    }
                    else {
                        swal({ text: 'Data Not Found', imageUrl: "assets/img/close.png" });
                    }

                });
            

                $scope.Getsyllabus_Approve_data = function (info) {

                    $scope.Approve_plan = [];
                    $scope.busy = true;


                    $http.post(ENV.apiUrl + "api/syllabus_lesson_PP/syllabus_Approve_PP", info).then(function (res) {
                        if (res.data.length > 0) {
                            $scope.Approve_plan = res.data;

                            $scope.busy = false;
                        } else {
                            swal({ text: 'Data Not Found',imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                            $scope.busy = false;
                        }
                    });
                }
            
                $scope.print = function (div) {
                    debugger;
                    var docHead = document.head.outerHTML;
                    var printContents = document.getElementById(div).outerHTML;
                    var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";

                    var newWin = window.open("", "_blank", winAttr);
                    var writeDoc = newWin.document;
                    writeDoc.open();
                    writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                    writeDoc.close();
                    newWin.focus();
                }
        }]);

})();



