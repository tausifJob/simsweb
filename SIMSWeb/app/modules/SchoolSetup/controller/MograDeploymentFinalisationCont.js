﻿(function () {
    'use strict';
   
    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('MograDeploymentFinalisationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            console.clear();
            $http.get(ENV.apiUrl + "api/MograVersionDetails/GetProjectNames").then(function (project) {

                $scope.ProjectNames = project.data;
                console.log('this is pr name', $scope.ProjectNames);
                console.log("aaa");
            });
            $http.get(ENV.apiUrl + "api/MograVersionDetails/GetProjectModules").then(function (modName) {

                $scope.ModuleName = modName.data;

            });
            $scope.getApplications = function (mod_code) {
                $http.get(ENV.apiUrl + "api/MograVersionDetails/GetProjectApplications?mod_code=" + mod_code).then(function (appname) {
                    $scope.ApplicationName = appname.data;
                    console.log('this is module name', $scope.ProjectNames);
                    console.log("bbb");
                });
            }
            //$http.get(ENV.apiUrl + "api/MograVersionDetails/GetProjectStatus").then(function (projstatus) {

            //    $scope.ProjectStatus = projstatus.data;
            //});


            $scope.reset = function () {


            }
            $scope.AddDetail = function () {

                $scope.edtdetails['cs_no'] = $scope.edtdetails.cs_no;
                $scope.edtdetails['comn_mod_code'] = $scope.edtdetails.comn_mod_code;
                $scope.edtdetails['comn_appl_code'] = $scope.edtdetails.comn_appl_code;
                $scope.edtdetails['sp_name'] = $scope.edtdetails.sp_name;
                $scope.edtdetails['comn_version_remark'] = $scope.edtdetails.comn_version_remark;
                $scope.edtdetails['sp_remark'] = $scope.edtdetails.sp_remark;
                $scope.edtdetails['is_doc_done'] = $scope.edtdetails.is_doc_done;
                $scope.edtdetails['is_test_case_design'] = $scope.edtdetails.is_test_case_design;
                $scope.edtdetails['comn_project_status_code'] = $scope.edtdetails.comn_project_status_code;

            }

        }]);

})();


