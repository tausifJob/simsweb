﻿(function () {
    'use strict';
    var obj1, obj2, temp, main;
    var opr = '';
    var Boardcode = [];
    var data1 = [];
    var deletecategorycode = [];
    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('BoardCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.edt = "";
            $scope.BoardDetail = true;
            $scope.editmode = false;
            var formdata = new FormData();
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {
                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str;
            //    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.BoardData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.check_all();
            }

            $scope.cancel = function () {
                $scope.BoardDetail = true;
                $scope.BoardData1 = false;
                //$scope.edt = "";
                $scope.edt.sims_board_code = "";
                $scope.edt.sims_board_short_name = "";
                $scope.edt.sims_board_name = "";
                $scope.edt.sims_board_contact_no = "";
                $scope.edt.sims_board_address = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                $scope.myForm.$setPristine();
                $scope.newmode = true;
                //$scope.check = true;
                $scope.edt = "";
                $scope.editmode = false;
                opr = 'S';
                $scope.readonly = false;
                $scope.BoardDetail = false;
                $scope.BoardData1 = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;

                $scope.edt = {};
                $scope.edt.sims_board_status = true;

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                // $scope.edt.sims_board_short_name = "";
            }
               
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                console.log("begin=" + begin); console.log("end=" + end);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.BoardData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.BoardData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.check_all();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                    item.sims_board_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_board_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_board_short_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_board_address.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_board_contact_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_board_contact_no == toSearch) ? true : false;

            }

            $http.get(ENV.apiUrl + "api/BoardDetail/getBoardDetail").then(function (Board_Data) {
                $scope.BoardData = Board_Data.data;
                $scope.totalItems = $scope.BoardData.length;
                $scope.todos = $scope.BoardData;
                $scope.makeTodos();
                console.log($scope.BoardData);
            });

            $http.get(ENV.apiUrl + "api/BoardDetail/getBoardDetail").then(function (res) {
                $scope.BoardData1 = false;
                $scope.BoardDetail = true;
                $scope.obj = res.data;
                console.log($scope.obj);
            });

            $scope.Save = function (myForm) {
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'I';
                    $scope.edt = "";
                    $scope.exist = false;
                    data1.push(data);
                    for (var i = 0; i < $scope.BoardData.length; i++) {
                        if ($scope.BoardData[i].sims_board_code == data.sims_board_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ text: "Record Already exists",imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                    else {
                        $http.post(ENV.apiUrl + "api/BoardDetail/BoardCUD?simsobj=", data1).then(function (msg) {
                            $scope.BoardData1 = false;
                            $http.get(ENV.apiUrl + "api/BoardDetail/getBoardDetail").then(function (Board_Data) {
                                $scope.BoardData = Board_Data.data;
                                $scope.totalItems = $scope.BoardData.length;
                                $scope.todos = $scope.BoardData;
                                $scope.makeTodos();
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        });
                        $scope.BoardDetail = true;
                        $scope.BoardData1 = false;
                    }
                    data1 = [];
                }
            }

            $scope.up = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.BoardDetail = false;
                $scope.BoardData1 = true;
                $scope.edt = {
                    sims_board_code: str.sims_board_code
                              , sims_board_short_name: str.sims_board_short_name
                              , sims_board_name: str.sims_board_name
                              , sims_board_contact_no: str.sims_board_contact_no
                              , sims_board_address: str.sims_board_address
                              , sims_board_status: str.sims_board_status


                };
            }

            }

            $scope.Update = function (myForm) {
                debugger;
                if (myForm) {

                    var data = $scope.edt;
                    data.opr = 'U';

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/BoardDetail/BoardCUD?simsobj=", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.BoardData1 = false;
                        $http.get(ENV.apiUrl + "api/BoardDetail/getBoardDetail").then(function (Board_Data) {
                            $scope.BoardData = Board_Data.data;
                            $scope.totalItems = $scope.BoardData.length;
                            $scope.todos = $scope.BoardData;
                            formdata = new FormData();
                            $scope.makeTodos();

                            if ($scope.msg1 == true) {
                                swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png",width: 300, height: 200 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Updated. ",imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });

                    })
                    $scope.BoardData1 = false;
                    $scope.BoardDetail = true;

                }
            }

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_board_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_board_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.check_once = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }

            $scope.deleterecord = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    debugger;
                    $scope.flag = false;
                    var deleteleave = [];
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_board_code + i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodulecode = ({
                                'sims_board_code': $scope.filteredTodos[i].sims_board_code,
                                opr: 'D'
                            });
                            deleteleave.push(deletemodulecode);
                        }
                    }
                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            confirmButtonText: 'Yes',
                            showCancelButton: true,
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                $http.post(ENV.apiUrl + "api/BoardDetail/BoardCUD", deleteleave).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/BoardDetail/getBoardDetail").then(function (Board_Data) {
                                                    $scope.BoardData = Board_Data.data;
                                                    $scope.totalItems = $scope.BoardData.length;
                                                    $scope.todos = $scope.BoardData;
                                                    $scope.makeTodos();
                                                    main.checked = false;
                                                });
                                                main = document.getElementById('all_chk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    $scope.row1 = '';
                                                }
                                            }

                                            $scope.currentPage = true;
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Record Already Mapped, can't Delete. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/BoardDetail/getBoardDetail").then(function (Board_Data) {
                                                    $scope.BoardData = Board_Data.data;
                                                    $scope.totalItems = $scope.BoardData.length;
                                                    $scope.todos = $scope.BoardData;
                                                    $scope.makeTodos();
                                                    main.checked = false;
                                                    $('tr').removeClass("row_selected");
                                                });
                                                main = document.getElementById('all_chk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    $scope.row1 = '';
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });
                            }
                            else {
                                main = document.getElementById('all_chk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }

                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById($scope.filteredTodos[i].sims_board_code + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }

                    $scope.currentPage = str;
                }
            }


        }]);

})();


