﻿﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var comp_code;
    var finance_year;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('MinutesofMeetingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = true;
            $scope.table = true;
            $scope.edt = [];
            $scope.temp = [];
            $scope.attendeesList = [];


            //
            $scope.gloSearch = function () {
                $rootScope.visible_stud = false;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = false;
                $rootScope.visible_Employee = true;
                $rootScope.chkMulti = false;
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                $scope.temp1 = true;
                $scope.temp2 = false;
                $scope.temp3 = false;

            }

            $scope.gloSearch1 = function () {
                $rootScope.visible_stud = false;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = false;
                $rootScope.visible_Employee = true;
                $rootScope.chkMulti = false;
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                $scope.temp1 = false;
                $scope.temp2 = true;
                $scope.temp3 = false;

            }


            $scope.gloSearch3 = function () {
                $rootScope.visible_stud = false;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = false;
                $rootScope.visible_Employee = true;
                $rootScope.chkMulti = true;
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                $scope.temp1 = false;
                $scope.temp2 = false;
                $scope.temp3 = true;

            }

            $scope.$on('global_cancel', function () {
                debugger;

                if ($scope.SelectedUserLst.length > 0) {

                    debugger
                    $scope.edt =
                        {
                            empName: $scope.SelectedUserLst[0].empName,
                            em_number: $scope.SelectedUserLst[0].em_number
                        }
                }
                if ($scope.temp1) {
                    $scope.temp['sims_mom_chairperson_user_code'] = $scope.edt.em_number;
                    $scope.temp['sims_mom_chairperson_user_name'] = $scope.edt.empName;

                    $scope.attendeesList.push({
                        opr: 'J',
                        sims_mom_chairperson_user_code: $scope.temp.sims_mom_chairperson_user_code,
                        sims_mom_chairperson_user_name:$scope.edt.empName,
                        sims_mom_reference_number: $scope.temp.sims_mom_reference_number
                    })
                }
                else if ($scope.temp2) {
                    $scope.temp['sims_mom_recorder_user_code'] = $scope.edt.em_number;
                    $scope.temp['sims_mom_recorder_user_name'] = $scope.edt.empName;

                    $scope.attendeesList.push({
                      opr: 'J',
                      sims_mom_chairperson_user_code: $scope.temp.sims_mom_recorder_user_code,
                      sims_mom_chairperson_user_name: $scope.edt.empName,
                      sims_mom_reference_number: $scope.temp.sims_mom_reference_number
                  })
                }

                else if ($scope.temp3) {
                    $scope.attendeesClick($scope.SelectedUserLst)
                    //$scope.temp['sims_mom_recorder_user_code'] = $scope.edt.em_number;

                }

            });
                $scope.attendees = false;
                $scope.selectChairPerson = function () {
                    $scope.edt['chairperson'] = 'Chair Person';
                }

                $scope.selectRecorder = function () {
                    $scope.edt['recorder'] = 'recorder';
                }

                $scope.meetinghistroy = function () {
                    //                $('#mhistroy').modal({ backdrop: "static" });
                    $scope.table = false;
                }

                $scope.viewCancel = function () {
                    $scope.table = true;
                }
                //$scope.checked = false;
                $timeout(function () {
                    $("#fixTable").tableHeadFixer({ 'top': 1 });
                }, 100);

                $http.get(ENV.apiUrl + "api/meeting/getMinutesOfMeeting").then(function (res) {
                    $scope.records = res.data;
                    console.log($scope.records);
                });


                $http.get(ENV.apiUrl + "api/meeting/getMeetingType").then(function (res) {
                    $scope.mtypes = res.data;
                    console.log($scope.records);
                });
                $scope.exits = false
                $scope.checkRefno = function (refno) {
                    debugger;
                    $http.get(ENV.apiUrl + "api/meeting/getReferenceNo?refno=" + refno).then(function (res) {
                        $scope.count = res.data;
                        if ($scope.count > 0) {
                            $scope.exits = true;
                            $scope.temp['sims_mom_reference_number'] = "";
                            $scope.exitsno = "Reference number already exits";
                        } else {
                            $scope.exitsno = "";
                        }
                        console.log($scope.count);
                    });
                }
                $scope.attendeesClick = function (glsearchemp) {
                    $scope.attendees = true;
                    for (var i = 0; $scope.SelectedUserLst.length > i; i++) {

                                    $scope.att = {
                                        opr: 'j',
                                        sims_mom_chairperson_user_code: $scope.SelectedUserLst[i].em_number,
                                        sims_mom_chairperson_user_name: $scope.SelectedUserLst[i].empName,
                                        sims_mom_reference_number: $scope.temp.sims_mom_reference_number
                                    }
                                    $scope.attendeesList.push($scope.att);
                        }                   
                    console.log($scope.attendeesList);
                }
                $scope.removeUser = function (rmuser, i) {
                    console.log(rmuser);
                    $scope.attendeesList.splice(i, 1);
                    console.log($scope.attendeesList);
                }

                $('.clockpicker').clockpicker({
                    autoclose: true
                });

                $scope.checktime = function (min, max) {
                    console.log(min);
                    console.log(max);
                    $scope.check_time_flag = true;
                    //if (min >= max) {
                    //    swal({ title: "Alert", text: "Please Select Correct Time", imageUrl: "assets/img/notification-alert.png", });
                    //    $scope.check_time_flag = false;
                    //}
                }

                //Select Data SHOW


                $scope.size = function (str) {
                    console.log(str);
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                }

                $scope.index = function (str) {
                    $scope.pageindex = str;
                    $scope.currentPage = str;
                    console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                    main.checked = false;
                }

                $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

                $scope.makeTodos = function () {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }

                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                };

                $scope.searched = function (valLists, toSearch) {
                    return _.filter(valLists,

                    function (i) {
                        /* Search Text in all  fields */
                        return searchUtil(i, toSearch);
                    });
                };


                //Search
                $scope.search = function () {
                    $scope.todos = $scope.searched($scope.fin_doc, $scope.searchText);
                    $scope.totalItems = $scope.todos.length;
                    $scope.currentPage = '1';
                    if ($scope.searchText == '') {
                        $scope.todos = $scope.fin_doc;
                    }
                    $scope.makeTodos();
                }

                function searchUtil(item, toSearch) {
                    /* Search Text in all 3 fields */
                    return (item.gldc_doc_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.gldc_doc_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_doc_srno == toSearch) ? true : false;
                }

                $scope.showdate = function (date, name1) {

                    var month = date.split("/")[0];
                    var day = date.split("/")[1];
                    var year = date.split("/")[2];

                    $scope.temp[name1] = year + "/" + month + "/" + day;
                }


                //NEW BUTTON
                //$scope.New = function () {

                //    debugger;

                //    $scope.disabled = false;
                //    $scope.gtcreadonly = true;
                //    $scope.gtdreadonly = false;
                //    $scope.gdisabled = false;
                //    $scope.aydisabled = false;
                //    $scope.table = false;
                //    $scope.display = true;
                //    $scope.save_btn = true;
                //    $scope.Update_btn = false;
                //    $scope.temp = "";

                //}

                //DATA SAVE INSERT
                var datasend = [];
                $scope.savedata = function (isValidate) {
                    debugger;
                    console.log($scope.temp);
                    var obj = {
                        opr: 'I',
                        sims_mom_reference_number: $scope.temp.sims_mom_reference_number,
                        sims_mom_type_code: $scope.temp.sims_mom_type_code,
                        sims_mom_subject: $scope.temp.sims_mom_subject,
                        sims_mom_venue: $scope.temp.sims_mom_venue,
                        sims_mom_date: $scope.temp.sims_mom_date,
                        sims_mom_start_time: $scope.temp.sims_mom_start_time,
                        sims_mom_end_time: $scope.temp.sims_mom_end_time,
                        sims_mom_chairperson_user_code: $scope.temp.sims_mom_chairperson_user_code,
                        sims_mom_recorder_user_code: $scope.temp.sims_mom_recorder_user_code,
                        sims_mom_requester_user_code: $scope.temp.sims_mom_requester_user_code,
                        sims_mom_approver_user_code: $scope.temp.sims_mom_approver_user_code,
                        sims_mom_date_creation_user_code: $scope.temp.sims_mom_date_creation_user_code,
                        sims_mom_agenda: $scope.temp.sims_mom_agenda
                    }
                    datasend.push(obj);
                    console.log(datasend);

                    $http.post(ENV.apiUrl + "api/meeting/CUDMinutesOfMeeting", obj).then(function (msg) {
                        $scope.result = msg.data;
                        if ($scope.result==true) {
                            swal('', 'Record Inserted Successfully');
                        } else if ($scope.result == false) {
                            swal('', 'Record Not Inserted. ' );
                        }
                        else {
                            swal("Error-" + $scope.result)
                        }
                    });
                    $http.post(ENV.apiUrl + "api/meeting/CUDInsertAttendees", $scope.attendeesList).then(function (msg1) {
                        $scope.result1 = msg1.data;
                        //if ($scope.result) {
                        //    swal('', 'Record Inserted Successfully');
                        //} else {
                        //    swal('', 'Record Not Inserted');
                        //}
                    });
                    //if (isValidate) {
                    //    console.log($scope.temp);
                    //}
                }


                //DATA CANCEL
                $scope.Cancel = function () {
                    $scope.temp = "";
                    //$scope.table = true;
                    //$scope.display = false;
                }

                //DATA EDIT
                $scope.edit = function (str) {
                    debugger;
                    $scope.gtcreadonly = true;
                    $scope.gtdreadonly = true;
                    $scope.gdisabled = false;
                    $scope.aydisabled = true;
                    //$scope.table = false;
                    //$scope.display = true;
                    $scope.save_btn = false;
                    $scope.Update_btn = true;
                    $scope.temp = str;

                    //$scope.getDocumentN(str.sims_doc_mod_code);


                    $scope.temp = {
                        gldc_doc_code: str.gldc_doc_code
                       , gldc_doc_name: str.gldc_doc_name
                       , gldc_doc_type: str.gldc_doc_type
                       , gldc_next_srl_no: str.gldc_next_srl_no
                       , gldc_next_prv_no: str.gldc_next_prv_no

                    };
                }

                //global search

            

            //end global search


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });


        }])

})();
