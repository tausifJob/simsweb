﻿(function () {
    'use strict';
    var obj1;
    var obj2; var temp, opr = '';
    var housecode = [];
    var main;
    var deletehouse = [];
    var data1 = [];
    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('HouseCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;

            $scope.color_code = '';
            $scope.table1 = true;
            $scope.editmode = false;
            var data1 = [];
            $scope.Academic_year = [];
            $scope.edt = {};

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.HouseData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }
            

            //$http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
            //    debugger;                
            //    $scope.curriculum = res.data;
                
            //    if (res.data.length > 0) {
            //        $scope.edt['sims_cur_code'] = res.data[0].sims_cur_code;
            //        $scope.getacyr($scope.edt.sims_cur_code);
            //    }
                
            //})

            function getCur(flag, comp_code) {
                if (flag) {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                        $scope.curriculum = res.data;
                        if (res.data.length > 0) {
                            $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                            $scope.getacyr($scope.curriculum[0].sims_cur_code);
                        }

                    });
                }
                else {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                        $scope.curriculum = res.data;
                        if (res.data.length > 0) {
                            $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                            $scope.getacyr($scope.curriculum[0].sims_cur_code);
                        }

                    });


                }

            }

            $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.global_count_comp = res.data;

                if ($scope.global_count_comp) {
                    getCur(true, $scope.user_details.comp);


                }
                else {
                    getCur(false, $scope.user_details.comp)
                }
            });





            $scope.getacyr = function (str) {

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    if (Academicyear.data.length > 0) {
                        $scope.edt.sims_academic_year = Academicyear.data[0].sims_academic_year;
                        //$scope.year($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                    }
                })
            }

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.editmode = false;
                opr = 'S';
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = {};
                $scope.edt['sims_house_status'] = true;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.colorchange = false;
                debugger;
                //if ($scope.curriculum.length > 0) {
                //    $scope.edt.sims_cur_code = $scope.curriculum[0].sims_cur_code;
                //    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.edt.sims_cur_code).then(function (Academicyear) {
                //        $scope.Academic_year = Academicyear.data;
                //        if (Academicyear.data.length > 0) {
                //            $scope.edt.sims_academic_year = Academicyear.data[0].sims_academic_year;
                //            $http.get(ENV.apiUrl + "api/house/getHouseNumber?curCode=" + $scope.edt.sims_cur_code + '&curYear=' + $scope.edt.sims_academic_year).then(function (houseNumber1) {
                //                $scope.houseNumber = houseNumber1.data;
                //                $scope.edt.sims_house_code = $scope.houseNumber;
                //                console.log(houseNumber1);

                //            });

                //        }
                //    })
                //}

                if ($scope.curriculum.length > 0) {
                    $scope.edt.sims_cur_code = $scope.curriculum[0].sims_cur_code;
                    $scope.getacyr($scope.edt.sims_cur_code);
                }
                if ($scope.Academic_year.length > 0) {
                    $scope.edt.sims_academic_year = $scope.Academic_year[0].sims_academic_year;
                    $scope.year($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                }

            }

            $scope.year = function (curr, academicYear) {
                debugger
                $http.get(ENV.apiUrl + "api/house/getHouseNumber?curCode=" + curr + '&curYear=' + academicYear).then(function (houseNumber1) {
                    $scope.houseNumber = houseNumber1.data;
                    $scope.edt.sims_house_code = $scope.houseNumber;
                    console.log(houseNumber1);

                });
            }

            $scope.up = function (str) {
                opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false; $scope.operation = true;
                debugger
                $scope.edt = {
                    sims_house_code: str.sims_house_code,
                    sims_cur_code: str.sims_cur_code,
                    sims_cur_short_name_en: str.sims_cur_short_name_en,
                    sims_academic_year: str.sims_academic_year,
                    sims_academic_year_desc: str.sims_academic_year_description,
                    sims_house_name: str.sims_house_name,
                    sims_house_color: str.sims_house_color,
                    sims_house_objective: str.sims_house_objective,
                    sims_house_status: str.sims_house_status,
                    sims_house_pledge: str.sims_house_pledge
                };
                $scope.getacyr(str.sims_cur_code);
            }

            $http.get(ENV.apiUrl + "api/house/getHouse").then(function (House_Data) {
                $scope.HouseData = House_Data.data;
                $scope.totalItems = $scope.HouseData.length;
                $scope.todos = $scope.HouseData;
                $scope.makeTodos();

                console.log($scope.HouseData);
            });

            //$scope.check = function () {
            //    main = document.getElementById('mainchk');
            //    del = [null];
            //    if (main.checked == true) {
            //        // $scope.obj1 = res.data;
            //        for (var i = 0; i < $scope.termData.length; i++) {
            //            var t = $scope.termData[i].sims_Criteria_code; console.log(t);
            //            var v = document.getElementById(t);
            //            v.checked = true;
            //            del.push({ id: t });
            //        }
            //    }
            //    else {
            //        // $scope.obj1 = res.data;
            //        for (var i = 0; i < $scope.termData.length; i++) {
            //            var t = $scope.termData[i].sims_Criteria_code;
            //            var v = document.getElementById(t);
            //            v.checked = false;
            //        }
            //    }
            //}

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.edt = {
                    sims_cur_code: '',
                    sims_academic_year: '',
                    sims_house_code: '',
                    sims_house_name: '',
                    sims_house_color: '',
                    sims_house_objective: '',
                    sims_house_pledge: '',
                    sims_house_status: '',
                    sims_academic_year_description: '',
                    sims_cur_short_name_en: '',
                }
            }

            $scope.Save = function (myForm) {
                if (myForm) {
                    data1 = [];
                    data = [];
                    var data = $scope.edt;
                    data.opr = 'I';                    
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/house/HouseCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully",imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Inserted. Because of Selected Academic Year Is Old Academic Year.",imageUrl: "assets/img/close.png", width: 450, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $http.get(ENV.apiUrl + "api/house/getHouse").then(function (House_Data) {
                            $scope.HouseData = House_Data.data;
                            $scope.totalItems = $scope.HouseData.length;
                            $scope.todos = $scope.HouseData;
                            $scope.makeTodos();
                            $scope.msg1 = msg.data;
                        });
                    });
                    $scope.table1 = true;
                    $scope.operation = false;
                    data1 = [];
                }
            }

            $scope.Update = function (myForm) {
                if (myForm) {
                    data1 = [];
                    data = [];
                    var data = $scope.edt;
                    data.opr = 'U';

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/house/HouseCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({  text: "Record Updated Successfully",imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        $http.get(ENV.apiUrl + "api/house/getHouse").then(function (House_Data) {
                            $scope.HouseData = House_Data.data;
                            $scope.totalItems = $scope.HouseData.length;
                            $scope.todos = $scope.HouseData;
                            $scope.makeTodos();
                        });
                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                    data1 = [];
                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            //$scope.CheckAllChecked = function () {

            //    housecode = [];
            //    main = document.getElementById('mainchk');

            //    if (main.checked == true) {
            //        for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //            var t = $scope.filteredTodos[i].sims_house_code;
            //            var v = document.getElementById(t);
            //            v.checked = true;
            //            housecode = housecode + $scope.filteredTodos[i].sims_house_code + ','
            //            $scope.row1 = 'row_selected';
            //        }
            //    }
            //    else {

            //        for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //            var t = $scope.filteredTodos[i].sims_house_code;
            //            var v = document.getElementById(t);
            //            v.checked = false;
            //            main.checked = false;
            //            $scope.row1 = '';
            //            housecode = [];
            //        }
            //    }
            //    console.log(housecode);
            //}

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].sims_house_code);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].sims_house_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }


            //$scope.deleterecord = function () {

            //    $scope.flag = false;
            //    for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //        var v = document.getElementById(i);
            //        if (v.checked == true) {
            //            $scope.flag = true;
            //            var deletelocacode = ({
            //                'sims_house_code': $scope.filteredTodos[i].sims_house_code,
            //                'sims_cur_code': $scope.filteredTodos[i].sims_cur_code,
            //                'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
            //                opr: 'D'
            //            });

            //            deletehouse.push(deletelocacode);
            //        }
            //    }

            //    if ($scope.flag) {
            //        swal({
            //            title: '',
            //            text: "Are you sure you want to Delete?",
            //            showCloseButton: true,
            //            showCancelButton: true,
            //            confirmButtonText: 'Yes',
            //            width: 380,
            //            cancelButtonText: 'No',

            //        }).then(function (isConfirm) {
            //            if (isConfirm) {
            //                $http.post(ENV.apiUrl + "api/house/HouseCUD", deletehouse).then(function (msg) {
            //                    $scope.msg1 = msg.data;
            //                    if ($scope.msg1 == true) {
            //                        swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
            //                            if (isConfirm) {
            //                                $http.get(ENV.apiUrl + "api/house/getHouse").then(function (House_Data) {
            //                                    $scope.HouseData = House_Data.data;
            //                                    $scope.totalItems = $scope.HouseData.length;
            //                                    $scope.todos = $scope.HouseData;
            //                                    $scope.makeTodos();
            //                                });
            //                                main = document.getElementById('mainchk');
            //                                if (main.checked == true) {
            //                                    main.checked = false;
            //                                    {
            //                                        $scope.row1 = '';
            //                                    }
            //                                }
            //                            }
            //                        });
            //                    }
            //                    else {
            //                        swal({ title: "Alert", text: "Record Not Deleted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
            //                            if (isConfirm) {
            //                                $http.get(ENV.apiUrl + "api/house/getHouse").then(function (House_Data) {
            //                                    $scope.HouseData = House_Data.data;
            //                                    $scope.totalItems = $scope.HouseData.length;
            //                                    $scope.todos = $scope.HouseData;
            //                                    $scope.makeTodos();
            //                                });
            //                                main = document.getElementById('mainchk');
            //                                if (main.checked == true) {
            //                                    main.checked = false;
            //                                    {
            //                                        $scope.row1 = '';
            //                                    }
            //                                }
            //                            }
            //                        });
            //                    }

            //                });
            //            }
            //            else {
            //                for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //                    var v = document.getElementById(i);
            //                    if (v.checked == true) {
            //                        v.checked = false;
            //                        main.checked = false;
            //                        $('tr').removeClass("row_selected");
            //                    }
            //                }
            //            }
            //        });
            //    }
            //    else {
            //        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
            //    }
            //    $scope.row1 = '';
            //    main.checked = false;
            //    $scope.currentPage = str;
            //}

            $scope.deleterecord = function () {
                debugger;
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i + $scope.filteredTodos[i].sims_house_code);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_house_code': $scope.filteredTodos[i].sims_house_code,
                            'sims_cur_code': $scope.filteredTodos[i].sims_cur_code,
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        confirmButtonText: 'Yes',
                        showCancelButton: true,
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/house/HouseCUD", deleteleave).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png",showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/house/getHouse").then(function (getLeaveDetail_data) {
                                                $scope.LeaveData = getLeaveDetail_data.data;
                                                $scope.totalItems = $scope.LeaveData.length;
                                                $scope.todos = $scope.LeaveData;
                                                $scope.makeTodos();
                                            });
                                        }

                                        $scope.currentPage = true;
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. " , imageUrl: "assets/img/close.png",showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/house/getHouse").then(function (getLeaveDetail_data) {
                                                $scope.LeaveData = getLeaveDetail_data.data;
                                                $scope.totalItems = $scope.LeaveData.length;
                                                $scope.todos = $scope.LeaveData;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i + $scope.filteredTodos[i].sims_house_code);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({  text: "Please Select Atleast One Record",imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.row1 = '';
                main.checked = false;
                $scope.currentPage = str;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.HouseData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.HouseData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_house_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_house_code == toSearch) ? true : false;
            }
            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */

                return (
                     item.sims_house_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_house_pledge.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_house_objective.toLowerCase().indexOf(toSearch.toLowerCase()) == toSearch) ? true : false;
            }


            $scope.colorcheck = function (curr, academicYear, code) {

                var color = document.getElementById('txt_color').value;

                $http.post(ENV.apiUrl + "api/house/chechcolor?curCode=" + curr + '&curYear=' + academicYear).then(function (chech_color) {
                    $scope.chechcolor1 = chech_color.data;

                    debugger;

                    if (opr == 'U') {
                        for (var i = 0; i < $scope.chechcolor1.length; i++) {

                            if (color == $scope.chechcolor1[i].sims_house_color && code == $scope.chechcolor1[i].sims_house_code) {
                                $scope.edt.sims_house_color = $scope.chechcolor1[i].sims_house_color;
                            }

                            else if (color == $scope.chechcolor1[i].sims_house_color) {
                                for (var i = 0; i < $scope.chechcolor1.length; i++) {
                                    if (code == $scope.chechcolor1[i].sims_house_code) {
                                        $scope.edt.sims_house_color = $scope.chechcolor1[i].sims_house_color;
                                        $scope.colorchange = true;
                                    }
                                }
                            }
                            else {
                                $scope.edt.sims_house_color == color;
                                $scope.colorchange = false;
                            }
                        }
                    }

                    else if (opr == 'S') {

                        for (var i = 0; i < $scope.chechcolor1.length; i++) {

                            if (color == $scope.chechcolor1[i].sims_house_color) {
                                $scope.edt.sims_house_color = '';
                                $scope.colorchange = true;
                                break;
                            }
                            else {
                                $scope.edt.sims_house_color = color;
                                $scope.colorchange = false;
                            }

                        }
                    }
                })
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);



        }])
})();
