﻿(function () {
    'use strict';
    var main;

    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

    var formdata = new FormData();

    simsController.controller('SyllabusCreationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var user = $rootScope.globals.currentUser.username;

            $scope.School_Logo = $scope.obj1.lic_school_logo;
            $scope.School_Name = $scope.obj1.lic_school_name;

            $scope.schoolLogoImg = ENV.apiUrl + 'Content/' + $http.defaults.headers.common.schoolId  + '/Images/SchoolLogo/' + $scope.School_Logo;
            //SIMSAPI/Content/sis/Images/SchoolLogo/{{}}

            var now = new Date();
            var month = (now.getMonth() + 1);
            var day = now.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            // $scope.doc_date = month + '/' + day + '/' + now.getFullYear();
            $scope.doc_date = day + '-' + month + '-' + now.getFullYear();

            $scope.busyindicator = true;
            $scope.headcolor = '1';
            $scope.syllabusnames = true;
            $scope.syllabusscreen = true;
            $scope.Grid = true;
            $scope.savebtn = true;
            $scope.pagesize = '5';
            $scope.Expand2 = true;
            $scope.Expand1 = true;
            $scope.busy = false;

            $scope.savebtndissyll = true;
            $scope.savebtndisunit = true;
            $scope.savebtndistopic = true;

             $scope.edt = {};
            $scope.unit = {};
            $scope.topic = {};
            $scope.sub_topic = {};
            //            $scope.user_details = {};
            $scope.edt["sims_syllabus_status"] = true;
            $scope.unit["sims_syllabus_unit_status"] = true;
            $scope.topic['sims_syllabus_unit_topic_status'] = true;
            $scope.topic['sims_syllabus_unit_sub_topic_status'] = true;

            $scope.edt['sims_syllabus_created_by'] = user;
            $scope.sub_topic['sims_syllabus_unit_sub_topic_created_by'] = user;
            $scope.unit['sims_syllabus_unit_created_by'] = user;
            $scope.topic['sims_syllabus_unit_topic_created_by'] = user;

            $scope.edt['sims_syllabus_creation_date'] = $scope.doc_date;
            $scope.unit['sims_syllabus_unit_creation_date'] = $scope.doc_date;
            $scope.topic['sims_syllabus_unit_topic_creation_date'] = $scope.doc_date;
            $scope.sub_topic['sims_syllabus_unit_sub_topic_creation_date'] = $scope.doc_date;

            var schoolname = $http.defaults.headers.common['schoolId'];

            $scope.path1 = ENV.apiUrl + "/Content/" + schoolname + "/Images/Syllabus/";

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;
                $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.getacyr($scope.curriculum[0].sims_cur_code);
            });

            $scope.getacyr = function (str) {
                $scope.cur_code = str;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    $scope.edt['sims_academic_year'] = $scope.Academic_year[0].sims_academic_year;
                    $scope.get();
                })
            }

            $(function () {
                $('#cmb_grade').multipleSelect({
                    width: '100%'
                });
            });

            $scope.MonthObject = [
                                  { month_name: 'April', month_code: '4' },
                                  { month_name: 'May', month_code: '5' },
                                  { month_name: 'June', month_code: '6' },
                                  { month_name: 'July', month_code: '7' },
                                  { month_name: 'August', month_code: '8' },
                                  { month_name: 'September', month_code: '9' },
                                  { month_name: 'October', month_code: '10' },
                                  { month_name: 'November', month_code: '11' },
                                  { month_name: 'December', month_code: '12' },
                                  { month_name: 'January', month_code: '1' },
                                  { month_name: 'February', month_code: '2' },
                                  { month_name: 'March', month_code: '3' }

            ]

            $scope.getsection = function () {

                var data = {

                    sims_cur_code: $scope.edt.sims_cur_code,
                    sims_academic_year: $scope.edt.sims_academic_year,
                    sims_subject_code: $scope.edt.sims_subject_code,
                    sims_grade_code: $scope.edt.sims_grade_code,
                    lesson_user: user

                }

                $http.post(ENV.apiUrl + "api/syllabus_lesson/syllabus_section", data).then(function (Allsection) {
                    $scope.section1 = Allsection.data;

                    setTimeout(function () {
                        $('#cmb_grade').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            };

            $scope.getsections = function () {

                var data = {

                    sims_cur_code: $scope.edt.sims_cur_code,
                    sims_academic_year: $scope.edt.sims_academic_year,
                    sims_subject_code: $scope.edt.sims_subject_code,
                    lesson_user: user

                }



                $http.post(ENV.apiUrl + "api/syllabus_lesson/syllabus_grade", data).then(function (res) {
                    $scope.grade = res.data;

                });
            };

            $scope.get = function (section) {

                var data = {

                    sims_cur_code: $scope.edt.sims_cur_code,
                    sims_academic_year: $scope.edt.sims_academic_year,
                    lesson_user: user

                }


                $http.post(ENV.apiUrl + "api/syllabus_lesson/syllabus_subject", data).then(function (getSectionsubject_name) {
                    $scope.cmb_subject_name = getSectionsubject_name.data;
                });
            }

            $scope.syllabus_table = false;

            $scope.closesyllabus_table = function () {
                $scope.syllabus_table = false;
                            }

            $scope.closesyllabus_table_topic=function()
            {
                $scope.Unit_topic_table = false;
            }

            $scope.closesyllabus_table_unit = function () {
                $scope.Unit_table = false;
            }

            $scope.Getsyllabus = function () {

                $scope.savebtndissyll = false;
               

                $scope.edt['sims_section_code1'] = '';
                var section_code = '';
                for (var i = 0; i < $scope.edt.sims_section_code.length; i++) {
                    section_code = section_code + $scope.edt.sims_section_code[i] + ',';
                }
                $scope.edt['sims_section_code1'] = section_code;

                $scope.syllabus_table = true;
                $scope.edt["lesson_user"] = user;

                $http.post(ENV.apiUrl + "api/syllabus_lesson/syllabus_lesson", $scope.edt).then(function (res) {
                    if (res.data !== null) {
                        $scope.Get_syllabus_lesson = res.data;
                    }
                    else {
                        swal({ text: 'Sorry Data Not Found' });
                    }

                });
            }

            $scope.SyllabusCreationShow = function (color) {

                $scope.headcolor = color;
                $scope.syllabusscreen = true;
                $scope.Unitscreen = false;
                $scope.Topicscreen = false;
                $scope.SubTopicscreen = false;
                $scope.savebtn = true;
                $scope.syllabusnames = true;
                $scope.ComposeSessonscreen = false;
                $scope.ApproveLessonscreen = false;
                $scope.viewApproveLessonscreen = false;
               
            }

            $scope.UnitCreationShow = function (color) {

                $scope.headcolor = color;
                $scope.syllabusscreen = false;
                $scope.Unitscreen = true;
                $scope.Topicscreen = false;
                $scope.SubTopicscreen = false;
                $scope.savebtn = true;
                $scope.syllabusnames = true;
                $scope.ComposeSessonscreen = false;
                $scope.ApproveLessonscreen = false;
                $scope.viewApproveLessonscreen = false;
                 
            }

            $scope.TopicCreationShow = function (color) {

                $scope.headcolor = color;
                $scope.syllabusscreen = false;
                $scope.Unitscreen = false;
                $scope.Topicscreen = true;
                $scope.SubTopicscreen = false;
                $scope.savebtn = true;
                $scope.syllabusnames = true;
                $scope.ComposeSessonscreen = false;
                $scope.ApproveLessonscreen = false;

                $scope.viewApproveLessonscreen = false;
                $scope.btn_topic_cancel_Click();

            }

            $scope.SubTopicCreationShow = function (color) {

                $scope.headcolor = color;
                $scope.syllabusscreen = false;
                $scope.Unitscreen = false;
                $scope.Topicscreen = false;
                $scope.SubTopicscreen = true;
                $scope.savebtn = true;
                $scope.syllabusnames = true;
                $scope.ComposeSessonscreen = false;
                $scope.viewApproveLessonscreen = false;
                $scope.ApproveLessonscreen = false;

            }

            $scope.ComposeLessonShow = function (color) {

                $scope.headcolor = color;
                $scope.syllabusscreen = false;
                $scope.Unitscreen = false;
                $scope.Topicscreen = false;
                $scope.SubTopicscreen = false;
                $scope.savebtn = false;
                $scope.syllabusnames = false;
                $scope.ComposeSessonscreen = true;
                $scope.ApproveLessonscreen = false;
                $scope.viewApproveLessonscreen = false;
                $scope.lesson_plan = [];

                $scope.edt["lesson_user"] = user;

                $http.post(ENV.apiUrl + "api/syllabus_lesson/syllabus_Topic_Sub_Topics_lesson_for_compose", $scope.edt).then(function (lesson_for_compose) {
                    if (lesson_for_compose.data !== null) {
                        $scope.lesson_for_compose = lesson_for_compose.data;
                    }
                    else {
                        swal({ text: 'Record Not Found',imageUrl: "assets/img/close.png", width: 300, height: 200, showCloseButton: true })
                    }
                });



            }

            $scope.ApproveLessonShow = function (color) {

                $scope.headcolor = color;
                $scope.syllabusscreen = false;
                $scope.Unitscreen = false;
                $scope.Topicscreen = false;
                $scope.SubTopicscreen = false;
                $scope.savebtn = false;
                $scope.syllabusnames = false;
                $scope.ComposeSessonscreen = false;
                $scope.ApproveLessonscreen = true;
                $scope.viewApproveLessonscreen = false;
                $scope.Approve_plan = [];
                $http.get(ENV.apiUrl + "api/syllabus_lesson/get_syllabus_lesson_for_approval?user=" + user).then(function (res) {
                    if (res.data !== null) {
                        $scope.lesson_approval_data = res.data;
                    }
                    else {
                        swal({ text: 'Data Not Found For Approval', imageUrl: "assets/img/close.png" });
                    }

                });

            }

            $scope.viewApproveLessonShow = function (color) {
                $scope.printdata = {};
                $scope.headcolor = color;
                $scope.syllabusscreen = false;
                $scope.Unitscreen = false;
                $scope.Topicscreen = false;
                $scope.SubTopicscreen = false;
                $scope.savebtn = false;
                $scope.syllabusnames = false;
                $scope.ComposeSessonscreen = false;
                $scope.ApproveLessonscreen = false;
                $scope.viewApproveLessonscreen = true;

                $scope.Approve_plan = [];
                $http.get(ENV.apiUrl + "api/syllabus_lesson/get_syllabus_lesson_for_approvaled?user=" + user+"&sims_academic_year="+$scope.edt.sims_academic_year).then(function (res) {
                    if (res.data !== null) {
                        $scope.lesson_approvaled_data = res.data;
                    }
                    else {
                        swal({ text: 'Data Not Found', imageUrl: "assets/img/close.png" });
                    }

                });
            }

            $scope.Submit_unit_data = function (isvalid) {
                if (isvalid) {
                    debugger
                    
                    var unitobject = [];
                    unitobject = $scope.unit.sims_syllabus_unit_name.split(",");
                    $scope.saveunitobject = [];
                    
                        for (var i = 0; i < $scope.edt.sims_section_code.length; i++) {
                            for (var j = 0; j < unitobject.length; j++) {
                               
                                var data = {};
                                if ($scope.Get_syllabus_lesson.length>i) {
                                    data.sims_cur_code = $scope.Get_syllabus_lesson[i].sims_cur_code;
                                    data.sims_academic_year = $scope.edt.sims_academic_year;
                                    data.sims_grade_code = $scope.edt.sims_grade_code;
                                    data.sims_section_code = $scope.Get_syllabus_lesson[i].sims_section_code;
                                    data.sims_subject_code = $scope.edt.sims_subject_code;
                                    data.sims_syllabus_code = $scope.edt.sims_syllabus_code;
                                    data.sims_syllabus_unit_code = $scope.unit.sims_syllabus_unit_code;
                                    data.sims_syllabus_unit_name = unitobject[j];
                                    data.sims_syllabus_unit_short_desc = $scope.unit.sims_syllabus_unit_short_desc;
                                    data.sims_syllabus_unit_creation_date = $scope.unit.sims_syllabus_unit_creation_date;
                                    data.sims_syllabus_unit_created_by = $scope.unit.sims_syllabus_unit_created_by;
                                    data.sims_syllabus_unit_approval_date = $scope.unit.sims_syllabus_unit_approval_date;
                                    data.sims_syllabus_unit_approved_by = $scope.unit.sims_syllabus_unit_approved_by;
                                    data.sims_syllabus_unit_approvar_remark = $scope.unit.sims_syllabus_unit_approvar_remark;
                                    data.sims_syllabus_unit_status = $scope.unit.sims_syllabus_unit_status;
                                    data.sims_month_code = $scope.edt.sims_month_code;
                                    data.opr = 'IU';
                                    $scope.saveunitobject.push(data);
                                }
                            }
                        
                    }


                    if ($scope.saveunitobject.length > 0) {

                        $http.post(ENV.apiUrl + "api/syllabus_lesson/CUD_syllabus_Units", $scope.saveunitobject).then(function (res) {
                            if (res.data == true) {
                                swal({ text: 'Unit Created Successfully',imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                                $scope.unit["sims_syllabus_unit_name"] = '';
                                $scope.unit["sims_syllabus_unit_short_desc"] = '';

                                $scope.savebtndissyll = true;
                             

                                $scope.unitDetails.$setPristine();
                                $scope.unitDetails.$setUntouched();
                                $scope.Edit($scope.edt);
                            }
                            else {
                                swal({ text: 'Unit Not Created', imageUrl: "assets/img/close.png",width: 320, showCloseButton: true });
                            }

                        });
                    }
                     
                }

            }

            $scope.Update_unit_data = function () {


                $scope.updatedataobject = [];
                for (var i = 0; i < $scope.syllabus_Units.length; i++) {

                    if ($scope.syllabus_Units[i].syllabus_status_unit) {

                        var data = {};

                        data.sims_month_code = $scope.edt.sims_month_code;
                        data.sims_cur_code = $scope.syllabus_Units[i].sims_cur_code;
                        data.sims_academic_year = $scope.syllabus_Units[i].sims_academic_year;
                        data.sims_grade_code = $scope.syllabus_Units[i].sims_grade_code;
                        data.sims_section_code = $scope.syllabus_Units[i].sims_section_code;
                        data.sims_subject_code = $scope.syllabus_Units[i].sims_subject_code;
                        data.sims_syllabus_code = $scope.syllabus_Units[i].sims_syllabus_code;
                        data.sims_syllabus_unit_code = $scope.syllabus_Units[i].sims_syllabus_unit_code;
                        data.sims_syllabus_unit_name = $scope.unit.sims_syllabus_unit_name;
                        data.sims_syllabus_unit_short_desc = $scope.unit.sims_syllabus_unit_short_desc;
                        data.sims_syllabus_unit_creation_date = $scope.unit.sims_syllabus_unit_creation_date;
                        data.sims_syllabus_unit_created_by = $scope.unit.sims_syllabus_unit_created_by;
                        data.sims_syllabus_unit_status = $scope.unit.sims_syllabus_unit_status;
                        data.opr = 'UU';
                        $scope.updatedataobject.push(data);
                    }
                }


                $http.post(ENV.apiUrl + "api/syllabus_lesson/CUD_syllabus_Units", $scope.updatedataobject).then(function (res) {
                    if (res.data == true) {
                        swal({ text: 'Unit Updated Successfully', width: 320, showCloseButton: true });
                        $scope.unit["sims_syllabus_unit_name"] = '';
                        $scope.unit["sims_syllabus_unit_short_desc"] = '';
                        $scope.unitDetails.$setUntouched();
                        $scope.unitDetails.$setPristine();
                        $scope.savebtn = true;
                       
                        $scope.Edit($scope.edt);
                    }
                    else if (res.data == false) {
                        swal({ text: 'Unit Not Updated. ' , width: 320, showCloseButton: true });
                    }
                    else {
                        swal("Error-" + res.data)
                    }

                });

            }

            $scope.Editunit = function (info) {

                if ($scope.disabled1 == true) {
                    $scope.savebtn = true;
                }
                else {
                    $scope.savebtn = false;

                    $scope.unit = angular.copy(info);

                    $scope.topic['sims_cur_code'] = info.sims_cur_code;
                    $scope.topic['sims_academic_year'] = info.sims_academic_year;
                    $scope.topic['sims_grade_code'] = info.sims_grade_code;
                    $scope.topic['sims_section_code'] = info.sims_section_code;
                    $scope.topic['sims_subject_code'] = info.sims_subject_code;
                    $scope.topic['sims_syllabus_code'] = info.sims_syllabus_code;
                    $scope.topic['sims_syllabus_unit_code'] = info.sims_syllabus_unit_code;
                    $scope.topic['lesson_user'] = user;

                }
            }

            $scope.Submit_topic_data = function (isvalid) {

                if (isvalid) {
                    $scope.TopicDataObject = [];
                    for (var i = 0; i < $scope.edt.sims_section_code.length; i++) {

                        var data = {};

                        data.sims_cur_code = $scope.edt.sims_cur_code;
                        data.sims_academic_year = $scope.edt.sims_academic_year;
                        data.sims_grade_code = $scope.edt.sims_grade_code;
                        data.sims_section_code = $scope.edt.sims_section_code[i];
                        data.sims_subject_code = $scope.edt.sims_subject_code;
                        data.sims_syllabus_unit_code = $scope.edt.sims_syllabus_unit_code;
                        data.sims_syllabus_unit_topic_name = $scope.topic.sims_syllabus_unit_topic_name;
                        data.sims_syllabus_unit_topic_short_desc = $scope.topic.sims_syllabus_unit_topic_short_desc;
                        data.sims_syllabus_unit_topic_creation_date = $scope.topic.sims_syllabus_unit_topic_creation_date;
                        data.sims_syllabus_unit_topic_created_by = $scope.topic.sims_syllabus_unit_topic_created_by;
                        data.sims_syllabus_unit_topic_status = $scope.topic.sims_syllabus_unit_topic_status
                        data.sims_month_code = $scope.edt.sims_month_code;
                        data.opr = 'TI';
                        $scope.TopicDataObject.push(data);

                    }
                    if ($scope.TopicDataObject.length > 0) {

                        $http.post(ENV.apiUrl + "api/syllabus_lesson/CUD_syllabus_Topics", $scope.TopicDataObject).then(function (res) {
                            if (res.data == true) {
                                swal({ text: 'Topic Created Successfully',imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                                
                                $scope.topic["sims_syllabus_unit_topic_name"] = '';
                                $scope.topic["sims_syllabus_unit_topic_short_desc"] = '';
                                                                
                                $scope.savebtndistopic = true;
                                $scope.Get_Existing_Topic_Name($scope.edt)
                            }
                            else {
                                swal({ text: 'Topic Not Created. ', imageUrl: "assets/img/close.png",width: 320, showCloseButton: true });
                            }

                        });

                    }  

                }
            }

            $scope.Update_topic_data = function () {

                $scope.topicUpdateObject = [];

                for (var i = 0; i < $scope.syllabus_uint_topic_name.length; i++) {
                    if ($scope.syllabus_uint_topic_name[i].syllabus_status_topic == true) {

                        var data = {};

                        data.sims_cur_code = $scope.syllabus_uint_topic_name[i].sims_cur_code;
                        data.sims_academic_year = $scope.syllabus_uint_topic_name[i].sims_academic_year;
                        data.sims_grade_code = $scope.syllabus_uint_topic_name[i].sims_grade_code;
                        data.sims_section_code = $scope.syllabus_uint_topic_name[i].sims_section_code;
                        data.sims_subject_code = $scope.syllabus_uint_topic_name[i].sims_subject_code;
                        data.sims_syllabus_code = $scope.syllabus_uint_topic_name[i].sims_syllabus_code;
                        data.sims_month_code = $scope.edt.sims_month_code;
                        data.sims_syllabus_unit_code = $scope.syllabus_uint_topic_name[i].sims_syllabus_unit_code;
                        data.sims_syllabus_unit_topic_code = $scope.topic.sims_syllabus_unit_topic_code;
                        data.sims_syllabus_unit_topic_name = $scope.topic.sims_syllabus_unit_topic_name;
                        data.sims_syllabus_unit_topic_short_desc = $scope.topic.sims_syllabus_unit_topic_short_desc;
                        data.sims_syllabus_unit_topic_creation_date = $scope.topic.sims_syllabus_unit_topic_creation_date;
                        data.sims_syllabus_unit_topic_created_by = $scope.topic.sims_syllabus_unit_topic_created_by;
                        data.sims_syllabus_unit_topic_status = $scope.topic.sims_syllabus_unit_topic_status;
                        data.opr = 'TU';
                        $scope.topicUpdateObject.push(data);

                    }
                }

                $http.post(ENV.apiUrl + "api/syllabus_lesson/CUD_syllabus_Topics", $scope.topicUpdateObject).then(function (res) {
                    if (res.data == true) {
                        swal({ text: 'Topic Updated Successfully', imageUrl: "assets/img/check.png",width: 320, showCloseButton: true });
                        $scope.topic["sims_syllabus_unit_topic_name"] = '';
                        $scope.topic["sims_syllabus_unit_topic_short_desc"] = '';

                        $scope.TopicDetails.$setPristine();
                        $scope.TopicDetails.$setUntouched();
                        $scope.Get_Existing_Topic_Name();
                    }
                    else if (res.data == false) {
                            swal({ text: 'Topic Not Updated. ', imageUrl: "assets/img/close.png",width: 320, showCloseButton: true });
                    }
                    else {
                        swal("Error-" + res.data)
                    }

                });

            }

            $scope.btn_topic_cancel_Click = function () {

                $scope.topic['sims_syllabus_unit_topic_name'] = '';
                $scope.topic['sims_syllabus_unit_topic_short_desc'] = '';
                $scope.topic['sims_syllabus_unit_topic_creation_date'] = $scope.doc_date;;
                $scope.topic['sims_syllabus_unit_topic_created_by'] = user;

                $scope.savebtn = true;

            }

            $scope.Edittopic = function (info) {

                if ($scope.unitdisabled == true) {

                    scope.savebtn = true;
                } else {

                    $scope.topic = angular.copy(info);
                    $scope.savebtn = false;

                }
            }

            $scope.Submit_Sub_topic_data = function (isvalid) {

                if (isvalid) {

                    var data = {};

                    data.sims_cur_code = $scope.sub_topic.sims_cur_code;
                    data.sims_academic_year = $scope.sub_topic.sims_academic_year;
                    data.sims_grade_code = $scope.sub_topic.sims_grade_code;
                    data.sims_section_code = $scope.sub_topic.sims_section_code;
                    data.sims_subject_code = $scope.sub_topic.sims_subject_code;
                    data.sims_syllabus_code = $scope.sub_topic.sims_syllabus_code;
                    data.sims_syllabus_unit_code = $scope.sub_topic.sims_syllabus_unit_code;
                    data.sims_syllabus_unit_topic_code = $scope.sub_topic.sims_syllabus_unit_topic_code;
                    data.sims_syllabus_unit_sub_topic_code = $scope.sub_topic.sims_syllabus_unit_sub_topic_code;
                    data.sims_syllabus_unit_sub_topic_name = $scope.sub_topic.sims_syllabus_unit_sub_topic_name;
                    data.sims_syllabus_unit_sub_topic_short_desc = $scope.sub_topic.sims_syllabus_unit_sub_topic_short_desc;
                    data.sims_syllabus_unit_sub_topic_creation_date = $scope.sub_topic.sims_syllabus_unit_sub_topic_creation_date;
                    data.sims_syllabus_unit_sub_topic_created_by = $scope.sub_topic.sims_syllabus_unit_sub_topic_created_by;
                    data.sims_syllabus_unit_sub_topic_status = $scope.sub_topic.sims_syllabus_unit_sub_topic_status;
                    data.opr = 'IS'

                    $http.post(ENV.apiUrl + "api/syllabus_lesson/CUD_syllabus_Sub_Topics", data).then(function (res) {
                        if (res.data == true) {
                            swal({ text: 'Sub Topic Created Successfully',imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                            
                            
                        }
                        else {
                            swal({ text: 'Sub Topic Not Created. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                        }

                    });

                }
            }

            $scope.Update_Sub_topic_data = function () {

                $scope.sub_topic['opr'] = 'US';

                $http.post(ENV.apiUrl + "api/syllabus_lesson/CUD_syllabus_Sub_Topics", $scope.sub_topic).then(function (res) {
                    if (res.data == true) {
                        swal({ text: 'Sub Topic Updated Successfully',imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                         
                    }
                    else if (res.data == false) {
                        swal({ text: 'Sub Topic Not Updated. ', imageUrl: "assets/img/close.png",width: 320, showCloseButton: true });
                    }
                    else {
                        swal("Error-" + res.data)
                    }

                });

            }

            $scope.EditSubtopic = function (info) {

                $scope.headcolor = '4';
                $scope.syllabusscreen = false;
                $scope.Unitscreen = false;
                $scope.Topicscreen = false;
                $scope.SubTopicscreen = true;

                $scope.savebtn = false;
                $scope.sub_topic = angular.copy(info);
            }

            $scope.link = function (str) {

                window.open($scope.path1 + str, '_new');
                
            }

            $scope.btn_cancel_Click = function () {

                $scope.edt["sims_grade_code"] = '';
                $scope.edt["sims_section_code"] = '';
                $scope.edt["sims_subject_code"] = '';
                $scope.edt["sims_syllabus_code"] = '';
                $scope.edt["sims_month_code"] = '';
                $scope.edt["sims_syllabus_remark"] = '';
                $scope.edt["sims_syllabus_start_date"] = '';
                $scope.edt["sims_syllabus_end_date"] = '';
                


                $scope.savebtn = true;
                $scope.SyllabusDetails.$setUntouched();
                $scope.SyllabusDetails.$setPristine();

            }

            $scope.btn_unit_cancel_Click = function () {

                $scope.unit['sims_syllabus_unit_name'] = '';
                $scope.unit['sims_syllabus_unit_short_desc'] = '';
                $scope.unit['sims_syllabus_unit_created_by'] = user;
                $scope.unit['sims_syllabus_unit_approvar_remark'] = '';
                $scope.unit['sims_syllabus_unit_creation_date'] = $scope.doc_date;;
                $scope.unit['sims_syllabus_unit_approval_date'] = '';
                $scope.unit['sims_syllabus_unit_approved_by'] = '';
                $scope.unit['sims_syllabus_unit_code'] = '';
                $scope.savebtn = true;


                $scope.unitDetails.$setUntouched();
                $scope.unitDetails.$setPristine();
            }

            $scope.btn_unit_sub_top_cancel_Click = function () {
                //$scope.sub_topic['sims_cur_code']= '';
                //$scope.sub_topic['sims_academic_year']= '';
                //$scope.sub_topic['sims_grade_code']= '';
                //$scope.sub_topic['sims_section_code']= '';
                //$scope.sub_topic['sims_subject_code']= '';
                //$scope.sub_topic['sims_syllabus_code']= '';
                //$scope.sub_topic['sims_syllabus_unit_code']= '';
                //$scope.sub_topic['sims_syllabus_unit_topic_code']= '';
                //$scope.sub_topic['sims_syllabus_unit_sub_topic_code']= '';

                $scope.sub_topic['sims_syllabus_unit_sub_topic_name'] = '';
                $scope.sub_topic['sims_syllabus_unit_sub_topic_short_desc'] = '';
                $scope.sub_topic['sims_syllabus_unit_sub_topic_creation_date'] = $scope.doc_date;;
                $scope.sub_topic['sims_syllabus_unit_sub_topic_created_by'] = user;

                $scope.savebtn = true;

            }

            $scope.CommaSeparetdTopic = function (info) {

                $scope.topic_object = [];

                info = info.split(',');
                for (var i = 0; i < info.length; i++) {
                    var data = {};
                    data.sims_syllabus_unit_topic_name = info[i];
                    $scope.topic_object.push(data);

                }
            }

            $scope.Getsyllabus_topic_subtopic = function (info) {
                debugger
                $scope.lesson_plan = [];
                $scope.busy = true;

                info["lesson_user"] = user;

                for (var i = 0; i < $scope.lesson_for_compose.length; i++)
                {
                    if(info.sims_grade_code==$scope.lesson_for_compose[i].sims_grade_code && info.sims_subject_code==$scope.lesson_for_compose[i].sims_subject_code && info.sims_month_code==$scope.lesson_for_compose[i].sims_month_code)
                    {
                        $scope.lesson_for_compose[i].row_color = '#fef8ae';
                    }   
                    else
                    {
                        $scope.lesson_for_compose[i].row_color='White'
                    }
                }


                $http.post(ENV.apiUrl + "api/syllabus_lesson/syllabus_Topic_Sub_Topics_lesson_compose", info).then(function (res) {
                    if (res.data.length > 0) {
                        $scope.lesson_plan = res.data;
                        $scope.busy = false;

                    }
                    else {
                        swal({ text: 'Unit(s) And Topic(s) Are Not Created / This Lesson Plan Already Approved. ' + res.data, width: 320,imageUrl: "assets/img/close.png", showCloseButton: true });
                        $scope.busy = false;

                    }
                });
            }

            $scope.Cancel_topic_subtopic = function () {

                $scope.edt['sims_grade_code'] = '';
                $scope.edt['sims_section_code'] = '';
                $scope.edt['sims_subject_code'] = '';
                $scope.edt['sims_syllabus_start_date'] = '';
                $scope.edt['sims_syllabus_end_date'] = '';

            }

            $scope.getTheFiles = function ($files) {

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };

            $scope.file_changed = function (element) {

                var str = '';

                var photofile = element.files[0];
                var photo_filename = (photofile.type);
                $scope.lesson_plan[0]['sims_syllabus_lesson_plan_doc_path'] = photofile.name;

                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        //$scope.mother_photo = e.target.result;
                    });
                };
                reader.readAsDataURL(photofile);

                var request = {
                    method: 'POST',
                    url: ENV.apiUrl + '/api/file/uploadsyllabus?filename=' + photofile.name.split('.')[0] + '&location=Syllabus',
                    data: formdata,
                    headers: {
                        'Content-Type': undefined
                    }
                };

                $http(request)
                       .success(function (d) {

                       }, function () {
                           alert("Err");
                       });
            };

            $scope.Update_compose_lesson_plan = function (info) {


                info['sims_syllabus_lesson_plan_doc_status'] = 'A';
                info['sims_syllabus_lesson_plan_doc_upload_date'] = $scope.doc_date;
                info['sims_syllabus_lesson_plan_doc_uploaded_by'] = user;
                info['sims_syllabus_lesson_plan_creation_date'] = $scope.doc_date;
                info['sims_syllabus_lesson_plan_created_by'] = user;
                info['sims_syllabus_lesson_plan_status'] = true;
                if (info['sims_syllabus_lesson_plan_no_of_periods'] != null || info['sims_syllabus_lesson_plan_no_of_periods'] != undefined || info['sims_syllabus_lesson_plan_no_of_periods'] != 'undefined' || info['sims_syllabus_lesson_plan_no_of_periods'] != "") {
                    info['sims_syllabus_lesson_plan_no_of_periods'] = parseInt(info['sims_syllabus_lesson_plan_no_of_periods']);
                }
                if (info['sims_syllabus_lesson_plan_no_of_exercise'] != null || info['sims_syllabus_lesson_plan_no_of_exercise'] != undefined || info['sims_syllabus_lesson_plan_no_of_exercise'] != 'undefined' || info['sims_syllabus_lesson_plan_no_of_exercise'] != "") {
                    info['sims_syllabus_lesson_plan_no_of_exercise'] = parseInt(info['sims_syllabus_lesson_plan_no_of_exercise']);
                }
                if (info['sims_syllabus_lesson_plan_no_of_weekly_test'] != null || info['sims_syllabus_lesson_plan_no_of_weekly_test'] != undefined || info['sims_syllabus_lesson_plan_no_of_weekly_test'] != 'undefined' || info['sims_syllabus_lesson_plan_no_of_weekly_test'] != "") {
                    info['sims_syllabus_lesson_plan_no_of_weekly_test'] = parseInt(info['sims_syllabus_lesson_plan_no_of_weekly_test']);
                }

                var data1 = [];


                var section_code = info.sims_section_code.split(',');

                for (var i = 0; i < section_code.length - 1; i++) {

                    var data = {
                        sims_cur_code: info.sims_cur_code
                     , sims_academic_year: info.sims_academic_year
                     , sims_grade_code: info.sims_grade_code
                     , sims_section_code: section_code[i]
                     , sims_subject_code: info.sims_subject_code
                     , sims_syllabus_code: info.sims_syllabus_code
                     , sims_syllabus_lesson_plan_code: info.sims_syllabus_lesson_plan_code
                     , sims_syllabus_lesson_plan_description: info.sims_syllabus_lesson_plan_description
                     , sims_syllabus_lesson_plan_no_of_periods: info.sims_syllabus_lesson_plan_no_of_periods
                     , sims_syllabus_lesson_plan_no_of_exercise: info.sims_syllabus_lesson_plan_no_of_exercise
                     , sims_syllabus_lesson_plan_no_of_activity: info.sims_syllabus_lesson_plan_no_of_activity
                     , sims_syllabus_lesson_plan_no_of_homework: info.sims_syllabus_lesson_plan_no_of_homework
                     , sims_syllabus_lesson_plan_no_of_weekly_test: info.sims_syllabus_lesson_plan_no_of_weekly_test
                     , sims_syllabus_lesson_plan_addl_requirement: info.sims_syllabus_lesson_plan_addl_requirement
                     , sims_syllabus_lesson_plan_creation_date: info.sims_syllabus_lesson_plan_creation_date
                     , sims_syllabus_lesson_plan_created_by: info.sims_syllabus_lesson_plan_created_by
                     , sims_syllabus_lesson_plan_approval_date: info.sims_syllabus_lesson_plan_approval_date
                     , sims_syllabus_lesson_plan_approved_by: info.sims_syllabus_lesson_plan_approved_by
                     , sims_syllabus_lesson_plan_approval_remark: info.sims_syllabus_lesson_plan_approval_remark
                     , sims_syllabus_lesson_plan_attr1: info.sims_syllabus_lesson_plan_attr1
                     , sims_syllabus_lesson_plan_attr2: info.sims_syllabus_lesson_plan_attr2
                     , sims_syllabus_lesson_plan_attr3: info.sims_syllabus_lesson_plan_attr3
                     , sims_syllabus_lesson_plan_attr4: info.sims_syllabus_lesson_plan_attr4
                     , sims_syllabus_lesson_plan_attr5: info.sims_syllabus_lesson_plan_attr5
                     , sims_syllabus_lesson_plan_status: info.sims_syllabus_lesson_plan_status
                     , sims_syllabus_lesson_plan_doc_path: info.sims_syllabus_lesson_plan_doc_path
                     , sims_syllabus_lesson_plan_doc_status: info.sims_syllabus_lesson_plan_doc_status
                     , sims_syllabus_lesson_plan_doc_upload_date: info.sims_syllabus_lesson_plan_doc_upload_date
                     , sims_syllabus_lesson_plan_doc_uploaded_by: info.sims_syllabus_lesson_plan_doc_uploaded_by
                    , sims_month_code: info.sims_month_code
                        , opr: "UL"
                    }

                    data1.push(data);
                }



                $http.post(ENV.apiUrl + "api/syllabus_lesson/CUDsyllabus_Topic_Sub_Topics_lesson_compose_for_Update", data1).then(function (res) {
                    if (res.data == true) {
                        info = {};
                        $scope.lesson_plan = [];
                        swal({ text: 'Lesson Plan Updated Successfully', imageUrl: "assets/img/check.png",width: 320, showCloseButton: true });
                    }
                    else if (res.data == false) {
                            swal({ text: 'Lesson Plan Not Updated. ' ,imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    }
                    else {
                        swal("Error-" + res.data)
                    }
                });

            }

            $scope.Submit_compose_lesson_plan = function (info) {

                debugger
                info['sims_syllabus_lesson_plan_doc_status'] = 'A';
                info['sims_syllabus_lesson_plan_doc_upload_date'] = $scope.doc_date;
                info['sims_syllabus_lesson_plan_doc_uploaded_by'] = user;
                info['sims_syllabus_lesson_plan_creation_date'] = $scope.doc_date;
                info['sims_syllabus_lesson_plan_created_by'] = user;
                info['sims_syllabus_lesson_plan_status'] = true;
                if (info['sims_syllabus_lesson_plan_no_of_periods'] != null || info['sims_syllabus_lesson_plan_no_of_periods'] != undefined || info['sims_syllabus_lesson_plan_no_of_periods'] != 'undefined' || info['sims_syllabus_lesson_plan_no_of_periods'] != "") {
                    info['sims_syllabus_lesson_plan_no_of_periods'] = parseInt(info['sims_syllabus_lesson_plan_no_of_periods']);
                }
                if (info['sims_syllabus_lesson_plan_no_of_exercise'] != null || info['sims_syllabus_lesson_plan_no_of_exercise'] != undefined || info['sims_syllabus_lesson_plan_no_of_exercise'] != 'undefined' || info['sims_syllabus_lesson_plan_no_of_exercise'] != "") {
                    info['sims_syllabus_lesson_plan_no_of_exercise'] = parseInt(info['sims_syllabus_lesson_plan_no_of_exercise']);
                }
                if (info['sims_syllabus_lesson_plan_no_of_weekly_test'] != null || info['sims_syllabus_lesson_plan_no_of_weekly_test'] != undefined || info['sims_syllabus_lesson_plan_no_of_weekly_test'] != 'undefined' || info['sims_syllabus_lesson_plan_no_of_weekly_test'] != "") {
                    info['sims_syllabus_lesson_plan_no_of_weekly_test'] = parseInt(info['sims_syllabus_lesson_plan_no_of_weekly_test']);
                }

                var data1 = [];


                var section_code = info.sims_section_code.split(',');

                for (var i = 0; i < section_code.length - 1; i++) {

                    var data = {
                        sims_cur_code: info.sims_cur_code
                     , sims_academic_year: info.sims_academic_year
                     , sims_grade_code: info.sims_grade_code
                     , sims_section_code: section_code[i]
                     , sims_subject_code: info.sims_subject_code
                     , sims_syllabus_code: info.sims_syllabus_code
                     , sims_syllabus_lesson_plan_code: info.sims_syllabus_lesson_plan_code
                     , sims_syllabus_lesson_plan_description: info.sims_syllabus_lesson_plan_description
                     , sims_syllabus_lesson_plan_no_of_periods: info.sims_syllabus_lesson_plan_no_of_periods
                     , sims_syllabus_lesson_plan_no_of_exercise: info.sims_syllabus_lesson_plan_no_of_exercise
                     , sims_syllabus_lesson_plan_no_of_activity: info.sims_syllabus_lesson_plan_no_of_activity
                     , sims_syllabus_lesson_plan_no_of_homework: info.sims_syllabus_lesson_plan_no_of_homework
                     , sims_syllabus_lesson_plan_no_of_weekly_test: info.sims_syllabus_lesson_plan_no_of_weekly_test
                     , sims_syllabus_lesson_plan_addl_requirement: info.sims_syllabus_lesson_plan_addl_requirement
                     , sims_syllabus_lesson_plan_creation_date: info.sims_syllabus_lesson_plan_creation_date
                     , sims_syllabus_lesson_plan_created_by: info.sims_syllabus_lesson_plan_created_by
                     , sims_syllabus_lesson_plan_approval_date: info.sims_syllabus_lesson_plan_approval_date
                     , sims_syllabus_lesson_plan_approved_by: info.sims_syllabus_lesson_plan_approved_by
                     , sims_syllabus_lesson_plan_approval_remark: info.sims_syllabus_lesson_plan_approval_remark
                     , sims_syllabus_lesson_plan_attr1: info.sims_syllabus_lesson_plan_attr1
                     , sims_syllabus_lesson_plan_attr2: info.sims_syllabus_lesson_plan_attr2
                     , sims_syllabus_lesson_plan_attr3: info.sims_syllabus_lesson_plan_attr3
                     , sims_syllabus_lesson_plan_attr4: info.sims_syllabus_lesson_plan_attr4
                     , sims_syllabus_lesson_plan_attr5: info.sims_syllabus_lesson_plan_attr5
                     , sims_syllabus_lesson_plan_status: info.sims_syllabus_lesson_plan_status
                     , sims_syllabus_lesson_plan_doc_path: info.sims_syllabus_lesson_plan_doc_path
                     , sims_syllabus_lesson_plan_doc_status: info.sims_syllabus_lesson_plan_doc_status
                     , sims_syllabus_lesson_plan_doc_upload_date: info.sims_syllabus_lesson_plan_doc_upload_date
                     , sims_syllabus_lesson_plan_doc_uploaded_by: info.sims_syllabus_lesson_plan_doc_uploaded_by
                        , sims_month_code: info.sims_month_code
                        , opr: "LI"
                    }

                    data1.push(data);
                }



                $http.post(ENV.apiUrl + "api/syllabus_lesson/CUDsyllabus_Topic_Sub_Topics_lesson_compose", data1).then(function (res) {
                    if (res.data == true) {
                        info = {};
                        $scope.lesson_plan = [];
                        swal({ text: 'Lesson Plan Composed Successfully',imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                    }
                    else {
                        swal({ text: 'Lesson Plan Not Composed. ',imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    }
                });

            }

            $scope.Getsyllabus_Approve_data = function (info) {
                debugger
                $scope.Approve_plan = [];
                $scope.busy = true;

                $scope.printdata = {};
                $scope.printdata.month_name = info.month_name;
                $scope.printdata.Academic_year = info.sims_academic_year_description;
                $scope.printdata.Subject = info.sims_subject_name;
                $scope.printdata.GardeSecton = info.sims_grade_name + '/' + info.sims_section_name;
                $scope.Preparedby=info.sims_syllabus_created_name;
                $scope.Approvedby = info.sims_syllabus_approved_name;

                if (angular.isUndefined($scope.lesson_approvaled_data)) {
                    $scope.lesson_approvaled_data = [];
                }

                for (var i = 0; i < $scope.lesson_approvaled_data.length; i++)
                {
                    if (info.sims_grade_code == $scope.lesson_approvaled_data[i].sims_grade_code && info.sims_subject_code == $scope.lesson_approvaled_data[i].sims_subject_code && info.sims_month_code == $scope.lesson_approvaled_data[i].sims_month_code)

                    {
                        $scope.lesson_approvaled_data[i].row_color = '#fef8ae';
                    }
                    else
                    {
                        $scope.lesson_approvaled_data[i].row_color='White'
                    }
                }


                info['lesson_user'] = user;

                $http.post(ENV.apiUrl + "api/syllabus_lesson/syllabus_Topic_Sub_Topics_lesson_Approve", info).then(function (res) {
                    if (res.data.length > 0) {
                        $scope.Approve_plan = res.data;

                        $scope.busy = false;
                    } else {
                        swal({ text: 'Data Not Found', width: 320, showCloseButton: true });
                        $scope.busy = false;
                    }
                });
            }

            $scope.Cancel_Approve_data = function () {

                $scope.edt['sims_grade_code'] = '';
                $scope.edt['sims_section_code'] = '';
                $scope.edt['sims_subject_code'] = '';
                $scope.edt['sims_syllabus_start_date'] = '';
                $scope.edt['sims_syllabus_end_date'] = '';

            }

            $scope.Reset_All_Data = function () {

                $scope.Approve_plan = [];
            }

            $scope.Approved_lesson_plan_data = function (info) {

                info['sims_syllabus_lesson_plan_approval_date'] = $scope.doc_date;
                info['sims_syllabus_lesson_plan_approved_by'] = user;
                info['sims_syllabus_lesson_plan_status'] = 'P';

                var section_code = info.sims_section_code.split(',');
                var data1 = [];
                for (var i = 0; i < section_code.length - 1; i++) {

                    var data = {
                        sims_cur_code: info.sims_cur_code
                    , sims_academic_year: info.sims_academic_year
                    , sims_grade_code: info.sims_grade_code
                    , sims_section_code: section_code[i]
                    , sims_subject_code: info.sims_subject_code
                    , sims_syllabus_code: info.sims_syllabus_code
                    , sims_syllabus_lesson_plan_code: info.sims_syllabus_lesson_plan_code
                    , sims_syllabus_lesson_plan_approval_date: info.sims_syllabus_lesson_plan_approval_date
                    , sims_syllabus_lesson_plan_approved_by: info.sims_syllabus_lesson_plan_approved_by
                    , sims_syllabus_lesson_plan_approval_remark: info.sims_syllabus_lesson_plan_approval_remark
                    , sims_syllabus_lesson_plan_status: info.sims_syllabus_lesson_plan_status
                    , sims_syllabus_lesson_plan_created_by: info.sims_syllabus_lesson_plan_created_by
                        , sims_month_code: info.sims_month_code
                    };
                    data1.push(data);

                }

                $http.post(ENV.apiUrl + "api/syllabus_lesson/CUD_syllabus_Topic_Sub_Topics_lesson_Approve", data1).then(function (res) {
                    if (res.data == true) {
                        $scope.lesson_approval_data = [];
                        $scope.Approve_plan = [];
                        swal({ text: 'Lesson Plan Approved Successfully',imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                    }
                    else {
                        swal({ text: 'Lesson Plan Not Approved. ' ,imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    }
                });

            }

            $scope.Reject_lesson_plan_data = function (info) {


                info['sims_syllabus_lesson_plan_approval_date'] = $scope.doc_date;
                info['sims_syllabus_lesson_plan_approved_by'] = user;
                info['sims_syllabus_lesson_plan_status'] = 'R';
                var section_code = info.sims_section_code.split(',');
                var data1 = [];
                for (var i = 0; i < section_code.length - 1; i++) {

                    var data = {
                        sims_cur_code: info.sims_cur_code
                    , sims_academic_year: info.sims_academic_year
                    , sims_grade_code: info.sims_grade_code
                    , sims_section_code: section_code[i]
                    , sims_subject_code: info.sims_subject_code
                    , sims_syllabus_code: info.sims_syllabus_code
                    , sims_syllabus_lesson_plan_code: info.sims_syllabus_lesson_plan_code
                    , sims_syllabus_lesson_plan_approval_date: info.sims_syllabus_lesson_plan_approval_date
                    , sims_syllabus_lesson_plan_approved_by: info.sims_syllabus_lesson_plan_approved_by
                    , sims_syllabus_lesson_plan_approval_remark: info.sims_syllabus_lesson_plan_approval_remark
                    , sims_syllabus_lesson_plan_status: info.sims_syllabus_lesson_plan_status
                    , sims_syllabus_lesson_plan_created_by: info.sims_syllabus_lesson_plan_created_by
                        , sims_month_code: info.sims_month_code
                    };
                    data1.push(data);

                }


                $http.post(ENV.apiUrl + "api/syllabus_lesson/CUD_syllabus_Topic_Sub_Topics_lesson_Approve", data1).then(function (res) {
                    if (res.data == true) {
                        info = {};
                        swal({ text: 'Lesson Plan Rejected.',imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                    }
                    else {
                        swal({ text: 'Lesson Plan Not Rejected. ' ,imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    }
                });

            }

            $scope.New = function () {

                main =   document.getElementById('mainchk');
                main.checked = false;

                $scope.edt = {};
                $scope.savebtn = true;
                $scope.Grid = false;
                $scope.edt['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.getacyr($scope.curriculum[0].sims_cur_code);

                $scope.Check_Multiple_Record();
            }

            $scope.Unit_table = false;

            $scope.Edit = function (info) {
                debugger
                var main1 =   document.getElementById('mainchkunit');
                if (main1.checked == true)
                {
                    main1.checked = false;
                }

                if (info.unitvalues == 'New') {

                     
                    $scope.savebtndisunit = false;
                    
                    $scope.savebtn = true;
                    $scope.disabled1 = true;
                    $scope.unit["sims_syllabus_unit_name"] = '';
                    $scope.unit["sims_syllabus_unit_short_desc"] = '';

                } else {
                    $scope.disabled1 = false;
                    $scope.Unit_table = true;
                    $scope.edt['sims_section_code1'] = '';
                    var section_code = '';
                    for (var i = 0; i < $scope.edt.sims_section_code.length; i++) {
                        section_code = section_code + $scope.edt.sims_section_code[i] + ',';
                    }

                    $scope.edt['sims_section_code1'] = section_code;

                    $scope.edt['lesson_user'] = user;
                    $scope.syllabus_Units = [];
                    $http.post(ENV.apiUrl + "api/syllabus_lesson/syllabus_Unit", $scope.edt).then(function (res) {
                        if (res.data !== null) {
                            $scope.syllabus_Units = res.data;
                            $scope.getunitsname();
                        }
                        else {
                            swal({ text: 'Sorry Data Not Found', imageUrl: "assets/img/close.png" });
                        }

                    });

                }

            }

            $scope.Submit = function (isvalid) {
                if (isvalid) {
                    var data1 = [];
                    debugger


                    for (var i = 0; i < $scope.edt.sims_section_code.length; i++) {
                        var data = {};
                        data.sims_cur_code = $scope.edt.sims_cur_code;
                        data.sims_academic_year = $scope.edt.sims_academic_year;
                        data.sims_grade_code = $scope.edt.sims_grade_code;
                        data.sims_section_code = $scope.edt.sims_section_code[i];
                        data.sims_subject_code = $scope.edt.sims_subject_code;
                        data.sims_syllabus_code = $scope.edt.sims_syllabus_code;
                        data.sims_month_code = $scope.edt.sims_month_code;
                        data.sims_syllabus_start_date = $scope.edt.sims_syllabus_start_date;
                        data.sims_syllabus_end_date = $scope.edt.sims_syllabus_end_date;
                        data.sims_syllabus_remark = $scope.edt.sims_syllabus_remark;
                        data.sims_syllabus_creation_date = $scope.edt.sims_syllabus_creation_date;
                        data.sims_syllabus_created_by = $scope.edt.sims_syllabus_created_by;
                        data.sims_syllabus_status = $scope.edt.sims_syllabus_status;
                        data.opr = 'I';

                        data1.push(data);

                    }

                    $http.post(ENV.apiUrl + "api/syllabus_lesson/CUDsyllabuslesson", data1).then(function (res) {
                        if (res.data == true) {

                            swal({ text: 'Lesson Plan Created Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });

                            $http.get(ENV.apiUrl + "api/syllabus_lesson/Get_syllabus_lesson").then(function (res) {
                                if (res.data !== null) {
                                    $scope.Grid = true;
                                    $scope.obj = res.data;
                                    $scope.totalItems = $scope.obj.length;
                                    $scope.todos = $scope.obj;
                                    $scope.makeTodos();
                                }
                                else {
                                    swal({ text: 'Sorry Data Not Found',imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                                }

                            });

                        }
                        else {
                            swal({ text: 'Lesson Plan Not Created. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                        }

                    });
                }
            }

            $scope.Update = function () {

                $scope.edt['opr'] = 'U';
                $http.post(ENV.apiUrl + "api/syllabus_lesson/CUDUpdatesyllabuslesson", $scope.edt).then(function (res) {
                    if (res.data == true) {
                        swal({ text: 'Lesson Plan Updated Successfully', imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                        $scope.Getsyllabus();
                    }
                    else if (res.data == false) {
                        swal({ text: 'Lesson Plan Not Updated. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                    }
                    else {
                        swal("Error-" + res.data)
                    }
                });
            }

            $scope.DeleteRecord = function () {

                $scope.deleteobject = [];

                for (var i = 0; i < $scope.Get_syllabus_lesson.length; i++) {
                    if ($scope.Get_syllabus_lesson[i].syllabus_status == true) {
                        $scope.Get_syllabus_lesson[i].opr = 'D';
                        $scope.deleteobject.push($scope.Get_syllabus_lesson[i]);
                    }
                }

                if ($scope.deleteobject.length > 0) {


                    swal({
                        text: "Are you sure,you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/syllabus_lesson/CUDeletesyllabuslesson", $scope.deleteobject).then(function (res) {
                                if (res.data == true) {
                                    swal({ text: 'Record Deleted Successfully',imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                                    var main =   document.getElementById('mainchk');
                                    if (main.checked == true) {
                                        main.checked = false;
                                    }
                                    $scope.Getsyllabus();

                                }
                                else if (res.data == false) {
                                        swal({ text: 'Record Not Deleted. ',imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                                    for (var i = 0; i < $scope.Get_syllabus_lesson.length; i++) {
                                        $scope.Get_syllabus_lesson[i].ischecked = false;
                                    }
                                }
                                else {
                                    swal("Error-" + res.data)
                                }
                            });
                        } 
                    });
                }
                else {
                    swal({ text: 'Select Atleast One Record To Delete', imageUrl: "assets/img/notification-alert.png",width: 320, showCloseButton: true });
                }
            }

            $scope.DeleteRecordunit = function () {

                $scope.deleteobject = [];

                for (var i = 0; i < $scope.syllabus_Units.length; i++) {

                    if ($scope.syllabus_Units[i].syllabus_status_unit == true) {
                        $scope.deleteobject.push($scope.syllabus_Units[i]);

                    }
                }

                if ($scope.deleteobject.length > 0) {

                    $http.post(ENV.apiUrl + "api/syllabus_lesson/CUDeletesyllabuslessonUnit", $scope.deleteobject).then(function (res) {
                        if (res.data == true) {
                            swal({ text: 'Record Deleted Successfully', imageUrl: "assets/img/check.png",width: 320, showCloseButton: true });
                            
                            $scope.Edit($scope.edt);
                        }
                        else if (res.data == false) {
                                swal({ text: 'Record Not Deleted. ' , imageUrl: "assets/img/close.png",width: 320, showCloseButton: true });

                            for (var i = 0; i < $scope.syllabus_Units.length; i++) {

                                if ($scope.syllabus_Units[i].syllabus_status_unit == true) {
                                    $scope.syllabus_Units[i].syllabus_status_unit = false;

                                }
                            }
                        }
                        else {
                            swal("Error-" + res.data)
                        }
                    });

                } else {

                    swal({ text: 'Select Atleast One Unit To Delete',imageUrl: "assets/img/notification-alert.png", width: 320, showCloseButton: true });
                }

            }

            $scope.DeleteRecordunitTopic = function () {

                $scope.deleteobject = [];

                for (var i = 0; i < $scope.syllabus_uint_topic_name.length; i++) {

                    if ($scope.syllabus_uint_topic_name[i].syllabus_status_topic == true) {
                        $scope.deleteobject.push($scope.syllabus_uint_topic_name[i]);
                    }
                }


                if ($scope.deleteobject.length > 0) {

                    $http.post(ENV.apiUrl + "api/syllabus_lesson/CUDeletesyllabuslessonUnitTopic", $scope.deleteobject).then(function (res) {
                        if (res.data == true) {
                            swal({ text: 'Record Deleted Successfully',imageUrl: "assets/img/check.png", width: 320, showCloseButton: true });
                            $scope.Get_Existing_Topic_Name();
                        }
                        else if (res.data == false) {
                                swal({ text: 'Record Not Deleted. ' ,imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                            for (var i = 0; i < $scope.syllabus_uint_topic_name.length; i++) {

                                if ($scope.syllabus_uint_topic_name[i].syllabus_status_topic == true) {
                                    $scope.syllabus_uint_topic_name[i].syllabus_status_topic = false;
                                }
                            }
                        }
                        else {
                            swal("Error-" + res.data)
                        }
                    });

                } else {

                    swal({ text: 'Select Atleast One Topic To Delete',imageUrl: "assets/img/notification-alert.png", width: 320, showCloseButton: true });
                }

            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            var dom;
            $scope.flag = true;

            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                "<table class='inner-table1' cellpadding='5' cellspacing='0'  style='width:100%;border:solid;border-width:02px'>" +
                "<tbody>" +
                "<tr> <td class='semi-bold'>" + "Created By" + "</td> <td class='semi-bold'>" + "Remark" + " </td><td class='semi-bold'>" + "Syllabus Start Date" + "</td><td class='semi-bold'>" + "Syllabus End Date" + "</td></tr>" +
                "<tr> <td>" + (info.sims_syllabus_created_by) + "</td> <td>" + (info.sims_syllabus_remark) + " </td><td>" + (info.sims_syllabus_start_date) + "</td><td>" + (info.sims_syllabus_end_date) + "</td></tr>" +

                " </table></td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    //$(dom).remove();
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }
            }

            $scope.SelectAllUnit = function (info) {

                if (info.unit_lst.length > 0) {
                    for (var i = 0; i < info.unit_lst.length; i++) {
                        if (info.unit_lst[i].unit_topic.length == 0) {
                            info.unit_lst[i].unit_topic = [];
                        }

                        for (var k = 0; k < info.unit_lst[i].unit_topic.length; k++) {
                            if (info.ischecked == true) {
                                info.unit_lst[i].ischecked = true;
                                info.unit_lst[i].unit_topic[k].ischecked = true;

                            }
                            else {
                                info.unit_lst[i].ischecked = false;
                                info.unit_lst[i].unit_topic[k].ischecked = false;
                            }
                        }
                    }
                }
            }

            $scope.print = function (div) {
                debugger;
                var docHead = document.head.outerHTML;
                var printContents =   document.getElementById(div).outerHTML;
                var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";

                var newWin = window.open("", "_blank", winAttr);
                var writeDoc = newWin.document;
                writeDoc.open();
                writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                writeDoc.close();
                newWin.focus();
            }

            $scope.SelectMultipleRecords = function () {
                var main =   document.getElementById('mainchk');
                for (var i = 0; i < $scope.Get_syllabus_lesson.length; i++) {
                    if (main.checked == true) {

                        $scope.Get_syllabus_lesson[i].syllabus_status = true;
                    }
                    else {
                        $scope.Get_syllabus_lesson[i].syllabus_status = false;

                    }
                }
            }

            $scope.gethidesyllabus = function () {

                $scope.savebtndissyll = false;
                $scope.edt['sims_section_code1'] = '';
                var section_code = '';
                for (var i = 0; i < $scope.edt.sims_section_code.length; i++) {
                    section_code = section_code + $scope.edt.sims_section_code[i] + ',';
                }

                $scope.edt['sims_section_code1'] = section_code;

                $scope.edt["lesson_user"] = user;

                $http.post(ENV.apiUrl + "api/syllabus_lesson/syllabus_lesson", $scope.edt).then(function (res) {
                    $scope.Get_syllabus_lesson = res.data;

                    $scope.getunitsname();
                });
            }

            $scope.getunitsname = function () {

                $http.post(ENV.apiUrl + "api/syllabus_lesson/syllabus_lesson_uint_name", $scope.edt).then(function (res) {
                    $scope.syllabus_uint_name = res.data;
                });
            }

            $scope.Unit_topic_table = false;

            $scope.Get_Existing_Topic_Name = function (info) {

                var main1 =   document.getElementById('mainchktopic');
                if (main1.checked == true)
                {
                    main1.checked = false;
                }

                if (info == 'New') {
                   
                    $scope.savebtndistopic = false;
                    $scope.unitdisabled = true;
                    $scope.topic["sims_syllabus_unit_topic_name"] = '';
                    $scope.topic["sims_syllabus_unit_topic_short_desc"] = '';
                    $scope.savebtn = true;
                }
                else {
                    $scope.unitdisabled = false;
                    $scope.savebtn = false;
                    $scope.Unit_topic_table = true;
                    $http.post(ENV.apiUrl + "api/syllabus_lesson/syllabus_Topic", $scope.edt).then(function (res) {
                        $scope.syllabus_uint_topic_name = res.data;
                    });
                }
            }

            $scope.SelectMultipleRecordsUnit = function () {
                var main1 =   document.getElementById('mainchkunit');

                for (var i = 0; i < $scope.syllabus_Units.length; i++) {
                    if (main1.checked == true) {
                        $scope.syllabus_Units[i].syllabus_status_unit = true;

                    } else {

                        $scope.syllabus_Units[i].syllabus_status_unit = false;
                    }
                }
            }

            $scope.SelectMultipleRecordsUnitTopic = function () {
                var main2 =   document.getElementById('mainchktopic');
                for (var i = 0; i < $scope.syllabus_uint_topic_name.length; i++) {
                    if (main2.checked == true) {
                        $scope.syllabus_uint_topic_name[i].syllabus_status_topic = true;
                    }
                    else {
                        $scope.syllabus_uint_topic_name[i].syllabus_status_topic = false;
                    }
                }
            }

        }]);

})();



