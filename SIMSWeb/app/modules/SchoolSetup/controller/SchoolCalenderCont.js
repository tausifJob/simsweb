﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SchoolCalenderController',
      ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
          //  swal("Student Fee Details Updated Successfully!", "success")
          //   swal({ title: "Alert", text: "Fee frequency does not match with the periods", imageUrl: "assets/img/notification-alert.png", });
          //swal({ title: "Are you sure?", text: "You will not be able to recover this imaginary file!", type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, delete it!", cancelButtonText: "No, cancel plx!", closeOnConfirm: false, closeOnCancel: false }, function (isConfirm) { if (isConfirm) { swal("Deleted!", "Your imaginary file has been deleted.", "success"); } else { swal("Cancelled", "Your imaginary file is safe :)", "error"); } });

          $scope.eventsList = [];
          $scope.event_object = [];
          $scope.display = true;
          $scope.btn_delete = false;
          $scope.check_time_flag = false;
          $http.get(ENV.apiUrl + "api/calender/getCalender").then(function (res) {
              $scope.display = true;
              $scope.obj = res.data;
              $scope.eventsList = [];
              //  debugger
              for (var i = 0; i < res.data.length; i++) {
                  var event = [];
                  event.sims_calendar_date = res.data[i].sims_calendar_date,
                  event.start = res.data[i].start,
                  event.end = res.data[i].end,
                  event.title = res.data[i].title,
                  event.sims_calendar_event_short_desc = res.data[i].sims_calendar_event_short_desc,
                  event.description = res.data[i].sims_calendar_event_desc,
                  event.id = res.data[i].id,
                  event.category = res.data[i].sims_calendar_event_category,
                  event.category_desc = res.data[i].sims_calendar_event_category_desc,
                  event.color = res.data[i].sims_event_color,
                  event.start_time = res.data[i].sims_calendar_event_start_time,
                  event.end_time = res.data[i].sims_calendar_event_end_time,
                  event.event_type = res.data[i].sims_calendar_event_type,
                  event.event_priority = res.data[i].sims_calendar_event_priority,
                  event.event_status = res.data[i].sims_calendar_event_status,
                  event.event_time_marker = res.data[i].sims_calendar_time_marker,
                  event.event_status = res.data[i].sims_calendar_event_status,
                  //event.textColor = 'black'; 
                  $scope.eventsList.push(event);
              }

              //console.log($scope.eventsList);
              $('#calendar').fullCalendar("removeEvents");
              $('#calendar').fullCalendar('addEventSource', $scope.eventsList);
              $('#calendar').fullCalendar('refetchEvents');
          })

          $http.get(ENV.apiUrl + "api/calender/getCategorize").then(function (res) {
              $scope.Categorize_obj = res.data;
          });

          $http.get(ENV.apiUrl + "api/calender/getTimeMarkers").then(function (res) {
              $scope.Timemarkers_obj = res.data;
          });

          $http.get(ENV.apiUrl + "api/calender/getEventRepeatParam").then(function (res) {
              $scope.EventRepeatParam = res.data;
              if($scope.EventRepeatParam.length>0){
                  $scope.event_object['sims_appl_parameter'] = $scope.EventRepeatParam[0].sims_appl_parameter;
              }
          });


          $scope.update_flag = false;

          $(document).ready(function () {
              $('#calendar').fullCalendar({
                  header: {
                      left: 'prev,next,today',
                      center: 'title',
                      right: 'month,basicWeek,basicDay'
                  },


                  formatDate: 'yyyy-MM-dd',
                  eventClick: function (event, jsEvent, view) {

                      debugger

                      //   $('#calendar').fullCalendar('removeEvents', event.id);
                      $scope.update_flag = true;
                      $scope.btn_delete = true;
                      $scope.check_time_flag = true;
                      var data = {};
                      data['id'] = event.id;
                      data['sims_calendar_number'] = event.id;
                      //if (moment(event.sims_calendar_date).format('DD') <= 12) {

                      var check_date = moment(event.start).format('DD-MM-YYYY');
                    //  var check_date = moment(event.start, 'YYYY/MM/DD').format('DD-MM-YYYY')
                      data['sims_calendar_date'] = check_date;
                      //}
                      //else
                      data['sims_calendar_date_end_date'] = check_date;
                      
                      //  data['sims_calendar_date'] = event.sims_calendar_date

                      data['start'] = event.start;
                      data['end'] = event.end;
                      data['title'] = event.title;
                      data['sims_calendar_event_short_desc'] = event.sims_calendar_event_short_desc,
                      data['sims_calendar_event_desc'] = event.description;
                      data['sims_calendar_event_category'] = event.category;
                      data['category_desc'] = event.category_desc;
                      // data['color'] = event.color;
                      data['sims_calendar_event_start_time'] = event.start_time;
                      data['sims_calendar_event_end_time'] = event.end_time;
                      data['sims_calendar_event_type'] = event.event_type;
                      data['sims_calendar_event_priority'] = event.event_priority;
                      data['event_status'] = event.event_status;
                      data['sims_calendar_time_marker'] = event.event_time_marker;
                      data['sims_calendar_event_status'] = event.event_status;
                      data['sims_calendar_event_priority1'] = event.event_status;

                      $scope.event_object = (data);
                      console.log(data);
                      console.log($scope.event_object);

                      $('#modalTitle').html(event.title);
                      //  $('#modalBody').html(event.description);
                      $('#label1').html(event.category_desc);
                      // $('#txt_subject').html(event.sims_calendar_event_short_desc);

                      var v = document.getElementById('label1');
                      v.style.backgroundColor = event.color;
                      v.style.color = 'black';

                      $("#Checkbox2").trigger("click");
                      $('#fullCalModal').modal({ backdrop: "static" });

                  },


                  defaultDate: '2015-02-02',
                  editable: false,
                  eventLimit: true,

                  events: $scope.eventsList

              })

          })

          $scope.EventDeleteById = function () {
              swal({
                  title: "Are you sure?", text: "You will not be able to recover this Event!",
                  type: "warning", showCancelButton: true, confirmButtonColor: "#DD6B55",
                  confirmButtonText: "Yes, delete it!", cancelButtonText: "No, cancel plx!",
                  closeOnConfirm: false, closeOnCancel: false
              }).then(function (isConfirm) {

                  if (isConfirm) {
                      var posdata;
                      $http.post(ENV.apiUrl + "api/calender/EventDeleteById", posdata = $scope.event_object).then(function (res) {
                          $scope.result = res.data;
                          console.log($scope.result);
                          $('#fullCalModal').modal('hide');
                          if ($scope.result == true) {
                              $scope.update_flag = false;
                              swal("Deleted!", "Your Event has been deleted.", "success");
                              $scope.eventsList = [];
                              $http.get(ENV.apiUrl + "api/calender/getCalender").then(function (res) {
                                  $scope.display = true;
                                  $scope.obj = res.data;
                                  //  debugger
                                  $scope.eventsList = [];
                                  //  debugger
                                  for (var i = 0; i < res.data.length; i++) {
                                      var event = [];
                                      event.sims_calendar_date = res.data[i].sims_calendar_date,
                                      event.start = res.data[i].start,
                                      event.end = res.data[i].end,
                                      event.title = res.data[i].title,
                                      event.sims_calendar_event_short_desc = res.data[i].sims_calendar_event_short_desc,
                                      event.description = res.data[i].sims_calendar_event_desc,
                                      event.id = res.data[i].id,
                                      event.category = res.data[i].sims_calendar_event_category,
                                      event.category_desc = res.data[i].sims_calendar_event_category_desc,
                                      event.color = res.data[i].sims_event_color,
                                      event.start_time = res.data[i].sims_calendar_event_start_time,
                                      event.end_time = res.data[i].sims_calendar_event_end_time,
                                      event.event_type = res.data[i].sims_calendar_event_type,
                                      event.event_priority = res.data[i].sims_calendar_event_priority,
                                      event.event_status = res.data[i].sims_calendar_event_status,
                                      event.event_time_marker = res.data[i].sims_calendar_time_marker,
                                      event.event_status = res.data[i].sims_calendar_event_status,
                                      //event.textColor = 'black'; 
                                      $scope.eventsList.push(event);
                                  }

                                  //console.log($scope.eventsList);
                                  $('#calendar').fullCalendar("removeEvents");
                                  $('#calendar').fullCalendar('addEventSource', $scope.eventsList);
                                  $('#calendar').fullCalendar('refetchEvents');
                              })

                          }
                          else if ($scope.result == false) {
                              swal({ title: "Alert", text: "Event Can not Delete. " , imageUrl: "assets/img/notification-alert.png", });
                          }
                          else {
                              swal("Error-" + $scope.result)
                          }
                      }, function () {
                          swal({  text: "Internal Server Error", imageUrl: "assets/img/notification-alert.png", });
                      });
                  }
                  else {
                      swal("Cancelled", "Your imaginary file is safe :)", "error");
                      $('#fullCalModal').modal('hide');
                  }
              });


          }

          $scope.CancelClear1 = function () {
              $scope.user = angular.copy($scope.event_object)
              $scope.Myform.$setPristine();
              $scope.Myform.$setValidity();
              $scope.Myform.$setUntouched();
              console.log($scope.event_object);
          }

          $scope.TimeMarkersdemo = function (tm) {
              debugger
              for (var i = 0; i < $scope.Categorize_obj.length; i++) {
                  if (tm == $scope.Categorize_obj[i].sims_appl_parameter) {
                      var c = $scope.Categorize_obj[i].color_code;
                      $('#label1').html($scope.Categorize_obj[i].sims_appl_form_field_value1);
                      var v = document.getElementById('label1');
                      v.style.backgroundColor = c;
                      v.style.color = 'black';
                      break;
                  }
              }
          }

          $scope.checktime = function (min, max) {
              
              console.log(min);
              console.log(max);
              $scope.check_time_flag = true;
              if (max != '') {
                  if (min >= max) {
                      swal({ text: "Please Select Correct Time", imageUrl: "assets/img/notification-alert.png", });
                 
                      $scope.event_object.sims_calendar_event_end_time=''
                      $scope.check_time_flag = false;
                  }
              }
          }



          $scope.getAddEventModal = function () {
              $scope.btn_delete = false;
              $scope.check_time_flag = true;
              $scope.event_object = "";
              $('#modalTitle').html('Add Event');
              $('#modalBody').html('');
              // $('#label1').html(event.category_desc);
              var v = document.getElementById('label1');
              v.style.backgroundColor = '';
              v.style.color = '';
              $('#fullCalModal').modal({ backdrop: "static" });
              $scope.update_flag = false;
              $scope.event_object = {
                  sims_calendar_date: moment(new Date()).format('DD-MM-YYYY'),
                  sims_calendar_date_end_date: moment(new Date()).format('DD-MM-YYYY'),
                  sims_calendar_event_status:true
              }
          }

          $scope.AddEventByDate = function (isvalid) {
              
              // console.log($scope.event_object);
              if (isvalid) {
                  if ($scope.update_flag == true && $scope.check_time_flag == true) {
                      $scope.check_time_flag = false;
                      console.log($scope.event_object);
                      var posdata;
                      $http.post(ENV.apiUrl + "api/calender/EventUpdateByDate", posdata = $scope.event_object).then(function (res) {
                          $scope.result = res.data;
                          console.log($scope.result);
                          $('#fullCalModal').modal('hide');
                          if ($scope.result == true) {
                              $scope.update_flag = false;
                              swal("Event Updated Successfully!", "success")
                              $scope.eventsList = [];
                              $http.get(ENV.apiUrl + "api/calender/getCalender").then(function (res) {
                                  $scope.display = true;
                                  $scope.obj = res.data;
                                  //  debugger
                                  $scope.eventsList = [];
                                  //  debugger
                                  for (var i = 0; i < res.data.length; i++) {
                                      var event = [];
                                      event.sims_calendar_date = res.data[i].sims_calendar_date,
                                      event.start = res.data[i].start,
                                      event.end = res.data[i].end,
                                      event.title = res.data[i].title,
                                      event.sims_calendar_event_short_desc = res.data[i].sims_calendar_event_short_desc,
                                      event.description = res.data[i].sims_calendar_event_desc,
                                      event.id = res.data[i].id,
                                      event.category = res.data[i].sims_calendar_event_category,
                                      event.category_desc = res.data[i].sims_calendar_event_category_desc,
                                      event.color = res.data[i].sims_event_color,
                                      event.start_time = res.data[i].sims_calendar_event_start_time,
                                      event.end_time = res.data[i].sims_calendar_event_end_time,
                                      event.event_type = res.data[i].sims_calendar_event_type,
                                      event.event_priority = res.data[i].sims_calendar_event_priority,
                                      event.event_status = res.data[i].sims_calendar_event_status,
                                      event.event_time_marker = res.data[i].sims_calendar_time_marker,
                                      event.event_status = res.data[i].sims_calendar_event_status,
                                      //event.textColor = 'black'; 
                                      $scope.eventsList.push(event);
                                  }

                                  //console.log($scope.eventsList);
                                  $('#calendar').fullCalendar("removeEvents");
                                  $('#calendar').fullCalendar('addEventSource', $scope.eventsList);
                                  $('#calendar').fullCalendar('refetchEvents');
                              })

                          }
                          else if ($scope.result == false) {
                              swal({ title: "Alert", text: "Event Can not Update. " , imageUrl: "assets/img/notification-alert.png", });
                          }
                          else {
                              swal("Error-" + $scope.result)
                          }
                      }, function () {
                          swal({ title: "Alert", text: "Internal Server Error", imageUrl: "assets/img/notification-alert.png", });
                      });
                  }
                  if ($scope.update_flag == false && $scope.check_time_flag == true) {
                      $scope.data = [];
                      debugger
                      var start_date = new Date();
                      start_date = moment($scope.event_object.sims_calendar_date, 'DD-MM-YYYY').format('YYYY/MM/DD')
                      //start_date = moment($scope.event_object.sims_calendar_date).format('YYYY/MM/DD');
                      var end_date = new Date();
                      //end_date = moment($scope.event_object.sims_calendar_date_end_date).format('YYYY/MM/DD');
                      end_date = moment($scope.event_object.sims_calendar_date_end_date, 'DD-MM-YYYY').format('YYYY/MM/DD')

                      var check_date = start_date;
                      $scope.event_object['sims_calendar_date'] = check_date;

                      $scope.obj = angular.copy($scope.event_object);
                      $scope.data.push($scope.obj)
                      debugger
                      for (; new Date(check_date) < new Date(end_date) ;) {
                          //  check_date = moment(check_date)
                          //  check_date.add(1, 'days');
                          check_date = moment(check_date, "YYYY/MM/DD").add('days', 1);
                          check_date = moment(check_date).format('YYYY/MM/DD');
                          $scope.event_object['sims_calendar_date'] = check_date;
                          $scope.obj = angular.copy($scope.event_object);
                          $scope.data.push($scope.obj)

                      }

                      $scope.check_time_flag = false;
                      console.log($scope.data);


                      $http.post(ENV.apiUrl + "api/calender/AddEventByDate", $scope.data).then(function (res) {
                          $scope.result = res.data;
                          console.log($scope.result);
                          $('#fullCalModal').modal('hide');
                          if ($scope.result == true) {
                              swal("Event Added Successfully!", "success")
                              $scope.eventsList = [];
                              $http.get(ENV.apiUrl + "api/calender/getCalender").then(function (res) {
                                  $scope.display = true;
                                  $scope.obj = res.data;
                                  //  debugger
                                  $scope.eventsList = [];
                                  //  debugger
                                  for (var i = 0; i < res.data.length; i++) {
                                      var event = [];
                                      event.sims_calendar_date = res.data[i].sims_calendar_date,
                                      event.start = res.data[i].start,
                                      event.end = res.data[i].end,
                                      event.title = res.data[i].title,
                                      event.sims_calendar_event_short_desc = res.data[i].sims_calendar_event_short_desc,
                                      event.description = res.data[i].sims_calendar_event_desc,
                                      event.id = res.data[i].id,
                                      event.category = res.data[i].sims_calendar_event_category,
                                      event.category_desc = res.data[i].sims_calendar_event_category_desc,
                                      event.color = res.data[i].sims_event_color,
                                      event.start_time = res.data[i].sims_calendar_event_start_time,
                                      event.end_time = res.data[i].sims_calendar_event_end_time,
                                      event.event_type = res.data[i].sims_calendar_event_type,
                                      event.event_priority = res.data[i].sims_calendar_event_priority,
                                      event.event_status = res.data[i].sims_calendar_event_status,
                                      event.event_time_marker = res.data[i].sims_calendar_time_marker,
                                      event.event_status = res.data[i].sims_calendar_event_status,
                                      //event.textColor = 'black'; 
                                      $scope.eventsList.push(event);
                                  }

                                  //console.log($scope.eventsList);
                                  $('#calendar').fullCalendar("removeEvents");
                                  $('#calendar').fullCalendar('addEventSource', $scope.eventsList);
                                  $('#calendar').fullCalendar('refetchEvents');
                              })

                          }
                          else if ($scope.result == false) {
                              swal({ title: "Alert", text: "Event Can not Add. " , imageUrl: "assets/img/notification-alert.png", });
                          }
                          else {
                              swal("Error-" + $scope.result)
                          }
                      }, function () {
                          swal({ title: "Alert", text: "Internal Server Error", imageUrl: "assets/img/notification-alert.png", });
                      });
                  }
              }
          }


          $scope.date_change = function (str) {
              $scope.event_object['sims_calendar_date_end_date'] = angular.copy(str);
          }


          $('#calendar').fullCalendar("removeEvents");
          //   $('#calendar').fullCalendar('removeEvents', event.id);
          $('.fc-header').hide();

          var currentDate = $('#calendar').fullCalendar('getDate');
          console.log(currentDate);

          $('#calender-current-day').html($.fullCalendar.formatDate(currentDate, "dddd"));
          $('#calender-current-date').html($.fullCalendar.formatDate(currentDate, " MMM yyyy"));
          //  $scope.calday = $.fullCalendar.formatDate(currentDate, "dddd");
          // $scope.caldate = $.fullCalendar.formatDate(currentDate, " MMM yyyy");


          $('#calender-next').click(function () {
              $('#calendar').fullCalendar('next');
              currentDate = $('#calendar').fullCalendar('getDate');
              $('#calender-current-day').html($.fullCalendar.formatDate(currentDate, "dddd"));
              $('#calender-current-date').html($.fullCalendar.formatDate(currentDate, "MMM yyyy"));
          });


          $('#calender-prev').click(function () {
              $('#calendar').fullCalendar('prev');
              currentDate = $('#calendar').fullCalendar('getDate');
              $('#calender-current-day').html($.fullCalendar.formatDate(currentDate, "dddd"));
              $('#calender-current-date').html($.fullCalendar.formatDate(currentDate, " MMM yyyy"));
          });

          $('#change-view-month').click(function () {
              $('#calendar').fullCalendar('changeView', 'month');
          });
          $('#change-view-week').click(function () {
              $('#calendar').fullCalendar('changeView', 'agendaWeek');
          });
          $('#change-view-day').click(function () {
              $('#calendar').fullCalendar('changeView', 'agendaDay');
          });



          $('.clockpicker').clockpicker({
              autoclose: true
          });

          $('*[data-datepicker="true"] input[type="text"]').datepicker({
              todayBtn: true,
              orientation: "top left",
              autoclose: true,
              todayHighlight: true
          });

          $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
              $('input[type="text"]', $(this).parent()).focus();
          });

      }])

})();