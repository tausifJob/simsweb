﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.SchoolSetup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.directive("limitTo", [function () {
        return {
            restrict: "A",
            link: function (scope, elem, attrs) {
                var limit = parseInt(attrs.limitTo);
                angular.element(elem).on("keydown", function () {
                    if (event.keyCode > 47 && event.keyCode < 127) {
                        if (this.value.length == limit)
                            return false;
                    }
                });
            }
        }
    }]);
    simsController.directive('onlyDigits', function () {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function (scope, element, attr, ctrl) {
                function inputValue(val) {
                    if (val) {
                        var digits = val.replace(/[^0-9]/g, '');

                        if (digits !== val) {
                            ctrl.$setViewValue(digits);
                            ctrl.$render();
                        }
                        return parseInt(digits, 10);
                    }
                    return undefined;
                }
                ctrl.$parsers.push(inputValue);
            }
        };
    });


    simsController.controller('CalendarExceptionCont', ['$scope', '$state', '$rootScope', '$timeout', '$compile', 'gettextCatalog', '$http', '$filter', 'ENV',
    function ($scope, $state, $rootScope, $timeout, $compile, gettextCatalog, $http, $filter, ENV) {

        $scope.IsHidden = true;
        $scope.ShowHide = function () {
            $scope.IsHidden = $scope.IsHidden == false ? true : false;
        }

        $scope.status_dis = true;
        $scope.sims_status = true;
        $scope.display = false;
        $scope.emp_stud_status = true; $scope.btn_insert_disabled = false; $scope.btn_update_disabled = true; $scope.btn_delete_disabled = true;
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!

        var yyyy = today.getFullYear();
        $scope.att_date = mm + '/' + dd + '/' + yyyy;

        $scope.sims_cal_status = true;
        var dt = new Date();
        $scope.from_date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
        $scope.to_date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
        $http.get(ENV.apiUrl + "api/calendarexception/getAttendanceCuriculum").then(function (res) {
            $scope.cur_obj = res.data;
            if (res.data.length > 0) {
                $scope.cur_code = res.data[0].sims_cur_code;
                $scope.cur_change(res.data[0].sims_cur_code);
            }
            $scope.display = true;
        });

        $scope.Acdm_yr_change = function () {
            $scope.excp_type = "";
            $http.get(ENV.apiUrl + "api/calendarexception/getExceptionType").then(function (res) {
                $scope.excep_type_obj = res.data;
                $scope.sims_status = false;
            });
        }
        $scope.cur_change = function (cur_code) {
            $http.get(ENV.apiUrl + "api/calendarexception/getAttendanceyear?cur_code=" + cur_code).then(function (res) {
                $scope.acdm_yr_obj = res.data;
                if (res.data.length > 0) {
                    $scope.acdm_yr = res.data[0].sims_academic_year;
                    $scope.Acdm_yr_change();
                }
            });
        }


        $scope.excep_type_change = function (acdm_yr, excep_type, cur_code) {
            debugger;
            $scope.clear(); $scope.status_dis = true;
            $scope.day_value = 1;
            $scope.btn_insert_disabled = false; $scope.btn_update_disabled = true; $scope.btn_delete_disabled = true;
            if (excep_type == 'W') {
                $scope.weekends = true;
                $scope.disabled = true; $scope.status_dis = true;

                for (var i = 0; i < $scope.acdm_yr_obj.length; i++) {
                    if ($scope.acdm_yr_obj[i].sims_academic_year == acdm_yr) {
                        $scope.from_date = moment($scope.acdm_yr_obj[i].sims_academic_year_start_date).format('DD-MM-YYYY');
                        $scope.to_date = moment($scope.acdm_yr_obj[i].sims_academic_year_end_date).format('DD-MM-YYYY');
                        break;
                    }
                }

                $http.get(ENV.apiUrl + "api/calendarexception/getDays").then(function (day) {
                    $scope.day_obj = day.data;
                    console.log($scope.day_obj);
                });
            }
            else {
                $scope.disabled = false;
                $scope.from_date = moment(new Date()).format('DD-MM-YYYY');
                $scope.to_date = moment(new Date()).format('DD-MM-YYYY');
            }

           
            $http.get(ENV.apiUrl + "api/calendarexception/getCalendarException?acdm_yr=" + acdm_yr + "&excp_ty_name=" + excep_type + "&cur=" + cur_code).then(function (res) {
                $scope.exception_details_obj = res.data;
                console.log($scope.exception_details_obj);
                $timeout(function () {
                    $("#fixTable").tableHeadFixer({ "left": 1 });
                }, 100);
            });

        }

        var day_names_list = [];
        $scope.weekend_click = function (day_code) {

            var v = document.getElementById(day_code);
            if (day_names_list.length < 2) {

                if (v.checked == true) {
                    console.log(day_code);
                    day_names_list.push(day_code);
                    console.log(day_names_list);
                }
                else {
                    console.log(day_code);
                    v.checked = false;

                    var index = day_names_list.indexOf(day_code);
                    console.log(index);

                    if (index > -1) {
                        day_names_list.splice(index, 1);
                    }
                    //del.pop({id:sims_concession_number});
                    console.log(day_names_list);
                }
            }
            else {
                if (v.checked == false) {
                    v.checked = false;

                    var index = day_names_list.indexOf(day_code);
                    console.log(index);

                    if (index > -1) {
                        day_names_list.splice(index, 1);
                    }
                    //del.pop({id:sims_concession_number});
                    console.log(day_names_list);
                }
                v.checked = false;
            }
        }

        $scope.stud_excep_click = function (stud_status) {
            if (stud_status) {
                $scope.grade = true;
                $http.get(ENV.apiUrl + "api/calendarexception/getAllGrades?cur_code=" + $scope.cur_code + "&acdm_yr=" + $scope.acdm_yr).then(function (res) {
                    $scope.grades_obj = res.data;
                    setTimeout(function () {
                        $('#cmb_grade').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }
            else {
                $scope.grade = false;
                $scope.section_code = null;
            }
        }
        $scope.user_group_click = function (str) {
            debugger;
            if (str.includes("03")) {
                $scope.usr_gp_staff = true;
                $http.get(ENV.apiUrl + "api/common/LeaveType/Getleavetype").then(function (res) {
                    $scope.stafftype_data = res.data;
                    setTimeout(function () {
                        $('#cmb_staff_type').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }
            else {
                $scope.usr_gp_staff = false;
            }
        }

        $scope.user_group_click1 = function (str) {
            debugger;
            if (str.includes("03")) {
                $scope.usr_gp_staff1 = true;
                $http.get(ENV.apiUrl + "api/common/LeaveType/Getleavetype").then(function (res) {
                    $scope.stafftype_data = res.data;
                    setTimeout(function () {
                        $('#cmb_staff_type').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }
            else {
                $scope.usr_gp_staff1 = false;
            }
        }

        $scope.emp_excep_click = function (emp_status) {
            if (emp_status) {
                $scope.usr_gp = true;
                $scope.usr_gp_staff = false;
                $http.get(ENV.apiUrl + "api/calendarexception/getUserGroup").then(function (res) {
                    $scope.user_group_obj = res.data;
                    setTimeout(function () {
                        $('#cmb_user_grp').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
               
            }
            else {
                $scope.usr_gp = false;
                $scope.usr_gp_staff = false;
                $scope.user_group = null;
            }
        }

        $scope.calendar_status_click = function (status) {

            if (status) {
                $scope.status_dis = false;
            } else {
                $scope.status_dis = true;
            }

        }
        $scope.dummy_cnt = 0;

        $scope.grade_code_click = function (grd_code) {

            setTimeout(function () {
                //console.log(grd_code); 
                $scope.grade = true;
                $scope.dummy_cnt = $scope.dummy_cnt + 1;
                if ($scope.dummy_cnt == 2) {
                    $http.get(ENV.apiUrl + "api/calendarexception/getSectionCodes?cur_code=" + $scope.cur_code + "&acdm_yr=" + $scope.acdm_yr + "&grd_code_list=" + grd_code).then(function (res) {
                        $scope.section_obj = res.data;
                        console.log($scope.section_obj);
                        $scope.dummy_cnt = 0;
                        setTimeout(function () {
                            $('#cmb_sec').change(function () {
                                console.log($(this).val());
                            }).multipleSelect({
                                width: '100%'
                            });
                        }, 1000);
                    });
                }
            }, 500);
        }

        $scope.section_code_click = function (sec_code) {
        }

        $scope.user_group = ""; $scope.section_code = "";

        var convertDate = function (usDate) {
            //  var dateParts = usDate.split(/(\d{1,2})\/(\d{1,2})\/(\d{4})/);
            var d = usDate.replace(/-/g, "/");
            return d.split("/").reverse().join("-");
        }

        $scope.btn_insert_click = function () {
            debugger;
            setTimeout(function () {
                $(document).ready(function () {
                    $("#fromdate, #cex_to_date").kendoDatePicker({
                        format: "dd-MM-yyyy",
                        value: ''
                    });
                });
            }, 200);
            var isvalid = true;
            if ($scope.excp_type != "" && $scope.excp_type && $scope.from_date != "" && $scope.from_date && $scope.to_date != "" && $scope.to_date && $scope.sims_color_code != "" && $scope.sims_color_code) {
                $scope.msg1 = false;
            if ($scope.student_excp_flag == true && $scope.section_code.length == 0) {
                isvalid = false;
                alert("Please Select Sections for Student");
            }
            if ($scope.emp_excep_flag == true && $scope.user_group.length == 0) {
                isvalid = false;
                alert("Please Select User Group for Emplyoee");
            }
            if ($scope.excp_type == 'W' && day_names_list.length == 0) {
                isvalid = false;
                alert("Please Select Weekend Days");
            }
            // alert(isvalid);
            if (isvalid) {
                var insert_data = {};
                var staff_type = [];
                try {
                    for (var i = 0; i < $scope.staff_code.length; i++) {
                        if (i == 0) {
                            staff_type = $scope.staff_code[i];
                        }
                        else {
                            staff_type = staff_type + "," + $scope.staff_code[i];
                        }
                    }
                } catch (e) {

                }

                insert_data['sims_cur_code'] = $scope.cur_code;
                insert_data['sims_academic_year'] = $scope.acdm_yr;
                insert_data['sims_calendar_exception_type'] = $scope.excp_type;
                insert_data['sims_from_date'] = $scope.from_date;
                insert_data['sims_to_date'] = $scope.to_date;
                insert_data['sims_calendar_exception_description'] = $scope.excep_desc;
                insert_data['sims_calendar_exception_day_value'] = $scope.day_value;
                insert_data['sims_calendar_exception_instruction_flag'] = $scope.excep_insrt_flag;
                insert_data['sims_calendar_exception_attendance_flag'] = $scope.excep_att_flag;
                insert_data['sims_status'] = $scope.sims_cal_status;
                insert_data['sims_calendar_exception_color_code'] = $scope.sims_color_code;
                insert_data['sims_calendar_exception_student_flag'] = $scope.student_excp_flag;
                insert_data['sims_calendar_exception_emp_flag'] = $scope.emp_excep_flag;
                insert_data['sims_section_code_list'] = $scope.section_code.toString();
                insert_data['weekendNames'] = day_names_list.toString();
                insert_data['sims_employee_group'] = $scope.user_group.toString();
                insert_data['sims_calendar_exception_staff_type'] = staff_type;
                console.log(insert_data);
                var str = $scope.section_code.toString();
                var notInsert = [];
                for (var i = 0; i < $scope.exception_details_obj.length; i++) {
                    if ($scope.exception_details_obj[i].sims_academic_year == $scope.acdm_yr && $scope.exception_details_obj[i].sims_calendar_exception_description == $scope.excep_desc && $scope.exception_details_obj[i].sims_calendar_exception_type_name == $scope.excp_type && $scope.exception_details_obj[i].sims_from_date == $scope.from_date && $scope.exception_details_obj[i].sims_to_date == $scope.to_date) {
                        notInsert = 'Not';
                    }
                }
                if (notInsert != 'Not') {
                    $http.post(ENV.apiUrl + "api/calendarexception/insertCalendarException", insert_data).then(function (day) {
                        $scope.insert_result = day.data;
                        if ($scope.insert_result) {
                            swal("Calendar Exception Details Inserted Successfully!", "success")
                            $http.get(ENV.apiUrl + "api/calendarexception/getCalendarException?acdm_yr=" + $scope.acdm_yr + "&excp_ty_name=" + $scope.excp_type).then(function (res) {
                                $scope.exception_details_obj = res.data;
                                console.log($scope.exception_details_obj);
                                $timeout(function () {
                                    $("#fixTable").tableHeadFixer({ "left": 1 });
                                }, 100);
                            });
                        }
                        else
                            swal({ text: "Error While Inserting Calendar Exception Details", imageUrl: "assets/img/notification-alert.png", });
                        console.log($scope.day_obj);
                    });
                }
                else {
                    swal({ text: "Error Already Present Details In Calendar Exception", imageUrl: "assets/img/notification-alert.png", });
                }

            }

        }
        else {
                    $scope.msg1 =

                        swal(
                        {
                            showCloseButton: true,
                            text: 'Please select Exception Type, From Date, To Date and Color',
                            imageUrl: "assets/img/notification-alert.png",
                            width: 350,
                            showCloseButon: true
                        });
                        
    }
        }

        $scope.update_click = function (update_data) {
            setTimeout(function () {
                $(document).ready(function () {
                    $("#fromdate, #cex_to_date").kendoDatePicker({
                        format: "dd-MM-yyyy",
                        value: ''
                    });
                });
            }, 200);
            $scope.weekends = false; $scope.usr_gp = false; $scope.grade = false;
            $scope.emp_stud_status = false; $scope.btn_insert_disabled = true; $scope.btn_update_disabled = false; $scope.btn_delete_disabled = false;
            $scope.acdm_yr = update_data.sims_academic_year;
            $scope.excep_type = update_data.sims_calendar_exception_type;
            $scope.excep_desc = update_data.sims_calendar_exception_description;
            $scope.from_date = update_data.sims_from_date;
            $scope.to_date = update_data.sims_to_date;
            $scope.day_value = update_data.sims_calendar_exception_day_value;
            $scope.excep_insrt_flag = update_data.sims_calendar_exception_instruction_flag;
            $scope.excep_att_flag = update_data.sims_calendar_exception_attendance_flag;
            $scope.sims_color_code = update_data.sims_calendar_exception_color_code;
            $scope.student_excp_flag = update_data.sims_calendar_exception_student_flag;
            $scope.emp_excep_flag = update_data.sims_calendar_exception_emp_flag;
            $scope.sims_cal_status = update_data.sims_status;

            console.log(update_data)
            upd_data['org_from_date'] =$scope.from_date// convertDate($scope.from_date);
            upd_data['org_to_date'] = $scope.to_date //= convertDate(update_data.sims_to_date);
            upd_data['sims_calendar_exception_number'] = update_data.sims_calendar_exception_number;
        }
        var upd_data = {};
        $scope.btn_update_click = function () {
            upd_data['sims_academic_year'] = $scope.acdm_yr;
            upd_data['sims_calendar_exception_type'] = $scope.excep_type;
            upd_data['sims_calendar_exception_description'] = $scope.excep_desc;
            upd_data['sims_calendar_exception_day_value'] = $scope.day_value;
            upd_data['sims_calendar_exception_instruction_flag'] = $scope.excep_insrt_flag;
            upd_data['sims_calendar_exception_attendance_flag'] = $scope.excep_att_flag;

            upd_data['sims_calendar_exception_color_code'] = $scope.sims_color_code;
            upd_data['sims_status'] = $scope.sims_cal_status;





            console.log(convertDate($scope.from_date));

            upd_data['sims_from_date'] = $scope.from_date //convertDate($scope.from_date);
            upd_data['sims_to_date'] = $scope.to_date;
            console.log(upd_data);
            $http.post(ENV.apiUrl + "api/calendarexception/calendarExceptionUpdate", upd_data).then(function (day) {
                $scope.updte_result = day.data;
                if ($scope.updte_result) {
                    swal("Calendar Exception Details Updated Successfully!", "success")
                    $http.get(ENV.apiUrl + "api/calendarexception/getCalendarException?acdm_yr=" + $scope.acdm_yr + "&excp_ty_name=" + $scope.excp_type).then(function (res) {
                        $scope.exception_details_obj = res.data;
                        console.log($scope.exception_details_obj);
                        $timeout(function () {
                            $("#fixTable").tableHeadFixer({ "left": 1 });
                        }, 100);
                    });
                }
                else
                    swal({ text: "Error While Updating Calendar Exception Details", imageUrl: "assets/img/notification-alert.png", });
                console.log($scope.updte_result);
            });
        }

        //$scope.btn_delete_click = function () {
        //    // upd_data['sims_calendar_exception_number']
        //    $http.post(ENV.apiUrl + "api/calendarexception/calendarExceptionDelete", upd_data).then(function (day) {
        //        $scope.delete_result = day.data;
        //        if ($scope.delete_result) {
        //            swal("Calendar Exception Details Deleted Successfully!", "success")
        //            $http.get(ENV.apiUrl + "api/calendarexception/getCalendarException?acdm_yr=" + $scope.acdm_yr + "&excp_ty_name=" + $scope.excp_type).then(function (res) {
        //                $scope.exception_details_obj = res.data;
        //                console.log($scope.exception_details_obj);
        //                $timeout(function () {
        //                    $("#fixTable").tableHeadFixer({ "left": 1 });
        //                }, 100);
        //            });
        //        }
        //        else
        //            swal({ title: "Alert", text: "Error While Deleting Calendar Exception Details", imageUrl: "assets/img/notification-alert.png", });

        //        console.log($scope.updte_result);
        //    });
        //}



        $scope.check_all = function () {
            for (var i = 0; i < $scope.exception_details_obj.length; i++) {

                if ($scope.obj_chk_all) {
                    $scope.exception_details_obj[i]['sims_calendar_exception_number_status'] = true;
                    // $scope.delete_obj = { sims_calendar_exception_number: $scope.exception_details_obj[i].sims_calendar_exception_number }
                    //  $scope.delete_lst.push($scope.delete_obj);
                }
                else {
                    $scope.exception_details_obj[i]['sims_calendar_exception_number_status'] = false;

                }
            }
        }

        $scope.btn_delete_click = function () {
            $scope.delete_lst = [];
            // upd_data['sims_calendar_exception_number']
            for (var i = 0; i < $scope.exception_details_obj.length; i++) {
                if ($scope.exception_details_obj[i].sims_calendar_exception_number_status) {
                    $scope.delete_obj = { sims_calendar_exception_number: $scope.exception_details_obj[i].sims_calendar_exception_number }
                    $scope.delete_lst.push($scope.delete_obj);
                }
            }


            if ($scope.delete_lst.length > 0) {
                $http.post(ENV.apiUrl + "api/calendarexception/calendarExceptionDeleteNew", $scope.delete_lst).then(function (day) {
                    $scope.delete_result = day.data;
                    if ($scope.delete_result) {
                        swal("Calendar Exception Details Deleted Successfully!", "success")
                        $http.get(ENV.apiUrl + "api/calendarexception/getCalendarException?acdm_yr=" + $scope.acdm_yr + "&excp_ty_name=" + $scope.excp_type).then(function (res) {
                            $scope.exception_details_obj = res.data;
                            console.log($scope.exception_details_obj);
                            $timeout(function () {
                                $("#fixTable").tableHeadFixer({ "left": 1 });
                            }, 100);
                        });
                    }
                    else
                        swal({text: "Error While Deleting Calendar Exception Details", imageUrl: "assets/img/notification-alert.png", });

                    console.log($scope.updte_result);
                });
            }
            else {
                swal({ text: "Please select at least one exception", imageUrl: "assets/img/notification-alert.png" });

            }
        }



        $scope.clear = function () {
            $scope.excep_type = "";
            $scope.excep_desc = "";
            $scope.from_date = "";
            $scope.to_date = "";
            $scope.day_value = "";
            $scope.excep_insrt_flag = "";
            $scope.excep_att_flag = "";
            $scope.sims_color_code = "";
            $scope.student_excp_flag = "";
            $scope.emp_excep_flag = "";
            $scope.sims_cal_status = "";
            $scope.emp_stud_status = true;
        }

        $scope.show_total_working_days = function () {
            $('#working_days_modal').modal({ backdrop: "static" });
            $scope.loading = true;
            $http.get(ENV.apiUrl + "api/calendarexception/gettotalWorkingDays").then(function (day) {
                $scope.total_working_dys_obj = day.data;

                $timeout(function () {
                    $("#fixTable1").tableHeadFixer({ "left": 1 });
                }, 100);
                $scope.loading = false;
                console.log($scope.total_working_dys_obj);
            });
        }

        $scope.edit_student_exception = function () {
            $('#edit_stud_excp_modal').modal({ backdrop: "static" });
            $scope.StudentExceptionDetails = "";
            $scope.stud = "";
            $http.get(ENV.apiUrl + "api/calendarexception/getExceptionType").then(function (res) {
                $scope.stud_excep_type_obj = res.data;
            });
        }
        $scope.stud_cur_change = function (cur_code) {

            $http.get(ENV.apiUrl + "api/calendarexception/getAttendanceyear?cur_code=" + cur_code).then(function (res) {
                $scope.stud_acdm_yr_obj = res.data;
            });
        }

        $scope.stud_Acdm_yr_change = function (cur_code, acdm_yr) {
            $http.get(ENV.apiUrl + "api/calendarexception/getStudGrades?cur_code=" + cur_code + "&acdm=" + acdm_yr).then(function (res) {
                $scope.stud_grades_obj = res.data;
            });
        }
        $scope.stud_grade_change = function (cur_code, acdm_yr, grd) {
            $http.get(ENV.apiUrl + "api/calendarexception/getStudSections?cur_code=" + cur_code + "&acdm=" + acdm_yr + "&grd=" + grd).then(function (res) {
                $scope.stud_section_obj = res.data;
                console.log($scope.stud_section_obj);
            });
        }
        $scope.getStud_excp_details = function () {
            var stud_d = {};
            stud_d = $scope.stud;
            if ($scope.stud.sims_cur_code != "" && $scope.stud.sims_cur_code && $scope.stud.sims_academic_year != "" && $scope.stud.sims_academic_year && $scope.stud.sims_grade_code != "" && $scope.stud.sims_grade_code && $scope.stud.sims_section_code != "" && $scope.stud.sims_section_code && $scope.stud.sims_calendar_exception_type != "" && $scope.stud.sims_calendar_exception_type) {
                $scope.msg1 = false;
            $http.post(ENV.apiUrl + "api/calendarexception/studentExceptionDetails", stud_d).then(function (res) {
                $scope.StudentExceptionDetails = res.data;
                $timeout(function () {
                    $("#fixheader2").tableHeadFixer({ "left": 1 });
                }, 100);
                console.log($scope.StudentExceptionDetails)
            });
            }
            else {
                $scope.msg1 =

                    swal(
                    {
                        showCloseButton: true,
                        text: 'All Fields are Mandatory',
                        width: 350,
                        showCloseButon: true
                    });

            }
        }

        $scope.Student_excep_update = function (j) {
            console.log(j);
            var up_data = {};
            up_data = $scope.stud;
            up_data['sims_attendance_date'] = convertDate(j.sims_from_date);
            up_data['sims_status'] = j.sims_status;
            up_data['sims_calendar_exception_number'] = j['sims_calendar_exception_number'];

            $http.post(ENV.apiUrl + "api/calendarexception/studentExceptionDetailsUpdate", up_data).then(function (res) {
                $scope.StudentExceptionDetails_updte_res = res.data;
                console.log($scope.StudentExceptionDetails_updte_res)
            });
        }

        $scope.edit_Emp_exception = function () {
            $('#edit_emp_excp_modal').modal({ backdrop: "static" });
            $scope.EmpExceptionDetails = "";
            $scope.emp = "";
            $http.get(ENV.apiUrl + "api/calendarexception/getUserGroup").then(function (res) {
                $scope.emp_user_group_obj = res.data;
            });
            $http.get(ENV.apiUrl + "api/calendarexception/getExceptionType").then(function (res) {
                $scope.emp_excep_type_obj = res.data;
            });
        }

        $scope.getEmp_excp_details = function (user_grp_code, excp_type, staff_code) {
            debugger;
            if ($scope.emp.comn_user_group_code != "" && $scope.emp.comn_user_group_code && $scope.emp.sims_appl_parameter != "" && $scope.emp.sims_appl_parameter && $scope.emp.staff_code != "" && $scope.emp.staff_code) {
                $scope.msg1 = false;
                $http.get(ENV.apiUrl + "api/calendarexception/getEmplyoeeExceptionDetails?user_grp_code=" + user_grp_code + "&excp_type=" + excp_type + "&staff_code=" + staff_code).then(function (res) {
                $scope.EmpExceptionDetails = res.data;
                console.log($scope.EmpExceptionDetails)
            });
            }
            else {
                $scope.msg1 =

                    swal(
                    {
                        showCloseButton: true,
                        text: 'All Fields are Mandatory',
                        width: 350,
                        showCloseButon: true
                    });

            }
        }

        $scope.emp_excep_deails_update = function (emp) {
            console.log(emp);
            var emp_excp_det_update = emp;
            emp_excp_det_update['sims_employee_group'] = $scope.emp.comn_user_group_code;
            $http.post(ENV.apiUrl + "api/calendarexception/EmplyoeeExceptionDetailsUpdate", emp_excp_det_update).then(function (res) {
                $scope.EmpExceptionDetails_up_res = res.data;
                console.log($scope.EmpExceptionDetails_up_res)
            });
        }

        $scope.btn_cancel_click = function () {
            $scope.status_dis = true; $scope.usr_gp = false; $scope.grade = false; $scope.weekends = false; $scope.excp_type = "";
            $scope.exception_details_obj = ""; $scope.section_code = {}; $scope.user_group = "";

            $scope.btn_insert_disabled = false; $scope.btn_update_disabled = true; $scope.btn_delete_disabled = true;
        }
        var pristineFormTemplate = $('#Myform').html();
        $scope.resetForm = function () {
            $('#Myform').empty().append($compile(pristineFormTemplate)($scope));
        }


        $('*[data-datepicker="true"] input[type="text"]').datepicker({
            todayBtn: true,
            orientation: "top left",
            autoclose: true,
            todayHighlight: true
        });

        $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
            $('input[type="text"]', $(this).parent()).focus();
        });

        //for collapse main menu
        $('body').addClass('grey condense-menu');
        $('#main-menu').addClass('mini');
        $('.page-content').addClass('condensed');
        $rootScope.isCondensed = true;
    }])

})();