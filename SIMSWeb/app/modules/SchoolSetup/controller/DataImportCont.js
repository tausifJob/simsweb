﻿(function () {
    'use strict';
    var sectionlist = [];
    var le = 0;
    var main, strMessage;
    

    var simsController = angular.module('sims.module.SchoolSetup');

    //simsController.run(function ($rootScope, $templateCache) {
    //    $rootScope.$on('$viewContentLoaded', function () {
    //        $templateCache.removeAll();
    //    });
    //});

    simsController.controller('DataImportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', 'XLSXReaderService', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, XLSXReaderService) {

            
            $scope.downloadFormat = function () {
                debugger;
                $scope.url = ENV.apiUrl + "Content/Format/EmployeeList.xlsx";
                window.open($scope.url, '_self', 'location=yes');

            }

            $scope.display = true;
            $scope.save = true;
            $scope.item = [];

            var date = new Date();
            //$scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss');
            $scope.showPreview = false;
            $scope.showJSONPreview = true;
            $scope.json_string = "";

            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {

                // $scope.filesize = true;
                // if ($files) {
                $('#uploadModal').modal({ backdrop: 'static', keyboard: false });

                //}
                $scope.flag = 0;
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                    var i = 0;
                    console.log($files[i].size);

                });
            };
            $scope.errorlabel = false;
            $scope.file_changed = function (element) {
                debugger;
                // SpinnerDialog.show('loading','...');
                $scope.modalbusy = true;
                $scope.busy = true;
                $scope.rgvtbl = false;
                var photofile = element.files[0];
                console.log(element.files[0].size);
                if (element.files[0].size > 600000) {
                    swal('Alert', 'Exceed size limit');
                }
                else {
                    $scope.filename = element.files[0];
                    $scope.photo_filename = (photofile.type);
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $scope.$apply(function () {
                            $scope.prev_img = e.target.result;
                        });
                    };
                    reader.readAsDataURL(photofile);
                    $scope.sheets = [];
                    $scope.excelFile = element.files[0];
                    XLSXReaderService.readFile($scope.excelFile, $scope.showPreview, $scope.showJSONPreview).then(function (xlsxData) {
                        console.log(xlsxData.sheets);
                        var main = document.getElementById('mainchk');
                        if (main.checked == true) {
                            main.checked = false;
                            $scope.filecount = "";
                        }
                        $scope.errorlabel = false;
                        $scope.display = false;
                        $scope.save = false;
                        $scope.employee_number = [];
                        console.log(xlsxData.sheets.Sheet1[0]);
                        var size = Object.keys(xlsxData.sheets.Sheet1[0]).length;
                        console.log(size);
                        if (size <= 8) {
                            $scope.sheets = xlsxData.sheets
                            console.log($scope.sheets);
                            $scope.sheetsData = [];
                            for (var i = 0; xlsxData.sheets.Sheet1.length > i; i++) {
                                $scope.subData = {};
                                //$scope.subData['temp'] = xlsxData.sheets.Sheet1[i].temp;
                                $scope.subData['employee_number'] = xlsxData.sheets.Sheet1[i].employee_number;
                                $scope.subData['company_code'] = xlsxData.sheets.Sheet1[i].company_code;
                                $scope.subData['pay_code'] = xlsxData.sheets.Sheet1[i].pay_code;
                                $scope.subData['effective_from_date'] = xlsxData.sheets.Sheet1[i].effective_from_date;
                                $scope.subData['amount'] = xlsxData.sheets.Sheet1[i].amount;
                                $scope.subData['effective_upto_date'] = xlsxData.sheets.Sheet1[i].effective_upto_date;
                                $scope.subData['remark'] = xlsxData.sheets.Sheet1[i].remark;
                                $scope.sheetsData.push($scope.subData);
                            }

                            $http.post(ENV.apiUrl + "api/DataImportController/DataImport", $scope.sheetsData).then(function (res) {
                                $scope.Pers326_Get_attednce_deatils = res.data;
                                $scope.datasend = [];
                                for (var i = 0; $scope.Pers326_Get_attednce_deatils.length > i; i++) {
                                    if ($scope.Pers326_Get_attednce_deatils[i].employee_number) {
                                        $scope.Pers326_Get_attednce_deatils[i].employee_number['fcolor'] = "black";
                                        $scope.modalbusy = false;
                                    } else {
                                        $scope.Pers326_Get_attednce_deatils[i].employee_number['fcolor'] = "red";
                                        $scope.modalbusy = false;
                                        $scope.errorlabel = true;
                                    }
                                }
                                console.log($scope.Pers326_Get_attednce_deatils);
                            });


                            $scope.sheetName = { sheet: 'Sheet1' };
                            var temp = "a";
                            $("#sheet_name").val(temp);
                        } else {
                            $scope.sheets = "";
                            swal({ title: "Document Upload Failed,Does not match Column" });
                            $scope.busy = false;
                            $scope.modalbusy = false;
                        }
                    });

                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + '/api/file/uploadDocument?filename=' + $scope.filename.name + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss') + "&location=" + "Docs/Student",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    $http(request).success(function (d) {
                        debugger;
                        var data = {
                            sims_admission_user_code: $rootScope.globals.currentUser.username,
                            sims_admission_doc_path: d
                        }
                        $http.post(ENV.apiUrl + "api/document/insertdoc", data).then(function (res) {
                            $scope.ins = res.data;
                            if (res.data) {
                                swal({ title: "Document Uploaded Successfully", imageUrl: "assets/img/check.png" });
                            } else {
                                swal({ title: "Document Upload Failed, Check Connection", imageUrl: "assets/img/close.png" });
                            }
                        });
                    });
                    angular.forEach(
                     angular.element("input[type='file']"),
                     function (inputElem) {
                         angular.element(inputElem).val(null);
                     });
                    //$scope.CheckMultiple();
                }
            };

            var datasend = [];
            $scope.save_data = function () {
                debugger;
                var data = $scope.Pers326_Get_attednce_deatils;
               // data.opr = 'I';
                //datasend.push(data);
                $http.post(ENV.apiUrl + "api/DataImportController/DataImportSave", data).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                    }
                    else {
                        swal({ text: "Record Not Inserted " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                    }

                    $http.get(ENV.apiUrl + "api/DataImportController/DataImport").then(function (res1) {
                        $scope.CreDiv = res1.data;
                        $scope.totalItems = $scope.CreDiv.length;
                        $scope.todos = $scope.CreDiv;
                        $scope.makeTodos();
                    });
                });
                //datasend = [];
                $scope.table = true;
                $scope.display = false;

            }
            $scope.uploadClick = function () {
                formdata = new FormData();
                $scope.modalbusy = false;
                $scope.busy = false;
            }
            $scope.cancelData = function () {
                $('#uploadModal').modal('hide');
                $scope.busy = false;
            }

        }]);

})();