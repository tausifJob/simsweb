﻿(function () {
    'use strict';

    var main, temp, del = [];

    var simsController = angular.module('sims.module.SchoolSetup');
    simsController.controller('GradeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.sms = true;
            var data1 = [];
            $scope.grades = [];
            var deletecode = [];
            $scope.pagesize = "10";
            $scope.pageindex = "0";
            $scope.edit_code = false;
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 25, $scope.maxSize = 5;
            $scope.edt = {};

            $timeout(function () {
                $("#example_wrapper").scrollbar();
                //$(".scroll-wrapper").css({ 'height': '300px' });
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            var user = $rootScope.globals.currentUser.username;

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.countData = [

                  { val: 25, data: 25 },
                  { val: 50, data: 50 },
                  { val: 'All', data: 'All' },

            ]

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };


            $http.get(ENV.apiUrl + "api/ERP/Grade/getCompany").then(function (res) {
                $scope.getCompanyCode = res.data;
                $scope.edt['comp_code'] = $scope.getCompanyCode[0].comp_code;

            });


            $scope.getAcdyr = function (crcode) {
                $http.get(ENV.apiUrl + "api/ERP/Grade/getAcademicYear?curCode=" + crcode).then(function (res) {
                    $scope.acyr = res.data;
                    $scope.edt.grade_academic_year = $scope.acyr[0].grade_academic_year;
                });
            }

            getCur();

            function getCur() {
                $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.curr = res.data;
                    $scope.edt.grade_cur_code = $scope.curr[0].sims_cur_code;
                    $scope.getAcdyr($scope.curr[0].sims_cur_code);
                });
            }
            //function getCur(flag,comp_code) {
            //    if (flag) {


            //        $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
            //            $scope.curr = res.data;
            //            $scope.edt['grade_cur_code'] = $scope.curr[0].sims_cur_code;
            //            $scope.getAcdyr($scope.curr[0].sims_cur_code);
            //        });
            //    }
            //    else {

            //        $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
            //            $scope.curr = res.data;
            //            $scope.edt['grade_cur_code'] = $scope.curr[0].sims_cur_code;
            //            $scope.getAcdyr($scope.curr[0].sims_cur_code);
            //        });


            //    }

            //}


            //$http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
            //    $scope.count = res.data;
            //    if ($scope.count) {
            //        getCur(true, $scope.user_details.comp);

            //    }
            //    else {
            //        getCur(false, $scope.user_details.comp)
            //    }


            //});

            //    getCur();

            $http.get(ENV.apiUrl + "api/ERP/Grade/getGrade").then(function (res) {
                $scope.dis1 = true;
                $scope.dis2 = false;
                $scope.grades = res.data;
                if ($scope.grades.length > 0) {
                    $scope.table = true;
                    $scope.pager = true;
                    if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.grades.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.grades.length, data: 'All' })
                    }
                    $scope.totalItems = $scope.grades.length;
                    $scope.todos = $scope.grades;
                    $scope.makeTodos();
                    $scope.grid = true;
                }
            });


            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edit = function (str) {
                //$scope.New();
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edit_code = true;
                $scope.dis1 = false;
                $scope.dis2 = true;
                if ($scope.count)
                {
                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                        $scope.curr = res.data;
                        $scope.edt['grade_cur_code'] = $scope.curr[0].sims_cur_code;
                        
                    });
                    //$http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                    //    $scope.curr = res.data;
                    //});
                }
                else
                {
                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                        $scope.curr = res.data;
                        $scope.edt['grade_cur_code'] = $scope.curr[0].sims_cur_code;
                        
                    });
                    //getCur();
                    //getCur(false, $scope.user_details.comp)
                    //$http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + str.comp_code).then(function (res) {
                    //    $scope.curr = res.data;
                    //});
                }

                $http.get(ENV.apiUrl + "api/ERP/Grade/getAcademicYear?curCode=" + str.grade_cur_code).then(function (res) {
                    $scope.acyr = res.data;

                    $scope.edt =
                        {
                            grade_cur_code: str.grade_cur_code,
                            grade_academic_year: str.grade_academic_year,
                            grade_code: str.grade_code,
                            grade_name_en: str.grade_name_en,
                            grade_name_ar: str.grade_name_ar,
                            grade_name_fr: str.sgrade_name_fr,
                            grade_name_ot: str.grade_name_ot,
                            sims_grade_status: str.sims_grade_status,
                            comp_code: str.comp_code,
                            sims_display_order: str.sims_display_order
                        }

                });
                // $scope.getAcdyr($scope.edt.grade_cur_code);
            }

            $scope.New = function () {
                $scope.edt = { sims_grade_status: true };//{ 'grade_cur_code': '' };
                $scope.edit_code = false;
                $scope.dis1 = false;
                $scope.dis2 = true;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;


                $http.get(ENV.apiUrl + "api/ERP/Grade/getCompany").then(function (res) {
                    $scope.getCompanyCode = res.data;
                    $scope.edt['comp_code'] = $scope.getCompanyCode[0].comp_code;
                });

                if ($scope.count) {
                    getCur();

                    // console.log($scope.user_details.comp);
                }
                else {
                    getCur()
                    //  $scope.user_details.comp
                }

            }



            $scope.Save = function (isvalidate) {

                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = ({
                            grade_cur_code: $scope.edt.grade_cur_code,
                            grade_academic_year: $scope.edt.grade_academic_year,
                            grade_code: $scope.edt.grade_code,
                            grade_name_en: $scope.edt.grade_name_en,
                            grade_name_ar: $scope.edt.grade_name_ar,
                            grade_name_fr: $scope.edt.grade_name_fr,
                            grade_name_ot: $scope.edt.grade_name_ot,
                            sims_grade_status: $scope.edt.sims_grade_status,
                            sims_display_order: $scope.edt.sims_display_order,
                            comp_code: $scope.edt.comp_code,
                            opr: 'I'
                        });

                        data1.push(data);


                        $http.post(ENV.apiUrl + "api/ERP/Grade/CUDInsertGrade", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            $rootScope.strMessage = $scope.msg1.strMessage;
                            if ($scope.msg1 == true) {
                                swal({ text: "Grade  Added Successfully", showCloseButton: true, imageUrl: "assets/img/check.png", width: 500, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Grade  Already Exists. ", showCloseButton: true, imageUrl: "assets/img/close.png", width: 500, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }

            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/ERP/Grade/getGrade").then(function (res) {
                    $scope.dis1 = true;
                    $scope.dis2 = false;
                    $scope.grades = res.data;
                    if ($scope.grades.length > 0) {
                        $scope.table = true;
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.grades.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.grades.length, data: 'All' })
                        }
                        $scope.totalItems = $scope.grades.length;
                        $scope.todos = $scope.grades;
                        $scope.makeTodos();
                        $scope.grid = true;
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {

                var data1 = [];

                if (isvalidate) {
                    var data = ({
                        grade_cur_code: $scope.edt.grade_cur_code,
                        grade_academic_year: $scope.edt.grade_academic_year,
                        grade_code: $scope.edt.grade_code,
                        grade_name_en: $scope.edt.grade_name_en,
                        grade_name_ar: $scope.edt.grade_name_ar,
                        grade_name_fr: $scope.edt.grade_name_fr,
                        grade_name_ot: $scope.edt.grade_name_ot,
                        sims_grade_status: $scope.edt.sims_grade_status,
                        sims_display_order: $scope.edt.sims_display_order,
                        comp_code: $scope.edt.comp_code,
                        opr: 'U'
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/ERP/Grade/CUDGradeUpdate", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Grade  Updated Successfully", showCloseButton: true, width: 500, imageUrl: "assets/img/check.png" }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: " Grade  Not Updated Successfully.", showCloseButton: true, width: 500, imageUrl: "assets/img/close.png" }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.Delete = function () {
                var deletecode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById("test_" + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'grade_cur_code': $scope.filteredTodos[i].grade_cur_code,
                            'grade_academic_year': $scope.filteredTodos[i].grade_academic_year,
                            'grade_code': $scope.filteredTodos[i].grade_code,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                        imageUrl: "assets/img/notification-alert.png",

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/ERP/Grade/CUDGradeUpdate", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Grade Deleted Successfully", showCloseButton: true, width: 380, imageUrl: "assets/img/check.png", }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });

                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Already Mapped.Can't be Deleted. ", showCloseButton: true, width: 380, imageUrl: "assets/img/close.png", }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }

                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById("test_" + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }

                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
            }

            $scope.cancel = function () {
                $scope.dis1 = true;
                $scope.dis2 = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test_" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test_" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function (info) {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }

            $scope.size = function (str) {
                if (str == 10 || str == 20) {
                    $scope.pager = true;
                }

                else {

                    $scope.pager = false;
                }
                if (str == "*") {
                    $scope.numPerPage = $scope.grades.length;
                    $scope.filteredTodos = $scope.grades;
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                }
                $scope.makeTodos();

            }


            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;

                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.grades, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.grades;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.grade_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.grade_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }



        }]);
})();

