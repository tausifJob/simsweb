(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Incidence');
    var flag = false;
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StudentDetentionTransactionA',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            var loginuser = $rootScope.globals.currentUser.username;
            $scope.emp_search = loginuser;
            $scope.created_user = loginuser;
            $scope.busyindicator = true;

            $scope.datasend = [];
            $scope.leveldata = [];
            $scope.Empdata = [];
            $scope.global_stud_arr = [];
            $scope.global_stud_arr_after_emp = [];
            $scope.user_details;
            console.log("user details" + $scope.user_details);
            $http.get(ENV.apiUrl + "api/StudentDetentionTransactionController/getCuriculum").then(function (res1) {
                $scope.cur_data = res1.data;
                $scope.sims_Cur = $scope.cur_data[0].sims_cur_code;
                $scope.getAcademic_year($scope.cur_data[0].sims_cur_code);
            });

            $scope.getAcademic_year = function (cur_code1) {
                debugger
                $http.get(ENV.apiUrl + "api/StudentDetentionTransactionController/getAcademicYear?curCode=" + cur_code1).then(function (res1) {
                    $scope.acad_data = res1.data;
                    $scope.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                    $scope.getAllGrades();
                    console.log($rootScope.globals.currentUser.username);
                });
            }

            $scope.getAllGrades = function () {
                debugger
                $http.get(ENV.apiUrl + "api/StudentDetentionTransactionController/getAllGrades?cur_code=" + $scope.sims_Cur + "&academic_year=" + $scope.sims_academic_year).then(function (res1) {
                    $scope.grade_data = res1.data;
                    setTimeout(function () {
                        $('#grade_box').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $(function () {
                $('#grade_box').multipleSelect({ width: '100%' });
            });

            $scope.getSectionFromGrade = function (cur_code1, grade_code1, academic_year1) {
                debugger
                $http.get(ENV.apiUrl + "api/StudentDetentionTransactionController/getSectionFromGrade?cur_code=" + $scope.sims_Cur + "&grade_code=" + $scope.sims_grade_code + "&academic_year=" + $scope.sims_academic_year).then(function (res1) {
                    $scope.section_data = res1.data;
                    //  $scope.sims_section_code = $scope.section_data[0].sims_section_code;
                    setTimeout(function () {
                        $('#section_box').change(function () {
                        }).multipleSelect({
                            width: '100%'
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }

            $(function () {
                $('#section_box').multipleSelect({ width: '100%' });
            });

            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.getStudentList = function () {
                $http.get(ENV.apiUrl + "api/StudentDetentionTransactionController/getStudentList?cur_code=" + $scope.sims_Cur + "&acad_year=" + $scope.sims_academic_year + "&grade_code=" + $scope.sims_grade_code + "&section_code=" + $scope.sims_section_code).then(function (res1) {
                    $scope.StudentList = res1.data;
                })
            }

            $scope.checklick = function () {
                $scope.datasend = [];

                for (var i = 0; i < $scope.StudentList.length; i++) {
                    var t = $scope.StudentList[i].enroll_no;
                    var v = document.getElementById(t + i);
                    if (v.checked == true) {
                        $scope.datasend.push($scope.StudentList[i]);
                    }
                }
                console.log('gloab_stud in checkckil', $scope.global_stud_arr_after_emp);
                if ($scope.global_stud_arr_after_emp.length > 0) {
                    for (var j = 0; j <= $scope.global_stud_arr_after_emp.length; j++) {
 //                       if ($scope.global_stud_arr_after_emp[j].enroll_no != '' && $scope.global_stud_arr_after_emp[j].enroll_no != undefined) {
                            $scope.data = {
                                student_name: $scope.global_stud_arr_after_emp[j].student_name,
                                enroll_no: $scope.global_stud_arr_after_emp[j].enroll_no,
                                sims_grade_code: $scope.global_stud_arr_after_emp[j].sims_grade_code,
                                sims_section_code: $scope.global_stud_arr_after_emp[j].sims_section_code,
                                sims_academic_year: $scope.global_stud_arr_after_emp[j].sims_academic_year,
                                sims_cur_code: $scope.global_stud_arr_after_emp[j].sims_cur_code,
                            }
                            $scope.datasend.push($scope.data);
 //                       }
                    }
                }
            }

            $scope.global_student1 = function () {
                console.log($scope.datasend);
            }

            $scope.RemoveEnrollMentNo = function ($event, index, str) {

                str.splice(index, 1);
            }

            $scope.global_student = function () {
                debugger;
                $rootScope.visible_stud = true;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = false;
                $rootScope.visible_Employee = false;
                $rootScope.chkMulti = true;
                $scope.searchglo();
                $scope.global_stud_arr = [];
            }

            $scope.$on('global_cancel', function () {
                $scope.global_stud_arr = $scope.SelectedUserLst;
                console.log('SelectedUserLst', $scope.SelectedUserLst);
                if ($scope.global_stud_arr.length > 0) {
                    for (var j = 0; j < $scope.SelectedUserLst.length; j++) {
                        if ($scope.SelectedUserLst[j].s_enroll_no != '' && $scope.SelectedUserLst[j].s_enroll_no != undefined) {
                            $scope.data = {
                                student_name: $scope.SelectedUserLst[j].name,
                                enroll_no: $scope.SelectedUserLst[j].s_enroll_no,
                                sims_grade_code: $scope.SelectedUserLst[j].grade_code,
                                sims_section_code: $scope.SelectedUserLst[j].section_code,
                                sims_academic_year: $scope.SelectedUserLst[j].sims_academic_year,
                                sims_cur_code: $scope.SelectedUserLst[j].s_cur_code,
                            }
                            $scope.datasend.push($scope.data);
                            $scope.global_stud_arr_after_emp.push($scope.data);
                        }
                    }
                    console.log('gloab_stud', $scope.global_stud_arr_after_emp);
                }
                //if ($scope.StudentList != undefined) {
                //    for (var i = 0; i < $scope.StudentList.length; i++) {
                //        var t = $scope.StudentList[i].enroll_no;
                //        var v = document.getElementById(t + i);
                //        if (v.checked == true) {
                //            $scope.datasend.push($scope.StudentList[i]);
                //        }
                //    }
                //}

            });

            $scope.getEmp = function () {

                // $scope.SelectedUserLst = [];

                $rootScope.visible_stud = false;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = false;
                $rootScope.visible_Employee = true;
                $rootScope.chkMulti = false;

                $scope.searchglo();

                $scope.$on('global_cancel', function () {
                    $scope.Empdata = [];
                    if ($scope.SelectedUserLst.length > 0) {
                        for (var k = 0; k < $scope.SelectedUserLst.length; k++) {
                            $scope.data2 = {
                                em_login_code: $scope.SelectedUserLst[k].em_login_code,
                                empName: $scope.SelectedUserLst[k].empName
                            }
                            $scope.Empdata.push($scope.data2);
                        }
                        if ($scope.Empdata.length > 0) {
                            $scope.emp_search = $scope.Empdata[0].em_login_code;
                        }
                    }

                });

            }

            $scope.searchglo = function () {
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.StudentList.length; i++) {
                        var v = document.getElementById($scope.StudentList[i].enroll_no + i);
                        v.checked = true;
                        $scope.datasend.push($scope.StudentList[i]);
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.StudentList.length; i++) {
                        var v = document.getElementById($scope.StudentList[i].enroll_no + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.datasend = [];
                        $scope.row1 = '';
                    }
                    if ($scope.SelectedUserLst.length > 0) {
                            for (var j = 0; j <= $scope.SelectedUserLst.length; j++) {
                                $scope.data = {
                                    student_name: $scope.SelectedUserLst[j].name,
                                    enroll_no: $scope.SelectedUserLst[j].s_enroll_no,
                                    sims_grade_code: $scope.SelectedUserLst[j].grade_code,
                                    sims_section_code: $scope.SelectedUserLst[j].section_code,
                                    sims_academic_year: $scope.SelectedUserLst[j].sims_academic_year,
                                    sims_cur_code: $scope.SelectedUserLst[j].s_cur_code,
                                }
                                $scope.datasend.push($scope.data);
                            }
                        }

                }
            }
            ///////LEVEL
            $http.get(ENV.apiUrl + "api/StudentDetentionTransactionController/getLevelCodeNew?user=" + $scope.created_user).then(function (res1) {
                $scope.level_data = res1.data;
                $scope.sims_detention_level_code = $scope.level_data[0].sims_detention_level_code;
                setTimeout(function () {
                    $('#level_box').change(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });

            $scope.Load_levels = function () {
                $http.get(ENV.apiUrl + "api/StudentDetentionTransactionController/getLevelCode").then(function (res1) {
                    $scope.level_data = res1.data;
                    $scope.sims_detention_level_code = $scope.level_data[0].sims_detention_level_code;
                    setTimeout(function () {
                        $('#level_box').change(function () {
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }

            $scope.Load_levels_new = function () {
                $http.get(ENV.apiUrl + "api/StudentDetentionTransactionController/getLevelCodeNew?user=" + $scope.created_user).then(function (res1) {
                    $scope.level_data = res1.data;
                    $scope.sims_detention_level_code = $scope.level_data[0].sims_detention_level_code;
                    setTimeout(function () {
                        $('#level_box').change(function () {
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }



            $http.get(ENV.apiUrl + "api/StudentDetentionTransactionController/getStatus").then(function (res1) {
                $scope.status_data = res1.data;
                $scope.sims_appl_parameter = $scope.status_data[0].sims_appl_parameter;
            });

            $(function () {
                $('#level_box').multipleSelect({ width: '100%' });
            });

            $scope.getLevelDesc = function (sims_detention_level_code) {

                $http.get(ENV.apiUrl + "api/StudentDetentionTransactionController/getLevelDesc?sims_detention_level_code=" + sims_detention_level_code).then(function (res1) {
                    $scope.level_desc_data = res1.data;
                })
            }


            $scope.checklick_det_level = function (obj,index) {
               
                var v = document.getElementById(obj.sims_detention_desc_code + index);
                if (v.checked == true) {
                    obj.isChecked = true;

                }
                else {
                    obj.isChecked = false;
                }
              
            }
            
            //$scope.checklick_det_level = function () {
            //    debugger
            //    for (var i = 0; i < $scope.level_desc_data.length; i++) {
            //        var t = $scope.level_desc_data[i].sims_detention_desc_code;
            //        var v = document.getElementById(t + i);
            //        if (v.checked == true) {
            //            $scope.leveldata.push($scope.level_desc_data[i]);
            //        }
            //    }
            //    console.log($scope.leveldata.length);
            //    console.log($scope.leveldata);
            //}


            $scope.InsertIntoDetensionTransaction = function () {
                debugger;

                if ($scope.student_remark == undefined || $scope.student_remark == '')
                {
                    swal({ title: "Alert", text: "Please insert remark", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                     
                }
                else{
                $scope.savedata = [];
                $scope.busyindicator = false;
                $(document).ready(function () {
                    $('#MyModal4').fadeIn('slow');
                });
                

                for (var i = 0; i < $scope.level_desc_data.length; i++) {
                    for (var j = 0; j < $scope.datasend.length; j++) {

                        if ($scope.level_desc_data[i].isChecked==true) {
                            var datasend = {
                                'sims_cur_code': $scope.datasend[j].sims_cur_code,
                                'sims_academic_year': $scope.datasend[j].sims_academic_year,
                                'sims_grade_code': $scope.datasend[j].sims_grade_code,
                                'sims_section_code': $scope.datasend[j].sims_section_code,
                                'enroll_no': $scope.datasend[j].enroll_no,
                                'sims_detention_level_code': $scope.level_desc_data[i].sims_detention_level_code,
                                'sims_detention_desc_code': $scope.level_desc_data[i].sims_detention_desc_code,
                                'sims_detention_description': $scope.level_desc_data[i].sims_detention_description,
                                'sims_detention_point': $scope.level_desc_data[i].sims_detention_point,
                                'sims_detention_remark': $scope.student_remark,
                                'sims_detention_transaction_notified_user_code': $scope.emp_search,
                                'sims_detention_transaction_status': $scope.sims_appl_parameter,
                                'sims_detention_transaction_created_user_code': $scope.created_user,
                                'sims_detention_transaction_updated_user_code': $scope.created_user,
                            }
                            $scope.savedata.push(datasend);
                        }
                        
                    }

                }

                console.log("savedata", $scope.savedata);

               
                $http.post(ENV.apiUrl + "api/StudentDetentionTransactionController/InsertDetention", $scope.savedata).then(function (msg) {
                    $scope.msg1 = msg.data;
                    console.log("msg", msg);

                    if ($scope.msg1 == true) {
                        $(document).ready(function () {
                            $('#MyModal4').fadeOut('fast');
                            $scope.datasend = [];
                        //    $scope.emp_search = [];
                            $scope.global_stud_arr = [];
                            $scope.savedata = [];
                            $scope.StudentList = [];
                            main = document.getElementById('mainchk');
                            main.checked = false;
                            $scope.student_remark = '';
                            $scope.level_desc_data = [];
                            $scope.Load_levels_new();
                        });
                        swal({ title: "Alert", text: "Records Inserted successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        $scope.busyindicator = true;
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        $('#MyModal4').fadeOut('fast');
                        $scope.busyindicator = true;
                    }
                });
             
            }
        }
            $scope.ViewHistory = function (str) {
                debugger;
                $scope.studenthistory = [];
                $scope.studentname = '';
                $scope.studentgrade = '';
                $scope.studentsection = '';
                $scope.studentacademic_year = '';
                $('#MyModal5').modal('show');
                $http.get(ENV.apiUrl + "api/StudentDetentionTransactionController/getStudentHistoy?sims_cur_code=" + str.sims_cur_code + "&sims_academic_year=" + str.sims_academic_year + "&sims_grade_code=" + str.sims_grade_code
                    + "&sims_section_code=" + str.sims_section_code + "&sims_enroll_number=" + str.enroll_no).then(function (res1) {
                        $scope.studenthistory = res1.data;
                        $scope.studentname = $scope.studenthistory[0].enroll_no + ' - ' + $scope.studenthistory[0].student_name;
                        $scope.studentgrade = $scope.studenthistory[0].sims_grade_name_en;
                        $scope.studentsection = $scope.studenthistory[0].sims_section_name_en;
                        $scope.studentacademic_year = $scope.studenthistory[0].sims_academic_year;
                        $scope.sims_academic_year_description = $scope.studenthistory[0].sims_academic_year_description;
                        $scope.enroll_no = $scope.studenthistory[0].enroll_no;
                });
            }

            $scope.Deletetranaction = function (str) {
                debugger;
                var data = [];
                var datasend = [];
                $scope.data = {
                    sims_cur_code:str.sims_cur_code ,
                    sims_academic_year:str.sims_academic_year,
                    sims_grade_code:str.sims_grade_code,
                    sims_section_code:str.sims_section_code,
                    enroll_no: str.enroll_no,
                    sims_detention_level_code:str.sims_detention_level_code,
                    sims_detention_desc_code:str.sims_detention_desc_code
                }
                
                datasend.push($scope.data);
                console.log($scope.datasend);
                $http.post(ENV.apiUrl + "api/StudentDetentionTransactionController/CUDDeletetranaction", datasend).then(function (msg) {
                       $scope.msg1 = msg.data;
                       console.log("msg", msg);

                       if ($scope.msg1 == true) {

                           swal({ title: "Alert", text: "Records Deleted successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                           $scope.ViewHistory(str);
                           $scope.busyindicator = true;
                       }
                       else {
                           $scope.ViewHistory(str);
                           swal({ title: "Alert", text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                       }

                   });
            }



        }])

})();
