﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Incidence');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('IncidenceActionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.pagesize = "10";
         
            $http.get(ENV.apiUrl + "api/incidence/getIncidenceActionByIndex").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.obj = res.data;

                if ($scope.obj.length != 0) {

                    $scope.refresh = $scope.obj;
                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.makeTodos();
                }
                 

            });
         
            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $http.get(ENV.apiUrl + "api/incidence/getAllActionCode1").then(function (ALL_action_code) {
                debugger;
                $scope.all_action_code = ALL_action_code.data;

            });
           
            $http.get(ENV.apiUrl + "api/incidence/getAllActionTypeNames").then(function (All_action_type_name) {
                $scope.All_action_type = All_action_type_name.data;

            });
           
            $http.get(ENV.apiUrl + "api/incidence/getAllActionColorTypeNames").then(function (All_Action_Color_Type_Names) {
                $scope.get_All_Action_Color_Type_Names = All_Action_Color_Type_Names.data;

            });
           
            $http.get(ENV.apiUrl + "api/incidence/getCuriculum").then(function (Get_All_Curr) {
                $scope.get_all_Curr = Get_All_Curr.data;

            });

            $scope.maindata = function () {

                $http.get(ENV.apiUrl + "api/incidence/getIncidenceActionByIndex").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.obj = res.data;
                    $scope.currentPage = 1;
                    if ($scope.obj.length != 0) {

                        $scope.totalItems = $scope.obj.length;
                        $scope.todos = $scope.obj;
                        $scope.makeTodos();
                    }
                    else {
                        swal({ text: 'Sorry Data Not Found', width: 380, showCloseButton: true });
                        $scope.totalItems = $scope.obj.length;
                        $scope.todos = $scope.obj;
                        $scope.makeTodos();
                    }

                });
            }

            $scope.getcur_level = function (str) {
                $http.get(ENV.apiUrl + "api/incidence/getCuriculumLevels?curcode=" + str).then(function (Get_All_Curr_level) {
                    $scope.get_all_Curr_level = Get_All_Curr_level.data;
                });
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {


                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    //$scope.CheckMultiple();
                }
              
            };

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.btn_save = false;
                $scope.btn_update = true;
                $scope.btn_delete = false;
                var data = angular.copy(str);
                $scope.edt = data;

                $scope.getcur_level(str.sims_cur_code);

                if (main.checked == true) {

                    for (var i = 0; i < $scope.obj.length; i++) {
                        var t = $scope.obj[i].sims_action_sub_code;
                        var v = document.getElementById(t);
                        v.checked = false;

                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.size = function (str) {
             
                /*$scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }*/

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }

                if (str == "All") {
                    $scope.currentPage = 1;
                    $scope.numPerPage = $scope.obj.length;
                    $scope.makeTodos();

                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }
            }

            $scope.Update = function () {
               
                var datasend = [];
                var data = '';
                data = $scope.edt;
                data.opr = 'U';
                datasend.push(data);

                $http.post(ENV.apiUrl + "api/incidence/CUDIncidenceActionDetails", datasend).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: 'Record  Updated Successfully', imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                        $scope.display = false;
                        $scope.grid = true;
                        $scope.maindata();
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: 'Record Not Updated. ', imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                        $scope.display = false;
                        $scope.grid = true;
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                });
            }

            $scope.SaveData = function (isvalid) {
                
                if (isvalid) {

                    var datasend = [];
                    var data = '';
                    data = $scope.edt;
                    data.opr = 'I';
                    datasend.push(data);

                    $http.post(ENV.apiUrl + "api/incidence/CUDIncidenceActionDetails", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: 'Record  Inserted Successfully', imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;
                            $scope.maindata();
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: 'Record Not Inserted. ', imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                            $scope.display = false;
                            $scope.grid = true;
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.btn_save = true;
                $scope.btn_update = false;
                $scope.btn_delete = false;
                $scope.edt = "";
                $scope.MyForm.$setPristine();

                $http.get(ENV.apiUrl + "api/incidence/getAutoGenerateIncidenceActionCode").then(function (AutoGenerateIncidenceActionCode) {
                    $scope.AutoGenerateIncidenceAction_Code = AutoGenerateIncidenceActionCode.data;
                    $scope.edt = {
                        sims_action_sub_code: $scope.AutoGenerateIncidenceAction_Code[0].sims_action_sub_code,
                        sims_cur_code: $scope.get_all_Curr[0].sims_cur_code
                    };
                    $scope.getcur_level($scope.edt.sims_cur_code);
                    
                })
                
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;

              
            }

            $scope.Delete = function () {

                    var datasend = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_action_sub_code);
                        if (v.checked == true) {
                            $scope.flag = true;
                            $scope.filteredTodos[i].opr = 'D';
                            datasend.push($scope.filteredTodos[i]);
                        }
                    }

                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) 
                            {
                                $http.post(ENV.apiUrl + "api/incidence/CUDIncidenceActionDetails",datasend).then(function (msg) {

                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: 'Record  Deleted Successfully', imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                                        $scope.display = false;
                                        $scope.grid = true;
                                        $scope.maindata();
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: 'Record Not Deleted. ', imageUrl: "assets/img/close.png", width: 300, showCloseButton: true });
                                        $scope.display = false;
                                        $scope.grid = true;
                                        main = document.getElementById('mainchk');
                                        if (main.checked == true) {
                                            main.checked = false;
                                            $scope.row1 = '';
                                            $scope.color = '#edefef';
                                        }
                                        $scope.multipledelete();
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                })


                                }
                                else {
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                    $scope.row1 = '';
                                }
                                $scope.multipledelete();
                                }
                        })
                        
                        }
                        else {
                        swal({ text: 'Select At Least One Record To Delete', imageUrl: "assets/img/notification-alert.png", width: 380, showCloseButton: true });

                        }


                    }

            $scope.multipledelete = function () { del=[]
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].sims_action_sub_code;
                    var v = document.getElementById(t);
                if (main.checked == true) {
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                else {
                        v.checked = false;
                        main.checked = false;
                        $('tr').removeClass("row_selected");
                    }

                }
                
            }

            $scope.delete_onebyone = function (str) {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_action_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_action_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }
            
        }])
})();