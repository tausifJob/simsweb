﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Incidence');



    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('DetentionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $scope.tempArray = [];
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.user_role_new = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/detension/getUserRoles").then(function (res) {
                    $scope.userData = res.data;
                    setTimeout(function () {
                        $('#cmb_user_role').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }


            //Select Data SHOW
            $scope.maindata = function () {
                debugger
                $scope.user_role_new();
                $http.get(ENV.apiUrl + "api/detension/getDetentionByIndex").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.obj = res.data;

                    $scope.currentPage = 1;
                    if ($scope.obj.length != 0) {
                        $scope.totalItems = $scope.obj.length;
                        $scope.todos = $scope.obj;
                        $scope.grid1 = true;
                        $scope.makeTodos();
                    }
                    else {
                        swal({ text: 'Sorry Data Not Found', width: 380, showCloseButton: true });
                        $scope.grid1 = false;
                    }

                    $scope.active = false;
                    $scope.inactive = false;

                });
            }

            // $scope.user_role_new();
            $scope.maindata();


            $scope.size = function (str) {


                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.obj;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked()

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_detention_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                        item.acno == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {
                debugger
                $scope.user_role_new();
                $scope.disabled = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.divcode_readonly = false;
                $scope.edt = "";

                $scope.temp = {};

            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.edt = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger
                $scope.tempArray = [];
                    $scope.gdisabled = false;
                    $scope.aydisabled = true;
                    $scope.table = false;
                    $scope.display = true;
                    $scope.save_btn = false;
                    $scope.Update_btn = true;
                    $scope.divcode_readonly = true;
                    $scope.edt = str;

              
            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.Save = function (Myform) {
                debugger
                if (Myform) {
                    var data = $scope.edt;
                    data.opr = 'I';
                    data.sims_level_created_user_code = $rootScope.globals.currentUser.username,
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/detension/CUDDetention", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.temp.sims_user_role = '';
                        }
                        else {
                            swal({ text: "Record Not Inserted " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.temp.sims_user_role = '';
                        }

                        $scope.maindata();
                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }

            }

            //DATA UPADATEsize
            var dataforUpdate = [];
            $scope.update = function () {
                debugger
                var data = $scope.edt;
                data.opr = "UU";
                $scope.user_access_role = $scope.temp.sims_user_role[0];
                for (var i = 1; i < $scope.temp.sims_user_role.length; i++) {
                    $scope.user_access_role = $scope.user_access_role + ',' + $scope.temp.sims_user_role[i];
                }
                data.sims_user_access_role = $scope.user_access_role;
                dataforUpdate.push(data);
                //dataupdate.push(data);
                $http.post(ENV.apiUrl + "api/detension/CUDDetention", dataforUpdate).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        $scope.temp.sims_user_role = '';
                    }
                    else {
                        swal({ text: "Not Updated " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        $scope.temp.sims_user_role = '';
                    }

                    $scope.maindata();
                });
                dataforUpdate = [];
                $scope.table = true;
                $scope.display = false;

            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + 5);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + 5);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }


            $scope.OkDelete = function () {
                debugger
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i + 5);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_detention_level_code': $scope.filteredTodos[i].sims_detention_level_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            debugger;
                            $http.post(ENV.apiUrl + "api/detension/CUDDetention", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.maindata();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Can not perform operation as Description is available for Selected Data. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.maindata();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i + 5);
                                if (v.checked == true) {
                                    v.checked = false;
                                    //$scope.row1 = '';
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });


        }])

})();
