﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Incidence');
    var flag = false;
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('DetentionHistoryRptCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            var loginuser = $rootScope.globals.currentUser.username;
            $scope.emp_search = loginuser;
            $scope.created_user = loginuser;
            $scope.busyindicator = true;

            $scope.datasend = [];
            $scope.Empdata = [];
            $scope.global_stud_arr = [];
            $http.get(ENV.apiUrl + "api/DetentionHistoryRptController/getAttendanceCuriculum").then(function (res1) {
                debugger
                $scope.sims_cur = res1.data;
                if (res1.data.length > 0) {
                    $scope.edt = {
                        sims_cur_code: $scope.sims_cur[0].sims_cur_code
                    }

                    $scope.getAcademic_year($scope.sims_cur[0].sims_cur_code);
                }
            });

            $scope.getAcademic_year = function (cur_code1) {
                debugger
                $http.get(ENV.apiUrl + "api/DetentionHistoryRptController/getAttendanceyear?cur_code=" + cur_code1).then(function (res1) {
                    $scope.acad_data = res1.data;
                    $scope.edt.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                    $scope.getAttendancegrade();
                    console.log($rootScope.globals.currentUser.username);
                });
            }

            $scope.getAttendancegrade = function () {
                debugger
                $http.get(ENV.apiUrl + "api/DetentionHistoryRptController/getAttendancegrade?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (res1) {
                    $scope.grade_data = res1.data;
                    setTimeout(function () {
                        $('#cmb_grades').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $(function () {
                $('#cmb_grade').multipleSelect({ width: '100%' });
            });

            $scope.getAttendancesectionTWAR = function () {
                debugger
                $http.get(ENV.apiUrl + "api/DetentionHistoryRptController/getAttendancesectionTWAR?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&grade_code=" + $scope.edt.sims_grade_code).then(function (res1) {
                    $scope.section_data = res1.data;
                    //  $scope.sims_section_code = $scope.section_data[0].sims_section_code;
                    setTimeout(function () {
                        $('#cmb_sections').change(function () {
                        }).multipleSelect({
                            width: '100%'
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }

            $(function () {
                $('#cmb_sections').multipleSelect({ width: '100%' });
            });

            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.getAttendancetableTWAR = function () {
                
                $http.get(ENV.apiUrl + "api/DetentionHistoryRptController/getAttendancetableTWAR?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&grade_code=" + $scope.edt.sims_grade_code + "&sectionCode=" + $scope.edt.sims_section_code).then(function (term_Data_rpt) {
                    $scope.term_Data_rpt = term_Data_rpt.data;
                      //  $scope.busy = false;
                    });
            }

            $scope.report = function (sims_enroll_number) {
                debugger
                $http.get(ENV.apiUrl + "api/DetentionHistoryRptController/getReportName").then(function (res) {
                    $scope.rname = res.data;



                    var data = {
                        //location: 'Sims.SIMR51FeeRec',
                        location: $scope.rname[0].sims_report_name,
                        parameter: {
                            cur_details: $scope.edt.sims_cur_code,
                            acad_year: $scope.edt.sims_academic_year,
                            search:sims_enroll_number

                        },
                        state: 'main.DetHis',
                        ready: function () {
                            this.refreshReport();
                        },
                    }
                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                    $state.go('main.ReportCardParameter')
                });
            }
           

        }])

})();
