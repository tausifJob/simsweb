﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Incidence');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('DetentionSummaryReportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            // Start
            //$scope.name = "Subject Teacher List";
            $scope.temp = {};
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.temp = {};
            $scope.display = false;
            $scope.dublicate = false;

            $scope.getAllGrades = function (cur_code, academic_year) {
                $http.get(ENV.apiUrl + "api/detension/getAllGrades?cur_code=" + cur_code + "&academic_year=" + academic_year).then(function (res1) {
                    $scope.grade_code = res1.data;
                    setTimeout(function () {
                        $('#grade_box').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }

            $scope.getAccYear = function (cur_code) {
                $http.get(ENV.apiUrl + "api/detension/getAcademicYear?curCode=" + cur_code).then(function (res1) {
                    $scope.academic_year = res1.data;
                    if (res1.data.length > 0) {
                        $scope.temp['sims_academic_year'] = res1.data[0].sims_academic_year;
                        $scope.getAllGrades(cur_code, res1.data[0].sims_academic_year)
                    }
                });
            }
            $http.get(ENV.apiUrl + "api/detension/getCuriculum").then(function (res1) {
                $scope.sims_cur = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_cur_code'] = res1.data[0].sims_cur_code;

                    $scope.getAccYear(res1.data[0].sims_cur_code);
                }
            });

            $(function () {
                $('#grade_box').multipleSelect({ width: '100%' });
                $('#section_box').multipleSelect({ width: '100%' });
            });

            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_start_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });


            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_end_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });


            $scope.getSectionFromGrade = function (cur_code, academic_year, grade_code) {
                $http.get(ENV.apiUrl + "api/detension/getSectionFromGrade?cur_code=" + cur_code + "&academic_year=" + academic_year + "&grade_code=" + grade_code).then(function (res1) {
                    $scope.section_code = res1.data;

                    setTimeout(function () {
                        $('#section_box').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }

           

            $scope.DetentionSummaryReport = function () {
                debugger
                $scope.colsvis = false;
                if ($scope.temp.sims_bell_lecture_per_week == undefined) $scope.temp.sims_bell_lecture_per_week = '';
                $http.get(ENV.apiUrl + "api/detension/getDetentionSummaryReport?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&mom_start_date=" + $scope.mom_start_date + "&mom_end_date=" + $scope.mom_end_date).then(function (res1) {
                    if (res1.data.length > 0) {
                        $scope.report_data = res1.data;
                        angular.forEach($scope.report_data, function (f) {
                            f.student_cnt = parseFloat(f.student_cnt);
                        });
                    }
                    else {
                        swal({ title: "Alert", text: " Data is not Available for selected date", showCloseButton: true, width: 300, height: 200 });
                    }
                    //angular.forEach($scope.report_data, function (f) {
                    //    f.sims_bell_lecture_per_week = parseFloat(f.sims_bell_lecture_per_week);
                    //});
                    console.log($scope.report_data);
                });
            }


            $scope.ViewHistory = function (str) {
                debugger;
                $scope.studenthistory = [];
                $scope.studentname = '';
                $scope.studentgrade = '';
                $scope.studentsection = '';
                $scope.studentacademic_year = '';
                $('#MyModal6').modal('show');
                $http.get(ENV.apiUrl + "api/detension/getStudent_Histoy?cur_code=" + str.sims_cur_code + "&academic_year=" + str.sims_academic_year + "&grade_code=" + str.sims_grade_code + "&section_code=" + str.sims_section_code + "&sims_enroll_number=" + str.sims_enroll_number).then(function (res1) {
                        $scope.studenthistory = res1.data;
                        $scope.studentname = $scope.studenthistory[0].student_name;
                        $scope.studentgrade = $scope.studenthistory[0].sims_grade_name_en;
                        $scope.studentsection = $scope.studenthistory[0].sims_section_name_en;
                        $scope.studentacademic_year = $scope.studenthistory[0].sims_academic_year;

                    });
            }

            $scope.suspension = function (temp) {
                debugger
                $('#MyModal5').modal('show');
                var student_name1;
                //$scope.display = true;
                $scope.student_name1 = temp.student_name;
                $scope.class_name1 = temp.class_name;
                $scope.sims_detention_name1 = temp.sims_detention_name;
                $scope.sims_detention_description1 = temp.sims_detention_description;
                $scope.reg_by1 = temp.reg_by;
                $scope.date = temp.date;
                $scope.remark1 = temp.sims_detention_remark;
                $scope.enroll_no = temp.sims_enroll_number;
                $scope.sims_academic_year1 = temp.sims_academic_year;
                $scope.cur_code1 = temp.sims_cur_code;
                $scope.sims_detention_level_code1=temp.sims_detention_level_code;
                $scope.sims_detention_desc_code1 = temp.sims_detention_desc_code;             

            }

            var data_send = [];
            var data1 = [];
            $scope.rem_det = function ()
            {
                debugger;
              
                $scope.data1 = {
                         cur_code:$scope.cur_code1,
                        academic_year:$scope.sims_academic_year1,
                        enroll: $scope.enroll_no,
                        sims_detention_level_code: $scope.sims_detention_level_code1,
                        sims_detention_desc_code: $scope.sims_detention_desc_code1,
                        opr:'T'
                }
                data_send.push($scope.data1);



                $http.post(ENV.apiUrl + "api/detension/remove_detention", data_send).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: " Detention Removed Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                    }
                    else {
                        swal({ text: "Not Remove Detention ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                    }


                });
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

          



            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            //$scope.colsvis = false;


            $scope.exportData = function () {
                //$scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('rpt_data').innerHTML], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                        });
                        saveAs(blob, "DetentionSummaryReport.xls");
                        //$scope.colsvis = false;

                    }

                });
            };

            $scope.print = function (div) {

                //$scope.colsvis = false;

                var docHead = document.head.outerHTML;
                var printContents = document.getElementById('rpt_data').outerHTML;
                //$scope.colsvis = false;
                var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
              

                var newWin = window.open("", "_blank", winAttr);
                var writeDoc = newWin.document;
                writeDoc.open();
                writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                writeDoc.close();
                newWin.focus();

            };

            $scope.Report_call = function (str) {
                debugger
                console.log(str.sims_cur_code, str.sims_academic_year, str.sims_grade_code, str.sims_section_code, str.sims_enroll_number);
                var data = {
                    location: 'Gradebook.SIMR121',
                    parameter: {
                        cur_code: str.sims_cur_code,
                        acad_year: str.sims_academic_year,
                        grade_code: str.sims_grade_code,
                        section_code: str.sims_section_code,
                        serch_student: str.sims_enroll_number
                                            },
                    state: 'main.DSR001',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')
            }

        }])

})();

