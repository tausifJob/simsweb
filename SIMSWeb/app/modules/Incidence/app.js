﻿(function () {
    'use strict';
    angular.module('sims.module.Incidence', [
        'sims',
        'gettext'
    ]);
})();