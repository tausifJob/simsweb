﻿(function () {
    'use strict';
    var obj1, temp, opr;
    var main, del = [];
    var simsController = angular.module('sims.module.Security');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ResetPasswordCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http','$filter','ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http,$filter, ENV) {
            $scope.pagesize = "5";
            $scope.pageindex = 0;

            $scope.operation = true;
           
           
           // $scope.global_Search_click1 = function () {
            $rootScope.visible_stud = false;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = true;
            $rootScope.visible_Employee = false;
            $rootScope.chkMulti = false;
            $scope.edt = {};

           
       //     }
            
            $scope.edit = function (str) {
                $scope.readonly = true;
                $scope.temp = str;
                $scope.opr = 'U';
                $scope.table = false; $scope.display = true; $scope.save_btn = false; $scope.Update_btn = true;

            }

            $scope.Global_Search_modal = function () {
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }

            $http.get(ENV.apiUrl + "api/ResetPassword/GetAll_User_Status").then(function (docstatus2) {
                $scope.User_stat = docstatus2.data;
                console.log($scope.docstatus2);
            });

            //$scope.GetData=function()
            //{
            //    $http.get(ENV.apiUrl + "api/ResetPassword/GetFillAllData?user_code=" + "EP5018").then(function (docstatus2) {
            //        $scope.getData = docstatus2.data;
            //        console.log($scope.getData);
            //        $scope.edt = {
            //            comn_user_name: $scope.getData[0].comn_user_alias,
            //            comn_user_email: $scope.getData[0].comn_user_email,
            //            comn_user_expiry_date: $scope.getData[0].comn_user_expiry_date,
            //            comn_user_captcha_status: $scope.getData[0].comn_user_captcha_status,
            //            comn_reset_user_status_code: $scope.getData[0].comn_reset_user_status_code,
            //            comn_reset_user_status: $scope.getData[0].comn_user_status_name,
            //            comn_user_code: $scope.getData[0].comn_user_code
            //        }
                    
            //    });
            //}


           
            $scope.$on('global_cancel', function ()
            {
                debugger;

                if ($scope.SelectedUserLst.length > 0) {
                
                    debugger
                    $scope.edt =
                        {
                            comn_user_name: $scope.SelectedUserLst[0].name,  //$scope.SelectedUserLst[0].user_name + '-' + 
                             comn_user_email: $scope.SelectedUserLst[0].mail_id,
                             comn_user_expiry_date: $scope.SelectedUserLst[0].user_expiry_date,
                             comn_user_captcha_status: $scope.SelectedUserLst[0].user_captcha_status,
                             comn_reset_user_status_code: $scope.SelectedUserLst[0].user_status,
                    //         comn_reset_user_status: $scope.SelectedUserLst.comn_user_status_name,
                             comn_user_code: $scope.SelectedUserLst[0].user_code
                    }
                   
                }
                //$scope.SelectedUserLst = [];
            });

            $scope.Reset=function()
            {
                $scope.edt = {};
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
              
            }

            $scope.Search=function()
            {
                $scope.global_Search_click();
                debugger;
                $('#Global_Search_Modal').modal({ backdrop: "static" });
              //  $scope.display = true;
                //$scope.SelectedUserLst = [];
            }

            //getCuriculum
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
            });

            // getAcademicYear
            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                });
            }

            //getAllGrades
            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
            }

            //getSectionFromGrade
            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }

            var dataforUpdate = [];
            $scope.Save = function (myForm) {
                debugger;
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = "U";
                    // for (var i = 0; i < $scope.data.length; i++)
                    {
                        if (data.comn_user_password == data.comn_user_Confirmpassword) {
                            dataforUpdate.push(data);
                            //dataupdate.push(data);
                            $http.post(ENV.apiUrl + "api/ResetPassword/CUDResetPassword", dataforUpdate).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1) {
                                    swal({ text: "Password changed successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                }
                                else {
                                    swal({ text: "Password Dose not Matched, Please check..", imageUrl: "assets/img/close.png", width: 350, height: 200 });
                                }
                            });
                            dataforUpdate = [];
                        }
                        else {
                            swal({ text: "Password Dose not Matched, Please check..", imageUrl: "assets/img/close.png", width: 350, height: 200 });

                        }
                    }

                }
            }
         

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
           
            $scope.edt = {
                comn_user_expiry_date: dd + '-' + mm + '-' + yyyy,
                comn_user_captcha_status : true
            }
           
           // $scope.edt.comn_user_captcha_status = true;

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
               // format:"mm/dd/yyyy",
                autoclose: true,
                todayHighlight: true,
                // format: 'yyyy-mm-dd'
                format: 'dd-mm-yyyy'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

           

        }])


   
})();
