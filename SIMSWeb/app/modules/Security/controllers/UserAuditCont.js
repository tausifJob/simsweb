﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Security');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('UserAuditCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "All";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.maindisplay = true;
            $scope.rst_btn = true;
            $scope.sub_btn = true;
            $scope.search_btn = true;
            $scope.parent = false;
            $scope.student = false;
            $scope.buttons = false;
            $scope.temp = [];
            $scope.usrCode = '';
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);
            var user = $rootScope.globals.currentUser.username;
           // var user = $rootScope.globals.currentUser.username;
            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            //$scope.countData = [

            //       { val: 5, data: 5 },
            //       { val: 10, data: 10 },
            //       { val: 15, data: 15 },

            //]

            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();

            //$scope.sdate = yyyy + '-' + mm + '-' + dd;
            //$scope.edate = yyyy + '-' + mm + '-' + dd;
            //$scope.edt = {
            //    from_date: yyyy + '-' + mm + '-' + dd,
            //    to_date: yyyy + '-' + mm + '-' + dd,
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
           

            $scope.UserAuditfun = function (modcode, appcode, sdt, edt, enroll, user) {

                debugger;
                if (sdt == undefined) {
                    $scope.temp['comn_audit_start_time'] = $scope.sdate;//$scope.temp.comn_audit_start_time;
                    $scope.temp['comn_audit_end_time'] = $scope.edate;//$scope.temp.comn_audit_end_time;
                    $scope.apply();
                }
                else {
                   // sdt = $scope.sdate;//$scope.temp.comn_audit_start_time;
                    //edt = edt;//$scope.temp.comn_audit_end_time;
                    $scope.apply();
                }

               // $scope.apply();
                $http.get(ENV.apiUrl + "api/UserAudit/getUserAudit?modulecode=" + modcode + "&applcode=" + appcode + "&startdate=" + $scope.temp['comn_audit_start_time'] + "&enddate=" + $scope.temp['comn_audit_end_time'] + "&comn_user_name=" + $scope.temp.enroll_number).then(function (auditDetails) {
                    $scope.audit_Details = auditDetails.data;
                    if ($scope.audit_Details.length > 0) {
                        $scope.table = true;
                        $scope.pager = true;
                        //if ($scope.countData.length > 3) {
                        //    $scope.countData.splice(3, 1);
                        //    $scope.countData.push({ val: $scope.audit_Details.length, data: 'All' })
                        //}
                        //else {
                        //    $scope.countData.push({ val: $scope.audit_Details.length, data: 'All' })
                        //}
                        $scope.totalItems = $scope.audit_Details.length;
                        $scope.todos = $scope.audit_Details;
                        $scope.makeTodos();
                        $scope.size("All");

                    }
                    else {
                        $scope.table = false;
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                        $scope.filteredTodos = [];
                    }


                    //if ($scope.audit_Details == "")
                    //{ swal({ title: "Alert", text: "Data Not Found", showCloseButton: true, width: 300, height: 200 }); }
                    //$scope.totalItems = $scope.audit_Details.length;
                    //$scope.todos = $scope.audit_Details;
                    //$scope.makeTodos();



                });
                // }
            }

            $scope.clear = function () { $scope.temp = ""; }

            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                $scope.temp[name1] = year + "/" + month + "/" + day;

                if ($scope.temp.comn_audit_end_time < $scope.temp.comn_audit_start_time) {
                    swal({ title: "Alert", text: "End Date Should greater than Start Date", width: 300, height: 200 });
                }
            }

            //$http.get(ENV.apiUrl + "api/UserAudit/getAllUserAudit").then(function (res1) {
            //    $scope.user_Audit = res1.data;
            //    if ($scope.user_Audit.length > 0) {
            //        $scope.table = true;
            //        $scope.pager = true;
            //        if ($scope.countData.length > 3) {
            //            $scope.countData.splice(3, 1);
            //            $scope.countData.push({ val: $scope.user_Audit.length, data: 'All' })
            //        }
            //        else {
            //            $scope.countData.push({ val: $scope.user_Audit.length, data: 'All' })
            //        }
            //        $scope.totalItems = $scope.user_Audit.length;
            //        $scope.todos = $scope.user_Audit;
            //        $scope.makeTodos();
            //    }
            //    else {
            //        $scope.table = false;
            //        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
            //        $scope.filteredTodos = [];
            //    }
            //    //$scope.totalItems = $scope.user_Audit.length;
            //    //$scope.todos = $scope.user_Audit;
            //    //$scope.makeTodos();
            //});

            $http.get(ENV.apiUrl + "api/UserAudit/getModuleName").then(function (modname) {
                $scope.module = modname.data;
                console.log($scope.module);
            });

            $scope.getApplication = function (modcode) {
                $http.get(ENV.apiUrl + "api/UserAudit/getApplicationName?modcode=" + modcode).then(function (appName) {
                    $scope.app_Name = appName.data;
                    console.log($scope.app_Name);
                });
            }

            //$scope.size = function (str) {
            //    if (str == 5 || str == 10 || str == 15) {
            //        $scope.pager = true;
            //    }
            //    else {
            //        $scope.pager = false;
            //    }
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str;
            //    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = "All", $scope.maxSize = "All";

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.audit_Details, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.audit_Details;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.comn_user_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.comn_mod_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.comn_appl_name_en == toSearch) ? true : false;
            }

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;
                console.log($scope.ComboBoxValues);
            });


            $rootScope.visible_stud = false;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = true;
            $rootScope.visible_Employee = false;
            $rootScope.chkMulti = false;

            $scope.SearchSudentModal = function () {
                //$scope.searchtable = false;
                //$scope.buttons = false;
                //$scope.busy = false;
                //$scope.temp = '';
                //$scope.sibling_result = '';
                //$('.nav-tabs a[href="#Student_Details"]').tab('show')
                //$scope.active = 'active';
                //$('#MyModal').modal('show');
                //$scope.searchtable = false;
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            };

            $scope.$on('global_cancel', function () {
                debugger;
                if ($scope.SelectedUserLst.length > 0) {
                    debugger
                    $scope.edt =
                        {
                            empName: $scope.SelectedUserLst[0].user_name,
                            em_number: $scope.SelectedUserLst[0].user_code
                        }
                }
                $scope.temp.enroll_number = $scope.edt.empName;
                $scope.usrCode = $scope.edt.em_number;

            });


            $scope.SearchParentModal = function () {
                $scope.parenttable = false;
                $scope.buttons = false;
                $scope.busy = false;
                $scope.edt = '';
                $scope.parent_result = '';
                $('.nav-tabs a[href="#Parent_information"]').tab('show')
                $('#MyModal').modal('show');
            }

            $scope.SearchParent = function () {
                $scope.parenttable = false;
                $scope.busy = true;
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchParent?data=" + JSON.stringify($scope.edt)).then(function (Allparent) {
                    $scope.parent_result = Allparent.data;
                    $scope.parenttable = true;
                    $scope.busy = false;
                });
            }

            $scope.SearchSudent = function () {
                $scope.searchtable = false;
                $scope.busy = true;
                $scope.sibling_result;
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.temp)).then(function (Allstudent) {
                    $scope.student = Allstudent.data;
                    $scope.searchtable = true;
                    $scope.busy = false;
                    $scope.parenttable = true;
                });
            }

            $scope.DataEnroll = function () {
                for (var i = 0; i < $scope.student.length; i++) {
                    var t = $scope.student[i].s_enroll_no;
                    var v = document.getElementById(t);
                    if (v.checked == true)
                        $scope.temp = {
                            enroll_number: $scope.student[i].s_enroll_no
                        };
                }
            }

            //$('*[data-datepicker="true"] input[type="text"]').datepicker({
            //    todayBtn: true,
            //    orientation: "top left",
            //    autoclose: true,
            //    todayHighlight: true,
            //    format: "yyyy-mm-dd"
            //});
            //$('input[name="StartEndDate"]').daterangepicker({
            //    singleDatePicker: true,
            //    showDropdowns: true,
            //    timePicker: true,
            //    timePickerIncrement: 60,
            //    locale: {
            //        format: 'MM-DD-YYYY h:mm A'
            //    }
            //});

            $('input[name="StartEndDate"]').daterangepicker({
                timePicker: true,
                timePickerIncrement: 30,
                locale: {
                    format: 'DD-MM-YYYY h:mm A'
                }
            });

            $('input[name="StartEndDate"]').daterangepicker({
                timePicker: true,
                timePickerIncrement: 30,
                locale: {
                    format: 'DD-MM-YYYY h:mm A'
                }
            }, function (start, end, label) {
                console.log("A new date selection was made: " + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD'));

                $scope.temp['comn_audit_start_time'] = start.format('DD-MM-YYYY');
                $scope.temp['comn_audit_end_time'] = (end.format('DD-MM-YYYY'));

            });


            $('input[name="StartEndDate"]').on('apply.daterangepicker', function (ev, picker) {

                $scope.temp['comn_audit_start_time'] = picker.startDate.format('DD-MM-YYYY');
                $scope.temp['comn_audit_end_time'] = (picker.endDate.format('DD-MM-YYYY'));
            });


            $scope.apply = function () {

                $('input[name="StartEndDate"]').on('apply.daterangepicker', function (ev, picker) {
                    $scope.temp['comn_audit_start_time'] = picker.startDate.format('DD-MM-YYYY');
                    $scope.temp['comn_audit_end_time'] = (picker.endDate.format('DD-MM-YYYY'));
                });

            }



            $scope.ExportData = function () {
                var check = true;
                debugger;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " UserAuditDetails.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger;
                            var blob = new Blob([document.getElementById('printdata3').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + "UserAuditDetails" + ".xls");
                        }
                    });
                }
                else {
                    swal({ text: "Report Not Save", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, })
                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.size = function (str) {
                debugger;

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }

                if (str == "All") {
                    $scope.currentPage = 1;
                    $scope.numPerPage = $scope.audit_Details.length;
                    $scope.makeTodos();

                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }
            }
        }])

})();
