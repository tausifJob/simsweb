﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Security');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('UserRoleCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.Rolecode = true;
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {
                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            $scope.temp = {};
            $scope.temp['sims_library_membership_type_status'] = true;
            $scope.temp.comn_role_status = true;
           

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            if ($http.defaults.headers.common['schoolId'] == 'staging') {
                $scope.role_code = true;
            }
            // get role code
            $scope.getrolecode = function () {
                $http.get(ENV.apiUrl + "api/UserRole/getUserRoleCode").then(function (res1) {
                    debugger
                    $scope.user_role_code = res1.data;
                    $scope.temp['comn_role_code'] = $scope.user_role_code[0].role_code;
                });
            }
            
            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/UserRole/getUserRole").then(function (res1) {
                debugger
                $scope.user_role = res1.data;
                $scope.totalItems = $scope.user_role.length;
                $scope.todos = $scope.user_role;
                $scope.makeTodos();

                $scope.getrolecode();
            });


            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.user_role;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.user_role, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.user_role;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {

                return (item.comn_role_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.comn_role_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {

                $scope.disabled = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.divcode_readonly = false;
                $scope.temp = {};
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = {};
                $scope.table = true;
                $scope.display = false;
                $scope.cancel_btn = false;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.Rolecode = true;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.getrolecode();

            }

            //DATA EDIT
            $scope.edit = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.table = false;
                    $scope.temp = angular.copy(str);
                    $scope.divcode_readonly = true;
                    $scope.save_btn = false;
                    $scope.Update_btn = true;
                    $scope.cancel_btn = true;
                    $scope.Rolecode = true;
                }
            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger;
                if (Myform) {
                    var data = $scope.temp;
                    data.opr = 'I';
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/UserRole/CUDUserRole", datasend).then(function (msg) {
                        datasend = [];
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();
                        $scope.temp["comn_role_name"] = "";
                        $scope.temp["comn_role_code"] = "";
                        $scope.msg1 = msg.data;
                        $http.get(ENV.apiUrl + "api/UserRole/getUserRole").then(function (res1) {
                            $scope.CreDiv = res1.data;
                            $scope.totalItems = $scope.CreDiv.length;
                            $scope.todos = $scope.CreDiv;
                            $scope.makeTodos();

                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                $scope.getrolecode();
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Role Code already present. ", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }

                        });

                    });

                    //$scope.table = true;
                    //$scope.display = false;

                }

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                if (Myform) {

                    var data = $scope.temp;
                    data.opr = "U";
                    dataforUpdate.push(data);
                    //dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/UserRole/CUDUserRole", dataforUpdate).then(function (msg) {
                        dataforUpdate = [];
                        $scope.Myform.$setPristine();
                        $scope.Myform.$setUntouched();
                        $scope.temp["comn_role_code"] = "";
                        $scope.temp["comn_role_name"] = "";
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({
                                text: "Record Not Updated. ",
                                imageUrl: "assets/img/close.png", width: 300, height: 200
                            });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        //   swal({ text: $scope.msg1.strMessage, timer: 5000 });

                        $http.get(ENV.apiUrl + "api/UserRole/getUserRole").then(function (res1) {
                            $scope.user_role = res1.data;
                            $scope.totalItems = $scope.user_role.length;
                            $scope.todos = $scope.user_role;
                            $scope.makeTodos();
                        });

                    });
                    $scope.table = true;
                    //$scope.table = true;
                    $scope.display = false;
                    $scope.cancel_btn = false;
                    $scope.save_btn = true;
                    $scope.Update_btn = false;
                    $scope.Rolecode = true;
                    $scope.role_code = true;
                }
            }


            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('user-' + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('user-'+i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    debugger;
                    deletefin = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('user-' + i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodulecode = ({
                                'comn_role_code': $scope.filteredTodos[i].comn_role_code,
                                opr: 'D'
                            });
                            deletefin.push(deletemodulecode);
                        }
                    }
                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $http.post(ENV.apiUrl + "api/UserRole/CUDUserRole", deletefin).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/UserRole/getUserRole").then(function (res1) {
                                                    $scope.user_role = res1.data;
                                                    $scope.totalItems = $scope.user_role.length;
                                                    $scope.todos = $scope.user_role;
                                                    $scope.makeTodos();
                                                });
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    {
                                                        $scope.row1 = '';
                                                    }
                                                }
                                                $scope.currentPage = true;
                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Record is Mapped Can't Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/UserRole/getUserRole").then(function (res1) {
                                                    $scope.user_role = res1.data;
                                                    $scope.totalItems = $scope.user_role.length;
                                                    $scope.todos = $scope.user_role;
                                                    $scope.makeTodos();
                                                });
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    {
                                                        $scope.row1 = '';
                                                    }
                                                }
                                                $scope.currentPage = true;
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });
                            }
                            else {
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById('user-' + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    $scope.currentPage = str;
                }
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });


        }])

})();







