﻿(function () {
    'use strict';
    var CurrentDate;
    var day;
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PurchaseReportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV',
            function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {


            var total = 0; var Ltotal = 0; var tot, Ltot; var remain = 0; var tremain; var maxdays;
            var username = $rootScope.globals.currentUser.username;

            $scope.CurrentDate = new Date();
            var today = new Date();
            var grade;
            var section;
            $scope.records = true;
            $scope.tabledata = false;

             //first graph start
            $http.get(ENV.apiUrl + "api/common/Empdashboard/getInventoryDataforOrders").then(function (gethrresult) {    
                $scope.hrms_data1 = gethrresult.data;
                $scope.chartLabels5 = [];
                $scope.chartData5 = [];
                $scope.chartColor = [];
//                var pieChartData = [];
                for (var i = 0; i < $scope.hrms_data1.length; i++) {
                    var color = Math.floor(Math.random() * 16777216).toString(16);
                    color: '#000000'.slice(0, -color.length) + color;
                    $scope.hrms_data1[i].color = color;
                    $scope.chartLabels5.push($scope.hrms_data1[i].service_req);
                    $scope.chartData5.push(parseInt($scope.hrms_data1[i].item_req));
                    $scope.chartColor.push($scope.hrms_data1[i].color);
                }
                var ctx3 = document.getElementById("orderdetail").getContext("2d");
                ctx3.canvas.parentNode.style.height = '230px';
                var data = {
                    labels: ['Item Request', 'Service Request'],
                    datasets: [{
                        data: $scope.chartData5,
                        backgroundColor: ['#5AD3D1', '#FF5A5E']
                    }]
                }
                window.bar1 = new Chart(ctx3, {
                    type: 'pie',
                    data: data,
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        legend: {
                            display: true,
                            labels: {
                                boxWidth: 10,
                                fontSize: 10,
                                position: 'bottom'
                            }
                        },
                        title: {
                            display: true,
                            text: 'Order Detail',
                            position: 'bottom'
                        }
                    }
                });
            });

            // first graph end

            // second graph start
            var d1_1 = []
            var d1_2 = [];
            var d1_3 = [];
            var d1_4 = [];

            $http.get(ENV.apiUrl + "api/common/Empdashboard/getInventoryPendingReceived").then(function (gethrresult1) {
                $scope.order_data1 = gethrresult1.data;
                for (var i = 0; i < $scope.order_data1.length; i++) {
                    if (i < 10) {
                        d1_1.push($scope.order_data1[i].orderedQty)
                        d1_2.push($scope.order_data1[i].receivedQty)
                        d1_3.push($scope.order_data1[i].totalOrderedAmount)
                        d1_4.push($scope.order_data1[i].totalReceivedAmount)
                    }
                }

                var ctxn = document.getElementById("Canvas1").getContext("2d");
                ctxn.canvas.parentNode.style.height = '230px';

               var data = {
                    labels: ["01", "02", "03", "04", "05", "06","07","08","09","10"],
                    datasets: [{
                        label: "Ordered Qty",
                        backgroundColor: "#e74e81",
                        borderColor: "#e74e81",
                        pointBackgroundColor: "#e74e81",
                        data: d1_1
                    }, {
                        label: "Received Qty",
                        backgroundColor: "#4365B0",
                        borderColor: "#4365B0",
                        pointBackgroundColor: "#4365B0",
                        data: d1_2
                    },
                        {
                            label: "Ordered Amt",
                            backgroundColor: "#5AD3D1",
                            borderColor: "#5AD3D1",
                            pointBackgroundColor: "#5AD3D1",
                            data: d1_3
                        },
                       {
                           label: "Received Amt",
                           backgroundColor: "#D00",
                           borderColor: "#D00",
                           pointBackgroundColor: "#D00",
                           data: d1_4
                       }
                    ]
                };


                window.bar7 = new Chart(ctxn, {
                    type: 'bar',
                    data: data,
                    options: {
                        responsive: true,
                        maintainAspectRatio: false,
                        legend: {
                            display: true,
                            labels: {
                                boxWidth: 10,
                                fontSize: 10
                            },
                        },
                        title: {
                            display: true,
                            text: 'Purchase Detail',
                            position: 'bottom'
                        },
                        pointRadius: [15, 10],
                        pointHoverRadius: [40, 40]
                    }
                });
            });

            // second graph end


            //third graph start
            var d2 = [];
            var d1 = [];
            var d3 = [];
            var d4 = [];
            $http.get(ENV.apiUrl + "api/common/Empdashboard/getInventoryOrders").then(function (gethrresult2) {
                $scope.getOrders = gethrresult2.data;
                debugger;
                for (var i = 0; i < gethrresult2.data.length; i++) {
                    d2.push(gethrresult2.data[i].ordered_Count)
                    d1.push(gethrresult2.data[i].received_Count)
                    d3.push(gethrresult2.data[i].total_Order_Count)
                    d4.push(gethrresult2.data[i].month_Name.substring(0, 3) + '-' + gethrresult2.data[i].year)
                }
                var ctn = document.getElementById("orders").getContext("2d");
                ctn.canvas.parentNode.style.height = '230px';

                var data = {
                    labels: d4,
                    datasets: [{
                        label: "Ordered",
                        backgroundColor: "#9467bd",
                        borderColor: "#9467bd",
                        pointBackgroundColor: "#9467bd",
                        data: d2,
                        fill: false,
                    }, {
                        label: "Received",
                        backgroundColor: "#4365B0",
                        borderColor: "#4365B0",
                        pointBackgroundColor: "#4365B0",
                        data: d1,
                        fill: false,
                    },
                     {
                         label: "Total",
                         backgroundColor: "#ff7f0e",
                         borderColor: "#ff7f0e",
                         pointBackgroundColor: "#ff7f0e",
                         data: d3,
                         fill: false,
                     }
                    ]
                };


                window.bar7 = new Chart(ctn, {
                    type: 'line',
                    data: data,
                    options: {
                        responsive: true,
                        showLines:true,
                        maintainAspectRatio: false,
                        legend: {
                            display: true,
                            labels: {
                                boxWidth: 10,
                                fontSize: 10
                            },
                        },
                        title: {
                            display: true,
                            text: 'Orders',
                            position: 'bottom'
                        },
                        pointRadius: [15, 10],
                        pointHoverRadius: [40, 40]
                    }
                });
            });
            // third graph end

            //$http.get(ENV.apiUrl + "api/common/Empdashboard/get_removed_Application_Dash?user=" + $rootScope.globals.currentUser.username).then(function (res) {
            //    for (var i = 0; i < res.data.length; i++) {
            //        var id = '#' + res.data[i].comn_user_appl_code;
            //        $(id).hide();
            //    }
            //});

            //$scope.removeWidget = function (e) {
            //    var id = $(e.target).parents('.remove_widget').attr('id');
            //    $http.post(ENV.apiUrl + "api/common/Empdashboard/remove_tile?user=" + $rootScope.globals.currentUser.username + "&status=" + 'N' + "&appl_code=" + id).then(function (res) {
            //    });

            //    $(e.target).parents('.remove_widget').hide();
            //}


        }])
})();
