﻿
(function () {
    'use strict';
    var main;
    var temp = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    
    simsController.controller('EmployeeLeavePolicyCont',
   ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
       $scope.pagesize = '10';
       $scope.user_data = false;
       $scope.save_btn = false;
       $scope.update_btn = true;
       $scope.beforeTable = true;
       $scope.temp = {};
       $scope.username = $rootScope.globals.currentUser.username;
       $scope.temp.le_leave_code = [];
       $scope.user_access = [];

       $scope.appcode = $state.current.name.split('.');
       var user = $rootScope.globals.currentUser.username;

       $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
           debugger;
           $scope.user_rights = usr_rights.data;

           for (var i = 0; i < $scope.user_rights.length; i++) {

               if (i == 0) {
                   $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                   $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                   $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                   $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                   $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                   $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
               }
           }

           //console.log($scope.user_access);
       });

       $http.get(ENV.apiUrl + "api/common/LeaveType/Get_leave_codes").then(function (res) {
           $scope.leaveCode_data = res.data;        
       });
       $scope.New = function () {
           if ($scope.user_access.data_insert == false) {
               swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
           }
           else {

               $scope.temp = {};
               $scope.user_data = true;
               $scope.beforeTable = false;
               $scope.temp.staff_code = 'Please Select';
               $scope.temp.le_leave_code = 'Please Select';
               $scope.save_btn = false;
               $scope.update_btn = true;
               $scope.temp["pre_holiday"] = true;
               $scope.temp["post_holiday"] = true;
               $scope.temp["pre_weekend"] = true;
               $scope.temp["post_weekend"] = true;
               $scope.temp["comb_holiday_weekend"] = true;
               $scope.temp.pays_empl_leave_policy_status = true;
               try {
                   $('#combinationLeave').multipleSelect('uncheckAll');
               } catch (e) {
               }
           }
       }
       $scope.cancelFunction = function () {       
           $scope.temp.staff_code = $scope.stafftype_data[0].staff_code;
           $scope.temp.le_leave_code = $scope.leaveCode_data[0].le_leave_code;
           $scope.temp.staff_code = 'Please Select';
           $scope.temp.le_leave_code = 'Please Select';
           try {
               $('#combinationLeave').multipleSelect('uncheckAll');
           } catch (e) {
           }
           $scope.comb_leave = [];
           $scope.temp.monthly_allowed = '';
           $scope.temp.termly_allowed = '';
           $scope.temp.yearly_allowed = '';
           $scope.temp.max_leave_application = '';

       }
       $scope.deleteReords = function () {
           if ($scope.user_access.data_delete == false) {
               swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
           }
           else {
               debugger;
               var deletRecordsData = [];
               for (var i = 0; i < $scope.emplleave.length; i++) {
                   var checkValue = document.getElementById("helt-" + i);
                   if (checkValue.checked == true) {
                       deletRecordsData.push($scope.emplleave[i]);
                   }
               }
               if (deletRecordsData.length > 0) {
                   $http.post(ENV.apiUrl + "api/EmployeeLeavePolicyController/IUDLeavePolicy", deletRecordsData).then(function (results) {
                       swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                       $scope.showDataEmploye();
                   });
               }
           }
       }
       $scope.cancel = function () {
           $scope.user_data = false;
           $scope.beforeTable = true;
           $scope.clearFunction();
           $scope.temp.staff_code = 'Please Select';
           $scope.temp.le_leave_code = 'Please Select';
       }
       $http.get(ENV.apiUrl + "api/common/LeaveType/Getleavetype").then(function (res) {
         
           $scope.stafftype_data = res.data;
           console.log($scope.stafftype_data);
       });
       $scope.showDataEmploye = function () {
           $http.get(ENV.apiUrl + "api/EmployeeLeavePolicyController/getallDetails").then(function (get_DriverGender) {
               debugger;
               $scope.emplleave = [];
               $scope.emplleave = get_DriverGender.data;
           });
       }
       $scope.edit = function (f) {
           debugger;
           if ($scope.user_access.data_update == false) {
               swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
           }
           else {
               $scope.save_btn = true;
               $scope.update_btn = false;
               $scope.temp.staff_code = f.staff_type_code;
               $scope.temp.new_le_leave_code = f.leave_code;
               $scope.temp.monthly_allowed = f.monthly_allowed;
               $scope.temp.termly_allowed = f.termly_allowed;
               $scope.temp.yearly_allowed = f.yearly_allowed;
               $scope.temp.max_leave_application = f.max_leave_per_Application;
               $scope.temp.pays_empl_leave_policy_effective_from = f.pays_empl_leave_policy_effective_from;
               $scope.temp.leave_policy_id = f.leave_policy_id;

               console.log("combination_of_leave", f.combination_of_leave);
               var leave_code_split = f.combination_of_leave.split(',');

               $scope.temp.le_leave_code = leave_code_split;

               setTimeout(function () {
                   $("#combinationLeave").multipleSelect("setSelects", $scope.temp.le_leave_code);
               }, 10)

               console.log("le_leave_code", $scope.temp.le_leave_code);
               //for (var i = 0; i < leave_code_split.length; i++) {
               //    $scope.temp.le_leave_code.push(leave_code_split[i]);
               //}
               //$scope.temp.le_leave_code = f.combination_of_leave;

               if (f.is_prefix_holiday_allowed == "Yes") {
                   $scope.temp.pre_holiday = true;
               }
               else {
                   $scope.temp.pre_holiday = false;
               }
               if (f.prefix_weekend_allowed == "Yes") {
                   $scope.temp.pre_weekend = true;
               }
               else {
                   $scope.temp.pre_weekend = false;
               }
               if (f.post_holiday_allowed == "Yes") {
                   $scope.temp.post_holiday = true;
               }
               else {
                   $scope.temp.post_holiday = false;
               }
               if (f.post_weekend_allowed == "Yes") {
                   $scope.temp.post_weekend = true;
               }
               else {
                   $scope.temp.post_weekend = false;
               }
               if (f.combination_of_holiday_and_weekend_allowed == "Yes") {
                   $scope.temp.comb_holiday_weekend = true;
               }
               else {
                   $scope.temp.comb_holiday_weekend = false;
               }
               if (f.pays_empl_leave_policy_status == "Yes") {
                   $scope.temp.pays_empl_leave_policy_status = true;
               }
               else {
                   $scope.temp.pays_empl_leave_policy_status = false;
               }
               $scope.user_data = true;
               $scope.beforeTable = false;
               //$scope.selectedValue($scope.temp.new_le_leave_code);
           }
       }
       $scope.showDataEmploye();

       $scope.UpdateFunction = function () {
          
               debugger;
               if ($scope.temp.staff_code != 'Please Select' && $scope.temp.le_leave_code != 'Please Select') {
                   var temp_leave_code = [];
                   for (var i = 0; i < $scope.temp.le_leave_code.length; i++) {
                       temp_leave_code.push({ leave_code: $scope.temp.le_leave_code[i] });
                       //if (i == 0) {
                       //    temp_leave_code = $scope.temp.le_leave_code[0];
                       //}
                       //else {
                       //    temp_leave_code = temp_leave_code + ',' + $scope.temp.le_leave_code[i];
                       //}
                   }
                   var final_dataInserting = []
                   var dataInserting = {
                       leave_policy_id: $scope.temp.leave_policy_id,
                       staff_type_code: $scope.temp.staff_code,
                       leave_code: $scope.temp.new_le_leave_code,
                       //combination_of_leave: temp_leave_code,
                       monthly_allowed: $scope.temp.monthly_allowed,
                       termly_allowed: $scope.temp.termly_allowed,
                       yearly_allowed: $scope.temp.yearly_allowed,
                       max_leave_per_Application: $scope.temp.max_leave_application,
                       is_prefix_holiday_allowed: $scope.temp.pre_holiday,
                       post_holiday_allowed: $scope.temp.post_holiday,
                       prefix_weekend_allowed: $scope.temp.pre_weekend,
                       post_weekend_allowed: $scope.temp.post_weekend,
                       combination_of_holiday_and_weekend_allowed: $scope.temp.comb_holiday_weekend,
                       pays_empl_leave_policy_effective_from: $scope.temp.pays_empl_leave_policy_effective_from,
                       pays_empl_leave_policy_status: $scope.temp.pays_empl_leave_policy_status,
                       pays_empl_leave_policy_created_by: $scope.username,
                       Opr: "U",
                   }
                   final_dataInserting.push(dataInserting);
                   if (final_dataInserting.length > 0) {
                       $http.post(ENV.apiUrl + "api/EmployeeLeavePolicyController/IUDLeavePolicy", final_dataInserting).then(function (results) {
                           var leave_code = results.data;

                           //swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                           $scope.user_data = false;
                           $scope.beforeTable = true;

                           if ($scope.temp.leave_policy_id != "") {
                               swal({ text: "Leave Policy Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                   if (isConfirm) {
                                       if (temp_leave_code.length > 0) {

                                           $http.post(ENV.apiUrl + "api/EmployeeLeavePolicyController/IUDLeavePolicyDetails?leave_policy_id=" + $scope.temp.leave_policy_id, temp_leave_code).then(function (results) {
                                               console.log("leave details post", results.data);
                                               $scope.showDataEmploye();
                                           });
                                       }
                                   }
                               });

                           }
                           else {
                               swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                               $scope.user_data = true;
                               $scope.beforeTable = false;
                           }
                       });
                       $scope.showDataEmploye();
                   }
               } else {
                   swal({ text: "Please select mandatory fields.", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
               }
           }
       
       $scope.addFunction = function () {
           debugger;
           if( $scope.temp.staff_code!= 'Please Select' && $scope.temp.le_leave_code != 'Please Select')
           {
               var temp_leave_code = [];
               for (var i = 0; i < $scope.temp.le_leave_code.length; i++) {

                   temp_leave_code.push({leave_code: $scope.temp.le_leave_code[i] });

                   //if (i == 0) {
                   //    temp_leave_code = $scope.temp.le_leave_code[0];
                   //}
                   //else {
                   //    temp_leave_code = temp_leave_code + ',' + $scope.temp.le_leave_code[i];
                   //}
               }
               console.log("temp_leave_code",temp_leave_code);

               var final_dataInserting=[]
               var dataInserting = {
                   staff_type_code :$scope.temp.staff_code,
                   leave_code: $scope.temp.new_le_leave_code,
                   //combination_of_leave: temp_leave_code,
                   monthly_allowed :$scope.temp.monthly_allowed, 
                   termly_allowed :$scope.temp.termly_allowed,
                   yearly_allowed :$scope.temp.yearly_allowed,
                   max_leave_per_Application :$scope.temp.max_leave_application,
                   is_prefix_holiday_allowed :$scope.temp.pre_holiday,
                   post_holiday_allowed :$scope.temp.post_holiday,
                   prefix_weekend_allowed :$scope.temp.pre_weekend,
                   post_weekend_allowed :$scope.temp.post_weekend,
                   combination_of_holiday_and_weekend_allowed: $scope.temp.comb_holiday_weekend,
                   pays_empl_leave_policy_effective_from: $scope.temp.pays_empl_leave_policy_effective_from,
                   pays_empl_leave_policy_status: $scope.temp.pays_empl_leave_policy_status,
                   pays_empl_leave_policy_created_by: $scope.username,
                   Opr:"I",
               }
               final_dataInserting.push(dataInserting);



               if (final_dataInserting.length > 0) {
                   $http.post(ENV.apiUrl + "api/EmployeeLeavePolicyController/IUDLeavePolicy", final_dataInserting).then(function (results) {
                       var leave_policy_id = results.data;
                       if (leave_policy_id != "") {
                           swal({ text: "Leave Policy Added Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                               if (isConfirm) {
                                   if (temp_leave_code.length > 0) {

                                       $http.post(ENV.apiUrl + "api/EmployeeLeavePolicyController/IUDLeavePolicyDetails?leave_policy_id=" + leave_policy_id, temp_leave_code).then(function (results) {
                                           console.log("leave details post", results.data);
                                           $scope.showDataEmploye();
                                       });
                                   }
                               }
                           });

                           $scope.user_data = false;
                           $scope.beforeTable = true;                           
                       }
                       else {
                           swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                           $scope.user_data = true;
                           $scope.beforeTable = false;
                       }
                   });
                   $scope.showDataEmploye();
               }
           } else {
               swal({ text: "Please select mandatory fields.", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
           }
       }
       $scope.clearFunction=function(){
           $scope.temp = [];
       }
       $scope.selectedValue = function () {
           debugger;
           //if (selectedValue != 'undefined') {
               var temp_comb_leave = [];
               var comb_leave_temp = [];
               $scope.comb_leave = [];
               
               $http.get(ENV.apiUrl + "api/common/LeaveType/Get_leave_codes").then(function (res) {

                   $scope.comb_leave = res.data;
                   //temp_comb_leave = res.data;
                   //for (var i = 0; i < temp_comb_leave.length; i++) {
                   //    if (temp_comb_leave[i].le_leave_code != selectedValue) {
                   //        comb_leave_temp[i] = temp_comb_leave[i];
                   //    }
                   //}
                   //$scope.comb_leave = comb_leave_temp;
                   setTimeout(function () {
                       $('#combinationLeave').change(function () {
                           console.log($(this).val());
                       }).multipleSelect({
                           width: '100%'
                       });
                   }, 1000);
               })
          // }
       }

       $scope.selectedValue();

       $(function () {
           debugger;    
           $('#combinationLeave').multipleSelect({
               width: '100%'
           });
           console.log("Print");
           console.log($scope.comb_leave);
       });


       $scope.CheckAllChecked = function () {
           main = document.getElementById('mainchk');
           if (main.checked == true) {
               for (var i = 0; i < $scope.emplleave.length; i++) {
                   var v = document.getElementById("helt-" + i);
                   v.checked = true;
                   $scope.row1 = 'row_selected';
                   $('tr').addClass("row_selected");
               }
           }
           else {

               for (var i = 0; i < $scope.emplleave.length; i++) {
                   var v = document.getElementById("helt-" + i);
                   v.checked = false;
                   main.checked = false;
                   $scope.row1 = '';
               }
           }

       }

       $scope.checkMonthlyAllowedLeave = function () {
           debugger
           if ($scope.temp.termly_allowed != undefined || $scope.temp.termly_allowed != "" || $scope.temp.termly_allowed != null) {
               $scope.temp.termly_allowed = "";
           }
           if ($scope.temp.yearly_allowed != undefined || $scope.temp.yearly_allowed != "" || $scope.temp.yearly_allowed != null) {
               $scope.temp.yearly_allowed = "";
           }
       }

       $scope.checkTermlyAllowedLeave = function () {
           if ($scope.temp.monthly_allowed != undefined || $scope.temp.monthly_allowed != "" || $scope.temp.monthly_allowed != null) {
               $scope.temp.monthly_allowed = "";
           }
           if ($scope.temp.yearly_allowed != undefined || $scope.temp.yearly_allowed != "" || $scope.temp.yearly_allowed != null) {
               $scope.temp.yearly_allowed = "";
           }
       }

       $scope.checkYearlyAllowedLeave = function () {
           if ($scope.temp.monthly_allowed != undefined || $scope.temp.monthly_allowed != "" || $scope.temp.monthly_allowed != null) {
               $scope.temp.monthly_allowed = "";
           }
           if ($scope.temp.termly_allowed != undefined || $scope.temp.termly_allowed != "" || $scope.temp.termly_allowed != null) {
               $scope.temp.termly_allowed = "";
           }
       }

       $scope.checkonebyonedelete = function () {

           $("input[type='checkbox']").change(function (e) {
               if ($(this).is(":checked")) {
                   $(this).closest('tr').addClass("row_selected");
                   $scope.color = '#edefef';
               } else {
                   $(this).closest('tr').removeClass("row_selected");
                   $scope.color = '#edefef';
               }
           });

           main = document.getElementById('mainchk');
           if (main.checked == true) {
               main.checked = false;
               $("input[type='checkbox']").change(function (e) {
                   if ($(this).is(":checked")) {
                       $(this).closest('tr').addClass("row_selected");
                   }
                   else {
                       $(this).closest('tr').removeClass("row_selected");
                   }
               });
           }
       }

      }])

    

})();