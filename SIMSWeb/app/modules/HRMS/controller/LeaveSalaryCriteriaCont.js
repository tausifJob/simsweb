﻿(function () {
    'use strict';
    var opr = '';
    var deletecode = [];
    var deletecalcualationcode = [];
    var main;
    var main1;
    var data1 = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('LeaveSalaryCriteriaCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.grid = true;
            $scope.edit_data = false;
            $scope.GratCri_data = [];
            $scope.edit_code = false;

            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            var company;

            $http.get(ENV.apiUrl + "api/OverTimeRate/getformuladesc").then(function (getformuladesc_data) {
                $scope.formuladesc = getformuladesc_data.data;
            });

            // ---this Api for get session value
            $http.get(ENV.apiUrl + "api/PDCSubmission/GetComp_Name?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.cmbCompany = res.data;
                if (res.data.length == 1) {
                    $scope.companyName = res.data[0].comp_name;

                    $scope.finnComp = res.data[0].comp_code;

                    $http.get(ENV.apiUrl + "api/PDCSubmission/GetFinancial_year?comp_code=" + $scope.finnComp).then(function (res) {

                        if (res.data.length > 0) {

                            console.log('Finn');

                            var data = {
                                year: res.data[0].financial_year,
                                company: $scope.finnComp,
                                companyame: $scope.companyName
                            }
                            window.localStorage["Finn_comp"] = JSON.stringify(data)
                            //$scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

                            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

                            $http.get(ENV.apiUrl + "api/GratuityCriteria/GetAll_Account_Names?comp_cd=" + $scope.finnDetail.company).then(function (res) {
                                $scope.accNo_data = res.data;
                            });
                            // $state.reload();
                        }
                    });


                }
                else if (res.data.length > 1) {
                    //$scope.financeWindow();
                }

            });

            // $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            //console.log($scope.finnDetail)

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };


            $scope.countData = [
                  { val: 10, data: 10 },
                  { val: 20, data: 20 },
                  { val: 'All', data: 'All' },

            ]

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $http.get(ENV.apiUrl + "api/LeaveSalaryCriteria/Get_paysLeaveCriteria").then(function (res) {
                debugger;
                $scope.display = false;
                $scope.grid = true;
                $scope.GratCri_data = res.data;
                console.log("GratCri_data", $scope.GratCri_data);
                if ($scope.GratCri_data.length > 0) {
                    $scope.pager = true;
                    if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.GratCri_data.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.GratCri_data.length, data: 'All' })
                    }

                    $scope.totalItems = $scope.GratCri_data.length;
                    $scope.todos = $scope.GratCri_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                }
                else {
                    // swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                    $scope.filteredTodos = [];
                }

            });


            $scope.getgrid = function () {
                $scope.currentPage = 1;

                $http.get(ENV.apiUrl + "api/LeaveSalaryCriteria/Get_paysLeaveCriteria").then(function (res) {
                    debugger;
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.GratCri_data = res.data;
                    if ($scope.GratCri_data.length > 0) {
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.GratCri_data.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.GratCri_data.length, data: 'All' })
                        }

                        $scope.totalItems = $scope.GratCri_data.length;
                        $scope.todos = $scope.GratCri_data;
                        $scope.makeTodos();
                        $scope.grid = true;
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                        $scope.filteredTodos = [];
                    }

                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.row1 = '';

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.checkLimit = function () {
                console.log($scope.GratCri_data);
                debugger
      
            }

            $scope.size = function (str) {
      
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.GratCri_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
           }

            $timeout(function () {
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            setTimeout(function () {
                $("#cmb_acc_code,#cmb_bank_code,#cmb_cash_code,#cmb_airfare_code,#cmb_prov_acc_code").select2();
            }, 100);


            $http.get(ENV.apiUrl + "api/LeaveSalaryCriteria/Get_All_pays_grade").then(function (res) {
                debugger;
                $scope.paysGrade_data = res.data;
            });



            $scope.makeTodos = function () {
                debugger;
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.min_day_text = true;
                    $scope.min_day_yrs = false;
                    $scope.min_day_month = false;
                    $scope.min_day_day = false;

                    $scope.display = true;
                    $scope.grid = false;
                    $scope.save1 = true;
                    $scope.update = false;
                    $scope.editmode = false;
                    $scope.newmode = true;
                    $scope.edt = [];
                    $scope.edt = {
                        leave_status: true,
                        leave_criteria_code: "",
                        leave_criteria_name: "",
                        leave_year: "",
                        leave_month: "",
                        leave_days: "",
                        leave_prov_acct_code: "",
                        //leave_working_days: "",
                        leave_criteria_min_days: "",
                        leave_criteria_max_days: "",
                        leave_payable_days: ""
                    }
                    $("#cmb_acc_code").select2("val", "");
                    $("#cmb_bank_code").select2("val", "");
                    $("#cmb_cash_code").select2("val", "");
                    $("#cmb_airfare_code").select2("val", "");
                    $("#cmb_prov_acc_code").select2("val", "");
                }
            }

            $scope.edit = function (str) {
                $scope.min_day_text = false;
                $scope.min_day_yrs = true;
                $scope.min_day_month = true;
                $scope.min_day_day = true;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.update = true;
                debugger;
                $scope.edt =
                    {
                        leave_criteria_code: str.leave_criteria_code,
                        leave_criteria_name: str.leave_criteria_name,
                        leave_criteria_min_days: str.leave_criteria_min_days,
                        leave_year: str.year,
                        leave_month: str.month,
                        leave_days: str.days,
                        leave_pay_grade: str.leave_pay_grade,

                        leave_acct_code: str.leave_acct_code,
                        leave_acct_name: str.leave_acct_name,

                        leave_bank_acct_code: str.leave_bank_acct_code,
                        leave_bank_acct_name: str.leave_bank_acct_name,

                        leave_cash_acct_code: str.leave_cash_acct_code,
                        leave_cash_acct_name: str.leave_cash_acct_name,

                        leave_airfare_acct_code: str.leave_airfare_acct_code,
                        leave_airfare_acct_name: str.leave_airfare_acct_name,

                        leave_prov_acct_code: str.leave_prov_acct_code,
                        leave_prov_acct_name: str.leave_prov_acct_name,
                        leave_status: str.leave_status,
                        leave_formula_code: str.leave_formula_code,
                        //leave_working_days: str.leave_working_days,
                        leave_payable_days: str.leave_payable_days,
                        leave_min_days: str.leave_criteria_min_days,
                        leave_max_days: str.leave_criteria_max_days
                    }

                if (str.leave_criteria_max_days == "0") {
                    $scope.edt.leave_max_days = "";
                }

                $("#cmb_acc_code").select2("val", str.leave_acct_code);
                $("#cmb_prov_acc_code").select2("val", str.leave_prov_acct_code);
                $("#cmb_bank_code").select2("val", str.leave_bank_acct_code);
                $("#cmb_cash_code").select2("val", str.leave_cash_acct_code);
                $("#cmb_airfare_code").select2("val", str.leave_airfare_acct_code);

            }

            $scope.checkExists = function () {
                for (var i = 0; i < $scope.GratCri_data.length; i++) {
                    if ($scope.GratCri_data[i].leave_criteria_code == $scope.edt.leave_criteria_code) {
                        swal({ title: "Alert", text: "Criteria Code already exists", width: 300, height: 200 });
                        $scope.edt.leave_criteria_code = '';
                    }
                }
            }

            $scope.Save = function (myForm) {
                debugger
                if (myForm) {
                    data1 = [];
                    data = [];
                    if ($scope.edt.leave_year != undefined) {
                        var year = $scope.edt.leave_year * 365;
                    }
                    else {
                        var year = 0;
                    }
                    if ($scope.edt.leave_month != undefined) {
                        var month = $scope.edt.leave_month * 30;
                    }
                    else {
                        var month = 0;
                    }
                    if ($scope.edt.leave_days != undefined) {
                        if ($scope.edt.leave_days > 0) {
                            var day = $scope.edt.leave_days * 1;
                        }
                        else {
                            var day = $scope.edt.leave_days * 0;
                        }
                    }
                    else {
                        var day = 0;
                    }

                    $scope.min_days = (year + month + day);

                    var data = ({
                        leave_criteria_code: $scope.edt.leave_criteria_code,
                        leave_criteria_name: $scope.edt.leave_criteria_name,
                        leave_criteria_min_days: $scope.edt.leave_min_days,
                        leave_pay_grade: $scope.edt.leave_pay_grade,
                        leave_acct_code: $scope.edt.leave_acct_code,
                        leave_bank_acct_code: $scope.edt.leave_bank_acct_code,
                        leave_cash_acct_code: $scope.edt.leave_cash_acct_code,
                        leave_airfare_acct_code: $scope.edt.leave_airfare_acct_code,
                        leave_prov_acct_code: $scope.edt.leave_prov_acct_code,
                        leave_status: $scope.edt.leave_status,
                        leave_formula_code: $scope.edt.leave_formula_code,
                        //leave_working_days: $scope.edt.leave_working_days,
                        leave_criteria_max_days: $scope.edt.leave_max_days,
                        leave_payable_days: $scope.edt.leave_payable_days,
                        opr: 'I'
                    });

                    $scope.samerecord = false;
                    debugger;
   
                    data1.push(data);
                    console.log("data1", data1);

                    $http.post(ENV.apiUrl + "api/LeaveSalaryCriteria/CUDInsertpaysLeaveCriteria", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $scope.getgrid();
                    });
                    //}
                }
            }

            $scope.Update = function (myForm) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    debugger
                    if (myForm) {
                        data1 = [];
                        data = [];
                        if ($scope.edt.leave_year != undefined) {
                            var year = $scope.edt.leave_year * 365;
                        }
                        else {
                            var year = 0;
                        }
                        if ($scope.edt.leave_month != undefined) {
                            var month = $scope.edt.leave_month * 30;
                        }
                        else {
                            var month = 0;
                        }
                        if ($scope.edt.leave_days != undefined) {
                            if ($scope.edt.leave_days > 0) {
                                var day = $scope.edt.leave_days * 1;
                            }
                            else {
                                var day = $scope.edt.leave_days * 0;
                            }
                        }
                        else {
                            var day = 0;
                        }

                        $scope.min_days = (year + month + day);

                        var data = ({
                            leave_criteria_code: $scope.edt.leave_criteria_code,
                            leave_criteria_name: $scope.edt.leave_criteria_name,
                            leave_criteria_min_days: $scope.edt.leave_min_days,
                            leave_pay_grade: $scope.edt.leave_pay_grade,
                            leave_acct_code: $scope.edt.leave_acct_code,
                            leave_bank_acct_code: $scope.edt.leave_bank_acct_code,
                            leave_cash_acct_code: $scope.edt.leave_cash_acct_code,
                            leave_airfare_acct_code: $scope.edt.leave_airfare_acct_code,
                            leave_prov_acct_code: $scope.edt.leave_prov_acct_code,
                            leave_status: $scope.edt.leave_status,
                            leave_formula_code: $scope.edt.leave_formula_code,
                            //leave_working_days: $scope.edt.leave_working_days,
                            leave_criteria_max_days: $scope.edt.leave_max_days,
                            leave_payable_days: $scope.edt.leave_payable_days,
                            opr: 'U'
                        });

                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/LeaveSalaryCriteria/CUDInsertpaysLeaveCriteria", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                            $scope.operation = false;
                            $scope.getgrid();

                        })
                        $scope.operation = false;
                        $scope.table1 = true;
                    }
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.edt = {

                    leave_criteria_code: "",
                    leave_criteria_name: "",
                    leave_year: "",
                    leave_month: "",
                    leave_days: "",
                    leave_prov_acct_code: "",
                    //leave_working_days: "",
                    leave_criteria_min_days: "",
                    leave_criteria_max_days: "",
                    leave_payable_days:""
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Delete = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    var deletecode = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].leave_criteria_code + i);

                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodercode = ({
                                'leave_criteria_code': $scope.filteredTodos[i].leave_criteria_code,
                                'opr': 'D'
                            });
                            deletecode.push(deletemodercode);
                        }
                    }

                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                $http.post(ENV.apiUrl + "api/LeaveSalaryCriteria/CUDDeleteLeaveCriteria", deletecode).then(function (res) {
                                    $scope.msg1 = res.data;

                                    debugger
                                    if ($scope.msg1 != '' || $scope.msg1 != null) {
                                        swal({ title: "Record Deleted Successfully", text: $scope.msg1, width: 300, height: 200 }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else {
                                        swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                });
                            }
                            else {
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }

                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById($scope.filteredTodos[i].leave_criteria_code + i);

                                    if (v.checked == true) {
                                        v.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                }

            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].leave_criteria_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].leave_criteria_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.GratCri_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.GratCri_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.leave_criteria_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.leave_criteria_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();

                $scope.chk = {}
                $scope.chk['check_all'] = false;
                $scope.row1 = '';
            }

            $scope.check_acccode = function () {

                debugger;
                if ($scope.edt.leave_acct_code != undefined && $scope.edt.leave_bank_acct_code != undefined || $scope.edt.leave_acct_code != undefined && $scope.edt.leave_cash_acct_code != undefined) {
                    if ($scope.edt.leave_acct_code == $scope.edt.leave_bank_acct_code) {

                        swal({ title: "Alert", text: "Account Code and Bank Code should not be same", width: 300, height: 200 });

                    }
                    if ($scope.edt.leave_acct_code == $scope.edt.leave_cash_acct_code) {

                        swal({ title: "Alert", text: "Account Code and Cash Code should not be same", width: 300, height: 200 });

                    }
                }
            }

            $scope.check_bankcode = function () {
                debugger
                if ($scope.edt.leave_acct_code != undefined && $scope.edt.leave_bank_acct_code != undefined || $scope.edt.leave_acct_code != undefined && $scope.edt.leave_cash_acct_code != undefined) {
                    if ($scope.edt.leave_bank_acct_code == $scope.edt.leave_cash_acct_code) {

                        swal({ title: "Alert", text: "Bank Code and Cash Code should not be same", width: 300, height: 200 });

                    }
                    if ($scope.edt.leave_acct_code == $scope.edt.leave_bank_acct_code) {

                        swal({ title: "Alert", text: "Bank Code and Account Code should not be same", width: 300, height: 200 });

                    }
                }
            }

            $scope.check_cashcode = function () {
                if ($scope.edt.leave_acct_code != undefined && $scope.edt.leave_bank_acct_code != undefined || $scope.edt.leave_acct_code != undefined && $scope.edt.leave_cash_acct_code != undefined) {
                    if ($scope.edt.leave_bank_acct_code == $scope.edt.leave_cash_acct_code) {

                        swal({ title: "Alert", text: "Cash Code and Bank Code should not be same", width: 300, height: 200 });

                    }
                    if ($scope.edt.leave_acct_code == $scope.edt.leave_cash_acct_code) {

                        swal({ title: "Alert", text: "Account Code and Cash Code should not be same", width: 300, height: 200 });

                    }
                }
            }
        }])
})();
