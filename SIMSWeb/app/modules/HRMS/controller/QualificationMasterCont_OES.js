﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('QualificationMasterOESCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/QualMasterOES/getAllQualification").then(function (res1) {
                $scope.all_Qual_Data = res1.data;                
                $scope.totalItems = $scope.all_Qual_Data.length;
                $scope.todos = $scope.all_Qual_Data;
                $scope.makeTodos();
            });

            $scope.size = function (str) {                
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.all_Qual_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.all_Qual_Data;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.pays_qualification_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.pays_qualificaion_display_oder.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.desg_company_code == toSearch) ? true : false;
            }

            $scope.New = function () {

                $scope.disabled = false;
                $scope.readonly = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = "";
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            var datasend = [];
            $scope.savedata = function (Myform) {
                datasend = [];
                if (Myform) {

                    debugger;
                    var flag = false;
                    var data = $scope.temp;
                    for (var i = 0; i < $scope.all_Qual_Data.length; i++) {
                        if ($scope.all_Qual_Data[i].pays_qualification_desc == $scope.temp.pays_qualification_desc ||
                            $scope.all_Qual_Data[i].pays_qualificaion_display_oder == $scope.temp.pays_qualificaion_display_oder) {
                            swal({ text: "Record Already Exist", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                            flag = true;
                            break;
                        }
                    }

                    if (!flag) {

                        data.opr = "I";
                        datasend.push(data);

                        $http.post(ENV.apiUrl + "api/QualMasterOES/CUDQualification", datasend).then(function (msg) {
                            $scope.msg1 = msg.data;

                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                            }
                            else {
                                swal({ text: "Record Not Inserted", imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                            }
                            $http.get(ENV.apiUrl + "api/QualMasterOES/getAllQualification").then(function (res1) {
                                $scope.all_Qual_Data = res1.data;
                                $scope.totalItems = $scope.all_Qual_Data.length;
                                $scope.todos = $scope.all_Qual_Data;
                                $scope.makeTodos();
                            });
                        });
                        datasend = [];
                        $scope.table = true;
                        $scope.display = false;
                    }
                    else {
                        swal({ text: "Record Already Present", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }

                }
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.desg_company_code = "";
                $scope.temp.desg_desc = "";
                $scope.temp.desg_communication_status = "";
            }

            $scope.edit = function (str) {
                $scope.readonly = true;
                $scope.disabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.temp = {
                    pays_qualification_code: str.pays_qualification_code,
                    pays_qualification_desc: str.pays_qualification_desc,
                    pays_qualification_desc_arabic: str.pays_qualification_desc_arabic,
                    pays_qualificaion_display_oder: str.pays_qualificaion_display_oder,
                    pays_qualificaiton_status: str.pays_qualificaiton_status
                };
            }

            var dataupdate = [];
            $scope.update = function () {//Myform
                //if (Myform) {
                var Udata = $scope.temp;
                Udata.opr = "U";
                dataupdate.push(Udata);

                $http.post(ENV.apiUrl + "api/QualMasterOES/CUDQualification", dataupdate).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                    }
                    else {
                        swal({ text: "Record Not Updated", imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                    }
                    $http.get(ENV.apiUrl + "api/QualMasterOES/getAllQualification").then(function (res1) {
                        $scope.all_Qual_Data = res1.data;
                        $scope.totalItems = $scope.all_Qual_Data.length;
                        $scope.todos = $scope.all_Qual_Data;
                        $scope.makeTodos();
                    });
                });
                dataupdate = [];
                $scope.table = true;
                $scope.display = false;
                // }
            }

            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].pays_qualification_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].pays_qualification_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].pays_qualification_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'pays_qualification_code': $scope.filteredTodos[i].pays_qualification_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/QualMasterOES/CUDQualification?data=", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/QualMasterOES/getAllQualification").then(function (res1) {
                                                $scope.all_Qual_Data = res1.data;
                                                $scope.totalItems = $scope.all_Qual_Data.length;
                                                $scope.todos = $scope.all_Qual_Data;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal({ text: "Record Not Deleted ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {

                                            $http.get(ENV.apiUrl + "api/QualMasterOES/getAllQualification").then(function (res1) {
                                                $scope.all_Qual_Data = res1.data;
                                                $scope.totalItems = $scope.all_Qual_Data.length;
                                                $scope.todos = $scope.all_Qual_Data;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].pays_qualification_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({
                        text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png",
                        showCloseButton: true, width: 380,
                    });
                }
                $scope.currentPage = str;
            }

        }])

})();
