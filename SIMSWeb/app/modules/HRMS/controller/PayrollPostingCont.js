﻿(function () {
    'use strict';
    var del = [];
    var main;
    var main1;
    var simsController = angular.module('sims.module.HRMS');
    simsController.controller('PayrollPostingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.voucher_details = false;
            $scope.gratuity_details = false;
            $scope.airfare_details = false;
            $http.post(ENV.apiUrl + "api/Payroll/PayrollPosting/Year_Month").then(function (Year_Month) {
                $scope.Year_Month = Year_Month.data;
                var d = new Date().getFullYear();
                var n = new Date().getMonth() + 1;
                var ym = d + '' + n;

                $scope.edt = { year_month: ym };
            });


            $http.post(ENV.apiUrl + "api/Payroll/PayrollPosting/PGrade").then(function (PGrade) {
                $scope.PGrade = PGrade.data;
            });

            $http.post(ENV.apiUrl + "api/Payroll/PayrollPosting/Department").then(function (Department) {
                $scope.Department = Department.data;
            });

            $http.post(ENV.apiUrl + "api/Payroll/PayrollPosting/Designation").then(function (Designation) {
                $scope.Designation = Designation.data;
            });


            $scope.Show_data = function () {
                $scope.chk_status = false;
                $scope.busy = true;
                $scope.rgvtbl = false;
                if ($scope.edt.gr_code == undefined)
                    $scope.edt.gr_code = '';
                if ($scope.edt.dept_code == undefined)
                    $scope.edt.dept_code = '';
                if ($scope.edt.dg_code == undefined)
                    $scope.edt.dg_code = '';


                var d = {
                    year_month: $scope.edt.year_month,
                    dept: $scope.edt.dept_code,
                    grade: $scope.edt.gr_code,
                    desg: $scope.edt.dg_code
                }

                $http.post(ENV.apiUrl + "api/Payroll/PayrollPosting/EmployeeDetails", d).then(function (EmployeeDetails) {
                  
                    $scope.EmployeeDetails = EmployeeDetails.data.table;
                   
                    if ($scope.EmployeeDetails != undefined) {
                        if ($scope.EmployeeDetails.length > 0) {
                            $scope.header_data = [];
                            
                            angular.forEach($scope.EmployeeDetails[0], function (value, key) {
                                $scope.header_data.push({ name: key });
                            });

                            $scope.busy = false;
                            $scope.rgvtbl = true;
                        }
                    }
                    else {
                        $scope.busy = false;
                        $scope.rgvtbl = false;
                        swal('', 'Payroll is not generated for this month.');
                    }


                });
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.le_leave_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                        item.acno == toSearch) ? true : false;
            }

            $scope.CheckAllChecked = function (str) {
                
                //$scope.chk_status_one = str;
                main = document.getElementById('mainchk');

                for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                    if (main.checked == true) {
                        $('tr').addClass("row_selected");
                        $scope.EmployeeDetails[i]['chk_status'] = true
                    }

                    else {
                        $('tr').removeClass("row_selected");
                        $scope.EmployeeDetails[i]['chk_status'] = false


                    }

                }
            }

            $scope.reset_form = function () {
                var d = new Date().getFullYear();
                var n = new Date().getMonth() + 1;
                var ym = d + '' + n;
                $scope.chk_status = false;
                $scope.edt = { year_month: ym };
                $scope.EmployeeDetails = [];
                $scope.rgvtbl = false;
                $scope.voucher_details = false;
                $scope.gratuity_details = false;
                $scope.airfare_details = false;
                $scope.finalize_date = '';
                $scope.grt_finalize_date = '';
                $scope.af_finalize_date = '';
            }

            $scope.payroll_posting = function () {
                
                $scope.emplist = '';
                for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                    if ($scope.EmployeeDetails[i].chk_status)
                        $scope.emplist = $scope.emplist + $scope.EmployeeDetails[i]['employee ID'] + ',';
                }
              
                if ($scope.edt.year_month == '' || $scope.edt.year_month == undefined) {
                    swal('', 'Please select Year Month.');
                }
                else {
                    if ($scope.emplist == '') {
                        swal('', 'Please select employee.');
                    }
                    else {
                        $scope.emplist = '';
                        for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                            if ($scope.EmployeeDetails[i]['posting Status'] == 'N' && $scope.EmployeeDetails[i].chk_status)
                                $scope.emplist = $scope.emplist + $scope.EmployeeDetails[i]['employee ID'] + ',';
                        }
                        if ($scope.emplist == '') {
                            swal('', 'Payroll Posting is already done of this month.');
                            $scope.close_btn();
                            $scope.reset_form();
                        }
                        else {
                            var data = {
                                year_month: $scope.edt.year_month,
                                emp_list: $scope.emplist,
                            }
                            $http.post(ENV.apiUrl + "api/Payroll/PayrollPosting/Voucher_details", data).then(function (Voucher_details) {
                                $scope.Voucher_details = Voucher_details.data;
                                if ($scope.Voucher_details.voucher1.length > 0) {
                                    $scope.busy = false;
                                    $('#myModal').modal({ backdrop: 'static', keyboard: false });
                                    $scope.voucher_details = true;
                                    $scope.gratuity_details = false;
                                    $scope.airfare_details = false;
                                }
                                else {
                                    $scope.busy = false;
                                    swal('', 'Record not found.');
                                }

                            });
                        }
                    }
                }

            }

            $scope.gratuity_posting = function () {
                $scope.emplist = '';
                for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                    if ($scope.EmployeeDetails[i].chk_status)
                        $scope.emplist = $scope.emplist + $scope.EmployeeDetails[i]['employee ID'] + ',';
                }
               
                if ($scope.edt.year_month == '' || $scope.edt.year_month == undefined) {
                    swal('', 'Please select Year Month.');
                }
                else {
                    if ($scope.emplist == '') {
                        swal('', 'Please select employee.');
                    }
                    else {
                        $scope.emplist = '';
                        for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                            if ($scope.EmployeeDetails[i]['gratuity Posting Status'] == 'N' && $scope.EmployeeDetails[i].chk_status)
                                $scope.emplist = $scope.emplist + $scope.EmployeeDetails[i]['employee ID'] + ',';
                        }
                        if ($scope.emplist == '') {
                            swal('', 'Gratuity Posting is already done of this month.');
                            $scope.close_btn();
                            $scope.reset_form();
                        }
                        else {
                            var data = {
                                year_month: $scope.edt.year_month,
                                emp_list: $scope.emplist,
                            }
                            
                            $http.post(ENV.apiUrl + "api/Payroll/PayrollPosting/Gratuity_details", data).then(function (Gratuity_details) {
                                $scope.Gratuity_details = Gratuity_details.data;
                                if ($scope.Gratuity_details.length > 0) {
                                    $scope.busy = false;

                                    $scope.Gratuity_details_summary = getdistinct_age_group();

                                    $scope.voucher_details = false;
                                    $scope.gratuity_details = true;
                                    $scope.airfare_details = false;
                                    $('#myModal').modal({ backdrop: 'static', keyboard: false });

                                }
                                else {
                                    $scope.busy = false;
                                    $scope.voucher_details = false;
                                    $scope.gratuity_details = false;
                                    $scope.airfare_details = false;
                                    swal('', 'Gratuity not defined for these employee.');
                                }

                            });
                        }
                    }
                }

            }

            $scope.airfare_posting = function () {
                
                $scope.emplist = '';
                for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                    if ($scope.EmployeeDetails[i].chk_status)
                        $scope.emplist = $scope.emplist + $scope.EmployeeDetails[i]['employee ID'] + ',';
                }
              
                if ($scope.edt.year_month == '' || $scope.edt.year_month == undefined) {
                    swal('', 'Please select Year Month.');
                }
                else {
                    if ($scope.emplist == '') {
                        swal('', 'Please select employee.');
                    }
                    else {
                        $scope.emplist = '';
                        for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                            if ($scope.EmployeeDetails[i]['airFare Posting Status'] == 'N' && $scope.EmployeeDetails[i].chk_status)
                                $scope.emplist = $scope.emplist + $scope.EmployeeDetails[i]['employee ID'] + ',';
                        }
                        if ($scope.emplist == '') {
                            swal('', 'Air Fare Posting is already done of this month.');
                            $scope.close_btn();
                            $scope.reset_form();
                        }
                        else {
                            var data = {
                                year_month: $scope.edt.year_month,
                                emp_list: $scope.emplist,
                            }
                            
                            $http.post(ENV.apiUrl + "api/Payroll/PayrollPosting/AirFare_details", data).then(function (AirFare_details) {
                                $scope.AirFare_details = AirFare_details.data;
                                if ($scope.AirFare_details.length > 0) {
                                    $scope.busy = false;

                                    $scope.Gratuity_details_summary = getdistinct_age_group_air();

                                    $scope.voucher_details = false;
                                    $scope.gratuity_details = false;
                                    $scope.airfare_details = true;
                                    $('#myModal').modal({ backdrop: 'static', keyboard: false });

                                }
                                else {
                                    $scope.busy = false;
                                    $scope.voucher_details = false;
                                    $scope.gratuity_details = false;
                                    $scope.airfare_details = false;
                                    swal('', 'Air Fare not defined for these employee.');
                                }

                            });
                        }
                    }
                }

            }

            function summary_fun(obj) {
                
                var ret = {};
                ret.sum = 0.00;
                ret.acc = '';
                ret.acc_name = '';
                ret.crt_code = '';
                ret.crt_name = '';
                for (var i = 0; i < $scope.Gratuity_details.length; i++) {
                    if ($scope.Gratuity_details[i].criteria_code == obj) {
                        ret.sum = ret.sum + parseFloat($scope.Gratuity_details[i].grt_amount);
                        ret.acc = $scope.Gratuity_details[i].account_no;
                        ret.acc_name = $scope.Gratuity_details[i].account_name;
                        ret.crt_code = $scope.Gratuity_details[i].criteria_code;
                        ret.crt_name = $scope.Gratuity_details[i].criteria_name;
                    }
                }
                return ret;
            }

            function getdistinct_age_group() {
                
                var ap = [];
                var table2array = [];
                for (var i = 0; i < $scope.Gratuity_details.length; i++) {
                    if (ap.includes($scope.Gratuity_details[i].criteria_code) == false) {
                        ap.push($scope.Gratuity_details[i].criteria_code);
                    }
                }
                for (var i = 0; i < ap.length; i++) {
                    var ob = {};
                    ob = summary_fun(ap[i]);
                    table2array.push(ob);
                }
                return table2array;
            }

            function summary_fun_air(obj) {
                
                var ret = {};
                ret.sum = 0.00;
                ret.acc = '';
                ret.acc_name = '';
                ret.crt_code = '';
                ret.crt_name = '';
                for (var i = 0; i < $scope.AirFare_details.length; i++) {
                    if ($scope.AirFare_details[i].criteria_code == obj) {
                        ret.sum = ret.sum + parseFloat($scope.AirFare_details[i].grt_amount);
                        ret.acc = $scope.AirFare_details[i].account_no;
                        ret.acc_name = $scope.AirFare_details[i].account_name;
                        ret.crt_code = $scope.AirFare_details[i].criteria_code;
                        ret.crt_name = $scope.AirFare_details[i].criteria_name;
                    }
                }
                return ret;
            }

            function getdistinct_age_group_air() {
                
                var ap = [];
                var table2array = [];
                for (var i = 0; i < $scope.AirFare_details.length; i++) {
                    if (ap.includes($scope.AirFare_details[i].criteria_code) == false) {
                        ap.push($scope.AirFare_details[i].criteria_code);
                    }
                }
                for (var i = 0; i < ap.length; i++) {
                    var ob = {};
                    ob = summary_fun_air(ap[i]);
                    table2array.push(ob);
                }
                return table2array;
            }


            $scope.Post = function () {
                if ($scope.finalize_date == undefined || $scope.finalize_date == '') {
                    swal('', 'Please select finalize date.')
                }
                else {
                    var d = {
                        year_month: $scope.edt.year_month,
                        emp_list: $scope.emplist,
                        posting_date: $scope.finalize_date
                    }
                    $http.post(ENV.apiUrl + "api/Payroll/PayrollPosting/Post_details", d).then(function (Post_details) {
                        $scope.Post_details = Post_details.data;
                        if ($scope.Post_details.voucher_msg1 != '' || $scope.Post_details.voucher_msg1 != undefined || $scope.Post_details.voucher_msg2 != '' || $scope.Post_details.voucher_msg2 != undefined)
                            swal('', 'Voucher No :' + $scope.Post_details.voucher_msg1 + ' ' + '\n Voucher No :' + $scope.Post_details.voucher_msg2 + '\n is Created.');
                        else
                            swal('', 'Voucher is not created.');
                        $scope.close_btn();
                        $scope.reset_form();
                    });
                }
            }

            $scope.Post_gratuity = function () {

                if ($scope.grt_finalize_date == undefined || $scope.grt_finalize_date == '') {
                    swal('', 'Please select finalize date.')
                }
                else {
                    var listemp = [];
                    for (var i = 0; i < $scope.Gratuity_details.length; i++) {
                        if ($scope.Gratuity_details[i].grt_amount != $scope.Gratuity_details[i].grt_amount_old) {
                            var v = {
                                grt_amount_old: $scope.Gratuity_details[i].grt_amount_old,
                                grt_amount: $scope.Gratuity_details[i].grt_amount,
                                criteria_code: $scope.Gratuity_details[i].criteria_code,
                                emp_number: $scope.Gratuity_details[i].emp_number,
                                update_user: $rootScope.globals.currentUser.username,
                                year_month: $scope.edt.year_month
                            }
                            listemp.push(v);
                        }
                    }


                    $http.post(ENV.apiUrl + "api/Payroll/PayrollPosting/Gratuity_update", listemp).then(function (Gratuity_update) {
                        $scope.Gratuity_update = Gratuity_update.data;
                        if (Gratuity_update) {
                            var p = {
                                year_month: $scope.edt.year_month,
                                posting_date: $scope.grt_finalize_date,
                                emp_list: $scope.emplist
                            }
                            $http.post(ENV.apiUrl + "api/Payroll/PayrollPosting/GratuityPosting", p).then(function (GratuityPosting) {
                                $scope.GratuityPosting = GratuityPosting.data;
                                if ($scope.GratuityPosting.voucher_msg1 != '' || $scope.GratuityPosting.voucher_msg1 != undefined)
                                    swal('', 'Voucher No :' + $scope.GratuityPosting.voucher_msg1 + '\n is Created.');
                                else
                                    swal('', 'Voucher is not created.');
                                $scope.close_btn();
                                $scope.reset_form();
                            });
                        }

                    });
                }
            }

            $scope.Post_airfare = function () {
                
                if ($scope.af_finalize_date == undefined || $scope.af_finalize_date == '') {
                    swal('', 'Please select finalize date.')
                }
                else {
                    var listemp = [];
                    for (var i = 0; i < $scope.AirFare_details.length; i++) {
                        if ($scope.AirFare_details[i].grt_amount != $scope.AirFare_details[i].grt_amount_old) {
                            var v = {
                                grt_amount_old: $scope.AirFare_details[i].grt_amount_old,
                                grt_amount: $scope.AirFare_details[i].grt_amount,
                                criteria_code: $scope.AirFare_details[i].criteria_code,
                                emp_number: $scope.AirFare_details[i].emp_number,
                                update_user: $rootScope.globals.currentUser.username,
                                year_month: $scope.edt.year_month
                            }
                            listemp.push(v);
                        }
                    }


                    $http.post(ENV.apiUrl + "api/Payroll/PayrollPosting/AirFare_update", listemp).then(function (AirFare_update) {
                        $scope.AirFare_update = AirFare_update.data;
                        if (AirFare_update) {
                            var p = {
                                year_month: $scope.edt.year_month,
                                posting_date: $scope.grt_finalize_date,
                                emp_list: $scope.emplist
                            }
                            $http.post(ENV.apiUrl + "api/Payroll/PayrollPosting/AirFarePosting", p).then(function (AirFarePosting) {
                                $scope.AirFarePosting = AirFarePosting.data;
                                if ($scope.AirFarePosting.voucher_msg1 != '' || $scope.AirFarePosting.voucher_msg1 != undefined)
                                    swal('', 'Voucher No :' + $scope.AirFarePosting.voucher_msg1 + ' is Created.');
                                else
                                    swal('', 'Voucher is not created.');
                                $scope.close_btn();
                                $scope.reset_form();
                            });
                        }

                    });
                }
            }

            $scope.txt_change = function (obj) {
                $scope.Gratuity_details_summary = getdistinct_age_group();

            }

            $scope.txt_change_air = function (obj) {
                $scope.Gratuity_details_summary = getdistinct_age_group_air();

            }

            $scope.close_btn = function () {
                $('#myModal').modal('hide');
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.select_date_finalize = function (date) {
                
                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //var date1 = year + "-" + month + "-" + day;
                var date1 = date;
                $scope.finalize_date = date1;
                $scope.grt_finalize_date = date1;
                $scope.af_finalize_date = date1;
            }


            $scope.onlyNumbers = function (event, leave_max) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }

                }

                event.preventDefault();


            };

        }])
})();