﻿(function () {
    'use strict';
    var main = '';
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PublishEmailPayrollCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.busyindicator = false;
            $scope.grid = true;
            $scope.color = '#edefef';
            $scope.rgvtbl = false;
            $scope.btnhide = true;
            $scope.edt = '';



            $http.get(ENV.apiUrl + "api/UpdateHoldEmpStatus/GetDept").then(function (GetDept) {
                $scope.GetDept = GetDept.data;

            });

            $http.get(ENV.apiUrl + "api/UpdateHoldEmpStatus/GetDesg").then(function (GetDesg) {
                $scope.GetDesg = GetDesg.data;

            });

            $http.get(ENV.apiUrl + "api/UpdateHoldEmpStatus/GetYearMonth").then(function (GetYearMonth) {
                $scope.GetYearMonth = GetYearMonth.data;

            });

            $scope.Show_data = function () {

                $http.post(ENV.apiUrl + "api/UpdateHoldEmpStatus/PaysheetDetails?dept=" + $scope.edt.dep_code + "&desg=" + $scope.edt.dg_code + "&year_month=" + $scope.edt.year_month + "&empcode=" + $scope.edt.emp_code).then(function (PaysheetDetails) {
                    $scope.PaysheetDetails = PaysheetDetails.data;

                    if ($scope.PaysheetDetails.length > 0) {
                        $scope.btnhide = false;
                        $scope.rgvtbl = true;
                    }
                    else {
                        $scope.rgvtbl = false;
                        $scope.btnhide = true;
                        swal('', 'Record not found.');
                    }
                });


            }




            $scope.publish = function () {

                var datacoll = [];
                for (var i = 0; i < $scope.PaysheetDetails.length; i++) {
                    if ($scope.PaysheetDetails[i].onhold_status == true) {
                        var data = {
                            emp_code: $scope.PaysheetDetails[i].emp_code,
                            year_month: $scope.PaysheetDetails[i].year_month,
                            onhold_status: $scope.PaysheetDetails[i].onhold_status,
                            pe_remark: $scope.PaysheetDetails[i].pe_remark
                        }
                        datacoll.push(data);
                    }                    
                }

                console.log(datacoll);
                if (datacoll.length > 0) {
                    $http.post(ENV.apiUrl + "api/UpdateHoldEmpStatus/publishPayroll", datacoll).then(function (msg1) {
                        $scope.msg = msg1.data;
                        if ($scope.msg)
                            swal({ title: 'Alert', text: 'Record updated successfully', width: 380 });
                        $scope.reset_form();
                    });
                }
                else {
                    swal({title:'Alert', text:'Please select at least one record',width:380});
                }                
            }

            $scope.publish_email = function () {
                var datacoll = [];
                for (var i = 0; i < $scope.PaysheetDetails.length; i++) {
                    if ($scope.PaysheetDetails[i].onhold_status == true) {
                        var data = {
                            emp_code: $scope.PaysheetDetails[i].emp_code,
                            year_month: $scope.PaysheetDetails[i].year_month,
                            onhold_status: $scope.PaysheetDetails[i].onhold_status,
                            pe_remark: $scope.PaysheetDetails[i].pe_remark
                        }
                        datacoll.push(data);
                    }
                }
                console.log(datacoll);
                if (datacoll.length > 0) {
                    $http.post(ENV.apiUrl + "api/UpdateHoldEmpStatus/publishEmailPayroll", datacoll).then(function (msg1) {
                        $scope.msg = msg1.data;
                        if ($scope.msg)
                            swal({ title: 'Alert', text: 'Record updated successfully', width: 380 });
                        $scope.reset_form();
                    });
                }
                else {
                    swal({ title: 'Alert', text: 'Please select at least one record', width: 380 });
                }
            }

            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');

                for (var i = 0; i < $scope.PaysheetDetails.length; i++) {
                    if (main.checked == true) {
                        $scope.PaysheetDetails[i].onhold_status = true;
                        $('tr').addClass("row_selected");
                    }

                    else {
                        $scope.PaysheetDetails[i].onhold_status = false;
                        $('tr').removeClass("row_selected");

                    }

                }
            }


            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);


            $scope.reset_form = function () {
                $scope.edt = '';
                $scope.PaysheetDetails = [];
                $scope.btnhide = true;
                $scope.rgvtbl = false;

            }


        }])
})();