﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SipEvaluationScheduleCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.pager = true;
            $scope.save_btn = true;
            $scope.search_btn = true;
            $scope.clr_btn = true;
            $scope.Update_btn = false;
            $scope.newdisplay = false;
            $scope.table = true;
            $scope.userText = false;
            $scope.copy1 = "";
            //$scope.mainPage = true;
            $scope.studentInfo = false;
            $scope.staffInfo = false;
            $scope.StudChecked = true;
            $scope.temp = {};

            $http.get(ENV.apiUrl + "api/SipEvaluation/getAllSipEvalSchedule").then(function (res1) {
                $scope.Sip_Eval_Schedule = res1.data;
                $scope.totalItems = $scope.Sip_Eval_Schedule.length;
                $scope.todos = $scope.Sip_Eval_Schedule;
                $scope.makeTodos();
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                   
                });
            }

           
            $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasureAchievement/getAcademicYear").then(function (AcademicYear) {
                $scope.Academic_Year = AcademicYear.data;
                
                if (AcademicYear.data.length > 0) {
                    $scope.temp['sims_goal_kpi_academic_year'] = AcademicYear.data[0].sims_goal_kpi_academic_year;
                }

              
                //$scope.temp = {
                //    sims_goal_kpi_academic_year: $scope.Academic_Year[0].sims_goal_kpi_academic_year,
                //}
                
            });

            $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasureAchievement/getGoalName").then(function (GoalName) {
                $scope.Goal_Name = GoalName.data;
               
            });

            $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasureAchievement/getMeasure").then(function (measureData) {
                $scope.measure_Data = measureData.data;
               
            });

            $scope.getGoalTarget = function (goalCode) {
                $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasureAchievement/getGoalTargetName?sims_sip_goal_code=" + goalCode).then(function (goalTarget) {
                    $scope.goal_Target = goalTarget.data;
                   
                });
            }

            $http.get(ENV.apiUrl + "api/SipEvaluation/getReviewer").then(function (reviewData) {
                $scope.review_Data = reviewData.data;
               
            });

            $scope.getStudentInfo = function (accYear, gradeCode, secCode) {
                $http.get(ENV.apiUrl + "api/SipEvaluation/getUserInfo?academic_year=" + accYear + "&grade_code=" + gradeCode + "&section_code=" + secCode).then(function (userName) {
                    $scope.user_Name = userName.data;
                  
                });
            }

            $scope.getStaffInfo = function (company_code, dept_code, desg_code) {
                $http.get(ENV.apiUrl + "api/SipEvaluation/getStaffInfo?company_code=" + company_code + "&dept_code=" + dept_code + "&desg_code=" + desg_code).then(function (userName) {
                    $scope.user_Name = userName.data;
                   
                });
            }

            $scope.getStudentInfoByUserCode = function (user_code) {
                $http.get(ENV.apiUrl + "api/SipEvaluation/getUserInfoByUserCode?user_code=" + user_code).then(function (StudName) {
                    $scope.user_Name = StudName.data;
                   
                });
            }

            $scope.clear = function () { $scope.temp = ''; }

            $scope.size = function (str) {
              
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
               $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

               

                $scope.filteredTodos = $scope.todos.slice(begin, end);


                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    $scope.filteredTodos[i].icon = 'fa fa-plus-circle';
                }
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Sip_Eval_Schedule, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Sip_Eval_Schedule;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_sip_user_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.emp_name1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.emp_name2.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_sip_goal_target_schedule1 == toSearch) ? true : false;
            }

            $scope.New = function () {
                var x = document.getElementById("yes1").checked;
              
                $scope.student_staff_radio_btn = true;
                $scope.staffInfo = false;
                $scope.studentInfo = false;
                $scope.user_dropdown = true;
                $scope.userText = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.goalreadonly = false;
                $scope.targetreadonly = false;
                $scope.table = false;
                $scope.newdisplay = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;


                $scope.temp = {};

              
                if ($scope.Academic_Year.length > 0) {
                    $scope.temp['sims_goal_kpi_academic_year'] = $scope.Academic_Year[0].sims_goal_kpi_academic_year;
                }

                var dt = new Date();
                $scope.temp.sims_sip_goal_target_schedule1 = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                $scope.temp.sims_sip_goal_target_schedule2 = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                $scope.temp.sims_sip_goal_target_schedule3 = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                $scope.temp.sims_sip_goal_target_schedule4 = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                $scope.temp.sims_sip_goal_target_schedule5 = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                               //$scope.temp = "";
            }


            var datasend = []; var data;
            
            $scope.savedata = function (Myform) {

                
                if (Myform) {
                    var data = $scope.temp;
                    data.opr = "I";

                    //for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    //    if ($scope.filteredTodos[i].sims_enrollment_number != $scope.temp.enroll_number) {


                    datasend.push(data);

                    $http.post(ENV.apiUrl + "api/SipEvaluation/CUDSipEvaluationSchedule", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        $http.get(ENV.apiUrl + "api/SipEvaluation/getAllSipEvalSchedule").then(function (res1) {
                            $scope.Sip_Eval_Schedule = res1.data;
                            $scope.totalItems = $scope.Sip_Eval_Schedule.length;
                            $scope.todos = $scope.Sip_Eval_Schedule;
                            $scope.makeTodos();

                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });

                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.newdisplay = false;
                    // }
                    // }
                    //else {

                    //    swal({ text: 'Record Already Exist', width: 300, height: 300 });
                    //}
                }
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.copy1 = "";
                $scope.table = true;
                $scope.newdisplay = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            $scope.clear = function () { $scope.temp = ''; }

            $scope.edit = function (str) {
                
                $scope.student_staff_radio_btn = false;
                $scope.user_dropdown = false;
                $scope.userText = true;
                $scope.user_Text_readOnly = true;
                $scope.staffInfo = false;
                $scope.studentInfo = false;
                $scope.getGoalTarget(str.sims_kpi_goal_code);
                $scope.table = false;
                $scope.newdisplay = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.goalreadonly = true;
                $scope.targetreadonly = true;
                var staffName = str.sims_sip_user_name;
                $scope.temp = {
                    sims_kpi_goal_code: str.sims_kpi_goal_code,
                    sims_kpi_goal_target_code: str.sims_kpi_goal_target_code,
                    sims_sip_user_code: str.sims_sip_user_code,
                    sims_user_name: staffName,
                    old_sims_sip_user_code: str.sims_sip_user_code,
                    sims_sip_goal_target_reviewer1_code: str.sims_sip_goal_target_reviewer1_code,
                    sims_sip_goal_target_reviewer2_code: str.sims_sip_goal_target_reviewer2_code,
                    sims_sip_goal_target_reviewer3_code: str.sims_sip_goal_target_reviewer3_code,
                    sims_sip_goal_target_reviewer4_code: str.sims_sip_goal_target_reviewer4_code,
                    sims_sip_goal_target_reviewer5_code: str.sims_sip_goal_target_reviewer5_code,
                    sims_sip_goal_target_schedule1: str.sims_sip_goal_target_schedule1,
                    sims_sip_goal_target_schedule2: str.sims_sip_goal_target_schedule2,
                    sims_sip_goal_target_schedule3: str.sims_sip_goal_target_schedule3,
                    sims_sip_goal_target_schedule4: str.sims_sip_goal_target_schedule4,
                    sims_sip_goal_target_schedule5: str.sims_sip_goal_target_schedule5,
                };
                $scope.getStudentInfoByUserCode(str.sims_sip_user_code);
            }

            var dataupdate = []; var data;
            $scope.update = function () {//Myform

                // if (Myform) {
                var data = $scope.temp;
                data.opr = "U";

                dataupdate.push(data);
                $http.post(ENV.apiUrl + "api/SipEvaluation/CUDSipEvaluationSchedule", dataupdate).then(function (msg) {
                    $scope.msg1 = msg.data;

                    $http.get(ENV.apiUrl + "api/SipEvaluation/getAllSipEvalSchedule").then(function (res1) {
                        $scope.Sip_Eval_Schedule = res1.data;
                        $scope.totalItems = $scope.Sip_Eval_Schedule.length;
                        $scope.todos = $scope.Sip_Eval_Schedule;
                        $scope.makeTodos();

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                });
                dataupdate = [];
                $scope.table = true;
                $scope.newdisplay = false;
                //   }
            }

            $scope.radioclick = function (str) {

                if (str == 'New') {
                    
                    $scope.getGrade = function (accYear, cur_code) {
                        $http.get(ENV.apiUrl + "api/common/getAllGrades?academic_year=" + $scope.temp.sims_goal_kpi_academic_year + "&cur_code=" + '01').then(function (Gradecode) {
                            $scope.Grade_code = Gradecode.data;
                           
                        });
                    }

                    $scope.getSection = function (cur_code, grade_code, accYear) {
                        $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + '01' + "&grade_code=" + $scope.temp.sims_grade_code + "&academic_year=" + $scope.temp.sims_goal_kpi_academic_year).then(function (Sectiondata) {
                            $scope.Section_data = Sectiondata.data;
                           
                        });
                    }

                    $scope.search_btn = true;
                    $scope.clr_btn = true;

                    $scope.studentInfo = true;
                    $scope.staffInfo = false;
                    $scope.edt = '';
                }
                else {

                    $http.get(ENV.apiUrl + "api/SipEvaluation/getCompanyName").then(function (res1) {
                        $scope.comp = res1.data;
                       
                    });

                    $http.get(ENV.apiUrl + "api/SipEvaluation/getDepartment").then(function (res1) {
                        $scope.dept = res1.data;
                       
                    });

                    $http.get(ENV.apiUrl + "api/SipEvaluation/getDesigantion").then(function (res1) {
                        $scope.desg = res1.data;
                        
                    });

                    $scope.staffInfo = true;
                    $scope.studentInfo = false;
                }

            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            var dom;
            $scope.flag = true;
            $scope.expand = function (info, $event) {

                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0' width='100%' style='border:solid;border-width:02px'>" +
                        "<tbody>" +

                         "<tr> <td class='semi-bold'>" + "Reviewer 3" + "</td> <td class='semi-bold'>" + "Date Schedule 3" + " </td><td class='semi-bold'>" + "Reviewer 4" + "</td><td class='semi-bold'>" + "Date Schedule 4" + "</td>" +
                         "<td class='semi-bold'>" + "Reviewer 5" + "</td> <td class='semi-bold'>" + "Date Schedule 5" + "</td>" +
                        "</tr>" +

                         "<tr><td>" + (info.emp_name3) + "</td> <td>" + (info.sims_sip_goal_target_schedule3) + " </td><td>" + (info.emp_name4) + "</td><td> " + (info.sims_sip_goal_target_schedule4) + "</td>" +
                        "<td>" + (info.emp_name5) + "</td><td>" + (info.sims_sip_goal_target_schedule5) + " </td>" + "</td>" +
                        "</tr>" +

                        " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }

                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }
            }

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.Sip_Eval_Schedule;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str; //$scope.makeTodos();
            }

            //Code for default date display

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;

          
            $scope.temp = {
                sims_sip_goal_target_schedule1: dd + '-' + mm + '-' + yyyy,
                sims_sip_goal_target_schedule2: dd + '-' + mm + '-' + yyyy,
                sims_sip_goal_target_schedule3: dd + '-' + mm + '-' + yyyy,
                sims_sip_goal_target_schedule4: dd + '-' + mm + '-' + yyyy,
                sims_sip_goal_target_schedule5: dd + '-' + mm + '-' + yyyy
            }

        }])

})();
