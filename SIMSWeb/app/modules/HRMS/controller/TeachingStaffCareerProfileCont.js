﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.HRMS');

    simsController.controller('TeachingStaffCareerProfileCont',
        ['$scope', '$compile', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$locale', '$http', 'ENV', function ($scope, $compile, $state, $rootScope, $timeout, gettextCatalog, $locale, $http, ENV) {

            var username = $rootScope.globals.currentUser.username;
            $scope.Emp_ID = username;
           // $scope.Emp_ID = 'EP1015';
            $scope.updatebtn = true;
            $scope.imgupload = true;

            if ($http.defaults.headers.common['schoolId'] == 'clarionschool') {
                
                $scope.updatebtn = false;
                $scope.imgupload = false;

            }
            else {
                $scope.updatebtn = true;
                $scope.imgupload = true;

            }

            $http.get(ENV.apiUrl + "api/EmpProfile/GetEPData").then(function (res) {
                $scope.obj = res.data;
                $scope.operation = true;
                $scope.Save_btn = true;
                $scope.display = true;
            });

            $http.get(ENV.apiUrl + "api/HRMS/getCompany").then(function (resc) {
                $scope.company_data = resc.data;
            });

            $scope.getdata = function () {

                $http.get(ENV.apiUrl + "api/EmpProfile/GetEmployeeProfileData?em_login_code=" + $scope.Emp_ID).then(function (res) {
                    $scope.temp1 = res.data;
                    $scope.temp = {
                        em_login_code: $scope.temp1[0].em_login_code,
                        em_number: $scope.temp1[0].em_number,
                        em_service_status_code: $scope.temp1[0].em_service_status_code,
                        em_status_code: $scope.temp1[0].em_status_code,
                        em_apartment_number: $scope.temp1[0].em_apartment_number,
                        em_family_name: $scope.temp1[0].em_family_name,
                        em_street_number: $scope.temp1[0].em_street_number,
                        em_area_number: $scope.temp1[0].em_area_number,
                        em_phone: $scope.temp1[0].em_phone,
                        em_fax: $scope.temp1[0].em_fax,
                        em_post: $scope.temp1[0].em_post,
                        em_emergency_contact_name2: $scope.temp1[0].em_emergency_contact_name2,
                        em_emergency_contact_number2: $scope.temp1[0].em_emergency_contact_number2,
                        em_joining_ref: $scope.temp1[0].em_joining_ref,
                        em_ethnicity_code: $scope.temp1[0].em_ethnicity_code,
                        em_handicap_status: $scope.temp1[0].em_handicap_status,
                        em_stop_salary_indicator: $scope.temp1[0].em_stop_salary_indicator,
                        em_agreement: $scope.temp1[0].em_agreement,
                        em_punching_status: $scope.temp1[0].em_punching_status,
                        em_bank_cash_tag: $scope.temp1[0].em_bank_cash_tag,
                        em_citi_exp_tag: $scope.temp1[0].em_citi_exp_tag,
                        em_leave_tag: $scope.temp1[0].em_leave_tag,
                        em_passport_issue_place: $scope.temp1[0].em_passport_issue_place,
                        em_passport_issuing_authority: $scope.temp1[0].em_passport_issuing_authority,
                        em_pssport_exp_rem_date: $scope.temp1[0].em_pssport_exp_rem_date,
                        em_visa_issuing_place: $scope.temp1[0].em_visa_issuing_place,
                        em_visa_issuing_authority: $scope.temp1[0].em_visa_issuing_authority,
                        em_visa_exp_rem_date: $scope.temp1[0].em_visa_exp_rem_date,
                        em_visa_type_code: $scope.temp1[0].em_visa_type_code,
                        em_agreement_start_date: $scope.temp1[0].em_agreement_start_date,
                        em_agreement_exp_date: $scope.temp1[0].em_agreement_exp_date,
                        em_agreemet_exp_rem_date: $scope.temp1[0].em_agreemet_exp_rem_date,
                        em_punching_id: $scope.temp1[0].em_punching_id,
                        em_dependant_full: $scope.temp1[0].em_dependant_full,
                        em_dependant_half: $scope.temp1[0].em_dependant_half,
                        em_dependant_infant: $scope.temp1[0].em_dependant_infant,
                        em_Bank_Code: $scope.temp1[0].em_Bank_Code,
                        em_bank_ac_no: $scope.temp1[0].em_bank_ac_no,
                        em_iban_no: $scope.temp1[0].em_iban_no,
                        em_route_code: $scope.temp1[0].em_route_code,
                        em_bank_swift_code: $scope.temp1[0].em_bank_swift_code,
                        em_gpf_ac_no: $scope.temp1[0].em_gpf_ac_no,
                        em_pan_no: $scope.temp1[0].em_pan_no,
                        em_labour_card_no: $scope.temp1[0].em_labour_card_no,
                        em_gosi_ac_no: $scope.temp1[0].em_gosi_ac_no,
                        em_gosi_start_date: $scope.temp1[0].em_gosi_start_date,
                        em_national_id: $scope.temp1[0].em_national_id,
                        em_secret_question_code: $scope.temp1[0].em_secret_question_code,
                        em_secret_answer: $scope.temp1[0].em_secret_answer,

                        em_building_number: $scope.temp1[0].em_building_number,
                        em_Company_Code: $scope.temp1[0].em_Company_Code,
                        em_Dept_Code: $scope.temp1[0].em_Dept_Code,
                        em_Designation_Code: $scope.temp1[0].em_Designation_Code,
                        em_Dest_Code: $scope.temp1[0].em_Dest_Code,
                        em_Grade_Code: $scope.temp1[0].em_Grade_Code,
                        em_Marital_Status_Code: $scope.temp1[0].em_Marital_Status_Code,
                        em_Nation_Code: $scope.temp1[0].em_Nation_Code,
                        em_Country_Code: $scope.temp1[0].em_Country_Code,
                        em_State_Code: $scope.temp1[0].em_State_Code,
                        em_City_Code: $scope.temp1[0].em_City_Code,
                        em_Religion_Code: $scope.temp1[0].em_Religion_Code,
                        em_Salutation_Code: $scope.temp1[0].em_Salutation_Code,
                        em_Sex_Code: $scope.temp1[0].em_Sex_Code,
                        em_Staff_Type_Code: $scope.temp1[0].em_Staff_Type_Code,
                        em_blood_group_code: $scope.temp1[0].em_blood_group_code,
                        em_date_of_birth: $scope.temp1[0].em_date_of_birth,
                        em_date_of_join: $scope.temp1[0].em_date_of_join,
                        em_dept_effect_from: $scope.temp1[0].em_dept_effect_from,
                        em_email: $scope.temp1[0].em_email,
                        em_emergency_contact_name1: $scope.temp1[0].em_emergency_contact_name1,
                        em_emergency_contact_number1: $scope.temp1[0].em_emergency_contact_number1,
                        em_first_name: $scope.temp1[0].em_first_name,
                        em_grade_effect_from: $scope.temp1[0].em_grade_effect_from,
                        em_last_name: $scope.temp1[0].em_last_name,
                        em_middle_name: $scope.temp1[0].em_middle_name,
                        em_mobile: $scope.temp1[0].em_mobile,
                        em_name_ot: $scope.temp1[0].em_name_ot,
                        em_national_id_expiry_date: $scope.temp1[0].em_national_id_expiry_date,
                        em_national_id_issue_date: $scope.temp1[0].em_national_id_issue_date,
                        em_passport_exp_date: $scope.temp1[0].em_passport_exp_date,
                        em_passport_issue_date: $scope.temp1[0].em_passport_issue_date,
                        em_passport_no: $scope.temp1[0].em_passport_no,
                        em_summary_address: $scope.temp1[0].em_summary_address,
                        em_summary_address_local_language: $scope.temp1[0].em_summary_address_local_language,
                        em_visa_exp_date: $scope.temp1[0].em_visa_exp_date,
                        em_stop_salary_from: $scope.temp1[0].em_stop_salary_from,
                        em_visa_issue_date: $scope.temp1[0].em_visa_issue_date,
                        em_visa_no: $scope.temp1[0].em_visa_no,
                        em_img: $scope.temp1[0].em_img,
                        em_resident_in_qatar_since: $scope.temp1[0].em_resident_in_qatar_since,
                        em_remarks_awards: $scope.temp1[0].em_remarks_awards,
                        em_name_of_spouse: $scope.temp1[0].em_name_of_spouse,
                        em_Spouse_Designation_Code: $scope.temp1[0].em_Spouse_Designation_Code,
                        em_spouse_organization: $scope.temp1[0].em_spouse_organization,
                        em_spouse_qatar_id: $scope.temp1[0].em_spouse_qatar_id,
                        em_spouse_contact_number: $scope.temp1[0].em_spouse_contact_number,
                        pays_work_type_code: $scope.temp1[0].pays_work_type_code,
                        pays_work_type_name: $scope.temp1[0].pays_work_type_name,
                        em_labour_card_no: $scope.temp1[0].em_labour_card_no,
                        em_work_permit_issue_date: $scope.temp1[0].em_work_permit_issue_date,
                        em_work_permit_expiry_date: $scope.temp1[0].em_work_permit_expiry_date,
                        em_personalemail: $scope.temp1[0].em_personalemail                        
                    }
                   
                    //  $scope.prev_img = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/' + $scope.temp1[0].em_img;                    
                    var num = Math.random();
                    var imgSrc = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/' + $scope.temp1[0].em_img + "?v=" + num;
                    $(function () {
                        $('#empimg').attr("src", imgSrc);
                    })

                    $scope.getQualiByEmpcode($scope.Emp_ID);
                    $scope.getExpByEmpcode($scope.Emp_ID);
                });
            }

            $scope.getdata();

            $scope.getQualiByEmpcode = function (empcode) {
                $http.get(ENV.apiUrl + "api/Qualification/getQualificationByEmployee?emp_code=" + empcode).then(function (res1) {
                    $scope.qualiList = res1.data;
                    $scope.showQualiMsg = false;
                    if ($scope.qualiList.length > 0) {
                        $scope.showQualiMsg = false;
                    }
                    else {
                        $scope.showQualiMsg = true;
                    }                    
                });             
            }

            $scope.getExpByEmpcode = function (empcode) {
                $http.get(ENV.apiUrl + "api/EmpProfile/GetSpecificEmployeeExp?em_login_code=" + empcode).then(function (res1) {
                    $scope.expList = res1.data;                    
                    $scope.experience = 0;
                    $scope.showMsg = false;
                    if ($scope.expList.length > 0) {
                        //$scope.total_experience = $scope.expList[0].total_experience;

                        angular.forEach($scope.expList, function (value, key) {                            
                            $scope.experience = parseFloat($scope.experience) + parseFloat(value.total_experience);
                        });

                        $scope.total_experience = $scope.experience;
                        $scope.showMsg = false;
                    }
                    else {
                        $scope.showMsg = true;
                    }                    
                });
            }

            $scope.UpdateData = function (Myform) {
                
                if (Myform) {
                    var data = $scope.temp;
                    if ($scope.photo_filename === undefined) {
                    }
                    else {
                        $scope.temp.em_img = '';
                        //data.em_img ='.' + $scope.photo_filename.split("/")[1];
                        data.em_img = $scope.temp.em_login_code + '.' + $scope.photo_filename.split("/")[1];

                    }
                    data.opr = 'U';
                    
                    $http.post(ENV.apiUrl + "api/EmpProfile/UpdateProile", data).then(function (res) {
                        $scope.CUDobj = res.data;
                        swal({ text: $scope.CUDobj.strMessage, timer: 10000 ,width:360,});
                        //$state.reload();
                        $state.go("main.EmpDshBrd");
                        $scope.getdata();                        
                        
                    });

                    if ($scope.ImgLoaded == true) {
                        var request = {
                            method: 'POST',
                            url: ENV.apiUrl + '/api/file/upload?filename=' + $scope.temp.em_login_code + "&location=" + "/EmployeeImages",
                            data: formdata,
                            headers: {
                                'Content-Type': undefined
                            }
                        };
                        $http(request).success(function (d) {
                        });
                    }
                    $scope.searchText = false;
                }
                else {
                    swal({ text: 'Kindly fill mandatory field', timer: 5000, width: 360 });
                }
            }

            $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/';

            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                    var i = 0;
                    if ($files[key].size > 200000) {
                        $scope.filesize = false;
                        swal({ title: "Alert", text: "File Should Not Exceed 2 Mb.", width: 320, height: 280 });
                    }
                });
            };

            $scope.file_changed = function (element) {
                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        //                        $scope.prev_img = e.target.result;
                        var imgSrc = e.target.result;
                        $(function () {
                            $('#empimg').attr("src", imgSrc);
                        })
                    });
                };
                reader.readAsDataURL(photofile);
            };

            $scope.image_click = function () {
                $scope.ImgLoaded = true;
            }

            simsController.directive('ngFiles', ['$parse', function ($parse) {

                function fn_link(scope, element, attrs) {
                    var onChange = $parse(attrs.ngFiles);
                    element.on('change', function (event) {
                        onChange(scope, { $files: event.target.files });
                    });
                };

                return {
                    link: fn_link
                }
            }])

            simsController.filter('randomSrc', function () {
                return function (input) {
                    if (input)
                        return input + '?r=' + Math.random();
                }
            })

        }]);
})();

