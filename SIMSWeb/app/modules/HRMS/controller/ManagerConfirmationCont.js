﻿(function () {
    'use strict';
    var opr = '';
    var mngrcode = [];
    var main;

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ManagerConfirmationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';

            $scope.table1 = true;

            $http.get(ENV.apiUrl + "api/ManagerConfirmation/getMngrConfirmation").then(function (getMngrConfirmation_Data) {
                $scope.MngrConfirmation = getMngrConfirmation_Data.data;
               // console.log("MGR Data:", $scope.MngrConfirmation);
                $scope.totalItems = $scope.MngrConfirmation.length;
                $scope.todos = $scope.MngrConfirmation;
                $scope.makeTodos();

            });

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/ManagerConfirmation/getMngrConfirmation").then(function (getMngrConfirmation_Data) {
                    $scope.MngrConfirmation = getMngrConfirmation_Data.data;
                    // console.log("MGR Data:", $scope.MngrConfirmation);
                    $scope.totalItems = $scope.MngrConfirmation.length;
                    $scope.todos = $scope.MngrConfirmation;
                    $scope.makeTodos();

                });
            }

            
            $http.get(ENV.apiUrl + "api/common/getCompanyName").then(function (getCompanyName_Data) {
                $scope.CompanyName_Data = getCompanyName_Data.data;
                //console.log($scope.CompanyName_Data);
            });

            $scope.GetLoandesc = function () {
                
                $http.get(ENV.apiUrl + "api/common/getAllEmpName?Company_code=" + $scope.edt.mc_company_code).then(function (res) {
                    $scope.LoanEmpName_Data = res.data;
                   
                });
            }

            $scope.operation = false;
            $scope.editmode = false;
            $scope.size = function (str) {
              
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;  $scope.makeTodos();
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
            }
            $scope.edt = "";

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.editmode = false;
                opr = 'S';
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;


            }

            $scope.up = function (str) {
                opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.operation = true;

                $scope.edt = str;
                $scope.GetLoandesc();
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
            }

            $scope.Save = function (myForm) {
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'I';
                    
                    $scope.exist = false;
                    for (var i = 0; i < $scope.MngrConfirmation.length; i++) {
                        if ($scope.MngrConfirmation[i].mc_number == data.mc_number) {
                            $scope.exist = true;
                        }
                    }

                    if ($scope.exist) {
                        swal({ text: "Record Already Exits", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }
                    //if ($scope.exist) {
                    //    $rootScope.strMessage = 'Record Already exists';

                    //    $('#message').modal('show');
                    //}

                    else {
                       
                    $http.post(ENV.apiUrl + "api/ManagerConfirmation/ManagerConfirmationCUD", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        //console.log($scope.msg1);
                            $scope.operation = false
                            $scope.msg1 = msg.data;
                           
                        
                          
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                    $scope.getgrid();
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                                
                          

                            //if ($scope.msg1 == true) {

                            //    $rootScope.strMessage = 'Record Inserted Successfully';

                            //    $('#message').modal('show');
                            //}
                            //else {

                            //    $rootScope.strMessage = 'Record not Inserted';

                            //    $('#message').modal('show');
                            //}
                           
                            $http.get(ENV.apiUrl + "api/ManagerConfirmation/getMngrConfirmation").then(function (getMngrConfirmation_Data) {
                                $scope.MngrConfirmation = getMngrConfirmation_Data.data;
                                //console.log("MGR Data:",$scope.MngrConfirmation);
                                $scope.totalItems = $scope.MngrConfirmation.length;
                                $scope.todos = $scope.MngrConfirmation;
                                $scope.makeTodos();
                            });
                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                }
            }

            $scope.Update = function () {

                var data = $scope.edt;
                data.opr = 'U';

                $http.post(ENV.apiUrl + "api/ManagerConfirmation/ManagerConfirmationCUD", data).then(function (msg) {
                   
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                    //if ($scope.msg1 == true) {

                    //    $rootScope.strMessage = 'Record Updated Successfully';
                    //    $('#message').modal('show');
                    //}
                    //else {

                    //    $rootScope.strMessage = 'Record Not Updated';
                    //    $('#message').modal('show');
                    //}
                    $scope.operation = false;
                   
                    $http.get(ENV.apiUrl + "api/ManagerConfirmation/getMngrConfirmation").then(function (getMngrConfirmation_Data) {
                        $scope.MngrConfirmation = getMngrConfirmation_Data.data;
                       // console.log("MGR Data:", $scope.MngrConfirmation);
                        $scope.totalItems = $scope.MngrConfirmation.length;
                        $scope.todos = $scope.MngrConfirmation;
                        $scope.makeTodos();
                     
                    });

                })

                $scope.operation = false;
                $scope.table1 = true;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

              
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].mc_number;
                        var v = document.getElementById(t);
                        v.checked = true;
                        mngrcode = mngrcode + $scope.filteredTodos[i].mc_number + ','
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].mc_number;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
            }

            $scope.deleterecord = function () {

                mngrcode = [];

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].mc_number;
                    var v = document.getElementById(t);

                    if (v.checked == true)

                        mngrcode = mngrcode + $scope.filteredTodos[i].mc_number + ',';
                }

                var deletelocacode = ({
                    'mc_number': mngrcode,
                    'opr': 'D'
                });

                $http.post(ENV.apiUrl + "api/ManagerConfirmation/ManagerConfirmationCUD", deletelocacode).then(function (msg) {
                    $scope.msg1 = msg.data;
                    $scope.table1 = true;
                    $scope.operation = false;
                    $rootScope.strMessage = $scope.msg1.strMessage;
                    $http.get(ENV.apiUrl + "api/ManagerConfirmation/getMngrConfirmation").then(function (getMngrConfirmation_Data) {
                        $scope.MngrConfirmation = getMngrConfirmation_Data.data;
                        $scope.totalItems = $scope.MngrConfirmation.length;
                        $scope.todos = $scope.MngrConfirmation;
                        $scope.makeTodos();

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 })
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 })
                        }
                        else
                        {
                            swal("Error-" + $scope.msg1)
                        }
                        //if ($scope.msg1 == true) {
                        //    $rootScope.strMessage = 'Record Deleted Successfully';
                        //    $('#message').modal('show');
                        //}
                        //else {
                        //    $rootScope.strMessage = 'Record Not Deleted';
                        //    $('#message').modal('show');
                        //}

                    });
                });

            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.MngrConfirmation, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.MngrConfirmation;
                }
                $scope.makeTodos();
            }

         

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                     item.employee_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.co_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.mc_old_basic.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.mc_new_basic.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     // item.mc_old_basic == toSearch ||
                    // item.mc_new_basic == toSearch ||
                     item.mc_number == toSearch) ? true : false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


  
            //var date = new Date();
            //var month = (date.getMonth() + 1);
            //var day = date.getDate();
            //if (month < 10)
            //    month = "0" + month;
            //if (day < 10)
            //    day = "0" + day;
            //var todaydate = (day) + '-' + (month) + '-' + date.getFullYear()
            //$scope.edt = { mc_effect_from: todaydate, mc_retro_effect_from: todaydate };



            $scope.createdate = function (end_date, start_date, name) {

              
                //var month1 = end_date.split("/")[0];
                //var day1 = end_date.split("/")[1];
                //var year1 = end_date.split("/")[2];
                //var new_end_date = year1 + "/" + month1 + "/" + day1;
                var new_end_date = end_date;

                //var year = start_date.split("/")[0];
                //var month = start_date.split("/")[1];
                //var day = start_date.split("/")[2];
                //var new_start_date = year + "/" + month + "/" + day;
                var new_start_date = start_date;

                if (new_end_date < new_start_date) {

                    $rootScope.strMessage = "Please Select Future Date";
                    $('#message').modal('show');

                    $scope.edt[name] = '';
                }
                else {

                    $scope.edt[name] = new_end_date;
                }
            }


            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!  edt.
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.edt = {
                mc_effect_from: dd + '-' + mm + '-' + yyyy,
                mc_retro_effect_from: dd + '-' + mm + '-' + yyyy,
            }
            $scope.edt = {
                mc_effect_from: dd + '-' + mm + '-' + yyyy,
                mc_retro_effect_from: dd + '-' + mm + '-' + yyyy,
            }



            $scope.showdate = function (date, name) {

                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //$scope.edt[name] = year + "/" + month + "/" + day;
                $scope.edt[name] = date;

            }
        }])
})();
