﻿
(function () {
    'use strict';

    var main;
    var subject_code = [], subject_code1 = [], sib_enroll_code = [];;

    var cur_code;
    var section_code, sectioncodemodal1;
    var simsController = angular.module('sims.module.Hrms');
    simsController.controller('EmolpyeeLeaveApplicationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var username1 = $rootScope.globals.currentUser.username;
            $http.get(ENV.apiUrl + "api/leavedetails/Get_EMPLeaveDetails").then(function (Get_EMPLeaveDetails) {
                $scope.EMPLeaveDetails = Get_EMPLeaveDetails.data;
               
            });

            $http.get(ENV.apiUrl + "api/leavedetails/Get_EmpInfo?empid=" + username1).then(function (Get_EmpInfo) {
                $scope.GetEmpInfo = Get_EmpInfo.data;
                
            });
            
            $scope.SaveLeaveDetails = function (isvalid) {
                if (isvalid) {
                    var month = $scope.edt.lt_strat_date.split("/")[0];
                    var day = $scope.edt.lt_strat_date.split("/")[1];
                    var year = $scope.edt.lt_strat_date.split("/")[2];

                    var start_date = year + '/' + month + '/' + day;


                    var month = $scope.edt.lt_end_date.split("/")[0];
                    var day = $scope.edt.lt_end_date.split("/")[1];
                    var year = $scope.edt.lt_end_date.split("/")[2];

                    var lt_end_date = year + '/' + month + '/' + day;

                    var data = {
                        em_login_code: $scope.GetEmpInfo.em_login_code,
                        comp_code: $scope.GetEmpInfo.comp_code,
                        leave_code: $scope.edt.leave_type,
                        lt_start_date: start_date,
                        lt_end_date: lt_end_date,
                        lt_days: $scope.edt.allowed_leaves,
                        lt_remarks: $scope.edt.lt_remarks,
                        lt_mgr_confirm_tag: "N",
                        lt_converted_hours: "",
                        lt_hours: $scope.edt.lt_hours,

                    };

                    $http.post(ENV.apiUrl + "api/leavedetails/InsertEMPLeaveDetails", data).then(function (res) {

                        $scope.msg = res.data;

                        if ($scope.msg == true) {
                            $scope.edt = "";
                            swal({
                                text: 'Employee Leave Applied Successfully',
                                imageUrl: "assets/img/check.png",
                                width: 300,
                                height: 300
                            });

                            $scope.edt = "";
                        }
                        else {
                            swal({
                                text: 'Employee Leave Not Applied',
                                imageUrl: "assets/img/close.png",
                                width: 300,
                                height: 300
                            });
                        }
                    });
                }
            }

            $scope.UserSearch = function () {

                $('#Employee').modal('show');
                $http.get(ENV.apiUrl + "api/leavedetails/GetEmployeePendingLeaveDetails?emp_code=" + username1).then(function (Get_EmployeePendingLeaveDetails) {
                    $scope.EmployeePendingLeaveDetails = Get_EmployeePendingLeaveDetails.data;
                    console.log($scope.EmployeePendingLeaveDetails);
                });
            }

            $scope.Getmaxdays = function (str)
            {
                
                for (var i = 0; i < $scope.EMPLeaveDetails.length; i++) {
                    if ($scope.EMPLeaveDetails[i].leave_type == str) {

                        $scope.edt = {
                            leave_type:$scope.EMPLeaveDetails[i].leave_type,
                            maxleave_allowed: $scope.EMPLeaveDetails[i].maxleave_allowed

                        };
                    }
                }

            }

            var diffDays = '';

            $scope.dayDiff = function (firstDate, secondDate,maxday,name1) {
              
                debugger;
                var allowed_leaves1 = 'allowed_leaves';
                var leavesremaining = 'leavesremaining';
                var lt_lwp_leave1 = 'lt_lwp_leave';

                var date1 = new Date(firstDate);
                var date2 = new Date(secondDate);

                $scope.maxday = maxday;
                
                if ($scope.edt.leave_type != undefined) {

                    if (firstDate > secondDate) {

                        $scope.edt[allowed_leaves1] = "";

                        $scope.edt[leavesremaining] = "";

                        $scope.edt[name1] = "";
                        $rootScope.strMessage = "End Date is Less than Start Date";
                        $('#message').modal('show');
                    }
                    else {

                        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                        diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)) + 1;

                        if (maxday - diffDays > 0) {
                            $scope.edt[allowed_leaves1] = diffDays
                            $scope.edt[leavesremaining] = maxday - diffDays;
                            $scope.edt[name1] = secondDate;
                        }
                        else {
                            $scope.edt[allowed_leaves1] = diffDays;
                            $scope.edt[leavesremaining] = "0";
                            $scope.edt[lt_lwp_leave1] = Math.abs(maxday - diffDays);
                            $scope.lt_lwp = Math.abs(maxday - diffDays);
                        }


                    }
                }
                else {

                    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
                    diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)) + 1;
                    $scope.edt[lt_lwp_leave1] = diffDays;
                    $scope.edt[allowed_leaves1] = diffDays;
                  
                }
            }

            $scope.FristdateHalfDay = function (allowed_leaves, leavesremaining,lt_lwpleave)
            {
                debugger;
               
                var leavesremaining1 = 'leavesremaining';
                var allowed_leaves1 = 'allowed_leaves';
                var lt_lwp_leave1 = 'lt_lwp_leave';
                var lt_hours = 'lt_hours';

                var second = document.getElementById('Last_Day_Half_Day');
                var first = document.getElementById('Frist_date_Half_Day');
                if ($scope.edt.leave_type != undefined && $scope.edt.leave_type != "LWP" && !lt_lwpleave > 0) {


                    if (first.checked == true && second.checked == false) {
                        $scope.edt[leavesremaining1] = leavesremaining + 0.50;
                        $scope.edt[allowed_leaves1] = allowed_leaves - 0.50;
                        $scope.edt[lt_hours] = "0.1"
                    }

                    else if (first.checked == true && second.checked == true) {

                        $scope.edt[leavesremaining1] = $scope.maxday - diffDays;
                        $scope.edt[allowed_leaves1] = diffDays;
                        $scope.edt[lt_hours] = "1.1"
                    }
                    else if (first.checked == false && second.checked == true) {

                        $scope.edt[leavesremaining1] = leavesremaining + 0.50;
                        $scope.edt[allowed_leaves1] = allowed_leaves - 0.50;
                        $scope.edt[lt_hours] = "0.1"
                    }
                    else {
                        $scope.edt[leavesremaining1] = $scope.maxday - diffDays;
                        $scope.edt[allowed_leaves1] = diffDays;
                        $scope.edt[lt_hours] = "0"
                    }
                }

                else {
                    var leave_type1 = 'leave_type';

                    $scope.edt[leave_type1] = "LWP";

                    if (first.checked == true && second.checked == false) {
                        $scope.edt[lt_lwp_leave1] = lt_lwpleave - 0.50;
                        $scope.edt[allowed_leaves1] = allowed_leaves - 0.50;
                        $scope.edt[lt_hours] = "1.1"
                    }
                    else if (first.checked == true && second.checked == true) {

                        $scope.edt[lt_lwp_leave1] = $scope.lt_lwp;
                        $scope.edt[allowed_leaves1] = diffDays;
                        $scope.edt[lt_hours] = "0.1"
                    }
                    else if (first.checked == false && second.checked == true) {

                        $scope.edt[lt_lwp_leave1] = lt_lwpleave - 0.50;
                        $scope.edt[allowed_leaves1] = allowed_leaves - 0.50;
                        $scope.edt[lt_hours] = "1.1"
                    }
                    else {
                        if (lt_lwpleave > 0) {
                            $scope.edt[lt_lwp_leave1] = $scope.lt_lwp;
                            $scope.edt[allowed_leaves1] = diffDays;
                            $scope.edt[lt_hours] = "0"
                        }
                        else {
                            $scope.edt[lt_lwp_leave1] = diffDays;
                            $scope.edt[allowed_leaves1] = diffDays;
                            $scope.edt[lt_hours] = "0"
                        }
                    }
                }
            }
            
            $scope.LastDayHalfDay = function (allowed_leaves, leavesremaining, lt_lwpleave) {
              
                debugger;
               
                var leavesremaining1 = 'leavesremaining';
                var allowed_leaves1 = 'allowed_leaves';
                var lt_lwp_leave1 = 'lt_lwp_leave';
                var lt_hours = 'lt_hours';

                var second = document.getElementById('Last_Day_Half_Day');
                var first = document.getElementById('Frist_date_Half_Day');
                if ($scope.edt.leave_type != undefined && $scope.edt.leave_type != "LWP" && !lt_lwpleave>0) {
                    if (second.checked == true && first.checked == false) {

                        $scope.edt[leavesremaining1] = leavesremaining + 0.50;
                        $scope.edt[allowed_leaves1] = allowed_leaves - 0.50;

                        $scope.edt[lt_hours] = "0.1";
                    }
                    else if (first.checked == true && second.checked == true) {

                        if (lt_lwpleave > 0) {
                            $scope.edt[lt_lwp_leave1] = $scope.lt_lwp;
                            $scope.edt[allowed_leaves1] = diffDays;
                            $scope.edt[lt_hours] = "1.1"
                        }
                        else {
                           $scope.edt[leavesremaining1] = $scope.maxday - diffDays;
                            $scope.edt[allowed_leaves1] = diffDays;
                            $scope.edt[lt_hours] = "1.1"
                        }
                    }

                    else if (first.checked == true && second.checked == false) {

                        $scope.edt[leavesremaining1] = leavesremaining + 0.50;
                        $scope.edt[allowed_leaves1] = allowed_leaves - 0.50;

                        $scope.edt[lt_hours] = "0.1";
                    }
                    else {
                        $scope.edt[leavesremaining1] = $scope.maxday - diffDays;
                        $scope.edt[allowed_leaves1] = diffDays;
                        $scope.edt[lt_hours]  = "0";

                    }
                }
             
                else {

                    var leave_type1 = 'leave_type';

                    $scope.edt[leave_type1] = "LWP";

                    if (first.checked == true && second.checked == false) {
                        $scope.edt[lt_lwp_leave1] = lt_lwpleave - 0.50;
                        $scope.edt[allowed_leaves1] = allowed_leaves - 0.50;
                        $scope.edt[lt_hours] = "0.1";
                    }
                    else if (first.checked == true && second.checked == true) {
                        if (lt_lwpleave > 0) {
                            $scope.edt[lt_lwp_leave1] = $scope.lt_lwp;
                            $scope.edt[allowed_leaves1] = diffDays;
                            $scope.edt[lt_hours] = "1.1"
                        }
                        else {
                            $scope.edt[lt_lwp_leave1] = diffDays;
                            $scope.edt[allowed_leaves1] = diffDays;
                            $scope.edt[lt_hours] = "1.1"
                        }
                    }
                    else if (first.checked == false && second.checked == true) {

                        $scope.edt[lt_lwp_leave1] = lt_lwpleave - 0.50;
                        $scope.edt[allowed_leaves1] = allowed_leaves - 0.50;
                        $scope.edt[lt_hours] = "0.1";
                    }
                    else {
                        if (lt_lwpleave > 0) {
                            $scope.edt[lt_lwp_leave1] = $scope.lt_lwp;
                            $scope.edt[allowed_leaves1] = diffDays;
                            $scope.edt[lt_hours] = "0"
                        }
                        else {
                            $scope.edt[lt_lwp_leave1] = diffDays;
                            $scope.edt[allowed_leaves1] = diffDays;
                            $scope.edt[lt_hours] = "0"
                        }
                    }
                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }])
})();