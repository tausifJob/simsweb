﻿
(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PaysParameterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/common/PaysParameter/getCompanyName").then(function (res1) {
                $scope.display = false;
                $scope.table = true;
                $scope.comp = res1.data;
                console.log($scope.comp);
            });

            $http.get(ENV.apiUrl + "api/common/PaysParameter/getApplicationName").then(function (res1) {
                $scope.appl_name = res1.data;
                console.log($scope.appl_name);
            });


            $http.get(ENV.apiUrl + "api/common/PaysParameter/getApplicationName").then(function (res1) {
                $scope.appl_name1 = res1.data;
                console.log($scope.appl_name1);
            });

            $http.get(ENV.apiUrl + "api/common/PaysParameter/getAllPaysParameter").then(function (res1) {
                $scope.obj1 = res1.data;
                console.log($scope.obj1);
                $scope.totalItems = $scope.obj1.length;
                console.log($scope.totalItems);
                $scope.todos = $scope.obj1;
                console.log($scope.totalItems);
                $scope.makeTodos();
            });

            $scope.getgrid = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/common/PaysParameter/getAllPaysParameter").then(function (res1) {
                    $scope.obj1 = res1.data;
                    console.log($scope.obj1);
                    $scope.totalItems = $scope.obj1.length;
                    console.log($scope.totalItems);
                    $scope.todos = $scope.obj1;
                    console.log($scope.totalItems);
                    $scope.makeTodos();
                });
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                    $scope.flag = true;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    $scope.filteredTodos[i].icon = 'fa fa-plus-circle';
                }
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj1, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj1;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.pays_appl_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.pays_appl_form_field.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.pays_appl_parameter.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.pays_appl_form_field_value1 == toSearch) ? true : false;

            }


            $scope.GetSelectedApplication = function (str) {
                debugger;
                if (str == "") {
                    $scope.getgrid();
                }
                else {

                    $http.get(ENV.apiUrl + "api/common/PaysParameter/getAllPaysParameter?comn_appl_code=" + str).then(function (res) {
                        $scope.display = false;
                        $scope.obj1 = res.data;
                        $scope.totalItems = $scope.obj1.length;
                        $scope.todos = $scope.obj1;
                        $scope.makeTodos();
                    });
                }

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                $scope.disabled = false;
                $scope.readonly = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = {};
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                if ($scope.comp.length > 0) {
                    $scope.temp.pays_comp_code = $scope.comp[0].pays_comp_code;
                }
            }

            var datasend = [];
            $scope.savedata = function (Myform) {
                if (Myform) {

                    var data = {
                        pays_comp_code: $scope.temp.pays_comp_code
                    , pays_appl_code: $scope.temp.pays_application_code
                    , pays_appl_form_field: $scope.temp.pays_appl_form_field
                    , pays_appl_parameter: $scope.temp.pays_appl_parameter
                    , pays_appl_form_field_value1: $scope.temp.pays_appl_form_field_value1
                    , pays_appl_form_field_value2: $scope.temp.pays_appl_form_field_value2
                    , pays_appl_form_field_value3: $scope.temp.pays_appl_form_field_value3
                    , pays_appl_form_field_value4: $scope.temp.pays_appl_form_field_value4
                    , opr: 'I'
                    };

                    datasend.push(data);

                    $http.post(ENV.apiUrl + "api/common/PaysParameter/CUDPaysParameter", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        $http.get(ENV.apiUrl + "api/common/PaysParameter/getAllPaysParameter").then(function (res1) {
                            $scope.obj1 = res1.data;
                            $scope.totalItems = $scope.obj1.length;
                            $scope.todos = $scope.obj1;
                            $scope.makeTodos();
                        });
                    });
                    datasend = [];
                    $scope.display = false;
                    $scope.table = true;

                }
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.pays_comp_code = "";
                $scope.temp.pays_application_code = "";
                $scope.temp.pays_appl_form_field = "";
                $scope.temp.pays_appl_parameter = "";
                $scope.temp.pays_appl_form_field_value1 = "";
                $scope.temp.pays_appl_form_field_value2 = "";
                $scope.temp.pays_appl_form_field_value3 = "";
                $scope.temp.pays_appl_form_field_value4 = "";
            }

            $scope.edit = function (str) {
                $scope.table = false;
                $scope.disabled = true;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;

                $scope.temp = {
                    pays_comp_code: str.pays_comp_code
                , pays_application_code: str.pays_appl_code
                , pays_appl_form_field: str.pays_appl_form_field
                , pays_appl_parameter: str.pays_appl_parameter
                , pays_appl_form_field_value1: str.pays_appl_form_field_value1
                , pays_appl_form_field_value2: str.pays_appl_form_field_value2
                , pays_appl_form_field_value3: str.pays_appl_form_field_value3
                , pays_appl_form_field_value4: str.pays_appl_form_field_value4
                , old_form: str.pays_appl_form_field
                , old_parameter: str.pays_appl_parameter
                , old_value1: str.pays_appl_form_field_value1
                }
            }

            var dataupdate = [];
            $scope.update = function (Myform) {
                if (Myform) {
                    var data = {
                        pays_comp_code: $scope.temp.pays_comp_code
                      , pays_appl_code: $scope.temp.pays_application_code
                      , pays_appl_form_field: $scope.temp.pays_appl_form_field
                      , pays_appl_parameter: $scope.temp.pays_appl_parameter
                      , pays_appl_form_field_value1: $scope.temp.pays_appl_form_field_value1
                      , pays_appl_form_field_value2: $scope.temp.pays_appl_form_field_value2
                      , pays_appl_form_field_value3: $scope.temp.pays_appl_form_field_value3
                      , pays_appl_form_field_value4: $scope.temp.pays_appl_form_field_value4
                      , old_form: $scope.temp.old_form
                      , old_parameter: $scope.temp.old_parameter
                      , old_value1: $scope.temp.old_value1
                      , opr: 'U'
                    };
                    dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/common/PaysParameter/CUDPaysParameter", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        $http.get(ENV.apiUrl + "api/common/PaysParameter/getAllPaysParameter").then(function (res1) {
                            $scope.obj1 = res1.data;
                            $scope.totalItems = $scope.obj1.length;
                            $scope.todos = $scope.obj1;
                            $scope.makeTodos();
                        });
                    });
                    dataupdate = [];
                    $scope.display = false;
                    $scope.table = true;
                }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].pays_comp_name + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].pays_comp_name + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.OkDelete = function () {
                debugger;
                var data1 = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].pays_comp_name + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'pays_comp_code': $scope.filteredTodos[i].pays_comp_code,
                            'pays_appl_form_field': $scope.filteredTodos[i].pays_appl_form_field,
                            'pays_appl_parameter': $scope.filteredTodos[i].pays_appl_parameter,
                            'pays_appl_form_field_value1': $scope.filteredTodos[i].pays_appl_form_field_value1,
                            'opr': "D"
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/common/PaysParameter/CUDPaysParameter", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/common/PaysParameter/getAllPaysParameter").then(function (res1) {
                                                $scope.obj1 = res1.data;
                                                $scope.totalItems = $scope.obj1.length;
                                                $scope.todos = $scope.obj1;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/common/PaysParameter/getAllPaysParameter").then(function (res1) {
                                                $scope.obj1 = res1.data;
                                                $scope.totalItems = $scope.obj1.length;
                                                $scope.todos = $scope.obj1;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].pays_comp_name + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                data1 = [];
            }

            var dom;
            $scope.flag = true;
            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table table table-bordered table-hover table-condensed' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                         "<tr style='background-color: #edefef'> <td class='semi-bold'>" + "Field Value 1" + "</td> <td class='semi-bold'>" + "Field Value 2" + " </td><td class='semi-bold'>" + "Field Value 3" + "</td><td class='semi-bold'>" + "Field Value 4" + "</td>" +
                        "</tr>" +

                          "<tr><td>" + (info.pays_appl_form_field_value1) + "</td> <td>" + (info.pays_appl_form_field_value2) + " </td><td>" + (info.pays_appl_form_field_value3) + "</td><td>" + (info.pays_appl_form_field_value4) + "</td>" +
                        "</tr>" +

                        " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;

                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                    
                }

            };

        }])

})();
