﻿(function () {
    'use strict';
    var obj1, obj2, temp, main;
    var opr = '';
    var gradecode = [];

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('OtherLoanCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.OtherLoanDetail = true;
            $scope.editmode = false;

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            var formdata = new FormData();

            $http.get(ENV.apiUrl + "api/OtherLoan/getComapany").then(function (res) {
              
                $scope.compname = res.data;

            })

            $http.get(ENV.apiUrl + "api/OtherLoan/getEmployee").then(function (res) {
                $scope.empname = res.data;

            })

            $http.get(ENV.apiUrl + "api/OtherLoan/getLedger").then(function (res) {
                $scope.gledger = res.data;
            })

            $http.get(ENV.apiUrl + "api/OtherLoan/getDescription").then(function (res) {
                $scope.gdesrcipt = res.data;
            })

            $scope.size = function (str) {
             
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;

                $scope.currentPage = str;  $scope.makeTodos();
            }

            $http.get(ENV.apiUrl + "api/OtherLoan/getOtherLoanDetail").then(function (OtherLoan_Data) {
              
                $scope.OtherLoanData = OtherLoan_Data.data;
                $scope.totalItems = $scope.OtherLoanData.length;
                $scope.todos = $scope.OtherLoanData;
                $scope.makeTodos();

               
            });
            $http.get(ENV.apiUrl + "api/OtherLoan/getOtherLoanDetail").then(function (res) {
                $scope.OtherLoanOperation = false;
                $scope.OtherLoanDetail = true;
                $scope.obj = res.data;
               
            });
            //  OtherLoanOperation OtherLoanDetail
            $scope.cancel = function () {
                $scope.OtherLoanDetail = true;
                $scope.OtherLoanOperation = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $(function () {
                $("#all_chk").change(function () {
                    $('input:checkbox').prop('checked', this.checked);
                    $('tr').toggleClass("selected_row", this.checked)
                });

                $('table tbody :checkbox').change(function (event) {
                    $(this).closest('tr').toggleClass("selected_row", this.checked);
                });
            });

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.editmode = false;
                opr = 'S';
                $scope.readonly = false;
                $scope.OtherLoanDetail = false;
                $scope.OtherLoanOperation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = {};
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();


            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

             

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);

                });

            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.OtherLoanData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.OtherLoanData;
                    main.checked = false;
                }
                $scope.makeTodos();

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.ol_account_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.ol_account_number == toSearch) ? true : false;

            }

            $scope.Save = function (myForm) {

                if (myForm) {

                    var data = $scope.edt;
                    data.opr = 'I';

                    $scope.exist = false;
                    for (var i = 0; i < $scope.OtherLoanData.length; i++) {
                        if ($scope.OtherLoanData[i].ol_account_number == data.ol_account_number) {
                            $scope.exist = true;
                        }

                    }
                    if ($scope.exist) {
                        swal({ text: "Record Already exists", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                    else {
                        $http.post(ENV.apiUrl + "api/OtherLoan/OtherLoanCUD", data).then(function (msg) {
                            $scope.msg1 = msg.data;
                            $scope.OtherLoanOperation = false;
                            $http.get(ENV.apiUrl + "api/OtherLoan/getOtherLoanDetail").then(function (Otherloan_Leave) {
                                $scope.OtherLoanData = Otherloan_Leave.data;
                                $scope.totalItems = $scope.OtherLoanData.length;
                                $scope.todos = $scope.OtherLoanData;
                                $scope.makeTodos();

                                $scope.msg1 = msg.data;


                                if ($scope.msg1 == true) {

                                    swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                }
                                else if ($scope.msg1 == false) {

                                    swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }

                            });

                        });

                    }
                    $scope.OtherLoanDetail = true;
                    $scope.OtherLoanOperation = false;




                }
            }

            $scope.up = function (str) {
                opr = 'U';
                $scope.edt = str;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.OtherLoanDetail = false;
                $scope.OtherLoanOperation = true;




            }

            $scope.Update = function () {


                var data = $scope.edt;
                data.opr = 'U';

                data1.push(data);
                $http.post(ENV.apiUrl + "api/EmpGrade/EmpGradeCUD", data).then(function (msg) {
                    $scope.msg1 = msg.data;
                    $scope.BoardData1 = false;
                    $http.get(ENV.apiUrl + "api/EmpGrade/getEmpgradeDetail").then(function (Pays_Leave) {
                        $scope.LeaveData = Pays_Leave.data;
                        $scope.totalItems = $scope.LeaveData.length;
                        $scope.todos = $scope.LeaveData;
                        formdata = new FormData();
                        $scope.makeTodos();

                        if ($scope.msg1 == true) {
                            $rootScope.strMessage = 'Record Updated Successfully';
                            $('#message').modal('show');
                        }
                        else if ($scope.msg1 == false) {

                            $rootScope.strMessage = 'Record Not Updated. ' ;

                            $('#message').modal('show');
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });

                })
                $scope.BoardData1 = false;
                $scope.BoardDetail = true;
            }

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].ol_account_number;

                        var v = document.getElementById(t + i);
                        v.checked = true;
                        gradecode = gradecode + $scope.filteredTodos[i].ol_account_number + ','
                        $scope.row1 = 'row_selected';
                        $('table tbody :checkbox').change(function (event) {
                            $(this).closest('tr').toggleClass("selected_row", this.checked);
                        });

                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].ol_account_number;
                        var v = document.getElementById(t + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }
            }

            $scope.check_once = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
                else {
                    main.checked = false;
                    $scope.row1 = '';
                    $('table tbody :checkbox').change(function (event) {
                        $(this).closest('tr').toggleClass("selected_row", this.checked);
                    });
                }
            }

            $scope.deleterecord = function () {
                swal({
                    title: "Are you sure?", text: "to Delete Record", width: 300, height: 200, showCancelButton: true,
                    confirmButtonColor: "#DD6B55", confirmButtonText: "Yes", closeOnConfirm: false
                },
               function (isConfirm) {
                   if (isConfirm) {

                       gradecode = [];
                       for (var i = 0; i < $scope.filteredTodos.length; i++) {
                           var t = $scope.filteredTodos[i].ol_account_number;
                           var v = document.getElementById(t + i);

                           if (v.checked == true)
                               gradecode = gradecode + $scope.filteredTodos[i].ol_account_number + ',';
                       }

                       var deletecountrycode = ({
                           'ol_account_number': gradecode,

                           'opr': 'D'
                       });

                       $http.post(ENV.apiUrl + "api/OtherLoan/OtherLoanCUD", deletecountrycode).then(function (msg) {
                           $scope.msg1 = msg.data;
                           $scope.OtherLoanDetail = true;
                           $scope.OtherLoanOperation = false;
                           $rootScope.strMessage = $scope.msg1.strMessage;
                           $http.get(ENV.apiUrl + "api/OtherLoan/getOtherLoanDetail").then(function (get_Ethnicity) {
                               $scope.Ethnicity_Data = get_Ethnicity.data;
                               $scope.totalItems = $scope.Ethnicity_Data.length;
                               $scope.todos = $scope.Ethnicity_Data;
                               $scope.makeTodos();
                           });
                           if ($scope.msg1 == true) {
                               swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                           }
                           else if ($scope.msg1 == false) {
                               swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                           }
                           else {
                               swal("Error-" + $scope.msg1)
                           }
                       });
                   }

                   gradecode = [];
                   $scope.OtherLoanDetail = true;
                   $scope.OtherLoanOperation = false;
               });
            }


        }]);

})();