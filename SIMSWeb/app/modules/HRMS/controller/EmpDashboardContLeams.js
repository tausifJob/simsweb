﻿(function () {
    'use strict';
    var CurrentDate;
    var day;
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmpDashboardCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', '$compile', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, $compile, ENV) {

            var date = new Date();
            var month = (date.getMonth() + 1);
            var day = date.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            $scope.todaydate = (day) + '-' + (month) + '-' + date.getFullYear();


            var dateyear = $filter('date')(new Date(), 'dd-MM-yyyy');
            $scope.dt = {
                sims_from_date: $scope.todaydate,
                sims_leave_date: $scope.todaydate,
                sims_attendance_date: $scope.todaydate,
                sims_admission_date: $scope.todaydate,
                sims_admission_cancel_date: $scope.todaydate,
                sims_collection_date: $scope.todaydate,
            }

            $scope.frm = {
                sims_admission_date: '',
            }
            $scope.to = {
                sims_admission_date: '',
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
            });

            $scope.edt = [];

            $http.get(ENV.apiUrl + "api/empdash/getEmpcount").then(function (res1) {
                $scope.empcounts = res1.data;
            });


            $http.get(ENV.apiUrl + "api/empdash/getEmpGradecount").then(function (res1) {
                $scope.empgradecounts = res1.data;
            });

            $http.get(ENV.apiUrl + "api/empdash/getEmpVisacount").then(function (res1) {
                $scope.empvisacounts = res1.data;
            });

            $scope.getServiceCode = function (curservice) {
                $http.get(ENV.apiUrl + "api/empdash/getEMPDetailsbyService?servicecode="+curservice).then(function (res1) {
                    $scope.servicedata = res1.data;
                });
            }

            $scope.getGradeCode = function (curgrade) {
                $http.get(ENV.apiUrl + "api/empdash/getEMPDetailsbyGrade?gradecode=" + curgrade).then(function (res1) {
                    $scope.gradedata = res1.data;
                });
            }

            $scope.getVisadetails = function (curtype) {
                $http.get(ENV.apiUrl + "api/empdash/getEMPDetailsbyVisa?visatype=" + curtype).then(function (res1) {
                    $scope.visatypedata = res1.data;
                });
            }

            $scope.dateChange = function (selecteddate) {
                $http.get(ENV.apiUrl + "api/dailystatusdash/getPunchedEmployee?date=" + selecteddate).then(function (res) {
                    $scope.punched_employee = res.data;
                });
                $http.get(ENV.apiUrl + "api/dailystatusdash/getNotPunchedEmployee?date=" + selecteddate).then(function (res2) {
                    $scope.not_punched_employee = res2.data;
                });
            }
            $scope.leaveTypes = [];
            $http.get(ENV.apiUrl + "api/empdash/getleaveType").then(function (lev) {
                $scope.leaveTypes = lev.data;
                if ($scope.leaveTypes.length > 0) {
                    $scope.leaveTypes.push({ leave_code: 'All', leave_type: 'All' })
                    $scope.edt['leave_code'] = 'All'; //$scope.leaveTypes[0].leave_code;
                    $scope.getLeaveType($scope.edt['leave_code'], $scope.dt.sims_leave_date);
                }
            });

            $scope.dateleaveChange = function (leavedate) {
                $scope.getLeaveType($scope.edt['leave_code'], leavedate);
            }

            $scope.getLeaveType = function (curleavecode, selectdate) {
                $http.get(ENV.apiUrl + "api/empdash/getleaveDetails?leavecode=" + curleavecode + "&date=" + $scope.dt.sims_leave_date).then(function (levcnt) {
                    $scope.empleavesCount = levcnt.data;
                });
            }

            $scope.dateattendanceChange = function (curdate) {
                $scope.getAttendanceCountData(curdate)
            }

            $scope.getAttendanceCountData = function (selecteddate) {
                $scope.presentCount = []
                $scope.absentCount = []
                $scope.lateCount = []
                $scope.lwpCount = []

                $http.get(ENV.apiUrl + "api/empdash/getEmpAttendanceCount?date=" + selecteddate).then(function (att) {
                    $scope.attendanceCount = att.data;
                    $scope.attdate = $scope.attendanceCount[0].att_date;
                    for (var i = 0; $scope.attendanceCount.length > i; i++) {
                        if ($scope.attendanceCount[i].att_absent_flag == 'P') {
                            $scope.presentCount.push($scope.attendanceCount[i]);
                        }
                        if ($scope.attendanceCount[i].att_absent_flag == 'A') {
                            $scope.absentCount.push($scope.attendanceCount[i]);
                        }
                        if ($scope.attendanceCount[i].att_absent_flag == 'LA') {
                            $scope.lateCount.push($scope.attendanceCount[i]);
                        }
                        if ($scope.attendanceCount[i].att_absent_flag == 'LWP') {
                            $scope.lwpCount.push($scope.attendanceCount[i]);
                        }
                    }
                });
            }

            $scope.getAttendanceCountData($scope.dt.sims_attendance_date);
            $scope.selecteto = 'TO';
            $scope.selecteCA = 'CA';
            $http.get(ENV.apiUrl + "api/dailystatusdash/getAcademicYear").then(function (res2) {
                $scope.acayears = res2.data;
                $scope.edt['sims_academic_year'] = $scope.acayears[0].sims_academic_year
                $scope.edt['sims_academic_year1'] = $scope.acayears[0].sims_academic_year

                $scope.getAdmissionData($scope.edt['sims_academic_year'], $scope.dt.sims_admission_date, $scope.selecteto);
                $scope.getCancelAdmissionData($scope.edt['sims_academic_year1'], $scope.dt.sims_admission_cancel_date, $scope.selecteCA);
            });

            $scope.close = function () {
                $http.get(ENV.apiUrl + "api/dailystatusdash/getAcademicYear").then(function (res2) {
                    $scope.acayears = res2.data;
                    $scope.edt['sims_academic_year'] = $scope.acayears[0].sims_academic_year;
                    $scope.edt['sims_academic_year1'] = $scope.acayears[0].sims_academic_year;
                    $scope.getAdmissionData($scope.edt['sims_academic_year'], $scope.dt.sims_admission_date, $scope.selecteto);
                    $scope.getCancelAdmissionData($scope.edt['sims_academic_year1'], $scope.dt.sims_admission_cancel_date, $scope.selecteCA);
                });
            }

            $scope.punchModal = function (cur) {
                $scope.getServiceCode(cur);
                if (cur == "L") {
                    $scope.leftcol = false;
                } else {
                    $scope.leftcol = true;
                }
                if (cur == "L" || cur == "V" || cur == "B" || cur == "N") {
                    $scope.servicecol = false;
                } else {
                    $scope.servicecol = true;
                }
                $('#punchModal').modal({ backdrop: 'static', keyboard: false });
            }
            $scope.leaveModal = function () {
                $('#leaveModal').modal({ backdrop: 'static', keyboard: false });
            }
            $scope.attendanceModal = function () {
                $('#attendanceModal').modal({ backdrop: 'static', keyboard: false });
            }
            $scope.admissionModal = function (cur) {
                $scope.getGradeCode(cur);
                $('#admissionModal').modal({ backdrop: 'static', keyboard: false });
            }
            $scope.canceladmissionModal = function (cur) {
                $scope.getVisadetails(cur)
                if (cur == "National id") {
                    $scope.nationalhide = false;
                } 
                if (cur == "Visa" || cur == "Visa expired" || cur == "Expire in Month") {
                    $scope.nationalhide = true;
                }
                $scope.visatypedata = '';
                $('#canceladmissionModal').modal({ backdrop: 'static', keyboard: false });
            }
            $scope.collectionModal = function () {
                $('#collectionModal').modal({ backdrop: 'static', keyboard: false });
            }

        }])
})();
