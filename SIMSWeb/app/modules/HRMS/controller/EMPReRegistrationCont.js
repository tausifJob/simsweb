﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmpReRegistrationCont',
        ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.display = true;
            $scope.grid = true;                       
            $scope.edt = {};
            $scope.Save_btn = false;
            $scope.showTable = false;
            $scope.temp = {};

            $http.get(ENV.apiUrl + "api/common/GlobalSearch/GetCompanyNameForShift").then(function (res) {
                $scope.cmbCompany = res.data;
                $scope.temp.company_code = $scope.cmbCompany[0].sh_company_code;
                $scope.getDeptFromCompany($scope.cmbCompany[0].sh_company_code);
                $scope.getDesignationFromCompany($scope.cmbCompany[0].sh_company_code);
            });

            $scope.getDeptFromCompany = function (company_code) {
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/GetDepartmentName?company_code=" + company_code).then(function (res) {
                    $scope.compDepartment = res.data;
                });
            }

            $scope.getDesignationFromCompany = function (company_code) {
                $http.get(ENV.apiUrl + "api/EmployeeReport/GetDesignation?com_code=" + company_code).then(function (res) {
                    $scope.compDesignation = res.data;
                });
            }

            $scope.searchGlobalClick = function () {
                $scope.Global_Search_by_employee();
            }

            $scope.emp_modal_cancel = function () {
                $('#myModal').modal('hide');
                $scope.edt.em_login_code = $scope.SelectedUserLst[0].em_login_code;
                $scope.temp = {};
                $scope.temp.company_code = $scope.cmbCompany[0].sh_company_code;
                $scope.NodataEmployee = false;
            }

            $scope.globalSearchCustom = function () {
                $scope.temp = {};
                $scope.NodataEmployee = false;                
                $scope.showEmployeeFilterBtn = true;
                $scope.EmployeeTable = false;                
                $scope.global_search_result = [];
                $('#myModal').modal('show');
                $scope.temp.company_code = $scope.cmbCompany[0].sh_company_code;
            }

            $scope.Search_by_employee = function () {                ;
                $scope.NodataEmployee = false;
                var obj = {};                
                obj.em_login_code = $scope.temp.em_login_code;
                obj.EmpName = $scope.temp.EmpName;
                obj.em_department = $scope.temp.em_department;
                obj.em_desg_code = $scope.temp.em_desg_code;
                obj.company_code = $scope.temp.company_code;
                obj.resignedEmpFlag = "Yes";

                $http.post(ENV.apiUrl + "api/EmpReRegistration/SearchResignedEmployee?data=" + JSON.stringify(obj)).then(function (SearchEmployee_Data) {
                    $scope.global_search_result = SearchEmployee_Data.data;                        
                    $scope.EmployeeTable = true;
                    $scope.showEmployeeFilterBtn = true;
                    $scope.showEmployeeFilter = true;
                    $scope.currentPage = 0;

                    if (SearchEmployee_Data.data.length > 0) {
                        //   $scope.EmployeeTable = true;
                    }
                    else {
                        $scope.global_search_result = [];
                        swal({ title: 'Alert', text: "No record(s) found.", showCloseButton: true, width: 450, height: 200, timer: 1000 });
                        $scope.NodataEmployee = true;
                        //  $scope.EmployeeTable = false;
                    }
                    $timeout(function () {
                        $("#fixTable1").tableHeadFixer({ 'top': 1 });
                    }, 100);
                    $scope.getPerPageRecords();
                });                
            }

            $timeout(function () {
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);

           
            $scope.getEmpReg = function (emp_login_code) {                
                $http.get(ENV.apiUrl + "api/EmpReRegistration/getEmployeDetails?login_code=" + emp_login_code).then(function (res) {
                    
                    if (res.data.length > 0) {
                        $scope.Save_btn = true;
                        $scope.showTable = true;

                        $scope.empData = res.data[0];

                        $scope.temp = {
                            em_login_code: $scope.empData.em_login_code,
                            em_full_name: $scope.empData.em_full_name,
                            em_date_of_join: $scope.empData.em_date_of_join,
                            dg_desc: $scope.empData.dg_desc,
                            com_name: $scope.empData.com_name,
                            dep_name: $scope.empData.dep_name,
                            reg_date: $scope.empData.reg_date,
                            reg_resion: $scope.empData.reg_resion,
                            em_status: $scope.empData.em_status
                        }
                        console.log($scope.temp);                        
                    }
                    else {
                        swal({ title: "Alert", text: "Please search only resigned employee", width: 300, height: 200 });
                    }
                });
            }

            $scope.SaveData = function () {
                if ($scope.temp.activation_date == undefined || $scope.temp.activation_date == "" || $scope.temp.activation_date == null) {
                    swal({ title: "Alert", text: "Please select activation date", width: 300, height: 200 });
                    return;
                }
                console.log("temp", $scope.temp);

                $http.post(ENV.apiUrl + "api/EmpReRegistration/ReregisterEmployee",$scope.temp).then(function (res) {
                    $scope.msg1 = res.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Employee Re-Register Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380 });
                        $scope.cancel_reg();
                    }
                    else {
                        swal({ title: "Alert", text: "Server Error" + $scope.msg1, showCloseButton: true, width: 380});
                    }
                });
            }

            $scope.cancel_reg = function () {
                $scope.edt = {};
                $scope.temp = {};
                $scope.Save_btn = false;
                $scope.showTable = false;
                $scope.temp.company_code = $scope.cmbCompany[0].sh_company_code;
            }

            $scope.reset = function () {
                $scope.edt.em_login_code = '';
            }

        }])


})();