﻿(function () {
    'use strict';
    var modulecode = [];
    var del = [];
    var data = [];
    var main;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AttendanceMachineCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.itemsPerPage = '50';

            var date = new Date();
            var month = (date.getMonth() + 1);
            //var month = (date.getMonth() + 1);
            //var day = date.getDate();
            //if (month < 10)
            //    month = "0" + month;
            //if (day < 10)
            //    day = "0" + day;
            //var todaydate = date.getFullYear() + '-' + (month) + '-' + (day)
            var todaydate = ('0' + date.getDate()).slice(-2) + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + date.getFullYear();
            $scope.edt = { sdate: todaydate, edate: todaydate };
            $scope.rgvtbl = false;
            $scope.buzy = false;
            $scope.showdate = function (date, name1) {
                //var date = new Date(date);
                //var month = (date.getMonth() + 1);
                //var day = date.getDate();
                //if (month < 10)
                //    month = "0" + month;
                //if (day < 10)
                //    day = "0" + day;
                //$scope.edt[name1] = date.getFullYear() + '-' + (month) + '-' + (day)
                $scope.edt[name1] = date
            }

            var first = '';
            var last = '';


            var t = document.getElementsByName('first');
            for (var i = 0; i < t.length; i++) {
                if (t[i].checked == true) {
                    first = t[i].value;
                }
            }

            var k = document.getElementsByName('last');
            for (var i = 0; i < k.length; i++) {
                if (k[i].checked == true) {
                    last = k[i].value;
                }
            }

            $scope.SelectRadio = function (val) {
                main = document.getElementById('showChk');
                if (main.checked == true) {
                    main.checked = false;
                }
                first = val;
            }

            $scope.SelectRadio1 = function (val) {
                main = document.getElementById('showChk');
                if (main.checked == true) {
                    main.checked = false;
                }
                last = val;

            }

            $scope.exportData = function () {
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('example_wrapper').innerHTML], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                        });
                        saveAs(blob, "Attendance_Machine_Data.xls");
                        
                        //alasql('SELECT * INTO XLSX("CirccularDetails.xlsx",{headers:true})\FROM HTML("#all_data",{headers:true,skipdisplaynone:true})');                       

                    }
                });
            };


            $scope.CheckAllChecked = function () {

               var main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].att_emp_id + i);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].att_emp_id + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

               var main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.movebtn = true;
            $(function () {
               
                $('#cmb_dept').multipleSelect({
                    width: '100%',
                    placeholder: "Select Department"
                });

                $('#cmb_designation').multipleSelect({
                    width: '100%',
                    placeholder: "Select Designation"
                });
              
            });
            $scope.designationFun=function () {
                $http.get(ENV.apiUrl + "api/common/getAllDesignationName").then(function (res) {
                    $scope.designation = res.data;
                    // console.log($scope.designation);
                    setTimeout(function () {
                        $('#cmb_designation').click(function () {
                            // console.log($(this).val());                        
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }
            $scope.designationFun();
            $scope.departmentFun = function () {
                $http.get(ENV.apiUrl + "api/common/getDepartmentName").then(function (res) {
                    $scope.department = res.data;
                    // console.log($scope.department);
                    setTimeout(function () {
                        $('#cmb_dept').change(function () {                                                       
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }
            $scope.departmentFun();
            var dept_code = "";
            var desg_code = "";

            $scope.show = function (edit) {

                if (edit != undefined) {
                    if (edit.sdate != '' && edit.edate != '') {
                        main = document.getElementById('showChk');
                        if (main.checked == true) {
                            edit.first = '';
                            edit.last = '';
                        } else {
                            edit.first = first;
                            edit.last = last;
                        }
                        dept_code = "";
                        for (var i = 0; i < $scope.edt.em_dept_code.length; i++) {
                            dept_code = dept_code + $scope.edt.em_dept_code[i] + ",";
                        }
                        desg_code = "";
                        for (var i = 0; i < $scope.edt.designation.length; i++) {
                            desg_code = desg_code + $scope.edt.designation[i] + ",";
                        }
                        console.log("dept_code", dept_code);
                        console.log("desg_code", desg_code);

                        $scope.rgvtbl = false;
                        $scope.busy = true;
                      
                        $http.get(ENV.apiUrl + "api/attendanceMachine/getAttendaceMachineDetails?sdate=" + edit.sdate + "&edate=" + edit.edate + "&punchin=" + edit.first + "&punchout=" + edit.last + "&dept_code=" + dept_code + "&desg_code=" + desg_code).then(function (res) {
                            $scope.obj = res.data;
                            $scope.div_hide = false;
                            $scope.display = false;
                            $scope.grid = true;
                            $scope.save1 = false;
                            $scope.value = false;
                            $scope.movebtn = false;
                           
                            $scope.totalItems = $scope.obj.length;
                            $scope.todos = $scope.obj;
                            $scope.makeTodos();

                            $scope.rgvtbl = true;
                            $scope.busy = false;
                        });
                    }
                } else {
                    swal('', 'Select Date to Display Record');
                }
            }
            $scope.display_allRecords = function (sdate, edate, first, last) {
                debugger;
                $scope.busy = false;
                if (sdate != undefined && edate != undefined) {
                    
                    $scope.busy = true;
                    $http.get(ENV.apiUrl + "api/attendanceMachine/getAttendaceMachineDetails?sdate=" + sdate + "&edate=" + edate + "&punchin=" + first + "&punchout=" + last + "&dept_code=" + dept_code + "&desg_code=" + desg_code).then(function (res) {
                        $scope.obj = res.data;
                        $scope.div_hide = false;
                        $scope.display = false;
                        $scope.grid = true;
                        $scope.save1 = false;
                        $scope.value = false;
                        $scope.movebtn = false;

                        $scope.totalItems = $scope.obj.length;
                        $scope.todos = $scope.obj;
                        $scope.makeTodos();

                        $scope.rgvtbl = true;
                        $scope.busy = false;
                    });
                }
            }
            $scope.save = function () {
                debugger;
                $scope.att_push_flag_status = true;
                $scope.row1 = '';
                var datasend = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var main = document.getElementById($scope.filteredTodos[i].att_emp_id + i);
                    if (main.checked == true) {
                        //if ($scope.filteredTodos[i].att_push_flag == 'N') {
                            var data = {
                                opr: 'I'
                                           , att_emp_id: $scope.filteredTodos[i].att_emp_id
                                           , att_date: $scope.filteredTodos[i].att_date
                                           , att_shift1_in: $scope.filteredTodos[i].att_shift1_in
                                           , att_shift1_out: $scope.filteredTodos[i].att_shift1_out
                                           , att_sdate: $scope.edt.sdate
                                           , att_edate: $scope.edt.edate
                                      
                            };                            
                            datasend.push(data);
                            $scope.att_push_flag_status = false;
                        //}
                    }                   
                }

                console.log("datasend", datasend);
                if (datasend.length > 0) {
                    $scope.rgvtbl = false;
                    $scope.busy = true;
                    $http.post(ENV.apiUrl + "api/attendanceMachine/CUDAttendaceMachine", datasend).then(function (msg) {
                        if (msg.data == true) {                            
                            swal('', 'Record Inserted Successfully');
                            debugger;
                            $scope.display_allRecords($scope.edt.sdate, $scope.edt.edate, $scope.edt.first, $scope.edt.last,dept_code,desg_code);
                            $scope.currentPage = 0;
                        } else if (msg.data == false) {                            
                            swal('', 'Record Not Inserted. ');
                        }
                        else {
                            swal("Error-" + msg.data)
                        }
                        $scope.rgvtbl = true;
                        $scope.busy = false;
                    });
                }
                else if ($scope.att_push_flag_status) {
                    swal('','Records already move to attendance.');
                }
                else {
                    swal('', 'Cannot proceed to confirmation page due to error "Please select at least one item"');
                }

                setTimeout(function () {
                    $('#cmb_dept').change(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 500);

                setTimeout(function () {
                    $('#cmb_designation').click(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 500);

            }
            $scope.Reset = function () {

                $scope.movebtn = true;
                $scope.currentPage = 0;
                $scope.notshow = false;
                $scope.edt = '';
                var t = document.getElementsByName('first');
                for (var i = 0; i < t.length; i++) {
                    if (t[i].checked == true) {
                        t[i].checked = false;
                    }
                }

                var k = document.getElementsByName('last');
                for (var i = 0; i < k.length; i++) {
                    if (k[i].checked == true) {
                        k[i].checked = false;
                    }
                }
                $scope.filteredTodos = '';
                var date = new Date();
                var month = (date.getMonth() + 1);
                var day = date.getDate();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                //var todaydate = date.getFullYear() + '-' + (month) + '-' + (day)
                var todaydate = (day) + '-' + (month) + '-' + date.getFullYear()
                $scope.edt = { sdate: todaydate, edate: todaydate };
                $scope.rgvtbl = false;
                $scope.buzy = false;
                $scope.showdate = function (date, name1) {
                    //var date = new Date(date);
                    //var month = (date.getMonth() + 1);
                    //var day = date.getDate();
                    //if (month < 10)
                    //    month = "0" + month;
                    //if (day < 10)
                    //    day = "0" + day;
                    //$scope.edt[name1] = date.getFullYear() + '-' + (month) + '-' + (day)
                    $scope.edt[name1] = date
                }

                var first = '';
                var last = '';


                var t = document.getElementsByName('first');
                for (var i = 0; i < t.length; i++) {
                    if (t[i].checked == true) {
                        first = t[i].value;
                    }
                }

                var k = document.getElementsByName('last');
                for (var i = 0; i < k.length; i++) {
                    if (k[i].checked == true) {
                        last = k[i].value;
                    }
                }

                dept_code = "";
                desg_code = "";

                setTimeout(function () {
                    $('#cmb_dept').change(function () {                        
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 500);

                setTimeout(function () {
                    $('#cmb_designation').click(function () {                        
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 500);

            }
            $scope.showAll = function (edit) {
                main = document.getElementById('showChk');
                if (main.checked == true) {
                    var t = document.getElementsByName('first');
                    for (var i = 0; i < t.length; i++) {
                        if (t[i].checked == true) {
                            t[i].checked = false;
                            edit.first = '';
                        }
                    }

                    var k = document.getElementsByName('last');
                    for (var i = 0; i < k.length; i++) {
                        if (k[i].checked == true) {
                            k[i].checked = false;
                            edit.last = '';

                        }
                    }

                } else {
                    var t = document.getElementsByName('first');
                    for (var i = 0; i < t.length; i++) {
                        if (t[0].checked == false) {
                            t[0].checked = true;
                            edit.first = first;
                        }
                    }

                    var k = document.getElementsByName('last');
                    for (var i = 0; i < k.length; i++) {
                        if (k[1].checked == false) {
                            k[1].checked = true;
                            edit.last = last;

                        }
                    }
                }


            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 50, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

              

                $scope.filteredTodos = $scope.todos.slice(begin, end);
             
            };

            $scope.size = function (str) {
              
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;  $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
            }

            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;


            };

            $scope.insertonebyone = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }


            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
            var dom;
            var count = 0;


            $scope.searched = function (valLists, toSearch) {


                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.att_emp_id.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.empname.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    || item.em_login_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    || item.att_shift1_in.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }




        }])
})();