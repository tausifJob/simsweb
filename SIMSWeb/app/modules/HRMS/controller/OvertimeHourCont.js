﻿(function () {
    'use strict';
    var opr = '';
    var overtimecode = [];
    var main;
    var data1 = [];
    var data = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('OvertimeHourCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.table1 = true;
            $scope.operation = false;
            $scope.editmode = false;
            $scope.edt = {};


            $http.post(ENV.apiUrl + "api/OvertimeHour/Company").then(function (Company) {
                $scope.Company = Company.data;
                $scope.edt = { comp_code: $scope.Company[0].comp_code };

            });

            $http.post(ENV.apiUrl + "api/OvertimeHour/Department").then(function (Department) {
                $scope.Department = Department.data;

            });

            $http.post(ENV.apiUrl + "api/OvertimeHour/Designation").then(function (Designation) {
                $scope.Designation = Designation.data;

            });

            $http.post(ENV.apiUrl + "api/OvertimeHour/Grade").then(function (Grade) {
                $scope.Grade = Grade.data;

            });

            $http.post(ENV.apiUrl + "api/OvertimeHour/Year_month").then(function (Year_month) {
                $scope.Year_month = Year_month.data;                
                //if (new Date().getMonth() + 1 >= '04' && new Date().getMonth() + 1 <= '12') {
                //    var d = new Date().getFullYear() + 1;
                //}
                //else {
                //    var d = new Date().getFullYear();
                //}
                //var n = new Date().getMonth() + 1;
                //var ym = d + '' + n;

                //$scope.edt.year_month = ym;
            });


            $scope.Show_data = function () {
                debugger
                if ($scope.edt.comp_code == undefined)
                    $scope.edt.comp_code = '';
                if ($scope.edt.dept_code == undefined)
                    $scope.edt.dept_code = '';
                if ($scope.edt.desg_code == undefined)
                    $scope.edt.desg_code = '';
                if ($scope.edt.grade_code == undefined)
                    $scope.edt.grade_code = '';
                if ($scope.edt.emp_id == undefined)
                    $scope.edt.emp_id = '';

                if ($scope.edt.year_month == undefined || $scope.edt.year_month == '' || $scope.edt.year_month == null) {
                    swal({ title:'Alert',text:'Please select year month'});
                }
                else {
                    var ottype = '';
                    debugger;
                    if ($scope.edt.ot_type == undefined)
                        swal({ title: 'Alert', text: 'Please select over time type' });
                    else {
                        for (var i = 0; i < $scope.edt.ot_type.length; i++) {
                            ottype = ottype + $scope.edt.ot_type[i] + ',';
                        }

                        if (ottype == '')
                            swal({ title: 'Alert', text: 'Please select over time type' });
                        else {
                            $scope.buzy = true;

                            //if (new Date().getMonth() + 1 >= '04' && new Date().getMonth() + 1 <= '12') {
                            //    var d = new Date().getFullYear() + 1;
                            //}
                            //else {
                            //    var d = new Date().getFullYear();
                            //}
                            //var n = new Date().getMonth() + 1;
                            //var ym = d + '' + n;

                            //$scope.edt.year_month = ym;

                            var data = {
                                emp_id: $scope.edt.emp_id,
                                comp_code: $scope.edt.comp_code,
                                dept_code: $scope.edt.dept_code,
                                desg_code: $scope.edt.desg_code,
                                grade_code: $scope.edt.grade_code,
                                year_month: $scope.edt.year_month,
                                ot_type: ottype
                            }
                            $http.post(ENV.apiUrl + "api/OvertimeHour/EmployeeOT", data).then(function (EmployeeOT) {
                                $scope.EmployeeOT = EmployeeOT.data;
                                if ($scope.EmployeeOT.length > 0) {
                                    $scope.rgvtbl = true;
                                }
                                $scope.buzy = false;


                            });
                        }
                    }
                }
            }


            $scope.reset_form = function () {
                $scope.EmployeeOT = '';
                $scope.edt = '';
                $scope.rgvtbl = false;

                try {
                    $("#cmb_grade").multipleSelect("uncheckAll");
                }
                catch (e) { }
                $http.post(ENV.apiUrl + "api/OvertimeHour/Company").then(function (Company) {
                    $scope.Company = Company.data;
                    $scope.edt = { comp_code: $scope.Company[0].comp_code };

                });

                $http.post(ENV.apiUrl + "api/OvertimeHour/Department").then(function (Department) {
                    $scope.Department = Department.data;

                });

                $http.post(ENV.apiUrl + "api/OvertimeHour/Designation").then(function (Designation) {
                    $scope.Designation = Designation.data;

                });

                $http.post(ENV.apiUrl + "api/OvertimeHour/Grade").then(function (Grade) {
                    $scope.Grade = Grade.data;

                });

                $http.post(ENV.apiUrl + "api/OvertimeHour/Year_month").then(function (Year_month) {
                    $scope.Year_month = Year_month.data;                    
                    //if (new Date().getMonth() + 1 >= '04' && new Date().getMonth() + 1  <= '12') {
                    //    var d = new Date().getFullYear() + 1;
                    //}
                    //else {
                    //    var d = new Date().getFullYear() ;
                    //}                    
                    //var n = new Date().getMonth() + 1;
                    //var ym = d + '' + n;

                    //$scope.edt.year_month = ym;
                });

                $scope.buzy = false;
            }
            $scope.updateOT = function () {
                //if (new Date().getMonth() + 1 >= '04' && new Date().getMonth() + 1 <= '12') {
                //    var d = new Date().getFullYear() + 1;
                //}
                //else {
                //    var d = new Date().getFullYear();
                //}
                //var n = new Date().getMonth() + 1;
                //var ym = d + '' + n;

                //$scope.edt.year_month = ym;

                var listdata = [];
                for (var i = 0; i < $scope.EmployeeOT.length; i++) {
                    if ($scope.EmployeeOT[i].ot_hou == '')
                        $scope.EmployeeOT[i].ot_hou = 0.0;
                    var data = {
                        emp_id: $scope.EmployeeOT[i].emp_id,
                        comp_code: $scope.edt.comp_code,
                        year_month: $scope.edt.year_month,
                        ot_type: $scope.EmployeeOT[i].ot_type,
                        ot_hou: $scope.EmployeeOT[i].ot_hou
                    }
                    listdata.push(data);
                }

                $http.post(ENV.apiUrl + "api/OvertimeHour/UpdateOT", listdata).then(function (UpdateOT) {
                    $scope.UpdateOT = UpdateOT.data;
                    if ($scope.UpdateOT==true)
                        swal('', 'Overtime updated successfully.');
                    else if($scope.UpdateOT == false)
                        swal('', 'Overtime not updated.');
                    else {
                        swal("Error-" + $scope.UpdateOT)
                    }
                });
                $scope.rgvtbl = false; $scope.buzy = true;
                $scope.reset_form();
            }

            $http.post(ENV.apiUrl + "api/OvertimeHour/OTtype").then(function (OTtype) {
                $scope.OTtype = OTtype.data;

                setTimeout(function () {
                    $('#cmb_grade').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);

            });

            $(function () {
                $('#cmb_grade').multipleSelect({
                    width: '100%'
                });
            });

            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);




        }])
})();





