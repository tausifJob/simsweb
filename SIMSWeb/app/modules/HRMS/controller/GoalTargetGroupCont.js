﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('GoalTargetGroupCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

          //=========================================================Start Goal Target KPI Measure================================================================
            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/GoalTarget/getAcademicYear").then(function (Acyear) {
                $scope.Acc_year = Acyear.data;
                console.log($scope.Acc_year);
                $scope.temp['sims_sip_academic_year'] = $scope.Acc_year[0].sims_sip_academic_year;
            });
           
            $http.get(ENV.apiUrl + "api/GoalTarget/getGoalName").then(function (goalName) {
                $scope.goal_Name = goalName.data;
                console.log($scope.goal_Name);
            });

            $scope.getGoalTarget = function (goalcode) {
                $http.get(ENV.apiUrl + "api/GoalTargetKPI/getGoalTargetName?sims_sip_goal_code=" + goalcode).then(function (targetName) {
                    $scope.target_Name = targetName.data;
                    console.log($scope.target_Name);
                });
            }

            $scope.getGoalTargetKPI = function (goalCode, tarCode) {
                $http.get(ENV.apiUrl + "api/GoalTargetGroup/getGoalTargetKPIMeasure?sims_sip_goal_code=" + goalCode + "&sims_sip_goal_target_code=" + tarCode).then(function (goalTargetKPI) {
                    $scope.goal_TargetKPI = goalTargetKPI.data;
                    console.log($scope.goal_TargetKPI);
                });
            }

            $http.get(ENV.apiUrl + "api/GoalTargetGroup/getAllGoalTargetKPIMeasure").then(function (res1) {
                $scope.goal_TarKPIMeasure = res1.data;
                $scope.totalItems = $scope.goal_TarKPIMeasure.length;
                $scope.todos = $scope.goal_TarKPIMeasure;
                $scope.makeTodos();
            });
            
            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    //$scope.makeTodos();
                    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.goal_TarKPI, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.goal_TarKPI;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {

                /* Search Text in all 3 fields */
                return (item.sims_sip_goal_target_kpi_measure_min_point.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_sip_goal_target_kpi_measure_max_point.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_sip_goal_target_kpi_measure_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_sip_goal_target_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_sip_goal_target_kpi_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_sip_goal_target_kpi_desc == toSearch) ? true : false;
            }

            $scope.chkMaxMin = function () {
                if ($scope.temp.sims_sip_goal_target_kpi_measure_max_point <= $scope.temp.sims_sip_goal_target_kpi_measure_min_point) {
                    swal({ title: "Alert", text: "Maximum Point should greater than Minimum Point", width: 300, height: 200 });
                }
            }

            $scope.NewGTM = function () {
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.gtdisabled = false;
                $scope.gtkpidisabled = false;
                $scope.gdescdisabled = false;
                $scope.disabled = false;
                $scope.readonly = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = "";
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp = {};
               
                $scope.temp['sims_sip_academic_year'] = $scope.Acc_year[0].sims_sip_academic_year;
                $scope.temp.sims_sip_goal_target_kpi_measure_status = true
             
            }

            var datasend = [];
            $scope.savedataGTM = function (MyformGTM) {
                debugger
                if (MyformGTM) {
                    var data = $scope.temp;
                    data.opr = 'I';
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/GoalTargetGroup/CUDGoalTargetKPIMeasure", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully M", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Inserted. M ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        $http.get(ENV.apiUrl + "api/GoalTargetGroup/getAllGoalTargetKPIMeasure").then(function (res1) {
                            $scope.goal_TarKPIMeasure = res1.data;
                            $scope.totalItems = $scope.goal_TarKPIMeasure.length;
                            $scope.todos = $scope.goal_TarKPIMeasure;
                            $scope.makeTodos();

                        });

                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.sims_sip_academic_year = "";
                $scope.temp.sims_sip_goal_code = "";
                $scope.temp.sims_sip_goal_target_code = "";
                $scope.temp.sims_sip_goal_target_kpi_code = "";
                $scope.temp.sims_sip_goal_target_kpi_measure_desc = "";
                $scope.temp.sims_sip_goal_target_kpi_measure_min_point = "";
                $scope.temp.sims_sip_goal_target_kpi_measure_max_point = "";
                $scope.temp.sims_sip_goal_target_kpi_measure_status = "";
            }

            $scope.edit = function (str) {
                $scope.stry = str;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.gdisabled = true;
                $scope.aydisabled = true;
                $scope.gtdisabled = true;
                $scope.gtkpidisabled = true;
                $scope.gdescdisabled = true;

                $scope.getGoalTarget(str.sims_sip_goal_code);
                $scope.getGoalTargetKPI(str.sims_sip_goal_code, str.sims_sip_goal_target_code);

                $scope.temp = {
                    sims_sip_goal_code: str.sims_sip_goal_code
                   , sims_sip_goal_target_code: str.sims_sip_goal_target_code
                   , sims_sip_academic_year: str.sims_sip_academic_year
                   , sims_sip_goal_target_kpi_code: str.sims_sip_goal_target_kpi_code
                    , sims_sip_goal_target_kpi_measure_code: str.sims_sip_goal_target_kpi_measure_code
                   , sims_sip_goal_target_kpi_measure_desc: str.sims_sip_goal_target_kpi_measure_desc
                   , sims_sip_goal_target_kpi_measure_max_point: str.sims_sip_goal_target_kpi_measure_max_point
                    , sims_sip_goal_target_kpi_measure_min_point: str.sims_sip_goal_target_kpi_measure_min_point
                    , sims_sip_goal_target_kpi_measure_status: str.sims_sip_goal_target_kpi_measure_status
                };

            }

            var dataupdate = [];
            $scope.updateGTM = function (MyformGTM) {
                if (MyformGTM) {
                    var data = $scope.temp;
                    data.opr = "U";
                    dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/GoalTargetGroup/CUDGoalTargetKPIMeasure", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully M", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. M", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        $http.get(ENV.apiUrl + "api/GoalTargetGroup/getAllGoalTargetKPIMeasure").then(function (res1) {
                            $scope.goal_TarKPIMeasure = res1.data;
                            $scope.totalItems = $scope.goal_TarKPIMeasure.length;
                            $scope.todos = $scope.goal_TarKPIMeasure;
                            $scope.makeTodos();
                        });
                    });
                    dataupdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                debugger;
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({

                            'sims_sip_goal_code': $scope.filteredTodos[i].sims_sip_goal_code,
                            'sims_sip_goal_target_code': $scope.filteredTodos[i].sims_sip_goal_target_code,
                            'sims_sip_goal_target_kpi_code': $scope.filteredTodos[i].sims_sip_goal_target_kpi_code,
                            'sims_sip_goal_target_kpi_measure_auto_code': $scope.filteredTodos[i].sims_sip_goal_target_kpi_measure_auto_code,
                            'opr': "D"
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/GoalTargetGroup/CUDGoalTargetKPIMeasure", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully M", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/GoalTargetGroup/getAllGoalTargetKPIMeasure").then(function (res1) {
                                                $scope.goal_TarKPIMeasure = res1.data;
                                                $scope.totalItems = $scope.goal_TarKPIMeasure.length;
                                                $scope.todos = $scope.goal_TarKPIMeasure;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. M", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/GoalTargetGroup/getAllGoalTargetKPIMeasure").then(function (res1) {
                                                $scope.goal_TarKPIMeasure = res1.data;
                                                $scope.totalItems = $scope.goal_TarKPIMeasure.length;
                                                $scope.todos = $scope.goal_TarKPIMeasure;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }

            //=========================================================Start Goal Target KPI================================================================

            $scope.getGoalTarget = function (goalcode) {
                $http.get(ENV.apiUrl + "api/GoalTargetGroup/getGoalTargetName?sims_sip_goal_code=" + goalcode).then(function (targetName) {
                    $scope.target_Name = targetName.data;
                    console.log($scope.target_Name);
                });
            }

            $http.get(ENV.apiUrl + "api/GoalTargetGroup/getAllGoalTargetKPI").then(function (res1) {
                $scope.Goal_target_KPI = res1.data;
                $scope.totalItems = $scope.Goal_target_KPI.length;
                $scope.todos = $scope.Goal_target_KPI;
                $scope.makeTodos();
            });

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Goal_target_KPI, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Goal_target_KPI;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_sip_goal_target_kpi_max_point.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_sip_goal_target_kpi_min_point.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_sip_goal_target_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_sip_goal_target_kpi_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_sip_goal_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_sip_goal_target_kpi_min_point == toSearch) ? true : false;
            }

            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "/" + month + "/" + day;
            }

            $scope.NewKPI = function () {

                $scope.disabled = false;
                $scope.gtcreadonly = true;
                $scope.gtdreadonly = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.readonly = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.temp = "";

                $scope.temp = {};
                $scope.temp = {
                    sims_sip_goal_target_kpi_status: true,
                    sims_sip_goal_target_kpi_measure_status:true,
                };

               // $scope.temp['sims_cur_code'] = $scope.curriculum[0].sims_cur_code;
                $scope.temp['sims_sip_academic_year'] = $scope.Acc_year[0].sims_sip_academic_year;

                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }


            var datasend = [];
            $scope.savedataKPI = function (MyformKPI) {

                if (MyformKPI) {

                    var data = $scope.temp;
                    data.opr = 'I';
                    datasend.push(data);

                    $http.post(ENV.apiUrl + "api/GoalTargetGroup/CUDGoalTargetKPI", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully KPI", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Inserted. KPI ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $http.get(ENV.apiUrl + "api/GoalTargetGroup/getAllGoalTargetKPI").then(function (res1) {
                            $scope.Goal_target_KPI = res1.data;
                            $scope.totalItems = $scope.Goal_target_KPI.length;
                            $scope.todos = $scope.Goal_target_KPI;
                            $scope.makeTodos();
                        });
                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            $scope.chkMaxMin = function () {
                if ($scope.temp.sims_sip_goal_target_kpi_max_point <= $scope.temp.sims_sip_goal_target_kpi_min_point) {
                    swal({ title: "Alert", text: "Maximum Point should greater than Minimum Point", width: 300, height: 200 });
                }
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.sims_sip_academic_year = "";
                $scope.temp.sims_sip_goal_code = "";
                $scope.temp.sims_sip_goal_target_code = "";
                $scope.temp.sims_sip_goal_target_kpi_code = "";
                $scope.temp.sims_sip_goal_target_kpi_desc = "";
                $scope.temp.sims_sip_goal_target_kpi_max_point = "";
                $scope.temp.sims_sip_goal_target_kpi_min_point = "";

            }

            $scope.edit = function (str) {

                $scope.gtcreadonly = true;
                $scope.gtdreadonly = true;
                $scope.gdisabled = true;
                $scope.aydisabled = true;
                $scope.readonly = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                // $scope.temp = str;
                $scope.getGoalTarget(str.sims_sip_goal_code);
                debugger;
                $scope.temp = {
                    sims_sip_academic_year: str.sims_sip_academic_year
                   , sims_sip_goal_code: str.sims_sip_goal_code
                    , sims_sip_goal_target_code: str.sims_sip_goal_target_code
                   , sims_sip_goal_target_kpi_desc: str.sims_sip_goal_target_kpi_desc
                   , sims_sip_goal_target_kpi_min_point: str.sims_sip_goal_target_kpi_min_point
                   , sims_sip_goal_target_kpi_max_point: str.sims_sip_goal_target_kpi_max_point
                   , sims_sip_goal_target_kpi_status: str.sims_sip_goal_target_kpi_status
                   , sims_sip_goal_target_kpi_code: str.sims_sip_goal_target_kpi_code
                    , sims_sip_goal_target_status: str.sims_sip_goal_target_status
                };
            }

            var dataupdate = [];
            $scope.updateKPI = function (MyformKPI) {
                if (MyformKPI) {
                    var data = $scope.temp;
                    data.opr = 'U';
                    dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/GoalTargetGroup/CUDGoalTargetKPI", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully KPI", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated KPI. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        $http.get(ENV.apiUrl + "api/GoalTargetGroup/getAllGoalTargetKPI").then(function (res1) {
                            $scope.Goal_target_KPI = res1.data;
                            $scope.totalItems = $scope.Goal_target_KPI.length;
                            $scope.todos = $scope.Goal_target_KPI;
                            $scope.makeTodos();
                        });
                    });
                    dataupdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk_kpi');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk_kpi');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                debugger;
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_sip_goal_target_code': $scope.filteredTodos[i].sims_sip_goal_target_code,
                            'sims_sip_goal_code': $scope.filteredTodos[i].sims_sip_goal_code,
                            'sims_sip_goal_target_kpi_code': $scope.filteredTodos[i].sims_sip_goal_target_kpi_code,
                            'opr': "D"
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/GoalTargetGroup/CUDGoalTargetKPI", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully KPI", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/GoalTargetGroup/getAllGoalTargetKPI").then(function (res1) {
                                                $scope.Goal_target_KPI = res1.data;
                                                $scope.totalItems = $scope.Goal_target_KPI.length;
                                                $scope.todos = $scope.Goal_target_KPI;
                                                $scope.makeTodos();
                                            });

                                            main = document.getElementById('mainchk_kpi');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. KPI ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/GoalTargetKPI/getAllGoalTargetKPI").then(function (res1) {
                                                $scope.Goal_target_KPI = res1.data;
                                                $scope.totalItems = $scope.Goal_target_KPI.length;
                                                $scope.todos = $scope.Goal_target_KPI;
                                                $scope.makeTodos();
                                            });

                                            main = document.getElementById('mainchk_kpi');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            main = document.getElementById('mainchk_kpi');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }

        //============================================================Goal G========================================================================

            $scope.chkMaxMin = function () {
                if ($scope.temp.sims_sip_goal_max_point <= $scope.temp.sims_sip_goal_min_point) {
                    swal({ title: "Alert", text: "Maximum Point should greater than Minimum Point", width: 300, height: 200 });
                }
            }

            $http.get(ENV.apiUrl + "api/GoalTarget/getGoalName").then(function (goalName) {
                $scope.goal_Name = goalName.data;
                console.log($scope.goal_Name);
            });

            $http.get(ENV.apiUrl + "api/GoalTarget/getAcademicYear").then(function (Acyear) {
                $scope.Acc_year = Acyear.data;
                console.log($scope.Acc_year);

                $scope.temp['sims_sip_academic_year'] = $scope.Acc_year[0].sims_sip_academic_year;

            });

            $http.get(ENV.apiUrl + "api/Goal/Get_revision_type").then(function (RevType) {
                $scope.Revision_type = RevType.data;
                console.log($scope.Revision_type);
                //  $scope.temp['sims_sip_academic_year'] = $scope.Acc_year[0].sims_sip_academic_year;
            });


            $http.get(ENV.apiUrl + "api/Goal/getAllGoal").then(function (res1) {
                $scope.Goal_Details = res1.data;
                $scope.totalItems = $scope.Goal_Details.length;
                $scope.todos = $scope.Goal_Details;
                $scope.makeTodos();
            });

            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.Goal_Details;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Goal_Details, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Goal_Details;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_sip_goal_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_sip_goal_revision_type_period.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_sip_goal_max_point.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_sip_goal_min_point.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_sip_goal_revision_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_sip_grading_scale == toSearch) ? true : false;
            }


            $scope.compareDate = function () {
                if ($scope.temp.sims_sip_goal_end_date < $scope.temp.sims_sip_goal_start_date) {
                    swal({ title: "Alert", text: "End Date Should greater than Start Date", width: 300, height: 200 });
                }
            }

            $scope.NewG = function () {
                $scope.temp = "";
                $scope.disabled = false;
                $scope.gtcreadonly = true;
                $scope.gtdreadonly = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;

                $scope.temp = {
                    sims_sip_goal_start_date: dd + '-' + mm + '-' + yyyy,
                    sims_sip_goal_end_date: dd + '-' + mm + '-' + yyyy,
                    sims_sip_goal_status: true
                }
                $scope.temp['sims_sip_academic_year'] = $scope.Acc_year[0].sims_sip_academic_year;

                $scope.MyformG.$setPristine();
                $scope.MyformG.$setUntouched();
            }

            var datasend = [];

            $scope.savedataG = function (MyformG) {
                debugger;
                if (MyformG) {
                    var data = {
                        sims_sip_academic_year: $scope.temp.sims_sip_academic_year
                   , sims_sip_goal_code: $scope.temp.sims_sip_goal_code
                   , sims_sip_goal_desc: $scope.temp.sims_sip_goal_desc
                   , sims_sip_goal_start_date: $scope.temp.sims_sip_goal_start_date
                   , sims_sip_goal_end_date: $scope.temp.sims_sip_goal_end_date
                   , sims_sip_goal_revision_type: $scope.temp.sims_sip_goal_revision_type
                   , sims_sip_goal_revision_type_period: $scope.temp.sims_sip_goal_revision_type_period
                   , sims_sip_goal_max_point: $scope.temp.sims_sip_goal_max_point
                   , sims_sip_goal_min_point: $scope.temp.sims_sip_goal_min_point
                   , sims_sip_goal_status: $scope.temp.sims_sip_goal_status
                   , sims_sip_grading_scale: $scope.temp.sims_sip_grading_scale
                   , opr: 'I'
                    };
                    datasend.push(data);

                    $http.post(ENV.apiUrl + "api/Goal/InsertUpdateGoal", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        $http.get(ENV.apiUrl + "api/Goal/getAllGoal").then(function (res1) {
                            $scope.Goal_Details = res1.data;
                            $scope.totalItems = $scope.Goal_Details.length;
                            $scope.todos = $scope.Goal_Details;
                            $scope.makeTodos();

                        });
                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

                $scope.temp.sims_sip_academic_year = "";
                $scope.temp.sims_sip_goal_code = "";
                $scope.temp.sims_sip_goal_desc = "";
                $scope.temp.sims_sip_goal_start_date = "";
                $scope.temp.sims_sip_goal_end_date = "";
                $scope.temp.sims_sip_goal_revision_type = "";
                $scope.temp.sims_sip_goal_revision_type_period = "";
                $scope.temp.sims_sip_goal_max_point = "";
                $scope.temp.sims_sip_goal_min_point = "";
                $scope.temp.sims_sip_goal_status = "";
                $scope.temp.sims_sip_grading_scale = "";
            }

            $scope.edit = function (str) {
                $scope.gtcreadonly = true;
                $scope.gtdreadonly = true;
                $scope.gdisabled = true;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;

                $scope.temp = {
                    sims_sip_academic_year: str.sims_sip_academic_year
                    , sims_sip_goal_code: str.sims_sip_goal_code
                    , sims_sip_goal_desc: str.sims_sip_goal_desc
                    , sims_sip_goal_start_date: str.sims_sip_goal_start_date
                    , sims_sip_goal_end_date: str.sims_sip_goal_end_date
                    , sims_sip_goal_revision_type: str.sims_sip_goal_revision_type
                    , sims_sip_goal_revision_type_period: str.sims_sip_goal_revision_type_period
                    , sims_sip_goal_max_point: str.sims_sip_goal_max_point
                    , sims_sip_goal_min_point: str.sims_sip_goal_min_point
                    , sims_sip_goal_status: str.sims_sip_goal_status
                    , sims_sip_grading_scale: str.sims_sip_grading_scale
                };
            }

            var dataupdate = [];
            $scope.updateG = function (MyformG) {

                if (MyformG) {
                    var data = $scope.temp;
                    data.opr = 'U';
                    dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/Goal/InsertUpdateGoal", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $http.get(ENV.apiUrl + "api/Goal/getAllGoal").then(function (res1) {
                            $scope.Goal_Details = res1.data;
                            $scope.totalItems = $scope.Goal_Details.length;
                            $scope.todos = $scope.Goal_Details;
                            $scope.makeTodos();
                        });
                    });
                    dataupdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                debugger;
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_sip_academic_year': $scope.filteredTodos[i].sims_sip_academic_year,
                            'sims_sip_goal_code': $scope.filteredTodos[i].sims_sip_goal_code,
                            'opr': "D"
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger;
                            $http.post(ENV.apiUrl + "api/Goal/InsertUpdateGoal", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Goal/getAllGoal").then(function (res1) {
                                                $scope.Goal_Details = res1.data;
                                                $scope.totalItems = $scope.Goal_Details.length;
                                                $scope.todos = $scope.Goal_Details;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Goal/getAllGoal").then(function (res1) {
                                                $scope.Goal_Details = res1.data;
                                                $scope.totalItems = $scope.Goal_Details.length;
                                                $scope.todos = $scope.Goal_Details;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;

                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }

                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.temp = {
                sims_sip_goal_start_date: dd + '-' + mm + '-' + yyyy,
                sims_sip_goal_end_date: dd + '-' + mm + '-' + yyyy,
                sims_sip_goal_status: true
            }


        //============================================================Goal Target GT========================================================================

            $scope.chkMaxMin = function () {
                if ($scope.temp.sims_sip_goal_target_max_point <= $scope.temp.sims_sip_goal_target_min_point) {
                    swal({ title: "Alert", text: "Maximum Point should greater than Minimum Point", width: 300, height: 200 });
                }
            }

            $http.get(ENV.apiUrl + "api/GoalTarget/getGoalName").then(function (goalName) {
                $scope.goal_Name = goalName.data;
                console.log($scope.goal_Name);
            });

            $http.get(ENV.apiUrl + "api/GoalTarget/getAcademicYear").then(function (Acyear) {
                $scope.Acc_year = Acyear.data;
                console.log($scope.Acc_year);
                $scope.temp['sims_sip_academic_year'] = $scope.Acc_year[0].sims_sip_academic_year;
            });


            $http.get(ENV.apiUrl + "api/GoalTarget/getAllGoalTarget").then(function (res1) {
                $scope.Goal_target = res1.data;
                $scope.totalItems = $scope.Goal_target.length;
                $scope.todos = $scope.Goal_target;
                $scope.makeTodos();
            });

            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.Goal_target;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Goal_target, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Goal_target;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_sip_goal_target_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_sip_goal_target_max_point.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_sip_goal_target_min_point.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_sip_goal_desc == toSearch) ? true : false;
            }

            $scope.compareDate = function () {
                if ($scope.temp.sims_sip_goal_target_end_date < $scope.temp.sims_sip_goal_target_start_date) {
                    swal({ title: "Alert", text: "End Date Should greater than Start Date", width: 300, height: 200 });
                }
            }

            $scope.NewGT = function () {
                $scope.temp = "";
                $scope.disabled = false;
                $scope.gtcreadonly = true;
                $scope.gtdreadonly = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp = {};
                $scope.temp = {
                    sims_sip_goal_target_end_date: dd + '-' + mm + '-' + yyyy,
                    sims_sip_goal_target_start_date: dd + '-' + mm + '-' + yyyy,
                    sims_sip_goal_target_status: true
                }
                $scope.temp['sims_sip_academic_year'] = $scope.Acc_year[0].sims_sip_academic_year;

            }

            var datasend = [];
            $scope.savedataGT = function (MyformGT) {
                if (MyformGT) {
                    var data = {
                        sims_sip_academic_year: $scope.temp.sims_sip_academic_year
                   , sims_sip_goal_code: $scope.temp.sims_sip_goal_code
                   , sims_sip_goal_target_code: $scope.temp.sims_sip_goal_target_code
                   , sims_sip_goal_target_desc: $scope.temp.sims_sip_goal_target_desc
                   , sims_sip_goal_target_start_date: $scope.temp.sims_sip_goal_target_start_date
                   , sims_sip_goal_target_end_date: $scope.temp.sims_sip_goal_target_end_date
                   , sims_sip_goal_target_min_point: $scope.temp.sims_sip_goal_target_min_point
                   , sims_sip_goal_target_max_point: $scope.temp.sims_sip_goal_target_max_point
                   , sims_sip_goal_target_status: $scope.temp.sims_sip_goal_target_status
                   , opr: 'I'
                    };
                    datasend.push(data);

                    $http.post(ENV.apiUrl + "api/GoalTarget/CUDGoalTarget", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        $http.get(ENV.apiUrl + "api/GoalTarget/getAllGoalTarget").then(function (res1) {
                            $scope.Goal_target = res1.data;
                            $scope.totalItems = $scope.Goal_target.length;
                            $scope.todos = $scope.Goal_target;
                            $scope.makeTodos();

                        });
                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.sims_sip_academic_year = "";
                $scope.temp.sims_sip_goal_code = "";
                $scope.temp.sims_sip_goal_target_desc = "";
                $scope.temp.sims_sip_goal_target_start_date = "";
                $scope.temp.sims_sip_goal_target_end_date = "";
                $scope.temp.sims_sip_goal_target_min_point = "";
                $scope.temp.sims_sip_goal_target_max_point = "";
                $scope.temp.sims_sip_goal_target_status = "";
            }

            $scope.edit = function (str) {

                $scope.gtcreadonly = true;
                $scope.gtdreadonly = true;
                $scope.gdisabled = true;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;

                $scope.temp = {
                    sims_sip_academic_year: str.sims_sip_academic_year
                   , sims_sip_goal_code: str.sims_sip_goal_code
                    , sims_sip_goal_target_code: str.sims_sip_goal_target_code
                   , sims_sip_goal_target_desc: str.sims_sip_goal_target_desc
                   , sims_sip_goal_target_start_date: str.sims_sip_goal_target_start_date
                   , sims_sip_goal_target_end_date: str.sims_sip_goal_target_end_date
                   , sims_sip_goal_target_min_point: str.sims_sip_goal_target_min_point
                   , sims_sip_goal_target_max_point: str.sims_sip_goal_target_max_point
                    , sims_sip_goal_target_status: str.sims_sip_goal_target_status
                };
            }

            var dataupdate = [];
            $scope.updateGT = function (MyformGT) {

                if (MyformGT) {
                    var data = $scope.temp;
                    data.opr = 'U';
                    dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/GoalTarget/CUDGoalTarget", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $http.get(ENV.apiUrl + "api/GoalTarget/getAllGoalTarget").then(function (res1) {
                            $scope.Goal_target = res1.data;
                            $scope.totalItems = $scope.Goal_target.length;
                            $scope.todos = $scope.Goal_target;
                            $scope.makeTodos();
                        });
                    });
                    dataupdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                debugger;
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_sip_goal_target_code': $scope.filteredTodos[i].sims_sip_goal_target_code,
                            'sims_sip_goal_code': $scope.filteredTodos[i].sims_sip_goal_code,
                            'opr': "D"
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/GoalTarget/CUDGoalTarget", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/GoalTarget/getAllGoalTarget").then(function (res1) {
                                                $scope.Goal_target = res1.data;
                                                $scope.totalItems = $scope.Goal_target.length;
                                                $scope.todos = $scope.Goal_target;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/GoalTarget/getAllGoalTarget").then(function (res1) {
                                                $scope.Goal_target = res1.data;
                                                $scope.totalItems = $scope.Goal_target.length;
                                                $scope.todos = $scope.Goal_target;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;

                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }

                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.temp = {
                sims_sip_goal_target_end_date: dd + '-' + mm + '-' + yyyy,
                sims_sip_goal_target_start_date: dd + '-' + mm + '-' + yyyy
            }




        }])

})();
