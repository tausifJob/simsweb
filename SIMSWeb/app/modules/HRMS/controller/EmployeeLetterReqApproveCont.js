﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeLetterReqApproveCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {
            
            $scope.filteration_approve_status = "A";
            $scope.filteration_request_status = "A";
            
            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.EmpleaveType_data = [];
            var data1 = [];
            $scope.edt = [];
            $scope.display = false;
            $scope.table = true;
            $scope.username = $rootScope.globals.currentUser.username;
            $scope.schoolUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/Requestdoc/';

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $http.get(ENV.apiUrl + "api/EmployeeLetterReq/Get_EmpDoc").then(function (res) {               
                $scope.document = res.data;
            });



            $scope.search_data = function () {
                $http.get(ENV.apiUrl + "api/EmployeeLetterReq/Get_ReqDocApprove?date_from=" + $scope.date_from + "&date_to=" + $scope.date_to + "&sims_certificate_approved_status=" + $scope.filteration_approve_status + "&sims_certificate_request_status=" + $scope.filteration_request_status + "&userName=" + $rootScope.globals.currentUser.username).then(function (res1) {
                    $scope.CreDiv = res1.data;
                    $scope.totalItems = $scope.CreDiv.length;
                    $scope.todos = $scope.CreDiv;
                    $scope.makeTodos();
                });
            }

            $scope.search_data();

            $scope.size = function (str) {                
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.CreDiv;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_certificate_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                        || item.sims_certificate_requested_by_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    ) ? true : false;
            }

            
            $scope.New = function () {
                $scope.display = true;
                $scope.table = false;
            }
            $scope.Cancel = function () {
                $scope.temp = {};
                $scope.search_data();
                $scope.display = false;
                $scope.table = true;
            }

            var data_send = [];
            $scope.edit= function (str)
            {                
                var data = {
                    'opr': 'U',                    
                    'sims_certificate_request_number': str.sims_certificate_request_number,
                    'sims_certificate_approved_status': 'A',
                    'sims_certificate_approved_by': $scope.username
                    
                }
                data_send.push(data);
                $http.post(ENV.apiUrl + "api/EmployeeLetterReq/CUDEmployeeReqLetter", data_send).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({
                            text: "Request approved", imageUrl: "assets/img/check.png",
                            showCloseButton: true, width: 300, height: 200
                        });
                        $scope.search_data();
                    }
                    else {
                        swal({ text: "Request not approved", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                    }
                });
                datasend = [];
                $scope.table = true;
                $scope.display = false;
            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                if (Myform) {
                    var data = {
                        'opr': 'I',
                        'sims_certificate_number': $scope.temp.sims_certificate_number,
                        'sims_certificate_reason': $scope.temp.sims_certificate_reason,
                        'sims_certificate_requested_by': $scope.username,
                        'sims_certificate_approved_status': 'I',
                        'sims_certificate_request_status': 'I'
                    }
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/EmployeeLetterReq/CUDEmployeeReqLetter", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({
                                text: "Request successfully approved.", imageUrl: "assets/img/check.png",
                                showCloseButton: true, width: 300, height: 200
                            });
                            $scope.getgrid();
                        }
                        else {
                            swal({ text: "Request not approved", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }
                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }

            }

            //Image Upload Section//

            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };
          

            $scope.CancelFileUpload = function (idx) {
                $scope.images.splice(idx, 1);

            };

        }])
})();