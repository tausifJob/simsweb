﻿(function () {
    'use strict';
    var obj1, obj2, temp, main;
    var opr = '';
    var data1 = [];

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SettlementTypeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.SettlementDetail = true;
            $scope.editmode = false;
            $scope.showPager = true;
            $scope.edt = {};
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });
            var formdata = new FormData();
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/SettlementType/getComapany").then(function (res) {
                $scope.compname = res.data;
                $scope.edt.se_company_code = $scope.compname[0].comp_code;
            })

            $http.get(ENV.apiUrl + "api/SettlementType/getFormulaCode").then(function (res) {
                $scope.fcode = res.data;
            })

            $scope.size = function (str) {                
                if (str == 'All') {
                    $scope.showPager = false;
                    $scope.currentPage = 1;
                    $scope.filteredTodos = $scope.SettlementData;
                }
                else {
                    $scope.showPager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }                
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.cancel = function () {
                $scope.SettlementDetail = true;
                $scope.SettlementDataoperation = false;
                $scope.edt = {};
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.myForm.$setPristine();
                    $scope.newmode = true;
                    $scope.check = true;
                    $scope.editmode = false;
                    opr = 'S';
                    $scope.readonly = false;
                    $scope.SettlementDetail = false;
                    $scope.SettlementDataoperation = true;
                    $scope.savebtn = true;
                    $scope.updatebtn = false;
                    $scope.edt = {};
                    $scope.edt.se_company_code = $scope.compname[0].comp_code;
                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.SettlementData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.SettlementData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.check_all();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.se_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.se_code == toSearch) ? true : false;
            }

            $http.get(ENV.apiUrl + "api/SettlementType/getSettlementType").then(function (Settlement_Data) {
                $scope.SettlementData = Settlement_Data.data;
                $scope.totalItems = $scope.SettlementData.length;
                $scope.todos = $scope.SettlementData;
                $scope.makeTodos();

                console.log($scope.SettlementData);
            });

            $scope.Save = function (myForm) {
                debugger
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'I';                  
                    $scope.exist = false;
                    data1.push(data);
                    console.log(data1);
                    for (var i = 0; i < $scope.SettlementData.length; i++) {
                        if ($scope.SettlementData[i].se_formula_code == data.se_formula_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ text: "Record Already exists", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                    else {
                        $http.post(ENV.apiUrl + "api/SettlementType/SettlementTypeCUD?simsobj=", data1).then(function (msg) {
                            $scope.BoardData1 = false;
                            $http.get(ENV.apiUrl + "api/SettlementType/getSettlementType").then(function (Settlement_Data) {
                                $scope.SettlementData = Settlement_Data.data;
                                $scope.totalItems = $scope.SettlementData.length;
                                $scope.todos = $scope.SettlementData;
                                $scope.makeTodos();
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                }
                                else {
                                    swal({ text: "Record Not Inserted. " + $scope.msg1, imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                }
                            });
                        });
                        $scope.edt = {};
                        data1 = [];
                        $scope.SettlementDetail = true;
                        $scope.SettlementDataoperation = false;
                    }
                }
            }

            $scope.up = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    opr = 'U';
                    debugger
                    $scope.editmode = true;
                    $scope.newmode = false;
                    $scope.savebtn = false;
                    $scope.updatebtn = true;
                    $scope.readonly = true;
                    $scope.SettlementDetail = false;
                    $scope.SettlementDataoperation = true;
                    $scope.edt = {
                        se_company_code: str.se_company_code
                          , se_code: str.se_code
                              , se_formula_code: str.se_formula_code
                              , se_leave_tag: str.se_leave_tag


                    };
                }
            }

            $scope.Update = function (myForm) {
                debugger
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/SettlementType/SettlementTypeCUD?simsobj=", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.SettlementDataoperation = false;
                        $http.get(ENV.apiUrl + "api/SettlementType/getSettlementType").then(function (Settlement_Data) {
                            $scope.SettlementData = Settlement_Data.data;
                            $scope.totalItems = $scope.SettlementData.length;
                            $scope.todos = $scope.SettlementData;
                            formdata = new FormData();
                            $scope.makeTodos();

                            if ($scope.msg1 == true) {
                                swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            }
                            else {
                                swal({ text: "Record Not Updated. " + $scope.msg1, imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                        });

                    })
                    $scope.SettlementDetail = true;
                    $scope.SettlementDataoperation = false;
                }
            }

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].se_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].se_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.check_once = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }

            $scope.deleterecord = function () {

                debugger;
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.flag = false;
                    var deleteleave = [];
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].se_code + i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodulecode = ({
                                'se_code': $scope.filteredTodos[i].se_code,
                                opr: 'D'
                            });
                            deleteleave.push(deletemodulecode);
                        }
                    }
                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            confirmButtonText: 'Yes',
                            showCancelButton: true,
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                $http.post(ENV.apiUrl + "api/SettlementType/SettlementTypeCUD", deleteleave).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/SettlementType/getSettlementType").then(function (Settlement_Data) {
                                                    $scope.SettlementData = Settlement_Data.data;
                                                    $scope.totalItems = $scope.SettlementData.length;
                                                    $scope.todos = $scope.SettlementData;
                                                    $scope.makeTodos();
                                                });
                                                main = document.getElementById('all_chk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    $scope.row1 = '';
                                                }
                                            }

                                            $scope.currentPage = true;
                                        });
                                    }
                                    else {
                                        swal({ text: "Record Already Mapped, can't Delete. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/SettlementType/getSettlementType").then(function (Settlement_Data) {
                                                    $scope.SettlementData = Settlement_Data.data;
                                                    $scope.totalItems = $scope.SettlementData.length;
                                                    $scope.todos = $scope.SettlementData;
                                                    $scope.makeTodos();
                                                });
                                                main = document.getElementById('all_chk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    $scope.row1 = '';
                                                }
                                            }
                                        });
                                    }
                                });
                            }
                            else {
                                main = document.getElementById('all_chk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }

                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById($scope.filteredTodos[i].se_code + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    $scope.currentPage = str;
                }
            }

        }]);

})();