﻿
(function () {

    'use strict';
    var obj1, temp, opr, comp, edt;
    var main;
    var simsController = angular.module('sims.module.HRMS');
    var formulaData = [];
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('FormulaDetailsCont',
      ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
          $scope.AddedTable = false;
          $scope.rm_edt = true;
          $scope.beforeTable = true;
          $scope.tableRecord = false;
          var updateButtonShow = false;
          $scope.pagesize = '10';
          $scope.Dataformula = [];
          $scope.TempDataformula = [];
          $scope.savebtn = false;
          $scope.updatebtn = false;
          $scope.user_access = [];

          $scope.appcode = $state.current.name.split('.');
          var user = $rootScope.globals.currentUser.username;

          $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
              debugger;
              $scope.user_rights = usr_rights.data;

              for (var i = 0; i < $scope.user_rights.length; i++) {

                  if (i == 0) {
                      $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                      $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                      $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                      $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                      $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                      $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                  }
              }

              //console.log($scope.user_access);
          });
          $scope.New = function () {
              if ($scope.user_access.data_insert == false) {
                  swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
              }
              else {
                  $scope.tableRecord = true;
                  $scope.savebtn = true;
                  $scope.beforeTable = false;
                  $scope.AddedTable = false;
                  $scope.temp["company_code"] = $scope.temp["company_code"];
                  $scope.temp["company_name"] = $scope.temp["company_name"];
                  $scope.temp["Formula_name"] = '';
                  $scope.temp["pd_pay_code"] = '';
                  $scope.temp["Serial_number"] = '';
                  $scope.temp["Percantage_number"] = '';
                  $scope.temp["Month_number"] = '';
                  $scope.temp["Operator"] = '';
                  $scope.AddedTable = false;
                  $scope.Dataformula = [];
                  $scope.savebtn = false;
                  $scope.updatebtn = false;
                  $scope.updateButtonShow = false;
                  //$scope.MyForm.$setPristine();
                  //$scope.MyForm.$setUntouched();
              }
          }
          $scope.temp = {}
          $http.get(ENV.apiUrl + "api/FormulaDetails/getCompanyName").then(function (comp) {

              $scope.comp_data = comp.data;
              $scope.temp["company_code"] = $scope.comp_data[0].company_code;
              $scope.temp["company_name"] = $scope.comp_data[0].company_name;

          });

          $http.get(ENV.apiUrl + "api/FormulaDetails/GetPayCode").then(function (GetPayCode) {
              $scope.GetPayCode = GetPayCode.data;
          });

          $scope.search = function (searchData) {
              debugger;
              if (searchData != '') {
                  $http.get(ENV.apiUrl + "api/FormulaDetails/getAllFormulaDetailsSearch?formulaName=" + searchData).then(function (getAllFormulaDetails) {
                      $scope.FormulaDetailData = getAllFormulaDetails.data;
                      $scope.totalItems = $scope.FormulaDetailData.length;
                      $scope.todos = $scope.FormulaDetailData;
                      $scope.makeTodos();
                  });

              }
              else {
                  $http.get(ENV.apiUrl + "api/FormulaDetails/getAllFormulaDetails").then(function (formulaDetails) {
                      $scope.FormulaDetailData = formulaDetails.data;
                      $scope.totalItems = $scope.FormulaDetailData.length;
                      $scope.todos = $scope.FormulaDetailData;
                      $scope.makeTodos();
                  });
              }
          }
          $scope.edit = function (str) {
              debugger
              if ($scope.user_access.data_update == false) {
                  swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
              }
              else {
                  $scope.tableRecord = true;
                  $scope.beforeTable = false;
                  $scope.savebtn = false;
                  $scope.updateButtonShow = true;
                  $scope.updatebtn = true;
                  $scope.AddedTable = true;
                  $scope.temp = [];
                  $scope.Dataformula = [];
                  $http.get(ENV.apiUrl + "api/FormulaDetails/getAllFormulaDetails?formulaName=" + str.formula_name).then(function (getAllFormulaDetails) {
                      str = [];
                      str = getAllFormulaDetails.data;
                      for (var i = 0; i < str.length; i++) {
                          $scope.TempDataformula = [];
                          $scope.temp["company_code"] = str[i].company_code;
                          $scope.temp["company_name"] = str[i].company_name;
                          $scope.TempDataformula = {
                              companyname: str[i].company_name,
                              company_code: str[i].company_code,
                              formulaname: str[i].formula_name,
                              OLD_formulaname: str[i].formula_name,
                              pdpaycode: str[i].pd_pay_code,
                              pdpaycode_description: str[i].pay_code_description,
                              percantagenumber: str[i].percantage_number,
                              monthnumber: str[i].month_number,
                              operatornumber: str[i].operatore
                          }
                          $scope.Dataformula.push($scope.TempDataformula);
                      }
                  });
              }
          }
          $scope.Remove = function ($event, index, str) {
              str.splice(index, 1);
          }

          $scope.changingFormulaName = function (Userdata) {
              console.log(Userdata);
              $scope.res.formulaname = Userdata;
              console.log($scope.res.formulaname);
          }
          $scope.pay_code_change = function (payCode) {
             
          }
          $scope.addFunction = function () {
              debugger;
              var falg = false;
              $scope.savebtn = true;
              $scope.AddedTable = true;
              for (var i = 0; i < $scope.Dataformula.length; i++) {
                  if ($scope.Dataformula[i].pdpaycode == $scope.temp.pd_pay_code) {
                      falg = true;
                  }
              }
              if (falg) {
                  swal({ text: "Pay Code Record alredy present Please try again. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });

              }
              else {

                  //$scope.TempDataformula = {
                  //    companyname: $scope.temp.company_name,
                  //    company_code: $scope.temp.company_code,
                  //    formulaname: $scope.temp.Formula_name,
                  //    pdpaycode: $scope.temp.pd_pay_code,
                  //    percantagenumber: $scope.temp.Percantage_number,
                  //    monthnumber: $scope.temp.Month_number,
                  //    operatornumber: $scope.temp.Operator,
                  //}
                  for (var i = 0; i < $scope.GetPayCode.length; i++) {
                      if ($scope.GetPayCode[i].pd_pay_code == $scope.temp.pd_pay_code) {
                          if ($scope.Dataformula.length<=0) {
                              $scope.TempDataformula = {
                                  companyname: $scope.temp.company_name,
                                  company_code: $scope.temp.company_code,
                                  formulaname: $scope.temp.Formula_name,
                                  pdpaycode: $scope.temp.pd_pay_code,
                                  percantagenumber: $scope.temp.Percantage_number,
                                  monthnumber: $scope.temp.Month_number,
                                  operatornumber: $scope.temp.Operator,
                                  pdpaycode_description: $scope.GetPayCode[i].pd_pay_code_nm
                              }
                          }
                          else {
                              $scope.TempDataformula = {
                                  companyname: $scope.temp.company_name,
                                  company_code: $scope.temp.company_code,
                                  formulaname:  $scope.Dataformula[0].formulaname,
                                  pdpaycode: $scope.temp.pd_pay_code,
                                  percantagenumber: $scope.temp.Percantage_number,
                                  monthnumber: $scope.temp.Month_number,
                                  operatornumber: $scope.temp.Operator,
                                  pdpaycode_description: $scope.GetPayCode[i].pd_pay_code_nm
                              }
                          }
                      }

                  }
                  $scope.Dataformula.push($scope.TempDataformula);
              }
            

          }

          $scope.cancelFunction = function () {

              $scope.temp["company_code"] = $scope.temp["company_code"];
              $scope.temp["company_name"] = $scope.temp["company_name"];
              $scope.temp["Formula_name"] = '';
              $scope.temp["pd_pay_code"] = '';
              $scope.temp["Serial_number"] = '';
              $scope.temp["Percantage_number"] = '';
              $scope.temp["Month_number"] = '';
              $scope.temp["Operator"] = '';


          }
          $scope.deleteSelectedData = function () {
              if ($scope.user_access.data_delete == false) {
                  swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
              }
              else {
                  debugger;
                  formulaData = [];

                  $scope.flag = false;
                  for (var i = 0; i < $scope.FormulaDetailData.length  ; i++) {
                      var v = document.getElementById('helt-' + i);
                      if (v.checked == true) {
                          $scope.flag = true;
                          var dataTempFormula = {
                              'company_code': $scope.FormulaDetailData[i].company_code
                                 , 'formulaname': $scope.FormulaDetailData[i].formula_name
                          }
                          formulaData.push(dataTempFormula);
                      }

                  }
                  if ($scope.flag) {
                      swal({
                          title: '',
                          text: "Are you sure you want to Delete?",
                          showCloseButton: true,
                          showCancelButton: true,
                          confirmButtonText: 'Yes',
                          width: 380,
                          cancelButtonText: 'No',

                      }).then(function (isConfirm) {
                          if (isConfirm) {
                              $http.post(ENV.apiUrl + "api/FormulaDetails/FormulaDetailsDeleting", formulaData).then(function (msg) {
                                  debugger;
                                  $scope.msg1 = msg.data;
                                  if ($scope.msg1 == true) {
                                      swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                          if (isConfirm) {
                                              $http.get(ENV.apiUrl + "api/FormulaDetails/getAllFormulaDetails").then(function (getAllFormulaDetails) {
                                                  $scope.FormulaDetailData = getAllFormulaDetails.data;
                                                  $scope.totalItems = $scope.FormulaDetailData.length;
                                                  $scope.todos = $scope.FormulaDetailData;
                                                  $scope.makeTodos();
                                              });
                                              main = document.getElementById('mainchk');
                                              if (main.checked == true) {
                                                  main.checked = false;
                                                  {
                                                      $scope.row1 = '';
                                                  }
                                              }
                                          }
                                      });
                                  }
                                  else if ($scope.msg1 == false) {
                                      swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                          if (isConfirm) {
                                              $http.get(ENV.apiUrl + "api/FormulaDetails/getAllFormulaDetails").then(function (getAllFormulaDetails) {
                                                  $scope.FormulaDetailData = getAllFormulaDetails.data;
                                                  $scope.totalItems = $scope.FormulaDetailData.length;
                                                  $scope.todos = $scope.FormulaDetailData;
                                                  $scope.makeTodos();
                                              });
                                              main = document.getElementById('mainchk');
                                              if (main.checked == true) {
                                                  main.checked = false;
                                                  {
                                                      $scope.row1 = '';
                                                  }
                                              }
                                          }
                                      });
                                  }
                                  else {
                                      swal("Error-" + $scope.msg1)
                                  }
                              });
                          }
                          else {
                              for (var i = 0; i < $scope.FormulaDetailData.length; i++) {
                                  var v = document.getElementById(i);
                                  if (v.checked == true) {
                                      v.checked = false;
                                      $scope.row1 = '';
                                      $('tr').removeClass("row_selected");
                                  }
                              }
                          }
                      });

                  }
              }

          }
          $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
          $scope.makeTodos = function () {

              var rem = parseInt($scope.totalItems % $scope.numPerPage);
              if (rem == '0') {
                  $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
              }
              else {
                  $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
              }
              var begin = (($scope.currentPage - 1) * $scope.numPerPage);
              var end = parseInt(begin) + parseInt($scope.numPerPage);

              $scope.FormulaDetailData = $scope.todos.slice(begin, end);

          };
          $http.get(ENV.apiUrl + "api/FormulaDetails/getAllFormulaDetails").then(function (formulaDetails) {
              for (var i = 0; i < formulaDetails.data.length; i++) {
                  if ($scope.FormulaDetailData.length < 0) {
                      $scope.FormulaDetailData = formulaDetails.data[i];
                  }
                  else {
                      for (var j = 0; j < $scope.FormulaDetailData.length; j++) {
                          if ($scope.FormulaDetailData[j].formula_name != formulaDetails.data[i].formula_name) {
                              $scope.FormulaDetailData = formulaDetails.data[i];
                          }
                      }
                  }
              }
              //$scope.FormulaDetailData = formulaDetails.data;

          });
          $http.get(ENV.apiUrl + "api/FormulaDetails/getAllFormulaDetails").then(function (formulaDetails) {
              $scope.FormulaDetailData = formulaDetails.data;

          });
          function removeDuplicates(originalArray, prop) {
              var newArray = [];
              var lookupObject = {};

              for (var i in originalArray) {
                  lookupObject[originalArray[i][prop]] = originalArray[i];
              }

              for (i in lookupObject) {
                  newArray.push(lookupObject[i]);
              }
              return newArray;
          }

          $scope.saveData = function () {
              debugger;
              var datasend = [];
              var data = $scope.Dataformula;
              if (data.length > 0) {

                  datasend.push(data);
                  var sendingData = [];
                  for (var i = 0; i < datasend.length; i++) {

                      sendingData = datasend[i];
                  }
                  var formulaData = [];
                  $scope.flag = false;
                  for (var i = 0; i < $scope.FormulaDetailData.length  ; i++) {
                      if ($scope.FormulaDetailData[i].formula_name == sendingData[0].OLD_formulaname) {
                          var dataTempFormula = {
                              'company_code': $scope.FormulaDetailData[i].company_code
                               , 'formulaname': $scope.FormulaDetailData[i].formula_name

                          }
                          formulaData.push(dataTempFormula);
                          break;
                      }
                  }
                  if (formulaData.length>0) {
                      $http.post(ENV.apiUrl + "api/FormulaDetails/FormulaDetailsDeleting", formulaData).then(function (msg) {
                          var temp = msg.data;
                          if (temp) {
                              $http.post(ENV.apiUrl + "api/FormulaDetails/insertFormulaDetailsData", sendingData).then(function (msg) {
                                  $scope.msg1 = msg.data;
                                  if ($scope.msg1 == true) {
                                      swal({ text: "Record Insert Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });

                                      $http.get(ENV.apiUrl + "api/FormulaDetails/getAllFormulaDetails").then(function (formulaDetails) {
                                          $scope.FormulaDetailData = formulaDetails.data;
                                      });
                                  }
                                  else if ($scope.msg1 == false) {
                                      swal({ text: "Record alredy present Or Please try again. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                  }
                                  else {
                                      swal("Error-" + $scope.msg1)
                                  }
                                  $scope.getcur();
                              });
                          }
                      });
                  }
                  else {
                      $http.post(ENV.apiUrl + "api/FormulaDetails/insertFormulaDetailsData", sendingData).then(function (msg) {
                          $scope.msg1 = msg.data;
                          if ($scope.msg1 == true) {
                              swal({ text: "Record Insert Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });

                              $http.get(ENV.apiUrl + "api/FormulaDetails/getAllFormulaDetails").then(function (formulaDetails) {
                                  $scope.FormulaDetailData = formulaDetails.data;
                              });
                          }
                          else if ($scope.msg1 == false) {
                              swal({ text: "Record alredy present Or Please try again. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                          }
                          else {
                              swal("Error-" + $scope.msg1)
                          }
                          $scope.getcur();
                      });
                  }
                  //$http.post(ENV.apiUrl + "api/FormulaDetails/insertFormulaDetailsData", sendingData).then(function (msg) {
                  //    $scope.msg1 = msg.data;
                  //    if ($scope.msg1 == true) {
                  //        swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });

                  //        $http.get(ENV.apiUrl + "api/FormulaDetails/getAllFormulaDetails").then(function (formulaDetails) {
                  //            $scope.FormulaDetailData = formulaDetails.data;
                  //        });
                  //    }
                  //    else if ($scope.msg1 == false) {
                  //        swal({ text: "Record alredy present Or Please try again. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                  //    }
                  //    else {
                  //        swal("Error-" + $scope.msg1)
                  //    }
                  //    $scope.getcur();
                  //});
                  //datasend = [];
                  $scope.tableRecord = false;
                  $scope.savebtn = false;
                  $scope.beforeTable = true;
                  //$scope.clear();
              }
              else {
                  swal({ text: "You not enterd reuired data, Please try again. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
              }
          }

          $scope.UpdateData = function () {
             
                  debugger;
                  var datasend = [];
                  var data = $scope.Dataformula;
                  if (data.length > 0) {

                      datasend.push(data);
                      var sendingData = [];
                      for (var i = 0; i < datasend.length; i++) {

                          sendingData = datasend[i];
                      }
                      formulaData = [];
                      $scope.flag = false;
                      for (var i = 0; i < $scope.FormulaDetailData.length  ; i++) {
                          if ($scope.FormulaDetailData[i].formula_name == sendingData[0].OLD_formulaname) {
                              var dataTempFormula = {
                                  'company_code': $scope.FormulaDetailData[i].company_code
                                   , 'formulaname': $scope.FormulaDetailData[i].formula_name

                              }
                              formulaData.push(dataTempFormula);
                              break;
                          }
                      }
                      //for (var j = 0; j < $scope.FormulaDetailData.length; j++) {

                      //    if ($scope.FormulaDetailData[i].formula_name == sendingData[0].OLD_formulaname) {
                      //        sendingData[0].pdpaycode = $scope.FormulaDetailData[i].pd_pay_code;                          
                      //    }
                      //}
                      $http.post(ENV.apiUrl + "api/FormulaDetails/FormulaDetailsDeleting", formulaData).then(function (msg) {
                          var temp = msg.data;
                          if (temp) {
                              $http.post(ENV.apiUrl + "api/FormulaDetails/insertFormulaDetailsData", sendingData).then(function (msg) {
                                  $scope.msg1 = msg.data;
                                  if ($scope.msg1 == true) {
                                      swal({ text: "Record Update Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });

                                      $http.get(ENV.apiUrl + "api/FormulaDetails/getAllFormulaDetails").then(function (formulaDetails) {
                                          $scope.FormulaDetailData = formulaDetails.data;
                                      });
                                  }
                                  else if ($scope.msg1 == false) {
                                      swal({ text: "Record alredy present Or Please try again. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                  }
                                  else {
                                      swal("Error-" + $scope.msg1)
                                  }
                                  $scope.getcur();
                              });
                          }
                      });


                      //$http.post(ENV.apiUrl + "api/FormulaDetails/updateFormulaDetailsData", sendingData).then(function (msg) {
                      //    $scope.msg1 = msg.data;
                      //    if ($scope.msg1 == true) {
                      //        swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                      //        $http.get(ENV.apiUrl + "api/FormulaDetails/getAllFormulaDetails").then(function (formulaDetails) {
                      //            $scope.FormulaDetailData = formulaDetails.data;
                      //        });
                      //    }
                      //    else if ($scope.msg1 == false) {
                      //        swal({ text: "Record alredy present Or Please try again. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                      //    }
                      //    else {
                      //        swal("Error-" + $scope.msg1)
                      //    }
                      //    $scope.getcur();
                      //});
                      //datasend = [];
                      $scope.tableRecord = false;
                      $scope.savebtn = false;
                      $scope.beforeTable = true;
                      $scope.updateButtonShow = false;
                      //$scope.clear();
                  }
                  else {
                      swal({ text: "You not enterd reuired data, Please try again. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                  }
              }
          
          $scope.cancel = function () {
              $scope.tableRecord = false;
              $scope.savebtn = false;
              $scope.beforeTable = true;
              $scope.temp["company_code"] = $scope.temp["company_code"];
              $scope.temp["company_name"] = $scope.temp["company_name"];
              $scope.temp["Formula_name"] = '';
              $scope.temp["pd_pay_code"] = '';
              $scope.temp["Serial_number"] = '';
              $scope.temp["Percantage_number"] = '';
              $scope.temp["Month_number"] = '';
              $scope.temp["Operator"] = '';
              $scope.AddedTable = false;
              $scope.Dataformula = [];
              $scope.savebtn = false;
              $scope.updatebtn = false;
          }
          $scope.clear = function () {
              $scope.temp["company_code"] = $scope.temp["company_code"];
              $scope.temp["company_name"] = $scope.temp["company_name"];
              $scope.temp["Formula_name"] = '';
              $scope.temp["pd_pay_code"] = '';
              $scope.temp["Serial_number"] = '';
              $scope.temp["Percantage_number"] = '';
              $scope.temp["Month_number"] = '';
              $scope.temp["Operator"] = '';
          }


      }])


})();