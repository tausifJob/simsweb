﻿(function () {
    'use strict';
    var opr = '';
    var comncode = [];
    var main;
    var data1 = [];

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('CommonCodeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;

            $scope.table1 = true;
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            $http.get(ENV.apiUrl + "api/CommonCode/getCommonCode").then(function (getCommonCode_Data) {
                $scope.CommonCode_Data = getCommonCode_Data.data;
                $scope.totalItems = $scope.CommonCode_Data.length;
                $scope.todos = $scope.CommonCode_Data;
                $scope.makeTodos();
            });

            $http.get(ENV.apiUrl + "api/CommonCode/getLeaveDocuments").then(function (res1) {
                $scope.leaveDocuments = res1.data;
                console.log("leaveDocuments",$scope.leaveDocuments);
            });

            $scope.operation = false;
            $scope.editmode = false;
            $scope.size = function (str) {
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.CommonCode_Data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
                
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }
            $scope.edt = {};

            $scope.list_leave_desc = [];
            $scope.add_new = function () {
                if ($scope.edt.applicable_from == undefined || $scope.edt.applicable_from == '' || $scope.edt.applicable_to == undefined || $scope.edt.applicable_to == '') {
                    swal('', 'Applicable from day and applicable to day are mandatory.');
                }
                else {
                    if ($scope.edt.applicable_value == undefined || $scope.edt.applicable_value == '')
                        $scope.edt.applicable_value = 0;
                    var data = {
                        srno: $scope.list_leave_desc.length + 1,
                        applicable_from: $scope.edt.applicable_from,
                        applicable_to: $scope.edt.applicable_to,
                        applicable_value: $scope.edt.applicable_value
                    }
                    $scope.list_leave_desc.push(data);
                    if ($scope.list_leave_desc.length > 0)
                        $scope.table_lst = true;
                    else
                        $scope.table_lst = false;
                    $scope.edt.applicable_from = '';
                    $scope.edt.applicable_to = '';
                    $scope.edt.applicable_value = '';
                }
            }

            $scope.remove_lst = function (list_obj) {
                debugger;
                var lst_temp = [];
                for (var i = 0; i < $scope.list_leave_desc.length; i++) {
                    if (list_obj.srno == $scope.list_leave_desc[i].srno) {
                        // $scope.list_leave_desc.splice(i, 1);
                    }
                    else {
                        var d = {
                            applicable_from: $scope.list_leave_desc[i].applicable_from,
                            applicable_to: $scope.list_leave_desc[i].applicable_to,
                            applicable_value: $scope.list_leave_desc[i].applicable_value
                        }
                        lst_temp.push(d);
                    }
                }
                $scope.list_leave_desc = [];
                if (lst_temp.length > 0) {
                    for (var i = 0; i < lst_temp.length; i++) {
                        var p = {
                            srno: i + 1,
                            applicable_from: lst_temp[i].applicable_from,
                            applicable_to: lst_temp[i].applicable_to,
                            applicable_value: lst_temp[i].applicable_value
                        }
                        $scope.list_leave_desc.push(p);

                    }
                }

            }


            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.myForm.$setPristine();
                    $scope.myForm.$setUntouched();
                    $scope.newmode = true;
                    $scope.check = true;
                    $scope.edt = '';
                    $scope.editmode = false;
                    $scope.readonly = false;
                    $scope.table1 = false;
                    $scope.operation = true;
                    $scope.savebtn = true;
                    $scope.updatebtn = false;
                    $scope.clCodeReadonly = false;
                    $scope.table_lst = false;
                    $scope.list_leave_desc = [];
                }
            }

            $scope.up = function (str) {

                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.editmode = true;
                    $scope.newmode = false;
                    $scope.savebtn = false;
                    $scope.updatebtn = true;
                    $scope.readonly = true;
                    $scope.table1 = false;
                    $scope.operation = true;
                    $scope.clCodeReadonly = true;
                    debugger;
                    $scope.edt = {
                        cl_code: str.cl_code,
                        cl_desc: str.cl_desc,
                        cl_change_desc: str.cl_change_desc,
                        cl_carry_forward: str.cl_carry_forward,
                        cl_leave_document_code: str.cl_leave_document_code
                    }
                    $http.post(ENV.apiUrl + "api/CommonCode/CommondetailsCode?code=" + str.cl_code).then(function (CommondetailsCode) {
                        $scope.list_leave_desc = CommondetailsCode.data;
                        if ($scope.list_leave_desc.length > 0)
                            $scope.table_lst = true;
                        else
                            $scope.table_lst = false;
                    });

                }
            }
            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.edt = {
                    cl_code: '',
                    cl_desc: '',
                    cl_change_desc: '',
                    cl_carry_forward:false
                }
                $http.get(ENV.apiUrl + "api/CommonCode/getCommonCode").then(function (getCommonCode_Data) {
                    $scope.CommonCode_Data = getCommonCode_Data.data;
                    $scope.totalItems = $scope.CommonCode_Data.length;
                    $scope.todos = $scope.CommonCode_Data;
                    $scope.makeTodos();
                });
            }

            $scope.save_all = function () {
                debugger;
                if ($scope.edt.cl_code == undefined || $scope.edt.cl_code == '' || $scope.edt.cl_desc == undefined || $scope.edt.cl_desc == '' || $scope.edt.cl_change_desc == undefined || $scope.edt.cl_change_desc == '') {
                    swal('', 'All fields are mandatory.');
                }
                else {
                    var tlt_lst = [];
                    if ($scope.list_leave_desc.length > 0) {

                    }
                    else {
                        var d = {
                            applicable_from: '0',
                            applicable_to: '0',
                            applicable_value: '0'
                        }
                        $scope.list_leave_desc.push(d);
                    }
                    var data = {
                        cl_code: $scope.edt.cl_code,
                        cl_desc: $scope.edt.cl_desc,
                        cl_change_desc: $scope.edt.cl_change_desc,
                        cl_carry_forward: $scope.edt.cl_carry_forward,
                        cmn_det: $scope.list_leave_desc,
                        cl_leave_document_code: $scope.edt.cl_leave_document_code
                    }
                    console.log("cl_carry_forward", $scope.edt.cl_carry_forward);
                    console.log("data", data);

                    $http.post(ENV.apiUrl + "api/CommonCode/InsertCommonCode", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1==true) {
                            swal('', 'Leave code inserted successfully.');
                            $scope.cancel();
                        }
                        else if($scope.msg1==false){
                            swal('', 'Leave code is not inserted.');
                            $scope.cancel();
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                    });

                }
            }

            $scope.Update = function () {
              
                    data1 = [];

                    if ($scope.list_leave_desc.length > 0) {

                    }
                    else {
                        var d = {
                            applicable_from: '0',
                            applicable_to: '0',
                            applicable_value: '0'
                        }
                        $scope.list_leave_desc.push(d);
                    }
                    var data = {
                        cl_code: $scope.edt.cl_code,
                        cl_desc: $scope.edt.cl_desc,
                        cl_change_desc: $scope.edt.cl_change_desc,
                        cl_carry_forward: $scope.edt.cl_carry_forward,
                        cmn_det: $scope.list_leave_desc,
                        cl_leave_document_code: $scope.edt.cl_leave_document_code
                    }

                    $http.post(ENV.apiUrl + "api/CommonCode/UpdateCommonCode", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal('', 'Leave code updated successfully.');
                            $scope.cancel();
                        }
                        else if ($scope.msg1 == false) {
                            swal('', 'Leave code is not updated. ');
                            $scope.cancel();
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                    });
                }
            

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].cl_code);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].cl_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.deleterecord = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    comncode = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].cl_code);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletecommoncode = ({
                                'cl_code': $scope.filteredTodos[i].cl_code,
                            });
                            comncode.push(deletecommoncode);
                        }
                    }
                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $http.post(ENV.apiUrl + "api/CommonCode/Common_codeDelete", comncode).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == 1) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/CommonCode/getCommonCode").then(function (getCommonCode_Data) {
                                                    $scope.CommonCode_Data = getCommonCode_Data.data;
                                                    $scope.totalItems = $scope.CommonCode_Data.length;
                                                    $scope.todos = $scope.CommonCode_Data;
                                                    $scope.makeTodos();
                                                });
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    {
                                                        $scope.row1 = '';
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    else if ($scope.msg1) {
                                        swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/CommonCode/getCommonCode").then(function (getCommonCode_Data) {
                                                    $scope.CommonCode_Data = getCommonCode_Data.data;
                                                    $scope.totalItems = $scope.CommonCode_Data.length;
                                                    $scope.todos = $scope.CommonCode_Data;
                                                    $scope.makeTodos();
                                                });
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    {
                                                        $scope.row1 = '';
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });
                            }
                            else {
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById($scope.filteredTodos[i].cl_code);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        $scope.row1 = '';
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    main.checked = false;
                    $scope.currentPage = true;
                    $scope.row1 = '';
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.CommonCode_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CommonCode_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                     item.cl_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.cl_change_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.cl_code == toSearch) ? true : false;
            }


        }])
})();
