﻿(function () {
    'use strict';
    var del = [];
    var main;
    var main1;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('LeaveTypeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;

            $scope.leaveType_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.edit_code = false;
            $scope.div_leave = false;

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            $scope.edt = {};
            $http.get(ENV.apiUrl + "api/common/ShiftMaster/getCompany").then(function (res) {
                $scope.comp_data = res.data;
                if (res.data.length > 0)
                {
                    $scope.edt.le_company_code = res.data[0].sh_company_code;
                }
                console.log($scope.edt.le_company_code);
            });

            $http.get(ENV.apiUrl + "api/common/LeaveType/Getleavetype").then(function (res) {
                $scope.stafftype_data = res.data;
                console.log($scope.stafftype_data);
            });

            $http.get(ENV.apiUrl + "api/common/LeaveType/Get_leave_codes").then(function (res) {
                $scope.leaveCode_data = res.data;
                console.log($scope.comp_data);
            });

            $http.get(ENV.apiUrl + "api/common/LeaveType/Get_pay_leavetype").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.leaveType_data = res.data;
                $scope.totalItems = $scope.leaveType_data.length;
                $scope.todos = $scope.leaveType_data;
                $scope.makeTodos();
                $scope.grid = true;
            });

            $http.get(ENV.apiUrl + "api/common/LeaveType/Get_accumulate_leave_types").then(function (res) {
                $scope.accumulate_types = res.data;
                console.log($scope.accumulate_types);
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edit_code = true;
                $scope.div_leave = true;

                $scope.edt =
                    {
                        le_company_code: str.le_company_code,
                        le_max_days_allowed: str.le_max_days_allowed,
                        le_leave_code: str.le_leave_code,
                        le_accumulated_tag: str.le_accumulated_tag,
                        staff_code: str.staff_code
                    }
                $scope.Getcatinfo(str.gr_company_code);
            }

            $scope.Save = function (isvalidate) {
                var data1 = [];
                debugger
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = ({
                            le_company_code: $scope.edt.le_company_code,
                            le_max_days_allowed: $scope.edt.le_max_days_allowed,
                            le_leave_code: $scope.edt.le_leave_code,
                            le_accumulated_tag: $scope.edt.le_accumulated_tag,
                            staff_code: $scope.edt.staff_code,
                            led_lwp_consider: $scope.edt.led_lwp_consider,
                            led_accural_status: $scope.edt.led_accural_status,
                            led_accural_type: $scope.edt.led_accural_type,
                            led_accural_days: $scope.edt.led_accural_days,
                            led_created_user: $scope.edt.led_created_user,
                            opr: 'I'
                        });

                        data1.push(data);

                        $http.post(ENV.apiUrl + "api/common/LeaveType/CUDLeaveType", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Leave Type Added Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal({ text: "Leave Type Already Exists. " + $scope.msg1, imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $scope.currentPage = 1;

                $http.get(ENV.apiUrl + "api/common/LeaveType/Get_pay_leavetype").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.leaveType_data = res.data;
                    $scope.totalItems = $scope.leaveType_data.length;
                    $scope.todos = $scope.leaveType_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.row1 = '';

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {
                debugger;
                var data1 = [];
                if (isvalidate) {
                    var data = ({
                        le_company_code: $scope.edt.le_company_code,
                        le_max_days_allowed: $scope.edt.le_max_days_allowed,
                        le_leave_code: $scope.edt.le_leave_code,
                        le_accumulated_tag: $scope.edt.le_accumulated_tag,
                        staff_code: $scope.edt.staff_code,
                        led_lwp_consider: $scope.edt.led_lwp_consider,
                        led_accural_status: $scope.edt.led_accural_status,
                        led_accural_type: $scope.edt.led_accural_type,
                        led_accural_days: $scope.edt.led_accural_days,
                        led_created_user: $scope.edt.led_created_user,
                        opr: 'U'
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/LeaveType/CUDLeaveType", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Leave Type Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal({ text: "This Leave Type is already Mapped.You cannot Update this Leave Type. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                    });
                }
            }

            $scope.Delete = function () {
                var deletecode = [];
                $scope.flag = false;
                debugger;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById('test-' + i);

                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'le_company_code': $scope.filteredTodos[i].le_company_code,
                            'le_leave_code': $scope.filteredTodos[i].le_leave_code,
                            'staff_code': $scope.filteredTodos[i].staff_code,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/common/LeaveType/CUDLeaveType", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Leave Type Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                                else {
                                    swal({ text: "This Leave Type is already Mapped.You cannot Delete this Leave Type. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }

            }

            $scope.New = function () {
                $scope.edit_code = false;
                var autoid;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = [];
                $scope.div_leave = false;

                $scope.edt =
                  {
                      le_accumulated_tag: true
                  }
               
                if ($scope.comp_data.length > 0) {
                    $scope.edt.le_company_code = $scope.comp_data[0].sh_company_code;
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.div_leave = false;
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('test-' + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('test-' + i);
                        v.checked = false;
                        main.checked = false;
                        //$scope.row1 = '';
                        $scope.row1 = 'row_selected';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function (str)
            {
                if (str.ischecked == true)
                {
                    for (var i = 0; i < $scope.filteredTodos.length; i++)
                    {
                        if ($scope.filteredTodos[i].le_leave_code == str.le_leave_code && $scope.filteredTodos[i].staff_code == str.staff_code)
                        {
                            str.ischecked = true;
                            str.row_selected = '#ffffcc';

                        }
                        else
                        {
                            $scope.filteredTodos[i].ischecked = false;
                            $scope.filteredTodos[i].row_selected = '#fff';
                        }
                    }
                }
                else
                {
                    str.ischecked = false;
                    str.row_selected = '#fff';
                }
            }

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.leaveType_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {
                debugger;
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                debugger
                $scope.todos = $scope.searched($scope.leaveType_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.leaveType_data;
                }
                $scope.makeTodos();


            }


            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.le_leave_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                        item.acno == toSearch) ? true : false;
            }

            //function searchUtil(item, toSearch) {
            //    /* Search Text in all 3 fields */
            //    return (item.cl_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            //}

        }])
})();