﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PayrollEmployeeEdisonCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.grid1 = false;
            $scope.temp = {};
            $scope.salCopy = [];

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str;
            //    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            //$scope.index = function (str) {
            //    $scope.pageindex = str;
            //    $scope.currentPage = str;
            //    console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            //}

            //$scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            //$scope.makeTodos = function () {
            //    var rem = parseInt($scope.totalItems % $scope.numPerPage);
            //    if (rem == '0') {
            //        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
            //    }
            //    else {
            //        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
            //    }

            //    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
            //    var end = parseInt(begin) + parseInt($scope.numPerPage);

            //    console.log("begin=" + begin); console.log("end=" + end);

            //    $scope.filteredTodos = $scope.todos.slice(begin, end);
            //};

            //$scope.searched = function (valLists, toSearch) {
            //    return _.filter(valLists,

            //    function (i) {
            //        /* Search Text in all  fields */
            //        return searchUtil(i, toSearch);
            //    });
            //};

            $http.get(ENV.apiUrl + "api/PaysGradeChange/Get_gc_company_Code").then(function (compcode) {
                debugger;
                $scope.comp_code = compcode.data;
                $scope.temp['gc_company_code'] = $scope.comp_code[0].gc_company_code;
                $scope.getdept($scope.temp['gc_company_code']);
                $scope.getdesg($scope.temp['gc_company_code']);

            });

            $scope.getdept = function (dept) {

                $http.get(ENV.apiUrl + "api/PayrollEmployee/Get_dept_Code?company_code=" + dept).then(function (Department) {
                    $scope.dept = Department.data;
                });
            }

            $scope.getdesg = function (design) {
                $http.get(ENV.apiUrl + "api/PayrollEmployee/Get_designation_Code?company_code=" + design).then(function (Designation) {
                    $scope.designation = Designation.data;
                });
            }

            $scope.submit = function () {
                debugger;
                $scope.busy = true;
                $scope.grid1 = true;
                $scope.ImageView = false;
                if ($scope.temp.gc_company_code == undefined || $scope.temp.gc_company_code == '') {
                    $scope.temp.gc_company_code = '';
                }
                if ($scope.temp.em_desg_code == undefined || $scope.temp.em_desg_code == '') {
                    $scope.temp.em_desg_code = '';
                }
                if ($scope.temp.dg_code == undefined || $scope.temp.dg_code == '') {
                    $scope.temp.dg_code = '';
                }
                if ($scope.temp.em_number == undefined || $scope.temp.em_number == '') {
                    $scope.temp.em_number = '';
                }
                $http.get(ENV.apiUrl + "api/PayrollEmployee/GetPayrollEmployee?com_code=" + $scope.temp.gc_company_code + '&dept_code=' + $scope.temp.dg_code + '&deg_code=' + $scope.temp.em_desg_code + '&empid=' + $scope.temp.em_number).then(function (Payroll) {

                    $scope.filteredTodos = Payroll.data;
                    $scope.totalItems = $scope.filteredTodos.length;
                    $scope.todos = $scope.filteredTodos;
                    //$scope.makeTodos();
                    $scope.busy = false;
                    $scope.page1 = true;
                    console.log($scope.filteredTodos);
                    if (Payroll.data.length > 0) { }
                    else {
                        $scope.ImageView = true;
                    }
                });
            }

            //DATA Cancel
            $scope.Cancel = function () {
                //$scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                //$scope.grid1 = false;
                //$scope.filteredTodos = '';
                //$scope.todos.slice = '';
                //$scope.page1 = false;
                $('#myModal').modal('hide');
            }

            //DATA Reset
            $scope.reset = function () {
                debugger
                $scope.temp['gc_company_code'] = $scope.comp_code[0].gc_company_code;
                $scope.getdept($scope.temp['gc_company_code']);
                $scope.getdesg($scope.temp['gc_company_code']);
                $scope.j = [];
                $scope.filteredTodos = '';
                $scope.todos.slice = '';
                $scope.page1 = false;
                $scope.table = true;
                $scope.grid1 = false;
                $scope.display = true;
            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = true;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.number = true;
                $scope.company = true;
                $scope.total = false;
                $scope.divcode_readonly = true;
                $scope.grid2 = true;
                $scope.grid3 = false;
                $scope.showPaycodeTable = false;
                $('#myModal').modal('show');
                $scope.save_btn = false;
                $scope.temp.em_number = str.em_number;
                $scope.Employee_name = str.sh_emp_name;
                var dt = new Date();
                //$scope.pa_effective_from = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
                $scope.pa_effective_from = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                $scope.Show();
            }

            $scope.Closehistory = function () {
                $scope.grid3 = false;
                $scope.grid2 = true;
            }

            //Show
            $scope.Show = function () {
                debugger;
                console.log($scope.pa_effective_from);

                if (new Date($scope.rdp_effective_upto) < new Date($scope.pa_effective_from)) {
                    swal({ title: "Alert", text: "Effective upto date cannot be less than from date", width: 380, height: 200 });
                    return;
                }
                $scope.total = true;
                $scope.showPaycodeTable = true;
                $scope.grid2 = true;
                $scope.grid3 = false;
                $scope.save_btn = true;
                $scope.busy = true;
                //var paycode_hide = ['eiama', 'eiam','eiagps'];
                $http.post(ENV.apiUrl + "api/PayrollEmployee/RemaingPaycode?sd_number=" + $scope.temp.em_number + '&start=' + $scope.pa_effective_from + '&end=' + $scope.rdp_effective_upto).then(function (RemaingPaycode) {
                    $scope.RemaingPaycode = RemaingPaycode.data;
                    console.log("RemaingPaycode",$scope.RemaingPaycode);
                });

                $http.get(ENV.apiUrl + "api/PayrollEmployee/GetSalaryGeneratedForSelectedMonthOrNotEidson?sd_number=" + $scope.temp.em_number + '&start=' + $scope.pa_effective_from + '&end=' + $scope.rdp_effective_upto).then(function (Sal) {
                    $scope.sal = Sal.data;
                    $scope.salCopy=angular.copy($scope.sal);
                    if ($scope.sal.length == 0) {
                        swal({ title: "Alert", text: "Data Not Found", width: 300, height: 200 });
                    }
                    $scope.total = 0;
                    for (var j = 0; j < Sal.data.length; j++) {
                        if (Sal.data[j].hide_flag == false)
                            $scope.total = $scope.total + Sal.data[j].pd_amount;
                    }

                    //if (paycode_hide.includes($http.defaults.headers.common['schoolId'])) {
                    //    for (var i = 0; i < $scope.sal.length; i++) {
                    //        $scope.sal[i].disabledPaycode = false;
                    //        if ($scope.sal[i].pe_insert == false) {
                    //            $scope.sal[i].disabledPaycode = true;
                    //        }
                    //    }
                    //}

                });

                //DATA SAVE INSERT

                var datasend = [];
                $scope.Save = function () {
                    debugger;
                    //if ($scope.pd_amount != null && $scope.j.pa_effective_from == null || $scope.j.pa_effective_from == undefined) {
                    //    swal({ title: "Alert", text: "Please Enter date", width: 300, height: 200 });
                    //}
                    console.log("sal", $scope.sal);
                    
                    for (var i = 0; i < $scope.sal.length; i++) {
                        for (var j = 0; j < $scope.salCopy.length; j++) {
                            if ($scope.sal[i].ischange == true) {
                                if (new Date($scope.sal[i].code) < new Date($scope.sal[i].pa_effective_from)) {
                                    swal({ title: "Alert", text: "Effective upto date cannot be less than from date", width: 380, height: 200 });
                                    return;
                                }
                                if (new Date($scope.sal[i].pa_effective_from) < new Date($scope.salCopy[i].pa_effective_from)) {
                                    swal({ title: "Alert", text: "Effective from date cannot be less than previous date", width: 380, height: 200 });
                                    return;
                                }
                                $scope.sal[i].em_number = $scope.temp.em_number;
                                $scope.sal[i].em_company_code = $scope.temp.gc_company_code;
                                if ($scope.sal[i].pd_amount != null || $scope.sal[i].pd_amount != undefined || $scope.sal[i].pd_amount != "") {
                                    if ($scope.sal[i].pa_effective_from == null || $scope.sal[i].pa_effective_from == undefined || $scope.sal[i].pa_effective_from == "") {
                                        $scope.sal[i].pa_effective_from = $filter('date')(new Date($scope.pa_effective_from), 'dd-MM-yyyy');
                                    }
                                }
                                datasend.push($scope.sal[i]);
                                break;
                            }
                        }
                    }
                    
                    console.log("datasend",datasend);

                    $http.post(ENV.apiUrl + "api/PayrollEmployee/InsertUpdatePaysPayable", datasend).then(function (Sal) {                        
                        $scope.msg1 = Sal.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted/Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else {
                            swal({ text: "Record Not Inserted/Updated", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        $scope.getgrid();
                    });
                    $scope.getgrid();
                    $scope.Cancel();
                }

                $scope.SetIschange = function (info) {
                    info.ischange = true;
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/PayrollEmployee/GetSalaryGeneratedForSelectedMonthOrNotEidson?sd_number=" + $scope.temp.em_number + '&start=' + $scope.pa_effective_from + '&end=' + $scope.rdp_effective_upto).then(function (Sal) {
                    $scope.sal = Sal.data;
                });
            }

            $scope.showHistory = function (obj) {
                console.log($scope.temp.em_number);
                console.log(obj.pd_pay_code);
                $http.post(ENV.apiUrl + "api/PayrollEmployee/HistoryPaycodeEdison?sd_number=" + $scope.temp.em_number + "&pay_code=" + obj.pd_pay_code).then(function (HistoryPaycode) {
                    $scope.HistoryPaycode = HistoryPaycode.data;
                    debugger;
                    if ($scope.HistoryPaycode.length > 0) {
                        $scope.grid2 = false;
                        $scope.grid3 = true;
                    }
                    else
                       swal({ title: "Alert", text: "No history found", width: 300, height: 200 });                      
                });
            }

            $scope.ADD_NEW = function () {

                var abc = [];
                debugger;
                if ($scope.temp.pd_pay_code == '' || $scope.temp.pd_pay_code == undefined) {                    
                    swal({ title: "Alert", text: "Please select paycode", width: 300, height: 200 });
                }
                else {
                    if ($scope.pd_amount == '' || $scope.pd_amount == undefined) {                        
                        swal({ title: "Alert", text: "Please enter amount.", width: 300, height: 200 });
                    }
                    else {
                        var data = {
                            em_number: $scope.temp.em_number,
                            em_company_code: '01',
                            pa_effective_from: $scope.pa_effective_from,
                            code: $scope.rdp_effective_upto,
                            pd_pay_code: $scope.temp.pd_pay_code,
                            pd_amount: $scope.pd_amount
                        }
                        abc.push(data);
                        $http.post(ENV.apiUrl + "api/PayrollEmployee/InsertUpdatePaysPayable", abc).then(function (pqrs) {
                            debugger
                            $scope.msg2 = pqrs.data;
                            if ($scope.msg2 == true) {
                                swal({ text: "Record Added Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                $scope.getgrid();
                                $scope.Reset();
                            }
                            else {
                                swal({ text: "Record Not Added", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                        });
                    }
                }
            }

            $scope.Reset = function () {
                $scope.temp.pd_pay_code = '';
                $scope.pd_amount = '';
                var dt = new Date();
                $scope.pa_effective_from = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();                
                $scope.rdp_effective_upto = '';
            }

            $scope.onlyNumbers = function (event) {
                debugger
                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };


        }])

})();