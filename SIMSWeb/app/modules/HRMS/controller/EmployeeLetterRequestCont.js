﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeLetterRequestCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http,$filter, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.other_pur = true;
            $scope.EmpleaveType_data = [];
            var data1 = [];
            $scope.edt = [];
            $scope.display = false;
            $scope.table = true;
            $scope.username = $rootScope.globals.currentUser.username;
            $scope.schoolUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/Requestdoc/';


            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            
            $http.get(ENV.apiUrl + "api/EmployeeLetterReq/Get_EmpDoc").then(function (res) {                
                $scope.document = res.data;
            });
           
            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.getGrid = function () {                
                $http.get(ENV.apiUrl + "api/EmployeeLetterReq/Get_ReqDoc?user=" + $scope.username).then(function (res1) {
                    debugger;
                    $scope.CreDiv = res1.data;
                    $scope.totalItems = $scope.CreDiv.length;                    
                    $scope.todos = $scope.CreDiv;
                    $scope.makeTodos();
                });
            }
            $scope.showOther = function (purposeName) {
                debugger;
                $scope.temp.other_sims_appl_parameter = "";
                if (purposeName=="8") {
                    $scope.other_pur = false;
                }
                else {
                    $scope.other_pur = true;
                }
            }
            $scope.getPurpose = function () {
                $http.get(ENV.apiUrl + "api/EmployeeLetterReq/Get_Purpose").then(function (res1) {
                    debugger;
                    $scope.obj1 = res1.data;
                    //$scope.totalItems = $scope.CreDiv.length;
                    //$scope.todos = $scope.CreDiv;
                    //$scope.makeTodos();
                });
            }
            

            var datasend1 = [];
            $scope.cnt = function (str) {
                debugger;
                var p_count = 0;
                p_count = str.print_count;
                p_count++;                
                
                var data1={
                    'opr': 'W',
                    'sims_certificate_print_count': p_count,
                    'sims_certificate_request_number': str.sims_certificate_request_number,
                    'sims_certificate_requested_by': str.sims_certificate_requested_by
                }
                datasend1.push(data1);
                if (str.print_count < 3) {
                    $http.post(ENV.apiUrl + "api/EmployeeLetterReq/CUDEmployeeIssueLetter", datasend1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {                      
                            $scope.getGrid();
                        }                       
                    });
                    $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/Requestdoc/' + str.sims_certificate_doc_path;
                    window.open($scope.url);

                }
                else {                    
                    swal('', 'Your downloading limit is exceeded (i.e. more than 3 times), please contact school for chargeable document downloads.');
                    return;
                }
            }

            $scope.getGrid();
            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.CreDiv;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                debugger;
                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_certificate_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  //  || item.sims_library_bin_code == toSearch
                    ) ? true : false;
            }

            $scope.New = function () {
                debugger;
                $scope.display = true;
                $scope.table = false;
                $scope.getPurpose();
            }
            $scope.Cancel = function () {
                $scope.temp = {};
                $scope.display = false;
                $scope.table = true;
            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                if (Myform) {                    
                    var data = {
                        'opr': 'I',                        
                        'sims_certificate_number': $scope.temp.sims_certificate_number,
                        'sims_certificate_reason': $scope.temp.sims_certificate_reason,
                        'sims_appl_parameter': $scope.temp.sims_appl_parameter,
                        'sims_certificate_requested_by': $scope.username,
                        'sims_certificate_other_purpose_reason': $scope.temp.other_sims_appl_parameter,
                        'sims_certificate_approved_status': 'I',
                        'sims_certificate_request_status': 'I'
                    }
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/EmployeeLetterReq/CUDEmployeeReqLetter", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.getGrid();
                        if ($scope.msg1 == true) {                            
                            swal({text: "Request successfully sent", imageUrl: "assets/img/check.png",
                                showCloseButton: true, width: 300, height: 200});
                            $scope.Cancel();
                        }
                        else {
                            swal({ text: "Request not sent", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }
                    });
                    $scope.getGrid();
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }

            }

            //Image Upload Section//
            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };
            var file_name = '';
            var file_name_name = '';
            $scope.file_doc = [];

            $scope.file_changed = function (element) {
                debugger
                file_name = '';
                file_name_name = '';
                var img_load = '';
                $scope.flag = false;
                var rate_name, rate_point;

                for (var i = 0; i < $scope.ratingArray.length; i++) {

                    var v = document.getElementById('mainchk' + $scope.ratingArray[i].ratingGroupCode + i);
                    if (v.checked == true) {
                        rate_name = $scope.ratingArray[i].ratingName;
                        rate_point = $scope.ratingArray[i].point;
                        $scope.flag = true;
                    }

                }
                if ($scope.flag == false) {
                    swal('', 'Please select the record');
                    return;
                }


                var v = new Date();
                $scope.grid2 = true;
                if ($scope.file_doc == undefined) {
                    $scope.file_doc = '';
                }

                if ($scope.file_doc.length > 14) {
                    swal('', 'Upload maximum 15 files.');
                    return;
                } else {
                    var img_code = [];
                    var img_code = $scope.Rating.DescriptionG;

                    file_name = 'Emoji' + '_' + $scope.Rating.DescriptionG + '_' + rate_name + '_' + rate_point;
                    $scope.photofile = element.files[0];
                    file_name_name = $scope.photofile.name;

                    var len = 0;
                    len = file_name_name.split('.');
                    var fortype = file_name_name.split('.')[len.length - 1];
                    var formatbool = false;
                    if (fortype == 'jpg' || fortype == 'png' || fortype == 'jpeg') {
                        formatbool = true;
                    }


                    if (formatbool == true) {
                        if ($scope.photofile.size > 2097152) {
                            swal('', 'File size limit not exceed upto 2 MB.')
                        } else {
                            $scope.uploading_doc1 = false;
                            $scope.photo_filename = ($scope.photofile.type);


                            var reader = new FileReader();
                            reader.onload = function (e) {
                                $scope.$apply(function () {

                                    $scope.prev_img = e.target.result;
                                    $('#' + ind).attr('src', $scope.prev_img)

                                    var request = {
                                        method: 'POST',
                                        url: ENV.apiUrl + 'api/SurveyRating/upload?filename=' + file_name + '.' + fortype + "&location=" + "Images/Emojis",
                                        data: formdata,
                                        headers: {
                                            'Content-Type': undefined
                                        }
                                    };
                                    $http(request).success(function (d) {


                                        var t = {
                                            sims508_doc: d,
                                            //sims_doc_line: sims_doc_line,
                                            sims_img_path: file_name,
                                        }
                                        $scope.file_doc.push(t);

                                        for (var i = 0; i < $scope.file_doc.length; i++) {
                                            img_load = $scope.file_doc[i].sims508_doc;
                                        }
                                        for (var i = 0; i < $scope.ratingArray.length; i++) {
                                            var v = document.getElementById('mainchk' + $scope.ratingArray[i].ratingGroupCode + i);
                                            if (img_load != undefined || img_load == '') {
                                                if (v.checked == true) {
                                                    $scope.ratingArray[i].sims_survey_rating_img_path = img_load;
                                                    v.checked = false;
                                                    $scope.row1 = '';
                                                    $('tr').removeClass("row_selected");
                                                }
                                            }
                                        }
                                        v.checked = false;
                                        $scope.row1 = '';
                                        $('tr').removeClass("row_selected");

                                        //main.checked = false;
                                        //$scope.row1 = '';
                                        //$('tr').removeClass("row_selected");
                                        $scope.uploading_doc1 = true;

                                    });

                                });
                            };
                            reader.readAsDataURL($scope.photofile);
                        }
                    } else {
                        swal('', '.' + fortype + ' File format not allowed.');
                    }
                }
                //$scope.ratingArray.sims_survey_rating_img_path = img_load;
            }

            $scope.CancelFileUpload = function (idx) {
                $scope.images.splice(idx, 1);
            };

            $scope.getGrid = function () {                
                $http.get(ENV.apiUrl + "api/EmployeeLetterReq/Get_ReqDoc?user=" + $scope.username).then(function (res1) {
                    debugger;
                    $scope.CreDiv = res1.data;
                    $scope.totalItems = $scope.CreDiv.length;                 
                    $scope.todos = $scope.CreDiv;
                    $scope.makeTodos();
                });
            }

        }])
})();