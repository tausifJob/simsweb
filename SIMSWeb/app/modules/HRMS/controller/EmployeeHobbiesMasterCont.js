﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeHobbiesMasterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            console.log("EmployeeHobbiesMasterCont controller");
            $scope.pagesize = "10";
            $scope.pageindex = 0;
            console.log("pagesize", $scope.pagesize);
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.pagerForAll = true;
            var flag = false;
            $scope.temp = {};

            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
                //function for reverse scope
            });

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/Hobbies/getAllEmpHobbiesMaster").then(function (res1) {
                $scope.all_Qual_Data = res1.data;
                $scope.totalItems = $scope.all_Qual_Data.length;
                $scope.todos = $scope.all_Qual_Data;
                $scope.makeTodos();
            });

            $scope.size = function (str) {
                if (str == 'All') {
                    $scope.currentPage = 1;
                    $scope.filteredTodos = $scope.all_Qual_Data;
                    $scope.pagerForAll = false;
                }
                else {
                    $scope.pagerForAll = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.all_Qual_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.all_Qual_Data;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.pays_hobbies_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.pays_hobbies_display_oder.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.desg_company_code == toSearch) ? true : false;
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {


                    $scope.disabled = false;
                    $scope.readonly = true;
                    $scope.table = false;
                    $scope.display = true;
                    $scope.save_btn = true;
                    $scope.Update_btn = false;
                    $scope.temp = {};
                    $scope.temp.pays_hobbies_status = true;
                    $scope.Myform.$setPristine();
                    $scope.Myform.$setUntouched();
                }
            }

            $scope.checkDescAlreadyExists = function () {
                flag = false;
                for (var i = 0; i < $scope.all_Hobbies_Data.length; i++) {
                    if ($scope.all_Qual_Data[i].pays_hobbies_desc == $scope.temp.pays_hobbies_desc) {
                        swal({ text: "hobbies description already exists", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                        $scope.temp.pays_hobbies_desc = "";
                        flag = true;
                        break;
                    }
                }
            }

            //$scope.checkOrderAlreadyExists = function () {                
            //    flag = false;
            //    for (var i = 0; i < $scope.all_Qual_Data.length; i++) {
            //        if ($scope.all_Qual_Data[i].pays_qualificaion_display_oder == $scope.temp.pays_qualificaion_display_oder) {
            //            swal({ title: "Alert", text: "Display order already exists", showCloseButton: true, width: 380, });
            //            $scope.temp.pays_qualificaion_display_oder = "";
            //            flag = true;
            //            break;
            //        }
            //    }
            //}

            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger;
                datasend = [];
                if (Myform) {

                    //debugger;
                    flag = false;
                    var data = $scope.temp;
                    //for (var i = 0; i < $scope.all_Qual_Data.length; i++) {
                    //    if ($scope.all_Qual_Data[i].pays_qualification_desc == $scope.temp.pays_qualification_desc || 
                    //        $scope.all_Qual_Data[i].pays_qualificaion_display_oder== $scope.temp.pays_qualificaion_display_oder) {
                    //        swal({ title: "Alert", text: "Record already exists", showCloseButton: true, width: 380, });
                    //        flag = true;
                    //        break;
                    //    }
                    //}

                   
                    //api/Hobbies/EmpHobbiesMaster : api File name;
                        data.opr = "I";
                        datasend.push(data);

                        $http.post(ENV.apiUrl + "api/Hobbies/EmpHobbiesMaster", datasend).then(function (msg) {
                            $scope.msg1 = msg.data;

                            if ($scope.msg1 == true) {
                                swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                            $http.get(ENV.apiUrl + "api/Hobbies/getAllEmpHobbiesMaster").then(function (res1) {
                                $scope.all_Qual_Data = res1.data;
                                $scope.totalItems = $scope.all_Qual_Data.length;
                                $scope.todos = $scope.all_Qual_Data;
                                $scope.makeTodos();
                            });
                        });
                        datasend = [];
                        $scope.table = true;
                        $scope.display = false;
                    
                    

                }
            }

            $scope.Cancel = function () {
                $scope.temp = {};
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.desg_desc = "";
                $scope.temp.desg_communication_status = "";
                flag = false;
            }

            $scope.edit = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.readonly = true;
                    $scope.disabled = true;
                    $scope.table = false;
                    $scope.display = true;
                    $scope.save_btn = false;
                    $scope.Update_btn = true;
                    $scope.temp = {
                        pays_hobbies_code: str.pays_hobbies_code,
                        pays_hobbies_desc: str.pays_hobbies_desc,
                        pays_hobbies_display_oder: str.pays_hobbies_display_oder,
                        pays_hobbies_status: str.pays_hobbies_status
                    };
                }
            }
            var dataupdate = [];
            $scope.update = function (Myform) {
           // $scope.update = function () {//Myform
                if (!flag) {
                    var Udata = $scope.temp;
                    Udata.opr = "U";
                    dataupdate.push(Udata);

                    $http.post(ENV.apiUrl + "api/Hobbies/EmpHobbiesMaster", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $http.get(ENV.apiUrl + "api/Hobbies/getAllEmpHobbiesMaster").then(function (res1) {
                            $scope.all_Qual_Data = res1.data;
                            $scope.totalItems = $scope.all_Qual_Data.length;
                            $scope.todos = $scope.all_Qual_Data;
                            $scope.makeTodos();
                        });
                    });
                    dataupdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
                else {
                    swal({ text: "Record already exists", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
            }

            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('test-' + $scope.filteredTodos[i].pays_hobbies_code);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('test-' + $scope.filteredTodos[i].pays_hobbies_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    deletefin = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('test-' + $scope.filteredTodos[i].pays_hobbies_code);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodulecode = ({
                                'pays_hobbies_code': $scope.filteredTodos[i].pays_hobbies_code,
                                opr: 'D'
                            });
                            deletefin.push(deletemodulecode);
                        }
                    }
                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $http.post(ENV.apiUrl + "api/Hobbies/EmpHobbiesMaster?data=", deletefin).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/Hobbies/getAllEmpHobbiesMaster").then(function (res1) {
                                                    $scope.all_Qual_Data = res1.data;
                                                    $scope.totalItems = $scope.all_Qual_Data.length;
                                                    $scope.todos = $scope.all_Qual_Data;
                                                    $scope.makeTodos();
                                                });
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    {
                                                        $scope.row1 = '';
                                                    }
                                                }
                                                $scope.currentPage = true;
                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Record Not Deleted.\n Or  Hobbies is assigned to employee", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {

                                                $http.get(ENV.apiUrl + "api/Hobbies/getAllEmpHobbiesMaster").then(function (res1) {
                                                    $scope.all_Qual_Data = res1.data;
                                                    $scope.totalItems = $scope.all_Qual_Data.length;
                                                    $scope.todos = $scope.all_Qual_Data;
                                                    $scope.makeTodos();
                                                });
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    {
                                                        $scope.row1 = '';
                                                    }
                                                }
                                                $scope.currentPage = true;
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });
                            }
                            else {
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById('test-' + $scope.filteredTodos[i].pays_hobbies_code);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    $scope.currentPage = str;
                }
            }

        }])

})();
