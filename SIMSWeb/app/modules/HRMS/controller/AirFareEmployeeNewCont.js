﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AirFareEmployeeNewCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.grid = true;
            $scope.edit_data = false;
            $scope.AirFareEmp_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.edit_code = false;
            $scope.filter_combo = true;

            $scope.empnum_Readonly = false;
            $scope.nation_Readonly = false;
            $scope.grade_Readonly = false;
            $scope.class_Readonly = false;

            $scope.enroll_number_lists = [];
            $scope.username = $rootScope.globals.currentUser.username;
            $rootScope.visible_stud = false;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = true;
            $rootScope.chkMulti = false;
            $scope.emp_name = '';
            $scope.edt = {};

            $scope.edit_data = false;
            $scope.btn_empsearch = false;
          
            $scope.countData = [
             { val: 5, data: 5 },
             { val: 10, data: 10 },
             { val: 15, data: 15 },

            ]

            $scope.size = function (str) {
                //if (str == 5 || str == 10 || str == 15) {
                //    $scope.pager = true;
                //}
                //else {
                //    $scope.pager = false;
                //}
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $timeout(function () {
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);


            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.New = function ()
            {
                $scope.display = true;
                $scope.grid = false;
                $scope.filter_combo = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.btn_empsearch = true;
                $scope.edit_data = false;
            }

            $scope.$on('global_cancel', function (str) {

                $scope.Empmodal_cancel();
            });

            $scope.getEmployee1 = function () {
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }

            $scope.Empmodal_cancel = function () 
            {
                for (var i = 0; i < $scope.SelectedUserLst.length; i++)
                {
                    $scope.enroll_number_lists.push($scope.SelectedUserLst[i]);
                    $scope.test_emp = $scope.SelectedUserLst[i];
                    $scope.emp_name = $scope.test_emp.em_login_code;
                    $scope.empName = $scope.test_emp.empName
                }
               // console.log($scope.SelectedUserLst);
                $scope.edt['afe_em_number'] = $scope.emp_name;
            }

           $http.get(ENV.apiUrl + "api/AirFareEmp/getAllAirFareEmployee").then(function (res) {
               $scope.grid = true;
                $scope.AirFareEmp_data = res.data;
                if ($scope.AirFareEmp_data.length > 0) {
                    $scope.pager = true;

                    if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.AirFareEmp_data.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.AirFareEmp_data.length, data: 'All' })
                    }

                    $scope.totalItems = $scope.AirFareEmp_data.length;
                    $scope.todos = $scope.AirFareEmp_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                    $scope.page_index = true;
                    $scope.currentPage = 1;
                }
                else {
                    swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                    $scope.page_index = false;
                    $scope.pager = false;
                    $scope.filteredTodos = [];
                }
            });
          
           $http.get(ENV.apiUrl + "api/AirFareEmp/getairfaredetails").then(function (res) {
               $scope.airfare_data = res.data;
               console.log($scope.airfare_data);
           });

           var v = [];
           $scope.getairfareother = function (str)
           {
               //console.log(str);
               v = str.split('-');
               //console.log(v);
               debugger
               $scope.gr_code = v[0];
               $scope.edt['afe_nation_code'] = v[1];
               $scope.edt['afe_dest_code'] = v[2];
               $scope.edt['afe_class'] = v[3];

               for (var i = 0; i < $scope.airfare_data.length; i++)
               {
                   if (str == $scope.airfare_data[i].gr_code)
                   {
                       $scope.edt['afe_applicable_frequency'] = $scope.airfare_data[i].af_applicable_frequency;
                       $scope.edt['afe_adult_rate'] = $scope.airfare_data[i].af_adult_rate;
                       $scope.edt['afe_infant_rate'] = $scope.airfare_data[i].af_infant_rate;
                       $scope.edt['afe_child_rate'] = $scope.airfare_data[i].af_child_rate;
                       $scope.edt['afe_applicable_month'] = $scope.airfare_data[i].af_default_applicable_month;
                   }
               }
           }

            $http.get(ENV.apiUrl + "api/AirFareEmp/getDestinationAirFare").then(function (res) {
                $scope.Destination_data = res.data;
                console.log($scope.Destination_data);
            });

            $http.get(ENV.apiUrl + "api/AirFareEmp/getNations").then(function (res) {
                $scope.Nation_data = res.data;
                console.log($scope.Nation_data);
            });

            $http.get(ENV.apiUrl + "api/AirFareEmp/getGrades").then(function (res) {
                $scope.Grade_data = res.data;
               // console.log($scope.Nation_data);
            });

            $http.get(ENV.apiUrl + "api/AirFareEmp/getClass").then(function (res) {
                $scope.Class_data = res.data;
                console.log($scope.Class_data);
            });

            $scope.show_Airfare_Emp_data = function (gr_code,na_code,ds_code,pays_class,em_number) {

                $http.get(ENV.apiUrl + "api/AirFareEmp/getAirFareEmployee?afe_grade_code=" + gr_code + "&afe_dest_code=" + ds_code +
                    "&afe_nation_code=" + na_code + "&afe_class=" + pays_class + "&afe_em_number="+em_number).then(function (res) {
                    
                    $scope.AirFareEmp_data = res.data;

                    if ($scope.AirFareEmp_data.length == 0) {
                        swal({ text: "Record not found", showCloseButton: true, width: 380, })
                        $scope.grid = false;
                    }
                    else {
                        $scope.display = false;
                        $scope.grid = true;
                        $scope.totalItems = $scope.AirFareEmp_data.length;
                        $scope.todos = $scope.AirFareEmp_data;
                        $scope.makeTodos();
                        $scope.grid = true;
                    }
                });
            }

            $scope.clear = function () {
                $scope.edt = {};
                $scope.getgrid();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
            };

            $scope.edit = function (str)
            {
                $scope.empnum_Readonly = true;
                $scope.nation_Readonly = true;
                $scope.grade_Readonly = true;
                $scope.class_Readonly = true;

                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edit_data = true;
                $scope.edit_code = true;
                $scope.filter_combo = false;
               debugger
             
                $scope.edt =
                    {
                        afe_em_number:str.afe_em_number,
                        afe_dest_code: str.afe_dest_code,
                        ds_name:str.ds_name,
                        afe_nation_code: str.afe_nation_code,
                        sims_country_name_en:str.sims_country_name_en,
                        gr_code: str.gr_code,
                        gr_desc:str.gr_desc,
                        afe_class: str.afe_class,
                        afe_class_name:str.afe_class_name,
                        afe_adult_rate: str.afe_adult_rate,
                        afe_child_rate: str.afe_child_rate,
                        afe_infant_rate: str.afe_infant_rate,
                        afe_finalize_status: str.afe_finalize_status,
                        afe_reimbursement: str.afe_reimbursement,
                        afe_airport: str.afe_airport,
                        afe_travelling_date: str.afe_travelling_date,
                        afe_returing_date: str.afe_returing_date,
                        afe_created_by:str.afe_created_by,
                        afe_creation_date: str.afe_creation_date,
                        afe_applicable_frequency: str.afe_applicable_frequency,
                        afe_grade_code: str.afe_grade_code,
                        afe_id: str.afe_id

                    }
                $scope.btn_empsearch = false;
            }

            $scope.Save = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        data1 = [];
                        var data = ({
                            afe_dest_code: $scope.edt.afe_dest_code,
                            afe_grade_code: $scope.gr_code,
                            afe_nation_code: $scope.edt.afe_nation_code,
                            afe_class: $scope.edt.afe_class,
                            //afe_applicable_frequency: $scope.edt.af_applicable_frequency,
                            afe_adult_rate: $scope.edt.afe_adult_rate,
                            afe_child_rate: $scope.edt.afe_child_rate,
                            afe_infant_rate: $scope.edt.afe_infant_rate,
                            afe_applicable_month: $scope.edt.afe_applicable_month,
                           // af_payroll_flag: $scope.edt.af_payroll_flag,
                           // af_gl_acct_no: $scope.edt.af_gl_acct_no,
                           // af_pay_code: $scope.edt.af_pay_code,
                            afe_airport: $scope.edt.afe_airport,
                            afe_travelling_date: $scope.edt.afe_travelling_date,
                            afe_returing_date: $scope.edt.afe_returing_date,
                            afe_finalize_status: $scope.edt.afe_finalize_status,
                            afe_reimbursement: $scope.edt.afe_reimbursement,
                            afe_created_by: $scope.username,
                            afe_em_number: $scope.edt.afe_em_number,
                            afe_old_frequency: $scope.edt.afe_old_frequency,
                            afe_applicable_frequency: $scope.edt.afe_applicable_frequency,
                            opr: 'I'
                        });

                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/AirFareEmp/CUAirFareEmpdetails", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            console.log($scope.msg1);
                            if ($scope.msg1 == true) {
                                swal({ text: "Airfare Added Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.clear();
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Airfare Data Already Exist.", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.clear();
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/AirFareEmp/getAllAirFareEmployee").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.AirFareEmp_data = res.data;
                    $scope.totalItems = $scope.AirFareEmp_data.length;
                    $scope.todos = $scope.AirFareEmp_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                    $scope.filter_combo = true;
                });

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {
                debugger;
                var data1 = [];
                if (isvalidate) {

                    var data = ({
                        //afe_em_number:$scope.edt.afe_em_number,
                        //afe_dest_code: $scope.edt.afe_dest_code,
                        //ds_name: $scope.edt.ds_name,
                        //afe_nation_code: $scope.edt.afe_nation_code,
                        //sims_country_name_en: $scope.edt.sims_country_name_en,
                        //afe_grade_code: $scope.edt.afe_grade_code,
                        //gr_desc: $scope.edt.gr_desc,
                        //afe_class: $scope.edt.afe_class,
                        //afe_class_name: $scope.edt.afe_class_name,
                        //afe_adult_rate: $scope.edt.afe_adult_rate,
                        //afe_child_rate: $scope.edt.afe_child_rate,
                        //afe_infant_rate: $scope.edt.afe_infant_rate,
                        //afe_finalize_status: $scope.edt.afe_finalize_status,

                        afe_dest_code: $scope.edt.afe_dest_code,
                        afe_grade_code: $scope.edt.afe_grade_code,
                        afe_nation_code: $scope.edt.afe_nation_code,
                        afe_class: $scope.edt.afe_class,
                        //afe_applicable_frequency: $scope.edt.af_applicable_frequency,
                        afe_adult_rate: $scope.edt.afe_adult_rate,
                        afe_child_rate: $scope.edt.afe_child_rate,
                        afe_infant_rate: $scope.edt.afe_infant_rate,
                        afe_applicable_month: $scope.edt.afe_applicable_month,
                        // af_payroll_flag: $scope.edt.af_payroll_flag,
                        // af_gl_acct_no: $scope.edt.af_gl_acct_no,
                        // af_pay_code: $scope.edt.af_pay_code,
                        afe_airport: $scope.edt.afe_airport,
                        afe_travelling_date: $scope.edt.afe_travelling_date,
                        afe_returing_date: $scope.edt.afe_returing_date,
                        afe_finalize_status: $scope.edt.afe_finalize_status,
                        afe_reimbursement: $scope.edt.afe_reimbursement,
                        afe_created_by: $scope.username,
                        afe_em_number: $scope.edt.afe_em_number,
                        afe_old_frequency: $scope.edt.afe_old_frequency,
                        afe_applicable_frequency: $scope.edt.afe_applicable_frequency,
                        afe_id: $scope.edt.afe_id,
                        opr: 'U'
                    });

                    data1.push(data);

                    console.log($scope.edt.afe_grade_code);

                    $http.post(ENV.apiUrl + "api/AirFareEmp/CUAirFareEmp", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Employee Airfare Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.clear();
                                    $scope.getgrid();
                                    $scope.filter_combo = true;
                                    $scope.edt = {};
                                }
                            });

                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Employee Airfare Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.clear();
                                    $scope.getgrid();
                                    $scope.filter_combo = true;
                                    $scope.edt = {};
                                }
                            });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.filter_combo = true;
                $scope.edt = {};
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.AirFareEmp_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.AirFareEmp_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.afe_em_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.gr_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.ds_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    || item.sims_country_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.size = function (str) {
               
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['check_all'] = false;
                $scope.row1 = '';
                main.checked = false;
            }

        }])
})();