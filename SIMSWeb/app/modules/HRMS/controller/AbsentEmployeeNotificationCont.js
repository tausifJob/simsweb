﻿/// <reference path="../views/StudentDocumentList.html" />
(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, modulecode = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AbsentEmployeeNotificationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.table = false;
            $scope.table1 = false;
            $scope.pagesize = "10";
            $scope.pageindex = 0;
            console.clear();
            $timeout(function () {
                $("#example").tableHeadFixer({ 'top': 1 });
            }, 100);

            var dateyear = $filter('date')(new Date(), 'dd-MM-yyyy');
            $scope.dt = {
                sims_from_date: dateyear,
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd-mm-yyyy'
            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.size = function (str) {
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }

                if (str == "All") {
                    $scope.currentPage = 1;
                    $scope.numPerPage = $scope.studlist.length;
                    $scope.makeTodos();

                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                //$scope.pageindex = str;
                //$scope.currentPage = str;
                //console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }
            }

            $scope.makeTodos = function () {

                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };
            
            $http.get(ENV.apiUrl + "api/EmployeeReport/GetCompanyNameForShift").then(function (res) {
                $scope.cmbCompany = res.data;
                $scope.edt = { sh_company_code: $scope.cmbCompany[0].sh_company_code }
                $scope.companyChange($scope.edt.sh_company_code);
                $scope.getARule($scope.edt.sh_company_code);
            })

            $scope.getcur = function () {
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                    $scope.curriculum = AllCurr.data;
                    $scope.edt = {
                        sims_cur_code: $scope.curriculum[0].sims_cur_code,
                    };
                    $scope.getAccYear($scope.edt.sims_cur_code);
                    $scope.getARule($scope.edt.sims_cur_code);
                });
            }

            $scope.getcur();

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                    $scope.edt = {
                        sims_cur_code: $scope.curriculum[0].sims_cur_code,
                        sh_company_code: $scope.cmbCompany[0].sh_company_code
                    };
                });
            }

            $scope.getARule = function (comcode) {
                $http.get(ENV.apiUrl + "api/StudentReport/getEmployeeAttendanceRule?comCode=" + comcode).then(function (attrule) {
                    $scope.arule = attrule.data;
                    $scope.edt = {
                        sh_company_code: $scope.edt.sh_company_code,
                        sims_attendance_code: $scope.arule[0].sims_attendance_code,
                        sims_cur_code: $scope.edt.sims_cur_code
                    };
                });
            }

            $http.get(ENV.apiUrl + "api/EmployeeReport/GetDepartmentName").then(function (res) {
                $scope.cmbDepartment = res.data;
                setTimeout(function () {
                    $('#cmb_dept_code').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            })

            $(function () {
                $('#cmb_dept_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.companyChange = function (str) {
                $http.get(ENV.apiUrl + "api/EmployeeReport/GetDesignation?com_code=" + str).then(function (res) {
                    $scope.cmbDesignation = res.data;
                    setTimeout(function () {
                        $('#cmb_desig_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                })
                $scope.getARule($scope.edt.sh_company_code);
            }

            $(function () {
                $('#cmb_desig_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.Show_Data = function (str, str1) {
                debugger;
                if ((str != undefined && str1 != undefined) || $scope.edt.sh_company_code != "" && $scope.temp.sims_academic_year != "") {

                    //if ($scope.edt1.sims_send_email == true && $scope.edt1.sims_send_email != undefined) {
                        $http.get(ENV.apiUrl + "api/StudentReport/getAbsentEmployeeList?cur_name=" + $scope.edt.sims_cur_code + "&com_code=" + $scope.edt.sh_company_code + "&academic_year=" + $scope.temp.sims_academic_year + "&dep_code=" + $scope.edt.dept_no + "&desg_code=" + $scope.edt.esi_code + "&employee_en=" + $scope.edt.em_login_code + "&select_date=" + $scope.dt.sims_from_date + "&att_code=" + $scope.edt.sims_attendance_code + "&opr_code=" +'S').then(function (res) {
                            $scope.studlist = res.data;
                            if ($scope.studlist.length > 0) {
                                $scope.totalItems = $scope.studlist.length;
                                $scope.todos = $scope.studlist;
                                $scope.makeTodos();
                                $scope.table = true;
                                $scope.table1 = true;
                            }
                            else {
                                swal({ text: "Record Not Found...", imageUrl: "assets/img/close.png", showCloseButton: true, width: 450, height: 200 });
                                $scope.table = false;
                                $scope.table1 = false;
                            }
                        })
                    //}

                    //if ($scope.edt1.sims_send_alert == true && $scope.edt1.sims_send_alert != undefined) {
                    //    $http.get(ENV.apiUrl + "api/StudentReport/getAbsentEmployeeList?cur_name=" + $scope.edt.sims_cur_code + "&com_code=" + $scope.edt.sh_company_code + "&academic_year=" + $scope.temp.sims_academic_year + "&dep_code=" + $scope.edt.dept_no + "&desg_code=" + $scope.edt.esi_code + "&employee_en=" + $scope.edt.em_login_code + "&select_date=" + $scope.dt.sims_from_date + "&att_code=" + $scope.edt.sims_attendance_code + "&opr_code=" + 'A').then(function (res) {
                    //        $scope.studlist = res.data;
                    //    })
                    //}
                    //if ($scope.edt1.sims_send_sms == true && $scope.edt1.sims_send_sms != undefined) {
                    //    $http.get(ENV.apiUrl + "api/StudentReport/getAbsentEmployeeList?cur_name=" + $scope.edt.sims_cur_code + "&com_code=" + $scope.edt.sh_company_code + "&academic_year=" + $scope.temp.sims_academic_year + "&dep_code=" + $scope.edt.dept_no + "&desg_code=" + $scope.edt.esi_code + "&employee_en=" + $scope.edt.em_login_code + "&select_date=" + $scope.dt.sims_from_date + "&att_code=" + $scope.edt.sims_attendance_code + "&opr_code=" + 'B').then(function (res) {
                    //        $scope.studlist = res.data;
                    //    })
                    //}
                    
                }

                else {
                    swal({ text: "Please Select Company & Academic Year Then Try Again...", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 450, height: 200 });
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {

                $scope.todos = $scope.searched($scope.studlist, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.studlist;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                return (item.student_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_attendance_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_grade_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_section_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sagar == toSearch) ? true : false;
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyoneselect = function () {

                $("input[type='checkbox']").change(function (e) {

                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.Submit = function () {

                if ($scope.edt1.sims_send_email != undefined || $scope.edt1.sims_send_alert != undefined || $scope.edt1.sims_send_sms != undefined) {
                    debugger;
                    if ($scope.edt1.sims_send_email == true && $scope.edt1.sims_send_email != undefined) {
                        var Savedata = [];
                        var empName = "";
                        for (var i = 0; i < $scope.filteredTodos.length; i++) {
                            var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                            if (v.checked == true) {
                                $scope.flag = true;
                                var alertsave = ({
                                    'sims_cur_code': $scope.edt.sh_company_code,
                                    'sims_academic_year': $scope.temp.sims_academic_year,
                                    'sims_grade_name': $scope.filteredTodos[i].sims_grade_name,
                                    'sims_grade_code': $scope.filteredTodos[i].sims_grade_name,
                                    'sims_enroll_number': $scope.filteredTodos[i].sims_enroll_number,
                                    'student_full_name': $scope.filteredTodos[i].student_full_name,
                                    'sims_contact_number': $scope.filteredTodos[i].sims_contact_number,
                                    'sims_from_date': $scope.dt.sims_from_date,
                                    'sims_email_id': $scope.filteredTodos[i].sims_email_id,
                                    'doc_path_available_flag': $scope.filteredTodos[i].doc_path_available_flag,
                                    opr: 'R'
                                });                                
                                Savedata.push(alertsave);

                                empName = empName + $scope.filteredTodos[i].student_full_name + '<br>';
                                console.log("EmpName :", empName);

                                var attendance_type = $("#attendance_type option:selected").text();
                                console.log("attendance_name", attendance_type);
                            }
                        }
                        var obj = {
                            emp_name: empName,
                            attendance_type: attendance_type,
                            sims_from_date: $scope.dt.sims_from_date
                        }
                        console.log("obj",obj);

                        $http.post(ENV.apiUrl + "api/StudentReport/CUDEmployeeAbsentStudentList?data1="+JSON.stringify(obj),Savedata).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1.strMessage != undefined) {
                                if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                    swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                                    $scope.Reset();
                                    $scope.currentPage = true;
                                }
                            }

                        });
                    }


                    if ($scope.edt1.sims_send_alert == true && $scope.edt1.sims_send_alert != undefined) {
                        var Savedata = [];
                        for (var i = 0; i < $scope.filteredTodos.length; i++) {
                            var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                            if (v.checked == true) {
                                $scope.flag = true;
                                var alertsave = ({
                                    'sims_cur_code': $scope.edt.sh_company_code,
                                    'sims_academic_year': $scope.temp.sims_academic_year,
                                    'sims_grade_name': $scope.filteredTodos[i].sims_grade_name,
                                    'sims_grade_code': $scope.filteredTodos[i].sims_grade_name,
                                    'sims_enroll_number': $scope.filteredTodos[i].sims_enroll_number,
                                    'student_full_name': $scope.filteredTodos[i].student_full_name,
                                    'sims_contact_number': $scope.filteredTodos[i].sims_contact_number,
                                    'sims_from_date': $scope.dt.sims_from_date,
                                    'sims_email_id': $scope.filteredTodos[i].sims_email_id,
                                    'doc_path_available_flag': $scope.filteredTodos[i].doc_path_available_flag,
                                    opr: 'I'
                                });
                                Savedata.push(alertsave);
                            }
                        }

                        $http.post(ENV.apiUrl + "api/StudentReport/CUDAbsentStudentList", Savedata).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1.strMessage != undefined) {
                                if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                    swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                                    $scope.Reset();
                                    $scope.currentPage = true;
                                }
                            }

                        });
                    }

                    if ($scope.edt1.sims_send_sms == true && $scope.edt1.sims_send_sms != undefined) {
                        var Savedata = [];
                        for (var i = 0; i < $scope.filteredTodos.length; i++) {
                            var v = document.getElementById($scope.filteredTodos[i].sims_enroll_number + i);
                            if (v.checked == true) {
                                $scope.flag = true;
                                var alertsave = ({
                                    'sims_cur_code': $scope.edt.sh_company_code,
                                    'sims_academic_year': $scope.temp.sims_academic_year,
                                    'sims_grade_name': $scope.filteredTodos[i].sims_grade_code,
                                    'sims_grade_code': $scope.filteredTodos[i].sims_grade_name,
                                    'sims_enroll_number': $scope.filteredTodos[i].sims_enroll_number,
                                    'student_full_name': $scope.filteredTodos[i].student_full_name,
                                    'sims_contact_number': $scope.filteredTodos[i].sims_contact_number,
                                    'sims_from_date': $scope.dt.sims_from_date,
                                    'sims_email_id': $scope.filteredTodos[i].sims_email_id,
                                    'doc_path_available_flag': $scope.filteredTodos[i].doc_path_available_flag,
                                    opr: 'M'
                                });
                                Savedata.push(alertsave);
                            }
                        }

                        $http.post(ENV.apiUrl + "api/StudentReport/CUDAbsentStudentList", Savedata).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1.strMessage != undefined) {
                                if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                    swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                                    $scope.Reset();
                                    $scope.currentPage = true;
                                }
                            }

                        });
                    }
                }

                else {
                    swal({ text: "Please Select Any One CheckBox To Perform Opration...", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 450, height: 200 });
                }
            }

            $scope.Reset = function () {

                $("#cmb_grade_code").select2("val", "");
                $("#cmb_section_code").select2("val", "");

                $scope.edt = {
                    sims_cur_code: '',
                    sims_student_enroll_number: '',
                }
                $scope.temp = {
                    sims_academic_year: '',
                }
                $scope.dt = {
                    sims_from_date: dateyear,
                }
                $scope.edt1 = {
                    sims_send_email: false,
                    sims_send_alert: false,
                    sims_send_sms: false
                }

                $scope.table = false;
                $scope.table1 = false;
                $scope.getcur();

            }

            $scope.Clear = function () {
                $scope.edt1 = {
                    sims_send_email: false,
                    sims_send_alert: false,
                    sims_send_sms: false,
                }
            }

            $scope.propertyName = null;
            $scope.reverse = false;

            $rootScope.visible_stud = false;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = true;
            $rootScope.chkMulti = false;

            $scope.Employee_Search = function () {
                $scope.global_Search_click();

                $('#Global_Search_Modal').modal({ backdrop: "static" });
                //  $scope.display = true;
                //$scope.SelectedUserLst = [];
            }

            $scope.$on('global_cancel', function () {
                debugger;
                if ($scope.SelectedUserLst.length > 0) {
                    $scope.edt =
                        {
                            em_name: $scope.SelectedUserLst[0].empName,
                            em_login_code: $scope.SelectedUserLst[0].em_login_code,
                            sims_cur_code: $scope.curriculum[0].sims_cur_code,
                            sh_company_code: $scope.cmbCompany[0].sh_company_code
                        }
                }
                $scope.em_number = $scope.edt.em_number;
            });

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

        }])
})();
