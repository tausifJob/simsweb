﻿(function () {
    'use strict';
    var CurrentDate;
    var day;
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmpDashboardCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {


            var total = 0; var Ltotal = 0; var tot, Ltot; var remain = 0; var tremain; var maxdays;
            var username = $rootScope.globals.currentUser.username;
            $scope.LeaveTaken = 0;
            //Emp Attendance
            $scope.CurrentDate = new Date();
            var today = new Date();
            // $scope.day = CurrentDate.getDay();
            //var d = new Date();
            //var n = d.getDay();
            var grade;
            var section;
            $scope.records = true;
            $scope.tabledata = false;

            var AJB = ['aji', 'ajb', 'ajn', 'egn', 'zps'];

            if (AJB.includes($http.defaults.headers.common['schoolId'])) {
                $scope.showDDFlagForAjbDash = true;
            }

            if ($http.defaults.headers.common['schoolId']=='qfis') {

                $scope.hide_feedback_tile = true;
            }

              
          

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable3").tableHeadFixer({ 'top': 1 });
            }, 100);
            $timeout(function () {
                $("#fixTable2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.getStaffDetails = function () {
                $('#MyModal3').modal('show');
                
                $http.get(ENV.apiUrl + "api/common/Empdashboard/getStaffPersonalData").then(function (getStaff) {
                    $scope.staff = getStaff.data;
                });
            }

            $scope.getResignedStaffDetails = function () {
                $('#MyModal4').modal('show');
             
                $http.get(ENV.apiUrl + "api/common/Empdashboard/getResignedstaffData").then(function (getRegStaff) {
                    $scope.Resignedstaff = getRegStaff.data;
                });
            }


          

            //  This is for inventory Jquery Call

           
            //$http.get(ENV.apiUrl + "api/common/Empdashboard/getInventoryDataforOrders").then(function (allcount) {
            //    $scope.alldata = allcount.data;
            //    var i = $scope.alldata[0].item_req;
            //    var s = $scope.alldata[0].service_req
            //    $("#sparkline-pie").sparkline([i,s], {
            //        type: 'pie',
            //        width: '100%',
            //        height: '100%',
            //        sliceColors: ['#91cdec', '#ff6666'],
            //        offset: 10,
            //        borderWidth: 0,
            //        borderColor: '#000000 '
            //    });


            //});
            //debugger
           
            $http.get(ENV.apiUrl + "api/common/Empdashboard/getInventoryDataforOrders").then(function (gethrresult) {
                $scope.hrms_data1 = gethrresult.data;


                $scope.chartLabels5 = [];
                $scope.chartData5 = [];
                $scope.chartColor = [];

                var pieChartData = [];

                for (var i = 0; i < $scope.hrms_data1.length; i++) {
                    var color = Math.floor(Math.random() * 16777216).toString(16);
                    color: '#000000'.slice(0, -color.length) + color;
                    $scope.hrms_data1[i].color = color;

                    $scope.chartLabels5.push($scope.hrms_data1[i].service_req);
                    $scope.chartData5.push(parseInt($scope.hrms_data1[i].item_req));
                    $scope.chartColor.push($scope.hrms_data1[i].color);
                }


            });


            var d1_1 = []
            var d1_2 = [];
            var order_no = [];
            $http.get(ENV.apiUrl + "api/common/Empdashboard/getInventoryPendingReceived").then(function (gethrresult1) {
                
                $scope.order_data1 = gethrresult1.data;
               
                for(var i=0;i<$scope.order_data1.length;i++)
                {
                   
                          if (i == 0)
                          {
                             d1_1.push([1325376000000, $scope.order_data1[i].orderedQty])
                             d1_2.push([1325376000000, $scope.order_data1[i].receivedQty])
                             order_no.push($scope.order_data1[i].ord_no)
                          }

                          if (i == 1) {
                              d1_1.push([1328054400000, $scope.order_data1[i].orderedQty])
                              d1_2.push([1328054400000, $scope.order_data1[i].receivedQty])
                              order_no.push($scope.order_data1[i].ord_no)

                          }

                          if (i == 2) {
                              d1_1.push([1330560000000, $scope.order_data1[i].orderedQty])
                              d1_2.push([1330560000000, $scope.order_data1[i].receivedQty])
                              order_no.push($scope.order_data1[i].ord_no)

                          }

                          if (i == 3) {
                              d1_1.push([1333238400000, $scope.order_data1[i].orderedQty])
                              d1_2.push([1333238400000, $scope.order_data1[i].receivedQty])
                              order_no.push($scope.order_data1[i].ord_no)

                          }


                          if (i == 4) {
                              d1_1.push([1335830400000, $scope.order_data1[i].orderedQty])
                              d1_2.push([1335830400000, $scope.order_data1[i].receivedQty])
                              order_no.push($scope.order_data1[i].ord_no)

                          }


                          if (i == 5) {
                              d1_1.push([1338514000000, $scope.order_data1[i].orderedQty])
                              d1_2.push([1338514000000, $scope.order_data1[i].receivedQty])
                              order_no.push($scope.order_data1[i].ord_no)

                          }
                }

                var data2 = [
       {
           label: "Order",
           data: d1_1,
           bars: {
               show: true,
               barWidth: 12 * 24 * 60 * 60 * 300 * 2,
               fill: true,
               lineWidth: 0,
               order: 0,
               fillColor: "rgba(243, 89, 88, 0.7)"
           },
           color: "rgba(243, 89, 88, 0.7)"
       },
       {
           label: "Received Order",
           data: d1_2,
           bars: {
               show: true,
               barWidth: 12 * 24 * 60 * 60 * 300 * 2,
               fill: true,
               lineWidth: 0,
               order: 0,
               fillColor: "rgba(251, 176, 94, 0.7)"
           },
           color: "rgba(251, 176, 94, 0.7)"
       },
                ];


                $.plot($('#stacked-ordered-chart'), data2, {
                    grid: {
                        hoverable: true,
                        clickable: false,
                        borderWidth: 1,
                        borderColor: '#f0f0f0',
                        labelMargin: 8
                    },
                    xaxis: {
                        min: (new Date(2011, 11, 15)).getTime(),
                        max: (new Date(2012, '05', 18)).getTime(),
                        mode: "time",
                        timeformat: "%b",
                        tickSize: [1, "month"],
                        monthNames: ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10"],
                        tickLength: 0, // hide gridlines
                        axisLabel: 'Month',
                        axisLabelUseCanvas: true,
                        axisLabelFontSizePixels: 12,
                        axisLabelFontFamily: 'Verdana, Arial, Helvetica, Tahoma, sans-serif',
                        axisLabelPadding: 5
                    },
                    stack: true
                });

            });

            //Stacked Charts JS
            //var d1_1 = [
            //    [1325376000000, 20],
            //    [1328054400000, 29],
            //    [1330560000000, 100],
            //    [1333238400000, 60],
            //    [1335830400000, 35],
            //    [1338514000000, 50]
            //];

            //var d1_2 = [
            //    [1325376000000, 20],
            //    [1328054400000, 0],
            //    [1330560000000, 30],
            //    [1333238400000, 35],
            //    [1335830400000, 30],
            //    [1338514000000, 20]
            //];


           

            //Float Chart JS
            var d2 = [];
            var d1 = [];
            $http.get(ENV.apiUrl + "api/common/Empdashboard/getInventoryOrders").then(function (gethrresult2) {
                $scope.getOrders = gethrresult2.data;
               
                for (var i = 0; i < gethrresult2.data.length; i++) {
                  //  var d = [];
                    
                    d2.push([i + 1 , gethrresult2.data[i].ordered_Count])
                    d1.push([i + 1, gethrresult2.data[i].received_Count])

                }

        //        var d2 = [[1, 30],
        //[2, 20],
        //[3, 10],
        //[4, 30],
        //[5, 15],
        //[6, 25],
        //[7, 40]

        //        ];
        //        var d1 = [
        //                [1, 30],
        //                [2, 30],
        //                [3, 20],
        //                [4, 40],
        //                [5, 30],
        //                [6, 45],
        //                [7, 50],
        //        ];
                var plot = $.plotAnimator($("#placeholder"), [
                        {
                            label: "Label 1",
                            data: d2,
                            lines: {
                                fill: 0.6,
                                lineWidth: 0,
                            },
                            color: ['#f89f9f']
                        }, {
                            data: d1,
                            animator: { steps: 60, duration: 1000, start: 0 },
                            lines: { lineWidth: 2 },
                            shadowSize: 0,
                            color: '#f35958'
                        }, {
                            data: d1,
                            points: { show: true, fill: true, radius: 6, fillColor: "#f35958", lineWidth: 3 },
                            color: '#fff',
                            shadowSize: 0
                        },
                        {
                            label: "Label 2",
                            data: d2,
                            points: { show: true, fill: true, radius: 6, fillColor: "#f5a6a6", lineWidth: 3 },
                            color: '#fff',
                            shadowSize: 0
                        }
                ], {
                    xaxis: {
                        tickLength: 0,
                        tickDecimals: 0,
                        min: 2,

                        font: {
                            lineHeight: 13,
                            style: "normal",
                            weight: "bold",
                            family: "sans-serif",
                            variant: "small-caps",
                            color: "#6F7B8A"
                        }
                    },
                    yaxis: {
                        ticks: 3,
                        tickDecimals: 0,
                        tickColor: "#f0f0f0",
                        font: {
                            lineHeight: 13,
                            style: "normal",
                            weight: "bold",
                            family: "sans-serif",
                            variant: "small-caps",
                            color: "#6F7B8A"
                        }
                    },
                    grid: {
                        backgroundColor: { colors: ["#fff", "#fff"] },
                        borderWidth: 1, borderColor: "#f0f0f0",
                        margin: 0,
                        minBorderMargin: 0,
                        labelMargin: 20,
                        hoverable: true,
                        clickable: true,
                        mouseActiveRadius: 6
                    },
                    legend: { show: false }
                });


            });
                


            $("#placeholder").bind("plothover", function (event, pos, item) {
                if (item) {
                    var x = item.datapoint[0].toFixed(2),
						y = item.datapoint[1].toFixed(2);

                    $("#tooltip").html(item.series.label + " of " + x + " = " + y)
						.css({ top: item.pageY + 5, left: item.pageX + 5 })
						.fadeIn(200);
                } else {
                    $("#tooltip").hide();
                }

            });

           //
          


            //WorldMAp API CONTROLLER
            $http.get(ENV.apiUrl + "api/common/Empdashboard/GetCountryCountData").then(function (res) {
                $scope.country_count = res.data;

                //$timeout(function () {

                //    var lic_lat = 21.524627;
                //    var lic_long = 77.871094;

                //    var location = new google.maps.LatLng(lic_lat, lic_long);
                //    var mapOptions = {
                //        zoom: 2,
                //        center: location,
                //        mapTypeId: google.maps.MapTypeId.TERRAIN
                //    }

                //    var map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

                //    var infowindow = new google.maps.InfoWindow();
                //    var marker, i;

                //    for (i = 0; i < $scope.country_count.length; i++) {

                //        var marker = new google.maps.Marker({
                //            map: map,
                //            position: new google.maps.LatLng($scope.country_count[i].sims_country_latitude, $scope.country_count[i].sims_country_longitude),
                //            title: $scope.country_count[i].sims_country_name_en + " Boys-" + $scope.country_count[i].male_student_count + " Girls-" + $scope.country_count[i].female_student_count,

                //        });

                //        

                //        google.maps.event.addListener(marker, 'click', (function (marker, i) {

                //            return function () {
                //                infowindow.setContent($scope.country_count[i].sims_country_name_en + ' Boys-' + $scope.country_count[i].male_student_count + ' Girls-' + $scope.country_count[i].female_student_count);
                //                infowindow.open(map, marker);
                //            }
                //            $scope.piechartsDataShow();
                //        })(marker, i));



                //    }

                //}, 500)



            });

            $http.get(ENV.apiUrl + "api/common/Empdashboard/getHRMSData").then(function (gethrresult) {
                $scope.hrms_data = gethrresult.data;


                $scope.chartLabels = [];
                $scope.chartData = [];
                $scope.chartColor = [];

                var pieChartData = [];

                for (var i = 0; i < $scope.hrms_data.length; i++) {
                    var color = Math.floor(Math.random() * 16777216).toString(16);
                    color: '#000000'.slice(0, -color.length) + color;
                    $scope.hrms_data[i].color = color;

                    $scope.chartLabels.push($scope.hrms_data[i].visa_type);
                    $scope.chartData.push(parseInt($scope.hrms_data[i].visa_type_count));
                    $scope.chartColor.push($scope.hrms_data[i].color);
                }


            });

            $http.get(ENV.apiUrl + "api/common/Empdashboard/getStudentData").then(function (getstud_data) {
                
                $scope.getstudent_data = getstud_data.data;

                $scope.labels = [];
                $scope.data = [];
                // $scope.value = [];

                var doughnutData = [];

                for (var i = 0; i < $scope.getstudent_data.length; i++) {
                    $scope.labels.push($scope.getstudent_data[i].gender);
                    $scope.data.push(parseInt($scope.getstudent_data[i].gender_count));
                    // $scope.value.push($scope.hrms_data[i].gender_code)
                }

                $scope.grid = true;

            });

            $http.get(ENV.apiUrl + "api/common/Empdashboard/getStaffDivisionCount").then(function (div_count) {
                $scope.divcount = div_count.data;
                $scope.chartLabels1 = [];
                $scope.chartData1 = [];
                $scope.chartColor1 = [];
                var pieChartData1 = [];
                for (var i = 0; i < $scope.divcount.length; i++) {
                    var color = Math.floor(Math.random() * 16777216).toString(16);
                    color: '#000001'.slice(0, -color.length) + color;
                    $scope.divcount[i].color = color;

                    $scope.chartLabels1.push($scope.divcount[i].gd_division_desc);
                    $scope.chartData1.push(parseInt($scope.divcount[i].staff_count));
                    $scope.chartColor1.push($scope.divcount[i].color);
                }

            });



          




            $http.get(ENV.apiUrl + "api/common/Empdashboard/getStaffDepartmentCount").then(function (div_count) {
                $scope.divcount = div_count.data;
                $scope.chartLabels2 = [];
                $scope.chartData2 = [];
                $scope.chartColor2 = [];
                var pieChartData2 = [];
                for (var i = 0; i < $scope.divcount.length; i++) {
                    var color = Math.floor(Math.random() * 16777216).toString(16);
                    color: '#000001'.slice(0, -color.length) + color;
                    $scope.divcount[i].color = color;

                    $scope.chartLabels2.push($scope.divcount[i].department);
                    $scope.chartData2.push(parseInt($scope.divcount[i].staff_count));
                    $scope.chartColor2.push($scope.divcount[i].color);
                }

            });
            
            $http.get(ENV.apiUrl + "api/common/Empdashboard/GetCountryCountTotal").then(function (result) {
                $scope.total_counts = result.data;
            });
            //


            //New API
            $http.get(ENV.apiUrl + "api/common/Empdashboard/GetDashboardData").then(function (res1) {
               
                $scope.portal_data = res1.data;

                if (res1.data.length > 0) {
                    $scope.portal_data = res1.data;
                }
                else {
                    $scope.portal_data_flg = '1'
                    $scope.portal_data.push({})
                }
            });



            $scope.leaveDesc = 'View Leave Balance';
            var d = new Date();
            var weekday = new Array(7);
            weekday[0] = "Sunday";
            weekday[1] = "Monday";
            weekday[2] = "Tuesday";
            weekday[3] = "Wednesday";
            weekday[4] = "Thursday";
            weekday[5] = "Friday";
            weekday[6] = "Saturday";
            var n = weekday[d.getDay()];
            $scope.day = n;

            var dd = today.getDate();
            var mm = today.getMonth() + 1;
            var yyyy = today.getFullYear();
            var att_date = yyyy + '-' + mm + '-' + dd;
            var cur_year = yyyy;
            var Co_Code = '1';
            $scope.record = false;
            var cur_date1 = dd + '-' + mm + '-' + yyyy

            $scope.cur_date = cur_date1;

            $scope.stud_att_date = att_date;

            $http.get(ENV.apiUrl + "api/common/Empdashboard/GetAllDetailsEmp?curr_date=" + att_date).then(function (res) {
               
                $scope.all_data_emp = res.data;
                if (res.data.length > 0) {
                    $scope.present = $scope.all_data_emp[0].presents;
                    $scope.abs = $scope.all_data_emp[0].absents;
                    $scope.lvwi = $scope.all_data_emp[0].leavewithougthallowns;
                    $scope.lev = $scope.all_data_emp[0].leave;
                    $scope.um = $scope.all_data_emp[0].unmark;
                    $scope.jtot = $scope.all_data_emp[0].total;
                }
                
            });

            $scope.presentflag = function () {
              

                $http.get(ENV.apiUrl + "api/common/Empdashboard/GetAllPerEmpDet?curr_date=" + att_date + "&status=" + 'P').then(function (res) {
                   
                    $scope.all_data_emp_perticular = res.data;
                    $scope.all_data_emp_perticular1 = res.data;
                    if ($scope.all_data_emp_perticular1.length > 0) {
                        $('#MyModal2').modal('show');
                        $scope.tabledata = true;
                    }
                    else {
                        swal({ title: "Alert", text: "Data not Availiable", width: 300, height: 200 });
                    }

                });

            }

            $scope.absentflag = function () {
                $scope.all_data_emp_perticular = [];
        
                $http.get(ENV.apiUrl + "api/common/Empdashboard/GetAllPerEmpDet?curr_date=" + att_date + "&status=" + 'A').then(function (res) {
                 
                    $scope.all_data_emp_perticular = res.data;
                    $scope.all_data_emp_perticular2 = res.data;
                    if ($scope.all_data_emp_perticular2.length > 0) {
                        $('#MyModal2').modal('show');
                        $scope.tabledata = true;
                    }
                    else {
                        swal({ title: "Alert", text: "Data not Availiable", width: 300, height: 200 });
                    }

                });


            }

            $scope.unmarkflag = function () {
                $scope.all_data_emp_perticular = [];
           
                $http.get(ENV.apiUrl + "api/common/Empdashboard/GetAllPerEmpDet?curr_date=" + att_date + "&status=" + 'UM').then(function (res) {
                  
                    $scope.all_data_emp_perticular = res.data;
                    $scope.all_data_emp_perticular3 = res.data;
                    if ($scope.all_data_emp_perticular3.length > 0) {
                        $('#MyModal2').modal('show');
                        $scope.tabledata = true;
                    }
                    else {

                        swal({ title: "Alert", text: "Data not Availiable", width: 300, height: 200 });
                    }

                });

            }

            $scope.leavewithoutallws = function () {
                $scope.all_data_emp_perticular = [];
          
                $http.get(ENV.apiUrl + "api/common/Empdashboard/GetAllPerEmpDet?curr_date=" + att_date + "&status=" + 'W').then(function (res) {
           
                    $scope.all_data_emp_perticular = res.data;
                    $scope.all_data_emp_perticular4 = res.data;
                    if ($scope.all_data_emp_perticular4.length > 0) {
                        $('#MyModal2').modal('show');
                        $scope.tabledata = true;
                    }
                    else {

                        swal({ title: "Alert", text: "Data not Availiable", width: 300, height: 200 });
                    }
                });

            }

            $scope.leaveflag = function () {
                $scope.all_data_emp_perticular = [];
           
                $http.get(ENV.apiUrl + "api/common/Empdashboard/GetAllPerEmpDet?curr_date=" + att_date + "&status=" + 'L').then(function (res) {
                 
                    $scope.all_data_emp_perticular = res.data;
                    $scope.all_data_emp_perticular5 = res.data;

                    if ($scope.all_data_emp_perticular5.length > 0) {
                        $('#MyModal2').modal('show');
                        $scope.tabledata = true;
                    }
                    else {
                        swal({ title: "Alert", text: "Data not Availiable", width: 300, height: 200 });
                    }
                });

            }

            $http.get(ENV.apiUrl + "api/common/Empdashboard/Getdivisionmale?co_code=" + Co_Code).then(function (res) {
                $scope.division = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/Empdashboard/getEmpAttendance?emp_att_date=" + att_date + "&emp_att_emp_id=" + username).then(function (empatten) {
                $scope.emp_atten = empatten.data;
                if (empatten.data.length > 0) {
                    $scope.user_name = $scope.emp_atten[0].em_first_name;
                    $scope.check_in = $scope.emp_atten[0].em_check_in;
                    $scope.check_out = $scope.emp_atten[0].em_check_out;
                }
                //if ($scope.emp_atten[0].emp_atten_flag == 'Y') {
                //    $scope.att_status = 'Present';
                //}
                //else if ($scope.emp_atten[0].emp_atten_flag == 'H') {
                //    $scope.att_status = 'Holiday';
                //}
                //else if ($scope.emp_atten[0].emp_atten_flag == 'L') {
                //    $scope.att_status = 'Leave'
                //}
                //else { $scope.att_status = 'Weekend' }


            });

            var v = '0.00';
            $scope.emp_absent_stud_percentage = v;

            $scope.demo = [];

            $http.get(ENV.apiUrl + "api/common/Empdashboard/getEmpGradePercentage?&emp_att_emp_id=" + username + "&att_date_temp=" + att_date).then(function (empstgrade) {
                $scope.emp_grade = empstgrade.data;

                if ($scope.emp_grade.length > 0) {
                    $timeout(function () {
                        for (var i = 0 ; i < $scope.emp_grade.length; i++) {
                            var name = '#test1' + i;
                            $(name).easyPieChart({
                                lineWidth: 10,
                                height: 50,
                                barColor: 'red',
                                trackColor: '#e5e9ec',
                                scaleColor: false
                            });
                        }
                    }, 100);

                    $timeout(function () {
                        for (var i = 0 ; i < $scope.emp_grade.length; i++) {
                            var name = '#test2' + i;
                            $(name).easyPieChart({
                                lineWidth: 10,
                                height: 50,
                                barColor: 'gray',
                                trackColor: '#e5e9ec',
                                scaleColor: false
                            });
                        }
                    }, 100);



                    $timeout(function () {
                        for (var i = 0 ; i < $scope.emp_grade.length; i++) {
                            var name = '#test3' + i;
                            $(name).easyPieChart({
                                lineWidth: 10,
                                height: 50,
                                barColor: 'yellow',
                                trackColor: '#e5e9ec',
                                scaleColor: false
                            });
                        }
                    }, 100);

                    $timeout(function () {
                        for (var i = 0 ; i < $scope.emp_grade.length; i++) {
                            var name = '#test' + i;
                            $(name).easyPieChart({
                                lineWidth: 10,
                                height: 50,
                                barColor: 'green',
                                trackColor: '#e5e9ec',
                                scaleColor: false
                            });
                        }
                    }, 100);
                }
                else {
                    $scope.strMessage = 'Records Not Available';
                }
            });

            $scope.PreAbTardy = function (grade, section, str, stud_att_date) {

                $http.get(ENV.apiUrl + "api/common/Empdashboard/getStudPerEnrollNumber?sims_grade_code=" + grade + "&sims_section_code=" + section + "&code=" + str + "&emp_code=" + username + "&stud_att_date=" + $scope.stud_att_date).then(function (studEnroll) {
                    $scope.stud_Enroll = studEnroll.data;

                    if (str == 'P') {
                        $scope.StudentData = 'Present Student';

                    }
                    else if (str == 'A') {

                        $scope.StudentData = 'Absent Student';
                    }
                    else {

                        $scope.StudentData = 'Trady Student';
                    }

                    $('#MyModal').modal('show');

                });

            }

            $http.get(ENV.apiUrl + "api/common/Empdashboard/getEmpSubjectName?emp_att_emp_id=" + username).then(function (empsub) {

                $scope.emp_sub = empsub.data;
                if ($scope.emp_sub.length == 0) {
                    $scope.one = true;
                    $scope.two = true;

                    $scope.strMessage1 = 'Records Not Found';
                }
                else {
                    $scope.one = false;
                    $scope.two = false;

                }
            });

            //$http.get(ENV.apiUrl + "api/common/Empdashboard/getLeaveTotal?att_emp_id=" + username).then(function (leaveTotal) {
                
            //    $scope.leave_Total = leaveTotal.data;
            //    for (var i = 0; i < $scope.leave_Total.length; i++) {
            //        Ltotal = parseInt(Ltotal) + parseInt($scope.leave_Total[i].le_max_days_allowed);
            //    }
            //    $scope.Ltot = Ltotal;
            //});

            $http.get(ENV.apiUrl + "api/common/Empdashboard/getLeaveType?att_emp_id=" + username).then(function (leaveType) {

                $scope.leave_type = leaveType.data;
                if ($scope.username == "admin") {
                    $scope.leave_type = "";
                }
                $scope.totalLeave = 0;
                $scope.totalLeaveTaken = 0;

                for (var i = 0; i < $scope.leave_type.length; i++) {
                    $scope.totalLeave = parseInt($scope.totalLeave) + parseInt($scope.leave_type[i].le_max_days_allowed);
                    $scope.totalLeaveTaken = parseFloat($scope.totalLeaveTaken) + parseFloat($scope.leave_type[i].le_days_taken);
                    $scope.leave_type[i]['le_days_taken'] = parseFloat($scope.leave_type[i].le_days_taken);
                }


            });

            $scope.getRemainLeave = function (leavemaxdays, leave) {
                debugger
                for (var i = 0; i < $scope.leave_type.length; i++) {
                    if (leavemaxdays == $scope.leave_type[i].emp_leave_code) {
                        $scope.maxdays = $scope.leave_type[i].le_max_days_allowed;
                        $scope.le_days_taken = $scope.leave_type[i].le_days_taken;
                        tremain = parseInt($scope.maxdays) - parseFloat($scope.leave_type[i].le_days_taken);
                    }
                }

                $scope.remain = tremain;

                $scope.leaveDesc = leave.emp_leave_name + ' =' + $scope.le_days_taken + '/' + leave.le_max_days_allowed;

            }

            $scope.subclick = function (str) {

                //$http.get(ENV.apiUrl + "api/common/Empdashboard/getEmpgradebook?emp_academic_year=" + cur_year + "&emp_att_emp_id=" + username + "&sub_code=" + str).then(function (empstgradebook) {
                //    $scope.empstgrade_book = empstgradebook.data;
                //});

                $http.get(ENV.apiUrl + "api/common/Empdashboard/getGradeSection?emp_academic_year=" + cur_year + "&emp_att_emp_id=" + username + "&sub_code=" + str).then(function (empstgradebook) {
                    $scope.empstgrade_book = empstgradebook.data;
                });

            }


            $scope.immportStaff = function () {

                var check = true;
              
                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " Staff Details.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            var blob = new Blob([document.getElementById('printdata2').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + " Staff Details" + ".xls");


                        }
                    });
                }
                else {
                    swal({ text: "Report Not Save", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, })
                }

            }

            $scope.immportStaffResigned = function () {
                var check = true;
                
                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " StaffResignedDetails.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                     
                            var blob = new Blob([document.getElementById('printdata3').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + " StaffResignedDetails" + ".xls");
                        }
                    });
                }
                else {
                    swal({ text: "Report Not Save", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, })
                }
            }

            $scope.immport = function () {
                //var blob = new Blob([document.getElementById('printdata').innerHTML], {
                //    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                //});
                //saveAs(blob, "EMPInfo.xls");
                var check = true;
                
                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " Employee Attendance.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            var blob = new Blob([document.getElementById('printdata').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + " Employee Attendance" + ".xls");
                            //alasql('SELECT * INTO XLSX("Report.xlsx",{headers:true}) \ FROM HTML("#printdata",{headers:true,skipdisplaynone:true})');

                        }
                    });
                }
                else {
                    swal({ text: "Report Not Save", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, })
                }

            }

            $scope.print = function (div) {
              
                var docHead = document.head.outerHTML;
                var printContents = document.getElementById(div).outerHTML;
                var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";

                var newWin = window.open("", "_blank", winAttr);
                var writeDoc = newWin.document;
                writeDoc.open();
                writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                writeDoc.close();
                newWin.focus();
            }


             $http.get(ENV.apiUrl + "api/common/Empdashboard/get_removed_Application_Dash?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                    for (var i = 0; i < res.data.length; i++) {
                        var id = '#' + res.data[i].comn_user_appl_code;
                        $(id).hide();
                    }
                });
           
            $scope.removeWidget = function (e) {
                var id = $(e.target).parents('.remove_widget').attr('id');
                $http.post(ENV.apiUrl + "api/common/Empdashboard/remove_tile?user=" + $rootScope.globals.currentUser.username + "&status=" + 'N' + "&appl_code=" + id).then(function (res) {
                });

                $(e.target).parents('.remove_widget').hide();
            }

            var data = { opr: 'G', subs_teacher_code: $rootScope.globals.currentUser.username };
            
            $http.post(ENV.apiUrl + "api/LectureSubstitution/LectureSubstitutionCommon", data).then(function (res) {
               
                $scope.AllSubstitution = res.data.table;
                $scope.AllDays = [];

                $(res.data.table1).each(function (k, v) {
                    $scope.AllDays[v.sims_appl_parameter] = v.sims_appl_form_field_value1;
                });
            })


            $scope.acknowledgeSubstitution = function (obj) {
                var data = {
                    opr: 'H', aca_year: obj.sims_bell_academic_year, bell_code: obj.sims_bell_code,
                    grade_code: obj.sims_bell_grade_code, sec_code: obj.sims_bell_section_code,
                    day_code: obj.sims_bell_day_code, slot_code: obj.sims_bell_slot_code,
                    filter_date: obj.sims_bell_substitution_date, subs_teacher_code: obj.sims_bell_substitution_teacher_code
                };

              

                $http.post(ENV.apiUrl + "api/LectureSubstitution/LectureSubstitutionCommon", data).then(function (res) {
                    
                });
            }
        }])
})();
