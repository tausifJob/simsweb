﻿(function () {
    'use strict';
    var main, temp1, opr, edt;
    var simsController = angular.module('sims.module.HRMS');

    simsController.controller('EmployeeMaster_ABQISCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.imageUrl = ENV.apiUrl + '/Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/';

            $scope.edt = {};
            $scope.temp = {};

            $http.get(ENV.apiUrl + "api/HRMS/emp/getEmpMasterData").then(function (res) {

                $scope.empstat = true;
                $scope.obj = res.data;

                $scope.operation = true;
                $scope.Save_btn = true;
                $scope.Update_btn = false;
                $scope.display = true;
                $scope.roleList = true;
                $scope.temp.em_Company_Code = $scope.obj[0].em_Company_Code;
                $scope.temp.em_service_status_code = 'V';
            });

            $scope.searchEmp = function ($event) {

                if ($event.keyCode == 13) {
                    $scope.EmpSearch1($scope.temp.enroll_number1);
                }
            }

            $scope.showAgreementDetails = true;
            $scope.uae = false;
            $scope.qatar = true;
            $scope.slisLabel = false;
            $scope.india = false;
            $scope.updateBtn = false;
            $scope.saveBtn = false;
            $scope.temp.pays_hobbies_code = [];

            $("#dob").kendoDatePicker({
                format: "dd-MM-yyyy",
                min: new Date(1900, 1, 1),
                max: new Date()
            });

         

            $scope.globalSrch = function () {

                $rootScope.visible_stud = false;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = false;
                $rootScope.visible_Employee = true;
                $rootScope.chkMulti = false;

                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                //$scope.temp.em_login_code = '';
                //$scope.temp.EmpName = '';
                $scope.showBtn = false;
            }
           

            $scope.$on('global_cancel', function (str) {


                if ($scope.SelectedUserLst.length > 0) {

                    $scope.temp = {};
                    $scope.temp = {
                        'enroll_number1': $scope.SelectedUserLst[0].em_login_code
                    }
                    $scope.temp['name'] = $scope.SelectedUserLst[0].empName;
                    $scope.EmpSearch1($scope.SelectedUserLst[0].em_login_code)
                }
            });

            $scope.searchGlobalClick = function () {
                $scope.Global_Search_by_employee();
            }

            if ($http.defaults.headers.common['schoolId'] == 'slis') {
                $scope.nationalIdLabel = "Iquma Number";
            }
            else {
                $scope.nationalIdLabel = "Emirates Id";
            }

            if ($http.defaults.headers.common['schoolId'] == 'elcportal') {
                $scope.serviceStatusLabel = "Employment Status";
            }
            else {
                $scope.serviceStatusLabel = "Service Status";
            }

            $http.get(ENV.apiUrl + "api/Qual/getAllQualification").then(function (res1) {
                $scope.all_Qual_Data = res1.data;
            });

            $http.get(ENV.apiUrl + "api/common/Subject/getSubject").then(function (res) {
                $scope.sub_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/HRMS/emp/getRole").then(function (rolesdata) {
                $scope.roles_data = rolesdata.data;

            });

            $http.get(ENV.apiUrl + "api/HRMSOES/GetAccommodationStatus").then(function (res) {;
                $scope.AccommondationStatus = res.data;
                console.log("AccommondationStatus", $scope.AccommondationStatus);
            });

            $scope.setGrade_Dept = function (str) {

                $scope.temp.em_dept_effect_from = str;
                $scope.temp.em_grade_effect_from = str;
            }

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;
                console.log("ComboBoxValues", $scope.ComboBoxValues);
            });

            $(function () {
                $('#hobbielist').multipleSelect({
                    width: '100%'
                });
            });

            $http.get(ENV.apiUrl + "api/HRMS/emp/getHobbies").then(function (res) {
                $scope.hobbies = res.data;
                console.log("hobbies",$scope.hobbies);
                setTimeout(function () {
                    $('#hobbielist').change(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });

            $http.get(ENV.apiUrl + "api/HRMS/getLicSchool").then(function (licschooldata) {
                $scope.lic_school_data = licschooldata.data;
                console.log("lic_school_data", $scope.lic_school_data);
                if ($scope.lic_school_data[0].lic_school_country == 'Qatar') {
                    $scope.uae = false;
                    $scope.qatar = true;
                    $scope.slisLabel = false;
                    $scope.india = false;
                }
                else if ($scope.lic_school_data[0].lic_school_country == 'Saudi Arabia') {
                    $scope.uae = false;
                    $scope.qatar = false;
                    $scope.slisLabel = true;
                    $scope.india = false;
                }
                else if ($scope.lic_school_data[0].lic_school_country == 'India') {
                    $scope.uae = false;
                    $scope.qatar = false;
                    $scope.slisLabel = false;
                    $scope.india = true;
                }
                else {
                    $scope.uae = true;
                    $scope.qatar = false;
                    $scope.slisLabel = false;
                    $scope.india = false;
                }
            });

            $scope.checkDateFormat = function (dateFormat, ID) {
                var str = dateFormat.indexOf('/');
                if (str != -1) {
                    swal({ title: "Alert", text: "Please enter date only DD-MM-YYYY format", width: 360, height: 200 });
                    $('#' + ID).val('');
                }
            }

            $scope.Cancel = function () {
                $scope.temp = {};
                $("#empimg").attr('src', '')
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.em_img = "";
                $scope.Save_btn = true;
                $scope.Update_btn = false;
                $scope.temp.em_Company_Code = $scope.obj[0].em_Company_Code;
                $scope.temp.em_service_status_code = 'V';
                var data = {};

                try {
                    $("#hobbie_code").multipleSelect("uncheckAll");
                }
                catch (e) {

                }
            }


            $scope.getstate = function (country) {
                $http.get(ENV.apiUrl + "api/HRMS/emp/getState?em_Country_Code=" + country).then(function (res) {
                    $scope.stt = res.data;
                });
            }

            $scope.getcity = function (state) {
                $http.get(ENV.apiUrl + "api/HRMS/emp/getCity?em_State_Code=" + state).then(function (res) {
                    $scope.ctt = res.data;
                });
            }

            $scope.getContract = function () {
                $http.get(ENV.apiUrl + "api/contract/Get_ContractDetails").then(function (Get_Contract) {
                    $scope.Contract_data = Get_Contract.data;

                });
            }
            $scope.getContract();

            $scope.setAgreementDate = function (contractID) {
                $scope.showAgreementDetails = false;

                for (var i = 0; i < $scope.Contract_data.length; i++) {
                    if ($scope.Contract_data[i].cn_code == contractID) {
                        var duration = $scope.Contract_data[i].cn_duration;
                        break;
                    }
                }

                var dt = new Date();
                var currentDate = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                $scope.temp.em_agreement_start_date = currentDate;

                var day = $scope.temp.em_date_of_join.split("-")[0];
                var month = $scope.temp.em_date_of_join.split("-")[1];
                var year = $scope.temp.em_date_of_join.split("-")[2];

                if (isNaN(month)) {
                    month = $scope.temp.em_date_of_join.split("-")[1];
                }

                var expMonth = parseInt(month) + parseInt(duration);


                if (expMonth > 12) {
                    var expYear = parseInt(year) + 1;
                    expMonth = parseInt(expMonth) - 12;
                }
                else {
                    expYear = year;
                }
                $scope.temp.em_agreement_exp_date = day + "-" + expMonth + "-" + expYear;

                if ($scope.temp.em_agreement_exp_date != undefined) {
                    if (expMonth == 1) {
                        var expYear = parseInt(expYear) - 1;
                        expMonth = 12;
                    }
                    else {
                        expMonth = (parseInt(expMonth) - 1)
                    }
                    $scope.temp.em_agreemet_exp_rem_date = day + "-" + expMonth + "-" + expYear;
                }
            }


            $scope.EmpSearch1 = function (emplogcode) {
                debugger;
                $scope.Save_btn = false;
                $scope.Update_btn = true;
                $http.get(ENV.apiUrl + "api/HRMS/emp/getSearchEmpMaster_ABQIS?em_login_code=" + $scope.temp.enroll_number1).then(function (res) {
                    $scope.temp1 = res.data;
                    $scope.Myform.$setPristine();
                    $scope.Myform.$setUntouched();
                    $scope.temp = {
                        enroll_number1: emplogcode,
                        em_login_code: $scope.temp1[0].em_login_code,
                        em_number: $scope.temp1[0].em_number,
                        em_service_status_code: $scope.temp1[0].em_service_status_code,
                        em_status_code: $scope.temp1[0].em_status_code,
                        em_apartment_number: $scope.temp1[0].em_apartment_number,
                        em_family_name: $scope.temp1[0].em_family_name,
                        em_street_number: $scope.temp1[0].em_street_number,
                        em_area_number: $scope.temp1[0].em_area_number,
                        em_phone: $scope.temp1[0].em_phone,
                        em_fax: $scope.temp1[0].em_fax,
                        em_post: $scope.temp1[0].em_post,
                        em_emergency_contact_name2: $scope.temp1[0].em_emergency_contact_name2,
                        em_emergency_contact_number2: $scope.temp1[0].em_emergency_contact_number2,
                        em_joining_ref: $scope.temp1[0].em_joining_ref,
                        em_ethnicity_code: $scope.temp1[0].em_ethnicity_code,
                        em_handicap_status: $scope.temp1[0].em_handicap_status,
                        em_stop_salary_indicator: $scope.temp1[0].em_stop_salary_indicator,
                        //em_agreement: $scope.temp1[0].em_agreement,
                        em_agreement_code: $scope.temp1[0].em_agreement_code,
                        em_punching_status: $scope.temp1[0].em_punching_status,
                        em_bank_cash_tag: $scope.temp1[0].em_bank_cash_tag,
                        em_citi_exp_tag: $scope.temp1[0].em_citi_exp_tag,
                        em_leave_tag: $scope.temp1[0].em_leave_tag,
                        em_passport_issue_place: $scope.temp1[0].em_passport_issue_place,
                        em_passport_issuing_authority: $scope.temp1[0].em_passport_issuing_authority,
                        em_pssport_exp_rem_date: $scope.temp1[0].em_pssport_exp_rem_date,
                        em_visa_issuing_place: $scope.temp1[0].em_visa_issuing_place,
                        em_visa_issuing_authority: $scope.temp1[0].em_visa_issuing_authority,
                        em_visa_exp_rem_date: $scope.temp1[0].em_visa_exp_rem_date,
                        em_visa_type_code: $scope.temp1[0].em_visa_type_code,
                        em_agreement_start_date: $scope.temp1[0].em_agreement_start_date,
                        em_agreement_exp_date: $scope.temp1[0].em_agreement_exp_date,
                        em_agreemet_exp_rem_date: $scope.temp1[0].em_agreemet_exp_rem_date,
                        em_punching_id: $scope.temp1[0].em_punching_id,
                        em_dependant_full: $scope.temp1[0].em_dependant_full,
                        em_dependant_half: $scope.temp1[0].em_dependant_half,
                        em_dependant_infant: $scope.temp1[0].em_dependant_infant,
                        em_Bank_Code: $scope.temp1[0].em_Bank_Code,
                        em_bank_ac_no: $scope.temp1[0].em_bank_ac_no,
                        em_iban_no: $scope.temp1[0].em_iban_no,
                        em_route_code: $scope.temp1[0].em_route_code,
                        em_bank_swift_code: $scope.temp1[0].em_bank_swift_code,
                        em_gpf_ac_no: $scope.temp1[0].em_gpf_ac_no,
                        em_pan_no: $scope.temp1[0].em_pan_no,
                        em_labour_card_no: $scope.temp1[0].em_labour_card_no,
                        en_labour_card_expiry_date: $scope.temp1[0].en_labour_card_expiry_date,
                        em_gosi_ac_no: $scope.temp1[0].em_gosi_ac_no,
                        em_gosi_start_date: $scope.temp1[0].em_gosi_start_date,
                        em_national_id: $scope.temp1[0].em_national_id,
                        em_secret_question_code: $scope.temp1[0].em_secret_question_code,
                        em_secret_answer: $scope.temp1[0].em_secret_answer,
                        em_building_number: $scope.temp1[0].em_building_number,
                        em_Company_Code: $scope.temp1[0].em_Company_Code,
                        em_Dept_Code: $scope.temp1[0].em_Dept_Code,
                        em_Designation_Code: $scope.temp1[0].em_Designation_Code,
                        em_Dest_Code: $scope.temp1[0].em_Dest_Code,
                        em_Grade_Code: $scope.temp1[0].em_Grade_Code,
                        em_Marital_Status_Code: $scope.temp1[0].em_Marital_Status_Code,
                        em_Nation_Code: $scope.temp1[0].em_Nation_Code,
                        em_Country_Code: $scope.temp1[0].em_Country_Code,
                        em_State_Code: $scope.temp1[0].em_State_Code,
                        em_City_Code: $scope.temp1[0].em_City_Code,
                        em_Religion_Code: $scope.temp1[0].em_Religion_Code,
                        em_Salutation_Code: $scope.temp1[0].em_Salutation_Code,
                        em_Sex_Code: $scope.temp1[0].em_Sex_Code,
                        em_Staff_Type_Code: $scope.temp1[0].em_Staff_Type_Code,
                        em_blood_group_code: $scope.temp1[0].em_blood_group_code,
                        em_date_of_birth: $scope.temp1[0].em_date_of_birth,
                        em_date_of_join: $scope.temp1[0].em_date_of_join,
                        em_dept_effect_from: $scope.temp1[0].em_dept_effect_from,
                        em_email: $scope.temp1[0].em_email,
                        em_emergency_contact_name1: $scope.temp1[0].em_emergency_contact_name1,
                        em_emergency_relation1: $scope.temp1[0].em_emergency_relation1,
                        em_emergency_relation2: $scope.temp1[0].em_emergency_relation2,
                        em_emergency_contact_number1: $scope.temp1[0].em_emergency_contact_number1,
                        em_first_name: $scope.temp1[0].em_first_name,
                        em_grade_effect_from: $scope.temp1[0].em_grade_effect_from,
                        em_last_name: $scope.temp1[0].em_last_name,
                        em_middle_name: $scope.temp1[0].em_middle_name,
                        em_mobile: $scope.temp1[0].em_mobile,
                        em_name_ot: $scope.temp1[0].em_name_ot,
                        em_national_id_expiry_date: $scope.temp1[0].em_national_id_expiry_date,
                        em_national_id_issue_date: $scope.temp1[0].em_national_id_issue_date,
                        em_passport_exp_date: $scope.temp1[0].em_passport_exp_date,
                        em_passport_issue_date: $scope.temp1[0].em_passport_issue_date,
                        em_passport_no: $scope.temp1[0].em_passport_no,
                        em_summary_address: $scope.temp1[0].em_summary_address,
                        em_summary_address_local_language: $scope.temp1[0].em_summary_address_local_language,
                        em_visa_exp_date: $scope.temp1[0].em_visa_exp_date,
                        em_stop_salary_from: $scope.temp1[0].em_stop_salary_from,
                        em_visa_issue_date: $scope.temp1[0].em_visa_issue_date,
                        em_visa_no: $scope.temp1[0].em_visa_no,
                        comn_role_code: $scope.temp1[0].comn_role_code,
                        comn_role_name: $scope.temp1[0].comn_role_name,
                        em_img: $scope.temp1[0].em_img,
                        em_accommodation_status: $scope.temp1[0].em_accommodation_status,
                        em_personalemail: $scope.temp1[0].em_personalemail,

                        em_adec_approval_number: $scope.temp1[0].em_adec_approval_number,
                        em_adec_approval_Date: $scope.temp1[0].em_adec_approval_Date,
                        em_adec_approved_qualification: $scope.temp1[0].em_adec_approved_qualification,
                        em_adec_approved_designation: $scope.temp1[0].em_adec_approved_designation,
                        em_adec_approved_subject: $scope.temp1[0].em_adec_approved_subject,
                        em_adec_approved_level: $scope.temp1[0].em_adec_approved_level,

                        em_health_card_no: $scope.temp1[0].em_health_card_no,
                        em_health_card_effective_from_date: $scope.temp1[0].em_health_card_effective_from_date,
                        em_health_card_effective_upto_date: $scope.temp1[0].em_health_card_effective_upto_date,
                        em_heath_card_status: $scope.temp1[0].em_heath_card_status,
                        em_teacher_status: $scope.temp1[0].em_teacher_status,
                        em_house: $scope.temp1[0].em_house,
                        pays_hobbies_code: $scope.temp1[0].pays_hobbies_code
                    }

                        $(document).ready(function () {
                            $scope.temp.pays_hobbies_code = $scope.temp1[0].pays_hobbies_code.split(',');
                            $("#hobbielist").multipleSelect("setSelects", $scope.temp.pays_hobbies_code);
                        });
                           
                    $scope.getstate($scope.temp.em_Country_Code);
                    $scope.getcity($scope.temp.em_State_Code);
                    console.log("$scope.temp1",$scope.temp1);
                    //var num = Math.random();
                    var imgSrc = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/' + $scope.temp1[0].em_img;
                    $(function () {
                        $('#empimg').attr("src", imgSrc);
                    });

                });
            }


            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };

            $scope.file_changed = function (element) {
                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);

                if ($scope.photo_filename != undefined || $scope.photo_filename != "") {
                    var imageType = $scope.photo_filename.split('/');
                    console.log("imageType", imageType[1]);
                    if (imageType[1] == 'jpeg' || imageType[1] == 'jpg' || imageType[1] == 'png') {

                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $scope.$apply(function () {
                                //$scope.prev_img = e.target.result;
                                var imgSrc = e.target.result;
                                $(function () {
                                    $('#empimg').attr("src", imgSrc);
                                });
                            });
                        };
                        reader.readAsDataURL(photofile);
                    }
                    else {
                        swal({ text: "Only jpeg and png image format supported." });
                        $('#empimg').attr("src", '');
                        return;
                    }
                }
                else {
                    swal({ text: "Image format not supported." });
                    $('#empimg').attr("src", '');
                    return;
                }


            };


            $scope.uploadImgClickF = function () {
                $scope.uploadImgClickFShow = true;
            }

            $scope.uploadImgCancel = function () {
                $scope.uploadImgClickFShow = false;
            }

            $scope.image_click = function () {
                $('#upload_img').modal('show');
                $scope.ImgLoaded = true;
            }

            $scope.globalSearch = function () { $('#MyModal').modal('show'); $scope.showBtn = false; }

            $scope.EmployeeDetails = function () {

                $scope.NodataEmployee = false;
                $http.post(ENV.apiUrl + "api/common/SearchEmployee?data=" + JSON.stringify($scope.temp)).then(function (SearchEmployee_Data) {
                    $scope.Employee_Details = SearchEmployee_Data.data;

                    $scope.EmployeeTable = true;

                    if (SearchEmployee_Data.data.length > 0) {
                    }
                    else {
                        $scope.NodataEmployee = true;
                    }
                });

            }

            $scope.EmployeeAdd = function (info) {
                $scope.temp = {
                    enroll_number: info.em_login_code,
                    emp_name: info.empName,
                    emp_desg: info.desg_name,
                    emp_dept: info.dept_name
                };
            }

            $scope.checkEmailExists = function (email_id) {
                if ($scope.temp1[0].em_email != email_id) {
                    $http.post(ENV.apiUrl + "api/HRMS/checkEmailExists?email_id=" + email_id).then(function (resc) {
                        $scope.comnUserData = resc.data;
                        if ($scope.comnUserData.length > 0) {
                            swal({ text: 'Official email id already exists!!', width: 350 });
                            $scope.temp.em_email = "";
                            return false;
                        }
                    });
                }
            }
            $scope.change_em_teacher_status = function () {
                if ($scope.temp.em_Staff_Type_Code != 'T') {
                    $scope.temp.em_teacher_status = false;
                } else {
                    $scope.temp.em_teacher_status = true;
                }

            }
            $scope.SaveData = function (isvalid) {

                if (isvalid) {
                    $scope.saveBtn = true;
                    $scope.updateBtn = true;

                    var data = $scope.temp;
                    if ($scope.photo_filename === undefined) {
                        data.em_img = null;
                    }
                    else {
                        data.em_img = '.' + $scope.photo_filename.split("/")[1];
                    }
                    data.pays_hobbies_code = $scope.temp.pays_hobbies_code.join(",");
                    data.opr = "I";
                    $http.post(ENV.apiUrl + "api/HRMS/emp/CUMasterEmployee_ABQIS", data).then(function (res) {
                        $scope.CUDobj = {};
                        $scope.CUDobj = res.data;
                        if ($scope.CUDobj.strMessage != undefined || $scope.CUDobj.strMessage != "" || $scope.CUDobj.strMessage != null) {
                            swal({ text: $scope.CUDobj.strMessage });

                            $scope.Cancel();

                            setTimeout(function () {
                                $scope.saveBtn = false;
                                $scope.updateBtn = false;
                            }, 3000);

                        }

                        var empid = $scope.CUDobj.strMessage.split(" ")[2];
                        var request = {
                            method: 'POST',
                            url: ENV.apiUrl + '/api/file/upload?filename=' + empid + "&location=" + "/EmployeeImages",
                            data: formdata,
                            headers: {
                                'Content-Type': undefined
                            }
                        };

                        $http(request).success(function (d) {
                            //alert(d);
                        });

                    });


                }
                else {
                    swal({ text: 'Kindly fill Mandatory fields', width: 350, timer: 5000, showCloseButton: true });
                }
            }

            $scope.UpdateData = function (Myform) {
                debugger
                if (Myform) {
                    $scope.saveBtn = true;
                    $scope.updateBtn = true;
                    var x = Math.floor((Math.random() * 1000) + 1);
                    var data = $scope.temp;
                    var t = "png"; //$scope.photo_filename.split("/")[1]
                    if ($scope.photo_filename === undefined) {
                    }
                    else {
                        data.em_img = $scope.temp.em_login_code + x + '.' + t;
                    }
                    data.pays_hobbies_code = $scope.temp.pays_hobbies_code.join(",");

                    data.opr = 'U';
                    data.subopr = 'Up';
                    console.log("temp",$scope.temp);
                    $http.post(ENV.apiUrl + "api/HRMS/emp/CUMasterEmployee_ABQIS", data).then(function (res) {
                        $scope.CUDobj = {};
                        $scope.CUDobj = res.data;
                        if ($scope.CUDobj.strMessage != undefined || $scope.CUDobj.strMessage != "" || $scope.CUDobj.strMessage != null) {
                            swal({ text: $scope.CUDobj.strMessage });

                            setTimeout(function () {
                                $scope.Cancel();
                                $scope.saveBtn = false;
                                $scope.updateBtn = false;
                            }, 3000);
                        }

                    });
                    debugger;
                    if ($scope.ImgLoaded == true) {
                        var request = {
                            method: 'POST',
                            url: ENV.apiUrl + '/api/file/uploadPNG?filename=' + $scope.temp.em_login_code + x + "&location=" + "/EmployeeImages",
                            data: formdata,
                            headers: {
                                'Content-Type': undefined
                            }
                        };
                        $http(request).success(function (d) {
                            //alert(d);
                        });
                    }
                }
                else {
                    swal({ text: 'Kindly fill Mandatory fields', width: 350, timer: 5000, showCloseButton: true });
                }
            }

            //$('*[data-datepicker="true"] input[type="text"]').datepicker({
            //    todayBtn: true,
            //    orientation: "top left",
            //    autoclose: true,
            //    todayHighlight: true,
            //    format: 'yyyy-mm-dd'
            //});

            $scope.UploadImageModal = function () {

                $("#prevImg").val('');
                $scope.edt.em_img = '';
                //imagename = str.em_number;
                $('#myModal').modal('show');
                $scope.ImgLoaded = true;
            }
        }]
        )


    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

})();