﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('GoalTargetKPIMeasureCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/GoalTarget/getGoalName").then(function (goalName) {
                $scope.goal_Name = goalName.data;
                console.log($scope.goal_Name);
            });

            $http.get(ENV.apiUrl + "api/GoalTarget/getAcademicYear").then(function (Acyear) {
                $scope.Acc_year = Acyear.data;
                console.log($scope.Acc_year);
                $scope.temp['sims_sip_academic_year'] = $scope.Acc_year[0].sims_sip_academic_year;
            });

            $scope.getGoalTarget = function (goalcode) {
                $http.get(ENV.apiUrl + "api/GoalTargetKPI/getGoalTargetName?sims_sip_goal_code=" + goalcode).then(function (targetName) {
                    $scope.target_Name = targetName.data;
                    console.log($scope.target_Name);
                });
            }

            $scope.getGoalTargetKPI = function (goalCode, tarCode) {
                $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasure/getGoalTargetKPIMeasure?sims_sip_goal_code=" + goalCode + "&sims_sip_goal_target_code=" + tarCode).then(function (goalTargetKPI) {
                    $scope.goal_TargetKPI = goalTargetKPI.data;
                    console.log($scope.goal_TargetKPI);
                });
            }

            $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasure/getAllGoalTargetKPIMeasure").then(function (res1) {
                $scope.goal_TarKPI = res1.data;
                $scope.totalItems = $scope.goal_TarKPI.length;
                $scope.todos = $scope.goal_TarKPI;
                $scope.makeTodos();
            });

            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str;
            //    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.goal_TarKPI;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.goal_TarKPI, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.goal_TarKPI;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                
                /* Search Text in all 3 fields */
                return (item.sims_sip_goal_target_kpi_measure_min_point.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_sip_goal_target_kpi_measure_max_point.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_sip_goal_target_kpi_measure_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_sip_goal_target_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sims_sip_goal_target_kpi_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_sip_goal_target_kpi_desc == toSearch) ? true : false;
            }
            $scope.chkMaxMin = function () {
                if ($scope.temp.sims_sip_goal_target_kpi_measure_max_point <= $scope.temp.sims_sip_goal_target_kpi_measure_min_point) {
                    swal({ title: "Alert", text: "Maximum Point should greater than Minimum Point", width: 300, height: 200 });
                }
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.gdisabled = false;
                    $scope.aydisabled = false;
                    $scope.gtdisabled = false;
                    $scope.gtkpidisabled = false;
                    $scope.gdescdisabled = false;
                    $scope.disabled = false;
                    $scope.readonly = true;
                    $scope.table = false;
                    $scope.display = true;
                    $scope.save_btn = true;
                    $scope.Update_btn = false;
                    $scope.temp = "";
                    $scope.Myform.$setPristine();
                    $scope.Myform.$setUntouched();
                    $scope.temp = {};
                    $scope.temp.sims_sip_goal_target_kpi_measure_status = true;
                    $scope.temp['sims_sip_academic_year'] = $scope.Acc_year[0].sims_sip_academic_year;
                }
            }

            var datasend = [];
            $scope.savedata = function (Myform) {

                if (Myform) {

                    var data = $scope.temp;
                    data.opr = 'I';
                    datasend.push(data);

                    $http.post(ENV.apiUrl + "api/GoalTargetKPIMeasure/CUDGoalTargetKPIMeasure", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasure/getAllGoalTargetKPIMeasure").then(function (res1) {
                            $scope.goal_TarKPI = res1.data;
                            $scope.totalItems = $scope.goal_TarKPI.length;
                            $scope.todos = $scope.goal_TarKPI;
                            $scope.makeTodos();

                        });

                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.sims_sip_academic_year = "";
                $scope.temp.sims_sip_goal_code = "";
                $scope.temp.sims_sip_goal_target_code = "";
                $scope.temp.sims_sip_goal_target_kpi_code = "";
                $scope.temp.sims_sip_goal_target_kpi_measure_desc = "";
                $scope.temp.sims_sip_goal_target_kpi_measure_min_point = "";
                $scope.temp.sims_sip_goal_target_kpi_measure_max_point = "";
                $scope.temp.sims_sip_goal_target_kpi_measure_status = "";
            }

            $scope.edit = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.stry = str;
                    $scope.table = false;
                    $scope.display = true;
                    $scope.save_btn = false;
                    $scope.Update_btn = true;
                    $scope.gdisabled = true;
                    $scope.aydisabled = true;
                    $scope.gtdisabled = true;
                    $scope.gtkpidisabled = true;
                    $scope.gdescdisabled = true;

                    $scope.getGoalTarget(str.sims_sip_goal_code);
                    $scope.getGoalTargetKPI(str.sims_sip_goal_code, str.sims_sip_goal_target_code);

                    $scope.temp = {
                        sims_sip_goal_code: str.sims_sip_goal_code
                       , sims_sip_goal_target_code: str.sims_sip_goal_target_code
                       , sims_sip_academic_year: str.sims_sip_academic_year
                       , sims_sip_goal_target_kpi_code: str.sims_sip_goal_target_kpi_code
                        , sims_sip_goal_target_kpi_measure_code: str.sims_sip_goal_target_kpi_measure_code
                        //,sims_sip_goal_target_kpi_measure_code:str.sims_sip_goal_target_kpi_measure_auto_code
                       , sims_sip_goal_target_kpi_measure_desc: str.sims_sip_goal_target_kpi_measure_desc
                       , sims_sip_goal_target_kpi_measure_max_point: str.sims_sip_goal_target_kpi_measure_max_point
                        , sims_sip_goal_target_kpi_measure_min_point: str.sims_sip_goal_target_kpi_measure_min_point
                        , sims_sip_goal_target_kpi_measure_status: str.sims_sip_goal_target_kpi_measure_status
                    };

                }
            }

            var dataupdate = [];
            $scope.update = function (Myform) {
                if (Myform) {
                    var data = $scope.temp;
                    data.opr = "U";
                    dataupdate.push(data);


                    $http.post(ENV.apiUrl + "api/GoalTargetKPIMeasure/CUDGoalTargetKPIMeasure", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasure/getAllGoalTargetKPIMeasure").then(function (res1) {
                            $scope.goal_TarKPI = res1.data;
                            $scope.totalItems = $scope.goal_TarKPI.length;
                            $scope.todos = $scope.goal_TarKPI;
                            $scope.makeTodos();
                        });
                    });
                    dataupdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }


            $scope.OkDelete = function () {
                debugger;
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    var deletefin = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodulecode = ({

                                'sims_sip_goal_code': $scope.filteredTodos[i].sims_sip_goal_code,
                                'sims_sip_goal_target_code': $scope.filteredTodos[i].sims_sip_goal_target_code,
                                'sims_sip_goal_target_kpi_code': $scope.filteredTodos[i].sims_sip_goal_target_kpi_code,
                                'sims_sip_goal_target_kpi_measure_auto_code': $scope.filteredTodos[i].sims_sip_goal_target_kpi_measure_auto_code,
                                'opr': "D"
                            });
                            deletefin.push(deletemodulecode);
                        }
                    }
                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $http.post(ENV.apiUrl + "api/GoalTargetKPIMeasure/CUDGoalTargetKPIMeasure", deletefin).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasure/getAllGoalTargetKPIMeasure").then(function (res1) {
                                                    $scope.goal_TarKPI = res1.data;
                                                    $scope.totalItems = $scope.goal_TarKPI.length;
                                                    $scope.todos = $scope.goal_TarKPI;
                                                    $scope.makeTodos();
                                                });
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    $scope.row1 = '';
                                                }
                                                $scope.currentPage = true;
                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/GoalTargetKPIMeasure/getAllGoalTargetKPIMeasure").then(function (res1) {
                                                    $scope.goal_TarKPI = res1.data;
                                                    $scope.totalItems = $scope.goal_TarKPI.length;
                                                    $scope.todos = $scope.goal_TarKPI;
                                                    $scope.makeTodos();
                                                });
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    $scope.row1 = '';
                                                }
                                                $scope.currentPage = true;
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });
                            }
                            else {
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById(i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    $scope.currentPage = str;
                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });


        }])

})();
