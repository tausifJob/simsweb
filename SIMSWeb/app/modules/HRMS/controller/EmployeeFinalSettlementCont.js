﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeFinalSettlementCont',
        ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.display = true;
            $scope.grid = true;
            $scope.edt = {};
            $scope.Save_btn = false;
            $scope.showTable = false;
            $scope.showPayrollDetails = false;
            $scope.showGratuityDetails = false;
            $scope.To_Updateemployee = false;
            $scope.temp = {};
            $scope.payrollMonths = [];
            $scope.allgratuityData = {};
            var main;
            $scope.edt.netPayable = 0;
            $scope.edt.leave_salary = 0;
            $scope.edt.airfare = 0;
            $scope.edt.totalEarning = 0;
            $scope.edt.totalDeduction = 0;
            $scope.disableSubmitBtn = false;
            $scope.busyindicator = true;

            $http.get(ENV.apiUrl + "api/common/GlobalSearch/GetCompanyNameForShift").then(function (res) {
                $scope.cmbCompany = res.data;
                $scope.temp.company_code = $scope.cmbCompany[0].sh_company_code;
                $scope.getDeptFromCompany($scope.cmbCompany[0].sh_company_code);
                $scope.getDesignationFromCompany($scope.cmbCompany[0].sh_company_code);
            });


            $scope.getDeptFromCompany = function (company_code) {
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/GetDepartmentName?company_code=" + company_code).then(function (res) {
                    $scope.compDepartment = res.data;
                });
            }

            $scope.getDesignationFromCompany = function (company_code) {
                $http.get(ENV.apiUrl + "api/EmployeeReport/GetDesignation?com_code=" + company_code).then(function (res) {
                    $scope.compDesignation = res.data;
                });
            }
            $http.get(ENV.apiUrl + "api/updatepaysheet/GetAllPayCodes").then(function (GetAllPayCodes) {
                $scope.GetAllPayCodes = GetAllPayCodes.data;
            });

            $http.get(ENV.apiUrl + "api/generatepayroll/GetCurrentFinYearFromFinsParameter").then(function (GetCurrentFinYearFromFinsParameter) {
                $scope.GetCurrentFinYearFromFinsParameter1 = GetCurrentFinYearFromFinsParameter.data;
                $scope.fin_year = $scope.GetCurrentFinYearFromFinsParameter1[0].fin_year;
                $scope.fin_year_desc = $scope.GetCurrentFinYearFromFinsParameter1[0].fin_year_desc;
            });

            $scope.getGetSalaryDetails = function (em_number) {
                $http.get(ENV.apiUrl + "api/EmployeeSettlement/GetSalaryDetails?em_number=" + em_number).then(function (GetSalaryDetails) {
                    $scope.GetSalaryDetailsByMonth = GetSalaryDetails.data;
                    console.log("GetSalaryDetailsByEmployee", $scope.GetSalaryDetailsByMonth);
                    if ($scope.GetSalaryDetailsByMonth.length > 0) {
                        for (var i = 0; i < $scope.GetSalaryDetailsByMonth.length; i++) {
                            //var v = document.getElementById('test-' + i);
                            //v.checked = true;
                            $scope.GetSalaryDetailsByMonth[i].check = true;
                            $scope.chk_all = true;
                            $scope.edt.netPayable += parseFloat($scope.GetSalaryDetailsByMonth[i].net_amount);
                            $scope.edt.totalEarning += parseFloat($scope.GetSalaryDetailsByMonth[i].earning_amount);
                            $scope.edt.totalDeduction += parseFloat($scope.GetSalaryDetailsByMonth[i].deduction_amount);
                        }
                    }
                    console.log("totalDeduction",$scope.edt.totalDeduction);

                });

                $http.get(ENV.apiUrl + "api/EmployeeSettlement/GetGratuityByEmployee?em_number=" + em_number).then(function (res) {
                    $scope.gratuityDeatails = res.data;
                    if ($scope.gratuityDeatails.length > 0){
                        $scope.edt.grt_amount= parseFloat($scope.gratuityDeatails[0].grt_amount).toFixed(3);
                        $scope.edt.grt_applicable_days = $scope.gratuityDeatails[0].grt_applicable_days;
                        $scope.edt.grt_working_days = $scope.gratuityDeatails[0].grt_working_days;

                        $scope.edt.grt_amount2 = $scope.gratuityDeatails[0].grt_amount;
                        $scope.edt.netPayable += parseFloat($scope.edt.grt_amount2);
                        $scope.edt.totalEarning += parseFloat($scope.edt.grt_amount2);
                    }
                    else{
                        $scope.edt.grt_amount = 0;
                    }                    
                    console.log("gratuityDeatails", $scope.gratuityDeatails);
                });
            }

            $scope.CheckAllChecked = function () {
                debugger
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.GetSalaryDetailsByMonth.length; i++) {
                        var v = document.getElementById('test-'+ i);
                        v.checked = true;
                        $scope.edt.netPayable += parseFloat($scope.GetSalaryDetailsByMonth[i].net_amount);
                        $scope.edt.totalEarning += parseFloat($scope.GetSalaryDetailsByMonth[i].earning_amount);
                    }
                }
                else {

                    for (var i = 0; i < $scope.GetSalaryDetailsByMonth.length; i++) {
                        var v = document.getElementById('test-' + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $scope.edt.netPayable = 0;
                    }
                }
            }

            $scope.checkonebyonedelete = function (info) {
                debugger
                if (info.check == true) {
                    info.check = true;
                    $scope.edt.netPayable += parseFloat(info.net_amount);
                    $scope.edt.totalEarning += parseFloat(info.earning_amount);
                }
                else {
                    info.check = false;
                    $scope.edt.netPayable -= parseFloat(info.net_amount);
                    $scope.edt.totalEarning -= parseFloat(info.earning_amount);
                    $scope.chk_all = false;
                }

                for (var i = 0; i < $scope.GetSalaryDetailsByMonth.length; i++) {
                    if ($scope.GetSalaryDetailsByMonth[i].check == true) {
                        $scope.chk_all = true;
                    }
                    else {
                        $scope.chk_all = false;
                    }
                }
               
            }

            $scope.setNetAmount = function (str) {
                debugger
                console.log($scope.edt.leave_salary);
                if (str == 'G') {
                    if ($scope.edt.grt_amount == null || $scope.edt.grt_amount == "") {
                        $scope.edt.grt_amount = 0;

                    } else {
                        $scope.edt.netPayable -= parseFloat($scope.edt.grt_amount2);
                        $scope.edt.netPayable += parseFloat($scope.edt.grt_amount);
                        $scope.edt.totalEarning -= parseFloat($scope.edt.grt_amount2);
                        $scope.edt.totalEarning += parseFloat($scope.edt.grt_amount);
                    }
                }
                else if (str == 'L') {
                    if ($scope.edt.leave_salary == null || $scope.edt.leave_salary == "") {
                        $scope.edt.leave_salary = 0;

                    } else {
                        $scope.edt.netPayable += parseFloat($scope.edt.leave_salary);
                        $scope.edt.totalEarning += parseFloat($scope.edt.leave_salary);
                    }
                }
                else if (str == 'A') {
                    if ($scope.edt.airfare == null || $scope.edt.airfare == "") {
                        $scope.edt.airfare = 0;

                    } else {
                        $scope.edt.netPayable += parseFloat($scope.edt.airfare);
                        $scope.edt.totalEarning += parseFloat($scope.edt.airfare);
                    }
                }                    
            }

            $scope.submit = function () {
                $scope.busyindicator = false;
                $scope.disableSubmitBtn = true;
                $scope.edt.reg_date = $scope.temp.reg_date;
                $scope.edt.com_code = '1' //$scope.temp.company_code;
                console.log("edt", $scope.edt);

                $http.post(ENV.apiUrl + "api/EmployeeSettlement/SubmitSettlement", $scope.edt).then(function (res) {
                    $scope.response = res.data;
                    console.log(res);
                    console.log($scope.response.strMessage);
                    swal({ title: "Alert", text: $scope.response.strMessage, showCloseButton: true, width: 380, })
                        $scope.disableSubmitBtn = false;
                        $scope.busyindicator = true;
                        $scope.reset();                      
                });
            }

            $scope.cancel = function () {
                $state.go('main.TimeLine');
            }

            $scope.reset = function () {
                $scope.edt = {};
                $scope.temp = {};
                $scope.showTable = false;
                $scope.edt.netPayable = 0;
                $scope.edt.leave_salary = 0;
                $scope.edt.airfare = 0;
                $scope.edt.totalEarning = 0;
                $scope.edt.totalDeduction = 0;
                $scope.disableSubmitBtn = false;
                $scope.busyindicator = true;
            }

            $scope.getPayrollGeneratedMonth = function (em_number) {
                $http.get(ENV.apiUrl + "api/EmployeeSettlement/GetPayrollGeneratedMonth?em_number=" + em_number).then(function (res) {
                    $scope.payrollMonths = res.data;
                    console.log("payrollMonths",$scope.payrollMonths);
                });

                $http.post(ENV.apiUrl + "api/OvertimeHour/Year_month").then(function (Year_month) {
                    $scope.Year_monthData = Year_month.data;
                    $scope.Year_month = [];
                    var d = new Date().getFullYear();
                    var n = new Date().getMonth() + 1;
                    var ym = d + '' + n;
                    //$scope.edt = { year_month: ym };
                    // $scope.edt.yearCode = ym;
                    //$scope.edt1.yearCode = ym;
                    setTimeout(function () {
                        for (var i = 0; i < $scope.Year_monthData.length ; i++) {

                            for (var j = 0; j < $scope.payrollMonths.length ; j++) {
                                if ($scope.Year_monthData[i].year_month.substr(4) == $scope.payrollMonths[j].pe_year_month.substr(4)) {
                                    var obj = {
                                        year_month: $scope.Year_monthData[i].year_month,
                                        year_month_name: $scope.Year_monthData[i].year_month_name
                                    }
                                    $scope.Year_month.push(obj);
                                }
                            }
                        }
                        console.log("Year_month", $scope.Year_month);
                    }, 500);
                    

                  //  $scope.setMonth();
                });
            }

            $scope.setMonth = function () {
                $scope.edt.year_month = $scope.edt.yearCode.substr(4);
                console.log("year_month", $scope.edt.year_month);
            }
            $scope.showPayrollDiv = function () {
                $scope.showPayrollDetails = true;
                $scope.showGratuityDetails = false;
            };
            $scope.showGratuityDiv = function () {
                $scope.showGratuityDetails = true;
                $scope.showPayrollDetails = false;
                //$scope.calculateGratuity();
            };

            $scope.getDetailsEmployeeToUpdate = function () {
                $scope.Updateemployee = false;
                $scope.To_Updateemployee = true;
                $scope.yearMonth = $scope.fin_year + $scope.edt.year_month;

                $http.get(ENV.apiUrl + "api/updatepaysheet/GetSalaryDetailsByEmployee?year_month=" + $scope.yearMonth + '&sd_number=' + $scope.edt.em_number).then(function (GetSalaryDetailsByEmployee) {
                    $scope.GetSalaryDetailsByEmployee = GetSalaryDetailsByEmployee.data;
                    console.log("GetSalaryDetailsByEmployee",$scope.GetSalaryDetailsByEmployee);
                    $scope.postSalaryDetails = angular.copy($scope.GetSalaryDetailsByEmployee);
                    $scope.totalItems = $scope.GetSalaryDetailsByEmployee.length;                                        
                    $scope.edt.total = 0;
                    $scope.edt.deductionAmt = 0;
                    $scope.edt.grossAmt = 0;
                    for (var j = 0; j < $scope.GetSalaryDetailsByEmployee.length; j++) {
                        if ($scope.GetSalaryDetailsByEmployee[j].pc_earn_dedn_tag == 'E') {
                            $scope.edt.total = $scope.edt.total + $scope.GetSalaryDetailsByEmployee[j].sd_amount;
                        }
                        if ($scope.GetSalaryDetailsByEmployee[j].pc_earn_dedn_tag == 'D') {
                            $scope.edt.deductionAmt = $scope.edt.deductionAmt + $scope.GetSalaryDetailsByEmployee[j].sd_amount;
                        }
                    }
                    $scope.To_Updateemployee = true;
                });


                $http.get(ENV.apiUrl + "api/updatepaysheet/GetComapnyCurrencyDetails").then(function (res) {
                    $scope.currencyDetails = res.data;
                    if ($scope.currencyDetails.length > 0) {
                        $scope.comp_curcy_dec = $scope.currencyDetails[0].comp_curcy_dec;
                    }
                    else {
                        $scope.comp_curcy_dec = 2;
                    }
                });

            }

            $scope.calculateGratuity = function () {
                var obj = {};
                //if (info.reg_date == undefined || info.reg_date == "" || info.reg_date == null) {
                //    obj.reg_date = $filter('date')(new Date(), 'dd-MM-yyyy');
                //}
                //else {
                //    obj.reg_date = $scope.empData.reg_date;
                //}
                obj.reg_date = $scope.empData.reg_date;
                obj.em_date_of_join = $scope.empData.em_date_of_join;
                obj.em_number = $scope.empData.em_login_code;
                obj.com_code = $scope.temp.company_code;
                obj.grt_updated_user = $rootScope.globals.currentUser.username;
                if ($http.defaults.headers.common['schoolId'] == 'sms') {
                    obj.opr = 'A';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'siso') {
                    obj.opr = 'B';
                }
                else {
                    obj.opr = 'C';
                }

                console.log(obj);

                $http.post(ENV.apiUrl + "api/common/MarkEmpResignation/calculateGratuity", obj).then(function (res) {
                    $scope.response = res.data;
                    if ($scope.response == true) {
                        $scope.getGratuity($scope.empData.em_login_code);
                    }
                    else {
                        swal({ title: "Alert", text: "Gratuity Calculation Failed", showCloseButton: true, width: 380, })
                    }
                    console.log($scope.response);
                });
            }

            $scope.getGratuity = function () {

                $http.get(ENV.apiUrl + "api/common/MarkEmpResignation/getAmt_ApplandWorkingDays?empl_code=" + $scope.empData.em_login_code).then(function (res1) {
                    $scope.amt_Appl_working_days = res1.data;
                    console.log("amt_Appl_working_days", $scope.amt_Appl_working_days);

                    if ($scope.amt_Appl_working_days.length > 0) {

                        $scope.allgratuityData = {
                            grt_amount: $scope.amt_Appl_working_days[0].grt_amount,
                            grt_applicable_days: $scope.amt_Appl_working_days[0].grt_applicable_days,
                            grt_working_days: $scope.amt_Appl_working_days[0].grt_working_days
                        }
                    }
                    else {
                        $scope.allgratuityData = {};
                    }
                });
            }

            $scope.searchGlobalClick = function () {
                $scope.Global_Search_by_employee();
            }

            $scope.emp_modal_cancel = function () {
                debugger
                $('#myModal').modal('hide');
                $scope.edt.em_login_code = $scope.SelectedUserLst[0].em_login_code;
                $scope.edt.em_number = $scope.SelectedUserLst[0].em_number;
                $scope.temp = {};
                $scope.temp.company_code = $scope.cmbCompany[0].sh_company_code;
                $scope.NodataEmployee = false;
                $scope.getEmpReg($scope.edt.em_number);
                //$scope.getPayrollGeneratedMonth($scope.edt.em_number);
               // $scope.getGetSalaryDetails($scope.edt.em_number);
            }

            $scope.globalSearchCustom = function () {
                $scope.temp = {};
                $scope.NodataEmployee = false;
                $scope.showEmployeeFilterBtn = true;
                $scope.EmployeeTable = false;
                $scope.global_search_result = [];
                $('#myModal').modal('show');
                $scope.temp.company_code = $scope.cmbCompany[0].sh_company_code;
            }

            $scope.Search_by_employee = function () {;
                $scope.NodataEmployee = false;
                var obj = {};
                obj.em_login_code = $scope.temp.em_login_code;
                obj.EmpName = $scope.temp.EmpName;
                obj.em_department = $scope.temp.em_department;
                obj.em_desg_code = $scope.temp.em_desg_code;
                obj.company_code = $scope.temp.company_code;
                obj.resignedEmpFlag = "Yes";

                $http.post(ENV.apiUrl + "api/EmpReRegistration/SearchResignedEmployee?data=" + JSON.stringify(obj)).then(function (SearchEmployee_Data) {
                    $scope.global_search_result = SearchEmployee_Data.data;
                    $scope.EmployeeTable = true;
                    $scope.showEmployeeFilterBtn = true;
                    $scope.showEmployeeFilter = true;
                    $scope.currentPage = 0;

                    if (SearchEmployee_Data.data.length > 0) {
                        //   $scope.EmployeeTable = true;
                    }
                    else {
                        $scope.global_search_result = [];
                        swal({ title: 'Alert', text: "No record(s) found.", showCloseButton: true, width: 450, height: 200, timer: 1000 });
                        $scope.NodataEmployee = true;
                        //  $scope.EmployeeTable = false;
                    }
                    $timeout(function () {
                        $("#fixTable1").tableHeadFixer({ 'top': 1 });
                    }, 100);
                    $scope.getPerPageRecords();
                });
            }

            $timeout(function () {
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);


            $scope.getEmpReg = function (emp_login_code) {
                $scope.reset();

                $http.get(ENV.apiUrl + "api/EmpReRegistration/getEmployeDetails?login_code=" + emp_login_code).then(function (res) {

                    if (res.data.length > 0) {
                        debugger
                        $scope.Save_btn = true;
                        $scope.showTable = true;

                        $scope.empData = res.data[0];

                        $scope.temp = {
                            em_login_code: $scope.empData.em_login_code,
                            em_full_name: $scope.empData.em_full_name,
                            em_date_of_join: $scope.empData.em_date_of_join,
                            dg_desc: $scope.empData.dg_desc,
                            com_name: $scope.empData.com_name,
                            dep_name: $scope.empData.dep_name,
                            reg_date: $scope.empData.reg_date,
                            reg_resion: $scope.empData.reg_resion,
                            em_status: $scope.empData.em_status
                        }
                        console.log($scope.temp);
                        $scope.edt.em_number = $scope.empData.em_number;
                        $scope.getGetSalaryDetails($scope.edt.em_number);
                    }
                    else {
                        swal({ title: "Alert", text: "Please search only resigned employee", width: 300, height: 200 });
                    }
                });
                //$scope.getPayrollGeneratedMonth(emp_login_code);                
                //$scope.edt.em_number = $scope.empData.em_number;
                
            }

            $scope.SaveData = function () {
                if ($scope.temp.activation_date == undefined || $scope.temp.activation_date == "" || $scope.temp.activation_date == null) {
                    swal({ title: "Alert", text: "Please select activation date", width: 300, height: 200 });
                    return;
                }
                console.log("temp", $scope.temp);

                $http.post(ENV.apiUrl + "api/EmpReRegistration/ReregisterEmployee", $scope.temp).then(function (res) {
                    $scope.msg1 = res.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Employee Re-Register Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380 });
                        $scope.cancel_reg();
                    }
                    else {
                        swal({ title: "Alert", text: "Server Error" + $scope.msg1, showCloseButton: true, width: 380 });
                    }
                });
            }

            $scope.cancel_reg = function () {
                $scope.edt = {};
                $scope.temp = {};
                $scope.Save_btn = false;
                $scope.showTable = false;
                $scope.temp.company_code = $scope.cmbCompany[0].sh_company_code;
            }


        }])


})();