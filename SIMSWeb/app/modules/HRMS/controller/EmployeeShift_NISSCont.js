﻿(function () {
    'use strict';
    var del = [], shiftdetails = [];
    var main;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeShift_NISSCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.display = true;
            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.grid = true;
            $scope.shifttemplate_data = [];
            var data2 = [];
            $scope.div_submit = true;
            $scope.gridNew = false;
            $scope.div_shift = false;
            $scope.BUSY = false;
            $scope.loaderShow = false;
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');

            $http.get(ENV.apiUrl + "api/common/ShiftMaster/getCompany").then(function (res) {
                $scope.comp_data = res.data;
                console.log($scope.comp_data[0].sh_company_code);
                $scope.edt['sh_company_code'] = $scope.comp_data[0].sh_company_code;api/common/EmployeeShift
                $scope.getdepartment($scope.comp_data[0].sh_company_code);
            });

            $timeout(function ()
            {
                $("#fixedtable,#fixedtable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.edt = {
                sh_start_date: $scope.ddMMyyyy,
                sh_end_date: $scope.ddMMyyyy,
            }

            $scope.getdepartment = function (comp_code)
            {
                $http.get(ENV.apiUrl + "api/common/EmployeeShiftNISS/GetDepartmentName?comp_code=" + comp_code).then(function (res) {
                    $scope.dept_data = res.data;
                });
                $http.get(ENV.apiUrl + "api/common/EmployeeShiftNISS/GetDesignationName?comp_code=" + comp_code).then(function (res) {
                    $scope.desg_data = res.data;
                });
            }

            $scope.getEmpTempHistory = function (emp_id)
            {
                $scope.emptemp_data = [];
                $('#myModal1').modal({ backdrop: 'static', keyboard: true });
                $http.get(ENV.apiUrl + "api/common/EmployeeShiftNISS/Get_EmpTemplateDetails?emp_id=" + emp_id).then(function (res) {
                    $scope.emptemp_data = res.data;
                });
            }

            $scope.btn_display = function ()
            {
                $scope.div_shift = true;
                $scope.div_submit = false;
                if ($scope.edt.sh_company_code != undefined)
                {
                    $http.get(ENV.apiUrl + "api/common/ShiftTemplate/getAllTemplateName?company_code=" + $scope.edt.sh_company_code).then(function (res) {
                        $scope.AllTemplateName = res.data;
                    });
                }

                if ($scope.edt.code != null || $scope.edt.code != "")
                {
                    $http.get(ENV.apiUrl + "api/common/EmployeeShiftNISS/GetAllEmployeeName?comp_code=" + $scope.edt.sh_company_code + "&dept_code=" + $scope.edt.code + "&desg_code=" + $scope.edt.em_desg_code).then(function (res) {
                        $scope.AllEmployeeName_data = res.data;
                        console.log($scope.AllEmployeeName_data);
                    });
                }
            }

            $scope.getTempaltedetails = function (template_id)
            {
                if (template_id != null || template_id != "")
                {
                    $scope.gridNew = true;
                    $http.get(ENV.apiUrl + "api/common/ShiftTemplate/GetTemplateDetails?company_code=" + $scope.edt.sh_company_code + "&template_id=" + template_id).then(function (res) {
                        $scope.shifttemplate_data = res.data;
                        console.log($scope.shifttemplate_data);
                    });
                }
            }

            $scope.CheckAllChecked = function ()
            {
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.AllEmployeeName_data.length; i++) {
                    if (main.checked == true) {

                        $scope.AllEmployeeName_data[i].sh_template_id1 = true;

                        $('tr').addClass("row_selected");
                    }

                    else {
                        $scope.AllEmployeeName_data[i].sh_template_id1 = false;
                        $('tr').removeClass("row_selected");
                        main.checked = false;
                    }
                }


            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.btn_SubmitNew = function ()
            {
                var data = [];
                var data2 = [];
                $scope.insert = false;
                $scope.BUSY = true;
                shiftdetails = [];

                if ($scope.edt.sh_template_id != undefined)
                {
                    for (var i = 0; i < $scope.AllEmployeeName_data.length; i++)
                    {
                        //if ($scope.AllEmployeeName_data[i].sh_template_id1 == true)
                        //var v = document.getElementById(i);

                        // if (v.checked == true)
                        if ($scope.AllEmployeeName_data[i].sh_template_id1 == true)
                        {
                            shiftdetails = shiftdetails + $scope.AllEmployeeName_data[i].em_number + ',';
                            $scope.insert = true;

                        }
                    }

                    if ($scope.insert)
                    {
                        for (var j = 0; j < $scope.shifttemplate_data.length; j++)
                        {
                            var data =
                                {
                                    em_number: shiftdetails,
                                    sh_template_id: $scope.edt.sh_template_id,
                                    sh_start_date: $scope.edt.sh_start_date,
                                    em_company_code: $scope.edt.sh_company_code,
                                    em_dept_code: $scope.edt.code,
                                    sh_day_status: $scope.shifttemplate_data[j].sh_day_status,
                                    sh_shift_day1: $scope.shifttemplate_data[j].sh_shift_day1,
                                    sh_shift_day2: $scope.shifttemplate_data[j].sh_shift_day2,
                                    sh_shift_day3: $scope.shifttemplate_data[j].sh_shift_day3,
                                    sh_shift_day4: $scope.shifttemplate_data[j].sh_shift_day4,
                                    sh_shift_day5: $scope.shifttemplate_data[j].sh_shift_day5,
                                    sh_shift_day6: $scope.shifttemplate_data[j].sh_shift_day6,
                                    sh_shift_day7: $scope.shifttemplate_data[j].sh_shift_day7,
                                    sh_shift_status: $scope.shifttemplate_data[j].sh_shift_status,
                                    sh_template_name: $scope.shifttemplate_data[j].sh_template_name,
                                    sh_template_status: $scope.shifttemplate_data[j].sh_template_status,
                                    sh_end_date: $scope.edt.sh_end_date,
                                };
                            data2.push(data);
                        }

                        $scope.loaderShow = true;
                        $http.post(ENV.apiUrl + "api/common/EmployeeShiftNISS/CUDInsertEmployeeShift", data2).then(function (res) {
                            $scope.shift_rec = res.data;
                            console.log($scope.shift_rec);

                            if ($scope.shift_rec == "1")
                            {
                                swal({ text: "Record Saved Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm)
                                    {
                                        $scope.loaderShow = false;
                                        $scope.BUSY = false;
                                        $scope.btn_display();
                                        $scope.btn_cancel();
                                    }
                                });
                            }

                            if ($scope.shift_rec == "2")
                            {
                                swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.loaderShow = false;
                                        $scope.BUSY = false;
                                        $scope.btn_display();
                                        $scope.btn_cancel();
                                    }
                                });
                            }
                            if ($scope.shift_rec == "3")
                            {
                                swal({ title: "Alert", text: "Applicable from Date is not in Financial Period.", showCloseButton: true, width: 380, });
                                $scope.BUSY = false;
                                $scope.loaderShow = false;
                            }
                            if ($scope.shift_rec == "4")
                            {
                                swal({ title: "Alert", text: "Applicable from Date is Greater than Financial Period", showCloseButton: true, width: 380, });
                                $scope.loaderShow = false;
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                        $scope.BUSY = false;
                        $scope.loaderShow = false;
                    }
                }
                else {
                    swal({ title: "Alert", text: "Please Select Shift Template", showCloseButton: true, width: 380, })
                    $scope.BUSY = false;
                    $scope.loaderShow = false;
                }
            }

            $scope.btn_cancel = function () {
                $scope.loaderShow = false;
                $scope.BUSY = false;
                $scope.edt = [];
                main.checked = false;
                $scope.div_shift = false;
                $scope.div_submit = true;
                $scope.gridNew = false;
                $scope.AllEmployeeName_data = [];
                $scope.edt =
                {
                    sh_start_date: $scope.ddMMyyyy,
                }

                $http.get(ENV.apiUrl + "api/common/ShiftMaster/getCompany").then(function (res) {
                    $scope.comp_data = res.data;
                    console.log($scope.comp_data[0].sh_company_code);
                    $scope.edt['sh_company_code'] = $scope.comp_data[0].sh_company_code;
                    $scope.getdepartment($scope.comp_data[0].sh_company_code);
                });

            }

            $scope.getcompareShiftdate = function ()
            {
                if (moment($scope.edt.sh_end_date, "DD-MM-YYYY").format('YYYY-MM-DD') < moment($scope.edt.sh_start_date, "DD-MM-YYYY").format('YYYY-MM-DD'))
                {
                    swal({ title: "Alert", text: "End Date is Less than Start Date.", showCloseButton: true, width: 380, });
                    $scope.edt.sh_end_date = undefined;
                   // $scope.edt.sh_start_date = undefined;
                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }])
})();