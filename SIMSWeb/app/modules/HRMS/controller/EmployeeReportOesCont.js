﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, modulecode = [];
    var simsController = angular.module('sims.module.HRMS');
    simsController.controller('EmployeeReportOesCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.arry = [];

            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.exportData = function () {
                alasql('SELECT * INTO XLSX("Report.xlsx",{headers:true}) \ FROM HTML("#customers",{headers:true,skipdisplaynone:true})');
            };

            $scope.items = [{
                "Name": "ANC101",
                "Date": "10/02/2014",
                "Terms": ["samsung", "nokia", "apple"],
                "temp": "ANC101"

            }, {
                "Name": "ABC102",
                "Date": "10/02/2014",
                "Terms": ["motrolla", "nokia", "iPhone"],
                "temp": "ANC102"
            }]

            $scope.header = ['name', 'dob']

            $scope.data = [
                {
                    name: 'xyz',
                    dob: '10/02/2014'
                },
                {
                    name: 'abc',
                    dob: '10/02/2014'
                }
            ]

            $scope.busyindicator = false;

            //$http.get(ENV.apiUrl + "api/EmployeeReport/GetCompanyNameForShift").then(function (res) {
            //    $scope.cmbCompany = res.data;
            //    debugger;
            //    $scope.companyChange_Designation($scope.cmbCompany.sh_company_code);
            //})
            $http.get(ENV.apiUrl + "api/finance/getComCode").then(function (res) {
                $scope.compcode = res.data;
                debugger;
                $scope.edt = { gdua_comp_code: $scope.compcode[0].gdua_comp_code }
                $scope.companyChange_Designation($scope.edt.gdua_comp_code);
            })
            //$http.get(ENV.apiUrl + "api/EmployeeReport/GetDepartmentName").then(function (res) {
            //    $scope.cmbDepartment = res.data;
            //    setTimeout(function () {
            //        $('#cmb_deprtment').change(function () {
            //            console.log($(this).val());
            //        }).multipleSelect({
            //            width: '100%'
            //        });
            //    }, 1000);
            //})

            //$(function () {
            //    $('#cmb_deprtment').multipleSelect({
            //        width: '100%'
            //    });
            //});
            $http.get(ENV.apiUrl + "api/EmployeeReport/GetDepartmentName").then(function (res) {
                $scope.cmbDepartment = res.data;
                setTimeout(function () {
                    $('#cmb_academic_year').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            })
            $(function () {
                $('#cmb_academic_year').multipleSelect({
                    width: '100%'
                });
            });
            //$scope.companyChange = function (str) {
            //    $http.get(ENV.apiUrl + "api/EmployeeReport/GetDesignation?com_code=" + str).then(function (res) {
            //        $scope.cmbDesignation = res.data;
            //        setTimeout(function () {
            //            $('#cmb_designation').change(function () {
            //                console.log($(this).val());
            //            }).multipleSelect({
            //                width: '100%'
            //            });
            //        }, 1000);
            //    })
            //}

            //$(function () {
            //    $('#cmb_designation').multipleSelect({
            //        width: '100%'
            //    });
            //});
            $scope.companyChange_Designation = function (str) {
                $http.get(ENV.apiUrl + "api/EmployeeReport/GetDesignation?com_code=" + str).then(function (res) {
                    $scope.cmbDesignation = res.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                })
            }
            $(function () {
                $('#cmb_grade_code').multipleSelect({
                    width: '100%'
                });
            });

            //$http.get(ENV.apiUrl + "api/EmployeeReport/GetServiceStatus").then(function (dres) {
            //    $scope.empservice = dres.data;
            //    setTimeout(function () {
            //        $('#cmd_service_status').change(function () {
            //            console.log($(this).val());
            //        }).multipleSelect({
            //            width: '100%'
            //        });
            //    }, 1000);
            //})

            //$(function () {
            //    $('#cmd_service_status').multipleSelect({
            //        width: '100%'
            //    });
            //});
            $http.get(ENV.apiUrl + "api/EmployeeReport/GetServiceStatus").then(function (dres) {
                $scope.empservice = dres.data;
                setTimeout(function () {
                    $('#Select1').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            })

            $(function () {
                $('#Select1').multipleSelect({
                    width: '100%'
                });
            });
            $scope.Show_Data = function () {
                $scope.table = true;
                $scope.busyindicator = true;
                debugger;                
                //$http.get(ENV.apiUrl + "api/EmployeeReport/GetEmployeeReportOes?comp=" + $scope.edt.gdua_comp_code + "&desg=" + $scope.code + "&dept=" + $scope.dept_no + "&sstatus=" + $scope.sesi_code).then(function (res) {
                $http.get(ENV.apiUrl + "api/EmployeeReport/GetEmployeeReportOes?comp=" + $scope.edt.gdua_comp_code + "&desg=" + $scope.code + "&dept=" + $scope.dept_no + "&sstatus=" + $scope.sesi_code + "&empID=" + $scope.txt_employee_id).then(function (res) {
                    $scope.empList = res.data;
                });

            }

            $scope.back = function () {
                $scope.table = false;
                $scope.busyindicator = false;
            }


            $scope.checkallClick = function (str) {
                debugger;
                if (str) {
                    $scope.chk_em_nationalities = true;
                    $scope.chk_em_company_name = true;
                    $scope.chk_em_qualification = true;
                    $scope.chk_em_Qualification_new = true;
                    $scope.chk_em_Qualification_Arabic = true;
                    $scope.chk_em_Accommodation_Status = true;
                    $scope.chk_em_dept_name = true;
                    $scope.chk_em_desg_desc = true;
                    $scope.chk_em_visa_issue_date = true;
                    $scope.chk_em_login_code = true;
                    $scope.chk_em_dept_effect_from = true;
                    $scope.chk_em_dest_desc = true;
                    $scope.chk_em_grade_name = true;
                    $scope.chk_em_number = true;
                    $scope.chk_em_visa_exp_date = true;
                    $scope.chk_em_grade_effect_from = true;
                    $scope.chk_em_staff_type = true;
                    $scope.chk_em_full_name = true;
                    $scope.chk_em_apartment_number = true;
                    $scope.chk_em_visa_exp_rem_date = true;
                    $scope.chk_em_family_name = true;
                    $scope.chk_em_name_ot = true;
                    $scope.chk_em_date_of_birth = true;
                    $scope.chk_em_building_number = true;
                    $scope.chk_em_visa_issuing_place = true;
                    $scope.chk_em_sex = true;
                    $scope.chk_em_marital_status = true;
                    $scope.chk_em_religion_name = true;
                    $scope.chk_em_street_number = true;
                    $scope.chk_em_visa_issuing_authority = true;
                    $scope.chk_em_etdnicity_name = true;
                    $scope.chk_em_summary_address = true;
                    $scope.chk_em_city = true;
                    $scope.chk_em_area_number = true;
                    $scope.chk_em_visa_type = true;
                    $scope.chk_em_state = true;
                    $scope.chk_em_country = true;
                    $scope.chk_em_phone = true;
                    $scope.chk_em_summary_address_local_language = true;
                    $scope.chk_em_mobile = true;
                    $scope.chk_em_email = true;
                    $scope.chk_em_Emp_Person_Email = true;
                    $scope.chk_em_Office_Email = true;
                    $scope.chk_em_fax = true;
                    $scope.chk_em_po_fax = true;
                    $scope.chk_em_dependant_full = true;
                    $scope.chk_em_dependant_half = true;
                    $scope.chk_em_emergency_contact_name1 = true;
                    $scope.chk_em_emergency_contact_name2 = true;
                    $scope.chk_em_emergency_contact_number1 = true;
                    $scope.chk_em_emergency_contact_number2 = true;
                    $scope.chk_em_date_of_join = true;
                    $scope.chk_em_service_status = true;
                    $scope.chk_em_ledger_ac_no = true;
                    $scope.chk_em_gpf_ac_no = true;
                    $scope.chk_em_gosi_ac_no = true;
                    $scope.chk_em_gosi_start_date = true;
                    $scope.chk_em_pan_no = true;
                    $scope.chk_em_iban_no = true;
                    $scope.chk_em_labour_card_no = true;
                    $scope.chk_em_status = true;
                    $scope.chk_em_leave_resume_date = true;
                    $scope.chk_em_joining_ref = true;
                    $scope.chk_em_bank_code = true;
                    $scope.chk_em_bank_ac_no = true;
                    $scope.chk_em_bank_swift_code = true;
                    $scope.chk_em_leave_start_date = true;
                    $scope.chk_em_leave_end_date = true;
                    $scope.chk_em_stop_salary_from = true;
                    $scope.chk_em_unpaid_leave = true;
                    $scope.chk_em_punching_id = true;
                    $scope.chk_em_passport_no = true;
                    $scope.chk_em_ethnicity_name = true;
                    $scope.chk_em_passport_issue_date = true;
                    $scope.chk_em_pssport_exp_rem_date = true;
                    $scope.chk_em_passport_exp_date = true;
                    $scope.chk_em_passport_ret_date = true;
                    $scope.chk_em_passport_issuing_authority = true;
                    $scope.chk_em_passport_issue_place = true;
                    $scope.chk_em_national_id = true;
                    $scope.chk_em_national_id_issue_date = true;
                    $scope.chk_em_national_id_expiry_date = true;
                    $scope.chk_em_modified_on = true;
                    $scope.chk_em_Nation_Code = true;
                    $scope.chk_em_pwd = true;
                    $scope.chk_em_secret_question_code = true;
                    $scope.chk_em_secret_answer = true;
                    $scope.chk_em_agreement_start_date = true;
                    $scope.chk_em_agreement_exp_date = true;
                    $scope.chk_em_agreemet_exp_rem_date = true;
                    $scope.chk_em_dependant_infant = true;
                    //$scope.chk_em_sponser_name = true;
                    //$scope.chk_em_relation = true;
                    //$scope.chk_em_occupation = true;
                    //$scope.chk_em_workplace = true;
                    //$scope.chk_em_office_phone = true;
                    //$scope.chk_em_mobile1 = true;
                    //$scope.chk_visa_num = true;
                    //$scope.chk_visa_issue_date = true;
                    //$scope.chk_visa_exp_date = true;
                    //$scope.chk_em_attr1 = true;
                    //$scope.chk_em_attr2 = true;
                    //$scope.chk_em_attr3 = true;
                    //$scope.chk_em_attr4 = true;
                    //$scope.chk_em_attr5 = true;
                   
                    //$scope.chk_em_national_id1 = true;
                    //$scope.chk_em_accommodation = true;
                    //$scope.chk_em_sponsership = true;
                    //$scope.chk_em_visa_Type = true;
                    $scope.chk_em_status1 = true;
                    $scope.chk_em_stat = true;
                    $scope.chk_em_expirydate = true;
                    $scope.chk_em_Sponsor_ID = true;
                    $scope.chk_em_Sponsor_Name_In_English = true;
                    $scope.chk_em_Sponsor_Name_In_Arabic = true;
                    $scope.chk_em_Sponsor_Mobile_No = true;
                    $scope.chk_em_Sponsor_Address_In_English = true;
                    $scope.chk_em_Sponsor_Address_In_Arabic = true;
                    $scope.chk_em_Sponsor_Work_Place_In_English = true;
                    $scope.chk_em_Sponsor_Work_Place_In_Arabic = true;

                }

                else {
                    $scope.chk_em_nationalities = false;
                    $scope.chk_em_company_name = false;
                    $scope.chk_em_qualification = false;
                    $scope.chk_em_Qualification_new = false;
                    $scope.chk_em_Qualification_Arabic = false;
                    $scope.chk_em_Accommodation_Status = false;
                    $scope.chk_em_dept_name = false;
                    $scope.chk_em_desg_desc = false;
                    $scope.chk_em_visa_issue_date = false;
                    $scope.chk_em_login_code = false;
                    $scope.chk_em_dept_effect_from = false;
                    $scope.chk_em_dest_desc = false;
                    $scope.chk_em_grade_name = false;
                    $scope.chk_em_number = false;
                    $scope.chk_em_visa_exp_date = false;
                    $scope.chk_em_grade_effect_from = false;
                    $scope.chk_em_staff_type = false;
                    $scope.chk_em_full_name = false;
                    $scope.chk_em_apartment_number = false;
                    $scope.chk_em_visa_exp_rem_date = false;
                    $scope.chk_em_family_name = false;
                    $scope.chk_em_name_ot = false;
                    $scope.chk_em_ethnicity_name = false;
                    $scope.chk_em_date_of_birth = false;
                    $scope.chk_em_building_number = false;
                    $scope.chk_em_visa_issuing_place = false;
                    $scope.chk_em_sex = false;
                    $scope.chk_em_marital_status = false;
                    $scope.chk_em_religion_name = false;
                    $scope.chk_em_street_number = false;
                    $scope.chk_em_visa_issuing_authority = false;
                    $scope.chk_em_etdnicity_name = false;
                    $scope.chk_em_summary_address = false;
                    $scope.chk_em_city = false;
                    $scope.chk_em_area_number = false;
                    $scope.chk_em_visa_type = false;
                    $scope.chk_em_state = false;
                    $scope.chk_em_country = false;
                    $scope.chk_em_phone = false;
                    $scope.chk_em_summary_address_local_language = false;
                    $scope.chk_em_mobile = false;
                    $scope.chk_em_email = false;
                    $scope.chk_em_Emp_Person_Email = false;
                    $scope.chk_em_Office_Email = false;
                    $scope.chk_em_fax = false;
                    $scope.chk_em_po_fax = false;
                    $scope.chk_em_dependant_infant = false;
                    $scope.chk_em_dependant_full = false;
                    $scope.chk_em_dependant_half = false;
                    $scope.chk_em_emergency_contact_name1 = false;
                    $scope.chk_em_emergency_contact_name2 = false;
                    $scope.chk_em_emergency_contact_number1 = false;
                    $scope.chk_em_emergency_contact_number2 = false;
                    $scope.chk_em_date_of_join = false;
                    $scope.chk_em_service_status = false;
                    $scope.chk_em_ledger_ac_no = false;
                    $scope.chk_em_gpf_ac_no = false;
                    $scope.chk_em_gosi_ac_no = false;
                    $scope.chk_em_gosi_start_date = false;
                    $scope.chk_em_pan_no = false;
                    $scope.chk_em_iban_no = false;

                    $scope.chk_em_labour_card_no = false;
                    $scope.chk_em_status = false;
                    $scope.chk_em_leave_resume_date = false;
                    $scope.chk_em_joining_ref = false;
                    $scope.chk_em_bank_code = false;
                    $scope.chk_em_bank_ac_no = false;
                    $scope.chk_em_bank_swift_code = false;
                    $scope.chk_em_leave_start_date = false;
                    $scope.chk_em_leave_end_date = false;
                    $scope.chk_em_stop_salary_from = false;
                    $scope.chk_em_unpaid_leave = false;
                    $scope.chk_em_punching_id = false;
                    $scope.chk_em_passport_no = false;
                    $scope.chk_em_passport_issuing_authority = false;

                    $scope.chk_em_passport_issue_date = false;
                    $scope.chk_em_pssport_exp_rem_date = false;
                    $scope.chk_em_passport_exp_date = false;
                    $scope.chk_em_passport_ret_date = false;
                    $scope.chk_em_passport_issuing_authority = false;
                    $scope.chk_em_passport_issue_place = false;
                    $scope.chk_em_national_id = false;

                    $scope.chk_em_national_id_issue_date = false;
                    $scope.chk_em_national_id_expiry_date = false;
                    $scope.chk_em_modified_on = false;
                    $scope.chk_em_Nation_Code = false;
                    $scope.chk_em_pwd = false;
                    $scope.chk_em_secret_question_code = false;
                    $scope.chk_em_secret_answer = false;
                    $scope.chk_em_agreement_start_date = false;
                    $scope.chk_em_agreement_exp_date = false;
                    $scope.chk_em_agreemet_exp_rem_date = false;

                    //$scope.chk_em_sponser_name = false;
                    //$scope.chk_em_relation = false;
                    //$scope.chk_em_occupation = false;
                    //$scope.chk_em_workplace = false;
                    //$scope.chk_em_office_phone = false;
                    //$scope.chk_em_mobile1 = false;
                    //$scope.chk_visa_num = false;
                    //$scope.chk_visa_issue_date = false;
                    //$scope.chk_visa_exp_date = false;
                    //$scope.chk_em_attr1 = false;
                    //$scope.chk_em_attr2 = false;
                    //$scope.chk_em_attr3 = false;
                    //$scope.chk_em_attr4 = false;
                    //$scope.chk_em_attr5 = false;
                    
                    //$scope.chk_em_national_id1 = false;
                    //$scope.chk_em_accommodation = false;
                    //$scope.chk_em_sponsership = false;
                    //$scope.chk_em_visa_Type = false;
                    $scope.chk_em_status1 = false;
                    $scope.chk_em_stat = false;
                    $scope.chk_em_expirydate = false;
                    $scope.chk_em_Sponsor_ID = false;
                    $scope.chk_em_Sponsor_Name_In_English = false;
                    $scope.chk_em_Sponsor_Name_In_Arabic = false;
                    $scope.chk_em_Sponsor_Mobile_No = false;
                    $scope.chk_em_Sponsor_Address_In_English = false;
                    $scope.chk_em_Sponsor_Address_In_Arabic = false;
                    $scope.chk_em_Sponsor_Work_Place_In_English = false;
                    $scope.chk_em_Sponsor_Work_Place_In_Arabic = false;

                }
            }

        }])
})();
