﻿
(function () {
    'use strict';

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CompanyMasterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.grid = true;
            $scope.btn_save = true;
            var main = '';
            var opr = 'opr';
            $http.get(ENV.apiUrl + "api/companyhrms/GetCurrency_Name").then(function (GetCurrency_Name) {
                $scope.GetCurrency_Name = GetCurrency_Name.data;
            });

            $http.get(ENV.apiUrl + "api/companyhrms/GetAllFormula_Description").then(function (GetAllFormula_Description) {
                $scope.All_Formula_Description = GetAllFormula_Description.data; 4
            });

            $http.get(ENV.apiUrl + "api/companyhrms/GetAll_Week_Day").then(function (GetAll_Week_Day) {
                $scope.All_Week_Day = GetAll_Week_Day.data;
            });

            $http.get(ENV.apiUrl + "api/companyhrms/GetAllsettlement_code").then(function (GetAllsettlement_code) {
                $scope.Allsettlement_code = GetAllsettlement_code.data;
            });

            $http.get(ENV.apiUrl + "api/companyhrms/GetAllCompany_Name").then(function (GetAllCompany_Name) {
                $scope.AllCompany_Name = GetAllCompany_Name.data;
            });

            $http.get(ENV.apiUrl + "api/companyhrms/GetAll_Company").then(function (GetAll_Company) {
                $scope.All_Company = GetAll_Company.data;

                

                $scope.totalItems = $scope.All_Company.length;
                $scope.todos = $scope.All_Company;
                $scope.makeTodos();
                
            });

            $scope.GetallCompanyInfo = function (str) {
                for (var i = 0; i < $scope.AllCompany_Name.length; i++) {

                    if ($scope.AllCompany_Name[i].company_code == str) {
                        $scope.edt = {
                            company_desc: $scope.AllCompany_Name[i].ca_company_code_name,
                            company_code: $scope.AllCompany_Name[i].company_code,
                            excg_curcy_code: $scope.AllCompany_Name[i].excg_curcy_code,
                            company_short_name: $scope.AllCompany_Name[i].company_short_name,
                        };
                    }
                }
                $http.get(ENV.apiUrl + "api/companyhrms/GetCurrency_Name?code=" + $scope.edt.excg_curcy_code).then(function (GetCurrency_Name) {
                    $scope.Currency_Name = GetCurrency_Name.data;
                });
            }
            
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

             
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
               
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;  $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();
            }

            $scope.Reset = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
            }

            $scope.btn_cancel_Click = function () {
                $scope.grid = true;
                $scope.display = false;

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            var data = [];

            $scope.btn_Save_Click = function (isvalid) {
                var exist = false;
                $scope.save = true;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.edt.company_code == $scope.filteredTodos[i].company_code) {
                        exist = true;
                    }
                }
                if (isvalid) {
                    if (exist != true) {
                        $scope.edt[opr] = "I";
                        data.push($scope.edt);
                        $http.post(ENV.apiUrl + "api/companyhrms/Insert_Company", data).then(function (msg) {
                            $scope.grid = true;
                            $scope.msg1 = msg.data;
                            data = [];
                            if ($scope.msg1) {
                                $http.get(ENV.apiUrl + "api/companyhrms/GetAll_Company").then(function (GetAll_Company) {
                                    $scope.All_Company = GetAll_Company.data;
                                    $scope.totalItems = $scope.All_Company.length;
                                    $scope.todos = $scope.All_Company;
                                    $scope.makeTodos();
                                });
                                swal('', 'Company Created Successfully');
                            }
                            else {
                                swal('', 'Error In Company Created. ' + $scope.msg1);

                            }
                        })
                    }
                    else {
                        $scope.grid = true;
                        swal('', 'Company Already Exist');
                    }
                }
            }

            var deletedata = [];

            $scope.btn_Delete_Click = function () {
              
                $scope.save = true;
                $scope.edt = "";
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = document.getElementById($scope.filteredTodos[i].company_code);
                    if (t.checked == true) {
                        $scope.filteredTodos[i].opr = "D";
                        deletedata.push($scope.filteredTodos[i]);
                    }
                }
            
              
                $http.post(ENV.apiUrl + "api/companyhrms/Insert_Company",deletedata).then(function (msg) {
                    $scope.grid = true;
                    $scope.msg1 = msg.data;
                    deletedata = [];
                    if ($scope.msg1) {


                        $http.get(ENV.apiUrl + "api/companyhrms/GetAll_Company").then(function (GetAll_Company) {
                            $scope.All_Company = GetAll_Company.data;
                            $scope.totalItems = $scope.All_Company.length;
                            $scope.todos = $scope.All_Company;
                            $scope.makeTodos();
                        });
                        swal('', 'Company Deleted Successfully');
                    }
                    else {
                        swal('', 'Error In Deleted Record. ' + $scope.msg1);
                    }
                })

            }

            $scope.table_row_Click = function (str) {
                $scope.btn_save = false;
                $scope.grid = false;
                $scope.edt = str;
               
                $http.get(ENV.apiUrl + "api/companyhrms/GetCurrency_Name?code=" + $scope.edt.excg_curcy_code).then(function (GetCurrency_Name) {
                    $scope.Currency_Name = GetCurrency_Name.data;
                });
            }

            $scope.btn_Update_Click = function () {
                
                $scope.edt[opr] = "U";
                data.push($scope.edt);
                $http.post(ENV.apiUrl + "api/companyhrms/Insert_Company", data).then(function (msg) {
                    $scope.grid = true;
                    
                    $scope.msg1 = msg.data;
                    data = [];
                    if ($scope.msg1) {


                        $http.get(ENV.apiUrl + "api/companyhrms/GetAll_Company").then(function (GetAll_Company) {
                            $scope.All_Company = GetAll_Company.data;
                            $scope.totalItems = $scope.All_Company.length;
                            $scope.todos = $scope.All_Company;
                            $scope.makeTodos();
                        });
                        swal('', 'Company Updated Successfully');
                    }
                    else {
                        swal('', 'Error In Updated Record. ' + $scope.msg1);
                    }
                })
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.codp_dept_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.glma_acct_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.btn_New_Click = function () {
                $scope.grid = false;
                $scope.btn_save = true;
                $scope.edt = "";
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.CheckMultiple = function () {
               
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = document.getElementById($scope.filteredTodos[i].company_code);
                    if (main.checked == true) {
                        t.checked = true;
                    }
                    else {
                        main.checked = false;
                        t.checked = false;
                    }
                }
            }

            $scope.OneByOneCheck=function()
            {
               
                main = document.getElementById('mainchk');
                if (main.checked == true)
                {
                    main.checked = false;
                }
            
            }

            var dom;
            $scope.expand = function (info, $event) {
               
                $(dom).remove();
                dom = $("<tr><td class='details' colspan='12'>" +
                "<table1 class='inner-table1' cellpadding='5' cellspacing='0'>" +
                "<tbody>" +
                "<tr style='text-align:center'><td class='semi-bold' colspan='2'>" + "Currency Tag M" + "</td> <td class='semi-bold'>" + "Cut Off Day" + " </td><td class='semi-bold'>" + "Holiday OverTime Formula" + "</td><td class='semi-bold'>" + "Holiday OverTime Rate" + "</td> <td class='semi-bold'>" + "Normal Hours" + " </td><td class='semi-bold'>" + "Normal Other Hrs" + "</td><td class='semi-bold' colspan='3'>" + "Normal OverTime rate" + " </td></tr>" +
                "<tr style='text-align:center'><td colspan='2'>" + (info.comp_currency_tag_M) + "</td> <td>" + (info.comp_cut_off_day) + " </td><td>" + (info.comp_hot_formula_desc) + "</td><td>" + (info.comp_hot_rate) + "</td> <td>" + (info.comp_normal_hrs) + " </td><td>" + (info.comp_normal_ot_hrs) + "</td><td colspan='3'>" + (info.comp_not_rate) + " </td></tr>" +
                 "<tr style='text-align:center'><td class='semi-bold' colspan='2'>" + "Short Formula" + "</td> <td class='semi-bold'>" + "Short Rate" + " </td><td class='semi-bold'>" + "Special OverTime Formula" + "</td><td class='semi-bold'>" + "Special OverTime Rate" + "</td> <td class='semi-bold'>" + "Company Special Hrs" + " </td><td class='semi-bold'>" + "Company Travel Rate" + "</td><td class='semi-bold' colspan='3'>" + "Normal OverTime Formula" + "</td> </tr>" +
                "<tr style='text-align:center'><td colspan='2'>" + (info.comp_shot_formula_desc) + "</td> <td>" + (info.comp_shot_rate) + " </td><td>" + (info.comp_sot_formula_desc) + "</td><td>" + (info.comp_sot_rate) + "</td> <td>" + (info.comp_special_hrs) + " </td><td>" + (info.comp_travel_rate) + "</td><td colspan='3'>" + (info.comp_not_formula_desc) + "</td> </tr>" +
                " </table1></td></tr>")
                $($event.currentTarget).parents("tr").after(dom);
            }


        }])
})();