﻿(function () {
    'use strict';
    var del = [];
    var main;
    var main1;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PayrollPubFinStatusCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var d = new Date();
            var n = d.getMonth() + 1;
            $scope.edt = {};
            $scope.EmployeeDetails = [];
            $scope.postFinalizeData = [];
            $scope.fi_date = true;
            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.showFinalizeBtn = false;
            $scope.showProcessBtn = true;
            $scope.processedBtn = false;
            $scope.edt.yearMonth = [];

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            //$scope.finalize_date = dd + '-' + mm + '-' + yyyy;
            
            $scope.select_date_publish = function (date) {
                debugger;
                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //var date1 = year + "-" + month + "-" + day;
                //$scope.publish_date = date1;
                $scope.publish_date = date;
            }

            $(function () {
                $('#yearMonth').multipleSelect({
                    width: '100%'
                });
            });

            $http.get(ENV.apiUrl + "api/generatepayroll/GetCurrentFinYearFromFinsParameter").then(function (GetCurrentFinYearFromFinsParameter) {

                $scope.GetCurrentFinYearFromFinsParameter = GetCurrentFinYearFromFinsParameter.data;
                $scope.fin_year = $scope.GetCurrentFinYearFromFinsParameter[0].fin_year;
                $scope.fin_year_desc = $scope.GetCurrentFinYearFromFinsParameter[0].fin_year_desc;
            });


            //$http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/PubYear").then(function (PubYear) {
            //    $scope.PubYear = PubYear.data;
            //    $scope.fin_year = $scope.PubYear[0].fin_year;
            //    $scope.fin_year_desc = $scope.PubYear[0].fin_year_desc;

            //});

            $http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/PubMonth").then(function (PubMonth) {
                $scope.PubMonth = PubMonth.data;
                //$scope.edt = { sd_year_month_code: n.toString() };
            });

            $http.post(ENV.apiUrl + "api/OvertimeHour/Year_month").then(function (Year_month) {
                $scope.Year_month = Year_month.data;
                var d = new Date().getFullYear();
                var n = new Date().getMonth() + 1;
                var ym = d + '' + n;
                //$scope.edt.yearMonth = ym;
                setTimeout(function () {
                    $('#yearMonth').change(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);

                $scope.setYearMonth();
            });

            $scope.setYearMonth = function () {
                debugger
                if ($scope.edt.yearMonth.length > 1) {
                    swal({title:'Alert',text: 'Please select only one month', width: 340, showCloseButton: true });
                }
                $scope.edt.sd_year_month_code = $scope.edt.yearMonth[0].substr(4);
                //$scope.fin_year = $scope.edt.yearMonth.substr(0, 4);
            }

            $scope.Show_data = function () {
                debugger

                if ($scope.edt.yearMonth.length > 1) {
                    swal({ title: 'Alert', text: 'Please select only one month', width: 340, showCloseButton: true });
                    $("#yearMonth").multipleSelect("uncheckAll");
                }
                $scope.edt.sd_year_month_code = $scope.edt.yearMonth[0].substr(4);

                $scope.busy = true;
                $scope.rgvtbl = false;                
                $http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/EmployeeDetails?year_month=" + $scope.fin_year + $scope.edt.sd_year_month_code).then(function (EmployeeDetails) {
                    $scope.EmployeeDetails = EmployeeDetails.data;
                    console.log("EmployeeDetails", $scope.EmployeeDetails);
                    
                    main = document.getElementById('mainchk');
                    main.checked = true;
                    for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                        if ($scope.EmployeeDetails[i].holdstatus == false) {
                            main.checked = false;
                        }
                    }
                    if ($scope.EmployeeDetails.length > 0) {
                        //    var ear = 0, ded = 0, tot = 0;
                        //    for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                        //        ear = ear + parseFloat($scope.EmployeeDetails[i].earn);
                        //        ded = ded + parseFloat($scope.EmployeeDetails[i].ded);
                        //        tot = tot + (ear - ded);
                        //    }
                        //    $scope.Earning = ear;
                        //    $scope.Deduction = ded;
                        //    $scope.Total = tot;
                        $scope.showFinalizeBtn = false;
                        $scope.showProcessBtn = true;
                        $scope.processedBtn = false;

                        $scope.busy = false;
                        $scope.rgvtbl = true;
                        //$("#yearMonth").multipleSelect("uncheckAll");                       
                    }
                    else {
                        swal('', 'Payroll not genrated or processed for selected month.'); 
                        $scope.busy = false;
                        $scope.rgvtbl = false;
                        //$scope.Show_data_finalize();
                        $scope.processedBtn = true;
                        //$("#yearMonth").multipleSelect("uncheckAll");                        
                    }

                });

                //$http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/PubFinDetails").then(function (PubFinDetails) {
                //    $scope.PubFinDetails = PubFinDetails.data;
                //    $scope.publish_date
                //    $scope.fin_status
                //    $scope.finalize_date


                //});
            }

            $scope.Show_data_finalize = function () {
                debugger
                var yearMonth = "";
                for (var i = 0; i < $scope.edt.yearMonth.length; i++) {
                    yearMonth += ($scope.fin_year + $scope.edt.yearMonth[i].substr(4)) +',';
                }

                console.log("yearMonth",yearMonth);
                //$scope.fin_year + $scope.edt.sd_year_month_code
                $scope.busy = true;
                $scope.rgvtbl = false;               
                $http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/EmployeeDetailsForFinalize?year_month="+ yearMonth).then(function (EmployeeDetails) {

                    $scope.EmployeeDetails = EmployeeDetails.data;
                    console.log("EmployeeDetails", $scope.EmployeeDetails);

                    main = document.getElementById('mainchk');
                    main.checked = true;
                    for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                        if ($scope.EmployeeDetails[i].holdstatus == false) {
                            main.checked = false;
                        }
                    }
                    if ($scope.EmployeeDetails.length > 0) {
                        //    var ear = 0, ded = 0, tot = 0;
                        //    for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                        //        ear = ear + parseFloat($scope.EmployeeDetails[i].earn);
                        //        ded = ded + parseFloat($scope.EmployeeDetails[i].ded);
                        //        tot = tot + (ear - ded);
                        //    }
                        //    $scope.Earning = ear;
                        //    $scope.Deduction = ded;
                        //    $scope.Total = tot;
                        $scope.showFinalizeBtn = true;
                        $scope.showProcessBtn = false;

                        $scope.busy = false;
                        $scope.rgvtbl = true;
                    }
                    else {
                        swal('', 'Payroll not processed for selected month.');
                        $scope.busy = false;
                        $scope.rgvtbl = false;
                    }

                });

                //$http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/PubFinDetails").then(function (PubFinDetails) {
                //    $scope.PubFinDetails = PubFinDetails.data;
                //    $scope.publish_date
                //    $scope.fin_status
                //    $scope.finalize_date


                //});
            }


            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.le_leave_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                        item.acno == toSearch) ? true : false;
            }

            //$scope.CheckAllChecked = function () {
            //    debugger;
            //    main = document.getElementById('mainchk');

            //    for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
            //        if (main.checked == true) {
            //            $scope.EmployeeDetails[i].check = true;
            //            $('tr').addClass("row_selected");
            //        }

            //        else {
            //            $scope.EmployeeDetails[i].check = false;
            //            $('tr').removeClass("row_selected");

            //        }

            //    }
            //}
            $scope.isSelectAll = function () {
                $scope.postFinalizeData = [];
                debugger
                if ($scope.selectedAll) {
                    $scope.selectedAll = true;
                    for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                        $scope.postFinalizeData.push($scope.EmployeeDetails[i].em_login_code);
                    }                    
                }
                else {
                    $scope.selectedAll = false;                    
                }

                angular.forEach($scope.EmployeeDetails, function (item) {
                    item.check = $scope.selectedAll;
                });
            }

            $scope.checkIfAllSelected = function () {
                var id = event.target.id;
                var value = event.target.value;
                if (document.getElementById(id).checked) {
                    $scope.postFinalizeData.push(id);
                    if ($scope.postFinalizeData.length == $scope.EmployeeDetails.length) {
                        $scope.selectedAll = true;
                    }                    
                } else {
                    $scope.selectedAll = false;
                    $scope.row1 = '';
                    var index = $scope.postFinalizeData.indexOf(id);
                    $scope.postFinalizeData.splice(index, 1);                    
                }
                console.log("postFinalizeData", $scope.postFinalizeData);
            };

            $scope.onhold_update = function () {
                console.log($scope.EmployeeDetails);
                var datalst = [];
                for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                    var data = {
                        holdstatus: $scope.EmployeeDetails[i].holdstatus,
                        year_month: +$scope.fin_year + $scope.edt.sd_year_month_code,
                        emp_number: $scope.EmployeeDetails[i].emp_number
                    }
                    datalst.push(data);
                }

                $http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/UpdatePubFinStatus", datalst).then(function (UpdatePubFinStatus) {
                    $scope.UpdatePubFinStatus = UpdatePubFinStatus.data;
                    debugger;
                    if ($scope.UpdatePubFinStatus) {
                        swal('', 'Record updated successfully.');

                        $scope.busy = false;
                        $scope.rgvtbl = true;
                        $scope.reset_form();
                    }
                    else {
                        $scope.Show_data();
                        swal('', 'Record not updated.');
                        $scope.busy = false;
                        $scope.rgvtbl = false;
                        $scope.reset_form();

                    }



                });
            }

            $scope.process_click = function () {
                $scope.emp_id = "";
               
                debugger
                if ($scope.postFinalizeData.length > 0) {

                    for (var i = 0; i < $scope.postFinalizeData.length; i++) {
                        $scope.emp_id = $scope.emp_id + $scope.postFinalizeData[i] + ',';
                    }

                    if ($scope.edt.yearMonth.length > 1) {
                        swal({ title: 'Alert', text: 'Please select only one month', width: 340, showCloseButton: true });
                        $("#yearMonth").multipleSelect("uncheckAll");
                    }
                    $scope.edt.sd_year_month_code = $scope.edt.yearMonth[0].substr(4);

                    $scope.busy = true;
                    $scope.rgvtbl = false;
                    $http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/FinDetails?year_month=" + $scope.fin_year + $scope.edt.sd_year_month_code + "&emp_id=" + $scope.emp_id).then(function (FinDetails) {
                        $scope.FinDetails = FinDetails.data;
                        debugger;
                        if ($scope.FinDetails.length > 0) {
                            var ear = 0, ded = 0, tot = 0;
                            for (var i = 0; i < $scope.FinDetails.length; i++) {
                                if ($scope.FinDetails[i].pc_earn_dedn_tag == 'E')
                                    ear = ear + parseFloat($scope.FinDetails[i].amount);
                                else
                                    ded = ded + parseFloat($scope.FinDetails[i].amount);


                            }
                            tot = tot + (ear - ded);
                            $scope.Earning = ear;
                            $scope.Deduction = ded;
                            $scope.Total = tot;
                            $('#myModal').modal({ backdrop: 'static', keyboard: false });
                        }
                        else {
                            swal('', 'Payroll not genrated for current month.');
                        }
                        $scope.busy = false;

                    });
                }
                else {
                    swal('', 'Please select at least one record.');
                }
            }

            $scope.finlize_click = function () {
                $scope.emp_id = "";

                debugger
                if ($scope.postFinalizeData.length > 0) {

                    for (var i = 0; i < $scope.postFinalizeData.length; i++) {
                        $scope.emp_id = $scope.emp_id + $scope.postFinalizeData[i] + ',';
                    }

                    var empId = $scope.emp_id; //"Int32,Int32,Int32,Int32,Int32,Int32,Int32,Int32,Int32,Double,Double,Double"
                    $scope.emp_id = Array.from(new Set($scope.emp_id.split(','))).toString();
                    console.log("remove duplicates ", $scope.emp_id);


                    var yearMonth = "";
                    for (var i = 0; i < $scope.edt.yearMonth.length; i++) {
                        yearMonth += ($scope.fin_year + $scope.edt.yearMonth[i].substr(4)) + ',';
                    }
                    //$scope.fin_year + $scope.edt.sd_year_month_code
                    console.log("yearMonth", yearMonth);
                    $scope.busy = true;
                    $scope.rgvtbl = false;

                    $http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/FinDetails?year_month=" + yearMonth + "&emp_id=" + $scope.emp_id).then(function (FinDetails) {
                        $scope.FinDetails = FinDetails.data;
                        debugger;
                        if ($scope.FinDetails.length > 0) {
                            var ear = 0, ded = 0, tot = 0;
                            for (var i = 0; i < $scope.FinDetails.length; i++) {
                                if ($scope.FinDetails[i].pc_earn_dedn_tag == 'E')
                                    ear = ear + parseFloat($scope.FinDetails[i].amount);
                                else
                                    ded = ded + parseFloat($scope.FinDetails[i].amount);


                            }
                            tot = tot + (ear - ded);
                            $scope.Earning = ear;
                            $scope.Deduction = ded;
                            $scope.Total = tot;
                            $('#myModal').modal({ backdrop: 'static', keyboard: false });
                        }
                        else {
                            swal('', 'Payroll not genrated for current month.');
                        }
                        $scope.busy = false;

                    });
                }
                else {
                    swal('', 'Please select at least one record.');
                }
            }

            $scope.reset_form = function () {
                //$http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/PubYear").then(function (PubYear) {
                //    $scope.PubYear = PubYear.data;
                //    $scope.fin_year = $scope.PubYear[0].fin_year;
                //    $scope.fin_year_desc = $scope.PubYear[0].fin_year_desc;
                //});

                //$http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/PubMonth").then(function (PubMonth) {
                //    $scope.PubMonth = PubMonth.data;
                //    $scope.edt = { sd_year_month_code: n.toString() };
                //});
                //$http.post(ENV.apiUrl + "api/OvertimeHour/Year_month").then(function (Year_month) {
                //    $scope.Year_month = Year_month.data;
                //    var d = new Date().getFullYear();
                //    var n = new Date().getMonth() + 1;
                //    var ym = d + '' + n;
                //    $scope.edt.yearMonth = ym;
                //    $scope.setYearMonth();
                //});
                // $scope.edt.sd_year_month_code = $scope.edt.yearMonth.substr(4);
                // $scope.fin_year = $scope.edt.yearMonth.substr(0, 4);

                $scope.EmployeeDetails = [];
                $scope.postFinalizeData = [];
                $scope.rgvtbl = false;
                $("#yearMonth").multipleSelect("uncheckAll");
            }

            $scope.SaveProcess = function () {
              
                if ($scope.edt.yearMonth.length > 1) {
                    swal({ title: 'Alert', text: 'Please select only one month', width: 340, showCloseButton: true });
                    $("#yearMonth").multipleSelect("uncheckAll");
                }
                $scope.edt.sd_year_month_code = $scope.edt.yearMonth[0].substr(4);

                if ($scope.finalize_date == undefined || $scope.finalize_date == '') {
                    swal('', 'Please select process date.')
                }
                else {
                    console.log("emp_id", $scope.emp_id);
                    $http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/UpdateFinProcess?year_month=" + $scope.fin_year + $scope.edt.sd_year_month_code + "&finalize_date=" + $scope.finalize_date + "&emp_id=" + $scope.emp_id + "&user_name=" + $rootScope.globals.currentUser.username).then(function (UpdateFin) {
                        $scope.UpdateFin = UpdateFin.data;
                        $scope.emp_id = "";
                        if ($scope.UpdateFin) {
                            swal({title:'Alert', text:'Payroll Processed Successfully'});
                            $scope.postFinalizeData = [];
                            $('#myModal').modal('hide');
                            $("#yearMonth").multipleSelect("uncheckAll");
                        }                            
                        else {
                            swal({ title: 'Alert', text: 'Payroll not processed done' });
                        }
                        $('#myModal').modal('hide');
                    });
                }
            }

            $scope.SaveFinalize = function () {

                var yearMonth = "";
                for (var i = 0; i < $scope.edt.yearMonth.length; i++) {
                    yearMonth += ($scope.fin_year + $scope.edt.yearMonth[i].substr(4)) + ',';
                }
                console.log("yearMonth", yearMonth);
                //$scope.fin_year + $scope.edt.sd_year_month_code

                if ($scope.finalize_date == undefined || $scope.finalize_date == '') {
                    swal('', 'Please select finalize date.')
                }
                else {
                    console.log("emp_id", $scope.emp_id);
                    $http.post(ENV.apiUrl + "api/Payroll/PayrollPubFin/UpdateFin?year_month=" + yearMonth + "&finalize_date=" + $scope.finalize_date + "&emp_id=" + $scope.emp_id + "&user_name=" + $rootScope.globals.currentUser.username).then(function (UpdateFin) {
                        $scope.UpdateFin = UpdateFin.data;
                        $scope.emp_id = "";
                        if ($scope.UpdateFin) {
                            swal({ title: 'Alert', text: 'Finalization Done Successfully' });
                            $('#myModal').modal('hide');
                            $("#yearMonth").multipleSelect("uncheckAll");
                        }
                        else {
                            swal({ title: 'Alert', text: 'Finalization not done' });
                        }
                                                 
                    });
                }
            }

            $scope.close_btn = function () {
                $('#myModal').modal('hide');
                $scope.emp_id = "";
            }

            $scope.openDocumentReport = function (obj) {
                debugger
                console.log($scope.edt.sd_year_month_code);

                var data = {
                    //location: 'Sims.SIMR51FeeRec',
                    location: 'Payroll.PERR19SISERP',
                    parameter: {
                        acad_year: $scope.fin_year,
                        year_month: $scope.edt.yearMonth,
                        emp_id: obj.em_login_code
                    },
                    state: 'main.Per352',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter');
            }

            //$('*[data-datepicker="true"] input[type="text"]').datepicker({
            //    todayBtn: true,
            //    orientation: "top left",
            //    autoclose: true,
            //    todayHighlight: true
            //});

            //$(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
            //    $('input[type="text"]', $(this).parent()).focus();
            //});

            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);


        }])
})();