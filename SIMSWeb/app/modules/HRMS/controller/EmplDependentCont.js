﻿(function () {
    'use strict';

    var main, temp, del = [];

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmplDependentCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var sendata = []
            $scope.sms = true;
            $scope.loading = false;
            $scope.pagesize = "10";
            $scope.dis1 = true;
            $scope.dis2 = false;
            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.temp = {};
            $scope.sectionsobj = [];

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $rootScope.visible_stud = false;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = true;
            $rootScope.chkMulti = false;
            $scope.showEMPName = false;
            $scope.showField = false;
            localStorage.removeItem("em_login_code");

            var schoolArray = ['asdportal','apsportal','elcportal','sis'];

            if(schoolArray.includes($http.defaults.headers.common['schoolId'])) {
                $scope.showField = true;
            }

            $scope.Employee_search = function () {
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }
         

            $scope.$on('global_cancel', function () {                
                if ($scope.SelectedUserLst.length > 0) {
                    $scope.showEMPName = true;
                    $scope.temp['em_login_code'] = $scope.SelectedUserLst[0].em_login_code;
                    $scope.empName = $scope.SelectedUserLst[0].empName;                    
                    localStorage.setItem("em_login_code", $scope.temp['em_login_code']);
                    if (!$scope.dis2) {
                        $scope.showSections();
                    }                    
                }
            });

            $http.get(ENV.apiUrl + "api/EmplDependent/getDependentRelationship").then(function (res) {
                $scope.dependentRelations = res.data;              
            });

            $scope.showSections = function () {

                $scope.loading = true;
                $http.post(ENV.apiUrl + "api/EmplDependent/employee_dependent?emp_id=" + $scope.temp.em_login_code).then(function (res) {
                    $scope.sectionsobj = res.data;
                    if ($scope.sectionsobj.length > 0) {
                        console.log("emp depn", $scope.sectionsobj);
                        $scope.table = true;
                        $scope.loading = false;
                        $scope.totalItems = $scope.sectionsobj.length;
                        $scope.todos = $scope.sectionsobj;
                        $scope.makeTodos();
                    }
                    else {
                        $scope.loading = false;
                        $scope.table = false;
                        swal({ text: 'Employee dependent details not found', width: 380 });
                    }                    
                });
            }

            $scope.check = function () {
                main = document.getElementById('mainchk');

                debugger;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].ed_sr_code);
                    if (main.checked == true) {
                        v.checked = true;

                        $('tr').addClass("row_selected");
                    }
                    else {
                        v.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.CheckAny = function (ed_sr_code) {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }

            }

            $scope.Update = function (str) {
                $scope.temp = angular.copy(str);

                $scope.update_btn = true;
                $scope.dis1 = false; $scope.dis2 = true;
                $scope.select_list = false;
                $scope.update_flag = true;

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }
                $scope.check();
            }

            $scope.save = function (Myform) {
                debugger
                if (Myform) {
                  //  if (str) {
                        if ($scope.temp.ed_dob == '')
                            $scope.temp.ed_dob = null;
                        if ($scope.temp.ed_visa_issue_date == '')
                            $scope.temp.ed_visa_issue_date = null;
                        if ($scope.temp.ed_visa_expiry_date == '')
                            $scope.temp.ed_visa_expiry_date = null;
                        if ($scope.temp.ed_nation_id_issue_date == '')
                            $scope.temp.ed_nation_id_issue_date = null;
                        if ($scope.temp.ed_nation_id_expiry_date == '')
                            $scope.temp.ed_nation_id_expiry_date = null;
                        if ($scope.temp.ed_passport_id_issue_date == '')
                            $scope.temp.ed_passport_id_issue_date = null;
                        if ($scope.temp.ed_passport_expiry_date == '')
                            $scope.temp.ed_passport_expiry_date = null;
                        if ($scope.temp.ed_visa_exp_rem_date == '')
                            $scope.temp.ed_visa_exp_rem_date = null;
                        if ($scope.temp.ed_health_insurance_effective_from_date == '')
                            $scope.temp.ed_health_insurance_effective_from_date = null;
                        if ($scope.temp.ed_health_insurance_effective_upto_date == '')
                            $scope.temp.ed_health_insurance_effective_upto_date = null;
                        var data = $scope.temp;
                        data.opr = "I";
                        debugger;
                        //sendata.push(data);
                        $http.post(ENV.apiUrl + "api/EmplDependent/CUDemployee_dependent", data).then(function (res) {
                            $scope.msg = res.data;
                            sendata = [];
                            if ($scope.msg == true) {
                                sendata = [];
                                $scope.dis1 = true;
                                $scope.dis2 = false;
                                swal({ text: 'Record Inserted Successfully', imageUrl: "assets/img/check.png", width: 380 });
                                $scope.showSections();

                            }
                            else {
                                $scope.showSections();
                                swal({ text: 'Record Not Inserted', imageUrl: "assets/img/close.png", width: 380 });
                            }

                        });

                  //  }
                }
                else {
                    swal({ text: 'Please fill required fields', width: 380 });
                }
            }

            $scope.Update_data = function (Myform) {
                debugger;

                if (Myform) {

                    if ($scope.temp.ed_dob == '')
                        $scope.temp.ed_dob = null;
                    if ($scope.temp.ed_visa_issue_date == '')
                        $scope.temp.ed_visa_issue_date = null;
                    if ($scope.temp.ed_visa_expiry_date == '')
                        $scope.temp.ed_visa_expiry_date = null;
                    if ($scope.temp.ed_nation_id_issue_date == '')
                        $scope.temp.ed_nation_id_issue_date = null;
                    if ($scope.temp.ed_nation_id_expiry_date == '')
                        $scope.temp.ed_nation_id_expiry_date = null;
                    if ($scope.temp.ed_passport_id_issue_date == '')
                        $scope.temp.ed_passport_id_issue_date = null;
                    if ($scope.temp.ed_passport_id_expiry_date == '')
                        $scope.temp.ed_passport_id_expiry_date = null;
                    if ($scope.temp.ed_visa_exp_rem_date == '')
                        $scope.temp.ed_visa_exp_rem_date = null;
                    if ($scope.temp.ed_health_insurance_effective_from_date == '')
                        $scope.temp.ed_health_insurance_effective_from_date = null;
                    if ($scope.temp.ed_health_insurance_effective_upto_date == '')
                        $scope.temp.ed_health_insurance_effective_upto_date = null;

                    var Update_data = $scope.temp;
                    Update_data.opr = 'U';

                    $http.post(ENV.apiUrl + "api/EmplDependent/CUDemployee_dependent", Update_data).then(function (res) {
                        $scope.msg = res.data;

                        if ($scope.msg == true) {
                            $scope.dis1 = true;
                            $scope.dis2 = false;
                            swal({ text: 'Record Updated Successfully', imageUrl: "assets/img/check.png", width: 380 });
                            $scope.showSections();

                        }
                        else {
                            swal({ text: 'Record Not Updated', imageUrl: "assets/img/close.png", width: 380 });
                            $scope.showSections();

                        }
                    })
                }
                else {
                    swal({ text: 'Please fill required fields', width: 380 });
                }
            }

            $scope.Delete_Records = function () {
                var data = {};
                sendata = [];
                debugger;
                var section_name = '';
                $scope.nodelete = false;
                $scope.sims_statusfordelete = false;
                var flag = false;

                var deletecount = 0;
                var tempabc=''
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].ed_sr_code);
                    if (v.checked == true) {
                        //if ($scope.filteredTodos[i].count == '0')
                        {
                            flag = true;
                            tempabc = tempabc + $scope.filteredTodos[i].ed_sr_code + ',';
                        }

                    }
                }

                data = {
                    em_login_code: $scope.temp.em_login_code,
                    ed_sr_code: tempabc,
                    opr: "D"
                };


                if (flag == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/EmplDependent/CUDemployee_dependent", data).then(function (res) {
                                $scope.sectionsobj = res.data;
                                sendata = [];
                                if ($scope.sectionsobj == true) {
                                    $scope.dis1 = true;
                                    $scope.dis2 = false;
                                    swal({ text: 'Deleted Successfully', imageUrl: "assets/img/check.png", width: 350, showCancelButton: true, });
                                    $scope.showSections();
                                }
                                else {
                                    $scope.showSections();
                                    swal({ text: 'Not Deleted', imageUrl: "assets/img/close.png", width: 350, showCancelButton: true, });
                                }

                            })
                        }
                        else {

                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            $scope.check();

                        }
                    });
                }
                else {
                    if ($scope.sims_statusfordelete != true) {
                        swal({ text: 'Select At Least One Record To Delete', imageUrl: "assets/img/notification-alert.png", width: 320, showCancelButton: true, });
                    }
                }
            }

            $scope.can = function () {
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

                $scope.dis1 = true;
                $scope.dis2 = false;
                $scope.temp = {};
                localStorage.removeItem("em_login_code");
                $scope.empName = "";
                del = [];              
            }

            $scope.New = function () {
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

                $scope.curiculum = false;
                $scope.ac_year = false;
                $scope.grade_nm = false;
                
                $scope.update_flag = false;
                $scope.new_funs = true;
                $scope.dis1 = false;
                $scope.dis2 = true;
                $scope.select_list = true;
                $scope.update_btn = false;
                $scope.temp = {};
                $scope.temp = {                    
                    //ed_visa_issue_date: dd + '-' + mm + '-' + yyyy,
                    //ed_visa_expiry_date: dd + '-' + mm + '-' + yyyy,
                    //ed_nation_id_issue_date: dd + '-' + mm + '-' + yyyy,
                    //ed_nation_id_expiry_date: dd + '-' + mm + '-' + yyyy,
                    //ed_passport_issue_date: dd + '-' + mm + '-' + yyyy,
                    //ed_passport_expiry_date: dd + '-' + mm + '-' + yyyy,
                    em_login_code:localStorage.getItem("em_login_code")
                }
                

                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();


            }

            var dom;
            $scope.flag = true;


            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.Homeroom_MultipleDelete();
                }
            };

            $scope.searched = function (valLists, toSearch) {



                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.sectionsobj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.sectionsobj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.ed_first_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.ed_middle_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.ed_last_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.setVisaExpiryReminderDate = function () {
                debugger;
                if ($scope.temp.ed_visa_expiry_date != undefined) {
                    console.log($scope.temp.ed_visa_expiry_date);
                    var VisaExpReminderDate = moment($scope.temp.ed_visa_expiry_date, "DD-MM-YYYY");                    
                    $scope.temp.ed_visa_exp_rem_date = VisaExpReminderDate.subtract(30, "days").format("DD-MM-YYYY");
                    console.log("em_probation_completion_date", $scope.temp.ed_visa_exp_rem_date);
                }
            };


            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;

            debugger
            $scope.temp = {               
                //ed_visa_issue_date: dd + '-' + mm + '-' + yyyy,
                //ed_visa_expiry_date: dd + '-' + mm + '-' + yyyy,
                //ed_nation_id_issue_date: dd + '-' + mm + '-' + yyyy,
                //ed_nation_id_expiry_date: dd + '-' + mm + '-' + yyyy,
                //ed_passport_issue_date: dd + '-' + mm + '-' + yyyy,
                //ed_passport_expiry_date: dd + '-' + mm + '-' + yyyy,
                em_login_code: localStorage.getItem("em_login_code")
            }

        }]);
})();

