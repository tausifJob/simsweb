﻿
(function () {
    'use strict';

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeAttendanceCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', 'XLSXReaderService', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, XLSXReaderService, $filter) {

            $scope.rgvtbl = false;
            $scope.edt = {};
            $scope.edt1 = {};
            $scope.edt2 = {};
            var del = [];
            var maindata = [];

            $http.get(ENV.apiUrl + "api/EmpAttendanceDailyController/getPers326Dept").then(function (Pers326_get_Dept) {
                $scope.dep = Pers326_get_Dept.data;
            });

            $http.get(ENV.apiUrl + "api/EmpAttendanceDailyController/getPers326desg").then(function (Pers326_get_desg) {
                $scope.deg = Pers326_get_desg.data;
            });

            $http.get(ENV.apiUrl + "api/common/ShiftMaster/getShiftMaster").then(function (res) {
                $scope.shift_data = res.data;
                console.log("shift_data", $scope.shift_data);
                $scope.showShift2Flag = false;
                var flag = 1;
                for (var i = 0; i < $scope.shift_data.length; i++) {
                    if (($scope.shift_data[i].sh_shift2_in == "" || $scope.shift_data[i].sh_shift2_in == undefined) && ($scope.shift_data[i].sh_shift2_out == "" || $scope.shift_data[i].sh_shift2_out == undefined)) {
                        $scope.showShift2Flag = false;
                        flag = 0;
                    }
                }
                if (flag) {
                    $scope.showShift2Flag = true;
                }
            });

            $scope.getAttandanceCode = function (str) {
                debugger;
                var data = [];
                for (var i = 0; i < str.length; i++) {
                    if (i == 0) {
                        data = str[i].leave_code;
                    }
                    else {
                        data = data + ',' + str[i].leave_code;
                    }
                }
                if (data == '') {
                    data = str.leave_code;
                }
                $http.get(ENV.apiUrl + "api/EmpAttendanceDailyController/getAttendanceCode?str=" + data).then(function (data_attendanceCode) {
                    $scope.attendance_codes_obj = data_attendanceCode.data;
                    console.log($scope.attendance_codes_obj);
                });

            }
            $scope.btn_reset_Click = function () {

                $scope.Pers326_Get_attednce_deatils = '';
                $scope.edt = {};
                $scope.edt1 = {};
                $scope.edt2 = {};
                $scope.rgvtbl = false;
            }
            $scope.edt1.sdate = $filter('date')(new Date(), 'dd-MM-yyyy');
            $scope.edt2.edate = $filter('date')(new Date(), 'dd-MM-yyyy');

            $scope.btn_Submit = function (str, str1, str2, str3) {
                debugger;
                if (str2 != undefined && str3 != undefined) {
                    setTimeout(function () {print
                        $(document).ready(function () {
                            $("#from_date, #to_date").kendoDatePicker({
                                format: "dd-MM-yyyy",
                                value: ''
                            });
                        });
                    }, 700);
                    if (str == undefined) {
                        $scope.edt.dep_code = '';
                        str = '';
                    }
                    if (str1 == undefined) {
                        $scope.edt.dg_code = '';
                        str1 = '';
                    }
                    $scope.rgvtbl = false;
                    $scope.busy = true;
                    maindata = [];
                    ///$http.post(ENV.apiUrl + "api/EmpAttendanceDailyController/Pers326_Get_attednce_deatils?sdate=" + $scope.edt1.sdate + "&edate=" + $scope.edt2.edate + "&dept=" + $scope.edt.dep_code + "&desg=" + $scope.edt.dg_code).then(function (Pers326_Get_attednce_deatils) {
                    $http.post(ENV.apiUrl + "api/EmpAttendanceController/EDA355_Get_attednce_deatils?sdate=" + str2 + "&edate=" + str3 + "&dept=" + str + "&desg=" + str1).then(function (Pers326_Get_attednce_deatils) {
                        debugger;
                        var temp = [];
                        $scope.Pers326_Get_attednce_deatils1 = Pers326_Get_attednce_deatils.data;
                        var check = document.getElementById('Checkbox8');
                        if (check.checked == true) {
                            for (var i = 0; i < $scope.Pers326_Get_attednce_deatils1.length; i++) {
                                try {
                                    var isAnPresent = false;
                                    for (var k = 0; k < temp.length ; k++) {
                                        if (temp[k].pays_attendance_code == $scope.Pers326_Get_attednce_deatils1[i].leave_code) {
                                            isAnPresent = true;
                                        }
                                    }
                                    if (isAnPresent == false) {
                                        temp = {
                                            pays_attendance_code: $scope.Pers326_Get_attednce_deatils1[i].leave_code,
                                            pays_attendance_description: $scope.Pers326_Get_attednce_deatils1[i].leave_name,
                                        }
                                    }
                                } catch (e) {

                                }
                                maindata.push($scope.Pers326_Get_attednce_deatils1[i]);
                            }
                        }
                        else {
                            for (var i = 0; i < $scope.Pers326_Get_attednce_deatils1.length; i++) {
                                try {
                                    var isAnPresent = false;
                                    for (var k = 0; k < temp.length ; k++) {
                                        if (temp[k].pays_attendance_code == $scope.Pers326_Get_attednce_deatils1[i].leave_code) {
                                            isAnPresent = true;
                                        }
                                    }
                                    if (isAnPresent == false) {
                                        temp.push($scope.Pers326_Get_attednce_deatils1[i]);
                                    }
                                } catch (e) {

                                }
                                maindata.push($scope.Pers326_Get_attednce_deatils1[i]);
                                //if ($scope.Pers326_Get_attednce_deatils1[i].leave_code != 'P') {
                                //    maindata.push($scope.Pers326_Get_attednce_deatils1[i]);
                                //}
                            }
                        }
                        $scope.getAttandanceCode(temp);
                        $scope.Pers326_Get_attednce_deatils = maindata;
                        $scope.busy = false;
                        $scope.rgvtbl = true;
                        $scope.colsvis = true;
                    });
                }
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.btn_Employeeupdate_click = function () {
                debugger;
                $scope.rgvtbl = false;
                $scope.busy = true;
                var senddataobject = [];
                for (var i = 0; i < $scope.Pers326_Get_attednce_deatils1.length; i++) {
                    // var v = document.getElementById('test-' + $scope.Pers326_Get_attednce_deatils1[i].att_emp_id);
                    // var x = $("#test-" + $scope.Pers326_Get_attednce_deatils1[i].att_emp_id).prop('checked',true)
                    if ($scope.Pers326_Get_attednce_deatils1[i].datastatus == true) {
                        senddataobject.push($scope.Pers326_Get_attednce_deatils1[i]);
                    }
                }
                console.log(senddataobject);
                if (senddataobject.length > 0) {
                    $http.post(ENV.apiUrl + "api/EmpAttendanceDailyController/Pers326_update_att_data", senddataobject).then(function (result) {
                        $scope.result = result.data;
                        $scope.busy = false;
                        if ($scope.result == true) {
                            swal('', 'Record Sucessfully Updated.');
                            $scope.btn_reset_Click();
                        }
                        else if ($scope.result == false) {
                            swal('', 'Record Is Not Updated.');
                            $scope.rgvtbl = true;
                        }
                        else {
                            swal("Error-" + $scope.result)
                        }
                        setTimeout(function () {
                            $(document).ready(function () {
                                $("#from_date, #to_date").kendoDatePicker({
                                    format: "dd-MM-yyyy",
                                    value: ''
                                });
                            });
                        }, 700);
                    });
                }
                else {
                    $scope.busy = false;
                    swal('', 'Please select employee id.');
                }
            }
            $scope.uploadClick1 = function () {
            }

            ///////////////////////////// Upload Doc Code //////////////////////////////////////////

            $scope.display = true;
            $scope.save = true;
            $scope.item = [];

            var date = new Date();
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss');
            $scope.showPreview = false;
            $scope.showJSONPreview = true;
            $scope.json_string = "";

            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {

                // $scope.filesize = true;
                // if ($files) {
                $('#uploadModal').modal({ backdrop: 'static', keyboard: false });

                //}
                $scope.flag = 0;
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                    var i = 0;
                    console.log($files[i].size);

                });
            };
            $scope.errorlabel = false;
            $scope.file_changed = function (element) {
                debugger;
                // SpinnerDialog.show('loading','...');
                $scope.modalbusy = true;
                $scope.busy = true;
                $scope.rgvtbl = false;
                var photofile = element.files[0];
                console.log(element.files[0].size);
                if (element.files[0].size > 600000) {
                    swal('Alert', 'Exceed size limit');
                }
                else {
                    $scope.filename = element.files[0];
                    $scope.photo_filename = (photofile.type);
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $scope.$apply(function () {
                            $scope.prev_img = e.target.result;
                        });
                    };
                    reader.readAsDataURL(photofile);
                    $scope.sheets = [];
                    $scope.excelFile = element.files[0];
                    XLSXReaderService.readFile($scope.excelFile, $scope.showPreview, $scope.showJSONPreview).then(function (xlsxData) {
                        console.log(xlsxData.sheets);
                        var main = document.getElementById('mainchk');
                        if (main.checked == true) {
                            main.checked = false;
                            $scope.filecount = "";
                        }
                        $scope.errorlabel = false;
                        $scope.display = false;
                        $scope.save = false;
                        $scope.em_numbers = [];
                        console.log(xlsxData.sheets.Sheet1[0]);
                        var size = Object.keys(xlsxData.sheets.Sheet1[0]).length;
                        console.log(size);
                        if (size <= 6) {
                            $scope.sheets = xlsxData.sheets
                            console.log($scope.sheets);
                            $scope.sheetsData = [];
                            for (var i = 0; xlsxData.sheets.Sheet1.length > i; i++) {
                                $scope.subData = {};
                                $scope.subData['att_emp_id'] = xlsxData.sheets.Sheet1[i].Emp_No;
                                $scope.subData['att_date'] = xlsxData.sheets.Sheet1[i].Date;
                                $scope.subData['att_shift1_in'] = xlsxData.sheets.Sheet1[i].Shift1_In;
                                $scope.subData['att_shift1_out'] = xlsxData.sheets.Sheet1[i].Shift1_Out;
                                $scope.subData['att_shift2_in'] = xlsxData.sheets.Sheet1[i].Shift2_In;
                                $scope.subData['att_shift2_out'] = xlsxData.sheets.Sheet1[i].Shift2_Out;
                                $scope.sheetsData.push($scope.subData);
                            }

                            $http.post(ENV.apiUrl + "api/EmpAttendanceDailyController/PersEmnumber", $scope.sheetsData).then(function (res) {
                                $scope.em_numbers = res.data;
                                $scope.datasend = [];
                                for (var i = 0; $scope.em_numbers.length > i; i++) {
                                    if ($scope.em_numbers[i].emp_id_flag) {
                                        $scope.em_numbers[i]['fcolor'] = "black";
                                        $scope.modalbusy = false;
                                    } else {
                                        $scope.em_numbers[i]['fcolor'] = "red";
                                        $scope.modalbusy = false;
                                        $scope.errorlabel = true;
                                    }
                                }
                                console.log($scope.em_numbers);
                            });


                            $scope.sheetName = { sheet: 'Sheet1' };
                            var temp = "a";
                            $("#sheet_name").val(temp);
                        } else {
                            $scope.sheets = "";
                            swal({ title: "Document Upload Failed,Does not match Column" });
                            $scope.busy = false;
                            $scope.modalbusy = false;
                        }
                    });

                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + '/api/file/uploadDocument?filename=' + $scope.filename.name + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss') + "&location=" + "Docs/Student",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    $http(request).success(function (d) {
                        var data = {
                            sims_admission_user_code: $rootScope.globals.currentUser.username,
                            sims_admission_doc_path: d
                        }
                        $http.post(ENV.apiUrl + "api/document/insertdoc", data).then(function (res) {
                            $scope.ins = res.data;
                            if (res.data) {
                                swal({ title: "Document Uploaded Successfully", imageUrl: "assets/img/check.png" });
                            } else {
                                swal({ title: "Document Upload Failed, Check Connection", imageUrl: "assets/img/close.png" });
                            }
                        });
                    });
                    angular.forEach(
                     angular.element("input[type='file']"),
                     function (inputElem) {
                         angular.element(inputElem).val(null);
                     });
                    $scope.CheckMultiple();
                }
            };


            $scope.CheckAllChecked = function () {
                debugger
                var main = document.getElementById('allchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.Pers326_Get_attednce_deatils.length; i++) {

                        $scope.Pers326_Get_attednce_deatils[i].datastatus = true;
                        //var v = document.getElementById('test-' + $scope.Pers326_Get_attednce_deatils1[i].att_emp_id);
                        //v.checked = true;
                        //$('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.Pers326_Get_attednce_deatils.length; i++) {

                        $scope.Pers326_Get_attednce_deatils[i].datastatus = false;
                        //var v = document.getElementById('test-' + $scope.Pers326_Get_attednce_deatils1[i].att_emp_id);
                        //v.checked = false;
                        //main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                var main = document.getElementById('allchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.uploadClick = function () {
                // $scope.filesize = true;
                formdata = new FormData();
                $scope.busy = false;
                $scope.modalbusy = false;
            }
            if ($rootScope.globals.studentsLoaded) {
                $scope.getstudentList();
            }

            $scope.insertData = function () {
                debugger;
                if ($scope.datasend != "") {
                    console.log($scope.datasend);
                    var datearr = []
                    for (var i = 0; i < $scope.datasend.length; i++) {

                        datearr.push($scope.datasend[i].att_date);

                    }

                    var max = '';
                    for (var i = 0; i < datearr.length; i++) {
                        if (datearr[i] > max) {
                            max = datearr[i];
                        }
                    }

                    var earliestDate = datearr[0];
                    for (var i = 1; i < datearr.length ; i++) {
                        var currentDate = datearr[i];
                        if (currentDate < earliestDate) {
                            earliestDate = currentDate;
                        }
                    }

                    console.log(max);
                    console.log(earliestDate);

                    console.log($scope.datasend);
                    $http.post(ENV.apiUrl + "api/EmpAttendanceDailyController/MarkEmpDailyAttendance", $scope.datasend).then(function (res) {
                        console.log(res.data);

                        if (res.data > 0) {
                            $scope.edt1 = { sdate: earliestDate };
                            $scope.edt2 = { edate: max };
                            $scope.cancelData();
                            $scope.busy = true;
                            $scope.rgvtbl = false;
                            maindata = [];
                            $http.post(ENV.apiUrl + "api/EmpAttendanceDailyController/Pers326_Get_attednce_deatils?sdate=" + $scope.edt1.sdate + "&edate="
                                + $scope.edt2.edate + "&dept=" + '' + "&desg=" + '').then(function (Pers326_Get_attednce_deatils) {

                                    $scope.Pers326_Get_attednce_deatils1 = Pers326_Get_attednce_deatils.data;
                                    var check = document.getElementById('Checkbox8');
                                    if (check.checked == true) {
                                        for (var i = 0; i < $scope.Pers326_Get_attednce_deatils1.length; i++) {
                                            maindata.push($scope.Pers326_Get_attednce_deatils1[i]);
                                        }
                                    }
                                    else {
                                        for (var i = 0; i < $scope.Pers326_Get_attednce_deatils1.length; i++) {
                                            maindata.push($scope.Pers326_Get_attednce_deatils1[i]); ///30-01-2018 For not show data after inserting record Kishor
                                            //if ($scope.Pers326_Get_attednce_deatils1[i].leave_code != 'P') {
                                            //    maindata.push($scope.Pers326_Get_attednce_deatils1[i]);
                                            //}
                                        }
                                    }
                                    $scope.Pers326_Get_attednce_deatils = maindata;
                                    $scope.busy = false;
                                    $scope.rgvtbl = true;

                                });


                            swal('Records Inserted Successfully');
                            $scope.datasend = [];
                            $scope.CheckMultiple();
                        } else if (res.data == 0) {
                            $scope.datasend = [];
                            swal('Records Not Inserted. ');
                        }
                        else {
                            swal("Error-" + res.data)
                        }
                    });
                } else {
                    swal('', 'Please Select Record to Insert');
                }
            }

            $scope.cancelData = function () {
                $('#uploadModal').modal('hide');
                $scope.busy = false;
            }

            $scope.cancel = function () {
                $scope.display = true;
                $scope.sheets = "";
            }

            $scope.showPreviewChanged = function (selectedSheetName) {
                $scope.sheets[$scope.selectedSheetName];
            }

            $scope.textFocus1 = function (val) {
                $scope.item['att_shift1_in'] = val;
            }
            $scope.textFocus2 = function (val) {
                $scope.item['att_shift1_out'] = val;
            }
            $scope.textFocus3 = function (val) {
                $scope.item['att_shift2_in'] = val;
            }
            $scope.textFocus4 = function (val) {
                $scope.item['att_shift2_out'] = val;
            }

            $scope.$on('student_change', function () {
                $scope.getstudentList();
            });

            $scope.datasend = [];

            $scope.Checkval = function (val, index) {
                val.opr = "Q";
                $scope.values = val;
                $scope.chekindex = index;
                var main = document.getElementById(index + val.emp_id);

                var date = new Date(val.att_date);
                var month = (date.getMonth() + 1);
                var day = date.getDate();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                $scope.d = date.getFullYear() + '-' + (month) + '-' + (day);
                if (main.checked == true) {
                    var data = {
                        no: val.emp_id,
                        att_date: $scope.d,
                        s1in: val.att_shift1_in,
                        s1out: val.att_shift1_out,
                        s2in: val.att_shift2_in,
                        s2out: val.att_shift2_out,
                        opr: "Q",
                    }
                    $scope.datasend.push(data);

                } else {
                    main.checked = false;
                    var rm = del.indexOf(index);
                    $scope.datasend.splice(rm, 1);
                }
                if ($scope.datasend.length > 0) {
                    $scope.filecount = "You Selected " + $scope.datasend.length + " Records";
                } else {
                    $scope.filecount = "";
                }
            }

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $scope.colsvis = false;
            $scope.printclick=false;

            $scope.CheckMultiple = function () {
                var main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.em_numbers.length; i++) {
                    var v = document.getElementById(i + $scope.em_numbers[i].emp_id);
                    if (main.checked == true) {
                        if (v.checked != true) {
                            v.checked = true;
                            var t = $scope.em_numbers[i];
                            $scope.Checkval(t, i);
                        }
                    }
                    else {
                        v.checked = false;
                        var t = $scope.em_numbers[i];
                        main.checked = false;
                        $scope.Checkval(t, i);
                    }
                }

            }


            //$scope.Export = function () {
            //    debugger;
            //    $scope.colsvis = true;
            //    swal({
            //        title: "Alert",
            //        text: "Do you want to Export in MS-Excel?",
            //        showCloseButton: true,
            //        showCancelButton: true,
            //        confirmButtonText: 'Yes',
            //        width: 380,
            //        cancelButtonText: 'No',
            //    }).then(function (isConfirm) {
            //        if (isConfirm) {
            //            var blob = new Blob([document.getElementById('fixTable').innerHTML],
            //             {
            //                 type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
            //             });
            //            $scope.detailsField = false;
            //            saveAs(blob, "Employee Daily Attendance Report.xls");
            //            $scope.colsvis = false;
            //            $scope.btn_Submit();

            //        }
            //        $scope.btn_Submit();
            //    });

            //};

            $scope.Export = function () {
                debugger;
                $scope.printclick = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('fixTable').innerHTML],
                         {
                             type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                         });
                        $scope.detailsField = false;
                        saveAs(blob, "Employee Daily Attendance Report.xls");
                        $scope.colsvis = true;
                        $scope.printclick = false;
                        $scope.btn_Submit();

                    }

                });
                $scope.printclick = true;

            };
          

            $scope.print = function () {
                debugger;
                $scope.printclick = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Print?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var docHead = document.head.outerHTML;
                        var printContents = document.getElementById('fixTable').outerHTML;
                        var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
                        var newWin = window.open("", "_blank", winAttr);
                        var writeDoc = newWin.document;
                        writeDoc.open();
                        writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                        writeDoc.close();
                        newWin.focus();
                        $scope.btn_Submit();

                    }
                    $scope.btn_Submit();
                });

            };

            $scope.printclick = false;

            $scope.downloadFormat = function () {
                $scope.url = ENV.apiUrl + "/Content/format/attendance/attendance.xls";
                window.open($scope.url, '_self', 'location=yes');

            }

        }]);


    simsController.directive('ngFiles', ['$parse', '$scope', function ($parse, $scope) {
        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

    simsController.factory("XLSXReaderService", ['$q', '$rootScope',
    function ($q, $rootScope) {
        var service = function (data) {
            angular.extend(this, data);
        }

        service.readFile = function (file, readCells, toJSON) {
            var deferred = $q.defer();

            XLSXReader(file, readCells, toJSON, function (data) {
                $rootScope.$apply(function () {
                    deferred.resolve(data);
                });
            });

            return deferred.promise;
        }


        return service;
    }
    ])



})();
