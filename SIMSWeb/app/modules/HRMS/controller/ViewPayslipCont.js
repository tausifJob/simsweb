﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ViewPayslipCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
      
            $scope.display = true;
            $scope.Financial = true;
            //$http.get(ENV.apiUrl + "api/common/ViewPayslip/Getallperioddata").then(function (periodsName) {
            //    $scope.periods_Name = periodsName.data;
            //    console.log($scope.periods_Name);
            //});
            
            $http.get(ENV.apiUrl + "api/common/ViewPayslip/GetReportName").then(function (Reportname) {
                $scope.Report_Name = Reportname.data;
              
            });
            
            $http.get(ENV.apiUrl + "api/attendance/getAttendanceyear?cur_code=01").then(function (academicyears) {
                $scope.academicyears_obj = academicyears.data;
                if (academicyears.data.length > 0) {
                    $scope.Acdm_yr = academicyears.data[0].sims_academic_year;
                    $scope.acdm_yr_change();
                }

            });

            $scope.acdm_yr_change = function ()
            {
                $http.post(ENV.apiUrl + "api/common/ViewPayslip/Year_month?year=" + $scope.Acdm_yr).then(function (Year_month) {
                    $scope.Year_month = Year_month.data;
                    
                    var d = new Date().getFullYear();
                    var n = new Date().getMonth() + 1;
                    var ym = d + '' + n;

                    $scope.edt = { year_month: ym };
                });
            }

            $http.post(ENV.apiUrl + "api/common/ViewPayslip/Year_month?year=" + $scope.Acdm_yr).then(function (Year_month) {
                $scope.Year_month = Year_month.data;
               
                var d = new Date().getFullYear();
                var n = new Date().getMonth() + 1;
                var ym = d + '' + n;

                $scope.edt = { year_month: ym };
            });

            
            $scope.Report = function (year,paramenter) {
                
                var yr1 = paramenter.substring(0, 4);
                var mnth1 = paramenter.substring(4);
                var data = {
                    location: $scope.Report_Name[0].fins_appl_form_field_value1,
                    parameter: {
                        paysheet_year: yr1,
                        paysheet_month: mnth1,
                        search: null
                    },            
                    state: 'main.Pe078V',
                    ready: function () {
                        this.refreshReport();
                    },
                    show_pdf:'y'
                }

                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')
            }
            
        }])

})();
