﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeLetterReqIssueCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.EmpleaveType_data = [];
            var data1 = [];
            $scope.edt = [];
            $scope.display = false;
            $scope.uploading_doc1 = true;
            $scope.table = true;
            $scope.divcode_readonly = true;
            $scope.username = $rootScope.globals.currentUser.username;

            $scope.schoolUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/Requestdoc/';

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $http.get(ENV.apiUrl + "api/EmployeeLetterReq/Get_EmpDoc").then(function (res) {
                $scope.document = res.data;
            });
                      
            $scope.openReport = function (k) {
                var rname = k.report_loc;

                var data = {
                    //location: 'Sims.SIMR51FeeRec',
                    location: rname,
                    //parameter: {
                    //    fee_rec_no: str.doc_no,
                    //},
                    state: 'main.LetIss',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')

            }


            $scope.search_data = function () {            
                $http.get(ENV.apiUrl + "api/EmployeeLetterReq/Get_ReqDocIssue?date_from=" + $scope.date_from + "&date_to=" + $scope.date_to).then(function (res1) {
                    $scope.CreDiv = res1.data;
                    $scope.totalItems = $scope.CreDiv.length;
                    $scope.todos = $scope.CreDiv;
                    $scope.makeTodos();
                });
            }

            $scope.search_data();
            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.CreDiv;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_certificate_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                      || item.sims_certificate_requested_by_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    ) ? true : false;
            }


            $scope.New = function () {
                $scope.display = true;
                $scope.table = false;
            }
            $scope.Cancel = function () {
                debugger;
                $scope.temp = {};
               
                $scope.file_doc = "";
                $scope.filteredTodos = [];
                $scope.file_form = "";
                $scope.search_data();
                $scope.display = false;
                $scope.table = true;
            }

            var data_send = [];
            $scope.edit = function (str) {
                $scope.table = false;
                $scope.display = true;
                console.log(str);                
                $scope.temp=
                    {
                        sims_certificate_request_number: str.sims_certificate_request_number,
                        sims_certificate_requested_by: str.sims_certificate_requested_by,
                        sims_certificate_requested_by_name: str.sims_certificate_requested_by_name,
                        sims_certificate_number: str.sims_certificate_number,
                        sims_certificate_name: str.sims_certificate_name,
                        sims_certificate_reason: str.sims_certificate_reason,
                        sims_certificate_dept_code: str.sims_certificate_dept_code,
                        sims_certificate_dept_name: str.sims_certificate_dept_name,
                    }
            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                if (Myform) {
                    var data = {
                            'opr': 'N',
                            'sims_certificate_request_number': $scope.temp.sims_certificate_request_number,
                            'sims_certificate_approved_status': 'A',
                            'sims_certificate_issued_by': $scope.username,
                            'sims_certificate_requested_by':$scope.temp.sims_certificate_requested_by,
                            'sims_certificate_request_status': 'A',
                            'sims_certificate_number': $scope.temp.sims_certificate_number,
                            'sims_certificate_doc_path': $scope.file_doc[0].sims_certificate_doc_path,
                            'sims_certificate_issue_employee_number': $scope.temp.sims_certificate_requested_by,
                            'sims_certificate_print_count':'0'
                    }
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/EmployeeLetterReq/CUDEmployeeIssueLetter", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({
                                text: "Document successfully issued.", imageUrl: "assets/img/check.png",
                                showCloseButton: true, width: 300, height: 200
                            });
                            $scope.Cancel();
                        }
                        else {
                            swal({ text: "Document not issued.", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }
                    });
                    $scope.search_data();
                    datasend = [];
                    $scope.file_form = "";
                    $scope.table = true;
                    $scope.display = false;                    
                }

            }

            //Image Upload Section//

            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };
            var file_name = '';
            var file_name_name = '';
            $scope.file_doc = [];

            $scope.file_changed = function (element) {
                debugger;
                file_name = '';
                file_name_name = '';
                var img_load = '';
                $scope.flag = false;
                var rate_name, rate_point;

                var v = new Date();
                $scope.grid2 = true;
                if ($scope.file_doc == undefined) {
                    $scope.file_doc = '';
                }

                if ($scope.file_doc.length > 14) {
                    swal('', 'Upload maximum 15 files.');
                    return;
                } 

                file_name = $scope.temp.sims_certificate_name + '_' + $scope.temp.sims_certificate_requested_by + '_' + $scope.temp.sims_certificate_request_number;
                file_name = file_name.replace(/ /g, "");
                    $scope.photofile = element.files[0];
                    file_name_name = $scope.photofile.name;

                    var len = 0;
                    len = file_name_name.split('.');
                    var fortype = file_name_name.split('.')[len.length - 1];
                    var formatbool = false;
                    if (fortype == 'pdf' || fortype == 'doc' || fortype == 'jpeg' || fortype == 'docx' || fortype == 'png') {
                        formatbool = true;
                    }


                    if (formatbool == true) {
                        if ($scope.photofile.size > 5097152) {
                            swal('', 'File size limit not exceed upto 5 MB.')
                        } else {
                            $scope.uploading_doc1 = false;
                            $scope.photo_filename = ($scope.photofile.type);


                            var reader = new FileReader();
                            reader.onload = function (e) {
                                $scope.$apply(function () {

                                    $scope.prev_img = e.target.result;
                                    

                                    var request = {
                                        method: 'POST',
                                        url: ENV.apiUrl + 'api/EmployeeLetterReq/upload?filename=' + file_name + '.' + fortype + "&location=" + "Images/Requestdoc",
                                        data: formdata,
                                        headers: {
                                            'Content-Type': undefined
                                        }
                                    };
                                    $http(request).success(function (d) {

                                        debugger;
                                        var t = {
                                            sims508_doc: d,
                                            //sims_doc_line: sims_doc_line,
                                            sims_certificate_doc_path: file_name + '.' + fortype,
                                        }
                                        $scope.file_doc.push(t);
                                        $scope.file_form = $scope.file_doc[0].sims_certificate_doc_path;
                                        v.checked = false;
                                        $scope.row1 = '';
                                        $('tr').removeClass("row_selected");

                                        //main.checked = false;
                                        //$scope.row1 = '';
                                        //$('tr').removeClass("row_selected");
                                        $scope.uploading_doc1 = true;

                                    });

                                });
                            };
                            reader.readAsDataURL($scope.photofile);
                            //$scope.file_form = $scope.file_doc[0].sims_certificate_doc_path;
                        }
                    } else {
                        swal('', '.' + fortype + ' File format not allowed.');
                    }
                
                //$scope.ratingArray.sims_survey_rating_img_path = img_load;
            }

            $scope.CancelFileUpload = function (idx) {
                $scope.images.splice(idx, 1);

            };

        }])
})();