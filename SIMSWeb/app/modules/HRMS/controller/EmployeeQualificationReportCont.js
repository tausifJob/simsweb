﻿(function () {
    'use strict';
    var del = [];
    var main;
    var main1;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeQualificationReportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var d = new Date();
            var n = d.getMonth() + 1;
            $scope.edt = {};
            $scope.temp = {};
            $scope.EmployeeDetails = [];
            $scope.fi_date = true;
            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);


            $scope.select_date_publish = function (date) {
                debugger;

                $scope.publish_date = date;
            }
            $(function () {
                // $('#grade_box').multipleSelect({ width: '100%' });
                // $('#section_box').multipleSelect({ width: '100%' });
                $('#department_box').multipleSelect({ width: '100%' });
                $('#qualification_box').multipleSelect({ width: '100%' });

            });

            $http.get(ENV.apiUrl + "api/Qualification_Report/getcompany").then(function (res1) {
                debugger;
                $scope.comp_name= res1.data;
                if (res1.data.length > 0) {
                    $scope.temp.comp_code = res1.data[0].comp_code;
                    $scope.getAccYear(res1.data[0].comp_code);
                  
                }
            });

            $scope.getAccYear = function (comp_code) {
                debugger;
                $http.get(ENV.apiUrl + "api/Qualification_Report/getAcademicYear?comp_code=" + $scope.temp.comp_code).then(function (res1) {
                    $scope.academic_year = res1.data;
                    if (res1.data.length > 0) {
                        for (var i = 0; i < $scope.academic_year.length; i++) {
                            if ($scope.academic_year[i].sims_academic_year_status == 'C') {
                                $scope.temp['sims_academic_year'] = $scope.academic_year[i].sims_academic_year;
                               
                            }
                        }
                    }
                    $scope.getdept(res1.data[0].comp_code, res1.data[0].sims_academic_year);
                });

            }


            $scope.getdept = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/Qualification_Report/getDepartment?comp_code=" + $scope.temp.comp_code + "&acad_year=" + $scope.temp.sims_academic_year).then(function (res1) {
                    $scope.dept = res1.data;
                    setTimeout(function () {
                        $('#department_box').change(function () {

                        })

                            .multipleSelect({
                                width: '100%'
                            });
                        //$("#subject_box").multipleSelect("checkAll");
                    }, 1000);

                });
            }

            $http.get(ENV.apiUrl + "api/Qualification_Report/getQualification").then(function (res1) {
                debugger
                $scope.qualification = res1.data;
                setTimeout(function () {
                    $('#qualification_box').change(function () {

                    })

                        .multipleSelect({
                            width: '100%'
                        });
                    //$("#subject_box").multipleSelect("checkAll");
                }, 1000);
            });
        

         

            $scope.Show_data = function () {               

                debugger
                if ($scope.temp.emp_name_id == undefined || $scope.temp.emp_name_id == "") {
                    $scope.temp.emp_name_id = "";
                }                
                $http.get(ENV.apiUrl + "api/Qualification_Report/getQualification_Details?comp_code=" + $scope.temp.comp_code + "&qcode=" + $scope.temp.pays_qualification_code + "&de_code=" + $scope.temp.codp_dept_no + "&emp_name_id=" + $scope.temp.emp_name_id).then(function (res1) {
                    if (res1.data.length > 0) {
                        $scope.report_data = res1.data;
                        
                    }
                    else {
                        swal({ title: "Alert", text: " Data is not Available for selected criteria", showCloseButton: true, width: 300, height: 200 });
                        $scope.report_data = [];
                    }
                    //angular.forEach($scope.report_data, function (f) {
                    //    f.sims_bell_lecture_per_week = parseFloat(f.sims_bell_lecture_per_week);
                    //});
                    console.log($scope.report_data, $scope.temp.sims_subject_type_code);
                });
            }

            $scope.clear = function () {
                debugger;
                $scope.temp.pays_qualification_code = '';

                try {
                    $('#department_box').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#qualification_box').multipleSelect('uncheckAll');
                } catch (e) {

                    try {
                        $('#Select6').multipleSelect('uncheckAll');
                    } catch (e) {

                    }

                }
                $scope.temp.codp_dept_no = '';
                $scope.temp.emp_name_id = '';
                $scope.report_data = [];




            }


            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.colsvis = false;


            $scope.exportData = function () {
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('rpt_data').innerHTML], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                        });
                        saveAs(blob, "Employee Qualification Report.xls");
                        $scope.colsvis = false;

                    }

                });
            };

            $scope.print = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Print?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                       
                            var docHead = document.head.outerHTML;
                            var printContents = document.getElementById('rpt_data').outerHTML;
                            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
                            var newWin = window.open("", "_blank", winAttr);
                            var writeDoc = newWin.document;
                            writeDoc.open();
                            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                            writeDoc.close();
                            newWin.focus();
                            $scope.colsvis = false;
                          
                    }
                    $scope.colsvis = false;
                });
               };



            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);


        }])
})();