﻿


(function () {
    'use strict';
    var simsController = angular.module('sims.module.HRMS');
    var stud_name;
    var mob_no;
    var reltn_id;
    var enroll_no;
    var person_nme;
    var img_upload;
    var sec_code;
    var cur_code;
    var grade_code;
    var selectedDetail = [];
    var aca_year;
    // var loadFile;
    var sHtmlContent = "";
    var iData, curriculum, Acc_year, Grade_code, Section_code;
    var a = {};
    var opr;
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('EmergencyPickupCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$locale', '$compile', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $locale, $compile, $http, ENV) {

           

           
            //CUR
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                // console.log("This is curiculum = ", AllCurr.data);
                $scope.curriculum = AllCurr.data;
                $scope.temp = {
                    'sims_cur_code': $scope.curriculum[0].sims_cur_code
                }
                $scope.getAccYear($scope.temp.sims_cur_code);

            });

            $scope.loadFile = function (event) {
                console.log(upimage);
                var output = document.getElementById('output');
                output.src = URL.createObjectURL(event.target.files[0]);
            };


            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = {
                        'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                        'sims_academic_year': $scope.Acc_year[0].sims_academic_year
                    }
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                });
            }

            //Grade
            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    $scope.temp = {
                        'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                        'sims_academic_year': $scope.Acc_year[0].sims_academic_year,
                        'sims_grade_code': $scope.Grade_code[0].sims_grade_code
                    }
                    $scope.getSection($scope.temp.sims_cur_code, $scope.temp.sims_grade_code, $scope.temp.sims_academic_year)
                });
            }

            //Section
            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                });
            }

            $scope.getData = function (curCode, accYear, gradeCode, secCode) {
                a = { opr: 'A', aca_year: accYear, cur_code: curCode, grade_code: gradeCode, sec_code: secCode };
                $http.post(ENV.apiUrl + "api/EmergencyPickup/EmergencyPickupDataImport", a).then(function (res) {
                    // console.log("res data ", res.data.table);
                    $scope.showTable = true;
                    $scope.iData = res.data.table;
                    $scope.Relations = res.data.table1;
                    $($scope.iData).each(function (key, val) {
                        val.isDirty = false;
                    });

                });
            }


            $scope.getSelectedData = function () {
                $scope.selectedDetail = [];
                $($scope.iData).each(function (key, val) {
                    if (val.isDirty == true) {
                        $scope.selectedDetail.push(val);
                    }
                });
            }

            $scope.onlyNumbers1 = function (event) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            //delete
            $scope.deleteTable = function (curCode, accYear, gradeCode, secCode) {
                debugger

                var deletefin = [];
                $scope.getSelectedData();
                for (var i = 0; i < $scope.selectedDetail.length; i++) {
                  
                  
                        $scope.flag = true;
                        var data = {
                            cur_code: $scope.selectedCur,
                            enroll_num: $scope.selectedDetail[i].sims_enroll_number,
                            img_path: $scope.selectedDetail[i].sims_pickup_contact_image_path,
                            mobile_num: $scope.selectedDetail[i].sims_pickup_contact_no1,
                            pickup_name: $scope.selectedDetail[i].sims_pickup_name,
                            relation_id: $scope.selectedDetail[i].sims_pickup_relation_id,
                        }
                        deletefin.push(data);
                     
                }

              
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete? Delete this field Related all record",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            debugger;
                            $http.post(ENV.apiUrl + "api/EmergencyPickup/EmergencyPickupDataDeletion", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        $scope.getData();
                                        if (isConfirm) {

                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                            }

                                            $scope.CheckAllChecked();
                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                                else {
                                    swal({ text: "Record Not Deleted", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main.checked = false;
                                            $('tr').removeClass("row_selected");
                                        }
                                    });
                                }

                            });
                            deletefin = [];
                        }
                        else {
                            for (var i = 0; i < $scope.selectedDetail.length; i++) {
                                var v = document.getElementById($scope.selectedDetail[i].isDirty + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }



            //$scope.deleteTable = function () {
            //    debugger;

            //    alert("hey");
            //    console.log(selectedDetail);
            //    $http.post(ENV.apiUrl + "api/EmergencyPickup/EmergencyPickupDataDeletion", selectedDetail).then(function (res) {
            //    });
            //}

        
            // save and Update
            $scope.saveTable = function (curCode, accYear, gradeCode, secCode) {
                // console.log("This is hahahah = ", selectedDetail)
                debugger;
                

                var data1 = [];
                $scope.getSelectedData();
                for (var i = 0; i < $scope.selectedDetail.length; i++) {
                    if ($scope.selectedDetail[i].sims_pickup_contact_no1 == undefined) {
                        swal({ text: "Plz insert mobile number for " + $scope.selectedDetail[i].sims_enroll_number, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        return;
                    }
                    var data = {
                        cur_code: $scope.selectedCur,
                        enroll_num: $scope.selectedDetail[i].sims_enroll_number,
                        img_path: $scope.selectedDetail[i].sims_pickup_contact_image_path,
                        mobile_num: $scope.selectedDetail[i].sims_pickup_contact_no1,
                        pickup_name: $scope.selectedDetail[i].sims_pickup_name,
                        relation_id: $scope.selectedDetail[i].sims_pickup_relation_id,
                    }
                    
                    
                    data1.push(data);
                }

                $http.post(ENV.apiUrl + "api/EmergencyPickup/EmergencyPickupDataUpdation", data1).then(function (res) {
                    console.log("res.date value = ", res.data);

                    $scope.msg1 = res.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        debugger;
                        $scope.getData(curCode, accYear, gradeCode, secCode);
                    }                                                        
                    else {
                        //swal({ title: "Alert", text: "Record NOt Inserted ", showCloseButton: true, width: 300, height: 200 });
                        swal('', 'Record not inserted');

                    }
                
                });
            }
 
            // get image/pic
            $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmergencyImages/';

            //image

            $scope.image_click = function (obj) {
                $scope.ImgLoaded = true;
                $scope.old_obj = obj;// angular.copy(obj);
                 formdata = new FormData();
            }


            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {
               // var formdata = new FormData();
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };

            $scope.UploadImageModal = function (str) {
                 formdata = new FormData();
                $scope.data['sims_pickup_contact_image_path'] = "";
                $scope.prev_img = "";

                imagename = str.sims_enroll_number;
                $('#myModal').modal('show');
            } 
            $scope.file_changed = function (element) {
                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                   //     $scope.prev_img = e.target.result;
                        $scope.old_obj['prev_img'] = e.target.result;
                        //$scope.temp.em_img = element.files[0].name;
                        //console.log($scope.temp.em_img);
                        if ($scope.ImgLoaded == true) {
                            var request = {
                                method: 'POST',
                                url: ENV.apiUrl + '/api/file/upload?filename=' + $scope.old_obj.sims_enroll_number + "&location=" + "/EmergencyImages",
                                data: formdata,
                                headers: {
                                    'Content-Type': undefined
                                }
                            };
                            $http(request).success(function (d) {
                                //alert(d);
                                $scope.old_obj['sims_pickup_contact_image_path'] = d;
                            });
                        }
                    });
                };
                reader.readAsDataURL(photofile);
            };
            $(document).ready(function () {
                $scope.showTable = false;
                $scope.getData();
            });
        }]);

})();













