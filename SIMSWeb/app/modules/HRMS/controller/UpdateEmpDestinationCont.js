﻿(function () {
    'use strict';
    var opr = '';
    var overtimecode = [];
    var main;
    var data1 = [];
    var data = [];
    var simsController = angular.module('sims.module.HRMS');
    simsController.controller('UpdateEmpDestinationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.table1 = true;
            $scope.operation = false;
            $scope.editmode = false;
            $scope.edt = {};
            $scope.rgvtbl = false;
            $scope.buzy = false;



            $http.post(ENV.apiUrl + "api/UEmpDestination/Department").then(function (Department) {
                $scope.Department = Department.data;

            });

            $http.post(ENV.apiUrl + "api/UEmpDestination/Designation").then(function (Designation) {
                $scope.Designation = Designation.data;

            });

            $http.post(ENV.apiUrl + "api/UEmpDestination/Grade").then(function (Grade) {
                $scope.Grade = Grade.data;

            });

            $http.post(ENV.apiUrl + "api/UEmpDestination/Destination").then(function (Destination) {
                $scope.Destination = Destination.data;

            });

            $scope.Show_data = function () {
                if ($scope.edt.dept_code == undefined)
                    $scope.edt.dept_code = '';
                if ($scope.edt.desg_code == undefined)
                    $scope.edt.desg_code = '';
                if ($scope.edt.grade_code == undefined)
                    $scope.edt.grade_code = '';
                if ($scope.edt.emp_id == undefined)
                    $scope.edt.emp_id = '';


                $scope.buzy = true;
                var data = {
                    emp_id: $scope.edt.emp_id,
                    dept_code: $scope.edt.dept_code,
                    desg_code: $scope.edt.desg_code,
                    grade_code: $scope.edt.grade_code
                }
                $http.post(ENV.apiUrl + "api/UEmpDestination/EmployeeDest", data).then(function (EmployeeDest) {
                    $scope.EmployeeDest = EmployeeDest.data;
                    if ($scope.EmployeeDest.length > 0) {
                        $scope.rgvtbl = true;
                    }
                    $scope.buzy = false;


                });
            }


            $scope.reset_form = function () {
                $scope.EmployeeDest = '';
                $scope.edt.dept_code = '';
                $scope.edt.desg_code = '';
                $scope.edt.grade_code = '';
                $scope.edt.emp_id = '';
                $scope.edt.dest_code = ''
                $scope.rgvtbl = false;
                main = document.getElementById('mainchk');
                main.checked = false;


                $http.post(ENV.apiUrl + "api/UEmpDestination/Department").then(function (Department) {
                    $scope.Department = Department.data;

                });

                $http.post(ENV.apiUrl + "api/UEmpDestination/Designation").then(function (Designation) {
                    $scope.Designation = Designation.data;

                });

                $http.post(ENV.apiUrl + "api/UEmpDestination/Grade").then(function (Grade) {
                    $scope.Grade = Grade.data;

                });

                $http.post(ENV.apiUrl + "api/UEmpDestination/Destination").then(function (Destination) {
                    $scope.Destination = Destination.data;

                });

                $scope.buzy = false;
            }

            $scope.UpdateDest1 = function () {
                debugger;
                if ($scope.edt.dest_code == '' || $scope.edt.dest_code == undefined) {
                    swal('', 'Please select destination.');
                }
                else {
                    var empl = '';
                    for (var i = 0; i < $scope.EmployeeDest.length; i++) {
                        var v = document.getElementById($scope.EmployeeDest[i].emp_id);
                        if (v.checked == true)  {
                            empl = empl + $scope.EmployeeDest[i].emp_id + ',';
                        }
                    }
                    if (empl == '') {
                        swal('', 'Please select at least one employee.');
                    }
                    else {
                        var data = {
                            emp_id: empl,
                            dest_code:$scope.edt.dest_code
                        }
                        $http.post(ENV.apiUrl + "api/UEmpDestination/UpdateDest", data).then(function (UpdateDest) {
                            $scope.UpdateDest = UpdateDest.data;
                            if ($scope.UpdateDest)
                                swal('', 'Destination updated successfully.');
                            else
                                swal('', 'Destination not updated.');
                        });
                        $scope.rgvtbl = false; $scope.buzy = true;
                        $scope.reset_form();
                    }
                }
            }

            $scope.check = function () {
                main = document.getElementById('mainchk');

                debugger;
                for (var i = 0; i < $scope.EmployeeDest.length; i++) {
                    var v = document.getElementById($scope.EmployeeDest[i].emp_id);
                    if (main.checked == true) {
                        v.checked = true;

                        $('tr').addClass("row_selected");
                    }
                    else {
                        v.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.CheckAny = function (ed_sr_code) {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }


            }

            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);




        }])
})();





