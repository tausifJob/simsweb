﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('PayrollEmployee_DPSDCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.grid1 = false;
            $scope.temp = {};
            $scope.salCopy = [];
            $scope.showForDPSMIS = false;

            var schoolArray = ['dpsmis'];

            if (schoolArray.includes($http.defaults.headers.common['schoolId'])) {
                $scope.showForDPSMIS = true;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
                //format:'dd-mm-yyyy'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


            $http.get(ENV.apiUrl + "api/PaysGradeChange/Get_gc_company_Code").then(function (compcode) {
                
                $scope.comp_code = compcode.data;
                $scope.temp['gc_company_code'] = $scope.comp_code[0].gc_company_code;
                $scope.getdept($scope.temp['gc_company_code']);
                $scope.getdesg($scope.temp['gc_company_code']);
            });

            $http.get(ENV.apiUrl + "api/Dps/HRMS/getEmpData").then(function (res) {
                $scope.obj = res.data;                
            });
           
            $scope.getdept = function (dept) {

                $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/Get_dept_Code?company_code=" + dept).then(function (Department) {
                    $scope.dept = Department.data;
                });
            }

            $scope.getdesg = function (design) {
                $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/Get_designation_Code?company_code=" + design).then(function (Designation) {
                    $scope.designation = Designation.data;
                });
            }

            $scope.submit = function () {
               
                $scope.busy = true;
                $scope.grid1 = true;
                $scope.ImageView = false;
                if ($scope.temp.gc_company_code == undefined || $scope.temp.gc_company_code == '') {
                    $scope.temp.gc_company_code = '';
                }
                if ($scope.temp.em_desg_code == undefined || $scope.temp.em_desg_code == '') {
                    $scope.temp.em_desg_code = '';
                }
                if ($scope.temp.dg_code == undefined || $scope.temp.dg_code == '') {
                    $scope.temp.dg_code = '';
                }
                if ($scope.temp.em_number == undefined || $scope.temp.em_number == '') {
                    $scope.temp.em_number = '';
                }
                if ($scope.temp.gr_code == undefined || $scope.temp.gr_code == '') {
                    $scope.temp.gr_code = '';
                }
                $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/GetPayrollEmployee?com_code=" + $scope.temp.gc_company_code + '&dept_code=' + $scope.temp.dg_code + '&deg_code=' + $scope.temp.em_desg_code + '&empid=' + $scope.temp.em_number + '&gr_code=' + $scope.temp.gr_code).then(function (Payroll) {

                    $scope.filteredTodos = Payroll.data;
                    $scope.totalItems = $scope.filteredTodos.length;
                    $scope.todos = $scope.filteredTodos;
                    //$scope.makeTodos();
                    $scope.busy = false;
                    $scope.page1 = true;                    
                    if (Payroll.data.length > 0) { }
                    else {
                        $scope.ImageView = true;
                    }
                });
            }

            //DATA Cancel
            $scope.Cancel = function () {
                //$scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                //$scope.grid1 = false;
                //$scope.filteredTodos = '';
                //$scope.todos.slice = '';
                //$scope.page1 = false;
                $('#myModal').modal('hide');
            }

            //DATA Reset
            $scope.reset = function () {
                debugger
                $scope.temp = {};
                $scope.temp['gc_company_code'] = $scope.comp_code[0].gc_company_code;
                $scope.getdept($scope.temp['gc_company_code']);
                $scope.getdesg($scope.temp['gc_company_code']);                
                $scope.j = [];
                $scope.filteredTodos = '';
                $scope.todos.slice = '';
                $scope.page1 = false;
                $scope.table = true;
                $scope.grid1 = false;
                $scope.grid2 = false;
                $scope.display = true;
                $("#from_date, #to_date").kendoDatePicker({
                    format: "dd-MM-yyyy",
                    value:''
                });
            }
            debugger
            //DATA EDIT
            $scope.edit = function (str) {
                debugger        
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = true;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.number = true;
                $scope.company = true;
                $scope.showTotal = false;
                $scope.divcode_readonly = true;
                $scope.grid2 = false;
                $scope.grid3 = false;
                $('#myModal').modal('show');
                $scope.save_btn = false;
                $scope.temp.em_number = str.em_number;               
                $scope.Employee_name = str.sh_emp_name;
                $scope.temp.date_of_joining = str.pe_doj;
                var dt = new Date();
                //$scope.pa_effective_from = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
                //$scope.pa_effective_from = dt.getDate() + "-" + (dt.getMonth() + 1) + "-" + dt.getFullYear();
                $scope.pa_effective_from = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                $scope.Show();
            }

            $scope.Closehistory = function () {
                $scope.grid3 = false;
                $scope.grid2 = true;
            }

            //Show
            $scope.Show = function () {
                debugger;
                $scope.showTotal = true;
                $scope.grid2 = true;
                $scope.grid3 = false;
                $scope.save_btn = true;
                $scope.busy = true;
                $scope.Employee_Details = $scope.temp.em_number;
                $http.post(ENV.apiUrl + "api/PayrollEmployeeDPSD/RemaingPaycode?sd_number=" + $scope.temp.em_number + '&start=' + $scope.pa_effective_from + '&end=' + $scope.rdp_effective_upto).then(function (RemaingPaycode) {
                    $scope.RemaingPaycode = RemaingPaycode.data;

                });

                $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/GetSalaryGeneratedForSelectedMonthOrNot?sd_number=" + $scope.temp.em_number + '&start=' + $scope.pa_effective_from + '&end=' + $scope.rdp_effective_upto).then(function (Sal) {
                    $scope.sal = Sal.data;
                    $scope.salCopy = angular.copy($scope.sal);
                    $scope.total = 0;
                    $scope.deductionTotal = 0;
                    $scope.totalGross = 0;
                    for (var j = 0; j < Sal.data.length; j++) {
                        if(Sal.data[j].pc_earn_dedn_tag == "E"){
                            $scope.total = $scope.total + Sal.data[j].pd_amount;
                        }
                        if (Sal.data[j].pc_earn_dedn_tag == "D") {
                            $scope.deductionTotal = $scope.deductionTotal + Sal.data[j].pd_amount;
                        }                        
                    }

                    $scope.calculateTotal = function (info) {                        
                        if (info.pd_amount == undefined || info.pd_amount == "" || info.pd_amount == null || isNaN(info.pd_amount)) {
                            info.pd_amount = 0;
                            $scope.total = 0;
                            $scope.deductionTotal = 0;
                            for (var j = 0; j < Sal.data.length; j++) {
                                if (Sal.data[j].pc_earn_dedn_tag == "E") {
                                    $scope.total = parseFloat($scope.total) + parseFloat(Sal.data[j].pd_amount);
                                }
                                if (Sal.data[j].pc_earn_dedn_tag == "D") {
                                    $scope.deductionTotal = parseFloat($scope.deductionTotal) + Sal.data[j].pd_amount;
                                }
                            }
                        }
                        else {
                            $scope.total = 0;
                            $scope.deductionTotal = 0;
                            for (var j = 0; j < Sal.data.length; j++) {
                                if (Sal.data[j].pc_earn_dedn_tag == "E") {
                                    $scope.total = parseFloat($scope.total) + parseFloat(Sal.data[j].pd_amount);
                                }
                                if (Sal.data[j].pc_earn_dedn_tag == "D") {
                                    $scope.deductionTotal = parseFloat($scope.deductionTotal) + Sal.data[j].pd_amount;
                                }
                            }
                        }
                    }
                    
                });

                //DATA SAVE INSERT
          

                $scope.History = function () {
                    $http.post(ENV.apiUrl + "api/PayrollEmployeeDPSD/HistoryPaycode?sd_number=" + $scope.temp.em_number).then(function (HistoryPaycode) {
                       // $scope.HistoryPaycode = HistoryPaycode.data;
                        debugger;
                        $scope.HistoryPaycode = [];
                        if(HistoryPaycode.data.length > 0) {
                            $scope.grid2 = true;
                            $scope.grid3 = true;

                            angular.forEach(HistoryPaycode.data, function (value, key) {

                                var obj = {
                                    paycode_name: value.paycode_name,
                                    inc_fixed_amout: $scope.numberWithCommas(parseFloat(value.inc_fixed_amout).toFixed($scope.comp_curcy_dec)),
                                    pa_remark: value.pa_remark,
                                    pa_effective_from: value.pa_effective_from,
                                    pa_effective_upto: value.pa_effective_upto,
                                    pc_earn_dedn_tag_name: value.pc_earn_dedn_tag_name,
                                    pa_status: value.pa_status,
                                    paycode_code: value.paycode_code
                                }                                
                                $scope.HistoryPaycode.push(obj);
                            });
                            
                        }
                        else
                            swal('', 'No history found.');
                    });


                }

                $scope.UpdateStatus = function (obj, pa_status) {
                    swal({
                        title: 'Alert',
                        text: "If you doing pay code Inactive.\n You can't make the code Active again",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/PayrollEmployeeDPSD/PayCodeStatusUpdate?pa_number=" + $scope.temp.em_number + '&pa_pay_code=' + obj.paycode_code + '&pa_effective_from=' + obj.pa_effective_from + '&pa_effective_upto=' + obj.pa_effective_upto + '&pa_status=' + pa_status).then(function (res) {
                                $scope.updatestatus = res.data;
                                console.log("res", res);
                                if (res.data == true) {
                                    swal({ title: "Alert", text: "Pay code status updated.", width: 380, height: 200 });
                                }
                                else if (res.data == false) {
                                    swal({ title: "Alert", text: "Pay code status not updated", width: 380, height: 200 });
                                }
                                else {
                                    swal({ title: "Alert", text: res.data.Message, width: 380, height: 200 });
                                }

                                $scope.History();
                            });
                        }
                    });

                }

              
                debugger
                $scope.Reset = function () {
                    $scope.temp.pd_pay_code = '';
                    $scope.pd_amount = '';
                    $scope.pa_remark = '';
                    var dt = new Date();                    
                    //$scope.pa_effective_from = dt.getDate() + "-" + (dt.getMonth() + 1) + "-" + dt.getFullYear();
                    $scope.pa_effective_from = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                    $scope.rdp_effective_upto = '';

                }
                
                var datasend = [];
                $scope.Save = function () {
                    debugger;
                    datasend = [];


                    //for (var i = 0; i < $scope.sal.length; i++) {
                    //    if ($scope.sal[i].ischange == true) {
                    //        $scope.sal[i].em_number = $scope.temp.em_number;
                    //        $scope.sal[i].em_company_code = $scope.temp.gc_company_code;
                    //        if ($scope.sal[i].pd_amount != null && $scope.sal[i].pa_effective_from == null) {
                    //            $scope.sal[i].pa_effective_from = $scope.pa_effective_from;
                    //        }
                    //        datasend.push($scope.sal[i]);
                    //    }
                    //}
                    for (var i = 0; i < $scope.sal.length; i++) {
                        for (var j = 0; j < $scope.salCopy.length; j++) {
                            if ($scope.sal[i].ischange == true) {

                                if (new Date($scope.sal[i].code) < new Date($scope.sal[i].pa_effective_from)) {
                                    swal({ title: "Alert", text: "Effective upto date cannot be less than from date", width: 380, height: 200 });
                                    return;
                                }
                                //if (new Date($scope.sal[i].pa_effective_from) < new Date($scope.salCopy[i].pa_effective_from)) {
                                //    swal({ title: "Alert", text: "Effective from date cannot be less than previous date", width: 380, height: 200 });
                                //    return;
                                //}
                                $scope.sal[i].em_number = $scope.temp.em_number;
                                $scope.sal[i].em_company_code = $scope.temp.gc_company_code;
                                if ($scope.sal[i].pd_amount != null || $scope.sal[i].pd_amount != undefined || $scope.sal[i].pd_amount != "") {
                                    if ($scope.sal[i].pa_effective_from == null || $scope.sal[i].pa_effective_from == undefined || $scope.sal[i].pa_effective_from == "") {
                                        $scope.sal[i].pa_effective_from = $filter('date')(new Date($scope.pa_effective_from), 'dd-MM-yyyy');
                                    }
                                }
                                datasend.push($scope.sal[i]);
                                break;
                            }
                        }
                    }

                    if (datasend.length > 0) {
                        $http.post(ENV.apiUrl + "api/PayrollEmployeeDPSD/InsertUpdatePaysPayable", datasend).then(function (Sal) {
                            debugger
                            $scope.msg1 = Sal.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record updated successfully", width: 300, height: 200 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ title: "Alert", text: "Record not updated. ", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }

                            $scope.getgrid();
                            $scope.Cancel();

                        });
                    }
                    else {
                        $scope.submit();
                        $scope.Cancel();
                        
                       // swal({ title: "Alert", text: "Change at least one record to update", width: 300, height: 200 });
                    }
                   
                }

                $scope.SetIschange = function (info) {
                    info.ischange = true;                    
                }
            }


            $scope.checkFromDate = function (info) {
                if (moment(info.pa_effective_from, "DD-MM-YYYY").format('YYYY-MM-DD') < moment($scope.temp.date_of_joining, "DD-MM-YYYY").format('YYYY-MM-DD')) {
                    swal('', 'Effective from date can not be less than date of joining.');
                    info.pa_effective_from = '';
                }
            };

            $scope.checkEndDate = function (info) {
                if (moment(info.code, "DD-MM-YYYY").format('YYYY-MM-DD') < moment(info.pa_effective_from, "DD-MM-YYYY").format('YYYY-MM-DD')) {
                    swal('', 'Effective upto date can not be less than effective from date.');
                    info.code = '';
                }
            };

            $http.get(ENV.apiUrl + "api/updatepaysheet/GetComapnyCurrencyDetails").then(function (res) {
                $scope.currencyDetails = res.data;
                if ($scope.currencyDetails.length > 0) {
                    $scope.comp_curcy_dec = $scope.currencyDetails[0].comp_curcy_dec;
                }
                else {
                    $scope.comp_curcy_dec = 2;
                }
            });

            $scope.ADD_NEW = function () {
                debugger;
              
                var abc = [];
                
                if ($scope.temp.pd_pay_code == '' || $scope.temp.pd_pay_code == undefined) {
                    swal('', 'Please select paycode.');
                }
                else if ($scope.pd_amount == '' || $scope.pd_amount == undefined)  {                     
                        swal('', 'Please enter amount.');
                    }
                    else if ($scope.pa_effective_from == '' || $scope.pa_effective_from == undefined) {
                        swal('', 'Please enter effective from date.');
                    }
                    else if (moment($scope.pa_effective_from, "DD-MM-YYYY").format('YYYY-MM-DD') < moment($scope.temp.date_of_joining, "DD-MM-YYYY").format('YYYY-MM-DD')) {                        
                        swal('', 'Effective from date can not be less than date of joining.');
                    }
                    else {
                        var data = {
                            em_number: $scope.Employee_Details,
                            em_company_code: '01',
                            pa_effective_from: $scope.pa_effective_from,
                            code: $scope.rdp_effective_upto,
                            pd_pay_code: $scope.temp.pd_pay_code,
                            pd_amount: $scope.pd_amount,
                            pa_remark: $scope.pa_remark
                        }
                        abc.push(data);
                        console.log("pa_effective_from", $scope.pa_effective_from);
                        console.log("pa_effective_from", abc);

                        $http.post(ENV.apiUrl + "api/PayrollEmployeeDPSD/InsertUpdatePaysPayable", abc).then(function (pqrs) {
                            debugger
                            $scope.msg2 = pqrs.data;
                            if ($scope.msg2 == true) {
                                swal({ title: "Alert", text: "Record Added Successfully", width: 300, height: 200 });                                
                                $scope.getgrid();

                                setTimeout(function () {
                                    $(document).ready(function () {
                                        $("#from_date, #to_date").kendoDatePicker({
                                            format: "dd-MM-yyyy",
                                            value: ''
                                        });
                                    });
                                },700);
                                $scope.Reset();


                            }
                            else if ($scope.msg2 == false) {
                                swal({ title: "Alert", text: "Record Not Added. " , width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg2)
                            }
                        });
                    }
                }
            
            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/GetSalaryGeneratedForSelectedMonthOrNot?sd_number=" + $scope.temp.em_number + '&start=' + $scope.pa_effective_from + '&end=' + $scope.rdp_effective_upto).then(function (Sal) {
                    $scope.sal = Sal.data;
                    $scope.salCopy = angular.copy($scope.sal);
                    $scope.total = 0;
                    for (var j = 0; j < Sal.data.length; j++) {
                        $scope.total = $scope.total + Sal.data[j].pd_amount;
                    }

                    $scope.calculateTotal = function (info) {
                        
                        if (info.pd_amount == undefined || info.pd_amount == "" || info.pd_amount == null || isNaN(info.pd_amount)) {
                            info.pd_amount = 0;
                            $scope.total = 0;
                            for (var j = 0; j < Sal.data.length; j++) {
                                $scope.total = parseFloat($scope.total) + parseFloat(Sal.data[j].pd_amount);
                            }
                        }
                        else {
                            $scope.total = 0;
                            for (var j = 0; j < Sal.data.length; j++) {
                                $scope.total = parseFloat($scope.total) + parseFloat(Sal.data[j].pd_amount);
                            }
                        }
                    }
                    
                });
            }

            $scope.numberWithCommas = function (x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }

            $scope.onlyNumbers = function (event) {
                debugger
                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };


        }])

})();