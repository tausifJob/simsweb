﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('DesignationWithDepartmentCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;

            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.temp = {};

            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            //$(function () {
            //    $('#Select3').multipleSelect({
            //        width: '100%'
            //    });

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/common/Designation/WithDepartment/getCompanyName").then(function (res1) {
                $scope.display = false;
                $scope.table = true;
                $scope.comp = res1.data;
                $scope.temp['desg_company_code'] = $scope.comp[0].desg_company_code;

            });

            
           

            $http.get(ENV.apiUrl + "api/common/Designation/WithDepartment/getDesigDept").then(function (res1) {
                debugger;
                $scope.obj1 = res1.data;
                console.log($scope.obj1);
                $scope.totalItems = $scope.obj1.length;
                $scope.todos = $scope.obj1;
                $scope.makeTodos();
            });

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;

                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.obj1;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);


                $scope.filteredTodos = $scope.todos.slice(begin, end);

            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj1, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj1;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.desg_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.desg_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.desg_company_code == toSearch) ? true : false;
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.disabled = false;
                    $scope.readonly = true;
                    $scope.table = false;
                    $scope.display = true;
                    $scope.save_btn = true;
                    $scope.Update_btn = false;
                    $scope.Myform.$setPristine();
                    $scope.Myform.$setUntouched();
                    $scope.temp = {};
                    $scope.temp.desg_communication_status = true;
                    $scope.temp['desg_company_code'] = $scope.comp[0].desg_company_code;

                }
            }
           
                $http.get(ENV.apiUrl + "api/common/Designation/WithDepartment/getDepartmentName").then(function (getDepartmentName) {
                debugger;
                $scope.DepartmentName = getDepartmentName.data;
                console.log($scope.DepartmentName);
                  setTimeout(function () {
                      $('#Select3').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });
       

            
                //$http.get(ENV.apiUrl + "api/common/Designation/WithDepartment/getDepartmentName?curcode=" + $scope.edt.sims_cur_code + "&gradecode=" + $scope.edt.sims_grade_code + "&academicyear=" + $scope.edt.sims_academic_year + "&section=" + $scope.edt.sims_section_name + "&login_user=" + user).then(function (getSectionsubject_name) {
                //    $scope.cmb_subject_name = getSectionsubject_name.data;
                //    setTimeout(function () {
                //        $('#Select3').change(function () {
                //            console.log($(this).val());
                //        }).multipleSelect({
                //            width: '100%'
                //        });
                //    }, 1000);
           

            
           /* $http.get(ENV.apiUrl + "api/common/Designation/WithDepartment/getDepartmentNames").then(function (res) {
                debugger;
                $scope.DepartmentName = res.data;
                $scope.temp['codp_dept_no'] = $scope.DepartmentName[0].codp_dept_no
            });*/

            /*       $http.get(ENV.apiUrl + "api/common/Designation/WithDepartment/getRoleofUser?username=" + user).then(function (res) {
                $scope.userRole = res.data;
                if ($scope.userRole > 0) {
                    $scope.updatebtn = false;
                    $scope.readonlytxt = false;
                }
            });


            // getDepartmentName
            $http.get(ENV.apiUrl + "api/common/Designation/WithDepartment/getDepartmentNames").then(function (res) {
                debugger;
                $scope.DepartmentName = res.data;
                $scope.temp['codp_dept_no'] = $scope.DepartmentName[0].codp_dept_no
            });*/

            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger;
               // console.log($scope.savedata);
                if (Myform) {
                    var dep_name = [];
                    try {
                        for (var i = 0; i < $scope.temp.dep_code.length; i++) {
                            if (i == 0) {
                                dep_name = $scope.temp.dep_code[i];
                                debugger;
                                console.log($scope.temp.dep_code[i]);
                            }
                            else {
                                dep_name = dep_name + "," + $scope.temp.dep_code[i];
                                debugger;
                                Console.log($scope.temp.dep_code[i]);



                            }
                        }
                    } catch (e) {

                    }
                    var flag = false;
                    var data = $scope.temp;
                    if ($scope.filteredTodos.length == 0) {
                        flag = true;
                    }
                    else {


                        for (var i = 0; i < $scope.filteredTodos.length; i++) {
                            if ($scope.filteredTodos[i].desg_desc == $scope.temp.desg_desc) {
                                swal({ title: "Alert", text: "Record Already Exist", showCloseButton: true, width: 380, });
                                flag = false;
                            }
                            else {
                                flag = true;
                            }
                        }
                    }
                            
                    if (flag == true){
                        debugger;
                        var data = $scope.temp;
                            data.opr = "I";
                            datasend.push(data);
                       // alert(isvalid);
                            //if (isvalid) {
                               //datasend['dg_company_code'] = $scope.desg_company_code;
                               // datasend['dg_code'] = $scope.desg_code;
                               // datasend['dg_desc'] = $scope.desg_desc;
                               // datasend['dg_arab'] = $scope.desg_arab;
                                //insert_data['dg_code_list'] = $scope.excep_desc;
                               // insert_data['dg_dept_code'] = $scope.dep_code;
                                //datasend['dg_communication_status'] = $scope.desg_communication_status;
                               datasend['dg_dept_name'] = dep_name;
                              debugger;
                                console.log(datasend);

                                 $http.post(ENV.apiUrl + "api/common/Designation/WithDepartment/CUDDesigDept", datasend).then(function (msg) {
                                        $scope.msg1 = msg.data;

                                        if ($scope.msg1 == true) {
                                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                                        }
                                    
                                        else if ($scope.msg1 == false) {
                                            swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                                        }
                                        else {
                                            swal("Error-" + $scope.msg1)
                                        }
                                    

                                $http.get(ENV.apiUrl + "api/common/Designation/WithDepartment/getDesigDept").then(function (res1) {
                                    debugger;
                                     $scope.obj1 = res1.data;
                                    $scope.totalItems = $scope.obj1.length;
                                    $scope.todos = $scope.obj1;
                                    $scope.makeTodos();

                                });
                            });
                            datasend = [];
                            $scope.table = true;
                            $scope.display = false;
                        
                    }

                }
            }


            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.desg_company_code = "";
                $scope.temp.desg_desc = "";
                $scope.temp.desg_arab = "";
                $scope.temp.desg_communication_status = "";
                $scope.temp.dep_code = "";

            }

            $scope.edit = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.readonly = true;
                    $scope.disabled = true;
                    $scope.table = false;
                    $scope.display = true;
                    $scope.save_btn = false;
                    $scope.Update_btn = true;
                    $scope.temp = {
                        desg_company_code: str.desg_company_code,
                        desg_desc: str.desg_desc,
                        desg_codp_dept_no:str.codp_dept_no,
                        desg_code: str.desg_code,
                        desg_arab: str.desg_arab,
                        desg_communication_status: str.desg_communication_status
                    };
                }
            }

            var dataupdate = [];
            $scope.update = function (Myform) {
                if (Myform) {
                    var Udata = $scope.temp;
                    Udata.opr = "U";
                    dataupdate.push(Udata);

                    $http.post(ENV.apiUrl + "api/common/Designation/WithDepartment/CUDDesigDept", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $http.get(ENV.apiUrl + "api/common/Designation/WithDepartment/getDesigDept").then(function (res1) {
                            $scope.obj1 = res1.data;
                            console.log($scope.obj1);
                            $scope.totalItems = $scope.obj1.length;
                            $scope.todos = $scope.obj1;
                            $scope.makeTodos();
                        });

                    });
                    dataupdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    var deletefin = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-" + i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodulecode = ({
                                'desg_code': $scope.filteredTodos[i].desg_code,
                                opr: 'D'
                            });
                            deletefin.push(deletemodulecode);
                        }
                    }
                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $http.post(ENV.apiUrl + "api/common/Designation/WithDepartment/CUDDesigDept?data=", deletefin).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/common/Designation/WithDepartment/getDesigDept").then(function (res1) {
                                                    $scope.obj1 = res1.data;
                                                    $scope.totalItems = $scope.obj1.length;
                                                    $scope.todos = $scope.obj1;
                                                    $scope.makeTodos();
                                                });
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    {
                                                        $scope.row1 = '';
                                                    }
                                                }
                                                $scope.currentPage = true;
                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {

                                                $http.get(ENV.apiUrl + "api/common/Designation/WithDepartment/getDesigDept").then(function (res1) {
                                                    $scope.obj1 = res1.data;
                                                    $scope.totalItems = $scope.obj1.length;
                                                    $scope.todos = $scope.obj1;
                                                    $scope.makeTodos();
                                                });
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    {
                                                        $scope.row1 = '';
                                                    }
                                                }
                                                $scope.currentPage = true;
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });
                            }
                            else {
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById("test-" + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    $scope.currentPage = str;
                }
            }

        }])

})();
