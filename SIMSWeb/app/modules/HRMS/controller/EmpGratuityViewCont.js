﻿(function () {
    'use strict';
    var prv;
    var nxt;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmpGratuityViewCont',
      ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {

          var att_date1, att_date;
          $scope.emptab = false;
          $scope.data1 = [];
          $scope.showForDPSMIS = false;
          $scope.year_data = [];
          $scope.temp = {};
          $scope.edt = {};
          $scope.busyindicator = true;
          $scope.emp_id = "";

          $http.get(ENV.apiUrl + "api/EmpGrtview/getCompanyName").then(function (comp) {
              debugger;
              $scope.comp_data = comp.data;
              $scope.temp.company_code= $scope.comp_data[0].company_code;
              $scope.getDept($scope.temp.company_code);
              $scope.getdesg($scope.temp.company_code);
          });

          var divisionFilter = ['dpsmis'];

          $(function () {
              $('#department').multipleSelect({
                  width: '100%'
              });
          });

          $(function () {
              $('#dept').multipleSelect({
                  width: '100%'
              });
          });

          if (divisionFilter.includes($http.defaults.headers.common['schoolId'])) {
              $scope.showForDPSMIS = true;

              $http.get(ENV.apiUrl + "api/Dps/HRMS/getEmpData").then(function (res) {
                  $scope.obj = res.data;
              });
          }
          
          $scope.getDeptFromDivision = function () {
              $http.get(ENV.apiUrl + "api/generatepayrollDPSMIS/GetDept?division_code=" + $scope.temp.gd_division_code + "&company_code=" + $scope.temp.company_code).then(function (GetDept) {
                  $scope.GetDept = GetDept.data;
                  setTimeout(function () {
                      $('#dept').change(function () {
                      }).multipleSelect({
                          width: '100%'
                      });
                  }, 1000);
              });
          }

          $scope.getDept = function (company_code) {
              
              $http.get(ENV.apiUrl + "api/EmpGrtview/getDeptName?comp_code=" + company_code).then(function (dept) {
                  $scope.dept_data = dept.data;
                  setTimeout(function () {
                      $('#department').change(function () {
                      }).multipleSelect({
                          width: '100%'
                      });
                  }, 1000);
              });
          }
          
          $scope.getdesg = function (company_code) {
              $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/Get_designation_Code?company_code=" + company_code).then(function (Designation) {
                  $scope.designation = Designation.data;
              });
          }

          $http.get(ENV.apiUrl + "api/EmpGrtview/getDesignation").then(function (designation) {
                  $scope.designation_data = designation.data;
              });

          $http.get(ENV.apiUrl + "api/EmpGrtview/getYear").then(function (year) {
              $scope.year_data = year.data;
              $scope.temp.sims_academic_year = $scope.year_data[0].sims_academic_year;
              $scope.getMonthbyYear($scope.year_data[0].sims_academic_year);              
          });

          $timeout(function () {
              $("#fixtable").tableHeadFixer({ 'top': 1 });
          }, 100);

          $scope.propertyName = null;
          $scope.reverse = false;

          $scope.sortBy = function (propertyName) {
              $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
              $scope.propertyName = propertyName;
          };

          $scope.clear = function () {
                  setTimeout(function () {
                      $('#dept').change(function () {
                      }).multipleSelect({
                          width: '100%'
                      });
                  }, 1000);
                  setTimeout(function () {
                      $('#department').change(function () {
                      }).multipleSelect({
                          width: '100%'
                      });
                  }, 1000);

                  $scope.emptab = false;
                  $scope.temp = {};
                  $scope.edt = {};
                  $scope.temp.sims_academic_year = $scope.year_data[0].sims_academic_year;
                  $scope.temp.company_code = $scope.comp_data[0].company_code;
                  $scope.temp.employee_no = "";
                  //$scope.temp.company_code = "";  
                  $scope.temp.dept_code = "";
                  $scope.temp.gr_code = "";
                  $scope.temp.staff_type_code = "";
                  $scope.temp.dg_code = "";                  
                  $scope.temp.sims_academic_month = "";
                  $scope.temp.sort_code = "";
                  $scope.temp.gd_division_code = "";
                  $scope.edt.att_emp_id = "";
                  $scope.all_EmpGrt_Data = [];
                  $scope.postData = [];
                  $scope.temp.yearCode = "";
                  $scope.submitBtnDisabled = false;
                  $scope.busyindicator = true;
                  $scope.emp_id = "";
                  $scope.Myform.$setPristine();
                  $scope.Myform.$setUntouched();                  
              }
           
          $http.get(ENV.apiUrl + "api/generatepayroll/GetCurrentFinYearFromFinsParameter").then(function (GetCurrentFinYearFromFinsParameter) {

              $scope.GetCurrentFinYearFromFinsParameter1 = GetCurrentFinYearFromFinsParameter.data;

              $scope.fin_year = $scope.GetCurrentFinYearFromFinsParameter1[0].fin_year;
              $scope.fin_year_desc = $scope.GetCurrentFinYearFromFinsParameter1[0].fin_year_desc;
          });

          $scope.getMonthbyYear = function (str) {
              debugger;

              $http.get(ENV.apiUrl + "api/OvertimeHour/GetYear_month?financial_year=" + str).then(function (Year_month) {
                  $scope.Year_month = Year_month.data;

                  var d = new Date().getFullYear();
                  var n = new Date().getMonth() + 1;
                  var ym = d + '' + n;
                  //$scope.edt = { year_month: ym };
                  // $scope.edt.yearCode = ym;
                  //$scope.edt1.yearCode = ym;
                  //  $scope.setMonth();

              });

              var starDate = '', endDate = '';
              for (var i = 0; i < $scope.year_data.length; i++) {
                  if ($scope.year_data[i].sims_academic_year == str) {
                      starDate = $scope.year_data[i].sims_academic_year_start_date;
                      endDate = $scope.year_data[i].sims_academic_year_end_date;
                      break;
                  }
              }

              $http.get(ENV.apiUrl + "api/attendance/getTermDetails?startDate=" + starDate + "&endDate=" + endDate).then(function (monthDetails) {
                  $scope.data = monthDetails.data;
              });

             
          }

        

          $scope.setMonth = function () {
              $scope.temp.sims_academic_month = $scope.temp.yearCode.substr(4);
              console.log("year_month", $scope.temp.sims_academic_month);
              console.log("year_month", $scope.temp.yearCode);
          }

          //$scope.getMonthbyYear = function (str) {
          //    $scope.data = [];
          //    debugger
          //    var startdate = new Date();
          //    var enddate = new Date();
             
          //    for (var i = 0; i < $scope.year_data.length; i++) {
          //        if ($scope.year_data[i].sims_academic_year == str) {
          //            startdate = moment($scope.year_data[i].sims_academic_year_start_date,'DD-MM-YYYY').format('YYYY/MM/DD');
          //            enddate = moment($scope.year_data[0].sims_academic_year_end_date,'DD-MM-YYYY').format("YYYY/MM/DD");
          //        }
          //    }

          //    var check_date = startdate;

          //    for (; new Date(check_date) < new Date(enddate) ;) {
          //        check_date = moment(check_date, "YYYY/MM/DD").add('months', 1);
          //        check_date = moment(check_date).format('YYYY/MM/DD');
          //        $scope.data.push({ date: moment(check_date).format('MMMM-YYYY'), dateCode: moment(check_date).format('MM') })
          //    }
          //    console.log($scope.data);
          //}

          //$scope.getMonthbyYear = function (str) {
              
          //        $scope.data = [];
          //        prv = parseInt(str) - 1;
          //        nxt = parseInt(str);

          //        var start_date = new Date();
          //        //start_date = moment(prv + '/08/01').format('YYYY/MM/DD');
          //        start_date = moment(prv + '/03/01').format('YYYY/MM/DD');

          //        var end_date = new Date();
          //        //end_date = moment(nxt + '/08/01').format('YYYY/MM/DD');
          //        end_date = moment(nxt + '/03/01').format('YYYY/MM/DD')
          //        var check_date = start_date;

          //        for (; new Date(check_date) < new Date(end_date) ;) {
          //            check_date = moment(check_date, "YYYY/MM/DD").add('months', 1);
          //            check_date = moment(check_date).format('YYYY/MM/DD');
          //            $scope.data.push({ date: moment(check_date).format('MMMM-YYYY'), dateCode: moment(check_date).format('MM') })
          //        }
          //    }

          //$scope.getDays_in_month = function (sims_month_name, sims_year) {
          //    debugger;
          //    var sims_year;
          //    if (sims_month_name == "09" || sims_month_name == "10" || sims_month_name == "11" || sims_month_name == "12") {
          //        sims_year = parseInt(sims_year) - 1;
          //    }
          //    else {
          //        sims_year = sims_year;
          //    }

          //    $scope.yr_mon = $scope.fin_year + $scope.temp.sims_academic_month;
          //}

          $scope.year_month = $scope.temp.sims_academic_year + $scope.temp.sims_academic_month;

          $scope.getEmpGrt = function (isvalid,comp_code, dept_code, desg_code, emp_no, yrmon)
          {
              
              debugger;
              if (isvalid) {

                  $http.get(ENV.apiUrl + "api/EmpGrtview/getAllGratuityData?comp_code=" + comp_code + "&dept_code=" + dept_code + "&desg_code=" + desg_code + "&emp_no=" + emp_no
                  + "&yearmonth=" + $scope.temp.sims_academic_year + $scope.temp.sims_academic_month + "&grtyearmonth=" + $scope.temp.yearCode).then(function (allEmpGrtData) {
                      debugger;
                      $scope.emp_id = "";
                      $scope.all_EmpGrt_Data = allEmpGrtData.data;
                      $scope.emptab = true;
                      console.log("all_EmpGrt_Data", $scope.all_EmpGrt_Data);
                      $scope.totalExpectedGratuity = 0;
                      $scope.totalCurrentMonthlyGratuity = 0;

                      for (var i = 0; i < $scope.all_EmpGrt_Data.length; i++) {
                          $scope.emp_id = $scope.emp_id + $scope.all_EmpGrt_Data[i].em_login_code + ',';

                          $scope.totalExpectedGratuity = parseFloat($scope.totalExpectedGratuity) + parseFloat($scope.all_EmpGrt_Data[i].total_grt_amount);
                          $scope.totalCurrentMonthlyGratuity = parseFloat($scope.totalCurrentMonthlyGratuity) + parseFloat($scope.all_EmpGrt_Data[i].current_month_amount);
                      }
                  });
              }
              else {
                  swal({ text: 'Please select mandatory fields', width: 380 });
              }

          }


          var dom;
          var count = 0;
          $scope.flag = true;
          var innerTable;

          $scope.expand = function (info, $event) {
              debugger
              var iconId = $event.target.id;
              console.log(info.paycodeList);
              if ($scope.flag == true) {                  
                  $(innerTable).remove();
                  $(dom).remove();
                  $("#" + iconId).addClass("fa fa-minus-circle");
                  var totalPaycodeAmount = 0;
                  //dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                  //    "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                  //    "<tbody>" +
                       
                  //     +
                  //     "<tr ng-repeat='paycode in  info.paycodeList'> <td class='semi-bold'>" + { 'paycode'pa_pay_code } + "</td> <td class='semi-bold'>" + { paycode.pa_amount } + " </td>" + "</tr>" +
                  //     "</tbody>"+
                  //    " </table></td></tr>")
                   
                  dom = "<table class='table table-bordered' cellpadding='5' cellspacing='0' style='background-color:#87cefa38;width:288px;'>";
                  dom = dom + "<tbody>";
                  dom = dom + "<tr>";
                  for (var i = 0; i < info.paycodeList.length; i++) {                      
                      dom = dom + "<td class='semi-bold col-md-8' style='text-align:right'>" + info.paycodeList[i].pay_discription + "</td>";
                  }
                  dom = dom + "<td class='semi-bold' style='text-align:right'>Total</td></tr>";
                  dom = dom + "<tr>";
                  for (var i = 0; i < info.paycodeList.length; i++) {
                      totalPaycodeAmount = parseFloat(totalPaycodeAmount) + parseFloat(info.paycodeList[i].pa_amount);
                      dom = dom + "<td class='semi-bold col-md-4' style='text-align:right'>" + info.paycodeList[i].pa_amount + "</td>";
                  }
                  dom = dom + "<td class='semi-bold' style='text-align:right'>" + totalPaycodeAmount + "</td></tr>";
                  //for (var i = 0; i < info.paycodeList.length; i++) {
                  //    totalPaycodeAmount = totalPaycodeAmount + info.paycodeList[i].pa_amount;                      
                  //    dom = dom + "<tr><td class='semi-bold'>" + info.paycodeList[i].pa_amount + " </td><td>" + totalPaycodeAmount + "</td></tr> ";
                  //}
                  dom = dom + "</tbody>";
                  dom = dom + "</table>";

                  var innerTable = $("<tr class='innerRow'><td class='details' colspan='12'>" + dom + "</tr>");

                  $($event.currentTarget).parents("tr").after(innerTable);                  
                  $scope.flag = false;
              }
              else {
                  $(".innerRow").css("display", "none");                                   
                  $("#" + iconId).removeClass("fa fa-minus-circle");
                  $("#" + iconId).addClass("fa fa-plus-circle");
                  $scope.flag = true;
              }
          };

          $scope.submit = function () {
              $scope.submitBtnDisabled = true;
              $scope.busyindicator = false;
              $scope.postData = [];
              $scope.postData = $scope.all_EmpGrt_Data;              
              $scope.opr = {
                  username:$rootScope.globals.currentUser.username,
                  yearmonth: $scope.temp.sims_academic_year + $scope.temp.sims_academic_month,
                  grtyearmonth:$scope.temp.yearCode
              }

              console.log("postData", $scope.postData);
              console.log("opr", $scope.opr);

              $http.post(ENV.apiUrl + "api/EmpGrtview/SaveAllGratuityData?data1=" + JSON.stringify($scope.opr), $scope.postData).then(function (msg1) {
                  $scope.msg = msg1.data;
                  if ($scope.msg == true) {                      
                     // swal({ text: 'Gratuity added successfully', width: 380 });
                      console.log("emp_id",$scope.emp_id);

                      $http.post(ENV.apiUrl + "api/EmpGrtview/PostingGratuity?year_month=" + $scope.temp.sims_academic_year + $scope.temp.sims_academic_month + "&emp_id=" + $scope.emp_id + "&company_code=" + $scope.temp.company_code + "&username=" + $rootScope.globals.currentUser.username).then(function (res) {
                          $scope.postingStatus = res.data;
                          $scope.emp_id = "";
                          if ($scope.postingStatus == true) {
                              swal({ title: 'Alert', text: 'Gratuity Posted Successfully' });

                              $scope.Myform.$setPristine();
                              $scope.Myform.$setUntouched();
                              $scope.clear();
                              $scope.busyindicator = true;

                          }
                          else {
                              swal({ title: 'Alert', text: 'Gratuity calculated-Posting failed' });
                          }
                          $('#myModal').modal('hide');
                      });

                  }
                  else if ($scope.msg == false) {                      
                      swal({ text: 'Gratuity not updated. ', width: 380 });                      
                  }
                  else {
                      swal("Error-" + $scope.msg)
                  }
              });
          }

      }])

})();
