﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('EmployeeConfirmCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.paysgrade_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.edit_code = false;
            $scope.display = false;
            $scope.grid = false;
            console.log("EmployeeConfirmCont called");            
            $scope.emailId_data = [];
            $scope.email = {};
            $scope.schoolASD = false;

            var schoolArray = ['asdportal'];

            if (schoolArray.includes($http.defaults.headers.common['schoolId'])) {
                $scope.schoolASD = true;
            }

            $scope.edt = {};
            $scope.temp = {};
            $scope.popobj = {};
            $scope.doc = {};
            debugger;
            $http.get(ENV.apiUrl + "api/HRMS/lems/getEmpMasterData").then(function (res) {

                $scope.empstat = true;
                $scope.obj = res.data;
                console.log("obj ", $scope.obj);

                $scope.operation = true;
                $scope.Save_btn = true;
                $scope.Update_btn = false;
                $scope.display = true;
                $scope.roleList = true;
                $scope.temp.em_Company_Code = $scope.obj[0].em_Company_Code;
                $scope.temp.em_service_status_code = 'V';
            });


            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/Get_designation_Code?company_code=1").then(function (Designation) {
                $scope.designation = Designation.data;
            });

            $scope.Show_Data = function () {
                debugger
                $http.get(ENV.apiUrl + "api/employeeOfferLetter/getEmployeeApplicationDetailsForConfirm?desg_code=" + $scope.temp.dg_code + "&app_from_date=" + $scope.temp.app_from_date + "&app_to_date=" + $scope.temp.app_to_date).then(function (res) {
                    $scope.employeeApplicationData = res.data;
                    console.log("employeeApplicationData", $scope.employeeApplicationData);
                    if ($scope.employeeApplicationData.length > 0) {
                        $scope.pager = true;

                        $scope.totalItems = $scope.employeeApplicationData.length;
                        $scope.todos = $scope.employeeApplicationData;
                        $scope.makeTodos();
                        $scope.grid = true;
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                        $scope.filteredTodos = [];
                    }
                });
            };

            $scope.confirm = function () {
                $scope.postData = [];
                $scope.flag = false;

                for(var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].checkConfirm == true) {
                        $scope.flag = true;
                        $scope.postData.push($scope.filteredTodos[i]);
                    }
                }
                if($scope.postData.length > 1) {
                    swal({ text: "Please select only one applicant ", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                    return;
                }
                console.log("$scope.postData", $scope.postData);
                var dt = new Date();
                var currentDate = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                console.log("currentDate " + currentDate);

                $('.nav-tabs a[href="#personal_information"]').tab('show');

                if ($scope.flag) {
                    //$scope.getDesignations($scope.postData[0].em_dept_code);
                    $scope.getstate($scope.postData[0].em_country_code);
                    $scope.getcity($scope.postData[0].em_state);

                    $scope.temp.em_first_name = $scope.postData[0].em_first_name;
                    $scope.temp.em_middle_name = $scope.postData[0].em_middle_name;
                    $scope.temp.em_last_name = $scope.postData[0].em_last_name;
                    $scope.temp.em_name_ot = $scope.postData[0].em_name_ot;
                    $scope.temp.em_mobile = $scope.postData[0].em_mobile;
                    $scope.temp.em_date_of_birth = $scope.postData[0].em_date_of_birth;
                    $scope.temp.em_apartment_number= $scope.postData[0].em_appartment_number;
                    $scope.temp.em_family_name = $scope.postData[0].em_family_name;
                    $scope.temp.em_building_number = $scope.postData[0].em_building_number;
                    $scope.temp.em_street_number= $scope.postData[0].em_street_number;
                    $scope.temp.em_area_number = $scope.postData[0].em_area_number;
                    $scope.temp.em_summary_address = $scope.postData[0].em_summary_address;
                    $scope.temp.em_Nation_Code = $scope.postData[0].em_nation_code;

                    $scope.temp.em_Country_Code = $scope.postData[0].em_country_code;
                    $scope.temp.em_State_Code = $scope.postData[0].em_state;
                    $scope.temp.em_City_Code = $scope.postData[0].em_city;
                    $scope.temp.em_Dest_Code = $scope.postData[0].em_dest_code;

                    $scope.temp.em_phone = $scope.postData[0].em_phone;
                    $scope.temp.em_fax = $scope.postData[0].em_fax;
                    $scope.temp.em_post = $scope.postData[0].em_po_box;
                    $scope.temp.em_pan_no = $scope.postData[0].em_pan_no;

                    $scope.temp.em_service_status_code = 'V';//$scope.postData[0].em_service_status_code;
                    $scope.temp.em_status_code= 'A';//$scope.postData[0].em_status_code;
                    
                    $scope.temp.em_emergency_contact_name2= $scope.postData[0].em_emergency_contact_name2;
                    $scope.temp.em_emergency_contact_number2= $scope.postData[0].em_emergency_contact_number2;
                    $scope.temp.em_joining_ref= $scope.postData[0].em_joining_ref;
                    $scope.temp.em_ethnicity_code= $scope.postData[0].em_ethnicity_code;
                    $scope.temp.em_handicap_status= $scope.postData[0].em_handicap_status;
                    $scope.temp.em_stop_salary_indicator= $scope.postData[0].em_stop_salary_indicator;
                              
                    $scope.temp.em_Marital_Status_Code = $scope.postData[0].em_marital_status;               
                    $scope.temp.em_Religion_Code = $scope.postData[0].em_religion_code;
                    $scope.temp.em_Salutation_Code = $scope.postData[0].em_salutation;
                    $scope.temp.em_Sex_Code = $scope.postData[0].em_sex;
                    $scope.temp.em_Staff_Type_Code = $scope.postData[0].em_staff_type;
                    $scope.temp.em_blood_group_code= $scope.postData[0].em_blood_group_code;
            
                    if($scope.postData[0].em_expected_date_of_join == undefined || $scope.postData[0].em_expected_date_of_join == "" || $scope.postData[0].em_expected_date_of_join == null){
                        $scope.temp.em_date_of_join = currentDate; 
                    }
                    else{
                        $scope.temp.em_date_of_join = $scope.postData[0].em_expected_date_of_join;
                    }

                    $scope.temp.em_Company_Code = $scope.postData[0].em_company_code;
                    $scope.temp.em_Dept_Code = $scope.postData[0].em_dept_code;
                    $scope.temp.em_Designation_Code = $scope.postData[0].em_desg_code;
                    if($scope.postData[0].em_grade_code == undefined || $scope.postData[0].em_grade_code == "" || $scope.postData[0].em_grade_code == null) {
                        $scope.temp.em_Grade_Code = $scope.obj[0].em_Grade_Code;
                    }
                    else {
                        $scope.temp.em_Grade_Code = $scope.postData[0].em_grade_code;
                    }
                    
                    $scope.temp.em_dept_effect_from = $scope.temp.em_date_of_join;
                    $scope.temp.em_grade_effect_from = $scope.temp.em_date_of_join;

                    $scope.temp.em_personalemail = $scope.postData[0].em_email;
                    //$scope.temp.em_email= $scope.postData[0].em_email;
                    $scope.temp.em_emergency_contact_name1= $scope.postData[0].em_emergency_contact_name1;                   
                    $scope.temp.em_emergency_contact_number1= $scope.postData[0].em_emergency_contact_number1;
                    
                    $scope.temp.em_passport_no = $scope.postData[0].em_passport_number;
                    $scope.temp.em_passport_exp_date = $scope.postData[0].em_passport_expiry_date;
                    $scope.temp.em_passport_issue_date = $scope.postData[0].em_passport_issue_date;                    
                    $scope.temp.em_passport_issue_place = $scope.postData[0].em_passport_issue_place;
                    $scope.temp.em_passport_issuing_authority = $scope.postData[0].em_passport_issuing_authority;

                    $scope.temp.em_visa_no = $scope.postData[0].em_visa_number;
                    $scope.temp.em_visa_exp_date = $scope.postData[0].em_visa_expiry_date;
                    $scope.temp.em_visa_issue_date = $scope.postData[0].em_visa_issue_date;                    
                    $scope.temp.em_visa_issuing_place = $scope.postData[0].em_visa_issuing_place;
                    $scope.temp.em_visa_issuing_authority = $scope.postData[0].em_visa_issuing_authority;
                    $scope.temp.em_visa_type_code = $scope.postData[0].em_visa_type

                    $scope.temp.em_agreement = $scope.postData[0].em_agreement;
                    $scope.temp.em_agreement_code = $scope.postData[0].em_agreement;
                    $scope.temp.em_agreement_start_date = $scope.postData[0].em_agreement_start_date;
                    $scope.temp.em_agreement_exp_date = $scope.postData[0].em_agreement_exp_date;
                             
                    $scope.temp.em_national_id = $scope.postData[0].em_national_id;
                    $scope.temp.em_national_id_expiry_date = $scope.postData[0].em_national_id_expiry_date;
                    $scope.temp.em_national_id_issue_date = $scope.postData[0].em_national_id_issue_date;
                    $scope.temp.em_labour_card_no = $scope.postData[0].em_labour_card_no;
                    $scope.temp.em_img = $scope.postData[0].em_img;

                    $scope.temp.em_offer_letter_send_date = $scope.postData[0].em_offer_letter_send_date;
                    $scope.temp.em_probation_period_days = $scope.postData[0].em_probation_period_days;
                    $scope.temp.em_probation_completion_date = $scope.postData[0].em_probation_completion_date;
                    $scope.temp.em_probation_confirmation_date = $scope.postData[0].em_probation_confirmation_date;
                    $scope.temp.em_applicant_id = $scope.postData[0].em_applicant_id;
                    console.log("temp", $scope.temp);

                    var imgSrc = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/' + $scope.temp.em_img;
                    $(function () {
                        $('#empimg').attr("src", imgSrc);
                    });
                    
                    $('#employeeMasterModal').modal('show');

                    $http.get(ENV.apiUrl + "api/Qualification/getQualificationByApplicant?applicant_id=" + $scope.postData[0].em_applicant_id).then(function (res1) {
                        $scope.qualiList = res1.data;
                        console.log("qualiList", $scope.qualiList);
                        $scope.combo_multiple = true;
                    });

                    $http.get(ENV.apiUrl + "api/EmpExp/getEmployeeExpByApplicant?applicant_id=" + $scope.postData[0].em_applicant_id).then(function (res) {
                        $scope.ExperienceList = res.data;
                        console.log("ExperienceList", $scope.ExperienceList);
                        $scope.showExperinceList = true;
                    });

                    //$scope.temp.em_punching_status= $scope.postData[0].em_punching_status,
                    //$scope.temp.em_bank_cash_tag= $scope.postData[0].em_bank_cash_tag,
                    //$scope.temp.em_citi_exp_tag= $scope.postData[0].em_citi_exp_tag,
                    //$scope.temp.em_leave_tag= $scope.postData[0].em_leave_tag,
                    //$scope.temp.em_pssport_exp_rem_date= $scope.postData[0].em_pssport_exp_rem_date,
                    //$scope.temp.em_visa_exp_rem_date= $scope.postData[0].em_visa_exp_rem_date,
                    
                    //$scope.temp.em_agreemet_exp_rem_date= $scope.postData[0].em_agreemet_exp_rem_date,
                    // $scope.temp.em_punching_id= $scope.postData[0].em_punching_id,
                    // $scope.temp.em_dependant_full= $scope.postData[0].em_dependant_full,
                    // $scope.temp.em_dependant_half= $scope.postData[0].em_dependant_half,
                    // $scope.temp.em_dependant_infant= $scope.postData[0].em_dependant_infant,
                    //$scope.temp.em_Bank_Code= $scope.postData[0].em_Bank_Code,
                    //$scope.temp.em_bank_ac_no= $scope.postData[0].em_bank_ac_no,
                    // $scope.temp.em_iban_no= $scope.postData[0].em_iban_no,
                    // $scope.temp.em_route_code= $scope.postData[0].em_route_code,
                    // $scope.temp.em_bank_swift_code= $scope.postData[0].em_bank_swift_code,
                    //$scope.temp.em_gpf_ac_no= $scope.postData[0].em_gpf_ac_no,
           
                    // $scope.temp.en_labour_card_expiry_date= $scope.postData[0].en_labour_card_expiry_date,
                    // $scope.temp.em_gosi_ac_no= $scope.postData[0].em_gosi_ac_no,
                    // $scope.temp.em_gosi_start_date= $scope.postData[0].em_gosi_start_date,

                    // $scope.temp.em_secret_question_code= $scope.postData[0].em_secret_question_code,
                    //$scope.temp.em_secret_answer= $scope.postData[0].em_secret_answer,
                    //$scope.temp.em_emergency_relation1= $scope.postData[0].em_emergency_relation1,
                    //$scope.temp.em_emergency_relation2= $scope.postData[0].em_emergency_relation2,
                    
                    //  $scope.temp.em_summary_address_local_language= $scope.postData[0].em_summary_address_local_language,
                    //  $scope.temp.em_stop_salary_from= $scope.postData[0].em_stop_salary_from,
                    //$scope.temp.comn_role_code= $scope.postData[0].comn_role_code,
                    //$scope.temp.comn_role_name= $scope.postData[0].comn_role_name,
                    //enroll_number1: emplogcode,
                    //em_login_code: $scope.postData[0].em_login_code,
                    //em_number: $scope.postData[0].em_number,
                   // $scope.temp.em_accommodation_status= $scope.postData[0].em_accommodation_status,
                    //$scope.temp.em_personalemail= $scope.postData[0].em_personalemail,
                    
                    //$scope.temp.em_adec_approval_number= $scope.postData[0].em_adec_approval_number,
                    //$scope.temp.em_adec_approval_Date= $scope.postData[0].em_adec_approval_Date,
                    //$scope.temp.em_adec_approved_qualification= $scope.postData[0].em_adec_approved_qualification,
                    //$scope.temp.em_adec_approved_designation= $scope.postData[0].em_adec_approved_designation,
                    //$scope.temp.em_adec_approved_subject= $scope.postData[0].em_adec_approved_subject,
                    //$scope.temp.em_adec_approved_level= $scope.postData[0].em_adec_approved_level,                    
                    //$scope.temp.em_health_card_no= $scope.postData[0].em_health_card_no,
                    //$scope.temp.em_health_card_effective_from_date= $scope.postData[0].em_health_card_effective_from_date,
                    //$scope.temp.em_health_card_effective_upto_date= $scope.postData[0].em_health_card_effective_upto_date,
                    //$scope.temp.em_heath_card_status= $scope.postData[0].em_heath_card_status,
                    //$scope.temp.em_teacher_status= $scope.postData[0].em_teacher_status,
                    //$scope.temp.em_house= $scope.postData[0].em_house
                    
                
                    //$scope.showEmpDocumentList($scope.temp1[0].em_login_code);

                    //$http.post(ENV.apiUrl + "api/employeeOfferLetter/ConfirmEmployee", $scope.postData).then(function (res) {
                    //    $scope.msg1 = res.data;
                    //    if ($scope.msg1 == true) {
                    //        swal({ text: "Employee Confirm Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                    //            //if (isConfirm) {
                    //            //    $scope.getgrid();
                    //            //}
                    //            $scope.Show_Data();
                    //        });
                    //    }
                    //    else if ($scope.msg1 == false) {
                    //        swal({ text: "Employee not confirm. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                    //            //if (isConfirm) {
                    //            //    $scope.getgrid();
                    //            //}
                    //            $scope.Show_Data()
                    //        });
                    //    }
                    //    else {
                    //        swal("Error-" + $scope.msg1)
                    //    }
                    //});
                }
                else {
                    swal({ text: "Please select at least one record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }

            }

       

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };


            $scope.reset = function () {
                $scope.temp = {};
            }

            $scope.cancel = function () {
                $scope.temp = {};
                $scope.grid = false;
            }

            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }
            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }
          

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.employeeApplicationData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.employeeApplicationData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.employeeApplicationData;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.em_applicant_id.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.emp_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.pays_vacancy_roles.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.em_application_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.dg_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.em_date_of_birth.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.em_expected_date_of_join.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.em_email.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.em_mobile.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            /**  Employee Master   **/


            $scope.showAgreementDetails = true;
            $scope.uae = false;
            $scope.qatar = true;
            $scope.slisLabel = false;
            $scope.india = false;
            $scope.updateBtn = false;
            $scope.saveBtn = false;
            $scope.combo_multiple = false;
            $scope.addDisabled = true;
            $scope.combo_multiple = false;
            $scope.qualiList = [];
            $scope.ExperienceList = [];
            var fortype = '';
            $scope.showFileName = false;


            $http.get(ENV.apiUrl + "api/Qual/getAllQualification").then(function (res1) {
                $scope.all_Qual_Data = res1.data;
            });

            $http.get(ENV.apiUrl + "api/common/Subject/getSubject").then(function (res) {
                $scope.sub_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/HRMS/lems/getRole").then(function (rolesdata) {
                $scope.roles_data = rolesdata.data;

            });

            $http.get(ENV.apiUrl + "api/HRMSOES/GetAccommodationStatus").then(function (res) {;
                $scope.AccommondationStatus = res.data;
                console.log("AccommondationStatus", $scope.AccommondationStatus);
            });

            $http.get(ENV.apiUrl + "api/common/MarkEmpResignation/getDisenagementType").then(function (res) {
                $scope.disengagmentTypes = res.data;
            });

            $http.get(ENV.apiUrl + "api/HRMS/getLicSchool").then(function (licschooldata) {
                $scope.lic_school_data = licschooldata.data;
                console.log("lic_school_data", $scope.lic_school_data);
                if ($scope.lic_school_data[0].lic_school_country == 'Qatar') {
                    $scope.uae = false;
                    $scope.qatar = true;
                    $scope.slisLabel = false;
                    $scope.india = false;
                }
                else if ($scope.lic_school_data[0].lic_school_country == 'Saudi Arabia') {
                    $scope.uae = false;
                    $scope.qatar = false;
                    $scope.slisLabel = true;
                    $scope.india = false;
                }
                else if ($scope.lic_school_data[0].lic_school_country == 'India') {
                    $scope.uae = false;
                    $scope.qatar = false;
                    $scope.slisLabel = false;
                    $scope.india = true;
                }
                else {
                    $scope.uae = true;
                    $scope.qatar = false;
                    $scope.slisLabel = false;
                    $scope.india = false;
                }
            });


            $scope.getstate = function (country) {
                $http.get(ENV.apiUrl + "api/HRMS/lems/getState?em_Country_Code=" + country).then(function (res) {
                    $scope.stt = res.data;
                });
            }

            $scope.getcity = function (state) {
                $http.get(ENV.apiUrl + "api/HRMS/lems/getCity?em_State_Code=" + state).then(function (res) {
                    $scope.ctt = res.data;
                });
            }

            $scope.getContract = function () {
                $http.get(ENV.apiUrl + "api/contract/Get_ContractDetails").then(function (Get_Contract) {
                    $scope.Contract_data = Get_Contract.data;

                });
            }
            $scope.getContract();

            $scope.AddEnable = function () { $scope.addDisabled = false; }

            $http.get(ENV.apiUrl + "api/Qualification/getQualificationName").then(function (QualificationName) {
                $scope.Qualification_Name = QualificationName.data;
            });

            $http.get(ENV.apiUrl + "api/Qualification/getQualificationType").then(function (qualtype) {
                $scope.qual_type_data = qualtype.data;
            });

            $http.get(ENV.apiUrl + "api/EmpExp/getMonthName").then(function (mon) {
                $scope.month_data = mon.data;
            });

            $scope.setGrade_Dept = function (str) {

                $scope.temp.em_dept_effect_from = str;
                $scope.temp.em_grade_effect_from = str;
            }
        

            $scope.Cancel = function () {
                $scope.temp = {};
                $("#empimg").attr('src', '')
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.Save_btn = true;
                $scope.Update_btn = false;
                var data = {};
                $scope.temp.em_Company_Code = $scope.obj[0].em_Company_Code;
                $scope.temp.em_service_status_code = 'V';
            }


            $scope.getstate = function (country) {
                $http.get(ENV.apiUrl + "api/HRMS/empASD/getState?em_Country_Code=" + country).then(function (res) {
                    $scope.stt = res.data;
                });
            }

            $scope.getcity = function (state) {
                $http.get(ENV.apiUrl + "api/HRMS/empASD/getCity?em_State_Code=" + state).then(function (res) {
                    $scope.ctt = res.data;
                });
            }

            $scope.getContract = function () {
                $http.get(ENV.apiUrl + "api/contract/Get_ContractDetails").then(function (Get_Contract) {
                    $scope.Contract_data = Get_Contract.data;
                    console.log($scope.Contract_data);
                });
            }
            $scope.getContract();

            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };

            $scope.file_changed = function (element) {
                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        //$scope.prev_img = e.target.result;
                        var imgSrc = e.target.result;
                        $(function () {
                            $('#empimg').attr("src", imgSrc);
                        });
                    });
                };
                reader.readAsDataURL(photofile);
            };


            $scope.uploadImgClickF = function () {
                $scope.uploadImgClickFShow = true;
            }

            $scope.uploadImgCancel = function () {
                $scope.uploadImgClickFShow = false;
            }

            $scope.image_click = function () {
                $('#upload_img').modal('show');
                $scope.ImgLoaded = true;
            }

            $scope.checkEmailExists = function (email_id) {
                if ($scope.temp1[0].em_email != email_id) {
                    $http.post(ENV.apiUrl + "api/HRMS/checkEmailExists?email_id=" + email_id).then(function (resc) {
                        $scope.comnUserData = resc.data;
                        if ($scope.comnUserData.length > 0) {
                            swal({ text: 'Official email id already exists!!', width: 350 });
                            $scope.temp.em_email = "";
                            return false;
                        }
                    });
                }
            }

            $scope.SaveData = function (isvalid) {

                if (isvalid) {
                    $scope.saveBtn = true;
                    $scope.updateBtn = true;

                    var data = $scope.temp;
                    if ($scope.photo_filename === undefined) {
                        data.em_img = null;
                    }
                    else {
                        data.em_img = '.' + $scope.photo_filename.split("/")[1];
                    }
                    data.opr = "I";
                    $http.post(ENV.apiUrl + "api/HRMS/lems/CUMasterEmployee", data).then(function (res) {
                        $scope.CUDobj = {};
                        $scope.CUDobj = res.data;
                        debugger;
                        if ($scope.CUDobj.strMessage != undefined || $scope.CUDobj.strMessage != "" || $scope.CUDobj.strMessage != null) {
                            swal({ text: 'Employee with id= ' + $scope.CUDobj.strMessage + ' created  successfully.' });
                            console.log("Em login Code " + $scope.CUDobj.strMessage);
                            $scope.saveQualification($scope.CUDobj.strMessage);
                            $scope.saveExperience($scope.CUDobj.strMessage);
                            $scope.Show_Data();

                            //$http.post(ENV.apiUrl + "api/employeeOfferLetter/ConfirmEmployee", $scope.postData).then(function (res) {
                            //    $scope.msg1 = res.data;
                            //    if ($scope.msg1 == true) {
                            //        $scope.Show_Data();
                            //    }
                            //    else {
                            //        swal("Error-" + $scope.msg1)
                            //    }
                            //});


                            $scope.Cancel();

                            setTimeout(function () {
                                $scope.saveBtn = false;
                                $scope.updateBtn = false;
                            }, 3000);

                            $('#employeeMasterModal').modal('hide');

                        }

                        var empid = $scope.CUDobj.strMessage.split(" ")[2];
                        var request = {
                            method: 'POST',
                            url: ENV.apiUrl + '/api/file/upload?filename=' + empid + "&location=" + "/EmployeeImages",
                            data: formdata,
                            headers: {
                                'Content-Type': undefined
                            }
                        };

                        $http(request).success(function (d) {
                            //alert(d);
                        });

                    });

                }
                else {
                    swal({ text: 'Kindly fill Mandatory fields', width: 350, timer: 5000, showCloseButton: true });
                }
            }

            $scope.SaveDataOLd = function (isvalid) {
                debugger;
                if (isvalid) {
                    $scope.saveBtn = true;
                    $scope.updateBtn = true;

                    var data = $scope.temp;
                    if ($scope.photo_filename === undefined) {
                        data.em_img = null;
                    }
                    else {
                        data.em_img = '.' + $scope.photo_filename.split("/")[1];
                    }
                    data.opr = "I";
                    $http.post(ENV.apiUrl + "api/HRMS/empASD/CUMasterEmployee", data).then(function (res) {
                        $scope.CUDobj = {};
                        $scope.CUDobj = res.data;

                        if ($scope.CUDobj.strMessage != undefined || $scope.CUDobj.strMessage != "" || $scope.CUDobj.strMessage != null) {
                            swal({ text: $scope.CUDobj.strMessage });


                            $http.post(ENV.apiUrl + "api/employeeOfferLetter/ConfirmEmployee",$scope.postData).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {                                   
                                     $scope.Show_Data();                                   
                                }                                
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });

                            setTimeout(function () {
                                $scope.Cancel();
                                $scope.saveBtn = false;
                                $scope.updateBtn = false;
                            }, 2000);
                        }

                        var empid = $scope.CUDobj.strMessage.split(" ")[2];
                        var request = {
                            method: 'POST',
                            url: ENV.apiUrl + '/api/file/upload?filename=' + empid + "&location=" + "/EmployeeImages",
                            data: formdata,
                            headers: {
                                'Content-Type': undefined
                            }
                        };

                        $http(request).success(function (d) {
                            //alert(d);
                        });
                    });
                    $scope.em_img = "";
                    $scope.prev_img = "";
                }
                else {
                    swal({ text: 'Kindly fill Mandatory fields', width: 350, timer: 5000, showCloseButton: true });
                }
            }

            $scope.saveQualification = function (em_login_code) {

                var demo1 = [];
                var modulecode = [];

                if ($scope.qualiList.length > 0) {

                    for (var i = 0; i < $scope.qualiList.length; i++) {
                        //if (parseInt($scope.qualiList[i].em_qual_from_year) > parseInt(currentYear) || parseInt($scope.qualiList[i].em_qual_to_year) > parseInt(currentYear)) {
                        //    swal({ title: "Alert", text: 'Future year not allowed', timer: 5000, width: 320 });
                        //    return;
                        //}
                        modulecode = $scope.qualiList[i].emp_qual_qual_code;
                        var data = {
                            'pays_qualification_applicant_id': $scope.temp.em_applicant_id,
                            'emp_qual_em_code': em_login_code,//$scope.popobj.emp_qual_em_code,
                            'emp_qual_qual_code': modulecode,
                            'emp_qual_institute': $scope.qualiList[i].emp_qual_institute,
                            'emp_qual_type_code': $scope.qualiList[i].emp_qual_type_code,
                            'em_qual_from_year': $scope.qualiList[i].em_qual_from_year,
                            'em_qual_year': $scope.qualiList[i].em_qual_to_year,
                            'emp_qual_percentage': $scope.qualiList[i].emp_qual_percentage,
                            'emp_qual_remark': $scope.qualiList[i].emp_qual_remark,
                            'em_qual_status': $scope.qualiList[i].em_qual_status,
                            'opr': 'E'
                        }
                        demo1.push(data);
                    }

                    $http.post(ENV.apiUrl + "api/Qualification/CUDQualificationApplicant?year=", demo1).then(function (msg) {
                        $scope.qualificationmsg = msg.data;
                        console.log("qualificationmsg ", $scope.qualificationmsg);
                        $scope.qualiList = [];
                        $scope.combo_multiple = false;
                    });
                }
            }

    
            $scope.downloadOfferLetter = function (str) {
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeDocument/' + str;
                window.open($scope.url);
            }


            $scope.addCol = function (str) {

                $scope.combo_multiple = true;
                var t = document.getElementById("qual_name");
                var selectedText = t.options[t.selectedIndex].text;

                for (var i = 0; i < $scope.qualiList.length; i++) {
                    if ($scope.qualiList[i].emp_qual_qual_code == str) {
                        swal({ text: 'Qualification Already Added !!', timer: 5000 });
                        return false;
                    }
                }

                var data = {
                    'emp_qual_qual_name': selectedText,
                    'emp_qual_qual_code': str
                }
                $scope.qualiList.push(data);

                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            $scope.Removerow = function ($event, index, str) {
                str.splice(index, 1);
            }

            $scope.addExperience = function () {
                $scope.showExperinceList = true;
                //var t = document.getElementById("qual_name");
                //var selectedText = t.options[t.selectedIndex].text;

                //for (var i = 0; i < $scope.qualiList.length; i++) {
                //    if ($scope.qualiList[i].emp_qual_qual_code == str) {
                //        swal({ text: 'Qualification Already Added !!', timer: 5000 });
                //        return false;
                //    }
                //}

                var data = {
                    'enroll_number': '',
                    'prev_company_name': '',
                    'prev_job_title': '',
                    'from_year': '',
                    'from_month': '',
                    'to_year': '',
                    'to_month': '',
                    'prev_job_remark': '',
                    'prev_job_responsibilities': '',
                    'prev_job_salary': '',
                    'job_status': '',
                    'em_previous_job_line_no': ''
                }
                $scope.ExperienceList.push(data);
            };


            $scope.saveExperience = function (em_login_code) {
                var datasend = [];

                if ($scope.ExperienceList.length > 0) {

                    for (var i = 0; i < $scope.ExperienceList.length; i++) {

                        var data = {
                            'em_applicant_id': $scope.temp.em_applicant_id,
                            'enroll_number': em_login_code,
                            'co_company_code': '1',
                            'prev_company_name': $scope.ExperienceList[i].prev_company_name,
                            'prev_job_title': $scope.ExperienceList[i].prev_job_title,
                            'from_year': $scope.ExperienceList[i].from_year,
                            'from_month': $scope.ExperienceList[i].from_month,
                            'to_year': $scope.ExperienceList[i].to_year,
                            'to_month': $scope.ExperienceList[i].to_month,
                            //'prev_job_remark': $scope.ExperienceList[i].prev_job_remark,
                            'prev_job_responsibilities': $scope.ExperienceList[i].prev_job_responsibilities,
                            'prev_job_salary': $scope.ExperienceList[i].prev_job_salary,
                            'job_status': $scope.ExperienceList[i].job_status,
                            'em_previous_job_line_no': $scope.ExperienceList[i].em_previous_job_line_no,
                            'opr': 'E'
                        }
                        datasend.push(data);
                    }

                    $http.post(ENV.apiUrl + "api/EmpExp/CUDEmployeeExperienceApplicant", datasend).then(function (msg) {
                        $scope.msgexp = msg.data;
                        console.log("emp exp", $scope.msgexp);
                        $scope.ExperienceList = [];
                        $scope.showExperinceList = false;
                    });

                }
            }

            $scope.RemoveExperinceRow = function ($event, index, str) {
                str.splice(index, 1);
            }

            simsController.directive('ngFiles', ['$parse', function ($parse) {

                function fn_link(scope, element, attrs) {
                    var onChange = $parse(attrs.ngFiles);
                    element.on('change', function (event) {
                        onChange(scope, { $files: event.target.files });
                    });
                };

                return {
                    link: fn_link
                }
            }])


        }])
})();