﻿
(function () {
    'use strict';
    var temp, obj1, opr, popobj, data1;
    var modulecode = [], datasend = [];
    var main;

    var simsController = angular.module('sims.module.HRMS');

    simsController.directive("limitTo", [function () {
        return {
            restrict: "A",
            link: function (scope, elem, attrs) {
                var limit = parseInt(attrs.limitTo);
                angular.element(elem).on("keydown", function () {
                    if (event.keyCode > 47 && event.keyCode < 127) {
                        if (this.value.length == limit)
                            return false;
                    }
                });
            }
        }
    }]);

    simsController.directive('thead', function () {
        return {
            restrict: 'A',
            scope: {
                ngModel: '='
            },
            link: function (scope, elem) {
                window.onscroll = function () {
                    elem.floatThead({
                        scrollingTop: 45,
                    });
                    elem.floatThead('reflow');
                }
            }
        };
    })

    simsController.controller('QualificationEmployeeADISWCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.grid = true;
            $scope.display = false;
            $scope.editmode = false;
            $scope.popobj = temp;
            $scope.qualiList = [];
            $scope.temp = {};
            $scope.popobj = {};

            //ashwin
            $scope.AddQualdisplay = false;
            $scope.addInlstBtn = true;
            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $http.get(ENV.apiUrl + "api/Qualification/getAllQualification?data=" + JSON.stringify(data1)).then(function (res1) {
                $scope.obj1 = res1.data;
                $scope.totalItems = $scope.obj1.length;
                $scope.todos = $scope.obj1;
                $scope.makeTodos();
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ "left": 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ "left": 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2015&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;
                $scope.temp.emp_qual_company_code = $scope.ComboBoxValues[0].fins_comp_code;
                $scope.popobj.emp_qual_company_code = $scope.ComboBoxValues[0].fins_comp_code;                
                //$scope.getEmp($scope.ComboBoxValues[0].fins_comp_code);
                $scope.getdept($scope.ComboBoxValues[0].fins_comp_code);
            });

            $scope.AddQualification = function () {
                $scope.AddQualdisplay = true;
                $scope.display = false;
                $scope.qual_save_btn = true;

            }

            $scope.size = function (str) {                
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                //$scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.obj1;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $http.get(ENV.apiUrl + "api/Qualification/getAllEmployeeName").then(function (res1) {
                $scope.empname = res1.data;
            });

            $http.get(ENV.apiUrl + "api/Qualification/getQualificationType").then(function (qualtype) {
                $scope.qual_type_data = qualtype.data;
            });


            $scope.getdept = function (dept) {
                $scope.dept = [];
                $scope.popobj.emp_qual_dept_code = "";
                $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/Get_dept_Code?company_code=" + dept).then(function (Department) {
                    $scope.dept = Department.data;
                });
            }

            $scope.getEmp = function (deptcode, comcode) {
                $http.get(ENV.apiUrl + "api/common/Qualification/getEmployeeName?em_dept_code=" + deptcode + "&em_company_code=" + comcode).then(function (res1) {
                    $scope.display = true;
                    $scope.empname = res1.data;                    
                });
            }
            //$scope.getEmp = function (comcode) {
            //    $http.get(ENV.apiUrl + "api/Qualification/getEmployeeName?em_company_code=" + comcode).then(function (res1) {
            //        //$scope.display = true;
            //        $scope.empname = res1.data;
            //        console.log($scope.empname);
            //    });
            //}

            $scope.AddEnable = function () { $scope.addDisabled = false; }

            $scope.addCol = function (str) {

                $scope.combo_multiple = true;
                var t = document.getElementById("qual_name");
                var selectedText = t.options[t.selectedIndex].text;

                for (var i = 0; i < $scope.qualiList.length; i++) {
                    if ($scope.qualiList[i].emp_qual_qual_code == str) {
                        swal({ text: 'Qualification Already Added !!', timer: 5000 });
                        return false;
                    }
                }

                var data = {
                    'emp_qual_qual_name': selectedText,
                    'emp_qual_qual_code': str
                }
                $scope.qualiList.push(data);

                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            $scope.Removerow = function ($event, index, str) {
                str.splice(index, 1);
            }
            var d = new Date();
            var currentYear = d.getFullYear();
           

            $scope.savedata = function (Myform) {
               
                var demo = [];
                var demo1 = [];
                if (Myform) {

                    if ($scope.qualiList.length == 0) {
                        swal({ text: 'Please Add Qualification', timer: 5000 });
                        return false;
                    }

                    for (var i = 0; i < $scope.qualiList.length; i++) {
                        if (parseInt($scope.qualiList[i].em_qual_from_year) > parseInt(currentYear) || parseInt($scope.qualiList[i].em_qual_to_year) > parseInt(currentYear)) {
                            swal({ title: "Alert",text: 'Future year not allowed', timer: 5000,width:320});
                            return ;
                        }
                        
                        modulecode = $scope.qualiList[i].emp_qual_qual_code;
                        var data = {
                            'emp_qual_em_code': $scope.popobj.emp_qual_em_code,
                            'emp_qual_qual_code': modulecode,
                            'emp_qual_institute': $scope.qualiList[i].emp_qual_institute,
                            'emp_qual_type_code': $scope.qualiList[i].emp_qual_type_code,
                            'em_qual_from_year': $scope.qualiList[i].em_qual_from_year,
                            'em_qual_year': $scope.qualiList[i].em_qual_to_year,
                            'emp_qual_percentage': $scope.qualiList[i].emp_qual_percentage,
                            'emp_qual_remark': $scope.qualiList[i].emp_qual_remark,
                            'em_qual_status': $scope.qualiList[i].em_qual_status,
                            'opr': 'I'
                        }
                        demo1.push(data);

                    }
                    
                    $http.post(ENV.apiUrl + "api/Qualification/CUDQualification?year=", demo1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        swal({ text: $scope.msg1.strMessage, timer: 5000 });
                        $scope.display = false;
                       
                        $http.get(ENV.apiUrl + "api/Qualification/getAllQualification?data=" + JSON.stringify(data1)).then(function (res1) {
                            $scope.obj1 = res1.data;
                            $scope.totalItems = $scope.obj1.length;
                            $scope.todos = $scope.obj1;
                            $scope.makeTodos();
                        });
                        demo = [];
                        demo1 = [];
                        $scope.grid = true;
                        $scope.display = false;
                        $scope.temp.emp_name_id = '';
                    })

                }
            }

            $scope.update = function (Myform) {
                var demo1 = [];
                if(Myform) {
                    for (var i = 0; i < $scope.qualiList.length; i++) {

                        if (parseInt($scope.qualiList[i].em_qual_from_year) > parseInt(currentYear) || parseInt($scope.qualiList[i].em_qual_to_year) > parseInt(currentYear)) {
                            swal({ title: "Alert",text: 'Future year not allowed', timer: 5000, width: 320 });
                            return;
                        }

                        modulecode = $scope.qualiList[i].emp_qual_qual_code;
                        var data = {
                            'emp_qual_em_code': $scope.popobj.emp_qual_em_code,
                            'emp_qual_qual_code': modulecode,

                            'emp_qual_institute': $scope.qualiList[i].emp_qual_institute,
                            'emp_qual_type_code': $scope.qualiList[i].emp_qual_type_code,
                            'em_qual_from_year': $scope.qualiList[i].em_qual_from_year,

                            'em_qual_year': $scope.qualiList[i].em_qual_to_year,
                            'emp_qual_percentage': $scope.qualiList[i].emp_qual_percentage,
                            'emp_qual_remark': $scope.qualiList[i].emp_qual_remark,
                            'em_qual_status': $scope.qualiList[i].em_qual_status,
                            'opr': 'U'
                        }
                        demo1.push(data);
                    }

                    if ($scope.qualiList.length!=0) {
                        $http.post(ENV.apiUrl + "api/Qualification/UpadateQualification?simsobj2=", demo1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            swal({ text: $scope.msg1.strMessage, timer: 5000 });
                            $scope.display = false;
                            var data = {
                                emp_qual_qual_code: $scope.popobj.emp_qual_qual_code1,
                                emp_qual_dept_code: $scope.popobj.emp_qual_dept_code1,
                                emp_qual_company_code: $scope.popobj.emp_qual_company_code1
                            }
                            $http.get(ENV.apiUrl + "api/Qualification/getAllQualification?data=" + JSON.stringify(data)).then(function (res1) {
                                $scope.obj1 = res1.data;
                                $scope.totalItems = $scope.obj1.length;
                                $scope.todos = $scope.obj1;
                                $scope.makeTodos();
                            });
                            $scope.grid = true;
                            $scope.display = false;
                        })
                    }
                    else {
                        swal({ text: 'You not edit correct record', timer: 5000 });
                    }
                }

            }

            $scope.GetData = function () {

                $http.get(ENV.apiUrl + "api/Qualification/getAllQualification?data=" + JSON.stringify($scope.temp)).then(function (res1) {
                    $scope.obj1 = res1.data;
                    $scope.totalItems = $scope.obj1.length;
                    $scope.todos = $scope.obj1;
                    $scope.makeTodos();
                });
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = 'fa fa-plus-circle';
                }

            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj1, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj1;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.emp_qual_dept_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.emp_qual_em_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.emp_qual_qual_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.emp_qual_remark.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.em_qual_year == toSearch) ? true : false;
            }

            $scope.getQualiByEmpcode = function (empcode) {
              
                $http.get(ENV.apiUrl + "api/Qualification/getQualificationByEmployee?emp_code=" + empcode).then(function (res1) {
                    $scope.qualiList = res1.data;                    
                });
                $scope.combo_multiple = true;
            }


            $scope.edit = function (str) {
                
                $scope.insertFrmDt = false;
                $scope.insertTotext = false;
                $scope.updtfrmYearText = true;
                $scope.updtToyearText = true;
                $scope.addInlstBtn = false;

                //if (str.em_qual_from_year != undefined || str.em_qual_from_year != "" || str.em_qual_from_year != null) {
                //    $scope.popobj.em_qual_from_year = $filter('date')(str.em_qual_from_year, 'yyyy-MM-dd');
                //}

                //if (str.em_qual_to_year != undefined || str.em_qual_to_year != "" || str.em_qual_to_year != null) {
                //    $scope.popobj.em_qual_to_year = $filter('date')(str.em_qual_year, 'yyyy-MM-dd')
                //}
                
                $scope.popobj = {
                    'emp_qual_qual_code': str.emp_qual_qual_code,
                    'emp_qual_qual_name': str.emp_qual_qual_name,
                    'emp_qual_company_code': str.emp_qual_company_code,
                    'emp_qual_company_name': str.emp_qual_company_name,
                    'emp_qual_dept_code': str.emp_qual_dept_code,
                    'emp_qual_dept_name': str.emp_qual_dept_name,
                    'emp_qual_em_code': str.emp_qual_em_code,
                    'emp_qual_em_name': str.emp_qual_em_name,
                    'emp_qual_institute': str.emp_qual_institute,
                    'emp_qual_type_code': str.emp_qual_type_code,
                    'em_qual_from_year': str.em_qual_from_year,
                    'emp_qual_percentage': str.emp_qual_percentage,
                    'em_qual_status': str.em_qual_status,
                    'em_qual_to_year': str.em_qual_year,
                    'emp_qual_remark': str.emp_qual_remark
                }
                // setTimeout(function () {
                $scope.getQualiByEmpcode($scope.popobj.emp_qual_em_code);
                // }, 1000);

                //for (var k = 0; k < $scope.qualiList.length; k++) {
                //    $scope.popobj.em_qual_from_year = $filter('date')(new Date($scope.qualiList[k].em_qual_from_year), 'yyyy-MM-dd');
                //     setTimeout(function () {

                //    $("#fromtest-" + k).val($scope.popobj.em_qual_from_year);
                //    console.log($("#fromtest-" + k).val());

                //     }, 1000)

                //     $scope.popobj.em_qual_to_year = $filter('date')(new Date($scope.qualiList[k].em_qual_to_year), 'yyyy-MM-dd');
                //    setTimeout(function () {

                //    $("#totest-" + k).val($scope.popobj.em_qual_to_year);
                //    console.log($("#totest-" + k).val());

                //     }, 1000)
                //}
                $scope.grid = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.qualOnEdit = false;
                $scope.addBtnOnEdit = false;
            }



            $scope.onKeyPress = function ($event) {
                if ($event.keyCode == 13) {
                    $scope.GetData();
                }
            };

            $scope.clear = function () {
                $scope.temp.emp_qual_dept_code = '';
                $scope.temp.emp_qual_qual_code = '';
                $scope.temp.emp_name_id = '';
                $scope.popobj = "";
            }

            $scope.QualCancel = function () {
                $scope.display = true;
                $scope.AddQualdisplay = false;

            }

            $scope.Cancel = function () {
                $scope.display = false;
                $scope.grid = true;
                $scope.temp.emp_qual_dept_code = '';
                $scope.temp.emp_qual_qual_code = '';
                $scope.temp.emp_name_id = '';
            }

            $scope.formopen = function () {
                $scope.insertFrmDt = true;
                $scope.insertTotext = true;
                $scope.popobj["emp_qual_dept_code"] = '';
                $scope.popobj["emp_qual_em_code"] = '';
                $scope.popobj["emp_qual_qual_code"] = '';
                //$scope.popobj = {};
                $scope.qualiList = [];
                $scope.qualOnEdit = true;
                $scope.addBtnOnEdit = true;
                $scope.qual_rmk_readonly = false;
                $scope.qual_yr_readonly = false;
                $scope.addDisabled = true;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.Quali = '';
                $scope.disabled = false;
                $scope.chkdisabled = true;
                $scope.grid = false;
                $scope.Update_btn = false;
                $scope.save_btn = true;
                $scope.display = true;
                $scope.combo_single = false;
                $scope.combo_multiple = false;
                $scope.popobj["em_qual_status"] = true;
                $scope.popobj["emp_qual_em_code"] = "";
                $scope.updtToyearText = false;
                $scope.updtfrmYearText = false;
                $scope.Qualification_Name = '';
                $http.get(ENV.apiUrl + "api/Qualification/getQualificationName").then(function (QualificationName) {
                    $scope.Qualification_Name = QualificationName.data;
                })
            }


            var dataQualsend = [];
            $scope.AddQual = function () {//Myform1
                dataQualsend = [];
                //if (Myform1) {


                //var flag = false;
                //var data = $scope.edt;
                //for (var i = 0; i < $scope.qual_type_data.length; i++) {
                //    if ($scope.qual_type_data[i].pays_qualification_desc == $scope.edt.pays_qualification_desc) 
                //       {
                //        swal({ title: "Alert", text: "Record Already Exist", showCloseButton: true, width: 380, });
                //        flag = true;
                //        break;
                //    }
                //}

                // if (!flag) {
                var data = $scope.edt;
                data.opr = "I";
                dataQualsend.push(data);

                $http.post(ENV.apiUrl + "api/Qualification/CQualification", dataQualsend).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Qualification Added in List", imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                        $scope.display = true;
                        $scope.AddQualdisplay = false;
                        $scope.formopen();
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Qualification Not Added in List. ", imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }

                });
                dataQualsend = [];
                //  }
                //else {
                //    swal({ title: "Alert", text: "This Qualification Already In List", showCloseButton: true, width: 380, });
                //}

                // }
            }


            var dom;
            $scope.flag = true;
            $scope.expand = function (j, $event) {

                if ($scope.flag == true) {
                    $(dom).remove();
                    j.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0' width='100%'>" +
                        "<tbody>" +

                         "<tr><td class='semi-bold'><i class='fa fa-align-justify'></i>&nbsp;" + "Qualification" + "</td> <td class='semi-bold'><i class='fa fa-building'></i>&nbsp;" + "Institute" + "</td> <td class='semi-bold'><i class='fa fa-bookmark-o'></i>&nbsp;" + "Qualification Type" + " </td> <td class='semi-bold'><i class='fa fa-calendar'></i>&nbsp;" + "From Year" + "</td> <td class='semi-bold'><i class='fa fa-calendar'></i>&nbsp;" + "To Year" + "</td>" +
                          "</tr> <td>" + (j.emp_qual_qual_name) + "</td> <td>" + (j.emp_qual_institute) + "</td> <td>" + (j.emp_qual_type_name) + "</td> <td>" + (j.em_qual_from_year) + "</td><td>" + (j.em_qual_year) + "</td>" +

                          "<tr> <td class='semi-bold'><i class='fa fa-sort-numeric-asc'></i>&nbsp;" + "Percentage" + "</td>" + "<td class='semi-bold'><i class='fa fa-bookmark'></i>&nbsp;" + "Remark" + "</td>" + "<td class='semi-bold'>" + "</td>" + "<td class='semi-bold'>" + "</td>" + "<td class='semi-bold'>" + "</td>" +
                            "</tr> <td>" + (j.emp_qual_percentage) + "</td> <td>" + (j.emp_qual_remark) + " </td>" + "</td> <td>" + " </td>" + "</td> <td>" + " </td>" + "</td> <td>" + " </td>" +

                        "</tr>" +

                        " </table>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }
            }


        }])
})();
