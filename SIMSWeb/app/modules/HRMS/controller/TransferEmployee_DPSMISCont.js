﻿(function () {
    'use strict';
    var main = '';
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

    simsController.controller('TransferEmployeeDPSMISCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {


            var uname = $rootScope.globals.currentUser.username;
            $scope.display = false;
            $scope.busyindicator = false;
            $scope.grid = true;
            $scope.color = '#edefef';
            $scope.rgvtbl = false;
            $scope.department = false;
            $scope.edt = {};

            $scope.Show_dept = function () {
                $scope.department = true;
            }
            $scope.Show_grade = function () {
                $scope.grade = true;
            }
            $scope.Show_designation = function () {
                $scope.designation = true;
            }
            $scope.Show_stafftype = function () {
                $scope.stafftype = true;
            }
            $scope.Reset_dept = function () {
                $scope.department = false;
            }
            $scope.Reset_grade = function () {
                $scope.grade = false;
            }
            $scope.Reset_designation = function () {
                $scope.designation = false;
            }
            $scope.Reset_stafftype = function () {
                $scope.stafftype = false;
            }


            $http.get(ENV.apiUrl + "api/TransferEmp/getCompany").then(function (compdata) {
                $scope.comp_data = compdata.data;
                $scope.tempD =
                {
                    'em_Company_Code': $scope.comp_data[0].em_Company_Code
                }
            });

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2015&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;
            });
            $scope.depts = [];
            $scope.getDept = function () {
                $scope.depts = [];
                for (var i = 0; i < $scope.ComboBoxValues.length; i++) {
                    if ($scope.edt.gd_division_code == "*") {
                        if ($scope.ComboBoxValues[i].codp_dept_name.toString().trim().length > 0)
                            $scope.depts.push($scope.ComboBoxValues[i]);
                    }
                    else {
                        if ($scope.ComboBoxValues[i].codp_division == $scope.edt.gd_division_code) {
                            $scope.depts.push($scope.ComboBoxValues[i]);
                        }
                    }
                }
            }
            console.log("$scope.depts", $scope.depts);

            $http.get(ENV.apiUrl + "api/TransferEmp/getDepartment").then(function (deptdata) {
                $scope.dept_data = deptdata.data;
            });
            $http.get(ENV.apiUrl + "api/TransferEmp/getGrade").then(function (gradedata) {
                $scope.grade_data = gradedata.data;
            });
            $http.get(ENV.apiUrl + "api/TransferEmp/getDesignation").then(function (desgdata) {
                $scope.desg_data = desgdata.data;
            });
            $http.get(ENV.apiUrl + "api/TransferEmp/getStaffType").then(function (staffdata) {
                $scope.staff_data = staffdata.data;
            });

            $scope.getEmployee = function (comp_code, dept_code, grade_code, designation_code, stafftype_code, emp_name, employee_no) {

                $http.get(ENV.apiUrl + "api/TransferEmp/getSearchEmployee?em_company_code=" + comp_code + "&em_dept_code=" + dept_code + "&em_grade_code=" + grade_code +
                "&em_desg_code=" + designation_code + "&em_staff_type_code=" + stafftype_code + "&emp_name=" + emp_name + "&em_login_code=" + employee_no).then(function (employeeData) {
                    $scope.employee_Data = employeeData.data;

                    angular.forEach($scope.employee_Data, function (value, key) {
                        if (value.em_dept_effect_from == "1900-01-01") {
                            value.em_dept_effect_from = '' //$filter('date')(new Date(), 'yyyy-MM-dd');
                        }
                        if (value.em_grade_effect_from == "1900-01-01") {
                            value.em_grade_effect_from = ''
                        }
                        if (value.em_desg_effect_from == "1900-01-01") {
                            value.em_desg_effect_from = ''
                        }
                        if (value.em_staff_type_effect_from == "1900-01-01") {
                            value.em_staff_type_effect_from = ''
                        }
                    });



                    if ($scope.employee_Data == "") {
                        swal({ title: "Alert!", text: "Records Not Found", showCloseButton: true, width: 380, });
                        $scope.stud_table = false;
                    }
                    else { }
                });
            }

            $scope.Reset = function () {
                $scope.tempD = "";
                $scope.employee_Data = "";
                $scope.edt.gd_division_code = "";
            }

            $scope.deptChange = function (info) { info.ischange = true; }
            $scope.DeptEfChange = function (info) { info.ischange = true; }

            $scope.EmpTransUpdate = function () {
                var currtime = $filter('date')(new Date(), 'hh:mm:ss');
                var currtime1 = $filter('date')(new Date(), 'hh:mm:ss');
                debugger;
                var dataupdate = [];
                var data = {};
                for (var k = 0; k < $scope.employee_Data.length; k++) {
                    if ($scope.employee_Data[k].ischange == true) {
                        data =
                       {
                           em_Company_Code: $scope.tempD.em_Company_Code,
                           em_login_code: $scope.employee_Data[k].em_login_code,
                           em_number: $scope.employee_Data[k].em_number,

                           em_Dept_code: $scope.employee_Data[k].em_Dept_code,
                           em_dept_effect_from: $scope.employee_Data[k].em_dept_effect_from,

                           em_Grade_code: $scope.employee_Data[k].em_Grade_code,
                           em_grade_effect_from: $scope.employee_Data[k].em_grade_effect_from,

                           em_Desg_code: $scope.employee_Data[k].em_Desg_code,
                           em_desg_effect_from: $scope.employee_Data[k].em_desg_effect_from + ' ' + currtime,

                           em_Staff_type_code: $scope.employee_Data[k].em_Staff_type_code,
                           em_staff_type_effect_from: $scope.employee_Data[k].em_staff_type_effect_from + ' ' + currtime1,
                           opr: 'U',
                       }
                        dataupdate.push(data);
                    }

                }
                debugger;
                if (dataupdate.length > 0) {
                    debugger;
                    $http.post(ENV.apiUrl + "api/TransferEmp/TransEmpUpdate", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        debugger;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, showCloseButton: true });
                            $scope.getEmployee($scope.tempD.em_Company_Code, $scope.tempD.em_Dept_code, $scope.tempD.em_Grade_code,
                                $scope.tempD.em_Desg_code, $scope.tempD.em_Staff_type_code, $scope.tempD.em_full_name, $scope.tempD.em_login_code);
                        }
                        else {
                            swal({ text: "Record Not Updated", imageUrl: "assets/img/close.png", width: 300, showCloseButton: true });
                        }

                        // $scope.stud_Stud_Img_Details_fun($scope.temp.sims_cur_code, $scope.temp.sims_academic_year, $scope.temp.sims_grade_code, $scope.temp.sims_section_code);
                    })
                    $scope.table = true;
                    $scope.newdisplay = false;
                    dataupdate = [];
                }
                else {
                    swal({ text: "Change Atleast one Record to Update", imageUrl: "assets/img/notification-alert.png", width: 380, showCloseButton: true });
                }
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }])
})();