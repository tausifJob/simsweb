﻿(function () {
    'use strict';

    var main;
    var subject_code = [], subject_code1 = [], sib_enroll_code = [];

    var cur_code;
    var section_code, sectioncodemodal1;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('LeaveDetailsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
          //  $http.defaults.headers.common['schoolId'] = 'siserp';
            $scope.div_lwphst = false;
            $scope.parent = false;
            $scope.student = false;
            $scope.buttons = false;
            $scope.currentab = '';
            var arr_check = [];
            var username1 = $rootScope.globals.currentUser.username;
            $scope.users = false;
            $scope.SubmitApproveLeaveBtn = false;
           
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            $scope.cdetails_data = [];

            $scope.pagesize_cancel = "10";
            $scope.pageindex_cancel = "0";
            $scope.pager3 = true;
            $scope.filteredTodos_cancel = [], $scope.currentPage_cancel = 1, $scope.numPerPage_cancel = 10, $scope.maxSize_cancel = 10;
            $scope.cdetails_data_cancel = [];
            $scope.cancel_approvedlist = [];

            $scope.pagesize_update = "10";
            $scope.pageindex_update = "0";
            $scope.pager2 = true;
            $scope.filteredTodos_update = [], $scope.currentPage_update = 1, $scope.numPerPage_update = 10, $scope.maxSize_update = 10;
            $scope.cdetails_data_update = [];
            $scope.update_approvedlist = [];

            $scope.pagesize_reschedule = "10";
            $scope.pageindex_reschedule = "0";
            $scope.pager6 = true;
            $scope.filteredTodos_reschedule = [], $scope.currentPage_reschedule = 1, $scope.numPerPage_reschedule = 10, $scope.maxSize_reschedule = 10;
            $scope.reschedule_list = [];

            $scope.pagesize_lwp = "10";
            $scope.pageindex_lwp = "0";
            $scope.pager4 = true;
            $scope.filteredTodos_lwp = [], $scope.currentPage_lwp = 1, $scope.numPerPage_lwp = 10, $scope.maxSize_lwp = 10;
            $scope.cdetails_data_lwp = [];
            $scope.lwp_approvedlist = [];

            $scope.pagesize_lwphst = "10";
            $scope.pageindex_lwphst = "0";
            $scope.pager4 = true;
            $scope.filteredTodos_lwphst = [], $scope.currentPage_lwphst = 1, $scope.numPerPage_lwphst = 10, $scope.maxSize_lwphst = 10;
            $scope.emp_lwpdetails_lwphst = [];
            //$scope.lwp_approvedlist = [];

            $scope.pagesize_holiday = "10";
            $scope.pageindex_holiday = "0";
            $scope.pager5 = true;
            $scope.filteredTodos_holiday = [], $scope.currentPage_holiday = 1, $scope.numPerPage_holiday = 10, $scope.maxSize_holiday = 10;
            $scope.cdetails_data_holiday = [];
            $scope.lwp_approvedlist = [];

            $rootScope.visible_stud = false;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = true;
            $scope.showForSISO = false;
            $scope.DisableUpdateLeaveBtn = false;
            $scope.disabledCancelLeave = false;
            $scope.disableRescheduleLeave = false;

            $rootScope.chkMulti = true;
            var dt = new Date();
            $scope.edt =
                {
                    leave_type2: '',
                    leave_type1: '',
                    lstart_date: ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear()
                }


            var schoolArray = ['siso', 'abqis', 'asis'];

            if (schoolArray.includes($http.defaults.headers.common['schoolId'])) {
                $scope.showForSISO = true;
            }

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixedtable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixedtable2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixedtable3").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixedtable4").tableHeadFixer({ 'top': 1 });
            }, 100);

            setTimeout(function () {
                $("#cmb_emps").select2();
            }, 100);
            setTimeout(function () {
                $("#cmb_emps1").select2();
            }, 100);        

            $http.get(ENV.apiUrl + "api/leavedetails/GET_ManagerConfirmTag").then(function (ManagerConfirmTag) {
                $scope.Manager_ConfirmTag = ManagerConfirmTag.data;
                console.log($scope.Manager_ConfirmTag);
            });

            $http.get(ENV.apiUrl + "api/leavedetails/GetAllEMPLeaveDetails?comp_code=1").then(function (res) {
                $scope.cdetails_data = res.data;
                $scope.totalItems = $scope.cdetails_data.length;
                $scope.todos = $scope.cdetails_data;
                $scope.makeTodos();
            });

            $scope.downloaddoc = function (str) {
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeDocument/' + str;
                window.open($scope.url);
            }

            $scope.getRescheduleleaves = function () {
                $http.get(ENV.apiUrl + "api/leavedetails/GetAllEMPLeaveDetailsForReschedule?comp_code=1").then(function (res) {
                    $scope.reschedule_list = res.data;
                    $scope.totalItems_reschedule = $scope.reschedule_list.length;
                    $scope.todos_reschedule = $scope.reschedule_list;
                    $scope.makeTodos_reschedule();
                });
            }

            $scope.getRescheduleleaves();
            

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.cdetails_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.cdetails_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.cdetails_data;
                }
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            function searchUtil(item, toSearch) {
                /* Search Text in all 2 fields */
                return (item.empName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.em_login_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $http.get(ENV.apiUrl + "api/leavedetails/Get_EMPLeaveDetails").then(function (Get_EMPLeaveDetails) {
                $scope.EMPLeaveDetails = Get_EMPLeaveDetails.data;
            });

            $http.get(ENV.apiUrl + "api/leavedetails/GetEmployeeData").then(function (GetEmployeeData) {
                $scope.GetEmployeeData = GetEmployeeData.data;
            });

            //$http.get(ENV.apiUrl + "api/leavedetails/Get_dept_Id").then(function (res) {
            //    $scope.Getdepartment = res.data;
            //    console.log($scope.Getdepartment);
            //});

            $http.get(ENV.apiUrl + "api/leavedetails/Get_dept_IdWithCompany?compcode" + $scope.compcode).then(function (res) {
                $scope.Getdepartment = res.data;
                console.log($scope.Getdepartment);
            });

            $scope.checkPayrollFinalized = function (info) {
                $scope.SubmitApproveLeaveBtn = false;
                $http.get(ENV.apiUrl + "api/leavedetails/GetFinalizeSatus?emp_id=" + info.em_login_code + "&leave_start_date=" + info.lt_start_date + "&leave_end_date=" + info.lt_end_date).then(function (res) {
                    $scope.getFinaliizedStaus = res.data;
                    if ($scope.getFinaliizedStaus.length > 0) {
                        if (($scope.getFinaliizedStaus[0].pe_status == 'P' || $scope.getFinaliizedStaus[0].pe_status == 'F') && (info.lt_mgr_confirm_tag == 'C')) {
                            swal({ title: "Alert", text: "You can not approve leave.Payroll is finalized for " + info.em_login_code, showCloseButton: true, width: 380, })
                            $scope.SubmitApproveLeaveBtn = true;
                        }
                    }

                    console.log("getFinaliizedStaus",$scope.getFinaliizedStaus);
                });
            }

            $scope.GetLeaveDetails = function () {

                $http.get(ENV.apiUrl + "api/leavedetails/GetEMPLeaveDetails?leave_code=" + $scope.edt.leave_type).then(function (GetEMPLeaveDetails) {

                    $scope.GetEmployeeLeaveDetails = GetEMPLeaveDetails.data;

                    console.log($scope.GetEmployeeLeaveDetails);
                });
            }

            $scope.UserSearch_Cancel = function (str) {
                $scope.filteredTodos_cancel = [];
                $scope.Get_approed_employee_list = [];
                $scope.filteredTodos_update = [];

               
                //var leave_t = $scope.edt.leave_type2;
                $scope.edt.comp_code = '';
                $("#cmb_emps").select2("val", "");

                if ($scope.edt.leave_type1 == 'undefined' || $scope.edt.leave_type1 == '') {
                    swal({ title: "Alert", text: "Please Select Leave Type.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }
                else {
                    $scope.Get_cancel_employee_list = [];
                    $('#ApproveModal').modal('hide');
                    $('#UpdateApprovedLeavesModel').modal('hide');
                    $('#CancelApprovedLeavesModel').modal({ backdrop: "static" });

                    $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res_company) {
                       
                        $scope.comp_data = res_company.data;
                        console.log($scope.comp_data);
                        if ($scope.comp_data.length > 0) {
                            $scope.edt.compcode1 = $scope.comp_data[0].dept_comp_code;

                            $http.get(ENV.apiUrl + "api/leavedetails/Get_dept_Idcompany?compcode=" + $scope.edt.compcode1).then(function (res) {
                                $scope.Getdepartment = res.data;
                                console.log($scope.Getdepartment);
                            });
                        }
                    });

                    $http.get(ENV.apiUrl + "api/leavedetails/Get_approed_employee_list?leave_code=" + $scope.edt.leave_type1 + "&dept_code=" + $scope.edt.comp_code + "&login_code=" + $("#cmb_emps").find("option:selected").text()).then(function (Get_cancel_employee_list) {
                        $scope.Get_cancel_employee_list = Get_cancel_employee_list.data;
                        $scope.busy = false;
                        $scope.searchtable_cancelapprove = true;
                    });

                }
            }

            var sendata = [];

            $scope.getfinsdepts = function () {

                $scope.filteredTodos_cancel = [];
                $scope.Get_approed_employee_list = [];
                $scope.filteredTodos_update = [];

                $http.get(ENV.apiUrl + "api/leavedetails/Get_dept_Idcompany?compcode=" + $scope.edt.compcode1).then(function (res) {
                    $scope.Getdepartment = res.data;
                    console.log($scope.Getdepartment);
                });
            }

            $scope.CheckEmployeeLeave = function () {

                //$('#ApproveModal').modal('show')
                $('#ApproveModal').modal({ backdrop: "static" });
                $scope.edt.leave_type = '';
                $scope.edt.login_code = '';

                $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res_company) {
                   
                    $scope.comp_data = res_company.data;
                    console.log($scope.comp_data);
                    if ($scope.comp_data.length > 0) {
                        $scope.edt.compcode1 = $scope.comp_data[0].dept_comp_code;

                        $http.get(ENV.apiUrl + "api/leavedetails/Get_dept_Idcompany?compcode=" + $scope.edt.compcode1).then(function (res) {
                            $scope.Getdepartment = res.data;
                            console.log($scope.Getdepartment);
                        });
                    }
                });               
            }

         

            $scope.SubmitApprovedLeave = function () {
                var datasend = [];
                $scope.SubmitApproveLeaveBtn = true;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    
                    if ($scope.filteredTodos[i].lt_mgr_confirm_tag != 'N' || $scope.filteredTodos[i].lt_mgr_confirm_tag == undefined) {
                        datasend.push($scope.filteredTodos[i]);
                    }
                }

                if ($scope.showForSISO) {
                    $http.post(ENV.apiUrl + "api/leavedetails/Update_pay_leavedoc_SISO", datasend).then(function (res) {

                        $scope.o1 = res.data;
                        console.log($scope.o1);
                        $scope.SubmitApproveLeaveBtn = false;
                        if ($scope.o1 == true || $scope.o1 == 1) {
                            swal('', 'Selected Leaves Updated Successfully.');
                        }
                        else if ($scope.o1 == 2) {
                            swal('', 'You can not approve leaves more than leave pending days');
                        }

                        $http.get(ENV.apiUrl + "api/leavedetails/GET_ManagerConfirmTag").then(function (ManagerConfirmTag) {
                            $scope.Manager_ConfirmTag = ManagerConfirmTag.data;
                            console.log($scope.Manager_ConfirmTag);
                        });

                        $http.get(ENV.apiUrl + "api/leavedetails/GetAllEMPLeaveDetails?comp_code=1").then(function (res) {

                            $scope.cdetails_data = res.data;
                            $scope.totalItems = $scope.cdetails_data.length;
                            $scope.todos = $scope.cdetails_data;
                            $scope.makeTodos();
                        });

                        $scope.getRescheduleleaves();
                    });
                }
                else {
                    $http.post(ENV.apiUrl + "api/leavedetails/Update_pay_leavedoc", datasend).then(function (res) {

                        $scope.o1 = res.data;
                        console.log($scope.o1);
                        $scope.SubmitApproveLeaveBtn = false;
                        if ($scope.o1 == true || $scope.o1 == 1) {
                            swal('', 'Selected Leaves Updated Successfully.');
                        }
                        else if ($scope.o1 == 2) {
                            swal('', 'You can not approve leaves more than leave pending days');
                        }

                        $http.get(ENV.apiUrl + "api/leavedetails/GET_ManagerConfirmTag").then(function (ManagerConfirmTag) {
                            $scope.Manager_ConfirmTag = ManagerConfirmTag.data;
                            console.log($scope.Manager_ConfirmTag);
                        });

                        $http.get(ENV.apiUrl + "api/leavedetails/GetAllEMPLeaveDetails?comp_code=1").then(function (res) {

                            $scope.cdetails_data = res.data;
                            $scope.totalItems = $scope.cdetails_data.length;
                            $scope.todos = $scope.cdetails_data;
                            $scope.makeTodos();
                        });

                        $scope.getRescheduleleaves();
                    }); 
                }
            }

            $scope.Useredit = function (info) {

               
                if ($scope.approvedleave == true) {
                    $scope.EMPLeave_Details1 = [];
                    $scope.EMPLeave_Details = [];

                    $http.post(ENV.apiUrl + "api/leavedetails/CAL_geteavesforEmp?empid=" + info.em_login_code + "&leave_code=" + $scope.edt.leave_type1).then(function (Get_EMPLeave_Details) {
                        $scope.EMPLeave_Details1 = Get_EMPLeave_Details.data;
                        $('#Employee').modal('hide');
                        if ($scope.EMPLeave_Details1.length > 0) {
                            $scope.EMPLeave_Details = Get_EMPLeave_Details.data;
                        }
                        else {
                            swal('', 'Record Not Found');
                        }
                    });
                }
                else if ($scope.lwp == true) {

                    $scope.GET_LWPforEmployees1 = [];
                    $scope.GET_LWPforEmployees = [];
                    $http.get(ENV.apiUrl + "api/leavedetails/GET_LWPforEmployees?empcode=" + info.em_login_code + "&date=" + $scope.edt.date).then(function (GET_LWPforEmployees) {
                        $scope.GET_LWPforEmployees1 = GET_LWPforEmployees.data;

                        if ($scope.GET_LWPforEmployees1.length > 0) {
                            $scope.GET_LWPforEmployees = GET_LWPforEmployees.data;

                            console.log($scope.GET_LWPforEmployees1);
                        }
                        else {
                            swal('', 'Record Not Found');
                        }
                    });
                }
            }

            var lwpsenddata = [];

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
           

            $scope.SearchAllTeacher = function () {

                $scope.searchtable_cancelapprove = false;
                $scope.searchtable_updateapprove = false;

               
                console.log('In searchallteacher');
                console.log($scope.edt.comp_code);
                console.log($scope.edt.login_code);
                console.log($scope.edt.leave_type);
                $scope.busy = true;

                $http.get(ENV.apiUrl + "api/leavedetails/Get_Employee_leave_balance?leave_code=" + $scope.edt.leave_type + "&dept_code=" + $scope.edt.comp_code + "&login_code=" + $scope.edt.login_code+"&compcode="+$scope.edt.compcode1).then(function (Get_approed_employee_list) {
                    $scope.Get_approed_employee_list = Get_approed_employee_list.data;
                    $scope.busy = false;
                    $scope.searchtable = true;
                });
            }
            
            $scope.Reset = function () {

                $scope.edt.leave_type2 = ''
                $scope.edt.comp_code = ''
                $scope.edt.login_code = ''
                $("#cmb_emps").select2("val", "");
                $("#cmb_emps1").select2("val", "");
            }

            //////////////////////////////////////////////
            //Cancel Approved Leaves

            $scope.Search_forCancelApprovedLeaves = function () {
               
                var leave_t = $scope.edt.leave_type1;

                if (leave_t == '' || leave_t == 'undefined') {
                    swal({ title: "Alert", text: "Please Select Leave Type.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }
                else {
                    //main = document.getElementById('mainchk1');
                    //if (main.checked == true) {
                    //    main.checked = false;
                    //}

                    $scope.searchtable = false;
                    $scope.searchtable_updateapprove = false;
                   
                    console.log('In searchallteacher');
                    console.log($scope.edt.comp_code);
                    console.log($scope.edt.login_code);
                    console.log($scope.edt.leave_type);
                    $scope.busy = true;

                    $http.get(ENV.apiUrl + "api/leavedetails/Get_approed_employee_list?leave_code=" + $scope.edt.leave_type1 + "&dept_code=" + $scope.edt.comp_code + "&login_code=" + $scope.edt.login_code).then(function (Get_cancel_employee_list) {
                        $scope.Get_cancel_employee_list = Get_cancel_employee_list.data;
                        $scope.busy = false;
                        $scope.searchtable_cancelapprove = true;
                    });
                }
            }

            $scope.GetEmployeeleaveDetails = function (info) {
                $http.get(ENV.apiUrl + "api/leavedetails/GetEmployeePendingLeaveDetails?emp_code=" + info.em_number).then(function (Get_EmployeePendingLeaveDetails) {
                    $scope.EmployeePendingLeaveDetails = Get_EmployeePendingLeaveDetails.data;
                    $('#ApproveModal').modal('hide')
                });
            }

            $scope.MultipleSelect = function () {

                $scope.enroll_number = [];
                main = document.getElementById('mainchk1');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.student.length; i++) {
                        var t = $scope.student[i].s_enroll_no;
                        var v = document.getElementById(t + i);
                        v.checked = true;

                        $scope.enroll_number.push($scope.student[i]);
                    }
                }
                else {
                    for (var i = 0; i < $scope.student.length; i++) {
                        var t = $scope.student[i].s_enroll_no;
                        var v = document.getElementById(t + i);
                        v.checked = false;
                        $scope.enroll_number = [];
                    }
                }


            }

            $scope.DataEnroll = function () {
               
                for (var i = 0; i < $scope.Get_cancel_employee_list.length; i++) {
                    var t = $scope.Get_cancel_employee_list[i].lt_no_cancel;
                    var v = document.getElementById(t + i);
                    if (v.checked == true)
                        $scope.cancel_approvedlist.push($scope.Get_cancel_employee_list[i]);
                }

                $scope.Get_cancel_employee_list = [];

                for (var i = 0; i < $scope.cancel_approvedlist.length; i++) {
                    // var date = 
                    //$scope.cancel_approvedlist[i].lt_start_date = moment($scope.cancel_approvedlist[i].lt_start_date).format('YYYY-MM-DD');
                    //$scope.cancel_approvedlist[i].lt_end_date = moment($scope.cancel_approvedlist[i].lt_end_date).format('YYYY-MM-DD');
                    $scope.cancel_approvedlist[i].lt_start_date = $scope.cancel_approvedlist[i].lt_start_date;
                    $scope.cancel_approvedlist[i].lt_end_date = $scope.cancel_approvedlist[i].lt_end_date;
                }
                
                $scope.cdetails_data_cancel = $scope.cancel_approvedlist;
                $scope.totalItems_cancel = $scope.cdetails_data_cancel.length;
                $scope.todos_cancel = $scope.cdetails_data_cancel;
                $scope.makeTodos_cancel();
            }

            $scope.makeTodos_cancel = function () {
                var rem_cancel = parseInt($scope.totalItems_cancel % $scope.numPerPage_cancel);
                if (rem_cancel == '0') {
                    $scope.pagersize_cancel = parseInt($scope.totalItems_cancel / $scope.numPerPage_cancel);
                }
                else {
                    $scope.pagersize_cancel = parseInt($scope.totalItems_cancel / $scope.numPerPage_cancel) + 1;
                }
                var begin_cancel = (($scope.currentPage_cancel - 1) * $scope.numPerPage_cancel);
                var end_cancel = parseInt(begin_cancel) + parseInt($scope.numPerPage_cancel);

                $scope.filteredTodos_cancel = $scope.todos_cancel.slice(begin_cancel, end_cancel);
            };

            $scope.size_cancel = function (str) {
                console.log(str);
                //$scope.pagesize_cancel = str;
                //$scope.currentPage_cancel = 1;
                //$scope.numPerPage_cancel = str; console.log("numPerPage=" + $scope.numPerPage_cancel); $scope.makeTodos_cancel();
               
                if (str == "All") {
                    $scope.currentPage_cancel = '1';
                    $scope.filteredTodos_cancel = $scope.cdetails_data_cancel;
                    $scope.pager3 = false;
                }
                else {
                    $scope.pager3 = true;
                    $scope.pagesize_cancel = str;
                    $scope.currentPage_cancel = 1;
                    $scope.numPerPage_cancel = str;
                    $scope.makeTodos_cancel();
                }
            }

            $scope.index_cancel = function (str) {
                $scope.pageindex_cancel = str;
                $scope.currentPage_cancel = str; console.log("currentPage=" + $scope.currentPage_cancel); $scope.makeTodos_cancel();
                main_cancel.checked = false;
                $scope.row1 = '';
            }

            $scope.search_cancel = function () {
                $scope.todos_cancel = $scope.searched_cancel($scope.cdetails_data_cancel, $scope.searchText_cancel);
                $scope.totalItems_cancel = $scope.todos_cancel.length;
                $scope.currentPage_cancel = '1';
                if ($scope.searchText_cancel == '') {
                    $scope.todos_cancel = $scope.cdetails_data_cancel;
                }
                $scope.makeTodos_cancel();
            }

            $scope.searched_cancel = function (valLists_cancel, toSearch_cancel) {

                return _.filter(valLists_cancel,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil_cancel(i, toSearch_cancel);
                });
            };

            function searchUtil_cancel(item, toSearch) {
                /* Search Text in all 2 fields */
                return (item.empName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.em_login_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos_cancel.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos_cancel.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function (info) {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
                
                $http.get(ENV.apiUrl + "api/leavedetails/GetFinalizeSatus?emp_id=" + info.em_number + "&leave_start_date=" + info.lt_start_date + "&leave_end_date=" + info.lt_end_date).then(function (res) {
                    $scope.getFinaliizedStaus = res.data;
                    if ($scope.getFinaliizedStaus.length > 0) {
                        if ($scope.getFinaliizedStaus[0].pe_status == 'P' || $scope.getFinaliizedStaus[0].pe_status == 'F') {
                            swal({ title: "Alert", text: "You can not update leave.Payroll is finalized for " + info.em_number, showCloseButton: true, width: 380, })
                            if (info.lt_no_update1 == true) {
                                info.lt_no_update1 = false;
                            }                            
                        }
                    }                    
                });
            }

            $scope.checkDateisEmpty = function (info) {

                if(info.lt_start_date_new == undefined || info.lt_start_date_new == "" || info.lt_start_date_new == null) {
                    swal({ title: "Alert", text: "Start date can not be blank", showCloseButton: true, width: 380 });
                    info.lt_reschedule = false;
                    return;
                }
                if (info.lt_end_date_new == undefined || info.lt_end_date_new == "" || info.lt_end_date_new == null) {
                    swal({ title: "Alert", text: "End date can not be blank", showCloseButton: true, width: 380 });
                    info.lt_reschedule = false;
                    return;
                }

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

                $http.get(ENV.apiUrl + "api/leavedetails/GetFinalizeSatus?emp_id=" + info.em_login_code + "&leave_start_date=" + info.lt_start_date + "&leave_end_date=" + info.lt_end_date).then(function (res) {
                    $scope.getFinaliizedStaus = res.data;
                    if ($scope.getFinaliizedStaus.length > 0) {
                        if ($scope.getFinaliizedStaus[0].pe_status == 'P' || $scope.getFinaliizedStaus[0].pe_status == 'F') {
                            swal({ title: "Alert", text: "You can not update leave.Payroll is finalized for " + info.em_login_code, showCloseButton: true, width: 380, })
                            if (info.lt_no_update1 == true) {
                                info.lt_no_update1 = false;
                            }
                        }
                    }
                });
            }

            $scope.ApprovedLeaveCancel = function () {
                $scope.disabledCancelLeave = true;
                for (var i = 0; i < $scope.filteredTodos_cancel.length; i++) {

                    if ($scope.filteredTodos_cancel[i].lt_no_cancel == true) {
                        sendata.push($scope.filteredTodos_cancel[i]);
                    }
                }

                if (sendata.length > 0) {
                    $http.post(ENV.apiUrl + "api/leavedetails/CALeaves", sendata).then(function (LeaveDetails) {
                        $scope.Leave_Details = LeaveDetails.data;
                        if ($scope.Leave_Details == true) {
                            swal('', 'Approved Leave Cancelled');
                            $scope.EMPLeave_Details = [];
                            $scope.filteredTodos_cancel = [];
                            $scope.edt.leave_type1 = null;
                            $scope.searchText_cancel = null;
                            //$scope.SelectedUserLst = null;
                        }
                        else {
                            swal('', 'Approved Leave Not Cancelled');
                        }
                        $scope.disabledCancelLeave = false;

                    });
                    sendata = [];
                }
                else {
                    swal('', 'Please select employee to cancel leave.');
                }
            }

            ///////////////////////////////////////////////////////////////
            //Update Approved Leave

            $scope.getfinsdepts_update = function (str) {

                $http.get(ENV.apiUrl + "api/leavedetails/Get_dept_Idcompany?compcode=" + str).then(function (res) {
                    $scope.Getdepartment = res.data;
                    console.log($scope.Getdepartment);
                });
            }


            $scope.UserSearch_update = function (str) {
                $scope.filteredTodos_update = [];
                $scope.filteredTodos_cancel = [];
                $scope.Get_approed_employee_list = [];
               
                //var leave_t = $scope.edt.leave_type2;
                $scope.edt.update_dept_code = '';
                $("#cmb_emps1").select2("val", "");

                $scope.searchtable = false;
                $scope.searchtable_cancelapprove = false;

                if ($scope.edt.leave_type2 == 'undefined' || $scope.edt.leave_type2 == '') {
                    swal({ title: "Alert", text: "Please Select Leave Type.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }
                else {
                    $scope.Get_update_employee_list = [];
                    // $scope.filteredTodos_update = [];
                    $('#ApproveModal').modal('hide');
                    $('#CancelApprovedLeavesModel').modal('hide');
                    $('#UpdateApprovedLeavesModel').modal({ backdrop: "static" });

                    $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res_company) {
                       
                        $scope.comp_data = res_company.data;
                        console.log($scope.comp_data);
                        if ($scope.comp_data.length > 0) {
                            $scope.edt.compcode1 = $scope.comp_data[0].dept_comp_code;

                            $http.get(ENV.apiUrl + "api/leavedetails/Get_dept_Idcompany?compcode=" + $scope.edt.compcode1).then(function (res) {
                                $scope.Getdepartment = res.data;
                                console.log($scope.Getdepartment);
                            });
                        }
                    });

                    $http.get(ENV.apiUrl + "api/leavedetails/Get_approed_employee_list?leave_code=" + $scope.edt.leave_type2 + "&dept_code=" + $scope.edt.update_dept_code + "&login_code=" + $("#cmb_emps1").find("option:selected").text()).then(function (Get_update_employee_list) {
                        $scope.Get_update_employee_list = Get_update_employee_list.data;
                        $scope.busy = false;
                        $scope.searchtable_updateapprove = true;
                    });
                    //$('#UpdateApprovedLeavesModel').modal('show')
                }
            }

            $scope.Reset_update = function () {

                $scope.edt.update_login_code = ''
                $scope.edt.update_dept_code = ''
                $("#cmb_emps").select2("val", "");
                $("#cmb_emps1").select2("val", "");
            }

            $scope.Search_forupdateApprovedLeaves = function () {
               
                // var leave_t = $scope.edt.leave_type2;

                if ($scope.edt.leave_type2 == 'undefined') {
                    swal({ title: "Alert", text: "Please Select Leave Type.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }
                else {
                    $scope.searchtable = false;
                    $scope.searchtable_cancelapprove = false;

                   
                    console.log('In searchallteacher');
                    console.log($scope.edt.update_dept_code);
                    console.log($scope.edt.login_code);
                    console.log($scope.edt.leave_type2);
                    $scope.busy = true;
                    //$("#cmb_emps1").find("option:selected").text()

                    $http.get(ENV.apiUrl + "api/leavedetails/Get_approed_employee_list?leave_code=" + $scope.edt.leave_type2 + "&dept_code=" + $scope.edt.update_dept_code + "&login_code=" + $scope.edt.update_login_code).then(function (Get_update_employee_list) {
                        $scope.Get_update_employee_list = Get_update_employee_list.data;
                        $scope.busy = false;
                        $scope.searchtable_updateapprove = true;
                    });
                }
            }

            $scope.DataEnroll_update = function () {
                $scope.update_approvedlist = [];
                for (var i = 0; i < $scope.Get_update_employee_list.length; i++) {
                    var t = $scope.Get_update_employee_list[i].lt_no_update;
                    var v = document.getElementById(t + i);
                    if (v.checked == true)
                        $scope.update_approvedlist.push($scope.Get_update_employee_list[i]);
                }

                $scope.Get_update_employee_list = [];

                for (var i = 0; i < $scope.update_approvedlist.length; i++) {
                    // var date = 
                    //$scope.update_approvedlist[i].lt_start_date = moment($scope.update_approvedlist[i].lt_start_date).format('YYYY/MM/DD');
                    //$scope.update_approvedlist[i].lt_end_date = moment($scope.update_approvedlist[i].lt_end_date).format('YYYY/MM/DD');
                    //$scope.update_approvedlist[i].lt_start_date = moment($scope.update_approvedlist[i].lt_start_date).format('DD-MM-YYYY');
                    //$scope.update_approvedlist[i].lt_end_date = moment($scope.update_approvedlist[i].lt_end_date).format('DD-MM-YYYY');
                    $scope.update_approvedlist[i].lt_start_date = $scope.update_approvedlist[i].lt_start_date;
                    $scope.update_approvedlist[i].lt_end_date = $scope.update_approvedlist[i].lt_end_date;

                }

                $scope.cdetails_data_update = $scope.update_approvedlist;
                $scope.totalItems_update = $scope.cdetails_data_update.length;
                $scope.todos_update = $scope.cdetails_data_update;
                $scope.makeTodos_update();
            }

            $scope.makeTodos_update = function () {
                var rem_update = parseInt($scope.totalItems_update % $scope.numPerPage_update);
                if (rem_update == '0') {
                    $scope.pagersize_update = parseInt($scope.totalItems_update / $scope.numPerPage_update);
                }
                else {
                    $scope.pagersize_update = parseInt($scope.totalItems_update / $scope.numPerPage_update) + 1;
                }
                var begin_update = (($scope.currentPage_update - 1) * $scope.numPerPage_update);
                var end_update = parseInt(begin_update) + parseInt($scope.numPerPage_update);

                $scope.filteredTodos_update = $scope.todos_update.slice(begin_update, end_update);
            };

            $scope.makeTodos_reschedule = function () {
                debugger
                var rem_reschedule = parseInt($scope.totalItems_reschedule % $scope.numPerPage_reschedule);
                if (rem_reschedule == '0') {
                    $scope.pagersize_reschedule = parseInt($scope.totalItems_reschedule / $scope.numPerPage_reschedule);
                }
                else {
                    $scope.pagersize_reschedule = parseInt($scope.totalItems_reschedule / $scope.numPerPage_reschedule) + 1;
                }
                var begin_reschedule = (($scope.currentPage_reschedule - 1) * $scope.numPerPage_reschedule);
                var end_reschedule = parseInt(begin_reschedule) + parseInt($scope.numPerPage_reschedule);

                $scope.filteredTodos_reschedule = $scope.todos_reschedule.slice(begin_reschedule, end_reschedule);
            };

            $scope.size_update = function (str) {
                //console.log(str);
                //$scope.pagesize_update = str;
                //$scope.currentPage_update = 1;
                //$scope.numPerPage_update = str; console.log("numPerPage=" + $scope.numPerPage_update); $scope.makeTodos_update();
                //$scope.currentPage_update = 1, $scope.numPerPage_update = 10, $scope.maxSize_update = 10;
               
                if (str == "All") {
                    $scope.currentPage_update = '1';
                    $scope.filteredTodos_update = $scope.todos_update;
                    $scope.pager2 = false;
                }
                else {
                    $scope.pager2 = true;
                    $scope.pagesize_update = str;
                    $scope.currentPage_update = 1;
                    $scope.numPerPage_update = str;
                    $scope.makeTodos_update();
                }
            }
            $scope.size_reschedule = function (str) {
              
                if (str == "All") {
                    $scope.currentPage_reschedule = '1';
                    $scope.filteredTodos_reschedule = $scope.todos_reschedule;
                    $scope.pager6 = false;
                }
                else {
                    $scope.pager6 = true;
                    $scope.pagesize_reschedule = str;
                    $scope.currentPage_reschedule = 1;
                    $scope.numPerPage_reschedule = str;
                    $scope.makeTodos_reschedule();
                }
            }

            $scope.index_update = function (str) {
                $scope.pageindex_update = str;
                $scope.currentPage_update = str; console.log("currentPage=" + $scope.currentPage_update); $scope.makeTodos_update();
                main_update.checked = false;
                $scope.row1 = '';
            }
            $scope.index_reschedule = function (str) {
                $scope.pageindex_reschedule = str;
                $scope.currentPage_reschedule = str; console.log("currentPage=" + $scope.currentPage_reschedule); $scope.makeTodos_reschedule();
                main_reschedule.checked = false;
                $scope.row1 = '';
            }
          //  _reschedule
            $scope.search_update = function () {
                $scope.todos_update = $scope.searched_update($scope.cdetails_data_update, $scope.searchText_update);
                $scope.totalItems_update = $scope.todos_update.length;
                $scope.currentPage_update = '1';
                if ($scope.searchText_update == '') {
                    $scope.todos_update = $scope.cdetails_data_update;
                }
                $scope.makeTodos_update();
            }
            $scope.search_reschedule = function () {
                $scope.todos_reschedule = $scope.searched_reschedule($scope.reschedule_list, $scope.searchText_reschedule);
                $scope.totalItems_reschedule = $scope.todos_reschedule.length;
                $scope.currentPage_reschedule = '1';
                if ($scope.searchText_reschedule == '') {
                    $scope.todos_reschedule = $scope.reschedule_list;
                }
                $scope.makeTodos_reschedule();
            }

            $scope.searched_update = function (valLists_update, toSearch_update) {

                return _.filter(valLists_update, 

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil_update(i, toSearch_update);
                });
            };

            $scope.searched_reschedule = function (valLists_reschedule, toSearch_reschedule) {

                return _.filter(valLists_reschedule,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil_reschedule(i, toSearch_reschedule);
                });
            };


            function searchUtil_update(item, toSearch)
            {
                /* Search Text in all 2 fields */
                return (item.empName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.em_login_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            function searchUtil_reschedule(item, toSearch) {
                /* Search Text in all 2 fields */
                return (item.empName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.em_login_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            var sendata_update = [];
            var date2;
            var date1;
            var timeDiff;
            var diffDays;

            $scope.ApprovedLeaveUpdate = function ()
            {
                $scope.insert = false;
                $scope.DisableUpdateLeaveBtn = true;

                for (var i = 0; i < $scope.filteredTodos_update.length; i++)
                {
                    var start_date = moment($scope.filteredTodos_update[i].lt_start_date, "DD-MM-YYYY").format('YYYY-MM-DD');
                    var end_date = moment($scope.filteredTodos_update[i].lt_end_date_new, "DD-MM-YYYY").format('YYYY-MM-DD');

                    date2 = new Date(end_date);
                    date1 = new Date(start_date);
                    timeDiff = Math.abs(date2.getTime() - date1.getTime());
                    diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                   

                     //$scope.filteredTodos_update[i].lt_end_date_new=
                    if ($scope.filteredTodos_update[i].lt_no_update1 == true)
                    {
                        if ($scope.filteredTodos_update[i].lt_end_date_new == '' || $scope.filteredTodos_update[i].lt_end_date_new == 'undefined') {
                            swal('', 'Please enter new updated end date for selected employee.');
                           // break;
                            return;
                        }
                        else
                        {
                            $scope.filteredTodos_update[i].lt_days = diffDays + 1;
                            console.log(diffDays);
                            $scope.insert = true;
                            sendata_update.push($scope.filteredTodos_update[i]);
                        }

                    }
                }
                console.log(sendata_update);
               
                if ($scope.insert)
                {
                    if (sendata_update.length > 0)
                    {
                        $http.post(ENV.apiUrl + "api/leavedetails/UpdateLeaves", sendata_update).then(function (LeaveDetails_update) {
                            $scope.LeaveDetails_update = LeaveDetails_update.data;
                            if ($scope.LeaveDetails_update == true) {
                                swal('', 'Approved Leave Updated.');
                                $scope.EMPLeave_Details = [];
                                $scope.filteredTodos_update = [];
                                $scope.edt.leave_type2 = null;
                                $scope.searchText_update = null;
                                //$scope.SelectedUserLst = null;
                            }
                            else if ($scope.LeaveDetails_update == false) {
                                swal('', 'Approved Leave Not Updated.');
                            }
                            else {
                                swal("Error-" + $scope.LeaveDetails_update)
                            }
                            $scope.DisableUpdateLeaveBtn = false;

                            $scope.getRescheduleleaves();

                        });

                        sendata_update = [];
                    }
                    else
                    {
                        swal('', 'Please select employee to update leave.');
                    }
                }
                else
                {
                    swal('', 'Please select atleast one record.');
                }
            }

            $scope.ApprovedLeaveReset = function ()
            {
                $scope.filteredTodos_update = [];
                $scope.edt.leave_type2 = '';
                $scope.searchText_update = '';
                $scope.disableRescheduleLeave = false;
                $scope.DisableUpdateLeaveBtn = false;

            }
            $scope.CancelApprovedLeaveReset = function () {
               
                $scope.filteredTodos_cancel = [];               
                $scope.cancel_approvedlist = [];
                 $scope.edt.leave_type1 = '';
                 $scope.searchText_cancel = '';
                 $scope.disabledCancelLeave = false;
            }
            $scope.MarkLWPForEmployeeReset = function () {
                $scope.filteredTodos_lwp = [];
                $scope.searchText_lwp = '';
            }

            $scope.GetUpdateApprovedData_Clear = function () {
                // $scope.filteredTodos_update = [];
                //$scope.edt.leave_type2 = '';
            }

            /////////////////////////////////////////////////////////////
            //MarK LWP EMPLOYEE

            if ($http.defaults.headers.common['schoolId'] == 'siserp')
            {
                $http.get(ENV.apiUrl + "api/leavedetails/GetAllEMPLeaveLWPDetails?comp_code=1").then(function (res) {
                    //$scope.emp_lwpdetails = res.data;
                    $scope.emp_lwpdetails_lwphst = res.data;
                    $scope.totalItems_lwphst = $scope.emp_lwpdetails_lwphst.length;
                    $scope.todos_lwphst = $scope.emp_lwpdetails_lwphst;
                    $scope.makeTodos_lwphst();
                    $scope.div_lwphst = true;
                });
            }
            else
            {
                $scope.div_lwphst = false;
            }


            $scope.UserSearch_lwp = function (str) {

                if ($scope.edt.lstart_date == undefined || $scope.edt.lstart_date == '')
                {
                    swal({ title: "Alert", text: "Please Select Leave Start Date.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }
                else
                {
                    $scope.global_Search_click();
                    $('#Global_Search_Modal').modal({ backdrop: "static" });
                }
            }

            $scope.$on('global_cancel', function (str)
            {
                console.log($scope.SelectedUserLst);

                if ($scope.currentab == 'lwp')
                {
                    if ($scope.SelectedUserLst.length > 0)
                    {
                        $scope.cdetails_data_lwp = $scope.SelectedUserLst;
                        $scope.totalItems_lwp = $scope.cdetails_data_lwp.length;
                        $scope.todos_lwp = $scope.cdetails_data_lwp;
                        $scope.makeTodos_lwp();


                      
                    }
                }
                else if ($scope.currentab == 'holiday')
                {
                    if ($scope.SelectedUserLst.length > 0)
                    {
                        $scope.filteredTodos_holiday = $scope.SelectedUserLst;

                        if ($scope.filteredTodos_holiday.length > 0)
                        {
                            $scope.users = true
                        }
                    }
                }
            });

            $scope.makeTodos_lwp = function () {
                var rem_lwp = parseInt($scope.totalItems_lwp % $scope.numPerPage_lwp);
                if (rem_lwp == '0') {
                    $scope.pagersize_lwp = parseInt($scope.totalItems_lwp / $scope.numPerPage_lwp);
                }
                else {
                    $scope.pagersize_lwp = parseInt($scope.totalItems_lwp / $scope.numPerPage_lwp) + 1;
                }
                var begin_lwp = (($scope.currentPage_lwp - 1) * $scope.numPerPage_lwp);
                var end_lwp = parseInt(begin_lwp) + parseInt($scope.numPerPage_lwp);

                $scope.filteredTodos_lwp = $scope.todos_lwp.slice(begin_lwp, end_lwp);
            };

            $scope.size_lwp = function (str) {
                //console.log(str);
                //$scope.pagesize_lwp = str;
                //$scope.currentPage_lwp = 1;
                //$scope.numPerPage_lwp = str; console.log("numPerPage=" + $scope.numPerPage_lwp); $scope.makeTodos_lwp();
               
                if (str == "All") {
                    $scope.currentPage_lwp = '1';
                    $scope.filteredTodos = $scope.Todos_lwp;
                    $scope.pager4 = false;
                }
                else {
                    $scope.pager4 = true;
                    $scope.pagesize_lwp = str;
                    $scope.currentPage_lwp = 1;
                    $scope.numPerPage_lwp = str;
                    $scope.makeTodos_lwp();
                }
            }

            $scope.index_lwp = function (str) {
                $scope.pageindex_lwp = str;
                $scope.currentPage_lwp = str; console.log("currentPage=" + $scope.currentPage_lwp); $scope.makeTodos_lwp();
                main_lwp.checked = false;
                $scope.row1 = '';
            }

            $scope.search_lwp = function () {
                $scope.todos_lwp = $scope.searched_lwp($scope.cdetails_data_lwp, $scope.searchText_lwp);
                $scope.totalItems_lwp = $scope.todos_lwp.length;
                $scope.currentPage_lwp = '1';
                if ($scope.searchText_lwp == '') {
                    $scope.todos_lwp = $scope.cdetails_data_lwp;
                }
                $scope.makeTodos_lwp();
            }

            $scope.searched_lwp = function (valLists_lwp, toSearch_lwp) {

                return _.filter(valLists_lwp,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil_lwp(i, toSearch_lwp);
                });
            };

            function searchUtil_lwp(item, toSearch) {
                /* Search Text in all 2 fields */
                return (item.empName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.em_login_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.makeTodos_lwphst = function () {
                var rem_lwphst = parseInt($scope.totalItems_lwphst % $scope.numPerPage_lwphst);
                if (rem_lwphst == '0') {
                    $scope.pagersize_lwphst = parseInt($scope.totalItems_lwphst / $scope.numPerPage_lwphst);
                }
                else {
                    $scope.pagersize_lwphst = parseInt($scope.totalItems_lwphst / $scope.numPerPage_lwphst) + 1;
                }
                var begin_lwphst = (($scope.currentPage_lwphst - 1) * $scope.numPerPage_lwphst);
                var end_lwphst = parseInt(begin_lwphst) + parseInt($scope.numPerPage_lwphst);

                $scope.filteredTodos_lwphst = $scope.todos_lwphst.slice(begin_lwphst, end_lwphst);
            };

            $scope.size_lwphst = function (str)
            {
                if (str == "All")
                {
                    $scope.currentPage_lwphst = '1';
                    $scope.filteredTodos_lwphst = $scope.todos_lwphst;
                    $scope.pager4 = false;
                }
                else
                {
                    $scope.pager4 = true;
                    $scope.pagesize_lwphst = str;
                    $scope.currentPage_lwphst = 1;
                    $scope.numPerPage_lwphst = str;
                    $scope.makeTodos_lwphst();
                }
            }

            $scope.index_lwphst = function (str) {
                $scope.pageindex_lwphst = str;
                $scope.currentPage_lwphst = str;
                console.log("currentPage=" + $scope.currentPage_lwphst);
                $scope.makeTodos_lwphst();
                main_lwphst.checked = false;
                $scope.row1 = '';
            }

            $scope.search_lwphst = function () {
                $scope.todos_lwphst = $scope.searched_lwphst($scope.emp_lwpdetails_lwphst, $scope.searchText_lwphst);
                $scope.totalItems_lwphst = $scope.todos_lwphst.length;
                $scope.currentPage_lwphst = '1';
                if ($scope.searchText_lwphst == '')
                {
                    $scope.todos_lwphst = $scope.emp_lwpdetails_lwphst;
                }
                $scope.makeTodos_lwphst();
            }

            $scope.searched_lwphst = function (valLists_lwphst, toSearch_lwphst) {

                return _.filter(valLists_lwphst,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil_lwphst(i, toSearch_lwphst);
                });
            };

            function searchUtil_lwphst(item, toSearch) {
                /* Search Text in all 2 fields */
                return (item.empName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.em_login_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.checkPayrollFinalizedForLWP = function (info)
            {
                console.log("lstart_date", $scope.edt.lstart_date);
               
                var day =   $scope.edt.lstart_date.split("-")[0];
                var month = $scope.edt.lstart_date.split("-")[1];
                var year =  $scope.edt.lstart_date.split("-")[2];

                var lwpDay = parseInt(day) + parseInt(info.ndays);
                if (lwpDay > 31 && month == '12')
                {
                    month = '01';
                    lwpDay = 31 - info.ndays;
                    year= parseInt(year)+1
                }
                else if (lwpDay > 31)
                {
                    month = parseInt(month) + 1;
                }                
                $scope.lwpEndDate = lwpDay + "-" + month + "-" + year;
                
                console.log("lstart_date", $scope.edt.lstart_date);
                console.log("lwpEndDate",$scope.lwpEndDate);
                $scope.UpdateLWPBtn = false;
                $http.get(ENV.apiUrl + "api/leavedetails/GetFinalizeSatus?emp_id=" + info.em_number + "&leave_start_date=" + $scope.edt.lstart_date + "&leave_end_date=" + $scope.lwpEndDate).then(function (res) {
                    $scope.getFinaliizedStaus = res.data;
                    if ($scope.getFinaliizedStaus.length > 0)
                    {
                        if ($scope.getFinaliizedStaus[0].pe_status == 'P' || $scope.getFinaliizedStaus[0].pe_status == 'F')
                        {
                            swal({ title: "Alert", text: "You can not mark LWP.Payroll is finalized for " + info.em_number, showCloseButton: true, width: 380, })
                            $scope.UpdateLWPBtn = true;
                        }
                    }
                });
            }

            $scope.AssignLWPforEmployees = function () 
            {
                for (var i = 0; i < $scope.filteredTodos_lwp.length; i++)
                {
                    if ($scope.filteredTodos_lwp[i].ndays != 'undefined')
                    {
                        $scope.filteredTodos_lwp[i].lstart_date = $scope.edt.lstart_date;
                        lwpsenddata.push($scope.filteredTodos_lwp[i])
                    }
                }

                if ($http.defaults.headers.common['schoolId'] == 'siserp')
                {
                    $scope.loaderShow = true;

                    $http.post(ENV.apiUrl + "api/leavedetails/CUDInsertEMPLeaveDetails", lwpsenddata).then(function (res) {
                        $scope.msg = res.data;
                    });

                    $http.post(ENV.apiUrl + "api/leavedetails/Assign_LWPforEmployees_siserp", lwpsenddata).then(function (Assign_LWPforEmployees) {
                        $scope.AssignLWPforEmp = Assign_LWPforEmployees.data;
                        console.log($scope.AssignLWPforEmp);

                        if ($scope.AssignLWPforEmp == true)
                        {
                            swal({text: 'Assigned LWP Successfully',imageUrl: "assets/img/check.png",width: 300,height: 300});
                            $scope.loaderShow = false;
                            $scope.GET_LWPforEmployees = [];
                            $scope.filteredTodos_lwp = null;
                            $scope.edt.lstart_date = null;
                            $scope.searchText_lwp = null;
                            //$scope.SelectedUserLst = null;
                        }
                        else if ($scope.AssignLWPforEmp == false)
                        {
                            swal({text: 'Assigned LWP Successfully.',imageUrl: "assets/img/check.png", width: 300,height: 300});
                            $scope.loaderShow = false;
                            $scope.GET_LWPforEmployees = [];
                            $scope.filteredTodos_lwp = null;
                            $scope.edt.lstart_date = null;
                            $scope.searchText_lwp = null;
                            //$scope.SelectedUserLst = null;
                        }


                    })

                    $http.get(ENV.apiUrl + "api/leavedetails/GetAllEMPLeaveLWPDetails?comp_code=1").then(function (res) {
                        //$scope.emp_lwpdetails = res.data;
                        $scope.emp_lwpdetails_lwphst = res.data;;
                        $scope.totalItems_lwphst = $scope.emp_lwpdetails_lwphst.length;
                        $scope.todos_lwphst = $scope.emp_lwpdetails_lwphst;
                        $scope.makeTodos_lwphst();
                    });
                }
                else
                {
                    $scope.loaderShow = true;
                    $http.post(ENV.apiUrl + "api/leavedetails/Assign_LWPforEmployees", lwpsenddata).then(function (Assign_LWPforEmployees) {
                        $scope.AssignLWPforEmp = Assign_LWPforEmployees.data;

                        console.log($scope.AssignLWPforEmp);
                        if ($scope.AssignLWPforEmp == true) {
                            swal({text: 'Assigned LWP Successfully',imageUrl: "assets/img/check.png", width: 300, height: 300});
                            $scope.loaderShow = false;
                            $scope.GET_LWPforEmployees = [];
                            $scope.filteredTodos_lwp = null;
                            $scope.edt.lstart_date = null;
                            $scope.searchText_lwp = null;
                            //$scope.SelectedUserLst = null;
                        }
                        else if ($scope.AssignLWPforEmp == false) {
                            swal({text: 'Assigned LWP Successfully.',imageUrl: "assets/img/check.png", width: 300,height: 300});
                            $scope.loaderShow = false;
                            $scope.GET_LWPforEmployees = [];
                            $scope.filteredTodos_lwp = null;
                            $scope.edt.lstart_date = null;
                            $scope.searchText_lwp = null;
                            //$scope.SelectedUserLst = null;
                        }
                    })
                }
            }

            //////////////////////////////////////////////////////////////
            //Staff Attendance for Holiday

            $scope.GetCurrentTab = function (str)
            {
                if (str == 'lwp') { $scope.currentab = 'lwp' }
                if (str == 'holiday') { $scope.currentab = 'holiday' }
            }

            $scope.getmonth = function (month)
            {
                $scope.edt['mnth'] = month;
                $scope.GetUsers($scope.emp_number, '');
            }

            $scope.UserSearch_holiday = function (str) {
                if ($scope.edt.mnth != undefined)
                {
                    $scope.global_Search_click();
                    $('#Global_Search_Modal').modal({ backdrop: "static" });
                }
                else
                {
                    swal({ title: "Alert", text: "Please Select Month.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }
            }

            $scope.data = [];
            var data1 = {};

            $scope.GetUsers = function (empnumber, empname) {
                debugger
                console.log(empname);
                $scope.data = [];
                $scope.emp_number=empnumber;
               
                if (document.getElementById(empnumber).checked == true) {
                    data1 = { str: empnumber, month: $scope.edt.mnth };

                    $scope.data.push(data1);
                    data1 = {};
                }
                else {
                    for (var i = $scope.data.length; i--;) {
                        if ($scope.data[i].str == empnumber) {
                            $scope.data.splice(i, 1);
                        }
                    }
                }               
            }

            $scope.ViewHolidays = function () {
                debugger                
                $http.post(ENV.apiUrl + "api/leavedetails/EMPAllHolidays", $scope.data).then(function (Getholiday) {
                   
                    $scope.Getholidays = [];
                    //for (var i = 0; i < Getholiday.data.length; i++) {
                    //    // var date = 
                    //    Getholiday.data[i].holiday = moment(Getholiday.data[i].holiday).format('YYYY-MM-DD');
                    //        //moment(Getholiday.data[i].holiday).format('YYYY-MM-DD')
                    //        //moment(Getholiday.data[i].holiday, "DD-MM-YYYY").format('YYYY-MM-DD');
                    //}

                    $scope.Getholidays = Getholiday.data;
                    console.log("Getholiday.data", Getholiday.data);
                    //debugger
                    //for (var i = 0; i <= Getholiday.data.length; i++) {
                    //    for (var j = 0; j <= $scope.Getholidays.length; j++) {
                    //        debugger
                    //        if (Getholiday.data[i].holiday == $scope.Getholidays[j].holiday) {
                    //            $scope.Getholidays.push(Getholiday.data[i]);
                    //        }
                    //        else {
                    //            $scope.Getholidays.push(Getholiday.data[i]);
                    //        }
                    //    }                        
                    //}

                    console.log("Getholidays", $scope.Getholidays);

                    if ($scope.Getholidays.length > 0) 
                    {

                    }
                    else
                    {
                        swal({
                            text: 'Holidays not defined for selected month.',
                            width: 300,
                            height: 300
                        });
                    }
                });
            }

         

            var holidaysenddata = [];
            var holidaysenddata_temp = [];

            $scope.AssignHolidaysforEmployees = function () {
               

                console.log($scope.filteredTodos_holiday);
                console.log($scope.Getholidays);
                var j = 0;

                //for (var j = 0; j < $scope.filteredTodos_holiday.length; j++)
                //{
                //     if ($scope.filteredTodos_holiday[j].flag == true)
                //     {
                //         for (var i = 0; i < $scope.Getholidays.length; i++)
                //         {
                //             if ($scope.Getholidays[i].holiday_flag == true)
                //             {
                //                 console.log($scope.filteredTodos_holiday[j].em_number);
                //                console.log($scope.Getholidays[i].holiday);

                //                $scope.Getholidays[i].em_login_code = $scope.filteredTodos_holiday[j].em_number;

                //                //holidaysenddata.push($scope.Getholidays[i])
                //                 //console.log(holidaysenddata);

                //                holidaysenddata.push($scope.Getholidays[i]);
                //                holidaysenddata_temp = holidaysenddata;
                //            }                            
                //        }                        
                //    }
                // }
                $scope.data = [];
                for (var j = 0; j < $scope.filteredTodos_holiday.length; j++) {
                    if ($scope.filteredTodos_holiday[j].flag == true) {
                        for (var i = 0; i < $scope.Getholidays.length; i++) {
                            if ($scope.Getholidays[i].holiday_flag == true) {

                                console.log($scope.filteredTodos_holiday[j].em_login_code);
                                console.log($scope.Getholidays[i].holiday);

                                $scope.Getholidays[i].em_login_code = $scope.filteredTodos_holiday[j].em_login_code;

                                var data1 = { em_login_code: $scope.Getholidays[i].em_login_code, holiday: $scope.Getholidays[i].holiday };

                                $scope.data.push(data1);
                            }
                        }
                    }
                }

                console.log($scope.data);
                $http.post(ENV.apiUrl + "api/leavedetails/AssignHolidaysforEmployees_1", $scope.data).then(function (Assign_HolidayforEmployees) {
                    $scope.AssignHolidayforEmp = Assign_HolidayforEmployees.data;
                    if ($scope.AssignHolidayforEmp == true) {
                        swal({
                            text: 'Attendance Marked Successfully.',
                            imageUrl: "assets/img/check.png",
                            width: 300,
                            height: 300
                        });
                        holidaysenddata = [];
                        $scope.Getholidays = [];
                        $scope.filteredTodos_holiday = [];
                        $scope.data = [];
                    } else {
                        swal({
                            text: 'Attendance Not Marked.',
                            imageUrl: "assets/img/close.png",
                            width: 300,
                            height: 300
                        });
                        holidaysenddata = [];
                        $scope.Getholidays = [];
                        $scope.data = [];
                        $scope.filteredTodos_holiday = [];
                    }
                });

               

                //$http.post(ENV.apiUrl + "api/leavedetails/AssignHolidaysforEmployees", holidaysenddata).then(function (Assign_HolidayforEmployees) {
                //    $scope.AssignHolidayforEmp = Assign_HolidayforEmployees.data;

                //if ($scope.AssignHolidayforEmp == true) {
                //    swal({
                //        text: 'Assigned Holidays Successfully',
                //        width: 300,
                //        height: 300
                //    });
                //    // $scope.GET_LWPforEmployees = [];
                //}
                //else {
                //    swal({
                //        text: 'Not Assigned Holidays',
                //        width: 300,
                //        height: 300
                //    });
                //}
                //})
            }

            $scope.checkdate = function (start_date, end_date, str)
            {
                var d1 = moment(end_date, "DD-MM-YYYY").format('YYYY-MM-DD');
                var d2 = moment(start_date, "DD-MM-YYYY").format('YYYY-MM-DD');

                if (d1 < d2) {

                    swal({ title: "Alert", text: "Start Date should be less than the End date.", showCloseButton: true, width: 380, });
                    str.lt_end_date_new = "";
                }
            }

            $scope.checkScheduleStartDate = function (start_date, end_date, str) {
                var d1 = moment(start_date, "DD-MM-YYYY").format('YYYY-MM-DD');
                var d2 = moment(end_date, "DD-MM-YYYY").format('YYYY-MM-DD');
                var curDate=moment().format('YYYY-MM-DD');

                if (d2 < d1) {
                    swal({ title: "Alert", text: "Start Date should be less than the End date.", showCloseButton: true, width: 380, });
                    str.lt_end_date_new = "";
                }
                if (d1 < curDate) {
                    swal({ title: "Alert", text: "Start Date should not be less than current date.", showCloseButton: true, width: 380, });
                    str.lt_start_date_new = "";
                }
            }
            //employee leave history details

            $scope.CheckEmployeeLeaveHistory = function (em_number)
            {
                $('#div_leaveHistory').modal({ backdrop: "static" });


                $http.get(ENV.apiUrl + "api/leavedetails/GetAllEMPLeaveHistoryDetails?comp_code=1&emp_id=" + em_number).then(function (res) {
                    $scope.empleave_history = res.data;
                });
            }

            $scope.SubmitResheduleLeave= function () {
                $scope.insert = false;                
                var sendata_reschedule = [];
                for (var i = 0; i < $scope.filteredTodos_reschedule.length; i++) {

                    var start_date = moment($scope.filteredTodos_reschedule[i].lt_start_date_new, "DD-MM-YYYY").format('YYYY-MM-DD');
                    var end_date = moment($scope.filteredTodos_reschedule[i].lt_end_date_new, "DD-MM-YYYY").format('YYYY-MM-DD');

                    date2 = new Date(end_date);
                    date1 = new Date(start_date);
                    timeDiff = Math.abs(date2.getTime() - date1.getTime());
                    diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

                    if ($scope.filteredTodos_reschedule[i].lt_reschedule == true) {
                        if($scope.filteredTodos_reschedule[i].lt_start_date_new == '' || $scope.filteredTodos_reschedule[i].lt_start_date_new == 'undefined') {
                            swal('', 'Please enter new updated end date for selected employee.');
                            // break;
                            return;
                        }

                        if ($scope.filteredTodos_reschedule[i].lt_end_date_new == '' || $scope.filteredTodos_reschedule[i].lt_end_date_new == 'undefined') {
                            swal('', 'Please enter new updated end date for selected employee.');
                            // break;
                            return;
                        }
                        else {
                            $scope.filteredTodos_reschedule[i].lt_days = diffDays + 1;
                            console.log(diffDays);
                            $scope.insert = true;
                            sendata_reschedule.push($scope.filteredTodos_reschedule[i]);
                        }
                    }
                }
                console.log(sendata_reschedule);

                $scope.GetRescheduleData = function () {
                    debugger
                    $scope.getRescheduleleaves();
                }

                if ($scope.insert) {
                    $scope.disableRescheduleLeave = true;
                    if (sendata_reschedule.length > 0) {
                        $http.post(ENV.apiUrl + "api/leavedetails/RescheduleLeaves", sendata_reschedule).then(function (LeaveDetails_update) {
                            $scope.LeaveDetails_update = LeaveDetails_update.data;
                            if ($scope.LeaveDetails_update == true) {
                                swal('', 'Leave reschedule successfully.');
                                $scope.EMPLeave_Details = [];
                                $scope.filteredTodos_update = [];
                                $scope.edt.leave_type2 = null;
                                $scope.searchText_update = null;
                                //$scope.SelectedUserLst = null;                                
                            }
                            else if ($scope.LeaveDetails_update == false) {
                                swal('', 'Leave not updated.');
                            }
                            else {
                                swal("Error-" + $scope.LeaveDetails_update)
                            }

                            $scope.getRescheduleleaves();

                            $http.get(ENV.apiUrl + "api/leavedetails/GetAllEMPLeaveDetails?comp_code=1").then(function (res) {
                                $scope.cdetails_data = res.data;
                                $scope.totalItems = $scope.cdetails_data.length;
                                $scope.todos = $scope.cdetails_data;
                                $scope.makeTodos();
                            });

                        });
                        sendata_reschedule = [];
                    }
                    else {
                        swal('', 'Please select employee to reschedule leave.');
                    }
                    $scope.disableRescheduleLeave = false;
                }
                else {
                    swal('', 'Please select atleast one record.');
                }
            };

        }])
})();