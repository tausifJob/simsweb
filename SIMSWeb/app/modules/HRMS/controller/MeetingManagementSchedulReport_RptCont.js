﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('MeetingManagementSchedulReport_RptCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            // Start
            $scope.temp = {};
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            $(function () {
                $('#schedule_box').multipleSelect({ width: '100%' });
                $('#status_box').multipleSelect({ width: '100%' });

            });


            $http.get(ENV.apiUrl + "api/HRMS/getmeetingcode").then(function (res1) {
                $scope.mom_type = res1.data;
                console.log($scope.mom_type)
                setTimeout(function () {
                    $('#schedule_box').change(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                        //$("#show_box").multipleSelect("checkAll");
                }, 1000);
            });



            $http.get(ENV.apiUrl + "api/HRMS/getstatus").then(function (res1) {
                //debugger;
                $scope.statuss = res1.data;

                console.log($scope.statuss)
                setTimeout(function () {
                    $('#status_box').change(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                    //$("#show_box").multipleSelect("checkAll");
                }, 1000);
            });


            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
                }
                var mm = today.getMonth() + 1; //January is 0!
                if (mm < 10) {
                    mm = '0' + mm;
                }
                var yyyy = today.getFullYear();          
                $scope.sdate = dd + '-' + mm + '-' + yyyy;
                $scope.edate = dd + '-' + mm + '-' + yyyy;
                $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
                $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;

                $(function () {
                    $("#chkmarks").click(function () {
                        if ($(this).is(":checked")) {
                            $("#fdate").attr("disabled", "disabled");
                            $scope.mom_start_date = '';

                        } else {

                            $("#fdate").removeAttr("disabled");
                            $("#fdate").focus();
                                $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
                        }
                    });
                });


                $(function () {
                    $("#chkmarks1").click(function () {
                        if ($(this).is(":checked")) {
                            $("#fdate").attr("disabled", "disabled");
                            $scope.mom_end_date = '';

                        } else {

                            $("#fdate").removeAttr("disabled");
                            $("#fdate").focus();
                            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
                        }
                    });
                });


                $scope.getMeetingManagementSchedulReport_Rpt = function (sims_mom_type_code, mom_start_date, mom_end_date, status) {
                    //debugger
                    $scope.colsvis = false;
                    $http.get(ENV.apiUrl + "api/HRMS/getMeetingManagementSchedulReport_Rpt?mom_type=" + sims_mom_type_code + "&mom_start_date=" + mom_start_date + "&mom_end_date=" + mom_end_date + "&status=" + status).then(function (res1) {
                    $scope.report_data_new = res1.data;                               
                    //console.log($scope.repreport_data_new);

                });
                }
            

              
               


            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }
            
            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.colsvis = false;


            $scope.exportData = function () {
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('rpt_data').innerHTML], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                        });
                        saveAs(blob, "MeetingManagementSchedule.xls");
                        $scope.colsvis = false;

                    }

                });
            };


            $scope.Report_call=function(str)
            {
                //debugger;
                var data = {
                    location: 'Sims.MeetingReciept',
                    parameter: {
                        refno: str,
                    },
                    state: 'main.MMSR20',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')
            }



        }])

})();

