﻿(function () {
    'use strict';
    var main = '';
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('UpdatePaysheetCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            
            var d = new Date();
            var n = d.getMonth() + 1;
            $scope.deletepaysheettable = false;
            $scope.deletepaysheet = false;
            $scope.updatepaysheet = false;
            $scope.update_payroll_show = true;
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            var user = $rootScope.globals.currentUser.username;

            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.postSalaryDetail = [];
            $scope.temp = {};
            $scope.edt = {};
            $scope.edt1 = {};

            $scope.showForDPSMIS = false;

            var schoolArray = ['dpsmis'];

            if (schoolArray.includes($http.defaults.headers.common['schoolId'])) {
                $scope.showForDPSMIS = true;
            }

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $http.get(ENV.apiUrl + "api/PaysGradeChange/Get_gc_company_Code").then(function (compcode) {
                $scope.comp_code = compcode.data;
                $scope.temp['gc_company_code'] = $scope.comp_code[0].gc_company_code;
                $scope.getdept($scope.temp['gc_company_code']);
                $scope.getdesg($scope.temp['gc_company_code']);
                $scope.getGrade($scope.temp['gc_company_code']);
            });

            $scope.getdept = function (dept) {

                $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/Get_dept_Code?company_code=" + dept).then(function (Department) {
                    $scope.dept = Department.data;
                });
            }

            $scope.getdesg = function (design) {
                $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/Get_designation_Code?company_code=" + design).then(function (Designation) {
                    $scope.designation = Designation.data;
                });
            }

            $scope.getGrade = function (company_code) {
                $http.get(ENV.apiUrl + "api/generatepayroll/GetAllPaysGradeName?company_code=" + company_code).then(function (GetAllPaysGradeName) {
                    $scope.GetAllPaysGradeName = GetAllPaysGradeName.data;                    
                });
            }

            $http.get(ENV.apiUrl + "api/updatepaysheet/GetCurrentFinYearFromFinsParameter").then(function (GetCurrentFinYearFromFinsParameter) {
                $scope.GetCurrentFinYearFromFinsParameter = GetCurrentFinYearFromFinsParameter.data;
            });

            $http.get(ENV.apiUrl + "api/updatepaysheet/GetAllPayCodes").then(function (GetAllPayCodes) {
                $scope.GetAllPayCodes = GetAllPayCodes.data;
            });

            $http.get(ENV.apiUrl + "api/generatepayroll/GetCurrentFinYearFromFinsParameter").then(function (GetCurrentFinYearFromFinsParameter) {

                $scope.GetCurrentFinYearFromFinsParameter1 = GetCurrentFinYearFromFinsParameter.data;
                
                $scope.fin_year = $scope.GetCurrentFinYearFromFinsParameter1[0].fin_year;
                $scope.fin_year_desc = $scope.GetCurrentFinYearFromFinsParameter1[0].fin_year_desc;

            });

            //$http.get(ENV.apiUrl + "api/updatepaysheet/GetMonth").then(function (GetMonth) {
            //    $scope.GetMonth = GetMonth.data;
            //    console.log("month",$scope.GetMonth);
            //    //$scope.edt = { sd_year_month_code: n.toString() };
            //    var n = new Date().getMonth() + 1;
            //    console.log(n);
            //    $scope.edt.year_month = n;
            //});

            $http.post(ENV.apiUrl + "api/OvertimeHour/Year_month").then(function (Year_month) {
                $scope.Year_month = Year_month.data;
                
                var d = new Date().getFullYear();
                var n = new Date().getMonth() + 1;
                var ym = d + '' + n;

                //$scope.edt = { year_month: ym };
               // $scope.edt.yearCode = ym;
                //$scope.edt1.yearCode = ym;
                $scope.setMonth();
                $scope.setMonthDeletePaysheet();
            });

            $scope.setMonth = function () {
                $scope.edt.year_month = $scope.edt.yearCode.substr(4);                
                console.log("year_month",$scope.edt.year_month);
            }
            $scope.setMonthDeletePaysheet = function () {
                $scope.edt1.year_month = $scope.edt1.yearCode.substr(4);
                console.log("year_month", $scope.edt1.year_month);
            }

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2015&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;                
            });
            $scope.depts = [];
            $scope.getDept = function () {
                $scope.depts = [];
                
                for (var i = 0; i < $scope.ComboBoxValues.length; i++) {
                    if ($scope.edt.gd_division_code == "*") {
                        if ( $scope.ComboBoxValues[i].codp_dept_name.toString().trim().length >0)
                            $scope.depts.push($scope.ComboBoxValues[i]);
                    }
                    else {
                        if ($scope.ComboBoxValues[i].codp_division == $scope.edt.gd_division_code) {
                            $scope.depts.push($scope.ComboBoxValues[i]);
                        }
                    }
                }
            }
            var year_month = '';
            var newvalue = false;
            $scope.deletbtn = true;

            $scope.ShowUpdatepaysheet = function () {
                $scope.updatepaysheet = true;
                $scope.deletepaysheet = false;
                $scope.update_payroll_show = true;
                $scope.delete_payroll_show = false;
                $scope.deletepaysheettable = false;
                $scope.disableUpdatePaysheet = false;
                $scope.disableDeletePaysheet = true;
                
                var d = new Date().getFullYear();
                var n = new Date().getMonth() + 1;
                var ym = d + '' + n;

                //$scope.edt = { year_month: ym };
                $('#deletePaysheetBtn').css('background-color','darkgray');
            }

            $scope.ShowDeletepaysheet = function () {
                $scope.btn_ResetEmployee_Click();
                $scope.updatepaysheet = false;
                $scope.deletepaysheet = true;
                $scope.update_payroll_show = false;
                $scope.delete_payroll_show = true;
                $scope.disableDeletePaysheet = false;
                $scope.disableUpdatePaysheet = true;
                
                var d = new Date().getFullYear();
                var n = new Date().getMonth() + 1;
                var ym = d + '' + n;

                //$scope.edt1 = { year_month: ym };
                //$scope.edt1 = { yearCode: ym };                
                $('#updatePaysheetBtn').css('background-color', 'darkgray');
            }

            /********************************************************Delete Paysheet Code***************************************************/
            $scope.GetEmployeeDatafordelete = function () {
                debugger;
                if ($scope.edt1 == '' || $scope.edt1 == undefined) {
                    swal('', 'Please select month.');
                }
                else {
                    //var curyear_month = $scope.edt1.year_month;
                    //var cur_month = $scope.edt1.year_month.substr(4);
                    //$scope.edt1.year_month = curyear_month;//$scope.fin_year + cur_month;

                    $scope.yearMonth1 = $scope.fin_year + $scope.edt1.year_month;

                    $http.post(ENV.apiUrl + "api/updatepayrollstatus/CheckPayrollStatus?year_month=" + $scope.yearMonth1).then(function (CheckPayrollStatus) {
                        $scope.CheckPayrollStatus = CheckPayrollStatus.data;
                        
                        if ($scope.CheckPayrollStatus != '') {
                            swal({ text: $scope.CheckPayrollStatus, showCloseButton: true, width: 380 });

                        }
                        else {
                            year_month = $scope.yearMonth1;
                            $scope.deletepaysheettable = true;
                            $http.get(ENV.apiUrl + "api/updatepaysheet/GetEmployeesFromSalaryDetails?year_month=" + year_month + "&company_code=" + $scope.temp.gc_company_code + "&dept_code=" + $scope.edt.dept_code
                                + "&desg_code=" + $scope.edt.dg_code + "&grade_code=" + $scope.edt.gr_code+"&login_code="+$scope.edt.em_login_code).then(function (GetEmployeesFromSalaryDetails) {
                                $scope.GetEmployeesFromSalaryDetails = GetEmployeesFromSalaryDetails.data;
                                if ($scope.GetEmployeesFromSalaryDetails.length > 0)
                                    $scope.deletbtn = false;
                            });
                        }
                    });

                }
            }

            $scope.Delete = function () {
                debugger;
                //var curyear_month = $scope.edt1.year_month;
               // var cur_month = $scope.edt1.year_month.substr(4);
                // $scope.edt1.year_month = curyear_month;//$scope.fin_year + cur_month;
                $scope.yearMonth1 = $scope.fin_year + $scope.edt1.year_month;

                $scope.flag = false;
                var del = [];
                for (var i = 0; i < $scope.GetEmployeesFromSalaryDetails.length; i++) {
                    
                    var t = $scope.GetEmployeesFromSalaryDetails[i].code;
                    var v = document.getElementById(t);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var c = {
                            sd_year_month: $scope.yearMonth1,
                            sd_number: $scope.GetEmployeesFromSalaryDetails[i].code
                        }
                        del.push(c);
                    }

                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/updatepaysheet/Payroll_Delete", del).then(function (msg1) {
                                $scope.msg = msg1.data;
                                $scope.deletbtn = true;
                                main.checked = false;
                                swal({ title: "", text: $scope.msg, showCloseButton: true, width: 380 }).then(function (isConfirm) {
                                    year_month = $scope.yearMonth1;
                                    $scope.deletepaysheettable = true;                                    
                                    $http.get(ENV.apiUrl + "api/updatepaysheet/GetEmployeesFromSalaryDetails?year_month=" + year_month + "&company_code=" + $scope.temp.gc_company_code + "&dept_code=" + $scope.edt.dept_code
                                        + "&desg_code=" + $scope.edt.dg_code + "&grade_code=" + $scope.edt.gr_code + "&login_code=" + $scope.edt.em_login_code).then(function (GetEmployeesFromSalaryDetails) {
                                            $scope.GetEmployeesFromSalaryDetails = GetEmployeesFromSalaryDetails.data;
                                            if ($scope.GetEmployeesFromSalaryDetails.length > 0)
                                                $scope.deletbtn = false;
                                        });
                                    $scope.Updateemployee = false;
                                    main = document.getElementById('mainchk');
                                    main.checked = false;
                                });
                            });
                        }
                        else {
                            
                            main = document.getElementById('mainchk');
                            for (var i = 0; i < $scope.GetEmployeesFromSalaryDetails.length; i++) {
                                var t = $scope.GetEmployeesFromSalaryDetails[i].code;
                                var v = document.getElementById(t);
                                v.checked = false;
                                $scope.row1 = "";
                                main.checked = false;
                                year_month = $scope.yearMonth1;  //$scope.edt1.year_month;
                                $scope.deletepaysheettable = true;
                                $http.get(ENV.apiUrl + "api/updatepaysheet/GetEmployeesFromSalaryDetails?year_month=" + year_month + "&company_code=" + $scope.temp.gc_company_code + "&dept_code=" + $scope.edt.dept_code
                                + "&desg_code=" + $scope.edt.dg_code + "&grade_code=" + $scope.edt.gr_code + "&login_code=" + $scope.edt.em_login_code).then(function (GetEmployeesFromSalaryDetails) {
                                    $scope.GetEmployeesFromSalaryDetails = GetEmployeesFromSalaryDetails.data;
                                    if ($scope.GetEmployeesFromSalaryDetails.length > 0)
                                        $scope.deletbtn = false;
                                });
                                $scope.Updateemployee = false;
                            }
                        }
                        //$http.post(ENV.apiUrl + "api/updatepayrollstatus/CheckPayrollStatus?year_month=" + $scope.yearMonth1).then(function (CheckPayrollStatus) {
                        //    $scope.CheckPayrollStatus = CheckPayrollStatus.data;
                            
                        //    if ($scope.CheckPayrollStatus != '') {
                        //        swal({ text: $scope.CheckPayrollStatus, showCloseButton: true, width: 380 });
                        //    }
                        //    else {
                                year_month = $scope.yearMonth1 //$scope.edt1.year_month;
                                $scope.deletepaysheettable = true;

                                $http.get(ENV.apiUrl + "api/updatepaysheet/GetEmployeesFromSalaryDetails?year_month=" + year_month + "&company_code=" + $scope.temp.gc_company_code + "&dept_code=" + $scope.edt.dept_code
                                + "&desg_code=" + $scope.edt.dg_code + "&grade_code=" + $scope.edt.gr_code + "&login_code=" + $scope.edt.em_login_code).then(function (GetEmployeesFromSalaryDetails) {
                                    $scope.GetEmployeesFromSalaryDetails = GetEmployeesFromSalaryDetails.data;
                                    if ($scope.GetEmployeesFromSalaryDetails.length > 0)
                                        $scope.deletbtn = false;
                                });
                            //}
                        //});

                    });
                }
                else {
                    swal({ title: "", text: "Please select at least one record", showCloseButton: true, width: 380, });
                }
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.edt = "";
            }

            $scope.btn_SearchEmployee_Click = function () {
                
                //var curyear_month = $scope.edt.year_month;
                //var cur_month = $scope.edt.year_month.substr(4);
               // $scope.edt.year_month = curyear_month;//$scope.fin_year + cur_month;
                $scope.yearMonth = $scope.fin_year + $scope.edt.year_month;

                console.log("$scope.fin_year", $scope.fin_year);
                console.log("$scope.edt.year_month ", $scope.edt.year_month);
                console.log("curyear_month", $scope.yearMonth);

                $http.post(ENV.apiUrl + "api/updatepayrollstatus/CheckPayrollStatus?year_month=" + $scope.yearMonth).then(function (CheckPayrollStatus) {
                    $scope.CheckPayrollStatus = CheckPayrollStatus.data;

                    if ($scope.CheckPayrollStatus != '') {
                        swal({ text: $scope.CheckPayrollStatus, showCloseButton: true, width: 380 });
                        $scope.filteredTodos = [];
                        $scope.Updateemployee = false;
                    }
                    else {
                        $scope.To_Updateemployee = false;

                        $scope.edt.sd_year_month = $scope.yearMonth //$scope.edt.year_month;
                        $scope.edt.Company_code = $scope.temp.gc_company_code;
                        
                        $http.post(ENV.apiUrl + "api/updatepaysheet/Employee_Genrated_salary_details", $scope.edt).then(function (Get_Genrated_salary_details) {
                            $scope.Get_Genrated_salary_details = Get_Genrated_salary_details.data;
                            console.log("Get_Genrated_salary_details",$scope.Get_Genrated_salary_details);
                            $scope.totalItems = $scope.Get_Genrated_salary_details.length;
                            $scope.todos = $scope.Get_Genrated_salary_details;
                            $scope.makeTodos();

                            $scope.Updateemployee = true;
                        });
                    }
                });

            }


            $scope.btn_ResetEmployee_Click = function () {
                $scope.edt = {};
                $scope.edt1 = {};
                $scope.To_Updateemployee = false;
                $scope.GetSalaryDetailsByEmployee = '';

                //$scope.edt = { sd_year_month_code: n.toString() };
                $scope.filteredTodos = [];
                $scope.Updateemployee = false;
                $scope.disableDeletePaysheet = false;
                $scope.disableUpdatePaysheet = false;
                $scope.updatepaysheet = false;
                $('#deletePaysheetBtn').css('background-color', '#f35958');
                $('#updatePaysheetBtn').css('background-color', '#0aa699');
            }

            $scope.CheckMultiple = function () {
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.GetEmployeesFromSalaryDetails.length; i++) {
                    var t = $scope.GetEmployeesFromSalaryDetails[i].code;
                    var v = document.getElementById(t);
                    if (main.checked == true) {
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $scope.color = '#edefef';
                    }
                    else {
                        v.checked = false;
                        $scope.row1 = '';
                        $scope.color = '#edefef';
                        main.checked = false;
                    }
                }
            }

            $scope.CheckOneByOne = function (str) {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }

            }


            /************************************************************Update code**********************************/


            $scope.getDetailsEmployeeToUpdate = function (info) {

                //var curyear_month = $scope.edt.year_month;
                //var cur_month = $scope.edt.year_month.substr(4);
                //$scope.edt.year_month = curyear_month;//$scope.fin_year + cur_month;
                
                $scope.yearMonth = $scope.fin_year + $scope.edt.year_month;
                $scope.employeenameforcmb = [];
                var data = {
                    em_login_code: info.sd_number,
                    emp_name: info.emp_name
                }
                
                $scope.employeenameforcmb.push(data);
                $scope.temp = { em_login_code: info.sd_number };


                $scope.Updateemployee = false;
                //$scope.edt.sd_year_month = $scope.edt.sd_year_month;
                $http.get(ENV.apiUrl + "api/updatepaysheet/GetSalaryDetailsByEmployee?year_month=" + $scope.yearMonth + '&sd_number=' + info.sd_number).then(function (GetSalaryDetailsByEmployee) {
                    $scope.GetSalaryDetailsByEmployee = GetSalaryDetailsByEmployee.data;
                    $scope.postSalaryDetails = angular.copy($scope.GetSalaryDetailsByEmployee);
                    
                    $scope.totalItems = $scope.GetSalaryDetailsByEmployee.length;
                    $scope.todos = $scope.GetSalaryDetailsByEmployee;
                    $scope.makeTodos();
                    $scope.edt.total = 0;
                    $scope.edt.deductionAmt = 0;
                    $scope.edt.grossAmt = 0;
                    for (var j = 0; j < $scope.GetSalaryDetailsByEmployee.length; j++) {
                        if ($scope.GetSalaryDetailsByEmployee[j].pc_earn_dedn_tag == 'E') {
                            $scope.edt.total = $scope.edt.total + $scope.GetSalaryDetailsByEmployee[j].sd_amount;
                        }
                        if ($scope.GetSalaryDetailsByEmployee[j].pc_earn_dedn_tag == 'D') {
                            $scope.edt.deductionAmt = $scope.edt.deductionAmt + $scope.GetSalaryDetailsByEmployee[j].sd_amount;
                        }
                    }
                    $scope.To_Updateemployee = true;

                    $scope.pe_status = '';
                    $scope.pe_status = info.pe_status;

                });

                $http.get(ENV.apiUrl + "api/updatepaysheet/GetComapnyCurrencyDetails").then(function (res) {
                     $scope.currencyDetails = res.data;
                     if ($scope.currencyDetails.length > 0) {
                         $scope.comp_curcy_dec = $scope.currencyDetails[0].comp_curcy_dec;
                     }
                     else {
                         $scope.comp_curcy_dec = 2;
                     }                     
                });

            }

            $scope.btn_closeupdate_click = function () {
                $scope.temp = '';
                newvalue = false;
                $scope.btn_SearchEmployee_Click();
            }

            $scope.btn_Employeepayrollupdate_click = function () {
                var employeedata = '';
                var sd_company_code = '1';
                var name = '';
                

                if (newvalue != true) {
                    for (var i = 0; i < $scope.GetSalaryDetailsByEmployee.length; i++) {
                        if ($scope.GetSalaryDetailsByEmployee[i].ischange == true) {
                            year_month = $scope.GetSalaryDetailsByEmployee[i].sd_year_month;
                            sd_company_code = $scope.GetSalaryDetailsByEmployee[i].sd_company_code;
                            employeedata = employeedata + $scope.GetSalaryDetailsByEmployee[i].sd_pay_code + '!' + $scope.GetSalaryDetailsByEmployee[i].sd_number + '!' + $scope.GetSalaryDetailsByEmployee[i].sd_amount + '!' + $scope.GetSalaryDetailsByEmployee[i].sd_remark + '/';
                            //name=name+
                        }
                    }

                    var data = {
                        sd_number: user,
                        sd_company_code: sd_company_code,
                        sd_year_month: year_month,
                        sd_pay_code: employeedata,
                        name: ''

                    }

                }
                else {
                    if ($scope.temp.sd_remark == undefined)
                        $scope.temp.sd_remark = '';
                    var data = {
                        sd_number: user,
                        sd_company_code: $scope.GetSalaryDetailsByEmployee[0].sd_company_code,
                        sd_year_month: $scope.GetSalaryDetailsByEmployee[0].sd_year_month,
                        sd_pay_code: $scope.temp.sd_pay_code + '!' + $scope.temp.em_login_code + '!' + $scope.temp.sd_amount + '!' + $scope.temp.sd_remark + '/',
                        name: ''
                    }                    
                }

                $http.post(ENV.apiUrl + "api/updatepaysheet/Update_Employee_Salary_Details", data).then(function (msg1) {
                    $scope.msg = msg1.data;
                    if ($scope.msg == true) {
                        $scope.temp = '';
                        newvalue = false;
                        swal({ text: 'Payroll updated successfully', width: 380 });
                        $scope.btn_SearchEmployee_Click();
                    }
                    else if ($scope.msg == false) {
                        $scope.temp = '';
                        swal({ text: 'Payroll not updated', width: 380 });
                        $scope.btn_SearchEmployee_Click();
                    }
                    else {
                        swal("Error-"+$scope.msg)
                    }
                });
            }
         
            $scope.btn_Employeepayrollupdate_new = function () {
                debugger;
                var employeedata = '';
                var sd_company_code = '1';
                var name = '';

                $scope.postSalaryDetails = angular.copy($scope.GetSalaryDetailsByEmployee);
                if ($scope.pe_status == 'P') {
                    swal({ title: 'Alert', text: 'You can not update pay sheet.Payroll is finalized for ' + $scope.temp.em_login_code, width: 380, showCloseButton: true });
                    return;
                }
                
                if (newvalue != true) {                    
                    
                }
                else {
                    if ($scope.temp.sd_remark == undefined)
                        $scope.temp.sd_remark = '';
                        
                    for (var i = 0; i < $scope.postSalaryDetails.length; i++) {
                        if ($scope.temp.sd_pay_code != $scope.postSalaryDetails[i].sd_pay_code) {
                            var obj = {
                                sd_number: $scope.temp.em_login_code,
                                sd_company_code: $scope.GetSalaryDetailsByEmployee[0].sd_company_code,
                                sd_year_month: $scope.GetSalaryDetailsByEmployee[0].sd_year_month,
                                sd_pay_code: $scope.temp.sd_pay_code,
                                sd_amount: $scope.temp.sd_amount,
                                sd_remark: $scope.temp.sd_remark,
                                name: '',
                                ischange: true
                            }
                            $scope.postSalaryDetails.push(obj);
                            break;
                        }
                    }                   
                }

                $http.post(ENV.apiUrl + "api/updatepaysheet/Update_Employee_Salary_Details_New", $scope.postSalaryDetails).then(function (msg1) {
                    $scope.msg = msg1.data;
                    if ($scope.msg == true) {
                        $scope.temp = '';
                        newvalue = false;
                        swal({ text: 'Payroll updated successfully', width: 380 });
                        $scope.btn_SearchEmployee_Click();
                    }
                    else if ($scope.msg == false) {
                        $scope.temp = '';
                        swal({ text: 'Payroll not updated. ' , width: 380 });
                        $scope.btn_SearchEmployee_Click();
                    }
                    else {
                        swal("Error-" + $scope.msg)
                    }
                });
            }

            $scope.checkedforedit = function (info) {

                info.ischange = true;                
                $scope.edt.total = 0;
                $scope.edt.deductionAmt = 0;
                for (var j = 0; j < $scope.GetSalaryDetailsByEmployee.length; j++) {
                    //$scope.edt.total = $scope.edt.total + $scope.GetSalaryDetailsByEmployee[j].sd_amount;
                    if ($scope.GetSalaryDetailsByEmployee[j].pc_earn_dedn_tag == 'E') {
                        $scope.edt.total = $scope.edt.total + $scope.GetSalaryDetailsByEmployee[j].sd_amount;
                    }
                    if ($scope.GetSalaryDetailsByEmployee[j].pc_earn_dedn_tag == 'D') {
                        $scope.edt.deductionAmt = $scope.edt.deductionAmt + $scope.GetSalaryDetailsByEmployee[j].sd_amount;
                    }
                }
                
            }

            $scope.Insertnew = function (info) {
                for (var i = 0; i < $scope.GetSalaryDetailsByEmployee.length; i++) {
                    if (info == $scope.GetSalaryDetailsByEmployee[i].sd_pay_code) {
                        $scope.temp = {
                            sd_pay_code: '',
                            em_login_code: $scope.GetSalaryDetailsByEmployee[i].sd_number,
                        };
                    } else {
                        newvalue = true;
                    }

                }         
            }

            $scope.Insertnew1 = function (info) {
                newvalue = true;
            }

            $scope.numberWithCommas = function (x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.Homeroom_MultipleDelete();
                }
            };

            $scope.size = function (str) {

                //if (str == 'All') {
                 
                //    $scope.totalItems = $scope.Get_Genrated_salary_details.length;
                //    $scope.todos = $scope.Get_Genrated_salary_details;
                //    //$scope.filteredTodos = $scope.GetAllStudentFee;
                //    $scope.numPerPage = $scope.Get_Genrated_salary_details.length;
                //    $scope.maxSize = $scope.Get_Genrated_salary_details.length
                //    $scope.makeTodos();
                //}
                //else {
                //    $scope.pagesize = str;
                //    $scope.currentPage = 1;
                //    $scope.numPerPage = str;
                //    $scope.makeTodos();
                //    //main = document.getElementById('mainchk');
                //    //if (main.checked == true) {
                //    //    main.checked = false;
                //    //    $scope.row1 = '';
                //    //    $scope.color = '#edefef';
                //    //   }
                //}

                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }
            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str;
            //    $scope.makeTodos();
            //    main = document.getElementById('mainchk');
            //    if (main.checked == true) {
            //        main.checked = false;
            //        $scope.row1 = '';
            //        $scope.color = '#edefef';
            //    }
            //}

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.onlyNumbers = function (event) {
                
                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event

                    }

                }

                event.preventDefault();


            };


            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Get_Genrated_salary_details, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Get_Genrated_salary_details;
                }
                $scope.makeTodos();
            }

            $scope.isNumberKey = function (evt) {
                
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode != 46 && charCode > 31
                  && (charCode < 48 || charCode > 57))
                    return false;

                return true;
            }


            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1 });
                $("#Table2").tableHeadFixer({ 'top': 1 });
                $("#Table3").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.Reset = function () {
                $scope.edt = {};
                $scope.edt1 = {};
                $scope.deletepaysheettable = false;
                $scope.disableDeletePaysheet = false;
                $scope.disableUpdatePaysheet = false;
                $scope.deletepaysheet = false;
                $('#deletePaysheetBtn').css('background-color', '#f35958');                
                $('#updatePaysheetBtn').css('background-color', '#0aa699');
            }


            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sd_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.emp_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.gr_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.dept_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.dg_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.acno == toSearch) ? true : false;
            }
        }])
})();