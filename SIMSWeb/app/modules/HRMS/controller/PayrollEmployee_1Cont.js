﻿(function () {
    'use strict';
    var main = '';
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('PayrollEmployee_1Cont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.busyindicator = false;
            $scope.grid = true;
            $scope.color = '#edefef';
            $scope.rgvtbl = false;
            $scope.btnhide = true;
            $scope.edt = {};

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format:'dd-mm-yyyy'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.setStart = function (date) {
                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //var date1 = year + "-" + month + "-" + day;

                $scope.pa_effective_from = date;

            }

            $scope.setEnd = function (date) {
                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //var date1 = year + "-" + month + "-" + day;

                $scope.pa_effective_upto = date;

            }

            $http.get(ENV.apiUrl + "api/PaycodewiseEmployee/GetDept").then(function (GetDept) {
                $scope.GetDept = GetDept.data;

            });

            $http.get(ENV.apiUrl + "api/PaycodewiseEmployee/GetDesg").then(function (GetDesg) {
                $scope.GetDesg = GetDesg.data;

            });

            $http.get(ENV.apiUrl + "api/PaycodewiseEmployee/GetGrade").then(function (GetGrade) {
                $scope.GetGrade = GetGrade.data;

            });

            $http.get(ENV.apiUrl + "api/PaycodewiseEmployee/GetPayCode").then(function (GetPayCode) {
                $scope.GetPayCode = GetPayCode.data;
            });

            $http.get(ENV.apiUrl + "api/updatepaysheet/GetComapnyCurrencyDetails").then(function (res) {
                $scope.currencyDetails = res.data;
                if ($scope.currencyDetails.length > 0) {
                    $scope.comp_curcy_dec = $scope.currencyDetails[0].comp_curcy_dec;
                }
                else {
                    $scope.comp_curcy_dec = 2;
                }
            });


            var date = new Date();
            var month = (date.getMonth() + 1);
            var day = date.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            //$scope.pa_effective_from = date.getFullYear() + '-' + (month) + '-' + (day);
            //$scope.pa_effective_from = (day) + '-' + (month) + '-' + date.getFullYear();
            $scope.pa_effective_from = (day) + '-' + (month) + '-' + date.getFullYear();
            $scope.changeno = function (obj) {

                //console.log(obj)
                //$scope.maindata = obj;

                //if ($scope.maindata.leave_max >= 365 || $scope.maindata.leave_max <= 0) {
                //    swal('', 'Please enter 0 to 365 days.');
                //    $scope.maindata.leave_max = $scope.maindata.leave_max_old;
                //}
                //else {

                //    if (parseFloat($scope.maindata.leave_taken) > parseFloat($scope.maindata.leave_max)) {
                //        swal('', 'Maximum leave not less than taken leave.');
                //        $scope.maindata.leave_max = $scope.maindata.leave_max_old;
                //    }
                //    else
                //        $scope.maindata.leave_max_old = $scope.maindata.leave_max;
                //}
            }

            $scope.onlyNumbers = function (event, leave_max) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }

                }

                event.preventDefault();


            };


            function numberWithCommas(x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }

            $scope.Show_data = function () {

              
                if ($scope.edt.dept_code == undefined)
                    $scope.edt.dept_code = '';
                if ($scope.edt.desg_code == undefined)
                    $scope.edt.desg_code = '';
                if ($scope.edt.gr_code == undefined)
                    $scope.edt.gr_code = '';
                if ($scope.edt.paycode_code == '' || $scope.edt.paycode_code == undefined) {
                    swal('', 'Please select paycode.');
                }
                else {
                    if ($scope.pa_effective_from == '' || $scope.pa_effective_from == undefined) {
                        swal('', 'Please select effective from date.');
                    }
                    else {

                        $scope.busy = true;
                        var datalst = {
                            dept_code: $scope.edt.dept_code,
                            desg_code: $scope.edt.desg_code,
                            gr_code: $scope.edt.gr_code,
                            paycode_code: $scope.edt.paycode_code,
                            pa_effective_from: $scope.pa_effective_from
                        }
                        $http.post(ENV.apiUrl + "api/PaycodewiseEmployee/EmpDetails", datalst).then(function (EmpDetails) {
                            //$scope.GetEmpDetails = EmpDetails.data;
                            console.log("GetEmpDetails",$scope.GetEmpDetails);
                            $scope.GetEmpDetails = [];
                            if (EmpDetails.data.length > 0) {
                                $scope.btnhide = false;
                                $scope.rgvtbl = true;
                                angular.forEach(EmpDetails.data, function (value, key) {
                                    var obj = {
                                        em_number: value.em_number,
                                        em_login_code: value.em_login_code,
                                        emp_name: value.emp_name,
                                        pa_effective_from: value.pa_effective_from,
                                        pa_effective_upto: value.pa_effective_upto,
                                        paycode_code: value.paycode_code,
                                        paycode_name: value.paycode_name,
                                        pay_amount: numberWithCommas(parseFloat(value.pay_amount).toFixed($scope.comp_curcy_dec)),
                                        //pay_amount: parseFloat(value.pay_amount).toFixed($scope.comp_curcy_dec),
                                        pd_remark: value.pd_remark
                                    }
                                    $scope.GetEmpDetails.push(obj);
                                });
                            }
                            else {
                                $scope.rgvtbl = false;
                                $scope.btnhide = true;
                                swal('', 'Record not found.');
                            }
                            $scope.busy = false;
                        });

                    }
                }

            }

            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.reset_form = function () {
                $scope.edt = {};
                $scope.GetEmpDetails = [];
                $scope.btnhide = true;
                $scope.rgvtbl = false;
                var date = new Date();
                var month = (date.getMonth() + 1);
                var day = date.getDate();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;

                //$scope.pa_effective_from = date.getFullYear() + '-' + (month) + '-' + (day);
                $scope.pa_effective_from = (day) + '-' + (month) + '-' + date.getFullYear();
                //$scope.pa_effective_from = (day) + '-' + (month) + '-' + date.getFullYear();
                $scope.pa_effective_upto = '';


            }

            $scope.genrate = function () {
                var datalst = [];
                $scope.busy = true;
                $scope.rgvtbl = false;
                $scope.btnhide = true;
               
                if ($scope.pa_effective_upto == '' || $scope.pa_effective_upto == undefined) {
                    $scope.pa_effective_upto = '';
                }
                for (var i = 0; i < $scope.GetEmpDetails.length; i++) {
                        var data = {
                            em_number: $scope.GetEmpDetails[i].em_number,
                            pa_effective_from: $scope.pa_effective_from,
                            pa_effective_upto: $scope.pa_effective_upto,
                            pd_pay_code: $scope.GetEmpDetails[i].paycode_code,
                            pd_amount: $scope.GetEmpDetails[i].pay_amount,
                            pd_remark: $scope.GetEmpDetails[i].pd_remark
                        }
                        datalst.push(data);

                }
                $http.post(ENV.apiUrl + "api/PaycodewiseEmployee/UpdatePaysPayable", datalst).then(function (UpdatePaysPayable) {
                    $scope.UpdatePaysPayable = UpdatePaysPayable.data;

                    swal('', 'Record successfully updated.');
                    $scope.busy = false;
                    $scope.reset_form();
                });

            }







        }])
})();