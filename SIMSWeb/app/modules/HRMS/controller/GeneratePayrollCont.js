﻿(function () {
    'use strict';
    var main = '';
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('GeneratePayrollCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {

            $scope.display = false;
            $scope.busyindicator = false;
            $scope.grid = true;
            $scope.color = '#edefef';
            $scope.rgvtbl = false;
            $scope.searchbox=false;
            $scope.fi_date = true;
            var d = new Date();
            var n = d.getMonth() + 1;
            var L = '';
            var O = '';
            var G = '';
            var AG = '';
            var M = '';
            var C = '';
            var NG = '';
            $scope.temp = {};
            $scope.edt = {};
            $scope.edt.publish_date = $filter('date')(new Date(), 'dd-MM-yyyy');
            //$scope.finalize_date = $filter('date')(new Date(), 'dd-MM-yyyy');
            $scope.edt.include_air_fare = false;
            $scope.edt.include_leave_salary = false;
            $scope.showForSOK = false;

            var schoolArray = ['sok'];
            if (schoolArray.includes($http.defaults.headers.common['schoolId'])) {
                $scope.showForSOK = true;
            }

            $http.get(ENV.apiUrl + "api/PaysGradeChange/Get_gc_company_Code").then(function (compcode) {
                $scope.comp_code = compcode.data;
                $scope.temp['gc_company_code'] = $scope.comp_code[0].gc_company_code;
                $scope.getdept($scope.temp['gc_company_code']);
                $scope.getdesg($scope.temp['gc_company_code']);
            });

            $scope.getdept = function (dept) {

                $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/Get_dept_Code?company_code=" + dept).then(function (Department) {
                    $scope.dept = Department.data;
                });
            }

            $scope.getdesg = function (design) {
                $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/Get_designation_Code?company_code=" + design).then(function (Designation) {
                    $scope.designation = Designation.data;
                });
            }

            $http.get(ENV.apiUrl + "api/generatepayroll/GetCurrentFinYearFromFinsParameter").then(function (GetCurrentFinYearFromFinsParameter) {

                $scope.GetCurrentFinYearFromFinsParameter = GetCurrentFinYearFromFinsParameter.data;                
                $scope.fin_year = $scope.GetCurrentFinYearFromFinsParameter[0].fin_year;
                $scope.fin_year_desc = $scope.GetCurrentFinYearFromFinsParameter[0].fin_year_desc;
                console.log("$scope.fin_year");
            });

            $http.get(ENV.apiUrl + "api/generatepayroll/GetDept").then(function (GetDept) {
                $scope.GetDept = GetDept.data;

            });

            $http.get(ENV.apiUrl + "api/generatepayroll/GetDesg").then(function (GetDesg) {
                $scope.GetDesg = GetDesg.data;

            });

            $(function () {
                $('#cmb_grade').multipleSelect({
                    width: '100%'
                });
            });

            $http.get(ENV.apiUrl + "api/generatepayroll/Get_employee_service_status").then(function (Get_employee_service_status) {
                $scope.Get_employee_service_status = Get_employee_service_status.data;                
                setTimeout(function () {
                    $('#cmb_grade').change(function () {                        
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);

            });


            $http.get(ENV.apiUrl + "api/generatepayroll/Get_alert_messages").then(function (Get_alert_messages) {
                $scope.Get_alert_messages = Get_alert_messages.data;
                for (var i = 0; i < $scope.Get_alert_messages.length; i++) {
                    if ($scope.Get_alert_messages[i].msg_code == 'L')
                        L = $scope.Get_alert_messages[i].msg_desc;
                    else if ($scope.Get_alert_messages[i].msg_code == 'O')
                        O = $scope.Get_alert_messages[i].msg_desc;
                    else if ($scope.Get_alert_messages[i].msg_code == 'G')
                        G = $scope.Get_alert_messages[i].msg_desc;
                    else if ($scope.Get_alert_messages[i].msg_code == 'AG')
                        AG = $scope.Get_alert_messages[i].msg_desc;
                    else if ($scope.Get_alert_messages[i].msg_code == 'M')
                        M = $scope.Get_alert_messages[i].msg_desc;
                    else if ($scope.Get_alert_messages[i].msg_code == 'C')
                        C = $scope.Get_alert_messages[i].msg_desc;
                    else if ($scope.Get_alert_messages[i].msg_code == 'NG')
                        NG = $scope.Get_alert_messages[i].msg_desc;
                }

            });

            $http.get(ENV.apiUrl + "api/generatepayroll/GetCashBankTags").then(function (GetCashBankTags) {
                $scope.GetCashBankTags = GetCashBankTags.data;                
                angular.forEach($scope.GetCashBankTags, function (value, key) {
                    if (value.bct_name == 'Both') {
                        $scope.edt.bct_code = 'A';
                    }
                });
            });

            $http.post(ENV.apiUrl + "api/OvertimeHour/Year_month").then(function (Year_month) {
                $scope.Year_month = Year_month.data;
                var d = new Date().getFullYear();
                var n = new Date().getMonth() + 1;
                var ym = d + '' + n;
                $scope.edt.yearMonth = ym;
                $scope.setYearMonth();
            });

            $scope.setYearMonth = function () {
                debugger
                $scope.edt.sd_year_month_code = $scope.edt.yearMonth.substr(4);
                //$scope.fin_year = $scope.edt.yearMonth.substr(0, 4);
                $scope.show_year = $scope.edt.yearMonth.substr(0, 4);
                console.log($scope.edt.yearMonth.substr(4));
                console.log($scope.edt.yearMonth.substr(0, 4));
            }

            $http.get(ENV.apiUrl + "api/generatepayroll/GetMonth").then(function (GetMonth) {
                $scope.GetMonth = GetMonth.data;
               // $scope.edt.sd_year_month_code = n.toString();                
            });

            $http.get(ENV.apiUrl + "api/updatepaysheet/GetComapnyCurrencyDetails").then(function (res) {
                $scope.currencyDetails = res.data;
                if ($scope.currencyDetails.length > 0) {
                    $scope.comp_curcy_dec = $scope.currencyDetails[0].comp_curcy_dec;
                }
                else {
                    $scope.comp_curcy_dec = 2;
                }
            });

            $scope.select_date_finalize = function (date) {
                debugger;
                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                var date1 = year + "-" + month + "-" + day;
                $scope.finalize_date = date1;
            }

            $scope.Show_data = function () {
                debugger
                if ($scope.edt.bct_code == undefined || $scope.edt.bct_code == '') {
                    swal('', 'Please select bank or cash Tag.');
                }
                else {
                    if ($scope.edt.service_Status_code == undefined || $scope.edt.service_Status_code == '') {
                        swal('', 'Please select service status.');
                    }
                    else {
                        if ($scope.edt.publish_date == undefined || $scope.edt.publish_date == '') {
                            swal('', 'Please select publish date.');
                        }
                        else {
                            if ($scope.fin_status == true && ($scope.finalize_date == '' || $scope.finalize_date == undefined)) {
                                swal('', 'Please select finalize date.');
                            }
                            else if ($scope.temp.gc_company_code == undefined || $scope.temp.gc_company_code == "" || $scope.temp.gc_company_code == null) {
                                swal('', 'Please select company.');
                            }
                            else {

                                main = document.getElementById('grademainchk');
                                main.checked = false;
                                $scope.row1 = '';
                                $scope.color = '#edefef';
                                main = document.getElementById('mainchk');
                                $scope.row2 = '';
                                $scope.color = '#edefef';
                                main.checked = false;
                                $scope.EmployeeNamesByGrade = '';
                                $scope.GetAllPaysGradeName = '';
                                $scope.rgvtbl = false;

                                $http.get(ENV.apiUrl + "api/generatepayroll/GetAllPaysGradeName?company_code=" + $scope.temp.gc_company_code).then(function (GetAllPaysGradeName) {
                                    $scope.GetAllPaysGradeName = GetAllPaysGradeName.data;
                                    if ($scope.GetAllPaysGradeName.length > 0) {
                                        $scope.rgvtbl = true;
                                    }
                                    else {
                                        $scope.rgvtbl = false;
                                        //swal({ title: 'Alert', text:'' });
                                    }
                                });
                                //$http.post(ENV.apiUrl + "api/EmpAttendanceDailyController/EmployeeNamesByGrade?sdate=" + $scope.edt.sdate + "&edate=" + $scope.edt.edate + "&dept=" + $scope.edt.dep_code + "&desg=" + $scope.edt.dg_code).then(function (Pers326_Get_attednce_deatils) {

                                //    $scope.Pers326_Get_attednce_deatils1 = Pers326_Get_attednce_deatils.data;
                                //});
                            }
                        }
                    }
                }



            }

            $scope.GetEmployeeData = function () {




                //var year_month = $scope.fin_year + $scope.edt.sd_year_month_code

                //$http.get(ENV.apiUrl + "api/generatepayroll/EmployeeNamesByGrade?list_grade=" + $scope.edt.grade_code + "&year_month=" + year_month + "&date=" + $scope.edt.bank_code).then(function (EmployeeNamesByGrade) {
                //    $scope.EmployeeNamesByGrade = EmployeeNamesByGrade.data;

                //    $scope.totalItems = $scope.EmployeeNamesByGrade.length;
                //    $scope.todos = $scope.EmployeeNamesByGrade;
                //    $scope.makeTodos();
                //});
            }

            var employee_list = '';
            var employee_list_arr = [];

            //$scope.genrate = function () {
            //    employee_list = '';
            //    for (var i = 0; i < $scope.EmployeeNamesByGrade.length; i++) {
            //        var v = document.getElementById($scope.EmployeeNamesByGrade[i].emp_number);
            //        if (v.checked == true) {
            //            employee_list_arr.push($scope.EmployeeNamesByGrade[i]);
            //            employee_list = employee_list + $scope.EmployeeNamesByGrade[i].emp_number + '/';
            //            //demo.push({ id: $scope.EmployeeNamesByGrade[i].code });
            //        }
            //    }
            //    if (employee_list_arr.count() > 0) {
            //        var gradecode = '';
            //        for (var i = 0; i < $scope.GetAllPaysGradeName.length; i++) {
            //            var grade = document.getElementById($scope.GetAllPaysGradeName[i].gr_code)
            //            if (grade.checked == true) {
            //                gradecode = gradecode + $scope.GetAllPaysGradeName[i].gr_code + '/';
            //            }
            //        }


            //        $http.post(ENV.apiUrl + "api/generatepayroll/Generate_Payroll?year_month=" + yrmonth + "&emp_list=" + employee_list + "&grade_list=" + gradecode + "&em_bank_cash_tag=" + $scope.edt.bct_code + "&empldate=" + $scope.edt.publish_date + "&dep_code=" + $scope.edt.dep_code + "&dg_code=" + $scope.edt.dg_code).then(function (msg1) {
            //            $scope.msg = msg1.data;

            //        });
            //    }
            //    else {
            //        swal('', 'Please Select Employee...!!!');
            //    }
            //}

            $scope.Generate_payroll = function () {
                debugger
                var str = '';
                var demo = [{ id: '' }];
                employee_list = '';
                employee_list_arr = [];
                var datasend = [];
                debugger


                for (var i = 0; i < $scope.EmployeeNamesByGrade.length; i++) {
                    var v = document.getElementById($scope.EmployeeNamesByGrade[i].emp_number);
                    if (v!=null) {
                        if (v.checked == true) {
                            var yrmonth = $scope.fin_year + $scope.edt.sd_year_month_code;

                            var data = {
                                sd_year_month: yrmonth
                                , emp_number: $scope.EmployeeNamesByGrade[i].emp_number
                            };
                            datasend.push(data);
                            //employee_list_arr.push($scope.EmployeeNamesByGrade[i]);
                            //employee_list = employee_list + $scope.EmployeeNamesByGrade[i].emp_number + '/';
                            //demo.push({ id: $scope.EmployeeNamesByGrade[i].code });
                        }
                    }
                }



                $http.post(ENV.apiUrl + "api/generatepayroll/chk_payroll_genrated_for_current_month", datasend).then(function (chk_payroll_genrated_for_current_month) {
                    //$scope.chk_payroll_genrated_for_current_month = chk_payroll_genrated_for_current_month.data;


                    if (chk_payroll_genrated_for_current_month.data.length == 0) {
                        $http.post(ENV.apiUrl + "api/generatepayroll/chk_payroll_genrated_for_last_month", datasend).then(function (chk_payroll_genrated_for_last_month) {

                            $scope.chk_payroll_genrated_for_last_month = chk_payroll_genrated_for_last_month.data;
                            employee_list_arr = [];
                            for (var i = 0; i < $scope.EmployeeNamesByGrade.length; i++) {
                                var v = document.getElementById($scope.EmployeeNamesByGrade[i].emp_number);
                                if (v != null) {
                                    if (v.checked == true) {
                                        employee_list_arr.push($scope.EmployeeNamesByGrade[i]);
                                        //demo.push({ id: $scope.EmployeeNamesByGrade[i].code });
                                    }
                                }
                            }


                            for (var i = 0, len = $scope.chk_payroll_genrated_for_last_month.length; i < len; i++) {
                                for (var j = 0, len2 = employee_list_arr.length; j < len2; j++) {
                                    if ($scope.chk_payroll_genrated_for_last_month[i].emp_lst1 === employee_list_arr[j].emp_number || $scope.chk_payroll_genrated_for_last_month[i].emp_lst2 === employee_list_arr[j].emp_number) {
                                        employee_list_arr.splice(j, 1);
                                        len2 = employee_list_arr.length;

                                    }

                                }
                            }
                            //if (employee_list_arr.length == 0) {
                            swal({
                                title: '',
                                text: L,
                                showCloseButton: true,
                                showCancelButton: true,
                                confirmButtonText: 'Yes',
                                width: 380,
                                cancelButtonText: 'No',

                            }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $http.post(ENV.apiUrl + "api/generatepayroll/update_att_and_ot_status?parameter=L" +
                                        "&status=" + true).then(function (msg1) {
                                            $scope.msg = msg1.data;


                                            swal({
                                                title: '',
                                                text: O,
                                                showCloseButton: true,
                                                showCancelButton: true,
                                                confirmButtonText: 'Yes',
                                                width: 380,
                                                cancelButtonText: 'No',

                                            }).then(function (isConfirm) {

                                                if (isConfirm) {
                                                    $http.post(ENV.apiUrl + "api/generatepayroll/update_att_and_ot_status?parameter=O" + "&status=" + true).then(function (msg1) {
                                                        $scope.msg = msg1.data;

                                                        if ($scope.msg === true) {
                                                            $scope.genrate();
                                                        }

                                                    });
                                                }
                                                else {

                                                    $http.post(ENV.apiUrl + "api/generatepayroll/update_att_and_ot_status?parameter=O" + "&status=" + false).then(function (msg1) {
                                                        $scope.msg = msg1.data;
                                                        if ($scope.msg === true) {
                                                            $scope.genrate();
                                                        }

                                                    });
                                                }

                                            });




                                        });
                                }
                                else {
                                    $http.post(ENV.apiUrl + "api/generatepayroll/update_att_and_ot_status?parameter=L" +
                                       "&status=" + false).then(function (msg1) {
                                           $scope.msg = msg1.data;

                                           swal({
                                               title: '',
                                               text: O,
                                               showCloseButton: true,
                                               showCancelButton: true,
                                               confirmButtonText: 'Yes',
                                               width: 380,
                                               cancelButtonText: 'No',

                                           }).then(function (isConfirm) {

                                               if (isConfirm) {
                                                   $http.post(ENV.apiUrl + "api/generatepayroll/update_att_and_ot_status?parameter=O" + "&status=" + true).then(function (msg1) {
                                                       $scope.msg = msg1.data;
                                                       if ($scope.msg === true) {
                                                           $scope.genrate();
                                                       }

                                                   });
                                               }
                                               else {
                                                   $http.post(ENV.apiUrl + "api/generatepayroll/update_att_and_ot_status?parameter=O" + "&status=" + false).then(function (msg1) {
                                                       $scope.msg = msg1.data;
                                                       if ($scope.msg === true) {
                                                           $scope.genrate();
                                                       }

                                                   });
                                               }

                                           });


                                       });

                                }
                            });
                            //}
                            //else {
                            //    $('#Employee_payrollMyModal').modal({ backdrop: 'static', keyboard: false });
                            //    $scope.chk_payroll = employee_list_arr;
                            //    $scope.abc = NG;
                            //    $scope.showokbtn = true;
                            //    $scope.reset_form();



                            //}





                        });


                    }
                    else {
                        $('#Employee_payrollMyModal').modal({ backdrop: 'static', keyboard: false });
                        $scope.chk_payroll = chk_payroll_genrated_for_current_month.data;
                        $scope.abc = AG;
                        $scope.showokbtn = false;



                    }

                });


            }

            $scope.current_month_yes = function () {

                $('#Employee_payrollMyModal').modal('hide');

                var len;
                var len2;
                swal({
                    title: '',
                    text: L,
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        $http.post(ENV.apiUrl + "api/generatepayroll/update_att_and_ot_status?parameter=L" +
                            "&status=" + true).then(function (msg1) {
                                $scope.msg = msg1.data;


                                swal({
                                    title: '',
                                    text: O,
                                    showCloseButton: true,
                                    showCancelButton: true,
                                    confirmButtonText: 'Yes',
                                    width: 380,
                                    cancelButtonText: 'No',

                                }).then(function (isConfirm) {

                                    if (isConfirm) {
                                        $http.post(ENV.apiUrl + "api/generatepayroll/update_att_and_ot_status?parameter=O" + "&status=" + true).then(function (msg1) {
                                            $scope.msg = msg1.data;

                                            if ($scope.msg === true) {
                                                $scope.genrate();
                                            }

                                        });
                                    }
                                    else {

                                        $http.post(ENV.apiUrl + "api/generatepayroll/update_att_and_ot_status?parameter=O" + "&status=" + false).then(function (msg1) {
                                            $scope.msg = msg1.data;
                                            if ($scope.msg === true) {
                                                $scope.genrate();
                                            }

                                        });
                                    }

                                });




                            });
                    }
                    else {
                        $http.post(ENV.apiUrl + "api/generatepayroll/update_att_and_ot_status?parameter=L" +
                           "&status=" + false).then(function (msg1) {
                               $scope.msg = msg1.data;

                               swal({
                                   title: '',
                                   text: O,
                                   showCloseButton: true,
                                   showCancelButton: true,
                                   confirmButtonText: 'Yes',
                                   width: 380,
                                   cancelButtonText: 'No',

                               }).then(function (isConfirm) {

                                   if (isConfirm) {
                                       $http.post(ENV.apiUrl + "api/generatepayroll/update_att_and_ot_status?parameter=O" + "&status=" + true).then(function (msg1) {
                                           $scope.msg = msg1.data;
                                           if ($scope.msg === true) {
                                               $scope.genrate();
                                           }

                                       });
                                   }
                                   else {
                                       $http.post(ENV.apiUrl + "api/generatepayroll/update_att_and_ot_status?parameter=O" + "&status=" + false).then(function (msg1) {
                                           $scope.msg = msg1.data;
                                           if ($scope.msg === true) {
                                               $scope.genrate();
                                           }

                                       });
                                   }

                               });


                           });

                    }
                });



                //for (var i = 0, len = $scope.chk_payroll.length; i < len; i++) {
                //    for (var j = 0, len2 = employee_list_arr.length; j < len2; j++) {
                //        if ($scope.chk_payroll[i].emp_number === employee_list_arr[j].emp_number) {
                //            employee_list_arr.splice(j, 1);
                //            len2 = employee_list_arr.length;
                //        }
                //    }
                //}

                //for (var i = 0; i < employee_list_arr.length; i++) {
                //    employee_list = employee_list + employee_list_arr[i].emp_number + '/';
                //}



            }

            $scope.current_month_no = function () {
                $('#Employee_payrollMyModal').modal('hide');
                $scope.reset_form();
            }



            $scope.Delete = function () {



                var del = "";

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].cn_code;
                    var v = document.getElementById(t);
                    if (v.checked == true) {
                        del = del + t + ',';
                    }
                }

                var data = {
                    cn_code: del,
                    'opr': 'D'

                };

                $http.post(ENV.apiUrl + "api/contract/CUD_Contract", data).then(function (InsertEmployeeLeaveAssign) {
                    $scope.EmployeeLeaveAssign = InsertEmployeeLeaveAssign.data;
                    $scope.grid = true;
                    $scope.display = false;
                    if ($scope.EmployeeLeaveAssign == true) {
                        swal({
                            text: 'Contract Deleted',
                            width: 300,
                            height: 300
                        });
                        $http.get(ENV.apiUrl + "api/contract/Get_Contract").then(function (Get_Contract) {
                            $scope.Contract_data = Get_Contract.data;
                            $scope.totalItems = $scope.Contract_data.length;
                            $scope.todos = $scope.Contract_data;
                            $scope.makeTodos();

                        });

                    }
                    else {
                        swal({
                            text: 'Contract Not Deleted',
                            width: 300,
                            height: 300
                        });
                    }

                });
            }

            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1 });
                $("#Table2").tableHeadFixer({ 'top': 1 });
                $("#Table3").tableHeadFixer({ 'top': 1 });
                $("#Table4").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.edt = {};
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.CheckMultiple();
                }
            };

            $scope.size = function (str) {                
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;  $scope.makeTodos();
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }
            }

    
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            $scope.CheckGradeMultiple = function () {
                main = document.getElementById('grademainchk');
                for (var i = 0; i < $scope.GetAllPaysGradeName.length; i++) {
                    var t = $scope.GetAllPaysGradeName[i].gr_code;
                    var v = document.getElementById(t);
                    if (main.checked == true) {
                        if ($scope.edt.bct_code == undefined || $scope.edt.bct_code == '') {
                            swal('', 'Please Select Bank or Cash Tag.');
                            main.checked = false;

                        }
                        else {
                            if ($scope.edt.service_Status_code == undefined || $scope.edt.service_Status_code == '') {
                                swal('', 'Please Select Service Status.');
                                main.checked = false;

                            }
                            else {
                                if ($scope.edt.publish_date == undefined || $scope.edt.publish_date == '') {

                                    swal('', 'Please Select Publish Date.');
                                    main.checked = false;

                                }
                                else {
                                    v.checked = true;
                                    $scope.row1 = 'row_selected';
                                    $scope.color = '#edefef';


                                    //$http.post(ENV.apiUrl + "api/EmpAttendanceDailyController/EmployeeNamesByGrade?sdate=" + $scope.edt.sdate + "&edate=" + $scope.edt.edate + "&dept=" + $scope.edt.dep_code + "&desg=" + $scope.edt.dg_code).then(function (Pers326_Get_attednce_deatils) {

                                    //    $scope.Pers326_Get_attednce_deatils1 = Pers326_Get_attednce_deatils.data;
                                    //});
                                }
                            }
                        }
                       
                    }
                    else {
                        v.checked = false;
                        $scope.row1 = '';
                        $scope.color = '#edefef';
                        main.checked = false;
                    }
                }
                debugger;

                if (($scope.edt.bct_code != undefined && $scope.edt.bct_code != '') && ($scope.edt.service_Status_code != undefined && $scope.edt.service_Status_code != '') && ($scope.edt.publish_date != undefined && $scope.edt.publish_date != '')) {

                    var gradecode = '';
                    for (var i = 0; i < $scope.GetAllPaysGradeName.length; i++) {
                        var grade = document.getElementById($scope.GetAllPaysGradeName[i].gr_code)
                        if (grade.checked == true) {
                            gradecode = gradecode + $scope.GetAllPaysGradeName[i].gr_code + '/';
                        }
                    }

                    var servicestatus = '';
                    for (var a = 0; a < $scope.edt.service_Status_code.length; a++) {
                        servicestatus = servicestatus + $scope.edt.service_Status_code[a] + '/';
                    }
                }
                var yrmonth = $scope.fin_year + $scope.edt.sd_year_month_code;
                var cashtag = $scope.edt.bct_code;
                var data = {
                    gr_code: gradecode,
                    sd_year_month: yrmonth,
                    em_bank_cash_tag: cashtag,
                    service_Status_code: servicestatus,
                    dep_code: $scope.edt.dep_code,
                    dg_name: $scope.edt.dg_code
                }
                debugger;
                $http.post(ENV.apiUrl + "api/generatepayroll/EmployeeNamesByGrade", data).then(function (EmployeeNamesByGrade) {
                  
                    $scope.EmployeeNamesByGrade = EmployeeNamesByGrade.data;
                    if ($scope.EmployeeNamesByGrade.length > 0) {
                        $scope.searchbox = true;
                    }


                });


            }

            $scope.gradeCheckOneByOne = function (str) {
                $scope.searchbox = true;
                var v = document.getElementById(str);
                if (($scope.edt.bct_code != undefined && $scope.edt.bct_code != '') && ($scope.edt.service_Status_code != undefined && $scope.edt.service_Status_code != '') && ($scope.edt.publish_date != undefined && $scope.edt.publish_date != '')) {
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) { //If the checkbox is checked
                            $(this).closest('tr').addClass("row_selected");
                            //Add class on checkbox checked
                            $scope.color = '#edefef';
                        } else {
                            $(this).closest('tr').removeClass("row_selected");
                            //Remove class on checkbox uncheck
                            $scope.color = '#edefef';
                        }
                    });

                    main = document.getElementById('grademainchk');
                    if (main.checked == true) {
                        main.checked = false;
                        $scope.color = '#edefef';
                    }
                }
                else {
                    v.checked = false;
                    $scope.searchbox = false;
                    swal({ text: 'Please Select All Field(s)', width: 300 });
                }
                if (($scope.edt.bct_code != undefined && $scope.edt.bct_code != '') && ($scope.edt.service_Status_code != undefined && $scope.edt.service_Status_code != '') && ($scope.edt.publish_date != undefined && $scope.edt.publish_date != '')) {

                    var gradecode = '';
                    for (var i = 0; i < $scope.GetAllPaysGradeName.length; i++) {
                        var grade = document.getElementById($scope.GetAllPaysGradeName[i].gr_code)
                        if (grade.checked == true) {
                            gradecode = gradecode + $scope.GetAllPaysGradeName[i].gr_code + '/';
                        }
                    }

                    var servicestatus = '';
                    for (var a = 0; a < $scope.edt.service_Status_code.length; a++) {
                        servicestatus = servicestatus + $scope.edt.service_Status_code[a] + '/';
                    }
                }
                var yrmonth = $scope.fin_year + $scope.edt.sd_year_month_code;
                var cashtag = $scope.edt.bct_code;
                main = document.getElementById('mainchk');
                main.checked = false;
                $scope.row2 = '';

                var data = {
                    gr_code: gradecode,
                    sd_year_month: yrmonth,
                    em_bank_cash_tag: cashtag,
                    service_Status_code: servicestatus,
                    dep_code: $scope.edt.dep_code,
                    dg_code: $scope.edt.dg_code
                }
                $http.post(ENV.apiUrl + "api/generatepayroll/EmployeeNamesByGrade", data).then(function (EmployeeNamesByGrade) {

                    $scope.EmployeeNamesByGrade = EmployeeNamesByGrade.data;

                });
            }

            $scope.select_chk_finalize = function () {
                debugger;
                if ($scope.fin_status == true)
                    $scope.fi_date = false;
                else
                    $scope.fi_date = true;


            }


            $scope.CheckEmpMultiple = function () {
                $scope.searchbox = true;
                main = document.getElementById('mainchk');
                for (var b = 0; b < $scope.EmployeeNamesByGrade.length; b++) {
                    var tb = $scope.EmployeeNamesByGrade[b].emp_number;
                    var vb = document.getElementById(tb);
                    if (main.checked == true) {
                        vb.checked = true;
                        $scope.row2 = 'row_selected';
                        $scope.color = '#edefef';


                    }
                    else {
                        $scope.searchbox = false;
                        vb.checked = false;
                        $scope.row2 = '';
                        $scope.color = '#edefef';
                        main.checked = false;
                    }
                }





            }

            $scope.EmpCheckOneByOne = function (str) {

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

            }

            $scope.genrate = function () {
                
                    var gradecode = '';
                    for (var i = 0; i < $scope.GetAllPaysGradeName.length; i++) {
                        var grade = document.getElementById($scope.GetAllPaysGradeName[i].gr_code)
                        if (grade.checked == true) {
                            gradecode = gradecode + $scope.GetAllPaysGradeName[i].gr_code + '/';
                        }
                    }
                    if ($scope.fin_date == '' || $scope.fin_date == undefined)
                        $scope.fin_date = '';
                    employee_list = '';
                    employee_list_arr = [];
                    var datasend = [];
                    for (var i = 0; i < $scope.EmployeeNamesByGrade.length; i++) {
                        var v = document.getElementById($scope.EmployeeNamesByGrade[i].emp_number);
                        if(v!=null) {
                        if (v.checked == true) {
                            var yrmonth = $scope.fin_year + $scope.edt.sd_year_month_code;
                            debugger;
                            var data = {
                                sd_year_month: yrmonth
                                , gr_code: gradecode
                                , bct_code: $scope.edt.bct_code
                                , publishdata: $scope.edt.publish_date
                                , emp_number: $scope.EmployeeNamesByGrade[i].emp_number
                                , finalize_date: $scope.finalize_date
                                , finalize_status: $scope.fin_status
                                , include_air_fare: $scope.edt.include_air_fare
                                , include_leave_salary: $scope.edt.include_leave_salary

                            };
                            datasend.push(data);
                            //employee_list_arr.push($scope.EmployeeNamesByGrade[i]);
                            //employee_list = employee_list + $scope.EmployeeNamesByGrade[i].emp_number + '/';
                            //demo.push({ id: $scope.EmployeeNamesByGrade[i].code });
                            }
                        }
                    }
                    $scope.busy = true;
                    $scope.rgvtbl = false;
                    var yrmonth = $scope.fin_year + $scope.edt.sd_year_month_code;
                    $http.post(ENV.apiUrl + "api/generatepayroll/Generate_Payroll", datasend).then(function (abcd) {

                        $scope.busy = false;
                        $scope.rgvtbl = true;

                        $('#genratedpayroll').modal({ backdrop: 'static', keyboard: false });
                        var month = '';
                        for (var i = 0; i < $scope.GetMonth.length > 0; i++) {
                            if ($scope.GetMonth[i].sd_year_month_code == $scope.edt.sd_year_month_code)
                                month = $scope.GetMonth[i].sd_year_month_name;
                        }
                        $scope.finaldata = abcd.data;
                        console.log("abcd.data",abcd.data);
                        $scope.payroll_generated_data = [];

                        function numberWithCommas(x) {
                            return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                        }
                        var yearn = '';                        
                        for (var i = 0; i < abcd.data.length; i++) {
                            var arr = abcd.data[0].emp_number.split('/');
                            yearn = arr[1];
                            debugger;
                            //if (typeof (abcd.data[i].emp_login_code) != "undefined" || typeof (abcd.data[i].emp_login_code) != undefined) {}
                              if(abcd.data[i].emp_login_code == "" || abcd.data[i].emp_login_code == undefined || abcd.data[i].emp_login_code == null) {
                               }
                                else {                                
                                    var obj = {
                                        emp_login_code: abcd.data[i].emp_login_code,
                                        emp_name: abcd.data[i].emp_name,
                                        earn: numberWithCommas(parseFloat(abcd.data[i].earn).toFixed($scope.comp_curcy_dec)),
                                        ded: numberWithCommas(parseFloat(abcd.data[i].ded).toFixed($scope.comp_curcy_dec)),
                                        total: numberWithCommas(parseFloat(abcd.data[i].total).toFixed($scope.comp_curcy_dec))
                                    }
                                    $scope.payroll_generated_data.push(obj);
                                }                            
                        }
                        console.log("payroll_generated_data",$scope.payroll_generated_data);

                        var d = new Date();
                        var curYear = d.getFullYear();

                        $scope.gen = $scope.show_year + ',' + month;

                        $scope.total_employees = 'Total Employee : ' + $scope.finaldata[$scope.finaldata.length - 1].total_employees;
                        $scope.active_employees = 'Active Employee : ' + $scope.finaldata[$scope.finaldata.length - 1].active_employees;
                        $scope.employees_on_vacation = 'Employee On Vacation : ' + $scope.finaldata[$scope.finaldata.length - 1].employees_on_vacation;
                        $scope.payroll_generated = 'Payroll Generated : ' + $scope.finaldata[$scope.finaldata.length - 1].payroll_generated;
                        $scope.currently_generated = 'Currently Payroll Generated : ' + $scope.finaldata[$scope.finaldata.length - 1].currently_generated;
                        $scope.showokbtn = true;

                    });
                

            }

            $scope.reset_form = function () {

                $('#genratedpayroll').modal('hide');
                //$scope.edt.sd_year_month_code = '';
                //$scope.edt.sd_year_month_code= n.toString();
                $scope.edt.bct_code = '';
                $scope.fin_status = false;
                $scope.f_date = false;
                $scope.finalize_date = '';
                $scope.edt.bct_code = 'A';
                $scope.edt.service_Status_code = '';
                setTimeout(function () {
                    $('#cmb_grade').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
                //$scope.edt.publish_date = '';
                $scope.edt.publish_date = $filter('date')(new Date(), 'dd-MM-yyyy');
                main = document.getElementById('grademainchk');
                main.checked = false;
                $scope.row1 = '';
                $scope.color = '#edefef';
                main = document.getElementById('mainchk');
                $scope.row2 = '';
                $scope.color = '#edefef';
                main.checked = false;
                $scope.EmployeeNamesByGrade = '';
                $scope.GetAllPaysGradeName = '';
                $scope.edt.dep_code = '';
                $scope.edt.dg_code = '';
                $scope.finalize_date = '';
                $scope.rgvtbl = false;
                $scope.searchbox = false;
                $scope.edt.include_air_fare = false;
                $scope.edt.include_leave_salary = false;
            }

            $scope.select_month = function () {
                $('#genratedpayroll').modal('hide');
                $scope.edt.bct_code = '';
                $scope.edt.service_Status_code = '';
                setTimeout(function () {
                    $('#cmb_grade').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
                $scope.edt.publish_date = '';

                main = document.getElementById('grademainchk');
                main.checked = false;
                $scope.row1 = '';
                $scope.color = '#edefef';
                main = document.getElementById('mainchk');
                $scope.row2 = '';
                $scope.color = '#edefef';
                main.checked = false;
                $scope.EmployeeNamesByGrade = '';
                $scope.GetAllPaysGradeName = '';
                $scope.edt.dep_code = '';
                $scope.edt.dg_code = '';
            }

            $scope.select_bct = function () {
                $('#genratedpayroll').modal('hide');
                $scope.edt.service_Status_code = '';
                setTimeout(function () {
                    $('#cmb_grade').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
                $scope.edt.publish_date = '';

                main = document.getElementById('grademainchk');
                main.checked = false;
                $scope.row1 = '';
                $scope.color = '#edefef';
                main = document.getElementById('mainchk');
                $scope.row2 = '';
                $scope.color = '#edefef';
                main.checked = false;
                $scope.EmployeeNamesByGrade = '';
                $scope.GetAllPaysGradeName = '';
                $scope.edt.dep_code = '';
                $scope.edt.dg_code = '';
            }



            $scope.select_dg = function () {


                main = document.getElementById('grademainchk');
                main.checked = false;
                $scope.row1 = '';
                $scope.color = '#edefef';
                main = document.getElementById('mainchk');
                $scope.row2 = '';
                $scope.color = '#edefef';
                main.checked = false;
                $scope.EmployeeNamesByGrade = '';
                $scope.GetAllPaysGradeName = '';
            }

            $scope.select_dep = function () {

                main = document.getElementById('grademainchk');
                main.checked = false;
                $scope.row1 = '';
                $scope.color = '#edefef';
                main = document.getElementById('mainchk');
                $scope.row2 = '';
                $scope.color = '#edefef';
                main.checked = false;
                $scope.EmployeeNamesByGrade = '';
                $scope.GetAllPaysGradeName = '';
            }

            $scope.select_service_status = function () {
                $('#genratedpayroll').modal('hide');

                //$scope.edt.publish_date = '';

                main = document.getElementById('grademainchk');
                main.checked = false;
                $scope.row1 = '';
                $scope.color = '#edefef';
                main = document.getElementById('mainchk');
                $scope.row2 = '';
                $scope.color = '#edefef';
                main.checked = false;
                $scope.EmployeeNamesByGrade = '';
                $scope.GetAllPaysGradeName = '';
                $scope.edt.dep_code = '';
                $scope.edt.dg_code = '';
            }

            $scope.clickme = function ($event) {
                debugger
                if ($event.keyCode == 13) {

                    $('#Employee_payrollMyModal').modal('hide');

                    $scope.reset_form();
                    // console.log("enetr");
                }
            };





            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
                //format: 'dd-mm-yyyy'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


        }])
})();