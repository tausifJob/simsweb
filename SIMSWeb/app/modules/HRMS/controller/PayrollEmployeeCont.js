﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PayrollEmployeeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.grid1 = false;
            $scope.temp = {};
            $scope.total = 0;
            $scope.showSchoolFlag = false;
            var paycode_hide = ['sisqatar'];
            
            if (paycode_hide.includes($http.defaults.headers.common['schoolId'])) {
                $scope.showSchoolFlag = true;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str;
            //    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            //$scope.index = function (str) {
            //    $scope.pageindex = str;
            //    $scope.currentPage = str;
            //    console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            //}

            //$scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            //$scope.makeTodos = function () {
            //    var rem = parseInt($scope.totalItems % $scope.numPerPage);
            //    if (rem == '0') {
            //        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
            //    }
            //    else {
            //        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
            //    }

            //    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
            //    var end = parseInt(begin) + parseInt($scope.numPerPage);

            //    console.log("begin=" + begin); console.log("end=" + end);

            //    $scope.filteredTodos = $scope.todos.slice(begin, end);
            //};

            //$scope.searched = function (valLists, toSearch) {
            //    return _.filter(valLists,

            //    function (i) {
            //        /* Search Text in all  fields */
            //        return searchUtil(i, toSearch);
            //    });
            //};

            $http.get(ENV.apiUrl + "api/PaysGradeChange/Get_gc_company_Code").then(function (compcode) {
               
                $scope.comp_code = compcode.data;
                $scope.temp['gc_company_code'] = $scope.comp_code[0].gc_company_code;
                $scope.getdept($scope.temp['gc_company_code']);
                $scope.getdesg($scope.temp['gc_company_code']);

            });

            $scope.getdept = function (dept) {

                $http.get(ENV.apiUrl + "api/PayrollEmployee/Get_dept_Code?company_code=" + dept).then(function (Department) {
                    $scope.dept = Department.data;
                });
            }

            $scope.getdesg = function (design) {
                $http.get(ENV.apiUrl + "api/PayrollEmployee/Get_designation_Code?company_code=" + design).then(function (Designation) {
                    $scope.designation = Designation.data;
                });
            }

            $scope.submit = function () {
               
                $scope.busy = true;
                $scope.grid1 = true;
                $scope.ImageView = false;
                if ($scope.temp.gc_company_code == undefined || $scope.temp.gc_company_code == '') {
                    $scope.temp.gc_company_code = '';
                }
                if ($scope.temp.em_desg_code == undefined || $scope.temp.em_desg_code == '') {
                    $scope.temp.em_desg_code = '';
                }
                if ($scope.temp.dg_code == undefined || $scope.temp.dg_code == '') {
                    $scope.temp.dg_code = '';
                }
                if ($scope.temp.em_number == undefined || $scope.temp.em_number == '') {
                    $scope.temp.em_number = '';
                }

                if ($scope.showSchoolFlag == true) {
                    $http.get(ENV.apiUrl + "api/PayrollEmployee/GetPayrollEmployeeSISQatar?com_code=" + $scope.temp.gc_company_code + '&dept_code=' + $scope.temp.dg_code + '&deg_code=' + $scope.temp.em_desg_code + '&empid=' + $scope.temp.em_number).then(function (Payroll) {
                        $scope.filteredTodos = Payroll.data;
                        $scope.totalItems = $scope.filteredTodos.length;
                        $scope.todos = $scope.filteredTodos;
                        //$scope.makeTodos();
                        $scope.busy = false;
                        $scope.page1 = true;
                       
                        if (Payroll.data.length > 0) { }
                        else {
                            $scope.ImageView = true;
                        }
                    });
                }
                else {
                    $http.get(ENV.apiUrl + "api/PayrollEmployee/GetPayrollEmployee?com_code=" + $scope.temp.gc_company_code + '&dept_code=' + $scope.temp.dg_code + '&deg_code=' + $scope.temp.em_desg_code + '&empid=' + $scope.temp.em_number).then(function (Payroll) {
                        $scope.filteredTodos = Payroll.data;
                        $scope.totalItems = $scope.filteredTodos.length;
                        $scope.todos = $scope.filteredTodos;
                        //$scope.makeTodos();
                        $scope.busy = false;
                        $scope.page1 = true;
                       
                        if (Payroll.data.length > 0) { }
                        else {
                            $scope.ImageView = true;
                        }
                    });
                }
                
            }

            //DATA Cancel
            $scope.Cancel = function () {
                //$scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                //$scope.grid1 = false;
                //$scope.filteredTodos = '';
                //$scope.todos.slice = '';
                //$scope.page1 = false;
                $('#myModal').modal('hide');
            }

            //DATA Reset
            $scope.reset = function () {
             
                $scope.temp = {};
                $scope.temp['gc_company_code'] = $scope.comp_code[0].gc_company_code;
                $scope.getdept($scope.temp['gc_company_code']);
                $scope.getdesg($scope.temp['gc_company_code']);
                $scope.j = [];
                $scope.filteredTodos = '';
                $scope.todos.slice = '';
                $scope.page1 = false;
                $scope.table = true;
                $scope.grid1 = false;
                $scope.display = true;
            }

            //DATA EDIT
            $scope.edit = function (str) {
               
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = true;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.number = true;
                $scope.company = true;
                $scope.showTotal = false;
                $scope.divcode_readonly = true;
                $scope.grid2 = false;
                $('#myModal').modal('show');
                $scope.save_btn = false;
                $scope.temp.em_number = str.em_number;
                var dt = new Date();
                //$scope.pa_effective_from = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
                $scope.pa_effective_from = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
            }

            //Show
            $scope.Show = function () {
               
                if (new Date($scope.rdp_effective_upto) < new Date($scope.pa_effective_from)) {
                    swal({ title: "Alert", text: "Effective upto date cannot be less than from date", width: 380, height: 200 });
                    return;
                }
                $scope.showTotal = true;
                $scope.grid2 = true;
                $scope.save_btn = true;
                $scope.busy = true;                
                
                $http.get(ENV.apiUrl + "api/PayrollEmployee/GetSalaryGeneratedForSelectedMonthOrNot?sd_number=" + $scope.temp.em_number + '&start=' + $scope.pa_effective_from + '&end=' + $scope.rdp_effective_upto).then(function (Sal) {
                    $scope.sal = Sal.data;
                    $scope.total = 0;
                    $scope.totalCopy = 0;
                    for (var j = 0; j < Sal.data.length; j++) {
                        if (Sal.data[j].hide_flag == false)
                            $scope.total = parseFloat($scope.total) + parseFloat(Sal.data[j].pd_amount);
                    }
                 
                    $scope.calculateTotal = function (info) {                       
                        if (paycode_hide.includes($http.defaults.headers.common['schoolId'])) {
                            if (info.pd_amount == undefined || info.pd_amount == "" || info.pd_amount == null || isNaN(info.pd_amount)) {
                                info.pd_amount = 0;
                                $scope.total = 0;
                                for (var j = 0; j < Sal.data.length; j++) {                                    
                                    $scope.total = parseFloat($scope.total) + parseFloat(Sal.data[j].pd_amount);
                                }
                            }
                            else {                                
                                $scope.total = 0;
                                for (var j = 0; j < Sal.data.length; j++) {
                                    $scope.total = parseFloat($scope.total) + parseFloat(Sal.data[j].pd_amount);
                                }                                                        
                            }
                        }                                                
                    }

                    //if (paycode_hide.includes($http.defaults.headers.common['schoolId'])) {
                    //    for (var i = 0; i < $scope.sal.length; i++) {
                    //        $scope.sal[i].disabledPaycode = false;
                    //        if ($scope.sal[i].pe_insert == false) {
                    //            $scope.sal[i].disabledPaycode = true;
                    //        }
                    //    }
                    //}
                    
                });

                //DATA SAVE INSERT

                var datasend = [];
                $scope.Save = function () {
                   
                    if ($scope.pd_amount != null && $scope.j.pa_effective_from == null) {
                        swal({ title: "Alert", text: "Please Enter date", width: 300, height: 200 });
                    }
                    for (var i = 0; i < $scope.sal.length; i++) {
                        if ($scope.sal[i].ischange == true) {
                            $scope.sal[i].em_number = $scope.temp.em_number;
                            $scope.sal[i].em_company_code = $scope.temp.gc_company_code;
                            if ($scope.sal[i].pd_amount != null && $scope.sal[i].pa_effective_from == null) {
                                $scope.sal[i].pa_effective_from = $scope.pa_effective_from;
                            }
                            datasend.push($scope.sal[i]);
                        }
                    }


                    $http.post(ENV.apiUrl + "api/PayrollEmployee/InsertUpdatePaysPayable", datasend).then(function (Sal) {
                       
                        $scope.msg1 = Sal.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted/Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Inserted/Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $scope.getgrid();
                    });
                    $scope.getgrid();
                    $scope.Cancel();
                }

                
                
                $scope.SetIschange = function (info) {                    
                    info.ischange = true;                    
                }

                
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/PayrollEmployee/GetSalaryGeneratedForSelectedMonthOrNot?sd_number=" + $scope.temp.em_number + '&start=' + $scope.pa_effective_from + '&end=' + $scope.rdp_effective_upto).then(function (Sal) {
                    $scope.sal = Sal.data;
                });
            }

            $scope.onlyNumbers = function (event) {
               
                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };


        }])

})();