﻿(function () {
    'use strict';
    var del = [];
    var main;
    var main1;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeePaySlipReportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var d = new Date();
            var n = d.getMonth() + 1;
            $scope.edt = {};
            $scope.EmployeeDetails = [];
             $scope.fi_date = true;
            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);


            $scope.select_date_publish = function (date) {
                debugger;
               
                $scope.publish_date = date;
            }

            $http.get(ENV.apiUrl + "api/generatepayroll/GetCurrentFinYearFromFinsParameter").then(function (GetCurrentFinYearFromFinsParameter) {

                $scope.GetCurrentFinYearFromFinsParameter = GetCurrentFinYearFromFinsParameter.data;
                //$scope.fin_year = $scope.GetCurrentFinYearFromFinsParameter[0].fin_year;
                $scope.fin_year_desc = $scope.GetCurrentFinYearFromFinsParameter[0].fin_year_desc;
            });



            $http.post(ENV.apiUrl + "api/Payroll/EmployeePaySlipReport/PubMonth").then(function (PubMonth) {
                $scope.PubMonth = PubMonth.data;
               
            });

            $http.post(ENV.apiUrl + "api/OvertimeHour/Year_month").then(function (Year_month) {
                $scope.Year_month = Year_month.data;
                var d = new Date().getFullYear();
                var n = new Date().getMonth() + 1;
                var ym = d + '' + n;
                $scope.edt.yearMonth = ym;
                $scope.setYearMonth();
            });

            $scope.setYearMonth = function () {
                debugger
                $scope.edt.sd_year_month_code = $scope.edt.yearMonth.substr(4);
                $scope.fin_year = $scope.edt.yearMonth.substr(0, 4);
            }

            $scope.Show_data = function () {
                debugger
                $scope.busy = true;
                $scope.rgvtbl = false;
                var month_year = $scope.fin_year + $scope.edt.sd_year_month_code
                $http.post(ENV.apiUrl + "api/Payroll/EmployeePaySlipReport/EmployeeDetails?year_month=" + month_year).then(function (EmployeeDetails) {
                    $scope.EmployeeDetails = EmployeeDetails.data;
                    console.log("EmployeeDetails", $scope.EmployeeDetails);

                    main = document.getElementById('mainchk');
                    main.checked = true;
                    for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                        if ($scope.EmployeeDetails[i].holdstatus == false) {
                            main.checked = false;
                        }
                    }
                    if ($scope.EmployeeDetails.length > 0) {
                        
                        $scope.busy = false;
                        $scope.rgvtbl = true;
                    }
                    else {
                        swal('', 'Payroll not genrated for selected month.');
                        $scope.busy = false;
                        $scope.rgvtbl = false;
                    }

                });

               
            }


            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.colsvis = false;


            $scope.exportData = function () {
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('rpt_data').innerHTML], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                        });
                        saveAs(blob, "Payroll.xls");
                        $scope.colsvis = false;

                    }

                });
            };
                    
            $scope.openDocumentReport = function (obj) {
                debugger
                console.log($scope.edt.sd_year_month_code);

                var data = {
                    //location: 'Sims.SIMR51FeeRec',
                    location: 'Payroll.PayslipReciept',
                    parameter: {

                        paysheet_year:$scope.fin_year,
                        paysheet_month:$scope.edt.sd_year_month_code,
                        emp_id: obj.em_login_code
                    },
                    state: 'main.EPSR01',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter');
            }

            //$('*[data-datepicker="true"] input[type="text"]').datepicker({
            //    todayBtn: true,
            //    orientation: "top left",
            //    autoclose: true,
            //    todayHighlight: true
            //});

            //$(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
            //    $('input[type="text"]', $(this).parent()).focus();
            //});

            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);


        }])
})();