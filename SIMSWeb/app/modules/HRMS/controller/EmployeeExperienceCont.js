﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.HRMS');
    simsController.controller('EmployeeExperienceCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.temp = {};

            $http.get(ENV.apiUrl + "api/EmpExp/getAllEmployeeExp").then(function (res) {
                $scope.Emp_exp_data = res.data;
                $scope.totalItems = $scope.Emp_exp_data.length;
                $scope.todos = $scope.Emp_exp_data;
                $scope.makeTodos();
            });

            $scope.getGrid = function () {
                $http.get(ENV.apiUrl + "api/EmpExp/getAllEmployeeExp").then(function (res) {
                    $scope.Emp_exp_data = res.data;
                    $scope.totalItems = $scope.Emp_exp_data.length;
                    $scope.todos = $scope.Emp_exp_data;
                    $scope.makeTodos();
                });
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $http.get(ENV.apiUrl + "api/EmpExp/getCompanyName").then(function (comp) {
                $scope.comp_data = comp.data;
                $scope.temp['co_company_code'] = $scope.comp_data[0].co_company_code;
            });

            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str;
            //    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    //$scope.makeTodos();
                    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                }
            }


            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Emp_exp_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Emp_exp_data;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.em_login_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.em_emp_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.em_previous_job_company.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.em_previous_job_salary_details == toSearch) ? true : false;
            }

            $scope.New = function () {

                $http.get(ENV.apiUrl + "api/EmpExp/getMonthName").then(function (mon) {
                    $scope.month_data = mon.data;
                });
                $scope.disabled = false;
                $scope.readonly = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                //$scope.searchText = false;
                $scope.searchBtn = false;
                $scope.temp = "";
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp = {};

                $scope.temp['co_company_code'] = $scope.comp_data[0].co_company_code;
                $scope.temp.job_status = true;

            }

            $scope.globalSearch = function () {
                debugger;
                $rootScope.visible_stud = false;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = false;
                $rootScope.visible_Employee = true;
                $rootScope.chkMulti = false;

                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                $scope.temp.em_login_code = '';
                $scope.temp.EmpName = '';
                $scope.showBtn = false;
            }

            $scope.$on('global_cancel', function (str) {
                console.log($scope.SelectedUserLst);
                if ($scope.SelectedUserLst.length > 0) {
                    $scope.temp['enroll_number'] = $scope.SelectedUserLst[0].em_login_code;
                    $scope.temp['name'] = $scope.SelectedUserLst[0].empName;
                    $scope.EmpSearch1($scope.SelectedUserLst[0].em_login_code)
                }
            });

            $scope.searchGlobalClick = function () {
                $scope.Global_Search_by_employee();
            }

            var datasend = [];
            $scope.savedata = function (Myform) {
                if (Myform) {
                   
                    var data = $scope.temp;
                    data.opr = "I";
                    datasend.push(data);

                        
                    $http.post(ENV.apiUrl + "api/EmpExp/CUDEmployeeExperience", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                        }
                        else {
                            swal({ text: "Record Not Inserted", imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                        }

                        $scope.getGrid();
                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                    
                }
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.desg_company_code = "";
                $scope.temp.desg_desc = "";
                $scope.temp.desg_communication_status = "";

            }

            $scope.edit = function (str) {
                debugger;
                $http.get(ENV.apiUrl + "api/EmpExp/getMonthName").then(function (mon) {
                    $scope.month_data = mon.data;
                });
                $scope.searchBtn = true;
                //$scope.searchText = true;
                $scope.readonly = true;
                $scope.disabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
               
                var from_year1 = str.em_previous_job_start_year_month;
                var from_year2 = from_year1.slice(0, 4);

                var to_year1 = str.em_previous_job_end_year_month;
                var to_year2 = to_year1.slice(0, 4);
              

                if (str.em_previous_job_start_year_month.length == '5') {
                    var from_month1 = str.em_previous_job_start_year_month;
                    var from_month2 = from_month1.substr(4,5);
                }
                else {
                    var from_month1 = str.em_previous_job_start_year_month;
                    var from_month2 = from_month1.substr(4,6);
                }

                if (str.em_previous_job_end_year_month.length == '5') {
                    var to_month1 = str.em_previous_job_end_year_month;
                    var to_month2 = to_month1.substr(4, 5);
                }
                else {
                    var to_month1 = str.em_previous_job_end_year_month;
                    var to_month2 = to_month1.substr(4, 6);
                }

                $scope.temp = {
                    enroll_number: str.em_login_code,
                    co_company_code: str.em_company_code,
                    co_desc:str.company_name,
                    prev_company_name:str.em_previous_job_company, 
                    prev_job_title:str.em_previous_job_title,
                    from_year:from_year2,
                    from_month: from_month2,
                    to_year:to_year2,
                    to_month: to_month2,
                    prev_job_remark:str.em_previous_job_remark, 
                    prev_job_responsibilities:str.em_previous_job_responsibilities,
                    prev_job_salary:str.em_previous_job_salary_details, 
                    job_status: str.em_previous_job_status,
                    em_previous_job_line_no: str.em_previous_job_line_no
                }


            }

            
        

            var dataupdate = [];
            $scope.update = function (Myform) {
                if (Myform) {
                    var Udata = $scope.temp;
                    Udata.opr = "U";
                    dataupdate.push(Udata);

                    $http.post(ENV.apiUrl + "api/EmpExp/CUDEmployeeExperience", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                        }
                        else {
                            swal({ text: "Record Not Updated", imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                        }
                        $scope.getGrid();

                    });
                    dataupdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].em_login_code+ i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].em_login_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].em_login_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'enroll_number': $scope.filteredTodos[i].em_login_code,
                            'co_company_code':$scope.filteredTodos[i].em_company_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/EmpExp/CUDEmployeeExperience?data=", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getGrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal({  text: "Record Not Deleted ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {

                                            $scope.getGrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].em_login_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }

        }])

})();
