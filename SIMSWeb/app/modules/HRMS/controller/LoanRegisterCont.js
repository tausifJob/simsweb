﻿(function () {
    'use strict';
    var opr = '';
    var Loanregcode = [];
    var main;

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('LoanRegisterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            
            $scope.table1 = true;
            $http.get(ENV.apiUrl + "api/LoanCodeRegister/getAllLoanRegister").then(function (getAllLoanRegister_Data) {
                $scope.AllLoanRegister_Data = getAllLoanRegister_Data.data;
                $scope.totalItems = $scope.AllLoanRegister_Data.length;
                $scope.todos = $scope.AllLoanRegister_Data;
                $scope.makeTodos();
             
            });
           

           
            $http.get(ENV.apiUrl + "api/common/getCompanyName").then(function (getCompanyName_Data) {
                $scope.CompanyName_Data = getCompanyName_Data.data;
                $scope.edt['lo_company_code'] = $scope.CompanyName_Data[0].pc_company_code;
               
                $scope.GetLoandesc();
                $scope.CheckEmployee(edt.lo_number);
            });

            $scope.GetLoandesc = function () {
                $http.get(ENV.apiUrl + "api/common/getAllLoanDesc?Company_code=" + $scope.edt.lo_company_code).then(function (getAllLoanDesc_Data) {
                    $scope.AllLoanDesc_Data = getAllLoanDesc_Data.data;
                   
                });

                $http.get(ENV.apiUrl + "api/common/getAllEmpName?Company_code=" + $scope.edt.lo_company_code).then(function (res) {
                    $scope.LoanEmpName_Data = res.data;
                 
                });
            }
            $scope.GetDesignation = function (code) {

              
                for(var i=0;i<$scope.LoanEmpName_Data.length;i++)
                {
                    if($scope.LoanEmpName_Data[i].lo_number ==code)
                    {
                       
                        $scope.temp = {


                            lo_gaunt_name:$scope.LoanEmpName_Data[i].em_first_name,
                            lo_gaunt_comp_name: $scope.LoanEmpName_Data[i].companyName,
                            lo_gaunt_address1: $scope.LoanEmpName_Data[i].lo_gaunt_address1,
                            lo_gaunt_desg: $scope.LoanEmpName_Data[i].designation
                           
                        };
                      
                    }
                }
                
            }
            $scope.employeedata = [];

            $scope.CheckEmployee=function(str)
            {
                $scope.employeedata = [];
                for(var i=0;i<$scope.LoanEmpName_Data.length;i++)
                {

                    if($scope.LoanEmpName_Data[i].lo_number!=str)
                    {
                        $scope.employeedata.push($scope.LoanEmpName_Data[i]);
                    }
                }

            }
            //$http.get(ENV.apiUrl + "api/common/getCreditDebit").then(function (getCreditDebit_Data) {
            //    $scope.CreditDebit_Data = getCreditDebit_Data.data;
            //});

            $scope.operation = false;
            $scope.editmode = false;

            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
               
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.AllLoanRegister_Data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

                //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
            }
            $scope.edt = "";

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.editmode = false;
                opr = 'S';
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = {};
               
                    $("#cmb_Ledger_Code").select2();
                    $("#cmb_Guarantor_No").select2();
               
                $scope.edt = {
                    lo_sanction_date: dd + '-' + mm + '-' + yyyy
                }
                $scope.edt.lo_mgr_confirm_tag = true;
                $scope.edt['lo_company_code'] = $scope.CompanyName_Data[0].pc_company_code;
                $scope.GetLoandesc();
                $scope.CheckEmployee(edt.lo_number);

               
            }


            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;

            
            $scope.edt = {
            lo_sanction_date: dd + '-' + mm + '-' + yyyy
            }


            $scope.up = function (str, str1) {
                opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.operation = true;

                $scope.edt = str;
                $scope.temp = str1;

                //$scope.getroutecodename(str.sims_academic_year);
                //$scope.getdirection(str.sims_transport_route_code, str.sims_academic_year);
              //  $scope.CheckEmployee(str.lo_number);
                $scope.GetLoandesc();
                $scope.CheckEmployee(str.lo_number);
                $scope.GetDesignation(str.lo_gaunt_no);

                
                //$("#cmb_Ledger_Code").select2("val", str.employeeName);
                $("#cmb_Ledger_Code").select2('destroy');
                $("#cmb_Guarantor_No").select2('destroy');
                $scope.str1.lo_gaunt_address1;
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
            }

            $scope.Save = function (myForm) {

             

                if (myForm) {
     
                    var data = {
                        lo_number: $scope.edt.lo_number,
                        lo_company_code: $scope.edt.lo_company_code,
                        employeeName: $scope.edt.employeeName,
                        lo_code: $scope.edt.lo_code,
                        lo_sanction_date: $scope.edt.lo_sanction_date,
                        lo_amount: $scope.edt.lo_amount,
                        lo_no_of_installment: $scope.edt.lo_no_of_installment,
                        lo_paid_amount: $scope.edt.lo_paid_amount,
                        lo_reference: $scope.edt.lo_reference,
                        lo_repaid_installment: $scope.edt.lo_repaid_installment,

                        lo_gaunt_no: $scope.edt.lo_gaunt_no,
                        lo_gaunt_name: $scope.temp.lo_gaunt_name,
                        lo_gaunt_comp_name: $scope.temp.lo_gaunt_comp_name,
                        lo_gaunt_address1: $scope.temp.lo_gaunt_address1,
                        lo_gaunt_desg: $scope.temp.lo_gaunt_desg,
                        lo_gaunt_address2: $scope.edt.lo_gaunt_address2,
                        lo_gaunt_address3: $scope.edt.lo_gaunt_address3,

                        opr: 'I'
                    
                    };

                    $http.post(ENV.apiUrl + "api/LoanCodeRegister/LoanCodeRegisterCUD", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                      
                            $scope.operation = false;

                    $http.get(ENV.apiUrl + "api/LoanCodeRegister/getAllLoanRegister").then(function (getAllLoanRegister_Data) {
                                $scope.AllLoanRegister_Data = getAllLoanRegister_Data.data;
                                $scope.totalItems = $scope.AllLoanRegister_Data.length;
                                $scope.todos = $scope.AllLoanRegister_Data;
                                $scope.makeTodos();
                                $scope.msg1 = msg.data;

                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 })
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 })
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }

                                //if ($scope.msg1 == true) {

                                //    $rootScope.strMessage = 'Record Inserted Successfully';

                                //    $('#message').modal('show');
                                //}
                                //else {

                                //    $rootScope.strMessage = 'Record not Inserted';

                                //    $('#message').modal('show');
                                //}

                            });

                        });
                    //}
                    $scope.table1 = true;
                    $scope.operation = false;
                }

            }

            $scope.Update = function () {
                
                var data = {
                    lo_number: $scope.edt.lo_number,
                    lo_company_code: $scope.edt.lo_company_code,
                    employeeName: $scope.edt.employeeName,
                    lo_code: $scope.edt.lo_code,
                    lo_sanction_date: $scope.edt.lo_sanction_date,
                    lo_amount: $scope.edt.lo_amount,
                    lo_no_of_installment: $scope.edt.lo_no_of_installment,
                    lo_paid_amount: $scope.edt.lo_paid_amount,
                    lo_reference: $scope.edt.lo_reference,
                    lo_repaid_installment: $scope.edt.lo_repaid_installment,

                    lo_gaunt_no: $scope.edt.lo_gaunt_no,
                    lo_gaunt_name: $scope.temp.lo_gaunt_name,
                    lo_gaunt_comp_name: $scope.temp.lo_gaunt_comp_name,
                    lo_gaunt_address1: $scope.temp.lo_gaunt_address1,
                    lo_gaunt_desg: $scope.temp.lo_gaunt_desg,
                    lo_gaunt_address2: $scope.edt.lo_gaunt_address2,
                    lo_gaunt_address3: $scope.edt.lo_gaunt_address3,

                    opr: 'U'

                };

                $http.post(ENV.apiUrl + "api/LoanCodeRegister/LoanCodeRegisterCUD", data).then(function (msg) {
                    $scope.msg1 = msg.data;

                    $scope.operation = false;
                    $http.get(ENV.apiUrl + "api/LoanCodeRegister/getAllLoanRegister").then(function (getAllLoanRegister_Data) {
                        $scope.AllLoanRegister_Data = getAllLoanRegister_Data.data;
                        $scope.totalItems = $scope.AllLoanRegister_Data.length;
                        $scope.todos = $scope.AllLoanRegister_Data;
                        $scope.makeTodos();
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 })
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 })
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        //if ($scope.msg1 == true) {

                        //    $rootScope.strMessage = 'Record Updated Successfully';

                        //    $('#message').modal('show');
                        //}
                        //else {

                        //    $rootScope.strMessage = 'Record Not Updated';

                        //    $('#message').modal('show');
                        //}
                    });

                })


                $scope.operation = false;
                $scope.table1 = true;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

              
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].lo_number;
                        var v = document.getElementById(t);
                        v.checked = true;
                        Loanregcode = Loanregcode + $scope.filteredTodos[i].lo_number + ','
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].lo_number;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
            }

            $scope.deleterecord = function () {

                Loanregcode = [];

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].lo_number;
                    var v = document.getElementById(t);

                    if (v.checked == true)

                        Loanregcode = Loanregcode + $scope.filteredTodos[i].lo_number + ',';
                }

                var deletelocacode = ({
                    'lo_number': Loanregcode,
                    'opr': 'D'
                });

                $http.post(ENV.apiUrl + "api/LoanCodeRegister/LoanCodeRegisterCUD", deletelocacode).then(function (msg) {
                    $scope.msg1 = msg.data;
                    $scope.table1 = true;
                    $scope.operation = false;
                    $rootScope.strMessage = $scope.msg1.strMessage;

                    $http.get(ENV.apiUrl + "api/LoanCodeRegister/getAllLoanRegister").then(function (getAllLoanRegister_Data) {
                        $scope.AllLoanRegister_Data = getAllLoanRegister_Data.data;
                        $scope.totalItems = $scope.AllLoanRegister_Data.length;
                        $scope.todos = $scope.AllLoanRegister_Data;
                        $scope.makeTodos();
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 })
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 })
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        //if ($scope.msg1 == true) {

                        //    $rootScope.strMessage = 'Record Deleted Successfully';

                        //    $('#message').modal('show');
                        //}
                        //else {


                        //    $rootScope.strMessage = 'Record Not Deleted';

                        //    $('#message').modal('show');
                        //}

                    });
                });

            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.AllLoanRegister_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.AllLoanRegister_Data;
                }
                $scope.makeTodos();
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                     item.lo_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.companyName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.employeeName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                      item.loanName == toSearch ||
                     item.lo_amount == toSearch) ? true : false;
            }


        }])
})();
