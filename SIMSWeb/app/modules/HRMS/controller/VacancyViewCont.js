﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('VacancyViewCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $http.get(ENV.apiUrl + "api/VacancyMst/Getdesignation12").then(function (result) {
                $scope.designation = result.data;
                
               // $scope.temp["dg_code"] = $scope.designation[0].dg_code;
            });

            $scope.searchable = function (date, desig, e_year)
            {
               
                if (date != undefined && desig != undefined && e_year != undefined) {
                    
                    $http.get(ENV.apiUrl + "api/VacancyMst/getAllvacancy?frmdate=" + date + "&desig=" + desig + "&experience=" + e_year).then(function (result) {
                        
                        $scope.allData = result.data;
                        
                    });
                }
                else {
                    
                    swal({ title: "Alert", text: "Please Select Date,Designation,Experience", showCloseButton: true, width: 380 });
                }

            }

            $scope.downloaddocfile = function (str) {
                
                $scope.url = 'http://api.mograsys.com/apierp/Content/qfis/Docs/Vacancy/' + str;
                window.open($scope.url);
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });


        }])

})();
