﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AirFareEmployeeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.grid = true;
            $scope.edit_data = false;
            $scope.AirFareEmp_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.edit_code = false;
            $scope.filter_combo = true;

            $scope.empnum_Readonly = false;
            $scope.nation_Readonly = false;
            $scope.grade_Readonly = false;
            $scope.class_Readonly = false;
          
            $scope.countData = [
             { val: 5, data: 5 },
             { val: 10, data: 10 },
             { val: 15, data: 15 },

            ]

            $scope.size = function (str) {
                //if (str == 5 || str == 10 || str == 15) {
                //    $scope.pager = true;
                //}
                //else {
                //    $scope.pager = false;
                //}
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $timeout(function () {
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);


            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

           //$http.get(ENV.apiUrl + "api/AirFareEmp/getAllAirFareEmployee").then(function (res) {
           //     $scope.display = false;
           //     $scope.grid = true;
           //     $scope.AirFareEmp_data = res.data;
           //     $scope.totalItems = $scope.AirFareEmp_data.length;
           //     $scope.todos = $scope.AirFareEmp_data;
           //     $scope.makeTodos();
           //     $scope.grid = true;
           // });

           $http.get(ENV.apiUrl + "api/AirFareEmp/getAllAirFareEmployee").then(function (res) {
               $scope.grid = true;
                $scope.AirFareEmp_data = res.data;
                if ($scope.AirFareEmp_data.length > 0) {
                    $scope.pager = true;

                    if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.AirFareEmp_data.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.AirFareEmp_data.length, data: 'All' })
                    }

                    $scope.totalItems = $scope.AirFareEmp_data.length;
                    $scope.todos = $scope.AirFareEmp_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                    $scope.page_index = true;
                    $scope.currentPage = 1;
                }
                else {
                    swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                    $scope.page_index = false;
                    $scope.pager = false;
                    $scope.filteredTodos = [];
                }
            });
          

            $http.get(ENV.apiUrl + "api/AirFareEmp/getDestinationAirFare").then(function (res) {
                $scope.Destination_data = res.data;
                console.log($scope.Destination_data);
            });

            $http.get(ENV.apiUrl + "api/AirFareEmp/getNations").then(function (res) {
                $scope.Nation_data = res.data;
                console.log($scope.Nation_data);
            });

            $http.get(ENV.apiUrl + "api/AirFareEmp/getGrades").then(function (res) {
                $scope.Grade_data = res.data;
                console.log($scope.Nation_data);
            });

            $http.get(ENV.apiUrl + "api/AirFareEmp/getClass").then(function (res) {
                $scope.Class_data = res.data;
                console.log($scope.Class_data);
            });

            $scope.show_Airfare_Emp_data = function (gr_code,na_code,ds_code,pays_class,em_number) {

                $http.get(ENV.apiUrl + "api/AirFareEmp/getAirFareEmployee?afe_grade_code=" + gr_code + "&afe_dest_code=" + ds_code +
                    "&afe_nation_code=" + na_code + "&afe_class=" + pays_class + "&afe_em_number="+em_number).then(function (res) {
                    
                    $scope.AirFareEmp_data = res.data;

                    if ($scope.AirFareEmp_data.length == 0) {
                        swal({ text: "Record not found", showCloseButton: true, width: 380, })
                        $scope.grid = false;
                    }
                    else {
                        $scope.display = false;
                        $scope.grid = true;
                        $scope.totalItems = $scope.AirFareEmp_data.length;
                        $scope.todos = $scope.AirFareEmp_data;
                        $scope.makeTodos();
                        $scope.grid = true;
                    }
                });
            }

            $scope.clear = function () {
                $scope.edt = '';
                $scope.getgrid();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
            };

            $scope.edit = function (str) {
                $scope.empnum_Readonly = true;
                $scope.nation_Readonly = true;
                $scope.grade_Readonly = true;
                $scope.class_Readonly = true;

                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edit_data = true;
                $scope.edit_code = true;
                $scope.filter_combo = false;
                // $scope.edt = str;
                debugger;
                $scope.edt =
                    {
                        afe_em_number:str.afe_em_number,
                        afe_dest_code: str.afe_dest_code,
                        ds_name:str.ds_name,
                        afe_nation_code: str.afe_nation_code,
                        sims_country_name_en:str.sims_country_name_en,
                        afe_grade_code: str.afe_grade_code,
                        gr_desc:str.gr_desc,
                        afe_class: str.afe_class,
                        afe_class_name:str.afe_class_name,
                        afe_adult_rate: str.afe_adult_rate,
                        afe_child_rate: str.afe_child_rate,
                        afe_infant_rate: str.afe_infant_rate,
                        afe_finalize_status: str.afe_finalize_status
                    }
            }

            $scope.Save = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        data1 = [];
                        var data = ({
                            af_dest_code: $scope.edt.af_dest_code,
                            gr_code: $scope.edt.gr_code,
                            af_nation_code: $scope.edt.af_nation_code,
                            af_class: $scope.edt.af_class,
                            af_applicable_frequency: $scope.edt.af_applicable_frequency,
                            af_adult_rate: $scope.edt.af_adult_rate,
                            af_child_rate: $scope.edt.af_child_rate,
                            af_infant_rate: $scope.edt.af_infant_rate,
                            af_applicable_month: $scope.edt.af_applicable_month,
                            af_payroll_flag: $scope.edt.af_payroll_flag,
                            af_gl_acct_no: $scope.edt.af_gl_acct_no,
                            af_pay_code: $scope.edt.af_pay_code,
                            opr: 'I'
                        });

                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/common/AirFare/CUDAirFareDetails", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            console.log($scope.msg1);
                            if ($scope.msg1 == true) {
                                swal({ text: "Airfare Added Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Airfare Data Already Exist.", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/AirFareEmp/getAllAirFareEmployee").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.AirFareEmp_data = res.data;
                    $scope.totalItems = $scope.AirFareEmp_data.length;
                    $scope.todos = $scope.AirFareEmp_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                });

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {
                debugger;
                var data1 = [];
                if (isvalidate) {

                    var data = ({
                        afe_em_number:$scope.edt.afe_em_number,
                        afe_dest_code: $scope.edt.afe_dest_code,
                        ds_name: $scope.edt.ds_name,
                        afe_nation_code: $scope.edt.afe_nation_code,
                        sims_country_name_en: $scope.edt.sims_country_name_en,
                        afe_grade_code: $scope.edt.afe_grade_code,
                        gr_desc: $scope.edt.gr_desc,
                        afe_class: $scope.edt.afe_class,
                        afe_class_name: $scope.edt.afe_class_name,
                        afe_adult_rate: $scope.edt.afe_adult_rate,
                        afe_child_rate: $scope.edt.afe_child_rate,
                        afe_infant_rate: $scope.edt.afe_infant_rate,
                        afe_finalize_status: $scope.edt.afe_finalize_status,
                        opr: 'U'
                    });

                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/AirFareEmp/CUAirFareEmp", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Employee Airfare Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                    $scope.filter_combo = true;
                                    $scope.edt = '';
                                }
                            });

                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Employee Airfare Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                    $scope.filter_combo = true;
                                    $scope.edt = '';
                                }
                            });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.filter_combo = true;
                $scope.edt = '';
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.AirFareEmp_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.AirFareEmp_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.afe_em_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.gr_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.ds_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    || item.sims_country_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.size = function (str) {
                //debugger;
                //if (str == 'All') {
                //    $scope.filteredTodos = $scope.AirFareEmp_data;
                //    $scope.numPerPage = $scope.AirFareEmp_data.length,
                //    $scope.maxSize = $scope.AirFareEmp_data.length;
                //    $scope.totalItems = $scope.AirFareEmp_data.length;
                //    $scope.todos = $scope.AirFareEmp_data;
                //    $scope.makeTodos();
                //}

                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                //console.log("numPerPage=" + $scope.numPerPage);
                //$scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['check_all'] = false;
                $scope.row1 = '';
                main.checked = false;
            }

        }])
})();