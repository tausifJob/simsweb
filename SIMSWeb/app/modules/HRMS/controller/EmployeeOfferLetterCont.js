﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('EmployeeOfferLetterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.paysgrade_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.edit_code = false;
            $scope.display = false;
            $scope.grid = true;
            console.log("EmployeeOfferLetterCont called");
            $scope.temp = {};
            $scope.emailId_data = [];
            $scope.email = {};
            $scope.edt = {};
            $scope.filenameassigningData = [];
            $scope.showTable = false;

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/Get_designation_Code?company_code=1" ).then(function (Designation) {
                $scope.designation = Designation.data;
            });

            $http.get(ENV.apiUrl + "api/employeeOfferLetter/getVacancy").then(function (vacancy) {
                $scope.vacancys = vacancy.data;
            });

            $http.get(ENV.apiUrl + "api/HRMS/emp/getEmpMasterData").then(function (res) {                
                $scope.obj = res.data;
            });

            $scope.Show_Data = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/employeeOfferLetter/getEmployeeApplicationDetails?pays_vacancy_id=" + $scope.temp.pays_vacancy_id + "&app_from_date=" + $scope.temp.app_from_date + "&app_to_date=" + $scope.temp.app_to_date).then(function (res) {
                    $scope.employeeApplicationData = res.data;
                    console.log("employeeApplicationData",$scope.employeeApplicationData);
                    if ($scope.employeeApplicationData.length > 0) {
                        $scope.pager = true;
                        
                        $scope.totalItems = $scope.employeeApplicationData.length;
                        $scope.todos = $scope.employeeApplicationData;
                        $scope.makeTodos();
                        $scope.showTable = true;
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                        $scope.filteredTodos = [];
                    }
                });
            };
            
            $scope.setProbationCompletionReminderDate = function () {
               debugger;
               if ($scope.temp.em_expected_date_of_join != undefined) {
                    console.log($scope.temp.em_expected_date_of_join);
                    var ProbationCompletionReminderDate = moment($scope.temp.em_expected_date_of_join, "DD-MM-YYYY");
                    console.log(ProbationCompletionReminderDate);
                    $scope.temp.em_probation_completion_date = ProbationCompletionReminderDate.add(parseInt($scope.temp.em_probation_period_days), "days").format("DD-MM-YYYY");
                    //startdate = startdate.format("DD-MM-YYYY");
                    console.log($scope.temp.em_probation_completion_date);
                    console.log("em_probation_completion_date", $scope.temp.em_probation_completion_date);
                    
                }
            };

            $scope.savePreparedInfo = function () {
                debugger;
                var data = $scope.temp;               
                data.opr = 'U';
                
                $http.post(ENV.apiUrl + "api/employeeOfferLetter/OfferLetterUpdate", data).then(function (res) {                                        
                    if (res.data == true) {
                        swal({ text: "Records save successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                    }
                    else if (res.data == false) {
                        swal({ text: "Records not save.", showCloseButton: true, width: 380, });
                    }
                    else{
                        swal({ text: res.data.Message, showCloseButton: true, width: 380, });
                    }
                    
                });
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

  
            $scope.showOfferLetter = function (obj) {
                debugger
                $('#viewdashDetailsModal').modal({ backdrop: 'static', keyboard: true });
                $scope.div_Communication = true;
                $scope.display = true;
                $scope.emailId_data = [];
                $scope.email.em_applicant_id = obj.em_applicant_id;
                $scope.email.dg_desc = obj.dg_desc;
                $scope.emailId_data.push({ 'emailid': obj.em_email,chk_email: true,'em_applicant_id':obj.em_applicant_id});                
                $scope.gettemplateBody();
            }

            $('#text-editor').wysihtml5();

            $scope.gettemplateBody = function () {
                $http.get(ENV.apiUrl + "api/employeeOfferLetter/getOfferLetterTemplate").then(function (res) {
                    $scope.tempBody_data = res.data;
                    debugger
                    
                    $scope.email['email_subject'] = $scope.tempBody_data.sims_msg_subject;
                    $scope.sims_msg_signature = $scope.tempBody_data.sims_msg_signature;

                    var body = $scope.tempBody_data.sims_msg_body;
                    var v = document.getElementById('text-editor');
                    v.value = body;
                    $scope.email.msgbody = v.value;
                    $scope.flag = false;
                    console.log($scope.tempBody_data.sims_msg_sr_no);
                    $('#text-editor').data("wysihtml5").editor.setValue($scope.email.msgbody);

                    //$http.get(ENV.apiUrl + "api/common/Email/GetcheckEmailProfile?sr_no=" + $scope.tempBody_data.sims_msg_sr_no).then(function (res) {
                    //    $scope.emailProfile_data = res.data;
                    //    console.log($scope.emailProfile_data);
                    //});
                });
            }

          
            /* This function not in used but dont delete */
            $scope.sendMail = function () {

                var data1 = [];
                var lst_cc = [];

                $scope.email_exists = false;

                    $http.get(ENV.apiUrl + "api/employeeOfferLetter/getOfferLetterTemplate").then(function (res) {
                        $scope.TemplatesBody_data = res.data;

                        $scope.sims_msg_body = $scope.TemplatesBody_data.sims_msg_body;
                        $scope.sims_msg_signature = $scope.TemplatesBody_data.sims_msg_signature;

                        var msgbody = $('#text-editor').val();
                        // console.log(msgbody);

                        for (var i = 0; i < $scope.emailId_data.length; i++) {
                            
                            if ($scope.emailId_data[i].chk_email == true) {
                             
                                var data =
                                   ({
                                       emailsendto: $scope.emailId_data[i].emailid,
                                       body: msgbody,
                                       subject: $scope.email.email_subject,                                       
                                       comm_desc: msgbody,                                      
                                       sender_emailid: $scope.emailProfile_data,
                                       em_applicant_id: $scope.email.em_applicant_id,
                                       dg_desc:$scope.email.dg_desc,
                                       cc_id: '0'
                                   });

                                data1.push(data);

                                $scope.email_exists = true;
                                //console.log(data1);
                            }
                        }

                        if (($scope.email.ccTo == "" || $scope.email.ccTo == undefined) && ($scope.email_exists == false)) {
                            swal({ title: "Alert", text: "Unable to Sent Mail", showCloseButton: true, width: 380, });
                            return;
                        }

                        if ($scope.email.ccTo != "" || $scope.email.ccTo != undefined) {
                            var v = [];
                            var s = $scope.email.ccTo;
                            if (s != null) {
                                v = s.split(',');
                            }

                            for (var i = 0; i < v.length; i++) {
                                var data =
                                  ({
                                      emailsendto: v[i],
                                      body: msgbody,
                                      subject: $scope.email.email_subject,                                      
                                      comm_desc: msgbody,
                                      sender_emailid: $scope.emailProfile_data,
                                      em_applicant_id: $scope.email.em_applicant_id,
                                      dg_desc: $scope.email.dg_desc,
                                      cc_id: '1'
                                  });

                                data1.push(data);
                            }

                            var data2 =
                                {
                                    attFilename: ''
                                }
                            lst_cc.push(data2);
                        }

                        console.log("data1", data1);
                        console.log("lst_cc", lst_cc);

                        $http.post(ENV.apiUrl + "api/employeeOfferLetter/SendOfferEmail?filenames=" + JSON.stringify(lst_cc), data1).then(function (res) {
                            $scope.ScheduleMails_data = res.data;

                            if ($scope.ScheduleMails_data == true) {
                                swal({ text: "The email has been sent successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                            }
                            else {
                                swal({ text: "The email not sent.", showCloseButton: true, width: 380, });
                            }
                            $scope.commnModaldisplay();
                        });
                        
                    });
                
            }

            /* above function not in used but dont delete */


            $scope.commnModaldisplay = function () {
                $scope.email = {};
                $scope.emailId_data = [];                
                $('#viewdashDetailsModal').modal('hide');
                //$('#text-editor').data("wysihtml5").editor.clear();
                $scope.div_Communication = false;
            }

            $scope.CancelEmailId = function (indx) {
                console.log(indx);
                $scope.emailId_data.splice(indx, 1);
                console.log($scope.emailId_data.splice(indx, 1));
            }

            $scope.reset = function () {
                $scope.temp = {};
            }

            $scope.CancelPrepareModal = function () {
                $('#showPrepareModal').modal('hide');
                $scope.temp = {};
            }

            $scope.UploadFileModal = function (str) {
                $scope.filenameassigningData = str;
                $('#myModal').modal('show');
                $scope.temp.em_applicant_id = str.em_applicant_id;
            }
            var formdata = new FormData();

            $scope.hideModal = function () {
                debugger;
                $scope.showFileName = true;

                $scope.filename1 = $scope.filename;
                if ($scope.filename1 == undefined || $scope.filename1 == "" || $scope.filename1 == null) {
                    $scope.showFileName = false;
                }

                $scope.filenameassigningData.offer_letter_doc = $scope.filename1;
                $('#myModal').modal('hide');

                $scope.file_name = $scope.temp.em_applicant_id +'_'+ $scope.filename1;


                    var data = {};
                    data.opr = 'E';
                    data.offer_letter_doc = $scope.file_name;
                    data.em_applicant_id = $scope.temp.em_applicant_id;

                    console.log("file_name", $scope.file_name);
                    console.log("data", data);

                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + '/api/EmpDocumentupload/upload?filename=' + $scope.file_name + "&location=" + "EmployeeDocument",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    $http(request)
                      .success(function (d) { 
                          $http.post(ENV.apiUrl + "api/employeeOfferLetter/OfferLetterUpdate", data).then(function (res) {
                              if (res.data == true) {
                                  swal({ text: "Offer letter save successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                              }
                              else if (res.data == false) {
                                  swal({ text: "Offer letter not save.", showCloseButton: true, width: 380, });
                              }
                              else {
                                  swal({ text: res.data.Message, showCloseButton: true, width: 380, });
                              }
                              $scope.Show_Data();
                          });
                      },
                    function () {
                        //alert("Err");
                        swal({ text: "Offer letter not uploded.", showCloseButton: true, width: 380, });
                    });

            };

            $scope.getTheFiles = function ($files) {

                //FileList[0].File.name = $scope.filename + '.' + fortype;
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);

                });
            };

            var fortype = '';
            $scope.file_changed = function (element) {
                debugger;
                var photofile = element.files[0];
                  var fortype = photofile.type.split("/")[1];

                  if (fortype == 'pdf') {
                  
                        //if (photofile.size > 200000) {
                        //    swal('', 'File size limit not exceed upto 200kb.')
                        //}
                     //    else {

                       $scope.filename = photofile.name;
                       $scope.edt= {employeeDocument:$scope.filename};
                       $.extend($scope.edt, $scope.edt)
                       //$scope.photo_filename = (photofile.type);

                       $scope.photo_filename = (photofile.name);

                       var len = 0;
                       len = $scope.photo_filename.split('.');
                       fortype = $scope.photo_filename.split('.')[len.length - 1];
                       // $scope.edt1 = { sims_timetable_filename: $scope.photo_filename }
                       // $.extend($scope.edt, $scope.edt1)
                       $scope.photo_filename = (photofile.type);

                       //element.files[0].FileList.File.name = $scope.filename + '.' + fortype;
                       var reader = new FileReader();
                       reader.onload = function (e) {
                           $scope.$apply(function () {
                               // $scope.prev_img = e.target.result;


                           });
                       };
                       reader.readAsDataURL(photofile);

                       //    }
                    }
                    else {
                      swal({ title: 'Alert', text: 'Only PDF format is allowed.'});
                    }
            };

            $scope.showPrepareModal = function (obj) {
                debugger;
                $scope.temp.em_desg_code = obj.em_desg_code;
                $scope.temp.em_staff_type = obj.em_staff_type;
                $scope.temp.em_dest_code = obj.em_dest_code;
                $scope.temp.em_grade_code = obj.em_grade_code;
                $scope.temp.em_visa_type = obj.em_visa_type;
                $scope.temp.em_agreement_start_date = obj.em_agreement_start_date;
                $scope.temp.em_agreement_exp_date = obj.em_agreement_exp_date;
                $scope.temp.en_labour_card_no = obj.en_labour_card_no;
                $scope.temp.em_applicant_id = obj.em_applicant_id;
                $scope.temp.em_expected_date_of_join = obj.em_expected_date_of_join;
                $scope.temp.em_probation_period_days = obj.em_probation_period_days;
                $scope.temp.em_probation_completion_date = obj.em_probation_completion_date;
                $scope.temp.em_probation_confirmation_date = obj.em_probation_confirmation_date;
                console.log(obj.em_probation_period_days);
                console.log(obj.em_probation_completion_date);
                console.log(obj.em_probation_confirmation_date);


                $('#showPrepareModal').modal({ backdrop: 'static', keyboard: true });
            }

            $scope.SendEmail = function (obj) {
                var data1 = [];

                if (obj.offer_letter_doc == undefined || obj.offer_letter_doc == "" || obj.offer_letter_doc == null) {
                    swal({ title: 'Alert', text: "Please Upload Offer Letter", showCloseButton: true, width: 380, });
                }
                else {

                    var offer_letter_path =   obj.offer_letter_doc;
                    var data =
                            ({
                                emailsendto: obj.em_email,
                                //body: msgbody,
                                //subject: $scope.email.email_subject,
                                //comm_desc: msgbody,
                                //sender_emailid: $scope.emailProfile_data,
                                em_applicant_id: obj.em_applicant_id,
                                dg_desc: $scope.email.dg_desc,
                                cc_id: '0',
                                offer_letter_path: offer_letter_path
                            });

                    data1.push(data);
                    console.log(data);

                    $http.post(ENV.apiUrl + "api/employeeOfferLetter/SendOfferLetterEmail", data1).then(function (res) {
                        $scope.ScheduleMails_data = res.data;

                        if ($scope.ScheduleMails_data == true) {
                            swal({ text: "Email has been sent successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                        }
                        else {
                            swal({ text: "Email not sent.", showCloseButton: true, width: 380});
                        }
                        $scope.Show_Data();
                        $scope.commnModaldisplay();
                    });
                }

            }

            $scope.downloaddoc = function (str) {
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeDocument/' + str;
                window.open($scope.url);
            }

            $scope.showOnRecruitment = function (obj) {
                var data = {};
                data.opr = 'G';
                data.em_offer_letter_show_flag = obj.em_offer_letter_show_flag;
                data.em_applicant_id = obj.em_applicant_id;

                console.log("data", data);

                $http.post(ENV.apiUrl + "api/employeeOfferLetter/OfferLetterUpdate", data).then(function (res) {
                    if (res.data == true){                        
                        if(obj.em_offer_letter_show_flag == true){
                            swal({ text: "Now offer letter will be shown in recruitment dashboard.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380});
                        }                        
                    }
                    else if (res.data == false) {
                        swal({ text: "Not updated.", showCloseButton: true, width: 380, });
                    }
                    else {
                        swal({ text: res.data.Message, showCloseButton: true, width: 380, });
                    }
                    $scope.Show_Data();
                });
            }

            $scope.PrintOfferLetter = function () {
                debugger
                $('#showPrepareModal').modal('hide');
                var data = {
                    location: 'Hrms.PRR106',
                    parameter: {
                        app_id: $scope.temp.em_applicant_id
                    },
                    state: 'main.empoff',
                    ready: function () {
                        this.refreshReport();
                    },
                }                
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                setTimeout(function () {
                    $state.go('main.ReportCardParameter');
                }, 500);
               
            }

            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("test-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }
            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }
            //$scope.size = function (str) {
            //    if (str == 5 || str == 10 || str == 15) {
            //        $scope.pager = true;
            //    }
            //    else {
            //        $scope.pager = false;
            //    }
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.employeeApplicationData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.employeeApplicationData;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            //var curr_month_day = new Date (), 
            //    y = curr_month_day.getFullYear(), 
            //    m = curr_month_day.getMonth(),
            //    d = curr_month_day.getDate();
            //console.log(m) ;
            //var prev6_month_begin = new Date(y, m - 6).clearTime().moveToFirstDayOfMonth();
            //var prev6_month_end = new Date(y, m - 6).clearTime().moveToLastDayOfMonth(); 
            //console.log(prev6_month_begin) ;
            //console.log(prev6_month_end);
            //var timeDiff = Math.abs(curr_month_day.getTime() - prev6_month_begin.getTime());
            //var Dur_prev6_month_start = Math.ceil(timeDiff / (1000 * 3600 * 24));
            //console.log(Dur_prev6_month_start); 
            //var timeDiff = Math.abs(curr_month_day.getTime() - prev6_month_end.getTime());
            //var Dur_prev6_month_end = Math.ceil(timeDiff / (1000 * 3600 * 24));
            //console.log(Dur_prev6_month_end);

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.em_applicant_id.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.emp_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.pays_vacancy_roles.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.em_application_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

        }])
})();