﻿
(function () {
    'use strict';
    var CurrentDate;
    var day;
    var simsController = angular.module('sims.module.HRMS');
    simsController.controller('EmailEmployeePayslipCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = "1";
            var str;
            var date = new Date();
            $scope.cdetails_data = [];
            var main; $scope.edt = [];
            var payslip_data = [];
            $scope.showPager = true;
            $scope.edt = {};

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);
            //$scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss');
            
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            /*start_paging_gridview*/

            $http.post(ENV.apiUrl + "api/OvertimeHour/Year_month").then(function (Year_month) {
                $scope.Year_month = Year_month.data;
                var d = new Date().getFullYear();
                var n = new Date().getMonth() + 1;
                var ym = d + '' + n;
                $scope.edt.year_month = ym;
                $scope.setYearMonth();
            });

            $scope.setYearMonth = function () {
                $scope.edt.month = $scope.edt.year_month.substr(4);
                $scope.edt.year = $scope.edt.year_month.substr(0, 4);                
            }
            
            $http.get(ENV.apiUrl + "api/EmployeePayslip/getpaysyear").then(function (res) {
                $scope.ob_paysyear = res.data;               
            });

            $http.get(ENV.apiUrl + "api/EmployeePayslip/getpaysmonth").then(function (res) {
                $scope.ob_paysmonth = res.data;
            });

            $http.get(ENV.apiUrl + "api/generatepayroll/GetCurrentFinYearFromFinsParameter").then(function (GetCurrentFinYearFromFinsParameter) {

                $scope.GetCurrentFinYearFromFinsParameter1 = GetCurrentFinYearFromFinsParameter.data;
                $scope.fin_year = $scope.GetCurrentFinYearFromFinsParameter1[0].fin_year;
                $scope.fin_year_desc = $scope.GetCurrentFinYearFromFinsParameter1[0].fin_year_desc;
            });

            $scope.SearchEmployee = function () {
                //var data = str;
                debugger
                console.log($scope.edt.year);
                
                $http.get(ENV.apiUrl + "api/EmployeePayslip/getpaysemployee?year=" + $scope.fin_year + "&month=" + $scope.edt.month).then(function (res) {
                    $scope.grid = true
                    $scope.currentPage = 1;
                    $scope.cdetails_data = res.data;
                    $scope.totalItems = $scope.cdetails_data.length;
                    $scope.todos = $scope.cdetails_data;
                    $scope.makeTodos();
                });
            };
            
            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = 1;
                    $scope.filteredTodos = $scope.cdetails_data;
                    $scope.showPager = false;
                }
                else {
                    $scope.showPager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }                
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            } 

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.cdetails_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.cdetails_data;
                }
                $scope.makeTodos();

                //main = document.getElementById('mainchk');
                //if (main.checked == true) {
                //    main.checked = false;
                //    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                //        var v = document.getElementById(i);
                //        v.checked = false;
                //        main.checked = false;
                //        $scope.row1 = '';
                //    }
                //}
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.empid.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.empname.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }           
                                   
            $(document).on('keypress', function (e) {
                if (e.keyCode == 27) {
                    e.preventDefault();
                    $('#viewModal').modal('hide');
                    return false;
                }
            });

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('test-'+i);
                        v.checked = true;
                        $scope.filteredTodos[i].ifchecked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('test-' + i);
                        v.checked = false;
                        $scope.filteredTodos[i].ifchecked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.viewReport = function (str) {
                
            }


            $scope.printpayslip = function (str) {
                debugger;
                $http.get(ENV.apiUrl + "api/AllPayslipByUser/getpath").then(function (res) {
                    if (res.data.length > 0)
                        var rname = res.data[0].pays_appl_form_field_value1;

                    var data = {

                        location: rname,
                        parameter: {
                            paysheet_month: $scope.fin_year + $scope.edt.month,
                            comp_details: '1',
                            search: str.empid
                        },
                        state: 'main.payslp',
                        ready: function () {
                            this.refreshReport();
                        },
                    }

                    console.log(data);
                    window.localStorage["ReportDetails"] = JSON.stringify(data);
                    $state.go('main.ReportCardParameter')

                });

                //var data = {
                //    location: 'Payroll.PERR94',
                //    parameter: {
                //        paysheet_year: $scope.edt.year,
                //        paysheet_month: $scope.edt.month,
                //        search: str.empid,
                //    },                    
                //    state: 'main.payslp',
                //    ready: function () {
                //        this.refreshReport();
                //    },
                //}
                //window.localStorage["ReportDetails"] = JSON.stringify(data)
                //$state.go('main.ReportCardParameter')
            }

            $scope.SendMail = function () {
                debugger
                var emp_ids = '';
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].ifchecked == true) {

                        var empid = $scope.filteredTodos[i].empid;
                        emp_ids = emp_ids + ',' + empid;
                    }
                }
                console.log("emp_ids",emp_ids);

                $http.get(ENV.apiUrl + "api/EmployeePayslip/getsend_payslipby_email?year=" + $scope.fin_year + "&month=" + $scope.edt.month + "&empemailid=" + emp_ids).then(function (res) {
                    swal({ text: "Email Sent Successfully to Selected Employees.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });

                    $scope.filteredTodos = [];
                    $scope.pagesize = "10";
                    $scope.pageindex = "1";
                    var str;
                    var date = new Date();
                    $scope.cdetails_data = [];
                    
                    var payslip_data = [];
                    var main = document.getElementById('mainchk');
                    main.checked = false;
                    $scope.edt.year = '';
                    $scope.edt.month = '';
                    $scope.edt.year_month = '';
                    
                    $http.get(ENV.apiUrl + "api/EmployeePayslip/getpaysyear").then(function (res) {
                        $scope.ob_paysyear = res.data;
                    });

                    $http.get(ENV.apiUrl + "api/EmployeePayslip/getpaysmonth").then(function (res) {
                        $scope.ob_paysmonth = res.data;
                    });
                })                
            }
        }])
})();
