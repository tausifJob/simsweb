﻿(function () {
    'use strict';
    var Employee_code = [];

    var main, strMessage;
    var grade_code, SectionSubject5 = [], subject_code = [], subject_code1 = [];

    var cur_code;
    var section_code, sectioncodemodal1;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeTeacherCreationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = true;
            $scope.busyindicator = false;
            $scope.temp = {};
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.showForDPSMIS = false;

            $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2015&routeCode=01").then(function (AllComboBoxValues) {
                $scope.ComboBoxValues = AllComboBoxValues.data;
                $scope.temp['Company_code'] = $scope.ComboBoxValues[0].fins_comp_code;
                $scope.getdept($scope.temp['Company_code']);
                $scope.getdesg($scope.temp['Company_code']);
                $scope.getGrade($scope.temp['Company_code']);
            });

            var divisionFilter = ['dpsmis'];

            if (divisionFilter.includes($http.defaults.headers.common['schoolId'])) {
                $scope.showForDPSMIS = true;

                $http.get(ENV.apiUrl + "api/Dps/HRMS/getEmpData").then(function (res) {
                    $scope.obj1 = res.data;                    
                });
            }

            $scope.getdept = function (dept) {

                $http.get(ENV.apiUrl + "api/PayrollEmployee/Get_dept_Code?company_code=" + dept).then(function (Department) {
                    $scope.dept = Department.data;
                });
            }

            $scope.getdesg = function (design) {
                $http.get(ENV.apiUrl + "api/PayrollEmployee/Get_designation_Code?company_code=" + design).then(function (Designation) {
                    $scope.designation = Designation.data;
                });
            }

            $scope.getGrade = function (company_code) {
                $http.get(ENV.apiUrl + "api/generatepayroll/GetAllPaysGradeName?company_code=" + company_code).then(function (GetAllPaysGradeName) {
                    $scope.GetAllPaysGradeName = GetAllPaysGradeName.data;
                });
            }

            $scope.Show = function () {

                var data = $scope.temp;
                if ($scope.temp != undefined && $scope.temp != '') {

                    if ($scope.temp.Company_code != '' && $scope.temp.Company_code != undefined) {
                        $scope.busy = true;
                        $scope.table = false;
                        $http.get(ENV.apiUrl + "api/employeeteachermapping/Get_Mapped_Employee1?data=" + JSON.stringify($scope.temp)).then(function (GetMappedEmployee) {
                            $scope.GetMapped_Employee = GetMappedEmployee.data;
                            $scope.totalItems = $scope.GetMapped_Employee.length;
                            $scope.todos = $scope.GetMapped_Employee;
                            $scope.makeTodos();

                            if ($scope.GetMapped_Employee.length > 0) {
                                $scope.table = true;
                                $scope.busy = false;
                            }
                            else {
                                $scope.table = false;
                                $scope.busy = false;
                                swal({ text: 'Data Not Available', width: 380, showCloseButton: true });

                            }
                        });
                    }
                    else {
                        $scope.table = false;
                        $scope.busy = false;
                        swal({ text: 'Please Select Company Name', width: 380, showCloseButton: true });
                }
                
                }
                else {
                    $scope.table = false;
                    $scope.busy = false;
                    swal({ text: 'Please Select Company Name', width: 380, showCloseButton: true });
                }

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {                
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
            }

            $scope.Reset = function () {

                //$scope.temp = '';
                $scope.temp = {
                    dept_code: '',
                    desg_code: '',
                    grade_code: '',
                    em_login_code: ''
                }
            }

            $scope.Update = function () {
                debugger
                var flag = false;
                var Sdata = [{}];
                for (var i = 0; i < $scope.GetMapped_Employee.length; i++) {
                    if ($scope.GetMapped_Employee[i].ischange == true && $scope.GetMapped_Employee[i].sims_emp_status != false) {
                        flag = true;
                        var le = Sdata.length;
                        Sdata[le] = {
                            'sims_emp_name': $scope.GetMapped_Employee[i].sims_emp_name,
                            'sims_emp_code': $scope.GetMapped_Employee[i].sims_emp_code,
                            'sims_teacher_type': $scope.GetMapped_Employee[i].sims_teacher_type,
                            'sims_emp_ca': $scope.GetMapped_Employee[i].sims_emp_ca,
                            'sims_emp_cc': $scope.GetMapped_Employee[i].sims_emp_cc,
                            'sims_emp_status': $scope.GetMapped_Employee[i].sims_emp_status,
                            'opr': 'U'
                        }
                    }
                }

                if (flag != false) {
                    Sdata.splice(0, 1);
                    var data = Sdata;
                    $scope.busy = true;
                    $scope.table = false;
                    $http.post(ENV.apiUrl + "api/employeeteachermapping/CUDpdate_Mapped_Teacher_Employee?opr=U" + "&lstEmp=", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        swal({ text: $scope.msg1.strMessage, width: 380, showCloseButton: true });
                        $scope.busy = false;
                        $scope.Show();
                    })
                }
                else if (flag == false) {
                    for (var i = 0; i < $scope.GetMapped_Employee.length; i++) {
                        if ($scope.GetMapped_Employee[i].ischange == true && $scope.GetMapped_Employee[i].sims_emp_status == false) {
                            flag = true;
                            var le = Sdata.length;
                            Sdata[le] = {
                                'sims_emp_name': $scope.GetMapped_Employee[i].sims_emp_name,
                                'sims_emp_code': $scope.GetMapped_Employee[i].sims_emp_code,
                                'sims_teacher_type': $scope.GetMapped_Employee[i].sims_teacher_type,
                                'sims_emp_ca': $scope.GetMapped_Employee[i].sims_emp_ca,
                                'sims_emp_cc': $scope.GetMapped_Employee[i].sims_emp_cc,
                                'sims_emp_status': $scope.GetMapped_Employee[i].sims_emp_status,
                                'opr': 'U'
                            }
                        }
                    }
                    Sdata.splice(0, 1);
                    var data = Sdata;
                    $http.post(ENV.apiUrl + "api/employeeteachermapping/CUDpdate_Mapped_Teacher_Employee?opr=U" + "&lstEmp=", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1.strMessage == "Teacher Created Sucessfully") {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 380, showCloseButton: true });
                            $scope.busy = false;
                            $scope.Show();

                        }
                        else {

                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 380, showCloseButton: true });
                        }
                    })
                }
                if (flag == false) {

                    swal({ text: 'Please Check  Status Or Teacher Type To Assign Teacher', showCloseButton: true });
                }

            }

            $scope.Change = function (emp_no) {
             
                var v=document.getElementById(emp_no.sims_emp_code+emp_no.sims_emp_name)
                if (v.checked == true) {
                    emp_no.sims_emp_status = true;
                    emp_no.ischange = true;
                }
                else {
                    emp_no.sims_emp_status = false;
                    emp_no.ischange = true;
                }

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.GetMapped_Employee, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.GetMapped_Employee;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_emp_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_emp_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }


        }])
})();