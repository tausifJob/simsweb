﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('EmpListRptCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.temp = {};
            $scope.detailsField = true;
            $scope.hideprintpdf = false;
            $scope.main_div = false;
            $scope.summary_div = false;
         //   $scope.temp.sims_view_mode = false;




            $scope.pagesize = '30';
            $scope.pageindex = "0";
            $scope.pager = true;

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['chk_all'] = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 30, $scope.maxSize = 100;

            $scope.makeTodos = function () {
                debugger;
                if ($scope.pagesize == 'All') {
                    $scope.currentPage = 1;
                    $scope.showPager = false;
                    $scope.filteredTodos = $scope.report_data_new;
                }
                else {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                }
            };


            $http.get(ENV.apiUrl + "api/common/getCompanyName").then(function (getCompanyName_Data) {
                $scope.CompanyName_Data = getCompanyName_Data.data;
                $scope.temp.company_code = $scope.CompanyName_Data[0].pc_company_code;

                $http.get(ENV.apiUrl + "api/hrms/getDesignation?comp_code=" + $scope.temp.company_code).then(function (res1) {
                    debugger;
                    $scope.designation = res1.data;
                    $scope.temp.designation = $scope.designation[0].dg_code;
                    setTimeout(function () {
                        $('#designation').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });


                $http.get(ENV.apiUrl + "api/hrms/getEmpDept?comp_code=" + $scope.temp.company_code).then(function (res1) {
                    debugger;
                    $scope.department = res1.data;
                    $scope.temp.department = $scope.department[0].codp_dept_no;

                    setTimeout(function () {
                        $('#department').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });

               

            });


           





           
     
            $http.get(ENV.apiUrl + "api/hrms/getStaffService?opr="+'W').then(function (res1) {
                debugger;
                $scope.staff = res1.data;
                $scope.temp.staff = $scope.staff[0].sims_appl_parameter;
                setTimeout(function () {
                    $('#staff').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);

            });

            $http.get(ENV.apiUrl + "api/hrms/getStaffService?opr="+'X').then(function (res1) {
                debugger;
                $scope.service = res1.data;
                $scope.temp.service = $scope.service[0].sims_appl_parameter;

                setTimeout(function () {
                    $('#service').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);

            });







            $scope.getdetails = function () {
                var v = document.getElementById('summary');
                if (v.checked == true) {
                    $http.get(ENV.apiUrl + "api/hrms/getEmpListSecond?comp_code=" + $scope.temp.company_code + "&dept_code=" + $scope.temp.department + "&designation_code=" + $scope.temp.designation + "&staff_type=" + $scope.temp.staff + "&service_status=" + $scope.temp.service).then(function (res1) {
                        $scope.report_data_summary = res1.data;
                        $scope.summary_div = true;
                        $scope.main_div = false;
                    });
                }
                else {

                    $http.get(ENV.apiUrl + "api/hrms/getEmpList?comp_code=" + $scope.temp.company_code + "&dept_code=" + $scope.temp.department + "&designation_code=" + $scope.temp.designation + "&staff_type=" + $scope.temp.staff + "&service_status=" + $scope.temp.service).then(function (res1) {
                        $scope.report_data = res1.data;
                        $scope.report_data = res1.data;
                        $scope.totalItems = $scope.report_data.length;
                        $scope.todos = $scope.report_data;
                        $scope.makeTodos();
                        $scope.main_div = true;
                        $scope.summary_div = false;
                    });
                }
            }



            $scope.getSection = function (cur_code, academic_year, grade_code) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.temp.sims_cur_code + "&grade_code=" + $scope.temp.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }




            $scope.getCircdetail = function () {
                debugger;
                $scope.report_data_new = [];
                $scope.detailsField = true;
                //string cur_code, string acad_year, string circular_status, string user_group,string from,string to,string search,string report_view)
                $http.get(ENV.apiUrl + "api/CircularStatusByParent/GetCircularuserstatus?cur_code=" + $scope.temp.sims_cur_code + "&acad_year=" + $scope.temp.sims_academic_year + "&circular_status=" + $scope.temp.sims_status + "&user_group=" +
                    $scope.temp.user_group_code + "&from=" + $scope.from_date + "&to=" + $scope.to_date + "&search=" + $scope.searchParent + "&report_view=" + $scope.temp.show_expire_circular).then(function (res1) {
                        if (res1.data.length > 0) {
                            $scope.report_data_new = res1.data;
                            $scope.totalItems = $scope.report_data_new.length;
                            $scope.todos = $scope.report_data_new;
                            $scope.makeTodos();
                        }
                        else {
                            $scope.filteredTodos = [];
                            swal({ title: "Alert", text: " Sorry !!!!Data is not Available", showCloseButton: true, width: 300, height: 200 });
                            $scope.report_data_new = [];
                        }

                    });
            }


            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };
            $scope.search = function () {

                $scope.todos = $scope.searched($scope.studlist, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.report_data_new;
                }
                $scope.makeTodos();
            }


            function searchUtil(item, toSearch) {
                debugger
                /* Search Text in all 2 fields */
                return (item.e_details[0].email_subject.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.e_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }




            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $scope.colsvis = false;


            $scope.exportData = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('pdf_print').innerHTML],
                         {
                             type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                         });
                        $scope.detailsField = false;
                        saveAs(blob, "SubstituteLectureDetailsReport.xls");
                        $scope.colsvis = false;
                        $scope.getsubdetail();

                    }

                });
                $scope.colsvis = true;

            };


            $scope.print = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Print?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var docHead = document.head.outerHTML;
                        var printContents = document.getElementById('pdf_print').outerHTML;
                        var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
                        var newWin = window.open("", "_blank", winAttr);
                        var writeDoc = newWin.document;
                        writeDoc.open();
                        writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                        writeDoc.close();
                        newWin.focus();
                        $scope.detailsField = false;
                        $scope.colsvis = false;
                        $scope.getsubdetail();

                    }
                });
            };

            $scope.reset_data = function () {
                debugger;
                // $scope.temp.sims_grade_code = '';

                $scope.filteredTodos = [];



                try {
                    $('#cmb_grade_code').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_section_code').multipleSelect('uncheckAll');
                } catch (e) {

                }
                $scope.temp.sims_bell_code = '';
                $scope.report_data_new = [];


            }

            $scope.ischeck = function () {
                debugger
                $scope.checklick = function () {
                    var v = document.getElementById('summary');
                    if (v.checked == true) {
                        v.checked == false;
                    }

                }
            }


        }])

})();

