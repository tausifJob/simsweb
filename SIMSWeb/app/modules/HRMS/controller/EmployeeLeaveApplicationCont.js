﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('EmployeeLeaveApplicationCont',
        ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.display = true;
            $scope.grid = true;
            $scope.div_pendingview = true;
            $scope.div_trans = false;
            $scope.div_emp_search = false;
            $scope.SaveBtnDisabled = false;
            var data1 = [];
            var data = [];
            $scope.year_data = [];
            $scope.div_pending = false;
            $scope.enroll_number_lists = [];
            $scope.Holileave_data = [];
            $scope.test_emp = [];
            $scope.role_data = [];
            $scope.emprole_data = [];
            var main;
            var date = new Date();
            //$scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');

            $scope.username = $rootScope.globals.currentUser.username;
            $rootScope.visible_stud = false;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = true;
            $rootScope.chkMulti = false;
            $scope.emp_name = '';
            $scope.edt = {};
            var get_dates = [];
            var forbidden = [];
            var dates = [];
            var newd = [];
            $scope.empleave_history = [];
            $scope.india = false;
            $scope.showWorkFlowType = false;

            var schoolArray = ['dmc', 'christ', 'svvmc', 'cesc', 'gvmcce','sis'];

            if (schoolArray.includes($http.defaults.headers.common['schoolId'])) {
                $scope.showWorkFlowType = true;
            }

            
            $timeout(function () {
                $("#fixedtable,#fixedtable1,#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/HRMS/getLicSchool").then(function (licschooldata) {
                $scope.lic_school_data = licschooldata.data;
                console.log("lic_school_data", $scope.lic_school_data);               
                if ($scope.lic_school_data[0].lic_school_country == 'India') {                    
                    $scope.india = true;
                }

                //if ($scope.india == true) {                
                //    $("#dp_start_date").kendoDatePicker({
                //        format: "dd-MM-yyyy",
                //        min: new Date('1900-01-01')
                //    });
                //    $("#dp_endadmdate").kendoDatePicker({
                //        format: "dd-MM-yyyy",
                //        min: new Date('01-01-1900')
                //    });

                //    console.log("in india");
                //}
                //else {
                //    $("#dp_start_date").kendoDatePicker({
                //        format: "dd-MM-yyyy",
                //        min: new Date()
                //    });
                //    $("#dp_endadmdate").kendoDatePicker({
                //        format: "dd-MM-yyyy",
                //        min: new Date()
                //    });
                //    console.log("else india")
                //}

            });

            
            $scope.$on('global_cancel', function (str) {
               
                $scope.Empmodal_cancel();
            });

            $scope.getEmployee1 = function () {
                $scope.edt = {};
                $scope.leavebal_data = '';
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                $scope.edt.sims_workflow_type = $scope.GetWorkFlowType[0].sims_workflow_type;
            }

          
            $scope.UploadFileModal = function () {                
                $('#myModal').modal('show');                
            }

            $scope.getDate = function () {
                //var st = document.getElementById('dp_start_date');
                //st.setAttribute('data-date-start-date', $scope.yearcurrentstart);
                //st.setAttribute('data-date-end-date', $scope.yearcurrentyend);

                //var end = document.getElementById('dp_endadmdate');
                //end.setAttribute('data-date-start-date', $scope.yearcurrentstart);
                //end.setAttribute('data-date-end-date', $scope.yearcurrentyend);
            }

            $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_Current_AcademicYear").then(function (res) {
                $scope.year_data = res.data;
                $scope.yearcurrentstart = $scope.year_data.yearcurrentstart;
                $scope.yearcurrentyend = $scope.year_data.yearcurrentyend;
                $scope.getDate();
            });

            $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_RoleDetails").then(function (res) {
                $scope.role_data = res.data;

                $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_empRoleDetails?emp_id=" + $scope.username).then(function (res) {
                    $scope.emprole_data = res.data;
                    for (var i = 0; i < $scope.role_data.length; i++)
                    {
                        for (var j = 0; j < $scope.emprole_data.length; j++)
                        {
                            if ($scope.role_data[i].sims_param_role_field === $scope.emprole_data[j].sims_param_role_id)
                            {
                                $scope.div_emp_search = true;
                                $scope.emp_name = $scope.username;
                            }
                        }
                    }
                });

            });

            $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_EmpInfo?emp_id=" + $scope.username).then(function (res) {
                $scope.emp_data = res.data;
                if (!jQuery.isEmptyObject($scope.emp_data)) {
                    $scope.comp_code = $scope.emp_data[0].comp_code;
                    $scope.dep_code = $scope.emp_data[0].dept_no;
                    console.log("emp_data", $scope.emp_data);

                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_EMPLeaveDetails?compcode=" + $scope.emp_data[0].comp_code + "&employee_id=" + $scope.username).then(function (res) {
                        $scope.leaveType_data = res.data;
                    });

                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_EMPAllLeaveDates?compcode=" + $scope.comp_code + "&employee_id=" + $scope.username).then(function (res) {
                        $scope.leavedate_data = res.data;

                        for (var j = 0; j < $scope.leavedate_data.length; j++) {
                            var d1 = moment($scope.leavedate_data[j].lt_start_date, "DD-MM-YYYY").format('YYYY-MM-DD');
                            var d2 = moment($scope.leavedate_data[j].lt_end_date, "DD-MM-YYYY").format('YYYY-MM-DD');
                            var dates = getDates(new Date(d1), new Date(d2));
                            get_dates.push(dates);
                        }

                        for (var k = 0; k <= get_dates.length; k++) {
                            newd = get_dates[k];
                            if (newd.length > 0) {
                                for (var l = 0; l < newd.length; l++) {
                                    var l1 = moment(newd[l], "DD-MM-YYYY").format('YYYY-MM-DD');
                                    var date = new Date(l1);
                                    var month = (date.getMonth() + 1);
                                    var day = date.getDate();
                                    //$scope.d = date.getFullYear() + '-' + parseInt(month) + '-' + parseInt(day);
                                    $scope.d = parseInt(day) + '-' + parseInt(month) + '-' + date.getFullYear();
                                    forbidden.push($scope.d);

                                    //$('#dp_start_date').datepicker({
                                    //    beforeShowDay: function (Date) {
                                    //        var curr_day = Date.getDate();
                                    //        var curr_month = Date.getMonth() + 1;
                                    //        var curr_year = Date.getFullYear();
                                    //        //var curr_date = curr_year + '-' + parseInt(curr_month) + '-' + parseInt(curr_day)
                                    //        //var curr_date = parseInt(curr_day) + '-' + parseInt(curr_month) + '-' + curr_year
                                    //        if (!jQuery.isEmptyObject(forbidden)) {
                                    //            if (forbidden.indexOf(curr_date) > -1) return false;
                                    //        }
                                    //    }
                                    //});

                                    //$('#dp_endadmdate').datepicker({
                                    //    beforeShowDay: function (Date) {
                                    //        var curr_day = Date.getDate();
                                    //        var curr_month = Date.getMonth() + 1;
                                    //        var curr_year = Date.getFullYear();
                                    //        //var curr_date = curr_year + '-' + parseInt(curr_month) + '-' + parseInt(curr_day)
                                    //        var curr_date = parseInt(curr_day) + '-' + parseInt(curr_month) + '-' + curr_year
                                    //        if (!jQuery.isEmptyObject(forbidden)) {
                                    //            if (forbidden.indexOf(curr_date) > -1) return false;
                                    //        }
                                    //    }
                                    //});
                                };
                                // forbidden = ["2015-12-1", "2015-08-2", "2015-08-3", "2015-08-4", "2015-08-5", "2015-08-6", "2015-08-7", "2015-08-8", "2015-08-9", "2015-08-10"];

                            }
                        }
                    });

                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_empHolidayDetails?emp_id=" + $scope.username).then(function (res) {
                        $scope.Holileave_data = res.data;
                    });

                    $http.get(ENV.apiUrl + "api/leavedetails/GetAllEMPLeaveHistoryDetails?comp_code=" + $scope.comp_code + "&emp_id=" + $scope.username).then(function (res) {
                        $scope.empleave_history = res.data;
                    });

                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/GetSubstituteEmployee?comp_code=" + $scope.comp_code + "&emp_code=" + $scope.username).then(function (res) {
                        $scope.substitute_employees = res.data;
                    });

                    $http.get(ENV.apiUrl + "api/Workflow/GetWorkFlowDepartmentWise?dept_code=" + $scope.emp_data[0].dept_no).then(function (GetWorkFlowType) {
                        $scope.GetWorkFlowType = GetWorkFlowType.data;
                        $scope.edt.sims_workflow_type = $scope.GetWorkFlowType[0].sims_workflow_type;
                    });
                }
            });

            $scope.Empmodal_cancel = function () {
                for (var i = 0; i < $scope.SelectedUserLst.length; i++) {
                    //if ($scope.SelectedUserLst[i].em_login_code1 == true)
                    // {
                    $scope.enroll_number_lists.push($scope.SelectedUserLst[i]);
                    $scope.test_emp = $scope.SelectedUserLst[i];
                    $scope.emp_name = $scope.test_emp.em_login_code;
                    // }
                }

                $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_EmpInfo?emp_id=" + $scope.emp_name).then(function (res) {
                    $scope.emp_data = res.data;
                    if (!jQuery.isEmptyObject($scope.emp_data)) {
                        $scope.comp_code = $scope.emp_data[0].comp_code;
                        $scope.dep_code = $scope.emp_data[0].dept_no;

                        console.log("emp_data", $scope.emp_data);

                        $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_EMPLeaveDetails?compcode=" + $scope.emp_data[0].comp_code + "&employee_id=" + $scope.emp_name).then(function (res) {
                            $scope.leaveType_data = res.data;
                        });

                        $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_EMPAllLeaveDates?compcode=" + $scope.emp_data[0].comp_code + "&employee_id=" + $scope.emp_name).then(function (res) {
                            $scope.leavedate_data = res.data;

                            for (var j = 0; j < $scope.leavedate_data.length; j++) {
                                var d1 = moment($scope.leavedate_data[j].lt_start_date, "DD-MM-YYYY").format('YYYY-MM-DD');
                                var d2 = moment($scope.leavedate_data[j].lt_end_date, "DD-MM-YYYY").format('YYYY-MM-DD');

                                var dates = getDates(new Date(d1), new Date(d2));
                                get_dates.push(dates);
                            }


                            for (var k = 0; k <= get_dates.length; k++) {
                                newd = get_dates[k];
                                if (newd.length > 0) {
                                    for (var l = 0; l < newd.length; l++) {
                                        var l1 = moment(newd[l], "DD-MM-YYYY").format('YYYY-MM-DD');
                                        var date = new Date(l1);
                                        var month = (date.getMonth() + 1);
                                        var day = date.getDate();
                                        //$scope.d = date.getFullYear() + '-' + parseInt(month) + '-' + parseInt(day);
                                        $scope.d = parseInt(day) + '-' + parseInt(month) + '-' + date.getFullYear();
                                        forbidden.push($scope.d);

                                        //$('#dp_start_date').datepicker({
                                        //    beforeShowDay: function (Date) {
                                        //        var curr_day = Date.getDate();
                                        //        var curr_month = Date.getMonth() + 1;
                                        //        var curr_year = Date.getFullYear();
                                        //        //var curr_date = curr_year + '-' + parseInt(curr_month) + '-' + parseInt(curr_day)
                                        //        var curr_date = parseInt(curr_day) + '-' + parseInt(curr_month) + '-' + curr_year
                                        //        if (!jQuery.isEmptyObject(forbidden)) {
                                        //            if (forbidden.indexOf(curr_date) > -1) return false;
                                        //        }
                                        //    }
                                        //});

                                        //$('#dp_endadmdate').datepicker({
                                        //    beforeShowDay: function (Date) {
                                        //        var curr_day = Date.getDate();
                                        //        var curr_month = Date.getMonth() + 1;
                                        //        var curr_year = Date.getFullYear();
                                        //        //var curr_date = curr_year + '-' + parseInt(curr_month) + '-' + parseInt(curr_day)
                                        //        var curr_date = parseInt(curr_day) + '-' + parseInt(curr_month) + '-' + curr_year
                                        //        if (!jQuery.isEmptyObject(forbidden)) {
                                        //            if (forbidden.indexOf(curr_date) > -1) return false;
                                        //        }
                                        //    }
                                        //});
                                    };
                                }
                                // forbidden = ["2015-12-1", "2015-08-2", "2015-08-3", "2015-08-4", "2015-08-5", "2015-08-6", "2015-08-7", "2015-08-8", "2015-08-9", "2015-08-10"];
                               
                            }

                        });

                        $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_empHolidayDetails?emp_id=" + $scope.emp_name).then(function (res) {
                            $scope.Holileave_data = res.data;
                        });

                        $http.get(ENV.apiUrl + "api/leavedetails/GetAllEMPLeaveHistoryDetails?comp_code=" + $scope.comp_code + "&emp_id=" + $scope.emp_name).then(function (res) {
                            $scope.empleave_history = res.data;
                        });

                        $http.get(ENV.apiUrl + "api/common/EmployeeLeave/GetSubstituteEmployee?comp_code=" + $scope.comp_code + "&emp_code=" + $scope.emp_name).then(function (res) {
                            $scope.substitute_employees = res.data;
                        });

                        $http.get(ENV.apiUrl + "api/Workflow/GetWorkFlowDepartmentWise?dept_code=" + $scope.emp_data[0].dept_no).then(function (GetWorkFlowType) {
                            $scope.GetWorkFlowType = GetWorkFlowType.data;
                            $scope.edt.sims_workflow_type = $scope.GetWorkFlowType[0].sims_workflow_type;
                        });
                    }
                });

                // $scope.EmployeeDetails = [];
            }


            var getDates = function (startDate, endDate) {
                dates = [];
                var currentDate = startDate,
                 addDays = function (days) {
                     var t1 = moment(this.valueOf(), "DD-MM-YYYY").format('YYYY-MM-DD')
                     var date = new Date(t1);
                     date.setDate(date.getDate() + days);
                     return date;
                 };
                while (currentDate <= endDate) {
                    //currentDate = $filter('date')(currentDate, "yyyy-MM-dd");
                    currentDate = $filter('date')(currentDate, "dd-MM-yyyy");
                    var datadate = {
                        checkdates: currentDate
                    }

                    if ($scope.checkeddateflag == true) {
                        dates.push(datadate);
                    }
                    else {
                        dates.push(currentDate);
                    }
                    currentDate = addDays.call(currentDate, 1);
                }
                return dates;
            };

            $scope.getLeaveBalance = function (leave_code)
            {
                debugger
                $scope.leavebal_data = '';
                //$scope.edt.lt_start_date = '';
                //$scope.edt.lt_end_date = '';
                $scope.edt.sims_start_chk = '';
                $scope.edt.sims_end_chk = '';
                $scope.edt.lt_days = '';
                $scope.edt.remain_leave = '';
                $scope.edt.lwp_leave = '';
                $scope.edt.lt_remarks = '';
                $scope.allowed_paid_leave = '';
                $("#dp_start_date").val('');
                $("#dp_endadmdate").val('');
                $scope.edt.lt_start_date = '';
                $scope.edt.lt_end_date = '';

                //setTimeout(function () {
                   // $(document).ready(function () {
                        //$("#dp_start_date, #dp_endadmdate").kendoDatePicker({
                        //    format: "dd-MM-yyyy",
                        //    value: ''
                        //});
                    //});
                //}, 500);

                if ($scope.div_emp_search == false)
                {
                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_EMPLeaveMaxDays?le_leave_code=" + leave_code + "&compcode=" + $scope.comp_code + "&employee_id=" + $scope.username).then(function (res) {
                        $scope.leavebal_data = res.data;
                    });

                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/GetLeaveDetails?emp_code=" + $scope.username + "&comp_code=" + $scope.comp_code).then(function (res) {
                        $scope.leavedeatails_data = res.data;
                        if ($scope.leavedeatails_data.length > 0) {
                            $scope.div_pending = true;
                        }
                    });

                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_AllowedLeaveStatus?le_leave_code=" + leave_code + "&compcode=" + $scope.comp_code + "&employee_id=" + $scope.username).then(function (res) {
                        $scope.allowed_paid_leave = res.data[0].allowed_paid_leave;
                        console.log("allowed_paid_leave", $scope.allowed_paid_leave);
                    });
                }
                else {
                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_EMPLeaveMaxDays?le_leave_code=" + leave_code + "&compcode=" + $scope.comp_code + "&employee_id=" + $scope.emp_name).then(function (res) {
                        $scope.leavebal_data = res.data;
                    });

                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/GetLeaveDetails?emp_code=" + $scope.emp_name + "&comp_code=" + $scope.comp_code).then(function (res) {
                        $scope.leavedeatails_data = res.data;
                        if ($scope.leavedeatails_data.length > 0) {
                            $scope.div_pending = true;
                        }
                    });

                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_AllowedLeaveStatus?le_leave_code=" + leave_code + "&compcode=" + $scope.comp_code + "&employee_id=" + $scope.emp_name).then(function (res) {
                        $scope.allowed_paid_leave = res.data[0].allowed_paid_leave;
                        console.log("allowed_paid_leave",$scope.allowed_paid_leave);
                    });
                }


                setTimeout(function () {
                    $(document).ready(function () {
                        $("#dp_start_date, #dp_endadmdate").kendoDatePicker({
                            format: "dd-MM-yyyy",
                            value: ''
                        });
                    });
                }, 500);
               
            }

            $scope.UserSearch = function () {
                $('#Employee').modal('show');
                $scope.div_pendingview = true;
                $scope.div_trans = false;
                //$scope.username

                if ($scope.div_emp_search == false) {
                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/GetLeaveDetails?emp_code=" + $scope.username + "&comp_code=" + $scope.comp_code).then(function (res) {
                        $scope.leavedeatails_data = res.data;
                    });
                }
                else {
                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/GetLeaveDetails?emp_code=" + $scope.emp_name + "&comp_code=" + $scope.comp_code).then(function (res) {
                        $scope.leavedeatails_data = res.data;
                    });
                }
            }

            $scope.viewDetails = function (lt_no) {
                $http.get(ENV.apiUrl + "api/common/EmployeeLeave/GetStatusofTransactionforEmployee?transactionId=" + lt_no).then(function (res) {
                    $scope.Empleavedeatails_data = res.data;
                    $scope.div_pendingview = false;
                    $scope.div_trans = true;
                });
            }

            $scope.viewWOrkflowDetails = function (lt_no) {
                $("#workflow_details").modal('show');
                $http.get(ENV.apiUrl + "api/common/EmployeeLeave/GetStatusofTransactionforEmployee?transactionId=" + lt_no).then(function (res) {
                    $scope.Empleavedeatails_data = res.data;                   
                });
            }

            $scope.edt =
           {
               sims_start_chk: false,
               sims_end_chk: false
           }


            var lt_hours;
            var date2;
            var date1;
            var timeDiff;
            var diffDays;
            var data1 = [];
            var balanceLeaves = 0;
            var lt_days_sum = 0;
            
            $scope.getDateLeaves = function ()
            {   debugger
                $scope.checkeddateflag = true;
                $scope.edt.lt_days = "";
                $scope.edt.lwp_leave = "";
                $scope.edt.remain_leave = "";

                if (($scope.edt.lt_start_date != '' || $scope.edt.lt_start_date != undefined) && ($scope.edt.lt_end_date != '' || $scope.edt.lt_start_date != undefined))
                {
                    if (moment($scope.edt.lt_end_date, "DD-MM-YYYY").format('YYYY-MM-DD') >= moment($scope.edt.lt_start_date, "DD-MM-YYYY").format('YYYY-MM-DD'))
                    {
                        var cnt = 0;
                        var holi_data = [];
                        var dates;
                        for (var j = 0; j < $scope.Holileave_data.length; j++) 
                        {
                            var start_holi = moment($scope.Holileave_data[j].lt_start_date, "DD-MM-YYYY").format('YYYY-MM-DD');
                            var end_holi = moment($scope.Holileave_data[j].lt_end_date, "DD-MM-YYYY").format('YYYY-MM-DD');
                            dates = getDates(new Date(start_holi), new Date(end_holi));
                            var dates1 = getDates(new Date(start_holi), new Date(end_holi));
                            if (dates.length > 0) {
                                for (var k = 0; k < dates.length; k++) {
                                    for (var l = 0; l < dates1.length; l++) {
                                        if (dates1[l].checkdates == dates[k].checkdates) {
                                            cnt = cnt + 1;
                                        }
                                    }
                                }
                            }
                        }

                        var start_date = moment($scope.edt.lt_start_date, "DD-MM-YYYY").format('YYYY-MM-DD');
                        var end_date = moment($scope.edt.lt_end_date, "DD-MM-YYYY").format('YYYY-MM-DD');

                        date2 = new Date(end_date);
                        date1 = new Date(start_date);



                        timeDiff = Math.abs(date2.getTime() - date1.getTime());
                        diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));

                        debugger;

                        if ($scope.edt.sims_start_chk == true && $scope.edt.sims_end_chk == true) {
                            diffDays = parseInt(diffDays) + 1;
                            lt_hours = "1.1";
                        }
                        if (($scope.edt.sims_start_chk == false || $scope.edt.sims_start_chk == undefined) && ($scope.edt.sims_end_chk == false || $scope.edt.sims_end_chk == undefined)) {
                            diffDays = parseInt(diffDays) + 1;
                            lt_hours = "0";
                        }
                        if ($scope.edt.sims_start_chk == true && ($scope.edt.sims_end_chk == false || $scope.edt.sims_end_chk == undefined)) {
                            diffDays = parseInt(diffDays) + 0.5;
                            lt_hours = "0.1";
                        }
                        if (($scope.edt.sims_start_chk == false || $scope.edt.sims_start_chk == undefined) && $scope.edt.sims_end_chk == true) {
                            diffDays = parseInt(diffDays) + 0.5;
                            lt_hours = "0.1";
                        }

                        diffDays = diffDays - cnt;
                        if (isNaN(diffDays)) {
                            diffDays = 0;
                        }
                        console.log("diffDays",diffDays);

                        $scope.edt.lt_days = diffDays + ' ' + "Days";

                        if (($scope.leavebal_data - diffDays) > 0) {
                            $scope.edt.remain_leave = ($scope.leavebal_data - diffDays);
                            $scope.edt.lwp_leave = 0;
                        }
                        else {
                            $scope.edt.lwp_leave = (diffDays - $scope.leavebal_data);
                            $scope.edt.remain_leave = "0";
                        }
                    }

                }

                if ($scope.allowed_paid_leave == 'N' && $scope.leavebal_data <= 0) {
                    $scope.edt.lwp_leave = 0;
                    diffDays = 0;
                    swal({ title: "Alert", text: "You have only " + $scope.leavebal_data + " balance leave.\n Can not apply more than balance leave.", showCloseButton: true, width: 380, });
                    $scope.edt.lt_end_date = "";
                }

                if ($scope.edt.lwp_leave > 0) {
                    /* Code is commented dont delete*/
                    //swal({ title: "Alert", text: "You have only " + $scope.leavebal_data + " balance leave.\n Can not apply more than balance leave.", showCloseButton: true, width: 380, });
                    //$scope.edt.lt_end_date = "";
                }

                //console.log(moment($scope.edt.lt_start_date, "DD-MM-YYYY").format('YYYY-MM-DD'));
                //  if ($scope.edt.lt_end_date < $scope.edt.lt_start_date)
                debugger
                if (moment($scope.edt.lt_end_date, "DD-MM-YYYY").format('YYYY-MM-DD') < moment($scope.edt.lt_start_date, "DD-MM-YYYY").format('YYYY-MM-DD'))
                {
                    swal({ title: "Alert", text: "End Date is Less than Start Date", showCloseButton: true, width: 380, });
                    $scope.edt.lt_end_date = "";
                    //$scope.edt.lt_start_date = undefined;
                }

                var emp_id = "";
                balanceLeaves = 0;                
                lt_days_sum = 0;
                
                for (var j = 0; j < $scope.empleave_history.length; j++) {

                    if ($scope.empleave_history[j].lt_leave_code == $scope.edt.leave_code) {
                        lt_days_sum += parseFloat($scope.empleave_history[j].lt_days);
                        balanceLeaves = $scope.empleave_history[j].balanceLeaves;
                    }
                    ////if ((new Date($scope.edt.lt_start_date) >= new Date(value.lt_start_date) && new Date($scope.edt.lt_end_date) <= new Date(value.lt_end_date)) || new Date($scope.edt.lt_start_date) <= new Date(value.lt_end_date))
                    //if ((new Date($scope.edt.lt_start_date) >= new Date($scope.empleave_history[j].lt_start_date) && new Date($scope.edt.lt_start_date) <= new Date($scope.empleave_history[j].lt_end_date))
                    //    || (new Date($scope.edt.lt_end_date) >= new Date($scope.empleave_history[j].lt_start_date) && new Date($scope.edt.lt_end_date) <= new Date($scope.empleave_history[j].lt_end_date))) {
                    //    swal({ title: "Alert", text: "You are already applied leave between" + $scope.edt.lt_start_date + " to " + $scope.edt.lt_end_date, showCloseButton: true, width: 380 });
                    //}
                    //else {

                    //    if ($scope.empleave_history[j].em_login_code == emp_id && $scope.empleave_history[j].lt_leave_code == $scope.edt.leave_code) {
                    //        balanceLeaves = $scope.empleave_history[j].balanceLeaves;
                    //    }
                    //}
                };

                console.log("balanceLeaves", balanceLeaves);
                
                //if (lt_days_sum > 0 && $scope.allowed_paid_leave == 'N') {
                //      /* Code is commented dont delete*/

                //    if (parseFloat(diffDays) > parseFloat(balanceLeaves)) {
                //        //swal({ title: "Alert", text: "You have only " + balanceLeaves + " balance leave.\n Can not apply more than balance leave.", showCloseButton: true, width: 380, });
                //        //$scope.edt.lt_end_date = "";
                //    }
                //    /* Code is commented dont delete*/
                //}

                if ($scope.div_emp_search == true) {
                    emp_id = $scope.emp_name;
                }
                else {
                    emp_id = $scope.username;
                }

                if($scope.edt.lt_start_date != "" && $scope.edt.lt_end_date != "") {
                    $http.get(ENV.apiUrl + "api/common/EmployeeLeave/GetCheckLeaveExists?emp_id=" + emp_id + "&lt_start_date=" + $scope.edt.lt_start_date + "&lt_end_date=" + $scope.edt.lt_end_date).then(function (res) {
                        $scope.checkLeaveExists = res.data;
                        if ($scope.checkLeaveExists[0].leave_exists == 'Y') {
                            swal({ title: "Alert", text: "You have already applied leave between " + $scope.edt.lt_start_date + " and " + $scope.edt.lt_end_date, showCloseButton: true, width: 380, });
                            $scope.edt.lt_start_date = "";
                            $scope.edt.lt_end_date = "";
                        }
                    });
                }
                
            }

            var emp = '';

            $scope.Save = function (isvalid) {
                if (isvalid) {
                    debugger;
                    if ($scope.allowed_paid_leave == 'N' && $scope.edt.lwp_leave > 0) {
                        //$scope.edt.lwp_leave = 0;
                       // diffDays = 0;
                        swal({ title: "Alert", text: "You have only " + $scope.leavebal_data + " balance leave.\n Can not apply more than balance leave.", showCloseButton: true, width: 380, });                     
                        return;
                    }
                    else if ($scope.allowed_paid_leave == 'N' && (parseFloat(diffDays) > parseFloat(balanceLeaves) && lt_days_sum > 0)) {
                            swal({ title: "Alert", text: "You have only " + balanceLeaves + " balance leave.\n Can not apply more than balance leave.", showCloseButton: true, width: 380, });
                            return;
                            //$scope.edt.lt_end_date = "";
                    }                                          
                    else {
                        $scope.SaveBtnDisabled = true;
                        $scope.edt.lt_days = diffDays;
                        $scope.edt.lt_hours = lt_hours;

                        if ($scope.div_emp_search == true) {
                            emp = $scope.emp_name;
                        }
                        else {
                            emp = $scope.username;
                        }

                        if ($scope.filename1 == undefined || $scope.filename1 == "" || $scope.filename1 == null) {
                            $scope.file_name = "";
                        }
                        else {
                            $scope.file_name = emp + '_' + $scope.pays_leave_doc_name + '_' + $scope.filename1;
                        }

                        if($scope.pays_leave_doc_is_mandatory == 'Y' && $scope.file_name == "") {
                            swal({ title: 'Alert', text: "Please upload leave document.", showCloseButton: true, width: 380, });
                            return;
                        }

                        console.log($scope.file_name);

                        var data1 = [];
                        var data = ({
                            em_login_code: emp,
                            comp_code: $scope.emp_data[0].comp_code,
                            leave_code: $scope.edt.leave_code,
                            lt_start_date: $scope.edt.lt_start_date,
                            lt_end_date: $scope.edt.lt_end_date,
                            lt_days: $scope.edt.lt_days,
                            lt_remarks: $scope.edt.lt_remarks,
                            lt_mgr_confirm_tag: "N",
                            lt_converted_hours: "",
                            lt_hours: $scope.edt.lt_hours,
                            lt_substitute_employee: $scope.edt.lt_substitute_employee,
                            em_doc_path: $scope.file_name,
                            pays_leave_doc_code: $scope.pays_leave_doc_code
                        });
                        data1.push(data);

                        $http.post(ENV.apiUrl + "api/common/EmployeeLeave/CUDInsertEMPLeaveDetails", data1).then(function (res) {
                            $scope.msg = res.data;

                            //if ($scope.msg!='' ||$scope.msg!=null)
                            if ($scope.msg > 0) {
                                swal({ text: "Leave Applied Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {

                                        if($scope.showWorkFlowType == true) {
                                            $http.post(ENV.apiUrl + "api/common/EmployeeLeave/CUDInsert_WorkflowDetails_DMC?transaction_id=" + $scope.msg + "&dept_code=" + $scope.emp_data[0].dept_no + "&workflow_type=" + $scope.edt.sims_workflow_type).then(function (res) {
                                                $scope.workflow_data = res.data;
                                            });
                                        }
                                        else {
                                            $http.post(ENV.apiUrl + "api/common/EmployeeLeave/CUDInsert_WorkflowDetails?transaction_id=" + $scope.msg + "&dept_code=" + $scope.emp_data[0].dept_no).then(function (res) {
                                                $scope.workflow_data = res.data;
                                            });
                                        }

                                        $scope.SaveBtnDisabled = false;

                                        if($scope.file_name != "") {
                                            var request = {
                                                method: 'POST',
                                                url: ENV.apiUrl + '/api/EmpDocumentupload/upload?filename=' + $scope.file_name + "&location=" + "EmployeeDocument",
                                                data: formdata,
                                                headers: {
                                                    'Content-Type': undefined
                                                }
                                            };
                                            $http(request)
                                              .success(function (d) {                                             
                                              },
                                            function () {
                                                //alert("Err");
                                                swal({ text: "Document not uploded.", showCloseButton: true, width: 380, });
                                            });
                                        }
                                        $scope.SaveBtnDisabled = false;
                                        $scope.cancel();
                                    }
                                });
                                

                                $http.get(ENV.apiUrl + "api/leavedetails/GetAllEMPLeaveHistoryDetails?comp_code=" + $scope.emp_data[0].comp_code + "&emp_id=" + emp).then(function (res) {
                                    $scope.empleave_history = res.data;
                                });
                            }
                            else {
                                swal({ text: "Leave Not Applied. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                                $scope.cancel();
                            }
                        });
                    }
                }
            }

            $scope.Employeeleave_Reset = function () {
                $scope.div_trans = false;
                $scope.div_pendingview = true;
            }

            $scope.cancel = function () {
                debugger;
                $scope.edt = {};
                $scope.div_pending = false;
                $scope.SaveBtnDisabled = false;
                $scope.leavebal_data = "";                
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.filename1 = "";
                $("#file1").val('');
                $scope.edt.sims_workflow_type = $scope.GetWorkFlowType[0].sims_workflow_type;
            }

            $scope.downloaddoc = function (str) {
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeDocument/' + str;
                window.open($scope.url);
            }

            $scope.getLeaveDocument = function(leave_code){
                console.log("$scope.leaveType_data", $scope.leaveType_data);
                for (var i = 0; i < $scope.leaveType_data.length; i++) {
                    if ($scope.leaveType_data[i].leave_code == leave_code) {
                        $scope.pays_leave_doc_code = $scope.leaveType_data[i].pays_leave_doc_code;
                        $scope.pays_leave_doc_name = $scope.leaveType_data[i].pays_leave_doc_name;
                        $scope.pays_leave_doc_is_mandatory = $scope.leaveType_data[i].pays_leave_doc_is_mandatory;
                        break;
                    }
                }
                console.log($scope.pays_leave_doc_code);
                console.log($scope.pays_leave_doc_name);
                console.log($scope.pays_leave_doc_is_mandatory);
            };

            var formdata = new FormData();
            $scope.hideModal = function () {
                debugger;
                $scope.showFileName = true;

                $scope.filename1 = $scope.filename;
                if ($scope.filename1 == undefined || $scope.filename1 == "" || $scope.filename1 == null) {
                    $scope.showFileName = false;
                }

                $scope.edt.em_doc_path = $scope.filename1;
                $('#myModal').modal('hide');

               
                //var request = {
                //    method: 'POST',
                //    url: ENV.apiUrl + '/api/EmpDocumentupload/upload?filename=' + $scope.file_name + "&location=" + "EmployeeDocument",
                //    data: formdata,
                //    headers: {
                //        'Content-Type': undefined
                //    }
                //};
                //$http(request)
                //  .success(function (d) {                           
                //  },
                //function () {
                //    //alert("Err");
                //    swal({ text: "Offer letter not uploded.", showCloseButton: true, width: 380, });
                //});

           };

            $scope.getTheFiles = function ($files) {

                //FileList[0].File.name = $scope.filename + '.' + fortype;
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);

                });
            };

            var fortype = '';
            $scope.file_changed = function (element) {
                debugger;
                  var photofile = element.files[0];
                  var fortype = photofile.type.split("/")[1];

                  //if (fortype == 'pdf') {
                  
                        //if (photofile.size > 200000) {
                        //    swal('', 'File size limit not exceed upto 200kb.')
                        //}
                     //    else {

                       $scope.filename = photofile.name;
                       $scope.edt.employeeDocument = $scope.filename;
                       $.extend($scope.edt, $scope.edt)
                       //$scope.photo_filename = (photofile.type);

                       $scope.photo_filename = (photofile.name);

                       var len = 0;
                       len = $scope.photo_filename.split('.');
                       fortype = $scope.photo_filename.split('.')[len.length - 1];
                       // $scope.edt1 = { sims_timetable_filename: $scope.photo_filename }
                       // $.extend($scope.edt, $scope.edt1)
                       $scope.photo_filename = (photofile.type);

                       //element.files[0].FileList.File.name = $scope.filename + '.' + fortype;
                       var reader = new FileReader();
                       reader.onload = function (e) {
                           $scope.$apply(function () {
                               // $scope.prev_img = e.target.result;


                           });
                       };
                       reader.readAsDataURL(photofile);

                      //    }

                    //}
                    //else {
                    //  swal({ title: 'Alert', text: 'Only PDF format is allowed.'});
                    //}
            };


            //$('*[data-datepicker="true"] input[type="text"]').datepicker({
            //    todayBtn: true,
            //    orientation: "top left",
            //    autoclose: true,
            //    todayHighlight: true,
            //  //  format: 'yyyy-mm-dd',
            //});

            //$(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
            //   $('input[type="text"]', $(this).parent()).focus();
            //});

        }])


})();