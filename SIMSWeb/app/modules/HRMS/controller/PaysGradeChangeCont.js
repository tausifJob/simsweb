﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PaysGradeChangeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.temp = {};
            $scope.showPager= true;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.globalSearch = function () {
                debugger;
                $rootScope.visible_stud = false;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = false;
                $rootScope.visible_Employee = true;
                $rootScope.chkMulti = false;

                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });                
            }

            $scope.$on('global_cancel', function (str) {
                console.log($scope.SelectedUserLst);
                if ($scope.SelectedUserLst.length > 0) {                    
                    $scope.temp.gc_number = $scope.SelectedUserLst[0].em_login_code;                    
                }
            });

            $scope.searchGlobalClick = function () {
                $scope.Global_Search_by_employee();
            }

            $scope.checkGrade = function () {
                if ($scope.temp.gc_old_grade == $scope.temp.gc_new_grade) {
                    swal({ title: "Alert", text: 'Old grade and new grade can not be same', showCloseButton: true, width: 380, })
                    $scope.temp.gc_new_grade = "";
                    return;
                }                
            }
            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/PaysGradeChange/Getall_grade_change").then(function (res1) {

                $scope.CreDiv = res1.data;
                $scope.totalItems = $scope.CreDiv.length;
                $scope.todos = $scope.CreDiv;
                $scope.makeTodos();
            });

            $scope.size = function (str) {
                if (str == 'All') {
                    $scope.currentPage = 1;
                    $scope.filteredTodos = $scope.CreDiv;
                    $scope.showPager = false;
                }
                else {
                    $scope.showPager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.gc_company_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.gc_emp_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.gc_number == toSearch) ? true : false;
            }

            $http.get(ENV.apiUrl + "api/PaysGradeChange/Get_gc_company_Code").then(function (compcode) {
                $scope.comp_code = compcode.data;
                $scope.temp.gc_company_code = $scope.comp_code[0].gc_company_code;
            });

            $http.get(ENV.apiUrl + "api/PaysGradeChange/Get_gc_Code").then(function (grcode) {
                $scope.old_grade = grcode.data;
            });

            //NEW BUTTON
            $scope.New = function () {

                $scope.disabled = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.divcode_readonly = false;
                $scope.number = false;
                $scope.company = false;                
                $scope.readonlyGCOldGrade = false;
                $scope.temp.gc_company_code = $scope.comp_code[0].gc_company_code;
                $scope.temp.gc_mgr_confirm_tag = true;
                var dt = new Date();
                $scope.temp.gc_effect_from = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                $scope.temp.gc_retro_effect_from = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = {};
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.number = true;
                $scope.company = true;
                $scope.divcode_readonly = true;
                $scope.readonlyGCOldGrade = true;
                //$scope.temp = str;
                $scope.temp = {
                    gc_company_code: str.gc_company_code,
                    gc_effect_from: str.gc_effect_from,
                    gc_mgr_confirm_tag: str.gc_mgr_confirm_tag,
                    gc_new_grade: str.gc_new_grade,
                    gc_number: str.gc_number,
                    gc_old_grade: str.gc_old_grade,
                    gc_retro_effect_from: str.gc_retro_effect_from
                }

            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {

                if (Myform) {
                    var data = $scope.temp;
                    data.opr = 'I';
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/PaysGradeChange/CUDPaysGradeChange", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        swal({ text: $scope.msg1.strMessage, timer: 5000 });
                        $scope.Cancel();

                        $http.get(ENV.apiUrl + "api/PaysGradeChange/Getall_grade_change").then(function (res1) {

                            $scope.CreDiv = res1.data;
                            $scope.totalItems = $scope.CreDiv.length;
                            $scope.todos = $scope.CreDiv;
                            $scope.makeTodos();
                        });
                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            //DATA UPDATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                if (Myform) {
                    var data = $scope.temp;
                    data.opr = "U";
                    dataforUpdate.push(data);

                    $http.post(ENV.apiUrl + "api/PaysGradeChange/CUDPaysGradeChange", dataforUpdate).then(function (msg) {

                        $scope.msg1 = msg.data;
                        swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                $scope.Cancel();
                                $scope.getgrid();
                            }
                        });
                    });
                    dataforUpdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }


            $scope.OkDelete = function () {
                deletefin = [];

                swal({
                    title: '',
                    text: "Are you sure you want to Delete?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        debugger
                        for (var i = 0; i < $scope.filteredTodos.length; i++) {

                            var v = document.getElementById(i);

                            if (v.checked == true) {
                                var deletemodulecode = ({

                                    'gc_number': $scope.filteredTodos[i].gc_number,
                                    'gc_company_code': $scope.filteredTodos[i].gc_company_code,
                                    opr: 'D'

                                });
                                deletefin.push(deletemodulecode);
                            }
                        }

                        $http.post(ENV.apiUrl + "api/PaysGradeChange/CUDPaysGradeChange", deletefin).then(function (msg) {
                            $scope.msg1 = msg.data;
                            swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.Cancel();
                                    $scope.getgrid();
                                }
                            });
                        });
                    }
                    else {
                    }
                });
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/PaysGradeChange/Getall_grade_change").then(function (res1) {
                    $scope.CreDiv = res1.data;
                    $scope.totalItems = $scope.CreDiv.length;
                    $scope.todos = $scope.CreDiv;
                    $scope.makeTodos();
                });

            }

        }])

})();