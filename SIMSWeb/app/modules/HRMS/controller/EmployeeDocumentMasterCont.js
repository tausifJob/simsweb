﻿(function () {
    'use strict';
    var obj1, obj2, edt, main;
    var opr = '';
    var empdocdetailcode = [];
    var categorycode = [];

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeDocumentMasterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.EmployeeDetail = true;
            $scope.pagesize = "10";
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.editmode = false;
            var formdata = new FormData();
            var data1 = [];
            $scope.edt = {};

            debugger
            var username = $rootScope.globals.currentUser.username;
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.countData = [
                     { val: 10, data: 10 },
                     { val: 20, data: 20 },
                     //{ val: 'All', data: 'All' },

            ]
            $scope.size = function (str) {
                if (str == 10 || str == 20 || str == 'All') {

                    if (str == "All") {
                        $scope.currentPage = '1';
                        $scope.filteredTodos = $scope.EmpDocData;
                        $scope.pager = false;
                    }
                    else {
                        $scope.pager = true;
                        $scope.pagesize = str;
                        $scope.currentPage = 1;
                        $scope.numPerPage = str;
                        $scope.makeTodos();
                    }

                   
                }

                else {
                    $scope.pager = false;
                }
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;

                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.getempoyeegrid = function () {
                $http.get(ENV.apiUrl + "api/EmpDocumentMaster/getEmpDocumentDetail").then(function (getEmpDocumentDetail_Data) {
                    debugger
                    $scope.EmpDocData = getEmpDocumentDetail_Data.data;

                    if ($scope.EmpDocData.length > 0) {

                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.EmpDocData.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.EmpDocData.length, data: 'All' })
                        }
                        $scope.totalItems = $scope.EmpDocData.length;
                        $scope.todos = $scope.EmpDocData;
                        $scope.makeTodos();
                    }
                    else {

                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                        $scope.totalItems = $scope.EmpDocData.length;
                        $scope.todos = $scope.EmpDocData;
                        $scope.makeTodos();
                    }

                    console.log($scope.EmpDocData);
                })

            }

            $http.get(ENV.apiUrl + "api/EmpDocumentMaster/getEmpDocumentDetail").then(function (getEmpDocumentDetail_Data) {
                debugger
                $scope.EmpDocData = getEmpDocumentDetail_Data.data;

                if ($scope.EmpDocData.length > 0) {

                    $scope.pager = true;
                    if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.EmpDocData.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.EmpDocData.length, data: 'All' })
                    }
                    $scope.totalItems = $scope.EmpDocData.length;
                    $scope.todos = $scope.EmpDocData;
                    $scope.makeTodos();
                }
                else {

                    swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                    $scope.totalItems = $scope.EmpDocData.length;
                    $scope.todos = $scope.EmpDocData;
                    $scope.makeTodos();
                }

                console.log($scope.EmpDocData);
            })

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {

                        var v = document.getElementById($scope.filteredTodos[i].pays_doc_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].pays_doc_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }


            }

            $scope.check_once = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }

            $scope.deleterecord = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    data1 = [];

                    $scope.flag = false;
                    var deleteleave = [];
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].pays_doc_code + i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodulecode = ({
                                'pays_doc_code': $scope.filteredTodos[i].pays_doc_code,
                                opr: 'D'
                            });
                            deleteleave.push(deletemodulecode);
                        }
                    }
                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            confirmButtonText: 'Yes',
                            showCancelButton: true,
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                $http.post(ENV.apiUrl + "api/EmpDocumentMaster/CUDgetAllRecords", deleteleave).then(function (msg) {
                                    debugger
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getempoyeegrid();
                                            }
                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        });
                                    }
                                    else {
                                        swal({ text: "Record Already Mapped, can't Delete. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getempoyeegrid();
                                            }
                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        });
                                    }
                                });
                            }
                            else {
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById($scope.filteredTodos[i].pays_doc_code + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    $scope.currentPage = str;
                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.cancel = function () {
                $scope.EmployeeDetail = true;
                $scope.EmployeeDataoperation = false;
                $scope.searchemp = false;
                $('#myModal').modal('hide');
                $scope.edt = {};
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    data1 = [];
                    $scope.myForm.$setPristine();
                    $scope.edt = {};
                    $scope.newmode = true;
                    $scope.check = true;
                    $scope.edt = {
                        pays_doc_status: true,
                        pays_recruitment_status:true,
                        pays_available_on_short_form:true, 
                        pays_available_on_long_form: true,
                        pays_emp_visible_status: true,
                        pays_emp_upload_doc: true
                    }
                    //$scope.edt = '';
                    $scope.editmode = true;
                    //$scope.Status = false;
                    $scope.readonly = true;
                    $scope.EmployeeDetail = false;
                    $scope.EmployeeDataoperation = true;
                    $scope.savebtn = true;

                    //$scope.newbtn = false;
                    //$scope.updatebtn = true;
                    $scope.updatebtn = false;
                    
                }
            }

            $scope.Save = function (myForm) {
                debugger;
                
                data1 = [];
                if (myForm) {
                    var flag = false;
                    var data = {
                        pays_doc_created_by: username,
                        pays_doc_desc: $scope.edt.pays_doc_desc,
                        pays_doc_status: $scope.edt.pays_doc_status,
                        pays_recruitment_status: $scope.edt.pays_recruitment_status,
                        pays_available_on_short_form: $scope.edt.pays_available_on_short_form,
                        pays_available_on_long_form: $scope.edt.pays_available_on_long_form,
                        pays_emp_visible_status: $scope.edt.pays_emp_visible_status,
                        pays_emp_upload_doc: $scope.edt.pays_emp_upload_doc,                        
                        opr: 'I'
                    }
                    data1.push(data);
                    if ($scope.filteredTodos.length == 0)
                    {
                        flag = true;
                    }

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        if ($scope.filteredTodos[i].pays_doc_desc == $scope.edt.pays_doc_desc) {
                            swal({  text: "Record Already Exist", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                            flag = false;
                            break;
                        }

                        else {
                            flag = true;
                        }
                    }
                    if (flag == true) {
                        $http.post(ENV.apiUrl + "api/EmpDocumentMaster/CUDgetAllRecords", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            $scope.EmployeeDataoperation = false;
                            if ($scope.msg1 == true) {
                                swal({  text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                                $scope.cancel();
                            }
                            else if ($scope.msg1 == false) {
                                swal({  text: "Record not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                            else{
                                swal("Error-"+ $scope.msg1)
                            }
                            $scope.getempoyeegrid();
                            $scope.EmployeeDetail = true;
                        });
                    }
               }
            }

            $scope.up = function (objone) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    $scope.edt = {
                        pays_doc_status: objone.pays_doc_status,
                        pays_doc_desc: objone.pays_doc_desc,
                        pays_doc_mod_code: objone.pays_doc_mod_code,
                        pays_doc_code: objone.pays_doc_code,
                        pays_recruitment_status: objone.pays_recruitment_status,
                        pays_available_on_short_form: objone.pays_available_on_short_form,
                        pays_available_on_long_form: objone.pays_available_on_long_form,
                        pays_emp_visible_status: objone.pays_emp_visible_status,
                        pays_emp_upload_doc: objone.pays_emp_upload_doc
                    };
                    //$scope.newmode = true;
                    //$scope.check = true;
                    //$scope.edt = '';
                    //$scope.editmode = true;

                    $scope.readonly = true;
                    $scope.EmployeeDetail = false;
                    $scope.EmployeeDataoperation = true;
                    $scope.savebtn = false;
                    //$scope.newbtn = false;
                    $scope.updatebtn = true;
                    $scope.uploadbtn = false;
                    //$scope.edt = {};

                }
            }

            $scope.Update = function (myForm) {

                data1 = [];
                if (myForm) {
                    var data = ({
                        pays_doc_desc: $scope.edt.pays_doc_desc,
                        pays_doc_code: $scope.edt.pays_doc_code,
                        pays_doc_status: $scope.edt.pays_doc_status,
                        pays_recruitment_status: $scope.edt.pays_recruitment_status,
                        pays_available_on_short_form: $scope.edt.pays_available_on_short_form,
                        pays_available_on_long_form: $scope.edt.pays_available_on_long_form,
                        pays_emp_visible_status: $scope.edt.pays_emp_visible_status,
                        pays_emp_upload_doc: $scope.edt.pays_emp_upload_doc,
                        opr: 'U'
                    });

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/EmpDocumentMaster/CUDgetAllRecords", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.EmployeeDataoperation = false;
                        if ($scope.msg1 == true) {
                            swal({  text: "Record  updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            $scope.cancel();
                        }
                        else if ($scope.msg1 == false) {
                            swal({  text: "Record not updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $scope.getempoyeegrid();
                    });
                    $scope.EmployeeDetail = true;

                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
            $scope.createdate = function (end_date, start_date, name) {

                //var month1 = end_date.split("/")[0];
                //var day1 = end_date.split("/")[1];
                //var year1 = end_date.split("/")[2];
                //var new_end_date = year1 + "/" + month1 + "/" + day1;
                ////var new_end_date = day1 + "/" + month1 + "/" + year1;

                //var year = start_date.split("/")[0];
                //var month = start_date.split("/")[1];
                //var day = start_date.split("/")[2];
                //var new_start_date = year + "/" + month + "/" + day;
                // var new_start_date = day + "/" + month + "/" + year;

                if (end_date < start_date) {

                    $rootScope.strMessage = "Please Select Future Date";
                    $('#message').modal('show');

                    $scope.edt[name] = '';
                }
                else {

                    $scope.edt[name] = new_end_date;
                }
            }
            $scope.showdate = function (date, name) {

                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //$scope.edt[name] = year + "/" + month + "/" + day;
                $scope.edt[name] = date;
            }
            simsController.directive('ngFiles', ['$parse', function ($parse) {

                function fn_link(scope, element, attrs) {
                    var onChange = $parse(attrs.ngFiles);
                    element.on('change', function (event) {
                        onChange(scope, { $files: event.target.files });
                    });
                };

                return {
                    link: fn_link
                }
            }])

        }]);

})();