﻿(function () {
    'use strict';
    var main = '';
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('UpdateHoldEmpStatusCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.busyindicator = false;
            $scope.grid = true;
            $scope.color = '#edefef';
            $scope.rgvtbl = false;
            $scope.btnhide = true;
            $scope.edt = {};



            $http.get(ENV.apiUrl + "api/UpdateHoldEmpStatus/GetDept").then(function (GetDept) {
                $scope.GetDept = GetDept.data;

            });

            $http.get(ENV.apiUrl + "api/UpdateHoldEmpStatus/GetDesg").then(function (GetDesg) {
                $scope.GetDesg = GetDesg.data;

            });

            //$http.get(ENV.apiUrl + "api/UpdateHoldEmpStatus/GetYearMonth").then(function (GetYearMonth) {
            //    $scope.GetYearMonth = GetYearMonth.data;

            //});

            $http.get(ENV.apiUrl + "api/generatepayroll/GetCurrentFinYearFromFinsParameter").then(function (GetCurrentFinYearFromFinsParameter) {

                $scope.GetCurrentFinYearFromFinsParameter = GetCurrentFinYearFromFinsParameter.data;
                $scope.fin_year = $scope.GetCurrentFinYearFromFinsParameter[0].fin_year;
                $scope.fin_year_desc = $scope.GetCurrentFinYearFromFinsParameter[0].fin_year_desc;
            });

            $http.post(ENV.apiUrl + "api/OvertimeHour/Year_month").then(function (Year_month) {
                $scope.GetYearMonth = Year_month.data;
                var d = new Date().getFullYear();
                var n = new Date().getMonth() + 1;
                var ym = d + '' + n;
                ////$scope.edt.yearMonth = ym;
                //setTimeout(function () {
                //    $('#yearMonth').change(function () {
                //    }).multipleSelect({
                //        width: '100%'
                //    });
                //}, 1000);

                $scope.setYearMonth();
            });

            $scope.setYearMonth = function () {               
                $scope.edt.sd_year_month_code = $scope.edt.year_month.substr(4);
                console.log();
                //$scope.fin_year = $scope.edt.yearMonth.substr(0, 4);
            }

            $scope.Show_data = function () {
                debugger
                $scope.edt.sd_year_month_code = $scope.edt.year_month.substr(4);
                

                $http.post(ENV.apiUrl + "api/UpdateHoldEmpStatus/PaysheetDetails?dept=" + $scope.edt.dep_code + "&desg=" + $scope.edt.dg_code + "&year_month=" + $scope.fin_year + $scope.edt.sd_year_month_code + "&empcode=" + $scope.edt.emp_code).then(function (PaysheetDetails) {
                    $scope.PaysheetDetails = PaysheetDetails.data;

                    if ($scope.PaysheetDetails.length > 0) {
                        $scope.btnhide = false;
                        $scope.rgvtbl = true;
                    }
                    else {
                        $scope.rgvtbl = false;
                        $scope.btnhide = true;
                        swal('', 'Record not found.');
                    }
                });


            }




            $scope.genrate = function () {
               
                var datacoll = [];
                for (var i = 0; i < $scope.PaysheetDetails.length; i++) {
                    var data = {
                        emp_code: $scope.PaysheetDetails[i].emp_code,
                        year_month: $scope.fin_year + $scope.edt.sd_year_month_code,//$scope.PaysheetDetails[i].year_month,
                        onhold_status: $scope.PaysheetDetails[i].onhold_status,
                        pe_remark: $scope.PaysheetDetails[i].pe_remark
                    }
                    datacoll.push(data);
                }

                $http.post(ENV.apiUrl + "api/UpdateHoldEmpStatus/EmpUpdateOnHoldFlag", datacoll).then(function (msg1) {
                    $scope.msg = msg1.data;
                    if ($scope.msg)
                        swal('', 'Record updated successfully.');

                    $scope.reset_form();

                });
            }

            $scope.CheckAllChecked = function () {
               
                main = document.getElementById('mainchk');

                for (var i = 0; i < $scope.PaysheetDetails.length; i++) {
                    if (main.checked == true) {
                        $scope.PaysheetDetails[i].onhold_status = true;
                        $('tr').addClass("row_selected");
                    }

                    else {
                        $scope.PaysheetDetails[i].onhold_status = false;
                        $('tr').removeClass("row_selected");

                    }

                }
            }


            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);


            $scope.reset_form = function () {
                $scope.edt = '';
                $scope.PaysheetDetails = [];
                $scope.btnhide = true;
                $scope.rgvtbl = false;

            }


        }])
})();