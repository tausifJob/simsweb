﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AirFareCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.edt = {};
            $scope.showSaveBtn = true;
            $scope.showUpdateBtn = false;
            $scope.editDisabled = false;

            $http.post(ENV.apiUrl + "api/common/AirFare/grade").then(function (grade) {
                $scope.grade = grade.data;
            });

            $http.post(ENV.apiUrl + "api/common/AirFare/nations").then(function (nations) {
                $scope.nations = nations.data;
            });

            $scope.selectnat = function (str) {
                $http.post(ENV.apiUrl + "api/common/AirFare/destination?nationcode=" + str).then(function (destination) {
                    $scope.destination = destination.data;
                });
            }

            $http.post(ENV.apiUrl + "api/common/AirFare/Classget").then(function (Classget) {
                $scope.Classget = Classget.data;
            });

            $http.post(ENV.apiUrl + "api/common/AirFare/applicableMonth").then(function (applicableMonth) {
                $scope.applicableMonth = applicableMonth.data;
            });

            $http.post(ENV.apiUrl + "api/common/AirFare/accountName").then(function (accountName) {
                $scope.accountName = accountName.data;
            });
            $scope.busy = true;
            $scope.rgvtbl = false;
            $http.post(ENV.apiUrl + "api/common/AirFare/AirFareDetails").then(function (AirFareDetails) {
                $scope.AirFareDetails = AirFareDetails.data;
                console.log("AirFareDetails",$scope.AirFareDetails);
                $scope.busy = false;
                $scope.rgvtbl = true;
            });


            $http.get(ENV.apiUrl + "api/PaycodewiseEmployee/GetPayCode").then(function (GetPayCode) {
                $scope.GetPayCode = GetPayCode.data;
            });

            $scope.delete_data = function (str) {

                var data = {
                    gr_code: str.gr_code,
                    ds_code: str.ds_code,
                    nation_code: str.nation_code
                }

                swal({
                    title: '',
                    text: "Are you sure you want to delete?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        $http.post(ENV.apiUrl + "api/common/AirFare/AirFaredelete", data).then(function (AirFaredelete) {
                            $scope.AirFaredelete = AirFaredelete.data;
                            if ($scope.AirFaredelete)
                                swal('', 'Record deleted successfully.');
                            else
                                swal('', 'Record not deleted.' + $scope.AirFaredelete);
                            $scope.busy = true;
                            $scope.rgvtbl = false;
                            $http.post(ENV.apiUrl + "api/common/AirFare/AirFareDetails").then(function (AirFareDetails) {
                                $scope.AirFareDetails = AirFareDetails.data;
                                $scope.busy = false;
                                $scope.rgvtbl = true;
                            });
                        });
                    }                    
                })
            }


            $scope.select_chk_upgrade = function () {
                debugger;

            }

            $scope.save_data = function () {
                debugger;
                var upgrade_after = '', upgrade_freq = '';

                if ($scope.edt.upgrade_flag == undefined) {
                    $scope.edt.upgrade_flag = false;
                }

                if ($scope.edt.upgrade_flag) {
                    upgrade_after = $scope.edt.update_after;
                    upgrade_freq = $scope.edt.update_freq;
                }

                if ((upgrade_after == '' || upgrade_freq == '' || upgrade_after == undefined || upgrade_freq == undefined) && $scope.edt.upgrade_flag)
                    swal('', 'Upgrade after and upgrade frequence fields are mandatory.');
                else {



                    if ($scope.edt.gr_code == '' || $scope.edt.gr_code == undefined ||
                    $scope.edt.nation_code == '' || $scope.edt.nation_code == undefined ||
                    $scope.edt.ds_code == '' || $scope.edt.ds_code == undefined ||
                    $scope.edt.af_class == '' || $scope.edt.af_class == undefined ||
                    $scope.edt.af_applicable_frequency == '' || $scope.edt.af_applicable_frequency == undefined ||
                    $scope.edt.af_adult_rate == '' || $scope.edt.af_adult_rate == undefined ||
                    $scope.edt.pays_month_code == '' || $scope.edt.pays_month_code == undefined ||
                    $scope.edt.af_gl_acct_no == '' || $scope.edt.af_gl_acct_no == undefined) {
                        swal('', 'Please fill mandatory fields.');
                    }
                    else {
                        var data = {
                            gr_code: $scope.edt.gr_code,
                            ds_code: $scope.edt.ds_code,
                            nation_code: $scope.edt.nation_code,
                            af_class: $scope.edt.af_class,
                            af_applicable_frequency: $scope.edt.af_applicable_frequency,
                            af_adult_rate: $scope.edt.af_adult_rate,
                            af_applicable_month: $scope.edt.pays_month_code,
                            upgrade_flag: $scope.edt.upgrade_flag,
                            update_after: upgrade_after,
                            update_freq: upgrade_freq,
                            af_payroll_flag: $scope.edt.af_payroll_flag,
                            af_gl_acct_no: $scope.edt.af_gl_acct_no,
                            af_pay_code:$scope.edt.af_pay_code,
                            opr:'I'
                        }

                        debugger;
                        $http.post(ENV.apiUrl + "api/common/AirFare/insertAirFare", data).then(function (insertAirFare) {
                            $scope.insertAirFare = insertAirFare.data;
                            if ($scope.insertAirFare)
                                swal('', 'Air fare added successfully.');
                            else
                                swal('', 'Record not inserted. ' + $scope.insertAirFare);

                            $scope.busy = true;
                            $scope.rgvtbl = false;
                            $http.post(ENV.apiUrl + "api/common/AirFare/AirFareDetails").then(function (AirFareDetails) {
                                $scope.AirFareDetails = AirFareDetails.data;
                                $scope.busy = false;
                                $scope.rgvtbl = true;
                            });
                        });
                        $scope.reset_data();

                       
                    }
                }
            }

            $scope.edit = function (obj) {
                console.log(obj);
                $scope.showSaveBtn = false;
                $scope.showUpdateBtn = true;
                $scope.editDisabled = true;
                $scope.selectnat(obj.nation_code);
                $scope.edt = {
                    gr_code: obj.gr_code,
                    nation_code: obj.nation_code,
                    ds_code: obj.ds_code,
                    af_class: obj.af_class,
                    af_applicable_frequency: obj.af_applicable_frequency,
                    af_adult_rate: obj.af_adult_rate,
                    pays_month_code: obj.af_default_applicable_month,
                    af_payroll_flag: obj.af_payroll_flag,
                    af_gl_acct_no: obj.af_gl_acct_no,
                    upgrade_flag: obj.upgrade_flag,
                    update_after: obj.update_after,
                    update_freq: obj.update_freq,
                    af_pay_code:obj.af_pay_code
                }
            }

            $scope.update_data = function () {
                debugger;
                var upgrade_after = '', upgrade_freq = '';

                if ($scope.edt.upgrade_flag == undefined) {
                    $scope.edt.upgrade_flag = false;
                }

                if ($scope.edt.upgrade_flag) {
                    upgrade_after = $scope.edt.update_after;
                    upgrade_freq = $scope.edt.update_freq;
                }

                if ((upgrade_after == '' || upgrade_freq == '' || upgrade_after == undefined || upgrade_freq == undefined) && $scope.edt.upgrade_flag)
                    swal('', 'Upgrade after and upgrade frequence fields are mandatory.');
                else {



                    if ($scope.edt.gr_code == '' || $scope.edt.gr_code == undefined ||
                    $scope.edt.nation_code == '' || $scope.edt.nation_code == undefined ||
                    $scope.edt.ds_code == '' || $scope.edt.ds_code == undefined ||
                    $scope.edt.af_class == '' || $scope.edt.af_class == undefined ||
                    $scope.edt.af_applicable_frequency == '' || $scope.edt.af_applicable_frequency == undefined ||
                    $scope.edt.af_adult_rate == '' || $scope.edt.af_adult_rate == undefined ||
                    $scope.edt.pays_month_code == '' || $scope.edt.pays_month_code == undefined ||
                    $scope.edt.af_gl_acct_no == '' || $scope.edt.af_gl_acct_no == undefined) {
                        swal('', 'All fields are mandatory.');
                    }
                    else {
                        var data = {
                            gr_code: $scope.edt.gr_code,
                            ds_code: $scope.edt.ds_code,
                            nation_code: $scope.edt.nation_code,
                            af_class: $scope.edt.af_class,
                            af_applicable_frequency: $scope.edt.af_applicable_frequency,
                            af_adult_rate: $scope.edt.af_adult_rate,
                            af_applicable_month: $scope.edt.pays_month_code,
                            upgrade_flag: $scope.edt.upgrade_flag,
                            update_after: upgrade_after,
                            update_freq: upgrade_freq,
                            af_payroll_flag: $scope.edt.af_payroll_flag,
                            af_gl_acct_no: $scope.edt.af_gl_acct_no,
                            af_pay_code: $scope.edt.af_pay_code,
                            opr:'U'
                        }

                        console.log(data);
                        $http.post(ENV.apiUrl + "api/common/AirFare/insertAirFare", data).then(function (insertAirFare) {
                            $scope.insertAirFare = insertAirFare.data;
                            if ($scope.insertAirFare)
                                swal('', 'Record Updated successfully.');
                            else
                                swal('', 'Record not updated.' + $scope.insertAirFare);

                            $scope.busy = true;
                            $scope.rgvtbl = false;
                            $http.post(ENV.apiUrl + "api/common/AirFare/AirFareDetails").then(function (AirFareDetails) {
                                $scope.AirFareDetails = AirFareDetails.data;
                                $scope.busy = false;
                                $scope.rgvtbl = true;
                            });
                        });
                        $scope.reset_data();

                    }
                }
            }

            $scope.reset_data = function () {
                $scope.edt = {};
                $scope.showSaveBtn = true;
                $scope.showUpdateBtn = false;
                $scope.editDisabled = false;
            }


            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });

            }, 100);
        }])
})();