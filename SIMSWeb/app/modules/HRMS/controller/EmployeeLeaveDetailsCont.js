﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeLeaveDetailsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.EmpleaveType_data = [];
            var data1 = [];
            $scope.edt = [];

            $scope.username = $rootScope.globals.currentUser.username;

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $http.get(ENV.apiUrl + "api/common/MarkEmpResignation/getCompanydetails").then(function (res) {
                $scope.comp_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/EmployeeLeaveDetails/GetStatusforLeave").then(function (res) {
                $scope.status_data = res.data;
                for (var i = 0; i < $scope.status_data.length; i++)
                {
                    if ($scope.status_data[i].sims_workflow_status_code == 'N')
                    {
                        
                        $scope.edt['sims_workflow_status_code'] = $scope.status_data[i].sims_workflow_status_code;
                    }
                }
                $scope.statusdatachange($scope.edt['sims_workflow_status_code']);
            });


            $scope.statusdatachange = function (status_code)
            {
                $http.get(ENV.apiUrl + "api/common/EmployeeLeave/Get_EmpInfo?emp_id=" + $scope.username).then(function (res) {
                    $scope.emp_data = res.data;
                    if ($scope.emp_data.length > 0) {
                        $scope.comp_code = $scope.emp_data[0].comp_code;
                        console.log($scope.emp_data[0].comp_code);
                    }

                    $http.get(ENV.apiUrl + "api/common/EmployeeLeaveDetails/GetEMPLeaveDetails?comp_id=" + $scope.comp_code + "&emp_id=" + $scope.username + "&status_code=" + status_code).then(function (res) {
                        $scope.display = false;
                        $scope.grid = true;
                        $scope.EmpleaveType_data = res.data;
                        $scope.totalItems = $scope.EmpleaveType_data.length;
                        $scope.todos = $scope.EmpleaveType_data;
                        $scope.makeTodos();
                        $scope.grid = true;
                    });


                });
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.getcanLeave = function (info) {
                swal({
                    title: '',
                    text: "Do You Want to Cancel Leave?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        $http.post(ENV.apiUrl + "api/common/EmployeeLeaveDetails/CUDUpdate_pay_empleavedoc", info).then(function (res) {
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Leave Application Cancelled", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getGrid();

                                    }
                                });
                            }
                            else {
                                swal({ text: "Leave Application Not Cancelled. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getGrid();
                                    }
                                });
                            }

                        });
                    }
                });

            }

            $scope.getGrid = function () {
                $http.get(ENV.apiUrl + "api/common/EmployeeLeaveDetails/GetEMPLeaveDetails?comp_id=" + 1 + "&emp_id=" + $scope.username).then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.EmpleaveType_data = res.data;
                    $scope.totalItems = $scope.EmpleaveType_data.length;
                    $scope.todos = $scope.EmpleaveType_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                });
            }

            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.EmpleaveType_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.EmpleaveType_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.EmpleaveType_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.empName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.leave_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.viewDetails = function (lt_no) {
                debugger
                $('#Employee').modal('show');
                $http.get(ENV.apiUrl + "api/common/EmployeeLeave/GetStatusofTransactionforEmployee?transactionId=" + lt_no).then(function (res) {
                    $scope.Empleavedeatails_data = res.data;
                    // $scope.div_pendingview = false;
                    $scope.div_trans = true;
                });
            }

        }])
})();