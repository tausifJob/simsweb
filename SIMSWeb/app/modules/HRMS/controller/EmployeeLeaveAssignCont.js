﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeLeaveAssignCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.GetMapped_Employee = [];
            $scope.info1 = [];
            var demo = [];
            var demo1 = [];
            $scope.display = true;
            $scope.busyindicator = false;
            $scope.div_assign = true;
            $scope.table = false;
           
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            //$http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2015&routeCode=01").then(function (AllComboBoxValues) {
            //    $scope.ComboBoxValues = AllComboBoxValues.data;
            //    console.log($scope.ComboBoxValues);
            //});

            $http.get(ENV.apiUrl + "api/common/MarkEmpResignation/getCompanydetails").then(function (res) {
                $scope.comp_data = res.data;
                $scope.chkComp = angular.copy(res.data);
                console.log($scope.comp_data[0].com_code);
                // $scope.temp['co_company_code'] = $scope.comp_data[0].com_code;
                $scope.temp =
                    {
                        co_company_code: $scope.comp_data[0].com_code
                    }
                $scope.getdepartment($scope.temp.co_company_code);
            });

             var dt = new Date();
            $scope.edt = {};
            $scope.edt.em_date_of_join = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();


            $http.get(ENV.apiUrl + "api/EmployeeLeave/getGender").then(function (res) {
                $scope.gender_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/EmployeeLeave/getReligion").then(function (res) {
                $scope.religion_data = res.data;
            });

           
            $http.get(ENV.apiUrl + "api/common/LeaveType/Getleavetype").then(function (res) {
                $scope.stafftype_data = res.data;
                console.log($scope.stafftype_data);
            });

            $http.get(ENV.apiUrl + "api/EmployeeLeave/getOrderbynames").then(function (getOrderbynames) {
                $scope.getOrderbynames = getOrderbynames.data;
            });


            $scope.getdepartment = function (comp_code)
            {
                if (comp_code != 'undefined') {
                    $http.get(ENV.apiUrl + "api/common/MarkEmpResignation/getCompanyDepartments?company_code=" + comp_code).then(function (res) {
                        $scope.dept_data = res.data;
                        console.log($scope.dept_data);
                    });

                    $http.get(ENV.apiUrl + "api/EmployeeLeave/getGrade?company_code=" + comp_code).then(function (getGrade) {
                        $scope.getGrade = getGrade.data;
                    });

                }
            }

            $scope.getdesignation = function (comp_code, dept_code) {
                if (comp_code != 'undefined' && dept_code != 'undefined') {
                    $http.get(ENV.apiUrl + "api/EmployeeLeave/getdept_design?company_code=" + comp_code + "&dept_code=" + dept_code).then(function (res) {
                        $scope.desg_data = res.data;
                        console.log($scope.dept_data);
                    });
                }
            }

            $scope.Show = function (Myform) {
                demo1 = [];

                var data = $scope.temp

                if (Myform) {

                    if (data != " " && data != undefined) {
                        $scope.busy = true;
                        $scope.table = false;
                        $scope.div_assign = false;
                        $http.get(ENV.apiUrl + "api/EmployeeLeave/GetEmployeeLeaveAssign?simsobj=" + JSON.stringify($scope.temp)).then(function (GetMappedEmployee) {
                            $scope.GetMapped_Employee = GetMappedEmployee.data;

                            if (GetMappedEmployee.data.length > 0) {
                                for (var j = 0; j < $scope.GetMapped_Employee[0].leavelist.length; j++) {
                                    demo = {
                                        'emp_leave_name': $scope.GetMapped_Employee[0].leavelist[j].emp_leave_name,
                                        'emp_leave_code': $scope.GetMapped_Employee[0].leavelist[j].emp_leave_code
                                    }
                                    demo1.push(demo);
                                }
                                $scope.info1 = demo1;
                            }
                            else {

                                $scope.subject = true;

                            }
                            $scope.totalItems = $scope.GetMapped_Employee.length;
                            $scope.todos = $scope.GetMapped_Employee;
                            $scope.makeTodos();

                            if ($scope.GetMapped_Employee.length > 0) {

                                $scope.table = true;
                                $scope.busy = false;
                            }
                            else {
                                $scope.table = false;
                                $scope.busy = false;
                                swal({
                                    text: 'Data Not Available',
                                    width: 300,
                                    height: 300
                                });
                            }
                            $timeout(function () {
                                $("#fixedtable").tableHeadFixer({ 'top': 1 });
                            }, 100);

                        });
                    }
                    
                }
                else {
                    $scope.table = false;
                    $scope.busy = false;

                    swal({
                        text: 'Please select required fields',
                        width: 300,
                        height: 300
                    });
                }

            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                if (str == "All")
                {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else
                {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.Reset = function () {

                $scope.temp = '';
                $scope.table = false;

                $scope.comp_data = $scope.chkComp;
                $scope.temp =
                    {
                        co_company_code: $scope.comp_data[0].com_code
                    }
                $scope.getdepartment($scope.temp.co_company_code);

            }

            $scope.Check_Single_Leave = function (info,info1,index)
            {
                debugger
                info.ischange = true;
                $scope.flag = false;
               
                for (var j = 0; j < info.leavelist.length; j++)
                {
                    //if (info.leavelist[j].emp_leave_status == true)
                    if (info.leavelist[index].ischange_new == false)
                    {
                        $scope.flag = true;
                        info.leavelist[index].leave_checked = true;
                    }
                    else
                    {
                        info.leavelist[index].leave_unchecked = true;
                        $scope.flag = false;
                        break;
                    }
                 
                }
                //if ($scope.flag)
                //{
                //    var v = document.getElementById(info.emp_code);
                //    v.checked = true;
                //}
                //else
                //{
                //    var v = document.getElementById(info.emp_code);
                //    v.checked = false;
                //}

                $scope.flag1 = false;
               
                for (var i = 0; i < $scope.filteredTodos.length; i++)
                {
                    for (var j = 0; j < $scope.filteredTodos[i].leavelist.length; j++)
                    {
                            if (info1[index].emp_leave_code == $scope.filteredTodos[i].leavelist[j].emp_leave_code)
                            {
                                if ($scope.filteredTodos[i].leavelist[j].emp_leave_status == true)
                                { 
                                    $scope.flag1 = true;
                                }
                                else
                                {
                                    $scope.flag1 = false;
                                    break;
                                }
                            }
                    }
                    if ($scope.flag1 == false)
                    {
                        break;
                    }

                }
                
                //if ($scope.flag1)
                //{
                //    var v = document.getElementById(info1[index].emp_leave_code);
                //    v.checked = true;
                //}
                //else 
                //{
                //    var v = document.getElementById(info1[index].emp_leave_code);
                //    v.checked = false;
                //}
            }

            $scope.Select_ALL_Leave = function (str)
            {
                debugger
                var check = document.getElementById(str);
                for (var i = 0; i < $scope.filteredTodos.length; i++)
                {
                   // $scope.filteredTodos[i].leavelist[check.tabIndex].emp_leave_status = check.checked;
                    //$scope.filteredTodos[i].emp_leave_status = check.checked;
                    for (var k = 0; k < $scope.filteredTodos[i].leavelist.length; k++)
                        if ($scope.filteredTodos[i].leavelist[k].emp_leave_code == str)
                        {
                            if (check.checked) {
                                $scope.filteredTodos[i].leavelist[k].emp_leave_status = check.checked;
                                $scope.filteredTodos[i].leavelist[k].leave_checked = true;
                            }
                            else {                                 
                                $scope.filteredTodos[i].leavelist[k].leave_unchecked = true;
                                $scope.filteredTodos[i].leavelist[k].emp_leave_status = check.checked;                                 
                            }                            
                        }
                    $scope.filteredTodos[i].ischange = true;
                }
            }

            $scope.Select_Emp_ALL_Leave = function (str)
            {
                var check = document.getElementById(str);
                for (var i = 0; i < $scope.filteredTodos.length; i++)
                {
                    if ($scope.filteredTodos[i].emp_code == str) {
                        for (var j = 0; j < $scope.filteredTodos[i].leavelist.length; j++) {
                            
                            if ($scope.filteredTodos[i].leavelist[j].emp_leave_status == false) {
                                $scope.filteredTodos[i].leavelist[j].emp_leave_status = check.checked;
                                $scope.filteredTodos[i].leavelist[j].leave_checked = true;
                            }
                            else {
                                $scope.filteredTodos[i].leavelist[j].leave_unchecked = true;
                                $scope.filteredTodos[i].leavelist[j].emp_leave_status = check.checked;
                            }
                            $scope.filteredTodos[i].ischange = true;
                        }
                    }
                }
            }

           $scope.searched = function (valLists, toSearch) {
            return _.filter(valLists,

            function (i) {
                /* Search Text in all  fields */
                return searchUtil(i, toSearch);
            });
        };

          //Search
           $scope.search = function ()
           {
             $scope.todos = $scope.searched($scope.GetMapped_Employee, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.GetMapped_Employee;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked()

            }

           function searchUtil(item, toSearch)
           {
                /* Search Text in all 3 fields */
            return (item.employee_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.emp_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
           }


           $scope.Submit = function ()
           {    debugger
                var senddata = [];
                var data = [];
                $scope.table = false;
                var Sdata = [];
                $scope.insert = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++)
                {
                    if ($scope.filteredTodos[i].ischange == true)
                    {
                        var le = Sdata.length;
                        data =
                            {
                            emp_code: $scope.filteredTodos[i].emp_code,
                            co_company_code: $scope.temp.co_company_code,
                            codp_dept_no: $scope.temp.codp_dept_no,
                            dg_code: $scope.temp.dg_code,
                            sims_appl_parameter: $scope.temp.emp_gender_code,
                            leave_name: '',
                            user_name: $rootScope.globals.currentUser.username
                            };
                        var sbcode = '';
                        var lavedeletecode = '';
                        for (var j = 0; j < $scope.filteredTodos[i].leavelist.length; j++) {
                           // if ($scope.filteredTodos[i].leavelist[j].emp_leave_status)//&& $scope.filteredTodos[i].leavelist[j].ischange_new == true)
                            if ($scope.filteredTodos[i].leavelist[j].leave_checked)
                            {
                                sbcode = sbcode + $scope.filteredTodos[i].leavelist[j].emp_leave_code + ',';
                            }
                            if ($scope.filteredTodos[i].leavelist[j].leave_unchecked) {
                                lavedeletecode = lavedeletecode + $scope.filteredTodos[i].leavelist[j].emp_leave_code + ',';
                            }
                        }
                        data.leave_name = sbcode;
                        data.leave_delete_list = lavedeletecode;
                        $scope.insert = true;
                        senddata.push(data);
                    }
                }
                $scope.busy = true;

                console.log(sbcode);
                if (sbcode == undefined) {
                    swal({ text: 'Please select Atleast one Record', imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, height: 300 });
                    $scope.Show(true);
                    return;
                }
                else
                {
                    $http.post(ENV.apiUrl + "api/EmployeeLeave/InsertEmployeeLeaveAssign", senddata).then(function (InsertEmployeeLeaveAssign) {
                        $scope.EmployeeLeaveAssign = InsertEmployeeLeaveAssign.data;
                        console.log($scope.EmployeeLeaveAssign);
                        $scope.table = true;
                        if ($scope.EmployeeLeaveAssign == false) {
                            swal({ text: 'Employee Leave Not Assigned. OR Leave is Allocated ', imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, height: 300 });
                            $scope.Show(true);
                        }
                        else {
                            swal({ text: 'Employee Leave Assigned', imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, height: 300 });
                            $scope.Show(true);
                        }

                    });
                    $scope.busy = false;
                }

                Sdata = [];
                data = [];
                sbcode = "";
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


            //$timeout(function () {
            //    $("#fixedtable").tableHeadFixer({ 'top': 1 });
            //}, 100);
            $scope.select_date = function (date) {
                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //var date1 = year + "-" + month + "-" + day;
                //$scope.temp.em_date_of_join = date1;
                $scope.edt.em_date_of_join = date;
            }
        }])
})();