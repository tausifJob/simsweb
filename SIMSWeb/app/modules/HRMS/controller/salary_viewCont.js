﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('salary_viewCont',
    ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, $filter, ENV) {

        var user = $rootScope.globals.currentUser.username;
        $http.get(ENV.apiUrl + "api/LibraryAttribute/get_emp_sal_year").then(function (res) {
            $scope.year_data = res.data;
            $scope.year_code = $scope.year_data[0].year_code;
        });

        $http.get(ENV.apiUrl + "api/LibraryAttribute/get_emp_sal_month").then(function (res) {
            $scope.month_data = res.data;
            console.log($scope.month_data);
            $scope.sd_year_month = $scope.month_data[0].sd_year_month;
        });

        $scope.get_emp_salary = function (year, month) {
            var year_month = year + month;
            $http.get(ENV.apiUrl + "api/LibraryAttribute/get_emp_salary?year=" + year_month + "&user=" + user).then(function (res) {
                $scope.emp_data = res.data;
                $scope.get_emp_attendance($scope.year_code, $scope.sd_year_month);
            });
        }

        $scope.get_emp_attendance = function (year, month) {
            debugger
            var att_year = year + month;
            $http.get(ENV.apiUrl + "api/LibraryAttribute/get_emp_attendance?year=" + att_year + "&user=" + user).then(function (res) {
                $scope.att_data = res.data[0];
            });
        }

        $scope.reset_form = function () {
            debugger
            $scope.emp_data = [];
            $scope.att_data = [];
        }
}])

})();
