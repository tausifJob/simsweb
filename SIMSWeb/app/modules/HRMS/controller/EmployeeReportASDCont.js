﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, modulecode = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeReportASDCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.chk_url();

            $scope.arry = [];
            //$scope.exportData = function () {
            //    var blob = new Blob([document.getElementById('exportable1').innerHTML], {
            //        type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
            //    });
            //    saveAs(blob, "Report.xls");
            //};

            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);


            $scope.exportData = function () {
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        //$scope.chk_hide();
                        alasql('SELECT * INTO XLSX("Report.xlsx",{headers:true})\FROM HTML("#customers",{headers:true,skipdisplaynone:true})');
                    }
                });
            };


            $scope.items = [{
                "Name": "ANC101",
                "Date": "10/02/2014",
                "Terms": ["samsung", "nokia", "apple"],
                "temp": "ANC101"

            }, {
                "Name": "ABC102",
                "Date": "10/02/2014",
                "Terms": ["motrolla", "nokia", "iPhone"],
                "temp": "ANC102"

            }]

            $scope.header = ['name', 'dob']

            $scope.data = [
                {
                    name: 'xyz',
                    dob: '10/02/2014'
                },
                {
                    name: 'abc',
                    dob: '10/02/2014'
                }
            ]


            $scope.busyindicator = false;

            $http.get(ENV.apiUrl + "api/EmployeeReport/GetCompanyNameForShift").then(function (res) {
                $scope.cmbCompany = res.data;
            })

            $http.get(ENV.apiUrl + "api/EmployeeReport/GetDepartmentName").then(function (res) {
                $scope.cmbDepartment = res.data;
            })


            $scope.companyChange = function (str) {
                $http.get(ENV.apiUrl + "api/EmployeeReport/GetDesignation?com_code=" + str).then(function (res) {
                    $scope.cmbDesignation = res.data;
                })
            }

            $http.get(ENV.apiUrl + "api/EmployeeReport/GetServiceStatus").then(function (dres) {
                $scope.empservice = dres.data;
            })

            $scope.Show_Data = function () {
                //$scope.table = true;
                $scope.busyindicator = true;
                debugger;
                $http.get(ENV.apiUrl + "api/EmployeeReport/GetEmployeeReportAsd?comp=" + $scope.sh_company_code + "&desg=" + $scope.desi_code + "&dept=" + $scope.dept_no + "&sstatus=" + $scope.sesi_code + '&emp_id=' + $scope.emp_id + '&name=' + $scope.emp_name).then(function (res) {
                    $scope.empList = res.data;
                    if ($scope.empList.length == 0) {
                        swal({ title: 'Alert', text: "No Records Found...", showCloseButton: true, width: 450, height: 200 });
                        $scope.busyindicator = false;
                        $scope.table = false;
                    }
                    else {
                        $scope.table = true;
                    }
                })
            }

            $scope.Reset = function () {
                $scope.sh_company_code = '';
                $scope.dept_no = '';
                $scope.desi_code = '';
                $scope.sesi_code = '';
                $scope.select_all = false;
                $scope.checkallClick(false);
            }

            $scope.back = function () {
                $scope.table = false;
                $scope.busyindicator = false;
            }


            $scope.checkallClick = function (str) {
                if (str) {
                    $scope.chk_em_company_name = true;
                    $scope.chk_em_qualification = true;
                    $scope.chk_em_dept_name = true;
                    $scope.chk_em_desg_desc = true;
                    $scope.chk_em_visa_issue_date = true;
                    $scope.chk_em_login_code = true;
                    $scope.chk_em_dept_effect_from = true;
                    $scope.chk_em_dest_desc = true;
                    $scope.chk_em_grade_name = true;
                    $scope.chk_em_number = true;
                    $scope.chk_em_visa_exp_date = true;
                    $scope.chk_em_grade_effect_from = true;
                    $scope.chk_em_staff_type = true;
                    $scope.chk_em_full_name = true;
                    $scope.chk_em_apartment_number = true;
                    $scope.chk_em_visa_exp_rem_date = true;
                    $scope.chk_em_family_name = true;
                    $scope.chk_em_name_ot = true;
                    $scope.chk_em_date_of_birth = true;
                    $scope.chk_em_building_number = true;
                    $scope.chk_em_visa_issuing_place = true;
                    $scope.chk_em_sex = true;
                    $scope.chk_em_marital_status = true;
                    $scope.chk_em_religion_name = true;
                    $scope.chk_em_street_number = true;
                    $scope.chk_em_visa_issuing_authority = true;

                    $scope.chk_em_etdnicity_name = true;
                    $scope.chk_em_summary_address = true;
                    $scope.chk_em_city = true;
                    $scope.chk_em_area_number = true;
                    $scope.chk_em_visa_type = true;
                    $scope.chk_em_state = true;
                    $scope.chk_em_country = true;
                    $scope.chk_em_phone = true;
                    $scope.chk_em_summary_address_local_language = true;
                    $scope.chk_em_mobile = true;
                    $scope.chk_em_email = true;
                    $scope.chk_em_fax = true;
                    $scope.chk_em_po_fax = true;
                    $scope.chk_em_dependant_full = true;
                    $scope.chk_em_dependant_half = true;
                    $scope.chk_em_emergency_contact_name1 = true;
                    $scope.chk_em_emergency_contact_name2 = true;
                    $scope.chk_em_emergency_contact_number1 = true;
                    $scope.chk_em_emergency_contact_number2 = true;
                    $scope.chk_em_date_of_join = true;
                    $scope.chk_em_service_status = true;
                    $scope.chk_em_ledger_ac_no = true;
                    $scope.chk_em_gpf_ac_no = true;
                    $scope.chk_em_gosi_ac_no = true;
                    $scope.chk_em_gosi_start_date = true;
                    $scope.chk_em_pan_no = true;
                    $scope.chk_em_iban_no = true;


                    $scope.chk_em_labour_card_no = true;
                    $scope.chk_em_status = true;
                    $scope.chk_em_leave_resume_date = true;
                    $scope.chk_em_joining_ref = true;
                    $scope.chk_em_bank_code = true;
                    $scope.chk_em_bank_ac_no = true;
                    $scope.chk_em_bank_swift_code = true;
                    $scope.chk_em_leave_start_date = true;
                    $scope.chk_em_leave_end_date = true;
                    $scope.chk_em_stop_salary_from = true;
                    $scope.chk_em_unpaid_leave = true;
                    $scope.chk_em_punching_id = true;
                    $scope.chk_em_passport_no = true;
                    $scope.chk_em_ethnicity_name = true;

                    $scope.chk_em_passport_issue_date = true;
                    $scope.chk_em_pssport_exp_rem_date = true;
                    $scope.chk_em_passport_exp_date = true;
                    $scope.chk_em_passport_ret_date = true;
                    $scope.chk_em_passport_issuing_authority = true;

                    $scope.chk_em_passport_issue_place = true;
                    $scope.chk_em_national_id = true;

                    $scope.chk_em_national_id_issue_date = true;
                    $scope.chk_em_national_id_expiry_date = true;
                    $scope.chk_em_modified_on = true;
                    $scope.chk_em_pwd = true;
                    $scope.chk_em_secret_question_code = true;
                    $scope.chk_em_secret_answer = true;
                    $scope.chk_em_agreement_start_date = true;
                    $scope.chk_em_agreement_exp_date = true;
                    $scope.chk_em_agreemet_exp_rem_date = true;
                    $scope.chk_em_dependant_infant = true;
                }

                else {
                    $scope.chk_em_company_name = false;
                    $scope.chk_em_qualification = false;
                    $scope.chk_em_dept_name = false;
                    $scope.chk_em_desg_desc = false;
                    $scope.chk_em_visa_issue_date = false;
                    $scope.chk_em_login_code = false;
                    $scope.chk_em_dept_effect_from = false;
                    $scope.chk_em_dest_desc = false;
                    $scope.chk_em_grade_name = false;
                    $scope.chk_em_number = false;
                    $scope.chk_em_visa_exp_date = false;
                    $scope.chk_em_grade_effect_from = false;
                    $scope.chk_em_staff_type = false;
                    $scope.chk_em_full_name = false;
                    $scope.chk_em_apartment_number = false;
                    $scope.chk_em_visa_exp_rem_date = false;
                    $scope.chk_em_family_name = false;
                    $scope.chk_em_name_ot = false;
                    $scope.chk_em_ethnicity_name = false;

                    $scope.chk_em_date_of_birth = false;
                    $scope.chk_em_building_number = false;
                    $scope.chk_em_visa_issuing_place = false;
                    $scope.chk_em_sex = false;
                    $scope.chk_em_marital_status = false;
                    $scope.chk_em_religion_name = false;
                    $scope.chk_em_street_number = false;
                    $scope.chk_em_visa_issuing_authority = false;
                    $scope.chk_em_etdnicity_name = false;
                    $scope.chk_em_summary_address = false;
                    $scope.chk_em_city = false;
                    $scope.chk_em_area_number = false;
                    $scope.chk_em_visa_type = false;
                    $scope.chk_em_state = false;
                    $scope.chk_em_country = false;
                    $scope.chk_em_phone = false;
                    $scope.chk_em_summary_address_local_language = false;
                    $scope.chk_em_mobile = false;
                    $scope.chk_em_email = false;
                    $scope.chk_em_fax = false;
                    $scope.chk_em_po_fax = false;

                    $scope.chk_em_dependant_infant = false;

                    $scope.chk_em_dependant_full = false;
                    $scope.chk_em_dependant_half = false;
                    $scope.chk_em_emergency_contact_name1 = false;
                    $scope.chk_em_emergency_contact_name2 = false;
                    $scope.chk_em_emergency_contact_number1 = false;
                    $scope.chk_em_emergency_contact_number2 = false;
                    $scope.chk_em_date_of_join = false;
                    $scope.chk_em_service_status = false;
                    $scope.chk_em_ledger_ac_no = false;
                    $scope.chk_em_gpf_ac_no = false;
                    $scope.chk_em_gosi_ac_no = false;
                    $scope.chk_em_gosi_start_date = false;
                    $scope.chk_em_pan_no = false;
                    $scope.chk_em_iban_no = false;

                    $scope.chk_em_labour_card_no = false;
                    $scope.chk_em_status = false;
                    $scope.chk_em_leave_resume_date = false;
                    $scope.chk_em_joining_ref = false;
                    $scope.chk_em_bank_code = false;
                    $scope.chk_em_bank_ac_no = false;
                    $scope.chk_em_bank_swift_code = false;
                    $scope.chk_em_leave_start_date = false;
                    $scope.chk_em_leave_end_date = false;
                    $scope.chk_em_stop_salary_from = false;
                    $scope.chk_em_unpaid_leave = false;
                    $scope.chk_em_punching_id = false;
                    $scope.chk_em_passport_no = false;
                    $scope.chk_em_passport_issuing_authority = false;

                    $scope.chk_em_passport_issue_date = false;
                    $scope.chk_em_pssport_exp_rem_date = false;
                    $scope.chk_em_passport_exp_date = false;
                    $scope.chk_em_passport_ret_date = false;
                    $scope.chk_em_passport_issuing_authority = false;
                    $scope.chk_em_passport_issue_place = false;
                    $scope.chk_em_national_id = false;

                    $scope.chk_em_national_id_issue_date = false;
                    $scope.chk_em_national_id_expiry_date = false;
                    $scope.chk_em_modified_on = false;
                    $scope.chk_em_pwd = false;
                    $scope.chk_em_secret_question_code = false;
                    $scope.chk_em_secret_answer = false;
                    $scope.chk_em_agreement_start_date = false;
                    $scope.chk_em_agreement_exp_date = false;
                    $scope.chk_em_agreemet_exp_rem_date = false;
                }
            }


            //$scope.chk_hide = function () {
            //    $scope.arry.push(0)

            //    if (!chk_em_company_name) {
            //        $scope.arry.push(parseInt(1) + 0)
            //    }
            //    if (!chk_em_dept_name) {
            //        $scope.arry.push(parseInt(1) + 1)
            //    }
            //    if (!chk_em_desg_desc) {
            //        $scope.arry.push(parseInt(1) + 2)
            //    }

            //    if (!chk_em_visa_issue_date) {
            //        $scope.arry.push(parseInt(1) + 3)
            //    }
            //    if (!chk_em_login_code) {
            //        $scope.arry.push(parseInt(1) + 4)
            //    }
            //    if (!chk_em_dept_effect_from) {
            //        $scope.arry.push(parseInt(1) + 5)
            //    }
            //    if (!chk_em_dest_desc) {
            //        $scope.arry.push(parseInt(1) + 6)
            //    }
            //    if (!chk_em_grade_name) {
            //        $scope.arry.push(parseInt(1) + 7)
            //    }
            //    if (!chk_em_number) {
            //        $scope.arry.push(parseInt(1) + 8)
            //    }
            //    if (!chk_em_visa_exp_date) {
            //        $scope.arry.push(parseInt(1) + 9)
            //    }
            //    if (!chk_em_grade_effect_from) {
            //        $scope.arry.push(parseInt(1) + 10)
            //    }
            //    if (!chk_em_staff_type) {
            //        $scope.arry.push(parseInt(1) + 11)
            //    }

            //    if (!chk_em_full_name) {
            //        $scope.arry.push(parseInt(1) + 12)
            //    }
            //    if (!chk_em_apartment_number) {
            //        $scope.arry.push(parseInt(1) + 13)
            //    }
            //    if (!chk_em_visa_exp_rem_date) {
            //        $scope.arry.push(parseInt(1) + 14)
            //    }
            //    if (!chk_em_family_name) {
            //        $scope.arry.push(parseInt(1) + 15)
            //    }

            //    if (!chk_em_name_ot) {
            //        $scope.arry.push(parseInt(1) + 16)
            //    }

            //    if (!chk_em_date_of_birth) {
            //        $scope.arry.push(parseInt(1) + 17)
            //    }
            //    if (!chk_em_building_number) {
            //        $scope.arry.push(parseInt(1) + 18)
            //    }
            //    if (!chk_em_visa_issuing_place) {
            //        $scope.arry.push(parseInt(1) + 19)
            //    }
            //    if (!chk_em_sex) {
            //        $scope.arry.push(parseInt(1) + 20)
            //    }
            //    if (!chk_em_marital_status) {
            //        $scope.arry.push(parseInt(1) + 21)
            //    }
            //    if (!chk_em_religion_name) {
            //        $scope.arry.push(parseInt(1) + 22)
            //    }
            //    if (!chk_em_street_number) {
            //        $scope.arry.push(parseInt(1) + 23)
            //    }
            //    if (!chk_em_visa_issuing_authority) {
            //        $scope.arry.push(parseInt(1) + 24)
            //    }
            //    if (!chk_em_ethnicity_name) {
            //        $scope.arry.push(parseInt(1) + 25)
            //    }
            //    if (!chk_em_summary_address) {
            //        $scope.arry.push(parseInt(1) + 26)
            //    }
            //    if (!chk_em_city) {
            //        $scope.arry.push(parseInt(1) + 27)
            //    }
            //    if (!chk_em_area_number) {
            //        $scope.arry.push(parseInt(1) + 28)
            //    }
            //    if (!chk_em_visa_type) {
            //        $scope.arry.push(parseInt(1) + 29)
            //    }
            //    if (!chk_em_state) {
            //        $scope.arry.push(parseInt(1) + 30)
            //    }
            //    if (!chk_em_country) {
            //        $scope.arry.push(parseInt(1) + 31)
            //    }
            //    if (!chk_em_phone) {
            //        $scope.arry.push(parseInt(1) + 32)
            //    }
            //    if (!chk_em_summary_address_local_language) {
            //        $scope.arry.push(parseInt(1) + 33)
            //    }
            //    if (!chk_em_mobile) {
            //        $scope.arry.push(parseInt(1) + 34)
            //    }
            //    if (!chk_em_email) {
            //        $scope.arry.push(parseInt(1) + 35)
            //    }
            //    if (!chk_em_fax) {
            //        $scope.arry.push(parseInt(1) + 36)
            //    }
            //    if (!chk_em_po_fax) {
            //        $scope.arry.push(parseInt(1) + 37)
            //    }
            //    if (!chk_em_dependant_full) {
            //        $scope.arry.push(parseInt(1) + 38)
            //    }
            //    if (!chk_em_dependant_half) {
            //        $scope.arry.push(parseInt(1) + 39)
            //    }
            //    if (!chk_em_dependant_infant) {
            //        $scope.arry.push(parseInt(1) + 40)
            //    }
            //    if (!chk_em_emergency_contact_name1) {
            //        $scope.arry.push(parseInt(1) + 41)
            //    }
            //    if (!chk_em_emergency_contact_name2) {
            //        $scope.arry.push(parseInt(1) + 42)
            //    }
            //    if (!chk_em_emergency_contact_number1) {
            //        $scope.arry.push(parseInt(1) + 43)
            //    }
            //    if (!chk_em_emergency_contact_number2) {
            //        $scope.arry.push(parseInt(1) + 44)
            //    }
            //    if (!chk_em_date_of_join) {
            //        $scope.arry.push(parseInt(1) + 45)
            //    }
            //    if (!chk_em_service_status) {
            //        $scope.arry.push(parseInt(1) + 46)
            //    }
            //    //if (!chk_em_ledger_ac_no) {
            //    //    $scope.arry.push(parseInt(1)+47)
            //    //}
            //    if (!chk_em_gpf_ac_no) {
            //        $scope.arry.push(parseInt(1) + 48)
            //    }
            //    if (!chk_em_gosi_ac_no) {
            //        $scope.arry.push(parseInt(1) + 49)
            //    }
            //    if (!chk_em_gosi_start_date) {
            //        $scope.arry.push(parseInt(1) + 50)
            //    }
            //    if (!chk_em_pan_no) {
            //        $scope.arry.push(parseInt(1) + 51)
            //    }
            //    if (!chk_em_iban_no) {
            //        $scope.arry.push(parseInt(1) + 52)
            //    }
            //    if (!chk_em_labour_card_no) {
            //        $scope.arry.push(parseInt(1) + 53)
            //    }
            //    if (!chk_em_status) {
            //        $scope.arry.push(parseInt(1) + 54)
            //    }
            //    if (!chk_em_leave_resume_date) {
            //        $scope.arry.push(parseInt(1) + 55)
            //    }
            //    if (!chk_em_joining_ref) {
            //        $scope.arry.push(parseInt(1) + 56)
            //    }
            //    if (!chk_em_bank_code) {
            //        $scope.arry.push(parseInt(1) + 57)
            //    }
            //    if (!chk_em_bank_ac_no) {
            //        $scope.arry.push(parseInt(1) + 58)
            //    }
            //    if (!chk_em_bank_swift_code) {
            //        $scope.arry.push(parseInt(1) + 59)
            //    }
            //    if (!chk_em_leave_start_date) {
            //        $scope.arry.push(parseInt(1) + 60)
            //    }
            //    if (!chk_em_leave_end_date) {
            //        $scope.arry.push(parseInt(1) + 61)
            //    }
            //    if (!chk_em_stop_salary_from) {
            //        $scope.arry.push(parseInt(1) + 62)
            //    }
            //    if (!chk_em_unpaid_leave) {
            //        $scope.arry.push(parseInt(1) + 63)
            //    }
            //    if (!chk_em_punching_id) {
            //        $scope.arry.push(parseInt(1) + 64)
            //    }
            //    if (!chk_em_passport_no) {
            //        $scope.arry.push(parseInt(1) + 65)
            //    }
            //    if (!chk_em_passport_issue_date) {
            //        $scope.arry.push(parseInt(1) + 66)
            //    }
            //    if (!chk_em_pssport_exp_rem_date) {
            //        $scope.arry.push(parseInt(1) + 67)
            //    }
            //    if (!chk_em_passport_exp_date) {
            //        $scope.arry.push(parseInt(1) + 68)
            //    }
            //    if (!chk_em_passport_ret_date) {
            //        $scope.arry.push(parseInt(1) + 69)
            //    }
            //    if (!chk_em_passport_issuing_authority) {
            //        $scope.arry.push(parseInt(1) + 70)
            //    }
            //    if (!chk_em_passport_issue_place) {
            //        $scope.arry.push(parseInt(1) + 71)
            //    }
            //    if (!chk_em_national_id) {
            //        $scope.arry.push(parseInt(1) + 72)
            //    }
            //    if (!chk_em_national_id_issue_date) {
            //        $scope.arry.push(parseInt(1) + 73)
            //    }
            //    if (!chk_em_national_id_expiry_date) {
            //        $scope.arry.push(parseInt(1) + 74)
            //    }
            //    if (!chk_em_modified_on) {
            //        $scope.arry.push(parseInt(1) + 75)
            //    }
            //    if (!chk_em_pwd) {
            //        $scope.arry.push(parseInt(1) + 76)
            //    }
            //    if (!chk_em_secret_question_code) {
            //        $scope.arry.push(parseInt(1) + 77)
            //    }
            //    if (!chk_em_secret_answer) {
            //        $scope.arry.push(parseInt(1) + 78)
            //    }
            //    if (!chk_em_agreement_start_date) {
            //        $scope.arry.push(parseInt(1) + 79)
            //    }
            //    if (!chk_em_agreement_exp_date) {
            //        $scope.arry.push(parseInt(1) + 80)
            //    }
            //    if (!chk_em_agreemet_exp_rem_date) {
            //        $scope.arry.push(parseInt(1) + 81)
            //    }
            //    $('#customers').tableExport({ type: 'excel', escape: 'false', ignoreColumn: $scope.arry });
            //}
        }])
})();
