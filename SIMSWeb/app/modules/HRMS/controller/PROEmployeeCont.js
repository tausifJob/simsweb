﻿(function () {
    'use strict';
    var main, temp1, opr, edt;
    var simsController = angular.module('sims.module.HRMS');

    simsController.controller('PROEmployeeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.imageUrl = ENV.apiUrl + '/Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/';

            $scope.edt = {};
            $scope.temp = {};
            $scope.popobj = {};
            $scope.doc = {};
            debugger;
            $http.get(ENV.apiUrl + "api/HRMS/lems/getEmpMasterData").then(function (res) {

                $scope.empstat = true;
                $scope.obj = res.data;
                console.log("obj ", $scope.obj);

                $scope.operation = true;
                $scope.Save_btn = true;
                $scope.Update_btn = false;
                $scope.display = true;
                $scope.roleList = true;
                $scope.temp.em_Company_Code = $scope.obj[0].em_Company_Code;
                $scope.temp.em_service_status_code = 'V';
            });


            $scope.searchEmp = function ($event) {

                if ($event.keyCode == 13) {
                    $scope.EmpSearch1($scope.temp.enroll_number1);
                }
            }

            $scope.showAgreementDetails = true;
            $scope.uae = false;
            $scope.qatar = true;
            $scope.slisLabel = false;
            $scope.india = false;
            $scope.updateBtn = false;
            $scope.saveBtn = false;
            $scope.combo_multiple = false;
            $scope.addDisabled = true;
            $scope.combo_multiple = false;
            $scope.qualiList = [];
            $scope.ExperienceList = [];
            var fortype = '';
            $scope.showFileName = false;
            $scope.notAllowedToEdit = true;

            $("#dob").kendoDatePicker({
                format: "dd-MM-yyyy",
                min: new Date(1900, 1, 1),
                max: new Date()
            });

            $('#dob').data('kendoDatePicker').enable(false);
            $('#DOJ').data('kendoDatePicker').enable(false);

            $scope.globalSrch = function () {

                $rootScope.visible_stud = false;
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_teacher = false;
                $rootScope.visible_User = false;
                $rootScope.visible_Employee = true;
                $rootScope.chkMulti = false;

                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                //$scope.temp.em_login_code = '';
                //$scope.temp.EmpName = '';
                $scope.showBtn = false;
            }



            $scope.$on('global_cancel', function (str) {


                if ($scope.SelectedUserLst.length > 0) {

                    $scope.temp = {};
                    $scope.temp = {
                        'enroll_number1': $scope.SelectedUserLst[0].em_login_code
                    }
                    $scope.temp['name'] = $scope.SelectedUserLst[0].empName;
                    $scope.EmpSearch1($scope.SelectedUserLst[0].em_login_code)
                }
            });

            $scope.searchGlobalClick = function () {
                $scope.Global_Search_by_employee();
            }

            if ($http.defaults.headers.common['schoolId'] == 'slis') {
                $scope.nationalIdLabel = "Iquma Number";
            }
            else {
                $scope.nationalIdLabel = "Emirates Id";
            }

            if ($http.defaults.headers.common['schoolId'] == 'elcportal') {
                $scope.serviceStatusLabel = "Employment Status";
            }
            else {
                $scope.serviceStatusLabel = "Service Status";
            }

            $http.get(ENV.apiUrl + "api/Qual/getAllQualification").then(function (res1) {
                $scope.all_Qual_Data = res1.data;
            });

            $http.get(ENV.apiUrl + "api/common/Subject/getSubject").then(function (res) {
                $scope.sub_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/HRMS/lems/getRole").then(function (rolesdata) {
                $scope.roles_data = rolesdata.data;

            });

            $http.get(ENV.apiUrl + "api/HRMSOES/GetAccommodationStatus").then(function (res) {;
                $scope.AccommondationStatus = res.data;
                console.log("AccommondationStatus", $scope.AccommondationStatus);
            });

            $http.get(ENV.apiUrl + "api/common/MarkEmpResignation/getDisenagementType").then(function (res) {
                $scope.disengagmentTypes = res.data;
            });

            $scope.setGrade_Dept = function (str) {

                $scope.temp.em_dept_effect_from = str;
                $scope.temp.em_grade_effect_from = str;
            }

            //$http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
            //    $scope.ComboBoxValues = AllComboBoxValues.data;
            //    console.log("ComboBoxValues", $scope.ComboBoxValues);
            //});

            $http.get(ENV.apiUrl + "api/HRMS/getLicSchool").then(function (licschooldata) {
                $scope.lic_school_data = licschooldata.data;
                console.log("lic_school_data", $scope.lic_school_data);
                if ($scope.lic_school_data[0].lic_school_country == 'Qatar') {
                    $scope.uae = false;
                    $scope.qatar = true;
                    $scope.slisLabel = false;
                    $scope.india = false;
                }
                else if ($scope.lic_school_data[0].lic_school_country == 'Saudi Arabia') {
                    $scope.uae = false;
                    $scope.qatar = false;
                    $scope.slisLabel = true;
                    $scope.india = false;
                }
                else if ($scope.lic_school_data[0].lic_school_country == 'India') {
                    $scope.uae = false;
                    $scope.qatar = false;
                    $scope.slisLabel = false;
                    $scope.india = true;
                }
                else {
                    $scope.uae = true;
                    $scope.qatar = false;
                    $scope.slisLabel = false;
                    $scope.india = false;
                }
            });

            $scope.checkDateFormat = function (dateFormat, ID) {
                var str = dateFormat.indexOf('/');
                if (str != -1) {
                    swal({ title: "Alert", text: "Please enter date only DD-MM-YYYY format", width: 360, height: 200 });
                    $('#' + ID).val('');
                }
            }

            $scope.Cancel = function () {
                $scope.temp = {};
                $("#empimg").attr('src', '')
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.em_img = "";
                $scope.Save_btn = true;
                $scope.Update_btn = false;
                $scope.temp.em_Company_Code = $scope.obj[0].em_Company_Code;
                $scope.temp.em_service_status_code = 'V';
                var data = {};
            }


            $scope.getstate = function (country) {
                $http.get(ENV.apiUrl + "api/HRMS/lems/getState?em_Country_Code=" + country).then(function (res) {
                    $scope.stt = res.data;
                });
            }

            $scope.getcity = function (state) {
                $http.get(ENV.apiUrl + "api/HRMS/lems/getCity?em_State_Code=" + state).then(function (res) {
                    $scope.ctt = res.data;
                });
            }

            $scope.getContract = function () {
                $http.get(ENV.apiUrl + "api/contract/Get_ContractDetails").then(function (Get_Contract) {
                    $scope.Contract_data = Get_Contract.data;

                });
            }
            $scope.getContract();

            $http.get(ENV.apiUrl + "api/Qualification/getQualificationName").then(function (QualificationName) {
                $scope.Qualification_Name = QualificationName.data;
            });

            $http.get(ENV.apiUrl + "api/Qualification/getQualificationType").then(function (qualtype) {
                $scope.qual_type_data = qualtype.data;
            });

            $http.get(ENV.apiUrl + "api/EmpExp/getMonthName").then(function (mon) {
                $scope.month_data = mon.data;
            });

            $scope.AddEnable = function () { $scope.addDisabled = false; }



            $scope.setAgreementDate = function (contractID) {
                $scope.showAgreementDetails = false;

                for (var i = 0; i < $scope.Contract_data.length; i++) {
                    if ($scope.Contract_data[i].cn_code == contractID) {
                        var duration = $scope.Contract_data[i].cn_duration;
                        break;
                    }
                }

                var dt = new Date();
                var currentDate = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                $scope.temp.em_agreement_start_date = currentDate;

                var day = $scope.temp.em_date_of_join.split("-")[0];
                var month = $scope.temp.em_date_of_join.split("-")[1];
                var year = $scope.temp.em_date_of_join.split("-")[2];

                if (isNaN(month)) {
                    month = $scope.temp.em_date_of_join.split("-")[1];
                }

                var expMonth = parseInt(month) + parseInt(duration);


                if (expMonth > 12) {
                    var expYear = parseInt(year) + 1;
                    expMonth = parseInt(expMonth) - 12;
                }
                else {
                    expYear = year;
                }
                $scope.temp.em_agreement_exp_date = day + "-" + expMonth + "-" + expYear;

                if ($scope.temp.em_agreement_exp_date != undefined) {
                    if (expMonth == 1) {
                        var expYear = parseInt(expYear) - 1;
                        expMonth = 12;
                    }
                    else {
                        expMonth = (parseInt(expMonth) - 1)
                    }
                    $scope.temp.em_agreemet_exp_rem_date = day + "-" + expMonth + "-" + expYear;
                }
            }


            $scope.EmpSearch1 = function (emplogcode) {
                debugger;
                $scope.Save_btn = false;
                $scope.Update_btn = true;
                $http.get(ENV.apiUrl + "api/HRMS/lems/getSearchEmpMaster?em_login_code=" + $scope.temp.enroll_number1).then(function (res) {
                    $scope.temp1 = res.data;
                    $scope.Myform.$setPristine();
                    $scope.Myform.$setUntouched();

                    $scope.getstate($scope.temp1[0].em_Country_Code);
                    $scope.getcity($scope.temp1[0].em_State_Code);

                    $scope.temp = {
                        enroll_number1: emplogcode,
                        em_login_code: $scope.temp1[0].em_login_code,
                        em_number: $scope.temp1[0].em_number,
                        em_service_status_code: $scope.temp1[0].em_service_status_code,
                        em_status_code: $scope.temp1[0].em_status_code,
                        em_apartment_number: $scope.temp1[0].em_apartment_number,
                        em_family_name: $scope.temp1[0].em_family_name,
                        em_street_number: $scope.temp1[0].em_street_number,
                        em_area_number: $scope.temp1[0].em_area_number,
                        em_phone: $scope.temp1[0].em_phone,
                        em_fax: $scope.temp1[0].em_fax,
                        em_post: $scope.temp1[0].em_post,
                        em_emergency_contact_name2: $scope.temp1[0].em_emergency_contact_name2,
                        em_emergency_contact_number2: $scope.temp1[0].em_emergency_contact_number2,
                        em_joining_ref: $scope.temp1[0].em_joining_ref,
                        em_ethnicity_code: $scope.temp1[0].em_ethnicity_code,
                        em_handicap_status: $scope.temp1[0].em_handicap_status,
                        em_stop_salary_indicator: $scope.temp1[0].em_stop_salary_indicator,
                        //em_agreement: $scope.temp1[0].em_agreement,
                        em_agreement_code: $scope.temp1[0].em_agreement_code,
                        em_punching_status: $scope.temp1[0].em_punching_status,
                        em_bank_cash_tag: $scope.temp1[0].em_bank_cash_tag,
                        em_citi_exp_tag: $scope.temp1[0].em_citi_exp_tag,
                        em_leave_tag: $scope.temp1[0].em_leave_tag,
                        em_passport_issue_place: $scope.temp1[0].em_passport_issue_place,
                        em_passport_issuing_authority: $scope.temp1[0].em_passport_issuing_authority,
                        em_pssport_exp_rem_date: $scope.temp1[0].em_pssport_exp_rem_date,
                        em_visa_issuing_place: $scope.temp1[0].em_visa_issuing_place,
                        em_visa_issuing_authority: $scope.temp1[0].em_visa_issuing_authority,
                        em_visa_exp_rem_date: $scope.temp1[0].em_visa_exp_rem_date,
                        em_visa_type_code: $scope.temp1[0].em_visa_type_code,
                        em_agreement_start_date: $scope.temp1[0].em_agreement_start_date,
                        em_agreement_exp_date: $scope.temp1[0].em_agreement_exp_date,
                        em_agreemet_exp_rem_date: $scope.temp1[0].em_agreemet_exp_rem_date,
                        em_punching_id: $scope.temp1[0].em_punching_id,
                        em_dependant_full: $scope.temp1[0].em_dependant_full,
                        em_dependant_half: $scope.temp1[0].em_dependant_half,
                        em_dependant_infant: $scope.temp1[0].em_dependant_infant,
                        em_Bank_Code: $scope.temp1[0].em_Bank_Code,
                        em_bank_ac_no: $scope.temp1[0].em_bank_ac_no,
                        em_iban_no: $scope.temp1[0].em_iban_no,
                        em_route_code: $scope.temp1[0].em_route_code,
                        em_bank_swift_code: $scope.temp1[0].em_bank_swift_code,
                        em_gpf_ac_no: $scope.temp1[0].em_gpf_ac_no,
                        em_pan_no: $scope.temp1[0].em_pan_no,
                        em_labour_card_no: $scope.temp1[0].em_labour_card_no,
                        en_labour_card_expiry_date: $scope.temp1[0].en_labour_card_expiry_date,
                        em_gosi_ac_no: $scope.temp1[0].em_gosi_ac_no,
                        em_gosi_start_date: $scope.temp1[0].em_gosi_start_date,
                        em_national_id: $scope.temp1[0].em_national_id,
                        em_secret_question_code: $scope.temp1[0].em_secret_question_code,
                        em_secret_answer: $scope.temp1[0].em_secret_answer,
                        em_building_number: $scope.temp1[0].em_building_number,
                        em_Company_Code: $scope.temp1[0].em_Company_Code,
                        em_Dept_Code: $scope.temp1[0].em_Dept_Code,
                        em_Designation_Code: $scope.temp1[0].em_Designation_Code,
                        em_Dest_Code: $scope.temp1[0].em_Dest_Code,
                        em_Grade_Code: $scope.temp1[0].em_Grade_Code,
                        em_Marital_Status_Code: $scope.temp1[0].em_Marital_Status_Code,
                        em_Nation_Code: $scope.temp1[0].em_Nation_Code,
                        em_Country_Code: $scope.temp1[0].em_Country_Code,
                        em_State_Code: $scope.temp1[0].em_State_Code,
                        em_City_Code: $scope.temp1[0].em_City_Code,
                        em_Religion_Code: $scope.temp1[0].em_Religion_Code,
                        em_Salutation_Code: $scope.temp1[0].em_Salutation_Code,
                        em_Sex_Code: $scope.temp1[0].em_Sex_Code,
                        em_Staff_Type_Code: $scope.temp1[0].em_Staff_Type_Code,
                        em_blood_group_code: $scope.temp1[0].em_blood_group_code,
                        em_date_of_birth: $scope.temp1[0].em_date_of_birth,
                        em_date_of_join: $scope.temp1[0].em_date_of_join,
                        em_dept_effect_from: $scope.temp1[0].em_dept_effect_from,
                        em_email: $scope.temp1[0].em_email,
                        em_emergency_contact_name1: $scope.temp1[0].em_emergency_contact_name1,
                        em_emergency_relation1: $scope.temp1[0].em_emergency_relation1,
                        em_emergency_relation2: $scope.temp1[0].em_emergency_relation2,
                        em_emergency_contact_number1: $scope.temp1[0].em_emergency_contact_number1,
                        em_first_name: $scope.temp1[0].em_first_name,
                        em_grade_effect_from: $scope.temp1[0].em_grade_effect_from,
                        em_last_name: $scope.temp1[0].em_last_name,
                        em_middle_name: $scope.temp1[0].em_middle_name,
                        em_mobile: $scope.temp1[0].em_mobile,
                        em_name_ot: $scope.temp1[0].em_name_ot,
                        em_national_id_expiry_date: $scope.temp1[0].em_national_id_expiry_date,
                        em_national_id_issue_date: $scope.temp1[0].em_national_id_issue_date,
                        em_passport_exp_date: $scope.temp1[0].em_passport_exp_date,
                        em_passport_issue_date: $scope.temp1[0].em_passport_issue_date,
                        em_passport_no: $scope.temp1[0].em_passport_no,
                        em_summary_address: $scope.temp1[0].em_summary_address,
                        em_summary_address_local_language: $scope.temp1[0].em_summary_address_local_language,
                        em_visa_exp_date: $scope.temp1[0].em_visa_exp_date,
                        em_stop_salary_from: $scope.temp1[0].em_stop_salary_from,
                        em_visa_issue_date: $scope.temp1[0].em_visa_issue_date,
                        em_visa_no: $scope.temp1[0].em_visa_no,
                        comn_role_code: $scope.temp1[0].comn_role_code,
                        comn_role_name: $scope.temp1[0].comn_role_name,
                        em_img: $scope.temp1[0].em_img,
                        em_accommodation_status: $scope.temp1[0].em_accommodation_status,
                        em_personalemail: $scope.temp1[0].em_personalemail,

                        em_adec_approval_number: $scope.temp1[0].em_adec_approval_number,
                        em_adec_approval_Date: $scope.temp1[0].em_adec_approval_Date,
                        em_adec_approved_qualification: $scope.temp1[0].em_adec_approved_qualification,
                        em_adec_approved_designation: $scope.temp1[0].em_adec_approved_designation,
                        em_adec_approved_subject: $scope.temp1[0].em_adec_approved_subject,
                        em_adec_approved_level: $scope.temp1[0].em_adec_approved_level,

                        em_health_card_no: $scope.temp1[0].em_health_card_no,
                        em_health_card_effective_from_date: $scope.temp1[0].em_health_card_effective_from_date,
                        em_health_card_effective_upto_date: $scope.temp1[0].em_health_card_effective_upto_date,
                        em_heath_card_status: $scope.temp1[0].em_heath_card_status,
                        em_teacher_status: $scope.temp1[0].em_teacher_status,
                        em_house: $scope.temp1[0].em_house,

                        en_labour_card_issue_date: $scope.temp1[0].en_labour_card_issue_date,
                        em_disengagement_type: $scope.temp1[0].em_disengagement_type,
                        offer_letter_doc: $scope.temp1[0].offer_letter_doc,
                        em_expected_date_of_join: $scope.temp1[0].em_expected_date_of_join,
                        em_offer_letter_send_date: $scope.temp1[0].em_offer_letter_send_date,
                        em_interview_date: $scope.temp1[0].em_interview_date,

                        //Other details
                        em_khda_noc_required: $scope.temp1[0].em_khda_noc_required,
                        em_khda_noc_submitted: $scope.temp1[0].em_khda_noc_submitted,
                        em_labour_noc_required: $scope.temp1[0].em_labour_noc_required,
                        em_labour_noc_submitted: $scope.temp1[0].em_labour_noc_submitted,
                        em_pro_visa_number: $scope.temp1[0].em_pro_visa_number,
                        em_visa_uid: $scope.temp1[0].em_visa_uid,

                        //Sponsor details
                        em_sponser_name: $scope.temp1[0].em_sponser_name,
                        em_relation: $scope.temp1[0].em_relation,
                        visa_num: $scope.temp1[0].visa_num,
                        visa_issue_date: $scope.temp1[0].visa_issue_date,
                        visa_exp_date: $scope.temp1[0].visa_exp_date

                    }

                    //var num = Math.random();
                    var imgSrc = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/' + $scope.temp1[0].em_img;
                    $(function () {
                        $('#empimg').attr("src", imgSrc);
                    });


                    $scope.getQualiByEmpcode = function (empcode) {

                        $http.get(ENV.apiUrl + "api/Qualification/getQualificationByEmployee?emp_code=" + $scope.temp1[0].em_login_code).then(function (res1) {
                            $scope.qualiList = res1.data;
                            console.log("qualiList", $scope.qualiList);
                            $scope.combo_multiple = true;
                        });

                    }
                    $scope.getQualiByEmpcode();

                    //$http.get(ENV.apiUrl + "api/EmpExp/getEmployeeExp?em_login_code=" + $scope.temp1[0].em_login_code).then(function (res) {
                    //    $scope.ExperienceList = res.data;
                    //    console.log("ExperienceList", $scope.ExperienceList);
                    //    $scope.showExperinceList = true;
                    //});

                   // $scope.showEmpDocumentList($scope.temp1[0].em_login_code);
                });
            }


            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };

            $scope.file_changed = function (element) {
                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);

                if ($scope.photo_filename != undefined || $scope.photo_filename != "") {
                    var imageType = $scope.photo_filename.split('/');
                    console.log("imageType", imageType[1]);
                    if (imageType[1] == 'jpeg' || imageType[1] == 'jpg' || imageType[1] == 'png') {

                        var reader = new FileReader();
                        reader.onload = function (e) {
                            $scope.$apply(function () {
                                //$scope.prev_img = e.target.result;
                                var imgSrc = e.target.result;
                                $(function () {
                                    $('#empimg').attr("src", imgSrc);
                                });
                            });
                        };
                        reader.readAsDataURL(photofile);
                    }
                    else {
                        swal({ text: "Only jpeg and png image format supported." });
                        $('#empimg').attr("src", '');
                        return;
                    }
                }
                else {
                    swal({ text: "Image format not supported." });
                    $('#empimg').attr("src", '');
                    return;
                }


            };


            $scope.uploadImgClickF = function () {
                $scope.uploadImgClickFShow = true;
            }

            $scope.uploadImgCancel = function () {
                $scope.uploadImgClickFShow = false;
            }

            $scope.image_click = function () {
                $('#upload_img').modal('show');
                $scope.ImgLoaded = true;
            }

            $scope.globalSearch = function () { $('#MyModal').modal('show'); $scope.showBtn = false; }

            $scope.EmployeeDetails = function () {

                $scope.NodataEmployee = false;
                $http.post(ENV.apiUrl + "api/common/SearchEmployee?data=" + JSON.stringify($scope.temp)).then(function (SearchEmployee_Data) {
                    $scope.Employee_Details = SearchEmployee_Data.data;

                    $scope.EmployeeTable = true;

                    if (SearchEmployee_Data.data.length > 0) {
                    }
                    else {
                        $scope.NodataEmployee = true;
                    }
                });

            }

            $scope.EmployeeAdd = function (info) {
                $scope.temp = {
                    enroll_number: info.em_login_code,
                    emp_name: info.empName,
                    emp_desg: info.desg_name,
                    emp_dept: info.dept_name
                };
            }

            $scope.checkEmailExists = function (email_id) {
                if ($scope.temp1[0].em_email != email_id) {
                    $http.post(ENV.apiUrl + "api/HRMS/checkEmailExists?email_id=" + email_id).then(function (resc) {
                        $scope.comnUserData = resc.data;
                        if ($scope.comnUserData.length > 0) {
                            swal({ text: 'Official email id already exists!!', width: 350 });
                            $scope.temp.em_email = "";
                            return false;
                        }
                    });
                }
            }
            $scope.change_em_teacher_status = function () {
                if ($scope.temp.em_Staff_Type_Code != 'T') {
                    $scope.temp.em_teacher_status = false;
                } else {
                    $scope.temp.em_teacher_status = true;
                }

            }

            $scope.SaveData = function (isvalid) {

                if (isvalid) {
                    $scope.saveBtn = true;
                    $scope.updateBtn = true;

                    var data = $scope.temp;
                    if ($scope.photo_filename === undefined) {
                        data.em_img = null;
                    }
                    else {
                        data.em_img = '.' + $scope.photo_filename.split("/")[1];
                    }
                    data.opr = "I";
                    $http.post(ENV.apiUrl + "api/HRMS/lems/CUMasterEmployee", data).then(function (res) {
                        $scope.CUDobj = {};
                        $scope.CUDobj = res.data;
                        debugger;
                        if ($scope.CUDobj.strMessage != undefined || $scope.CUDobj.strMessage != "" || $scope.CUDobj.strMessage != null) {
                            swal({ text: 'Employee with id= ' + $scope.CUDobj.strMessage + ' created  successfully.' });
                            console.log("Em login Code " + $scope.CUDobj.strMessage);
                            $scope.saveQualification($scope.CUDobj.strMessage);
                            $scope.saveExperience($scope.CUDobj.strMessage);
                            $scope.Cancel();

                            setTimeout(function () {
                                $scope.saveBtn = false;
                                $scope.updateBtn = false;
                            }, 3000);

                        }

                        var empid = $scope.CUDobj.strMessage.split(" ")[2];
                        var request = {
                            method: 'POST',
                            url: ENV.apiUrl + '/api/file/upload?filename=' + empid + "&location=" + "/EmployeeImages",
                            data: formdata,
                            headers: {
                                'Content-Type': undefined
                            }
                        };

                        $http(request).success(function (d) {
                            //alert(d);
                        });

                    });


                }
                else {
                    swal({ text: 'Kindly fill Mandatory fields', width: 350, timer: 5000, showCloseButton: true });
                }
            }

            $scope.UpdateData = function (Myform) {
                debugger
                if (Myform) {
                    $scope.saveBtn = true;
                    $scope.updateBtn = true;
                    //var x = Math.floor((Math.random() * 1000) + 1);
                   
                    //var t = "png"; //$scope.photo_filename.split("/")[1]
                    //if ($scope.photo_filename === undefined) {
                    //}
                    //else {
                    //    data.em_img = $scope.temp.em_login_code + x + '.' + t;
                    //}

                    var data = $scope.temp;
                    data.opr = "UU"; /// To update only pro details
                    //data.subopr = 'U';

                    $http.post(ENV.apiUrl + "api/HRMS/lems/PROEmployeeUpdate", data).then(function (res) {
                        $scope.CUDobj = {};
                        $scope.CUDobj = res.data;
                        if ($scope.CUDobj.strMessage != undefined || $scope.CUDobj.strMessage != "" || $scope.CUDobj.strMessage != null) {
                            swal({ text: $scope.CUDobj.strMessage });

                            setTimeout(function () {
                                $scope.Cancel();
                                $scope.saveBtn = false;
                                $scope.updateBtn = false;
                            }, 1000);
                        }

                    });
                    //debugger;
                    //if ($scope.ImgLoaded == true) {
                    //    var request = {
                    //        method: 'POST',
                    //        url: ENV.apiUrl + '/api/file/uploadPNG?filename=' + $scope.temp.em_login_code + x + "&location=" + "/EmployeeImages",
                    //        data: formdata,
                    //        headers: {
                    //            'Content-Type': undefined
                    //        }
                    //    };
                    //    $http(request).success(function (d) {
                    //        //alert(d);
                    //    });
                    //}
                }
                else {
                    swal({ text: 'Kindly fill Mandatory fields', width: 350, timer: 5000, showCloseButton: true });
                }
            }

            $scope.saveQualification = function (em_login_code) {

                var demo1 = [];
                var modulecode = [];

                if ($scope.qualiList.length > 0) {

                    for (var i = 0; i < $scope.qualiList.length; i++) {
                        //if (parseInt($scope.qualiList[i].em_qual_from_year) > parseInt(currentYear) || parseInt($scope.qualiList[i].em_qual_to_year) > parseInt(currentYear)) {
                        //    swal({ title: "Alert", text: 'Future year not allowed', timer: 5000, width: 320 });
                        //    return;
                        //}
                        modulecode = $scope.qualiList[i].emp_qual_qual_code;
                        var data = {
                            'emp_qual_em_code': em_login_code,//$scope.popobj.emp_qual_em_code,
                            'emp_qual_qual_code': modulecode,
                            'emp_qual_institute': $scope.qualiList[i].emp_qual_institute,
                            'emp_qual_type_code': $scope.qualiList[i].emp_qual_type_code,
                            'em_qual_from_year': $scope.qualiList[i].em_qual_from_year,
                            'em_qual_year': $scope.qualiList[i].em_qual_to_year,
                            'emp_qual_percentage': $scope.qualiList[i].emp_qual_percentage,
                            'emp_qual_remark': $scope.qualiList[i].emp_qual_remark,
                            'em_qual_status': $scope.qualiList[i].em_qual_status,
                            'opr': 'I'
                        }
                        demo1.push(data);
                    }

                    $http.post(ENV.apiUrl + "api/Qualification/CUDQualification?year=", demo1).then(function (msg) {
                        $scope.qualificationmsg = msg.data;
                        console.log("qualificationmsg ", $scope.qualificationmsg);
                        $scope.qualiList = [];
                        $scope.combo_multiple = false;
                    });
                }
            }

            $scope.updateQualification = function () {
                var demo1 = [];
                var modulecode = [];

                if ($scope.qualiList.length > 0) {
                    for (var i = 0; i < $scope.qualiList.length; i++) {

                        //if (parseInt($scope.qualiList[i].em_qual_from_year) > parseInt(currentYear) || parseInt($scope.qualiList[i].em_qual_to_year) > parseInt(currentYear)) {
                        //    swal({ title: "Alert", text: 'Future year not allowed', timer: 5000, width: 320 });
                        //    return;
                        //}

                        modulecode = $scope.qualiList[i].emp_qual_qual_code;
                        var data = {
                            'emp_qual_em_code': $scope.temp.em_login_code,//$scope.popobj.emp_qual_em_code,
                            'emp_qual_qual_code': modulecode,

                            'emp_qual_institute': $scope.qualiList[i].emp_qual_institute,
                            'emp_qual_type_code': $scope.qualiList[i].emp_qual_type_code,
                            'em_qual_from_year': $scope.qualiList[i].em_qual_from_year,

                            'em_qual_year': $scope.qualiList[i].em_qual_to_year,
                            'emp_qual_percentage': $scope.qualiList[i].emp_qual_percentage,
                            'emp_qual_remark': $scope.qualiList[i].emp_qual_remark,
                            'em_qual_status': $scope.qualiList[i].em_qual_status,
                            'opr': 'U'
                        }
                        demo1.push(data);
                    }

                    if ($scope.qualiList.length != 0) {
                        $http.post(ENV.apiUrl + "api/Qualification/UpadateQualification?simsobj2=", demo1).then(function (msg) {
                            $scope.updateQualimsg1 = msg.data;
                            console.log($scope.updateQualimsg1);
                            $scope.qualiList = [];
                            $scope.combo_multiple = false;
                        });
                    }
                    else {
                        swal({ text: 'You not edit correct record', timer: 5000 });
                    }
                }
            }


            $scope.downloadOfferLetter = function (str) {
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeDocument/' + str;
                window.open($scope.url);
            }

            $scope.addCol = function (str) {

                $scope.combo_multiple = true;
                var t = document.getElementById("qual_name");
                var selectedText = t.options[t.selectedIndex].text;

                for (var i = 0; i < $scope.qualiList.length; i++) {
                    if ($scope.qualiList[i].emp_qual_qual_code == str) {
                        swal({ text: 'Qualification Already Added !!', timer: 5000 });
                        return false;
                    }
                }

                var data = {
                    'emp_qual_qual_name': selectedText,
                    'emp_qual_qual_code': str
                }
                $scope.qualiList.push(data);

                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            $scope.Removerow = function ($event, index, str) {
                str.splice(index, 1);
            }

            $scope.addExperience = function () {
                $scope.showExperinceList = true;
                //var t = document.getElementById("qual_name");
                //var selectedText = t.options[t.selectedIndex].text;

                //for (var i = 0; i < $scope.qualiList.length; i++) {
                //    if ($scope.qualiList[i].emp_qual_qual_code == str) {
                //        swal({ text: 'Qualification Already Added !!', timer: 5000 });
                //        return false;
                //    }
                //}

                var data = {
                    'enroll_number': '',
                    'prev_company_name': '',
                    'prev_job_title': '',
                    'from_year': '',
                    'from_month': '',
                    'to_year': '',
                    'to_month': '',
                    'prev_job_remark': '',
                    'prev_job_responsibilities': '',
                    'prev_job_salary': '',
                    'job_status': '',
                    'em_previous_job_line_no': ''
                }
                $scope.ExperienceList.push(data);
            };

            $scope.saveExperience = function (em_login_code) {
                var datasend = [];

                if ($scope.ExperienceList.length > 0) {

                    for (var i = 0; i < $scope.ExperienceList.length; i++) {

                        var data = {
                            'enroll_number': em_login_code,
                            'co_company_code': '1',
                            'prev_company_name': $scope.ExperienceList[i].prev_company_name,
                            'prev_job_title': $scope.ExperienceList[i].prev_job_title,
                            'from_year': $scope.ExperienceList[i].from_year,
                            'from_month': $scope.ExperienceList[i].from_month,
                            'to_year': $scope.ExperienceList[i].to_year,
                            'to_month': $scope.ExperienceList[i].to_month,
                            //'prev_job_remark': $scope.ExperienceList[i].prev_job_remark,
                            'prev_job_responsibilities': $scope.ExperienceList[i].prev_job_responsibilities,
                            'prev_job_salary': $scope.ExperienceList[i].prev_job_salary,
                            'job_status': $scope.ExperienceList[i].job_status,
                            'opr': 'I'
                        }
                        datasend.push(data);
                    }

                    $http.post(ENV.apiUrl + "api/EmpExp/CUDEmployeeExperience", datasend).then(function (msg) {
                        $scope.msgexp = msg.data;
                        console.log("emp exp", $scope.msgexp);
                        $scope.ExperienceList = [];
                        $scope.showExperinceList = false;
                    });

                }
            }


            $scope.updateExperience = function () {
                var dataupdate = [];

                if ($scope.ExperienceList.length > 0) {

                    for (var i = 0; i < $scope.ExperienceList.length; i++) {

                        var data = {
                            'enroll_number': $scope.temp.em_login_code,
                            'co_company_code': '1',
                            'prev_company_name': $scope.ExperienceList[i].prev_company_name,
                            'prev_job_title': $scope.ExperienceList[i].prev_job_title,
                            'from_year': $scope.ExperienceList[i].from_year,
                            'from_month': $scope.ExperienceList[i].from_month,
                            'to_year': $scope.ExperienceList[i].to_year,
                            'to_month': $scope.ExperienceList[i].to_month,
                            //'prev_job_remark': $scope.ExperienceList[i].prev_job_remark,
                            'prev_job_responsibilities': $scope.ExperienceList[i].prev_job_responsibilities,
                            'prev_job_salary': $scope.ExperienceList[i].prev_job_salary,
                            'job_status': $scope.ExperienceList[i].job_status,
                            'em_previous_job_line_no': $scope.ExperienceList[i].em_previous_job_line_no,
                            'opr': 'U'
                        }
                        dataupdate.push(data);
                    }

                    $http.post(ENV.apiUrl + "api/EmpExp/CUDEmployeeExperience", dataupdate).then(function (msg) {
                        $scope.msgupdateExp = msg.data;
                        console.log("emp exp update", $scope.msgupdateExp);
                        $scope.ExperienceList = [];
                        $scope.showExperinceList = false;
                    });
                }
            }

            $scope.RemoveExperinceRow = function ($event, index, str) {
                str.splice(index, 1);
            }



            $scope.UploadImageModal = function () {

                $("#prevImg").val('');
                $scope.edt.em_img = '';
                //imagename = str.em_number;
                $('#myModal').modal('show');
                $scope.ImgLoaded = true;
            }

            /*----------------   Upoload Documnet Code begin------------- */

            $scope.showEmpDocumentList = function (em_login_code) {

                var obj = {
                    pays_doc_empl_id: em_login_code,
                    show: 'all'
                }
                $http.post(ENV.apiUrl + "api/EmpDocumentupload/DocumentdetailData", obj).then(function (res) {

                    $scope.DocumentdetailsData = res.data;
                    console.log("DocumentdetailsData", $scope.DocumentdetailsData);
                    $scope.showFileName = false;
                    if ($scope.DocumentdetailsData.length > 0) {
                        $scope.table = true;
                        $scope.pager = true;
                    }
                    else {
                        $scope.table = false;
                        //swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                    }

                    $scope.feetemp = res.data;
                    $scope.PAcols = [];
                    var fc = false;


                    $scope.allDt = [];
                    for (var d = 0; d < $scope.feetemp.length; d++) {
                        var dt = $scope.feetemp[d].pays_doc_empl_name;
                        if ($scope.allDt[dt] == undefined) {
                            $scope.allDt[dt] = getRecDateWise(dt)
                        }
                    }

                    $scope.finalData = [];
                    for (var item in $scope.allDt) {
                        var ob = {
                            'dt': item,
                            'arr': $scope.allDt[item]
                        };
                        $scope.finalData.push(ob);
                    }
                });
            };

            $scope.UploadDocumentModal = function (str) {
                $scope.filenameassigningData = str;
                $('#DocumenModal').modal('show');
                console.log("str ", str);
            }

            $scope.hideDocModal = function () {
                debugger;
                $scope.showFileName = true;

                $scope.filename1 = $scope.filename_doc;
                if ($scope.filename1 == undefined || $scope.filename1 == "" || $scope.filename1 == null) {
                    $scope.showFileName = false;
                }
                $scope.filenameassigningData.pays_doc_path = $scope.filename1;
                $('#DocumenModal').modal('hide');
            };


            $scope.getTheDocFiles = function ($files) {
                //FileList[0].File.name = $scope.filename + '.' + fortype;
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };

            $scope.file_changed_doc = function (element) {
                debugger;
                var photofile = element.files[0];
                //fortype = photofile.type.split("/")[1];
                //if (fortype == 'png' || fortype == 'jpeg' || fortype == 'jpg' || fortype == 'pdf') {
                //    if (photofile.size > 200000) {
                //        swal('', 'File size limit not exceed upto 200kb.')
                //    }
                //    else {

                $scope.filename_doc = photofile.name;
                $scope.edt = { employeeDocument: $scope.filename_doc }
                $.extend($scope.edt, $scope.edt)
                //$scope.photo_filename = (photofile.type);

                $scope.photo_filename_doc = (photofile.name);

                var len = 0;
                len = $scope.photo_filename_doc.split('.');
                fortype = $scope.photo_filename_doc.split('.')[len.length - 1];
                // $scope.edt1 = { sims_timetable_filename: $scope.photo_filename }
                // $.extend($scope.edt, $scope.edt1)
                $scope.photo_filename_doc = (photofile.type);

                //element.files[0].FileList.File.name = $scope.filename + '.' + fortype;
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        // $scope.prev_img = e.target.result;


                    });
                };
                reader.readAsDataURL(photofile);

                //    }
                //}
                //else {
                //    swal('', 'PDF and Image Files are allowed.');
                //}
            };

            var issueoexpirydate = false;
            $scope.getRegDate = function (info, str) {

                issueoexpirydate = false;
                $('#dateModal').modal({ backdrop: 'static', keyboard: true });
                info.ischecked = true;
                if (str == 'issue') {
                    issueoexpirydate = true;
                    $scope.doc =
                        {
                            reg_date: info.pays_doc_issue_date
                        }
                }
                else {
                    issueoexpirydate = false;
                    $scope.doc =
                       {
                           reg_date: info.pays_doc_expiry_date
                       }
                }
            }

            $scope.getDatedetails = function () {

                for (var i = 0; i < $scope.finalData.length; i++) {
                    for (var j = 0; j < $scope.finalData[i].arr.length; j++) {
                        if ($scope.finalData[i].arr[j].ischecked == true) {
                            if (issueoexpirydate == true)
                                $scope.finalData[i].arr[j].pays_doc_issue_date = $scope.doc.reg_date;
                            else
                                $scope.finalData[i].arr[j].pays_doc_expiry_date = $scope.doc.reg_date;
                            $scope.finalData[i].arr[j].ischecked = false;
                            break;
                        }
                    }
                }
                // $scope.temp.reg_date =null;
            }

            ///Save Document
            $scope.saveDocument = function (str) {
                debugger
                var datasave = [];
                var dataupdate = [];
                var saveorupdate = false;
                $scope.showUpdateBtn = false;
                document.getElementById('file1_doc').value = '';
                $scope.filename_doc = str.pays_doc_empl_id + '_' + str.pays_doc_desc1;
                if (str.pays_doc_srno == '') {
                    saveorupdate = true;
                    $scope.showUpdateBtn = false;
                    var data = {
                        pays_doc_empl_id: str.pays_doc_empl_id,
                        pays_doc_mod_code: str.pays_doc_mod_code,
                        pays_doc_code: str.pays_doc_code,
                        pays_doc_desc: str.pays_doc_desc,
                        pays_doc_path: str.pays_doc_path,
                        pays_doc_issue_date: str.pays_doc_issue_date,
                        pays_doc_expiry_date: str.pays_doc_expiry_date,
                        pays_doc_status: true,
                    }
                    datasave.push(data);
                }
                //else {
                //    saveorupdate = false;
                //    $scope.showUpdateBtn = true;
                //    var data = {
                //        pays_doc_srno: str.pays_doc_srno,
                //        pays_doc_empl_id: str.pays_doc_empl_id,
                //        pays_doc_mod_code: str.pays_doc_mod_code,
                //        pays_doc_code: str.pays_doc_code,
                //        pays_doc_desc: str.pays_doc_desc,
                //        pays_doc_path: str.pays_doc_path,
                //        pays_doc_issue_date: str.pays_doc_issue_date,
                //        pays_doc_expiry_date: str.pays_doc_expiry_date,
                //        pays_doc_status: true,
                //    }
                //    dataupdate.push(data);

                //}

                $scope.pays_doc_empl_id = str.pays_doc_empl_id;
                $scope.pays_doc_desc1 = str.pays_doc_desc1;


                var t = $scope.photo_filename_doc.split("/")[1];
                ///+ '.' + fortype
                $scope.filename1 = $scope.filename_doc + '.' + t;
                var data = [];
                var senddata = [];

                if ($scope.photo_filename_doc == undefined || $scope.photo_filename_doc == '') {
                    swal({ title: "Alert", text: "Please Select File", width: 300, height: 200 });
                }
                else {

                    if (saveorupdate == true) {
                        for (var i = 0; i < datasave.length; i++) {
                            data = {
                                pays_doc_empl_id: datasave[i].pays_doc_empl_id,
                                pays_doc_mod_code: datasave[i].pays_doc_mod_code,
                                pays_doc_code: datasave[i].pays_doc_code,
                                pays_doc_desc: datasave[i].pays_doc_desc,
                                pays_doc_path: $scope.filename1,
                                pays_doc_issue_date: datasave[i].pays_doc_issue_date,
                                pays_doc_expiry_date: datasave[i].pays_doc_expiry_date,
                                pays_doc_status: true,
                                opr: "I"
                            }
                        }
                    }
                    //else {
                    //    for (var i = 0; i < dataupdate.length; i++) {
                    //        data = {
                    //            pays_doc_srno: dataupdate[i].pays_doc_srno,
                    //            pays_doc_empl_id: dataupdate[i].pays_doc_empl_id,
                    //            pays_doc_mod_code: dataupdate[i].pays_doc_mod_code,
                    //            pays_doc_code: dataupdate[i].pays_doc_code,
                    //            pays_doc_desc: dataupdate[i].pays_doc_desc,
                    //            pays_doc_path: $scope.filename1,
                    //            pays_doc_issue_date: dataupdate[i].pays_doc_issue_date,
                    //            pays_doc_expiry_date: dataupdate[i].pays_doc_expiry_date,
                    //            pays_doc_status: true,
                    //            opr: "U"
                    //        }
                    //    }
                    //}

                    senddata.push(data);

                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + '/api/EmpDocumentupload/upload?filename=' + $scope.filename1 + "&location=" + "EmployeeDocument",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    $http(request)
                    .success(function (d) {
                    },
                    function () {
                        //alert("Err");
                        console.log("file upload Err");
                    });


                    $http.post(ENV.apiUrl + "api/EmpDocumentupload/InsertUpdateDocumentDetail", senddata).then(function (msg) {
                        $scope.msg1_doc = msg.data;

                        $('#DocumenModal').modal('hide');
                        if ($scope.msg1_doc == true) {
                            $scope.photo_filename_doc = '';
                            //swal({ text: "Employee document uploaded successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            $scope.showEmpDocumentList($scope.temp.em_login_code);
                        }

                        else if ($scope.msg1_doc == false) {
                            //swal({ text: "Employee document not uploaded. Please enter emp id or name. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            //$scope.show();
                        }
                        else {
                            //swal("Error-" + $scope.msg1_doc)
                            console.log("Error-" + $scope.msg1_doc)
                            $scope.showEmpDocumentList($scope.temp.em_login_code);
                        }

                    },
                        function () {
                            $('#ErrorMessage').modal({ backdrop: "static" });
                        });
                }

            }

            $scope.uploadDocument = function (str) {
                debugger
                var datasave = [];
                var dataupdate = [];
                var saveorupdate = false;
                $scope.showUpdateBtn = false;
                document.getElementById('file1_doc').value = '';
                $scope.filename_doc = str.pays_doc_empl_id + '_' + str.pays_doc_desc1;
                if (str.pays_doc_srno == '') {
                    saveorupdate = true;
                    $scope.showUpdateBtn = false;
                    var data = {
                        pays_doc_empl_id: str.pays_doc_empl_id,
                        pays_doc_mod_code: str.pays_doc_mod_code,
                        pays_doc_code: str.pays_doc_code,
                        pays_doc_desc: str.pays_doc_desc,
                        pays_doc_path: str.pays_doc_path,
                        pays_doc_issue_date: str.pays_doc_issue_date,
                        pays_doc_expiry_date: str.pays_doc_expiry_date,
                        pays_doc_status: true,
                    }
                    datasave.push(data);
                }
                //else {
                //    saveorupdate = false;
                //    $scope.showUpdateBtn = true;
                //    var data = {
                //        pays_doc_srno: str.pays_doc_srno,
                //        pays_doc_empl_id: str.pays_doc_empl_id,
                //        pays_doc_mod_code: str.pays_doc_mod_code,
                //        pays_doc_code: str.pays_doc_code,
                //        pays_doc_desc: str.pays_doc_desc,
                //        pays_doc_path: str.pays_doc_path,
                //        pays_doc_issue_date: str.pays_doc_issue_date,
                //        pays_doc_expiry_date: str.pays_doc_expiry_date,
                //        pays_doc_status: true,
                //    }
                //    dataupdate.push(data);

                //}

                $scope.pays_doc_empl_id = str.pays_doc_empl_id;
                $scope.pays_doc_desc1 = str.pays_doc_desc1;


                var t = $scope.photo_filename_doc.split("/")[1];
                ///+ '.' + fortype
                $scope.filename1 = $scope.filename_doc + '.' + t;
                var data = [];
                var senddata = [];

                if ($scope.photo_filename_doc == undefined || $scope.photo_filename_doc == '') {
                    swal({ title: "Alert", text: "Please Select File", width: 300, height: 200 });
                }
                else {

                    if (saveorupdate == true) {
                        for (var i = 0; i < datasave.length; i++) {
                            data = {
                                pays_doc_empl_id: datasave[i].pays_doc_empl_id,
                                pays_doc_mod_code: datasave[i].pays_doc_mod_code,
                                pays_doc_code: datasave[i].pays_doc_code,
                                pays_doc_desc: datasave[i].pays_doc_desc,
                                pays_doc_path: $scope.filename1,
                                pays_doc_issue_date: datasave[i].pays_doc_issue_date,
                                pays_doc_expiry_date: datasave[i].pays_doc_expiry_date,
                                pays_doc_status: true,
                                opr: "I"
                            }
                        }
                    }
                    //else {
                    //    for (var i = 0; i < dataupdate.length; i++) {
                    //        data = {
                    //            pays_doc_srno: dataupdate[i].pays_doc_srno,
                    //            pays_doc_empl_id: dataupdate[i].pays_doc_empl_id,
                    //            pays_doc_mod_code: dataupdate[i].pays_doc_mod_code,
                    //            pays_doc_code: dataupdate[i].pays_doc_code,
                    //            pays_doc_desc: dataupdate[i].pays_doc_desc,
                    //            pays_doc_path: $scope.filename1,
                    //            pays_doc_issue_date: dataupdate[i].pays_doc_issue_date,
                    //            pays_doc_expiry_date: dataupdate[i].pays_doc_expiry_date,
                    //            pays_doc_status: true,
                    //            opr: "U"
                    //        }
                    //    }
                    //}

                    senddata.push(data);

                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + '/api/EmpDocumentupload/upload?filename=' + $scope.filename1 + "&location=" + "EmployeeDocument",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    $http(request)
                    .success(function (d) {
                    },
                    function () {
                        //alert("Err");
                        console.log("file upload Err");
                    });


                    $http.post(ENV.apiUrl + "api/EmpDocumentupload/InsertUpdateDocumentDetail", senddata).then(function (msg) {
                        $scope.msg1_doc = msg.data;

                        $('#DocumenModal').modal('hide');
                        if ($scope.msg1_doc == true) {
                            $scope.photo_filename_doc = '';
                            //swal({ text: "Employee document uploaded successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            $scope.showEmpDocumentList($scope.temp.em_login_code);
                        }

                        else if ($scope.msg1_doc == false) {
                            //swal({ text: "Employee document not uploaded. Please enter emp id or name. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            //$scope.show();
                        }
                        else {
                            //swal("Error-" + $scope.msg1_doc)
                            console.log("Error-" + $scope.msg1_doc)
                            $scope.showEmpDocumentList($scope.temp.em_login_code);
                        }

                    },
                        function () {
                            $('#ErrorMessage').modal({ backdrop: "static" });
                        });
                }

            }

            $scope.UpdateDocument = function (str) {
                debugger
                var datasave = [];
                var dataupdate = [];
                var saveorupdate = false;
                $scope.showUpdateBtn = false;
                document.getElementById('file1_doc').value = '';
                var v = new Date();

                $scope.filename_doc = str.pays_doc_empl_id + '_' + str.pays_doc_desc1 + '_' + v.getHours() + v.getSeconds();
                if (str.pays_doc_srno == '') {
                    saveorupdate = true;
                    $scope.showUpdateBtn = false;
                    var data = {
                        pays_doc_empl_id: str.pays_doc_empl_id,
                        pays_doc_mod_code: str.pays_doc_mod_code,
                        pays_doc_code: str.pays_doc_code,
                        pays_doc_desc: str.pays_doc_desc,
                        pays_doc_path: str.pays_doc_path,
                        pays_doc_issue_date: str.pays_doc_issue_date,
                        pays_doc_expiry_date: str.pays_doc_expiry_date,
                        pays_doc_status: true,
                    }
                    datasave.push(data);
                }
                else {
                    saveorupdate = false;
                    $scope.showUpdateBtn = true;
                    var data = {
                        pays_doc_srno: str.pays_doc_srno,
                        pays_doc_empl_id: str.pays_doc_empl_id,
                        pays_doc_mod_code: str.pays_doc_mod_code,
                        pays_doc_code: str.pays_doc_code,
                        pays_doc_desc: str.pays_doc_desc,
                        pays_doc_path: str.pays_doc_path,
                        pays_doc_issue_date: str.pays_doc_issue_date,
                        pays_doc_expiry_date: str.pays_doc_expiry_date,
                        pays_doc_status: true,
                    }
                    dataupdate.push(data);

                }

                $scope.pays_doc_empl_id = str.pays_doc_empl_id;
                $scope.pays_doc_desc1 = str.pays_doc_desc1;

                var data = [];
                var senddata = [];
                if ($scope.photo_filename_doc == undefined || $scope.photo_filename_doc == '' || $scope.photo_filename_doc == null) {
                    for (var i = 0; i < dataupdate.length; i++) {
                        data = {
                            pays_doc_srno: dataupdate[i].pays_doc_srno,
                            pays_doc_empl_id: dataupdate[i].pays_doc_empl_id,
                            pays_doc_mod_code: dataupdate[i].pays_doc_mod_code,
                            pays_doc_code: dataupdate[i].pays_doc_code,
                            pays_doc_desc: dataupdate[i].pays_doc_desc,
                            pays_doc_path: dataupdate[i].pays_doc_path,
                            pays_doc_issue_date: dataupdate[i].pays_doc_issue_date,
                            pays_doc_expiry_date: dataupdate[i].pays_doc_expiry_date,
                            pays_doc_status: true,
                            opr: "U"
                        }
                    }
                    senddata.push(data);
                }
                else {

                    // var t = $scope.photo_filename.split("/")[1];
                    var t = $scope.photo_filename_doc.split("/")[1];
                    ///+ '.' + fortype
                    $scope.filename1 = $scope.filename_doc + '.' + t;
                    //$scope.filename1 = $scope.filename ;

                    for (var i = 0; i < dataupdate.length; i++) {
                        data = {
                            pays_doc_srno: dataupdate[i].pays_doc_srno,
                            pays_doc_empl_id: dataupdate[i].pays_doc_empl_id,
                            pays_doc_mod_code: dataupdate[i].pays_doc_mod_code,
                            pays_doc_code: dataupdate[i].pays_doc_code,
                            pays_doc_desc: dataupdate[i].pays_doc_desc,
                            pays_doc_path: $scope.filename1,
                            pays_doc_issue_date: dataupdate[i].pays_doc_issue_date,
                            pays_doc_expiry_date: dataupdate[i].pays_doc_expiry_date,
                            pays_doc_status: true,
                            opr: "U"
                        }
                    }
                    senddata.push(data);

                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + '/api/EmpDocumentupload/upload?filename=' + $scope.filename1 + "&location=" + "EmployeeDocument",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    $http(request)
                      .success(function (d) {
                      },
                    function () {
                        alert("Err");

                    });
                }


                $http.post(ENV.apiUrl + "api/EmpDocumentupload/InsertUpdateDocumentDetail", senddata).then(function (msg) {
                    $scope.msg1updatedoc = msg.data;
                    $('#DocumenModal').modal('hide');
                    if ($scope.msg1updatedoc == true) {
                        console.log("msg1updatedoc ", $scope.msg1updatedoc);
                        $scope.showEmpDocumentList($scope.temp.em_login_code);
                        $scope.photo_filename_doc = '';
                    }
                    else if ($scope.msg1updatedoc == false) {
                        console.log("msg1updatedoc ", $scope.msg1updatedoc);
                        $scope.showEmpDocumentList($scope.temp.em_login_code);
                    }
                    else {
                        //swal("Error-" + $scope.msg1updatedoc)
                        console.log("Error ", $scope.msg1updatedoc);
                    }
                },
                function () {
                    $('#ErrorMessage').modal({ backdrop: "static" });
                });
            }

            $scope.deleteDoc = function (obj) {
                console.log(obj);
                var deleteObj = {
                    pays_doc_code: obj.pays_doc_code,
                    pays_doc_srno: obj.pays_doc_srno,
                    pays_doc_empl_id: obj.pays_doc_empl_id
                }
                console.log(deleteObj);

                swal({
                    title: '',
                    text: "Are you sure you want to Delete?",
                    showCloseButton: true,
                    confirmButtonText: 'Yes',
                    showCancelButton: true,
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        $http.post(ENV.apiUrl + "api/EmpDocumentupload/DocumentDelete", deleteObj).then(function (msg) {
                            debugger
                            $scope.msg1 = msg.data;
                            console.log(msg);
                            if ($scope.msg1 == true) {
                                swal({ text: "Document deleted successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.showEmpDocumentList($scope.temp.em_login_code);
                                    }
                                });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Document not deleted", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                });
                            }
                            else {
                                swal({ text: $scope.msg1, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                });
                            }

                        });
                    }

                });
            }

            $scope.downloaddoc = function (str) {
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeDocument/' + str;
                window.open($scope.url);
            }

            function getRecDateWise(dt) {
                var a = [];
                for (var i = 0; i < $scope.feetemp.length; i++) {
                    if ($scope.feetemp[i].pays_doc_empl_name == dt) {
                        a.push($scope.feetemp[i]);
                    }
                }
                DeleteColumns(a);
                return a;
            }

            function DeleteColumns(arr) {
                var l = arr.length;
                var temp = [];
                for (var i = 0; i < l; i++) {
                    for (var v in arr[i].PAvals) {
                        var ob = { 'mode': v, 'value': 0 };
                        temp.push(ob);
                    }
                    break;
                }
                for (var i = 0; i < l; i++) {
                    for (var t = 0; t < temp.length; t++) {
                        temp[t].value = parseFloat(temp[t].value) + parseFloat(arr[i].PAvals[temp[t].mode]);
                    }
                }
                var finalPA = {};
                for (var x = 0; x < temp.length; x++) {
                    if (temp[x].value != 0) {
                        finalPA[temp[x].mode] = temp[x].value;
                    }
                }
                for (var i = 0; i < l; i++) {
                    var n = {};
                    var he = [];
                    for (var x in finalPA) {
                        n[x] = arr[i].PAvals[x];
                        he.push(x);
                    }
                    arr[i].PAvals = n;
                    arr[i].PAcols = he;
                }
            }

            /*----------------   Upoload Document Code end ------------- */

        }]
        )


    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

})();