﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AirFareEmployeeDetailsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.grid = true;
            $scope.edit_data = false;
            $scope.AirFareEmp_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.edit_code = false;
            $scope.filter_combo = true;

            $scope.empnum_Readonly = false;
            $scope.nation_Readonly = false;
            $scope.grade_Readonly = false;
            $scope.class_Readonly = false;

            $scope.enroll_number_lists = [];
            $scope.username = $rootScope.globals.currentUser.username;
            $rootScope.visible_stud = false;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = true;
            $rootScope.chkMulti = false;
            $scope.emp_name = '';
            $scope.edt = {};
            $scope.temp = {};

            $scope.edit_data = false;
            $scope.btn_empsearch = false;
            $scope.edt.afe_reimbursement = 'C';

            $scope.countData = [
             { val: 5, data: 5 },
             { val: 10, data: 10 },
             { val: 15, data: 15 },

            ]

            $scope.size = function (str) {
                //if (str == 5 || str == 10 || str == 15) {
                //    $scope.pager = true;
                //}
                //else {
                //    $scope.pager = false;
                //}
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $timeout(function () {
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);


            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.filter_combo = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.btn_empsearch = true;
                $scope.edit_data = false;
                $scope.edt.afe_reimbursement = 'C';
            }

            $scope.$on('global_cancel', function (str) {

                $scope.Empmodal_cancel();
            });

            $scope.getEmployee1 = function () {
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }

            $scope.globalSearchCustom = function () {
                $scope.temp = {};
                $scope.NodataEmployee = false;
                $scope.showEmployeeFilterBtn = true;
                $scope.EmployeeTable = false;
                $scope.global_search_result = [];
                $('#myModal').modal('show');
                $scope.temp.company_code = $scope.cmbCompany[0].sh_company_code;
            }

            $http.get(ENV.apiUrl + "api/common/GlobalSearch/GetCompanyNameForShift").then(function (res) {
                $scope.cmbCompany = res.data;
                $scope.temp.company_code = $scope.cmbCompany[0].sh_company_code;
                $scope.getDeptFromCompany($scope.cmbCompany[0].sh_company_code);
                $scope.getDesignationFromCompany($scope.cmbCompany[0].sh_company_code);
            });

            $scope.getDeptFromCompany = function (company_code) {
                ////$http.get(ENV.apiUrl + "api/common/GlobalSearch/GetDepartmentName?company_code=" + company_code).then(function (res) {
                ////    $scope.compDepartment = res.data;
                ////});
                $http.get(ENV.apiUrl + "api/emp/attendance/getDeptName?comp_code=" + company_code).then(function (dept) {
                    $scope.compDepartment = dept.data;
                });
            }

            $scope.getDesignationFromCompany = function (company_code) {
                $http.get(ENV.apiUrl + "api/EmployeeReport/GetDesignation?com_code=" + company_code).then(function (res) {
                    $scope.compDesignation = res.data;
                });
            }

            $scope.Search_by_employee = function () {;
                debugger
                $scope.NodataEmployee = false;
                var obj = {};
                obj.em_login_code = $scope.temp.em_login_code;
                obj.EmpName = $scope.temp.EmpName;
                obj.em_department = $scope.temp.em_department;
                obj.em_desg_code = $scope.temp.em_desg_code;
                obj.company_code = $scope.temp.company_code;
                //obj.resignedEmpFlag = "Yes";
                console.log("obj", obj);

                $http.post(ENV.apiUrl + "api/AirFareEmp/AifareAssignedEmployee?data=" + JSON.stringify(obj)).then(function (SearchEmployee_Data) {
                   
                    $scope.global_search_result = SearchEmployee_Data.data;
                    $scope.EmployeeTable = true;

                    //if (SearchEmployee_Data.data.length > 0) {
                    //    $scope.global_search_result = SearchEmployee_Data.data;
                    //    $scope.EmployeeTable = true;                                              
                    //}
                    //else {
                    //    $scope.global_search_result = [];
                    //    swal({ title: 'Alert', text: "No record(s) found.", showCloseButton: true, width: 450, height: 200, timer: 1000 });
                    //    $scope.NodataEmployee = true;
                    //    //  $scope.EmployeeTable = false;
                    //}                   
                    
                });
            }

            $scope.emp_modal_cancel = function () {
                $('#myModal').modal('hide');
                $scope.temp = {};
                for (var i = 0; i < $scope.SelectedUserLst.length; i++) {
                    $scope.enroll_number_lists.push($scope.SelectedUserLst[i]);
                    $scope.test_emp = $scope.SelectedUserLst[i];
                    $scope.emp_name = $scope.test_emp.em_login_code;
                    $scope.empName = $scope.test_emp.empName
                }
                // console.log($scope.SelectedUserLst);
                $scope.edt['afe_em_number'] = $scope.emp_name;

                console.log($scope.AirFare_data);

                for (var j = 0; j < $scope.AirFare_data.length; j++) {

                    if ($scope.AirFare_data[j].afe_em_number == $scope.emp_name) {
                        
                            $scope.edt.afe_em_number= $scope.AirFare_data[j].afe_em_number;
                            $scope.edt.afe_dest_code= $scope.AirFare_data[j].afe_dest_code;
                            $scope.edt.ds_name= $scope.AirFare_data[j].ds_name;
                            $scope.edt.afe_nation_code=$scope.AirFare_data[j].afe_nation_code;
                            $scope.edt.sims_country_name_en= $scope.AirFare_data[j].sims_country_name_en;
                            $scope.edt.gr_code= $scope.AirFare_data[j].gr_code;
                            $scope.edt.gr_desc= $scope.AirFare_data[j].gr_desc;
                            $scope.edt.afe_class= $scope.AirFare_data[j].afe_class,
                            $scope.edt.afe_class_name= $scope.AirFare_data[j].afe_class_name;
                            $scope.edt.afe_adult_rate= $scope.AirFare_data[j].afe_adult_rate;
                            $scope.edt.afe_child_rate= $scope.AirFare_data[j].afe_child_rate;
                            $scope.edt.afe_infant_rate= $scope.AirFare_data[j].afe_infant_rate;
                            $scope.edt.afe_finalize_status= $scope.AirFare_data[j].afe_finalize_status;                            
                            $scope.edt.afe_airport= $scope.AirFare_data[j].afe_airport;
                            $scope.edt.afe_travelling_date= $scope.AirFare_data[j].afe_travelling_date;
                            $scope.edt.afe_returing_date= $scope.AirFare_data[j].afe_returing_date;
                            $scope.edt.afe_created_by= $scope.AirFare_data[j].afe_created_by;
                            $scope.edt.afe_creation_date= $scope.AirFare_data[j].afe_creation_date;                            
                            $scope.edt.afe_grade_code = $scope.AirFare_data[j].afe_grade_code;
                            if($scope.AirFare_data[j].afe_applicable_frequency == "12"){
                                $scope.edt.afe_applicable_frequency= "Yearly";
                            }
                            else {
                                $scope.edt.afe_applicable_frequency= "2 Yearly";
                            }
                            if ($scope.AirFare_data[j].afe_reimbursement == undefined || $scope.AirFare_data[j].afe_reimbursement == " " || $scope.AirFare_data[j].afe_reimbursement == "") {
                                $scope.edt.afe_reimbursement = 'C';
                            }
                            else {
                                $scope.edt.afe_reimbursement = $scope.AirFare_data[j].afe_reimbursement;
                            }
                            $scope.edt.afe_applicable_month = $scope.AirFare_data[j].afe_applicable_month;
                            $scope.edt.afe_id = $scope.AirFare_data[j].afe_id;
                            $scope.edt.afe_parent_id = $scope.AirFare_data[j].afe_parent_id;
                            $scope.edt.afe_amount_ticket = $scope.AirFare_data[j].afe_amount_ticket;

                            //if ($scope.edt.gr_code == $scope.AirFare_data[j].gr_code) {                                
                            //    $scope.edt['afe_adult_rate'] = $scope.AirFare_data[j].af_adult_rate;
                            //    $scope.edt['afe_infant_rate'] = $scope.AirFare_data[j].af_infant_rate;
                            //    $scope.edt['afe_child_rate'] = $scope.AirFare_data[j].af_child_rate;
                            //    $scope.edt['afe_applicable_month'] = $scope.AirFare_data[j].af_default_applicable_month;
                            //}
                            
                    }
                }
                console.log("edt", $scope.edt);
            }


            $scope.Empmodal_cancel = function () {
                for (var i = 0; i < $scope.SelectedUserLst.length; i++) {
                    $scope.enroll_number_lists.push($scope.SelectedUserLst[i]);
                    $scope.test_emp = $scope.SelectedUserLst[i];
                    $scope.emp_name = $scope.test_emp.em_login_code;
                    $scope.empName = $scope.test_emp.empName
                }
                // console.log($scope.SelectedUserLst);
                $scope.edt['afe_em_number'] = $scope.emp_name;
            }

            $http.get(ENV.apiUrl + "api/emp/attendance/getCompanyName").then(function (comp) {
                $scope.comp_data = comp.data;
                $scope.edt.company_code = $scope.comp_data[0].company_code;
                $scope.Comp_Change($scope.edt['company_code']);
            });

            $scope.Comp_Change = function (company_code) {
                $http.get(ENV.apiUrl + "api/emp/attendance/getDeptName?comp_code=" + $scope.edt.company_code).then(function (dept) {
                    $scope.dept_data = dept.data;                   
                });

                $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/Get_designation_Code?company_code=" + company_code).then(function (Designation) {
                    $scope.designation = Designation.data;
                });

                $http.get(ENV.apiUrl + "api/generatepayroll/GetAllPaysGradeName?company_code=" + company_code).then(function (GetAllPaysGradeName) {
                    $scope.GetAllPaysGradeName = GetAllPaysGradeName.data;
                });
            }

            $http.get(ENV.apiUrl + "api/emp/attendance/getStaffType").then(function (stafftype) {
                $scope.stafftype_data = stafftype.data;
            });

            $http.get(ENV.apiUrl + "api/emp/attendance/getDesignation").then(function (designation) {
                $scope.designation_data = designation.data;
            });

            $http.get(ENV.apiUrl + "api/emp/attendance/getYear").then(function (year) {
                $scope.year_data = year.data;
                $scope.edt.financial_year = year.data[0].sims_academic_year;
            });


            $http.get(ENV.apiUrl + "api/AirFareEmp/getairfaredetails").then(function (res) {
                $scope.airfare_data = res.data;
                console.log($scope.airfare_data);
            });

            var v = [];
            $scope.getairfareother = function (str) {
                //console.log(str);
                v = str.split('-');
                //console.log(v);

                $scope.gr_code = v[0];
                $scope.edt['afe_nation_code'] = v[1];
                $scope.edt['afe_dest_code'] = v[2];
                $scope.edt['afe_class'] = v[3];

                for (var i = 0; i < $scope.airfare_data.length; i++) {
                    if (str == $scope.airfare_data[i].gr_code) {
                        //$scope.edt['afe_applicable_frequency'] = $scope.airfare_data[i].af_applicable_frequency;
                        if ($scope.airfare_data[j].afe_applicable_frequency == "12") {
                            $scope.edt.afe_applicable_frequency = "Yearly";
                        }
                        else {
                            $scope.edt.afe_applicable_frequency = "2 Yearly";
                        }
                        $scope.edt['afe_adult_rate'] = $scope.airfare_data[i].af_adult_rate;
                        $scope.edt['afe_infant_rate'] = $scope.airfare_data[i].af_infant_rate;
                        $scope.edt['afe_child_rate'] = $scope.airfare_data[i].af_child_rate;
                        $scope.edt['afe_applicable_month'] = $scope.airfare_data[i].af_default_applicable_month;
                    }
                }
            }

            $http.get(ENV.apiUrl + "api/AirFareEmp/getDestinationAirFare").then(function (res) {
                $scope.Destination_data = res.data;
                console.log($scope.Destination_data);
            });

            $http.get(ENV.apiUrl + "api/AirFareEmp/getNations").then(function (res) {
                $scope.Nation_data = res.data;
                console.log($scope.Nation_data);
            });

            $http.get(ENV.apiUrl + "api/AirFareEmp/getGrades").then(function (res) {
                $scope.Grade_data = res.data;
                // console.log($scope.Nation_data);
            });

            $http.get(ENV.apiUrl + "api/AirFareEmp/getClass").then(function (res) {
                $scope.Class_data = res.data;
                console.log($scope.Class_data);
            });

            $scope.show_Airfare_Emp_data = function () {

                $http.get(ENV.apiUrl + "api/AirFareEmp/getAirFareEmployeeDetails?company_code=" + $scope.edt.company_code + "&dept_code=" + $scope.edt.dept_code +
                    "&grade_code=" + $scope.edt.gr_code + "&staff_type_code=" + $scope.edt.staff_type_code + "&dg_code=" + $scope.edt.dg_code + "&afe_em_number=" + $scope.edt.afe_em_number).then(function (res) {

                        $scope.AirFareEmp_data = res.data;

                        if ($scope.AirFareEmp_data.length == 0) {
                            swal({ text: "Record not found", showCloseButton: true, width: 380, })
                            $scope.grid = false;
                        }
                        else {
                            $scope.display = false;
                            $scope.grid = true;
                            $scope.totalItems = $scope.AirFareEmp_data.length;
                            $scope.todos = $scope.AirFareEmp_data;
                            $scope.makeTodos();
                            $scope.grid = true;
                        }
                    });
            }

            $scope.clear = function () {
                $scope.edt = {};
                $scope.edt.company_code = $scope.comp_data[0].company_code;
                $scope.edt.financial_year = $scope.year_data[0].sims_academic_year;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
            };

            $scope.edit = function (str) {
                debugger
                $scope.empnum_Readonly = true;
                $scope.nation_Readonly = true;
                $scope.grade_Readonly = true;
                $scope.class_Readonly = true;

                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edit_data = true;
                $scope.edit_code = true;
                $scope.filter_combo = false;
                    
                $scope.edt.afe_em_number = str.afe_em_number;
                $scope.edt.afe_dest_code = str.afe_dest_code;
                $scope.edt.ds_name = str.ds_name;
                $scope.edt.afe_nation_code = str.afe_nation_code;
                $scope.edt.sims_country_name_en = str.sims_country_name_en;
                $scope.edt.gr_code = str.gr_code;
                $scope.edt.gr_desc = str.gr_desc;
                $scope.edt.afe_class = str.afe_class;
                $scope.edt.afe_class_name = str.afe_class_name;
                $scope.edt.afe_adult_rate = str.afe_adult_rate;
                $scope.edt.afe_child_rate = str.afe_child_rate;
                $scope.edt.afe_infant_rate = str.afe_infant_rate;
                $scope.edt.afe_finalize_status = str.afe_finalize_status;
                $scope.edt.afe_reimbursement = str.afe_reimbursement;
                $scope.edt.afe_airport = str.afe_airport;
                $scope.edt.afe_travelling_date = str.afe_travelling_date;
                $scope.edt.afe_returing_date = str.afe_returing_date;
                $scope.edt.afe_created_by = str.afe_created_by;
                $scope.edt.afe_creation_date = str.afe_creation_date;                
                $scope.edt.afe_grade_code = str.afe_grade_code;
                $scope.edt.afe_id = str.afe_id;
                $scope.edt.afe_parent_id = str.afe_parent_id;
                $scope.edt.afe_amount_ticket = str.afe_amount_ticket;
                if(str.afe_applicable_frequency == '12'){
                    $scope.edt.afe_applicable_frequency = 'Yearly';
                }
                else {
                    $scope.edt.afe_applicable_frequency = '2 Yearly';
                }

                if(str.afe_reimbursement == undefined || str.afe_reimbursement == " " || str.afe_reimbursement == "") {
                    $scope.edt.afe_reimbursement = 'C';
                }
                else {
                    $scope.edt.afe_reimbursement = str.afe_reimbursement;
                }
                $scope.edt.afe_academic_year = str.afe_academic_year;
                    
                $scope.btn_empsearch = false;
            }

            $scope.Save = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        if ($scope.edt.afe_applicable_frequency == 'Yearly') {
                            $scope.edt.afe_applicable_frequency = '12'
                        }
                        else {
                            $scope.edt.afe_applicable_frequency = '24'
                        }

                        data1 = [];
                        var data = ({
                            afe_dest_code: $scope.edt.afe_dest_code,
                            afe_grade_code: $scope.edt.afe_grade_code,
                            afe_nation_code: $scope.edt.afe_nation_code,
                            afe_class: $scope.edt.afe_class,
                            //afe_applicable_frequency: $scope.edt.af_applicable_frequency,
                            afe_adult_rate: $scope.edt.afe_adult_rate,
                            afe_child_rate: $scope.edt.afe_child_rate,
                            afe_infant_rate: $scope.edt.afe_infant_rate,
                            afe_applicable_month: $scope.edt.afe_applicable_month,
                            // af_payroll_flag: $scope.edt.af_payroll_flag,
                            // af_gl_acct_no: $scope.edt.af_gl_acct_no,
                            // af_pay_code: $scope.edt.af_pay_code,
                            afe_airport: $scope.edt.afe_airport,
                            afe_travelling_date: $scope.edt.afe_travelling_date,
                            afe_returing_date: $scope.edt.afe_returing_date,
                            afe_finalize_status: $scope.edt.afe_finalize_status,
                            afe_reimbursement: $scope.edt.afe_reimbursement,
                            afe_created_by: $scope.username,
                            afe_em_number: $scope.edt.afe_em_number,
                            afe_old_frequency: $scope.edt.afe_old_frequency,
                            afe_applicable_frequency: $scope.edt.afe_applicable_frequency,
                            afe_amount_ticket: $scope.edt.afe_amount_ticket,
                            afe_id: $scope.edt.afe_id,
                            afe_academic_year: $scope.edt.afe_academic_year,
                            opr: 'N'
                        });

                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/AirFareEmp/CUAirFareEmpTransaction", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            console.log($scope.msg1);
                            if ($scope.msg1 == true) {
                                swal({ text: "Airfare Transaction Save Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.clear();
                                        $scope.show_Airfare_Emp_data();
                                        $scope.cancel();
                                    }
                                });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Airfare Data Already Exist.", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.clear();
                                        $scope.show_Airfare_Emp_data();
                                        $scope.cancel();
                                    }
                                });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/AirFareEmp/getAllAirFareEmployee").then(function (res) {                   
                    $scope.AirFare_data = res.data;                    
                });
            }

            $scope.getgrid();

            $scope.Update = function (isvalidate) {
                debugger;
                var data1 = [];
                if (isvalidate) {

                    //var data = ({
                    //    //afe_em_number:$scope.edt.afe_em_number,
                    //    //afe_dest_code: $scope.edt.afe_dest_code,
                    //    //ds_name: $scope.edt.ds_name,
                    //    //afe_nation_code: $scope.edt.afe_nation_code,
                    //    //sims_country_name_en: $scope.edt.sims_country_name_en,
                    //    //afe_grade_code: $scope.edt.afe_grade_code,
                    //    //gr_desc: $scope.edt.gr_desc,
                    //    //afe_class: $scope.edt.afe_class,
                    //    //afe_class_name: $scope.edt.afe_class_name,
                    //    //afe_adult_rate: $scope.edt.afe_adult_rate,
                    //    //afe_child_rate: $scope.edt.afe_child_rate,
                    //    //afe_infant_rate: $scope.edt.afe_infant_rate,
                    //    //afe_finalize_status: $scope.edt.afe_finalize_status,

                    //    afe_dest_code: $scope.edt.afe_dest_code,
                    //    afe_grade_code: $scope.edt.afe_grade_code,
                    //    afe_nation_code: $scope.edt.afe_nation_code,
                    //    afe_class: $scope.edt.afe_class,
                    //    //afe_applicable_frequency: $scope.edt.af_applicable_frequency,
                    //    afe_adult_rate: $scope.edt.afe_adult_rate,
                    //    afe_child_rate: $scope.edt.afe_child_rate,
                    //    afe_infant_rate: $scope.edt.afe_infant_rate,
                    //    afe_applicable_month: $scope.edt.afe_applicable_month,
                    //    // af_payroll_flag: $scope.edt.af_payroll_flag,
                    //    // af_gl_acct_no: $scope.edt.af_gl_acct_no,
                    //    // af_pay_code: $scope.edt.af_pay_code,
                    //    afe_airport: $scope.edt.afe_airport,
                    //    afe_travelling_date: $scope.edt.afe_travelling_date,
                    //    afe_returing_date: $scope.edt.afe_returing_date,
                    //    afe_finalize_status: $scope.edt.afe_finalize_status,
                    //    afe_reimbursement: $scope.edt.afe_reimbursement,
                    //    afe_created_by: $scope.username,
                    //    afe_em_number: $scope.edt.afe_em_number,
                    //    afe_old_frequency: $scope.edt.afe_old_frequency,
                    //    afe_applicable_frequency: $scope.edt.afe_applicable_frequency,
                    //    opr: 'U'
                    //});

                    if ($scope.edt.afe_applicable_frequency == 'Yearly') {
                            $scope.edt.afe_applicable_frequency = '12'
                        }
                        else {
                            $scope.edt.afe_applicable_frequency = '24'
                        }

                        data1 = [];
                        var data = ({
                            afe_dest_code: $scope.edt.afe_dest_code,
                            afe_grade_code: $scope.edt.afe_grade_code,
                            afe_nation_code: $scope.edt.afe_nation_code,
                            afe_class: $scope.edt.afe_class,
                            //afe_applicable_frequency: $scope.edt.af_applicable_frequency,
                            afe_adult_rate: $scope.edt.afe_adult_rate,
                            afe_child_rate: $scope.edt.afe_child_rate,
                            afe_infant_rate: $scope.edt.afe_infant_rate,
                            afe_applicable_month: $scope.edt.afe_applicable_month,
                            // af_payroll_flag: $scope.edt.af_payroll_flag,
                            // af_gl_acct_no: $scope.edt.af_gl_acct_no,
                            // af_pay_code: $scope.edt.af_pay_code,
                            afe_airport: $scope.edt.afe_airport,
                            afe_travelling_date: $scope.edt.afe_travelling_date,
                            afe_returing_date: $scope.edt.afe_returing_date,
                            afe_finalize_status: $scope.edt.afe_finalize_status,
                            afe_reimbursement: $scope.edt.afe_reimbursement,
                            afe_created_by: $scope.username,
                            afe_em_number: $scope.edt.afe_em_number,
                            afe_old_frequency: $scope.edt.afe_old_frequency,
                            afe_applicable_frequency: $scope.edt.afe_applicable_frequency,
                            afe_amount_ticket: $scope.edt.afe_amount_ticket,
                            afe_id: $scope.edt.afe_id,
                            afe_academic_year: $scope.edt.afe_academic_year,
                            opr: 'N'
                        });

                    data1.push(data);

                    console.log($scope.edt.afe_grade_code);

                    $http.post(ENV.apiUrl + "api/AirFareEmp/CUAirFareEmpTransaction", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Employee Airfare Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.clear();
                                    $scope.getgrid();
                                    $scope.filter_combo = true;
                                    $scope.edt = {};
                                    $scope.cancel();
                                }
                            });

                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Employee Airfare Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.clear();
                                    $scope.getgrid();
                                    $scope.filter_combo = true;
                                    $scope.edt = {};
                                    $scope.cancel();
                                }
                            });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.cancel = function () {

                $scope.edt = {};
                $scope.edt.company_code = $scope.comp_data[0].company_code;
                $scope.edt.financial_year = $scope.year_data[0].sims_academic_year;
                $scope.grid = true;
                $scope.display = false;
                $scope.filter_combo = true;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.AirFareEmp_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.AirFareEmp_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.afe_em_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.gr_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.ds_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    || item.sims_country_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.size = function (str) {

                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['check_all'] = false;
                $scope.row1 = '';
                main.checked = false;
            }

        }])
})();