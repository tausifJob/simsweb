﻿(function () {
    'use strict';
    var main = '';
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('PayrollEmployee_DPSMISCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.busyindicator = false;
            $scope.grid = true;
            $scope.color = '#edefef';
            $scope.rgvtbl = false;
            $scope.btnhide = true;
            $scope.edt = {};
            $scope.temp = {};
            $scope.GetEmpDetails= [];
            $scope.GetEmpDetailsCopy = [];


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
                //format: 'dd-mm-yyyy'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);
            
            $scope.setStart = function (date) {
                //var year = date.split("/")[0];
                //var month = date.split("/")[1];
                //var day = date.split("/")[2];
                //var date1 = year + "-" + month + "-" + day;
                $scope.pa_effective_from = date;

            }

            $scope.setEnd = function (date) {
                //var year = date.split("/")[0];
                //var month = date.split("/")[1];
                //var day = date.split("/")[2];
                //var date1 = year + "-" + month + "-" + day;
                $scope.pa_effective_upto = date;

            }

            $http.get(ENV.apiUrl + "api/Dps/HRMS/getEmpData").then(function (res) {
                $scope.obj = res.data;
                console.log($scope.obj);
            });

            $http.get(ENV.apiUrl + "api/PaycodewiseEmployee/GetDept").then(function (GetDept) {
                $scope.GetDept = GetDept.data;

            });

            $http.get(ENV.apiUrl + "api/PaycodewiseEmployee/GetDesg").then(function (GetDesg) {
                $scope.GetDesg = GetDesg.data;

            });

            $http.get(ENV.apiUrl + "api/PaycodewiseEmployee/GetGrade").then(function (GetGrade) {
                $scope.GetGrade = GetGrade.data;

            });

            $http.get(ENV.apiUrl + "api/PaycodewiseEmployee/GetPayCode").then(function (GetPayCode) {
                $scope.GetPayCode = GetPayCode.data;
            });

            var date = new Date();
            var month = (date.getMonth() + 1);
            var day = date.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            //$scope.pa_effective_from = date.getFullYear() + '-' + (month) + '-' + (day);
           // $scope.pa_effective_from = ('0' + date.getDate()).slice(-2) + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + date.getFullYear();
            //$scope.pa_effective_upto = ('0' + date.getDate()).slice(-2) + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + date.getFullYear();
            //$scope.pa_effective_from = (day) + '-' + (month) + '-' + date.getFullYear();

            $scope.changeno = function (obj) {

                console.log(obj)
              
                obj.isChange = true;

                //for (var i = 0; i < $scope.GetEmpDetails.length; i++) {

                //    var v = document.getElementById(i + $scope.GetEmpDetails[i].em_login_code);

                //    if (v.checked == true) {                        
                //        var data = {
                //            em_number: $scope.GetEmpDetails[i].em_number,
                //            pa_effective_from: $scope.pa_effective_from,
                //            pa_effective_upto: $scope.pa_effective_upto,
                //            pd_pay_code: $scope.GetEmpDetails[i].paycode_code,
                //            pd_amount: $scope.GetEmpDetails[i].pay_amount,
                //            pd_remark: $scope.GetEmpDetails[i].pd_remark
                //        }
                //        datalst.push(data);
                //        $scope.flag = true;
                //    }
                //}
                //console.log("datalst", datalst);


                //console.log(obj)
                //$scope.maindata = obj;

                //if ($scope.maindata.leave_max >= 365 || $scope.maindata.leave_max <= 0) {
                //    swal('', 'Please enter 0 to 365 days.');
                //    $scope.maindata.leave_max = $scope.maindata.leave_max_old;
                //}
                //else {

                //    if (parseFloat($scope.maindata.leave_taken) > parseFloat($scope.maindata.leave_max)) {
                //        swal('', 'Maximum leave not less than taken leave.');
                //        $scope.maindata.leave_max = $scope.maindata.leave_max_old;
                //    }
                //    else
                //        $scope.maindata.leave_max_old = $scope.maindata.leave_max;
                //}
            }

            $scope.onlyNumbers = function (event, leave_max) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }

                }

                event.preventDefault();


            };

            $scope.Show_data = function () {

                debugger;
                if ($scope.edt.dept_code == undefined)
                    $scope.edt.dept_code = '';
                if ($scope.edt.desg_code == undefined)
                    $scope.edt.desg_code = '';
                if ($scope.edt.gr_code == undefined)
                    $scope.edt.gr_code = '';
                if ($scope.edt.paycode_code == '' || $scope.edt.paycode_code == undefined) {
                    swal('', 'Please select paycode.');
                }
                else {
                    if ($scope.pa_effective_from == '' || $scope.pa_effective_from == undefined) {
                        swal('', 'Please select effective from date.');
                    }
                    else {

                        $scope.busy = true;
                        var datalst = {
                            dept_code: $scope.edt.dept_code,
                            desg_code: $scope.edt.desg_code,
                            gr_code: $scope.edt.gr_code,
                            paycode_code: $scope.edt.paycode_code,
                            pa_effective_from: $scope.pa_effective_from
                        }
                        $http.post(ENV.apiUrl + "api/PaycodewiseEmployee/EmpDetails", datalst).then(function (EmpDetails) {
                            $scope.GetEmpDetails = EmpDetails.data;
                            $scope.GetEmpDetailsCopy = angular.copy(EmpDetails.data);

                            if ($scope.GetEmpDetails.length > 0) {
                                $scope.btnhide = false;
                                $scope.rgvtbl = true;
                            }
                            else {
                                $scope.rgvtbl = false;
                                $scope.btnhide = true;
                                swal('', 'Record not found.');
                            }
                            $scope.busy = false;
                        });

                    }
                }

            }

            $timeout(function () {
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.reset_form = function () {
                $scope.edt = {};
                $scope.GetEmpDetails = [];
                $scope.btnhide = true;
                $scope.rgvtbl = false;
                var date = new Date();
                var month = (date.getMonth() + 1);
                var day = date.getDate();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;

                $scope.pa_effective_from = '';
                //$scope.pa_effective_from = date.getFullYear() + '-' + (month) + '-' + (day);
                //$scope.pa_effective_from = ('0' + date.getDate()).slice(-2) + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + date.getFullYear();
                //$scope.pa_effective_from = (day) + '-' + (month) + '-' + date.getFullYear();
                $scope.pa_effective_upto = '';

            }

            $scope.genrate = function () {
                var datalst = [];
                
                //$scope.rgvtbl = false;
                //$scope.btnhide = true;
                console.log("GetEmpDetails", $scope.GetEmpDetails);
                console.log("GetEmpDetailsCopy", $scope.GetEmpDetailsCopy);

                $scope.flag = false;
                debugger;
                if ($scope.pa_effective_upto == '' || $scope.pa_effective_upto == undefined) {
                    $scope.pa_effective_upto = '';
                }
                for (var i = 0; i < $scope.GetEmpDetails.length; i++) {
               
                    var v = document.getElementById(i + $scope.GetEmpDetails[i].em_login_code);

                    if (v.checked == true) {
                        $scope.busy = true;

                        for (var j = 0; j < $scope.GetEmpDetailsCopy.length; j++) {
                            if ($scope.GetEmpDetails[i].em_login_code == $scope.GetEmpDetailsCopy[j].em_login_code && $scope.GetEmpDetails[i].pd_pay_code == $scope.GetEmpDetailsCopy[j].pd_pay_code) {
                                var data = {
                                    //em_number: $scope.GetEmpDetails[i].em_number,
                                    em_number: $scope.GetEmpDetails[i].em_login_code,
                                    pa_effective_from: $scope.pa_effective_from,
                                    pa_effective_upto: $scope.pa_effective_upto,
                                    pd_pay_code: $scope.GetEmpDetails[i].paycode_code,
                                    pd_amount: $scope.GetEmpDetails[i].pay_amount,
                                    pd_remark: $scope.GetEmpDetails[i].pd_remark,
                                    ph_new_amount: $scope.GetEmpDetails[i].pay_amount,
                                    ph_old_amount: $scope.GetEmpDetailsCopy[j].pay_amount,
                                    ph_user_name: $rootScope.globals.currentUser.username
                                }
                                datalst.push(data);
                                $scope.flag = true;
                            }                            
                        }                        
                    }
                }
                console.log("post data", datalst);

                if ($scope.flag) {
                    $http.post(ENV.apiUrl + "api/PaycodewiseEmployee/UpdatePaysPayable", datalst).then(function (UpdatePaysPayable) {
                        $scope.UpdatePaysPayable = UpdatePaysPayable.data;

                        swal('', 'Record successfully updated.');

                        setTimeout(function () {
                            $(document).ready(function () {
                                $("#effective_date, #effective_up_date").kendoDatePicker({
                                    format: "dd-MM-yyyy",
                                    value: ''
                                });
                            });
                        }, 500);

                        $scope.busy = false;
                        $scope.reset_form();
                    });
                }
                else {
                   // $scope.busy = false;
                    swal('', 'Please Select Employee.');
                }

            }

            $scope.History = function (obj) {
                
                $http.post(ENV.apiUrl + "api/PayrollEmployeeDPSD/HistoryPaycode?sd_number=" + obj.em_login_code).then(function (HistoryPaycode) {
                    $scope.HistoryPaycode = HistoryPaycode.data;
                    debugger;
                    if ($scope.HistoryPaycode.length > 0) {
                        $('#historyModal').modal('show');
                        $scope.empName = obj.emp_name;
                        $scope.empLoginId = obj.em_login_code;
                    }
                    else {
                        swal({title:'Alert', title:'No history found.',width:320});
                        $scope.empName = '';
                        $scope.empLoginId = '';
                    }
                      
                });
            }
            $scope.Closehistory = function () {
                $scope.HistoryPaycode = [];
                $scope.empName = '';
                $scope.empLoginId = '';
                $('#historyModal').modal('hide');
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.GetEmpDetails.length; i++) {
                        var v = document.getElementById(i + $scope.GetEmpDetails[i].em_login_code);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.GetEmpDetails.length; i++) {
                        var v = document.getElementById(i + $scope.GetEmpDetails[i].em_login_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = 'row_selected';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';                        
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }


        }])
})();