﻿(function () {
    'use strict';
    var main = '';
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

    simsController.controller('TransferEmployeeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {


            var uname = $rootScope.globals.currentUser.username;
            $scope.display = false;
            $scope.busyindicator = false;
            $scope.grid = true;
            $scope.color = '#edefef';
            $scope.rgvtbl = false;
            $scope.department = false;
            $scope.tempD = {};            
            $scope.showTable = false;

            $scope.Show_dept = function () {
                $scope.department = true;
            }
            $scope.Show_grade = function () {
                $scope.grade = true;
            }
            $scope.Show_designation = function () {
                $scope.designation = true;
            }
            $scope.Show_stafftype = function () {
                $scope.stafftype = true;
            }
            $scope.Reset_dept = function () {
                $scope.department = false;
            }
            $scope.Reset_grade = function () {
                $scope.grade = false;
            }
            $scope.Reset_designation = function () {
                $scope.designation = false;
            }
            $scope.Reset_stafftype = function () {
                $scope.stafftype = false;
            }


            $http.get(ENV.apiUrl + "api/TransferEmp/getCompany").then(function (compdata) {
                $scope.comp_data = compdata.data;
                $scope.tempD['em_Company_Code'] = $scope.comp_data[0].em_Company_Code;
                $scope.getdept($scope.tempD['em_Company_Code']);
                $scope.getdesg($scope.tempD['em_Company_Code']);
                $scope.getGrade($scope.tempD['em_Company_Code']);               
            });

            $scope.getdept = function (dept) {

                $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/Get_dept_Code?company_code=" + dept).then(function (Department) {
                    $scope.dept = Department.data;
                });
            }

            $scope.getdesg = function (design) {
                $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/Get_designation_Code?company_code=" + design).then(function (Designation) {
                    $scope.designation = Designation.data;
                });
            }

            $scope.getGrade = function (company_code) {
                $http.get(ENV.apiUrl + "api/generatepayroll/GetAllPaysGradeName?company_code=" + company_code).then(function (GetAllPaysGradeName) {
                    $scope.GetAllPaysGradeName = GetAllPaysGradeName.data;
                });
            }
            //$http.get(ENV.apiUrl + "api/TransferEmp/getDepartment").then(function (deptdata) {
            //    $scope.dept_data = deptdata.data;
            //});
            //$http.get(ENV.apiUrl + "api/TransferEmp/getGrade").then(function (gradedata) {
            //    $scope.grade_data = gradedata.data;
            //});
            //$http.get(ENV.apiUrl + "api/TransferEmp/getDesignation").then(function (desgdata) {
            //    $scope.desg_data = desgdata.data;
            //});
            $http.get(ENV.apiUrl + "api/TransferEmp/getStaffType").then(function (staffdata) {
                $scope.staff_data = staffdata.data;
            });

            $scope.getEmployee = function (comp_code, dept_code, grade_code, designation_code, stafftype_code, emp_name, employee_no) {
                $scope.busyindicator = true;
                $scope.showTable = false;

                $http.get(ENV.apiUrl + "api/TransferEmp/getSearchEmployee?em_company_code=" + comp_code + "&em_dept_code=" + dept_code + "&em_grade_code=" + grade_code +
                "&em_desg_code=" + designation_code + "&em_staff_type_code=" + stafftype_code + "&emp_name=" + emp_name + "&em_login_code=" + employee_no).then(function (employeeData) {
                    $scope.employee_Data = employeeData.data;


                    if ($scope.employee_Data.length > 0) {
                        $scope.busyindicator = false;
                        $scope.showTable = true;
                    }
                    else {
                        swal({ title: "Alert!", text: "Records Not Found", showCloseButton: true, width: 380, });
                        $scope.stud_table = false;
                        $scope.busyindicator = false;
                        $scope.showTable = false;
                    }

                    angular.forEach($scope.employee_Data, function (value, key) {
                        if (value.em_dept_effect_from == "1900-01-01") {
                            value.em_dept_effect_from = '' //$filter('date')(new Date(), 'yyyy-MM-dd');
                        }
                        if (value.em_grade_effect_from == "1900-01-01") {
                            value.em_grade_effect_from = ''
                        }
                        if (value.em_desg_effect_from == "1900-01-01") {
                            value.em_desg_effect_from = ''
                        }
                        if (value.em_staff_type_effect_from == "1900-01-01") {
                            value.em_staff_type_effect_from = ''
                        }
                    });

                });
            }

            $scope.Reset = function () {
                $scope.tempD = {};
                $scope.tempD['em_Company_Code'] = $scope.comp_data[0].em_Company_Code;
                $scope.employee_Data = [];
                $scope.showTable = false;
            }

            $scope.deptChange = function (info) { info.ischange = true; }
            $scope.DeptEfChange = function (info) { info.ischange = true; }

            $scope.EmpTransUpdate = function () {
                var currtime = $filter('date')(new Date(), 'hh:mm:ss');
                var currtime1 = $filter('date')(new Date(), 'hh:mm:ss');
                var currDate = $filter('date')(new Date(), 'dd-MM-yyyy');                
                var dataupdate = [];
                var data = {};
                for (var k = 0; k < $scope.employee_Data.length; k++) {
                    if ($scope.employee_Data[k].ischange == true) {
                        if ($scope.employee_Data[k].em_dept_effect_from == undefined || $scope.employee_Data[k].em_dept_effect_from == "") {
                            var deptEffectFrom = currDate;
                        }
                        else {
                            deptEffectFrom = $scope.employee_Data[k].em_dept_effect_from;
                        }
                        if ($scope.employee_Data[k].em_grade_effect_from == undefined || $scope.employee_Data[k].em_grade_effect_from == "") {
                            var gradeEffectFrom = currDate;
                        }
                        else {
                            gradeEffectFrom = $scope.employee_Data[k].em_grade_effect_from;
                        }
                        if ($scope.employee_Data[k].em_desg_effect_from == undefined || $scope.employee_Data[k].em_desg_effect_from == "") {
                            var desgEffectFrom = currDate;
                        }
                        else {
                            desgEffectFrom = $scope.employee_Data[k].em_desg_effect_from;
                        }
                        if ($scope.employee_Data[k].em_staff_type_effect_from == undefined || $scope.employee_Data[k].em_staff_type_effect_from == "") {
                            var staffEffectFrom = currDate;
                        }
                        else {
                            staffEffectFrom = $scope.employee_Data[k].em_staff_type_effect_from;
                        }
                        if ($scope.employee_Data[k].em_company_effect_from == undefined || $scope.employee_Data[k].em_company_effect_from == "") {
                            var companyEffectFrom = currDate;
                        }
                        else {
                            companyEffectFrom = $scope.employee_Data[k].em_company_effect_from;
                        }

                        data =
                       {
                           em_Company_Code: $scope.employee_Data[k].em_Company_Code,
                           em_login_code: $scope.employee_Data[k].em_login_code,
                           em_number: $scope.employee_Data[k].em_number,

                           em_Dept_code: $scope.employee_Data[k].em_Dept_code,
                           em_dept_effect_from: deptEffectFrom,

                           em_Grade_code: $scope.employee_Data[k].em_Grade_code,
                           em_grade_effect_from: gradeEffectFrom,

                           em_Desg_code: $scope.employee_Data[k].em_Desg_code,
                           em_desg_effect_from: desgEffectFrom,

                           em_Staff_type_code: $scope.employee_Data[k].em_Staff_type_code,
                           em_staff_type_effect_from: staffEffectFrom,
                           em_company_effect_from: companyEffectFrom,
                           opr: 'U'
                       }
                        dataupdate.push(data);
                    }

                }                
                if (dataupdate.length > 0) {                   
                    $http.post(ENV.apiUrl + "api/TransferEmp/TransEmpUpdate", dataupdate).then(function (msg) {
                        $scope.msg1 = msg.data;                        
                        if ($scope.msg1 == true) {
                            swal({ text: "Record updated successfully", imageUrl: "assets/img/check.png", width: 300, showCloseButton: true });
                            $scope.getEmployee($scope.tempD.em_Company_Code, $scope.tempD.em_Dept_code, $scope.tempD.em_Grade_code,
                                $scope.tempD.em_Desg_code, $scope.tempD.em_Staff_type_code, $scope.tempD.em_full_name, $scope.tempD.em_login_code);
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record not updated", imageUrl: "assets/img/close.png", width: 300, showCloseButton: true });
                        }
                        else {
                            swal("Error-"+$scope.msg1)
                        }

                        // $scope.stud_Stud_Img_Details_fun($scope.temp.sims_cur_code, $scope.temp.sims_academic_year, $scope.temp.sims_grade_code, $scope.temp.sims_section_code);
                    })
                    $scope.table = true;
                    $scope.newdisplay = false;
                    dataupdate = [];
                }
                else {
                    swal({ text: "Change atleast one record to update", imageUrl: "assets/img/notification-alert.png", width: 380, showCloseButton: true });
                }
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#Table2").tableHeadFixer({ 'top': 1 });
            }, 100);

        }])
})();