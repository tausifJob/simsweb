﻿(function () {
    'use strict';
    var opr = '';
    var payconfirm = [];
    var main;

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('LoanConfirmationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            
            $scope.table1 = true;
            $http.get(ENV.apiUrl + "api/LoanCodeConfirmation/getAllLoanConfirmation").then(function (getAllLoanConfirmation) {
                $scope.LoanConfirmation_Data = getAllLoanConfirmation.data;
                $scope.totalItems = $scope.LoanConfirmation_Data.length;
                $scope.todos = $scope.LoanConfirmation_Data;
                $scope.makeTodos();

            });


            $http.get(ENV.apiUrl + "api/common/getCompanyName").then(function (getCompanyName_Data) {
                $scope.CompanyName_Data = getCompanyName_Data.data;
              
            });

            $scope.GetLoandesc = function () {
                
                $http.get(ENV.apiUrl + "api/common/getAllLoanDesc?Company_code=" + $scope.edt.lm_company_code).then(function (getAllLoanDesc_Data) {
                    $scope.AllLoanDesc_Data = getAllLoanDesc_Data.data;
                    
                });

                $http.get(ENV.apiUrl + "api/common/getAllEmpName?Company_code=" + $scope.edt.lm_company_code).then(function (res) {
                    $scope.LoanEmpName_Data = res.data;
                   
                });
            }
        

            $scope.operation = false;
            $scope.editmode = false;

            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
               
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.LoanConfirmation_Data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

                //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
            }
            $scope.edt = "";

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.editmode = false;
                opr = 'U';
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = false;
                $scope.updatebtn = true;


            }

            $scope.up = function (str) {
                opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.operation = true;

                $scope.edt = str;
                $scope.GetLoandesc();

            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
            }

            $scope.Save = function (myForm) {
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'I';
                    $scope.exist = false;
                    for (var i = 0; i < $scope.LoanCode_Data.length; i++) {
                        if ($scope.LoanCode_Data[i].cp_display_order == data.cp_display_order) {
                            $scope.exist = true;
                        }

                    }
                    if ($scope.exist) {
                        swal({ text: "Display order Already exists", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 })
                    }

                    //if ($scope.exist) {
                    //    $rootScope.strMessage = 'Display order Already exists';

                    //    $('#message').modal('show');
                    //}

                    else {
                        $http.post(ENV.apiUrl + "api/LoanCode/LoanCodeCUD", data).then(function (msg) {
                            $scope.msg1 = msg.data;
                            $scope.operation = false;
                            $http.get(ENV.apiUrl + "api/LoanCode/getLoanCode").then(function (getLoanCode_Data) {
                                $scope.LoanCode_Data = getLoanCode_Data.data;
                                $scope.totalItems = $scope.LoanCode_Data.length;
                                $scope.todos = $scope.LoanCode_Data;
                                $scope.makeTodos();

                                $scope.msg1 = msg.data;

                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 })
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 })
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                                //if ($scope.msg1 == true) {

                                //    $rootScope.strMessage = 'Record Inserted Successfully';

                                //    $('#message').modal('show');
                                //}
                                //else {

                                //    $rootScope.strMessage = 'Record not Inserted';

                                //    $('#message').modal('show');
                                //}

                            });

                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                }

            }

            $scope.Update = function () {

                var data = $scope.edt;
                data.opr = 'U';
                
                $http.post(ENV.apiUrl + "api/LoanCodeConfirmation/LoanCodeConfirmationCUD", data).then(function (msg) {
                    $scope.msg1 = msg.data;

                    $scope.operation = false;
                    $http.get(ENV.apiUrl + "api/LoanCodeConfirmation/getAllLoanConfirmation").then(function (getAllLoanConfirmation) {
                        $scope.LoanConfirmation_Data = getAllLoanConfirmation.data;
                      
                        $scope.totalItems = $scope.LoanConfirmation_Data.length;
                        $scope.todos = $scope.LoanConfirmation_Data;
                        $scope.makeTodos();

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 })
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 })
                        }
                        else
                        {
                            swal("Error-" + $scope.msg1)
                        }

                        //if ($scope.msg1 == true) {

                        //    $rootScope.strMessage = 'Record Updated Successfully';

                        //    $('#message').modal('show');
                        //}
                        //else {

                        //    $rootScope.strMessage = 'Record Not Updated';

                        //    $('#message').modal('show');
                        //}
                    });

                })


                $scope.operation = false;
                $scope.table1 = true;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

               

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].lm_number;
                        var v = document.getElementById(t);
                        v.checked = true;
                        payconfirm = payconfirm + $scope.filteredTodos[i].lm_number + ','
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].lm_number;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
            }

            $scope.deleterecord = function () {

                payconfirm = [];

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].lm_number;
                    var v = document.getElementById(t);

                    if (v.checked == true)

                        payconfirm = payconfirm + $scope.filteredTodos[i].lm_number + ',';
                }

                var deletelocacode = ({
                    'lm_number': payconfirm,
                    'opr': 'D'
                });
               
                $http.post(ENV.apiUrl + "api/LoanCodeConfirmation/LoanCodeConfirmationCUD", deletelocacode).then(function (msg) {
                    $scope.msg1 = msg.data;

                    $scope.table1 = true;
                    $scope.operation = false;
                    $rootScope.strMessage = $scope.msg1.strMessage;
                    $http.get(ENV.apiUrl + "api/LoanCodeConfirmation/getAllLoanConfirmation").then(function (getAllLoanConfirmation) {
                        $scope.LoanConfirmation_Data = getAllLoanConfirmation.data;
                        $scope.totalItems = $scope.LoanConfirmation_Data.length;
                        $scope.todos = $scope.LoanConfirmation_Data;
                        $scope.makeTodos();

                        if ($scope.msg1 == true) {
                            swal({  text: "Record Deleted Successfully",imageUrl: "assets/img/check.png", width: 300, height: 200 })
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 })
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        //if ($scope.msg1 == true) {
                        //    $rootScope.strMessage = 'Record Deleted Successfully';
                        //    $('#message').modal('show');
                        //}
                        //else {
                        //    $rootScope.strMessage = 'Record Not Deleted';

                        //    $('#message').modal('show');
                        //}

                    });
                });

            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.PayCode_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.PayCode_Data;
                }
                $scope.makeTodos();
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                     item.lm_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    // item.companyName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                   //  item.employeeName.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.employeeName == toSearch ||
                     item.lm_amount == toSearch) ? true : false;
            }


        }])
})();


                                            