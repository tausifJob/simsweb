﻿(function () {
    'use strict';
    var formdata = new FormData();

    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeImageViewCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '8';
            $scope.showDDFlagForNISS = false;
            $scope.size = function (str) {                
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;  $scope.makeTodos();
            }
            $scope.edt = {};

            //if ($http.defaults.headers.common['schoolId'] == 'sms') {
            //    $scope.showDDFlagForNISS = true;
            //}

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();
            }

            $http.get(ENV.apiUrl + "api/common/GlobalSearch/GetCompanyNameForShift").then(function (res) {
                $scope.cmbCompany = res.data;
            });

            $scope.getDeptFromCompany = function (company_code) {
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/GetDepartmentName?company_code=" + company_code).then(function (res) {
                    $scope.compDepartment = res.data;
                });
            }

            $http.get(ENV.apiUrl + "api/HRMS/EmpImgShow/getDepartmentName").then(function (res) {
                $scope.departmentname = res.data;
            })

            $http.get(ENV.apiUrl + "api/HRMS/EmpImgShow/getGradeName").then(function (res) {
                $scope.gradename = res.data;
            })

            $scope.getGradeFromCompany = function (company_code) {
                $http.get(ENV.apiUrl + "api/HRMS/EmpImgShow/getGradeNameForNIS?company_code=" + company_code).then(function (res) {
                    $scope.gradecomp = res.data;
                })
            }

            $http.get(ENV.apiUrl + "api/HRMS/EmpImgShow/getStaffType").then(function (res) {
                $scope.staffname = res.data;
            })

            $http.get(ENV.apiUrl + "api/HRMS/EmpImgShow/getServiceStatus").then(function (serstat) {
                $scope.service_status_data = serstat.data;
            })

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 8;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.Reset = function () {                
                $scope.edt.codp_dept_no = "";
                $scope.edt.gr_code = "";
                $scope.edt.sims_appl_parameter = "";
                $scope.edt.pays_service_status_code = "";
                $scope.edt.company_code = "";
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.EmployeeData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.EmployeeData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.em_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.em_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.em_full_name == toSearch) ? true : false;
            }

            $scope.Show_Data = function () {
                debugger
                $scope.table1 = true;
                $scope.ImageView = false;
                
                if ($scope.showDDFlagForNISS) {
                    $http.get(ENV.apiUrl + "api/HRMS/EmpImgShow/getEmployeeDetailForNIS?codp_dept_no=" + $scope.edt.codp_dept_no + "&gr_code=" + $scope.edt.gr_code +
                    "&sims_appl_parameter=" + $scope.edt.sims_appl_parameter + "&pays_service_status_code=" + $scope.edt.pays_service_status_code + "&company_code=" + $scope.edt.company_code).then(function (Employee_Data) {
                        $scope.EmployeeData = Employee_Data.data;
                        $scope.totalItems = $scope.EmployeeData.length;
                        //$scope.imageUrl = ENV.apiUrl + 'Content/sjs/Images/EmployeeImages/';
                        $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/';
                        $scope.imageUrl_public = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'];
                        $scope.todos = $scope.EmployeeData;
                        $scope.makeTodos();                        
                        if (Employee_Data.data.length > 0) { }
                        else {
                            $scope.ImageView = true;
                        }
                    });
                }
                else {
                    $http.get(ENV.apiUrl + "api/HRMS/EmpImgShow/getEmployeeDetail?codp_dept_no=" + $scope.edt.codp_dept_no + "&gr_code=" + $scope.edt.gr_code +
                    "&sims_appl_parameter=" + $scope.edt.sims_appl_parameter + "&pays_service_status_code=" + $scope.edt.pays_service_status_code).then(function (Employee_Data) {
                        $scope.EmployeeData = Employee_Data.data;
                        $scope.totalItems = $scope.EmployeeData.length;
                        //$scope.imageUrl = ENV.apiUrl + 'Content/sjs/Images/EmployeeImages/';
                        $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/';

                        $scope.todos = $scope.EmployeeData;

                        $scope.makeTodos();                        
                        if (Employee_Data.data.length > 0) { }
                        else {
                            $scope.ImageView = true;
                        }
                    });
                }

            }

            //$scope.Show_Data = function () {
            //    
            //    $scope.table1 = true;
            //    $scope.ImageView = false;

            //    $http.get(ENV.apiUrl + "api/HRMS/EmpImgShow/getEmployeeDetail?codp_dept_no=" + $scope.edt.codp_dept_no + "&gr_code=" + $scope.edt.gr_code +
            //        "&sims_appl_parameter=" + $scope.edt.sims_appl_parameter + "&pays_service_status_code=" + $scope.edt.pays_service_status_code).then(function (Employee_Data) {
            //            
            //            $scope.EmployeeData = Employee_Data.data;
            //            $scope.totalItems = $scope.EmployeeData.length;
            //            //$scope.imageUrl = ENV.apiUrl + 'Content/sjs/Images/EmployeeImages/';
            //            $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/';
            //            $scope.todos = $scope.EmployeeData;
            //            $scope.makeTodos();
            //            console.log($scope.EmployeeData);
            //            if (Employee_Data.data.length > 0) { }
            //            else {
            //                $scope.ImageView = true;
            //            }
            //        });

            //}

            var imagename = '';

            $scope.UploadImageModal = function (str) {
                debugger;
                $("#prevImg").val('');
                $scope.edt.em_img = '';
                imagename = str.em_number;
                $('#myModal').modal('show');

                $scope.clear();
            }
            $scope.clear = function () {
                angular.element("input[type='file']").val(null);
                //$scope.temp['sims_student_img'] = '';
                
                $scope.prev_img = '';
                $scope.edt.em_img = '';
                 

            }
            $scope.delete = function (obj) {

                var deleteEmployeeImage = ({
                    'em_number': obj.em_number,
                    'em_img': obj.em_img,
                    'opr': 'N'
                });

                $http.post(ENV.apiUrl + "api/HRMS/EmpImgShow/CUDEmployeeDetails", deleteEmployeeImage).then(function (msg) {
                    $scope.msg1 = msg.data;
                   
                    if ($scope.msg1 == true) {
                        swal({ text: "Image Deleted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        $scope.Show_Data();
                    }
                    else if ($scope.msg1 == false) {

                        swal({ text: "Image Not Deleted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                });
                //$http.get(ENV.apiUrl + "api/HRMS/EmpImgShow/getEmployeeDetail").then(function (Employee_Data) {
                //    $scope.EmployeeData = Employee_Data.data;
                //    $scope.totalItems = $scope.EmployeeData.length;
                //    $scope.todos = $scope.EmployeeData;
                //    $scope.makeTodos();
                //    $scope.msg1 = msg.data;
                //});
            }


            $scope.file_changed = function (element) {

                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;
                        $(function () {
                            $('#prevImg').attr("src",$scope.prev_img);
                        })
                    });
                   
                };
                reader.readAsDataURL(photofile);

                $scope.Upload = function () {
                    debugger;
                    var random = Math.random();
                    var data = [];
                    //var t = $scope.photo_filename.split("/")[1];                    
                    data = {
                        em_number: imagename,
                        em_img: imagename + random + '.png',
                        opr: "K"
                    }
                    

                    $http.post(ENV.apiUrl + "api/HRMS/EmpImgShow/CUDUpdateEmpPics", data).then(function (res) {
                        $scope.result = res.data;
                        $("#prevImg").val('');
                        $scope.edt.em_img = '';
                     
                        $('#myModal').modal('hide');
                        if ($scope.result == true) {
                            swal({ text: "Image Upload Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            //$scope.Show_Data();
                           
                        }

                        var request = {
                            method: 'POST',
                            url: ENV.apiUrl + '/api/file/Imageupload?filename=' + data.em_number + random + "&location=" + "/EmployeeImages",
                            data: formdata,
                            headers: {
                                'Content-Type': undefined
                            }
                        };
                        $http(request)
                      .success(function (d) {
                          $scope.Show_Data();
                          $scope.clear();
                      }, function () {
                          //alert("Err");
                          $scope.clear();
                      });

                    }, function () {
                        $('#ErrorMessage').modal({ backdrop: "static" });
                        $scope.clear();
                    });
                    //$scope.clear();
                    $scope.prev_img = '';
                    $scope.edt.em_img = '';
                }


                $scope.getTheFiles = function ($files) {
                    $scope.filesize = true;

                    angular.forEach($files, function (value, key) {
                        formdata.append(key, value);

                        var i = 0;
                        if ($files[i].size > 800000) {
                            $scope.filesize = false;
                            $scope.edt.photoStatus = false;

                            swal({ title: "Alert", text: "File Should Not Exceed 800Kb.", imageUrl: "assets/img/notification-alert.png", });

                        }
                        else {

                        }

                    });
                };
            }


            simsController.directive('ngFiles', ['$parse', function ($parse) {

                function fn_link(scope, element, attrs) {
                    var onChange = $parse(attrs.ngFiles);
                    element.on('change', function (event) {
                        onChange(scope, { $files: event.target.files });
                    });
                };

                return {
                    link: fn_link
                }
            }])

        }])
})();
