﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.HRMS');

    simsController.controller('UpdateEmployeeProfileCont',
        ['$scope', '$compile', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$locale', '$http', 'ENV', function ($scope, $compile, $state, $rootScope, $timeout, gettextCatalog, $locale, $http, ENV) {

            var username = $rootScope.globals.currentUser.username;
            $scope.Emp_ID = username;
            //$scope.Emp_ID = 'EMP008';
            $scope.updatebtn = true;
            $scope.imgupload = true;
            $scope.addDisabled = true;
            $scope.combo_multiple = false;
            $scope.addDisabled = true;
            $scope.combo_multiple = false;
            $scope.qualiList = [];
            $scope.ExperienceList = [];
            var fortype = '';
            $scope.showFileName = false;
            $scope.edt = {};
            $scope.temp = {};
            $scope.popobj = {};
            $scope.doc = {};

            if ($http.defaults.headers.common['schoolId'] == 'clarionschool') {

                $scope.updatebtn = false;
                $scope.imgupload = false;

            }
            else {
                $scope.updatebtn = true;
                $scope.imgupload = true;

            }

            $http.get(ENV.apiUrl + "api/EmpProfile/GetEPData").then(function (res) {
                $scope.obj = res.data;
                $scope.operation = true;
                $scope.Save_btn = true;
                $scope.display = true;
            });

            /*vvvv Qualification vvvvvvvvvv*/

            $http.get(ENV.apiUrl + "api/HRMS/getCompany").then(function (resc) {
                $scope.company_data = resc.data;
            });

            $http.get(ENV.apiUrl + "api/Qualification/getQualificationName").then(function (QualificationName) {
                $scope.Qualification_Name = QualificationName.data;
            });

            $http.get(ENV.apiUrl + "api/Qualification/getQualificationType").then(function (qualtype) {
                $scope.qual_type_data = qualtype.data;
            });

            $scope.AddEnable = function () { $scope.addDisabled = false; }

            /* ^^^^ Qualification  ^^^^^ */

            $http.get(ENV.apiUrl + "api/EmpExp/getMonthName").then(function (mon) {
                $scope.month_data = mon.data;
            });

            

            $scope.getdata = function () {

                $http.get(ENV.apiUrl + "api/EmpProfile/GetEmployeeProfileData?em_login_code=" + $scope.Emp_ID).then(function (res) {
                    $scope.temp1 = res.data;
                    $scope.temp = {
                        em_login_code: $scope.temp1[0].em_login_code,
                        em_number: $scope.temp1[0].em_number,
                        em_service_status_code: $scope.temp1[0].em_service_status_code,
                        em_status_code: $scope.temp1[0].em_status_code,
                        em_apartment_number: $scope.temp1[0].em_apartment_number,
                        em_family_name: $scope.temp1[0].em_family_name,
                        em_street_number: $scope.temp1[0].em_street_number,
                        em_area_number: $scope.temp1[0].em_area_number,
                        em_phone: $scope.temp1[0].em_phone,
                        em_fax: $scope.temp1[0].em_fax,
                        em_post: $scope.temp1[0].em_post,
                        em_emergency_contact_name2: $scope.temp1[0].em_emergency_contact_name2,
                        em_emergency_contact_number2: $scope.temp1[0].em_emergency_contact_number2,
                        em_joining_ref: $scope.temp1[0].em_joining_ref,
                        em_ethnicity_code: $scope.temp1[0].em_ethnicity_code,
                        em_handicap_status: $scope.temp1[0].em_handicap_status,
                        em_stop_salary_indicator: $scope.temp1[0].em_stop_salary_indicator,
                        em_agreement: $scope.temp1[0].em_agreement,
                        em_punching_status: $scope.temp1[0].em_punching_status,
                        em_bank_cash_tag: $scope.temp1[0].em_bank_cash_tag,
                        em_citi_exp_tag: $scope.temp1[0].em_citi_exp_tag,
                        em_leave_tag: $scope.temp1[0].em_leave_tag,
                        em_passport_issue_place: $scope.temp1[0].em_passport_issue_place,
                        em_passport_issuing_authority: $scope.temp1[0].em_passport_issuing_authority,
                        em_pssport_exp_rem_date: $scope.temp1[0].em_pssport_exp_rem_date,
                        em_visa_issuing_place: $scope.temp1[0].em_visa_issuing_place,
                        em_visa_issuing_authority: $scope.temp1[0].em_visa_issuing_authority,
                        em_visa_exp_rem_date: $scope.temp1[0].em_visa_exp_rem_date,
                        em_visa_type_code: $scope.temp1[0].em_visa_type_code,
                        em_agreement_start_date: $scope.temp1[0].em_agreement_start_date,
                        em_agreement_exp_date: $scope.temp1[0].em_agreement_exp_date,
                        em_agreemet_exp_rem_date: $scope.temp1[0].em_agreemet_exp_rem_date,
                        em_punching_id: $scope.temp1[0].em_punching_id,
                        em_dependant_full: $scope.temp1[0].em_dependant_full,
                        em_dependant_half: $scope.temp1[0].em_dependant_half,
                        em_dependant_infant: $scope.temp1[0].em_dependant_infant,
                        em_Bank_Code: $scope.temp1[0].em_Bank_Code,
                        em_bank_ac_no: $scope.temp1[0].em_bank_ac_no,
                        em_iban_no: $scope.temp1[0].em_iban_no,
                        em_route_code: $scope.temp1[0].em_route_code,
                        em_bank_swift_code: $scope.temp1[0].em_bank_swift_code,
                        em_gpf_ac_no: $scope.temp1[0].em_gpf_ac_no,
                        em_pan_no: $scope.temp1[0].em_pan_no,
                        em_labour_card_no: $scope.temp1[0].em_labour_card_no,
                        em_gosi_ac_no: $scope.temp1[0].em_gosi_ac_no,
                        em_gosi_start_date: $scope.temp1[0].em_gosi_start_date,
                        em_national_id: $scope.temp1[0].em_national_id,
                        em_secret_question_code: $scope.temp1[0].em_secret_question_code,
                        em_secret_answer: $scope.temp1[0].em_secret_answer,

                        em_building_number: $scope.temp1[0].em_building_number,
                        em_Company_Code: $scope.temp1[0].em_Company_Code,
                        em_Dept_Code: $scope.temp1[0].em_Dept_Code,
                        em_Designation_Code: $scope.temp1[0].em_Designation_Code,
                        em_Dest_Code: $scope.temp1[0].em_Dest_Code,
                        em_Grade_Code: $scope.temp1[0].em_Grade_Code,
                        em_Marital_Status_Code: $scope.temp1[0].em_Marital_Status_Code,
                        em_Nation_Code: $scope.temp1[0].em_Nation_Code,
                        em_Country_Code: $scope.temp1[0].em_Country_Code,
                        em_State_Code: $scope.temp1[0].em_State_Code,
                        em_City_Code: $scope.temp1[0].em_City_Code,
                        em_Religion_Code: $scope.temp1[0].em_Religion_Code,
                        em_Salutation_Code: $scope.temp1[0].em_Salutation_Code,
                        em_Sex_Code: $scope.temp1[0].em_Sex_Code,
                        em_Staff_Type_Code: $scope.temp1[0].em_Staff_Type_Code,
                        em_blood_group_code: $scope.temp1[0].em_blood_group_code,
                        em_date_of_birth: $scope.temp1[0].em_date_of_birth,
                        em_date_of_join: $scope.temp1[0].em_date_of_join,
                        em_dept_effect_from: $scope.temp1[0].em_dept_effect_from,
                        em_email: $scope.temp1[0].em_email,
                        em_emergency_contact_name1: $scope.temp1[0].em_emergency_contact_name1,
                        em_emergency_contact_number1: $scope.temp1[0].em_emergency_contact_number1,
                        em_first_name: $scope.temp1[0].em_first_name,
                        em_grade_effect_from: $scope.temp1[0].em_grade_effect_from,
                        em_last_name: $scope.temp1[0].em_last_name,
                        em_middle_name: $scope.temp1[0].em_middle_name,
                        em_mobile: $scope.temp1[0].em_mobile,
                        em_name_ot: $scope.temp1[0].em_name_ot,
                        em_national_id_expiry_date: $scope.temp1[0].em_national_id_expiry_date,
                        em_national_id_issue_date: $scope.temp1[0].em_national_id_issue_date,
                        em_passport_exp_date: $scope.temp1[0].em_passport_exp_date,
                        em_passport_issue_date: $scope.temp1[0].em_passport_issue_date,
                        em_passport_no: $scope.temp1[0].em_passport_no,
                        em_summary_address: $scope.temp1[0].em_summary_address,
                        em_summary_address_local_language: $scope.temp1[0].em_summary_address_local_language,
                        em_visa_exp_date: $scope.temp1[0].em_visa_exp_date,
                        em_stop_salary_from: $scope.temp1[0].em_stop_salary_from,
                        em_visa_issue_date: $scope.temp1[0].em_visa_issue_date,
                        em_visa_no: $scope.temp1[0].em_visa_no,
                        em_img: $scope.temp1[0].em_img,
                        em_resident_in_qatar_since: $scope.temp1[0].em_resident_in_qatar_since,
                        em_remarks_awards: $scope.temp1[0].em_remarks_awards,
                        em_name_of_spouse: $scope.temp1[0].em_name_of_spouse,
                        em_Spouse_Designation_Code: $scope.temp1[0].em_Spouse_Designation_Code,
                        em_spouse_organization: $scope.temp1[0].em_spouse_organization,
                        em_spouse_qatar_id: $scope.temp1[0].em_spouse_qatar_id,
                        em_spouse_contact_number: $scope.temp1[0].em_spouse_contact_number,
                        pays_work_type_code: $scope.temp1[0].pays_work_type_code,
                        pays_work_type_name: $scope.temp1[0].pays_work_type_name,
                        em_labour_card_no: $scope.temp1[0].em_labour_card_no,
                        em_work_permit_issue_date: $scope.temp1[0].em_work_permit_issue_date,
                        em_work_permit_expiry_date: $scope.temp1[0].em_work_permit_expiry_date,
                        em_personalemail: $scope.temp1[0].em_personalemail
                    }

                    //  $scope.prev_img = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/' + $scope.temp1[0].em_img;                    
                    var num = Math.random();
                    var imgSrc = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/' + $scope.temp1[0].em_img + "?v=" + num;
                    $(function () {
                        $('#empimg').attr("src", imgSrc);
                    })

                    //$scope.getQualiByEmpcode($scope.Emp_ID);
                    //$scope.getExpByEmpcode($scope.Emp_ID);

                    $scope.getQualiByEmpcode = function (empcode) {

                        $http.get(ENV.apiUrl + "api/Qualification/getQualificationByEmployee?emp_code=" + $scope.Emp_ID).then(function (res1) {
                            $scope.qualiList = res1.data;
                            console.log("qualiList", $scope.qualiList);
                            $scope.combo_multiple = true;
                        });
                    }
                    $scope.getQualiByEmpcode();

                    $http.get(ENV.apiUrl + "api/EmpExp/getEmployeeExp?em_login_code=" + $scope.Emp_ID).then(function (res) {
                        $scope.ExperienceList = res.data;
                        console.log("ExperienceList", $scope.ExperienceList);
                        $scope.showExperinceList = true;
                    });

                    $scope.showEmpDocumentList($scope.Emp_ID);
                });
            }

            $scope.getdata();

            $scope.getQualiByEmpcode = function (empcode) {
                $http.get(ENV.apiUrl + "api/Qualification/getQualificationByEmployee?emp_code=" + empcode).then(function (res1) {
                    $scope.qualiList = res1.data;
                    $scope.showQualiMsg = false;
                    if ($scope.qualiList.length > 0) {
                        $scope.showQualiMsg = false;
                    }
                    else {
                        $scope.showQualiMsg = true;
                    }
                });
            }

            $scope.getExpByEmpcode = function (empcode) {
                $http.get(ENV.apiUrl + "api/EmpProfile/GetSpecificEmployeeExp?em_login_code=" + empcode).then(function (res1) {
                    $scope.expList = res1.data;
                    $scope.experience = 0;
                    $scope.showMsg = false;
                    if ($scope.expList.length > 0) {
                        //$scope.total_experience = $scope.expList[0].total_experience;

                        angular.forEach($scope.expList, function (value, key) {
                            $scope.experience = parseFloat($scope.experience) + parseFloat(value.total_experience);
                        });

                        $scope.total_experience = $scope.experience;
                        $scope.showMsg = false;
                    }
                    else {
                        $scope.showMsg = true;
                    }
                });
            }

            $scope.UpdateData = function (Myform) {

                if (Myform) {
                    var data = $scope.temp;
                    if ($scope.photo_filename === undefined) {
                    }
                    else {
                        $scope.temp.em_img = '';
                        //data.em_img ='.' + $scope.photo_filename.split("/")[1];
                        data.em_img = $scope.temp.em_login_code + '.' + $scope.photo_filename.split("/")[1];

                    }
                    data.opr = 'U';

                    $http.post(ENV.apiUrl + "api/EmpProfile/UpdateProile", data).then(function (res) {
                        $scope.CUDobj = res.data;
                        swal({ text: $scope.CUDobj.strMessage, timer: 10000, width: 360, });
                        $scope.updateQualification();
                        $scope.updateExperience();
                        //$state.reload();
                        //$state.go("main.EmpDshBrd");
                        $scope.getdata();

                    });

                    if ($scope.ImgLoaded == true) {
                        var request = {
                            method: 'POST',
                            url: ENV.apiUrl + '/api/file/upload?filename=' + $scope.temp.em_login_code + "&location=" + "/EmployeeImages",
                            data: formdata,
                            headers: {
                                'Content-Type': undefined
                            }
                        };
                        $http(request).success(function (d) {
                        });
                    }
                    $scope.searchText = false;
                }
                else {
                    swal({ text: 'Kindly fill mandatory field', timer: 5000, width: 360 });
                }
            }

            $scope.updateQualification = function () {
                var demo1 = [];
                var modulecode = [];

                if ($scope.qualiList.length > 0) {
                    for (var i = 0; i < $scope.qualiList.length; i++) {

                        //if (parseInt($scope.qualiList[i].em_qual_from_year) > parseInt(currentYear) || parseInt($scope.qualiList[i].em_qual_to_year) > parseInt(currentYear)) {
                        //    swal({ title: "Alert", text: 'Future year not allowed', timer: 5000, width: 320 });
                        //    return;
                        //}

                        modulecode = $scope.qualiList[i].emp_qual_qual_code;
                        var data = {
                            'emp_qual_em_code': $scope.temp.em_login_code,//$scope.popobj.emp_qual_em_code,
                            'emp_qual_qual_code': modulecode,

                            'emp_qual_institute': $scope.qualiList[i].emp_qual_institute,
                            'emp_qual_type_code': $scope.qualiList[i].emp_qual_type_code,
                            'em_qual_from_year': $scope.qualiList[i].em_qual_from_year,

                            'em_qual_year': $scope.qualiList[i].em_qual_to_year,
                            'emp_qual_percentage': $scope.qualiList[i].emp_qual_percentage,
                            'emp_qual_remark': $scope.qualiList[i].emp_qual_remark,
                            'em_qual_status': $scope.qualiList[i].em_qual_status,
                            'opr': 'U'
                        }
                        demo1.push(data);
                    }

                    if ($scope.qualiList.length != 0) {
                        $http.post(ENV.apiUrl + "api/Qualification/UpadateQualification?simsobj2=", demo1).then(function (msg) {
                            $scope.updateQualimsg1 = msg.data;
                            console.log($scope.updateQualimsg1);
                            $scope.qualiList = [];
                            $scope.combo_multiple = false;
                        });
                    }
                    else {
                        swal({ text: 'You not edit correct record', timer: 5000 });
                    }
                }
            }

            $scope.addCol = function (str) {

                $scope.combo_multiple = true;
                var t = document.getElementById("qual_name");
                var selectedText = t.options[t.selectedIndex].text;

                for (var i = 0; i < $scope.qualiList.length; i++) {
                    if ($scope.qualiList[i].emp_qual_qual_code == str) {
                        swal({ text: 'Qualification Already Added !!', timer: 5000 });
                        return false;
                    }
                }

                var data = {
                    'emp_qual_qual_name': selectedText,
                    'emp_qual_qual_code': str
                }
                $scope.qualiList.push(data);

                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            $scope.Removerow = function ($event, index, str) {
                str.splice(index, 1);
            }

            $scope.addExperience = function () {
                $scope.showExperinceList = true;
                //var t = document.getElementById("qual_name");
                //var selectedText = t.options[t.selectedIndex].text;

                //for (var i = 0; i < $scope.qualiList.length; i++) {
                //    if ($scope.qualiList[i].emp_qual_qual_code == str) {
                //        swal({ text: 'Qualification Already Added !!', timer: 5000 });
                //        return false;
                //    }
                //}

                var data = {
                    'enroll_number': '',
                    'prev_company_name': '',
                    'prev_job_title': '',
                    'from_year': '',
                    'from_month': '',
                    'to_year': '',
                    'to_month': '',
                    'prev_job_remark': '',
                    'prev_job_responsibilities': '',
                    'prev_job_salary': '',
                    'job_status': '',
                    'em_previous_job_line_no': ''
                }
                $scope.ExperienceList.push(data);
            };

            $scope.updateExperience = function () {
                var dataupdate = [];

                if ($scope.ExperienceList.length > 0) {

                    for (var i = 0; i < $scope.ExperienceList.length; i++) {

                        var data = {
                            'enroll_number': $scope.temp.em_login_code,
                            'co_company_code': '1',
                            'prev_company_name': $scope.ExperienceList[i].prev_company_name,
                            'prev_job_title': $scope.ExperienceList[i].prev_job_title,
                            'from_year': $scope.ExperienceList[i].from_year,
                            'from_month': $scope.ExperienceList[i].from_month,
                            'to_year': $scope.ExperienceList[i].to_year,
                            'to_month': $scope.ExperienceList[i].to_month,
                            //'prev_job_remark': $scope.ExperienceList[i].prev_job_remark,
                            'prev_job_responsibilities': $scope.ExperienceList[i].prev_job_responsibilities,
                            'prev_job_salary': $scope.ExperienceList[i].prev_job_salary,
                            'job_status': $scope.ExperienceList[i].job_status,
                            'em_previous_job_line_no': $scope.ExperienceList[i].em_previous_job_line_no,
                            'opr': 'U'
                        }
                        dataupdate.push(data);
                    }

                    $http.post(ENV.apiUrl + "api/EmpExp/CUDEmployeeExperience", dataupdate).then(function (msg) {
                        $scope.msgupdateExp = msg.data;
                        console.log("emp exp update", $scope.msgupdateExp);
                        $scope.ExperienceList = [];
                        $scope.showExperinceList = false;
                    });
                }
            }

            $scope.RemoveExperinceRow = function ($event, index, str) {
                str.splice(index, 1);
            }

            $scope.imageUrl = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeImages/';

            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                    var i = 0;
                    if ($files[key].size > 200000) {
                        $scope.filesize = false;
                        swal({ title: "Alert", text: "File Should Not Exceed 2 Mb.", width: 320, height: 280 });
                    }
                });
            };

            $scope.file_changed = function (element) {
                var photofile = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        //                        $scope.prev_img = e.target.result;
                        var imgSrc = e.target.result;
                        $(function () {
                            $('#empimg').attr("src", imgSrc);
                        })
                    });
                };
                reader.readAsDataURL(photofile);
            };

            $scope.image_click = function () {
                $scope.ImgLoaded = true;
            }

            /*----------------   Upoload Documnet Code begin------------- */

            $scope.showEmpDocumentList = function (em_login_code) {

                var obj = {
                    pays_doc_empl_id: em_login_code,
                    show: 'all'
                }
                $http.post(ENV.apiUrl + "api/EmpDocumentupload/DocumentdetailData", obj).then(function (res) {

                    $scope.DocumentdetailsData = res.data;
                    console.log("DocumentdetailsData", $scope.DocumentdetailsData);
                    $scope.showFileName = false;
                    if ($scope.DocumentdetailsData.length > 0) {
                        $scope.table = true;
                        $scope.pager = true;
                    }
                    else {
                        $scope.table = false;
                        //swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                    }

                    $scope.feetemp = res.data;
                    $scope.PAcols = [];
                    var fc = false;


                    $scope.allDt = [];
                    for (var d = 0; d < $scope.feetemp.length; d++) {
                        var dt = $scope.feetemp[d].pays_doc_empl_name;
                        if ($scope.allDt[dt] == undefined) {
                            $scope.allDt[dt] = getRecDateWise(dt)
                        }
                    }

                    $scope.finalData = [];
                    for (var item in $scope.allDt) {
                        var ob = {
                            'dt': item,
                            'arr': $scope.allDt[item]
                        };
                        $scope.finalData.push(ob);
                    }
                });
            };

            $scope.UploadDocumentModal = function (str) {
                $scope.filenameassigningData = str;
                $('#DocumenModal').modal('show');
                console.log("str ", str);
            }

            $scope.hideDocModal = function () {
                debugger;
                $scope.showFileName = true;

                $scope.filename1 = $scope.filename_doc;
                if ($scope.filename1 == undefined || $scope.filename1 == "" || $scope.filename1 == null) {
                    $scope.showFileName = false;
                }
                $scope.filenameassigningData.pays_doc_path = $scope.filename1;
                $('#DocumenModal').modal('hide');
            };


            $scope.getTheDocFiles = function ($files) {
                //FileList[0].File.name = $scope.filename + '.' + fortype;
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                });
            };

            $scope.file_changed_doc = function (element) {
                debugger;
                var photofile = element.files[0];
                //fortype = photofile.type.split("/")[1];
                //if (fortype == 'png' || fortype == 'jpeg' || fortype == 'jpg' || fortype == 'pdf') {
                //    if (photofile.size > 200000) {
                //        swal('', 'File size limit not exceed upto 200kb.')
                //    }
                //    else {

                $scope.filename_doc = photofile.name;
                $scope.edt = { employeeDocument: $scope.filename_doc }
                $.extend($scope.edt, $scope.edt)
                //$scope.photo_filename = (photofile.type);

                $scope.photo_filename_doc = (photofile.name);

                var len = 0;
                len = $scope.photo_filename_doc.split('.');
                fortype = $scope.photo_filename_doc.split('.')[len.length - 1];
                // $scope.edt1 = { sims_timetable_filename: $scope.photo_filename }
                // $.extend($scope.edt, $scope.edt1)
                $scope.photo_filename_doc = (photofile.type);

                //element.files[0].FileList.File.name = $scope.filename + '.' + fortype;
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        // $scope.prev_img = e.target.result;


                    });
                };
                reader.readAsDataURL(photofile);

                //    }
                //}
                //else {
                //    swal('', 'PDF and Image Files are allowed.');
                //}
            };

            var issueoexpirydate = false;
            $scope.getRegDate = function (info, str) {

                issueoexpirydate = false;
                $('#dateModal').modal({ backdrop: 'static', keyboard: true });
                info.ischecked = true;
                if (str == 'issue') {
                    issueoexpirydate = true;
                    $scope.doc =
                        {
                            reg_date: info.pays_doc_issue_date
                        }
                }
                else {
                    issueoexpirydate = false;
                    $scope.doc =
                       {
                           reg_date: info.pays_doc_expiry_date
                       }
                }
            }

            $scope.getDatedetails = function () {

                for (var i = 0; i < $scope.finalData.length; i++) {
                    for (var j = 0; j < $scope.finalData[i].arr.length; j++) {
                        if ($scope.finalData[i].arr[j].ischecked == true) {
                            if (issueoexpirydate == true)
                                $scope.finalData[i].arr[j].pays_doc_issue_date = $scope.doc.reg_date;
                            else
                                $scope.finalData[i].arr[j].pays_doc_expiry_date = $scope.doc.reg_date;
                            $scope.finalData[i].arr[j].ischecked = false;
                            break;
                        }
                    }
                }
                // $scope.temp.reg_date =null;
            }

            $scope.uploadDocument = function (str) {
                debugger
                var datasave = [];
                var dataupdate = [];
                var saveorupdate = false;
                $scope.showUpdateBtn = false;
                document.getElementById('file1_doc').value = '';
                $scope.filename_doc = str.pays_doc_empl_id + '_' + str.pays_doc_desc1;
                if (str.pays_doc_srno == '') {
                    saveorupdate = true;
                    $scope.showUpdateBtn = false;
                    var data = {
                        pays_doc_empl_id: str.pays_doc_empl_id,
                        pays_doc_mod_code: str.pays_doc_mod_code,
                        pays_doc_code: str.pays_doc_code,
                        pays_doc_desc: str.pays_doc_desc,
                        pays_doc_path: str.pays_doc_path,
                        pays_doc_issue_date: str.pays_doc_issue_date,
                        pays_doc_expiry_date: str.pays_doc_expiry_date,
                        pays_doc_status: true,
                    }
                    datasave.push(data);
                }
                //else {
                //    saveorupdate = false;
                //    $scope.showUpdateBtn = true;
                //    var data = {
                //        pays_doc_srno: str.pays_doc_srno,
                //        pays_doc_empl_id: str.pays_doc_empl_id,
                //        pays_doc_mod_code: str.pays_doc_mod_code,
                //        pays_doc_code: str.pays_doc_code,
                //        pays_doc_desc: str.pays_doc_desc,
                //        pays_doc_path: str.pays_doc_path,
                //        pays_doc_issue_date: str.pays_doc_issue_date,
                //        pays_doc_expiry_date: str.pays_doc_expiry_date,
                //        pays_doc_status: true,
                //    }
                //    dataupdate.push(data);

                //}

                $scope.pays_doc_empl_id = str.pays_doc_empl_id;
                $scope.pays_doc_desc1 = str.pays_doc_desc1;


                var t = $scope.photo_filename_doc.split("/")[1];
                ///+ '.' + fortype
                $scope.filename1 = $scope.filename_doc + '.' + t;
                var data = [];
                var senddata = [];

                if ($scope.photo_filename_doc == undefined || $scope.photo_filename_doc == '') {
                    swal({ title: "Alert", text: "Please Select File", width: 300, height: 200 });
                }
                else {

                    if (saveorupdate == true) {
                        for (var i = 0; i < datasave.length; i++) {
                            data = {
                                pays_doc_empl_id: datasave[i].pays_doc_empl_id,
                                pays_doc_mod_code: datasave[i].pays_doc_mod_code,
                                pays_doc_code: datasave[i].pays_doc_code,
                                pays_doc_desc: datasave[i].pays_doc_desc,
                                pays_doc_path: $scope.filename1,
                                pays_doc_issue_date: datasave[i].pays_doc_issue_date,
                                pays_doc_expiry_date: datasave[i].pays_doc_expiry_date,
                                pays_doc_status: true,
                                opr: "I"
                            }
                        }
                    }
                    //else {
                    //    for (var i = 0; i < dataupdate.length; i++) {
                    //        data = {
                    //            pays_doc_srno: dataupdate[i].pays_doc_srno,
                    //            pays_doc_empl_id: dataupdate[i].pays_doc_empl_id,
                    //            pays_doc_mod_code: dataupdate[i].pays_doc_mod_code,
                    //            pays_doc_code: dataupdate[i].pays_doc_code,
                    //            pays_doc_desc: dataupdate[i].pays_doc_desc,
                    //            pays_doc_path: $scope.filename1,
                    //            pays_doc_issue_date: dataupdate[i].pays_doc_issue_date,
                    //            pays_doc_expiry_date: dataupdate[i].pays_doc_expiry_date,
                    //            pays_doc_status: true,
                    //            opr: "U"
                    //        }
                    //    }
                    //}

                    senddata.push(data);

                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + '/api/EmpDocumentupload/upload?filename=' + $scope.filename1 + "&location=" + "EmployeeDocument",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    $http(request)
                    .success(function (d) {
                    },
                    function () {
                        //alert("Err");
                        console.log("file upload Err");
                    });


                    $http.post(ENV.apiUrl + "api/EmpDocumentupload/InsertUpdateDocumentDetail", senddata).then(function (msg) {
                        $scope.msg1_doc = msg.data;

                        $('#DocumenModal').modal('hide');
                        if ($scope.msg1_doc == true) {
                            $scope.photo_filename_doc = '';
                            swal({ text: "Document Uploaded successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                            $scope.showEmpDocumentList($scope.temp.em_login_code);
                        }

                        else if ($scope.msg1_doc == false) {
                            swal({ text: "Document Not Uploaded", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                            //$scope.show();
                        }
                        else {
                            swal("Error-" + $scope.msg1_doc);
                            console.log("Error-" + $scope.msg1_doc)
                            $scope.showEmpDocumentList($scope.temp.em_login_code);
                        }

                    },
                        function () {
                            $('#ErrorMessage').modal({ backdrop: "static" });
                        });
                }

            }

            $scope.UpdateDocument = function (str) {
                debugger
                var datasave = [];
                var dataupdate = [];
                var saveorupdate = false;
                $scope.showUpdateBtn = false;
                document.getElementById('file1_doc').value = '';
                var v = new Date();

                $scope.filename_doc = str.pays_doc_empl_id + '_' + str.pays_doc_desc1 + '_' + v.getHours() + v.getSeconds();
                if (str.pays_doc_srno == '') {
                    saveorupdate = true;
                    $scope.showUpdateBtn = false;
                    var data = {
                        pays_doc_empl_id: str.pays_doc_empl_id,
                        pays_doc_mod_code: str.pays_doc_mod_code,
                        pays_doc_code: str.pays_doc_code,
                        pays_doc_desc: str.pays_doc_desc,
                        pays_doc_path: str.pays_doc_path,
                        pays_doc_issue_date: str.pays_doc_issue_date,
                        pays_doc_expiry_date: str.pays_doc_expiry_date,
                        pays_doc_status: true,
                    }
                    datasave.push(data);
                }
                else {
                    saveorupdate = false;
                    $scope.showUpdateBtn = true;
                    var data = {
                        pays_doc_srno: str.pays_doc_srno,
                        pays_doc_empl_id: str.pays_doc_empl_id,
                        pays_doc_mod_code: str.pays_doc_mod_code,
                        pays_doc_code: str.pays_doc_code,
                        pays_doc_desc: str.pays_doc_desc,
                        pays_doc_path: str.pays_doc_path,
                        pays_doc_issue_date: str.pays_doc_issue_date,
                        pays_doc_expiry_date: str.pays_doc_expiry_date,
                        pays_doc_status: true,
                    }
                    dataupdate.push(data);

                }

                $scope.pays_doc_empl_id = str.pays_doc_empl_id;
                $scope.pays_doc_desc1 = str.pays_doc_desc1;

                var data = [];
                var senddata = [];
                if ($scope.photo_filename_doc == undefined || $scope.photo_filename_doc == '' || $scope.photo_filename_doc == null) {
                    for (var i = 0; i < dataupdate.length; i++) {
                        data = {
                            pays_doc_srno: dataupdate[i].pays_doc_srno,
                            pays_doc_empl_id: dataupdate[i].pays_doc_empl_id,
                            pays_doc_mod_code: dataupdate[i].pays_doc_mod_code,
                            pays_doc_code: dataupdate[i].pays_doc_code,
                            pays_doc_desc: dataupdate[i].pays_doc_desc,
                            pays_doc_path: dataupdate[i].pays_doc_path,
                            pays_doc_issue_date: dataupdate[i].pays_doc_issue_date,
                            pays_doc_expiry_date: dataupdate[i].pays_doc_expiry_date,
                            pays_doc_status: true,
                            opr: "U"
                        }
                    }
                    senddata.push(data);
                }
                else {

                    // var t = $scope.photo_filename.split("/")[1];
                    var t = $scope.photo_filename_doc.split("/")[1];
                    ///+ '.' + fortype
                    $scope.filename1 = $scope.filename_doc + '.' + t;
                    //$scope.filename1 = $scope.filename ;

                    for (var i = 0; i < dataupdate.length; i++) {
                        data = {
                            pays_doc_srno: dataupdate[i].pays_doc_srno,
                            pays_doc_empl_id: dataupdate[i].pays_doc_empl_id,
                            pays_doc_mod_code: dataupdate[i].pays_doc_mod_code,
                            pays_doc_code: dataupdate[i].pays_doc_code,
                            pays_doc_desc: dataupdate[i].pays_doc_desc,
                            pays_doc_path: $scope.filename1,
                            pays_doc_issue_date: dataupdate[i].pays_doc_issue_date,
                            pays_doc_expiry_date: dataupdate[i].pays_doc_expiry_date,
                            pays_doc_status: true,
                            opr: "U"
                        }
                    }
                    senddata.push(data);

                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + '/api/EmpDocumentupload/upload?filename=' + $scope.filename1 + "&location=" + "EmployeeDocument",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    $http(request)
                      .success(function (d) {
                      },
                    function () {
                        alert("Err");

                    });
                }


                $http.post(ENV.apiUrl + "api/EmpDocumentupload/InsertUpdateDocumentDetail", senddata).then(function (msg) {
                    $scope.msg1updatedoc = msg.data;
                    $('#DocumenModal').modal('hide');
                    if ($scope.msg1updatedoc == true) {
                        console.log("msg1updatedoc ", $scope.msg1updatedoc);
                        swal({ text: "Document Uploaded successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                        $scope.showEmpDocumentList($scope.temp.em_login_code);
                        $scope.photo_filename_doc = '';
                    }
                    else if ($scope.msg1updatedoc == false) {
                        console.log("msg1updatedoc ", $scope.msg1updatedoc);
                        swal({ text: "Document Not Uploaded", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                        $scope.showEmpDocumentList($scope.temp.em_login_code);
                    }
                    else {
                        //swal("Error-" + $scope.msg1updatedoc)
                        console.log("Error ", $scope.msg1updatedoc);
                        swal({ text: $scope.msg1updatedoc, imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                    }
                },
                function () {
                    $('#ErrorMessage').modal({ backdrop: "static" });
                });
            }

            $scope.deleteDoc = function (obj) {
                console.log(obj);
                var deleteObj = {
                    pays_doc_code: obj.pays_doc_code,
                    pays_doc_srno: obj.pays_doc_srno,
                    pays_doc_empl_id: obj.pays_doc_empl_id
                }
                console.log(deleteObj);

                swal({
                    title: '',
                    text: "Are you sure you want to Delete?",
                    showCloseButton: true,
                    confirmButtonText: 'Yes',
                    showCancelButton: true,
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {
                        $http.post(ENV.apiUrl + "api/EmpDocumentupload/DocumentDelete", deleteObj).then(function (msg) {
                            debugger
                            $scope.msg1 = msg.data;
                            console.log(msg);
                            if ($scope.msg1 == true) {
                                swal({ text: "Document deleted successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.showEmpDocumentList($scope.temp.em_login_code);
                                    }
                                });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Document not deleted", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                });
                            }
                            else {
                                swal({ text: $scope.msg1, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                });
                            }

                        });
                    }

                });
            }

            $scope.downloaddoc = function (str) {
                $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/EmployeeDocument/' + str;
                window.open($scope.url);
            }

            function getRecDateWise(dt) {
                var a = [];
                for (var i = 0; i < $scope.feetemp.length; i++) {
                    if ($scope.feetemp[i].pays_doc_empl_name == dt) {
                        a.push($scope.feetemp[i]);
                    }
                }
                DeleteColumns(a);
                return a;
            }

            function DeleteColumns(arr) {
                var l = arr.length;
                var temp = [];
                for (var i = 0; i < l; i++) {
                    for (var v in arr[i].PAvals) {
                        var ob = { 'mode': v, 'value': 0 };
                        temp.push(ob);
                    }
                    break;
                }
                for (var i = 0; i < l; i++) {
                    for (var t = 0; t < temp.length; t++) {
                        temp[t].value = parseFloat(temp[t].value) + parseFloat(arr[i].PAvals[temp[t].mode]);
                    }
                }
                var finalPA = {};
                for (var x = 0; x < temp.length; x++) {
                    if (temp[x].value != 0) {
                        finalPA[temp[x].mode] = temp[x].value;
                    }
                }
                for (var i = 0; i < l; i++) {
                    var n = {};
                    var he = [];
                    for (var x in finalPA) {
                        n[x] = arr[i].PAvals[x];
                        he.push(x);
                    }
                    arr[i].PAvals = n;
                    arr[i].PAcols = he;
                }
            }

            /*----------------   Upoload Document Code end ------------- */

            simsController.directive('ngFiles', ['$parse', function ($parse) {

                function fn_link(scope, element, attrs) {
                    var onChange = $parse(attrs.ngFiles);
                    element.on('change', function (event) {
                        onChange(scope, { $files: event.target.files });
                    });
                };

                return {
                    link: fn_link
                }
            }])

            simsController.filter('randomSrc', function () {
                return function (input) {
                    if (input)
                        return input + '?r=' + Math.random();
                }
            })

        }]);
})();

