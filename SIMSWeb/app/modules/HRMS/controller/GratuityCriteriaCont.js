﻿(function () {
    'use strict';
    var opr = '';
    var deletecode = [];
    var deletecalcualationcode = [];
    var main;
    var main1;
    var data1 = [];
    var simsController = angular.module('sims.module.HRMS');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('GratuityCriteriaCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.grid = true;
            $scope.edit_data = false;
            $scope.GratCri_data = [];
            $scope.edit_code = false;

            $scope.propertyName = null;
            $scope.reverse = false;

            var company;

            $http.get(ENV.apiUrl + "api/OverTimeRate/getformuladesc").then(function (getformuladesc_data) {
                $scope.formuladesc = getformuladesc_data.data;
            });

            // ---this Api for get session value
            $http.get(ENV.apiUrl + "api/PDCSubmission/GetComp_Name?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.cmbCompany = res.data;
                if (res.data.length == 1) {
                    $scope.companyName = res.data[0].comp_name;

                    $scope.finnComp = res.data[0].comp_code;

                    $http.get(ENV.apiUrl + "api/PDCSubmission/GetFinancial_year?comp_code=" + $scope.finnComp).then(function (res) {

                        if (res.data.length > 0) {

                            console.log('Finn');

                            var data = {
                                year: res.data[0].financial_year,
                                company: $scope.finnComp,
                                companyame: $scope.companyName
                            }
                            window.localStorage["Finn_comp"] = JSON.stringify(data)
                            //$scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

                            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

                            $http.get(ENV.apiUrl + "api/GratuityCriteria/GetAll_Account_Names?comp_cd=" + $scope.finnDetail.company).then(function (res) {
                                $scope.accNo_data = res.data;
                            });
                            // $state.reload();
                        }
                    });


                }
                else if (res.data.length > 1) {
                    //$scope.financeWindow();
                }

            });

            // $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            //console.log($scope.finnDetail)

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };


            $scope.countData = [
                  { val: 10, data: 10 },
                  { val: 20, data: 20 },
                  { val: 'All', data: 'All' },

            ]

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $http.get(ENV.apiUrl + "api/GratuityCriteria/Get_paysGratuityCriteria").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.GratCri_data = res.data;
                console.log("GratCri_data",$scope.GratCri_data);
                if ($scope.GratCri_data.length > 0) {
                    $scope.pager = true;
                    if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.GratCri_data.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.GratCri_data.length, data: 'All' })
                    }

                    $scope.totalItems = $scope.GratCri_data.length;
                    $scope.todos = $scope.GratCri_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                }
                else {
                   // swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                    $scope.filteredTodos = [];
                }

            });


            $scope.getgrid = function () {
                $scope.currentPage = 1;

                $http.get(ENV.apiUrl + "api/GratuityCriteria/Get_paysGratuityCriteria").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.GratCri_data = res.data;
                    if ($scope.GratCri_data.length > 0) {
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.GratCri_data.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.GratCri_data.length, data: 'All' })
                        }

                        $scope.totalItems = $scope.GratCri_data.length;
                        $scope.todos = $scope.GratCri_data;
                        $scope.makeTodos();
                        $scope.grid = true;
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                        $scope.filteredTodos = [];
                    }

                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }

                $scope.row1 = '';

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.checkLimit = function () {
                console.log($scope.GratCri_data);
                debugger
                //$scope.edt.grt_pay_grade;
                //for (var i = 0; i < $scope.GratCri_data.length; i++) {
                //    if ($scope.GratCri_data[i].grt_pay_grade == $scope.edt.grt_pay_grade && $scope.GratCri_data[i].grt_criteria_order == '1') {
                //        if ($scope.edt.grt_min_days <= $scope.GratCri_data[i].grt_criteria_max_days) {
                //            swal({ title: "Alert", text: "Criteria min days should be greater than max days of previous criteria", width: 400, height: 200 });
                //            $scope.edt.grt_min_days = "";
                //        }                        
                //        //grt_criteria_max_days
                //        //grt_max_days
                //    }
                //}
            }

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.GratCri_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $timeout(function () {
                $("#fixtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            setTimeout(function () {
                $("#cmb_acc_code,#cmb_bank_code,#cmb_cash_code,#cmb_airfare_code,#cmb_prov_acc_code").select2();
            }, 100);


            $http.get(ENV.apiUrl + "api/GratuityCriteria/Get_All_pays_grade").then(function (res) {
                $scope.paysGrade_data = res.data;
            });

         

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.New = function () {
                $scope.min_day_text = true;
                $scope.min_day_yrs = false;
                $scope.min_day_month = false;
                $scope.min_day_day = false;

                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.editmode = false;
                $scope.newmode = true;
                //  $scope.edt = {};
                //  $scope.edt['grt_status'] = true;
                $scope.edt = [];
                $scope.edt = {
                    grt_status:true,
                    grt_criteria_code: "",
                    grt_criteria_name: "",
                    grt_year: "",
                    grt_month: "",
                    grt_days: "",
                    grt_prov_acct_code: "",
                    grt_working_days: "",
                    grt_criteria_min_days: "",
                    grt_criteria_max_days: ""
                }
                $("#cmb_acc_code").select2("val", "");
                $("#cmb_bank_code").select2("val", "");
                $("#cmb_cash_code").select2("val", "");
                $("#cmb_airfare_code").select2("val", "");
                $("#cmb_prov_acc_code").select2("val", "");
            }

            $scope.edit = function (str) {
                $scope.min_day_text = false;
                $scope.min_day_yrs = true;
                $scope.min_day_month = true;
                $scope.min_day_day = true;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.editmode = true;
                $scope.newmode = false;
                debugger;
                $scope.edt =
                    {
                        grt_criteria_code: str.grt_criteria_code,
                        grt_criteria_name: str.grt_criteria_name,
                        grt_criteria_min_days: str.grt_criteria_min_days,
                        grt_year: str.year,
                        grt_month: str.month,
                        grt_days: str.days,
                        grt_pay_grade: str.grt_pay_grade,
                        grt_acct_code: str.grt_acct_code,
                        grt_acct_name: str.grt_acct_name,



                        grt_bank_acct_code: str.grt_bank_acct_code,
                        grt_bank_acct_name: str.grt_bank_acct_name,

                        grt_cash_acct_code: str.grt_cash_acct_code,
                        grt_cash_acct_name: str.grt_cash_acct_name,

                        grt_airfare_acct_code: str.grt_airfare_acct_code,
                        grt_airfare_acct_name: str.grt_airfare_acct_name,

                        grt_prov_acct_code: str.grt_prov_acct_code,
                        grt_prov_acct_name: str.grt_prov_acct_name,
                        grt_status: str.grt_status,
                        grt_formula_code: str.grt_formula_code,
                        grt_working_days: str.grt_working_days,
                        grt_min_days: str.grt_criteria_min_days,
                        grt_max_days: str.grt_criteria_max_days
                    }

                if(str.grt_criteria_max_days == "0") {
                    $scope.edt.grt_max_days = "";
                }

                $("#cmb_acc_code").select2("val", str.grt_acct_code);
                $("#cmb_prov_acc_code").select2("val", str.grt_prov_acct_code);
                $("#cmb_bank_code").select2("val", str.grt_bank_acct_code);
                $("#cmb_cash_code").select2("val", str.grt_cash_acct_code);
                $("#cmb_airfare_code").select2("val", str.grt_airfare_acct_code);

            }

            $scope.checkExists = function () {
                for (var i = 0; i < $scope.GratCri_data.length; i++) {
                    if ($scope.GratCri_data[i].grt_criteria_code == $scope.edt.grt_criteria_code) {
                        swal({ title: "Alert", text: "Criteria Code already exists", width: 300, height: 200 });
                        $scope.edt.grt_criteria_code = '';
                    }                       
                }                
            }

            $scope.Save = function (myForm) {
                debugger
                if (myForm) {
                    data1 = [];
                    data = [];
                    if ($scope.edt.grt_year != undefined) {
                        var year = $scope.edt.grt_year * 365;
                    }
                    else {
                        var year = 0;
                    }
                    if ($scope.edt.grt_month != undefined) {
                        var month = $scope.edt.grt_month * 30;
                    }
                    else {
                        var month = 0;
                    }
                    if ($scope.edt.grt_days != undefined) {
                        if ($scope.edt.grt_days > 0) {
                            var day = $scope.edt.grt_days * 1;
                        }
                        else {
                            var day = $scope.edt.grt_days * 0;
                        }
                    }
                    else {
                        var day = 0;
                    }

                    $scope.min_days = (year + month + day);

                    var data = ({
                        grt_criteria_code: $scope.edt.grt_criteria_code,
                        grt_criteria_name: $scope.edt.grt_criteria_name,
                        grt_criteria_min_days: $scope.edt.grt_min_days,
                        grt_pay_grade: $scope.edt.grt_pay_grade,
                        grt_acct_code: $scope.edt.grt_acct_code,
                        grt_bank_acct_code: $scope.edt.grt_bank_acct_code,
                        grt_cash_acct_code: $scope.edt.grt_cash_acct_code,
                        grt_airfare_acct_code: $scope.edt.grt_airfare_acct_code,
                        grt_prov_acct_code: $scope.edt.grt_prov_acct_code,
                        grt_status: $scope.edt.grt_status,
                        grt_formula_code: $scope.edt.grt_formula_code,
                        grt_working_days: $scope.edt.grt_working_days,
                        grt_criteria_max_days: $scope.edt.grt_max_days,
                        opr: 'I'
                    });

                    $scope.samerecord = false;
                    debugger;
                    //if ($scope.edt.grt_acct_code != undefined && $scope.edt.grt_bank_acct_code != undefined || $scope.edt.grt_acct_code != undefined && $scope.edt.grt_cash_acct_code != undefined || $scope.edt.grt_acct_code != undefined && $scope.edt.grt_prov_acct_code != "" || $scope.edt.grt_bank_acct_code != undefined && $scope.edt.grt_cash_acct_code != undefined) {
                    //    if (data.grt_acct_code == data.grt_bank_acct_code ||
                    //            data.grt_acct_code == data.grt_cash_acct_code ||
                    //            data.grt_acct_code == data.grt_prov_acct_code ||
                    //            data.grt_bank_acct_code == data.grt_cash_acct_code) {
                    //        swal({ title: "Alert", text: "Account Code, Bank Code, Cash Code or Provision Account Code should not be same", width: 300, height: 200 });
                    //    }
                    //}

                    //if (data.grt_acct_code == data.grt_bank_acct_code || data.grt_acct_code == data.grt_cash_acct_code ||
                    //    data.grt_acct_code == data.grt_prov_acct_code || data.grt_bank_acct_code == data.grt_cash_acct_code) {
                    //    swal({ title: "Alert", text: "Account Code, Bank Code, Cash Code or Provision Account Code should not be same", width: 300, height: 200 });
                    //    return
                    //}
                    //else {
                        data1.push(data);
                        console.log("data1", data1);

                        $http.post(ENV.apiUrl + "api/GratuityCriteria/CUDInsertpaysGratuityCriteria", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({  text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                            $scope.getgrid();
                        });
                    //}
                }
            }

            $scope.Update = function (myForm) {
                debugger
                if (myForm) {
                    data1 = [];
                    data = [];
                    if ($scope.edt.grt_year != undefined) {
                        var year = $scope.edt.grt_year * 365;
                    }
                    else {
                        var year = 0;
                    }
                    if ($scope.edt.grt_month != undefined) {
                        var month = $scope.edt.grt_month * 30;
                    }
                    else {
                        var month = 0;
                    }
                    if ($scope.edt.grt_days != undefined) {
                        if ($scope.edt.grt_days > 0) {
                            var day = $scope.edt.grt_days * 1;
                        }
                        else {
                            var day = $scope.edt.grt_days * 0;
                        }
                    }
                    else {
                        var day = 0;
                    }

                    $scope.min_days = (year + month + day);

                    var data = ({
                        grt_criteria_code: $scope.edt.grt_criteria_code,
                        grt_criteria_name: $scope.edt.grt_criteria_name,
                        //grt_criteria_min_days: $scope.min_days,
                        grt_criteria_min_days: $scope.edt.grt_min_days,
                        grt_pay_grade: $scope.edt.grt_pay_grade,
                        grt_acct_code: $scope.edt.grt_acct_code,
                        grt_bank_acct_code: $scope.edt.grt_bank_acct_code,
                        grt_cash_acct_code: $scope.edt.grt_cash_acct_code,
                        grt_airfare_acct_code: $scope.edt.grt_airfare_acct_code,
                        grt_prov_acct_code: $scope.edt.grt_prov_acct_code,
                        grt_status: $scope.edt.grt_status,
                        grt_formula_code: $scope.edt.grt_formula_code,
                        grt_working_days: $scope.edt.grt_working_days,
                        grt_criteria_max_days: $scope.edt.grt_max_days,
                        opr: 'U'
                    });

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/GratuityCriteria/CUDInsertpaysGratuityCriteria", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $scope.operation = false;
                        $scope.getgrid();

                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.edt = {

                    grt_criteria_code: "",
                    grt_criteria_name: "",
                    grt_year: "",
                    grt_month: "",
                    grt_days: "",
                    grt_prov_acct_code: "",
                    grt_working_days: "",
                    grt_criteria_min_days: "",
                    grt_criteria_max_days: "",
                }
                
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Delete = function () {
                var deletecode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].grt_criteria_code + i);

                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'grt_criteria_code': $scope.filteredTodos[i].grt_criteria_code,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/GratuityCriteria/CUDDeleteGratuityCrite", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                //if ($scope.msg1 != '' || $scope.msg1 != null) {
                                //    swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 });
                                //}
                                //else {
                                //    swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 });
                                //}
                                debugger
                                if ($scope.msg1 != '' || $scope.msg1 != null) {
                                    swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].grt_criteria_code + i);

                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }

            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].grt_criteria_code + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].grt_criteria_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.GratCri_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.GratCri_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.grt_criteria_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.grt_criteria_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();

                $scope.chk = {}
                $scope.chk['check_all'] = false;
                $scope.row1 = '';
            }

            $scope.check_acccode = function () {

                debugger;
                if ($scope.edt.grt_acct_code != undefined && $scope.edt.grt_bank_acct_code != undefined || $scope.edt.grt_acct_code != undefined && $scope.edt.grt_cash_acct_code != undefined) {
                    if ($scope.edt.grt_acct_code == $scope.edt.grt_bank_acct_code) {

                        swal({ title: "Alert", text: "Account Code and Bank Code should not be same", width: 300, height: 200 });

                    }
                    if ($scope.edt.grt_acct_code == $scope.edt.grt_cash_acct_code) {

                        swal({ title: "Alert", text: "Account Code and Cash Code should not be same", width: 300, height: 200 });

                    }
                }
            }

            $scope.check_bankcode = function () {
                debugger
                if ($scope.edt.grt_acct_code != undefined && $scope.edt.grt_bank_acct_code != undefined || $scope.edt.grt_acct_code != undefined && $scope.edt.grt_cash_acct_code != undefined) {
                    if ($scope.edt.grt_bank_acct_code == $scope.edt.grt_cash_acct_code) {

                        swal({ title: "Alert", text: "Bank Code and Cash Code should not be same", width: 300, height: 200 });

                    }
                    if ($scope.edt.grt_acct_code == $scope.edt.grt_bank_acct_code) {

                        swal({ title: "Alert", text: "Bank Code and Account Code should not be same", width: 300, height: 200 });

                    }
                }
            }

            $scope.check_cashcode = function () {
                if ($scope.edt.grt_acct_code != undefined && $scope.edt.grt_bank_acct_code != undefined || $scope.edt.grt_acct_code != undefined && $scope.edt.grt_cash_acct_code != undefined) {
                    if ($scope.edt.grt_bank_acct_code == $scope.edt.grt_cash_acct_code) {

                        swal({ title: "Alert", text: "Cash Code and Bank Code should not be same", width: 300, height: 200 });

                    }
                    if ($scope.edt.grt_acct_code == $scope.edt.grt_cash_acct_code) {

                        swal({ title: "Alert", text: "Account Code and Cash Code should not be same", width: 300, height: 200 });

                    }
                }
            }

            //------------------------------- Graduity_Criteria_Calculation--------------------------------------------//

            $scope.OpengraCalculation = function (gratCri) {
              
                $('#MyModalGrt').modal('show');
                $scope.pagesize1 = '5';
                $scope.temp = {
                    grt_applicable_days: "",
                    grt_working_days: "",
                    grt_status: true,
                }
                $scope.myFormmodal.$setPristine();
                $scope.myFormmodal.$setUntouched();
                $scope.Modaltable = true;
                $scope.modaloperation = true;
               //$scope.savebtn1 = true;
                $scope.showForm = false;

                $scope.grt_criteria_code = gratCri.grt_criteria_code;

                $scope.size1 = function (str1) {
                    console.log(str1);
                    $scope.pagesize1 = str1;
                    $scope.currentPage1 = 1;
                    $scope.numPerPage1 = str1; console.log("numPerPage1=" + $scope.numPerPage1); $scope.makeTodos1();
                }
                $scope.index1 = function (str1) {
                    $scope.pageindex1 = str1;
                    $scope.currentPage1 = str1; console.log("currentPage1=" + $scope.currentPage1); $scope.makeTodos1();

                    $scope.chkmd = {}
                    $scope.chkmd['check_all_modal'] = false;
                }

                $scope.filteredTodos1 = [], $scope.currentPage1 = 1, $scope.numPerPage1 = 5, $scope.maxSize1 = 5;

                $scope.makeTodos1 = function () {
                    var rem1 = parseInt($scope.totalItems1 % $scope.numPerPage1);
                    if (rem1 == '0') {
                        $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1);
                    }
                    else {
                        $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1) + 1;
                    }
                    var begin1 = (($scope.currentPage1 - 1) * $scope.numPerPage1);
                    var end1 = parseInt(begin1) + parseInt($scope.numPerPage1);

                    console.log("begin1=" + begin1); console.log("end1=" + end1);

                    $scope.filteredTodos1 = $scope.todos1.slice(begin1, end1);
                };

                $scope.getGratuityCalculation = function () {
                    $http.get(ENV.apiUrl + "api/GratuityCriteria/getpaysGratuityCalculation?criteria_code=" + $scope.grt_criteria_code).then(function (Get_paysGratuityCalculation_data) {                        
                        if (Get_paysGratuityCalculation_data.data.length > 0) {
                            $scope.paysGratuityCalculation = Get_paysGratuityCalculation_data.data;
                            $scope.totalItems1 = $scope.paysGratuityCalculation.length;
                            $scope.todos1 = $scope.paysGratuityCalculation;
                            $scope.makeTodos1();
                            $scope.savebtn1 = false;
                            $scope.updatebtn1 = true;                            
                            $scope.showForm = false;
                            $scope.Modaltable = true;
                        }
                        else {
                            $scope.showForm = true;
                            $scope.Modaltable = false;
                            $scope.savebtn1 = true;
                            $scope.updatebtn1 = false;
                            //$scope.modaloperation = true;
                            //$scope.Modaltable = false;
                        }                        
                    });
                }
                $scope.getGratuityCalculation();

                $scope.New1 = function () {
                    $scope.temp = {
                        grt_applicable_days: "",
                        grt_working_days: "",
                        grt_status: "",
                    }
                    $scope.savebtn1 = true;
                    $scope.updatebtn1 = false;
                    $scope.modaloperation = true;
                    $scope.Modaltable = false;
                    $scope.myFormmodal.$setPristine();
                    $scope.myFormmodal.$setUntouched();
                }

                $scope.up1 = function (str1) {
                    $scope.modaloperation = true;
                    $scope.Modaltable = false;
                    $scope.showForm = true;

                    $scope.savebtn1 = false;
                    $scope.updatebtn1 = true;
                    $scope.readonly = true;

                    $scope.temp = {
                        grt_applicable_days: str1.grt_applicable_days,
                        grt_working_days: str1.grt_working_days,
                        grt_status: str1.grt_status
                    };


                }

                $scope.cancel1 = function () {
                    $('#MyModalGrt').modal('hide');
                    $scope.modaloperation = false;
                    $scope.Modaltable = true;
                    $scope.temp = {
                        grt_applicable_days: "",
                        grt_working_days: "",
                        grt_status: "",
                    }

                    $scope.myFormmodal.$setPristine();
                    $scope.myFormmodal.$setUntouched();
                }

                $scope.Save1 = function (myFormmodal) {
                    if (myFormmodal) {
                        data1 = [];
                        var data = {

                            grt_criteria_code: $scope.grt_criteria_code,
                            grt_applicable_days: $scope.temp.grt_applicable_days,
                            grt_working_days: $scope.temp.grt_working_days,
                            grt_status: $scope.temp.grt_status,
                            opr: 'I',
                        };
                        $scope.exist = false;
                        debugger
                        //for (var i = 0; i < $scope.paysGratuityCalculation.length; i++) {
                        //    if ($scope.paysGratuityCalculation.length > 0) {
                        //        $scope.exist = true;
                        //    }
                        //}
                        //if ($scope.exist) {
                        //    swal({ title: 'Alert', text: "You can insert only one record against each Gratuity Criteria Code", width: 300, height: 200 })
                        //}
                        //else {
                            debugger
                            data1.push(data);
                            $http.post(ENV.apiUrl + "api/GratuityCriteria/CUDInsertGratuityCalculation", data1).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 })
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 })
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }

                                $scope.getGratuityCalculation();
                            })
                       // }
                        
                    }
                }

                //$scope.Save1 = function (myFormmodal) {
                //    if (myFormmodal) {
                //        data1 = [];
                //        var data = {

                //            grt_criteria_code: $scope.grt_criteria_code,
                //            grt_applicable_days: $scope.temp.grt_applicable_days,
                //            grt_working_days: $scope.temp.grt_working_days,
                //            grt_status:$scope.temp.grt_status,
                //            opr: 'I',
                //        };
                //        $scope.exist = false;

                //        for (var i = 0; i < $scope.paysGratuityCalculation.length; i++) {
                //            if ($scope.paysGratuityCalculation[i].grt_criteria_code == data.grt_applicable_days &&
                //                 $scope.paysGratuityCalculation[i].grt_applicable_days == data.grt_applicable_days &&
                //                $scope.paysGratuityCalculation[i].grt_working_days == data.grt_working_days)
                //            {
                //                $scope.exist = true;
                //            }
                //        }
                //        if ($scope.exist) {
                //            swal({ title: 'Alert', text: "Record Already Exits", width: 300, height: 200 })
                //        }
                //        else {
                //            debugger
                //            data1.push(data);
                //            $http.post(ENV.apiUrl + "api/GratuityCriteria/CUDInsertGratuityCalculation", data1).then(function (msg) {
                //                $scope.msg1 = msg.data;
                //                if ($scope.msg1 == true) {
                //                    swal({ title: 'Alert', text: "Record Inserted Successfully", width: 300, height: 200 })
                //                }
                //                else {
                //                    swal({ title: 'Alert', text: "Record Not Inserted", width: 300, height: 200 })
                //                }
                //                $http.get(ENV.apiUrl + "api/GratuityCriteria/getpaysGratuityCalculation?criteria_code=" + $scope.grt_criteria_code).then(function (Get_paysGratuityCalculation_data) {
                //                    $scope.paysGratuityCalculation = Get_paysGratuityCalculation_data.data;
                //                    $scope.totalItems1 = $scope.paysGratuityCalculation.length;
                //                    $scope.todos1 = $scope.paysGratuityCalculation;
                //                    $scope.makeTodos1();
                //                });
                //            })
                //        }
                //        $scope.modaloperation = false;
                //        $scope.Modaltable = true;
                //    }
                //}

                $scope.Update1 = function (myFormmodal) {
                    if (myFormmodal) {
                        data1 = [];

                        var data = {
                            grt_criteria_code: $scope.grt_criteria_code,
                            grt_applicable_days: $scope.temp.grt_applicable_days,
                            grt_working_days: $scope.temp.grt_working_days,
                            grt_status: $scope.temp.grt_status,
                            opr: 'U',
                        };
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/GratuityCriteria/CUDInsertGratuityCalculation", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 })
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 })
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }                            
                            $scope.getGratuityCalculation();                            
                        })                        
                    }
                }

                $scope.CheckAllCheckedModal = function () {
                    debugger
                    var main1 = document.getElementById('mainchk1');
                    for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                        var v = document.getElementById($scope.filteredTodos1[i].grt_criteria_code + $scope.filteredTodos1[i].grt_applicable_days + i);
                        if (main1.checked == true) {
                            v.checked = true;
                            //$('tr').addClass("row_selected");
                        }

                        else {
                            v.checked = false;
                            //  $('tr').removeClass("row_selected");
                        }
                    }

                }

                $scope.deleterecord1 = function () {

                    deletecalcualationcode = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                        var v = document.getElementById($scope.filteredTodos1[i].grt_criteria_code + $scope.filteredTodos1[i].grt_applicable_days + i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deleteLedgerAccountCntrl = {
                                grt_criteria_code: $scope.filteredTodos1[i].grt_criteria_code,
                                //grt_applicable_days: $scope.filteredTodos[i].grt_applicable_days,
                                //grt_working_days: $scope.filteredTodos[i].grt_working_days,
                                opr: 'D',
                            };
                            deletecalcualationcode.push(deleteLedgerAccountCntrl);
                        }
                    }

                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                $http.post(ENV.apiUrl + "api/GratuityCriteria/CUDInsertGratuityCalculation", deletecalcualationcode).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {

                                                $('#MyModalGrt').modal('hide');
                                                $scope.modaloperation = false;
                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $('#MyModalGrt').modal('hide');
                                                $scope.modaloperation = false;
                                            }
                                        });
                                    }
                                    else
                                    {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });
                            }
                            else {
                                for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                                    var v = document.getElementById($scope.filteredTodos1[i].grt_criteria_code + $scope.filteredTodos1[i].grt_applicable_days + i);
                                    if (v.checked == true) {
                                        v.checked = false;

                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    $scope.currentPage1 = true;                   
                }
            }

            //------------------------------- Graduity_Criteria_Calculation_END----------------------------------------//
        }])
})();
