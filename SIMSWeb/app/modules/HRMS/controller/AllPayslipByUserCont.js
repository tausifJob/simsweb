﻿(function () {
    'use strict';
    var teacher_code = [];
    var simsController = angular.module('sims.module.Fee');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AllPayslipByUserCont',
    ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
        $scope.display = true;
        $scope.busyindicator = false;
        $scope.show_ins_table_loading = false;
        var user = $rootScope.globals.currentUser.username;

        $http.get(ENV.apiUrl + "api/AllPayslipByUser/getfinanceyear").then(function (res1) {
            $scope.paysheet_year_data = res1.data;
            $scope.paysheet_year = $scope.paysheet_year_data[0].paysheet_year;
            setTimeout(function () {
                $('#year_box').change(function () {
                }).multipleSelect({
                    width: '100%'
                });
            }, 1000);
        });
        $(function () {
            $('#year_box').multipleSelect({ width: '100%' });
        });

        $scope.getpayslipList = function (paysheet_year) {
            $http.get(ENV.apiUrl + "api/AllPayslipByUser/getpayslipList?paysheet_year=" + paysheet_year + "&pe_number=" + user).then(function (res1) {
                $scope.payslip_data = res1.data;
                console.log($scope.payslip_data);
            });
        }


        $scope.colname = null;
        $scope.reverse = false;

        $scope.sort = function (col) {
            $scope.colname = col;
            $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
        }

        $scope.viewReport = function (str) {
            $http.get(ENV.apiUrl + "api/AllPayslipByUser/getpath").then(function (res) {
                if (res.data.length>0)
                var rname = res.data[0].pays_appl_form_field_value1;

                var data = {

                    location: rname,
                    parameter: {
                        paysheet_month: str.pe_year_month,
                        comp_details: '1',
                        search: str.pe_number
                    },
                    state: 'main.SALUSR',
                    ready: function () {
                        this.refreshReport();
                    },
                    show_pdf: 'y'
                }

                console.log(data);
                window.localStorage["ReportDetails"] = JSON.stringify(data);
                $state.go('main.ReportCardParameter')

            });
        }
    }]);
})();