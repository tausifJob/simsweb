﻿
(function () {
    'use strict';
    var del = [], feedetails = [];

    var main;
    var date1, date3, date4, grade = "", section = "", desgination = "", departt = "";

    var simsController = angular.module('sims.module.Collaboration');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('EmailSmsToEmployeeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.display = true;
            $scope.grid = true;
            $scope.btn_search = true;
            $scope.filesize = true;
            $scope.EmailId_result = [];
            $scope.MobileNo_result = [];
            $scope.parent_result = [];
            $scope.getfather_indi = [];
            $scope.getmother_indi = [];
            $scope.getguardian_indi = [];
            $scope.getEmp_indi = [];
            //$scope.SelectedUserLst = [];
            $scope.div_recipient = true;
            $scope.show_grade = false;
            $scope.show_Bus = false;
            $scope.show_designation = false;
            $scope.obj = [];
            $scope.BUSY = false;
            $scope.gradedisable = false;
            $scope.itemsPerPage = '5';
            $scope.currentPage = 0;
            var date = new Date();

            //$scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss');
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-mm-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss');

            $timeout(function () {
                $("#fixTableEmp,#fixedSmstbl,#fixedEmailtbl,#fixParenttbl").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.username = $rootScope.globals.currentUser.username;
            // console.log($scope.username);

            $rootScope.visible_stud = false;
            $rootScope.visible_parent = false;
            $rootScope.visible_search_parent = false;
            $rootScope.visible_teacher = false;
            $rootScope.visible_User = false;
            $rootScope.visible_Employee = false;
            $rootScope.chkMulti = true;

            //HTML5 editor
            $('#text-editor').wysihtml5();
            $scope.temp_show_grad_bus = false;
            $scope.size = function (pagesize) {
                $scope.itemsPerPage = pagesize;

            }

            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;


            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.parent_result.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;
            };

            $http.get(ENV.apiUrl + "api/common/EmailSms/GetcheckEmailProfile").then(function (res) {
                $scope.getProfile = res.data;
                // console.log($scope.getProfile);
            });

            $(function () {
                $('#cmb_grade').multipleSelect({
                    width: '100%',
                    placeholder: "Select Grade"
                });

                $('#cmb_section').multipleSelect({
                    width: '100%',
                    placeholder: "Select Section"
                });

                $('#cmb_dept').multipleSelect({
                    width: '100%',
                    placeholder: "Select Department"
                });

                $('#cmb_designation').multipleSelect({
                    width: '100%',
                    placeholder: "Select Designation"
                });
                $('#cmb_buscode').multipleSelect({
                    width: '100%',
                    placeholder: "Select Bus Code"
                });
                $('#cmb_routeCode_name').multipleSelect({
                    width: '100%',
                    placeholder: "Select Route Direction"
                });
            });

            $scope.edt = {
                sims_Email: 'true',
                existing_rec: '0',
                sims_father_indi: false,
                sims_mother_indi: false,
                sims_guardian_indi: false,
                sims_father: false,
                sims_mother: false,
                sims_guardian: false,
                sims_active: false,
                sims_emergency: false,
                sims_sms_char: '0',
                sims_sms_length: '0'
            }

            $scope.get_msgtypechecked = function (msg_type) {
                if (msg_type == "true") {
                    $scope.name_unique_email = 'Unique parent id for Email';
                    $scope.chk_emerg = false;
                    $scope.chk_sms = false;
                    $scope.chk_email = true;
                    $scope.chk_emerg = false;
                    // $scope.obj2 = [];
                    $scope.chk_rec = true;
                    $scope.chk_rec1 = true;
                    $scope.edt.existing_rec = 0;
                    // $scope.edt.sims_parent = false;
                    $scope.div_recipient = true;
                    $scope.getcleared();
                    // $scope.cleardata();
                }
                else if (msg_type == "false") {
                    $scope.name_unique_email = 'Unique parent id for SMS';
                    $scope.edt.sims_arabic = 'false',
                    $scope.chk_emerg = true;
                    $scope.chk_sms = true;
                    $scope.chk_email = false;
                    //  $scope.edt.sims_parent = false;
                    $scope.div_recipient = true;
                    // $scope.obj2 = [];
                    $scope.chk_rec = true;
                    $scope.chk_rec1 = true;
                    $scope.edt.existing_rec = 0;
                    $scope.chk_emerg = true;
                    $scope.edt.sims_sms_msg = "Enter your message below ( max 1000 chars.1 sms is 160 characters.)";
                    $scope.edt.sims_sms_subjectblock = "These special characters ' ][}{|~^ ' will be treated as two character";
                    //$scope.cleardata();
                    $scope.getcleared();
                }
            }
            //$scope.getroutebybuscode = function (vehicleCode) {
            //   
            //    var currentTime = new Date();
            //    var year = currentTime.getFullYear();
            //    $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getRouteNameByBusCode?AcaYear=" + year + "&Vehicle_Code=" + vehicleCode).then(function (getRouteNameByBusCode_Data) {
            //        $scope.AllRouteName = getRouteNameByBusCode_Data.data;
            //        setTimeout(function () {
            //            $('#cmb_routeCode_name').change(function () {
            //                //  console.log($(this).val());
            //                grade = $(this).val();
            //            }).multipleSelect({
            //                width: '100%',
            //            });
            //        }, 1000);
            //    });
            //}

            $scope.dataGettingbyvehicleCode = function (year, vehicleCode) {
                if (vehicleCode.length != 0) {
                    $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getRouteNameByBusCode?AcaYear=" + year + "&Vehicle_Code=" + vehicleCode).then(function (getRouteNameByBusCode_Data) {
                        $scope.AllRouteName = getRouteNameByBusCode_Data.data;
                        setTimeout(function () {
                            $('#cmb_routeCode_name').change(function () {
                                grade = $(this).val();
                            }).multipleSelect({
                                width: '100%',
                            });
                        }, 1000);
                    });
                }
            }
            $scope.show_grad_bus = function () {
                $scope.temp_show_grad_bus = true;
                $scope.show_designation = false;
                $scope.searchMobileNo_result = [];
                $scope.searchEmailId_result = [];
                $scope.edt.existing_rec = '0';
                $scope.edt.sims_grade = false;
                $scope.chk_rec = false;
                $scope.chk_rec1 = false;
                $scope.cleardata();

            }
            $http.get(ENV.apiUrl + "api/common/Attendance/getCuriculum").then(function (cur_res) {
                $scope.obj2 = cur_res.data;
                $scope.chkCur = angular.copy(cur_res.data);
                //console.log($scope.obj2);
                // $scope.getAcademicyr()
            });
            $scope.get_ischecked = function (Recepient, grade) {
                $scope.cleardata();
                debugger;
                if (grade == "true") {
                    $scope.show_bus = false;
                    if (Recepient == "true") {
                        $scope.show_grade = true;
                        $scope.show_designation = false;
                        grade = "";
                        section = "";
                        $scope.chk_rec = false;
                        $scope.chk_rec = false;
                        $scope.edt.existing_rec = 0;
                        //$scope.edt.sims_parent = "true";
                        //$scope.getcleared();

                        // $scope.cleardata();
                    }
                    if (Recepient == "false") {

                        $scope.temp_show_grad_bus = false;
                        $scope.div_recipient = false;
                        $scope.show_grade = false;
                        $scope.show_designation = true;
                        $scope.edt.existing_rec = 0;
                        $scope.chk_rec = false;
                        $scope.chk_rec1 = true;
                        //$scope.getcleared();

                        $http.get(ENV.apiUrl + "api/common/getAllDesignationName").then(function (res) {
                            $scope.designation = res.data;
                            // console.log($scope.designation);
                            setTimeout(function () {
                                $('#cmb_designation').click(function () {
                                    // console.log($(this).val());
                                    desgination = $(this).val();
                                }).multipleSelect({
                                    width: '100%'
                                });
                            }, 1000);
                        });

                        $http.get(ENV.apiUrl + "api/common/getDepartmentName").then(function (res) {
                            $scope.department = res.data;
                            // console.log($scope.department);
                            setTimeout(function () {
                                $('#cmb_dept').change(function () {
                                    //console.log($(this).val());
                                    departt = $(this).val();
                                }).multipleSelect({
                                    width: '100%'
                                });
                            }, 1000);
                        });


                        //$scope.cleardata();
                    }
                }
                else {
                    $scope.show_bus = true;
                    $scope.show_grade = false;
                    $scope.show_designation = false;
                    $scope.chk_rec = false;
                    $scope.chk_rec1 = false;
                    $http.get(ENV.apiUrl + "api/UpdateCancleStudentTransDetails/getBusCode").then(function (getBusCode_Data) {
                        $scope.E_BusCode_Data = getBusCode_Data.data;
                        setTimeout(function () {
                            $('#cmb_buscode').change(function () {
                                grade = $(this).val();
                            }).multipleSelect({
                                width: '100%',
                            });
                        }, 1000);
                    });
                }
            }

            $scope.get_receiptClear = function () {
                //$scope.obj2 = [];
                //$scope.obj2 = $scope.chkCur;
                $scope.EmailId_result = [];
                $scope.MobileNo_result = [];

                $scope.chk_rec = true;
                $('#cmb_grade').multipleSelect('enable');
                $('#cmb_section').multipleSelect('enable');

                $('#cmb_routeCode_name').multipleSelect('enable');

                try {
                    $('#cmb_grade').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_section').multipleSelect('uncheckAll');
                }
                catch (e) {

                }

                try {
                    $('#cmb_dept').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_designation').multipleSelect('uncheckAll');
                } catch (e) {
                }


            }

            $scope.getcleared = function () {
                //$scope.div_recipient = true;
                //$scope.edt.sims_Email = "true";
                $scope.edt.sims_father = false;
                $scope.edt.sims_mother = false;
                $scope.edt.sims_guardian = false;
                $scope.edt.sims_active = false;
                $scope.edt.sims_emergency = false;
                $scope.edt.sims_father_indi = false;
                $scope.edt.sims_mother_indi = false;
                $scope.edt.sims_guardian_indi = false;
                $scope.edt.screening_cur_code = '';
                $scope.edt.sims_academic_year = '';
                $scope.edt.screening_grade_code = '';
                $scope.edt.grade_section_code = '';
                //$scope.obj2 = [];
                $scope.department = [];
                $scope.designation = [];
                $scope.EmailId_result = [];
                $scope.MobileNo_result = [];
                $('#cmb_grade').multipleSelect('enable');
                $('#cmb_section').multipleSelect('enable');

                try {
                    $('#cmb_grade').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_section').multipleSelect('uncheckAll');
                }
                catch (e) {

                }

                try {
                    $('#cmb_dept').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_designation').multipleSelect('uncheckAll');
                } catch (e) {

                }
                try {
                    $('#cmb_buscode').multipleSelect('uncheckAll');
                } catch (e) {

                } try {
                    $('#cmb_routeCode_name').multipleSelect('uncheckAll');
                } catch (e) {

                }

            }

            $scope.getAcademicyr = function (cur) {
                debugger;
                $scope.grade = [];
                grade = [];
                $scope.section = [];
                $scope.acyrs = [];
                setTimeout(function () {
                    $('#cmb_section').change(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur).then(function (yer) {
                    $scope.acyrs = yer.data;
                    $scope.getGrade(cur, $scope.acyrs[0].sims_academic_year);
                    //$('#cmb_grade').multipleSelect({
                    //    width: '100%',
                    //    placeholder: "Select Grade"
                    //});

                    //$('#cmb_section').multipleSelect({
                    //    width: '100%',
                    //    placeholder: "Select Section"
                    //});


                });
            }

            $scope.getGrade = function (cur, acyr) {
                $scope.grade = [];
                grade = [];
                $scope.section = [];
                setTimeout(function () {
                    $('#cmb_section').change(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);

                if ($scope.edt.sims_parent == "true") {
                    if (cur != "" || cur != undefined) {
                        $scope.chk_rec = false;
                        $http.get(ENV.apiUrl + "api/common/EmailSms/Get_Grade_CodebyCuriculum?cur_code=" + cur + "&acad_yr=" + acyr).then(function (res) {
                            //                        $http.get(ENV.apiUrl + "api/common/Get_Grade_CodebyCuriculum?cur_code=" + cur).then(function (res) {
                            $scope.grade = res.data;
                            setTimeout(function () {
                                $('#cmb_grade').change(function () {
                                    //  console.log($(this).val());
                                    grade = $(this).val();
                                }).multipleSelect({
                                    width: '100%',
                                });
                            }, 1000);
                        });
                    }
                }
            }

            $scope.getSection = function (cur, grade) {

                var temp_grade = [];
                if (grade.length != 0) {
                    if (grade != ',') {

                        for (var i = 0; i < grade.length; i++) {
                            if (i == 0) {
                                temp_grade = grade[i];
                            }
                            else {
                                temp_grade = temp_grade + ',' + grade[i];
                            }
                        }
                    }
                    if ($scope.edt.sims_parent == "true") {
                        if (cur != "" && grade != "" || cur != undefined && grade != undefined) {
                            grade = [];
                            grade = temp_grade;
                            $scope.section = [];
                            $http.get(ENV.apiUrl + "api/common/Get_Section_CodebyCuriculum?cur_code=" + cur + "&grade_code=" + grade).then(function (res) {
                                $scope.section = res.data;
                                setTimeout(function () {
                                    $('#cmb_section').change(function () {
                                        section = $(this).val();
                                    }).multipleSelect({
                                        multiple: true,
                                        width: '100%'
                                    });
                                    $("#cmb_section").multipleSelect("checkAll");
                                }, 1000);
                            });
                        }
                    }
                }
            }


            $scope.chk_father = function (father) {

                if ($scope.edt.sims_Email == "true" && $scope.edt.sims_parent == "true") {
                    if ($scope.edt.screening_cur_code != undefined && $scope.edt.screening_grade_code != undefined && $scope.edt.grade_section_code != undefined) {
                        $("#cmb_grade,#cmb_section").multipleSelect("disable");
                    }

                    if (father == true) {
                        $scope.edt.sims_active = false;
                        $scope.edt.sims_father_indi = false;
                        $scope.edt.sims_mother_indi = false;
                        $scope.edt.sims_guardian_indi = false;
                        $scope.btn_search = true;
                        $scope.GetParentEmailInfo();
                    }
                    else {
                        if ($scope.edt.sims_father == true || $scope.edt.sims_mother == true || $scope.edt.sims_guardian == true || $scope.edt.sims_active == true) {
                            $("#cmb_grade,#cmb_section").multipleSelect("disable");
                        }
                        else {
                            $("#cmb_grade,#cmb_section").multipleSelect("enable");
                        }

                        $scope.EmailId_result = [];
                        $scope.MobileNo_result = [];
                        $scope.GetParentEmailInfo();
                    }
                }
                else if ($scope.edt.sims_Email == "false" && $scope.edt.sims_parent == "true") {
                    if ($scope.edt.screening_cur_code != undefined && $scope.edt.screening_grade_code != undefined && $scope.edt.grade_section_code != undefined) {
                        $("#cmb_grade,#cmb_section").multipleSelect("disable");
                    }
                    if (father == true) {
                        $scope.edt.sims_active = false;
                        $scope.edt.sims_father_indi = false;
                        $scope.edt.sims_mother_indi = false;
                        $scope.edt.sims_guardian_indi = false;
                        $scope.btn_search = true;
                        $scope.GetParentMobileContactsInfo();
                    }
                    else {
                        if ($scope.edt.sims_father == true || $scope.edt.sims_mother == true || $scope.edt.sims_guardian == true || $scope.edt.sims_active == true) {
                            $("#cmb_grade,#cmb_section").multipleSelect("disable");
                        }
                        else {
                            $("#cmb_grade,#cmb_section").multipleSelect("enable");
                        }
                        $scope.EmailId_result = [];
                        $scope.MobileNo_result = [];
                        $scope.GetParentMobileContactsInfo();
                    }
                }
                //else if ($scope.edt.sims_Email == "true" && $scope.edt.sims_parent == "undefined")
                //{
                //    swal({ title: "Alert", text: "Please Select Recipient.", showCloseButton: true, width: 380, });
                //}
            }

            $scope.chk_mother = function (mother) {
                if ($scope.edt.sims_Email == "true" && $scope.edt.sims_parent == "true") {
                    if ($scope.edt.screening_cur_code != undefined && $scope.edt.screening_grade_code != undefined && $scope.edt.grade_section_code != undefined) {
                        $("#cmb_grade,#cmb_section").multipleSelect("disable");
                    }

                    if (mother == true) {
                        $scope.edt.sims_active = false;
                        $scope.edt.sims_father_indi = false;
                        $scope.edt.sims_mother_indi = false;
                        $scope.edt.sims_guardian_indi = false;
                        $scope.btn_search = true;
                        $scope.GetParentEmailInfo();
                    }
                    else {
                        if ($scope.edt.sims_father == true || $scope.edt.sims_mother == true || $scope.edt.sims_guardian == true || $scope.edt.sims_active == true) {
                            $("#cmb_grade,#cmb_section").multipleSelect("disable");
                        }
                        else {
                            $("#cmb_grade,#cmb_section").multipleSelect("enable");
                        }
                        $scope.EmailId_result = [];
                        $scope.MobileNo_result = [];
                        $scope.GetParentEmailInfo();
                    }
                }
                else if ($scope.edt.sims_Email == "false" && $scope.edt.sims_parent == "true") {
                    if ($scope.edt.screening_cur_code != undefined && $scope.edt.screening_grade_code != undefined && $scope.edt.grade_section_code != undefined) {
                        $("#cmb_grade,#cmb_section").multipleSelect("disable");
                    }

                    if (mother == true) {
                        $scope.edt.sims_active = false;
                        $scope.edt.sims_father_indi = false;
                        $scope.edt.sims_mother_indi = false;
                        $scope.edt.sims_guardian_indi = false;
                        $scope.btn_search = true;
                        $scope.GetParentMobileContactsInfo();
                    }
                    else {
                        if ($scope.edt.sims_father == true || $scope.edt.sims_mother == true || $scope.edt.sims_guardian == true || $scope.edt.sims_active == true) {
                            $("#cmb_grade,#cmb_section").multipleSelect("disable");
                        }
                        else {
                            $("#cmb_grade,#cmb_section").multipleSelect("enable");
                        }
                        $scope.EmailId_result = [];
                        $scope.MobileNo_result = [];
                        $scope.GetParentMobileContactsInfo();
                    }
                }

            }

            $scope.chk_guar = function (guar) {
                if ($scope.edt.sims_Email == "true" && $scope.edt.sims_parent == "true") {
                    if ($scope.edt.screening_cur_code != undefined && $scope.edt.screening_grade_code != undefined && $scope.edt.grade_section_code != undefined) {
                        $("#cmb_grade,#cmb_section").multipleSelect("disable");
                    }
                    if (guar == true) {
                        $scope.edt.sims_active = false;
                        $scope.edt.sims_father_indi = false;
                        $scope.edt.sims_mother_indi = false;
                        $scope.edt.sims_guardian_indi = false;
                        $scope.btn_search = true;
                        $scope.GetParentEmailInfo();
                    }
                    else {
                        if ($scope.edt.sims_father == true || $scope.edt.sims_mother == true || $scope.edt.sims_guardian == true || $scope.edt.sims_active == true) {
                            $("#cmb_grade,#cmb_section").multipleSelect("disable");
                        }
                        else {
                            $("#cmb_grade,#cmb_section").multipleSelect("enable");
                        }
                        $scope.EmailId_result = [];
                        $scope.MobileNo_result = [];
                        $scope.GetParentEmailInfo();
                    }
                }
                if ($scope.edt.sims_Email == "false" && $scope.edt.sims_parent == "true") {
                    if ($scope.edt.screening_cur_code != undefined && $scope.edt.screening_grade_code != undefined && $scope.edt.grade_section_code != undefined) {
                        $("#cmb_grade,#cmb_section").multipleSelect("disable");
                    }

                    if (guar == true) {
                        $scope.edt.sims_active = false;
                        $scope.edt.sims_father_indi = false;
                        $scope.edt.sims_mother_indi = false;
                        $scope.edt.sims_guardian_indi = false;
                        $scope.btn_search = true;
                        $scope.GetParentMobileContactsInfo();
                    }
                    else {
                        if ($scope.edt.sims_father == true || $scope.edt.sims_mother == true || $scope.edt.sims_guardian == true || $scope.edt.sims_active == true) {
                            $("#cmb_grade,#cmb_section").multipleSelect("disable");
                        }
                        else {
                            $("#cmb_grade,#cmb_section").multipleSelect("enable");
                        }
                        $scope.EmailId_result = [];
                        $scope.MobileNo_result = [];
                        $scope.GetParentMobileContactsInfo();
                    }
                }

            }

            $scope.chk_active = function (active) {
                if ($scope.edt.sims_Email == "true" && $scope.edt.sims_parent == "true") {
                    if ($scope.edt.screening_cur_code != undefined && $scope.edt.screening_grade_code != undefined && $scope.edt.grade_section_code != undefined) {
                        $("#cmb_grade,#cmb_section").multipleSelect("disable");
                    }

                    if (active == true) {
                        $scope.edt.sims_father = false;
                        $scope.edt.sims_mother = false;
                        $scope.edt.sims_guardian = false;
                        $scope.edt.sims_emergency = false;

                        $scope.edt.sims_father_indi = false;
                        $scope.edt.sims_mother_indi = false;
                        $scope.edt.sims_guardian_indi = false;
                        $scope.btn_search = true;
                        $scope.GetParentEmailInfo();
                    }
                    else {
                        if ($scope.edt.sims_father == true || $scope.edt.sims_mother == true || $scope.edt.sims_guardian == true || $scope.edt.sims_active == true) {
                            $("#cmb_grade,#cmb_section").multipleSelect("disable");
                        }
                        else {
                            $("#cmb_grade,#cmb_section").multipleSelect("enable");
                        }
                        $scope.GetParentEmailInfo();
                    }
                }
                if ($scope.edt.sims_Email == "false" && $scope.edt.sims_parent == "true") {
                    if ($scope.edt.screening_cur_code != undefined && $scope.edt.screening_grade_code != undefined && $scope.edt.grade_section_code != undefined) {
                        $("#cmb_grade,#cmb_section").multipleSelect("disable");
                    }
                    if (active == true) {
                        $scope.edt.sims_father = false;
                        $scope.edt.sims_mother = false;
                        $scope.edt.sims_guardian = false;
                        $scope.edt.sims_emergency = false;

                        $scope.edt.sims_father_indi = false;
                        $scope.edt.sims_mother_indi = false;
                        $scope.edt.sims_guardian_indi = false;
                        $scope.btn_search = true;
                        $scope.GetParentMobileContactsInfo();
                    }
                    else {
                        if ($scope.edt.sims_father == true || $scope.edt.sims_mother == true || $scope.edt.sims_guardian == true || $scope.edt.sims_active == true) {
                            $("#cmb_grade,#cmb_section").multipleSelect("disable");
                        }
                        else {
                            $("#cmb_grade,#cmb_section").multipleSelect("enable");
                        }
                        $scope.EmailId_result = [];
                        $scope.MobileNo_result = [];
                        $scope.GetParentMobileContactsInfo();
                    }
                }
            }

            $scope.chk_emergency = function (emergency) {
                if ($scope.edt.sims_Email == "false" && $scope.edt.sims_parent == "true") {
                    if ($scope.edt.screening_cur_code != undefined && $scope.edt.screening_grade_code != undefined && $scope.edt.grade_section_code != undefined) {
                        $("#cmb_grade,#cmb_section").multipleSelect("disable");
                    }
                    if (emergency == true) {
                        $scope.edt.sims_active = false;
                        $scope.edt.sims_father_indi = false;
                        $scope.edt.sims_mother_indi = false;
                        $scope.edt.sims_guardian_indi = false;
                        $scope.btn_search = true;
                        $scope.GetParentMobileContactsInfo();
                    }
                    else {
                        if ($scope.edt.sims_father == true || $scope.edt.sims_mother == true || $scope.edt.sims_guardian == true || $scope.edt.sims_active == true) {
                            $("#cmb_grade,#cmb_section").multipleSelect("disable");
                        }
                        else {
                            $("#cmb_grade,#cmb_section").multipleSelect("enable");
                        }
                        $scope.EmailId_result = [];
                        $scope.MobileNo_result = [];
                        $scope.GetParentMobileContactsInfo();
                    }
                }

            }

            $scope.chk_father_indi = function (father_indi) {
                debugger;
                if (($scope.edt.sims_Email == "true" || $scope.edt.sims_Email == "false") && father_indi == true) {
                    $scope.get_receiptClear();
                    $scope.div_recipient = false;
                    $scope.edt.sims_father = false;
                    $scope.edt.sims_mother = false;
                    $scope.edt.sims_guardian = false;
                    $scope.edt.sims_active = false;
                    $scope.edt.sims_emergency = false;
                    $scope.btn_search = false;
                    $scope.chk_rec = true;
                    $scope.chk_rec1 = false;
                    $scope.edt.existing_rec = 0;
                    if ($scope.show_grade == true) {
                        $scope.show_grade = false;
                    }
                    else if ($scope.show_designation == true) {
                        $scope.show_designation = false;
                    }
                    else if ($scope.show_bus == true) {
                        $scope.show_bus = false;
                    }

                }
                else {
                    //swal({ title: "Alert", text: "Please Select message Type and Recipient.", showCloseButton: true, width: 380, });
                    $scope.edt.sims_father_indi = false;
                    $scope.div_recipient = false;
                    $scope.edt.sims_father = false;
                    $scope.edt.sims_mother = false;
                    $scope.edt.sims_guardian = false;
                    $scope.edt.sims_active = false;
                    $scope.edt.sims_emergency = false;
                    $scope.btn_search = false;
                    //$scope.div_recipient = true;
                    if ($scope.edt.sims_father_indi == false && $scope.edt.sims_mother_indi == false && $scope.edt.sims_guardian_indi == false) {
                        $scope.chk_rec = false;
                        $scope.chk_rec1 = false;
                        if ($scope.edt.sims_grade == 'true') {
                            $scope.show_grade = true;
                            $scope.show_bus = false;
                        }
                        else if ($scope.edt.sims_grade == 'false') {
                            $scope.show_grade = false;
                            $scope.show_bus = true;
                        }
                        else if ($scope.edt.sims_parent == 'false') {
                            $scope.show_designation = true;
                        }
                    }


                }
            }

            $scope.chk_mother_indi = function (mother_indi) {
                if (($scope.edt.sims_Email == "true" || $scope.edt.sims_Email == "false") && mother_indi == true) {
                    $scope.get_receiptClear();
                    $scope.edt.sims_father = false;
                    $scope.edt.sims_mother = false;
                    $scope.edt.sims_guardian = false;
                    $scope.edt.sims_active = false;
                    $scope.edt.sims_emergency = false;
                    $scope.btn_search = false;
                    $scope.edt.existing_rec = 0;
                    $scope.div_recipient = false;
                    $scope.chk_rec = true;
                    $scope.chk_rec1 = false;
                    if ($scope.show_grade == true) {
                        $scope.show_grade = false;
                    }
                    else if ($scope.show_designation == true) {
                        $scope.show_designation = false;
                    }
                    else if ($scope.show_bus == true) {
                        $scope.show_bus = false;
                    }
                }
                else {
                    // swal({ title: "Alert", text: "Please Select message Type and Recipient.", showCloseButton: true, width: 380, });
                    $scope.edt.sims_mother_indi = false;
                    $scope.edt.sims_father = false;
                    $scope.edt.sims_mother = false;
                    $scope.edt.sims_guardian = false;
                    $scope.edt.sims_active = false;
                    $scope.edt.sims_emergency = false;
                    $scope.btn_search = false;
                    $scope.div_recipient = false;
                    if ($scope.edt.sims_father_indi == false && $scope.edt.sims_mother_indi == false && $scope.edt.sims_guardian_indi == false) {
                        $scope.chk_rec = false;
                        $scope.chk_rec1 = false;
                    }
                    if ($scope.edt.sims_father_indi == false && $scope.edt.sims_mother_indi == false && $scope.edt.sims_guardian_indi == false) {
                        $scope.chk_rec = false;
                        $scope.chk_rec1 = false;
                        if ($scope.edt.sims_grade == 'true') {
                            $scope.show_grade = true;
                            $scope.show_bus = false;
                        }
                        else if ($scope.edt.sims_grade == 'false') {
                            $scope.show_grade = false;
                            $scope.show_bus = true;
                        }
                        else if ($scope.edt.sims_parent == 'false') {
                            $scope.show_designation = true;
                        }
                    }

                    // $scope.div_recipient = true;
                }
            }

            $scope.chk_guardian_indi = function (guardian_indi) {
                if (($scope.edt.sims_Email == "true" || $scope.edt.sims_Email == "false") && guardian_indi == true) {
                    $scope.get_receiptClear();
                    $scope.edt.sims_father = false;
                    $scope.edt.sims_mother = false;
                    $scope.edt.sims_guardian = false;
                    $scope.edt.sims_active = false;
                    $scope.edt.sims_emergency = false;
                    $scope.btn_search = false;
                    $scope.edt.existing_rec = 0;
                    $scope.div_recipient = false;
                    $scope.chk_rec = true;
                    $scope.chk_rec1 = false;
                    if ($scope.show_grade == true) {
                        $scope.show_grade = false;
                    }
                    else if ($scope.show_designation == true) {
                        $scope.show_designation = false;
                    }
                    else if ($scope.show_bus == true) {
                        $scope.show_bus = false;
                    }
                }
                else {
                    //swal({ title: "Alert", text: "Please Select message Type and Recipient.", showCloseButton: true, width: 380, });
                    $scope.edt.sims_guardian_indi = false;
                    $scope.edt.sims_father = false;
                    $scope.edt.sims_mother = false;
                    $scope.edt.sims_guardian = false;
                    $scope.edt.sims_active = false;
                    $scope.edt.sims_emergency = false;
                    $scope.btn_search = false;
                    $scope.div_recipient = false;
                    if ($scope.edt.sims_father_indi == false && $scope.edt.sims_mother_indi == false && $scope.edt.sims_guardian_indi == false) {
                        $scope.chk_rec = false;
                        $scope.chk_rec1 = false;
                    }
                    if ($scope.edt.sims_father_indi == false && $scope.edt.sims_mother_indi == false && $scope.edt.sims_guardian_indi == false) {
                        $scope.chk_rec = false;
                        $scope.chk_rec1 = false;
                        if ($scope.edt.sims_grade == 'true') {
                            $scope.show_grade = true;
                            $scope.show_bus = false;
                        }
                        else if ($scope.edt.sims_grade == 'false') {
                            $scope.show_grade = false;
                            $scope.show_bus = true;
                        }
                        else if ($scope.edt.sims_parent == 'false') {
                            $scope.show_designation = true;
                        }
                    }

                }
            }


            $scope.GetParentEmailInfo = function () {

                $scope.edt.existing_rec = 0;
                // $("#cmb_grade,#cmb_section").multipleSelect("enable");
                $scope.ParentEmailIds = [];
                var v = document.getElementById('cmb_cur');
                $scope.edt.screening_cur_code = v.value;

                if ($scope.edt.sims_parent == 'true' || $scope.edt.sims_parent == 'false') {
                    if ($scope.edt.sims_father == false && $scope.edt.sims_mother == false && $scope.edt.sims_guardian == false && $scope.edt.sims_active == false && $scope.edt.sims_father_indi == false && $scope.edt.sims_mother_indi == false && $scope.edt.sims_guardian_indi == false) {
                        $scope.edt.existing_rec = 0;
                        swal({ text: "Please Select Recipient", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    else if (grade == undefined && section == undefined) {
                        swal({ text: "Please Select Grade & Section", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                        $scope.edt.sims_father = false;
                        $scope.edt.sims_mother = false;
                        $scope.edt.sims_guardian = false;
                        $scope.edt.sims_active = false;
                    }
                    else {
                        if ($scope.edt.sims_father == undefined || $scope.edt.sims_father == '') {
                            $scope.edt['sims_father'] = false;
                        }

                        if ($scope.edt.sims_mother == undefined || $scope.edt.sims_mother == '') {
                            $scope.edt['sims_mother'] = false;
                        }

                        if ($scope.edt.sims_guardian == undefined || $scope.edt.sims_guardian == '') {
                            $scope.edt['sims_guardian'] = false;
                        }
                        var data = $scope.edt;
                        var section_code;
                        var sec = $scope.edt.grade_section_code;
                        section_code = section_code + ',' + sec;
                        var str2 = section_code.substr(section_code.indexOf(',') + 1);
                        if ($scope.edt.sims_grade == 'false') {
                            str2 = v.value;
                            if ($scope.edt.unique_email) {
                                str2 = str2 + 'true';
                            }
                            var route_code_sending = []; var temp_route_code_sending = [], Sending_route_Code = [];;
                            route_code_sending = $scope.item.sims_transport_route_code;
                            for (var i = 0; i < route_code_sending.sims_transport_route_code.length; i++) {
                                if (temp_route_code_sending.length == 0) {
                                    temp_route_code_sending = route_code_sending.sims_transport_route_code[i].sims_transport_route_direction;
                                    Sending_route_Code = route_code_sending.sims_transport_route_code[i].sims_transport_route_code;

                                }
                                else {
                                    temp_route_code_sending = temp_route_code_sending + "," + route_code_sending.sims_transport_route_code[i].sims_transport_route_direction;
                                    Sending_route_Code = Sending_route_Code + ',' + route_code_sending.sims_transport_route_code[i].sims_transport_route_code;
                                }
                            }

                            $http.get(ENV.apiUrl + "api/common/EmailSms/GetSpecificParentEmailIdsByBUS?iffather=" + data.sims_father + "&ifmother=" + data.sims_mother + "&ifguardian=" + data.sims_guardian + "&active=" + data.sims_active + "&curCode=" + str2 + "&route_code=" + Sending_route_Code + "&route_direction=" + temp_route_code_sending + "&ac_year=" + $scope.edt.sims_academic_year).then(function (res) {
                                $scope.ParentEmailIds = res.data;
                                $scope.EmailId_result = $scope.ParentEmailIds;
                                $scope.searchEmailId_result = $scope.EmailId_result;
                                // $scope.SelectedUserLst = $scope.EmailId_result;
                                //  console.log($scope.ParentEmailIds.length);

                                $scope.edt.existing_rec = $scope.ParentEmailIds.length;
                            });
                        }
                        else {

                            str2 = section_code.substr(section_code.indexOf(',') + 1);
                            if ($scope.edt.unique_email) {
                                str2 = str2 + 'true';
                            }
                            $http.get(ENV.apiUrl + "api/common/EmailSms/GetSpecificParentEmailIds?iffather=" + data.sims_father + "&ifmother=" + data.sims_mother + "&ifguardian=" + data.sims_guardian + "&active=" + data.sims_active + "&sections=" + str2).then(function (res) {
                                $scope.ParentEmailIds = res.data;
                                $scope.EmailId_result = $scope.ParentEmailIds;
                                $scope.searchEmailId_result = $scope.EmailId_result;
                                // $scope.SelectedUserLst = $scope.EmailId_result;
                                //  console.log($scope.ParentEmailIds.length);

                                $scope.edt.existing_rec = $scope.ParentEmailIds.length;
                            });
                        }

                    }

                }
                else {
                    swal({ text: "Please Select message Type and Recipient.", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }

            }

            $scope.GetParentMobileContactsInfo = function () {
                debugger;
                $scope.parentMobile = [];
                var v = document.getElementById('cmb_cur');
                $scope.edt.screening_cur_code = v.value;

                if ($scope.edt.sims_parent == 'true' || $scope.edt.sims_parent == 'false') {
                    if ($scope.edt.sims_father == false && $scope.edt.sims_mother == false && $scope.edt.sims_guardian == false && $scope.edt.sims_active == false && $scope.edt.sims_emergency == false && $scope.edt.sims_father_indi == false && $scope.edt.sims_mother_indi == false && $scope.edt.sims_guardian_indi == false) {
                        $scope.edt.existing_rec = 0;
                        swal({ text: "Please Select Recipient", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    else if (grade == undefined && section == undefined) {
                        swal({ text: "Please Select Grade & Section", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                        $scope.edt.sims_father = false;
                        $scope.edt.sims_mother = false;
                        $scope.edt.sims_guardian = false;
                        $scope.edt.sims_active = false;
                    }
                    else {

                        var data = $scope.edt;
                        // console.log(data.sims_father);
                        var section_code;
                        var sec = $scope.edt.grade_section_code;
                        section_code = section_code + ',' + sec;
                        var str2 = section_code.substr(section_code.indexOf(',') + 1);
                        if ($scope.edt.sims_grade == 'false') {
                            str2 = v.value;
                            if ($scope.edt.unique_email) { //unique_email is use for unique mobile number
                                str2 = str2 + 'true';
                            }
                            var route_code_sending = []; var temp_route_code_sending = [], Sending_route_Code = [];;
                            route_code_sending = $scope.item.sims_transport_route_code;
                            for (var i = 0; i < route_code_sending.sims_transport_route_code.length; i++) {
                                if (temp_route_code_sending.length == 0) {
                                    temp_route_code_sending = route_code_sending.sims_transport_route_code[i].sims_transport_route_direction;
                                    Sending_route_Code = route_code_sending.sims_transport_route_code[i].sims_transport_route_code;

                                }
                                else {
                                    temp_route_code_sending = temp_route_code_sending + "," + route_code_sending.sims_transport_route_code[i].sims_transport_route_direction;
                                    Sending_route_Code = Sending_route_Code + ',' + route_code_sending.sims_transport_route_code[i].sims_transport_route_code;
                                }
                            }

                            $http.get(ENV.apiUrl + "api/common/EmailSms/GetSpecificParentMobileByBUS?iffather=" + data.sims_father + "&ifmother=" + data.sims_mother + "&ifguardian=" + data.sims_guardian + "&active=" + data.sims_active + "&ifemergency=" + data.sims_emergency + "&curCode=" + str2 + "&route_code=" + Sending_route_Code + "&route_direction=" + temp_route_code_sending + "&ac_year=" + $scope.edt.sims_academic_year).then(function (res) {
                                $scope.parentMobile = res.data;
                                $scope.parentMobile1 = $scope.parentMobile;
                                $scope.MobileNo_result = $scope.parentMobile1;
                                $scope.searchMobileNo_result = $scope.MobileNo_result;
                                // $scope.SelectedUserLst = $scope.MobileNo_result;
                                // console.log($scope.parentMobile);

                                $scope.edt.existing_rec = $scope.parentMobile.length;
                            });
                        }
                        else {
                            str2 = section_code.substr(section_code.indexOf(',') + 1);
                            if ($scope.edt.unique_email) { //unique_email is use for unique mobile number
                                str2 = str2 + 'true';
                            }
                            $http.get(ENV.apiUrl + "api/common/EmailSms/GetSpecificParentMobile?iffather=" + data.sims_father + "&ifmother=" + data.sims_mother + "&ifguardian=" + data.sims_guardian + "&active=" + data.sims_active + "&ifemergency=" + data.sims_emergency + "&sections=" + str2).then(function (res) {
                                $scope.parentMobile = res.data;
                                $scope.parentMobile1 = $scope.parentMobile;
                                $scope.MobileNo_result = $scope.parentMobile1;
                                $scope.searchMobileNo_result = $scope.MobileNo_result;
                                // $scope.SelectedUserLst = $scope.MobileNo_result;
                                // console.log($scope.parentMobile);

                                $scope.edt.existing_rec = $scope.parentMobile.length;
                            });
                        }


                    }
                }
                else {
                    swal({ text: "Please Select message Type and Recipient.", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }

            }

            $scope.GetEmployeeInfo = function () {
                $scope.edt.existing_rec = '0';
                grade = "";
                section = "";
                if ($scope.edt.sims_Email == "true") {
                    if ($scope.edt.sims_parent == 'false') {
                        var desig_code, dept_code;
                        var desg = $scope.edt.designation;
                        desig_code = desig_code + ',' + desg;
                        var design = desig_code.substr(desig_code.indexOf(',') + 1);

                        var dept = $scope.edt.em_dept_code;
                        dept_code = dept_code + ',' + dept;
                        var department = dept_code.substr(dept_code.indexOf(',') + 1);

                        if (design != undefined || department != undefined) {
                            $http.get(ENV.apiUrl + "api/common/EmailSms/GetEmployeesEmailIds?desg=" + design + "&dept=" + department).then(function (res) {

                                // $scope.SelectedUserLst = $scope.EmailId_result;
                                // console.log($scope.empEmailIds.length);

                                if ($scope.edt.em_dept_code.length != 0) {
                                    $scope.empEmailIds = res.data;
                                    $scope.EmailId_result = $scope.empEmailIds;
                                    $scope.searchEmailId_result = $scope.EmailId_result;

                                    $scope.edt.existing_rec = $scope.EmailId_result.length;

                                }
                                else {
                                    $scope.edt.existing_rec = '0';
                                }

                            });
                        }

                    }
                }
                else if ($scope.edt.sims_Email == "false") {
                    if ($scope.edt.sims_parent == 'false') {
                        var desig_code, dept_code;
                        var desg = $scope.edt.designation;
                        desig_code = desig_code + ',' + desg;
                        var design = desig_code.substr(desig_code.indexOf(',') + 1);

                        var dept = $scope.edt.em_dept_code;
                        dept_code = dept_code + ',' + dept;
                        var department = dept_code.substr(dept_code.indexOf(',') + 1);

                        if (design != undefined && department != undefined) {
                            $http.get(ENV.apiUrl + "api/common/EmailSms/GetEmployeesContacts?desg=" + design + "&dept=" + department).then(function (res) {

                                //  $scope.SelectedUserLst = $scope.MobileNo_result;
                                //console.log($scope.empMobContacts.length);

                                if ($scope.edt.em_dept_code.length != 0) {
                                    $scope.empMobContacts = res.data;
                                    $scope.EmpMobile = $scope.empMobContacts;
                                    $scope.MobileNo_result = $scope.EmpMobile;
                                    $scope.searchMobileNo_result = $scope.MobileNo_result;
                                    $scope.edt.existing_rec = $scope.empMobContacts.length;
                                }
                                else {
                                    $scope.edt.existing_rec = '0';
                                }


                                //$scope.EmpMobile = res.data;
                                //$scope.MobileNo_result = $scope.EmpMobile;
                                //console.log($scope.EmpMobile.length);
                                //$scope.edt.existing_rec = $scope.EmpMobile.length;
                            });
                        }
                    }
                }

            }

            //$scope.Getsms_details = function () {
            //    $scope.GetParentMobileContactsInfo();
            //}

            $scope.msg_details = function () {
                if ($scope.edt.sims_smstext != "" && $scope.edt.sims_smstext != undefined) {
                    var q = 0, cnt = 0;
                    $scope.edt.sims_sms_char = 0;

                    var v = document.getElementById('txt_smstext');
                    var r = v.value.length;
                    var sms = $scope.edt.sims_smstext;
                    //console.log(v.value.length);

                    var nameList = new Array('[', ']', '{', '}');
                    if (sms != "") {
                        for (var x = 0, c = ''; c = sms.charAt(x) ; x++) {
                            for (var i = 0; i < nameList.length; i++) {
                                if (nameList[i] === c) {
                                    q = q + 1;
                                }
                            }
                        }
                        var ss = q * 2;
                        var smsLen = r;
                        cnt = (smsLen - q) + ss;
                        $scope.edt.sims_sms_char = cnt;
                    }

                    if ($scope.edt.sims_sms_char <= 160 && $scope.edt.sims_sms_char > 0)
                        $scope.edt.sims_sms_length = "1";
                    if ($scope.edt.sims_sms_char <= 320 && $scope.edt.sims_sms_char > 160)
                        $scope.edt.sims_sms_length = "2";
                    if ($scope.edt.sims_sms_char <= 480 && $scope.edt.sims_sms_char > 320)
                        $scope.edt.sims_sms_length = "3";
                    if ($scope.edt.sims_sms_char <= 640 && $scope.edt.sims_sms_char > 480)
                        $scope.edt.sims_sms_length = "4";
                    if ($scope.edt.sims_sms_char <= 800 && $scope.edt.sims_sms_char > 640)
                        $scope.edt.sims_sms_length = "5";
                    if ($scope.edt.sims_sms_char <= 960 && $scope.edt.sims_sms_char > 800)
                        $scope.edt.sims_sms_length = "6";
                    if ($scope.edt.sims_sms_char <= 1000 && $scope.edt.sims_sms_char > 960)
                        $scope.edt.sims_sms_length = "7";
                }
                else {
                    $scope.edt.sims_sms_char = "0";
                    $scope.edt.sims_sms_length = "0";
                }
            }

            $scope.search2 = function () {
            }

            $scope.cleardata = function () {
                $scope.edt.existing_rec = '0';
                //var data = $scope.edt;
                //$scope.edt = [];
                $scope.parentMobile1 = [];
                $scope.edt.sims_father = false;
                $scope.edt.sims_mother = false;
                $scope.edt.sims_guardian = false;
                $scope.edt.sims_active = false;
                $scope.edt.sims_emergency = false;
                $scope.edt.sims_father_indi = false;
                $scope.edt.sims_mother_indi = false;
                $scope.edt.sims_guardian_indi = false;
                $scope.getfather_indi = [];
                $scope.getmother_indi = [];
                $scope.getguardian_indi = [];
                $scope.getEmp_indi = [];
                //$scope.chk_rec = true;
                //$scope.chk_rec1 = true;
                //$scope.edt.sims_parent = false;
                //$scope.edt.screening_cur_code = "";
                //  $scope.obj2 = [];\
                //$scope.acyrs = [];
                //var acadamic_year = $scope.acyrs;                
                //$scope.acyrs = acadamic_year[0];
                $scope.grade = [];
                $scope.section = [];
                $scope.EmailId_result = [];
                $scope.MobileNo_result = [];
                //$scope.acyrs = [];
                $('#cmb_grade').multipleSelect('enable');
                $('#cmb_section').multipleSelect('enable');
                $('#cmb_dept').multipleSelect('enable');
                $('#cmb_designation').multipleSelect('enable');


                try {
                    $('#cmb_grade').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_section').multipleSelect('uncheckAll');
                }
                catch (e) {

                }

                try {
                    $('#cmb_dept').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_designation').multipleSelect('uncheckAll');
                } catch (e) {

                }


            }

            if ($scope.edt.sims_parent == 'true') {
                $scope.get_ischecked($scope.edt.sims_parent, $scope.edt.sims_grade_designation);
            }

            if ($scope.edt.sims_Email == 'true') {
                $scope.get_msgtypechecked($scope.edt.sims_Email);
            }

            /*start_attached Files*/

            var formdata = new FormData();
            $scope.images_data = [];
            $scope.images_att = [];

            $scope.getTheFiles = function ($files) {
                $scope.filesize = true;
                var i = 0, exists = "no";

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);

                    var i = 0;
                    if ($files[i].size > 5000000) {
                        $scope.filesize = false;
                        // $scope.edt.photoStatus = false;
                        swal({ title: "Alert", text: "File Should Not Exceed 5MB.", showCloseButton: true, width: 380, });
                    }
                    else {
                        if ($scope.images_data.length > 0) {
                            for (var j = 0 ; j < $scope.images_data.length; j++) {
                                if ($scope.images_data[j].attFilename == $files[i].name) {
                                    exists = "yes";
                                    return;
                                }
                            }
                            if (exists == "no") {
                                $scope.images_data.push({
                                    attFilename: $files[i].name,
                                    fsize: $files[i].size
                                });

                            }
                            exists = "no";
                        }
                        else {
                            $scope.images_data.push({
                                attFilename: $files[i].name,
                                fsize: $files[i].size

                            });
                        }
                        // $scope.$apply();
                    }
                    i++;
                });
            };

            $scope.file_changed = function (element, str) {
                var photofile = element.files[0];
                var v = photofile.name.indexOf('.');
                $scope.name = photofile.name.substr(0, v);
                $scope.photo_filename = (photofile.type);

                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;

                    });
                };
                reader.readAsDataURL(photofile);

                if ($scope.filesize == true) {
                    //+ '_' + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss')
                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + 'api/fileNew/uploadDocument?filename=' + $scope.name + "&location=" + "Docs/Attachments",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };

                    $http(request).success(function (d) {
                        // console.log(d);
                        //$scope.images_att.push(
                        //    {
                        //        att_Filename: d
                        //    });
                        //console.log($scope.images_att);

                    });
                }
            };

            $scope.uploadClick = function (str) {
                $scope.filesize = true;
                $scope.edt1 = str;
                $scope.ins = false;
                formdata = new FormData();
            }

            $scope.CancelFileUpload = function (idx) {
                // console.log(idx);
                $scope.images_data.splice(idx, 1);
                // console.log($scope.images_data.splice(idx));
                //formdata = new FormData();
            };

            /*end_attached Files*/

            $scope.search = function () {
                if (($scope.edt.sims_Email == "true" || $scope.edt.sims_Email == "false")) {
                    if ($scope.edt.sims_parent == "true") {
                        if ($scope.edt.sims_father_indi == false && $scope.edt.sims_mother_indi == false && $scope.edt.sims_guardian_indi == false) {
                            swal({ text: "Please Select Recipient", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                        }
                        else {
                            $rootScope.visible_stud = true;
                            $rootScope.visible_parent = true;
                            $rootScope.visible_search_parent = true;
                            $rootScope.visible_Employee = false;
                            $scope.getEmployee1();
                            // $scope.SearchParentModal();
                        }
                    }
                    else {
                        $rootScope.visible_stud = false;
                        $rootScope.visible_Employee = true;
                        $rootScope.visible_parent = false;
                        $rootScope.visible_search_parent = false;
                        $scope.getEmployee1();
                        // $scope.SearchEmpModal();
                    }
                }

            }

            $scope.SearchParentModal = function () {
                $scope.parenttable = false;
                $scope.buttons = false;
                $scope.busy = false;

                $scope.pp = '';
                $scope.parent_result = '';
                $('.nav-tabs a[href="#Parent_information"]').tab('show')
                $('#MyModal').modal('show');
            }

            $scope.SearchParent = function () {
                $scope.parenttable = false;
                // $scope.busy = true;
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchParent?data=" + JSON.stringify($scope.pp)).then(function (Allparent) {
                    $scope.parent_result = Allparent.data;
                    $scope.parenttable = true;
                    $scope.busy = false;
                });
                $scope.currentPage = 0;

            }

            $scope.SearchEmpModal = function () {
                $('.nav-tabs a[href="#emp_information"]').tab('show')
                $('#Emp_Search_Modal').modal('show');
            }

            $scope.SearchEmployee = function () {
                $scope.NodataEmployee = false;
                $http.post(ENV.apiUrl + "api/common/GlobalSearch/SearchEmployee?data=" + JSON.stringify($scope.temp)).then(function (SearchEmployee_Data) {
                    $scope.EmployeeDetails = SearchEmployee_Data.data;
                    // console.log($scope.EmployeeDetails);
                    $scope.EmployeeTable = true;
                });
            }

            $scope.get_checkedparent = function (info) {
                info.ischecked = true;
            }

            $scope.$on('global_cancel', function (str) {

                console.log($scope.SelectedUserLst);
                if ($scope.edt.sims_Email == "true" && $scope.edt.sims_parent == "true") {
                    $scope.getparentdata();
                }
                else if ($scope.edt.sims_Email == "true" && $scope.edt.sims_parent == "false") {
                    $scope.getEmployeedata();
                }
                else if ($scope.edt.sims_Email == "false" && $scope.edt.sims_parent == "true") {
                    $scope.getparentdata();
                }
                else if ($scope.edt.sims_Email == "false" && $scope.edt.sims_parent == "false") {
                    $scope.getEmployeedata();
                }

                try {
                    $('#cmb_gradeparent').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_sectionparent').multipleSelect('uncheckAll');
                }
                catch (e) {

                }


            });

            $scope.getEmployee1 = function () {
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
            }

            $scope.getparentdata = function () {

                $scope.parent_result = $scope.SelectedUserLst;

                console.log('ppp' + $scope.parent_result);
                $scope.ParentEmailIds = [];
                $scope.parentMobile1 = [];

                if ($scope.edt.sims_Email == "true") {
                    for (var i = 0; i < $scope.parent_result.length; i++) {
                        //if ($scope.parent_result[i].s_enroll_no1 == true)
                        if ($scope.parent_result[i].user_chk == true) {
                            if ($scope.edt.sims_father_indi == true) {
                                if ($scope.parent_result[i].mail_id != null && $scope.parent_result[i].mail_id != '') {
                                    // fcnt = fcnt + 1;
                                    $scope.getfather_indi.push({
                                        username: $scope.parent_result[i].s_parent_id,
                                        father_emailid: $scope.parent_result[i].mail_id,
                                        emailsendto: $scope.parent_result[i].mail_id,
                                        name: $scope.parent_result[i].s_parent_name,
                                        // name: $scope.parent_result[i].search_parent_first_name + ' ' + $scope.parent_result[i].search_parent_last_name,
                                        sender_emailid: $scope.getProfile,
                                        fflag: "1",
                                        enrollnumber: $scope.parent_result[i].s_enroll_no
                                    });

                                    $scope.parent_result[i].ischecked = false;
                                }
                            }
                            if ($scope.edt.sims_mother_indi == true) {

                                if ($scope.parent_result[i].mother_email_id != null && $scope.parent_result[i].mother_email_id != '') {
                                    // mcnt = mcnt + 1;
                                    $scope.getfather_indi.push({
                                        username: $scope.parent_result[i].s_parent_id,
                                        mother_emailid: $scope.parent_result[i].mother_email_id,
                                        emailsendto: $scope.parent_result[i].mother_email_id,
                                        enrollnumber: $scope.parent_result[i].s_enroll_no,
                                        name: $scope.parent_result[i].s_parent_name,
                                        //name: $scope.parent_result[i].search_parent_first_name + ' ' + $scope.parent_result[i].search_parent_last_name,
                                        sender_emailid: $scope.getProfile,
                                        mflag: "1"
                                    });

                                    $scope.parent_result[i].ischecked = false;

                                }
                            }
                            if ($scope.edt.sims_guardian_indi == true) {
                                if ($scope.parent_result[i].guardian_email_id != null && $scope.parent_result[i].guardian_email_id != '') {
                                    // gcnt = gcnt + 1;
                                    $scope.getfather_indi.push({
                                        username: $scope.parent_result[i].s_parent_id,
                                        guardian_emailid: $scope.parent_result[i].guardian_email_id,
                                        emailsendto: $scope.parent_result[i].guardian_email_id,
                                        enrollnumber: $scope.parent_result[i].s_enroll_no,
                                        name: $scope.parent_result[i].s_parent_name,
                                        // name: $scope.parent_result[i].search_parent_first_name + ' ' + $scope.parent_result[i].search_parent_last_name,
                                        sender_emailid: $scope.getProfile,
                                        gflag: "1"
                                    });

                                    $scope.parent_result[i].ischecked = false;
                                }
                            }
                        }
                    }

                    if ($scope.getfather_indi.length > 0) {
                        $scope.ParentEmailIds = $scope.getfather_indi;
                        $scope.EmailId_result = $scope.ParentEmailIds;
                        $scope.searchEmailId_result = $scope.EmailId_result;
                        console.log($scope.ParentEmailIds);

                        $scope.edt.existing_rec = $scope.getfather_indi.length;
                    }

                }
                else if ($scope.edt.sims_Email == "false") {

                    for (var i = 0; i < $scope.parent_result.length; i++) {
                        //if ($scope.parent_result[i].s_enroll_no1 == true)
                        if ($scope.parent_result[i].user_chk == true) {
                            if ($scope.edt.sims_father_indi == true) {
                                if ($scope.parent_result[i].mobile_no != null && $scope.parent_result[i].mobile_no != '') {
                                    // fcnt = fcnt + 1;
                                    $scope.getfather_indi.push({
                                        username: $scope.parent_result[i].s_parent_id,
                                        sms_father_contact_number1: $scope.parent_result[i].mobile_no,
                                        sms_smssendto: $scope.parent_result[i].mobile_no,
                                        sender_emailid: $scope.getProfile,
                                        name: $scope.parent_result[i].s_parent_name,
                                        //  name: $scope.parent_result[i].search_parent_first_name + ' ' + $scope.parent_result[i].search_parent_last_name,
                                        sms_father_flag: "1",
                                    });
                                    $scope.parent_result[i].ischecked = false;
                                }
                            }
                            if ($scope.edt.sims_mother_indi == true) {

                                if ($scope.parent_result[i].mother_mobile_no != null && $scope.parent_result[i].mother_mobile_no != '') {
                                    // mcnt = mcnt + 1;
                                    $scope.getfather_indi.push({
                                        username: $scope.parent_result[i].s_parent_id,
                                        sms_mother_contact_number2: $scope.parent_result[i].mother_mobile_no,
                                        sms_smssendto: $scope.parent_result[i].mother_mobile_no,
                                        sender_emailid: $scope.getProfile,
                                        name: $scope.parent_result[i].s_parent_name,
                                        // name: $scope.parent_result[i].search_parent_first_name + ' ' + $scope.parent_result[i].search_parent_last_name,
                                        sms_mother_flag: "1"

                                    });
                                    $scope.parent_result[i].ischecked = false;

                                }
                            }
                            if ($scope.edt.sims_guardian_indi == true) {
                                if ($scope.parent_result[i].guardian_mobile_no != null && $scope.parent_result[i].guardian_mobile_no != '') {
                                    $scope.getfather_indi.push({
                                        username: $scope.parent_result[i].s_parent_id,
                                        sms_guardian_contact_number3: $scope.parent_result[i].guardian_mobile_no,
                                        sms_smssendto: $scope.parent_result[i].guardian_mobile_no,
                                        sender_emailid: $scope.getProfile,
                                        name: $scope.parent_result[i].s_parent_name,
                                        // name: $scope.parent_result[i].search_parent_first_name + ' ' + $scope.parent_result[i].search_parent_last_name,
                                        sms_guardian_flag: "1"
                                    });
                                    $scope.parent_result[i].ischecked = false;
                                }
                            }
                        }
                    }


                    if ($scope.getfather_indi.length > 0) {
                        $scope.parentMobile1 = $scope.getfather_indi;
                        $scope.MobileNo_result = $scope.parentMobile1;
                        $scope.searchMobileNo_result = $scope.MobileNo_result;
                        console.log($scope.parentMobile1);

                        $scope.edt.existing_rec = $scope.getfather_indi.length;
                    }

                }
            }

            $scope.getEmployeedata = function () {
                $scope.EmployeeDetails = $scope.SelectedUserLst;
                $scope.EmpEmailIds = [];
                $scope.EmpMobile = [];
                //$scope.getEmp_indi = [];

                if ($scope.edt.sims_Email == "true") {
                    for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                        //if ($scope.EmployeeDetails[i].em_login_code1 == true)
                        if ($scope.EmployeeDetails[i].user_chk == true) {
                            if ($scope.EmployeeDetails[i].em_email != null && $scope.EmployeeDetails[i].em_email != '') {

                                $scope.getEmp_indi.push({
                                    username: $scope.EmployeeDetails[i].em_login_code,
                                    father_emailid: $scope.EmployeeDetails[i].em_email,
                                    emailsendto: $scope.EmployeeDetails[i].em_email,
                                    sender_emailid: $scope.getProfile,
                                    name: $scope.EmployeeDetails[i].empName,
                                    fflag: "1",
                                });
                            }
                        }
                    }

                    if ($scope.getEmp_indi.length > 0) {
                        //$scope.EmpEmailIds = $scope.getEmp_indi;
                        //$scope.EmailId_result = $scope.EmpEmailIds;
                        //$scope.edt.existing_rec = $scope.getEmp_indi.length;

                        $scope.empEmailIds = $scope.getEmp_indi;
                        $scope.EmailId_result = $scope.empEmailIds;
                        $scope.searchEmailId_result = $scope.EmailId_result;

                        $scope.edt.existing_rec = $scope.getEmp_indi.length;
                    }
                }
                else if ($scope.edt.sims_Email == "false") {

                    for (var i = 0; i < $scope.EmployeeDetails.length; i++) {
                        //if ($scope.EmployeeDetails[i].em_login_code1 == true)
                        if ($scope.EmployeeDetails[i].user_chk == true) {

                            if ($scope.EmployeeDetails[i].em_mobile != null && $scope.EmployeeDetails[i].em_mobile != '') {
                                $scope.getEmp_indi.push({
                                    username: $scope.EmployeeDetails[i].em_login_code,
                                    sms_father_contact_number1: $scope.EmployeeDetails[i].em_mobile,
                                    sms_smssendto: $scope.EmployeeDetails[i].em_mobile,
                                    name: $scope.EmployeeDetails[i].empName,
                                    sms_father_flag: "1",
                                });
                            }
                        }
                    }

                    if ($scope.getEmp_indi.length > 0) {
                        $scope.EmpMobile = $scope.getEmp_indi;
                        $scope.empMobContact = $scope.EmpMobile;
                        $scope.MobileNo_result = $scope.EmpMobile;
                        $scope.searchMobileNo_result = $scope.MobileNo_result;

                        $scope.edt.existing_rec = $scope.getEmp_indi.length;
                    }
                }
            }

            $scope.EmailIdDetails = function () {
                if ($scope.edt.sims_Email == 'true') {
                    $scope.tbl_email = true;
                    $scope.tbl_sms = false;
                }
                else {
                    $scope.tbl_sms = true;
                    $scope.tbl_email = false;
                }

                $('#EmailIdModal').modal('show');

            }

            $scope.cancelemailIddata = function (emailindex) {
                if ($scope.edt.sims_Email == "true" && $scope.edt.sims_parent == "true") {
                    //$scope.searchEmailId_result.splice(emailindex, 1);
                    $scope.EmailId_result.splice(emailindex, 1);

                    $scope.edt.existing_rec = $scope.EmailId_result.length;
                    // $scope.edt.existing_rec = $scope.searchEmailId_result.length;
                    $scope.ParentEmailIds = $scope.EmailId_result;
                    // $scope.searchEmailId_result = $scope.EmailId_result;
                }

                else if ($scope.edt.sims_Email == "true" && $scope.edt.sims_parent == "false") {
                    // $scope.searchEmailId_result.splice(emailindex, 1);
                    $scope.EmailId_result.splice(emailindex, 1);

                    $scope.edt.existing_rec = $scope.EmailId_result.length;
                    // $scope.edt.existing_rec = $scope.searchEmailId_result.length;
                    $scope.empEmailIds = $scope.EmailId_result;
                    //$scope.searchEmailId_result = $scope.EmailId_result;
                }
            }

            $scope.cancelmobiledata = function (mobileindex) {
                if ($scope.edt.sims_Email == "false" && $scope.edt.sims_parent == "true") {
                    $scope.MobileNo_result.splice(mobileindex, 1);

                    $scope.edt.existing_rec = $scope.MobileNo_result.length;
                    // $scope.edt.existing_rec = $scope.searchMobileNo_result.length;
                    $scope.parentMobile1 = $scope.MobileNo_result;
                    // $scope.searchMobileNo_result = $scope.MobileNo_result;
                }

                else if ($scope.edt.sims_Email == "false" && $scope.edt.sims_parent == "false") {
                    $scope.MobileNo_result.splice(mobileindex, 1);

                    $scope.edt.existing_rec = $scope.MobileNo_result.length;
                    // $scope.edt.existing_rec = $scope.searchMobileNo_result;
                    $scope.EmpMobile = $scope.MobileNo_result;
                    // $scope.searchMobileNo_result = $scope.MobileNo_result;
                }
            }

            $scope.send_mail_details = function () {
                /* EMAIL*/
                //if (isvalidate)
                //{
                var count_of_selected_data = $scope.edt.existing_rec;
                if (count_of_selected_data != 0) {
                    if ($scope.edt.sims_Email == "true") {
                        swal({ text: "Do You Want to Send Email?", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                $scope.sendMaildetails();
                            }
                        });
                    }
                    if ($scope.edt.sims_Email == "false") {
                        swal({
                            title: '',
                            text: "Do You Want to Send SMS?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $scope.OnsendSMSdetails();
                            }
                        });
                    }
                }
                else {
                    swal({ text: "Please select recepients Count is zero", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                }
                //}
            }

            $scope.sendMaildetails = function () {
                $scope.edt.body = document.getElementById('text-editor').value;
                console.log($scope.edt.sims_recepient_cc_id)
                var flag = true;
                /* EMAIL*/
                if ($scope.edt.sims_Email == "true") {
                    if (($scope.edt.sims_subject == "" || $scope.edt.sims_subject == undefined) && ($scope.edt.body == "" || $scope.edt.body == undefined)) {
                        flag = false;
                        swal({ text: "Subject & Message Body Is Required", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }

                    if (flag == true) {
                        if ($scope.edt.sims_parent == "true") {
                            if (!jQuery.isEmptyObject($scope.ParentEmailIds)) //($scope.ParentEmailIds.length > 0)
                            {
                                //$scope.BUSY = true;
                                for (var i = 0; i < $scope.ParentEmailIds.length; i++) {
                                    $scope.ParentEmailIds[i].subject = $scope.edt.sims_subject;
                                    $scope.ParentEmailIds[i].body = $scope.edt.body;
                                    $scope.ParentEmailIds[i].sender_emailid = $scope.getProfile;
                                    if (i == 0) {
                                        $scope.ParentEmailIds[i].sims_recepient_cc_id = $scope.edt.sims_recepient_cc_id;
                                    }
                                    else { $scope.ParentEmailIds[i].sims_recepient_cc_id = null; }
                                }
                                try {
                                    var data = $scope.ParentEmailIds;

                                    data[0].comn_email_attachments = $scope.images_data;
                                    //  console.log($scope.images_data);

                                    //data[0].comn_email_attachments = $scope.images_att;
                                    //console.log($scope.images_att);
                                    $http.post(ENV.apiUrl + "api/common/EmailSms/ScheduleMails", data).then(function (res) {
                                        $scope.display = true;
                                        $scope.msg1 = res.data;
                                        if ($scope.msg1 == true) {
                                            swal({ text: "Mail Sent Successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                                            $scope.Emailcancel();
                                        }
                                        else {
                                            swal({ text: "Mail Not Sent.", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                                            $scope.Emailcancel();
                                        }
                                    });
                                }
                                catch (ex) {
                                }
                            }
                            else {
                                swal({ text: "Please Specify Atleast One Recipient", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                            }
                        }

                        else if ($scope.edt.sims_parent == "false") {
                            if (!jQuery.isEmptyObject($scope.empEmailIds))//if ($scope.empEmailIds.length > 0)
                            {
                                //  $scope.BUSY = true;

                                for (var i = 0; i < $scope.empEmailIds.length; i++) {
                                    $scope.empEmailIds[i].subject = $scope.edt.sims_subject;
                                    $scope.empEmailIds[i].body = $scope.edt.body;
                                    $scope.empEmailIds[i].sender_emailid = $scope.getProfile;

                                    if (i == 0) {
                                        $scope.empEmailIds[i].sims_recepient_cc_id = $scope.edt.sims_recepient_cc_id;
                                    }
                                    else { $scope.empEmailIds[i].sims_recepient_cc_id = null; }
                                }
                                try {
                                    var data = $scope.empEmailIds;

                                    data[0].comn_email_attachments = $scope.images_data;
                                    // console.log($scope.images_data);

                                    $http.post(ENV.apiUrl + "api/common/EmailSms/ScheduleMails", data).then(function (res) {
                                        $scope.display = true;
                                        $scope.msg1 = res.data;
                                        if ($scope.msg1 == true) {
                                            swal({ text: "Mail Sent Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                                            $scope.Emailcancel();
                                        }
                                        else {
                                            swal({ text: "Mail Not Sent.", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                                            $scope.Emailcancel();
                                        }
                                    });
                                }
                                catch (ex) {
                                }
                            }
                            else {
                                swal({ text: "Please Specify Atleast One Recipient", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                            }
                        }
                    }
                }
            }

            $scope.cancel_mail_details = function () {
                $scope.Emailcancel();
            }

            $scope.OnsendSMSdetails = function () {

                if ($scope.edt.sims_Email == "false" && $scope.edt.sims_parent == "true") {
                    var smssendto = "";

                    if (!jQuery.isEmptyObject($scope.parentMobile1)) {
                        //$scope.BUSY = true;
                        for (var i = 0; i < $scope.parentMobile1.length; i++) {
                            $scope.parentMobile1[i].sims_smstext = $scope.edt.sims_smstext,
                            $scope.parentMobile1[i].usercode = $scope.username,
                            $scope.parentMobile1[i].smssendto = $scope.parentMobile1[i].sms_smssendto

                            if (i == 0) {
                                $scope.parentMobile1[i].sims_recepient_cc_id = $scope.edt.sims_recepient_cc_id;
                            }
                            else {
                                $scope.parentMobile1[i].sims_recepient_cc_id = null;
                            }

                            if ($scope.sims_arabic == 'Y') {
                                $scope.parentMobile1[i].arabicsms = true;
                            }
                            if ($scope.sims_arabic == 'N') {
                                $scope.parentMobile1[i].arabicsms = false;
                            }
                        }

                        try {
                            var data = $scope.parentMobile1;
                            console.log(data);
                            $http.post(ENV.apiUrl + "api/common/EmailSms/CUD_Insert_SMS_Schedule", data).then(function (res) {
                                $scope.display = true;
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "SMS sent successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                                    $scope.Emailcancel();
                                }
                                else {
                                    swal({ text: "SMS Not Sent", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                                    $scope.Emailcancel();
                                }
                            });
                        }
                        catch (ex) {
                        }
                    }
                    else {
                        swal({ text: "SMS Not Sent", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                    }
                    // }

                }
                else if ($scope.edt.sims_Email == "false" && $scope.edt.sims_parent == "false") {
                    if (!jQuery.isEmptyObject($scope.EmpMobile)) {
                        //$scope.BUSY = true;
                        for (var i = 0; i < $scope.EmpMobile.length; i++) {
                            $scope.EmpMobile[i].sims_smstext = $scope.edt.sims_smstext;
                            $scope.EmpMobile[i].usercode = $scope.username,
                            $scope.EmpMobile[i].smssendto = $scope.EmpMobile[i].sms_smssendto;

                            if (i == 0) {
                                $scope.EmpMobile[i].sims_recepient_cc_id = $scope.edt.sims_recepient_cc_id;
                            }
                            else {
                                $scope.EmpMobile[i].sims_recepient_cc_id = null;
                            }

                            if ($scope.sims_arabic == 'Y') {
                                $scope.EmpMobile[i].arabicsms = true;
                            }
                            if ($scope.sims_arabic == 'N') {
                                $scope.EmpMobile[i].arabicsms = false;
                            }
                        }

                        try {
                            var data = $scope.EmpMobile;
                            console.log(data);
                            $http.post(ENV.apiUrl + "api/common/EmailSms/CUD_Insert_SMS_Schedule", data).then(function (res) {
                                $scope.display = true;
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    $scope.Emailcancel();
                                    swal({ text: "SMS sent successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });

                                }
                                else {
                                    $scope.Emailcancel();
                                    swal({ text: "SMS Not Sent.", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });

                                }
                            });
                        }
                        catch (ex) {
                        }
                    }
                }
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search2 = function () {
                if ($scope.edt.sims_Email == "true") {
                    $scope.EmailId_result = $scope.searched($scope.EmailId_result, $scope.searchText2);
                    if ($scope.searchText2 != '') {
                        $scope.EmailId_result = $scope.EmailId_result;
                    }
                    else {
                        $scope.EmailId_result = $scope.searchEmailId_result;
                    }
                }
                else if ($scope.edt.sims_Email == "false") {
                    $scope.MobileNo_result = $scope.searched($scope.MobileNo_result, $scope.searchText2);
                    if ($scope.searchText2 != '') {
                        $scope.MobileNo_result = $scope.MobileNo_result;
                    }
                    else {
                        $scope.MobileNo_result = $scope.searchMobileNo_result;
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.username.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.GetArabicMsgFlag = function (str_arabicflag) {

                if (str_arabicflag == "false") {
                    $scope.sims_arabic = 'N';
                }
                else if (str_arabicflag == "true") {
                    $scope.sims_arabic = 'Y';
                }
            };

            $scope.Emailcancel = function () {
                $scope.edt = [];
                $('#text-editor').data("wysihtml5").editor.clear();
                $scope.images_data = [];
                $scope.edt.sims_Email = "true";
                $scope.chk_email = true;
                $scope.chk_sms = false;
                $scope.chk_rec = true;
                $scope.chk_rec1 = true;
                $scope.parentMobile1 = [];
                $scope.edt.existing_rec = 0;
                $scope.edt.sims_sms_char = "0";
                $scope.edt.sims_sms_length = "0";
                $scope.chk_emerg = false;
                $scope.div_recipient = true;
                $scope.EmailId_result = [];
                $scope.MobileNo_result = [];
                $scope.show_designation = false;
                //$scope.show_grade = true;
                $scope.BUSY = false;


                $('#cmb_grade,#cmb_section').multipleSelect('enable');


                try {
                    $('#cmb_grade').multipleSelect('uncheckAll');
                }
                catch (e) {

                }


                try {
                    $('#cmb_section').multipleSelect('uncheckAll');
                }
                catch (e) {

                }

                try {
                    $('#cmb_dept').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_designation').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_buscode').multipleSelect('uncheckAll');
                } catch (e) {

                } try {
                    $('#cmb_routeCode_name').multipleSelect('uncheckAll');
                } catch (e) {

                }


            }


        }])

    simsController.directive('ngFiles', ['$parse', function ($parse) {
        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return
        {
            link: fn_link
        }
    }])
})();