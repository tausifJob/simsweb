﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Collaboration');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CommunicationInitiatedDetailsReportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            // Start
            //$scope.name = "Subject Teacher List";
            $scope.temp = {};
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.temp = {};
            $scope.display = false;
            $scope.dublicate = false;

            

            $http.get(ENV.apiUrl + "api/communicationinitiateddetails/getUserCode").then(function (res1) {
                debugger
                $scope.user_code = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['sims_appl_parameter'] = res1.data[0].sims_appl_parameter;

                                  }
            });

          
            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.from_date = dd + '-' + mm + '-' + yyyy;
            $scope.to_date = dd + '-' + mm + '-' + yyyy;

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.from_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.from_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });


            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.to_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.to_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });


           
            $scope.communicationInitiatedDetails = function () {
                debugger
                              
                $http.get(ENV.apiUrl + "api/communicationinitiateddetails/getCommunicationDetailsReport?search=" + $scope.temp.search + "&user_code=" + $scope.temp.sims_appl_parameter + "&from_date=" + $scope.from_date + "&to_date=" + $scope.to_date).then(function (res1) {
                    if (res1.data.length > 0) {
                        $scope.report_data = res1.data;
                        angular.forEach($scope.report_data, function (f) {
                            f.student_cnt = parseFloat(f.student_cnt);
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Sorry!!!!! Data is not Available", showCloseButton: true, width: 300, height: 200 });
                        $scope.report_data = res1.data;
                    }
                    //angular.forEach($scope.report_data, function (f) {
                    //    f.sims_bell_lecture_per_week = parseFloat(f.sims_bell_lecture_per_week);
                    //});
                    console.log($scope.report_data);
                });
            }

            $scope.messagedetails = function (res2) {
                debugger
                $('#MyModal6').modal('show');
                $http.get(ENV.apiUrl + "api/communicationinitiateddetails/getmessagedetails?user_code=" + $scope.temp.sims_appl_parameter +"&sender_id=" + res2.sims_comm_sender_id + "&from_date=" + res2.sims_comm_tran_date + "&to_date=" + res2.sims_comm_tran_date +"&search=" + $scope.temp.search).then(function (res2) {
                    if (res2.data.length > 0) {
                        $scope.report_data1 = res2.data;
                       // $scope.totalItems = $scope.report_data1.length;
                       // $scope.todos = $scope.report_data1;
                       // $scope.makeTodos();

                        $scope.sender_id = $scope.report_data1[0].sims_comm_sender_id;
                        $scope.sender_name = $scope.report_data1[0].sender_name;
                        $scope.date = $scope.report_data1[0].sims_comm_tran_date;
                        $scope.messagecount = $scope.report_data1[0].no_msg_sent;

                       

                    }
                    else {
                        swal({ title: "Alert", text: "Sorry!!!! Data is not Available", showCloseButton: true, width: 300, height: 200 });
                        $scope.report_data1 = res2.data;
                    }
                    //angular.forEach($scope.report_data, function (f) {
                    //    f.sims_bell_lecture_per_week = parseFloat(f.sims_bell_lecture_per_week);
                    //});
                    console.log($scope.report_data1);
                });
            }


         

           

            var data_send = [];
            var data1 = [];
            
            //$scope.makeTodos = function () {
            //    var rem = parseInt($scope.totalItems % $scope.numPerPage);
            //    if (rem == '0') {
            //        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
            //    }
            //    else {
            //        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
            //    }

            //    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
            //    var end = parseInt(begin) + parseInt($scope.numPerPage);

            //    console.log("begin=" + begin); console.log("end=" + end);

            //    $scope.filteredTodos = $scope.todos.slice(begin, end);
            //};

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //$scope.search = function () {
            //    debugger;
            //    $scope.todos = $scope.searched($scope.report_data1, $scope.searchText);
            //    $scope.totalItems = $scope.todos.length;
            //    $scope.currentPage = '1';
            //    if ($scope.searchText == '') {
            //        $scope.todos = $scope.report_data1;
            //    }
            //    $scope.makeTodos();
            //   // main.checked = false;
            //    //$scope.CheckAllChecked();
            //}

            //function searchUtil(item, toSearch) {
            //    /* Search Text in all 3 fields */
            //    return (item.sims_comm_recepient_id.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
            //        item.sims_comm_message.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
            //        item.reciver_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;

            //}

          

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }





            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            //$scope.colsvis = false;


            

        }])

})();

