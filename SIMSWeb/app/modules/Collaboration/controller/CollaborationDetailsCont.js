﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CollaborationDetailsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.cdetails_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.edit_code = false;
            $scope.templategrmyframeid = false;
            $scope.email_tempsrno = ''
            $scope.sms_tempsrno = ''
            $scope.alert_tempsrno = ''
            $scope.edit_srno = ''

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);           

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5, $scope.obj_temp = [], $scope.obj_temp_sms = [], $scope.obj_temp_alert = [];

            $http.get(ENV.apiUrl + "api/common/CollaborationDetails/getModules").then(function (res) {
                $scope.obj_module = res.data;
               });

            $scope.GetApplication = function (modecode) {
                $http.get(ENV.apiUrl + "api/common/CollaborationDetails/getApplications?modcode="+modecode).then(function (res) {
                    $scope.obj_appl = res.data;
                   });
            }

            $http.get(ENV.apiUrl + "api/common/CollaborationDetails/getSMTPIds").then(function (res) {
                $scope.obj_smtp = res.data;
               
            });           

            $http.get(ENV.apiUrl + "api/common/CollaborationDetails/getCollaborationdetails").then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.cdetails_data = res.data;
                $scope.totalItems = $scope.cdetails_data.length;
                $scope.todos = $scope.cdetails_data;
                $scope.makeTodos();
                $scope.grid = true;
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        v.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }
            };
            
            $scope.GetMsgTemplates_Email = function (modecode)
            {
                if (modecode != undefined) {
                    
                   
                    $http.get(ENV.apiUrl + "api/common/CollaborationDetails/getEmailSMSAlertTemplates?modcode=" + modecode + "&msgtype=E").then(function (res1) {

                        $scope.obj_temp = res1.data;

                       
                        //$scope.totalItems1 = $scope.obj_temp.length;
                        //var rem = parseInt($scope.totalItems1 % $scope.numPerPage);
                        //if (rem == '0') {
                        //    $scope.pagersize = parseInt($scope.totalItems1 / $scope.numPerPage);
                        //}
                        //else {
                        //    $scope.pagersize = parseInt($scope.totalItems1 / $scope.numPerPage) + 1;
                        //}
                        //var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                        //var end = parseInt(begin) + parseInt($scope.numPerPage);

                        $scope.filteredTodos1 = $scope.obj_temp;

                    });

                    $scope.templategrid = true;

                    $scope.templategrid_sms = false;

                    $scope.templategrid_alert = false;
                }
                else {
                    
                    swal({ text: "Please Select Module.", imageUrl: "assets/img/check.png",showCloseButton: true, width: 380, });
                }
            }

            $scope.GetMsgTemplates_SMS = function (modecode) {
                if (modecode != undefined ) {
                   $http.get(ENV.apiUrl + "api/common/CollaborationDetails/getEmailSMSAlertTemplates?modcode=" + modecode + "&msgtype=S").then(function (res) {
                    $scope.obj_temp_sms = res.data;
                   
                    //$scope.totalItems2 = $scope.obj_temp_sms.length;
                    //var rem = parseInt($scope.totalItems2 % $scope.numPerPage);
                    //if (rem == '0') {
                    //    $scope.pagersize = parseInt($scope.totalItems2 / $scope.numPerPage);
                    //}
                    //else {
                    //    $scope.pagersize = parseInt($scope.totalItems2 / $scope.numPerPage) + 1;
                    //}
                    //var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    //var end = parseInt(begin) + parseInt($scope.numPerPage);

                  //  $scope.filteredTodos_sms = $scope.obj_temp_sms.slice(begin, end);
                    $scope.filteredTodos_sms = $scope.obj_temp_sms;
                });

                $scope.templategrid = false;
                
                $scope.templategrid_sms = true;
                
                $scope.templategrid_alert = false;
                }
                else {
                    swal({  text: "Please Select Module.",imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
            }

            $scope.GetMsgTemplates_Alert = function (modecode) {
                if (modecode != undefined) {
                   $http.get(ENV.apiUrl + "api/common/CollaborationDetails/getEmailSMSAlertTemplates?modcode=" + modecode + "&msgtype=A").then(function (res) {
                    $scope.obj_temp_alert = res.data;
                    
                    //$scope.totalItems3 = $scope.obj_temp_alert.length;
                    //var rem = parseInt($scope.totalItems3 % $scope.numPerPage);
                    //if (rem == '0') {
                    //    $scope.pagersize = parseInt($scope.totalItems3 / $scope.numPerPage);
                    //}
                    //else {
                    //    $scope.pagersize = parseInt($scope.totalItems3 / $scope.numPerPage) + 1;
                    //}
                    //var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    //var end = parseInt(begin) + parseInt($scope.numPerPage);

                    //$scope.filteredTodos_alert = $scope.obj_temp_alert.slice(begin, end);
                    $scope.filteredTodos_alert = $scope.obj_temp_alert;
                });

                $scope.templategrid = false;

                $scope.templategrid_sms = false;

                $scope.templategrid_alert = true;
                }
                else {
                    swal({  text: "Please Select Module.", imageUrl: "assets/img/notification-alert.png",showCloseButton: true, width: 380, });
                }
            }

            $scope.edit = function (str) {
               
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edit_code = true;
                var email_recipients=''
                var sms_recipients = ''

                $scope.templategrid = false;
                $scope.templategrid_sms = false;
                $scope.templategrid_alert = false;

              
                $scope.edt =
                        {
                            appl_srno: str.appl_srno,
                            modcode: str.modcode,
                            applcode: str.applcode,
                            smtpID: str.smtpID,
                            form_control: str.form_control,
                            ifemail: str.ifemail,
                            email_template: str.email_temp_srno,
                            ifsms: str.ifsms,
                            sms_template: str.sms_template_srno,
                            ifalert: str.ifalert,
                            alert_template: str.alert_template_srno,
                            ifmalert: str.ifmalert,
                            ifemailfather:str.ifemailfather,
                            ifemailmother:str.ifemailmother,
                            ifemailguardian: str.ifemailguardian,
                            ifemailactive: str.ifemailactive,
                            ifsmsfather:str.ifemailfather,
                            ifsmsmother:str.ifemailmother,
                            ifsmsguardian: str.ifemailguardian,
                            ifsmsactive: str.ifemailactive,
                            email_temp_srno : str.email_temp_srno,
                            sms_temp_srno: str.sms_temp_srno,
                            alert_temp_srno: str.alert_temp_srno
                    }
                var e = $scope.edt.email_temp_srno;
                var s = $scope.edt.sms_temp_srno;
                var a = $scope.edt.alert_temp_srno;
                $scope.email_tempsrno = $scope.edt.email_temp_srno;
                $scope.sms_tempsrno = $scope.edt.sms_temp_srno;
                $scope.alert_tempsrno = $scope.edt.alert_temp_srno;
                $scope.edit_srno = $scope.edt.appl_srno;
                $scope.GetApplication($scope.edt.modcode);

                var v = document.getElementById('Application');
                v.disabled = true;
                var v1 = document.getElementById('Module');
                v1.disabled = true;
            }

            $scope.Save = function (isvalidate) {
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        
                        var fctrl = 'A';
                       // $http.post(ENV.apiUrl + "api/common/CollaborationDetails/CheckExistingCollaborationdetails?modcode" + $scope.edt.modcode + "&appcode=" + $scope.edt.applcode + "&formcontrol='A'" + $scope.edt.form_control).then(function (res) {
                        $http.get(ENV.apiUrl + "api/common/CollaborationDetails/GetExistingCollaborationdetails?modcode=" + $scope.edt.modcode + "&appcode=" + $scope.edt.applcode + "&formcontrol=" + fctrl).then(function (res) {
                            $scope.msg1 = res.data;
                            if ($scope.msg1.status == true) {
                                swal({ text: "Collaboration Details Already Exists", imageUrl: "assets/img/notification-alert.png",showCloseButton: true, width: 380, });
                                $scope.edt = "";
                            }
                            else {

                                var emailrec = ''
                                var smsrec = ''
                               
                                var iffather_email = $scope.edt.ifemailfather
                                var ifmother_email = $scope.edt.ifemailmother
                                var ifguardian_email = $scope.edt.ifemailguardian
                                var ifactive_email = $scope.edt.ifemailactive

                                var iffather_sms = $scope.edt.ifsmsfather
                                var ifmother_sms = $scope.edt.ifsmsmother
                                var ifguardian_sms = $scope.edt.ifsmsguardian
                                var ifactive_sms = $scope.edt.ifsmsactive

                                if (iffather_email == true)
                                    emailrec = emailrec + 'F';
                                if (ifmother_email == true)
                                    emailrec = emailrec + 'M';
                                if (ifguardian_email == true)
                                    emailrec = emailrec + 'G';
                                if (ifactive_email == true)
                                    emailrec = emailrec + 'A';

                                if (iffather_sms == true)
                                    smsrec = smsrec + 'F';
                                if (ifmother_sms == true)
                                    smsrec = smsrec + 'M';
                                if (ifguardian_sms == true)
                                    smsrec = smsrec + 'G';
                                if (ifactive_sms == true)
                                    smsrec = smsrec + 'A';

                                var ifsave = true;
                                
                                var em_chk = document.getElementById('em_chk');
                                if (em_chk.checked == true) {
                                    if (emailrec == '' || emailrec == undefined) {
                                        ifsave = false;
                                        swal({ text: "Please Select at least one Email receipient.", imageUrl: "assets/img/notification-alert.png",showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        });                                    
                                    }
                                }

                                var sms_chk = document.getElementById('sms_chk');
                                if (sms_chk.checked == true) {
                                    if (smsrec == '' || smsrec == undefined) {
                                        ifsave = false;
                                        swal({ text: "Please Select at least one SMS receipient.", imageUrl: "assets/img/notification-alert.png",showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        });
                                    }
                                }

                                
                                if (ifsave==true) {
                                    var data = ({
                                        opr: 'I',
                                        modcode: $scope.edt.modcode,
                                        applcode: $scope.edt.applcode,
                                        form_control: $scope.edt.form_control,
                                        SmtpID: $scope.edt.smtpID,
                                        ifemail: $scope.edt.ifemail,
                                        email_temp_srno: $scope.email_tempsrno,//$scope.email_tempsrno,
                                        email_recipients: emailrec,
                                        ifsms: $scope.edt.ifsms,
                                        sms_temp_srno: $scope.sms_tempsrno,
                                        sms_recipients: smsrec,
                                        ifalert: $scope.edt.ifalert,
                                        alert_temp_srno: $scope.alert_tempsrno,
                                        ifstatus: 'A',
                                        ifmalert: $scope.edt.ifmalert,
                                        appl_srno: $scope.edit_srno
                                    });

                                    data1.push(data);

                                    $http.post(ENV.apiUrl + "api/common/CollaborationDetails/CUDCollaborationdetails", data1).then(function (res) {
                                        $scope.display = true;
                                        $scope.msg1 = res.data;
                                        if ($scope.msg1 == true) {
                                            swal({  text: "Collaboration Details Added Successfully",imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                                if (isConfirm) {
                                                    $scope.getgrid();
                                                }
                                            });
                                        }
                                        else if ($scope.msg1 == false) {
                                            swal({ text: "Collaboration Details Not Added Successfully. " ,imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                                if (isConfirm) {
                                                    $scope.getgrid();
                                                }
                                            });
                                        }
                                        else {
                                            swal("Error-" + $scope.msg1)
                                        }
                                    });



                                }
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }

                }
            }

            $scope.savedetails = function () {
                var data = ({
                    opr: 'I',
                    modcode: $scope.edt.modcode,
                    applcode: $scope.edt.applcode,
                    form_control: $scope.edt.form_control,
                    SmtpID: $scope.edt.smtpID,
                    ifemail: $scope.edt.ifemail,
                    email_temp_srno: $scope.email_tempsrno,//$scope.email_tempsrno,
                    email_recipients: emailrec,
                    ifsms: $scope.edt.ifsms,
                    sms_temp_srno: $scope.sms_tempsrno,
                    sms_recipients: smsrec,
                    ifalert: $scope.edt.ifalert,
                    alert_temp_srno: $scope.alert_tempsrno,
                    ifstatus: 'A',
                    ifmalert: $scope.edt.ifmalert,
                    appl_srno: $scope.edit_srno
                });

                data1.push(data);

                $http.post(ENV.apiUrl + "api/common/CollaborationDetails/CUDCollaborationdetails", data1).then(function (res) {
                    $scope.display = true;
                    $scope.msg1 = res.data;
                    if ($scope.msg1 == true) {
                        swal({  text: "Collaboration Details Added Successfully",imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                $scope.getgrid();
                            }
                        });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Collaboration Details Not Added Successfully. ",imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                $scope.getgrid();
                            }
                        });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                });

            }

            $scope.getgrid = function () {
                $scope.currentPage = 1;
                $http.get(ENV.apiUrl + "api/common/CollaborationDetails/getCollaborationdetails").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.cdetails_data = res.data;
                    $scope.totalItems = $scope.cdetails_data.length;
                    $scope.todos = $scope.cdetails_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                });

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {
                var data1 = [];

                var emailrec = ''
                var smsrec = ''
                
                var iffather_email = $scope.edt.ifemailfather
                var ifmother_email = $scope.edt.ifemailmother
                var ifguardian_email = $scope.edt.ifemailguardian
                var ifactive_email = $scope.edt.ifemailactive

                var iffather_sms = $scope.edt.ifsmsfather
                var ifmother_sms = $scope.edt.ifsmsmother
                var ifguardian_sms = $scope.edt.ifsmsguardian
                var ifactive_sms = $scope.edt.ifsmsactive

                if (iffather_email == true)
                    emailrec = emailrec + 'F';
                if (ifmother_email == true)
                    emailrec = emailrec + 'M';
                if (ifguardian_email == true)
                    emailrec = emailrec + 'G';
                if (ifactive_email == true)
                    emailrec = emailrec + 'A';

                if (iffather_sms == true)
                    smsrec = smsrec + 'F';
                if (ifmother_sms == true)
                    smsrec = smsrec + 'M';
                if (ifguardian_sms == true)
                    smsrec = smsrec + 'G';
                if (ifactive_sms == true)
                    smsrec = smsrec + 'A';

                if (isvalidate) {
                    var data = ({
                        modcode: $scope.edt.modcode,
                        applcode: $scope.edt.applcode,
                        form_control: $scope.edt.form_control,
                        smtpID: $scope.edt.smtpID,
                        ifemail:$scope.edt.ifemail,
                        email_temp_srno: $scope.email_tempsrno,//$scope.email_tempsrno,
                        email_recipients: emailrec,
                        ifsms:$scope.edt.ifsms,
                        sms_temp_srno: $scope.sms_tempsrno,
                        sms_recipients:smsrec,
                        ifalert:$scope.edt.ifalert,
                        alert_temp_srno: $scope.alert_tempsrno,
                        ifstatus:'A',
                        ifmalert:$scope.edt.ifmalert,
                        appl_srno : $scope.edit_srno,
                        opr: 'U'
                    });

                    data1.push(data);
                    
                    var dd=
                    $http.post(ENV.apiUrl + "api/common/CollaborationDetails/CUDCollaborationdetails", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Collaboration Details Updated Successfully.",imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Collaboration Details  Not Updated Successfully. " , imageUrl: "assets/img/close.png",showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {

                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            } 

            $scope.Delete = function () {
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'appl_srno': $scope.filteredTodos[i].appl_srno,
                            'opr': 'K'
                        });
                        deletecode.push(deletemodercode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/common/CollaborationDetails/CUDCollaborationdetails", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    
                                    swal({ text: "Collaboration Details Deleted Successfully.",imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {

                                            $scope.getgrid();
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({  text: "Collaboration Details Cannot be Deleted. ", imageUrl: "assets/img/close.png",showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                            else{
                                swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }

            }

            $scope.New = function () {
                $scope.edit_code = false;
                var autoid;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = [];
               
                $scope.filteredTodos_alert = ''
                $scope.filteredTodos_sms = ''
                $scope.filteredTodos1 = ''
                $scope.templategrid = false;
                $scope.templategrid_sms = false;
                $scope.templategrid_alert = false;

                var v = document.getElementById('Application');
                v.disabled = false;
                var v1 = document.getElementById('Module');
                v1.disabled = false;

            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;  $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.cdetails_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.cdetails_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 2 fields */
                return (item.application_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.form_control.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $('#text-editor').wysihtml5();

            $scope.getCommunicate_body = function (emailbody) {
                
                
                $('#text-editor').data("wysihtml5").editor.clear();
                $('#commnModal').modal({ backdrop: 'static', keyboard: true });               
                $('#text-editor').data("wysihtml5").editor.setValue(emailbody);
            }

            $scope.getCommunicate_sig = function (emailsig) {

               
                $('#text-editor').data("wysihtml5").editor.clear();
                $('#commnModal').modal({ backdrop: 'static', keyboard: true });
                $('#text-editor').data("wysihtml5").editor.setValue(emailsig);
            }

            $scope.getSerialEmail = function (email_tempsrno) {

                $scope.email_tempsrno = email_tempsrno;
            }

            $scope.getSerialSMS = function (sms_tempsrno) {

                $scope.sms_tempsrno = sms_tempsrno;
            }

            $scope.getSerialalert = function (alert_tempsrno) {

                $scope.alert_tempsrno = alert_tempsrno;
            }

            $scope.ClickSMS = function () {
                
                var smschk = document.getElementById('active_smschk');
                if (smschk.checked == true) {
                    
                    $scope.edt.ifsmsfather = false;
                    $scope.edt.ifsmsmother = false;
                    $scope.edt.ifsmsguardian = false;
                    //fchk = document.getElementById('father_smschk');
                    //fchk.checked = false;
                    //mchk = document.getElementById('mother_smschk');
                    //mchk.checked = false;
                    //gchk = document.getElementById('guardian_smschk');
                    //gchk.checked = false;
                }
            }

            $scope.ClickEmail = function () {

                var echk = document.getElementById('active_echk');
                if (echk.checked == true) {

                    $scope.edt.ifemailfather = false;
                    $scope.edt.ifemailmother = false;
                    $scope.edt.ifemailguardian = false;
                }
            }

            $scope.ClickMSMS = function () {
                
                var mchk = document.getElementById('mother_smschk');
                if (mchk.checked == true) {
                    //var fchk = document.getElementById('father_schk');
                    //if (fchk.checked == true) {
                        var gchk = document.getElementById('guardian_smschk');
                        if (gchk.checked == true) {
                            $scope.edt.ifsmsguardian = false;
                        }
                    //}
                }

                var achk = document.getElementById('active_smschk');
                var fchk = document.getElementById('mother_smschk');
                if (fchk.checked == true) {
                    if (achk.checked == true) {
                        $scope.edt.ifsmsactive = false;
                    }
                }
            }

            $scope.ClickGSMS = function () {

                var mchk = document.getElementById('guardian_smschk');
                if (mchk.checked == true) {

                    //var fchk = document.getElementById('father_schk');
                    //if (fchk.checked == true) {
                        var gchk = document.getElementById('mother_smschk');
                        if (gchk.checked == true) {
                            $scope.edt.ifsmsmother = false;
                        }
                    //}
                }

                var fchk = document.getElementById('guardian_smschk');
                if (fchk.checked == true) {
                    var achk = document.getElementById('active_smschk');
                    if (achk.checked == true) {
                        $scope.edt.ifsmsactive = false;
                    }
                }
            }

            $scope.ClickFSMS = function () {               

                var fchk = document.getElementById('father_schk');
                if (fchk.checked == true) {
                    var achk = document.getElementById('active_smschk');
                    if (achk.checked == true) {
                        $scope.edt.ifsmsactive = false;
                    }
                }
            }

            //Email related

            $scope.ClickMEmail = function () {
                debugger
                var mchk = document.getElementById('mother_echk');
                if (mchk.checked == true) {
                    //var fchk = document.getElementById('father_echk');
                    //if (fchk.checked == true) {
                        var gchk = document.getElementById('guardian_echk');
                        if (gchk.checked == true) {
                            $scope.edt.ifemailguardian = false;
                        }
                    //}
                }

                var achk = document.getElementById('active_echk');
                var fchk = document.getElementById('mother_echk');
                if (fchk.checked == true) {
                    if (achk.checked == true) {
                        $scope.edt.ifemailactive = false;
                    }
                }
            }

            $scope.ClickGEmail = function () {

                var mchk = document.getElementById('guardian_echk');
                if (mchk.checked == true) {

                    //var fchk = document.getElementById('father_echk');
                    //if (fchk.checked == true) {
                        var gchk = document.getElementById('mother_echk');
                        if (gchk.checked == true) {
                            $scope.edt.ifemailmother = false;
                        }
                    //}
                }

                var fchk = document.getElementById('guardian_echk');
                if (fchk.checked == true) {
                    var achk = document.getElementById('active_echk');
                    if (achk.checked == true) {
                        $scope.edt.ifemailactive = false;
                    }
                }
            }

            $scope.ClickFEmail = function () {

                var fchk = document.getElementById('father_echk');
                if (fchk.checked == true) {
                    var achk = document.getElementById('active_echk');
                    if (achk.checked == true) {
                        $scope.edt.ifemailactive = false;
                    }
                }
            }

            $scope.ClickIFEmail = function () {

                var em_chk = document.getElementById('em_chk');
                if (em_chk.checked == true) {
                    $scope.edt.ifemailactive = true;
                }
            }

            $scope.ClickIFSMS = function () {

                var sms_chk = document.getElementById('sms_chk');
                if (sms_chk.checked == true) {
                    $scope.edt.ifsmsactive = true;
                }
            }

        }])
})();