﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Collaboration');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CircularCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'];
            $scope.itemsPerPage = '10';
            $scope.currentPage = 0;
            $scope.view = true;
            $scope.create = false;
            $scope.pageshow = true;

            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            $scope.cdetails_data = [];
            var download = "false";
            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);
            $scope.formData = {};
            //$scope.rem = {circular_remark:''};

            /// alert("In CircularCont::"+$scope.circularobj.userCircularCount);
            // $scope.circularobj.userCircularCount = 44;
            //$scope.circularobj = { userCircularCount: '44' }

            //alert("$scope.circularobj.userCircularCount::" + $scope.circularobj.userCircularCount);
            // document.getElementById('mainform.msgs-badge').textContent = $scope.circularobj.userCircularCount;

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            var date = new Date();
            //$scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');

            debugger;
            //$scope.startDate = $scope.ddMMyyyy;
            //var date = $scope.ddMMyyyy;
            //var month = date.split("-")[1];
            //var day = date.split("-")[0];
            //var year = date.split("-")[2];
            //var date1 = year + "-" + month + "-" + "01";
            //$scope.startDate = date1;
            $scope.endDate = $scope.ddMMyyyy;

            $scope.startDate = $filter('date')(new Date(), '01-MM-yyyy');

            $scope.setStart = function (date) {
                debugger;
                var month = date.split("/")[1];
                var day = date.split("/")[0];
                var year = date.split("/")[2];
                var date1 = '01' + "/" + month + "/" + year;
                $scope.sdate = date1;
                $scope.startDate = $scope.sdate;
            }
          //  $scope.setStart($scope.ddMMyyyy);
            $scope.setEnd = function (date) {
                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];
                var date1 = year + "-" + month + "-" + day;
                $scope.edate = date1;
            }

            $http.get(ENV.apiUrl + "api/common/Circular/GetUserCirculars?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.create = false;
                $scope.view = true;
                $scope.obj = res.data;
               

                $scope.cdetails_data = res.data;
                $scope.totalItems = $scope.cdetails_data.length;
                $scope.todos = $scope.cdetails_data;
                $scope.makeTodos();

            });

            $scope.cancel = function () {
                $scope.view = true;
                $scope.create = false;
            }

            $scope.size = function (str) {
               
                if (str == "All") {
                    $scope.pageshow = false;
                    $scope.currentPage = 1;
                    $scope.numPerPage = $scope.obj.length;
                    $scope.makeTodos();

                }
                else {
                    $scope.pageshow = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.cdetails_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.cdetails_data;
                }
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            function searchUtil(item, toSearch) {
                /* Search Text in all 2 fields */
                return (item.sims_circular_title.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_circular_short_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            //File Download
            $scope.download = function (str) {
               // $scope.download = "true";
                $('#commnModal').modal('hide');
                
                window.open($scope.url + "/Images/CircularFiles/" + str, "_new");
               // $scope.download = "false";
            };

            //Search
            $scope.search_click = function (str) {
                debugger;

                $scope.sdate = $scope.startDate;
                $scope.edate = $scope.endDate;

                $http.get(ENV.apiUrl + "api/common/Circular/GetUserCircularsBYDate?username=" + $rootScope.globals.currentUser.username + "&fromDate=" + $scope.sdate + "&toDate=" + $scope.edate).then(function (res) {
                    $scope.create = false;
                    $scope.view = true;
                    $scope.obj = res.data;
                    
                    $scope.cdetails_data = res.data;
                    $scope.totalItems = $scope.cdetails_data.length;
                    $scope.todos = $scope.cdetails_data;
                    $scope.makeTodos();
                });
            };
            $scope.reset = function () {

                $http.get(ENV.apiUrl + "api/common/Circular/GetUserCirculars?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.create = false;
                    $scope.view = true;
                    $scope.obj = res.data;
                   
                    $scope.cdetails_data = res.data;
                    $scope.totalItems = $scope.cdetails_data.length;
                    $scope.todos = $scope.cdetails_data;
                    $scope.makeTodos();

                });

                $scope.startDate = $scope.ddMMyyyy;
                $scope.endDate = $scope.ddMMyyyy;
                $scope.searchText = '';
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $('#text-editor').wysihtml5();

            $scope.edt1 = {
                ctitle: '',
                cdesc: '',
                cdate1: '',
                cnumber1: '',
                remark: ''
            }

            $scope.UpdateCStatus = function (title, desc, cdate, cnumber, remark1) {
                debugger;
                //alert("IN UpdateCStatus::circular_number::" + cnum + "$rootScope.globals.currentUser.username::" + $rootScope.globals.currentUser.username);
                $scope.ctitle = title;
                $scope.cdesc = desc;
                $scope.cdate1 = cdate;
                $scope.cnumber1 = cnumber;
                $scope.remark = remark1;
                document.getElementById('remark').value = remark1;
                $scope.ctitle = title + ' (' + $scope.cdate1 + ')';
                $('#commnModal').modal({ backdrop: 'static', keyboard: true });

                $('#text-editor').data("wysihtml5").editor.setValue($scope.cdesc);
                $('#text-editor').data('wysihtml5').editor.composer.disable();
                $('#text-editor').data('wysihtml5').editor.toolbar.hide();
            }

            $scope.acknowledgecircular = function (cnum) {
                debugger;
                var remk = document.getElementById('remark').value;
                console.log($scope.formData.membershipNo);
                if (remk != undefined && remk != "" && remk != '') {
                    $http.post(ENV.apiUrl + "api/common/Circular/UpdateCircularAsRead?circular_number=" + cnum + "&loggeduser=" + $rootScope.globals.currentUser.username + "&remark=" + remk).then(function (res) {
                        $scope.view = true;
                        $scope.create = false;
                        $scope.msg1 = res.data;
                        $rootScope.strMessage = $scope.msg1.strMessage;
                       
                        $('#commnModal').modal('hide');
                        $('#text-editor').data("wysihtml5").editor.clear();
                        $http.get(ENV.apiUrl + "api/common/Circular/GetUserCirculars?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                            $scope.create = false;
                            $scope.view = true;
                            $scope.obj = res.data;
                            
                            $scope.cdetails_data = res.data;
                            $scope.totalItems = $scope.cdetails_data.length;
                            $scope.todos = $scope.cdetails_data;
                            $scope.makeTodos();
                        });
                    });
                }
            }
        }])
})();