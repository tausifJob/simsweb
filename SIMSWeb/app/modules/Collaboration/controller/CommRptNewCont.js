﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, modulecode = [];
    var simsController = angular.module('sims.module.Collaboration');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CommRptNewCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var active = 'A';
            var inactive = '';

            $scope.showdisabled = false;

            $timeout(function () {
                $("#example").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.active_stud = true;
            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.mom_end_date_check = true;
            $scope.mom_start_date_check = true;
            $scope.temp = {};
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.mom_start_date = '';
            $scope.mom_end_date = '';

            $(function () {
                $('#bell_box').multipleSelect({ width: '100%' });
                $('#teacher_box').multipleSelect({ width: '100%', filter: true });

            });

            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_start_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });
            if ($scope.mom_start_date_check == true) {
                $scope.mom_start_date = '';
            }
            else {
                $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
            }

            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_end_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });
            if ($scope.mom_end_date_check == true) {
                $scope.mom_end_date = '';
            }
            else {
                $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
            }

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            }
            $scope.exportData = function () {
                //var blob = new Blob([document.getElementById('exportable').innerHTML], {
                //    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                //});
                //saveAs(blob, "Report.xls");


                var check = true;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {//exportable
                            //var blob = new Blob([document.getElementById('exportable').innerHTML], {
                            //    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            //});
                            //saveAs(blob, "Report.xls");

                            alasql('SELECT * INTO XLSX("Report.xlsx",{headers:true}) \ FROM HTML("#example",{headers:true,skipdisplaynone:true})');

                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Report Not Save", showCloseButton: true, width: 380, })
                }
            };

            $scope.items = [{
                "Name": "ANC101",
                "Date": "10/02/2014",
                "Terms": ["samsung", "nokia", "apple"],
                "temp": "ANC101"

            }, {
                "Name": "ABC102",
                "Date": "10/02/2014",
                "Terms": ["motrolla", "nokia", "iPhone"],
                "temp": "ANC102"
            }]

            $scope.busyindicator = false;
            $scope.ShowCheckBoxes = true;

            //$http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
            //    $scope.curriculum = AllCurr.data;
            //    $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
            //    $scope.getAccYear($scope.edt.sims_cur_code);
            //});


            function getCur(flag, comp_code) {
                if (flag) {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                        debugger;
                        $scope.curriculum = res.data;
                        $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                        $scope.getAccYear($scope.edt.sims_cur_code);

                    });
                }

                else {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                        debugger;
                        $scope.curriculum = res.data;
                        $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                        $scope.getAccYear($scope.edt.sims_cur_code);

                    });
                }

            }

            $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                debugger;
                $scope.global_count_comp = res.data;
                debugger;
                if ($scope.global_count_comp) {
                    getCur(true, $scope.user_details.comp);


                }
                else {
                    getCur(false, $scope.user_details.comp)
                }
            });

            $scope.getAccYear = function (curCode) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                    $scope.getGrade(curCode, $scope.temp.sims_academic_year);
                });
            }

            $scope.GetGrade = function () {
                debugger

                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }

            $scope.getGrade = function (curCode, accYear) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                   
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%',
                        });
                        $("#cmb_grade_code").multipleSelect("checkAll");
                    }, 1000);

                });
            }

            $(function () {
                $('#cmb_grade_code').multipleSelect({
                    width: '100%'
                });
            });

            $scope.getSection = function (curCode, gradeCode, accYear) {
                debugger;
                $http.get(ENV.apiUrl + "api/StudentReport/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                  
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%',
                        });
                        $("#cmb_section_code").multipleSelect("checkAll");
                    }, 1000);


                });
            }



            $http.get(ENV.apiUrl + "api/cirulardate/getstatusCommunicationABPS").then(function (res1) {
                debugger;
                $scope.status_mode = res1.data;
                $scope.comn_appl_parameter	 = $scope.status_mode[0].comn_appl_parameter	;
                console.log($scope.status_mode)
                setTimeout(function () {
                    $('#status_id').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%',
                    });
                    $("#status_id").multipleSelect("checkAll");
                }, 1000);
               
            });

            $http.get(ENV.apiUrl + "api/cirulardate/getCommunicationUserABPS").then(function (res1) {
                debugger;
                $scope.user_mode= res1.data;
                $scope.comn_user_group_code	 = $scope.user_mode[0].comn_user_group_code	;
                console.log($scope.user_mode)
                setTimeout(function () {
                    $('#user_id').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%',
                    });
                    $("#user_id").multipleSelect("checkAll");
                }, 1000);

            });



            $(function () {
                $('#cmb_section_code').multipleSelect({
                    width: '100%'
                });
            });
            $scope.Show_Data = function (str, str1) {
                debugger;
                if (str != undefined && str1 != undefined) {
                    if ($scope.chk_sender == true || $scope.chk_receiver == true || $scope.chk_stu_name == true || $scope.chk_grade_name == true || $scope.chk_section_name == true || $scope.chk_stu_subject == true || $scope.chk_message == true || $scope.chk_comm_date || $scope.chk_read_unread == true || $scope.chk_status == true) {
                        $http.get(ENV.apiUrl + "api/cirulardate/getCommunicationData?cur_code=" + $scope.edt.sims_cur_code + "&acad_year=" + $scope.temp.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&from_date=" + $scope.mom_start_date + "&to_date=" + $scope.mom_end_date + "&status=" + $scope.temp.sims_status_data + "&search=" + $scope.temp.sims_search_id + "&group=" + $scope.temp.sims_user_data).then(function (res) {
                            $scope.studlist = res.data;
                          
                            if ($scope.studlist.length == 0) {
                                swal({ title: 'Alert', text: "Please Try Again Record Not Found...", showCloseButton: true, width: 450, height: 200 });
                                $scope.ShowCheckBoxes = true;
                            }
                            else {
                                $scope.table = true;
                                $scope.busyindicator = true;
                                $scope.ShowCheckBoxes = false;
                            }
                        })
                    }
                    else {
                        swal({ title: 'Alert', text: "Please Select Atleast One Checkbox Then Try Again...", showCloseButton: true, width: 450, height: 200 });
                    }
                }
                else {
                    swal({ title: 'Alert', text: "Please Select Curriculum & Academic Year Then Try Again...", showCloseButton: true, width: 450, height: 200 });
                }
            }
            $scope.back = function () {

                $scope.table = false;
                $scope.busyindicator = false;
                $scope.ShowCheckBoxes = true;

            }
            $scope.checkallClick = function (str) {
                if (str) {


                    $scope.chk_sender = true;
                    $scope.chk_receiver = true;
                    $scope.chk_stu_name = true;
                    $scope.chk_grade_name = true;
                    $scope.chk_section_name = true;
                    $scope.chk_stu_subject = true;
                    $scope.chk_message = true;
                    $scope.chk_read_unread = true;
                    $scope.chk_status = true;
                    $scope.chk_comm_date = true;

                    //$scope.showdisabled = false;
                    //$scope.chk_language_details = true;
                    //$scope.chk_health_details = true;
                    //$scope.chk_enoll_no = true;
                    //$scope.chk_stu_name = true;
                    //$scope.chk_stu_first_name = true;
                    //$scope.chk_stu_middel_name = true;
                    //$scope.chk_stu_last_name = true;
                    //$scope.chk_stu_nickname = true;
                    //$scope.chk_stu_family_name = true;
                    //$scope.chk_stu_name_ol = true;
                    //$scope.chk_stu_first_name_ol = true;
                    //$scope.chk_stu_middel_name_ol = true;
                    //$scope.chk_stu_last_name_ol = true;
                    //$scope.chk_stu_family_name_ol = true;
                    //$scope.chk_stu_gander = true;
                    //$scope.chk_stu_birth_place = true;
                    //$scope.chk_stu_admitted_class = true;
                    //$scope.chk_stu_roll_no = true;
                    //$scope.chk_stu_parent_id = true;
                    //$scope.chk_stu_grade_name = true;
                    //$scope.chk_stu_section_name = true;
                    //$scope.chk_stu_house_name = true;
                    //$scope.chk_stu_student_date = true;
                    //$scope.chk_stu_commence_date = true;
                    //$scope.chk_stu_student_dob = true;
                    //$scope.chk_stu_dob1 = true;

                    //$scope.chk_stu_country = true;
                    //$scope.chk_stu_religion = true;
                    //$scope.chk_stu_nationality = true;
                    //$scope.chk_stu_remark = true;
                    //$scope.chk_stu_admission_grade_code = true;
                    //$scope.chk_stu_admission_ac_year = true;
                    //$scope.chk_stu_previous_school = true;
                    //$scope.chk_stu_visa_number = true;
                    //$scope.chk_stu_visa_type = true;
                    //$scope.chk_stu_visa_place = true;
                    //$scope.chk_stu_visa_auth = true;
                    //$scope.chk_stu_visa_issue_date = true;
                    //$scope.chk_stu_visa_expiry_date = true;
                    //$scope.chk_stu_nationality_id = true;
                    //$scope.chk_stu_nationality_issue_date = true;
                    //$scope.chk_stu_nationality_expiry_date = true;
                    //$scope.chk_stu_esis_number = true;
                    //$scope.chk_stu_eduard = true;
                    //$scope.chk_stu_eduas = true;
                    //$scope.chk_stu_eduats = true;
                    //$scope.chk_stu_paassport_no = true;
                    //$scope.chk_stu_passport_issue_aut = true;
                    //$scope.chk_stu_passport_issue_place = true;
                    //$scope.chk_stu_passport_issue_date = true;
                    //$scope.chk_stu_passport_expiy_date = true;
                    //$scope.chk_stu_student_age = true;
                    //$scope.chk_stu_sibling_no = true;
                    //$scope.chk_em_stu_transport_bus_name = true;
                    //$scope.chk_stu_bus_no = true;
                    //$scope.chk_stu_exam_no = true;
                    //$scope.chk_stu_blood_group = true;
                    //$scope.chk_stu_ethnicity_name = true;
                    //$scope.chk_stu_admission_status = true;
                    //$scope.chk_stu_mai_status = true;
                    //$scope.chk_stu_aids_status = true;
                    //$scope.chk_stu_llma_status = true

                    //$scope.chk_father_street = true;
                    //$scope.chk_father_area = true;
                    //$scope.chk_father_state = true;
                    //$scope.chk_father_city = true;
                    //$scope.chk_fat_father_name = true;
                    //$scope.chk_fat_father_nationality1 = true;
                    //$scope.chk_fat_father_nationality2 = true;
                    //$scope.chk_fat_name_ol = true;
                    //$scope.chk_fat_occupation = true;
                    //$scope.chk_fat_company = true;
                    //$scope.chk_fat_address = true;
                    //$scope.chk_fat_phone_no = true;
                    //$scope.chk_fat_mobile_no = true;
                    //$scope.chk_fat_mail = true;
                    //$scope.chk_fat_fax = true;
                    //$scope.chk_fat_passport_no = true;
                    //$scope.chk_fat_passport_expiry_date = true;
                    //$scope.chk_fat_nationality_id = true;
                    //$scope.chk_fat_nationality_id_expiry_date = true;

                    //$scope.chk_mat_mothar_name = true;
                    //$scope.chk_mother_street = true;
                    //$scope.chk_mother_area = true;
                    //$scope.chk_mother_state = true;
                    //$scope.chk_mother_city = true;
                    //$scope.chk_mat_mother_nationality1 = true;
                    //$scope.chk_mat_mother_nationality2 = true;
                    //$scope.chk_mat_name_ol = true;
                    //$scope.chk_mat_occupation = true;
                    //$scope.chk_mat_company = true;
                    //$scope.chk_mat_address = true;
                    //$scope.chk_mat_phone_no = true;
                    //$scope.chk_mat_mobile_no = true;
                    //$scope.chk_mat_mail = true;
                    //$scope.chk_mat_fax = true;
                    //$scope.chk_mat_passport_no = true;
                    //$scope.chk_mat_passport_expiry_date = true;
                    //$scope.chk_mat_nationality_id = true;
                    //$scope.chk_mat_nationality_id_expiry_date = true;

                    //$scope.chk_grd_Guardian_name = true;
                    //$scope.chk_grd_Guardian_nationality1 = true;
                    //$scope.chk_grd_Guardian_nationality2 = true;
                    //$scope.chk_grd_name_ol = true;
                    //$scope.chk_grd_occupation = true;
                    //$scope.chk_grd_company = true;
                    //$scope.chk_grd_address = true;
                    //$scope.chk_grd_phone_no = true;
                    //$scope.chk_grd_mobile_no = true;
                    //$scope.chk_grd_mail = true;
                    //$scope.chk_grd_fax = true;
                    //$scope.chk_grd_passport_no = true;
                    //$scope.chk_grd_passport_expiry_date = true;
                    //$scope.chk_grd_nationality_id = true;
                    //$scope.chk_grd_nationality_id_expiry_date = true;
                    //$scope.chk_Fee_Payment_Contact_Preference = true;
                    //$scope.chk_Primary_Contact_Code = true;
                    //$scope.chk_Primary_Contact_Preference = true;
                    //$scope.chk_Voter_ID = true;
                    //$scope.chk_Pan_Number = true;
                    //$scope.chk_Emergency_Contact_Number2 = true;
                    //$scope.chk_Emergency_Contact_Name2 = true;
                    //$scope.chk_Emergency_Contact_Number1 = true;
                    //$scope.chk_Emergency_Contact_Name1 = true;
                    //$scope.chk_is_company_employee = true;
                    //$scope.chk_employee_user_name = true;
                    //$scope.chk_previous_school_name = true;
                }
                else {
                    $scope.Uncheck();
                }
            }

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (propertyName) {
                debugger
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };


            $scope.Uncheck = function () {

                $scope.showdisabled = false;

                $scope.chk_sender = false;
                $scope.chk_receiver = false;
                $scope.chk_stu_name = false;
                $scope.chk_grade_name = false;
                $scope.chk_section_name = false;
                $scope.chk_stu_subject = false;
                $scope.chk_message = false;
                $scope.chk_read_unread = false;
                $scope.chk_status = false;
                $scope.chk_comm_date = false;







                //$scope.chk_language_details = false;
                //$scope.chk_health_details = false;
                //$scope.chk_enoll_no = false;
                //$scope.chk_stu_name = false;
                //$scope.chk_stu_first_name = false;
                //$scope.chk_stu_middel_name = false;
                //$scope.chk_stu_last_name = false;
                //$scope.chk_stu_nickname = false;
                //$scope.chk_stu_family_name = false;
                //$scope.chk_stu_name_ol = false;
                //$scope.chk_stu_first_name_ol = false;
                //$scope.chk_stu_middel_name_ol = false;
                //$scope.chk_stu_last_name_ol = false;
                //$scope.chk_stu_family_name_ol = false;
                //$scope.chk_stu_gander = false;
                //$scope.chk_stu_birth_place = false;
                //$scope.chk_stu_admitted_class = false;
                //$scope.chk_stu_roll_no = false;
                //$scope.chk_stu_parent_id = false;
                //$scope.chk_stu_grade_name = false;
                //$scope.chk_stu_section_name = false;
                //$scope.chk_stu_house_name = false;
                //$scope.chk_stu_student_date = false;
                //$scope.chk_stu_commence_date = false;
                //$scope.chk_stu_student_dob = false;
                //$scope.chk_stu_dob1 = false;
                //$scope.chk_stu_country = false;
                //$scope.chk_stu_religion = false;
                //$scope.chk_stu_nationality = false;
                //$scope.chk_stu_remark = false;
                //$scope.chk_stu_admission_grade_code = false;
                //$scope.chk_stu_admission_ac_year = false;
                //$scope.chk_stu_previous_school = false;
                //$scope.chk_stu_visa_number = false;
                //$scope.chk_stu_visa_type = false;
                //$scope.chk_stu_visa_place = false;
                //$scope.chk_stu_visa_auth = false;
                //$scope.chk_stu_visa_issue_date = false;
                //$scope.chk_stu_visa_expiry_date = false;
                //$scope.chk_stu_nationality_id = false;
                //$scope.chk_stu_nationality_issue_date = false;
                //$scope.chk_stu_nationality_expiry_date = false;
                //$scope.chk_stu_esis_number = false;
                //$scope.chk_stu_eduard = false;
                //$scope.chk_stu_eduas = false;
                //$scope.chk_stu_eduats = false;
                //$scope.chk_stu_paassport_no = false;
                //$scope.chk_stu_passport_issue_aut = false;
                //$scope.chk_stu_passport_issue_place = false;
                //$scope.chk_stu_passport_issue_date = false;
                //$scope.chk_stu_passport_expiy_date = false;
                //$scope.chk_stu_student_age = false;
                //$scope.chk_stu_sibling_no = false;
                //$scope.chk_em_stu_transport_bus_name = false;
                //$scope.chk_stu_bus_no = false;
                //$scope.chk_stu_exam_no = false;
                //$scope.chk_stu_blood_group = false;
                //$scope.chk_stu_ethnicity_name = false;
                //$scope.chk_stu_admission_status = false;
                //$scope.chk_stu_mai_status = false;
                //$scope.chk_stu_aids_status = false;
                //$scope.chk_stu_llma_status = false

                //$scope.chk_fat_father_name = false;
                //$scope.chk_fat_father_nationality1 = false;
                //$scope.chk_fat_father_nationality2 = false;
                //$scope.chk_fat_name_ol = false;
                //$scope.chk_fat_occupation = false;
                //$scope.chk_fat_company = false;
                //$scope.chk_fat_address = false;
                //$scope.chk_fat_phone_no = false;
                //$scope.chk_fat_mobile_no = false;
                //$scope.chk_fat_mail = false;
                //$scope.chk_fat_fax = false;
                //$scope.chk_fat_passport_no = false;
                //$scope.chk_fat_passport_expiry_date = false;
                //$scope.chk_fat_nationality_id = false;
                //$scope.chk_fat_nationality_id_expiry_date = false;
                //$scope.chk_father_street = false;
                //$scope.chk_father_area = false;
                //$scope.chk_father_state = false;
                //$scope.chk_father_city = false;

                //$scope.chk_mat_mothar_name = false;
                //$scope.chk_mother_street = false;
                //$scope.chk_mother_area = false;
                //$scope.chk_mother_state = false;
                //$scope.chk_mother_city = false;
                //$scope.chk_mat_mother_nationality1 = false;
                //$scope.chk_mat_mother_nationality2 = false;
                //$scope.chk_mat_name_ol = false;
                //$scope.chk_mat_occupation = false;
                //$scope.chk_mat_company = false;
                //$scope.chk_mat_address = false;
                //$scope.chk_mat_phone_no = false;
                //$scope.chk_mat_mobile_no = false;
                //$scope.chk_mat_mail = false;
                //$scope.chk_mat_fax = false;
                //$scope.chk_mat_passport_no = false;
                //$scope.chk_mat_passport_expiry_date = false;
                //$scope.chk_mat_nationality_id = false;
                //$scope.chk_mat_nationality_id_expiry_date = false;

                //$scope.chk_grd_Guardian_name = false;
                //$scope.chk_grd_Guardian_nationality1 = false;
                //$scope.chk_grd_Guardian_nationality2 = false;
                //$scope.chk_grd_name_ol = false;
                //$scope.chk_grd_occupation = false;
                //$scope.chk_grd_company = false;
                //$scope.chk_grd_address = false;
                //$scope.chk_grd_phone_no = false;
                //$scope.chk_grd_mobile_no = false;
                //$scope.chk_grd_mail = false;
                //$scope.chk_grd_fax = false;
                //$scope.chk_grd_passport_no = false;
                //$scope.chk_grd_passport_expiry_date = false;
                //$scope.chk_grd_nationality_id = false;
                //$scope.chk_grd_nationality_id_expiry_date = false;
                //$scope.chk_Fee_Payment_Contact_Preference = false;
                //$scope.chk_Primary_Contact_Code = false;
                //$scope.chk_Primary_Contact_Preference = false;
                //$scope.chk_Voter_ID = false;
                //$scope.chk_Pan_Number = false;
                //$scope.chk_Emergency_Contact_Number2 = false;
                //$scope.chk_Emergency_Contact_Name2 = false;
                //$scope.chk_Emergency_Contact_Number1 = false;
                //$scope.chk_Emergency_Contact_Name1 = false;
                //$scope.chk_is_company_employee = false;
                //$scope.chk_employee_user_name = false;
                //$scope.chk_previous_school_name = false;
            }

            $scope.Reset = function () {


                active = '';
                inactive = '';

                $scope.Uncheck();
                $scope.select_all = false;
                $scope.active_stud = false;

                $scope.inactive_stud = false;
                $scope.edt = {
                    sims_cur_code: '',
                    sims_grade_code: '',
                    sims_section_code: '',
                }
                $scope.temp = {
                    sims_academic_year: '',
                }

            }

            $scope.checkevent = function () {
                if ($scope.chk_sender == true || $scope.chk_receiver == true || $scope.chk_stu_name == true || $scope.chk_grade_name == true || $scope.chk_section_name == true || $scope.chk_stu_subject == true || $scope.chk_message == true || $scope.chk_comm_date ==true|| $scope.chk_read_unread == true || $scope.chk_status == true) {
                    $scope.showdisabled = false;
                }
                else {
                    $scope.showdisabled = true;
                }
            }



            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj4, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj4;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.comn_appl_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.comn_appl_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.activestudent = function (ca) {
                debugger;

                if (ca) {
                    active = 'A';
                }
                else {
                    active = undefined;
                }
            }

            $scope.inactivestudent = function (cina) {
                debugger;
                if (cina) {
                    inactive = 'I';
                }
                else {
                    inactive = undefined;
                }
            }

        }])
})();