﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Collaboration');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmailApprovalCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$stateParams', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $stateParams) {
            $scope.pagesize = 'All';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.temp = {};
            $scope.Rating = {};

            $scope.show_grid = function ()
            {
                
                $http.get(ENV.apiUrl + "api/EmailApproval/getEmailApprovalDetails").then(function (emailAlldata) {
                    debugger;
                    $scope.Get_approved_email_list = emailAlldata.data;
                    //for (var i = 0; i < $scope.Get_approved_email_list.length; i++) {
                    //    $scope.Get_approved_email_list[i]['icon'] = "fa fa-plus-circle";
                    //}
                    $scope.totalItems = $scope.Get_approved_email_list.length;
                    $scope.todos = $scope.Get_approved_email_list;
                    $scope.makeTodos();
                    console.log($scope.Get_approved_email_list);
                });

            }
            $scope.show_grid();
            $(function () {
                $("#Checkbox1").click(function () {
                    debugger;
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.startDate = '';
                    }
                    //} else {

                    //    $("#fdate").removeAttr("disabled");
                    //    $("#fdate").focus();
                    //    $scope.startDate = dd + '-' + mm + '-' + yyyy;
                    //}
                });
            });


            $(function () {
                $("#Checkbox2").click(function () {
                    debugger;
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.endDate = '';
                    }
                    //} else {

                    //    $("#fdate").removeAttr("disabled");
                    //    $("#fdate").focus();
                    //    $scope.endDate = dd + '-' + mm + '-' + yyyy;
                    //}
                });
            });

            $scope.Approve_details = function () {
                debugger
                var checked_lst=''
                for (var i = 0; i < $scope.Get_approved_email_list.length; i++) {
                    if ($scope.Get_approved_email_list[i].checked == true) {
                        checked_lst = checked_lst+ $scope.Get_approved_email_list[i].sims_email_number + ',';
                    }
                }
                console.log(checked_lst);

                $http.post(ENV.apiUrl + "api/EmailApproval/approve_email?lst=" + checked_lst).then(function (emailAlldata) {
                    if (emailAlldata.data) {
                        swal({ title: "Alert", text: "Approved Successfully.", showCloseButton: true, width: 380, });
                        $scope.show_grid();
                    }
                    else {
                        swal({ title: "Alert", text: " Not Approved. ", showCloseButton: true, width: 380, });

                    }
                });
            }


            $scope.reject = function () {
                debugger;
                var checked_lst = ''
                for (var i = 0; i < $scope.Get_approved_email_list.length; i++) {
                    if ($scope.Get_approved_email_list[i].checked == true) {
                        checked_lst = checked_lst + $scope.Get_approved_email_list[i].sims_email_number + ',';
                    }
                }
                console.log(checked_lst);

                $http.post(ENV.apiUrl + "api/EmailApproval/reject_email?lst=" + checked_lst).then(function (emailAlldata) {
                    if (emailAlldata.data) {
                        swal({ title: "Alert", text: "Rejected Successfully.", showCloseButton: true, width: 380, });
                        $scope.show_grid();
                    }
                    else {
                        swal({ title: "Alert", text: " Not Rejected. ", showCloseButton: true, width: 380, });

                    }
                });
            }


            $scope.Export_Data = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('email_data').innerHTML], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                        });
                        saveAs(blob, "EmailList.xls");
                        $scope.colsvis = false;

                    }

                });
            };
            $scope.search_details = function () {
                debugger;
                

                $http.get(ENV.apiUrl + "api/EmailApproval/Get_search_email_deatils?startDate=" + $scope.startDate + "&endDate=" + $scope.endDate + "&email=" + $scope.Rating.EmailAddress + "&subject=" + $scope.Rating.EmailSubject).then(function (Get_search_email_deatils) {
                    $scope.filteredTodos = Get_search_email_deatils.data;
                                  });

            }
            $scope.size = function (str) {
                debugger;
                if (str == 'All') {
                    //$scope.currentPage = '1';
                    $scope.filteredTodos = $scope.Get_approved_email_list;
                    $scope.makeTodos();
                    $scope.pager = false;
                }
                else {
                    debugger
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['chk_all'] = false;
                $scope.row1 = '';
            }
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 50, $scope.maxSize = 50;

            $scope.makeTodos = function () {
                debugger
                if ($scope.pagesize == 'All') {
                    $scope.currentPage = 1;
                    $scope.showPager = false;
                    $scope.filteredTodos = $scope.todos;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                }
                else {
                    debugger
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                    $scope.Get_approved_email_list = [];
                    $scope.Get_approved_email_list = $scope.filteredTodos;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }

                 

                }
            };
            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('check-'+i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('check-' + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    debugger;
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            var dom;
            $scope.flag = true;

            $scope.expand = function (info, $event) {
                debugger;
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                    "<table class='inner-table'  width='100%' cellpadding='5' cellspacing='0' style='border-color:red;border:solid;border-width:02px'>" +
                    "<tbody>" +

                    "<tr><td class='semi-bold'>" + "Message" + "</td> </tr>" +
                        "<tr><td>" + "<span class='body' ng-bind-html= "+ (info.message) +"| html> </span>" + "</td></tr>" +
                    "</tbody>" +
                    " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                } else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.Get_approved_email_list.length; i++) {
                        $scope.Get_approved_email_list[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }
            };

            $scope.searched = function (valLists, toSearch) {
                debugger;
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                debugger;
                $scope.todos = $scope.searched($scope.Get_approved_email_list, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Get_approved_email_list;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                debugger;
                /* Search Text in all 3 fields */
                return (item.receiveMail.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.subject.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

            $scope.reset = function () {
                debugger;
                $scope.startDate = '';
                $scope.endDate = '';
                $scope.Rating.EmailAddress = '';
                $scope.Rating.EmailSubject = '';
                //$scope.edt = {
                //    sims_grade_code: '',
                //    sims_section_name: ''
                //}
            }
        }])
})();