﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Collaboration');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmailDetailsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV, $moment) {

            $scope.expanded = true;
            $scope.display = true;
            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;
            $scope.cdetails_data = [];

            $scope.pagesize_update = "5";
            $scope.pageindex_update = "1";
            $scope.filteredTodos_update = [], $scope.currentPage_update = 1, $scope.numPerPage_update = 10, $scope.maxSize_update = 5;
            $scope.cdetails_data_update = [];

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd",
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            //var date = new Date();
            //$scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');
            //$scope.ddMMyyyy = $filter('date')(new Date(), 'dd-mm-yyyy');
            var dt = new Date();
            $scope.search_from_date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
            $scope.search_to_date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();

            //$scope.search_from_date = $scope.date;
            // $scope.search_to_date = $scope.date;
            
            $http.get(ENV.apiUrl + "api/EmailDetails/getEmailDetailsDatewise?fromdate=" + $scope.search_from_date + "&todate=" + $scope.search_to_date).then(function (res1) {
                debugger;
                //$scope.dets = res1.data;

                $scope.cdetails_data = res1.data;
                $scope.totalItems = $scope.cdetails_data.length;
                $scope.todos = $scope.cdetails_data;
                $scope.makeTodos();
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                debugger;
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                if (str == 10 || str == 20) {
                    $scope.pager = true;
                }

                else {

                    $scope.pager = false;
                }
                if (str == "*") {
                    $scope.numPerPage = $scope.cdetails_data.length;
                    $scope.filteredTodos = $scope.cdetails_data;
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                }
                $scope.makeTodos();

            }


            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.search = function () {
                debugger
                $scope.todos = $scope.searched($scope.cdetails_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.cdetails_data;
                }
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            function searchUtil(item, toSearch) {
                debugger
                /* Search Text in all 2 fields */  
                return (item.e_details[0].email_subject.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.e_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.SearchbyDate = function (sfromdate, stodate) {

                if (sfromdate = undefined)
                    sfromdate = ''
                if (stodate = undefined)
                    stodate = ''
                sfromdate = $scope.search_from_date;
                stodate = $scope.search_to_date;

                $http.get(ENV.apiUrl + "api/EmailDetails/getEmailDetailsDatewise?fromdate=" + sfromdate + "&todate=" + stodate).then(function (res1) {
                    $scope.cdetails_data = res1.data;
                    $scope.totalItems = $scope.cdetails_data.length;
                    $scope.todos = $scope.cdetails_data;
                    $scope.makeTodos();
                });
            }

            //Modal Pop Up methods

            $scope.makeTodos_update = function () {
                var rem_update = parseInt($scope.totalItems_update % $scope.numPerPage_update);
                if (rem_update == '0') {
                    $scope.pagersize_update = parseInt($scope.totalItems_update / $scope.numPerPage_update);
                }
                else {
                    $scope.pagersize_update = parseInt($scope.totalItems_update / $scope.numPerPage_update) + 1;
                }
                var begin_update = (($scope.currentPage_update - 1) * $scope.numPerPage_update);
                var end_update = parseInt(begin_update) + parseInt($scope.numPerPage_update);

                $scope.filteredTodos_update = $scope.todos_update.slice(begin_update, end_update);
            };

            $scope.size_update = function (str) {
                console.log(str);
                $scope.pagesize_update = str;
                $scope.currentPage_update = 1;
                $scope.numPerPage_update = str; console.log("numPerPage=" + $scope.numPerPage_update); $scope.makeTodos_update();
            }

            $scope.index_update = function (str) {
                $scope.pageindex_update = str;
                $scope.currentPage_update = str; console.log("currentPage=" + $scope.currentPage_update); $scope.makeTodos_update();
                main_update.checked = false;
                $scope.row1 = '';
            }

            $scope.search_update = function () {
                $scope.todos_update = $scope.searched_update($scope.cdetails_data_update, $scope.searchText_update);
                $scope.totalItems_update = $scope.todos_update.length;
                $scope.currentPage_update = '1';
                if ($scope.searchText_update == '') {
                    $scope.todos_update = $scope.cdetails_data_update;
                }
                $scope.makeTodos_update();
            }

            $scope.searched_update = function (valLists_update, toSearch_update) {

                return _.filter(valLists_update,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil_update(i, toSearch_update);
                });
            };

            function searchUtil_update(item, toSearch) {
                /* Search Text in all 2 fields */
                return (item.email_body.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.email_id.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }
            
            $scope.viewe_details = function (str,esubject) {

                var e_date = str;
                var e_subject = esubject;
                $scope.email_subjects = esubject;
                $scope.pagesize_update = "5";
                $scope.pageindex_update = "1";
                $scope.filteredTodos_update = [], $scope.currentPage_update = 1, $scope.numPerPage_update = 5, $scope.maxSize_update = 5;
                $scope.cdetails_data_update = [];

                $http.get(ENV.apiUrl + "api/EmailDetails/getEmailDetailsbody_ids?edate=" + e_date + "&esubject=" + e_subject).then(function (res2) {
                                     
                    $scope.cdetails_data_update = res2.data;
                    $scope.totalItems_update = $scope.cdetails_data_update.length;
                    $scope.todos_update = $scope.cdetails_data_update;
                    $scope.makeTodos_update();
                });

                $('#emailModal').modal({ backdrop: "static" });

                
            }

            $scope.Reset = function () {
                debugger
                $scope.filteredTodos_update = [];
                $scope.filteredTodos = [];
                setTimeout(function () {
                    $(document).ready(function () {
                        $("#fromdate, #todate").kendoDatePicker({
                            format: "dd-MM-yyyy",
                            value: ''
                        });
                    });
                }, 100);
                var dt = new Date();
                $scope.search_from_date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                $scope.search_to_date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
            }
        }])
})();