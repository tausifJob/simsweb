﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Collaboration');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });



    simsController.controller('AlertConfigCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$stateParams', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $stateParams, $filter) {

            $scope.alertconfig = {};
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.deleteList = [];
            $scope.showForm = false;
            $scope.updatebtn = false;
            $scope.disableDate = false;
            $scope.display = false;
            $scope.showTable = true;
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');
            console.log($scope.ddMMyyyy);
            $scope.showForm2 = false;

           
            //fetching all data 
            $scope.fetchalldata = function () {
                $http.get(ENV.apiUrl + "api/AlertConfig/AlertConfigCommon").then(function (res) {
                    $scope.allData = res.data;
                    $scope.totalItems = $scope.allData.length;
                    $scope.todos = $scope.allData;
                    $scope.makeTodos();
                });
            }
          

            //New Functioality
            $scope.New = function () {
                
                $scope.showForm2 = true;
                $scope.showTable = false;
                $scope.save1 = true;
                $scope.updatebtn = false;
                $scope.deletebtn = false;
                $scope.disableDate = false;
                $scope.alertconfig.sims_alert_name= '';
                $scope.alertconfig.sims_scriptname = '';
                $scope.alertconfig.sims_startdate = '';
                $scope.alertconfig.sims_alertdays = '';
                $scope.alrtconfig.sims_nextdate = '';
                $scope.alertconfig.sims_control_field = '';
                $scope.alertconfig.sims_alert_desc = '';
                $scope.alertconfig.sims_status1 = false;
               // $scope.alertconfig.sims_startdate = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                $('#dp_admdate').data('kendoDatePicker').enable(true);
                $('#start_date').data('kendoDatePicker').enable(true);

                $scope.edt=
                    {
                        admission_date: $scope.ddMMyyyy
                    }
            }

            //Edit Functionality
            $scope.edit = function (data) {
                debugger
                $scope.showForm2 = true;
                $scope.showTable = false;
                $scope.disableDate = true;
                $scope.save1 = false;
                $scope.updatebtn = true;
                $scope.deletebtn = false;
                //$scope.alertconfig = data;


                $scope.alertconfig = {
                    sims_alert_name:data.sims_alert_name,
                    sims_scriptname:data.sims_scriptname,
                    sims_startdate:data.sims_startdate,
                    sims_alertdays:data.sims_alertdays,
                    sims_nextdate:data.sims_nextdate,
                    sims_control_field:data.sims_control_field,
                    sims_alert_desc:data.sims_alert_desc,
                    sims_status1: data.sims_status1,
                    sims_scriptid:data.sims_scriptid

                }
               


                $('#dp_admdate').data('kendoDatePicker').enable(false);
                $('#start_date').data('kendoDatePicker').enable(false);


            }


            //Cancel
            $scope.cancel = function () {
                $scope.showTable = true;
                $scope.showForm2 = false;
                $scope.alertconfig.sims_alert_name = '';
                $scope.alertconfig.sims_scriptname = '';
                $scope.alertconfig.sims_startdate = '';
                $scope.alertconfig.sims_alertdays = '';
                $scope.alrtconfig.sims_nextdate = '';
                $scope.alertconfig.sims_control_field = '';
                $scope.alertconfig.sims_alert_desc = '';
                $scope.alertconfig.sims_status = false;
            }


            
            //save
            var datasend = [];
            $scope.save = function (showForm) {
                if (showForm) {    
                    var data = $scope.alertconfig;
                    data.opr = 'I';
                    datasend.push(data);
                  
                    $http.post(ENV.apiUrl + "api/AlertConfig/CUDAlertConfig", datasend).then(function (res) {
                        if (res.data == true) {
                            swal({ text: 'Record added Successfully',imageUrl: "assets/img/check.png", width: 330, showCloseButton: true });
                            $scope.showForm2 = false;
                            $scope.showTable = true;
                            $scope.fetchalldata();
                        }
                        else if ($scope.data == false) {
                                swal({ text: 'Record not Added',imageUrl: "assets/img/close.png", width: 330, showCloseButton: true });
                            $scope.showForm2 = false;
                            $scope.showTable = true;
                            $scope.fetchalldata();
                        }
                        else {
                            swal("Error-" + $scope.data)
                        }
                    });
                    datasend = [];

                }
            }
            

            //update
            var dataforUpdate = [];
            $scope.Update = function () {
                if (showForm) {    
               
                    var data = $scope.alertconfig;
                    data.opr = "U";
                    dataforUpdate.push(data);

                  

                    $http.post(ENV.apiUrl + "api/AlertConfig/CUDAlertConfig", dataforUpdate).then(function (res) {
                        if (res.data == true) {
                            swal({ text: 'Record updated Successfully',imageUrl: "assets/img/check.png", width: 330, showCloseButton: true });
                            $scope.showForm2 = false;
                            $scope.showTable = true;
                            $scope.fetchalldata();
                        }
                        else if ($scope.data == false) {
                                swal({ text: 'Record not Updated',imageUrl: "assets/img/close.png", width: 330, showCloseButton: true });
                            $scope.showForm2 = false;
                            $scope.showTable = true;
                            $scope.fetchalldata();
                        }
                        else {
                            swal("Error-" + $scope.data)
                        }


                    });
                    dataforUpdate = [];
                }
            }


            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseFloat($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseFloat($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseFloat(begin) + parseFloat($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                $scope.selectedAll = false;
                $scope.isSelectAll();

            };


            //page size (10/20/all)
            $scope.size = function (str) {               
                debugger;
                if (str == "All") {                
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.allData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); 

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }
                
            
            //select all
            $scope.isSelectAll = function () {
                debugger
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.allData.length; i++) {
                        var v = document.getElementById("ac-" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.allData.length; i++) {
                        var v = document.getElementById("ac-"+i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            //select row
            $scope.checkIfAllSelected = function () {
                var id = event.target.id;
                var value = event.target.value;
                if (document.getElementById(id).checked) {
                    $scope.deleteList.push(value);
                    if ($scope.deleteList.length == $scope.filteredTodos.length) {
                        $scope.selectedAll = true;
                    }
                    $("#" + id).closest('tr').addClass("row_selected");
                } else {
                    $scope.selectedAll = false;
                    $scope.row1 = '';
                    var index = $scope.deleteList.indexOf(value);
                    $scope.deleteList.splice(index, 1);
                    $("#" + id).closest('tr').removeClass("row_selected");
                }
            };

            //delete
            $scope.OkDelete = function () {
                debugger;
              //  $scope.search.$ = '';
                var data = [];

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    debugger;
                    var v = document.getElementById("ac-" + i);
                    if (v != null)
                    {

                        if (v.checked == true) {
                            var obj = {
                                opr: 'D',
                                sims_scriptid: $scope.filteredTodos[i].sims_scriptid
                            }
                            data.push(obj);
                        }
                    }
                }
            

                if (data.length > 0) {

                    swal({
                        title: '',
                        text: "Are you sure you want to delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/AlertConfig/CUDAlertConfig", data).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: 'Record Deleted Successfully',imageUrl: "assets/img/check.png", width: 360, showCloseButton: true });
                                    $scope.showForm = false;
                                    $scope.showTable = true;
                                    $scope.deleteList = [];
                                    $scope.fetchalldata();
                                }
                                else if ($scope.msg1 == false) {
                                        swal({ text: 'Record Not Deleted. ',imageUrl: "assets/img/close.png", width: 360, showCloseButton: true });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            })

                        }
                        else {
                            $scope.selectedAll = false;
                            $scope.isSelectAll();
                        }
                    })

                }
                else {
                    swal({ text: 'Select Atleast One Record To Delete',imageUrl: "assets/img/notification-alert.png", width: 320, showCloseButton: true });
                }
            }

           


            $(document).ready(function () {
                $scope.fetchalldata();
            })


          
            
        }])
})();