﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Collaboration');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('DefaulterStatusSISO',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            $scope.pagesize = "20";


            $('*[data-datepicker="true"] input[type="text"]').datepicker(
                         {
                             todayBtn: true,
                             orientation: "top left",
                             autoclose: true,
                             todayHighlight: true,
                             format: 'yyyy-mm-dd'
                         });


            /* PAGER FUNCTIONS*/
            $scope.edt = [];
            $scope.edt = {};
            $('#text-editor').wysihtml5();
            $scope.cur_change = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllAcademicYears?cur_code=" + $scope.edt.sims_cur_code).then(function (admissionYear) {
                    $scope.admissionYear = admissionYear.data;
                    if (admissionYear.data.length > 0) {
                        $scope.edt.sims_academic_year = admissionYear.data[0].sims_academic_year;

                    }
                    //for (var i = 0; i < $scope.admissionYear.length; i++) {
                    //    if ($scope.admissionYear[i].sims_academic_year_status == 'C') {
                    //        //   $scope.edt['sims_cur_code'] = cur_values;
                    //        $scope.edt['sims_academic_year'] = $scope.admissionYear[i].sims_academic_year;
                    //    }
                    //}
                    $scope.acdm_yr_change();
                });
            }

            $http.get(ENV.apiUrl + "api/Fee/SFS/GetDefaulterEmailMessage").then(function (email_temp) {
                $scope.email_temp = email_temp.data;
                console.log($scope.email_temp)
                //  document.getElementById('text-editor').innerHTML = $scope.email_temp;
                //var $ta = $('textarea#text-editor');
                //var w5ref = $ta.data('wysihtml5');
                //if (w5ref) {
                //    w5ref.editor.setValue($scope.email_temp);
                //}
            });

            $scope.settemplate = function (msg_sr_no) {
                debugger;
                for (var i = 0; i < $scope.email_temp.length; i++) {
                    if ($scope.email_temp[i].sims_msg_sr_no == msg_sr_no) {

                        document.getElementById('text-editor').innerHTML = $scope.email_temp[i].email_default_template;
                        var $ta = $('textarea#text-editor');
                        var w5ref = $ta.data('wysihtml5');
                        if (w5ref) {
                            w5ref.editor.setValue($scope.email_temp[i].email_default_template);
                        }

                    }
                }

            }



            $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllCurName").then(function (admissionCur) {
                $scope.admissionCur = admissionCur.data;
                if (admissionCur.data.length > 0) {
                    $scope.edt.sims_cur_code = admissionCur.data[0].sims_cur_code;
                    $scope.cur_change($scope.edt.sims_cur_code);
                }
            });

            $http.get(ENV.apiUrl + "api/Fee/SFS/GetDefaulterfeeTypes").then(function (feetypes) {
                $scope.feetypes = feetypes.data;
                setTimeout(function () {
                    $('#cmb_feeTypes').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);

            });

            $http.get(ENV.apiUrl + "api/Fee/SFS/GetDefaulterviewList").then(function (listtype) {
                $scope.listtype = listtype.data;
            });

            $http.get(ENV.apiUrl + "api/Fee/SFS/GetDefaulterviewMode").then(function (listmode) {
                $scope.listmode = listmode.data;
            });

            $scope.acdm_yr_change = function () {
                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year).then(function (admissionGrade) {
                    $scope.admissionGrade = admissionGrade.data;
                    setTimeout(function () {
                        $('#cmb_grade').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });

                $http.get(ENV.apiUrl + "api/Fee/SFS/GetDefaulterTerms?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (terms) {
                    $scope.terms = terms.data;
                });
            }

            $scope.grades = '';
            $scope.grade_change = function () {
                $scope.grades = '';
                try {
                    $("#cmb_section").multipleSelect("uncheckAll");
                }
                catch (e) { }
                for (var i = 0; i < $scope.edt.sims_grade_code.length; i++)
                    $scope.grades = $scope.grades + ($scope.edt.sims_grade_code[i]) + ',';

                $http.get(ENV.apiUrl + "api/Fee/SFS/GetDefaulterSection?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year + "&g_code=" + $scope.grades).then(function (admissionsection) {
                    $scope.admissionsection = admissionsection.data;
                    setTimeout(function () {
                        $('#cmb_section').change(function () {
                        }).multipleSelect({ width: '100%' });


                    }, 1000);

                });
            }



            $http.get(ENV.apiUrl + "api/Fee/SFS/getAllPercentage").then(function (get_per) {
                $scope.percentage = get_per.data;
            });




            $scope.btnPreview_click = function (str) {
                debugger
                  
                var Valuearr = [];
                Valuearr = str.split('-');
                var min_value = Valuearr[0];
                var max_value = Valuearr[1];



                   var academic_status='A,D,C,E,I';
                   $http.get(ENV.apiUrl + "api/Fee/SFS/getDefaulterListNEw?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&grade_code=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&feetype=" + $scope.edt.sims_fee_code + "&academic_status=" + academic_status + "&doc_date=" + $scope.edt.doc_date + "&min_value=" + min_value + "&max_value=" + max_value).then(function (res) {
                       $scope.defaulterdata = res.data;
                       $scope.totalItems = $scope.defaulterdata.length;
                       $scope.todos = $scope.defaulterdata;
                       $scope.makeTodos();
                       $scope.newbtn = true; 
                   });

             
            }

            $scope.OkExport = function (info) {

                var check = true;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " Email To Defaulter.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            var blob = new Blob([document.getElementById('exportdata').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + " Email To Defaulter" + ".xls");
                            //alasql('SELECT * INTO XLSX("Report.xlsx",{headers:true}) \ FROM HTML("#printdata",{headers:true,skipdisplaynone:true})');

                        }
                    });
                }
                else {
                    swal({  text: "Report Not Save",imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, })
                }


            }


            $scope.reset = function () {
                $scope.admissions = [];
                $scope.filteredTodos = [];
                $scope.defaulterdata = [];
                $scope.edt = { 'sims_cur_code': $scope.edt.sims_cur_code, 'sims_academic_year': $scope.edt.sims_academic_year }
                $scope.acdm_yr_change();
                $scope.edt.sims_start_date = '';
                $scope.edt.sims_end_date = '';
                $scope.edt.admission_no = '';
                $scope.email_subject = '';
                $('#text-editor').data("wysihtml5").editor.clear();
                try {
                    $("#cmb_grade").multipleSelect("uncheckAll");
                }
                catch (e) { }
                try {
                    $("#cmb_section").multipleSelect("uncheckAll");
                }
                catch (e) { } try {
                    $("#cmb_feeTypes").multipleSelect("uncheckAll");
                }
                catch (e) { }
            }

            $scope.message = function () {
                $('#MyModal').modal({ backdrop: 'static', keyboard: true });
            }


            $(function () {
                $('#cmb_grade').multipleSelect({ width: '100%' });
                $('#cmb_section').multipleSelect({ width: '100%' });
                $('#cmb_feeTypes').multipleSelect({ width: '100%' });

            });

            /* PAGER FUNCTIONS*/

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 20, $scope.maxSize = 20;

            $scope.makeTodos = function () {
                $scope.currentPage_ind = 0;

                if ((Math.round($scope.totalItems / $scope.numPerPage) * $scope.numPerPage) > $scope.totalItems)
                    $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage);
                else
                    $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage) + 1;

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);

            };





            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = 1;
                    $scope.numPerPage = $scope.summeryData.length;
                    $scope.makeTodos();

                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.currentPage_ind = str;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage_ind = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            /* PAGER FUNCTIONS*/

            $scope.CheckMultipleSearch = function () {
                var main = document.getElementById('chkSearch');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if (main.checked == true) {
                        $scope.filteredTodos[i].isSelected = true;
                    }
                    else if (main.checked == false) {
                        $scope.row1 = '';
                        $scope.color = '#edefef';
                        main.checked = false;
                        $scope.filteredTodos[i].isSelected = false;
                    }
                }
            }
            $scope.CheckOneByOneSearch = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                    }
                });

                var main = document.getElementById('chkSearch');
                if (main.checked == true) {
                    main.checked = false;
                    //$scope.color = '#edefef';
                }

            }

            $scope.submitdata = function () {
                debugger
                if ($scope.email_subject == undefined || document.getElementById('text-editor').value == undefined) {
                    swal(
                    {
                        showCloseButton: true,
                        text: 'Please Define Message Or Subject',
                        imageUrl: "assets/img/notification-alert.png",
                        width: 350,
                        showCloseButon: true
                    });
                }
                else {
                    $scope.data = [];
                    var t = $('select[name=cmb_feeTypes] option:selected').text();
                    for (var x = 0; x < $scope.filteredTodos.length; x++) {
                        if ($scope.filteredTodos[x].isSelected == true && $scope.filteredTodos[x].sims_parent_email != '') {
                            $scope.filteredTodos[x].sims_cur_code = $scope.edt.sims_cur_code;
                            $scope.filteredTodos[x].sims_academic_year = $scope.edt.sims_academic_year;
                            $scope.filteredTodos[x].email_subject = $scope.email_subject;
                            $scope.message_details = "<table border=1 style='border-collapse:collapse;border-width:3px;width:450px'><tr><td Colspan=2>Fee Due Details</td></tr><tr><td Colspan=2>Enrollment No:" + $scope.filteredTodos[x].sims_enroll_number + "</td></tr><tr><td Colspan=2> Student Name:" + $scope.filteredTodos[x].student_name + "</td></tr><tr><td>Fee Type</td><td>Fee Amount</td></tr>";
                            for (var i = 0; i < $scope.defaulterdata.length; i++) {
                                if ($scope.defaulterdata[i].sims_enroll_number == $scope.filteredTodos[x].sims_enroll_number) {
                                    $scope.message_details = $scope.message_details += "<tr><td>" + $scope.edt.sims_fee_code + "</td><td style='text-align:right'>" + $scope.defaulterdata[i].final_total + "</td></tr>";
                                }
                            }
                            $scope.message_details = $scope.message_details += "<tr><td>Grand Total :</td><td style='text-align:right'>" + $scope.filteredTodos[x].final_total + "</td></tr>";
                            $scope.message_details = $scope.message_details + "</table>";

                            // $scope.filteredTodos[x].email_message = document.getElementById('text-editor').value + "<br/><br/>" + $scope.message_details;
                            var data_format = document.getElementById('text-editor').value;
                            var result_data = data_format.replace("{details}", $scope.message_details);
                            $scope.filteredTodos[x].email_message = result_data + "<br/><br/>";
                            $scope.data.push($scope.filteredTodos[x]);
                        }
                    }
                    if ($scope.data.length <= 0) {
                        swal(
                        {
                            showCloseButton: true,
                            text: 'Please Select student to send Emailsubmitdata Message.',
                            width: 350,
                            showCloseButon: true
                        });
                    }
                    else {

                        $http.post(ENV.apiUrl + "api/Fee/SFS/emailtoDefaulters_SISO", $scope.data).then(function (resl) {
                            if (resl.data == true) {
                                $('#MyModal').modal('hide');
                                $scope.email_subject = '';
                                $scope.email_message = '';
                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'Email Message Scheduled Or Sent Sucessfully.',
                                    imageUrl: "assets/img/check.png",
                                    width: 350,
                                    showCloseButon: true
                                });
                            }
                            else {
                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'Error in Sending Email Message.',
                                    imageUrl: "assets/img/notification-alert.png",
                                    width: 350,
                                    showCloseButon: true
                                });
                            }
                        });

                    }
                }
            }



            //Events End
        }])
})();



