﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Collaboration');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AlertCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$stateParams', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $stateParams) {

            $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'];
            $scope.itemsPerPage = '5';
            $scope.currentPage = 0;
            $scope.view = true;
            $scope.create = false;
            $scope.edt = [];

            // scope.alert_count = 0;
            /// alert("In CircularCont::"+$scope.circularobj.userCircularCount);
            // $scope.circularobj.userCircularCount = 44;
            //$scope.circularobj = { userCircularCount: '44' }

            //alert("$scope.circularobj.userCircularCount::" + $scope.circularobj.userCircularCgetUserCountount);
            // document.getElementById('mainform.msgs-badge').textContent = $scope.circularobj.userCircularCount;
            var username = $rootScope.globals.currentUser.username;

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

           

            var date = new Date();
            var month = (date.getMonth() + 1);
            var day = date.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            //$scope.startDate = date.getFullYear() + '-' + (month) + '-' + (day);
            //$scope.endDate = date.getFullYear() + '-' + (month) + '-' + (day);

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.setStart = function (date) {
                ////var month = date.split("/")[0];
                ////var day = date.split("/")[1];
                ////var year = date.split("/")[2];
                ////var date1 = year + "-" + month + "-" + day;
                //$scope.startDate = date;

            }

            $scope.setEnd = function (date) {
                ////var month = date.split("/")[0];
                ////var day = date.split("/")[1];
                ////var year = date.split("/")[2];
                ////var date1 = year + "-" + month + "-" + day;
                //$scope.endDate = date;
            }

            $scope.size = function (pagesize) {
                $scope.itemsPerPage = pagesize;
            }

            $http.get(ENV.apiUrl + "api/common/Alert/getModule?username=" + $rootScope.globals.currentUser.username).then(function (getModule) {
                $scope.getModule = getModule.data;
                if ($stateParams.flag=='ALL') {
                    var data1 = {
                        fromdate: '',
                        todate: $scope.endDate,
                        usercode: username,
                        alert_status: 'U',
                        module_code: null,
                    }
                    $scope.buzy = true;
                    $http.post(ENV.apiUrl + "api/common/Alert/AlertDetails", data1).then(function (AlertDetailss) {
                        $scope.AlertDetails = AlertDetailss.data;
                        if ($scope.AlertDetails.length > 0) {
                            $scope.rgvtbl = true;
                            $scope.buzy = false;

                        }
                        else {
                            $scope.rgvtbl = false;
                            $scope.buzy = false;
                            // swal({ title: "Alert", text: "Record not found", showCloseButton: true, width: 300, height: 200 });
                        }
                    });
                }
            });

            $http.get(ENV.apiUrl + "api/common/Alert/getAlertStatus").then(function (getAlertStatus) {
                $scope.getAlertStatus = getAlertStatus.data;
                $scope.edt = { alert_status: 'U' }

              //  $scope.show_alert();
            });

          
            //Search
            $scope.show_alert = function () {
                debugger;
                console.log($scope.edt);
                if ($scope.edt.module_code == undefined)
                    $scope.edt.module_code = null;
                
                var data = {
                    fromdate: $scope.startDate,
                    todate: $scope.endDate,
                    usercode: username,
                    alert_status: $scope.edt.alert_status,
                    module_code: $scope.edt.module_code,
                }
               
                $scope.buzy = true;
                $http.post(ENV.apiUrl + "api/common/Alert/AlertDetails", data).then(function (AlertDetailsd) {
                    $scope.AlertDetails = AlertDetailsd.data;
                    if ($scope.AlertDetails.length > 0) {
                        $scope.rgvtbl = true;
                        $scope.buzy = false;

                    }
                    else {
                        $scope.rgvtbl = false;
                        $scope.buzy = false;
                        swal({  text: "Record not found", imageUrl: "assets/img/close.png",showCloseButton: true, width: 300, height: 200 });
                    }

                    //                   Scope.alert_count = $scope.AlertDetails.length;

                });
            };

            $timeout(function () {
                $scope.buzy = true;
                //$scope.show_alert();
            },3000)

            $scope.show_reset = function ()
            {
                $scope.startDate = (day) + '-' + (month) + '-' + date.getFullYear();
                $scope.endDate = (day) + '-' + (month) + '-' + date.getFullYear();
                $scope.edt = "";
                $scope.rgvtbl = false;
            }



            $http.get(ENV.apiUrl + "api/common/Alert/getUserCount?user=" + $rootScope.globals.currentUser.username).then(function (AlertDetails) {
                $scope.AlertDetails = AlertDetails.data;
                $scope.alert_count = AlertDetails.data.length;
                $scope.alert_count_change(AlertDetails.data[0].unread_status);
            });

            $scope.getAlertBynumber=function(str){
                $http.get(ENV.apiUrl + "api/common/Alert/getAlertBynumber?alertnum=" + str).then(function (res) {
                    //if (res.data.length > 0)
                        $scope.mainobj = res.data;
                    $scope.alertData = res.data;
                    $scope.alert_date = $scope.alertData.alert_date;
                    $scope.module_name = $scope.alertData.module_name;
                    $scope.alert_msg = $scope.alertData.alert_msg;
                    $('#myModal').modal('show');
                    $http.get(ENV.apiUrl + "api/common/Alert/getUserCount?user=" + $rootScope.globals.currentUser.username).then(function (AlertDetails) {                       
                        $scope.alert_count = AlertDetails.data.length;
                        $scope.alert_count_change(AlertDetails.data[0].unread_status);
                    });
                    var data2 = {
                        fromdate: '',
                        todate: $scope.endDate,
                        usercode: username,
                        alert_status: 'U',
                        module_code: null,
                    }
                    $scope.buzy = true;
                    $http.post(ENV.apiUrl + "api/common/Alert/AlertDetails", data2).then(function (AlertDetailss) {
                        $scope.AlertDetails = AlertDetailss.data;
                        if ($scope.AlertDetails.length > 0) {
                            $scope.rgvtbl = true;
                            $scope.buzy = false;
                        }
                        else {
                            $scope.rgvtbl = false;
                            $scope.buzy = false;                       
                        }
                    });
                });
            }
            if ($stateParams.flag=='one') {
                $scope.getAlertBynumber($stateParams.curnum)
            }
            
            $scope.row_click = function (str) {
                $scope.mainobj = str;
                $scope.alert_date = $scope.mainobj.alert_date;
                $scope.module_name = $scope.mainobj.module_name;
                $scope.alert_msg = $scope.mainobj.alert_msg;
                $http.post(ENV.apiUrl + "api/common/Alert/UpdateAlertUnAsRead?alert_number=" + $scope.mainobj.alert_number).then(function (UpdateAlertUnAsRead) {
                    $scope.UpdateAlertUnAsRead = UpdateAlertUnAsRead.data;
                    if ($scope.UpdateAlertUnAsRead) {
                        for (var i = 0; i < $scope.AlertDetails.length; i++) {
                            if ($scope.AlertDetails[i].alert_number == $scope.mainobj.alert_number && $scope.AlertDetails[i].alert_status == 'U') {
                                $scope.AlertDetails[i].alert_status = 'R';
                                $scope.AlertDetails[i].alert_status_name = 'Read';
                            }
                        }
                    }
                    else {
                        $scope.rgvtbl = false;
                    }
                });
                $http.get(ENV.apiUrl + "api/common/Alert/getUserCount?user=" + $rootScope.globals.currentUser.username).then(function (AlertDetails) {
                    // $scope.AlertDetails = AlertDetails.data;
                    $scope.alert_count = AlertDetails.data.length;
                    $scope.alert_count_change(AlertDetails.data[0].unread_status);
                });
                $('#myModal').modal('show');
            }

            $('#text-editor').wysihtml5();

            $scope.edt1 = {
                ctitle: '',
                cdesc: '',
                cdate1: '',
                cnumber1: '',
                remark: ''
            }

            $scope.go_application = function () {
                debugger
                if ($scope.mainobj.comn_alert_appl_code != '') {
                    $('#myModal').modal('hide');
                    setTimeout(function () {
                        $state.go('main.'+ $scope.mainobj.comn_alert_appl_code)
                    }, 1000);
                }
            }

            $scope.UpdateCStatus = function (title, desc, cdate, cnumber, remark1) {
                //alert("IN UpdateCStatus::circular_number::" + cnum + "$rootScope.globals.currentUser.username::" + $rootScope.globals.currentUser.username);
                $scope.ctitle = title;
                $scope.cdesc = desc;
                $scope.cdate1 = cdate;
                $scope.cnumber1 = cnumber;
                $scope.remark = remark1;
                $scope.ctitle = title + ' (' + $scope.cdate1 + ')';
               
                $('#commnModal').modal('show');

                $('#text-editor').data("wysihtml5").editor.setValue($scope.cdesc);

                //$http.post(ENV.apiUrl + "api/common/Circular/UpdateCircularAsRead?circular_number=" + cnum + "&loggeduser=" + $rootScope.globals.currentUser.username).then(function (res) {
                //    $scope.view = true;
                //    $scope.create = false;
                //    $scope.msg1 = res.data;
                //    $rootScope.strMessage = $scope.msg1.strMessage;
                //    


                //    $http.get(ENV.apiUrl + "api/common/Circular/GetUserCirculars?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                //        $scope.create = false;
                //        $scope.view = true;
                //        $scope.obj = res.data;
                //       
                //    });
                //});
            }

            $scope.acknowledgecircular = function (cnum) {

                $http.post(ENV.apiUrl + "api/common/Circular/UpdateCircularAsRead?circular_number=" + cnum + "&loggeduser=" + $rootScope.globals.currentUser.username + "&remark=" + $scope.remark).then(function (res) {
                    $scope.view = true;
                    $scope.create = false;
                    $scope.msg1 = res.data;
                    $rootScope.strMessage = $scope.msg1.strMessage;
                   

                    $('#commnModal').modal('hide');
                    $('#text-editor').data("wysihtml5").editor.clear();
                    $http.get(ENV.apiUrl + "api/common/Circular/GetUserCirculars?username=" + $rootScope.globals.currentUser.username).then(function (res) {
                        $scope.create = false;
                        $scope.view = true;
                        $scope.obj = res.data;
                       
                    });
                });
            }

            $scope.startDate = (day) + '-' + (month) + '-' + date.getFullYear();
            $scope.endDate = (day) + '-' + (month) + '-' + date.getFullYear();

        }])
})();