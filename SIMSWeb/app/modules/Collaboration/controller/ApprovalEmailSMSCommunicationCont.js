﻿(function () {
    'use strict';
 
    var simsController = angular.module('sims.module.Collaboration');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ApprovalEmailSMSCommunicationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$stateParams', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $stateParams) {
       

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.fromData = dd + '-' + mm + '-' + yyyy;
            $scope.toDate = dd + '-' + mm + '-' + yyyy;
            $scope.showing_groupby = true;
            var typeOfSelection = [];
            $scope.email_table = false;
            $scope.sms_table = false;
            $scope.comm_table = false;
            $scope.show_email = true;
            $scope.showingEmail = function () {
                $scope.showing_groupby = true;
                $scope.typeOfSelection = 'Email';
                $scope.show_email = true;
                $scope.show_SMS = false;
                $scope.show_Comm = false;
                $scope.email_table = true;
                $scope.sms_table = false;
                $scope.comm_table = false;
                $scope.fromData = dd + '-' + mm + '-' + yyyy;
                $scope.toDate = dd + '-' + mm + '-' + yyyy;
            }
            $scope.showingSMS = function () {
                $scope.showing_groupby = false;
                $scope.typeOfSelection = 'SMS'; 
                $scope.show_email = false;
                $scope.show_SMS = true;
                $scope.show_Comm = false;
                $scope.email_table = false;
                $scope.sms_table = true;
                $scope.comm_table = false;
                $scope.fromData = dd + '-' + mm + '-' + yyyy;
                $scope.toDate = dd + '-' + mm + '-' + yyyy;
            }
            $scope.showingCommunication = function () {
                $scope.showing_groupby = false;
                $scope.typeOfSelection = 'Comm';
                $scope.show_email = false;
                $scope.show_SMS = false;
                $scope.show_Comm = true;
                $scope.email_table = false;
                $scope.sms_table = false;
                $scope.comm_table = true;
                $scope.fromData = dd + '-' + mm + '-' + yyyy;
                $scope.toDate = dd + '-' + mm + '-' + yyyy;
            }

            $scope.approvalData = function () {
                debugger;
                var temp_approvaldata = [];
                if ($scope.typeOfSelection == 'Email') {
                    for (var i = 0; i < $scope.approvalDataEmail.length; i++) {
                        var v = document.getElementById(i + 'email1');
                        if (v.checked == true) {
                            temp_approvaldata.push($scope.approvalDataEmail[i].sims_email_number)                                
                        }
                    }
                    try {
                        $http.post(ENV.apiUrl + "api/ApprovalEmailSMSCommunicationController/approvalSelectedDataEmail" , temp_approvaldata).then(function (mesg) {
                            var message = mesg.data;
                            if (message == true) {
                                swal({ text: "Approve Email Successfully.", imageUrl: "assets/img/check.png", width: 380, })
                            }
                            else {
                                swal({ text: "Approve Email Not Successfully", imageUrl: "assets/img/notification-alert.png", width: 380, });
                            }
                        })
                    } catch (e) {

                    }
                }
                if ($scope.typeOfSelection == 'SMS') {
                    for (var i = 0; i < $scope.approvalDataSMS.length; i++) {
                        var v = document.getElementById(i + 'sms1');
                        if (v.checked == true) {
                            temp_approvaldata.push($scope.approvalDataSMS[i].sims_sms_number)
                        }
                    }
                    try {
                        $http.post(ENV.apiUrl + "api/ApprovalEmailSMSCommunicationController/approvalSelectedDataSMS" , temp_approvaldata).then(function (mesg) {
                            var message = mesg.data;
                            if (message == true) {
                                swal({ text: "Approve SMS Successfully.", imageUrl: "assets/img/check.png", width: 380, })
                            }
                            else {
                                swal({ text: "Approve SMS Not Successfully", imageUrl: "assets/img/notification-alert.png", width: 380, });
                            }
                        })
                    } catch (e) {

                    }
                } 
                if ($scope.typeOfSelection == 'Comm') {

                    for (var i = 0; i < $scope.approvalDataComm.length; i++) {
                        var v = document.getElementById(i + 'com1');
                        if (v.checked == true) {
                            temp_approvaldata.push($scope.approvalDataComm[i].sims_comm_number)
                        }
                    }
                    try {
                        $http.post(ENV.apiUrl + "api/ApprovalEmailSMSCommunicationController/approvalSelectedDataCommunication" , temp_approvaldata).then(function (mesg) {
                            var message = mesg.data;
                            if (message == true) {
                                swal({ text: "Approve Communication Successfully.", imageUrl: "assets/img/check.png", width: 380, })
                            }
                            else {
                                swal({ text: "Approve Communication Not Successfully", imageUrl: "assets/img/notification-alert.png", width: 380, });
                            }
                        })
                    } catch (e) {

                    }
                }
            }
            $scope.Show_Data = function () {
                var type = [];
                try {
                    var checkBoxsubect = document.getElementById("chk_subject");
                    if (checkBoxsubect.checked == true) {
                        type = 'G';
                    }
                    var checkBoxdate = document.getElementById("chk_date");
                    if (checkBoxdate.checked == true) {
                        type = 'H';
                    }
                    var checkBoxall = document.getElementById("chk_all");
                    if (checkBoxall.checked == true) {
                        type = 'E';
                    }
                    debugger;
                    var checkBoxTo = document.getElementById("Checkbox1");
                    if (checkBoxTo.checked == true) {
                        $scope.fromData = undefined;
                    } else {

                    }
                    var checkBoxfrom = document.getElementById("Checkbox2");
                    if (checkBoxfrom.checked == true) {
                        $scope.toDate = undefined;
                    } else {

                    }
                      
                    if ($scope.typeOfSelection == 'Email') {
                        try {
                            $http.get(ENV.apiUrl + "api/ApprovalEmailSMSCommunicationController/getApprovalData?from_date=" + $scope.fromData + "&to_date=" + $scope.toDate + "&sims_sms_subject=" + $scope.subjectEmail + "&sims_recepient_id=" + $scope.emailAddress + "&type=E&operand="+type).then(function (approvalData) {
                                $scope.approvalDataEmail = approvalData.data;
                                if ($scope.approvalDataEmail.length<=0)
                                {
                                    swal({ text: "Reocrd Not Found", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                                }
                                
                            })
                        } catch (e) {

                        }
                    }
                    if ($scope.typeOfSelection == 'SMS') {
                        try {
                            type = 'S';
                            $http.get(ENV.apiUrl + "api/ApprovalEmailSMSCommunicationController/getApprovalData?from_date=" + $scope.fromData + "&to_date=" + $scope.toDate + "&sims_sms_subject=" + $scope.mobilemessage + "&sims_recepient_id=" + $scope.mobileNo + "&type=S&operand=" + type).then(function (approvalData) {
                                $scope.approvalDataSMS = approvalData.data;
                                if ($scope.approvalDataSMS.length <= 0) {
                                    swal({ text: "Reocrd Not Found", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                                }
                            })
                        } catch (e) {

                        }
                    }
                    if ($scope.typeOfSelection == 'Comm') {
                        try {
                            type = 'C';
                            $http.get(ENV.apiUrl + "api/ApprovalEmailSMSCommunicationController/getApprovalData?from_date=" + $scope.fromData + "&to_date=" + $scope.toDate + "&sims_sms_subject=" + $scope.parentID + "&sims_recepient_id=" + $scope.commMessage + "&type=C&operand=" + type).then(function (approvalData) {
                                $scope.approvalDataComm = approvalData.data;
                                if ($scope.approvalDataComm.length <= 0) {
                                    swal({ text: "Reocrd Not Found", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                                }
                            })
                        } catch (e) {

                        }
                    }

                   
                } catch (e) {

                }
            }    
            $scope.CheckAllCheckedEmail = function () {
                debugger;
                
                var main = document.getElementById('mainchkemail');
                    if (main.checked == true) {
                        for (var i = 0; i < $scope.approvalDataEmail.length; i++) {
                            var v = document.getElementById(i + 'email1');
                            v.checked = true;
                            $scope.row1 = 'row_selected';
                            $('tr').addClass("row_selected");
                        }
                    }
                    else {

                        for (var i = 0; i < $scope.approvalDataEmail.length; i++) {
                            var v = document.getElementById(i + 'email1');
                            v.checked = false;
                            main.checked = false;
                            $scope.row1 = '';
                            $('tr').removeClass("row_selected");
                        }
                    }
            }
            $scope.CheckAllCheckedSMS = function () {
                debugger;
                var main = document.getElementById('mainchksms');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.approvalDataSMS.length; i++) {
                        var v = document.getElementById( i + "sms1");
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.approvalDataSMS.length; i++) {
                        var v = document.getElementById( i + "sms1");
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }
            }
            $scope.CheckAllCheckedComm = function () {
                debugger;
                var main = document.getElementById('mainchkComm');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.approvalDataComm.length; i++) {
                        var v = document.getElementById(i + "com1");
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.approvalDataComm.length; i++) {
                        var v = document.getElementById(i + "com1");
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }
            }             
            
            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    debugger;
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }
        }])
})();