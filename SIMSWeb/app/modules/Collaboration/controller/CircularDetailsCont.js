﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Collaboration');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CircularDetailsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.edt = [];
            $scope.temp = [];
            var btnName = [];
            $scope.btn_all = false;
            $scope.btn_student = false;
            $scope.btn_parent = false;
            $scope.btn_staff = false;
            $scope.btn_system = false;
            $scope.btn_Support = false;
            $scope.btn_product = false;
            $scope.btn_visiting = false;
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.todos = [];
            $scope.obj5 = [];
            $scope.listOfCircular = [];
            $scope.size = function (str) {


                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    $scope.filteredTodos[i].icon = 'fa fa-plus-circle';
                }

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                $scope.getAccYear($scope.edt.sims_cur_code);
            });

            $scope.getAccYear = function (curCode) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                    $scope.getGrade(curCode, $scope.temp.sims_academic_year);
                });
            }
            $http.get(ENV.apiUrl + "api/common/Circular/getAllCircularUserGroup").then(function (res) {
                $scope.obj5 = res.data;
                setTimeout(function () {
                    $('#cmb_user_group').change(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });
            $(function () {
                $('#cmb_user_group').multipleSelect({
                    width: '100%'
                });
            });
            $scope.temp.sims_news_title = [];
            $scope.temp.sims_circular_user_group_code = [];
            $scope.search_data = function () {
                debugger;
                var sims_circular_title=[];
                var sims_circular_user_group_code = [];
                try {
                    for (var i = 0; i < $scope.temp.sims_news_title.length; i++) {
                        if (i == 0) {
                            sims_circular_title = $scope.temp.sims_news_title[i].sims_news_title;
                        }
                        else {
                            sims_circular_title = sims_circular_title + "," + $scope.temp.sims_news_title[i].sims_news_title;
                        }

                    }
                    for (var i = 0; i < $scope.temp.sims_circular_user_group_code.length; i++) {
                        if (i == 0) {
                            sims_circular_user_group_code = $scope.temp.sims_circular_user_group_code[i];
                        }
                        else {
                            sims_circular_user_group_code = sims_circular_user_group_code + "," + $scope.temp.sims_circular_user_group_code[i];
                        }

                    }
                } catch (e) {

                }
                try {
                    $http.get(ENV.apiUrl + "api/common/CircularDetailsController/getDetailsOfCircular?sims_academic_year=" + $scope.temp.sims_academic_year + "&sims_cur_code=" + $scope.edt.sims_cur_code + "&from_date=" + $scope.temp.sims_from_data + "&to_date=" + $scope.temp.sims_to_data + "&sims_circular_user_group_code=" + sims_circular_user_group_code + "&sims_circular_title=" + sims_circular_title).then(function (result_Data) {
                        $scope.todos = result_Data.data;
                        $scope.makeTodos();
                    });
                } catch (e) {

                }
            }
            $scope.show_List_Of_Circular = function (strData) {
                $scope.listOfCircular = [];
                try {
                    $('#cmb_list_of_circular').multipleSelect('uncheckAll');
                } catch (e) {

                }
                $(function () {
                    $('#cmb_list_of_circular').multipleSelect({
                        width: '100%'
                    });
                });
                if (strData == "News") {
                    $http.get(ENV.apiUrl + "api/common/CircularDetailsController/newsDetails?sims_academic_year=" + $scope.temp.sims_academic_year + "&sims_cur_code=" + $scope.edt.sims_cur_code ).then(function (news_data) {
                        $scope.listOfCircular = news_data.data;
                        setTimeout(function () {
                            $('#cmb_list_of_circular').change(function () {
                            }).multipleSelect({
                                width: '100%'
                            });
                        }, 1000);
                    });
                }
                else if (strData == "Circular") {
                    $http.get(ENV.apiUrl + "api/common/CircularDetailsController/circularDetails?sims_academic_year=" + $scope.temp.sims_academic_year + "&sims_cur_code=" + $scope.edt.sims_cur_code).then(function (circular_data) {
                        $scope.listOfCircular = circular_data.data;
                        setTimeout(function () {
                            $('#cmb_list_of_circular').change(function () {
                            }).multipleSelect({
                                width: '100%'
                            });
                        }, 1000);
                    });
                }

            }
            $(function () {
                $('#cmb_list_of_circular').multipleSelect({
                    width: '100%'
                });
            });
            $scope.resetFunctionality = function () {
                $scope.temp.sims_from_data = undefined;
                $scope.temp.sims_to_data = undefined;
                $scope.filteredTodos = [];
                try {
                    $('#cmb_list_of_circular').multipleSelect('uncheckAll');
                } catch (e) {

                }
                try {
                    $('#cmb_user_group').multipleSelect('uncheckAll');
                } catch (e) {

                }
            }
            $scope.clickAcknowledgment = function (data,type) {
                try {
                    $scope.circular_number_outer = data.sims_circular_number
                    $scope.sims_circular_title = data.sims_circular_title
                    $scope.click_type = type;
                    $scope.all_function($scope.circular_number_outer, type);
                    $('#MyModal').modal('show');
                } catch (e) {

                }
           
            }
            $scope.ModalClear = function () {
                $('#MyModal').modal('hide');
            }
            $scope.all_function = function (circularNo, click_type) {
                $scope.btnName = 'A';
                $scope.btn_all = true;
                $scope.btn_student = false;
                $scope.btn_parent = false;
                $scope.btn_staff = false;
                $scope.btn_system = false;
                $scope.btn_Support = false;
                $scope.btn_product = false;
                $scope.btn_visiting = false;
                $scope.dataAcknowledgment_all = [];
                
                try {
                    $http.get(ENV.apiUrl + "api/common/CircularDetailsController/deailsacknowledgment?sims_circular_number=" + circularNo + "&type=X" + "&status="+click_type).then(function (circular_data) {
                        $scope.dataAcknowledgment_all = circular_data.data;
                    });
                } catch (e) {

                }
                
            }   
            $scope.student_function = function (circularNo, click_type) {
                $scope.btnName = 's';
                $scope.btn_all = false;
                $scope.btn_student = true;
                $scope.btn_parent = false;
                $scope.btn_staff = false;
                $scope.btn_system = false;
                $scope.btn_Support = false;
                $scope.btn_product = false;
                $scope.btn_visiting = false;
                $scope.dataAcknowledgment_Student = [];
                click_type = click_type + '/' + $scope.edt.sims_cur_code + '/' + $scope.temp.sims_academic_year;
                try {
                    $http.get(ENV.apiUrl + "api/common/CircularDetailsController/deailsacknowledgment?sims_circular_number=" + circularNo + "&type=P" + "&status=" + click_type).then(function (circular_data) {
                        $scope.dataAcknowledgment_Student = circular_data.data;
                    });
                } catch (e) {

                }
                
            }
            $scope.parent_function = function (circularNo, click_type) {
                $scope.btnName = 'p';
                $scope.btn_all = false;
                $scope.btn_student = false;
                $scope.btn_parent = true;
                $scope.btn_staff = false;
                $scope.btn_system = false;
                $scope.btn_Support = false;
                $scope.btn_product = false;
                $scope.btn_visiting = false;
                $scope.dataAcknowledgment_parent = [];
                click_type = click_type + '/' + $scope.edt.sims_cur_code + '/' + $scope.temp.sims_academic_year;
                try {
                    $http.get(ENV.apiUrl + "api/common/CircularDetailsController/deailsacknowledgment?sims_circular_number=" + circularNo + "&type=Q" + "&status=" + click_type).then(function (circular_data) {
                        $scope.dataAcknowledgment_parent = circular_data.data;
                    });
                } catch (e) {

                }
            }
            $scope.staff_function = function (circularNo, click_type) {
                $scope.btnName = 'staff'; 
                $scope.btn_all = false;
                $scope.btn_student = false;
                $scope.btn_parent = false;
                $scope.btn_staff = true;
                $scope.btn_system = false;
                $scope.btn_Support = false;
                $scope.btn_product = false;
                $scope.btn_visiting = false;
                $scope.dataAcknowledgment_staff = [];
                try {
                    $http.get(ENV.apiUrl + "api/common/CircularDetailsController/deailsacknowledgment?sims_circular_number=" + circularNo + "&type=R" + "&status=" + click_type).then(function (circular_data) {
                        $scope.dataAcknowledgment_staff = circular_data.data;
                    });
                } catch (e) {

                }
            }
            $scope.system_function = function (circularNo, click_type) {
                $scope.btnName = 'sys';
                $scope.btn_all = false;
                $scope.btn_student = false;
                $scope.btn_parent = false;
                $scope.btn_staff = false;
                $scope.btn_system = true;
                $scope.btn_Support = false;
                $scope.btn_product = false;
                $scope.btn_visiting = false;
                $scope.dataAcknowledgment_system = [];
                try {
                    $http.get(ENV.apiUrl + "api/common/CircularDetailsController/deailsacknowledgment?sims_circular_number=" + circularNo + "&type=T" + "&status=" + click_type).then(function (circular_data) {
                        $scope.dataAcknowledgment_system = circular_data.data;
                    });
                } catch (e) {

                }
            }
            $scope.support_function = function (circularNo, click_type) {
                $scope.btnName = 'sup';
                $scope.btn_all = false;
                $scope.btn_student = false;
                $scope.btn_parent = false;
                $scope.btn_staff = false;
                $scope.btn_system = false;
                $scope.btn_Support = true;
                $scope.btn_product = false;
                $scope.btn_visiting = false;
                $scope.dataAcknowledgment_Support = [];
                try {
                    $http.get(ENV.apiUrl + "api/common/CircularDetailsController/deailsacknowledgment?sims_circular_number=" + circularNo + "&type=U" + "&status=" + click_type).then(function (circular_data) {
                        $scope.dataAcknowledgment_Support = circular_data.data;
                    });
                } catch (e) {

                }
                
            }
            $scope.product_function = function (circularNo, click_type) {
                $scope.btnName = 'pro'; 
                $scope.btn_all = false;
                $scope.btn_student = false;
                $scope.btn_parent = false;
                $scope.btn_staff = false;
                $scope.btn_system = false;
                $scope.btn_Support = false;
                $scope.btn_product = true;
                $scope.btn_visiting = false;
                $scope.dataAcknowledgment_product = [];
                try {
                    $http.get(ENV.apiUrl + "api/common/CircularDetailsController/deailsacknowledgment?sims_circular_number=" + circularNo + "&type=V" + "&status=" + click_type).then(function (circular_data) {
                        $scope.dataAcknowledgment_product = circular_data.data;
                    });
                } catch (e) {

                }
            }
            $scope.visiting_function = function (circularNo, click_type) {
                $scope.btnName = 'vis';
                $scope.btn_all = false;
                $scope.btn_student = false;
                $scope.btn_parent = false;
                $scope.btn_staff = false;
                $scope.btn_system = false;
                $scope.btn_Support = false;
                $scope.btn_product = false;
                $scope.btn_visiting = true;
                $scope.dataAcknowledgment_visiting = [];    
                try {
                    $http.get(ENV.apiUrl + "api/common/CircularDetailsController/deailsacknowledgment?sims_circular_number=" + circularNo + "&type=W" + "&status=" + click_type).then(function (circular_data) {
                        $scope.dataAcknowledgment_visiting = circular_data.data;
                    });
                } catch (e) {

                }
            }
            
            $scope.exportData = function () {
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        if ($scope.btnName == 'A') {
                            var blob = new Blob([document.getElementById('all_data').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, "CircularDetails.xls");
                        }
                        if ($scope.btnName == 's') {
                            var blob = new Blob([document.getElementById('student_data').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, "CircularDetails.xls");

                        }
                        if ($scope.btnName == 'p') {
                            var blob = new Blob([document.getElementById('parent_data').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, "CircularDetails.xls");

                        }
                        if ($scope.btnName == 'staff') {
                            var blob = new Blob([document.getElementById('staff_data').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, "CircularDetails.xls");

                        }
                        if ($scope.btnName == 'sys') {
                            var blob = new Blob([document.getElementById('system_data').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, "CircularDetails.xls");

                        }
                        if ($scope.btnName == 'sup') {
                            var blob = new Blob([document.getElementById('support_data').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, "CircularDetails.xls");

                        }
                        if ($scope.btnName == 'pro') {
                            var blob = new Blob([document.getElementById('product_data').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, "CircularDetails.xls");

                        }
                        if ($scope.btnName == 'vis') {
                            var blob = new Blob([document.getElementById('visit_data').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, "CircularDetails.xls");

                        }                      
                        //alasql('SELECT * INTO XLSX("CirccularDetails.xlsx",{headers:true})\FROM HTML("#all_data",{headers:true,skipdisplaynone:true})');                       
                      
                    }
                });
            };

        }])
})();