﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Collaboration');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CommunicationToDefaulters_SokController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            $scope.maingrid = true;
            $('*[data-datepicker="true"] input[type="text"]').datepicker(
            {
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });
            $scope.display = true;
            $scope.pagesize = '50';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.Emailbtn = false;
            $scope.Communicatebtn = true;

            //if ($http.defaults.headers.common['schoolId'] == 'sok' || $http.defaults.headers.common['schoolId'] == 'sis' || $http.defaults.headers.common['schoolId'] == 'tiadxb' || $http.defaults.headers.common['schoolId'] == 'tiashj') {
            //    $scope.Emailbtn = false;
            //    $scope.Communicatebtn = true;
            //}

            /* PAGER FUNCTIONS*/
            $scope.edt = [];
            $scope.ifvalue = true;

            $('#text-editor').wysihtml5();
            $('#text-editor1').wysihtml5();

            $('#cmbViewList').multipleSelect({
                width: '100%',
                placeholder: "Please Select"
            });
            $('#stud_status').multipleSelect({ width: '100%' });

            $scope.countData = [
                 { val: 10, data: 10 },
                 { val: 20, data: 20 }
            ]

            $scope.cur_change = function () {
                $http.get(ENV.apiUrl + "api/CommDefaulter/GetAllAcademicYears?cur_code=" + $scope.edt.sims_cur_code).then(function (admissionYear) {
                    $scope.admissionYear = admissionYear.data;
                    console.log($scope.admissionYear);
                    for (var i = 0; i < $scope.admissionYear.length; i++) {
                        if ($scope.admissionYear[i].sims_academic_year_status == 'C') {
                            //   $scope.edt['sims_cur_code'] = cur_values;
                            $scope.edt['sims_academic_year'] = $scope.admissionYear[i].sims_academic_year;
                        }
                    }
                    $scope.acdm_yr_change();
                });
            }


            $http.get(ENV.apiUrl + "api/CommDefaulter/GetDefaulterEmailMessage").then(function (email_temp) {
                $scope.email_temp = email_temp.data;
                //  document.getElementById('text-editor').innerHTML = $scope.email_temp;
                var $ta = $('textarea#text-editor');
                var w5ref = $ta.data('wysihtml5');
                if (w5ref) {
                    w5ref.editor.setValue($scope.email_temp);
                }
            });

            //$http.get(ENV.apiUrl + "api/CommDefaulter/GetDefaulterAlertMessage").then(function (email_temp) {
            //    $scope.email_temp = email_temp.data;

            //    //Alert CODE
            //    //$scope.sims_smstext = $scope.email_temp;
            //    //$scope.sims_sms_msg = "Enter your message below ( max 1000 chars.1 sms is 160 characters.)";
            //    //$scope.sims_sms_subjectblock = "These special characters ' ][}{|~^ ' will be treated as two character";
            //});

            $http.get(ENV.apiUrl + "api/CommDefaulter/GetAllCurName").then(function (admissionCur) {
                $scope.admissionCur = admissionCur.data;
                if (admissionCur.data.length > 0) {
                    $scope.edt['sims_cur_code'] = admissionCur.data[0].sims_cur_code;
                    $scope.cur_change($scope.edt['sims_cur_code']);
                }
            });

            $http.get(ENV.apiUrl + "api/CommDefaulter/GetDefaulterfeeTypes").then(function (feetypes) {
                $scope.feetypes = feetypes.data;
                setTimeout(function () {
                    $('#cmb_feeTypes').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);

            });

            $http.get(ENV.apiUrl + "api/CommDefaulter/GetDefaulterviewList").then(function (listtype) {
                $scope.listtype = listtype.data;
            });

            $http.get(ENV.apiUrl + "api/CommDefaulter/GetDefaulterviewMode").then(function (listmode) {
                $scope.listmode = listmode.data;
            });

            $scope.acdm_yr_change = function () {
                $http.get(ENV.apiUrl + "api/CommDefaulter/GetAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year).then(function (admissionGrade) {
                    $scope.admissionGrade = admissionGrade.data;
                    setTimeout(function () {
                        $('#cmb_grade').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });

                //$http.get(ENV.apiUrl + "api/CommDefaulter/GetDefaulterTerms?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (terms) {
                //    $scope.terms = terms.data;
                //});
            }

            $scope.grades = '';
            $scope.grade_change = function () {
                $scope.grades = '';
                try {
                    $("#cmb_section").multipleSelect("uncheckAll");
                }
                catch (e) { }
                for (var i = 0; i < $scope.edt.sims_grade_code.length; i++)
                    $scope.grades = $scope.grades + ($scope.edt.sims_grade_code[i]) + ',';

                $http.get(ENV.apiUrl + "api/CommDefaulter/GetDefaulterSection?cur_code=" + $scope.edt.sims_cur_code + "&ac_year=" + $scope.edt.sims_academic_year + "&g_code=" + $scope.grades).then(function (admissionsection) {
                    $scope.admissionsection = admissionsection.data;
                    console.log(admissionsection.data);
                    setTimeout(function () {
                        $('#cmb_section').change(function () {
                        }).multipleSelect({ width: '100%' });


                    }, 1000);

                });
            }

            $scope.getStudentStatus = function () {
                $http.get(ENV.apiUrl + "api/CommDefaulter/GetStudentStatus").then(function (result) {
                    $scope.studentStatusList = result.data;
                    setTimeout(function () {
                        $('#stud_status').change(function () {
                        }).multipleSelect({ width: '100%' });
                    }, 1000);

                });
            }
            $scope.getStudentStatus();



            $scope.btnPreview_click = function () {

                if ($scope.edt.sims_grade_code != "" && $scope.edt.sims_grade_code && $scope.edt.sims_section_code != "" && $scope.edt.sims_section_code && $scope.edt.sims_fee_code != "" && $scope.edt.sims_fee_code && $scope.edt.duration != "" && $scope.edt.duration && $scope.edt.sims_term_code != "" && $scope.edt.sims_term_code) {
                    $scope.msg1 = false;

                    $('#loader').modal({ backdrop: 'static', keyboard: false });

                    debugger;
                    $scope.grades = '';
                    $scope.sections = '';
                    $scope.sims_fee_code = '';
                    $scope.sims_term_code1 = '';
                    $scope.sims_student_status = '';

                    for (var i = 0; i < $scope.edt.sims_grade_code.length; i++)
                        $scope.grades = $scope.grades + ($scope.edt.sims_grade_code[i]) + ',';

                    for (var i = 0; i < $scope.edt.sims_section_code.length; i++)
                        $scope.sections = $scope.sections + ($scope.edt.sims_section_code[i]) + ',';

                    for (var i = 0; i < $scope.edt.sims_fee_code.length; i++)
                        $scope.sims_fee_code = $scope.sims_fee_code + ($scope.edt.sims_fee_code[i]) + ',';

                    $scope.edt1 = { 'sims_term_code': '' };
                    $scope.edt1.sims_cur_code = $scope.edt.sims_cur_code;
                    $scope.edt1.sims_academic_year = $scope.edt.sims_academic_year;
                    $scope.edt1.sims_grade_code = $scope.grades;
                    $scope.edt1.sims_section_code = $scope.sections;
                    $scope.edt1.sims_fee_code = $scope.sims_fee_code;
                    $scope.edt1.sims_view_list_code = $scope.edt.sims_view_list_code;
                    if ($scope.edt.duration == '2') {
                        $scope.edt.sims_view_mode_code = '2';
                    }
                    else if ($scope.edt.duration == '3') {
                        $scope.edt.sims_view_mode_code = '3';
                    }
                    $scope.edt1.sims_view_mode_code = $scope.edt.sims_view_mode_code;
                    //$scope.edt1.sims_term_code = $scope.edt.sims_term_code;

                    for (var i = 0; i < $scope.edt.sims_term_code.length; i++)
                        $scope.sims_term_code1 = $scope.sims_term_code1 + ($scope.edt.sims_term_code[i]) + ',';

                    $scope.edt1.sims_term_code = $scope.sims_term_code1;

                    if ($scope.edt.sims_search != undefined)
                        $scope.edt1.sims_search = $scope.edt.sims_search;
                    else
                        $scope.edt1.sims_search = undefined;

                    if ($scope.edt.sims_amount != undefined)
                        $scope.edt1.sims_amount = $scope.edt.sims_amount;
                    else
                        $scope.edt1.sims_amount = undefined;

                    for (var i = 0; i < $scope.edt.sims_academic_year_status.length; i++) {
                        $scope.sims_student_status = $scope.sims_student_status + ($scope.edt.sims_academic_year_status[i]) + ',';
                    }
                    $scope.edt1.sims_student_status = $scope.sims_student_status;

                    $http.post(ENV.apiUrl + "api/CommDefaulter/DefaultersList1", $scope.edt1).then(function (defaulterdata) {
                        $scope.defaulterdata = defaulterdata.data;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.defaulterdata.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.defaulterdata.length, data: 'All' })
                        }

                        $scope.summeryData = $scope.defaulterdata[1];
                        if ($scope.defaulterdata[0].length <= 0) {
                            $('#loader').modal('hide');
                            swal(
                            {
                                showCloseButton: true,
                                text: 'No Data Found',
                                width: 350,
                                showCloseButon: true
                            });
                        }
                        else {
                            $('#loader').modal('hide');

                            if ($scope.summeryData.length > 0) {


                                $scope.pager = true;
                                if ($scope.countData.length > 3) {
                                    $scope.countData.splice(3, 1);
                                    $scope.countData.push({ val: $scope.summeryData.length, data: 'All' })
                                }
                                else {
                                    $scope.countData.push({ val: $scope.summeryData.length, data: 'All' })
                                }

                                $scope.totalItems = $scope.summeryData.length;
                                $scope.todos = $scope.summeryData;
                                $scope.makeTodos();
                            }
                        }
                    });
                }
                else {
                    $scope.msg1 =

                        swal(
                        {
                            showCloseButton: true,
                            text: 'Please select Grade, Section, Fee types, Duration and Term',
                            imageUrl: "assets/img/notification-alert.png",
                            width: 350,
                            showCloseButon: true
                        });

                }

            }
            $scope.printData = function (info) {

            }
            $scope.reset = function () {
                $scope.admissions = [];
                $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 50, $scope.maxSize = 50;
                $scope.pager = false;

                $scope.defaulterdata = [];
                $scope.edt = { 'sims_cur_code': $scope.edt.sims_cur_code, 'sims_academic_year': $scope.edt.sims_academic_year }
                $scope.acdm_yr_change();
                $scope.edt.sims_start_date = '';
                $scope.edt.sims_end_date = '';
                $scope.edt.admission_no = '';
                $scope.email_subject = '';
                $('#text-editor').data("wysihtml5").editor.clear();
                $('#text-editor1').data("wysihtml5").editor.clear();

                try {
                    $("#cmb_grade").multipleSelect("uncheckAll");
                }
                catch (e) { }
                try {
                    $("#cmb_section").multipleSelect("uncheckAll");
                }
                catch (e) { } try {
                    $("#cmb_feeTypes").multipleSelect("uncheckAll");
                }
                catch (e) { }
                try {
                    $('#cmbViewList').multipleSelect('uncheckAll');
                } catch (e) {
                }
                try {
                    $('#stud_status').multipleSelect('uncheckAll');
                } catch (e) {
                }

            }

            $scope.message = function () {
                $('#MyModal10').modal({ backdrop: 'static', keyboard: true });
            }
            $(function () {
                $('#cmb_grade').multipleSelect({ width: '100%' });
                $('#cmb_section').multipleSelect({ width: '100%' });
                $('#cmb_feeTypes').multipleSelect({ width: '100%' });

            });

            /* PAGER FUNCTIONS*/

            //$scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 25, $scope.maxSize = 7, $scope.currentPage_ind = 0;
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 50, $scope.maxSize = 50;

            //$scope.makeTodos = function () {
            //    $scope.currentPage_ind = 0;

            //    if ((Math.round($scope.totalItems / $scope.numPerPage) * $scope.numPerPage) > $scope.totalItems)
            //        $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage);
            //    else
            //        $scope.pagersize = Math.round($scope.totalItems / $scope.numPerPage) + 1;

            //    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
            //    var end = parseInt(begin) + parseInt($scope.numPerPage);
            //    $scope.filteredTodos = $scope.todos.slice(begin, end);
            //    var main = document.getElementById('chkSearch');
            //    main.checked = false;
            //    $scope.CheckMultipleSearch();
            //};

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            //$scope.size = function (str) {

            //    if (str == 25 || str == 50 || str == 75) {
            //        $scope.pager = true;
            //    }
            //    else {
            //        $scope.pager = false;
            //    }

            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.currentPage_ind = str;
            //    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}

            //$scope.index = function (str) {
            //    $scope.pageindex = str;
            //    $scope.currentPage_ind = str;
            //    $scope.currentPage = str;
            //    console.log("currentPage=" + $scope.currentPage);
            //    console.log("PageNumber=" + $scope.numPerPage);


            //    $scope.makeTodos();
            //}

            $scope.size = function (str) {

                //if (str == 25 || str == 50 || str == 75) {
                //    $scope.pager = true;
                //}
                //else {
                //    $scope.pager = false;
                //}

                //$scope.pagesize = str;
                //$scope.currentPage = 1;

                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                // $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }



            //Search
            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            debugger;
            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.defaulterdata, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.defaulterdata;
                }
                $scope.makeTodos();
            }
            //Search
            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_father_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_enroll_number == toSearch) ? true : false;
            }






            /* PAGER FUNCTIONS*/

            $scope.CheckMultipleSearch = function () {
                var main = document.getElementById('chkSearch');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if (main.checked == true) {
                        $scope.filteredTodos[i].isSelected = true;
                    }
                    else if (main.checked == false) {
                        $scope.row1 = '';
                        $scope.color = '#edefef';
                        main.checked = false;
                        $scope.filteredTodos[i].isSelected = false;
                    }
                }
            }

            $scope.CheckOneByOneSearch = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                    }
                });

                var main = document.getElementById('chkSearch');
                if (main.checked == true) {
                    main.checked = false;
                    //$scope.color = '#edefef';
                }

            }


            //SMS Code
            $scope.sms = function () {
                $scope.sims_smstext = '';
                $http.get(ENV.apiUrl + "api/CommDefaulter/GetDefaulterSMSMessage").then(function (email_temp) {
                    $scope.email_temp = email_temp.data;

                    //SMS CODE
                    $scope.sims_smstext = $scope.email_temp;
                    $scope.sims_sms_msg = "Enter your message below ( max 1000 chars.1 sms is 160 characters.)";
                    $scope.sims_sms_subjectblock = "These special characters ' ][}{|~^ ' will be treated as two character";

                    $scope.msg_details();
                });

                $('#SMS_Modal').modal({ backdrop: 'static', keyboard: true });
            }

            $http.get(ENV.apiUrl + "api/CommDefaulter/GetDefaulterSMSMessage").then(function (email_temp) {
                $scope.email_temp = email_temp.data;

                //SMS CODE
                $scope.sims_smstext = $scope.email_temp;
                $scope.sims_sms_msg = "Enter your message below ( max 1000 chars.1 sms is 160 characters.)";
                $scope.sims_sms_subjectblock = "These special characters ' ][}{|~^ ' will be treated as two character";

                $scope.msg_details();
            });

            $http.get(ENV.apiUrl + "api/CommDefaulter/GetDefaulterAlertMessage").then(function (email_temp) {
                $scope.sms_temp = email_temp.data;
                $scope.sims_alerttext = $scope.sms_temp;
            });

            $scope.msg_details = function () {
                debugger;
                if ($scope.sims_smstext != "" && $scope.sims_smstext != undefined) {
                    var q = 0, cnt = 0;
                    $scope.sims_sms_char = 0;

                    var v = document.getElementById('txt_smstext');
                    var r = $scope.sims_smstext.length;//v.value.length;
                    var sms = $scope.sims_smstext;
                    //console.log(v.value.length);

                    var nameList = new Array('[', ']', '{', '}');
                    if (sms != "") {
                        for (var x = 0, c = ''; c = sms.charAt(x) ; x++) {
                            for (var i = 0; i < nameList.length; i++) {
                                if (nameList[i] === c) {
                                    q = q + 1;
                                }
                            }
                        }
                        var ss = q * 2;
                        var smsLen = r;
                        cnt = (smsLen - q) + ss;
                        $scope.sims_sms_char = cnt;
                    }

                    if ($scope.sims_sms_char <= 160 && $scope.sims_sms_char > 0)
                        $scope.sims_sms_length = "1";
                    if ($scope.sims_sms_char <= 320 && $scope.sims_sms_char > 160)
                        $scope.sims_sms_length = "2";
                    if ($scope.sims_sms_char <= 480 && $scope.sims_sms_char > 320)
                        $scope.sims_sms_length = "3";
                    if ($scope.sims_sms_char <= 640 && $scope.sims_sms_char > 480)
                        $scope.sims_sms_length = "4";
                    if ($scope.sims_sms_char <= 800 && $scope.sims_sms_char > 640)
                        $scope.sims_sms_length = "5";
                    if ($scope.sims_sms_char <= 960 && $scope.sims_sms_char > 800)
                        $scope.sims_sms_length = "6";
                    if ($scope.sims_sms_char <= 1000 && $scope.sims_sms_char > 960)
                        $scope.sims_sms_length = "7";
                }
                else {
                    $scope.sims_sms_char = "0";
                    $scope.sims_sms_length = "0";
                }
            }

            $scope.submitdata_sms = function () {
                debugger;
                if ($scope.sims_smstext == undefined || document.getElementById('txt_smstext').value == undefined) {
                    swal(
                    {
                        showCloseButton: true,
                        text: 'Please Define Message',
                        imageUrl: "assets/img/notification-alert.png",
                        width: 350,
                        showCloseButon: true
                    });
                }
                else {
                    $scope.data = [];

                    for (var x = 0; x < $scope.filteredTodos.length; x++) {
                        if ($scope.filteredTodos[x].isSelected == true && $scope.filteredTodos[x].parent_mobile != '') {
                            $scope.filteredTodos[x].sims_cur_code = $scope.edt.sims_cur_code;
                            $scope.filteredTodos[x].sims_academic_year = $scope.edt.sims_academic_year;
                            $scope.filteredTodos[x].email_subject = $scope.email_subject;
                            $scope.filteredTodos[x].email_message = $scope.sims_smstext;
                            $scope.data.push($scope.filteredTodos[x]);
                        }
                    }
                    console.log($scope.data);
                    if ($scope.data.length <= 0) {
                        swal(
                        {
                            showCloseButton: true,
                            text: 'Please Select student to send SMS Message.',
                            imageUrl: "assets/img/notification-alert.png",
                            width: 350,
                            showCloseButon: true
                        });
                    }
                    else {
                        console.log($scope.data);

                        $http.post(ENV.apiUrl + "api/CommDefaulter/smstoDefaulters", $scope.data).then(function (resl) {
                            if (resl.data == true) {
                                $('#SMS_Modal').modal('hide');
                                $scope.sims_smstext = '';
                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'SMS Scheduled Or Sent Sucessfully.',
                                    imageUrl: "assets/img/check.png",
                                    width: 350,
                                    showCloseButon: true
                                });
                            }
                            else {
                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'Error in Sending SMS Message.',
                                    imageUrl: "assets/img/close.png",
                                    width: 350,
                                    showCloseButon: true
                                });
                            }
                        });

                    }
                    console.log($scope.data);
                }
            }

            $scope.resetdata_sms = function () {
                $scope.sims_smstext = '';
                $scope.msg_details();
            }

            //Alert Code
            $scope.alert = function () {
                debugger;
                $scope.sims_alerttext = '';
                $http.get(ENV.apiUrl + "api/CommDefaulter/GetDefaulterAlertMessage").then(function (email_temp) {
                    $scope.sms_temp = email_temp.data;
                    $scope.sims_alerttext = $scope.sms_temp;
                });

                $('#Alert_Modal').modal({ backdrop: 'static', keyboard: true });
            }

            $scope.submitdata_alert = function () {
                debugger;
                if ($scope.sims_alerttext == undefined || document.getElementById('sims_alerttext').value == undefined) {
                    swal(
                    {
                        showCloseButton: true,
                        text: 'Please Define Message',
                        imageUrl: "assets/img/check.png",
                        width: 350,
                        showCloseButon: true
                    });
                }
                else {
                    $scope.data = [];

                    for (var x = 0; x < $scope.filteredTodos.length; x++) {
                        if ($scope.filteredTodos[x].isSelected == true && $scope.filteredTodos[x].sims_sibling_parent_number != '') {
                            $scope.filteredTodos[x].sims_cur_code = $scope.edt.sims_cur_code;
                            $scope.filteredTodos[x].sims_academic_year = $scope.edt.sims_academic_year;
                            $scope.filteredTodos[x].email_subject = $scope.email_subject;
                            $scope.filteredTodos[x].email_message = $scope.sims_smstext;
                            $scope.data.push($scope.filteredTodos[x]);
                        }
                    }
                    console.log($scope.data);
                    if ($scope.data.length <= 0) {
                        swal(
                        {
                            showCloseButton: true,
                            text: 'Please Select student to send Alert Message.',
                            imageUrl: "assets/img/notification-alert.png",
                            width: 350,
                            showCloseButon: true
                        });
                    }
                    else {
                        console.log($scope.data);

                        $http.post(ENV.apiUrl + "api/CommDefaulter/alerttoDefaulters", $scope.data).then(function (resl) {
                            if (resl.data == true) {
                                $('#Alert_Modal').modal('hide');
                                $scope.sims_alerttext = '';
                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'Alert Scheduled Or Sent Sucessfully.',
                                    imageUrl: "assets/img/check.png",
                                    width: 350,
                                    showCloseButon: true
                                });
                            }
                            else {
                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'Error in Sending Alert Message.',
                                    imageUrl: "assets/img/notification-alert.png",
                                    width: 350,
                                    showCloseButon: true
                                });
                            }
                        });

                    }
                    console.log($scope.data);
                }
            }

            $scope.resetdata_alert = function () {
                $scope.sims_alerttext = '';
            }

            //Alert Code
            $scope.block_ppn = function () {
                debugger;
                $scope.sims_blocktext = '';
                $http.get(ENV.apiUrl + "api/CommDefaulter/GetDefaulterBlockPPNMessage").then(function (email_temp) {
                    $scope.sms_temp = email_temp.data;
                    $scope.sims_blocktext = $scope.sms_temp;
                });

                $('#BlockPPN_Modal').modal({ backdrop: 'static', keyboard: true });
            }

            $scope.submitdata_block = function () {
                debugger;
                if ($scope.sims_blocktext == undefined || document.getElementById('sims_blocktext').value == undefined) {
                    swal(
                    {
                        showCloseButton: true,
                        text: 'Please Define Message',
                        imageUrl: "assets/img/check.png",
                        width: 350,
                        showCloseButon: true
                    });
                }
                else {
                    $scope.data = [];

                    for (var x = 0; x < $scope.filteredTodos.length; x++) {
                        if ($scope.filteredTodos[x].isSelected == true && $scope.filteredTodos[x].sims_sibling_parent_number != '') {
                            $scope.filteredTodos[x].sims_cur_code = $scope.edt.sims_cur_code;
                            $scope.filteredTodos[x].sims_academic_year = $scope.edt.sims_academic_year;
                            $scope.filteredTodos[x].email_subject = $scope.email_subject;
                            $scope.filteredTodos[x].email_message = $scope.sims_blocktext;
                            $scope.data.push($scope.filteredTodos[x]);
                        }
                    }
                    console.log($scope.data);
                    if ($scope.data.length <= 0) {
                        swal(
                        {
                            showCloseButton: true,
                            text: 'Please Select student to send Message.',
                            imageUrl: "assets/img/notification-alert.png",
                            width: 350,
                            showCloseButon: true
                        });
                    }
                    else {
                        console.log($scope.data);

                        $http.post(ENV.apiUrl + "api/CommDefaulter/BlockDefaulters", $scope.data).then(function (resl) {
                            if (resl.data == true) {
                                $('#Alert_Modal').modal('hide');
                                $scope.sims_alerttext = '';
                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'Message Scheduled Or Sent Sucessfully.',
                                    imageUrl: "assets/img/check.png",
                                    width: 350,
                                    showCloseButon: true
                                });
                            }
                            else {
                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'Error in Sending Alert Message.',
                                    imageUrl: "assets/img/notification-alert.png",
                                    width: 350,
                                    showCloseButon: true
                                });
                            }
                        });

                    }
                    console.log($scope.data);
                }
            }

            $scope.resetdata_block = function () {
                $scope.BlockDefaulters = '';
            }

            //Events End

            //New Methods
            $scope.getduration = function (str) {
                debugger;
                $scope.ifvalue = true;

                if (str == '1') {
                    $scope.ifvalue = true;
                }

                else if (str == '2') {
                    $scope.ifvalue = false;
                    $http.get(ENV.apiUrl + "api/CommDefaulter/GetDefaulterTerms?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (terms) {
                        $scope.terms = terms.data;
                    });

                }

                else if (str == '3') {
                    $scope.ifvalue = false;
                    $http.post(ENV.apiUrl + "api/CommDefaulter/Year_month?year=" + $scope.edt.sims_academic_year).then(function (Year_month) {
                        $scope.Year_month = Year_month.data;
                        debugger;
                        var d = new Date().getFullYear();
                        var n = new Date().getMonth() + 1;
                        var ym = d + '' + n;

                        //$scope.edt = { year_month: ym };

                        $scope.terms = Year_month.data;
                    });
                }

                setTimeout(function () {
                    $('#cmbViewList').change(function () {
                        console.log($(this).val());
                        //grade = $(this).val();
                    }).multipleSelect({
                        width: '100%',
                    });
                    //$("#cmbViewList").multipleSelect("uncheckAll");
                }, 1000);

                $scope.pager = false;
                $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 50, $scope.maxSize = 50;

            }
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };



            $scope.Cancel = function () {
                $scope.view = "";
                $scope.maingrid1 = true;
                $scope.Screening = false;
                $scope.Upload_doc = false;
                $scope.div_Communication = false;
                $scope.div_CommunicationHist = false;

            }

            //HTML5 editor
            var admdetails = [];
            $scope.getCommunicate = function () {
                debugger;
                console.log('ppp');
                admdetails = [];
                var adm_data = [];

                debugger;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    // var v = document.getElementById($scope.dash_data[i].admission_number);
                    // if ($scope.dash_data[i].admission_number1 == true)

                    var t = $scope.filteredTodos[i].sims_enroll_number;
                    var v = document.getElementById(t);

                    if (v.checked == true) {
                        adm_data.push($scope.filteredTodos[i].sims_enroll_number);
                        admdetails = admdetails + $scope.filteredTodos[i].sims_enroll_number + ',';
                    }
                }


                //if (adm_data.length <= 1)
                // {
                console.log(admdetails);
                //$scope.div_Communication = true;
                // $scope.maingrid = false;
                $('#MyModalNew').modal({ backdrop: 'static', keyboard: true });

                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetAdmissionTemplates").then(function (res) {
                    $scope.template_data = res.data;
                });

                $http.get(ENV.apiUrl + "api/CommDefaulter/GetAdmission_EmailIds?admission_nos=" + admdetails).then(function (res) {
                    $scope.emailId_data = res.data;
                });

            }

            setTimeout(function () {
                $('#text_editor_new').wysihtml5();
            }, 20000);

            $scope.getbody = function (msg_type) {
                $http.get(ENV.apiUrl + "api/common/AdmissionDashboard/GetAdmissionTemplatesBody?template_subject=" + msg_type).then(function (res) {
                    $scope.tempBody_data = res.data;
                    var body = $scope.tempBody_data.sims_msg_body + ' ' + $scope.tempBody_data.sims_msg_signature;
                    var v = document.getElementById('text_editor_new');
                    v.value = body;
                    $scope.email.msgbody = v.value;
                    $scope.flag = false;
                    console.log($scope.tempBody_data.sims_msg_sr_no);
                    $('#text_editor_new').data("wysihtml5").editor.setValue($scope.email.msgbody);

                    $http.get(ENV.apiUrl + "api/common/Email/GetcheckEmailProfile?sr_no=" + $scope.tempBody_data.sims_msg_sr_no).then(function (res) {
                        $scope.emailProfile_data = res.data;
                        console.log($scope.emailProfile_data);
                    });
                });
            }
            $scope.commnModaldisplay = function () {
                $scope.email = [];
                $scope.emailId_data = [];
                $scope.maingrid1 = true;
                // $('#commnModal').modal('hide');
                $('#text_editor_new').data("wysihtml5").editor.clear();
                $scope.div_Communication = false;
            }

            var data1 = [];
            var lst_cc = [];
            $scope.Admcomm_data = [];


            //$scope.email_subject == undefined ||
            $scope.submitdatanew = function () {
                debugger;
                if (document.getElementById('text-editor1').value == undefined) {
                    swal(
                    {
                        showCloseButton: true,
                        text: 'Please Define Message Or Subject',
                        imageUrl: "assets/img/notification-alert.png",
                        width: 350,
                        showCloseButon: true
                    });
                }
                else {
                    $scope.data = [];

                    for (var x = 0; x < $scope.filteredTodos.length; x++) {
                        if ($scope.filteredTodos[x].isSelected == true && $scope.filteredTodos[x].parent_email != '') {
                            $scope.filteredTodos[x].sims_cur_code = $scope.edt.sims_cur_code;
                            $scope.filteredTodos[x].sims_academic_year = $scope.edt.sims_academic_year;
                            $scope.filteredTodos[x].email_subject = $scope.email.email_subject;
                            $scope.filteredTodos[x].email_cc = $scope.email.ccTo;
                            $scope.filteredTodos[x].sims_msg_sr_no = $scope.sims_msg_sr_no;
                            $scope.message_details = "<table border=1 style='border-collapse:collapse;border-width:3px;width:450px'><tr><td Colspan=4 style='font-weight: bold;'>Fee Due Details</td></tr><tr><td Colspan=4 style='font-weight: bold;'>Enrollment No:" + $scope.filteredTodos[x].sims_enroll_number + "</td></tr><tr><td Colspan=4 style='font-weight: bold;'> Student Name:" + $scope.filteredTodos[x].student_name + "</td></tr><tr><td Colspan=4 style='font-weight: bold;'> Grade Name:" + $scope.filteredTodos[x].grade_name + "</td></tr><tr><td Colspan=4 style='font-weight: bold;'> Section Name:" + $scope.filteredTodos[x].section_name + "</td></tr><tr><td style='font-weight: bold;'>Fee Type</td><td style='font-weight: bold;'>Expected Fee</td><td style='font-weight: bold;'>Paid Fee</td><td style='font-weight: bold;'>Fee Amount</td></tr>";
                            for (var i = 0; i < $scope.defaulterdata[0].length; i++) {
                                if ($scope.defaulterdata[0][i].sims_enroll_number == $scope.filteredTodos[x].sims_enroll_number) {
                                    $scope.message_details = $scope.message_details += "<tr><td>" + $scope.defaulterdata[0][i].sims_fee_type_desc + "</td><td style='text-align:right;'>" + $scope.defaulterdata[0][i].sims_expected_fee + "</td><td style='text-align:right'>" + $scope.defaulterdata[0][i].sims_paid_fee + "</td><td style='text-align:right'>" + $scope.defaulterdata[0][i].sims_balance_amount + "</td></tr>";
                                }
                            }
                            $scope.message_details = $scope.message_details += "<tr><td style='font-weight: bold;'>Grand Total :</td><td style='text-align:right' Colspan=4>" + $scope.filteredTodos[x].sims_balance_amount_total + "</td></tr>";
                            $scope.message_details = $scope.message_details + "</table>";

                            // $scope.filteredTodos[x].email_message = document.getElementById('text-editor').value + "<br/><br/>" + $scope.message_details;
                            var data_format = document.getElementById('text-editor1').value;
                            var result_data = data_format.replace("{details}", $scope.message_details);
                            $scope.filteredTodos[x].email_message = result_data + "<br/><br/>";
                            $scope.data.push($scope.filteredTodos[x]);
                        }
                    }
                    console.log($scope.data);
                    if ($scope.data.length <= 0) {
                        swal(
                        {
                            showCloseButton: true,
                            text: 'Please Select student to send Email Message.',
                            imageUrl: "assets/img/notification-alert.png",
                            width: 350,
                            showCloseButon: true
                        });
                    }
                    else {
                        console.log($scope.data);

                        $http.post(ENV.apiUrl + "api/CommDefaulter/emailtoDefaultersNew", $scope.data).then(function (resl) {
                            if (resl.data == true) {
                                $('#MyModalNew').modal('hide');
                                $scope.email_subject = '';
                                $scope.email_message = '';
                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'Email Message Scheduled Or Sent Sucessfully.',
                                    imageUrl: "assets/img/check.png",
                                    width: 350,
                                    showCloseButon: true
                                });
                            }
                            else {
                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'Error in Sending Email Message.',
                                    imageUrl: "assets/img/notification-alert.png",
                                    width: 350,
                                    showCloseButon: true
                                });
                            }
                        });

                    }
                    console.log($scope.data);
                }
            }

            $http.get(ENV.apiUrl + "api/Fee/SFS/GetDefaulterEmailMessage").then(function (email_temp) {
                debugger;
                $scope.email_temp = email_temp.data;
                console.log($scope.email_temp)
                //  document.getElementById('text-editor').innerHTML = $scope.email_temp;
                //var $ta = $('textarea#text-editor');
                //var w5ref = $ta.data('wysihtml5');
                //if (w5ref) {
                //    w5ref.editor.setValue($scope.email_temp);
                //}
            });

            $scope.settemplate = function (msg_sr_no) {
                debugger;
                
                for (var i = 0; i < $scope.email_temp.length; i++) {
                    if ($scope.email_temp[i].sims_msg_sr_no == msg_sr_no) {

                        document.getElementById('text-editor1').innerHTML = $scope.email_temp[i].email_default_template;
                        var $ta = $('textarea#text-editor1');
                        var w5ref = $ta.data('wysihtml5');
                        if (w5ref) {
                            w5ref.editor.setValue($scope.email_temp[i].email_default_template);
                        }

                    }
                }

            }



            $scope.CancelEmailId = function (indx) {
                console.log(indx);
                $scope.emailId_data.splice(indx, 1);
                console.log($scope.emailId_data.splice(indx, 1));
            }
        }])
})();



