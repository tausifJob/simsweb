﻿(function () {
    'use strict';
    var formdata = new FormData();
    var imagename = '';
    var simsController = angular.module('sims.module.Collaboration');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('QueryReportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {
         
            $scope.pagesize = '30';
            $scope.pageindex = "0";
            $scope.busyindicator = true;
            $scope.Table = true;




            $scope.getcur = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                    $scope.sims_cur = AllCurr.data;
                    //$scope.temp.sims_cur_code = $scope.sims_cur[0].sims_cur_code;
                    $scope.temp = { sims_cur_code: $scope.sims_cur[0].sims_cur_code };
                    $scope.getAccYear($scope.temp.sims_cur_code);
                });
            }

           
            


            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.academic_year = Acyear.data;
                    $scope.temp = { sims_academic_year: $scope.academic_year[0].sims_academic_year };
                    $scope.getGrade(curCode, $scope.temp.sims_academic_year);
                    $scope.temp.sims_cur_code = curCode;
                    //$scope.health();
                });
            }

            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.grade_code = Gradecode.data;
                    console.log($scope.Grade_code);
                });
            }

            $scope.getSection = function (curCode, accYear, gradeCode) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                    $scope.section_code = Sectioncode.data;
                    console.log($scope.Section_code);
                });
            }


            $scope.getData = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/QueryReportController/getQueryReport?cur_code=" + $scope.temp.sims_cur_code + "&acad_year=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&from_date=" + $scope.temp.from_date + "&to_date=" + $scope.temp.to_date).then(function (res5) {
                    $scope.get_details = res5.data;
                    $scope.totalItems = $scope.get_details.length;
                    $scope.todos = $scope.get_details;
                    $scope.makeTodos();
                });
            }


            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();

                    //$scope.makeTodos();
                    // $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 30, $scope.maxSize = 30;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {

                $scope.todos = $scope.searched($scope.getAllTerm_details, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.getAllTerm_details;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sender_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_parent_father_mobile.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sender_id.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;

            }

            $(document).ready(function () {
                $scope.getcur();
            });
        }])
})();
