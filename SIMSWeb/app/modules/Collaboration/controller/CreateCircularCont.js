﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Collaboration');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CreateCircularCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.url = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'];

            $scope.view = false;
            $scope.create = true;
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            $scope.save_btn = false;
            var arr_check = [];
            var arr_files = [];
            var arr_files1 = [];
            $scope.images = [];
            $scope.images1 = [];
            $scope.del_images = [];
            var checkboxes = new Array();
            var comn_files = "";
            $scope.obj = [];
            $scope.cdetails_data = [];
            $scope.filesize = true;
            
            //$scope.New();



            //HTML5 editor
            $('#text-editor').wysihtml5();

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            var str_text = null;
            var $ta = $('textarea#text-editor');
            var w5ref = $ta.data('wysihtml5');

            //$http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
            //    $scope.obj2 = res.data;
            //});


            function getCur(flag, comp_code) {
                if (flag) {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                        $scope.obj2 = res.data;
                        $scope.edt['sims_cur_code'] = $scope.obj2[0].sims_cur_code;
                        $scope.Getacademicyear($scope.obj2[0].sims_cur_code);

                    });
                }
                else {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                        $scope.obj2 = res.data;
                        $scope.edt['sims_cur_code'] = $scope.obj2[0].sims_cur_code;
                        $scope.Getacademicyear($scope.obj2[0].sims_cur_code);

                    });
                }
            }

            $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.global_count_comp = res.data;

                if ($scope.global_count_comp) {
                    getCur(true, $scope.user_details.comp);


                }
                else {
                    getCur(false, $scope.user_details.comp)
                }
            });


            $http.get(ENV.apiUrl + "api/common/Circular/getCircularType").then(function (res) {
                $scope.obj3 = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/Circular/getCircularCategory").then(function (res) {
                $scope.obj4 = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/Circular/getAllCircularUserGroup").then(function (res) {
                $scope.obj5 = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/Circular/GetAllDesignationNames").then(function (res) {
                $scope.designation = res.data;
                setTimeout(function () {
                    $('#cmb_designation').change(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });

            $http.get(ENV.apiUrl + "api/common/Circular/GetAllDepartments").then(function (res) {
                $scope.department = res.data;
                setTimeout(function () {
                    $('#cmb_department').change(function () {
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });

            $scope.GetGrade = function (cur, acad_yr) {
             
                if (cur != undefined && acad_yr != undefined) {
                    $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + cur + "&academic_year=" + acad_yr).then(function (res) {
                        $scope.grade = res.data;
                        setTimeout(function () {
                            $('#cmb_grade').change(function () {
                            }).multipleSelect({
                                width: '100%'
                            });
                        }, 1000);

                        setTimeout(function () {
                            try {
                                $("#cmb_grade").multipleSelect("setSelects", $scope.edt.lst_grade);
                            }
                            catch (e) {
                            }
                        }, 1000);

                    });
                }
            }

            $scope.GetSection = function (cur, acad_yr, grade) {

                if (cur != null && acad_yr != null && grade != null) {
                    $http.get(ENV.apiUrl + "api/common/Circular/getAllSection?cur_code=" + cur + "&ac_year=" + acad_yr + "&grade=" + grade).then(function (res) {
                        $scope.section = res.data;
                        setTimeout(function () {
                            $('#cmb_section').change(function () {
                            }).multipleSelect({
                                width: '100%'
                            });
                        }, 1000);
                        setTimeout(function () {

                            try {
                                $("#cmb_section").multipleSelect("setSelects", $scope.edt.lst_section);
                            }
                            catch (e) {
                            }
                        }, 1000);


                    });
                }
            }

            $scope.GetDesg = function () {
                $http.get(ENV.apiUrl + "api/common/Circular/GetAllDesignationNames").then(function (res) {
                    $scope.designation = res.data;
                    setTimeout(function () {
                        $('#cmb_designation').change(function () {
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }

            $scope.GetDept = function () {
                $http.get(ENV.apiUrl + "api/common/Circular/GetAllDepartments").then(function (res) {
                    $scope.department = res.data;
                    setTimeout(function () {
                        $('#cmb_department').change(function () {
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }

            $scope.size = function (str) {
                
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.cdetails_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str;  //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }


            //$scope.New();

            $http.get(ENV.apiUrl + "api/common/Circular/getCircular?loggeduser=" + $rootScope.globals.currentUser.username).then(function (res) {

                $scope.view = false;
                $scope.create = true;

                $scope.New();

                $scope.cdetails_data = res.data;
                $scope.totalItems = $scope.cdetails_data.length;
                $scope.todos = $scope.cdetails_data;
                $scope.makeTodos();
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.cdetails_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.cdetails_data;
                }
                $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            function searchUtil(item, toSearch) {
                /* Search Text in all 2 fields */
                return (item.sims_circular_title.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_circular_short_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $scope.publishcircular = function (str) {
                var data = str;

                $http.post(ENV.apiUrl + "api/common/Circular/UpdateCircularPublishDate", data).then(function (res) {
                    $scope.display = true;
                    $scope.msg1 = res.data;
                    $rootScope.strMessage = $scope.msg1.strMessage;
                    //$('#message').modal('show');

                    swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });

                    // alert("inserted");

                    $scope.getgrid();
                });
            };

            $scope.unpublishcircular = function (str) {
                var data = str;

                $http.post(ENV.apiUrl + "api/common/Circular/UpdateCircularStatus", data).then(function (res) {

                    $scope.msg1 = res.data;
                    $rootScope.strMessage = $scope.msg1.strMessage;
                    //$('#message').modal('show');

                    swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });

                    $scope.getgrid();
                });
            };

            $scope.delcircular = function (str1) {
                

                swal({
                    title: '',
                    text: "Are you sure you want to Delete?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {
                    if (isConfirm) {

                        $http.post(ENV.apiUrl + "api/common/Circular/DCirculars", str1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            $rootScope.strMessage = $scope.msg1.strMessage;
                            //$('#message').modal('show');

                            swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        });
                    };
                });
            };

            $scope.New = function () {

                $scope.save_btn = true;

                $scope.view = false;
                $scope.create = true;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = {};
                $scope.obj5 = [];
                
                $scope.new = true;
                
                var dt = new Date();
                $scope.edt =
              {
                  sims_circular_status: true,
                  sims_circular_publish_date : ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear(),
                  sims_circular_expiry_date : ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear(),
              }


                if ($scope.global_count_comp) {
                    getCur(true, $scope.user_details.comp);


                }
                else {
                    getCur(false, $scope.user_details.comp)
                }

                // $scope.edt['sims_cur_code'] = $scope.obj2[0].sims_cur_code;
                // $scope.Getacademicyear($scope.obj2[0].sims_cur_code);

                setTimeout(function () {
                    $scope.edt['sims_academic_year_desc'] = $scope.ob_acyear[0].sims_academic_year;
                    $scope.GetGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year_desc);
                }, 2000);

                $http.get(ENV.apiUrl + "api/common/Circular/getAllCircularUserGroup").then(function (res) {
                    $scope.obj5 = res.data;
                });

                $scope.images = [];
                $scope.images1 = [];

                try {
                    $("#cmb_grade").multipleSelect("uncheckAll");
                }
                catch (e) {
                }

                try {
                    $("#cmb_section").multipleSelect("uncheckAll");
                }
                catch (e) {
                }

                try {
                    $("#cmb_designation").multipleSelect("uncheckAll");
                }
                catch (e) {
                }

                try {
                    $("#cmb_department").multipleSelect("uncheckAll");
                }
                catch (e) {
                }

                $("#cmb_grade").multipleSelect("disable");
                $("#cmb_section").multipleSelect("disable");
                $("#cmb_designation").multipleSelect("disable");
                $("#cmb_department").multipleSelect("disable");

                $http.get(ENV.apiUrl + "api/common/Circular/getCircularNumber").then(function (res) {
                    $scope.obj6 = res.data;
                    document.getElementById("txt_number").disabled = true;
                    var v = document.getElementById('txt_number');
                    v.value = $scope.obj6[0].sims_circular_number;
                });

                $('#text-editor').data("wysihtml5").editor.clear();
                $scope.Date();
            }

            $scope.Date = function () {
                debugger;
                setTimeout(function () {
                    $(document).ready(function () {
                        $("#fromdate, #todate").kendoDatePicker({ format: "dd-MM-yyyy", value: '' });
                    });
                }, 700);

            }


            $scope.Getacademicyear = function (curcode) {

                if (curcode != undefined) {
                    
                    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curcode).then(function (res) {
                        $scope.ob_acyear = res.data;
                        //if($scope.new)
                        //    $scope.edt['sims_academic_year_desc'] = $scope.ob_acyear[0].sims_academic_year_desc;
                    });

                    $scope.GetGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year_desc);                }
            }

            $scope.GetUserGroups = function (gname, gcode) {
                debugger;
                if (document.getElementById(gcode).checked == true) {
                    arr_check.push(gname);

                    if (gcode == '04' || gcode == '05') {
                        try { $("#cmb_grade").multipleSelect("enable"); }
                        catch (e) {
                        }
                        try { $("#cmb_section").multipleSelect("enable"); }
                        catch (e) {
                        }
                    }
                    if (gcode == '03') {
                        try { $("#cmb_designation").multipleSelect("enable"); }
                        catch (e) {
                        }
                        try { $("#cmb_department").multipleSelect("enable"); }
                        catch (e) {
                        }
                    }
                }
                else {
                    for (var i = arr_check.length; i--;) {
                        if (arr_check[i] == gname) {
                            arr_check.splice(i, 1);
                            if (gcode == '04' || gcode == '05') {
                                try { $("#cmb_grade").multipleSelect("disable"); }
                                catch (e) {
                                }
                                try { $("#cmb_section").multipleSelect("disable"); }
                                catch (e) {
                                }
                                try {
                                    $("#cmb_grade").multipleSelect("uncheckAll");
                                }
                                catch (e) {
                                }

                                try {
                                    $("#cmb_section").multipleSelect("uncheckAll");
                                }
                                catch (e) {
                                }
                            }
                            if (gcode == '03') {
                                try {
                                    $("#cmb_designation").multipleSelect("disable");
                                }
                                catch (e) {
                                }
                                try { $("#cmb_department").multipleSelect("disable"); }
                                catch (e) {
                                }
                                try {
                                    $("#cmb_designation").multipleSelect("uncheckAll");
                                }
                                catch (e) {
                                }

                                try {
                                    $("#cmb_department").multipleSelect("uncheckAll");
                                }
                                catch (e) {
                                }
                            }
                        }
                    }
                }

            }

            function convertdate(dt) {
                
                if (dt != null) {
                    var d1 = new Date(dt);
                    var month = d1.getMonth() + 1;
                    var day = d1.getDate();
                    if (month < 10)
                        month = "0" + month;
                    if (day < 10)
                        day = "0" + day;
                    var d = d1.getFullYear() + "-" + (month) + "-" + (day);
                    return d;
                }
            }

            function Compare(first,second) {
                
                var firstDate = new Date(first.split('/')[2], first.split('/')[1], first.split('/')[0]);

                var secondDate = new Date(second.split('/')[2], second.split('/')[1], second.split('/')[0]);

                var Difference = firstDate.getDate() - secondDate.getDate();

                if (Difference > 0) alert('firstDate is larger');

                else if (Difference == 0) alert('Dates are equal');

                else alert('secondDate is larger');

            }

            $scope.Save = function (ifalt) {
                debugger;
                $scope.save_btn = false;
                $scope.edt.sims_circular_ifalert = ifalt;
                var c_desc = document.getElementById('text-editor').value;

                var publish_date = $scope.edt.sims_circular_publish_date;
                var expiry_date = $scope.edt.sims_circular_expiry_date;
                var flag = true;

                var grade = $scope.edt.sims_circular_grade;
                var section = $scope.edt.sims_circular_gradesections;
                if (arr_check == 'Parent' || arr_check == 'Student') {
                    if (grade == undefined || grade == '') {
                        flag = false;
                        $('#loader').modal('hide');
                        swal({ text: "Please select Grade.", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                            }
                        });
                    }
                    if (section == undefined || section == '') {
                        flag = false;
                        $('#loader').modal('hide');
                        swal({ text: "Please select Section.", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                            }
                        });
                    }
                }

                if (arr_check.length <= 0) {
                    flag = false;
                    $('#loader').modal('hide');
                    swal({  text: "Please Select atleast one User Group.",imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }

                //var date1 = convertdate($scope.edt.sims_circular_publish_date);
                //var date2 = convertdate($scope.edt.sims_circular_expiry_date);

                //if (date1 > date2) {
                //    flag = false;
                //    $('#loader').modal('hide');
                //    swal({ title: "Alert", text: "Expiry date should be greater than publish date.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                //        if (isConfirm) {
                //        }
                //    });
                //}

                var dt1 = publish_date.split('-')[2] +'-'+ publish_date.split('-')[1] +'-'+ publish_date.split('-')[0];
                var dt2 = expiry_date.split('-')[2] +'-'+ expiry_date.split('-')[1] +'-'+ expiry_date.split('-')[0];

                //var firstDate = new date(dt1);
                //var secondDate = new date(dt2);

                if (dt1 > dt2) {
                    flag = false;
                    $('#loader').modal('hide');
                    swal({ text: "Expiry date should be greater than publish date.", imageUrl: "assets/img/notification-alert.png",showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }

                //var firstDate = new Date(publish_date.split('-')[2], publish_date.split('-')[1], publish_date.split('-')[0]);

                //var secondDate = new Date(expiry_date.split('-')[2], expiry_date.split('-')[1], expiry_date.split('-')[0]);

                //var Difference = firstDate.getDate() - secondDate.getDate();

                //if (Difference > 0) alert('firstDate is larger');

                //else if (Difference == 0) alert('Dates are equal');

                //else alert('secondDate is larger');


                if (c_desc == undefined || c_desc == '') {
                    flag = false;
                    $('#loader').modal('hide');
                    swal({ text: "Please enter Circular Description.",imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }
                debugger;
                if ($scope.edt.sims_circular_publish_date != undefined &&
                    $scope.edt.sims_cur_code != undefined && $scope.edt.sims_academic_year_desc != undefined &&
                    $scope.edt.sims_circular_type != undefined && $scope.edt.sims_circular_category != undefined &&
                    $scope.edt.sims_circular_title != undefined) {
                    if (flag == true) {
                        //comn_files = '';
                        //for (var j = 0 ; j < $scope.images.length; j++) {
                        //    comn_files = comn_files + "," + $scope.images[j].name;
                        //}
                        $('#loader').modal({ backdrop: 'static', keyboard: false });

                        var data = $scope.edt;
                       

                        var grade_1 = '', gradesec = '', desg = '', dept = '';
                        var grades = $scope.edt.sims_circular_gradesections;
                        gradesec = gradesec + ',' + grades;
                        var str2 = gradesec.substr(gradesec.indexOf(',') + 1);

                        var designation = $scope.edt.sims_circular_desg;
                        desg = desg + ',' + designation;
                        var str3 = desg.substr(desg.indexOf(',') + 1);

                        var department = $scope.edt.sims_circular_dept;
                        dept = dept + ',' + department;
                        var str4 = dept.substr(dept.indexOf(',') + 1);

                        var gd = $scope.edt.sims_circular_grade;
                        grade_1 = grade_1 + ',' + gd;
                        var str5 = grade_1.substr(grade_1.indexOf(',') + 1);

                        data.sims_circular_gradesections = str2;
                        data.sims_circular_desg = str3;
                        data.sims_circular_dept = str4;
                        data.sims_circular_grade = str5;

                        data.lst_groups = arr_check;
                        data.sims_circular_number = document.getElementById('txt_number').value;
                        data.sims_circular_desc = document.getElementById('text-editor').value;
                        data.sims_circular_publish_date = $scope.edt.sims_circular_publish_date;
                        data.sims_circular_expiry_date = $scope.edt.sims_circular_expiry_date;
                        data.sims_circular_created_user = $rootScope.globals.currentUser.username;
                        data.opr = "I";

                        if (data.sims_circular_desg == undefined)
                            data.sims_circular_desg = ''
                        if (data.sims_circular_dept == undefined)
                            data.sims_circular_dept = ''
                        if (data.sims_circular_expiry_date == undefined)
                            data.sims_circular_expiry_date = '';
                        if (data.sims_circular_publish_date != undefined) {
                            debugger
                            $http.post(ENV.apiUrl + "api/common/Circular/CCirculars", data).then(function (res) {
                                $scope.view = false;
                                $scope.create = true;
                                $scope.msg1 = res.data;

                                //var request = {
                                //    method: 'POST',
                                //    url: ENV.apiUrl + '/api/file/upload_file?filename=' + document.getElementById('txt_number').value + "&location=" + "CircularFiles" + "&value=" + comn_files + "&update=N",
                                //    data: formdata,
                                //    headers: {
                                //        'Content-Type': undefined
                                //    }
                                //};

                                //$http(request).success(function (d) {

                                //});

                                swal({  text: "Circular Created Successfully.",imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {

                                        arr_check = [];
                                        $scope.images = [];
                                        $('#loader').modal('hide');
                                        $scope.currentPage = 1
                                        $http.get(ENV.apiUrl + "api/common/Circular/getCircular?loggeduser=" + $rootScope.globals.currentUser.username).then(function (res) {
                                            $scope.view = false;
                                            $scope.create = true;

                                            $scope.New();

                                            $scope.cdetails_data = res.data;
                                            $scope.totalItems = $scope.cdetails_data.length;
                                            $scope.todos = $scope.cdetails_data;
                                            $scope.makeTodos();
                                        });

                                        $scope.myForm.$setPristine();
                                        $scope.myForm.$setUntouched();
                                    }
                                });
                            });
                        }
                    }
                }
            }
                        
            $scope.getgrid = function () {
                $scope.currentPage = 1
                $http.get(ENV.apiUrl + "api/common/Circular/getCircular?loggeduser=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.view = false;
                    $scope.create = true;

                    $scope.cdetails_data = res.data;
                    $scope.totalItems = $scope.cdetails_data.length;
                    $scope.todos = $scope.cdetails_data;
                    $scope.makeTodos();
                });

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function () {
                $('#loader').modal({ backdrop: 'static', keyboard: false });
                $scope.new = false;
                $scope.save_btn = false;
                
                var c_desc = document.getElementById('text-editor').value;

                var publish_date = $scope.edt.sims_circular_publish_date;
                var expiry_date = $scope.edt.sims_circular_expiry_date;
                var flag = true;

                if (arr_check.length <= 0) {
                    flag = false;
                    $('#loader').modal('hide');
                    swal({ text: "Please Select atleast one User Group.",imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }

                if (publish_date > expiry_date) {
                    flag = false;
                    $('#loader').modal('hide');
                    swal({  text: "Expiry date should be greater than publish date.",imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }
                if (c_desc == undefined || c_desc == '') {
                    flag = false;
                    $('#loader').modal('hide');
                    swal({ text: "Please enter Circular Description.", imageUrl: "assets/img/notification-alert.png",showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                        }
                    });
                }
                if (flag == true) {
                    //comn_files = ''
                    //for (var j = 0 ; j < $scope.images.length; j++) {
                    //    comn_files = comn_files + "," + $scope.images[j].name;
                    //}
                    var data = $scope.edt;

                   

                    var grade_1, gradesec, desg, dept;
                    var grades = $scope.edt.sims_circular_gradesections;
                    gradesec = gradesec + ',' + grades;
                    var str2 = gradesec.substr(gradesec.indexOf(',') + 1);

                    var designation = $scope.edt.sims_circular_desg;
                    desg = desg + ',' + designation;
                    var str3 = desg.substr(desg.indexOf(',') + 1);

                    var department = $scope.edt.sims_circular_dept;
                    dept = dept + ',' + department;
                    var str4 = dept.substr(dept.indexOf(',') + 1);

                    var gd = $scope.edt.sims_circular_grade;
                    grade_1 = grade_1 + ',' + gd;
                    var str5 = grade_1.substr(grade_1.indexOf(',') + 1);

                    data.sims_circular_gradesections = str2;
                    data.sims_circular_desg = str3;
                    data.sims_circular_dept = str4;
                    data.sims_circular_grade = str5;

                    data.lst_groups = arr_check;
                    data.sims_circular_number = document.getElementById('txt_number').value;
                    data.sims_circular_desc = document.getElementById('text-editor').value;
                    data.sims_circular_publish_date = $scope.edt.sims_circular_publish_date;
                    data.sims_circular_expiry_date = $scope.edt.sims_circular_expiry_date;
                    data.opr = "U";
                    $http.post(ENV.apiUrl + "api/common/Circular/UCirculars", data).then(function (res) {
                        $$scope.view = false;
                        $scope.create = true;
                        $scope.msg1 = res.data;
                        flag = false;
                        //var request = {
                        //    method: 'POST',
                        //    url: ENV.apiUrl + '/api/file/upload_file?filename=' + document.getElementById('txt_number').value + "&location=" + "CircularFiles" + "&value=" + comn_files + "&update=Y",
                        //    data: formdata,
                        //    headers: {
                        //        'Content-Type': undefined
                        //    }
                        //};

                        //$http(request).success(function (d) {
                        //    //$scope.getgrid();
                        //});

                        swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                arr_check = [];
                                $scope.images = [];
                                $('#loader').modal('hide');
                                $scope.currentPage = 1
                                $http.get(ENV.apiUrl + "api/common/Circular/getCircular?loggeduser=" + $rootScope.globals.currentUser.username).then(function (res) {
                                    $scope.view = false;
                                    $scope.create = true;

                                    $scope.cdetails_data = res.data;
                                    $scope.totalItems = $scope.cdetails_data.length;
                                    $scope.todos = $scope.cdetails_data;
                                    $scope.makeTodos();
                                });

                                $scope.myForm.$setPristine();
                                $scope.myForm.$setUntouched();
                            }
                        });
                    });
                }
            }

            $scope.cancel = function () {

                $scope.view = false;
                $scope.create = true;

                try {
                    $("#cmb_grade").multipleSelect("uncheckAll");
                }
                catch (e) {
                }

                try {
                    $("#cmb_section").multipleSelect("uncheckAll");
                }
                catch (e) {
                }

                try {
                    $("#cmb_designation").multipleSelect("uncheckAll");
                }
                catch (e) {
                }

                try {
                    $("#cmb_department").multipleSelect("uncheckAll");
                }
                catch (e) {
                }

                $scope.edt = "";
                $scope.obj5 = [];

                $http.get(ENV.apiUrl + "api/common/Circular/getAllCircularUserGroup").then(function (res) {
                    $scope.obj5 = res.data;
                    });

                $scope.edt =
               {
                   sims_circular_status: true,
                   sims_circular_desc: null,
               }

                arr_check = [];
                $scope.images = [];
                $('#loader').modal('hide');
                $scope.currentPage = 1
                $http.get(ENV.apiUrl + "api/common/Circular/getCircular?loggeduser=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.view = false;
                    $scope.create = true;
                    $scope.New();
                    $scope.cdetails_data = res.data;
                    $scope.totalItems = $scope.cdetails_data.length;
                    $scope.todos = $scope.cdetails_data;
                    $scope.makeTodos();
                });

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.Date();
            }

            //File Download
            $scope.download = function (str) {
                window.open($scope.url + "/Images/CircularFiles/" + str, "_new");
            };

            /*start_attached Files*/

            var formdata = new FormData();
            $scope.images = [];
            $scope.images_att = [];

            $scope.getTheFiles = function ($files) {
                $scope.filesize = true;
                var i = 0, exists = "no";

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);

                    var i = 0;
                    if ($files[i].size > 10485760) {
                        $scope.filesize = false;
                        // $scope.edt.photoStatus = false;
                        swal({ text: "File Should Not Exceed 10MB.", imageUrl: "assets/img/notification-alert.png",showCloseButton: true, width: 380, });
                    }
                    else {
                        arr_files.push($files[i].name);
                        if ($scope.images.length > 0) {
                            for (var j = 0 ; j < $scope.images.length; j++) {
                                if ($scope.images[j].name == $files[i].name) {
                                    exists = "yes";
                                    return;
                                }
                            }

                            if (exists == "no") {
                                $scope.images.push({
                                    name: $files[i].name,
                                    file: $files[i].size
                                });
                                if ($scope.images.length > 3) {
                                    $scope.images.splice(3, $scope.images.length);
                                    swal({ text: "You cannot upload more than 3 files.", imageUrl: "assets/img/notification-alert.png",showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                        }
                                    });
                                    return;
                                }
                            }
                            exists = "no";
                        }
                        else {
                            $scope.images.push({
                                name: $files[i].name,
                                file: $files[i].size

                            });
                        }
                        // $scope.$apply();
                    }
                    i++;
                });
            };

            $scope.file_changed = function (element, str) {
                debugger;
                var photofile = element.files[0];
                var v = photofile.name.indexOf('.');
                $scope.name = photofile.name.substr(0, v);
                $scope.fileext = photofile.name.substr(v + 1);
                $scope.photo_filename = (photofile.type);

                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;

                    });
                };
                reader.readAsDataURL(photofile);

                if (element.files[0].size < 2000000) {
                    var filenm = '';
                    var cnumber = document.getElementById('txt_number').value;
                    //+ '_' + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss')

                    var ind = 0;

                    if ($scope.edt.sims_circular_file_path1 == '' || $scope.edt.sims_circular_file_path1 == undefined) {
                        filenm = cnumber + '_1';
                        $scope.edt.sims_circular_file_path1 = filenm + '.' + $scope.fileext;
                        $scope.edt.sims_circular_file_path1_en = element.files[0].name;
                    }
                    else if ($scope.edt.sims_circular_file_path2 == '' || $scope.edt.sims_circular_file_path2 == undefined) {
                        filenm = cnumber + '_2';
                        $scope.edt.sims_circular_file_path2 = filenm + '.' + $scope.fileext;
                        $scope.edt.sims_circular_file_path2_en = element.files[0].name;
                    }
                    else if ($scope.edt.sims_circular_file_path3 == '' || $scope.edt.sims_circular_file_path3 == undefined) {
                        filenm = cnumber + '_3';
                        $scope.edt.sims_circular_file_path3 = filenm + '.' + $scope.fileext;
                        $scope.edt.sims_circular_file_path3_en = element.files[0].name;
                    }

                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + 'api/file/uploadCircularDocument?filename=' + filenm + "&location=" + "CircularFiles",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                                        $http(request).success(function (d) {

                                            angular.forEach(
                                    angular.element("input[type='file']"),
                                    function (inputElem) {
                                        angular.element(inputElem).val(null);
                                    });
                    });

                }
            };

            //$scope.file_changed = function (element, str) {
            //    ;
            //    var photofile = element.files[0];
            //    var v = photofile.name.indexOf('.');
            //    $scope.name = photofile.name.substr(0, v);
            //    $scope.fileext = photofile.name.substr(v + 1);
            //    $scope.photo_filename = (photofile.type);

            //    var reader = new FileReader();
            //    reader.onload = function (e) {
            //        $scope.$apply(function () {
            //            $scope.prev_img = e.target.result;

            //        });
            //    };
            //    reader.readAsDataURL(photofile);

            //    if ($scope.filesize) {
            //        var filenm = '';
            //        var cnumber = document.getElementById('txt_number').value;
            //        //+ '_' + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss')

            //        var ind = 0;

            //        if ($scope.images.length == 0) {
            //            filenm = cnumber + '_1';
            //            $scope.edt.sims_circular_file_path1 = cnumber + '_1' + '.' + $scope.fileext;
            //        }
            //        else if ($scope.images.length > 0) {
            //            if ($scope.images.length == 1) {
            //                if ($scope.delpush.length == 0) {
            //                    var x = 0;

            //                    if ($scope.edt.sims_circular_file_path1 == '' || $scope.edt.sims_circular_file_path1 == undefined) {
            //                        filenm = cnumber + '_1';
            //                        $scope.edt.sims_circular_file_path1 = filenm + '.' + $scope.fileext;
            //                    }
            //                    else if ($scope.edt.sims_circular_file_path2 == '' || $scope.edt.sims_circular_file_path2 == undefined) {
            //                        filenm = cnumber + '_2';
            //                        $scope.edt.sims_circular_file_path2 = filenm + '.' + $scope.fileext;
            //                    }
            //                    else if ($scope.edt.sims_circular_file_path3 == '' || $scope.edt.sims_circular_file_path3 == undefined) {
            //                        filenm = cnumber + '_3';
            //                        $scope.edt.sims_circular_file_path3 = filenm + '.' + $scope.fileext;
            //                    }
            //                    //}
            //                }
            //                else if ($scope.delpush.length > 0) {
            //                    ind = $scope.delpush[0];
            //                    if (ind == 0) {
            //                        if ($scope.edt.sims_circular_file_path1 == '')
            //                            $scope.edt.sims_circular_file_path1 = cnumber + '_1' + '.' + $scope.fileext;
            //                        else if (($scope.edt.sims_circular_file_path2 == ''))
            //                            $scope.edt.sims_circular_file_path2 = cnumber + '_1' + '.' + $scope.fileext;
            //                        else if (($scope.edt.sims_circular_file_path3 == ''))
            //                            $scope.edt.sims_circular_file_path3 = cnumber + '_1' + '.' + $scope.fileext;
            //                        filenm = cnumber + '_1';
            //                    }
            //                    if (ind == 1) {
            //                        if ($scope.edt.sims_circular_file_path1 == '')
            //                            $scope.edt.sims_circular_file_path1 = cnumber + '_2' + '.' + $scope.fileext;
            //                        else if (($scope.edt.sims_circular_file_path2 == ''))
            //                            $scope.edt.sims_circular_file_path2 = cnumber + '_2' + '.' + $scope.fileext;
            //                        else if (($scope.edt.sims_circular_file_path3 == ''))
            //                            $scope.edt.sims_circular_file_path3 = cnumber + '_2' + '.' + $scope.fileext;
            //                        filenm = cnumber + '_2';
            //                    }
            //                    if (ind == 2) {
            //                        if ($scope.edt.sims_circular_file_path1 == '')
            //                            $scope.edt.sims_circular_file_path1 = cnumber + '_3' + '.' + $scope.fileext;
            //                        else if (($scope.edt.sims_circular_file_path2 == ''))
            //                            $scope.edt.sims_circular_file_path2 = cnumber + '_3' + '.' + $scope.fileext;
            //                        else if (($scope.edt.sims_circular_file_path3 == ''))
            //                            $scope.edt.sims_circular_file_path3 = cnumber + '_3' + '.' + $scope.fileext;
            //                        filenm = cnumber + '_3';
            //                    }

            //                    $scope.delpush.splice(0, $scope.delpush.length);
            //                }
            //            }
            //            if ($scope.images.length == 2) {
            //                if ($scope.delpush.length == 0) {

            //                    if ($scope.edt.sims_circular_file_path1 == '' || $scope.edt.sims_circular_file_path1 == undefined) {
            //                            filenm = cnumber + '_1';
            //                            $scope.edt.sims_circular_file_path1 = filenm + '.' + $scope.fileext;                                        
            //                        }
            //                    else if ($scope.edt.sims_circular_file_path2 == '' || $scope.edt.sims_circular_file_path2 == undefined) {
            //                            filenm = cnumber + '_2';
            //                            $scope.edt.sims_circular_file_path2 = filenm + '.' + $scope.fileext;
            //                        }
            //                    else if ($scope.edt.sims_circular_file_path3 == '' || $scope.edt.sims_circular_file_path3 == undefined) {
            //                            filenm = cnumber + '_3';
            //                            $scope.edt.sims_circular_file_path3 = filenm + '.' + $scope.fileext;
            //                        }
            //                }
            //                else if ($scope.delpush.length > 0) {
            //                    ind = $scope.delpush[0];
            //                    if (ind == 0) {
            //                        if ($scope.edt.sims_circular_file_path1 == '')
            //                            $scope.edt.sims_circular_file_path1 = cnumber + '_1' + '.' + $scope.fileext;
            //                        else if (($scope.edt.sims_circular_file_path2 == ''))
            //                            $scope.edt.sims_circular_file_path2 = cnumber + '_1' + '.' + $scope.fileext;
            //                        else if (($scope.edt.sims_circular_file_path3 == ''))
            //                            $scope.edt.sims_circular_file_path3 = cnumber + '_1' + '.' + $scope.fileext;
            //                        filenm = cnumber + '_1';
            //                    }
            //                    if (ind == 1) {
            //                        if ($scope.edt.sims_circular_file_path1 == '')
            //                            $scope.edt.sims_circular_file_path1 = cnumber + '_2' + '.' + $scope.fileext;
            //                        else if (($scope.edt.sims_circular_file_path2 == ''))
            //                            $scope.edt.sims_circular_file_path2 = cnumber + '_2' + '.' + $scope.fileext;
            //                        else if (($scope.edt.sims_circular_file_path3 == ''))
            //                            $scope.edt.sims_circular_file_path3 = cnumber + '_3' + '.' + $scope.fileext;
            //                        filenm = cnumber + '_2';
            //                    }
            //                    if (ind == 2) {
            //                        if ($scope.edt.sims_circular_file_path1 == '')
            //                            $scope.edt.sims_circular_file_path1 = cnumber + '_3' + '.' + $scope.fileext;
            //                        else if (($scope.edt.sims_circular_file_path2 == ''))
            //                            $scope.edt.sims_circular_file_path2 = cnumber + '_3' + '.' + $scope.fileext;
            //                        else if (($scope.edt.sims_circular_file_path3 == ''))
            //                            $scope.edt.sims_circular_file_path3 = cnumber + '_3' + '.' + $scope.fileext;
            //                        filenm = cnumber + '_3';
            //                    }

            //                    $scope.delpush.splice(0, $scope.delpush.length);
            //                }
            //            }
            //        }

            //        var request = {
            //            method: 'POST',
            //            url: ENV.apiUrl + 'api/file/uploadCircularDocument?filename=' + filenm + "&location=" + "CircularFiles",
            //            data: formdata,
            //            headers: {
            //                'Content-Type': undefined
            //            }
            //        };
            //        
            //        $http(request).success(function (d) {

            //        });

            //       
            //    }
            //};

            $scope.uploadClick = function (str) {

                $scope.filesize = true;
                $scope.edt1 = str;
                $scope.ins = false;
                formdata = new FormData();
            }

            $scope.delpush = [];

            $scope.CancelFileUpload = function (idx) {
                
                $scope.delpush.push(idx);

                $scope.images.splice(idx, 1);
                if (idx == 0)
                    $scope.edt.sims_circular_file_path1 = '';
                if (idx == 1)
                    $scope.edt.sims_circular_file_path2 = '';
                if (idx == 2)
                    $scope.edt.sims_circular_file_path3 = '';
                };

            $scope.edit = function (str) {
                
                $scope.save_btn = true;
                $scope.view = false;
                $scope.create = true;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
               
                $scope.edt = str;
                $scope.images = [];
                $scope.delpush = [];
                //Set User Group Checkboxes
               
                $('input:checkbox').each(function () {
                    var chk_id = $(this).attr("id");
                    var chk_val = $(this).attr("value");
                    var i = 0;
                    for (var i = 0; i < $scope.edt.lst_groups.length; i++) {
                        if ($scope.edt.lst_groups[i] == chk_id) {
                            this.checked = true;
                            arr_check.push(chk_val);
                        }
                    }

                    $scope.images = [];
                    $scope.del_images = [];
                    if ($scope.edt.sims_circular_file_path1 == "") { }
                    else {
                        $scope.images.push({
                            name: $scope.edt.sims_circular_file_path1,
                            file: ''
                        });
                    }
                    if ($scope.edt.sims_circular_file_path2 == "") { }
                    else {
                        $scope.images.push({
                            name: $scope.edt.sims_circular_file_path2,
                            file: ''
                        });
                    }
                    if ($scope.edt.sims_circular_file_path3 == "") { }
                    else {
                        $scope.images.push({
                            name: $scope.edt.sims_circular_file_path3,
                            file: ''
                        });
                    }

                });

                $scope.Getacademicyear($scope.edt.sims_cur_code);
                $scope.GetGrade($scope.edt.sims_cur_code, $scope.edt.sims_academic_year_desc);
                $scope.GetSection($scope.edt.sims_cur_code, $scope.edt.sims_academic_year_desc, $scope.edt.sims_circular_grade);

                try {
                    $("#cmb_designation").multipleSelect("setSelects", $scope.edt.lst_desg);
                }
                catch (e) {
                }

                try {
                    $("#cmb_department").multipleSelect("setSelects", $scope.edt.lst_dept);
                }
                catch (e) {
                }

                var $ta = $('textarea#text-editor');
                var w5ref = $ta.data('wysihtml5');
                if (w5ref) {
                    w5ref.editor.setValue($scope.edt.sims_circular_desc);
                } else {
                    $ta.html($scope.edt.sims_circular_desc);
                }
                wysihtml5.editor.setValue = $scope.edt.sims_circular_desc;
            }
        }])

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {

            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])
})();