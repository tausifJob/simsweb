﻿(function () {
    'use strict';
    angular.module('sims.module.Collaboration', [
        'sims',
        'gettext'
    ]);
})();