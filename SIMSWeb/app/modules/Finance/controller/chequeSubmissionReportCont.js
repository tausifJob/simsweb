﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('chequeSubmissionReportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.temp = {};
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.temp.report_status = 'D';

            $(function () {
                $('#bell_box').multipleSelect({ width: '100%' });
                $('#teacher_box').multipleSelect({ width: '100%', filter: true });

            });

            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_start_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });


            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_end_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });


            
            
            $scope.getdetail = function () {
                debugger;
                $scope.colsvis = false;
                $http.get(ENV.apiUrl + "api/checqsubrpt/getchequeSubmissionReport?mom_start_date=" + $scope.mom_start_date + "&mom_end_date=" + $scope.mom_end_date + "&doc_search=" + $scope.temp.sims_doc_code).then(function (res1) {
                    if (res1.data.length > 0) {
                        $scope.report_data = res1.data;
                        console.log("$scope.report_data",$scope.report_data);
                        ///$scope.report_data1 = res1.data[0].list1;
                        //console.log($scope.repreport_data_new);
                        $scope.temp.no_sub_cheque_total = 0;
                        $scope.temp.no_re_sub_cheque_total = 0;
                        $scope.temp.no_out_cheque_total = 0;
                        $scope.temp.no_ret_cheque_total = 0;
                        $scope.temp.no_cancle_cheque_total = 0;
                        $scope.temp.no_realise_cheque_total = 0;
                        $scope.temp.realise_cheque_amt_total = 0;
                        $scope.temp.sub_cheque_amt_total = 0;
                        $scope.temp.re_sub_cheque_amt_total = 0;
                        $scope.temp.out_cheque_amt_total = 0;
                        $scope.temp.ret_cheque_amt_total = 0;
                        $scope.temp.cancle_cheque_amt_total = 0;
                        

                        for (var j = 0; j < $scope.report_data.length; j++) {
                                  $scope.temp.no_sub_cheque_total = parseFloat($scope.temp.no_sub_cheque_total) + parseFloat($scope.report_data[j].no_sub_cheque);
                                  $scope.temp.no_re_sub_cheque_total = parseFloat($scope.temp.no_re_sub_cheque_total) + parseFloat($scope.report_data[j].no_re_sub_cheque);
                                  $scope.temp.no_out_cheque_total = parseFloat($scope.temp.no_out_cheque_total) + parseFloat($scope.report_data[j].no_out_cheque);
                                  $scope.temp.no_ret_cheque_total = parseFloat($scope.temp.no_ret_cheque_total) + parseFloat($scope.report_data[j].no_ret_cheque);
                                  $scope.temp.no_cancle_cheque_total = parseFloat($scope.temp.no_cancle_cheque_total) + parseFloat($scope.report_data[j].no_cancle_cheque);

                                $scope.temp.no_realise_cheque_total = parseFloat($scope.temp.no_realise_cheque_total) + parseFloat($scope.report_data[j].no_realise_cheque);
                                $scope.temp.sub_cheque_amt_total = parseFloat($scope.temp.sub_cheque_amt_total) + parseFloat($scope.report_data[j].sub_cheque_amt);
                                $scope.temp.re_sub_cheque_amt_total = parseFloat($scope.temp.re_sub_cheque_amt_total) + parseFloat($scope.report_data[j].re_sub_cheque_amt);
                                $scope.temp.out_cheque_amt_total = parseFloat($scope.temp.out_cheque_amt_total) + parseFloat($scope.report_data[j].out_cheque_amt);
                                $scope.temp.ret_cheque_amt_total = parseFloat($scope.temp.ret_cheque_amt_total) + parseFloat($scope.report_data[j].ret_cheque_amt);
                                $scope.temp.cancle_cheque_amt_total = parseFloat($scope.temp.cancle_cheque_amt_total) + parseFloat($scope.report_data[j].cancle_cheque_amt);
                                $scope.temp.realise_cheque_amt_total = parseFloat($scope.temp.realise_cheque_amt_total) + parseFloat($scope.report_data[j].realise_cheque_amt);
                                
                            //
                        }
                    }
                    else {
                        swal({ title: "Alert", text: " Sorry !!!!Data is not Available", showCloseButton: true, width: 300, height: 200 });
                        $scope.report_data = [];
                        $scope.temp.no_sub_cheque_total = 0;
                        $scope.temp.no_re_sub_cheque_total = 0;
                        $scope.temp.no_out_cheque_total = 0;
                        $scope.temp.no_ret_cheque_total = 0;
                        $scope.temp.no_cancle_cheque_total = 0;
                        $scope.temp.no_realise_cheque_total = 0;
                        $scope.temp.realise_cheque_amt_total = 0;
                        $scope.temp.sub_cheque_amt_total = 0;
                        $scope.temp.re_sub_cheque_amt_total = 0;
                        $scope.temp.out_cheque_amt_total = 0;
                        $scope.temp.ret_cheque_amt_total = 0;
                        $scope.temp.cancle_cheque_amt_total = 0;
                    }

                  

                });
            }


            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.colsvis = false;


            $scope.exportData = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {


                        if ($scope.temp.report_status == 'S') {
                            var blob = new Blob([document.getElementById('rpt_data').innerHTML],
                             {
                                 type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                             });
                            $scope.detailsField = false;

                        }
                        else {

                            var blob = new Blob([document.getElementById('rpt_data1').innerHTML],
                            {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            $scope.showsumaryField = false;

                        }
                        saveAs(blob, "SubstituteLectureDetailsReport.xls");
                        $scope.colsvis = false;
                        $scope.getsubdetail();

                    }

                });
                $scope.colsvis = true;

            };


            $scope.print = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Print?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {

                        var docHead = document.head.outerHTML;
                        var printContents = document.getElementById('rpt_data1').outerHTML;
                        //$scope.colsvis = false;
                        var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";


                        var newWin = window.open("", "_blank", winAttr);
                        var writeDoc = newWin.document;
                        writeDoc.open();
                        writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                        writeDoc.close();
                        newWin.focus();

                    }

                });
                $scope.colsvis = true;

            };

            $scope.reset_data = function () {
                debugger;
                $scope.temp.no_sub_cheque_total = 0;
                $scope.temp.no_re_sub_cheque_total = 0;
                $scope.temp.no_out_cheque_total = 0;
                $scope.temp.no_ret_cheque_total = 0;
                $scope.temp.no_cancle_cheque_total = 0;
                $scope.temp.no_realise_cheque_total = 0;
                $scope.temp.realise_cheque_amt_total = 0;
                $scope.temp.sub_cheque_amt_total = 0;
                $scope.temp.re_sub_cheque_amt_total = 0;
                $scope.temp.out_cheque_amt_total = 0;
                $scope.temp.ret_cheque_amt_total = 0;
                $scope.temp.cancle_cheque_amt_total = 0;
                
                $scope.temp.sims_doc_code = '';
                $scope.report_data = [];


            }


            //sort
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };
           

          


        }])

})();

