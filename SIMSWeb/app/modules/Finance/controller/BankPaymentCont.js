﻿(function () {
    'use strict';
    var main, temp;
    var data1 = [];
    var data2 = [];
    var data3 = [];
    var Bankdetails = [];
    var status = "";
    var msg_flag1 = false;
    var acconcode = "";
    var prvno = "";
    var cmbvalue = "";
    var data = [];
    var cost = [];
    var chk;
    var coad_pty_short_name = [];
    var simsController = angular.module('sims.module.Finance');


    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('BankPaymentCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            
            $scope.edt = "";
            $scope.edt1 = [];
            $scope.operation = true;
            $scope.table1 = true;
            $scope.addbtn = true;
            $scope.btnDelete = false;
            $scope.users = [];
            $scope.chkcost_Ac = false;
            $scope.data = [];
            $scope.Bankdetails = "";
            $scope.Bankdetails = [];
            $scope.chkcost = false;
            $scope.cost_combo = false;
            $scope.geDocCode_new = [];
            $scope.edt5 = [];
            $scope.temp = {};
            $scope.temp3 = {};
            $scope.supplierdetails = true;
            $scope.pendingsupplierdetails = true;
            $scope.othersupplierdetails = true;
            var user = $rootScope.globals.currentUser.username.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
            var vat_percent = $scope.finnDetail.comp_vat_per;
            var comp_vat_status = $scope.finnDetail.comp_vat_status;
            var input_vat_acct = $scope.finnDetail.input_vat_acct;
            var vat_ac_name = '';

            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=Single").then(function (res) {
                $scope.final_doc_url = res.data;
                });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            //$http.get(ENV.apiUrl + "api/BankPayment/getDocCode?comp_code=" + comp_code + "&finance_year=" + finance_year)
            $http.get(ENV.apiUrl + "api/JVCreation/getDocCodeJV?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                debugger;
                $scope.geDocCode = res.data;

                for (var i = 0; i < $scope.geDocCode.length; i++) {
                    if ($scope.geDocCode[i].gldc_doc_code_type == "BP") {
                        $scope.geDocCode_new.push($scope.geDocCode[i]);
                    }
                }
                if ($scope.geDocCode_new.length > 1) {
                    $scope.dis_doc_cd = false;
                    $scope.temp3['gltd_doc_code'] = $scope.geDocCode_new[0].gltd_doc_code;

                    $http.get(ENV.apiUrl + "api/BankPayment/GetAllCashBankGLMasterAccountNo?cmp_cd=" + comp_code + "&financialyear=" + finance_year + "&doc_cd=" + $scope.temp3.gltd_doc_code + "&username=" + user).then(function (docstatus2) {

                        $scope.bankname = docstatus2.data
                        $scope.edt5['master_acno'] = docstatus2.data[0];
                        $scope.getAccountName(docstatus2.data[0])
                    });
                }
                else if ($scope.geDocCode_new.length ==1) {
                    $scope.dis_doc_cd = true;
                    $scope.temp3['gltd_doc_code'] = $scope.geDocCode_new[0].gltd_doc_code;
                    $http.get(ENV.apiUrl + "api/BankPayment/GetAllCashBankGLMasterAccountNo?cmp_cd=" + comp_code + "&financialyear=" + finance_year + "&doc_cd=" + $scope.geDocCode_new[0].gltd_doc_code + "&username=" + user).then(function (docstatus2) {

                        $scope.bankname = docstatus2.data
                        $scope.edt5['master_acno'] = docstatus2.data[0];
                        $scope.getAccountName(docstatus2.data[0])
                    });
                }

                //for (var i = 0; i < $scope.geDocCode.length; i++) {
                //    if ($scope.geDocCode[i].gltd_doc_code == "BP") {
                //        $scope.getDocCode = $scope.geDocCode[i].gltd_doc_code;
                //    }
                //}
                //$scope.temp3 = {
                //    gltd_doc_code: $scope.getDocCode
                //}
                //$http.get(ENV.apiUrl + "api/BankPayment/GetAllCashBankGLMasterAccountNo?cmp_cd=" + comp_code + "&financialyear=" + finance_year + "&doc_cd=" + $scope.getDocCode + "&username=" + user).then(function (docstatus2) {
                   
                //    $scope.bankname = docstatus2.data
                //    $scope.edt5['master_acno'] = docstatus2.data[0];
                //    $scope.getAccountName(docstatus2.data[0])

                //    //    $scope.edt5={

                //    //        master_acno: $scope.bankname[0].master_acno,
                //    //        master_ac_cdname: $scope.bankname[0].master_ac_cdname
                //    //}
                //    //    $scope.getAccountName($scope.edt5.master_acno);
                //});
            });

            $scope.show_master_ac = function (doc_cd) {
                debugger;
                $http.get(ENV.apiUrl + "api/BankPayment/GetAllCashBankGLMasterAccountNo?cmp_cd=" + comp_code + "&financialyear=" + finance_year + "&doc_cd=" + doc_cd + "&username=" + user).then(function (docstatus2) {

                    $scope.bankname = docstatus2.data
                    $scope.edt5['master_acno'] = docstatus2.data[0];
                    $scope.getAccountName(docstatus2.data[0])
                });

            }
            
            $scope.CheckPrintReport = function () {
                

                var data = {
                    location: $scope.final_doc_url,
                    parameter: {
                        comp_detail: comp_code,
                        doc_code: $scope.temp3.gltd_doc_code,
                        doc_no: $scope.Docno,

                    },

                    state: 'main.Fin141'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')


            }
           
            $scope.cancel = function () {
                $scope.addbtn = true;
                $scope.updatebtn = false;
                $scope.chkcost = false;
                $scope.cost_combo = false;
                $scope.edt = "";
                $scope.edt1 = "";
                $("#cmb_acc_Code3").select2("val", "");
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                format: "dd-mm-yyyy",
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            //var yyyy = today.getFullYear();
            //$scope.sdate = yyyy + '-' + mm + '-' + dd;
            //$scope.edate = yyyy + '-' + mm + '-' + dd;
            //$scope.edt4 = {
            //    DocDate: yyyy + '-' + mm + '-' + dd,
            //    PostDate: yyyy + '-' + mm + '-' + dd,
            //    ChequeDate: yyyy + '-' + mm + '-' + dd,
            //}

            var yyyy = today.getFullYear();
            $scope.sdate = dd +'-' + mm + '-' + yyyy ;
            $scope.edate = dd + '-' + mm + '-' + yyyy;//yyyy + '-' + mm + '-' + dd;
            $scope.edt4 = {
                DocDate: dd + '-' + mm + '-' + yyyy,
                PostDate: dd + '-' + mm + '-' + yyyy,
                ChequeDate: dd + '-' + mm + '-' + yyyy,
                gltd_payment_date: dd + '-' + mm + '-' + yyyy,
            }


            $scope.getDepartcode = function (str) {
               
                $http.get(ENV.apiUrl + "api/BankPayment/getCostCenter?aacno=" + str + "&comp_cd=" + comp_code + "&fyear=" + finance_year).then(function (cost_center) {

                    $scope.costcenter = cost_center.data;
                    if ($scope.costcenter.length > 0) {
                        $scope.chkcost = true;
                    }
                    else {
                        $scope.chkcost = false;
                        $scope.temp.cost_chk = false;
                        $scope.cost_combo = false;
                    }
                });

                for (var i = 0; i<$scope.getAllAccNos.length; i++) {
                    if ($scope.getAllAccNos[i].gldd_acct_code == str) {

                        $scope.edt1 = {
                            coad_dept_no: $scope.getAllAccNos[i].gldd_dept_code + '-' + $scope.getAllAccNos[i].gldd_dept_name,
                            //coad_dept_no: $scope.getAllAccNos[i].gldd_dept_code,
                            gldd_acct_code: $scope.getAllAccNos[i].gldd_acct_code,
                            codp_dept_name: $scope.getAllAccNos[i].gldd_dept_name,
                            gldd_acct_name: $scope.getAllAccNos[i].gldd_acct_name,
                        }

                    }
                }


            }

            $scope.cost_center = function (str) {
                
                chk = str;
                if (str == true) {
                    $scope.cost_combo = true;
                }
                else {
                    $scope.cost_combo = false;
                }
            }

            $scope.cost_center_Account = function (str) {
                chk = str;
                
                if (str == true) {
                    $scope.cost_combo_AC = true;
                }
                else {
                    $scope.cost_combo_AC = false;
                }

            }



            //Fill Combo Ledger
            $http.get(ENV.apiUrl + "api/BankPayment/GetLedgerNumber?financialyear="+finance_year + "&comp_code="+comp_code).then(function (docstatus2) {
                $scope.LdgrCode = docstatus2.data;
                });




            $scope.GetAllGLAcc = function (acconcode, cmpnycode) {
                
                $http.get(ENV.apiUrl + "api/BankPayment/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {
                    $scope.getAllAccNos = docstatus.data;
                });
            }

                $http.get(ENV.apiUrl + "api/BankPayment/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {
                    
                    $scope.getAllAccNos = docstatus.data;
                });

            //Fill Combo SLACCNO
            $scope.getSLAccNo = function (slcode) {

                $scope.getAllAccNos = [];
                var acconcode = $scope.getAllAccNos.gldd_acct_code;
                if (slcode == "00") {
                    $scope.GetAllGLAcc(acconcode, comp_code);
                }
                else {

                    $http.get(ENV.apiUrl + "api/BankPayment/GetSLAccNumber?pbslcode=" + slcode + "&cmp_cd=" + comp_code + "&fyear=" + finance_year).then(function (docstatus3) {
                        // $scope.sllc_ldgr_code = slcode;
                        $scope.getAllAccNos = docstatus3.data;
                        //$scope.slacno = docstatus3.data;
                    });
                }
                $("#cmb_acc_Code3").select2("val", cmbvalue);
            }

            

            setTimeout(function () {
                $("#cmb_acc_Code3").select2();

            }, 100);

            $scope.summ = function () {

                $scope.total = 0;

                for (var i = 0; i < $scope.Bankdetails.length; i++) {

                    $scope.total = $scope.total + parseInt($scope.Bankdetails[i].gldd_doc_amount);
                    }

            }

            $scope.datagrid = function (myForm) {
                debugger
                $scope.chkcost = false;
                $scope.cost_combo = false;
                $scope.temp['cost_chk']=false;

                if ($scope.edt.gldd_acct_code == undefined || $scope.edt.gldd_acct_code == "") {
                    $scope.flag1 = true;
                    swal({  text: "Please select Account name", showCloseButton: true, width: 380, });

                }
                else if ($scope.edt.gldd_doc_amount == undefined || $scope.edt.gldd_doc_amount == "") {
                    $scope.flag1 = true;
                    swal({  text: "Please Enter Amount", showCloseButton: true, width: 380, });
                }
                else {

                    var f = false;
                    var ledcode = "";
                    if (true) {
                        for (var j = 0; j < $scope.Bankdetails.length; j++) {
                            if ($scope.Bankdetails[j].sllc_ldgr_code == $scope.edt.sllc_ldgr_code && $scope.Bankdetails[j].slma_acno == $scope.edt1.gldd_acct_code && $scope.Bankdetails[j].gldd_doc_narr == $scope.edt.gldd_doc_narr) {
                                f = true;
                            }
                        }
                        if (!f) {
                            if ($scope.edt.sllc_ldgr_code == undefined) {
                                 ledcode = '00';
                                
                                //var acconcode = $scope.getAllAccNos.gldd_acct_code;
                                //$scope.GetAllGLAcc(acconcode, comp_code);
                            }
                            else {
                                ledcode = $scope.edt.sllc_ldgr_code;
                            }

                            var dept_no = $scope.edt1.coad_dept_no.split('-');

                                 var costcenter1 = document.getElementById("costCenter")
                                 var costcentername = costcenter1.options[costcenter1.selectedIndex].text;
                                 var terminal = document.getElementById("cmb_acc_Code3");
                                 $scope.selectedText = terminal.options[terminal.selectedIndex].text;
                                 $scope.Bankdetails.push({
                                'gldd_comp_code': comp_code,
                                'sllc_ldgr_code': ledcode,
                                'slma_acno': $scope.edt1.gldd_acct_code,
                                'coad_pty_full_name': $scope.selectedText,
                                'coad_dept_no': dept_no[0],
                                'gldd_party_ref_date': $scope.edt.gldd_party_ref_date,
                                'gldd_doc_narr': $scope.edt.gldd_doc_narr,
                                'gldd_doc_amount': $scope.edt.gldd_doc_amount,
                                'glco_cost_centre_code': $scope.edt.glco_cost_centre_code,
                                'coce_cost_centre_name1': costcentername,
                                'gldd_fc_amount': 0.00

                            });
                            $scope.total = 0;
                            $scope.fctotal = 0;

                            for (var i = 0; i < $scope.Bankdetails.length; i++) {

                                
                                $scope.total = parseFloat(parseFloat($scope.total) + parseFloat($scope.Bankdetails[i].gldd_doc_amount)).toFixed(3);
                                // $scope.fctotal = $scope.fctotal + parseInt($scope.Bankdetails[i].gldd_fc_amount);
                                $scope.fctotal = parseFloat($scope.fctotal) + parseFloat($scope.Bankdetails[i].gldd_fc_amount);
                                }
                            //$scope.btnDelete = true;


                            //$scope.fctotal = 0;

                            //for (var i = 0; i < $scope.Bankdetails.length; i++) {
                            //    $scope.fctotal = $scope.fctotal + parseInt($scope.Bankdetails[i].gldd_fc_amount);
                            //    //}
                            $scope.edt = "";
                            $scope.edt1 = "";
                            $scope.edt2 = "";
                            $("#cmb_acc_Code3").select2("val", "");
                            $scope.myForm.$setPristine();
                            $scope.myForm.$setUntouched();
                            Bankdetails = [];
                        }
                        else {
                            swal({ text: "This Account Name Already Present", width: 380 });
                            return;
                        }
                        //$scope.selectedText = "";
                    }
                    var acconcode = $scope.getAllAccNos.gldd_acct_code;
                    $scope.GetAllGLAcc(acconcode, comp_code);
                }
            };

            $scope.up = function (str) {

                $scope.Add = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                //$scope.edt = str;
                //var terminal1 = document.getElementById("cmb_acc_Code3");
                //$scope.selectedText = terminal1.options[terminal1.selectedIndex].text;

                $scope.edt =
                {
                    // slma_acno:str.edt.slma_acno,
                    ////coad_dept_no: str.coad_dept_no,
                    ////gldd_acct_code: str.gldd_acct_code,
                    ////gldd_acct_name:str.gldd_acct_name,
                    //sllc_ldgr_code: str.sllc_ldgr_code,
                    ////gldd_party_ref_date: str.gldd_party_ref_date,
                    ////gldd_doc_amount: str.gldd_doc_amount,
                    ////gldd_doc_narr: str.gldd_doc_narr,
                    // gldd_acct_name: str.gldd_acct_name,
                    // gldd_acct_code: str.slma_acno,

                    //  selectedText:select2("val", str.gldd_acct_code)
                    coad_dept_no: str.coad_dept_no,
                    gldd_acct_code: str.slma_acno,
                    gldd_acct_name: str.coad_pty_full_name,
                    sllc_ldgr_code: str.sllc_ldgr_code,
                    gldd_party_ref_date: str.gldd_party_ref_date,
                    gldd_doc_amount: str.gldd_doc_amount,
                    gldd_doc_narr: str.gldd_doc_narr,

                };
                cmbvalue = str.slma_acno;
                //  $scope.getSLAccNo(str.sllc_ldgr_code);
                //$scope.getDepartcode($scope.edt.gldd_acct_code);
                setTimeout(function () {
                    // $('#cmb_acc_Code3').val(str.slma_acno);
                    $('#cmb_acc_Code3').val(str.slma_acno)
                    $('#cmb_acc_Code3').trigger('change')
                    // $("#cmb_acc_Code3").select2("val", str.slma_acno);
                }, 1000);
                $scope.edt1 =
              {
                  coad_dept_no: str.coad_dept_no,
                  gldd_acct_code: str.slma_acno,
                  gldd_acct_name: str.gldd_acct_name
              };
            }
            $scope.totalAmountCount = function () {
                debugger;
               
                $scope.total = 0;

                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    $scope.outstandingAmt = parseFloat($scope.Bankdetails[i].gldd_doc_amount);
                    $scope.invoiceAmt = parseFloat($scope.Bankdetails[i].invoice);
                    if ($scope.outstandingAmt > $scope.invoiceAmt) {
                        swal({ text: "Amount should be less than " + $scope.invoiceAmt, imageUrl: "assets/img/close.png", width: 400, height: 250 });
                        
                        $scope.Bankdetails[i].gldd_doc_amount = parseFloat($scope.Bankdetails[i].invoice).toFixed(3);
                        $scope.total = parseFloat($scope.Bankdetails[i].invoice).toFixed(3);
                    }
                    else {
                        $scope.total = parseFloat(parseFloat($scope.total) + parseFloat($scope.Bankdetails[i].gldd_doc_amount)).toFixed(3);
                    }
                }
            }

            $scope.Update = function () {

                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    //  $scope.data2.push($scope.Bankdetails);
                    if ($scope.Bankdetails.slma_acno == "")

                    { }
                    else {
                        var terminal = document.getElementById("cmb_acc_Code3");
                        var selectedText = terminal.options[terminal.selectedIndex].text;
                        $scope.Bankdetails.pop({
                            'gldd_comp_code': comp_code,
                            'sllc_ldgr_code': $scope.edt.sllc_ldgr_code,
                            'slma_acno': $scope.edt1.gldd_acct_code,
                            'coad_pty_full_name': selectedText,
                            'coad_dept_no': $scope.edt1.coad_dept_no,
                            'gldd_party_ref_date': $scope.edt.gldd_party_ref_date,
                            'gldd_doc_narr': $scope.edt.gldd_doc_narr,
                            'gldd_doc_amount': $scope.edt.gldd_doc_amount,
                            'gldd_fc_amount': 0.00

                        });
                        $scope.Bankdetails.push({
                            'gldd_comp_code': comp_code,
                            'sllc_ldgr_code': $scope.edt.sllc_ldgr_code,
                            'slma_acno': $scope.edt1.gldd_acct_code,
                            'coad_pty_full_name': selectedText,
                            'coad_dept_no': $scope.edt1.coad_dept_no,
                            'gldd_party_ref_date': $scope.edt.gldd_party_ref_date,
                            'gldd_doc_narr': $scope.edt.gldd_doc_narr,
                            'gldd_doc_amount': $scope.edt.gldd_doc_amount,
                            'gldd_fc_amount': 0.00

                        });
                        swal({  text: "Record Updated", width: 300, height: 200 });
                        $scope.total = 0;
                        for (var i = 0; i < $scope.Bankdetails.length; i++) {
                            //$scope.total = $scope.total + parseInt($scope.Bankdetails[i].gldd_doc_amount);
                            $scope.total = parseFloat(parseFloat($scope.total) + parseFloat($scope.Bankdetails[i].gldd_doc_amount)).toFixed(3);
                            
                            
                        }
                        //$scope.btnDelete = true;

                        $scope.fctotal = 0;
                        for (var i = 0; i < $scope.Bankdetails.length; i++) {

                            $scope.fctotal = $scope.fctotal + parseInt($scope.Bankdetails[i].gldd_fc_amount);
                            }
                        $scope.edt = "";
                        $scope.edt1 = "";
                        $scope.edt2 = "";
                        $scope.updatebtn = false;
                        $("#cmb_acc_Code3").select2("val", "");
                        // $scope.edt.slma_acno = '';
                        $scope.myForm.$setPristine();
                        $scope.myForm.$setUntouched();
                        Bankdetails = [];
                        //$scope.edt = "";
                        //$scope.edt1 = "";
                        //$scope.edt2 = "";
                        //$scope.updatebtn = false;
                        //$scope.Add = true;
                        //$("#cmb_acc_Code3").select2("val", "");
                    };

                }
            }

            $scope.coad_pty_full_name = $scope.selectedText;
            

           
            $scope.Delete = function (str) {
                debugger;
                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    if ($scope.Bankdetails[i].slma_acno == str.slma_acno && $scope.Bankdetails[i].gldd_doc_amount == str.gldd_doc_amount) {
                        $scope.Bankdetails.splice(i, 1);
                        break;
                    }
                }
                $scope.total = 0;
                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    //$scope.total = parseFloat($scope.total) + parseFloat($scope.Bankdetails[i].gldd_doc_amount);
                    $scope.total = parseFloat(parseFloat($scope.total) + parseFloat($scope.Bankdetails[i].gldd_doc_amount)).toFixed(3);
                }

                //$scope.Bankdetails = [];
                //$scope.Billdetailsdata = [];
                //$scope.voucherdata = [];
            }




            $scope.getAccountName = function (str)
            {
               
                cost = str.master_acno;
                var cost_center = cost;

                $http.get(ENV.apiUrl + "api/BankPayment/getCostCenter?aacno=" + cost_center + "&comp_cd=" + comp_code + "&fyear=" + finance_year).then(function (cost_center) {
                   
                    $scope.costcenter_account = cost_center.data;
                    if ($scope.costcenter_account.length > 0) {
                        $scope.chkcost_Ac = true;
                    }
                    else {
                        $scope.chkcost_Ac = false;
                        $scope.temp.cost_chk_ac = false;
                        $scope.cost_combo_AC = false;
                    }

                    });



                for (var i = 0; i < $scope.bankname.length; i++) {
                    if ($scope.bankname[i].master_acno == str.master_acno) {
                        acconcode = $scope.bankname[i].master_acno;
                        $scope.edt3 = {
                            master_ac_cdname: $scope.bankname[i].master_ac_cdname,
                        }
                    }

                }
            }

           

            $http.get(ENV.apiUrl + "api/BankPayment/GetVerifyAuthorizeUsers?username=" + user + "&comp_code=" + comp_code + "&fyear=" + finance_year).then(function (users_list) {

                $scope.users = users_list.data;
                 if ($scope.users.length != 0) {

                    if ($scope.users[0] == user && $scope.users[1] == user) {
                        $scope.preparebtn = false;
                        $scope.verifybtn = false;
                        $scope.authorizebtn = false;
                    }
                    else if ($scope.users[0] == user && ($scope.users[1] == "" || $scope.users[1] != user)) {
                        $scope.verifybtn = false;
                        $scope.preparebtn = false;
                        $scope.authorizebtn = true;
                    }
                    else {
                        $scope.preparebtn = false;
                        $scope.verifybtn = true;
                        $scope.authorizebtn = true;
                    }
                }
            });

            $scope.Authorizeuser = function () {
                $http.get(ENV.apiUrl + "api/BankPayment/GetVerifyAuthorizeUsers?username=" + user + "&comp_code=" + comp_code + "&fyear=" + finance_year).then(function (users_list) {

                    $scope.users = users_list.data;
                    if ($scope.users.length != 0) {

                        if ($scope.users[0] == user && $scope.users[1] == user) {
                            $scope.preparebtn = false;
                            $scope.verifybtn = false;
                            $scope.authorizebtn = false;
                        }
                        else if ($scope.users[0] == user && $scope.users[0] != user) {
                            $scope.verifybtn = false;
                            $scope.preparebtn = true;
                            $scope.authorizebtn = true;
                        }
                        else if ($scope.users[0] != user && $scope.users[0] != user) {
                            $scope.verifybtn = true;
                            $scope.preparebtn = false;
                            $scope.authorizebtn = true;
                        }
                    }
                });
            }

            $scope.Insert_temp_docs = function () {

                var datasend = [];


                var verifyUser, verifyDate, authorize_user, authorize_date;
                if (status == "Verify") {
                    verifyUser = user;
                    verifyDate = $scope.edt4.DocDate;
                }
                else if (status == "Authorize") {
                    verifyUser = user;
                    verifyDate = $scope.edt4.DocDate;
                    authorize_user = "";
                    authorize_date = "";
                }
                else {
                    verifyUser = "";
                    verifyDate = "";
                    authorize_user = "";
                    authorize_date = "";

                }
                if ($scope.temp == undefined) {
                    $scope.temp = "";
                }
                var data = {
                    gltd_comp_code: comp_code,
                    gltd_prepare_user: user,
                    gltd_doc_code: $scope.temp3.gltd_doc_code,
                    gltd_doc_date: $scope.edt4.DocDate,
                    gltd_cur_status: status,
                    gltd_post_date: $scope.edt4.PostDate,
                    gltd_doc_narr: $scope.temp.gltd_doc_narr1,
                    gltd_remarks: $scope.temp.gltd_doc_remark,
                    gltd_prepare_date: $scope.edt4.DocDate,
                    gltd_final_doc_no: "0",
                    gltd_verify_user: verifyUser,
                    gltd_verify_date: verifyDate,
                    gltd_authorize_user: authorize_user,
                    gltd_authorize_date: authorize_date,
                    gltd_paid_to: $scope.temp.gltd_paid_to,
                    gltd_cheque_no: $scope.temp.gltd_cheque_No,
                    gltd_payment_date: $scope.edt4.gltd_payment_date
                }
                datasend.push(data);
                
                $http.post(ENV.apiUrl + "api/BankPayment/Insert_Fins_temp_docs", datasend).then(function (msg) {
                    $scope.prvno = msg.data;

                    if ($scope.prvno != "" && $scope.prvno != null) {
                        $scope.Insert_Fins_temp_doc_details();
                    }
                    else {
                        swal({  text: "Record Not inserted. " + $scope.prvno, imageUrl: "assets/img/close.png",width: 300, height: 200 });
                    }
                });

            }

            $scope.Insert_Fins_temp_doc_details = function () {
                debugger;
                var dataSend = [];
                var j = 1;

                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    var data = {
                        gldd_doc_narr: $scope.Bankdetails[i].gldd_doc_narr,
                        gldd_dept_code: $scope.Bankdetails[i].coad_dept_no,
                        gldd_ledger_code: $scope.Bankdetails[i].sllc_ldgr_code,
                        gldd_acct_code: $scope.Bankdetails[i].slma_acno,
                        gldd_party_ref_no: $scope.Bankdetails[i].slma_acno,
                        gldd_party_ref_date: $scope.Bankdetails[i].gldd_party_ref_date,
                        gldd_doc_amount: parseFloat($scope.Bankdetails[i].gldd_doc_amount),
                        gldd_cost_center_code: $scope.Bankdetails[i].glco_cost_centre_code,
                        gldd_comp_code: comp_code,
                        gldd_doc_code: $scope.temp3.gltd_doc_code,
                        gldd_prov_doc_no: $scope.prvno,
                        gldd_final_doc_no: "0",
                        gldd_line_no: j,
                        gltd_remarks: $scope.Bankdetails[i].gldd_doc_code + "/" + $scope.Bankdetails[i].gldd_final_doc_no+ '#' + $scope.Bankdetails[i].gldd_line_no + '$'+ $scope.Bankdetails[i].gldd_doc_amount,
                    }
                    j++;
                    dataSend.push(data);
                }
                var data = {
                    gldd_comp_code: comp_code,
                    gldd_doc_code: $scope.temp3.gltd_doc_code,
                    gldd_prov_doc_no: $scope.prvno,
                    gldd_final_doc_no: "0",
                    gldd_line_no: j,
                    gldd_ledger_code: "00",
                    gldd_acct_code: $scope.edt5.master_acno.master_acno,
                    gldd_cost_center_code: $scope.edt5.glco_cost_centre_code,
                    gldd_doc_amount: ("-" + $scope.total),
                    gldd_fc_amount_debit: 0,
                    gldd_fc_code: "",
                    gldd_fc_rate: 0,
                    gldd_doc_narr: $scope.temp.gltd_doc_narr1,
                    gldd_dept_code: $scope.edt5.master_acno.gldd_dept_code,
                }
                dataSend.push(data);
                $http.post(ENV.apiUrl + "api/BankPayment/Insert_Fins_temp_doc_details", dataSend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if (status == "Authorize") {
                        msg_flag1 = true;
                        $scope.Authorize_Insert_temp_doc_details($scope.msg1);
                    }
                    else if (status == "Save") {
                        msg_flag1 = false;
                        $scope.Authorize_Insert_temp_doc_details($scope.msg1);

                    }
                    else if (status == "Verify") {
                        msg_flag1 = false;
                        $scope.Authorize_Insert_temp_doc_details($scope.msg1);

                    }
                    if ($scope.msg1 == true) {

                    }
                    else {
                        swal({  text: "Record Not inserted. " + $scope.msg1,imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                });
            }

            $scope.Authorize_Insert_temp_doc_details = function (str) {
                
                if (msg_flag1) {
                    msg_flag1 = false;
                    var auth_date = $scope.edt4.DocDate;
                    $http.post(ENV.apiUrl + "api/BankPayment/Authorize_Posting?comp_code=" + comp_code + "&doc_code=" + $scope.temp3.gltd_doc_code + "&prv_no=" + $scope.prvno + "&auth_user=" + user + "&auth_date=" + auth_date).then(function (Auth) {

                        $scope.Docno = Auth.data;
                        if ($scope.Docno != "") {
                            swal({  text: "Posted Successfully\nFinal Doc No is=" + $scope.Docno, imageUrl: "assets/img/check.png",width: 300, height: 200 });
                            $scope.checkprintbtn = true;
                            $scope.ClearInsert();
                            $scope.Authorizeuser();
                            $scope.cancel();
                        }
                        else {
                            swal({  text: "Posting Failed", width: 300, height: 200 });
                        }
                    });

                }
                else if ((msg_flag1 == false) && (status == "Save")) {
                    msg_flag1 = false;
                    if (str) {
                        swal({
                            text: "Record Inserted Successfully\nProvision No:-" + $scope.temp3.gltd_doc_code + "--" + $scope.prvno, imageUrl: "assets/img/check.png",width: 300, height: 200
                        });
                        $scope.ClearInsert();
                        $scope.Authorizeuser();
                        $scope.cancel();
                    }
                    else {
                        swal({  text: "Record Not inserted", imageUrl: "assets/img/close.png",width: 300, height: 200 });
                    }
                }

                else if ((msg_flag1 == false) && (status == "Verify")) {
                    msg_flag1 = false;
                    if (str) {
                        swal({  text: "Record Verified Successfully\nProvision No:-" + $scope.temp3.gltd_doc_code + "--" + $scope.prvno, imageUrl: "assets/img/check.png",width: 300, height: 200 });
                        $scope.ClearInsert();
                        $scope.Authorizeuser();
                        $scope.cancel();
                    }
                    else {
                        swal({  text: "Verification Failed", width: 300, height: 200 });
                    }
                }

                $scope.ClearInsert();
                $scope.Authorizeuser();
            }

            $scope.Prepare = function () {
                $scope.chkcost_Ac = false;
                $scope.cost_combo_Ac = false;

                try {
                    if ($scope.Bankdetails.length > 0) {
                        $scope.flag1 = false;
                        if ($scope.edt4.PostDate == null || $scope.edt4.PostDate == "") {
                            $scope.flag1 = true;
                            swal({  text: "Please Select Posting Date", showCloseButton: true, width: 380, });

                        }

                        else {

                            $scope.preparebtn = true;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                            status = "Save";
                            $scope.Insert_temp_docs();

                        }
                    }
                    else {
                        swal({  text: "Please Add Doc Details", showCloseButton: true, width: 380, });
                    }
                }


                catch (e) {

                }


            }

            $scope.Verify = function () {
                try {
                    if ($scope.Bankdetails.length > 0) {
                        $scope.flag1 = false;
                        if ($scope.edt4.PostDate == null || $scope.edt4.PostDate == "") {
                            $scope.flag1 = true;
                            swal({  text: "Please Select Posting Date", showCloseButton: true, width: 380, });

                        }

                        else {
                            $scope.preparebtn = true;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                            status = "Verify";
                            $scope.Insert_temp_docs();
                        }
                    }
                    else {
                        swal({  text: "Please Add Doc Details", showCloseButton: true, width: 380, });
                    }
                }
                catch (e) {
                }
            }

            $scope.Authorize = function () {
                try {
                    if ($scope.Bankdetails.length > 0) {
                        $scope.flag1 = false;
                        if ($scope.edt4.PostDate == null || $scope.edt4.PostDate == "") {
                            $scope.flag1 = true;
                            swal({  text: "Please Select Posting Date", showCloseButton: true, width: 380, });

                        }

                        else {
                            $scope.preparebtn = true;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                            status = "Authorize";
                            $scope.Insert_temp_docs();
                        }
                    }
                    else {
                        swal({  text: "Please Add Doc Details", showCloseButton: true, width: 380, });
                    }


                } catch (e) {

                }
            }

            $scope.ClearInsert = function () {
                $scope.Bankdetails = [];
                $scope.total = "";
                $scope.temp = {
                    gltd_cheque_No: "",
                    gltd_paid_to: "",
                    gltd_doc_narr1: "",
                    gltd_doc_remark: "",
                }
            }

            $scope.onlyNumbers = function (event) {
                ;
                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $scope.check_post_date = function () {
                if ($scope.edt4.PostDate != undefined || $scope.edt4.PostDate != '') {

                    $http.get(ENV.apiUrl + "api/JVCreation/Get_Check_Postdate?post_date=" + $scope.edt4.PostDate + "&comp_cd=" + comp_code + "&fyear=" + finance_year).then(function (pronum) {
                        $scope.msg = pronum.data;

                        if ($scope.msg != '') {
                            swal({  text: $scope.msg, width: 300, height: 200 });

                            $scope.preparebtn = true;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                        }
                        else {
                            if ($scope.users.length != 0) {
                                if ($scope.users[0] == user && $scope.users[1] == user) {
                                    $scope.preparebtn = false;
                                    $scope.verifybtn = false;
                                    $scope.authorizebtn = false;
                                }
                                else if ($scope.users[0] == user && ($scope.users[1] == "" || $scope.users[1] != user)) {
                                    $scope.verifybtn = false;
                                    $scope.preparebtn = false;
                                    $scope.authorizebtn = true;
                                }
                                else {
                                    $scope.preparebtn = false;
                                    $scope.verifybtn = true;
                                    $scope.authorizebtn = true;
                                }
                            }
                        }

                    });

                }

            }

           /* $scope.showBillDetails = function () {
                if ($scope.edt.sllc_ldgr_code == '00' || $scope.edt.sllc_ldgr_code == undefined || $scope.edt.sllc_ldgr_code == "") {

                    swal({ text: "Please Select except General Ledger Code", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                }
                else {
                    if ($scope.edt.gldd_acct_code) {

                        $('#Billdetails').modal('show');
                    }
                    else {
                           swal({ text: "Please Select Account Name", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                       }
                }
            }*/
           /* $scope.btn_submit = function () {

                var data = {
                            gldd_comp_code: comp_code,
                            finance_year: finance_year,
                            gldd_doc_code: $scope.temp3.gltd_doc_code,
                            gldd_ledger_code: $scope.edt.sllc_ldgr_code,
                            gldd_acct_code: $scope.edt.gldd_acct_code,
                            gldd_party_ref_no: $scope.edt1.gldd_acct_code,
                }
                $http.post(ENV.apiUrl + "api/BankPayment/BillDetails", data).then(function (res) {
                                    $scope.billdata = res.data;

                                    if ($scope.billdata) {

                                        for (var i = 0; i < $scope.billdata.length; i++) {

                                            $scope.Bankdetails.push($scope.billdata[i]);
                                        }

                                    }
                                    else {

                                        $scope.Bankdetails = [];
                                    }
                                });
            }*/

            $scope.show_bills = function () {
                debugger;
                var main = document.getElementById('mainchk');
                main.checked = false;
               
                var data = {
                    gldd_comp_code: comp_code,
                    finance_year: finance_year,
                    gldd_doc_code: $scope.temp3.gltd_doc_code,
                    gldd_ledger_code: $scope.edt.sllc_ldgr_code,
                    gldd_acct_code: $scope.edt.gldd_acct_code,
                    gldd_party_ref_no: $scope.edt1.gldd_acct_code,
                }
                
                if ($scope.edt.sllc_ldgr_code == '00'|| $scope.edt.sllc_ldgr_code == undefined || $scope.edt.sllc_ldgr_code =="") {
                    
                    swal({ text: "Please Select Except General Ledger", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                }
                else {
                    if ($scope.edt.gldd_acct_code) {
                        
                        $scope.Billdetailsdata = [];
                        $('#Billdetails').modal('show');
                        $http.post(ENV.apiUrl + "api/BankPayment/BillDetails", data).then(function (res) {
                            $scope.billdata = res.data;

                            if ($scope.billdata) {

                                for (var i = 0; i < $scope.billdata.length; i++) {

                                    $scope.Billdetailsdata.push($scope.billdata[i]);
                                }

                               
                            }
                            else {

                                $scope.Billdetailsdata = [];
                            }
                        });
                    }

                    else {
                        swal({ text: "Please Select Account Name", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                }
              }

          
           
            $scope.CheckAllChecked = function () {
               var main = document.getElementById('mainchk');
               $scope.Bankdetails = [];
                if (main.checked == true) {
                    for (var i = 0; i < $scope.Billdetailsdata.length; i++) {
                        $scope.Billdetailsdata[i].voucher_check = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                        $scope.voucherdata.push($scope.Billdetailsdata[i]);
                        
                    }

                }
                else {
                    for (var i = 0; i < $scope.Billdetailsdata.length; i++) {
                        $scope.Billdetailsdata[i].voucher_check = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }
            $scope.voucherdata = [];
            $scope.selectonebyone = function (index,str) {
                debugger;
                //var main = document.getElementById('mainchk');
                $scope.Bankdetails = [];
                if (str.voucher_check == true) {
                    $scope.voucherdata.push(str);
                }
               
            }
            
            $scope.btn_Add = function () {
                debugger;
                $('#Billdetails').modal('hide');
                var main = document.getElementById('mainchk');
                for (var j = 0; j < $scope.voucherdata.length; j++) {
                    if ($scope.voucherdata[j].voucher_check == true )
                    $scope.Bankdetails.push($scope.voucherdata[j]);
                }
                $scope.total = 0;
                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    $scope.total = parseFloat(parseFloat($scope.total) + parseFloat($scope.Bankdetails[i].gldd_doc_amount)).toFixed(3);
                }
                main.checked = false;
                
            }

            $scope.ShowAmountDetails = function (data) {
                debugger;
                $scope.supplierdetails = false;
                $scope.othersupplierdetails = false;
                data.doc_code = $scope.temp3.gltd_doc_code;
                data.finance_year = finance_year;
               // console.log(data);
                $http.post(ENV.apiUrl + "api/BankPayment/ShowAmountDetails",data).then(function (res) {
                    $scope.Amountdetails = res.data;
                    console.log($scope.Amountdetails);

                });
            }

            $scope.ShowPendingAmountDetails = function (data) {
                debugger;
                $scope.supplierdetails = false;
                $scope.pendingsupplierdetails = false;
                data.doc_code = $scope.temp3.gltd_doc_code;
                data.finance_year = finance_year;
                $http.post(ENV.apiUrl + "api/BankPayment/ShowPendingAmountDetails", data).then(function (res) {
                    $scope.PendingAmountdetails = res.data;
                    
                });

               
            }

            $scope.back = function () {
                $scope.supplierdetails = true;
                $scope.pendingsupplierdetails = true;
                $scope.othersupplierdetails = true;
            }
            
            //$http.get(ENV.apiUrl + "api/BankPayment/getInsertMesage").then(function (res) {
            //    ;
            //    $scope.insertmsg = res.data;
            //});

        }]
        )
})();
