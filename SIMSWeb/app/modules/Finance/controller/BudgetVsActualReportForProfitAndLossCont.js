﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('BudgetVsActualReportForProfitAndLossCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            // Start
            //$scope.name = "Subject Teacher List";
            $scope.temp = {};
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.temp = {};
            $scope.display = false;
            $scope.dublicate = true;
            $scope.showVarianceField = false;
            $scope.showsumaryField = true;
            $scope.detailsField = false;
            $scope.temp.view = 'smr';
            $scope.temp.report_status = 'TB';

            $http.get(ENV.apiUrl + "api/BAPL_Report/getCompCode1").then(function (res1) {
                debugger;
                $scope.sims_cur = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['comp_code'] = res1.data[0].comp_code;

                    $scope.getAllGroupCode(res1.data[0].comp_code);
                }
            });

            $scope.getAllGroupCode = function (comp_code) {
                debugger;
                $http.get(ENV.apiUrl + "api/BAPL_Report/getAllGroupCode?comp_code=" + comp_code).then(function (res1) {
                    $scope.group_code1 = res1.data;
                    
                    setTimeout(function () {
                        $('#grade_box').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }
            $scope.getAllSubGroupCode = function (comp_code, mom_start_date, group_code) {
                debugger;
                $http.get(ENV.apiUrl + "api/BAPL_Report/getAllSubGroupCode?comp_code=" + comp_code + "&mom_start_date=" + $scope.mom_start_date + "&group_code=" + $scope.temp.group_code).then(function (res1) {
                    $scope.sub_group_code = res1.data;

                    setTimeout(function () {
                        $('#section_box').change(function ()                        

                        {

                        }).multipleSelect({
                            width: '100%'
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }
           
            $(function () {
                $('#grade_box').multipleSelect({ width: '100%' });
                $('#section_box').multipleSelect({ width: '100%' });
            });

            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_start_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });

            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_end_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });

            $scope.getBAPLSummaryReport = function () {
                debugger
                $scope.colsvis = false;


                
                $http.get(ENV.apiUrl + "api/BAPL_Report/getBAPLSummaryReport?comp_code=" + $scope.temp.comp_code + "&mom_start_date=" +$scope.mom_start_date + "&mom_end_date=" + $scope.mom_end_date + "&report_status=" + $scope.temp.report_status + "&group_code=" +$scope.temp.group_code + "&sub_group_code=" + $scope.temp.subgroup_code + "&view=" + $scope.temp.view).then(function (res1) {
                    debugger;
                 
                   if (res1.data.length > 0) {
                        debugger;
                       $scope.report_data = res1.data;
                       $scope.temp.totalactual = 0;
                       $scope.temp.totalbudget = 0;
                       $scope.temp.totalvariance = 0;
                       $scope.temp.totalvariance_per = 0;

                        for (var j = 0; j < $scope.report_data.length; j++) {
                           debugger;
                                               
                           $scope.temp.totalbudget = parseFloat($scope.temp.totalbudget) + parseFloat($scope.report_data[j].glpr_bud_amt);
                           $scope.temp.totalactual = parseFloat($scope.temp.totalactual) + parseFloat($scope.report_data[j].actual_amount);
                           $scope.temp.totalvariance = parseFloat($scope.temp.totalvariance) + parseFloat($scope.report_data[j].variance);
                      $scope.temp.totalvariance_per = parseFloat($scope.temp.totalvariance_per) + parseFloat($scope.report_data[j].variance_per);
                        //
                        }


                        angular.forEach($scope.report_data, function (f) {

                        });
                    }
                    else {
                        swal({ title: "Alert", text: " Sorry !!!!Data is not Available", showCloseButton: true, width: 300, height: 200 });
                        $scope.report_data = res1.data;
                        $scope.temp.totalactual = 0;
                        $scope.temp.totalbudget = 0;
                        $scope.temp.totalvariance = 0;
                        $scope.temp.totalvariance_per = 0;
                       
                    }
                   console.log($scope.report_data);
                   console.log($scope.temp.totalactual);
                   console.log($scope.temp.totalbudget);

                   debugger;
                   if ($scope.temp.view == 'smr') {
                       $scope.showsumaryField = true;
                       $scope.detailsField = false;
                   }
                   else {
                       $scope.showsumaryField = false;
                       $scope.detailsField = true;
                   }

                  
                });
            }
            var data_send = [];
            var data1 = [];

            $scope.show_variance = function () {
                if ($scope.temp.showVariance == true) {
                    $scope.showVarianceField = true;
                }
                else {
                    $scope.showVarianceField = false;
                }
            }
            
            $scope.positivevalue = function (str) {
                return Math.abs(str)
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            //$scope.colsvis = false;


            $scope.exportData = function () {
                //$scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('rpt_data').innerHTML], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                        });
                        saveAs(blob, "CumulativeSubledgerSummaryReport.xls");
                        //$scope.colsvis = false;

                    }

                });
            };

            $scope.print = function (div) {

                //$scope.colsvis = false;

                var docHead = document.head.outerHTML;
                var printContents = document.getElementById('rpt_data').outerHTML;
                //$scope.colsvis = false;
                var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";


                var newWin = window.open("", "_blank", winAttr);
                var writeDoc = newWin.document;
                writeDoc.open();
                writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                writeDoc.close();
                newWin.focus();

            };






        }])

})();

