﻿/// <reference path="VehicleExpenseCont.js" />
(function () {
    'use strict';

    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('VehicleExpenseCont',
          ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

             
              $scope.vehicle_data = [];
              $scope.table = true;
              $scope.hide_check_detais = false;
              $scope.hide_cc_detais = false;
              $scope.pagesize = "5";
              $scope.pageindex = "1";
              $rootScope.visible_stud = true;
              $rootScope.chkMulti = true;
              $scope.paymentmode = [];

              $timeout(function () {
                  $("#table").tableHeadFixer({ 'top': 1 });
              }, 100);

              var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd');
              $scope.dt = {
                  fvex_vechile_expense_date: dateyear,
                  fvex_dd_cheque_due_date: dateyear,
              }

              setTimeout(function () {
                  $("#cmb_vehicle_code,#cmb_bank_code").select2();
              }, 100);

              $http.get(ENV.apiUrl + "api/StudentReport/getVehicleName").then(function (res) {
                
                  $scope.vehicle_data = res.data;
              });

              $http.get(ENV.apiUrl + "api/StudentReport/getBankName").then(function (res1) {
                  $scope.bank_data = res1.data;
              });

              
              $scope.get_ischecked = function (str) {
                
                  if (str == "CH") {
                      $scope.hide_check_detais = true;
                      $scope.hide_cc_detais = false;
                  }
                  else if (str == "CC") {
                      $scope.hide_check_detais = false;
                      $scope.hide_cc_detais = true;
                  }
                  else if (str == "DD") {
                      $scope.hide_check_detais = false;
                      $scope.hide_cc_detais = false;
                  }
                  else if (str == "CA") {
                      $scope.hide_check_detais = false;
                      $scope.hide_cc_detais = false;
                  }

                  var t = document.getElementsByName('Recepient');
                  for (var j = 0; j < t.length; j++) {
                      if (t[j].checked === true) {
                          $scope.paymentmode = t[j].defaultValue;
                      }
                  }

              }

              $scope.size = function (str) {
                  $scope.pagesize = str;
                  $scope.currentPage = 1;
                  $scope.numPerPage = str;
                  $scope.makeTodos();
              }

              $scope.index = function (str) {
                  $scope.pageindex = str;
                  $scope.currentPage = str;
                   $scope.makeTodos();
                  main.checked = false;
                  $scope.CheckAllChecked();
              }

              $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

              $scope.makeTodos = function () {
                  var rem = parseInt($scope.totalItems % $scope.numPerPage);
                  if (rem == '0') {
                      $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                  }
                  else {
                      $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                  }

                  var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                  var end = parseInt(begin) + parseInt($scope.numPerPage);

                 

                  $scope.filteredTodos = $scope.todos.slice(begin, end);
              };

              $('*[data-datepicker="true"] input[type="text"]').datepicker({
                  todayBtn: true,
                  orientation: "top left",
                  autoclose: true,
                  todayHighlight: true,
                  format: 'yyyy-mm-dd'
              });

              $scope.showdata = function () {
                 
                  $http.get(ENV.apiUrl + "api/StudentReport/getVehicleExpanseDetails").then(function (res1) {
                      $scope.obj = res1.data;
                      $scope.totalItems = $scope.obj.length;
                      $scope.todos = $scope.obj;
                      $scope.makeTodos();
                      if ($scope.obj.length == 0) {
                          swal({ title: 'Alert', text: "No Record Found...", showCloseButton: true, width: 450, height: 200 });
                          $scope.tablehide = false;

                      }
                      else {
                          $scope.tablehide = true;
                      }
                  })
              }

              $scope.showdata();

              $scope.Show = function () {
                  $http.get(ENV.apiUrl + "api/StudentReport/getTcCertificateRequiestDate?date_wisesearch=" + $scope.dt.sims_tc_certificate_request_date).then(function (res1) {
                      $scope.obj = res1.data;
                      $scope.totalItems = $scope.obj.length;
                      $scope.todos = $scope.obj;
                      $scope.makeTodos();
                      if ($scope.obj.length == 0) {
                          swal({ title: 'Alert', text: "No Record Found...", showCloseButton: true, width: 450, height: 200 });
                          $scope.tablehide = false;

                      }
                      else {
                          $scope.tablehide = true;
                      }
                  })
              }

              $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                  $('input[type="text"]', $(this).parent()).focus();
              });

              $scope.Save = function () {
                  
                  if($scope.dt.fvex_vehicle_code==undefined || $scope.dt.fvex_vehicle_code=='')
                  {
                      swal({ title: "Alert", text: "Please Select Vehicle...", showCloseButton: true, width: 380, });
                  }

                  else if($scope.paymentmode==undefined || $scope.paymentmode=='')
                  {
                      swal({ title: "Alert", text: "Please Select Expense Mode...", showCloseButton: true, width: 380, });
                  }

                  else if($scope.dt.fvex_expense_amount==undefined || $scope.dt.fvex_expense_amount=='')
                  {
                      swal({ title: "Alert", text: "Please Enter Amount...", showCloseButton: true, width: 380, });
                  }

                  else
                  {
                      var user = $rootScope.globals.currentUser.username;
                      var Savedata = [];

                      var obj = {
                          'fvex_vechile_expense_date': $scope.dt.fvex_vechile_expense_date,
                          'fvex_vehicle_code': $scope.dt.fvex_vehicle_code,
                          'fvex_expense_type': $scope.paymentmode,
                          'fvex_expense_amount': $scope.dt.fvex_expense_amount,
                          'fvex_expense_remark': $scope.dt.fvex_expense_remark,
                          'fvex_expense_creation_user': user,
                          'fvex_dd_cheque_number': $scope.dt.fvex_dd_cheque_number,
                          'fvex_dd_cheque_due_date': $scope.dt.fvex_dd_cheque_due_date,
                          'fvex_dd_cheque_bank_code': $scope.dt.fvex_dd_cheque_bank_code,
                          'fvex_cc_number': $scope.dt.fvex_cc_number,
                          opr: 'I'

                      };
                  }

                  Savedata.push(obj);

                  $http.post(ENV.apiUrl + "api/StudentReport/CUDVehicleExpense", Savedata).then(function (msg) {

                      $scope.msg1 = msg.data;
                      if ($scope.msg1.strMessage != undefined) {
                          if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                              swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                              $scope.clear();
                          }
                      }
                      else {
                          swal({ title: "Alert", text: "Record Not Inserted..." , showCloseButton: true, width: 380, });
                      }
                      $scope.showdata();
                  });

              }

              $scope.searched = function (valLists, toSearch) {
                  return _.filter(valLists,
                  function (i) {
                      return searchUtil(i, toSearch);
                  });
              };

              $scope.search = function () {
                  $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                  $scope.totalItems = $scope.todos.length;
                  $scope.currentPage = '1';
                  if ($scope.searchText == '') {
                      $scope.todos = $scope.obj;
                  }
                  $scope.makeTodos();
              }

              function searchUtil(item, toSearch) {
                  return (item.sims_tc_certificate_request_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sager == toSearch) ? true : false;
              }

              $scope.clear = function () {
                  $scope.dt = {
                      
                      fvex_vehicle_code:'',
                      fvex_expense_amount:'',
                      fvex_expense_remark:'',
                      
                      fvex_dd_cheque_number:'',
                      
                      fvex_dd_cheque_bank_code:'',
                      fvex_cc_number:''
                  }
                  $scope.searchText = '';
              }

              $scope.Reset = function () {

                  $scope.clear();
                  $scope.tablehide = false;
              }

          }]);
})();

