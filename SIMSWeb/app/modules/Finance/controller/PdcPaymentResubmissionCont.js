﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var curriculum_code, ac_year, grade, admino;
    var grandtotal = 0;
    var finanacecode = [];
    //var companyCode = "1";
    var bankcode;
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('PdcPaymentResubmissionCont',
        ['$scope', '$state', '$rootScope', '$filter', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $filter, $timeout, gettextCatalog, $http, ENV) {

            var username = $rootScope.globals.currentUser.username;
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            var companyCode = $scope.finnDetail.company;
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
            var user = $rootScope.globals.currentUser.username.toLowerCase();

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.display = true;
            //$scope.checked = true;
            var dataforSave = [];
            $scope.table = false;
            $scope.filterVisible = false;
            
            $scope.edt1 = {
                copy1: 'S',
            }
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            //$scope.sdate = yyyy + '-' + mm + '-' + dd;
            //$scope.edate = yyyy + '-' + mm + '-' + dd;
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.temp1 = {
                //startdate: yyyy + '-' + mm + '-' + dd,
                //enddate: yyyy + '-' + mm + '-' + dd,
                startdate: dd + '-' + mm + '-' + yyyy,
                enddate: dd + '-' + mm + '-' + yyyy,
            }

            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //Select Data SHOW
            $scope.total = 0;
            $http.get(ENV.apiUrl + "api/PDCBillOtherProcess/GetDepartment?comp_code=" + companyCode).then(function (compdept) {
                $scope.Departments = compdept.data;

            });

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.temp = {
                chk_previous_clear: true,
            }

            $scope.ClearPreviousChange = function (str) {


            }

            $scope.ShowFilterText = function (str) {
                if (str == "") {
                    $scope.filterVisible = false;
                }
                else {

                    $scope.filterVisible = true;
                }
            }


            $(function () {
                $('#app_on1').multipleSelect({
                    width: '100%'
                });
            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;




            $http.get(ENV.apiUrl + "api/AdmissionFees/getBankDeatails").then(function (getBankDetails) {
                $scope.AllBankNames = getBankDetails.data;
                setTimeout(function () {
                    $('#app_on1').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);

            });

            setTimeout(function () {
                $("#cmb_acc_Code").select2();

            }, 100);


            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.myFunct = function (keyEvent) {
                
                if (keyEvent.which == 13)
                    //$scope.temp.startdate = yyyy + '-' + mm + '-' + dd;
                    $scope.temp.startdate = dd + '-' + mm + '-' + yyyy;
            }

            $scope.myFunct1 = function (keyEvent) {
                
                if (keyEvent.which == 13)
                    //$scope.temp.enddate = yyyy + '-' + mm + '-' + dd;
                    $scope.temp.enddate = dd + '-' + mm + '-' + yyyy;
            }




            //CheckBox



            //Bind All Grids

            $http.get(ENV.apiUrl + "api/BankPayment/GetLedgerNumber?financialyear=" + finance_year + "&comp_code=" + comp_code).then(function (docstatus2) {

                $scope.LdgrCode = docstatus2.data;

            });


            $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + "" + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {

                $scope.getAllAccNos = docstatus.data;

            });

            $scope.GetAllGLAcc = function (acccode, cmpnycode) {

                    $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acccode + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {
                    $scope.getAllAccNos = docstatus.data;

                });
            }

            $scope.getSLAccNo = function (slcode) {
                
                $scope.getAllAccNos = [];
                if (slcode == "00" || slcode == undefined) {
                    $scope.GetAllGLAcc($scope.temp.payable_acc_code, companyCode);
                }
                else {
                    //$http.get(ENV.apiUrl + "api/PDCBill/GetSLAccNumber?pbslcode=" + slcode).then(function (docstatus3) {
                   $http.get(ENV.apiUrl + "api/BankPayment/GetSLAccNumber?pbslcode=" + slcode + "&cmp_cd=" + comp_code + "&fyear=" + finance_year).then(function (docstatus3) {
                        $scope.getAllAccNos = docstatus3.data;

                    });
                }
                if (cmbvalue == "") {
                    $scope.edt.gldd_acct_code = "";
                    $scope.edt1.coad_dept_no = "";
                    $("#cmb_acc_Code").select2("val", cmbvalue);
                }
                else {
                    $scope.edt.gldd_acct_code = cmbvalue;
                    //$("#cmb_acc_Code").select2("val", cmbvalue);
                }
                //$("#cmb_acc_Code").val(cmbvalue);

                //$("#cmb_acc_Code").select2("val", cmbvalue);

            }






            $scope.getBankDetails = function () {
                $http.get(ENV.apiUrl + "api/AdmissionFees/getBankDeatails").then(function (getBankDetails) {
                    $scope.BankDetails = getBankDetails.data;
                    
                })
            }

            $scope.ChkDate = function (noissueafter) {
                
                //var date = new Date();
                //$scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy/MM/dd');
                //var year1 = noissueafter.split("-")[0];
                //var month1 = noissueafter.split("-")[1];
                //var day1 = noissueafter.split("-")[2];
                //var new_end_date = year1 + "-" + month1 + "-" + day1;
                var day1 = noissueafter.split("-")[0];
                var month1 = noissueafter.split("-")[1];
                var year1 = noissueafter.split("-")[2];
                var new_end_date = day1 + "-" + month1 + "-" + year1;

                if (new_end_date < $scope.temp.startdate) {
                    //$rootScope.strMessage = "Please Select Today or Next Date";
                    //$('#message').modal('show');
                    swal({ title: "Alert", text: "Please Select Next Date from start Date", showCloseButton: true, width: 380, });
                    $scope.temp.enddate = "";
                }

            }





            $scope.getGrade = function () {

                
                var dept_no, bankcode1, ldgr_code, slma_acno, from_date, to_date, filter_text, filter_range, sub_ref_no;
                $scope.flag1 = false;

                if ($scope.edt1.copy1 == 'S') {
                    if ($scope.temp1.startdate == null || $scope.temp1.startdate == "") {
                        $scope.flag1 = true;
                        swal({ title: "Alert", text: "Please Select Start Date", showCloseButton: true, width: 380, });
                    }
                    else if ($scope.temp1.enddate == null || $scope.temp1.enddate == "") {
                        $scope.flag1 = true;
                        swal({ title: "Alert", text: "Please Select End Date", showCloseButton: true, width: 380, });
                    }
                    if (!$scope.flag1) {
                        bankcode = [];
                        if ($scope.edt.slma_pty_bank_id.length == 0 || $scope.edt == undefined) {
                            dept_no = '';
                        }
                        else {
                            bankcode1 = $scope.edt.slma_pty_bank_id;
                            bankcode = bankcode + ',' + bankcode1;;
                        }

                        from_date = $scope.temp1.startdate;
                        to_date = $scope.temp1.enddate;
                        if ($scope.temp.codp_dept_no == undefined || $scope.temp == undefined) {
                            dept_no = '';
                        }
                        else {
                            dept_no = $scope.temp.codp_dept_no;
                        }
                        if ($scope.edt.sllc_ldgr_code == undefined || $scope.edt == undefined) {
                            ldgr_code = '';
                        }
                        else {
                            ldgr_code = $scope.edt.sllc_ldgr_code;
                        }
                        if ($scope.edt.gldd_acct_code == undefined || $scope.edt == undefined) {
                            slma_acno = '';
                        }
                        else {
                            slma_acno = $scope.edt.gldd_acct_code;
                        }
                        if ($scope.edt.SelectOption == undefined || $scope.edt == undefined) {
                            filter_text = '';
                        }
                        else {
                            filter_text = $scope.edt.SelectOption;
                        }
                        if ($scope.edt.txt_filter_range == undefined || $scope.edt == undefined) {
                            filter_range = '';
                        }
                        else {
                            filter_range = $scope.edt.txt_filter_range;
                        }

                        //var str2 = bankcode.substr(bankcode.indexOf(',') + 1);

                        $http.get(ENV.apiUrl + "api/PDCBillOtherProcess/getPDCReSubmissionBillsFinn214?compcode=" + companyCode + "&dept_no=" + dept_no + "&ldgr_code=" + ldgr_code + "&slma_acno=" + slma_acno + "&from_date=" + from_date + "&to_date=" + to_date + "&bank_cd=" + bankcode + "&filter_text=" + filter_text + "&filter_range=" + filter_range + "&sub_ref_no=" + "").then(function (getAllGrades) {
                            $scope.GetAllPDCBillsData = getAllGrades.data;
                            $scope.totalItems = $scope.GetAllPDCBillsData.length;
                            $scope.todos = $scope.GetAllPDCBillsData;
                            $scope.makeTodos();
                            $scope.total = 0;
                            for (var i = 0; i < $scope.GetAllPDCBillsData.length; i++) {

                                $scope.total = parseFloat($scope.total) + parseFloat($scope.GetAllPDCBillsData[i].pb_amount);

                            }
                        });
                        $scope.table = true;
                    }

                }

            }

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.AdmissionFeesData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.AdmissionFeesData;
                }
                $scope.makeTodos();
                main.checked = false;

            }

            function searchUtil(item, toSearch) {

                /* Search Text in all 3 fields */
                return (
                        item.pb_sl_acno_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.pb_amount == toSearch) ? true : false;
            }


            //DATA CANCEL
            $scope.reset = function () {

                $scope.table = false;
                $scope.display = true;
                $scope.temp = [];
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.getgrid();
            }

            $scope.cancel = function () {


                $scope.temp = {
                    sims_cur_code: $scope.curriculum_code,
                    sims_academic_year: $scope.ac_year,
                    sims_grade_code: $scope.grade,
                    admissionNo: $scope.admino,

                };
                $scope.temp.Checkno = "";
                $scope.temp.referenceno = "";
                $scope.temp.slma_pty_bank_id = "";
                $scope.temp.chequedate = "";
                $scope.temp.grandtotal = "";
                $scope.row1 = '';
                $scope.edit = {
                    copy1: 'Ca'
                }
                main = document.getElementById('mainchk');
                main.checked = false;

                $scope.CheckAllChecked();
                $scope.AdmissionGradeFeesData = [];

                // $scope.getgrid($scope.curriculum_code, $scope.ac_year, $scope.grade, $scope.admino);
            }

            //total
            $scope.total = function (str) {
                var v = document.getElementById(i);
                if (v.checked == true) {
                    $scope.grandtotal = parseInt($scope.grandtotal) + parseInt(str.sims_grade_fee_amount);
                }
                else {
                    $scope.grandtotal = parseInt($scope.grandtotal) - parseInt(str.sims_grade_fee_amount);
                }
                $scope.temp.grandtotal = $scope.grandtotal;
            }

            // Submit Fees


            $scope.ChkDate = function (noissueafter) {

                var date = new Date();
                //$scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy/MM/dd');
                //var month1 = noissueafter.split("/")[0];
                //var day1 = noissueafter.split("/")[1];
                //var year1 = noissueafter.split("/")[2];
                //var new_end_date = year1 + "/" + month1 + "/" + day1;

                $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');
                var day1 = noissueafter.split("-")[0];
                var month1 = noissueafter.split("-")[1];
                var year1 = noissueafter.split("-")[2];
                var new_end_date = day1 + "-" + month1 + "-" + year1;

                if (new_end_date < $scope.ddMMyyyy) {
                    //$rootScope.strMessage = "Please Select Today or Next Date";
                    //$('#message').modal('show');
                    swal({ title: "Alert", text: "Please Select Today or Next Date", showCloseButton: true, width: 380, });
                    $scope.temp.chequedate = "";
                }

            }


            //DATA EDIT
            $scope.edit = function (str) {

                $scope.temp = {
                    sims_student_full_name: str.sims_student_full_name,
                    sims_grade_name_en: str.sims_grade_name_en,
                    sims_admission_no: str.sims_admission_no
                };

            }

            $scope.edt =
                {
                    copy1: 'Ca'
                }


            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.GetAllPDCBillsData.length; i++) {
                    var v = document.getElementById(i);
                    if (main.checked == true) {
                        v.checked = true;
                        $scope.GetAllPDCBillsData[i].row_color = '#ffffcc'
                    }
                    else {
                        v.checked = false;
                        main.checked = false;
                        $scope.GetAllPDCBillsData[i].row_color = ''
                    }
                }

            }

            $scope.checkonebyonedelete = function (info) {
                
                if (info.checked) {
                    info.row_color = '#ffffcc'
                } else {
                    info.row_color = 'white';

                }

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;

                }


            }


            $scope.Submit = function () {
                
                var submitcode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.GetAllPDCBillsData.length; i++) {
                    //var t = $scope.GetAllPDCBillsData[i].uom_code;
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'pb_comp_code': $scope.GetAllPDCBillsData[i].pb_comp_code,
                            'pb_sl_ldgr_code': $scope.GetAllPDCBillsData[i].pb_sl_ldgr_code,
                            'pb_sl_acno': $scope.GetAllPDCBillsData[i].pb_sl_acno,
                            'pb_cheque_no': $scope.GetAllPDCBillsData[i].pb_cheque_no,
                            'pb_amount': $scope.GetAllPDCBillsData[i].pb_amount,
                            'pb_bank_from': $scope.GetAllPDCBillsData[i].pb_bank_from,
                            'pb_ldgr_code_name': user

                        });
                        submitcode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {
                    
                    swal({
                        title: '',
                        text: "Are you sure you want to Submit?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/PDCBillOtherProcess/ReSubmit_Pdc_Bilss_Finn214", submitcode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 != "" && $scope.msg1 != null) {
                                    swal({ title: "Alert", text: "Cheques Re-Submitted\n Re-Submission No:" + $scope.msg1, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getGrade();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;


                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Cheques Not Re-Submitted", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getGrade();
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                            $scope.currentPage = true;

                                        }
                                    });
                                }

                            });
                        }
                        else {

                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.GetAllPDCBillsData.length; i++) {
                                var v = document.getElementById(i);
                                //var t = $scope.GetAllPDCBillsData[i].sims_fee_category;

                                if (v.checked == true) {
                                    v.checked = false;
                                    //$scope.row1 = '';
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }


                            }

                        }


                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }

                $scope.currentPage = str;
            }



            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                //format: "yyyy-mm-dd"
                format: "dd-mm-yyyy"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.showdate = function (date, name1) {

                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];
                //$scope.temp[name1] = year + "/" + month + "/" + day;
                var day = date.split("-")[0];
                var month = date.split("-")[1];
                var year = date.split("-")[2];
                $scope.temp[name1] = day + "-" + month + "-" + month;
            }

            $scope.FilterCode = function () {

                var splitno = [];
                if ($scope.temp.chk_previous_clear == true) {
                    main = document.getElementById('mainchk');
                    for (var i = 0; i < $scope.GetAllPDCBillsData.length; i++) {

                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $scope.GetAllPDCBillsData[i].row_color = 'white';

                    }
                    if ($scope.edt.SelectOption == 'Select Records') {
                        

                        for (var i = 0; i < $scope.edt.txt_filter_range; i++) {
                            var v = document.getElementById(i);
                            v.checked = true;
                            $scope.GetAllPDCBillsData[i].row_color = '#ffffcc'
                        }



                    }

                    else if ($scope.edt.SelectOption == 'Cheque Amount between') {


                        if ($scope.edt.txt_filter_range != undefined) {
                            splitno = $scope.edt.txt_filter_range.split('-');
                        }
                        if (splitno.length >= 2) {

                            for (var i = 0; i < $scope.GetAllPDCBillsData.length; i++) {
                                if ($scope.GetAllPDCBillsData[i].pb_amount >= splitno[0] && $scope.GetAllPDCBillsData[i].pb_amount <= splitno[1]) {

                                    var v = document.getElementById(i);
                                    v.checked = true;
                                    $scope.GetAllPDCBillsData[i].row_color = '#ffffcc'
                                }
                            }
                        }
                    }
                    else if ($scope.edt.SelectOption == 'Cheque Series between') {


                        if ($scope.edt.txt_filter_range != undefined) {
                            
                            splitno = $scope.edt.txt_filter_range.split('-');
                        }
                        if (splitno.length >= 2) {
                            $scope.minchkno = 0;
                            $scope.maxchkno = 0;
                            for (var j = 0; j < splitno.length; j++) {
                                if (j == 0) {
                                    $scope.minchkno = parseInt(splitno[0]);
                                }
                                else {
                                    $scope.maxchkno = parseInt(splitno[1]);
                                }

                            }

                            for (var i = 0; i < $scope.GetAllPDCBillsData.length; i++) {
                                
                                if ($scope.GetAllPDCBillsData[i].pb_cheque_no >= $scope.minchkno && $scope.GetAllPDCBillsData[i].pb_cheque_no <= $scope.maxchkno) {

                                    var v = document.getElementById(i);
                                    v.checked = true;
                                    $scope.GetAllPDCBillsData[i].row_color = '#ffffcc'
                                }
                            }
                        }

                    }
                }

                else {

                    if ($scope.edt.SelectOption == 'Select Records') {

                        if ($scope.GetAllPDCBillsData.length >= $scope.edt.txt_filter_range) {
                            for (var i = 0; i < $scope.edt.txt_filter_range; i++) {
                                var v = document.getElementById(i);
                                v.checked = true;
                                $scope.GetAllPDCBillsData[i].row_color = '#ffffcc'

                            }
                        }
                    }

                    else if ($scope.edt.SelectOption == 'Cheque Amount between') {


                        if ($scope.edt.txt_filter_range != undefined) {
                            splitno = $scope.edt.txt_filter_range.split('-');
                        }
                        if (splitno.length >= 2) {

                            for (var i = 0; i < $scope.GetAllPDCBillsData.length; i++) {
                                if ($scope.GetAllPDCBillsData[i].pb_amount >= splitno[0] && $scope.GetAllPDCBillsData[i].pb_amount <= splitno[1]) {

                                    var v = document.getElementById(i);
                                    v.checked = true;
                                    $scope.GetAllPDCBillsData[i].row_color = '#ffffcc'
                                }
                            }
                        }
                    }
                    else if ($scope.edt.SelectOption == 'Cheque Series between') {


                        if ($scope.edt.txt_filter_range != undefined) {
                            splitno = $scope.edt.txt_filter_range.split('-');
                        }
                        if (splitno.length >= 2) {

                            $scope.minchkno = 0;
                            $scope.maxchkno = 0;
                            for (var j = 0; j < splitno.length; j++) {
                                if (j == 0) {
                                    $scope.minchkno = parseInt(splitno[0]);
                                }
                                else {
                                    $scope.maxchkno = parseInt(splitno[1]);
                                }

                            }

                            for (var i = 0; i < $scope.GetAllPDCBillsData.length; i++) {
                                
                                if ($scope.GetAllPDCBillsData[i].pb_cheque_no >= $scope.minchkno && $scope.GetAllPDCBillsData[i].pb_cheque_no <= $scope.maxchkno) {

                                    var v = document.getElementById(i);
                                    v.checked = true;
                                    $scope.GetAllPDCBillsData[i].row_color = '#ffffcc'
                                }
                            }

                            //for (var i = 0; i < $scope.GetAllPDCBillsData.length; i++) {
                            //    if ($scope.GetAllPDCBillsData[i].pb_cheque_no >= splitno[0] && $scope.GetAllPDCBillsData[i].pb_cheque_no <= splitno[1]) {

                            //        var v = document.getElementById(i);
                            //        v.checked = true;
                            //        $scope.GetAllPDCBillsData[i].row_color = '#ffffcc'
                            //    }
                            //}
                        }

                    }

                }
            }



        }])

})();
