﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var companyCode = '1';
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('FixedAssetDepreciationCalculationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = true;
            $scope.table = false;
            $scope.export_btn = false;
            $scope.busy = false;
            $scope.temp = [];
            $scope.show_btn = false;

            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            setTimeout(function () {
                $("#cmb_acc_Code3").select2();
            }, 100);

            $scope.temp['year1']=finance_year;

            $http.get(ENV.apiUrl + "api/AssetDepreciation/getDepreciationAcademicyear?comp_year=" + comp_code).then(function (year) {
                $scope.getyear = year.data;
                $scope.sims_academic_year = $scope.getyear[0].sims_academic_year;
            });
                  
            
            $scope.getItemsName = function (asset_type) {
                
                $http.get(ENV.apiUrl + "api/AssetDepreciation/getItemName?item_name=" + asset_type).then(function (docstatus) {
                    $scope.getAsset_type = docstatus.data;
                    
                    //var values = $("#cmb_acc_Code3").select2("val", cmbvalue);
                });
            }


            $http.get(ENV.apiUrl + "api/AssetDepreciation/getAssetType").then(function (ass_type) {
                $scope.asset_type_data = ass_type.data;
            });

            $http.get(ENV.apiUrl + "api/AssetDepreciation/getPeriods?fins_year=" + finance_year + "&fins_comp=" + comp_code).then(function (periodsName) {
                $scope.periods_Name = periodsName.data;

            });

            //$http.get(ENV.apiUrl + "api/AssetDepreciation/getCurrentYear").then(function (res) {
            //    
            //    $scope.currentyear = res.data;
            //    if ($scope.currentyear != undefined || $scope.currentyear != null) {
            //        if ($scope.temp == undefined) {
            //            $scope.temp = '';
            //        }
            //        $scope.temp = {
            //            year1: finance_year,
            //        }
                   
            //    }
            //    else {
            //        swal({ title: "Alert", text: "Financial Current Year Not Define", showCloseButton: true, width: 380, });
            //    }
            //});

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                 $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                 $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = 'fa fa-plus-circle';
                }
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.achieveData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.achieveData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_appl_form_field_value1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_sip_achievement_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_sip_achievement_point == toSearch) ? true : false;
            }


            $scope.companyCode = comp_code;

            $scope.calculateDep = function (year1, fins_financial_period_no, companyCode) {
                
                $scope.busy = true;

                $http.get(ENV.apiUrl + "api/AssetDepreciation/get_calculateAssetDepreciation?companyCode=" + $scope.companyCode + "&year1=" + $scope.sims_academic_year + "&period_no=" + fins_financial_period_no + "&gal_asst_type=" + $scope.temp.gal_asst_type + "&gam_item_no=" + $scope.temp.gam_item_no).then(function (msg) {
                    $scope.msg1 = msg.data;
                    
                    var v = $scope.msg1;
                    var d = v.split('-');
                    var t = d[1];
                    var n = d[0];

                    if (t == "0") {
                        swal({ title: "Alert", text: n, showCloseButton: true, width: 380, });
                        $scope.busy = false;
                        $scope.show_btn = false;
                    }
                    else {
                        $scope.export_btn = true;
                        //$scope.table = true;
                        swal({ title: "Alert", text: n, showCloseButton: true, width: 380, });
                        $scope.busy = false;
                        $scope.show_btn = true;
                    }

                    $scope.currentPage = true;

                });

            }


            $scope.Show = function () {
                $scope.export_btn = true;
                $scope.table = true;
                if ($scope.temp.gam_asst_type == '' || $scope.temp.gam_asst_type == 'undefined' || $scope.temp.gam_item_no == '' || $scope.temp.gam_item_no == 'undefined') {
                    swal({ title: "Alert", text: "Please select Asset type or Item number ", showCloseButton: true, width: 380, });
                    return;
                }
                $scope.busy = true;
                $http.get(ENV.apiUrl + "api/AssetDepreciation/getAssetsData?compnycode=" + $scope.companyCode + "&year=" + $scope.sims_academic_year + "&month=" + $scope.temp.fins_financial_period_no + "&gal_asst_type=" + $scope.temp.gal_asst_type + "&gam_item_no=" + $scope.temp.gam_item_no).then(function (assetData) {
                    
                    $scope.asset_Data = assetData.data;
                    $scope.busy = false;
                });
                $scope.currentPage = true;
            }


            $scope.immport = function () {
                var check = true;
                

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save FixedAssectDepreciation.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            
                            var blob = new Blob([document.getElementById('example_wrapper').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, "FixedAssectDepreciation" + ".xls");

                            //var exportHref = Excel.tableToExcel("fixedAsset", 'WireWorkbenchDataExport');
                            //$timeout(function () { location.href = exportHref; }, 100); // trigger download
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Report Not Save", showCloseButton: true, width: 380, })
                }

            }


            $scope.Clear = function () {
                $scope.temp = "";
                $scope.table = false;
                $scope.show_btn = false;

            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.sims_sip_achievement_type = "";
                $scope.temp.sims_sip_achievement_desc = "";
                $scope.temp.sims_sip_achievement_point = "";
                $scope.temp.sims_sip_achievement_status = "";
            }

            $scope.edit = function (str) {

                $scope.disabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;

                $scope.temp = {
                    sims_sip_achievement_type: str.sims_sip_achievement_type
                   , sims_sip_achievement_code: str.sims_sip_achievement_code
                   , sims_sip_achievement_desc: str.sims_sip_achievement_desc
                   , sims_sip_achievement_status: str.sims_sip_achievement_status
                   , sims_sip_achievement_point: str.sims_sip_achievement_point
                   , sims_appl_form_field_value1: str.sims_appl_form_field_value1

                };
            }

            var dom; $scope.flag = true;
            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                         "<tr><td class='semi-bold'>" + "Quantity" + "</td> <td class='semi-bold'>" + "Invoice Amount" + " </td><td class='semi-bold'>" + "Book Value" + "</td><td class='semi-bold'>" + "Cum Depreciation" + "</td>" +
                          "<td class='semi-bold'>" + "Yearly Depr" + "</td> <td class='semi-bold'>" + "Monthly Depr" + " </td><td class='semi-bold'>" + "Sales Value" + "</td><td class='semi-bold'>" + "Replacement Cost" + "</td>" +
                          "<td class='semi-bold'>" + "Depr Percent" + "</td> <td class='semi-bold'>" + "Used Item" + " </td><td class='semi-bold'>" + "Status" + "</td><td class='semi-bold'>" + "Status Name" + "</td>" +
                        "</tr>" +

                          "<tr><td>" + (info.gam_quantity) + "</td> <td>" + (info.gam_invoice_amount) + " </td><td>" + (info.gam_book_value) + "</td><td>" + (info.gam_cum_deprn) + "</td>" +
                           "<td>" + (info.gam_ytd_deprn) + "</td> <td>" + (info.gam_mth_deprn) + " </td><td>" + (info.gam_sale_value) + "</td><td>" + (info.gam_repl_cost) + "</td>" +
                             "<td>" + (info.gam_deprn_percent) + "</td> <td>" + (info.gam_used_on_item) + " </td><td>" + (info.gam_status) + "</td><td>" + (info.gam_status_name) + "</td>" +
                        "</tr>" +

                        " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom); $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }

            };

        }])

})();
