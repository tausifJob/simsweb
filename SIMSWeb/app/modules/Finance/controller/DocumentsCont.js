﻿
(function () {
    'use strict';

    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('DocumentsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.grid = true;
            $scope.btn_save = true;
            var main = '';
            $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetFinn_gl_master_details_Finn014").then(function (GetFinn_gl_master_details_Finn014) {
                $scope.obj = GetFinn_gl_master_details_Finn014.data;
                $scope.totalItems = $scope.obj.length;
                $scope.todos = $scope.obj;
                $scope.makeTodos();


            });

            $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetAll_ledgercode").then(function (GetAll_ledgercode) {
                $scope.GetAll_ledgercode = GetAll_ledgercode.data;
            });

            $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetAll_glmaster_Department_Code").then(function (GetAll_glmaster_Department_Code) {
                $scope.GetAll_glmaster_Department_Code = GetAll_glmaster_Department_Code.data;
            });

            $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetAll_partyclass_Code").then(function (GetAll_partyclass_Code) {
                $scope.GetAll_partyclass_Code = GetAll_partyclass_Code.data;
            });

            $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetAll_Accountcode").then(function (GetAll_Accountcode) {
                $scope.GetAll_Accountcode = GetAll_Accountcode.data;
            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;  $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();
            }

            $scope.Reset = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
            }

            $scope.btn_cancel_Click = function () {
                $scope.grid = true;
                $scope.display = false;

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            var data = [];

            $scope.btn_Save_Click = function (isvalid) {

                if (isvalid) {
                    $scope.save = true;

                    for (var i = 0; i < $scope.GetAll_Accountcode.length; i++) {
                        if ($scope.GetAll_Accountcode[i].glma_acct_code == $scope.edt.glma_acct_code) {
                            $scope.edt.glma_acct_name = $scope.GetAll_Accountcode[i].glma_acct_name;
                        }
                    }
                    data.push($scope.edt);
                    $http.post(ENV.apiUrl + "api/GLAccountsCreateView/InsertFinn_gl_master_Finn014", $scope.edt).then(function (msg) {
                        $scope.grid = true;
                        data = [];
                        $scope.msg1 = msg.data;

                        swal({
                            text: $scope.msg1,
                            width: 300,
                            height: 300
                        });
                        $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetFinn_gl_master_details_Finn014").then(function (res) {
                            $scope.obj = res.data;
                            $scope.totalItems = $scope.obj.length;
                            $scope.todos = $scope.obj;
                            $scope.makeTodos();
                        });
                    })
                }
            }

            var deletedata = [];

            $scope.btn_Delete_Click = function () {
                $scope.save = true;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].glac_acct_code;
                    var v = document.getElementById(t);
                    if (v.checked == true) {
                        $scope.filteredTodos[i].opr = "D";
                        deletedata.push($scope.filteredTodos[i]);
                    }
                }
                $http.post(ENV.apiUrl + "api/CreateEditAccount/CUDAccountCodeDetails", deletedata).then(function (msg) {
                    $scope.display = true;
                    deletedata = [];
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({
                            text: 'Account Number Deleted Successfully',
                            width: 300,
                            height: 300
                        });
                        $http.get(ENV.apiUrl + "api/CreateEditAccount/getAllFinsAccountCodes").then(function (res) {
                            $scope.obj = res.data;
                            $scope.totalItems = $scope.obj.length;
                            $scope.todos = $scope.obj;
                            $scope.makeTodos();
                        });
                    }
                    else if ($scope.msg1 == false) {
                        swal({
                            text: 'Account Number Not Deleted.' ,
                            width: 300,
                            height: 300
                        });
                    }
                    else ("Error-" + $scope.msg1)
                });

            }

            $scope.table_row_Click = function (str) {
                $scope.btn_save = false;
                $scope.grid = false;
                $scope.edt = str;
                }

            $scope.btn_Update_Click = function () {
                $scope.edt.opr = "U";

                $http.post(ENV.apiUrl + "api/GLAccountsCreateView/UpdateFinn_gl_master_Finn014", $scope.edt).then(function (msg) {
                    $scope.display = true;
                    data = [];
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({
                            text: 'GL Account  Updated Successfully',
                            width: 300,
                            height: 300
                        });
                        $scope.btn_save = true;
                        $scope.grid = true;
                        $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetFinn_gl_master_details_Finn014").then(function (GetFinn_gl_master_details_Finn014) {
                            $scope.obj = GetFinn_gl_master_details_Finn014.data;
                            $scope.totalItems = $scope.obj.length;
                            $scope.todos = $scope.obj;
                            $scope.makeTodos();
                        });
                    }
                    else if ($scope.msg1 == false) {
                        swal({
                            text: 'GL Account Not Updated. ',
                            width: 300,
                            height: 300
                        });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                });
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.codp_dept_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.glma_acct_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.btn_New_Click = function () {
                $scope.grid = false;
                $scope.btn_save = true;
                $scope.edt = "";
            }

        }])
})();