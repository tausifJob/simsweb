﻿
(function () {
    'use strict';

    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('GLAccountsCreateViewCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = 'All';
            $scope.pageindex = "0";
            $scope.pager = false;
            $scope.grid = true;
            $scope.btn_save = true;
            $scope.opbalance = true;
           
           
            var main = '';
            $scope.edt = [];
            setTimeout(function () {
                $("#account_codes5").select2();
            }, 1000)


            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
             


            debugger;
            $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetAll_Accountcode?co_code=" + comp_code + "&year=" + finance_year).then(function (GetAll_Accountcode) {
                $scope.GetAll_Accountcode = GetAll_Accountcode.data;
            });






            $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetAll_glmaster_Department_Code?co_code=" + comp_code + "&year=" + finance_year).then(function (GetAll_glmaster_Department_Code) {
                $scope.GetAll_glmaster_Department_Code = GetAll_glmaster_Department_Code.data;
            });



            $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetFinn_gl_master_details_Finn014?co_code=" + comp_code + "&year=" + finance_year).then(function (GetFinn_gl_master_details_Finn014) {
                debugger;
                $scope.obj = GetFinn_gl_master_details_Finn014.data;
                $scope.totalItems = $scope.obj.length;
                $scope.todos = $scope.obj;
                $scope.makeTodos();

               
            });
           
            $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetAll_ledgercode?co_code=" + comp_code + "&year=" + finance_year).then(function (GetAll_ledgercode) {
                $scope.GetAll_ledgercode = GetAll_ledgercode.data;
                $scope.edt['glma_ldgr_code'] = $scope.GetAll_ledgercode[0].glma_ldgr_code;
                
            });

           

            $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetAll_partyclass_Code?co_code=" + comp_code + "&year=" + finance_year).then(function (GetAll_partyclass_Code) {
                $scope.GetAll_partyclass_Code = GetAll_partyclass_Code.data;
                
                $scope.edt['glma_pty_class'] = $scope.GetAll_partyclass_Code[0].glma_pty_class;
            });


            //$scope.dept_get = function (det) {

            //    $http.get(ENV.apiUrl + "api/BankMaster/getAllAccNumber?code=" + comp_code + "&year=" + finance_year + "&dept=" + det).then(function (GetAll_Accountcode) {
            //        $scope.GetAll_Accountcode = GetAll_Accountcode.data;
            //    });
            //}


          
            
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 1000, $scope.maxSize = 1000;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);


                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.obj;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
            }

            $scope.Reset = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
            }

            $scope.btn_cancel_Click = function () {
                $scope.grid = true;
                $scope.display = false;

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            var data = [];

            $scope.btn_Save_Click = function (isvalid) {
                debugger;
                if (isvalid) {
                    $scope.save = true;
                   
                    for (var i = 0; i < $scope.GetAll_Accountcode.length; i++)
                    {
                        if ($scope.GetAll_Accountcode[i].glma_acct_code == $scope.edt.glma_acct_code) {
                            $scope.edt.glma_acct_name = $scope.GetAll_Accountcode[i].glma_acct_name.split('-')[2];
                        }
                    }

                    $scope.edt['comp_code'] = comp_code;
                    $scope.edt['finance_year'] = finance_year;
                    $scope.edt['user_name'] = user;
                    data.push($scope.edt);
                    $http.post(ENV.apiUrl + "api/GLAccountsCreateView/InsertFinn_gl_master_Finn014", $scope.edt).then(function (msg) {
                        $scope.grid = true;
                        data = [];
                        $scope.msg1 = msg.data;
                       
                        swal('', $scope.msg1);
                        $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetFinn_gl_master_details_Finn014?co_code=" + comp_code + "&year=" + finance_year).then(function (GetFinn_gl_master_details_Finn014) {
                                $scope.obj = GetFinn_gl_master_details_Finn014.data;
                                $scope.totalItems = $scope.obj.length;
                                $scope.todos = $scope.obj;
                                $scope.makeTodos();


                            });


                        })
                }
            }

            var deletedata = [];

            $scope.btn_Delete_Click = function () {
                $scope.save = true;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].glac_acct_code;
                    var v = document.getElementById(t);
                    if (v.checked == true) {
                        $scope.filteredTodos[i].opr = "D";
                        deletedata.push($scope.filteredTodos[i]);
                    }
                }
                $http.post(ENV.apiUrl + "api/CreateEditAccount/CUDAccountCodeDetails", deletedata).then(function (msg) {
                    $scope.display = true;
                    deletedata = [];
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal('','Account Number Deleted Successfully');
                        $http.get(ENV.apiUrl + "api/CreateEditAccount/getAllFinsAccountCodes").then(function (res) {
                            $scope.obj = res.data;
                            $scope.totalItems = $scope.obj.length;
                            $scope.todos = $scope.obj;
                            $scope.makeTodos();
                        });
                    }
                    else if ($scope.msg1 == false) {
                        swal('','Account Number Not Deleted');
                    }
                    else {
                        swal("Error-"+$scope.msg1)
                    }
                });

            }

            $scope.table_row_Click = function (str) {
                console.log('edit');
                console.log(str);
                $scope.btn_save = false;
                $scope.grid = false;
                

                $scope.edt = {
                    glma_dept_no:str.glma_dept_no,
                    glma_acct_code:str.glma_acct_code,
                    glma_pty_class:str.glma_pty_class,
                    glma_ldgr_code:str.glma_ldgr_name,
                    glma_op_bal_amt: str.glma_op_bal_amt,
                    glpr_bud_amt: str.glpr_bud_amt,
                    account_budget_quntity: str.account_budget_quntity,
                    glma_qty_flag:str.glma_qty_flag,
                    glma_status:str.glma_status,
                    glma_fc_flag: str.glma_fc_flag,
                    glma_stmt_reqd: str.glma_stmt_reqd
                }

                $("#account_codes5").select2("val", str.glma_acct_name);
                $scope.edt.glma_acct_name = str.glma_acct_name;
                $scope.acctcodeinput = false;
                document.getElementById('cmb_glma_dept_no').disabled = true;
                document.getElementById('accountcodeinput').disabled = true;
                
                
                /*setTimeout(function () {
                    $("#account_codes5").select2("val", str.glma_acct_code);
                }, 500000)*/
                
                //document.getElementById('account_codes5').disabled = true;

              //  $scope.edt = str;
            }

            $scope.btn_Update_Click = function () {
               
                $scope.edt['comp_code'] = comp_code;
                $scope.edt['finance_year'] = finance_year;
                $scope.edt.opr = "U";

                $http.post(ENV.apiUrl + "api/GLAccountsCreateView/UpdateFinn_gl_master_Finn014", $scope.edt).then(function (msg) {
                    $scope.display = true;
                    data = [];
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal('', 'GL Account  Updated Successfully');
                        $scope.btn_save = true;
                        $scope.grid = true;
                        $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetFinn_gl_master_details_Finn014?co_code=" + comp_code + "&year=" + finance_year + "&user_name" + user).then(function (GetFinn_gl_master_details_Finn014) {
                            $scope.obj = GetFinn_gl_master_details_Finn014.data;
                            $scope.totalItems = $scope.obj.length;
                            $scope.todos = $scope.obj;
                            $scope.makeTodos();
                        });
                    }
                    else if ($scope.msg1 == false) {
                        swal('', 'GL Account Not Updated');
                    }
                    else {
                        swal("Error-"+ $scope.msg1 )
                    }
                });
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.codp_dept_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.glma_acct_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }




            $scope.btn_New_Click = function () {
                $scope.grid = false;
                $scope.btn_save = true;
                document.getElementById('cmb_glma_dept_no').disabled = false;
                $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetAll_glmaster_Department_Code?co_code=" + comp_code + "&year=" + finance_year).then(function (GetAll_glmaster_Department_Code) {
                    $scope.GetAll_glmaster_Department_Code = GetAll_glmaster_Department_Code.data;
                });
                
                $scope.edt = "";
                $scope.edt = {
                    glma_op_bal_amt: '0',
                    glpr_bud_amt: '0',
                    account_budget_quntity: '0'
                   
                };

                $scope.acctcodeinput = true;
                $scope.edt.glma_status = true;
                $scope.edt.glma_stmt_reqd = true;
                $scope.opbalance = true;
               
                $scope.edt['glma_ldgr_code'] = $scope.GetAll_ledgercode[0].glma_ldgr_code;
                $scope.edt['glma_pty_class'] = $scope.GetAll_partyclass_Code[0].glma_pty_class;

            }

        }])
})();