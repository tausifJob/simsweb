﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('FinancialYearCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.save_btn = true;

            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;

            //Fill ComboBox Status
            $http.get(ENV.apiUrl + "api/Financialyear/getDocumentStatus").then(function (docstatus) {
                $scope.docstatus_Data = docstatus.data;
                $scope.temp['sims_financial_year_status'] = $scope.docstatus_Data[0].sims_financial_year_status
                 });


            //Fill ComboYEAR
            $http.get(ENV.apiUrl + "api/Financialyear/getDocumentDDLYear").then(function (getDocumentDDLYear_Data) {

                $scope.docyear_Data1 = getDocumentDDLYear_Data.data;
                $scope.temp['sims_financial_year'] = $scope.docyear_Data1[6].sims_academic_year;
                });


            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/Financialyear/getAllFinancialYear").then(function (res1) {
                $scope.fin_year = res1.data;
                $scope.totalItems = $scope.fin_year.length;
                $scope.todos = $scope.fin_year;
                $scope.makeTodos();

            });

            //$http.get(ENV.apiUrl + "api/GoalTarget/getAcademicYear").then(function (Acyear) {
            //    $scope.Acc_year = Acyear.data;
            //    });
            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                 $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.fin_year, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.fin_year;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_financial_year.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_financial_year_status1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.sims_doc_srno == toSearch) ? true : false;
            }

            $scope.showdate = function (date, name1) {

                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];

                //$scope.temp[name1] = year + "/" + month + "/" + day;
                $scope.temp[name1] = date;
            }


            //NEW BUTTON
            $scope.New = function () {
                $scope.disabled = false;
                $scope.gtcreadonly = true;
                $scope.gtdreadonly = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.cmbstatus = true;
                $scope.txtyear = false;
                $scope.temp = "";
                $scope.temp = {};
              

                $scope.temp = {
                    sims_financial_year_start_date: dd + '-' + mm + '-' + yyyy,
                    sims_financial_year_end_date: dd + '-' + mm + '-' + yyyy
                }

                $scope.temp['sims_financial_year'] = $scope.docyear_Data1[6].sims_academic_year;
                $scope.temp['sims_financial_year_status'] = $scope.docstatus_Data[0].sims_financial_year_status
            }


            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                if (Myform) {
                    
                    var data = $scope.temp;
                    data.opr = 'I';
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/Financialyear/CUDFinantialYear", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ title: "Alert", text: "Year Already Present. " , width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        $http.get(ENV.apiUrl + "api/Financialyear/getAllFinancialYear").then(function (res1) {
                            $scope.fin_year = res1.data;
                            $scope.totalItems = $scope.fin_year.length;
                            $scope.todos = $scope.fin_year;
                            $scope.makeTodos();
                            $scope.table = true;
                            $scope.display = false;
                        });

                    });
                    datasend = [];
                }
            }

            //Date FORMAT
            $scope.showdate = function (date, name1) {

                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];

                //$scope.temp[name1] = year + "-" + month + "-" + day;
                $scope.temp[name1] = date;
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {
                $scope.gtcreadonly = true;
                $scope.gtdreadonly = true;
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.txtyear = true;
                $scope.cmbstatus = false;
                $scope.start_date = true;
                $scope.End_date = true;
                $scope.temp = str;
            }


            //DATA UPADATE
            var dataupdate = [];
            $scope.update = function () {

                var data = $scope.temp;
                data.opr = "U";
                dataupdate.push(data);
                $http.post(ENV.apiUrl + "api/Financialyear/CUDFinantialYear", dataupdate).then(function (msg) {

                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ title: "Alert", text: "Record Not Updated. " , width: 300, height: 200 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }

                    $http.get(ENV.apiUrl + "api/Financialyear/getAllFinancialYear").then(function (res1) {
                        $scope.fin_doc = res1.data;
                        $scope.totalItems = $scope.fin_doc.length;
                        $scope.todos = $scope.fin_doc;
                        $scope.makeTodos();
                        $scope.table = true;
                        $scope.display = false;
                    });

                });
                dataupdate = [];

            }


            //DELETE RECORD
            $scope.CheckAllChecked = function () {
                
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }





            $scope.OkDelete = function () {
                
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_financial_year': $scope.filteredTodos[i].sims_financial_year,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/Financialyear/CUDFinantialYear", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Financialyear/getAllFinancialYear").then(function (res1) {
                                                $scope.fin_year = res1.data;
                                                $scope.totalItems = $scope.fin_year.length;
                                                $scope.todos = $scope.fin_year;
                                                $scope.makeTodos();
                                                $scope.table = true;
                                                $scope.display = false;
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ title: "Alert", text: "Record Not Deleted. " , showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/Financialyear/getAllFinancialYear").then(function (res1) {
                                                $scope.fin_year = res1.data;
                                                $scope.totalItems = $scope.fin_year.length;
                                                $scope.todos = $scope.fin_year;
                                                $scope.makeTodos();
                                                $scope.table = true;
                                                $scope.display = false;
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }

           



            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });


            //Selection Procedure
            $scope.Getdates = function (str) {
                for (var i = 0; i < $scope.docyear_Data1.length; i++) {
                    if ($scope.docyear_Data1[i].sims_academic_year == str) {
                        $scope.temp = {
                            sims_financial_year_start_date: $scope.docyear_Data1[i].sims_financial_year_start_date,
                            sims_financial_year_end_date: $scope.docyear_Data1[i].sims_financial_year_end_date,
                            sims_financial_year: $scope.docyear_Data1[i].sims_academic_year,


                        };
                    }
                }
            }

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;

            $scope.temp = {
                sims_financial_year_start_date: dd + '-' + mm + '-' + yyyy,
                sims_financial_year_end_date: dd + '-' + mm + '-' + yyyy
            }

        }])

})();
