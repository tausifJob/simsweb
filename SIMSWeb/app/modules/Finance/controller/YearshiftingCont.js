﻿(function () {
    'use strict';
    var obj1, temp, opr, comp, doc_cd_final_doc_no;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('YearshiftingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.pagesize = '10';

            $scope.inventory_table_show = false;
            $scope.fee_data_show = false;
            $scope.result_msg ='';
            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            console.log($scope.finnDetail)
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            $scope.size = function (str) {

                if (str == "All") {

                    $scope.filteredTodos = $scope.fee_data_details;
                    $scope.todos = $scope.fee_data_details;
                    $scope.totalItems = $scope.fee_data_details.length;
                    $scope.numPerPage = $scope.fee_data_details.length;
                    $scope.makeTodos();
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            var user = $rootScope.globals.currentUser.username;

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                $scope.SelectMultiple($scope.edt.main_status = false);

                $timeout(function () {
                    $("#fixTable1").tableHeadFixer({ "left": 4 });
                    $("#fixTable").tableHeadFixer({ 'top': 1 });

                }, 100);
            };

            $http.get(ENV.apiUrl + "api/yearshit/Get_Button_details").then(function (Get_Button_details) {
                $scope.Button_details = Get_Button_details.data;
            });

            $http.get(ENV.apiUrl + "api/common/FeePosting/GetAllCurName").then(function (res) {
                $scope.cur_data = res.data;
            });

            $scope.getacadyr = function (cur_code) {
                $http.get(ENV.apiUrl + "api/common/FeePosting/GetAllAcademicYear?cur_code=" + cur_code).then(function (res) {
                    $scope.acadyr_data = res.data;
                });
            }

            $scope.getDetails = function (info) {
                debugger;
                var sp_name = info.comn_rollover_proc_name_opr.split('/')[0];
                var opr = info.comn_rollover_proc_name_opr.split('/')[1];

                $scope.comn_rollover_code = info.comn_rollover_code;

                if (info.comn_rollover_code == '01') {
                    $scope.inventory_table_show = true;
                    $scope.fee_data_show = false;
                    $scope.finance_table_show = false;
                    $scope.academicyear_data_show = false;

                    $http.get(ENV.apiUrl + "api/yearshit/Get_details?opr=" + opr + "&sp_name=" + sp_name + "&comn_rollover_code=" + info.comn_rollover_code).then(function (details) {
                        $scope.all_details = details.data;
                    });

                    $http.get(ENV.apiUrl + "api/PDCSubmission/GetComp_Name?username=" + $rootScope.globals.currentUser.username).then(function (GetComp_Name) {
                        $scope.cmbCompany = GetComp_Name.data;
                        $scope.company_change($scope.cmbCompany[0].comp_code);
                    });
                }
                else if (info.comn_rollover_code == '02') {
                    $scope.inventory_table_show = false;
                    $scope.fee_data_show = true;
                    $scope.finance_table_show = false;
                    $scope.academicyear_data_show = false;
                }
                else if (info.comn_rollover_code == '03') {
                    $scope.inventory_table_show = false;
                    $scope.fee_data_show = false;
                    $scope.finance_table_show = true;
                    $scope.academicyear_data_show = false;

                    $http.get(ENV.apiUrl + "api/PDCSubmission/GetComp_Name?username=" + $rootScope.globals.currentUser.username).then(function (GetComp_Name) {
                        $scope.cmbCompany = GetComp_Name.data;
                        $scope.company_change($scope.cmbCompany[0].comp_code);
                    });

                } else if (info.comn_rollover_code == '04') {
                    $scope.inventory_table_show = false;
                    $scope.fee_data_show = false;
                    $scope.finance_table_show = false;
                    $scope.academicyear_data_show = true;
                }
                else {
                    $http.get(ENV.apiUrl + "api/yearshit/Get_year_processing_sp_execution?opr=" + opr + "&sp_name=" + sp_name + "&comn_rollover_code=" + info.comn_rollover_code + "&comp_code=" + comp_code + "&financial_year=" + finance_year + "&user_name=" + user).then(function (details) {
                        $scope.result_msg = details.data;

                        swal({ title: "Alert", text: $scope.result_msg, width: 300, height: 200 });
                    });

                }
            }

            $scope.getFeeDatadetails = function () {

                $scope.fee_table_show = false;
                $scope.busy_indi = true;
                $http.get(ENV.apiUrl + "api/yearshit/Get_fee_data_details?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&Default=" + $scope.edt.Defaulter).then(function (Get_fee_data_details) {
                    $scope.fee_data_details = Get_fee_data_details.data;
                    if ($scope.fee_data_details.length > 0) {
                        $scope.busy_indi = false;

                        $scope.fee_table_show = true;
                        $scope.totalItems = $scope.fee_data_details.length;
                        $scope.todos = $scope.fee_data_details;
                        $scope.fee_summury = $scope.fee_data_details[0].fee_det;
                        $scope.makeTodos();
                    } else {
                        $scope.busy_indi = false;
                        $scope.fee_table_show = false;
                        swal({ text: 'Data Not Found', width: 250, height: 150, showCloseButton: true });
                    }
                });
            }

            $scope.company_change = function (str) {
                $http.get(ENV.apiUrl + "api/PDCSubmission/GetFinancial_year?comp_code=" + $scope.fin.comp_code).then(function (GetFinancial_year) {
                    $scope.cmbYear = GetFinancial_year.data;

                });
            }

            $scope.Academic_Year_Data_Shifting = function () {
                $http.get(ENV.apiUrl + "api/yearshit/Get_Academic_Year_Data_Shifting?sims_academic_year=" + $scope.edt.sims_academic_year + "&comn_rollover_code=" + $scope.comn_rollover_code).then(function (Year_Data_Shifting) {
                    $scope.Year_Data_Shifting = Year_Data_Shifting.data;

                    if ($scope.Year_Data_Shifting.lenth > 0) {
                        $('#MyModal').modal('show')
                    }

                });
            }

            $scope.Finance_Data_Shifting = function () {
                $http.get(ENV.apiUrl + "api/yearshit/Get_Finance_Data_Shifting?comp_code=" + $scope.fin.comp_code + "&fin_year=" + $scope.fin.financial_year + "&comn_rollover_code=" + $scope.comn_rollover_code).then(function (Financial_year) {
                    $scope.Financial_year = Financial_year.data;

                    if ($scope.Financial_year.lenth > 0) {
                        $('#MyModal').modal('show')
                    }
                });
            }

            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.SelectMultiple = function (str) {
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if (str == true) {
                        $scope.filteredTodos[i].student_status = true;
                    } else {
                        $scope.filteredTodos[i].student_status = false;
                    }
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.fee_data_details, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.fee_data_details;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                return (item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.Save_Fee_Data_Btn_Click = function () {
                $scope.DatasendObject = [];

                var data = {};
                data.sims_academic_year = $scope.edt.sims_academic_year;
                data.sims_cur_code = $scope.edt.sims_cur_code;
                data.comn_rollover_process_remark = $scope.edt.comn_rollover_process_remark;
                data.user_name = user;
                data.comn_rollover_code = $scope.comn_rollover_code;
                var fee_code1 = '';

                for (var j = 0; j < $scope.filteredTodos[0].fee_det.length; j++) {
                    if ($scope.filteredTodos[0].fee_det[j].fee_amount > 0) {
                        fee_code1 = fee_code1 + $scope.filteredTodos[0].fee_det[j].posting_fee_code + ',';
                    }
                }
                data.sims_fee_code = fee_code1;
                $scope.DatasendObject = data;

                if ($scope.DatasendObject.sims_fee_code != undefined && $scope.DatasendObject.sims_fee_code != '') {
                    $http.post(ENV.apiUrl + "api/yearshit/Fee_posting_data", $scope.DatasendObject).then(function (Financial_year) {
                        $scope.Financial_year = Financial_year.data;

                        if ($scope.Financial_year.lenth > 0) {
                            $scope.filteredTodos = [];
                            $scope.fee_data_details = [];
                            $('#MyModal').modal('show');
                        }
                         
                    });
                } else {
                    swal({ text: 'Fee Number Not Found To Posting Fee', width: 300, height: 250, showCloseButton: true });
                }
            }

            $scope.Inventory_Data_Shifting = function () {
                $http.get(ENV.apiUrl + "api/yearshit/Get_inventory_Data_Shifting?comp_code=" + $scope.fin.comp_code + "&fin_year=" + $scope.fin.financial_year + "&comn_rollover_code=" + $scope.comn_rollover_code).then(function (Financial_year) {
                    $scope.Financial_year = Financial_year.data;

                    if ($scope.Financial_year.lenth > 0) {
                        $('#MyModal').modal('show')
                    }
                });
            }

        }])
})();