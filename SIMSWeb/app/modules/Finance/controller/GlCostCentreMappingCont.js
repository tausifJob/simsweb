﻿(function () {
    'use strict';
    var obj1, obj2, temp, main;
    var opr = '';
    var categorycode = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('GlCostCentreMappingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.GlCostCentreDetail = true;
            $scope.editmode = false;
            $scope.pager = true;
            $scope.edt = "";
            var data1 = [];
            $scope.items = [];
           
            setTimeout(function () {
                $("#account_codes2").select2();
            }, 100);

            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
            var codp_dept_no_old;

            var glac_acct_code_old;

            //$(function () {
            //    $('#cmb_center_code').multipleSelect({
            //        width: '100%'
            //    });
            //});

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $scope.size = function (str) {
               
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.gl_cost_centre_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                 $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.gl_cost_centre_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.gl_cost_centre_data;
                }

                $scope.makeTodos();
                $scope.check_all();
                main.checked = false;
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                        item.coce_cost_centre_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.glma_acct_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.codp_dept_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.glco_comp_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.glco_year.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.glco_year == toSearch) ? true : false;

            }

            $scope.Reset = function () {
                $scope.bindcentercode();
                $scope.edt.codp_dept_no = '';
                $scope.edt.glac_acct_code = '';
                //$scope.edt.coce_cost_centre_code = "";
            }

            $scope.cancel = function () {
                $scope.bindcentercode();
                $scope.GlCostCentreDetail = true;
                $scope.GlCostCentreOperation = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.newmode = true;
                $scope.editmode = false;
                opr = 'S';
                $scope.readonly = false;
                $scope.GlCostCentreDetail = false;
                $scope.GlCostCentreOperation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = {};
                $scope.edt.glco_status = true;
                $scope.bindcentercode();
                $scope.item1 = '';
                $scope.items = [];
                $scope.add();
            }

            $scope.add = function () {
                
                $scope.items = [];
                for (var j = 0; j < $scope.centrecode.length; j++) {
                    $scope.items.push({
                        val: $scope.centrecode.coce_cost_centre_code
                    });

                }


            }

            $http.get(ENV.apiUrl + "api/GlCostCentre/getGlCostCentreDetail?comp_code=" + comp_code + "&year=" + finance_year).then(function (gl_cost_centre) {
                $scope.gl_cost_centre_data = gl_cost_centre.data;
                $scope.totalItems = $scope.gl_cost_centre_data.length;
                $scope.todos = $scope.gl_cost_centre_data;
                $scope.makeTodos();
            });


            $http.get(ENV.apiUrl + "api/GlCostCentre/getDepartmentDetail?comp_code=" + comp_code + "&year=" + finance_year).then(function (cost_centre_department) {

                $scope.department = cost_centre_department.data;

                //$("#deptname").kendoAutoComplete({
                //    dataTextField: "codp_dept_name", // The widget is bound to the "name" field
                //    //dataValueField: "gldd_acct_code",
                //    dataSource: cost_centre_department.data,
                //    filter: "contains",
                //    select: function (e) {
                //        var dataItem = this.dataItem(e.item.index());
                //        $scope.codp_dept_no = dataItem.codp_dept_no;
                //        $scope.getAccountdetails(dataItem.codp_dept_no)
                //    },

                //    //select: function (ev) {
                //    //    debugger
                //    //    var dataItem = this.dataItem(ev.item.index());
                //    //    alert(dataItem.codp_dept_no);

                //    //}
                //});
            });

            $scope.getAccountdetails = function (str) {
                $http.get(ENV.apiUrl + "api/GlCostCentre/getAccountCodeDetail?dep_code=" + str + "&comp_code=" + comp_code + "&year=" + finance_year).then(function (cost_centre_account) {
                    $scope.account = cost_centre_account.data;
                   
                    if (cost_centre_account.data.length > 0) { }
                    else {
                        //$scope.ImageView = true;
                        swal({ title: "Alert", text: "Sorry There is no Account Name in this Section!", showCloseButton: true, width: 380, });
                    }
                });
            }

            $scope.bindcentercode = function () {
                $http.get(ENV.apiUrl + "api/GlCostCentre/getCentreCodeDetail?comp_code=" + comp_code + "&year=" + finance_year).then(function (cost_centre_centrecode) {
                  
                    $scope.centrecode = cost_centre_centrecode.data;
                    //$scope.txt = '';
                    for (var i = 0; i < cost_centre_centrecode.data.length; i++) {
                        $scope.chked = false;
                    }
                    //setTimeout(function () {
                    //    $('#cmb_center_code').change(function () {
                    //         }).multipleSelect({
                    //        width: '100%'
                    //    });
                    //}, 1000);
                });

            }

            $http.get(ENV.apiUrl + "api/GlCostCentre/getCentreCodeDetail?comp_code=" + comp_code + "&year=" + finance_year).then(function (cost_centre_centrecode) {
               
                $scope.centrecode = cost_centre_centrecode.data;
                //$scope.txt = '';
                for (var i = 0; i < cost_centre_centrecode.data.length; i++) {
                    $scope.chked = false;
                }
                //setTimeout(function () {
                //    $('#cmb_center_code').change(function () {
                //         }).multipleSelect({
                //        width: '100%'
                //    });
                //}, 1000);
            });

            $scope.Save = function (myForm) {
                if (myForm) {
                    var data1 = [];
                    for (var i = 0; i < $scope.centrecode.length; i++) {
                        // var M = $scope[item1].value;
                        //var n = document.getElementById("M").value;

                        if ($scope.centrecode[i].chked == true) {
                            var mn = $scope.centrecode[i].money;
                            var data = {
                                coce_cost_centre_code: $scope.centrecode[i].coce_cost_centre_code,
                                money: $scope.centrecode[i].money,
                                comp_code: comp_code,
                                finance_year: finance_year,
                                codp_dept_no: $scope.edt.codp_dept_no,
                                glac_acct_code: $scope.edt.glac_acct_code,
                                glco_status: $scope.edt.glco_status,
                            }
                            data1.push(data);
                            data.opr = 'I';
                        }
                    }
                    $scope.exist = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        if ($scope.filteredTodos[i].coce_cost_centre_code == data.coce_cost_centre_code && $scope.filteredTodos[i].codp_dept_no == data.codp_dept_no &&
                            $scope.filteredTodos[i].glac_acct_code == data.glac_acct_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ title: "Alert", text: "Record Already exists", width: 300, height: 200 });
                    }
                    else {
                        $http.post(ENV.apiUrl + "api/GlCostCentre/GlCostCentreDetailCUD?simsobj=", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            $scope.GlCostCentreOperation = false;
                            $http.get(ENV.apiUrl + "api/GlCostCentre/getGlCostCentreDetail?comp_code=" + comp_code + "&year=" + finance_year).then(function (gl_cost_centre) {
                                $scope.gl_cost_centre_data = gl_cost_centre.data;
                                $scope.totalItems = $scope.gl_cost_centre_data.length;
                                $scope.todos = $scope.gl_cost_centre_data;
                                $scope.makeTodos();
                                if ($scope.msg1 == true) {

                                    swal({ title: "Alert", text: "Record Inserted Successfully", showCloseButton: true, width: 380 });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ title: "Alert", text: "Record Already exists. " , showCloseButton: true, width: 380 });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        });
                        $scope.GlCostCentreDetail = true;
                        $scope.GlCostCentreOperation = false;
                    }
                    data1 = [];
                }
            }

            $scope.up = function (str) {
                debugger
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.GlCostCentreDetail = false;
                $scope.GlCostCentreOperation = true;
                
                $scope.edt =
                    {
                        codp_dept_no: str.codp_dept_no
                      , codp_dept_name: str.codp_dept_name
                      , glac_acct_code: str.glac_acct_code
                      , glma_acct_name: str.glma_acct_name
                      , coce_cost_centre_name: str.coce_cost_centre_name
                      , coce_cost_centre_code: str.glco_cost_centre_code
                      , glco_status: str.glco_status
                      , comp_code: comp_code
                      , finance_year: finance_year
                      , glco_op_bal_amt: str.glco_op_bal_amt
                    };

                setTimeout(function () {
                    $("#account_codes2").select2("val", str.glac_acct_code);

                },500)
                codp_dept_no_old = str.codp_dept_no;
                glac_acct_code_old = str.glac_acct_code;
                $scope.getAccountdetails(str.codp_dept_no, str.glac_acct_code);
            }

            $scope.Update = function () {
                
                data1 = [];
                // var data = $scope.edt;
                var data =
                   {
                       codp_dept_no: $scope.edt.codp_dept_no
                     , glac_acct_code: $scope.edt.glac_acct_code
                     , codp_dept_no_old1: codp_dept_no_old
                     , glac_acct_code_old1: glac_acct_code_old
                     , coce_cost_centre_code: $scope.edt.coce_cost_centre_code
                     , glco_status: $scope.edt.glco_status
                     , comp_code: comp_code
                     , finance_year: finance_year
                     , money: $scope.edt.glco_op_bal_amt
                   };
                data.opr = 'U';
                data1.push(data);
                $http.post(ENV.apiUrl + "api/GlCostCentre/GlCostCentreDetailCUD", data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Record Updated Successfully", showCloseButton: true, width: 380 });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ title: "Alert", text: "Record Not Updated. " , showCloseButton: true, width: 380 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                    $http.get(ENV.apiUrl + "api/GlCostCentre/getGlCostCentreDetail?comp_code=" + comp_code + "&year=" + finance_year).then(function (gl_cost_centre) {
                        $scope.gl_cost_centre_data = gl_cost_centre.data;
                        $scope.totalItems = $scope.gl_cost_centre_data.length;
                        $scope.todos = $scope.gl_cost_centre_data;
                        $scope.makeTodos();
                    });
                })
                $scope.GlCostCentreOperation = false;
                $scope.GlCostCentreDetail = true;
                $scope.bindcentercode();

            }

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.check_once = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }

            $scope.deleterecord = function () {
               
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'coce_cost_centre_code': $scope.filteredTodos[i].glco_cost_centre_code,
                            'glac_acct_code': $scope.filteredTodos[i].glac_acct_code,
                            'codp_dept_no': $scope.filteredTodos[i].codp_dept_no,
                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        confirmButtonText: 'Yes',
                        showCancelButton: true,
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                           
                            $http.post(ENV.apiUrl + "api/GlCostCentre/GlCostCentreDetailCUD", deleteleave).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/GlCostCentre/getGlCostCentreDetail?comp_code=" + comp_code + "&year=" + finance_year).then(function (gl_cost_centre) {
                                                $scope.gl_cost_centre_data = gl_cost_centre.data;
                                                $scope.totalItems = $scope.gl_cost_centre_data.length;
                                                $scope.todos = $scope.gl_cost_centre_data;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }

                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ title: "Alert", text: "Record Not Deleted. " + $scope.msg1, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        $http.get(ENV.apiUrl + "api/GlCostCentre/getGlCostCentreDetail?comp_code=" + comp_code + "&year=" + finance_year).then(function (gl_cost_centre) {
                                            $scope.gl_cost_centre_data = gl_cost_centre.data;
                                            $scope.totalItems = $scope.gl_cost_centre_data.length;
                                            $scope.todos = $scope.gl_cost_centre_data;
                                            $scope.makeTodos();
                                        });
                                        main = document.getElementById('all_chk');
                                        if (main.checked == true) {
                                            main.checked = false;
                                            $scope.row1 = '';
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.row1 = '';
                $scope.currentPage = str;
            }

            $scope.onlyNumbers = function (event) {
                ;
                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };


        }]);

})();