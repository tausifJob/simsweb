﻿(function () {
    'use strict';
    var main, temp;
    var data1 = [];
    var data2 = [];
    var data3 = [];
    var Bankdetails = [];
    var status = "";
    var msg_flag1 = false;
    var acconcode = "";
    var prvno = "";
    var cmbvalue = "";
    var data = [];
    var cost = [];
    var chk;
    var coad_pty_short_name = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });



    simsController.controller('PC_Disbursement_DPSCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.edt = "";
            $scope.operation = true;
            $scope.table1 = true;
            $scope.addbtn = true;
            $scope.btnDelete = false;
            $scope.users = [];
            $scope.chkcost_Ac = false;
            $scope.data = [];
            $scope.Bankdetails = "";
            $scope.Bankdetails = [];
            $scope.chkcost = false;
            $scope.cost_combo = false;
            $scope.edt5 = [];
            $scope.temp3 = {};
            
            var user = $rootScope.globals.currentUser.username.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=Single").then(function (res) {
                $scope.final_doc_url = res.data;
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;  $scope.makeTodos();
            }

            $http.get(ENV.apiUrl + "api/JVCreation/getDocCodePCDI?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                $scope.geDocCode_jv = res.data;
                if (res.data.length > 0) {
                    $scope.temp3['gltd_doc_code'] = $scope.geDocCode_jv[0].gltd_doc_code;
                }


                $http.get(ENV.apiUrl + "api/BankPayment/GetVerifyAuthorizeUsers_PC?username=" + user + "&comp_code=" + comp_code + "&fyear=" + finance_year + "&doc_code=" + $scope.temp3.gltd_doc_code).then(function (users_list) {

                    $scope.users = users_list.data;
                    if ($scope.users.length != 0) {

                        //$scope.preparebtn = true;
                        //$scope.verifybtn = true;
                        //$scope.authorizebtn = false;
                        if ($scope.users[0] == user && $scope.users[1] == user) {
                            $scope.preparebtn = true;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = false;
                        }
                        else if ($scope.users[0] == user && ($scope.users[1] == "" || $scope.users[1] != user)) {
                            $scope.preparebtn = true;
                            $scope.verifybtn = false;
                            $scope.authorizebtn = true;
                        }
                        else {
                            $scope.preparebtn = false;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                        }
                    }
                });

                $http.get(ENV.apiUrl + "api/BankPayment/GetAllPetty_CashAccount?cmp_cd=" + comp_code + "&fyear=" + finance_year + "&doc_cd=" + $scope.temp3.gltd_doc_code + "&username=" + user).then(function (docstatus2) {
                    
                    $scope.bankname = docstatus2.data;
                    if (docstatus2.data.length > 0) {
                        $scope.edt5['glpc_acct_code'] = docstatus2.data[0];
                        $scope.getAccountName(docstatus2.data[0])
                    }

                    //    $scope.edt5={

                    //        master_acno: $scope.bankname[0].master_acno,
                    //        master_ac_cdname: $scope.bankname[0].master_ac_cdname
                    //}
                    //    $scope.getAccountName($scope.edt5.master_acno);
                });


            });


          

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);


                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckPrintReport = function () {
                

                var data = {
                    location: $scope.final_doc_url,
                    parameter: {
                        comp_detail: comp_code,
                        doc_code: $scope.temp3.gltd_doc_code,
                        doc_no: $scope.Docno,

                    },

                    state: 'main.Fin157_DPS'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')


            }

            $scope.New = function () {
                $scope.operation = true;
                $scope.addbtn = true;
                $scope.updatebtn = false;
                $scope.Delete = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.cancel = function () {
                $scope.addbtn = true;
                $scope.updatebtn = false;
                $scope.chkcost = false;
                $scope.cost_combo = false;
                $scope.edt = "";
                $scope.edt1 = "";
                $("#cmb_acc_Code3").select2("val", "");
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd-mm-yyyy'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            //$scope.sdate = yyyy + '-' + mm + '-' + dd;
            //$scope.edate = yyyy + '-' + mm + '-' + dd;
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.edt4 = {
                //DocDate: yyyy + '-' + mm + '-' + dd,
                //PostDate: yyyy + '-' + mm + '-' + dd,
                //ChequeDate: yyyy + '-' + mm + '-' + dd,
                DocDate: dd + '-' + mm + '-' + yyyy,
                PostDate: dd + '-' + mm + '-' + yyyy,
                ChequeDate: dd + '-' + mm + '-' + yyyy,
            }


            $scope.getDepartcode = function (str) {
                
                $http.get(ENV.apiUrl + "api/BankPayment/getCostCenter?aacno=" + str + "&comp_cd=" + comp_code + "&fyear=" + finance_year).then(function (cost_center) {

                    $scope.costcenter = cost_center.data;
                    if ($scope.costcenter.length > 0) {
                        $scope.chkcost = true;
                    }
                    else {
                        $scope.chkcost = false;
                        $scope.temp.cost_chk = false;
                        $scope.cost_combo = false;
                    }
                });

                for (var i = 0; $scope.getAllAccNos.length; i++) {
                    if ($scope.getAllAccNos[i].gldd_acct_code == str) {

                        $scope.edt1 = {
                            coad_dept_no: $scope.getAllAccNos[i].gldd_dept_code,
                            gldd_acct_code: $scope.getAllAccNos[i].gldd_acct_code,
                            codp_dept_name: $scope.getAllAccNos[i].gldd_dept_name,
                            gldd_acct_name: $scope.getAllAccNos[i].gldd_acct_name,
                        }
                    }
                }


            }

            $scope.cost_center = function (str) {
                
                chk = str;
                if (str == true) {
                    $scope.cost_combo = true;
                }
                else {
                    $scope.cost_combo = false;
                }
            }

            $scope.cost_center_Account = function (str) {
                chk = str;
                
                if (str == true) {
                    $scope.cost_combo_AC = true;
                }
                else {
                    $scope.cost_combo_AC = false;
                }

            }



            //Fill Combo Ledger
            $http.get(ENV.apiUrl + "api/BankPayment/GetLedgerNumber?financialyear="+finance_year + "&comp_code="+comp_code).then(function (docstatus2) {
                $scope.LdgrCode = docstatus2.data;
            });




            $scope.GetAllGLAcc = function (acconcode, cmpnycode) {
                $http.get(ENV.apiUrl + "api/BankPayment/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {

                    $scope.getAllAccNos = docstatus.data;
                });
            }

            $http.get(ENV.apiUrl + "api/BankPayment/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {
                $scope.getAllAccNos = docstatus.data;
                $scope.expence_ac_list = docstatus.data;
            });

            //Fill Combo SLACCNO
            $scope.getSLAccNo = function (slcode) {

                $scope.getAllAccNos = [];
                var acconcode = $scope.getAllAccNos.gldd_acct_code;
                if (slcode == "00") {
                    $scope.GetAllGLAcc(acconcode, comp_code);
                }
                else {

                    $http.get(ENV.apiUrl + "api/BankPayment/GetSLAccNumber?pbslcode=" + slcode + "&cmp_cd=" + comp_code + "&fyear=" + finance_year).then(function (docstatus3) {
                        // $scope.sllc_ldgr_code = slcode;
                        $scope.getAllAccNos = docstatus3.data;
                        //$scope.slacno = docstatus3.data;
                    });
                }
                $("#cmb_acc_Code3").select2("val", cmbvalue);
            }

            $http.get(ENV.apiUrl + "api/BankPayment/getBankPayment").then(function (BankPayment_Data) {

                $scope.BankPaymentData = BankPayment_Data.data;
                $scope.totalItems = $scope.BankPaymentData.length;
                $scope.todos = $scope.BankPaymentData;
                $scope.makeTodos();

            });

            setTimeout(function () {
                $("#cmb_acc_Code3").select2();
                $("#rcmb_expence_ac").select2();

            }, 100);

            $scope.summ = function () {

                $scope.total = 0;

                for (var i = 0; i < $scope.Bankdetails.length; i++) {

                    $scope.total = $scope.total + parseInt($scope.Bankdetails[i].gldd_doc_amount);
                }

            }

            $scope.datagrid = function (myForm) {
                
                $scope.chkcost = false;
                $scope.cost_combo = false;
                $scope.temp.cost_chk=false;

                
                if ($scope.edt.gldd_acct_code == undefined || $scope.edt.gldd_acct_code == "") {
                    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please select Account name", showCloseButton: true, width: 380, });

                }
                else if ($scope.edt.gldd_doc_amount == undefined || $scope.edt.gldd_doc_amount == "") {
                    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please Enter Amount", showCloseButton: true, width: 380, });
                }

                if (parseFloat($scope.edt.gldd_doc_amount) > $scope.edt5.glpc_acct_code.glpc_curr_balance) {
                    swal({ title: "Alert", text: "amount exceeds current bal", showCloseButton: true, width: 380, });
                    return;
                }
                
                if ((parseFloat($scope.total) + parseFloat($scope.edt.gldd_doc_amount)) > $scope.edt5.glpc_acct_code.glpc_curr_balance) {

                    swal({ title: "Alert", text: "Total amount exceeds current bal", showCloseButton: true, width: 380, });
                    return;
                }
                else if ((parseFloat($scope.total) + parseFloat($scope.edt.gldd_doc_amount)) > $scope.edt5.glpc_acct_code.glpc_max_limit) {
                    swal({ title: "Alert", text: "Total amount exceeds max limit", showCloseButton: true, width: 380, });
                    return;
                
                }

                if (parseFloat($scope.edt.gldd_doc_amount) > $scope.edt5.glpc_acct_code.glpc_max_limit || parseFloat($scope.edt.gldd_doc_amount) > $scope.edt5.glpc_acct_code.glpc_max_limit_per_transaction) {
                    swal({ title: "Alert", text: "max limit exceeds", showCloseButton: true, width: 380, });
                    return;
                }
                else {

                    var f = false;
                    var ledcode = "";
                    if (myForm) {
                        for (var j = 0; j < $scope.Bankdetails.length; j++) {
                            if ($scope.Bankdetails[j].sllc_ldgr_code == $scope.edt.sllc_ldgr_code && $scope.Bankdetails[j].slma_acno == $scope.edt1.gldd_acct_code) {
                                f = true;
                            }
                        }
                        if (!f) {
                            if ($scope.edt.sllc_ldgr_code == undefined) {
                                ledcode = '00';
                            }
                            else {
                                ledcode = $scope.edt.sllc_ldgr_code;
                            }

                                 var costcenter1 = document.getElementById("costCenter")
                                 var costcentername = costcenter1.options[costcenter1.selectedIndex].text;
                                 var terminal = document.getElementById("cmb_acc_Code3");
                                 $scope.selectedText = terminal.options[terminal.selectedIndex].text;
                                 $scope.Bankdetails.push({
                                'gldd_comp_code': comp_code,
                                'sllc_ldgr_code': ledcode,
                                'slma_acno': $scope.edt1.gldd_acct_code,
                                'coad_pty_full_name': $scope.selectedText,
                                'coad_dept_no': $scope.edt1.coad_dept_no,
                                'gldd_party_ref_date': $scope.edt.gldd_party_ref_date,
                                'gldd_doc_narr': $scope.edt.gldd_doc_narr,
                                'gldd_doc_amount': $scope.edt.gldd_doc_amount,
                                'glco_cost_centre_code': $scope.edt.glco_cost_centre_code,
                                'coce_cost_centre_name1': costcentername,
                                'gldd_fc_amount': 0.00

                            });
                            $scope.total = 0;
                            $scope.fctotal = 0;

                            for (var i = 0; i < $scope.Bankdetails.length; i++) {

                                //$scope.total = $scope.total + parseInt($scope.Bankdetails[i].gldd_doc_amount);
                                $scope.total = parseFloat($scope.total) + parseFloat($scope.Bankdetails[i].gldd_doc_amount);
                                // $scope.fctotal = $scope.fctotal + parseInt($scope.Bankdetails[i].gldd_fc_amount);
                                $scope.fctotal = parseFloat($scope.fctotal) + parseFloat($scope.Bankdetails[i].gldd_fc_amount);
                            }
                            //$scope.btnDelete = true;


                            //$scope.fctotal = 0;

                            //for (var i = 0; i < $scope.Bankdetails.length; i++) {
                            //    $scope.fctotal = $scope.fctotal + parseInt($scope.Bankdetails[i].gldd_fc_amount);
                            //    }
                            $scope.edt = "";
                            $scope.edt1 = "";
                            $scope.edt2 = "";
                            $("#cmb_acc_Code3").select2("val", "");
                            $scope.myForm.$setPristine();
                            $scope.myForm.$setUntouched();
                            Bankdetails = [];
                        }
                        else {
                            swal({ title: "Alert", text: "This Account Name Already Present", width: 380 });
                        }
                        //$scope.selectedText = "";
                    }
                }
            };

            $scope.up = function (str) {

                $scope.Add = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                //$scope.edt = str;
                //var terminal1 = document.getElementById("cmb_acc_Code3");
                //$scope.selectedText = terminal1.options[terminal1.selectedIndex].text;

                $scope.edt =
                {
                    // slma_acno:str.edt.slma_acno,
                    ////coad_dept_no: str.coad_dept_no,
                    ////gldd_acct_code: str.gldd_acct_code,
                    ////gldd_acct_name:str.gldd_acct_name,
                    //sllc_ldgr_code: str.sllc_ldgr_code,
                    ////gldd_party_ref_date: str.gldd_party_ref_date,
                    ////gldd_doc_amount: str.gldd_doc_amount,
                    ////gldd_doc_narr: str.gldd_doc_narr,
                    // gldd_acct_name: str.gldd_acct_name,
                    // gldd_acct_code: str.slma_acno,

                    //  selectedText:select2("val", str.gldd_acct_code)
                    coad_dept_no: str.coad_dept_no,
                    gldd_acct_code: str.slma_acno,
                    gldd_acct_name: str.coad_pty_full_name,
                    sllc_ldgr_code: str.sllc_ldgr_code,
                    gldd_party_ref_date: str.gldd_party_ref_date,
                    gldd_doc_amount: str.gldd_doc_amount,
                    gldd_doc_narr: str.gldd_doc_narr,

                };
                cmbvalue = str.slma_acno;
                //  $scope.getSLAccNo(str.sllc_ldgr_code);
                //$scope.getDepartcode($scope.edt.gldd_acct_code);
                setTimeout(function () {
                    // $('#cmb_acc_Code3').val(str.slma_acno);
                    $('#cmb_acc_Code3').val(str.slma_acno)
                    $('#cmb_acc_Code3').trigger('change')
                    // $("#cmb_acc_Code3").select2("val", str.slma_acno);
                }, 1000);
                $scope.edt1 =
              {
                  coad_dept_no: str.coad_dept_no,
                  gldd_acct_code: str.slma_acno,
                  gldd_acct_name: str.gldd_acct_name
              };
            }

            $scope.totalAmountCount = function (str) {
                
                if (str > $scope.edt5.glpc_acct_code.glpc_curr_balance) {
                    swal({ title: "Alert", text: "amount exceeds current bal", showCloseButton: true, width: 380, });
                    return;
                }

                if ((parseFloat($scope.total) + parseFloat(str)) > $scope.edt5.glpc_acct_code.glpc_curr_balance) {

                    swal({ title: "Alert", text: "Total amount exceeds current bal", showCloseButton: true, width: 380, });
                    return;
                }
                else if ((parseFloat($scope.total) + parseFloat(str)) > $scope.edt5.glpc_acct_code.glpc_max_limit) {
                    swal({ title: "Alert", text: "Total amount exceeds max limit", showCloseButton: true, width: 380, });
                    return;

                }

                
                $scope.total = 0;

                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    $scope.total = parseFloat($scope.total) + parseFloat($scope.Bankdetails[i].gldd_doc_amount);
                }
            }

            $scope.Update = function () {

                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    //  $scope.data2.push($scope.Bankdetails);
                    if ($scope.Bankdetails.slma_acno == "")

                    { }
                    else {
                        var terminal = document.getElementById("cmb_acc_Code3");
                        var selectedText = terminal.options[terminal.selectedIndex].text;
                        $scope.Bankdetails.pop({
                            'gldd_comp_code': comp_code,
                            'sllc_ldgr_code': $scope.edt.sllc_ldgr_code,
                            'slma_acno': $scope.edt1.gldd_acct_code,
                            'coad_pty_full_name': selectedText,
                            'coad_dept_no': $scope.edt1.coad_dept_no,
                            'gldd_party_ref_date': $scope.edt.gldd_party_ref_date,
                            'gldd_doc_narr': $scope.edt.gldd_doc_narr,
                            'gldd_doc_amount': $scope.edt.gldd_doc_amount,
                            'gldd_fc_amount': 0.00

                        });
                        $scope.Bankdetails.push({
                            'gldd_comp_code': comp_code,
                            'sllc_ldgr_code': $scope.edt.sllc_ldgr_code,
                            'slma_acno': $scope.edt1.gldd_acct_code,
                            'coad_pty_full_name': selectedText,
                            'coad_dept_no': $scope.edt1.coad_dept_no,
                            'gldd_party_ref_date': $scope.edt.gldd_party_ref_date,
                            'gldd_doc_narr': $scope.edt.gldd_doc_narr,
                            'gldd_doc_amount': $scope.edt.gldd_doc_amount,
                            'gldd_fc_amount': 0.00

                        });
                        swal({ title: "Alert", text: "Record Updated", width: 300, height: 200 });
                        $scope.total = 0;
                        for (var i = 0; i < $scope.Bankdetails.length; i++) {
                            $scope.total = $scope.total + parseInt($scope.Bankdetails[i].gldd_doc_amount);
                            // $scope.fctotal = $scope.fctotal + parseInt($scope.Bankdetails[i].gldd_fc_amount);
                        }
                        //$scope.btnDelete = true;

                        $scope.fctotal = 0;
                        for (var i = 0; i < $scope.Bankdetails.length; i++) {

                            $scope.fctotal = $scope.fctotal + parseInt($scope.Bankdetails[i].gldd_fc_amount);
                            
                        }
                        $scope.edt = "";
                        $scope.edt1 = "";
                        $scope.edt2 = "";
                        $scope.updatebtn = false;
                        $("#cmb_acc_Code3").select2("val", "");
                        // $scope.edt.slma_acno = '';
                        $scope.myForm.$setPristine();
                        $scope.myForm.$setUntouched();
                        Bankdetails = [];
                        //$scope.edt = "";
                        //$scope.edt1 = "";
                        //$scope.edt2 = "";
                        //$scope.updatebtn = false;
                        //$scope.Add = true;
                        //$("#cmb_acc_Code3").select2("val", "");
                    };

                }
            }

            $scope.coad_pty_full_name = $scope.selectedText;

            $scope.Delete = function (str) {
                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    if ($scope.Bankdetails[i].slma_acno == str.slma_acno && $scope.Bankdetails[i].gldd_doc_amount == str.gldd_doc_amount) {
                        $scope.Bankdetails.splice(i, 1);
                        break;
                    }
                }
                $scope.total = 0;
                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    // $scope.total = $scope.total + parseInt($scope.Bankdetails[i].gldd_doc_amount);
                    $scope.total = parseFloat($scope.total) + parseFloat($scope.Bankdetails[i].gldd_doc_amount);
                }
            }

            $scope.getAccountName = function (str)
            {
                
                cost = str.glpc_acct_code;
                var cost_center = cost;

                $http.get(ENV.apiUrl + "api/BankPayment/getCostCenter?aacno=" + cost_center + "&comp_cd=" + comp_code + "&fyear=" + finance_year).then(function (cost_center) {

                    $scope.costcenter_account = cost_center.data;

                    if ($scope.costcenter_account.length > 0) {
                        $scope.chkcost_Ac = true;
                    }
                    else {
                        $scope.chkcost_Ac = false;
                        $scope.temp.cost_chk_ac = false;
                        $scope.cost_combo_AC = false;
                    }
                });

                if ($scope.temp == undefined) {
                    $scope.temp = {};
                }

                for (var i = 0; i < $scope.bankname.length; i++) {
                    if ($scope.bankname[i].glpc_acct_code == str.glpc_acct_code) {
                        acconcode = $scope.bankname[i].glpc_acct_code;
                        $scope.temp['glpc_curr_balance'] = $scope.bankname[i].glpc_curr_balance;
                        $scope.temp['glpc_max_limit'] = $scope.bankname[i].glpc_max_limit;
                    }

                }
            }

            $scope.Insert_temp_docs = function () {

                var datasend = [];

                
                var verifyUser, verifyDate, authorize_user, authorize_date;
                if (status == "Verify") {
                    verifyUser = user;
                    verifyDate = $scope.edt4.DocDate;
                }
                else if (status == "Authorize") {
                    verifyUser = user;
                    verifyDate = $scope.edt4.DocDate;
                    authorize_user = "";
                    authorize_date = "";
                }
                else {
                    verifyUser = "";
                    verifyDate = "";
                    authorize_user = "";
                    authorize_date = "";

                }
                if ($scope.temp == undefined) {
                    $scope.temp = "";
                }
                var data = {
                    gltd_comp_code: comp_code,
                    gltd_prepare_user: user,
                    gltd_doc_code: $scope.temp3.gltd_doc_code,
                    gltd_doc_date: $scope.edt4.DocDate,
                    gltd_cur_status: status,
                    gltd_post_date: $scope.edt4.PostDate,
                    gltd_doc_narr: $scope.temp.gltd_doc_narr1,
                    gltd_remarks: $scope.temp.gltd_doc_remark,
                    gltd_prepare_date: $scope.edt4.DocDate,
                    gltd_final_doc_no: "0",
                    gltd_verify_user: verifyUser,
                    gltd_verify_date: verifyDate,
                    gltd_authorize_user: authorize_user,
                    gltd_authorize_date: authorize_date,
                    gltd_paid_to: $scope.temp.gltd_paid_to,
                    gltd_cheque_no: $scope.temp.gltd_cheque_No,
                    glpc_amount: $scope.total,
                    glpc_petty_cash_acno: $scope.edt5.glpc_acct_code.glpc_acct_code,
                    glpc_expense_acno: $scope.temp.gltd_acct_code.gldd_acct_code,
                }
                datasend.push(data);
                
                $http.post(ENV.apiUrl + "api/PettyCash/Insert_Fins_temp_docs_PCDI", datasend).then(function (msg) {
                    $scope.prvno = msg.data;

                    if ($scope.prvno != "" && $scope.prvno != null) {
                        $scope.Insert_Fins_temp_doc_details();
                    }
                    else {
                        swal({ title: "Alert", text: "Document Not Created", width: 300, height: 200 });
                    }
                });

            }

            $scope.Insert_Fins_temp_doc_details = function () {
                
                var dataSend = [];
                var j = 1;

                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    var data = {
                        gldd_doc_narr: $scope.Bankdetails[i].gldd_doc_narr,
                        gldd_dept_code: $scope.Bankdetails[i].coad_dept_no,
                        gldd_ledger_code: $scope.Bankdetails[i].sllc_ldgr_code,
                        gldd_acct_code: $scope.Bankdetails[i].slma_acno,
                        gldd_party_ref_no: $scope.Bankdetails[i].slma_acno,
                        gldd_party_ref_date: $scope.Bankdetails[i].gldd_party_ref_date,
                        gldd_doc_amount: parseFloat($scope.Bankdetails[i].gldd_doc_amount),
                        gldd_cost_center_code: $scope.Bankdetails[i].glco_cost_centre_code,
                        gldd_comp_code: comp_code,
                        gldd_doc_code: $scope.temp3.gltd_doc_code,
                        gldd_prov_doc_no: $scope.prvno,
                        gldd_final_doc_no: "0",
                        gldd_line_no: j,

                    }
                    j++;
                    dataSend.push(data);
                }
                var data = {
                    gldd_comp_code: comp_code,
                    gldd_doc_code: $scope.temp3.gltd_doc_code,
                    gldd_prov_doc_no: $scope.prvno,
                    gldd_final_doc_no: "0",
                    gldd_line_no: j,
                    gldd_ledger_code: "00",
                    gldd_acct_code: $scope.edt5.glpc_acct_code.glpc_acct_code,
                    gldd_cost_center_code: $scope.edt5.glco_cost_centre_code,
                    gldd_doc_amount: ("-" + $scope.total),
                    gldd_fc_amount_debit: 0,
                    gldd_fc_code: "",
                    gldd_fc_rate: 0,
                    gldd_doc_narr: $scope.temp.gltd_doc_narr1,
                }
                dataSend.push(data);
                $http.post(ENV.apiUrl + "api/BankPayment/Insert_Fins_temp_doc_details", dataSend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if (status == "Authorize") {
                        msg_flag1 = true;
                        $scope.Authorize_Insert_temp_doc_details($scope.msg1);
                    }
                    else if (status == "Save") {
                        msg_flag1 = false;
                        $scope.Authorize_Insert_temp_doc_details($scope.msg1);

                    }
                    else if (status == "Verify") {
                        msg_flag1 = false;
                        $scope.Authorize_Insert_temp_doc_details($scope.msg1);

                    }
                    if ($scope.msg1 == true) {

                    }
                    else {
                        swal({ title: "Alert", text: "Voucher Not Created", width: 300, height: 200 });
                    }
                });
            }

            $scope.Authorize_Insert_temp_doc_details = function (str) {
                
                if (msg_flag1) {
                    msg_flag1 = false;
                    var auth_date = $scope.edt4.PostDate;
                    $http.post(ENV.apiUrl + "api/BankPayment/Authorize_Posting?comp_code=" + comp_code + "&doc_code=" + $scope.temp3.gltd_doc_code + "&prv_no=" + $scope.prvno + "&auth_user=" + user + "&auth_date=" + auth_date).then(function (Auth) {

                        $scope.Docno = Auth.data;
                        if ($scope.Docno != "") {
                            swal({ title: "Alert", text: "Voucher Posted Successfully\nFinal Doc No is=" + $scope.Docno, width: 300, height: 200 });
                            $scope.checkprintbtn = true;
                            $scope.ClearInsert();
                            $scope.Authorizeuser();
                            $scope.cancel();
                        }
                        else {
                            swal({ title: "Alert", text: "Posting Failed", width: 300, height: 200 });
                        }
                    });

                }
                else if ((msg_flag1 == false) && (status == "Save")) {
                    msg_flag1 = false;
                    if (str) {
                        swal({
                            title: "Alert", text: "Voucher Created Successfully\nProvision No:-" + $scope.temp3.gltd_doc_code + "-" + $scope.prvno, width: 300, height: 200
                        });
                        $scope.ClearInsert();
                        $scope.Authorizeuser();
                        $scope.cancel();
                    }
                    else {
                        swal({ title: "Alert", text: "Voucher Not Created", width: 300, height: 200 });
                    }
                }

                else if ((msg_flag1 == false) && (status == "Verify")) {
                    msg_flag1 = false;
                    if (str) {
                        swal({ title: "Alert", text: "Voucher Verified Successfully\nProvision No:-" + $scope.temp3.gltd_doc_code + "-" + $scope.prvno, width: 300, height: 200 });
                        $scope.ClearInsert();
                        $scope.Authorizeuser();
                        $scope.cancel();
                    }
                    else {
                        swal({ title: "Alert", text: "Verification Failed", width: 300, height: 200 });
                    }
                }

                $scope.ClearInsert();
                $scope.Authorizeuser();
            }

            $scope.Prepare = function () {
                $scope.chkcost_Ac = false;
                $scope.cost_combo_Ac = false;

                
                try {
                    if ($scope.Bankdetails.length > 0) {
                        $scope.flag1 = false;
                        if ($scope.edt4.PostDate == null || $scope.edt4.PostDate == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Select Posting Date", showCloseButton: true, width: 380, });

                        }

                        else {

                            $scope.preparebtn = true;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                            status = "Save";
                            $scope.Insert_temp_docs();

                        }
                    }
                    else {
                        swal({ title: "Alert", text: "Please Add Doc Details", showCloseButton: true, width: 380, });
                    }
                }


                catch (e) {

                }


            }

            $scope.Verify = function () {
                try {
                    if ($scope.Bankdetails.length > 0) {
                        $scope.flag1 = false;
                        if ($scope.edt4.PostDate == null || $scope.edt4.PostDate == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Select Posting Date", showCloseButton: true, width: 380, });

                        }

                        else {
                            $scope.preparebtn = true;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                            status = "Verify";
                            $scope.Insert_temp_docs();
                        }
                    }
                    else {
                        swal({ title: "Alert", text: "Please Add Doc Details", showCloseButton: true, width: 380, });
                    }
                }
                catch (e) {
                }
            }

            $scope.Authorize = function () {
                try {
                    if ($scope.Bankdetails.length > 0) {
                        $scope.flag1 = false;
                        if ($scope.edt4.PostDate == null || $scope.edt4.PostDate == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Select Posting Date", showCloseButton: true, width: 380, });

                        }

                        else {
                            $scope.preparebtn = true;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                            status = "Authorize";
                            $scope.Insert_temp_docs();
                        }
                    }
                    else {
                        swal({ title: "Alert", text: "Please Add Doc Details", showCloseButton: true, width: 380, });
                    }


                } catch (e) {

                }
            }

            $scope.ClearInsert = function () {
                $scope.Bankdetails = [];
                $scope.total = "";
                $scope.temp = {
                    gltd_cheque_No: "",
                    gltd_paid_to: "",
                    gltd_doc_narr1: "",
                    gltd_doc_remark: "",
                }

                $http.get(ENV.apiUrl + "api/BankPayment/GetVerifyAuthorizeUsers_PC?username=" + user + "&comp_code=" + comp_code + "&fyear=" + finance_year + "&doc_code=" + $scope.getDocCode.gltd_doc_code).then(function (users_list) {

                    $scope.users = users_list.data;
                    if ($scope.users.length != 0) {

                        //$scope.preparebtn = true;
                        //$scope.verifybtn = true;
                        //$scope.authorizebtn = false;
                        if ($scope.users[0] == user && $scope.users[1] == user) {
                            $scope.preparebtn = true;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = false;
                        }
                        else if ($scope.users[0] == user && ($scope.users[1] == "" || $scope.users[1] != user)) {
                            $scope.preparebtn = true;
                            $scope.verifybtn = false;
                            $scope.authorizebtn = true;
                        }
                        else {
                            $scope.preparebtn = false;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                        }
                    }
                });

                $http.get(ENV.apiUrl + "api/BankPayment/GetAllPetty_CashAccount?cmp_cd=" + comp_code + "&fyear=" + finance_year + "&doc_cd=" + $scope.getDocCode.gltd_doc_code + "&username=" + user).then(function (docstatus2) {
                    
                    $scope.bankname = docstatus2.data;
                    if (docstatus2.data.length > 0) {
                        $scope.edt5['glpc_acct_code'] = docstatus2.data[0];
                        $scope.getAccountName(docstatus2.data[0])
                    }

                });

            }

            $scope.onlyNumbers = function (event) {
                ;
                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };


        }]
        )
})();
