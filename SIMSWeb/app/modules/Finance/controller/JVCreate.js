﻿(function () {
    'use strict';
    var main, temp;
    var data1 = [];
    var data2 = [];
    var data3 = [];
    var status = "";
    var msg_flag1 = false, jvaut = false, jver = false;
    var prdocdata = [];
    var acconcode = "";
    var prvno = "";
    var cmbvalue = "";
    var comp_code = "1", chk_rcur;
    var data = [];
    var chk;
    var cost;
    var coad_pty_short_name = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('JVCreateCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.edt = "";
            $scope.amount_difference = 0;
            $scope.operation = true;
            $scope.chkcost = true;
            $scope.cost_combo = true;
            // $scope.vis = {};
            $scope.doc_cd_prov_no = '';
            $scope.doc_cd_final_doc_no = '';
            $scope.block1 = [];
            $scope.block2 = [];
            $scope.block3 = [];
            $scope.temp_doc = [];
            $scope.vis = [];
            $scope.vis['main'] = true;
            $scope.vis.onchangedoccodeshow = false;
            $scope.dramount = false;
            $scope.cramount = false;
            $scope.vis.doc_detail_grid = true;
            $scope.vis.main_acct_grid = true;
            $scope.save1 = true;
            $scope.vis.add_grid = true;
            $scope.checkprintbtn = false;
            $scope.vis.Add_show_hide = true;
            $scope.prdocdata = [];
            $scope.cmb_doc_cd = [];
            $scope.vis.update = false;
            //new sgr
            $scope.chkcost = false;
            $scope.block3.gldd_vat_enable = false;
            $scope.block3.gldd_include_vat = false;
            $scope.block3.gldd_doc_vat_amt = 0;

            $scope.cost_combo = false;
            var costcentername;

            $scope.readonly = true;
            //$scope.vis.SendToVerify = false;
            $scope.vis.ok = true;
            $scope.vis.is_recur = true;
            $scope.preparebtn = true;
            $scope.verifybtn = true;
            $scope.authorizebtn = true;

            $scope.vis.cmb_acct = true;
            $scope.vis.txt_acct = false;
            $scope.dis_recur_date = true;

            $scope.revdelete = false;

            $scope.edt2 = [];
            $scope.edt = [];
            $scope.voucher_report_show = true;
            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();
            

            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
            var vat_percent = $scope.finnDetail.comp_vat_per;
            var comp_vat_status = $scope.finnDetail.comp_vat_status;
            var input_vat_acct = $scope.finnDetail.input_vat_acct;
            var vat_ac_name = '';

            //console.log('comp_data_log');

            //console.log($scope.finnDetail);
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();

            $scope.block1 = {
                gltd_payment_date: dd + '-' + mm + '-' + yyyy,
            }
            if (comp_vat_status == 'Y') {
                $scope.vis_vat = true;
                $scope.vis_vat_amt = true;
                $scope.block3.gldd_vat_enable = true;
                $scope.block3.gldd_include_vat = true;
            }
            else {
                $scope.vis_vat = false;
                $scope.vis_vat_amt = false;
                $scope.block3.gldd_doc_vat_amt = 0;
                $scope.block3.gldd_vat_enable = false;
                $scope.block3.gldd_include_vat = false;
            }


            //var comp_code = '1';
            //var finance_year = '2016';
            setTimeout(function () {
                $("#cmb_acc_Code3").select2();

            }, 100);


           $scope.deleteReverted = function () {
                
               $scope.data = [];
               var datasend = {
                   'gltd_doc_code': $scope.block1.gltd_doc_code,
                   'gltd_prov_doc_no': $scope.block1.gltd_prov_doc_no,
                   'gltd_comp_code': comp_code,

               }
               $scope.data.push(datasend);

               $http.post(ENV.apiUrl + "api/JVCreation/revertedDelete", $scope.data).then(function (msg) {
                   
                   $scope.msg1 = msg.data;
                   if ($scope.msg1 == true) {
                       swal({ title: "Alert", text: "Record Deleted successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                       window.location.reload();
                   }
                   else {
                       swal({ title: "Alert", text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                   }
                      /* $http.post(ENV.apiUrl + "api/JVCreation/deleteTempDocs", $scope.data).then(function (message) {
                           $scope.message1 = message.data;
                           if ($scope.message1 == true) {
                               swal({ title: "Alert", text: "Records Deleted successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                           }
                       });*/

               });
            }

                
            $http.get(ENV.apiUrl + "api/JVCreation/getLoggedUser").then(function (res) {
                $scope.data = [];
                $scope.loggedUser = res.data;
              
                for (var i = 0; i < $scope.loggedUser.length; i++) {
                   
                    if($scope.loggedUser[i].fins_appl_form_field_value1.toLowerCase() == user && ($scope.block1.gltd_doc_code != '' || $scope.block1.gltd_doc_code != 'undefined') && ($scope.block1.gltd_prov_doc_no != '' || $scope.block1.gltd_prov_doc_no != 'undefined')) {
                        $scope.revdelete = true;
                        
                    }
                    else {
                        $scope.revdelete = false;
                    }

                }
                  
           });
            
  

            $scope.maxmize = function () {
                $('#jv_Voucher').css({ 'height': '100%', 'width': '100%' })
                $scope.windowflg = true;
            }

            $scope.manimize = function () {
                $('#jv_Voucher').css({ 'height': '600px', 'width': '900px' })
                $scope.windowflg = false;

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
                $("#Table1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.Prepare = function (str) {
                //$('#myModal_Prepare').modal('show');
                jQuery('.verifybtn').removeClass('voucherbtn');
                jQuery('.preparebtn').addClass('voucherbtn');
                jQuery('.authorizebtn').removeClass('voucherbtn');
                
                $scope.vis.prepare = true;
                $scope.vis.verify = false;
                $scope.vis.Autorize = false;
                $scope.vis['main'] = false;
                $scope.jvtitle = '- Prepare';

                $http.get(ENV.apiUrl + "api/JVCreation/getRevertedData?comp_code=" + comp_code + "&username=" + user).then(function (docstatus2) {
                    $scope.revertdocument = docstatus2.data;
                    if ($scope.revertdocument[0].revert_doc_no != '')
                        $scope.vis.revert_grd = true;
                    else
                        $scope.vis.revert_grd = false;

                });

                $http.get(ENV.apiUrl + "api/BankPayment/GetVerifyAuthorizeUsers?username=" + user + "&comp_code=" + comp_code + "&fyear=" + finance_year).then(function (users_list) {
                    
                    $scope.users = users_list.data;
                    if ($scope.users.length != 0) {
                        
                        if ($scope.users[0] == user && $scope.users[1] == user) {

                            $scope.vis.autoriz = true;
                            $scope.vis.very = true;
                        }
                        else if ($scope.users[0] == user && ($scope.users[1] == "" || $scope.users[1] != user)) {

                            $scope.vis.autoriz = false;
                            $scope.vis.very = true;
                        }
                        else {

                            $scope.vis.autoriz = false;
                            $scope.vis.very = false;
                        }
                    }
                });
            }

            $scope.Verify = function () {
                //   $('#myModal_Verify').modal('show');
                jQuery('.verifybtn').addClass('voucherbtn');
                jQuery('.preparebtn').removeClass('voucherbtn');
                jQuery('.authorizebtn').removeClass('voucherbtn');
                $scope.edt = "";
                $scope.edt1 = "";
                $scope.edt2 = "";
                $scope.vis['main'] = true;
                $scope.jvtitle = '- Verify';

                $scope.vis.prepare = false;
                $scope.vis.verify = true;
                $scope.vis.Autorize = false;
                $scope.vis.user_vry = false;
                $scope.vis.user_aut = false;

                $('#loader').modal({ backdrop: 'static', keyboard: false });

                $http.get(ENV.apiUrl + "api/JVCreation/getFinn140_SearchFins_temp_docs?compcode=" + comp_code + "&username=" + user).then(function (prdoc_data) {

                    $scope.verify_data = prdoc_data.data;

                    $('#loader').modal('hide');
                    $(".modal-backdrop").removeClass("modal-backdrop");
                });



            }

            $scope.Authorize = function (str) {
                jQuery('.verifybtn').removeClass('voucherbtn');
                jQuery('.preparebtn').removeClass('voucherbtn');
                jQuery('.authorizebtn').addClass('voucherbtn');
                $scope.jvtitle = '- Authorize';
                $scope.edt = "";
                $scope.edt1 = "";
                $scope.edt2 = "";
                $scope.vis['main'] = true;

                $scope.vis.prepare = false;
                $scope.vis.verify = false;
                $scope.vis.Autorize = true;

                $scope.vis.user_vry = true;
                $scope.vis.user_aut = false;

                $('#loader').modal({ backdrop: 'static', keyboard: false });
                
                $http.get(ENV.apiUrl + "api/JVCreation/getFinn142_SearchFins_temp_docs?compcode=" + comp_code + "&username=" + user).then(function (prdoc_data) {

                    $scope.Authorize_data = prdoc_data.data;

                    $('#loader').modal('hide');
                    $(".modal-backdrop").removeClass("modal-backdrop");
                });

                //   $('#myModal_Authorize').modal('show');
            }

            //Back button
            $scope.back = function () {
               
                jQuery('.verifybtn').removeClass('voucherbtn');
                jQuery('.preparebtn').removeClass('voucherbtn');
                jQuery('.authorizebtn').removeClass('voucherbtn');

                $scope.vis.main = true;
                $scope.vis.verify = false;
                $scope.vis.Autorize = false;
                $scope.jvtitle = ' ';
                $scope.checkprintbtn = false;
                $scope.totalDebit = 0;
                $scope.totalcredit = 0;
                $scope.prvno = '';

                $scope.dcode = false;
                $scope.block1 = [];
                $scope.block2 = [];
                $scope.block3 = [];
                $scope.temp_doc = [];

                $scope.edt2 = [];
                $scope.edt = [];
                $scope.prdocdata = [];

                $scope.vis.cmb_acct = true;
                $scope.vis.txt_acct = false;
                $scope.ldgcd = false;

                $scope.vis.is_recur = true;
                $scope.vis.ok = true;
                $scope.vis.update = false;
                $scope.vis.SendToVerify = false;
                $scope.checkprintbtn = false;

                $scope.vis.onchangedoccodeshow = false;

                //$scope.Chk_recur_voucher($scope.vis.is_recur);
                $scope.dis_recur_date = true;
                $scope.lbl_recur = '';
                $scope.vis.recur_periods = false;

                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                if (mm < 10) {
                    mm = '0' + mm;
                }
                var yyyy = today.getFullYear();
                $scope.sdate = dd + '-' + mm + '-' + yyyy;
                $scope.edate = dd + '-' + mm + '-' + yyyy;
                $scope.temp_doc = {
                    gltd_doc_date: dd + '-' + mm + '-' + yyyy,
                    gltd_post_date: dd + '-' + mm + '-' + yyyy
                }

            }

            //reset button
            $scope.clear = function () {
                $scope.dcode = false;
                $scope.block1 = [];
                $scope.block2 = [];
                $scope.block3 = [];
                $scope.temp_doc = [];
                $scope.checkprintbtn = false;
                $scope.prvno = '';

                $scope.totalDebit = 0;
                $scope.totalcredit = 0;

                $scope.edt2 = [];
                $scope.edt = [];
                $scope.prdocdata = [];

                $scope.vis.cmb_acct = true;
                $scope.vis.txt_acct = false;
                $scope.ldgcd = false;

                $scope.vis.is_recur = true;
                $scope.vis.ok = true;
                $scope.vis.update = false;
                $scope.vis.SendToVerify = false;
                $scope.checkprintbtn = false;
                $scope.vis.onchangedoccodeshow = false;
                jvaut = false;
                jver = false;

                $scope.dis_recur_date = true;
                $scope.lbl_recur = '';
                $scope.vis.recur_periods = false;
                //$scope.Chk_recur_voucher($scope.vis.is_recur);

                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                if (mm < 10) {
                    mm = '0' + mm;
                }
                var yyyy = today.getFullYear();
                $scope.sdate = dd + '-' + mm + '-' + yyyy;
                $scope.edate = dd + '-' + mm + '-' + yyyy;
                $scope.temp_doc = {
                    gltd_doc_date: dd + '-' + mm + '-' + yyyy,
                    gltd_post_date: dd + '-' + mm + '-' + yyyy
                }


                $http.get(ENV.apiUrl + "api/BankPayment/GetVerifyAuthorizeUsers?username=" + user + "&comp_code=" + comp_code + "&fyear=" + finance_year).then(function (users_list) {
                    
                    $scope.users = users_list.data;
                    if ($scope.users.length != 0) {
                        
                        if ($scope.users[0] == user && $scope.users[1] == user) {

                            $scope.vis.autoriz = true;
                            $scope.vis.very = true;
                        }
                        else if ($scope.users[0] == user && ($scope.users[1] == "" || $scope.users[1] != user)) {

                            $scope.vis.autoriz = false;
                            $scope.vis.very = true;
                        }
                        else {

                            $scope.vis.autoriz = false;
                            $scope.vis.very = false;
                        }
                    }
                });
            }

            //Show SearchedData

            $scope.Show = function () {
                

                if (($scope.block1.gltd_doc_code != undefined && $scope.block1.gltd_doc_code != "") && ($scope.block1.gltd_prov_doc_no != undefined && $scope.block1.gltd_prov_doc_no != "")) {
                    $scope.dcode = true;
                    $http.get(ENV.apiUrl + "api/JVCreation/getAllRecords?doccode=" + $scope.block1.gltd_doc_code + "&prnumber=" + $scope.block1.gltd_prov_doc_no + "&comp_code=" + comp_code).then(function (prdoc_data) {

                        $scope.prdocdata = prdoc_data.data;

                        $scope.totalDebit = 0;
                        $scope.totalcredit = 0;

                        for (var i = 0; i < $scope.prdocdata.length; i++) {

                            if (i == 0) {
                                $scope.temp_doc.gltd_comp_code = $scope.prdocdata[i].gltd_comp_code;
                                $scope.temp_doc.gltd_doc_code = $scope.prdocdata[i].gltd_doc_code;

                                $scope.temp_doc.gltd_doc_narr = $scope.prdocdata[i].gltd_doc_narr;
                                $scope.temp_doc.gltd_remarks = $scope.prdocdata[i].gltd_remarks;
                                $scope.temp_doc.gltd_doc_date = $scope.prdocdata[i].gltd_doc_date;
                                $scope.temp_doc.gltd_post_date = $scope.prdocdata[i].gltd_post_date;
                                $scope.block1.gltd_cheque_No = $scope.prdocdata[i].gltd_cheque_no;
                                $scope.block1.gltd_paid_to = $scope.prdocdata[i].gltd_paid_to;

                                $scope.showhide();
                            }


                            $scope.totalDebit = parseFloat(parseFloat($scope.totalDebit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_debit)).toFixed(3);
                            $scope.totalcredit = parseFloat(parseFloat($scope.totalcredit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_credit)).toFixed(3);

                            

                        }

                    });

                    $scope.vis.ok = false;
                    $scope.vis.update = true;
                    $scope.vis.SendToVerify = true;
                    $scope.vis.is_recur = false;

                    $scope.vis.autoriz = false;
                    $scope.vis.very = false;
                }
                else {
                    $scope.flag1 = true;
                    swal({ title: "Alert", text: "Please select Doc Code & Provision No", showCloseButton: true, width: 380, });
                }
            }

            //Single Check click

            $scope.chk_click_ver = function (obj) {
                if (obj.gltd_revert) {
                    obj.gltd_revert = false;
                    obj.gltd_verify = true;
                }

            }

            $scope.chk_click_rev = function (obj) {
                if (obj.gltd_verify) {
                    obj.gltd_revert = true;
                    obj.gltd_verify = false;
                }

            }

            $scope.chk_click_aut = function (obj) {

                if (obj.gltd_verify) {
                    obj.gltd_verify = false;
                    obj.gltd_authorize = true;
                }


            }

            $scope.chk_click_very = function (obj) {
                if (obj.gltd_authorize) {
                    obj.gltd_verify = true;
                    obj.gltd_authorize = false;
                }

            }

            //check all Authorize button

            $scope.chk_clickAll = function (str) {
                
                if (str) {
                    for (var i = 0; i < $scope.Authorize_data.length; i++) {
                        $scope.Authorize_data[i].gltd_authorize = true;
                    }
                }
                else {
                    for (var i = 0; i < $scope.Authorize_data.length; i++) {
                        $scope.Authorize_data[i].gltd_authorize = false;
                    }
                }
            }

            //check all verify button

            $scope.chk_click_verifyAll = function (str) {

                if (str) {
                    for (var i = 0; i < $scope.verify_data.length; i++) {
                        $scope.verify_data[i].gltd_verify = true;
                        $scope.verify_data[i].gltd_revert = false;
                    }
                    $scope.v_data.gltd_revert1 = false;
                }
                else {
                    for (var i = 0; i < $scope.verify_data.length; i++) {
                        $scope.verify_data[i].gltd_verify = false;
                    }
                }
            }

            //check all revert button

            $scope.chk_click_revertAll = function (str) {

                if (str) {
                    for (var i = 0; i < $scope.verify_data.length; i++) {
                        $scope.verify_data[i].gltd_revert = true;
                        $scope.verify_data[i].gltd_verify = false;
                    }
                    $scope.v_data.gltd_verify1 = false;
                }
                else {
                    for (var i = 0; i < $scope.verify_data.length; i++) {
                        $scope.verify_data[i].gltd_revert = false;
                    }
                }
            }

            //check all authorize button

            $scope.chk_clickAll = function (str) {

                if (str) {
                    for (var i = 0; i < $scope.Authorize_data.length; i++) {
                        $scope.Authorize_data[i].gltd_authorize = true;
                        $scope.Authorize_data[i].gltd_verify = false;
                    }
                    $scope.A_data.gltd_very1 = false;
                }
                else {
                    for (var i = 0; i < $scope.Authorize_data.length; i++) {
                        $scope.Authorize_data[i].gltd_authorize = false;
                    }
                }
            }

            //check all revert verify button

            $scope.very_chk_clickAll = function (str) {

                if (str) {
                    for (var i = 0; i < $scope.Authorize_data.length; i++) {
                        $scope.Authorize_data[i].gltd_verify = true;
                        $scope.Authorize_data[i].gltd_authorize = false;

                    }
                    $scope.A_data.gltd_authorize1 = false;
                }
                else {
                    for (var i = 0; i < $scope.Authorize_data.length; i++) {
                        $scope.Authorize_data[i].gltd_verify = false;
                    }
                }
            }



            $scope.ShowFilterText = function (str) {
                
                if (str == "") {
                    $scope.filterVisible = false;
                }
                else {

                    $scope.filterVisible = true;
                }
                $scope.block1.chk_previous_clear = true;
                $scope.edt.txt_filter_range = '0';
            }

            $scope.ShowFilterText_Aut = function (str) {
                if (str == "") {
                    $scope.filterVisible_aut = false;
                }
                else {

                    $scope.filterVisible_aut = true;
                }
                $scope.block1.chk_previous_clear_aut = true;
                $scope.edt.txt_filter_range_aut = '0';

            }


            //On Change Doc Code
            $scope.showhide = function () {
                debugger;
                $scope.cmb_doc_cd = [];
                var index = $("#cmb_doc_code").prop('selectedIndex');
                $scope.cmb_doc_cd = $scope.geDocCode_jv[index];

                console.log($scope.cmb_doc_cd.gldc_doc_code_type);

                $http.get(ENV.apiUrl + "api/JVCreation/GetAllCashBankGLMasterAccountNo?cmp_cd=" + comp_code + "&financialyear=" + finance_year + "&doc_cd=" + $scope.temp_doc.gltd_doc_code + "&username=" + user).then(function (docstatus2) {
                    
                    $scope.bankname = docstatus2.data
                    if ($scope.bankname.length > 0) {
                        $scope.block2['master_acno'] = docstatus2.data[0];
                        $scope.getAccountName(docstatus2.data[0].master_ac_cdname)
                    }
                });

                $scope.vis.main_acct_grid = true;

                if ($scope.cmb_doc_cd.gldc_doc_code_type == "BP") {
                    $scope.vis.onchangedoccodeshow = true;

                    $scope.block2['gldd_doc_amount_debit'] = '0.00';
                    $scope.block2['gldd_doc_amount_credit'] = '0.00';

                    $scope.content = 'Paid To';
                    $scope.vis.cheq_no = true;
                    $scope.dvalue = true;
                    $scope.cvalue = false;
                    $scope.dramount = false;
                    $scope.cramount = true;


                }
                else if ($scope.cmb_doc_cd.gldc_doc_code_type == "BR") {
                    $scope.vis.onchangedoccodeshow = true;

                    $scope.block2['gldd_doc_amount_credit'] = '0.00';
                    $scope.vis.cheq_no = true;
                    $scope.content = 'Received From';



                    $scope.dvalue = false;
                    $scope.cvalue = true;
                    $scope.dramount = true;
                    $scope.cramount = false;

                }
                else if ($scope.cmb_doc_cd.gldc_doc_code_type == "CP") {
                    $scope.vis.onchangedoccodeshow = true;

                    $scope.block2['gldd_doc_amount_debit'] = "0.00";

                    $scope.vis.cheq_no = false;

                    $scope.content = 'Paid To';
                    $scope.dvalue = true;
                    $scope.cvalue = false;
                    $scope.dramount = false;
                    $scope.cramount = true;

                }
                else if ($scope.cmb_doc_cd.gldc_doc_code_type == "CR") {
                    $scope.vis.onchangedoccodeshow = true;

                    $scope.block2['gldd_doc_amount_credit'] = '0.00';



                    $scope.content = 'Received From';
                    $scope.vis.cheq_no = false;
                    $scope.dvalue = false;
                    $scope.cvalue = true;
                    $scope.dramount = true;
                    $scope.cramount = false;

                }
                else if ($scope.cmb_doc_cd.gldc_doc_code_type == "CN") {
                    $scope.vis.onchangedoccodeshow = true;

                    $scope.block2['gldd_doc_amount_debit'] = '0.00';
                    $scope.block2['gldd_doc_amount_credit'] = '0.00';

                    $scope.content = 'Paid To';
                    $scope.vis.cheq_no = true;

                    $scope.dvalue = false;
                    $scope.cvalue = false;
                    $scope.dramount = false;
                    $scope.cramount = false;
                    $scope.vis.main_acct_grid = false;

                }

                else {
                    $scope.vis.onchangedoccodeshow = false;
                    $scope.dramount = false;
                    $scope.cramount = false;

                }
            }

            
            $http.get(ENV.apiUrl + "api/JVCreation/getDocCodeJV?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                $scope.geDocCode_jv = res.data;
            });

            $http.get(ENV.apiUrl + "api/BankPayment/GetVerifyAuthorizeUsers?username=" + user + "&comp_code=" + comp_code + "&fyear=" + finance_year).then(function (users_list) {
                
                $scope.users = users_list.data;
                if ($scope.users.length != 0) {
                    
                    if ($scope.users[0] == user && $scope.users[1] == user) {
                        $scope.preparebtn = false;
                        $scope.verifybtn = false;
                        $scope.authorizebtn = false;

                        $scope.vis.autoriz = true;
                        $scope.vis.very = true;
                    }
                    else if ($scope.users[0] == user && ($scope.users[1] == "" || $scope.users[1] != user)) {
                        $scope.verifybtn = false;
                        $scope.preparebtn = false;
                        $scope.authorizebtn = true;

                        $scope.vis.autoriz = false;
                        $scope.vis.very = true;
                    }
                    else {
                        $scope.preparebtn = false;
                        $scope.verifybtn = true;
                        $scope.authorizebtn = true;

                        $scope.vis.autoriz = false;
                        $scope.vis.very = false;
                    }
                }
            });

            //$http.post(ENV.apiUrl + "api/JVCreation/Check_status_for_doc_users_In_comn_user_application_fins?comp_code=" + comp_code).then(function (res) {
            //    
            //    $scope.result = res.data;
            //    if ($scope.result == true) {
            //        
            //        $scope.status_for_doc_users_completed();
            //    }
            //});


            //$http.get(ENV.apiUrl + "api/BankPayment/GetLedgerNumber").then(function (docstatus2) {
            $http.get(ENV.apiUrl + "api/BankPayment/GetLedgerNumber?financialyear=" + finance_year + "&comp_code=" + comp_code).then(function (docstatus2) {
                $scope.LdgrCode = docstatus2.data;
                });

            $scope.fillaccountno = function () {
                //
                $http.get(ENV.apiUrl + "api/JVCreation/GetAllCashBankGLMasterAccountNo?cmp_cd=" + comp_code + "&financialyear=" + finance_year + "&doc_cd=" + $scope.temp_doc.gltd_doc_code + "&username=" + user).then(function (docstatus2) {
                    
                    $scope.bankname = docstatus2.data
                    if ($scope.bankname.length > 0) {
                        $scope.block2['master_acct_detail'] = docstatus2.data[0];
                        $scope.getAccountName(docstatus2.data[0].master_ac_cdname)
                    }


                });
            }

            $scope.getAccountName = function (str) {
                
                cost = str.master_acno;
                var cost_center = cost;

                $http.get(ENV.apiUrl + "api/BankPayment/getCostCenter?aacno=" + cost_center + "&comp_cd=" + comp_code + "&fyear=" + finance_year).then(function (cost_center) {
                    
                    $scope.costcenter_account = cost_center.data;

                    if ($scope.costcenter_account.length > 0) {
                        $scope.chkcost_Ac = true;
                    }
                    else {
                        $scope.chkcost_Ac = false;
                        $scope.temp.cost_chk_ac = false;
                        $scope.cost_combo_AC = false;
                    }
                    });



                //for (var i = 0; i < $scope.bankname.length; i++) {
                //    if ($scope.bankname[i].master_acno == str.master_acno) {
                //        acconcode = $scope.bankname[i].master_acno;
                //        $scope.block1 = {
                //            master_ac_cdname: $scope.bankname[i].master_ac_cdname,
                //        }
                //    }

                //}
            }

            $scope.getDepartcode = function (str) {

                //Code sgr
                
                $http.get(ENV.apiUrl + "api/BankPayment/getCostCenter?aacno=" + str + "&comp_cd=" + comp_code + "&fyear=" + finance_year).then(function (cost_center) {
                    
                    $scope.cost_center = cost_center.data;
                    if ($scope.cost_center.length > 0) {
                        $scope.chkcost = true;
                    }
                    else {
                        $scope.chkcost = false;
                        $scope.temp.cost_chk = false;
                        $scope.cost_combo = false;
                    }
                });


                for (var i = 0; i < $scope.getAllAccNos.length; i++) {
                    if ($scope.getAllAccNos[i].gldd_acct_code == $scope.block3.gldd_acct_code) {

                        $scope.block3.gldd_dept_code = $scope.getAllAccNos[i].gldd_dept_code;
                        $scope.block3.gldd_party_ref_no = $scope.getAllAccNos[i].gldd_party_ref_no;
                        $scope.block3.codp_dept_name = $scope.getAllAccNos[i].gldd_dept_name;
                        $scope.block3.gldd_acct_name = $scope.getAllAccNos[i].gldd_acct_name;

                    }
                }



            }

            //code sgr
            $scope.cost_center2 = function (str) {
                chk = str;
                
                if (str == true) {
                    $scope.cost_combo = true;
                }
                else {
                    $scope.cost_combo = false;
                }

            }

            $scope.cost_center_Account = function (str) {
                chk = str;
                
                if (str == true) {
                    $scope.cost_combo_AC = true;

                    $scope.getAccountName($scope.block2.master_acno)
                }
                else {
                    $scope.cost_combo_AC = false;
                }

            }

            $scope.Chk_recur_voucher = function (chk_recur) {

                
                if (chk_recur == true) {
                    $scope.dis_recur_date = false;
                    $scope.lbl_recur = '/ Recurring Date';
                    $scope.vis.recur_periods = true;
                    $scope.temp_doc.gltd_recur_period = '0';
                    $scope.temp_doc.gltd_recur_period_days = '0';

                }
                else {
                    $scope.dis_recur_date = true;
                    $scope.lbl_recur = '';
                    $scope.vis.recur_periods = false;
                    $scope.temp_doc.gltd_recur_period = '0';
                    $scope.temp_doc.gltd_recur_period_days = '0';

                }
                //$scope.temp_doc.gltd_recur_date = '';

            }

            $scope.GetAllGLAcc = function (acconcode, cmpnycode) {
                $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {

                    $scope.getAllAccNos = docstatus.data;
                });


            }

            $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {
                
                $scope.getAllAccNos = docstatus.data;
            });

            
            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=Final").then(function (res) {
                $scope.final_doc_url = res.data;
                });

            
            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=Prov").then(function (res) {
                $scope.prov_doc_url = res.data;
                 });


            //Fill Combo SLACCNO
            $scope.getSLAccNo = function () {

                
                var acconcode = $scope.getAllAccNos.gldd_acct_code;
                if ($scope.block3.gldd_ledger_code != "00") {

                    $http.get(ENV.apiUrl + "api/JVCreation/GetSLAccNumber?ldgr_code=" + $scope.block3.gldd_ledger_code + "&cmp_cd=" + comp_code + "&year=" + finance_year).then(function (docstatus3) {
                        // $scope.sllc_ldgr_code = slcode;
                        $scope.getAllAccNos = docstatus3.data;
                        //$scope.slacno = docstatus3.data;
                    });
                }
                else {
                    $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {

                        $scope.getAllAccNos = docstatus.data;
                    });
                }


            }

           

            $scope.getprovno = function () {

                
                $http.get(ENV.apiUrl + "api/JVCreation/Get_provno?comp_code=" + comp_code + "&fyear=" + finance_year + "&doccode=" + $scope.block1.gltd_doc_code + "&user=" + user).then(function (pronum) {
                    $scope.prnumber = pronum.data;
                    $scope.revdelete = false;
                    
                });
                //$scope.fillaccountno();

            }

            //Fill Combo Ledger

            $scope.AddMasterDataInGrid = function () {

                $scope.cost_combo_AC = false;
                $scope.chkcost_Ac = false;
                $scope.temp = { cost_center_ac: false }

                
                if ($scope.block2.master_acno != undefined) {
                    if ($scope.block2.gldd_doc_amount_debit == undefined || $scope.block2.gldd_doc_amount_debit == '0.00') {
                        $scope.block2.gldd_doc_amount_debit = '0.00';
                    }
                    if ($scope.block2.gldd_doc_amount_credit == undefined || $scope.block2.gldd_doc_amount_credit == '0.00') {
                        $scope.block2.gldd_doc_amount_credit = '0.00';
                    }

                    if ($scope.block2.gldd_doc_amount_debit > '0.00' || $scope.block2.gldd_doc_amount_credit > '0.00') {
                        var terminal = document.getElementById("cmb_acc_Code3");
                        //$scope.selectedText = terminal.options[terminal.selectedIndex].text;
                        $scope.prdocdata.push({
                            'gldd_comp_code': comp_code,
                            //'gldd_doc_code': $scope.temp_doc.gltd_doc_code,
                            'gldd_ledger_code': "00",
                            'gldd_acct_code': $scope.block2.master_acno.master_acno,
                            'gldd_acct_name': $scope.block2.master_acno.master_ac_cdname,
                            'gldd_party_ref_no': $scope.block2.pty_ref_no,
                            'glco_cost_centre_code': ($scope.edt5 != undefined && $scope.edt5.glco_cost_centre_code != undefined) ? $scope.edt5.glco_cost_centre_code.glco_cost_centre_code : null,
                            'coce_cost_centre_name': ($scope.edt5 != undefined && $scope.edt5.glco_cost_centre_code != undefined) ? $scope.edt5.glco_cost_centre_code.coce_cost_centre_name : null,
                            'gldd_dept_code': $scope.block2.master_acno.gldd_dept_code,
                            'gldd_doc_narr': $scope.block2.gldd_doc_narr,
                            'gldd_doc_amount_debit': $scope.block2.gldd_doc_amount_debit,
                            'gldd_doc_amount_credit': $scope.block2.gldd_doc_amount_credit,
                            'gltd_payment_date': $scope.block1.gltd_payment_date,
                            'ptyref_date': $scope.block1.gltd_payment_date,
                        });
                        $scope.totalDebit = 0;
                        $scope.totalcredit = 0;

                        for (var i = 0; i < $scope.prdocdata.length; i++) {

                            $scope.totalDebit = parseFloat(parseFloat($scope.totalDebit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_debit)).toFixed(3);
                            $scope.totalcredit = parseFloat(parseFloat($scope.totalcredit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_credit)).toFixed(3);

                        }
                        $scope.block2 = {};



                        $scope.myForm.$setPristine();
                        $scope.myForm.$setUntouched();

                        if ($scope.cmb_doc_cd.gldc_doc_code_type == "CN") {

                            $scope.block2['gldd_doc_amount_debit'] = '0.00';
                            $scope.block2['gldd_doc_amount_credit'] = '0.00';

                            $scope.dvalue = false;
                            $scope.cvalue = false;


                        }
                        //Bankdetails = [];
                        // $scope.dcode = false;
                        //$scope.selectedText = "";
                        //}
                    }
                    else {
                        swal({ text: "  Please Enter Amount ", width: 380 });

                    }

                }
                else {
                    swal({ text: "  please select Master Account No ", width: 380 });

                }
            };

            $scope.UpdateMasterDataInGrid = function () { }

            $scope.CancelMasterDataInGrid = function () { }


            $scope.Add_inner_grid = function () {
                //code sgr
                $scope.chkcost = false;
                $scope.cost_combo = false;
                $scope.temp = { cost_chk_ac: false }

                var ledcode = "";
                // if (myForm) {
                if ($scope.block3.gldd_acct_code != undefined) {

                    if ($scope.block3.gldd_ledger_code == undefined) {
                        ledcode = '00';
                    }
                    else {
                        ledcode = $scope.block3.gldd_ledger_code;
                    }
                    
                    if ($scope.block3.gldd_doc_amount_debit == "" || $scope.block3.gldd_doc_amount_debit == undefined || $scope.block3.gldd_doc_amount_debit == '0.00') {
                        $scope.block3.gldd_doc_amount_debit = '0.00';
                    }
                    if ($scope.block3.gldd_doc_amount_credit == "" || $scope.block3.gldd_doc_amount_credit == undefined || $scope.block3.gldd_doc_amount_credit == '0.00') {
                        $scope.block3.gldd_doc_amount_credit = '0.00';
                    }
                    
                    if (parseFloat($scope.block3.gldd_doc_amount_debit) <= 0 && parseFloat($scope.block3.gldd_doc_amount_credit) <= 0) {
                        swal({ text: "Please Enter Debit/Credit Amount", width: 380 });
                        return;
                    }
                    var terminal = document.getElementById("cmb_acc_Code3");
                    $scope.selectedText = terminal.options[terminal.selectedIndex].text;
                    var found = false;

                    for (var j = 0; j < $scope.prdocdata.length; j++) {
                        if ($scope.prdocdata[j].gldd_acct_code == $scope.block3.gldd_acct_code && $scope.prdocdata[j].gldd_doc_narr == $scope.block3.gldd_doc_narr) {
                            found = true;
                            break;
                        }
                    }
                    if (found == true) {
                        swal({ text: $scope.block3.gldd_acct_name + " A/c Already Added", width: 380 });
                    }
                    else {
                        //code sgr
                        var costcenter1 = document.getElementById("costCenter")
                        var costcentername = costcenter1.options[costcenter1.selectedIndex].text;
                        $scope.center = $scope.center || ($scope.block2.glco_cost_centre_code != '')

                        //if ($scope.block3.gldd_include_vat == false) {
                        //    if ($scope.block3.gldd_doc_amount_debit != 0) {
                        //        $scope.checkevaluesdebit();
                        //    }
                        //    else {
                        //        $scope.checkevaluescredit();
                        //    }
                        //}
                        if (comp_vat_status == 'Y' && $scope.block3.gldd_include_vat == true && $scope.block3.gldd_vat_enable == true) {

                            if ($scope.block3.gldd_doc_amount_debit != 0) {
                                var vat_deb_amt = parseFloat($scope.block3.gldd_doc_amount_debit);
                                $scope.block3.gldd_doc_vat_amt = parseFloat(vat_deb_amt * parseFloat(vat_percent) / (100 + parseFloat(vat_percent))).toFixed(2);
                                $scope.block3.gldd_doc_amount_debit = parseFloat(vat_deb_amt) - parseFloat(vat_deb_amt * parseFloat(vat_percent) / (100 + parseFloat(vat_percent))).toFixed(2);
                                //$scope.gldd_doc_vat_amt = 0;
                                //$scope.block3.gldd_include_vat = true;
                            }
                            else {
                                var vat_cre_amt = parseFloat($scope.block3.gldd_doc_amount_credit);
                                $scope.block3.gldd_doc_vat_amt = parseFloat(vat_cre_amt * parseFloat(vat_percent) / (100 + parseFloat(vat_percent))).toFixed(2);
                                $scope.block3.gldd_doc_amount_credit = parseFloat(vat_cre_amt) - parseFloat(vat_cre_amt * parseFloat(vat_percent) / (100 + parseFloat(vat_percent))).toFixed(2);
                                //$scope.block3.gldd_include_vat = true;
                            }

                        }

                        $scope.prdocdata.push({
                            'gldd_comp_code': comp_code,
                            //'gldd_doc_code': $scope.edt.gltd_doc_code,
                            'gldd_ledger_code': ledcode,
                            'gldd_acct_code': $scope.block3.gldd_acct_code,
                            'gldd_acct_name': $scope.block3.gldd_acct_name,
                            'gldd_dept_code': $scope.block3.gldd_dept_code,
                            'gldd_party_ref_no': $scope.block3.gldd_party_ref_no,
                            'gldd_doc_narr': $scope.block3.gldd_doc_narr,

                            'gldd_doc_amount_debit': $scope.block3.gldd_doc_amount_debit,
                            'gldd_doc_amount_credit': $scope.block3.gldd_doc_amount_credit,
                            'glco_cost_centre_code': $scope.block2.glco_cost_centre_code,
                            'coce_cost_centre_name': costcentername,
                        });

                        if (comp_vat_status == 'Y' && $scope.block3.gldd_vat_enable == true) {
                            if (vat_ac_name == '')
                                for (var i = 0; i < $scope.getAllAccNos.length; i++) {
                                    if ($scope.getAllAccNos[i].gldd_acct_code == input_vat_acct) {
                                        vat_ac_name = $scope.getAllAccNos[i].gldd_acct_name;
                                    }
                                }

                            $scope.prdocdata.push({

                                'gldd_comp_code': comp_code,
                                //'gldd_doc_code': $scope.edt.gltd_doc_code,
                                'gldd_ledger_code': ledcode,
                                'gldd_acct_code': input_vat_acct,
                                'gldd_acct_name': vat_ac_name,
                                'gldd_dept_code': input_vat_acct.substr(1, 2),
                                'gldd_party_ref_no': '',
                                'gldd_doc_narr': 'Vat amount' + ' - ' + $scope.block3.gldd_acct_name,

                                'gldd_doc_amount_debit': ($scope.block3.gldd_doc_amount_debit != 0) ? $scope.block3.gldd_doc_vat_amt : 0,
                                'gldd_doc_amount_credit': ($scope.block3.gldd_doc_amount_credit != 0) ? $scope.block3.gldd_doc_vat_amt : 0,

                                'glco_cost_centre_code': $scope.block2.glco_cost_centre_code,
                                'coce_cost_centre_name': costcentername,
                            });
                        }

                        $scope.totalDebit = 0;
                        $scope.totalcredit = 0;

                        for (var i = 0; i < $scope.prdocdata.length; i++) {

                            $scope.totalDebit = parseFloat(parseFloat($scope.totalDebit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_debit)).toFixed(3);

                            $scope.totalcredit = parseFloat(parseFloat($scope.totalcredit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_credit)).toFixed(3);
                            }
                        //$scope.btnDelete = true;


                        //$scope.edt = "";
                        $scope.edt1 = "";
                        $scope.edt2 = "";
                        $scope.block3.gldd_doc_vat_amt = 0;
                        $("#cmb_acc_Code3").select2("val", "");
                        // $scope.edt.slma_acno = '';
                        $scope.myForm.$setPristine();
                        $scope.myForm.$setUntouched();
                        // Bankdetails = [];
                        $scope.dcode = false;
                       
                        $scope.cancel();
                    }
                }
                else {
                    swal({ text: "  please select Account Code ", width: 380 });

                }
                var acconcode = $scope.getAllAccNos.gldd_acct_code;
                $scope.GetAllGLAcc(acconcode, comp_code);
                $scope.block3.gldd_vat_enable = true;
};

            $scope.update_grid = function (obj) {

                $scope.vis.main_acct_grid = true;

                $scope.tempobj = obj;
                $scope.vis.updatebtn = true;
                $scope.vis.add_grid = false;
                $scope.vis.cmb_acct = false;
                $scope.vis.txt_acct = true;
                $scope.ldgcd = true;


                $scope.block3 = JSON.parse(JSON.stringify(obj));


                $scope.getSLAccNo();

               

                
            }

            $scope.update_grid_data = function () {

                $scope.temp_obj = $scope.block3;
                
                if ($scope.block3.gldd_doc_amount_debit == "" || $scope.block3.gldd_doc_amount_debit == undefined || $scope.block3.gldd_doc_amount_debit == '0.00') {
                    $scope.block3.gldd_doc_amount_debit = '0.00';
                }
                if ($scope.block3.gldd_doc_amount_credit == "" || $scope.block3.gldd_doc_amount_credit == undefined || $scope.block3.gldd_doc_amount_credit == '0.00') {
                    $scope.block3.gldd_doc_amount_credit = '0.00';
                }
                
                if (parseFloat($scope.block3.gldd_doc_amount_debit) <= 0 && parseFloat($scope.block3.gldd_doc_amount_credit) <= 0) {
                    swal({ text: "Please Enter Debit/Credit Amount", width: 380 });
                    return;
                }

                $scope.totalDebit = 0;
                $scope.totalcredit = 0;
                for (var i = 0; i < $scope.prdocdata.length; i++) {

                    if ($scope.prdocdata[i].$$hashKey == $scope.tempobj.$$hashKey) {
                        $scope.prdocdata[i].gldd_doc_amount_credit = $scope.block3.gldd_doc_amount_credit
                        $scope.prdocdata[i].gldd_doc_amount_debit = $scope.block3.gldd_doc_amount_debit;
                        $scope.prdocdata[i].gldd_doc_narr = $scope.block3.gldd_doc_narr;
                    }
                    $scope.totalDebit = parseFloat(parseFloat($scope.totalDebit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_debit)).toFixed(3);
                    $scope.totalcredit = parseFloat(parseFloat($scope.totalcredit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_credit)).toFixed(3);
                }
                $scope.cancel();
            }

            $scope.Updatedatavalue = function () {
                if ($scope.temp_doc.gltd_doc_code != "" && $scope.temp_doc.gltd_doc_code != undefined) {

                    if ($scope.temp_doc.gltd_post_date == "" || $scope.temp_doc.gltd_post_date == undefined) {
                        swal({ text: "  please select Post Date ", width: 380 });
                        return;
                    }
                    if ($scope.prdocdata.length <= 0) {
                        swal({ text: "  Please Add Transaction Details ", width: 380 });
                        return;
                    }
                    if ($scope.totalDebit != $scope.totalcredit) {
                        swal({ text: "Debit Amount: " + $scope.totalDebit + "  Is Not Equal To Credit Amount: " + $scope.totalcredit, width: 380 });

                    }
                    else {
                        var datasend = [];
                        status = "Save";
                        if ($scope.temp == undefined) {
                            $scope.temp = "";
                        }
                        var data = {
                            gltd_comp_code: comp_code,
                            gltd_prepare_user: user,
                            gltd_doc_code: $scope.block1.gltd_doc_code,
                            gltd_prov_doc_no: $scope.block1.gltd_prov_doc_no,
                            gltd_doc_date: $scope.temp_doc.gltd_doc_date,
                            gltd_cur_status: status,
                            gltd_post_date: $scope.temp_doc.gltd_post_date,
                            gltd_doc_narr: $scope.temp_doc.gltd_doc_narr,
                            gltd_remarks: $scope.temp_doc.gltd_remarks,
                            gltd_prepare_date: $scope.temp_doc.gltd_doc_date,
                            gltd_final_doc_no: "0",
                            gltd_verify_user: user,
                            gltd_verify_date: $scope.temp_doc.gltd_doc_date,
                            gltd_authorize_user: user,
                            gltd_authorize_date: $scope.temp_doc.gltd_doc_date,
                            gltd_paid_to: $scope.block1.gltd_paid_to,
                            gltd_cheque_no: $scope.block1.gltd_cheque_No
                        }
                        //datasend.push(data);

                        
                        $http.post(ENV.apiUrl + "api/JVCreation/UpdateFins_temp_docs", data).then(function (msg) {
                            $scope.msg1 = msg.data;

                            if ($scope.msg1 == true) {
                                $scope.prvno = $scope.block1.gltd_prov_doc_no;
                                $scope.Insert_Fins_temp_doc_details();
                            }
                            else if ($scope.msg1 == false) {
                                swal({ title: "Alert", text: "Record Not Updated. " , width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });

                    }
                }
                else {
                    swal({ text: "  please select Doc Code ", width: 380 });
                }

            }

            $scope.cancel = function () {

                $scope.vis.updatebtn = false;
                $scope.vis.add_grid = true;
                $scope.cramount = false;
                $scope.dramount = false;
                $scope.vis.cmb_acct = true;
                $scope.vis.txt_acct = false;
                $scope.ldgcd = false;
                $scope.chkcost = false;
                $scope.cost_combo = false;
                $scope.block3 = [];
                $scope.block3.gldd_include_vat =false;
                $scope.block3.gldd_vat_enable = false;


                if ($scope.cmb_doc_cd.gldc_doc_code_type == "CN") {
                    $scope.vis.main_acct_grid = false;

                }
            }

            // main acct dr & cr on change

            $scope.checkevaluesdebit = function () {

                
                if ($scope.block3.gldd_doc_amount_debit != "" || $scope.block3.gldd_doc_amount_debit != undefined) {
                    var dr_amt = parseFloat($scope.block3.gldd_doc_amount_debit)
                    if (dr_amt > 0) {
                        $scope.block3.gldd_doc_amount_credit = 0;

                        if (comp_vat_status == 'Y' && $scope.block3.gldd_vat_enable == true) {
                            if ($scope.block3.gldd_include_vat == false) {
                                var vat_deb_amt = parseFloat($scope.block3.gldd_doc_amount_debit)
                                $scope.block3.gldd_doc_vat_amt = parseFloat(vat_deb_amt * parseFloat(vat_percent) / 100);
                                $scope.block3.gldd_include_vat = false;
                            }
                            else {
                                $scope.block3.gldd_doc_vat_amt = 0;
                               // $scope.block3.gldd_include_vat = true;
                            }
                        }

                        $scope.cramount = true;
                        $scope.dramount = false;

                    }
                    else {
                        $scope.block3.gldd_doc_amount_credit = 0;
                        $scope.cramount = false;
                        $scope.dramount = false;
                        $scope.block3.gldd_doc_vat_amt = 0;
                      //  $scope.block3.gldd_include_vat = true;

                    }


                }
                else {
                    $scope.block3.gldd_doc_amount_credit = 0;
                    $scope.cramount = false;
                    $scope.dramount = false;
                    $scope.block3.gldd_doc_vat_amt = 0;


                }

            }

            $scope.checkevaluescredit = function () {
                if ($scope.block3.gldd_doc_amount_credit != "" || $scope.block3.gldd_doc_amount_credit != undefined) {
                    var dr_amt = parseFloat($scope.block3.gldd_doc_amount_credit)
                    if (dr_amt > 0) {
                        $scope.block3.gldd_doc_amount_debit = 0;

                        if (comp_vat_status == 'Y' && $scope.block3.gldd_vat_enable == true) {
                            if ($scope.block3.gldd_include_vat == false) {
                                var vat_cre_amt = parseFloat($scope.block3.gldd_doc_amount_credit)
                                $scope.block3.gldd_doc_vat_amt = parseFloat(vat_cre_amt * parseFloat(vat_percent) / 100);
                                $scope.block3.gldd_include_vat = false;
                            }
                            else {
                                $scope.block3.gldd_doc_vat_amt = 0;
                                //$scope.block3.gldd_include_vat = true;
                            }
                        }

                        $scope.dramount = true;
                        $scope.cramount = false;
                    }
                    else {
                        $scope.block3.gldd_doc_amount_debit = 0;
                        $scope.cramount = false;
                        $scope.dramount = false;
                        $scope.block3.gldd_doc_vat_amt = 0;
                        //$scope.block3.gldd_include_vat = true;

                    }
                }
                else {
                    $scope.block3.gldd_doc_amount_debit = 0;
                    $scope.cramount = false;
                    $scope.dramount = false;
                    $scope.block3.gldd_doc_vat_amt = 0;
                   // $scope.block3.gldd_include_vat = true;

                }

            }

            $scope.chk_including_vat = function () {
                if ($scope.block3.gldd_include_vat == false) {
                    if ($scope.block3.gldd_doc_amount_debit != 0) {
                        $scope.checkevaluesdebit();
                    }
                    else {
                        $scope.checkevaluescredit();
                    }
                }
                else {
                    $scope.block3.gldd_doc_vat_amt = 0;
                }
            }

            //master acct dr & cr on change 

            $scope.chk_mstr_ac_dr = function () {

                if ($scope.cmb_doc_cd.gldc_doc_code_type == "CN") {
                    
                    if ($scope.block2.gldd_doc_amount_debit != "" || $scope.block2.gldd_doc_amount_debit != undefined) {
                        var dr_amt = parseFloat($scope.block2.gldd_doc_amount_debit)
                        if (dr_amt > 0) {
                            $scope.block2.gldd_doc_amount_credit = 0;
                            $scope.cvalue = true;
                            $scope.dvalue = false;
                        }
                        else {
                            $scope.block2.gldd_doc_amount_credit = 0;
                            $scope.cvalue = false;
                            $scope.dvalue = false;

                        }
                    }
                    else {
                        $scope.block2.gldd_doc_amount_credit = 0;
                        $scope.cvalue = false;
                        $scope.dvalue = false;

                    }

                }
            }

            $scope.chk_mstr_ac_cr = function () {

                if ($scope.cmb_doc_cd.gldc_doc_code_type == "CN") {
                    if ($scope.block2.gldd_doc_amount_credit != "" || $scope.block2.gldd_doc_amount_credit != undefined) {
                        var dr_amt = parseFloat($scope.block2.gldd_doc_amount_credit)
                        if (dr_amt > 0) {
                            $scope.block2.gldd_doc_amount_debit = 0;
                            $scope.dvalue = true;
                            $scope.cvalue = false;
                        }
                        else {
                            $scope.block2.gldd_doc_amount_debit = 0;
                            $scope.cvalue = false;
                            $scope.dvalue = false;

                        }
                    }
                    else {
                        $scope.block2.gldd_doc_amount_debit = 0;
                        $scope.cvalue = false;
                        $scope.dvalue = false;

                    }
                }
            }

            //Authorize & verify Single Voucher
            $scope.JVerify_click = function () {
                jver = true;
                jvaut = false;
                $scope.Insert_temp_docs();
            }

            $scope.JVAuthorize_click = function () {
                jvaut = true;
                jver = false;
                $scope.Insert_temp_docs();
            }

            //Insert  Temp Doc
            $scope.Insert_temp_docs = function () {

                if ($scope.temp_doc.gltd_doc_code != "" && $scope.temp_doc.gltd_doc_code != undefined) {

                    if ($scope.temp_doc.gltd_post_date == "" || $scope.temp_doc.gltd_post_date == undefined) {
                        swal({ text: "  please select Post Date ", width: 380 });
                        return;
                    }
                    if ($scope.prdocdata.length <= 0) {
                        swal({ text: "  Please Add Transaction Details ", width: 380 });
                        return;
                    }
                    if ($scope.totalDebit != $scope.totalcredit) {
                        swal({ text: "Debit Amount: " + $scope.totalDebit + "  Is Not Equal To Credit Amount: " + $scope.totalcredit, width: 380 });
                        return;
                    }
                    if (($scope.temp_doc.chk_recur == true) && ($scope.temp_doc.gltd_recur_period == "0" || $scope.temp_doc.gltd_recur_period == "" || $scope.temp_doc.gltd_recur_period == undefined)) {
                        swal({ text: "Please Enter Recurring Period", width: 380 });
                        return;
                    }
                    else {
                        var datasend = [];
                        
                        if (jvaut == true) {
                            status = "Aut";
                        }
                        else if (jver == true) {
                            status = "Verify";
                        }
                        else
                            status = "Save";

                        if ($scope.temp == undefined) {
                            $scope.temp = "";
                        }
                        if ($scope.temp_doc.chk_recur == true) {
                            $scope.chk_rcur = "R";
                        }
                        var data = {
                            gltd_comp_code: comp_code,
                            gltd_prepare_user: user,
                            gltd_doc_code: $scope.temp_doc.gltd_doc_code,
                            gltd_doc_date: $scope.temp_doc.gltd_doc_date,
                            gltd_cur_status: status,
                            gltd_post_date: $scope.temp_doc.gltd_post_date,
                            gltd_doc_narr: $scope.temp_doc.gltd_doc_narr,
                            gltd_remarks: $scope.temp_doc.gltd_remarks,
                            gltd_prepare_date: $scope.temp_doc.gltd_doc_date,
                            gltd_final_doc_no: "0",
                            gltd_verify_user: (jver == true) ? user : null,
                            gltd_verify_date: (jver == true) ? $scope.temp_doc.gltd_doc_date : null,
                            gltd_authorize_user: (jvaut == true) ? user : null,
                            gltd_authorize_date: (jvaut == true) ? $scope.temp_doc.gltd_doc_date : null,
                            gltd_paid_to: $scope.block1.gltd_paid_to,
                            gltd_cheque_no: $scope.block1.gltd_cheque_No,
                            gltd_batch_source: $scope.chk_rcur,
                            gltd_payment_date: $scope.block1.gltd_payment_date,
                        }
                        datasend.push(data);
                        
                        $http.post(ENV.apiUrl + "api/BankPayment/Insert_Fins_temp_docs", datasend).then(function (msg) {
                            $scope.prvno = msg.data;

                            if ($scope.prvno != "" && $scope.prvno != null) {
                                $scope.Insert_Fins_temp_doc_details();
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not inserted. " + $scope.prvno, width: 300, height: 200 });
                            }
                        });
                    }
                }
                else {
                    swal({ text: "  please select Doc Code ", width: 380 });
                }
            }

            //Insert  Temp Doc Details

            $scope.Insert_Fins_temp_doc_details = function () {
                
                var dataSend = [];
                var j = 1;

                for (var i = 0; i < $scope.prdocdata.length; i++) {
                    var data = {
                        gldd_doc_narr: $scope.prdocdata[i].gldd_doc_narr,
                        gldd_dept_code: $scope.prdocdata[i].gldd_dept_code,
                        gldd_ledger_code: $scope.prdocdata[i].gldd_ledger_code,
                        gldd_acct_code: $scope.prdocdata[i].gldd_acct_code,
                        gldd_party_ref_no: $scope.prdocdata[i].gldd_party_ref_no,
                        gldd_party_ref_date: $scope.prdocdata[i].ptyref_date,
                        gldd_cost_center_code: $scope.prdocdata[i].glco_cost_centre_code,
                        gldd_doc_amount_debit: $scope.prdocdata[i].gldd_doc_amount_debit,
                        gldd_doc_amount_credit: $scope.prdocdata[i].gldd_doc_amount_credit,
                        gldd_comp_code: $scope.prdocdata[i].gldd_comp_code,
                        gldd_doc_code: $scope.temp_doc.gltd_doc_code,
                        gldd_prov_doc_no: $scope.prvno,
                        gldd_final_doc_no: "0",
                        gldd_line_no: j,

                    }
                    j++;
                    dataSend.push(data);

                }
                

                $http.post(ENV.apiUrl + "api/BankPayment/Insert_JV_Fins_temp_doc_details", dataSend).then(function (msg) {
                    
                    $scope.msg1 = msg.data;
                    if ($scope.prvno != "" && $scope.msg1 == true) {
                        if ($scope.vis.update == true) {
                            swal({ title: "Alert", text: "Document Updated Successfully\nProvision No.:" + $scope.temp_doc.gltd_doc_code + "-" + $scope.prvno, width: 300, height: 200 });
                            //$scope.checkprintbtn = true;
                        }
                        else {
                            if (($scope.temp_doc.chk_recur == true) && ($scope.temp_doc.gltd_doc_code != undefined) && ($scope.prvno != undefined || $scope.prvno != "")) {

                                
                                $http.post(ENV.apiUrl + "api/JVCreation/Recurring_Fins_temp_doc_details?comp_cd=" + comp_code + "&doccd=" + $scope.temp_doc.gltd_doc_code + "&prv_no=" + $scope.prvno + "&recur_period=" + $scope.temp_doc.gltd_recur_period + "&recur_days=" + $scope.temp_doc.gltd_recur_period_days).then(function (msg) {
                                    $scope.msg1 = msg.data;

                                    if ($scope.msg1 == true) {

                                        if (jvaut == true) {
                                            $http.post(ENV.apiUrl + "api/BankPayment/Authorize_Posting?comp_code=" + comp_code + "&doc_code=" + $scope.temp_doc.gltd_doc_code + "&prv_no=" + $scope.prvno + "&auth_user=" + user + "&auth_date=" + $scope.temp_doc.gltd_doc_date).then(function (Auth) {

                                                $scope.Docno = Auth.data;
                                                if ($scope.Docno != "") {
                                                    swal({ title: "Alert", text: "Voucher Posted Successfully\nFinal Doc No.: " + $scope.Docno, width: 300, height: 200 });

                                                    var data = {
                                                        location: $scope.final_doc_url,
                                                        parameter: { comp_detail: comp_code, doc: $scope.temp_doc.gltd_doc_code + $scope.Docno },
                                                        state: 'main.Fin150'
                                                    }

                                                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                                                    $state.go('main.ReportCardParameter')

                                                }
                                                else {
                                                    swal({ title: "Alert", text: "Posting Failed...!\nDocument Created Successfully\nProvision No.: " + $scope.temp_doc.gltd_doc_code + "-" + $scope.prvno, width: 300, height: 200 });
                                                }
                                            });
                                        }
                                        else if (jver == true) {
                                            swal({ title: "Alert", text: "Document Verified Successfully\nProvision No.:" + $scope.temp_doc.gltd_doc_code + "-" + $scope.prvno, width: 300, height: 200 });
                                            $scope.clear();
                                            return;
                                        }
                                        else {
                                            swal({ title: "Alert", text: "Document Created Successfully\nProvision No.:" + $scope.temp_doc.gltd_doc_code + "-" + $scope.prvno, width: 300, height: 200 });
                                        }
                                    }
                                    else {
                                        swal({ title: "Alert", text: "All Document Not Created Successfully\nProvision No.:" + $scope.temp_doc.gltd_doc_code + "-" + $scope.prvno, width: 300, height: 200 });
                                    }
                                });
                            }
                            else {
                                if (jvaut == true) {
                                    $http.post(ENV.apiUrl + "api/BankPayment/Authorize_Posting?comp_code=" + comp_code + "&doc_code=" + $scope.temp_doc.gltd_doc_code + "&prv_no=" + $scope.prvno + "&auth_user=" + user + "&auth_date=" + $scope.temp_doc.gltd_doc_date).then(function (Auth) {

                                        $scope.Docno = Auth.data;
                                        if ($scope.Docno != "") {
                                            swal({ title: "Alert", text: "Voucher Posted Successfully\nFinal Doc No.: " + $scope.Docno, width: 300, height: 200 });

                                            var data = {
                                                location: $scope.final_doc_url,
                                                parameter: { comp_detail: comp_code, doc: $scope.temp_doc.gltd_doc_code + $scope.Docno },
                                                state: 'main.Fin150'
                                            }

                                            window.localStorage["ReportDetails"] = JSON.stringify(data)
                                            $state.go('main.ReportCardParameter')
                                        }
                                        else {
                                            swal({ title: "Alert", text: "Posting Failed...!\nDocument Created Successfully\nProvision No.: " + $scope.temp_doc.gltd_doc_code + "-" + $scope.prvno, width: 300, height: 200 });
                                        }
                                    });
                                }
                                else if (jver == true) {
                                    swal({ title: "Alert", text: "Document Verified Successfully\nProvision No.:" + $scope.temp_doc.gltd_doc_code + "-" + $scope.prvno, width: 300, height: 200 });
                                    $scope.clear();
                                    return;
                                }
                                else {
                                    swal({ title: "Alert", text: "Document Created Successfully\nProvision No.:" + $scope.temp_doc.gltd_doc_code + "-" + $scope.prvno, width: 300, height: 200 });
                                }
                            }


                        }

                        if (jvaut == true) {


                        }
                        else {
                            $scope.vis.ok = false;
                            $scope.checkprintbtn = true;
                            $scope.vis.update = false;
                            $scope.vis.SendToVerify = true;

                            $scope.vis.autoriz = false;
                            $scope.vis.very = false;
                        }
                    }
                    else {

                        swal({ title: "Alert", text: "Record Not inserted in doc details. " , width: 300, height: 200 });
                    }

                });

            }

            //send to verify

            $scope.UpdateVerify = function () {
                if (($scope.block1.gltd_doc_code != undefined) && ($scope.block1.gltd_prov_doc_no != undefined)) {

                    if ($scope.temp_doc.gltd_doc_code == "" && $scope.temp_doc.gltd_doc_code == undefined) {
                        swal({ text: "Voucher Data Not Loaded", width: 380 });
                        return;
                    }
                    if ($scope.prdocdata.length <= 0) {
                        swal({ text: "  Please Add Transaction Details ", width: 380 });
                        return;
                    }
                    var data = {
                        gltd_comp_code: comp_code,
                        gltd_prepare_user: user,
                        gltd_doc_code: $scope.block1.gltd_doc_code,
                        gltd_prov_doc_no: $scope.block1.gltd_prov_doc_no,
                        gltd_verify_user: user,
                        gltd_verify_date: dd + '-' + mm + '-' + yyyy
                    }
                    
                    $http.post(ENV.apiUrl + "api/JVCreation/UpdateFins_temp_docs_current_status_sendtoverify", data).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Document Verified Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Document Not Verified Successfully. " , width: 300, height: 200 });
                        }
                    });
                }
                else if (($scope.temp_doc.gltd_doc_code != undefined) && ($scope.prvno != undefined)) {
                    var data = {
                        gltd_comp_code: comp_code,
                        gltd_prepare_user: user,
                        gltd_doc_code: $scope.temp_doc.gltd_doc_code,
                        gltd_prov_doc_no: $scope.prvno,
                        gltd_verify_user: user,
                        gltd_verify_date: dd + '-' + mm + '-' + yyyy
                    }
                    
                    $http.post(ENV.apiUrl + "api/JVCreation/UpdateFins_temp_docs_current_status_sendtoverify", data).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Document Verified Successfully", width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Document Not Verified Successfully. " , width: 300, height: 200 });
                        }
                    });
                }

            }

            $scope.Verify_data = function () {
                var dataSend = [];
                var vcnt = 0, rcnt = 0;

                

                for (var i = 0; i < $scope.verify_data.length; i++) {

                    if ($scope.verify_data[i].gltd_verify == true || $scope.verify_data[i].gltd_revert == true) {

                        if ($scope.verify_data[i].gltd_verify == true) {
                            vcnt = vcnt + 1;
                        }
                        else if ($scope.verify_data[i].gltd_revert == true) {
                            rcnt = rcnt + 1;
                        }

                        var data = {
                            gltd_comp_code: $scope.verify_data[i].gltd_comp_code,
                            gltd_doc_code: $scope.verify_data[i].gltd_doc_code,
                            gltd_prov_doc_no: $scope.verify_data[i].gltd_prov_doc_no,
                            gltd_verify: $scope.verify_data[i].gltd_verify,
                            gltd_revert: $scope.verify_data[i].gltd_revert,
                            gltd_verify_user: user,
                            gltd_verify_date: dd + '-' + mm + '-' + yyyy

                        }
                        dataSend.push(data);
                    }

                }


                $http.post(ENV.apiUrl + "api/JVCreation/UpdateFinn140_temp_docs", dataSend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true && vcnt > 0) {
                        swal({ title: "Alert", text: "Verified Successfully", width: 300, height: 200 });
                        // $scope.checkprintbtn = true;
                        $scope.Verify();

                    }
                    else if ($scope.msg1 == true && rcnt > 0) {
                        swal({ title: "Alert", text: "Reverted Successfully", width: 300, height: 200 });
                        // $scope.checkprintbtn = true;
                        $scope.Verify();

                    }
                    else {
                        swal({ title: "Alert", text: "Documents Not Verified. ", width: 300, height: 200 });
                    }

                });
            }

            $scope.Authorize_click = function () {
                debugger;
                var dataSend = [];
                var acnt = 0, arcnt = 0;

                $scope.doc_cd_prov_no = '';
                
                for (var i = 0; i < $scope.Authorize_data.length; i++) {

                    if ($scope.Authorize_data[i].gltd_authorize == true || $scope.Authorize_data[i].gltd_verify == true) {


                        if ($scope.Authorize_data[i].gltd_authorize == true) {
                            acnt = acnt + 1;
                        }
                        else if ($scope.Authorize_data[i].gltd_verify == true) {
                            arcnt = arcnt + 1;
                        }

                        var data = {
                            gltd_comp_code: $scope.Authorize_data[i].gltd_comp_code,
                            gltd_doc_code: $scope.Authorize_data[i].gltd_doc_code,
                            gltd_prov_doc_no: $scope.Authorize_data[i].gltd_prov_doc_no,
                            gltd_verify: $scope.Authorize_data[i].gltd_verify,
                            gltd_authorize: $scope.Authorize_data[i].gltd_authorize,
                            gltd_authorize_user: user,
                            gltd_authorize_date: dd + '-' + mm + '-' + yyyy

                        }
                        dataSend.push(data);
                        if ($scope.Authorize_data[i].gltd_authorize == true) {
                            $scope.doc_cd_prov_no = $scope.doc_cd_prov_no + $scope.Authorize_data[i].gltd_doc_code + $scope.Authorize_data[i].gltd_prov_doc_no + ',';
                        }
                    }

                }


                $http.post(ENV.apiUrl + "api/JVCreation/UpdateFinn142_temp_docs", dataSend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true && acnt > 0) {
                        swal({ title: "Alert", text: "Authorized Successfully", width: 300, height: 200 });
                        $('#vouchermodal').modal('show');
                        $scope.voucher_report_show = false;

                        $http.get(ENV.apiUrl + "api/JVCreation/Getdoc_cd_prov_no?doc_prov_no=" + $scope.doc_cd_prov_no + "&comp_code=" + comp_code).then(function (final_doc_no) {
                            $scope.doc_cd_final_doc_no = final_doc_no.data;
                            
                            //var data = {
                            //    location: $scope.final_doc_url,
                            //    parameter: { doc: $scope.doc_cd_final_doc_no },
                            //    state: 'main.Fin150'
                            //}

                            var data = {
                                location: $scope.final_doc_url,
                                parameter: { comp_detail: comp_code, doc: $scope.doc_cd_final_doc_no },
                                state: 'main.Fin150'
                            }

                            window.localStorage["ReportDetails"] = JSON.stringify(data);
                            try {
                                $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                                $scope.location = $scope.rpt.location;
                                $scope.parameter = $scope.rpt.parameter;
                                $scope.state = $scope.rpt.state;
                            }
                            catch (ex) {
                            }
                            var s;
                            s = "SimsReports." + $scope.location + ",SimsReports";
                            var url = window.location.href;
                            var domain = url.substring(0, url.indexOf(':'))
                            if ($http.defaults.headers.common['schoolId'] == 'sms') {
                                if (domain == 'https')
                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                                else
                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                            }
                            else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc') {
                                if (domain == 'https')
                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                                else
                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                            }

                            else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                                if (domain == 'https')
                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                                else
                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                            }
                            else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                                if (domain == 'https')
                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                                else
                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                            }

                            else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                                if (domain == 'https')
                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                                else
                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                            }

                            else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                                if (domain == 'https')
                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                                else
                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                            }
                            else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                                if (domain == 'https')
                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                                else
                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                            }

                            else {
                                if (domain == 'https')
                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                                else
                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                            }
                            console.log(service_url);
                            $scope.parameters = {}

                            $("#reportViewer2")
                                           .telerik_ReportViewer({
                                               serviceUrl: service_url,
                                               viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                               scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                               // Zoom in and out the report using the scale
                                               // 1.0 is equal to 100%, i.e. the original size of the report
                                               scale: 1.0,
                                               ready: function () {
                                                   //this.refreshReport();
                                               }

                                           });
                            var reportViewer = $("#reportViewer2").data("telerik_ReportViewer");
                            reportViewer.reportSource({
                                report: s,
                                parameters: $scope.parameter,
                            });

                            setInterval(function () {

                                $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                                $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                            }, 1000);

                            $timeout(function () {
                                $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                                $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                            }, 100)

                            //$state.go('main.ReportCardParameter')
                            // sims.Controls.Childwindow.AdmRptViewer jvrpt = new Controls.Childwindow.AdmRptViewer($scope.doc_cd_final_doc_no,true);
                            //jvrpt.Show();
                            //Report Name:-FinR19
                        });
                            
                        $scope.Authorize();

                    }
                    else if ($scope.msg1 == true && arcnt > 0) {
                        swal({ title: "Alert", text: "Reverted Successfully", width: 300, height: 200 });

                        $scope.Authorize();

                    }
                    else {
                        swal({ title: "Alert", text: "Documents Not Authorized. ", width: 300, height: 200 });
                    }

                });
            }


            $scope.coad_pty_full_name = $scope.selectedText;
            $scope.JVCreates = [];


            $scope.Delete_Grid = function (item, $event, index, str) {

                
                $scope.totalDebit =  parseFloat(parseFloat($scope.totalDebit) - parseFloat(str.gldd_doc_amount_debit)).toFixed(3);
                $scope.totalcredit = parseFloat(parseFloat($scope.totalcredit) - parseFloat(str.gldd_doc_amount_credit)).toFixed(3);

                item.splice(index, 1);


            }

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.temp_doc = {
                gltd_doc_date: dd + '-' + mm + '-' + yyyy,
                gltd_post_date: dd + '-' + mm + '-' + yyyy,
                ChequeDate: dd + '-' + mm + '-' + yyyy,
            }

            $scope.myFunct = function (keyEvent) {

                if (keyEvent.which == 13)
                    $scope.temp_doc.gltd_post_date = dd + '-' + mm + '-' + yyyy;
            }

            $scope.myFunct1 = function (keyEvent) {

                if (keyEvent.which == 13)
                    $scope.temp.ChequeDate = dd + '-' + mm + '-' + yyyy;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd-mm-yyyy'
        });

    $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
        $('input[type="text"]', $(this).parent()).focus();
    });

    $scope.FilterCode = function () {

        if ($scope.block1.chk_previous_clear == true) {

            for (var i = 0; i < $scope.verify_data.length; i++) {
                $scope.verify_data[i].gltd_revert = false;
                $scope.verify_data[i].gltd_verify = false;
            }
        }
        if ($scope.edt.SelectOption == 'Select Verify Records') {

            //if ($scope.verify_data.length >= $scope.edt.txt_filter_range) {
            for (var i = 0; i < $scope.edt.txt_filter_range; i++) {
                $scope.verify_data[i].gltd_verify = true;
                $scope.verify_data[i].gltd_revert = false;

            }
            //}
        }
        else if ($scope.edt.SelectOption == 'Select Revert Records') {

            // if ($scope.verify_data.length >= $scope.edt.txt_filter_range) {
            for (var i = 0; i < $scope.edt.txt_filter_range; i++) {
                $scope.verify_data[i].gltd_revert = true;
                $scope.verify_data[i].gltd_verify = false;

            }
            //}
        }
    }

    $scope.FilterCode_aut = function () {
        

        if ($scope.block1.chk_previous_clear_aut == true) {

            for (var i = 0; i < $scope.Authorize_data.length; i++) {
                $scope.Authorize_data[i].gltd_authorize = false;
                $scope.Authorize_data[i].gltd_verify = false;
            }
        }
        if ($scope.edt.SelectOption_aut == 'Select Revert Records') {

            //if ($scope.Authorize_data.length >= $scope.edt.txt_filter_range_aut) {
            for (var i = 0; i < $scope.edt.txt_filter_range_aut; i++) {
                $scope.Authorize_data[i].gltd_verify = true;
                $scope.Authorize_data[i].gltd_authorize = false;
            }
            //}
        }
        else if ($scope.edt.SelectOption_aut == 'Select Authorize Records') {

            //if ($scope.Authorize_data.length >= $scope.edt.txt_filter_range_aut) {
            for (var i = 0; i < $scope.edt.txt_filter_range_aut; i++) {
                $scope.Authorize_data[i].gltd_authorize = true;
                $scope.Authorize_data[i].gltd_verify = false;

            }
            //}
        }
    }
    $scope.docCode1;
    $scope.provNo1;
    $scope.backModal = function () {

        $scope.report_show = true;

    }
    $scope.closemodal = function () {

        $scope.report_show = true;
    }
    $scope.report_show = true;
    $scope.CheckPrintReport = function () {
        $scope.report_show = false;
        var data = {
            location: $scope.prov_doc_url,
            parameter: {
                comp_detail: comp_code,
                doc_code: $scope.docCode1,
                doc_no: $scope.provNo1,

            },

            state: 'main.Fin150'
        }
        window.localStorage["ReportDetails"] = JSON.stringify(data)
       // $state.go('main.ReportCardParameter')

        try {
            $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

            $scope.location = $scope.rpt.location;
            $scope.parameter = $scope.rpt.parameter;
            $scope.state = $scope.rpt.state;
        }
        catch (ex) {
        }
        var s;

        //if ($http.defaults.headers.common['schoolId'] == 'dpsmis')
        //    s = "SimsReports." + $scope.location + ",SimsReports";


        //else
        //    s = "SimsReports." + $scope.location + ",SimsReports" + $http.defaults.headers.common['schoolId'];


        //  window.localStorage["Finn_comp"] = JSON.stringify(data)

        s = "SimsReports." + $scope.location + ",SimsReports";

        var url = window.location.href;
        var domain = url.substring(0, url.indexOf(':'))
        if ($http.defaults.headers.common['schoolId'] == 'sms') {
            if (domain == 'https')
                var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
            else
                var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

        }
        else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc' || $http.defaults.headers.common['schoolId'] == 'christ') {
            if (domain == 'https')
                var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
            else
                var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
        }

        else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
            if (domain == 'https')
                var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
            else
                var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
        }

        else if ($http.defaults.headers.common['schoolId'] == 'imert') {
            if (domain == 'https')
                var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
            else
                var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
        }
        else if ($http.defaults.headers.common['schoolId'] == 'portal') {
            if (domain == 'https')
                var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
            else
                var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
        }

        else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
            if (domain == 'https')
                var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
            else
                var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
        }

        else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
            if (domain == 'https')
                var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
            else
                var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
        }
        else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
            if (domain == 'https')
                var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
            else
                var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
        }

        else {
            if (domain == 'https')
                var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
            else
                var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
        }
        // console.log(service_url);


        $scope.parameters = {}

        $("#reportViewer1")
                       .telerik_ReportViewer({
                           //serviceUrl: ENV.apiUrl + "api/reports/",
                           serviceUrl: service_url,

                           viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                           scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                           // Zoom in and out the report using the scale
                           // 1.0 is equal to 100%, i.e. the original size of the report
                           scale: 1.0,
                           ready: function () {
                               //this.refreshReport();
                           }

                       });





        var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
        reportViewer.reportSource({
            report: s,
            parameters: $scope.parameter,
        });

        //var rv = $("#reportViewer1").data("telerik_ReportViewer");
        //rv.commands.print.exec();

        setInterval(function () {

            $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
            $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

            //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
            //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
            //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
            //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
            //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
            //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });

        }, 1000);


        $timeout(function () {
            $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
            $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
            //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
            //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
            //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
            //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
            //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
            //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });


        }, 100)


    }
   

    $scope.PrintReport = function () {
       
        var data = {
            location: $scope.prov_doc_url,
            parameter: {
                comp_detail: comp_code,
                doc_code: $scope.temp_doc.gltd_doc_code,
                doc_no: $scope.prvno,

            },

            state: 'main.Fin150'
        }
        window.localStorage["ReportDetails"] = JSON.stringify(data)
        $state.go('main.ReportCardParameter')


    }

    $scope.Fetchdata = function (cmp_cd, docd, prov_n) {
        $scope.docCode1 = docd;
        $scope.provNo1 = prov_n;
        
        $('#myModal4').modal('show')


        $http.get(ENV.apiUrl + "api/JVCreation/getAllRecords?doccode=" + docd + "&prnumber=" + prov_n + "&comp_code=" + cmp_cd).then(function (prdoc_data) {

            $scope.prdocdata = prdoc_data.data;
            $scope.block1 = [];

            $scope.totalDebit = 0;
            $scope.totalcredit = 0;

            for (var i = 0; i < $scope.prdocdata.length; i++) {

                if (i == 0) {
                    $scope.temp_doc.gltd_comp_code = $scope.prdocdata[i].gltd_comp_code;
                    $scope.temp_doc.gltd_doc_code = $scope.prdocdata[i].gltd_doc_code;
                    $scope.temp_doc.gldc_doc_short_name = $scope.prdocdata[i].gldc_doc_short_name;
                    $scope.temp_doc.gltd_doc_narr = $scope.prdocdata[i].gltd_doc_narr;
                    $scope.temp_doc.gltd_remarks = $scope.prdocdata[i].gltd_remarks;
                    $scope.temp_doc.gltd_doc_date = $scope.prdocdata[i].gltd_doc_date;
                    $scope.temp_doc.gltd_post_date = $scope.prdocdata[i].gltd_post_date;
                    $scope.temp_doc.gltd_prov_doc_no = prov_n;

                    $scope.temp_doc.gldu_authorize = $scope.prdocdata[i].gldu_authorize;
                    $scope.temp_doc.gldu_verify = $scope.prdocdata[i].gldu_verify;
                    $scope.temp_doc.gltd_authorize_date = $scope.prdocdata[i].gltd_authorize_date;
                    $scope.temp_doc.gltd_verify_date = $scope.prdocdata[i].gltd_verify_date;

                }
                $scope.cost = $scope.cost || ($scope.prdocdata[i].gldd_cost_center_code != '' || $scope.prdocdata[i].gldd_cost_center_code != "");

                $scope.totalDebit = parseFloat(parseFloat($scope.totalDebit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_debit)).toFixed(3);
                $scope.totalcredit = parseFloat(parseFloat($scope.totalcredit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_credit)).toFixed(3);

                

            }

        });

    }


    $scope.check_post_date = function () {
        if ($scope.temp_doc.gltd_post_date != undefined || $scope.temp_doc.gltd_post_date != '') {

            $http.get(ENV.apiUrl + "api/JVCreation/Get_Check_Postdate?post_date=" + $scope.temp_doc.gltd_post_date + "&comp_cd=" + comp_code + "&fyear=" + finance_year).then(function (pronum) {
                $scope.msg = pronum.data;

                if ($scope.msg != '') {
                    swal({ title: "Alert", text: $scope.msg, width: 300, height: 200 });

                    $scope.vis.ok = false;
                    $scope.vis.autoriz = false;
                    $scope.vis.very = false;
                }
                else {
                    if ($scope.users.length != 0) {
                        
                        if ($scope.users[0] == user && $scope.users[1] == user) {
                            
                            $scope.vis.ok = true;
                            $scope.vis.autoriz = true;
                            $scope.vis.very = true;
                        }
                        else if ($scope.users[0] == user && ($scope.users[1] == "" || $scope.users[1] != user)) {

                            $scope.vis.ok = true;
                            $scope.vis.autoriz = false;
                            $scope.vis.very = true;
                        }
                        else {
                            $scope.vis.ok = true;
                            $scope.vis.autoriz = false;
                            $scope.vis.very = false;
                        }
                    }
                }

            });

        }

    }

}]
        )
})();
