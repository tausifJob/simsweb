﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('AssetMasterTransactionCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter,ENV) {


            $scope.assetmaster_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.dept_comp_code;
            $scope.slma_acno_dept_no;
            $scope.finyr;
            $scope.valstatus = false;

            $scope.edit_data = false;
            $scope.display = false;
            $scope.pagesize = "5";
            $scope.pageindex = "1";
            $scope.grid = true;
            $scope.grid1 = false;
            $scope.ifedit = false;
            $scope.tr_status = '1';
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
                debugger;
                $scope.comp_data = res.data;
                console.log($scope.comp_data);
                $scope.dept_comp_code = $scope.comp_data[0].dept_comp_code;

                $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetCurrentFinancialYear").then(function (res) {
                    debugger;
                    $scope.finyr_data = res.data;
                    $scope.finyr = $scope.finyr_data;

                    $http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetAllDepartmentName?comp_code=" + $scope.dept_comp_code + "&fins_year=" + $scope.finyr).then(function (res) {
                        $scope.dept_data = res.data;
                    });
                });
            });


            $http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetAutoGenerate_AssetItemNumber").then(function (res) {
                $scope.auto_data = res.data;

            });

            //Custodian Change
            $scope.Custodian1 = [];
            setTimeout(function () {
                $("#cmb_cust").select2();
            }, 100);
            //Custodian Change
            $http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetCustodianEmployees").then(function (res122) {
                debugger;
                $scope.Custodian1 = res122.data;
            });

            $http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetAssetType").then(function (res) {
                $scope.assetType_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetAllLocationName").then(function (res) {
                $scope.location_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetAllSupplierNames").then(function (res) {
                $scope.sup_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetAllOrderNo").then(function (res) {
                $scope.order_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetAssetStatus").then(function (res) {
                $scope.status_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/AssetCustodian/Getemployee_name").then(function (Geemployee_name) {
                $scope.Geemployeename = Geemployee_name.data;
            });

            $scope.getAccountNo = function (asset_type) {
                if (asset_type != null) {
                    //$http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetAccountNo?asset_type=" + asset_type).then(function (res) {
                    //    $scope.AccountNo_data = res.data;
                    //    console.log($scope.AccountNo_data);
                    //    for (var i = 0; i < $scope.AccountNo_data.length; i++) {
                    //        $scope.edt.gam_acct_code = $scope.AccountNo_data[0].gam_acct_code;
                    //        edt.gam_deprn_percent
                    //    }
                    //});

                    for (var i = 0; i < $scope.assetType_data.length; i++) {
                        if ($scope.assetType_data[i].gam_asst_type == asset_type) {

                            $scope.edt.gam_acct_code = $scope.assetType_data[i].gam_acct_code;
                            $scope.edt.gam_deprn_percent = $scope.assetType_data[i].gam_deprn_percent;


                        }
                    }

                }
                else {
                    $scope.edt.gam_acct_code = '';
                    $scope.edt.gam_deprn_percent = '';
                }
            }

            $scope.search_type = function (type) {
                $scope.tr_status = '1';

                $http.get(ENV.apiUrl + "api/AssetMasterTransaction/GetFins_AssetMaster?type=" + $scope.edt.gam_asst_type + "&gam_items=" + $scope.edt.gam_item_no).then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.grid1 = true;
                    $scope.assetmaster_data = res.data;
                    $scope.totalItems = $scope.assetmaster_data.length;
                    $scope.todos = $scope.assetmaster_data;
                    $scope.makeTodos();
                    if (res.data.length == 0) {
                        swal({  text: "Sorry!! No Data Found", showCloseButton: true, width: 380, });
                        $scope.grid1 = false;
                    }
                });                
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edit = function (str) {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edit_data = true;
                $scope.ifedit = true;
                debugger;

                $scope.edt =
                    {
                        gam_comp_code: $scope.dept_comp_code,
                        financialyear: $scope.finyr,
                        gam_dept_code: str.gam_dept_code,
                        gam_asst_type: str.gam_asst_type,
                        gam_item_no: str.gam_item_no,
                        gam_location: str.gam_location,
                        gam_desc_1: str.gam_desc_1,
                        gam_desc_2: str.gam_desc_2,
                        gam_supl_code: str.gam_supl_code,
                        gam_acct_code: str.gam_acct_code,
                        gam_order_no: str.gam_order_no,
                        gam_invoice_no: str.gam_invoice_no,
                        gam_new_sec: str.gam_new_sec,
                        gam_page_no: str.gam_page_no,
                        gam_line_no: str.gam_line_no,
                        gam_quantity: str.gam_quantity,
                        gam_invoice_amount: str.gam_invoice_amount,
                        gam_val_add_cum: str.gam_val_add_cum,
                        gam_val_add_mtd: str.gam_val_add_mtd,
                        gam_val_add_ytd: str.gam_val_add_ytd,
                        gam_book_value: str.gam_book_value,
                        gam_cum_deprn: str.gam_cum_deprn,
                        gam_ytd_deprn: str.gam_ytd_deprn,
                        gam_mth_deprn: str.gam_mth_deprn,
                        gam_sale_value: str.gam_sale_value,
                        gam_repl_cost: str.gam_repl_cost,
                        gam_receipt_date: convertdate(str.gam_receipt_date),
                        gam_sale_date: convertdate(str.gam_sale_date),
                        gam_entry_date: convertdate(str.gam_entry_date),
                        gam_amend_date: convertdate(str.gam_amend_date),
                        gam_deprn_percent: str.gam_deprn_percent,
                        gam_used_on_item: str.gam_used_on_item,
                        gam_status_code: str.gam_status_code,
                        empcode: str.empcode,
                    }

                var s = $scope.edt.empcode;
                $("#cmb_sup").select2("val", str.empcode);
            }

            $scope.New = function () {
                $scope.ifedit = false;
                var autoid;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edit_data = false;
                $scope.edt = [];
                $scope.grid1 = false;
                $http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetAutoGenerate_AssetItemNumber").then(function (res) {
                    autoid = res.data;
                    $scope.edt['gam_item_no'] = autoid;
                });
            }

            function convertdate(dt) {
                if (dt != null) {
                    var d1 = new Date(dt);
                    var month = d1.getMonth() + 1;
                    var day = d1.getDate();
                    if (month < 10)
                        month = "0" + month;
                    if (day < 10)
                        day = "0" + day;
                    var d = d1.getFullYear() + "-" + (month) + "-" + (day);
                    console.log(d);
                    $scope.convertdated = d;
                    return d;
                }
            }

            $scope.Save = function (isvalidate) {
                var data1 = [];
                // if (isvalidate) {
                if ($scope.update1 == false) {
                    var data = ({
                        gam_comp_code: $scope.dept_comp_code,
                        financialyear: $scope.finyr,
                        gam_dept_code: $scope.edt.gam_dept_code,
                        gam_asst_type: $scope.edt.gam_asst_type,
                        gam_item_no: $scope.edt.gam_item_no,
                        gam_location: $scope.edt.gam_location,
                        gam_desc_1: $scope.edt.gam_desc_1,
                        gam_desc_2: $scope.edt.gam_desc_2,
                        gam_supl_code: $scope.edt.gam_supl_code,
                        gam_acct_code: $scope.edt.gam_acct_code,
                        gam_order_no: $scope.edt.gam_order_no,
                        gam_invoice_no: $scope.edt.gam_invoice_no,
                        gam_new_sec: $scope.edt.gam_new_sec,
                        gam_page_no: $scope.edt.gam_page_no,
                        gam_line_no: $scope.edt.gam_line_no,
                        gam_quantity: $scope.edt.gam_quantity,
                        gam_invoice_amount: $scope.edt.gam_invoice_amount,
                        gam_val_add_cum: $scope.edt.gam_val_add_cum,
                        gam_val_add_mtd: $scope.edt.gam_val_add_mtd,
                        gam_val_add_ytd: $scope.edt.gam_val_add_ytd,
                        gam_book_value: $scope.edt.gam_book_value,
                        gam_cum_deprn: $scope.edt.gam_cum_deprn,
                        gam_ytd_deprn: $scope.edt.gam_ytd_deprn,
                        gam_mth_deprn: $scope.edt.gam_mth_deprn,
                        gam_sale_value: $scope.edt.gam_sale_value,
                        gam_repl_cost: $scope.edt.gam_repl_cost,
                        gam_receipt_date: convertdate($scope.edt.gam_receipt_date),
                        gam_sale_date: convertdate($scope.edt.gam_sale_date),
                        gam_entry_date: convertdate($scope.edt.gam_entry_date),
                        gam_amend_date: convertdate($scope.edt.gam_amend_date),
                        gam_deprn_percent: $scope.edt.gam_deprn_percent,
                        gam_used_on_item: $scope.edt.gam_used_on_item,
                        gam_status_code: $scope.edt.gam_status_code,
                        empcode: $scope.edt.empcode,
                        opr: 'I',
                    });

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/common/AssetMaster_dpsd/CUDInsertFins_AssetMaster", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({  text: "Record Added Successfully",imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    //$scope.getgrid();
                                    $scope.grid1 = false;
                                }
                            });

                            $scope.edit_data = false;
                            $scope.display = false;
                            $scope.pagesize = "5";
                            $scope.pageindex = "1";
                            $scope.grid = true;
                            $scope.grid1 = false;
                            $scope.edt.gam_asst_type = '';
                            $scope.edt.gam_item_no = '';
                        }
                        else {
                            swal({  text: "Record Not Inserted. " + $scope.msg1,imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    //$scope.getgrid();
                                }
                            });
                        }
                    });
                }
                else {
                    $scope.Update(isvalidate);
                }
                // }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetFins_AssetMaster").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.assetmaster_data = res.data;
                    console.log($scope.assetmaster_data);
                    $scope.totalItems = $scope.assetmaster_data.length;
                    $scope.todos = $scope.assetmaster_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                });
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {
                debugger;
                var data1 = [];
                //if (isvalidate) {
                var data = ({
                    gam_comp_code: $scope.dept_comp_code,
                    financialyear: $scope.finyr,
                    gam_dept_code: $scope.edt.gam_dept_code,
                    gam_asst_type: $scope.edt.gam_asst_type,
                    gam_item_no: $scope.edt.gam_item_no,
                    gam_location: $scope.edt.gam_location,
                    gam_desc_1: $scope.edt.gam_desc_1,
                    gam_desc_2: $scope.edt.gam_desc_2,
                    gam_supl_code: $scope.edt.gam_supl_code,
                    gam_acct_code: $scope.edt.gam_acct_code,
                    gam_order_no: $scope.edt.gam_order_no,
                    gam_invoice_no: $scope.edt.gam_invoice_no,
                    gam_new_sec: $scope.edt.gam_new_sec,
                    gam_page_no: $scope.edt.gam_page_no,
                    gam_line_no: $scope.edt.gam_line_no,
                    gam_quantity: $scope.edt.gam_quantity,
                    gam_invoice_amount: $scope.edt.gam_invoice_amount,
                    gam_val_add_cum: $scope.edt.gam_val_add_cum,
                    gam_val_add_mtd: $scope.edt.gam_val_add_mtd,
                    gam_val_add_ytd: $scope.edt.gam_val_add_ytd,
                    gam_book_value: $scope.edt.gam_book_value,
                    gam_cum_deprn: $scope.edt.gam_cum_deprn,
                    gam_ytd_deprn: $scope.edt.gam_ytd_deprn,
                    gam_mth_deprn: $scope.edt.gam_mth_deprn,
                    gam_sale_value: $scope.edt.gam_sale_value,
                    gam_repl_cost: $scope.edt.gam_repl_cost,
                    gam_receipt_date: convertdate($scope.edt.gam_receipt_date),
                    gam_sale_date: convertdate($scope.edt.gam_sale_date),
                    gam_entry_date: convertdate($scope.edt.gam_entry_date),
                    gam_amend_date: convertdate($scope.edt.gam_amend_date),
                    gam_deprn_percent: $scope.edt.gam_deprn_percent,
                    gam_used_on_item: $scope.edt.gam_used_on_item,
                    gam_status_code: $scope.edt.gam_status_code,
                    empcode: $scope.edt.empcode,
                    opr: 'U',
                });

                data1.push(data);
                $http.post(ENV.apiUrl + "api/common/AssetMaster_dpsd/CUDUpdateFins_AssetMaster", data1).then(function (res) {
                    $scope.display = true;
                    $scope.msg1 = res.data;
                    if ($scope.msg1 == true) {
                        swal({  text: "Record Updated Successfully", imageUrl: "assets/img/check.png",showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                //$scope.getgrid();
                                $scope.grid1 = false;
                                $scope.edt.gam_asst_type = "";
                            }
                        });

                        $scope.edit_data = false;
                        $scope.display = false;
                        $scope.pagesize = "5";
                        $scope.pageindex = "1";
                        $scope.grid = true;
                        $scope.grid1 = false;
                        $scope.edt.gam_asst_type = '';
                        $scope.edt.gam_item_no = '';
                    }
                    else {
                        swal({  text: "Record Not Updated. " + $scope.msg1, imageUrl: "assets/img/close.png",showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                //$scope.getgrid();
                            }
                        });
                    }
                });
                //  }
            }

            $scope.AssetHistory = function () {

                $('#MyModal2').modal({ backdrop: 'static', keyboard: true });
            }

            $scope.removeItem = function (obj, index) {
                debugger;
                $scope.assetitems.splice(index, 1);
            }

            $scope.SearchbyDate = function (fromdate, todate) {

                $http.post(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetDepHistorDetails?fromdate='" + fromdate + "'&todate='" + todate + "'&gamitemno=" + $scope.edt.gam_item_no).then(function (res) {
                    $scope.GetItems = res.data;
                });
            }

            $scope.Reset = function () {

                $scope.search_from_date = '';
                $scope.search_to_date = '';
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.edt.gam_asst_type = '';
                $scope.edt.gam_item_no = '';
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Delete = function () {
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById('asset-' + i);

                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'gam_comp_code': $scope.dept_comp_code,
                            'gam_dept_code': $scope.filteredTodos[i].gam_dept_code,
                            'gam_item_no': $scope.filteredTodos[i].gam_item_no,
                            'gam_asst_type': $scope.filteredTodos[i].gam_asst_type,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/common/AssetMaster_dpsd/CUDUpdateFins_AssetMaster", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({  text: "Asset Master Deleted Successfully",imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        debugger;
                                        if (isConfirm) {
                                            //$scope.getgrid();
                                            $scope.grid1 = false;
                                            $scope.edt.gam_asst_type = "";

                                        }
                                    });

                                }
                                else {
                                    swal({  text: "Asset Master Not Deleted Successfully. " + $scope.msg1,imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            //$scope.getgrid();
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById('asset-' + i);
                                v.checked = false;
                                main.checked = false;
                                $scope.row1 = '';

                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById('asset-' + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }

                    });
                }
                else {
                    swal({  text: "Please Select Atleast One Record",imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('asset-' + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('asset-' + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
            }

            $scope.onlyNumbers = function (event, str) {

                if (str > 100) {
                    swal({  text: "Invalid Discount", width: 300, height: 200 });
                }

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $scope.getaccountsinfo = function () {
                $http.get(ENV.apiUrl + "api/AssetMasterTransaction/getaccounts?assettype=" + $scope.edt.gam_asst_type + "&compcode=1").then(function (res_account) {
                    debugger;
                    $scope.RevMaster = res_account.data;
                    $scope.DisMaster = res_account.data;
                });
            }

            $scope.SellItems = function (str) {
                $scope.assetitems = [];
                var assetitems1 = [];
                debugger;
                $scope.tr_status = '1';
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById('asset-' + i);

                    if (v.checked == true) {
                        $scope.flag = true;
                        var sell_item = ({
                            'gam_desc_1': $scope.filteredTodos[i].gam_desc_1,
                            'gam_item_no': $scope.filteredTodos[i].gam_item_no,
                            'gam_asst_type': $scope.filteredTodos[i].gam_asst_type,
                            'gam_transaction_code': '1',
                            'ast_qty': '',
                            'ast_emp': '',
                            'ast_amount': '',
                            'revacc': '',
                            'disacc': '',
                            'ast_remarks': ''
                        });
                        assetitems1.push(sell_item);
                        console.log(assetitems1);
                    }
                }

                $scope.assetitems = assetitems1;

                $scope.getaccountsinfo();

                $('#MyModal2').modal({ backdrop: 'static', keyboard: true });

            }

            $scope.DisposeItems = function (str) {
                $scope.assetitems = [];
                var assetitems1 = [];
                $scope.tr_status = '2';

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById('asset-' + i);

                    if (v.checked == true) {
                        $scope.flag = true;
                        var sell_item = ({
                            'gam_desc_1': $scope.filteredTodos[i].gam_desc_1,
                            'gam_item_no': $scope.filteredTodos[i].gam_item_no,
                            'gam_asst_type': $scope.filteredTodos[i].gam_asst_type,
                            'gam_transaction_code': '2',
                            'ast_qty': '',
                            'ast_emp': '',
                            'ast_amount': '',
                            'revacc': '',
                            'disacc': '',
                            'ast_remarks':''
                        });
                        assetitems1.push(sell_item);
                        console.log(assetitems1);
                    }
                }

                $scope.assetitems = assetitems1;

                $scope.getaccountsinfo();

                $('#MyModal2').modal({ backdrop: 'static', keyboard: true });

            }

            $scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');

            $scope.okbuttonclick1 = function () {
                debugger;
                var datasend = [];

                for (var i1 = 0; i1 < $scope.assetitems.length; i1++) {

                    if ($scope.assetitems[i1].ast_qty == 'undefined' || $scope.assetitems[i1].ast_emp == 'undefined' || $scope.assetitems[i1].ast_qty == '' || $scope.assetitems[i1].ast_emp == '') {

                        swal({  text: 'Quantity and Employee are mandatory.', width: 300, height: 200 });

                        return;

                    }

                    if ($scope.assetitems[i1].revacc == 'undefined' || $scope.assetitems[i1].revacc == '') {

                        swal({  text: 'Revenue Account for Selling Assets are mandatory.', width: 300, height: 200 });

                        return;
                    }                    
                }

                for (var i = 0; i < $scope.assetitems.length; i++) {

                    var revacc = '';
                    var disacc = '';

                    if ($scope.tr_status == '1') {
                        revacc = $scope.assetitems[i].revacc;
                        disacc = '';
                    }
                    else if ($scope.tr_status == '2') {
                        disacc = $scope.assetitems[i].revacc;
                        revacc = '';
                    }

                    var data = {
                        'opr': 'I',
                        'gam_item_no': $scope.assetitems[i].gam_item_no,
                        'gam_transaction_code': $scope.assetitems[i].gam_transaction_code,
                        'gam_quantity': $scope.assetitems[i].ast_qty,
                        'gam_sell_to': $scope.assetitems[i].ast_emp,
                        'gam_cust_code': $scope.assetitems[i].ast_emp,
                        'gam_transaction_amount': $scope.assetitems[i].ast_amount,
                        'gam_revenue_acno': revacc,
                        'gam_asset_disposal_acno': disacc,
                        'gam_created_by': $rootScope.globals.currentUser.username,
                        'gam_creater_remarks': $scope.assetitems[i].ast_remarks,
                        'gam_approved_by': '',
                        'gam_approved_date': $scope.ddMMyyyy,
                        'gam_approver_remarks': '',
                        'gam_transaction_status': '1',
                    }

                    datasend.push(data);
                }
                
                $http.post(ENV.apiUrl + "api/AssetMasterTransaction/CUDInsertFinsAssetMasterTransaction", datasend).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 = true) {
                        if ($scope.tr_status == '1')
                            swal({ text: 'Records Sold Successfully.', width: 300, height: 200, imageUrl: "assets/img/check.png" });
                        if ($scope.tr_status == '2')
                            swal({ text: 'Records Disposed Successfully.', width: 300, height: 200, imageUrl: "assets/img/close.png" });

                        $http.get(ENV.apiUrl + "api/AssetMasterTransaction/GetFins_AssetMaster?type=" + $scope.edt.gam_asst_type + "&gam_items=" + $scope.edt.gam_item_no).then(function (res) {
                            $scope.display = false;
                            $scope.grid = true;
                            $scope.grid1 = true;
                            $scope.assetmaster_data = res.data;
                            $scope.totalItems = $scope.assetmaster_data.length;
                            $scope.todos = $scope.assetmaster_data;
                            $scope.makeTodos();
                        });
                    }
                    else {
                        swal({  text: 'Error in Transaction.', width: 300, height: 200 });
                    }
                });
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }])
})();