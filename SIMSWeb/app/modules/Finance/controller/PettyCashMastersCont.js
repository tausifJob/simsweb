﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('PettyCashMastersCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.dept = false
            $scope.new_btn = true;
            $scope.account = false;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);
           
            setTimeout(function () {
                $("#AccountNo").select2();
            }, 100);

            var user = $rootScope.globals.currentUser.username;
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            $http.get(ENV.apiUrl + "api/BankMaster/getapiNewButtonData?login=" + user).then(function (getadmin_data) {
                $scope.get_allData_new = getadmin_data.data;
              
                if ($scope.get_allData_new.length > 0) {
                    $scope.new_btn = false;
                }
            });

            $scope.getAccounts=function(depart)
            {
                
                $http.get(ENV.apiUrl + "api/BankMaster/getAllAccNumber?code=" + comp_code + "&year=" + finance_year + "&dept=" + depart).then(function (docstatus2) {
                    $scope.departments = docstatus2.data;
                });
            }

            $http.get(ENV.apiUrl + "api/BankMaster/getAllDepartments?tbl_cond=" + comp_code).then(function (docstatus2) {
                $scope.Department = docstatus2.data;
            });

           


            $http.get(ENV.apiUrl + "api/BankMaster/getPettyCashDeatailsData").then(function (getAll_data) {
                $scope.get_allData = getAll_data.data;
                $scope.totalItems = $scope.get_allData.length;
                $scope.todos = $scope.get_allData;
                $scope.makeTodos();
            });


            $scope.max_limit_cal = function (max, min)
            {
                
                $scope.max_value = parseInt(max);
                $scope.min_value = parseInt(min);
                if ($scope.min_value > $scope.max_value)
                {
                    swal({ title: "Alert", text: "Minimum Value less than Maximum Value", showCloseButton: true, width: 300, height: 200 });

                    $scope.temp["glpc_min_limit"] = '';
                }
            }

            $scope.max_limit_cal1 = function (max, min)
            {
                
                $scope.max_value1 = parseInt(max);
                $scope.min_value1 = parseInt(min);
                if ($scope.min_value1 > $scope.max_value1) {
                    swal({ title: "Alert", text: "Minimum Value less than Maximum Value", showCloseButton: true, width: 300, height: 200 });
                    $scope.temp["glpc_max_limit_per_transaction"] = '';
                }

            }

            //Combobox Dependency

           



            //Fill Combo GLAccountNo
            $http.get(ENV.apiUrl + "api/BankMaster/GetGLAccountNumber").then(function (docstatus1) {
                $scope.GlACNO = docstatus1.data;
                
            });

            //$http.get(ENV.apiUrl + "api/GoalTarget/getAcademicYear").then(function (Acyear) {
            //    $scope.Acc_year = Acyear.data;
          
            //});
            $scope.size = function (str) {
               
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                main.checked = false;
               $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

               

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Bankmaster, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Bankmaster;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.pb_bank_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.pb_bank_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.pb_srl_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.pb_gl_acno.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.pb_gl_acno == toSearch) ? true : false;
            }

            $scope.showdate = function (date, name1) {

                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];

                //$scope.temp[name1] = year + "/" + month + "/" + day;
                var day = date.split("-")[0];
                var month = date.split("-")[1];
                var year = date.split("-")[2];

                $scope.temp[name1] = day + "-" + month + "/" + year;
            }


            //NEW BUTTON
            $scope.New = function () {
                
                $scope.dept = false
                $scope.account = false;
                //var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd');
                var dateyear = $filter('date')(new Date(), 'dd-MM-yyyy');
                $("#AccountNo").select2("val", "");
                $scope.dt = {
                    'glpc_cur_date': dateyear,
                    'glpc_creation_date': dateyear
                }

                $scope.temp = {
                    'glpc_creation_user': user,
                }

                $scope.disabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
               

            }

            //EnableDesable

            $scope.setvisible = function () {
                
                $scope.ldgno = true;
                $scope.slno = true;
            }

            var dataforSave = [];
            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                
                var data=
                    {
                            'glpc_comp_code':comp_code,
                            'glpc_year':finance_year,
                            'glpc_dept_no':$scope.temp.codp_dept_no,
                            'glpc_acct_code': $scope.temp.glma_acct_code,
                            'glpc_max_limit':$scope.temp.glpc_max_limit,
                            'glpc_min_limit':$scope.temp.glpc_min_limit,
                            'glpc_max_limit_per_transaction':$scope.temp.glpc_max_limit_per_transaction,
                            'glpc_curr_balance':0,
                            'glpc_creation_date':$scope.dt.glpc_creation_date,
                            'glpc_creation_user': user,
                            'glpc_cur_date':$scope.dt.glpc_cur_date,
                            'glpc_cost_center_flag':$scope.temp.glpc_cost_center_flag,
                            'glpc_status': $scope.temp.glpc_status,
                            'opr' :'I'
                     }
               
                             
                             dataforSave.push(data);
                             $http.post(ENV.apiUrl + "api/BankMaster/CUDPettyCashMasters", dataforSave).then(function (msg) {
                                 $scope.msg1 = msg.data;
                                 if ($scope.msg1 == true) {
                                     swal({ title: "Alert", text: "Record Inserted Successfully", showCloseButton: true, width: 300, height: 200 });
                                 }
                                 else {
                                     swal({ title: "Alert", text: "Record Not Inserted " + $scope.msg1, showCloseButton: true, width: 300, height: 200 });
                                 }
                                 $http.get(ENV.apiUrl + "api/BankMaster/getPettyCashDeatailsData").then(function (getAll_data) {
                                     $scope.get_allData = getAll_data.data;
                                     $scope.totalItems = $scope.get_allData.length;
                                     $scope.todos = $scope.get_allData;
                                     $scope.makeTodos();
                                 });

                        });
                        datasend = [];
                        $scope.table = true;
                        $scope.display = false;
                
            }

            //Date FORMAT
            $scope.showdate = function (date, name1) {
                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];

                //$scope.temp[name1] = year + "-" + month + "-" + day;
                var day = date.split("-")[0];
                var month = date.split("-")[1];
                var year = date.split("-")[2];

                $scope.temp[name1] = day + "-" + month + "-" + year;
            }
            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }
            //DATA EDIT
            $scope.edit = function (str) {
                
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.txtyear = true;
                $scope.cmbstatus = false;
                $scope.dept = true
                $scope.account = true;
                // $scope.temp = str;
                 var accNumber =str.glpc_acct_code;

                $scope.temp = {
                    'glpc_number':str.glpc_number,
                    'glma_acct_code': accNumber,
                    'codp_dept_no': str.glpc_dept_no,
                    'glpc_max_limit': str.glpc_max_limit,
                    'glpc_min_limit': str.glpc_min_limit,
                    'glpc_max_limit_per_transaction': str.glpc_max_limit_per_transaction,
                    'glpc_curr_balance': str.glpc_curr_balance,
                    'glpc_creation_user': str.glpc_creation_user,
                    'glpc_cost_center_flag': str.glpc_cost_center_flag,
                    'glpc_status': str.glpc_status
                }
                $scope.dt={
                    'glpc_creation_date':str.glpc_creation_date,
                    'glpc_cur_date':str.glpc_cur_date

                }

                $scope.getAccounts(str.glpc_dept_no);

            }


            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function () {
                
               // $scope.newAcno = $scope.temp.acno.substring(3)
                var data =
                    {
                        'glpc_comp_code': comp_code,
                        'glpc_year': finance_year,
                        'glpc_dept_no': $scope.temp.codp_dept_no,
                        'glpc_acct_code': $scope.temp.glma_acct_code,
                        'glpc_max_limit': $scope.temp.glpc_max_limit,
                        'glpc_min_limit': $scope.temp.glpc_min_limit,
                        'glpc_max_limit_per_transaction': $scope.temp.glpc_max_limit_per_transaction,
                        'glpc_curr_balance': 0,
                        'glpc_creation_date': $scope.dt.glpc_creation_date,
                        'glpc_creation_user': user,
                        'glpc_cur_date': $scope.dt.glpc_cur_date,
                        'glpc_cost_center_flag': $scope.temp.glpc_cost_center_flag,
                        'glpc_status': $scope.temp.glpc_status,
                        'opr': 'U'
                    }

                    dataforUpdate.push(data);
                    $http.post(ENV.apiUrl + "api/BankMaster/CUDPettyCashMasters", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", showCloseButton: true, width: 300, height: 200 });
                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated. " + $scope.msg1, showCloseButton: true, width: 300, height: 200 });
                        }
                        $http.get(ENV.apiUrl + "api/BankMaster/getPettyCashDeatailsData").then(function (getAll_data) {
                            $scope.get_allData = getAll_data.data;
                            $scope.totalItems = $scope.get_allData.length;
                            $scope.todos = $scope.get_allData;
                            $scope.makeTodos();
                        });
                    });
                  
                    $scope.table = true;
                    $scope.display = false;
                
            }

            //DELETE RECORD
            $scope.CheckAllChecked = function () {
                
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].glpc_number);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].glpc_number);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.OkDelete = function () {
                
                var deletefin = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    
                    var accNumber = $scope.filteredTodos[i].glpc_comp_code + $scope.filteredTodos[i].glpc_dept_no + $scope.filteredTodos[i].glpc_acct_code;
                    var v = document.getElementById(i + $scope.filteredTodos[i].glpc_number);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'glpc_comp_code': $scope.filteredTodos[i].glpc_comp_code,
                            'glpc_year': $scope.filteredTodos[i].glpc_year,
                            'glpc_dept_no': $scope.filteredTodos[i].glpc_dept_no,
                            'glpc_acct_code': $scope.filteredTodos[i].glpc_acct_code,
                            opr: 'D'
                        });
                        
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/BankMaster/CUDPettyCashMasters", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BankMaster/getPettyCashDeatailsData").then(function (getAll_data) {
                                                $scope.get_allData = getAll_data.data;
                                                $scope.totalItems = $scope.get_allData.length;
                                                $scope.todos = $scope.get_allData;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: "Record Not Deleted. " + $scope.msg1, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BankMaster/getPettyCashDeatailsData").then(function (getAll_data) {
                                                $scope.get_allData = getAll_data.data;
                                                $scope.totalItems = $scope.get_allData.length;
                                                $scope.todos = $scope.get_allData;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i + $scope.filteredTodos[i].glpc_number);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");

                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
               
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                //format: 'dd-mm-yyyy'
                format: 'yyyy-mm-dd'
            });






        }])

})();
