﻿(function () {
    'use strict';
    var main, temp = [];
    var data1 = [];
    var data2 = [];
    var block1 = [];
    var status = "";
    var msg_flag1 = false;
    var prdocdata = [];
    var acconcode = "";
    var cmbvalue = "";
    var comp_code = "1";
    var chk;
    var data = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('BankReconciliationViewCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '5';
            var user = $rootScope.globals.currentUser.username;
            $scope.tble_show = false;
            //$scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            //var comp_code = $scope.finnDetail.company;
            //var finance_year = $scope.finnDetail.year;
            var comp_code = 1;
            var finance_year = 2018;
            $scope.cost_combo = false;
            $scope.block1 = {};
            $scope.temp = []

            setTimeout(function () {
                $("#cmb_acc_Code3").select2();

            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });


            debugger
            $http.get(ENV.apiUrl + "api/GLSLQuery/getDate_SL_Query?finance_year=" + finance_year).then(function (res) {
                $scope.getDates = res.data;

                $scope.block1 = {
                    from_date: $scope.getDates[0].sims_financial_year_start_date,
                    to_date: $scope.getDates[0].sims_financial_year_end_date
                }


                console.log($scope.getDates)
            });




            $scope.positivevalue = function (str) {
                return Math.abs(str)
            }


            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };


            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


            $scope.cost_center = function (str) {

                chk = str;
                if (str == true) {
                    $scope.cost_combo = true;
                }
                else {
                    $scope.cost_combo = false;
                    $scope.block1.glco_cost_centre_code = null;
                    $scope.getViewDetails();

                }
            }

            $scope.getDepartcode = function (str) {

                //$http.get(ENV.apiUrl + "api/BankReceipt/getCostCenter?aacno=" + str).then(function (cost_center) {
                $http.get(ENV.apiUrl + "api/BankPayment/getCostCenter?aacno=" + str + "&comp_cd=" + comp_code + "&fyear=" + finance_year).then(function (cost_center) {
                    $scope.costcenter = cost_center.data;
                    if ($scope.costcenter.length > 0) {
                        $scope.chkcost = true;
                    }
                    else {
                        $scope.chkcost = false;
                        $scope.temp.cost_chk = false;
                        $scope.cost_combo = false;
                    }
                });
            }


            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=Single").then(function (res) {
                $scope.final_doc_url = res.data;
            });

            $scope.Fetchdata = function (cmp_cd, docd, final_doc_no) {
                debugger
                $scope.docCode1 = docd;
                $scope.provNo1 = final_doc_no;

                $('#myModal4').modal('show')
                //$scope.prdocdata = [];

                $http.get(ENV.apiUrl + "api/VoucherReversal/getAllRecords?doccode=" + docd + "&prnumber=" + final_doc_no + "&comp_code=" + cmp_cd).then(function (prdoc_data) {

                    $scope.prdocdata = prdoc_data.data;
                    // $scope.block1 = [];
                    $scope.temp_doc = [];
                    $scope.totalDebit = 0;
                    $scope.totalcredit = 0;

                    for (var i = 0; i < $scope.prdocdata.length; i++) {

                        if (i == 0) {
                            $scope.temp_doc.gltd_comp_code = $scope.prdocdata[i].gltd_comp_code;
                            $scope.temp_doc.gltd_doc_code = $scope.prdocdata[i].gltd_doc_code;

                            $scope.temp_doc.gltd_doc_narr = $scope.prdocdata[i].gltd_doc_narr;
                            $scope.temp_doc.gltd_remarks = $scope.prdocdata[i].gltd_remarks;
                            $scope.temp_doc.gltd_doc_date = $scope.prdocdata[i].gltd_doc_date;
                            $scope.temp_doc.gltd_post_date = $scope.prdocdata[i].gltd_post_date;
                            $scope.temp_doc.gltd_prov_doc_no = $scope.prdocdata[i].gltd_prov_doc_no;
                            $scope.temp_doc.gltd_final_doc_no = final_doc_no;

                            $scope.temp_doc.gldu_authorize = $scope.prdocdata[i].gldu_authorize;
                            $scope.temp_doc.gldu_verify = $scope.prdocdata[i].gldu_verify;
                            $scope.temp_doc.gltd_authorize_date = $scope.prdocdata[i].gltd_authorize_date;
                            $scope.temp_doc.gltd_verify_date = $scope.prdocdata[i].gltd_verify_date;

                        }
                        $scope.cost = $scope.cost || ($scope.prdocdata[i].gldd_cost_center_code != '' || $scope.prdocdata[i].gldd_cost_center_code != "");
                        $scope.totalDebit = parseFloat($scope.totalDebit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_debit);
                        $scope.totalcredit = parseFloat($scope.totalcredit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_credit);

                    }

                });

            }

            $scope.CheckPrintReport = function () {


                var data = {
                    location: $scope.final_doc_url,
                    parameter: {
                        comp_detail: comp_code,
                        doc_code: $scope.temp_doc.gltd_doc_code,
                        doc_no: $scope.temp_doc.gltd_final_doc_no,

                    },

                    state: 'main.Fin064'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')


            }



            $http.get(ENV.apiUrl + "api/GLSLQuery/getDocCode_GL_Query?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                $scope.geDocCode_jv = res.data;
            });

            $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {

                $scope.getAllAccNos = docstatus.data;
            });

            $scope.FetchOPBal = function (str) {
                //For COst Center
                //$scope.cost_combo = false;


                //$http.get(ENV.apiUrl + "api/BankReceipt/getCostCenter?aacno=" + str).then(function (cost_center) {
                $http.get(ENV.apiUrl + "api/BankPayment/getCostCenter?aacno=" + str + "&comp_cd=" + comp_code + "&fyear=" + finance_year).then(function (cost_center) {
                    $scope.costcenter = cost_center.data;
                    if ($scope.costcenter.length > 0) {
                        $scope.chkcost = true;
                    }
                    else {
                        $scope.chkcost = false;
                        $scope.temp.cost_chk = false;
                        $scope.cost_combo = false;
                    }
                });



                //Code sgr

                for (var i = 0; i < $scope.getAllAccNos.length; i++) {
                    if ($scope.getAllAccNos[i].gldd_acct_code == $scope.block1.gldd_acct_code) {

                        $scope.block1.sltr_yob_amt = $scope.getAllAccNos[i].sltr_yob_amt;
                    }
                }
                if ($scope.block1.sltr_yob_amt > 0) {
                    $scope.opbal = $scope.block1.sltr_yob_amt;
                    $scope.cr = ' dr';
                }
                else if ($scope.block1.sltr_yob_amt < 0) {
                    $scope.opbal = Math.abs($scope.block1.sltr_yob_amt);
                    $scope.cr = ' cr';
                }
                else
                    $scope.opbal = $scope.block1.sltr_yob_amt;


            }

            $scope.getViewDetails = function () {
                debugger
                $scope.tble_show = true;

                $('#loader').modal({ backdrop: 'static', keyboard: false });

                var data = {
                    gltr_comp_code: comp_code,
                    gltr_doc_code: ($scope.block1['gltd_doc_code'] != undefined || $scope.block1['gltd_doc_code'] != "") ? $scope.block1['gltd_doc_code'] : null,
                    gltr_acct_code: ($scope.block1.gldd_acct_code != undefined || $scope.block1.gldd_acct_code != "") ? $scope.block1.gldd_acct_code : null,
                    gltr_pstng_date: ($scope.block1.from_date != undefined || $scope.block1.from_date != "") ? $scope.block1.from_date : null,
                    gltr_doc_date: ($scope.block1.to_date != undefined || $scope.block1.to_date != "") ? $scope.block1.to_date : null,
                    glco_cost_centre_code: ($scope.block1['glco_cost_centre_code'] != undefined || $scope.block1['glco_cost_centre_code'] != "") ? $scope.block1['glco_cost_centre_code'] : null,
                }
                var close_amt;
                $scope.closebal = 0;
                $http.post(ENV.apiUrl + "api/GLSLQuery/list_All_GL_Query", data).then(function (res1) {
                    $scope.trans_data = res1.data;

                    $('#loader').modal('hide');
                    $(".modal-backdrop").removeClass("modal-backdrop");

                    if ($scope.trans_data.length > 0) {
                        $scope.length = $scope.trans_data.length - 1;

                        if ($scope.block1.sltr_yob_amt > 0) {
                            $scope.trans_data[$scope.length].gltr_total_dr_amount = parseFloat($scope.trans_data[$scope.length].gltr_total_dr_amount) + $scope.block1.sltr_yob_amt;
                        }
                        else if ($scope.block1.sltr_yob_amt < 0) {
                            $scope.trans_data[$scope.length].gltr_total_cr_amount = parseFloat($scope.trans_data[$scope.length].gltr_total_cr_amount) + Math.abs($scope.block1.sltr_yob_amt);
                        }
                        close_amt = parseFloat($scope.trans_data[$scope.length].gltr_total_dr_amount) - parseFloat($scope.trans_data[$scope.length].gltr_total_cr_amount);

                        if (close_amt > 0) {
                            $scope.closebal = close_amt;
                            $scope.dr = ' dr';
                        }
                        else if (close_amt < 0) {
                            $scope.closebal = Math.abs(close_amt);
                            $scope.dr = ' cr';
                        }
                        else
                            $scope.closebal = close_amt;
                    }

                });

            }

            $scope.exportStaffResigned = function () {
                var check = true;


                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " GLQueryDetails.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            var blob = new Blob([document.getElementById('printdata3').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + " GLQueryDetails" + ".xls");
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Report Not Save", showCloseButton: true, width: 380, })
                }
            }

            //Code For Direct Print Button Option
            $scope.printStaffResigned = function (div) {

                var docHead = document.head.outerHTML;
                var printContents = document.getElementById(div).outerHTML;
                var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";

                var newWin = window.open("", "_blank", winAttr);
                var writeDoc = newWin.document;
                writeDoc.open();
                writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                writeDoc.close();
                newWin.focus();
            }

        }]
        )
})();
