﻿(function () {
    'use strict';
    var main, temp=[];
    var data1 = [];
    var data2 = [];
    var block1 = [];
    var status = "";
    var msg_flag1 = false;
    var prdocdata = [];
    var acconcode = "";
    var cmbvalue = "";
    var comp_code = "1";
    var chk;
    var data = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('GLQueryCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '5';
            var user = $rootScope.globals.currentUser.username;
            $scope.tble_show = false;
            $scope.print_pdf = false;
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
            $scope.cost_combo = false;
            $scope.block1 = {};
            $scope.temp = []
            $scope.show_table_header = false;
            setTimeout(function () {
                $("#cmb_acc_Code3").select2();

            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });


            debugger
            $http.get(ENV.apiUrl + "api/GLSLQuery/getDate_SL_Query?finance_year=" + finance_year).then(function (res) {
                $scope.getDates = res.data;

                if ($scope.getDates.length > 0) {
                    $scope.sDate = $scope.getDates[0].sims_financial_year_start_date;
                    $scope.eDate = $scope.getDates[0].sims_financial_year_end_date;

                    $scope.block1 = {
                        from_date: $scope.getDates[0].sims_financial_year_start_date,
                        to_date: $scope.getDates[0].sims_financial_year_end_date
                    }

                   $(document).ready(function () {
                        $("#from_date_new").kendoDatePicker({

                            format: "dd-MM-yyyy",
                            min: new Date(moment($scope.sDate, 'DD-MM-YYYY').format('YYYY-MM-DD')),
                            max: new Date(moment($scope.eDate, 'DD-MM-YYYY').format('YYYY-MM-DD'))


                        });
                    });


                    $(document).ready(function () {
                        $("#to_date_new").kendoDatePicker({

                            format: "dd-MM-yyyy",
                            min: new Date(moment($scope.sDate, 'DD-MM-YYYY').format('YYYY-MM-DD')),
                            max: new Date(moment($scope.eDate, 'DD-MM-YYYY').format('YYYY-MM-DD'))

                            //format: "dd/MM/yyyy"
                            //format: "yyyy/MM/dd"
                        });
                    });
                }

                else {
                    $(document).ready(function () {
                        $("#from_date_new").kendoDatePicker({

                            format: "dd-MM-yyyy",
                           

                        });
                    });


                    $(document).ready(function () {
                        $("#to_date_new").kendoDatePicker({

                            format: "dd-MM-yyyy",
                           
                            //format: "dd/MM/yyyy"
                            //format: "yyyy/MM/dd"
                        });
                    });

                }
                //console.log($scope.getDates)
            });




            $scope.onChange1 = function () {

                var check_date = moment($scope.eDate, 'DD-MM-YYYY').format('YYYY-MM-DD')
                var check_date1 = moment($scope.block1.to_date, 'DD-MM-YYYY').format('YYYY-MM-DD')

                if (moment(check_date1) > moment(check_date)) {
                    // this.value() = '';
                    $scope.block1.from_date = '';
                    swal({ title: "Alert", text: "Please select date between " + $scope.sDate + " to " + $scope.eDate + " in financial year", showCloseButton: true, width: 380, })
                }



            }

            $scope.onChange = function () {
                debugger
                var check_date = moment($scope.sDate, 'DD-MM-YYYY').format('YYYY-MM-DD')
                var check_date1 = moment($scope.block1.from_date, 'DD-MM-YYYY').format('YYYY-MM-DD')

                if (moment(check_date1) < moment(check_date)) {

                    $scope.block1.from_date = '';
                    swal({ title: "Alert", text: "Please select date between " + $scope.sDate + " to " + $scope.eDate + " in financial year", showCloseButton: true, width: 380, })

                }

            }


            $scope.positivevalue = function (str) {
                return Math.abs(str)
            }


            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };


            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


            $scope.cost_center = function (str) {
                
                chk = str;
                if (str == true) {
                    $scope.cost_combo = true;
                }
                else {
                    $scope.cost_combo = false;
                    $scope.block1.glco_cost_centre_code = null;
                    $scope.getViewDetails();

                }
            }

            $scope.getDepartcode = function (str) {
                
                //$http.get(ENV.apiUrl + "api/BankReceipt/getCostCenter?aacno=" + str).then(function (cost_center) {
                $http.get(ENV.apiUrl + "api/BankPayment/getCostCenter?aacno=" + str + "&comp_cd=" + comp_code + "&fyear=" + finance_year).then(function (cost_center) {
                    $scope.costcenter = cost_center.data;
                    if ($scope.costcenter.length > 0) {
                        $scope.chkcost = true;
                    }
                    else {
                        $scope.chkcost = false;
                        $scope.temp.cost_chk = false;
                        $scope.cost_combo = false;
                    }
                    });
            }


            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=Single").then(function (res) {
                $scope.final_doc_url = res.data;
            });

            $scope.Fetchdata = function (cmp_cd, docd, final_doc_no) {
                debugger
                $scope.docCode1 = docd;
                $scope.provNo1 = final_doc_no;

                $('#myModal4').modal('show')
                //$scope.prdocdata = [];

                $http.get(ENV.apiUrl + "api/VoucherReversal/getAllRecords?doccode=" + docd + "&prnumber=" + final_doc_no + "&comp_code=" + cmp_cd).then(function (prdoc_data) {

                    $scope.prdocdata = prdoc_data.data;
                    // $scope.block1 = [];
                    $scope.temp_doc = [];
                    $scope.totalDebit = 0;
                    $scope.totalcredit = 0;
                      
                    for (var i = 0; i < $scope.prdocdata.length; i++) {

                        if (i == 0) {
                            $scope.temp_doc.gltd_comp_code = $scope.prdocdata[i].gltd_comp_code;
                            $scope.temp_doc.gltd_doc_code = $scope.prdocdata[i].gltd_doc_code;

                            $scope.temp_doc.gltd_doc_narr = $scope.prdocdata[i].gltd_doc_narr;
                            $scope.temp_doc.gltd_remarks = $scope.prdocdata[i].gltd_remarks;
                            $scope.temp_doc.gltd_doc_date = $scope.prdocdata[i].gltd_doc_date;
                            $scope.temp_doc.gltd_post_date = $scope.prdocdata[i].gltd_post_date;
                            $scope.temp_doc.gltd_prov_doc_no = $scope.prdocdata[i].gltd_prov_doc_no;
                            $scope.temp_doc.gltd_final_doc_no = final_doc_no;
                            $scope.temp_doc.gltd_paid_to = $scope.prdocdata[i].gltd_paid_to;
                            $scope.temp_doc.gltd_cheque_no = $scope.prdocdata[i].gltd_cheque_no;
                            $scope.temp_doc.gldu_authorize = $scope.prdocdata[i].gldu_authorize;
                            $scope.temp_doc.gldu_verify = $scope.prdocdata[i].gldu_verify;
                            $scope.temp_doc.gltd_authorize_date = $scope.prdocdata[i].gltd_authorize_date;
                            $scope.temp_doc.gltd_verify_date = $scope.prdocdata[i].gltd_verify_date;

                        }
                        $scope.cost = $scope.cost || ($scope.prdocdata[i].gldd_cost_center_code != '' || $scope.prdocdata[i].gldd_cost_center_code != "");
                        $scope.totalDebit = parseFloat($scope.totalDebit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_debit);
                        $scope.totalcredit = parseFloat($scope.totalcredit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_credit);

                    }

                });

            }
           

            $scope.CheckPrintReport = function () {


                var data = {
                    location: $scope.final_doc_url,
                    parameter: {
                        comp_detail: comp_code,
                        doc_code: $scope.temp_doc.gltd_doc_code,
                        doc_no: $scope.temp_doc.gltd_final_doc_no,

                    },

                    state: 'main.Fin064'
                }

                window.localStorage["ReportDetails"] = JSON.stringify(data)
                //first hide modal and open report
                $('#myModal4').modal('hide');
                setTimeout(function () {
                    $state.go('main.ReportCardParameter')
                }, 700);


            }



            $http.get(ENV.apiUrl + "api/GLSLQuery/getDocCode_GL_Query?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                $scope.geDocCode_jv = res.data;
            });

            $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code+ "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {
                
                $scope.getAllAccNos = docstatus.data;
            });

            $scope.FetchOPBal = function (str) {
                //For COst Center
                //$scope.cost_combo = false;

                
                //$http.get(ENV.apiUrl + "api/BankReceipt/getCostCenter?aacno=" + str).then(function (cost_center) {
                $http.get(ENV.apiUrl + "api/BankPayment/getCostCenter?aacno=" + str + "&comp_cd=" + comp_code + "&fyear=" + finance_year).then(function (cost_center) {
                    $scope.costcenter = cost_center.data;
                    if ($scope.costcenter.length > 0) {
                        $scope.chkcost = true;
                    }
                    else {
                        $scope.chkcost = false;
                        $scope.temp.cost_chk = false;
                        $scope.cost_combo = false;
                    }
                    });

              
                //debugger;
                //$http.get(ENV.apiUrl + "api/GLSLQuery/Get_GL_opening_bal?comp_code=" + comp_code + "&fyear=" + finance_year + "&acct_code=" + str + "&from_date=" + $scope.block1.from_date).then(function (res) {
                //    $scope.block1.sltr_yob_amt = res.data;

                //    if ($scope.block1.sltr_yob_amt > 0) {
                //        $scope.opbal = $scope.block1.sltr_yob_amt;
                //        $scope.cr = ' dr';
                //    }
                //    else if ($scope.block1.sltr_yob_amt < 0) {
                //        $scope.opbal = Math.abs($scope.block1.sltr_yob_amt);
                //        $scope.cr = ' cr';
                //    }
                //    else
                //        $scope.opbal = $scope.block1.sltr_yob_amt;

                //});


                //Code sgr
                
                //for (var i = 0; i < $scope.getAllAccNos.length; i++) {
                //    if ($scope.getAllAccNos[i].gldd_acct_code == $scope.block1.gldd_acct_code) {

                //        $scope.block1.sltr_yob_amt = $scope.getAllAccNos[i].sltr_yob_amt;
                //    }
                //}
                //if ($scope.block1.sltr_yob_amt > 0) {
                //    $scope.opbal = $scope.block1.sltr_yob_amt;
                //    $scope.cr = ' dr';
                //}
                //else if ($scope.block1.sltr_yob_amt < 0)
                //{
                //    $scope.opbal = Math.abs($scope.block1.sltr_yob_amt);
                //    $scope.cr = ' cr';
                //    }
                //else
                //    $scope.opbal = $scope.block1.sltr_yob_amt;


            }

            $scope.getViewDetails = function () {
                debugger
                $scope.acct_name = $("#cmb_acc_Code3 option:selected").text();
                $scope.from = $scope.block1.from_date;
                $scope.to = $scope.block1.to_date;
                $scope.tble_show = true;

                if ($scope.block1.gldd_acct_code != "" && $scope.block1.gldd_acct_code != undefined)
                    $scope.msg1 = '';
                else {
                    $scope.msg1 = 'Field is Required';
                    return;
                }

                $('#loader').modal({ backdrop: 'static', keyboard: false });

                var data = {
                    gltr_comp_code: comp_code,
                    gltr_doc_code: ($scope.block1['gltd_doc_code'] != undefined || $scope.block1['gltd_doc_code'] != "") ? $scope.block1['gltd_doc_code'] : null,
                    gltr_acct_code: ($scope.block1.gldd_acct_code != undefined || $scope.block1.gldd_acct_code != "") ? $scope.block1.gldd_acct_code : null,
                    gltr_pstng_date: ($scope.block1.from_date != undefined || $scope.block1.from_date != "") ? $scope.block1.from_date : null,
                    gltr_doc_date: ($scope.block1.to_date != undefined || $scope.block1.to_date != "") ? $scope.block1.to_date : null,
                    glco_cost_centre_code: ($scope.block1['glco_cost_centre_code'] != undefined || $scope.block1['glco_cost_centre_code'] != "") ? $scope.block1['glco_cost_centre_code'] : null,
                }

                debugger;
                $http.get(ENV.apiUrl + "api/GLSLQuery/Get_GL_opening_bal?comp_code=" + comp_code + "&fyear=" + finance_year + "&acct_code=" + $scope.block1.gldd_acct_code + "&from_date=" + $scope.block1.from_date).then(function (res) {
                    $scope.block1.sltr_yob_amt = res.data;

                    if ($scope.block1.sltr_yob_amt > 0) {
                        $scope.opbal = $scope.block1.sltr_yob_amt;
                        $scope.cr = ' dr';
                    }
                    else if ($scope.block1.sltr_yob_amt < 0) {
                        $scope.opbal = Math.abs($scope.block1.sltr_yob_amt);
                        $scope.cr = ' cr';
                    }
                    else
                        $scope.opbal = $scope.block1.sltr_yob_amt;

                });

                var close_amt;
                $scope.closebal = 0;
                $http.post(ENV.apiUrl + "api/GLSLQuery/list_All_GL_Query", data).then(function (res1) {
                    $scope.trans_data = res1.data;


                    $('#loader').modal('hide');
                    $(".modal-backdrop").removeClass("modal-backdrop");

                    if ($scope.trans_data.length > 0) {
                        $scope.length = $scope.trans_data.length - 1;

                        //if ($scope.block1.sltr_yob_amt > 0) {
                        //    $scope.trans_data[$scope.length].gltr_total_dr_amount = parseFloat($scope.trans_data[$scope.length].gltr_total_dr_amount);//+ $scope.block1.sltr_yob_amt;
                        //}
                        //else if ($scope.block1.sltr_yob_amt < 0) {
                        //    $scope.trans_data[$scope.length].gltr_total_cr_amount = parseFloat($scope.trans_data[$scope.length].gltr_total_cr_amount);//+Math.abs($scope.block1.sltr_yob_amt);
                        //}
                        close_amt = parseFloat($scope.trans_data[$scope.length].gltr_total_dr_amount) - parseFloat($scope.trans_data[$scope.length].gltr_total_cr_amount);


                        if ($scope.block1.sltr_yob_amt >= 0) {
                            close_amt = close_amt + parseFloat($scope.block1.sltr_yob_amt);
                        }
                        else if ($scope.block1.sltr_yob_amt < 0) {
                            close_amt = parseFloat(close_amt) +parseFloat($scope.block1.sltr_yob_amt);
                        }


                        if (close_amt >= 0) {
                            $scope.closebal = close_amt;
                            $scope.dr = ' dr';
                        }
                        else if (close_amt < 0) {
                            $scope.closebal = Math.abs(close_amt);
                            $scope.dr = ' cr';
                        }
                        else
                            $scope.closebal = close_amt;
                    }

                });
                
            }

            $scope.exportStaffResigned = function () {
                var check = true;
                //$scope.acct_name = $("#cmb_acc_Code3 option:selected").text();
                //$scope.from = $scope.block1.from_date;
                //$scope.to = $scope.block1.to_date;
               // $scope.block1.gldd_acct_code = '';

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.acct_name + "_GLQueryDetails.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            
                            var blob = new Blob([document.getElementById('printdata3').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + $scope.acct_name + "_GLQueryDetails" + ".xls");
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Report Not Save", showCloseButton: true, width: 380, })
                }
            }

            //Code For Direct Print Button Option
            $scope.printStaffResigned = function (div) {
                debugger
               // $scope.gldd_acct_code = '';
              
                var docHead = document.head.outerHTML;
                var printContents = document.getElementById('printdata4').outerHTML;
                var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";

                var newWin = window.open("", "_blank", winAttr);
                var writeDoc = newWin.document;
                writeDoc.open();
                writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                writeDoc.close();
                newWin.focus();
                //$scope.show_table_header = false;
            }

           

            $scope.maxmize = function () {
                
                $('#jv_Voucher').css({ 'height': '100%', 'width': '100%' })
                //$('#modalcontentbox').css({ 'height': '100%', 'width': '100%' })

                $scope.windowflg = true;
            }

            $scope.manimize = function () {
                $('#jv_Voucher').css({ 'height': '600px', 'width': '900px' })
                $scope.windowflg = false;

            }

            $scope.back = function () {

                $scope.report_show = true;

            }
            $scope.closemodal = function () {

                $scope.report_show = true;
            }
            $scope.report_show = true;
            $scope.Report = function () {
                debugger;

                $scope.report_show = false;

                var data = {
                    location: $scope.final_doc_url,
                    parameter: {
                        comp_detail: comp_code,
                        doc_code: $scope.temp_doc.gltd_doc_code,
                        doc_no: $scope.temp_doc.gltd_final_doc_no,

                    },

                    state: 'main.Fin064',

                    /*ready: function () {
                        this.refreshReport();
                    },*/
                }

                window.localStorage["ReportDetails"] = JSON.stringify(data)

                // });

                //For Report Tool////////////////////////////////////////////////////////////////////////////////


                try {
                    $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                    $scope.location = $scope.rpt.location;
                    $scope.parameter = $scope.rpt.parameter;
                    $scope.state = $scope.rpt.state;
                }
                catch (ex) {
                }
                var s;

                //if ($http.defaults.headers.common['schoolId'] == 'dpsmis')
                //    s = "SimsReports." + $scope.location + ",SimsReports";


                //else
                //    s = "SimsReports." + $scope.location + ",SimsReports" + $http.defaults.headers.common['schoolId'];


                //  window.localStorage["Finn_comp"] = JSON.stringify(data)

                s = "SimsReports." + $scope.location + ",SimsReports";

                var url = window.location.href;
                var domain = url.substring(0, url.indexOf(':'))
                if ($http.defaults.headers.common['schoolId'] == 'sms') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                }
                else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc' || $http.defaults.headers.common['schoolId'] == 'christ') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'imert') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'ibserp') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.ibskuwait.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.ibskuwait.com/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                }

                else {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                }
               // console.log(service_url);


                $scope.parameters = {}

                $("#reportViewer1")
                               .telerik_ReportViewer({
                                   //serviceUrl: ENV.apiUrl + "api/reports/",
                                   serviceUrl: service_url,

                                   viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                   scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                   // Zoom in and out the report using the scale
                                   // 1.0 is equal to 100%, i.e. the original size of the report
                                   scale: 1.0,
                                   ready: function () {
                                       //this.refreshReport();
                                   }

                               });





                var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                reportViewer.reportSource({
                    report: s,
                    parameters: $scope.parameter,
                });

                //var rv = $("#reportViewer1").data("telerik_ReportViewer");
                //rv.commands.print.exec();

                setInterval(function () {

                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                    //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });

                }, 1000);


                $timeout(function () {
                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                    //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });


                }, 100)




            }
        }]
        )
})();
