﻿(function () {
    'use strict';
    var obj1, obj2, temp, main;
    var opr = '';
    var formdata = new FormData();
    var categorycode = [];


    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ScheduleGroupDetailsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;

            $scope.ScheduleGroupDetailDetail = true;
            $scope.editmode = false;
            var data1 = [];
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $(function () {
                $("#all_chk").change(function () {
                    $('input:checkbox').prop('checked', this.checked);
                    $('tr').toggleClass("selected_row", this.checked)
                });

                $('table tbody :checkbox').change(function (event) {
                    $(this).closest('tr').toggleClass("selected_row", this.checked);
                });
            });

            setTimeout(function () {
                $("#account_codes6").select2();
            }, 100)

            


            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;

                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.SGDData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }


            $http.get(ENV.apiUrl + "api/schedulegroupdetails/getScheduleGroupDetail?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (SGD_Data) {
                $scope.SGDData = SGD_Data.data;
                $scope.totalItems = $scope.SGDData.length;
                $scope.todos = $scope.SGDData;
                $scope.makeTodos();
            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);


                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.SGDData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.SGDData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                    item.codegroupdes.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.glsccodename.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                    item.glsccodename == toSearch) ? true : false;

            }

            $scope.cancel = function () {
                $scope.ScheduleGroupDetailDetail = true;
                $scope.ScheduleGroupDetailOperation = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.editmode = false;
                opr = 'S';
                $scope.readonly = false;
                $scope.ScheduleGroupDetailDetail = false;
                $scope.ScheduleGroupDetailOperation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = {};
                $scope.edt['gsgd_sign'] = true;
            }

            $http.get(ENV.apiUrl + "api/schedulegroupdetails/getGroupName").then(function (res) {
                $scope.gn = res.data;

            });

            $http.get(ENV.apiUrl + "api/schedulegroupdetails/getScheduleCode").then(function (res) {
                $scope.gc = res.data;

            });

            $scope.Save = function (myForm) {
                
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'I';
                    $scope.exist = false;
                    data1.push(data);
                    for (var i = 0; i < $scope.SGDData.length; i++) {
                        if ($scope.SGDData[i].gsgd_group_code == data.gsgd_group_code && $scope.SGDData[i].gsgd_schedule_code == data.gsgd_schedule_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ title: "Alert", text: "Record Already exists", width: 300, height: 200 });
                    }

                    else {

                        $http.post(ENV.apiUrl + "api/schedulegroupdetails/ScheduleGroupDetailCUD?simsobj=", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {

                                swal({ title: "Alert", text: "Record Inserted Successfully", showCloseButton: true, width: 380 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ title: "Alert", text: "Record Not Inserted. ", showCloseButton: true, width: 380 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                            $scope.ScheduleGroupDetailOperation = false;
                            $http.get(ENV.apiUrl + "api/schedulegroupdetails/getScheduleGroupDetail?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (SGD_Data) {
                                $scope.SGDData = SGD_Data.data;
                                $scope.totalItems = $scope.SGDData.length;
                                $scope.todos = $scope.SGDData;
                                $scope.makeTodos();
                            });
                           

                        });

                        $scope.ScheduleGroupDetailDetail = true;
                        $scope.ScheduleGroupDetailOperation = false;
                    }
                    data1 = [];
                }
            }

            $scope.up = function (str) {
                
                opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.ScheduleGroupDetailDetail = false;
                $scope.ScheduleGroupDetailOperation = true;
                $scope.edt = {
                    gsgd_comp_code: str.gsgd_comp_code
                          , gsgd_group_code: str.gsgd_group_code
                          , gsgd_schedule_code: str.gsgd_schedule_code
                          , gsgd_sign: str.gsgd_sign
                };

                setTimeout(function () {
                    $("#account_codes5").select2("val", str.gsgd_schedule_code);
                }, 500)

            }

            $scope.Update = function () {

                
                var data = $scope.edt;
                data.opr = 'U';
                $scope.edt = "";
                data1.push(data);
                $http.post(ENV.apiUrl + "api/schedulegroupdetails/ScheduleGroupDetailCUD", data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    $scope.ScheduleGroupDetailOperation = false;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", showCloseButton: true, width: 380 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ title: "Alert", text: "Record Not Updated. ", showCloseButton: true, width: 380 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                      $http.get(ENV.apiUrl + "api/schedulegroupdetails/getScheduleGroupDetail?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (SGD_Data) {
                                $scope.SGDData = SGD_Data.data;
                                $scope.totalItems = $scope.SGDData.length;
                                $scope.todos = $scope.SGDData;
                                $scope.makeTodos();
                            });
                   
                })
                $scope.ScheduleGroupDetailOperation = false;
                $scope.ScheduleGroupDetailDetail = true;
                data1 = [];
            }

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.check_once = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.deleterecord = function () {
                
                var data1 = [];
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'gsgd_schedule_code': $scope.filteredTodos[i].gsgd_schedule_code,
                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            
                            $http.post(ENV.apiUrl + "api/schedulegroupdetails/ScheduleGroupDetailCUD", deleteleave).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/schedulegroupdetails/getScheduleGroupDetail?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (SGD_Data) {
                                                $scope.SGDData = SGD_Data.data;
                                                $scope.totalItems = $scope.SGDData.length;
                                                $scope.todos = $scope.SGDData;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    
                                    swal({ title: "Alert", text: "Record Not Deleted. ", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/schedulegroupdetails/getScheduleGroupDetail?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (SGD_Data) {
                                                $scope.SGDData = SGD_Data.data;
                                                $scope.totalItems = $scope.SGDData.length;
                                                $scope.todos = $scope.SGDData;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            
                            main = document.getElementById('all_chk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
                $scope.row1 = '';
            }


        }]);

})();