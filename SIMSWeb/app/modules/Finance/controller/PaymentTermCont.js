﻿(function () {
    'use strict';
    var opr = '';
    var loancode = [];
    var main;
    var data1 = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('PaymentTermCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;


            $scope.table1 = true;
            $http.get(ENV.apiUrl + "api/PaymentTermDetails/getPaymentTerm").then(function (getPaymentTerm_Data) {
                $scope.PaymentTerm_Data = getPaymentTerm_Data.data;
                $scope.totalItems = $scope.PaymentTerm_Data.length;
                $scope.todos = $scope.PaymentTerm_Data;
                $scope.makeTodos();
                });
            $scope.edt = {};
            $http.get(ENV.apiUrl + "api/common/getFinsCompCode").then(function (getFinsCompCode_Data) {
                $scope.FinsCompCode_Data = getFinsCompCode_Data.data;
                if (getFinsCompCode_Data.data.length > 0) {
                    $scope.edt.pT_COMP_Code = getFinsCompCode_Data.data[0].glsc_comp_code;
                }
                });

            $scope.operation = false;
            $scope.editmode = false;
            $scope.size = function (str) {
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;  $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.PaymentTerm_Data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();
            }
            $scope.edt = "";

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.editmode = false;
                opr = 'S';
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;

                $scope.edt = {};

                if ($scope.FinsCompCode_Data.length > 0) {
                    $scope.edt.pT_COMP_Code = $scope.FinsCompCode_Data[0].glsc_comp_code;
                }

                $scope.edt['pT_ENABLED'] = true;
            }

            $scope.up = function (str) {
             
                opr = 'U';
                var org_PT_TERM_NAME = 'org_PT_TERM_NAME';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.operation = true;

                $scope.edt = str;
                $scope.edt[org_PT_TERM_NAME] = str.pT_TERM_NAME;
               

            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
            }

            $scope.Save = function (myForm) {

                if (myForm) {
                    var data = [];
                    var data1 = [];

                     data = $scope.edt;
                    data.opr = 'I';
                    $scope.exist = false;
                    
                    for (var i = 0; i < $scope.PaymentTerm_Data.length; i++) {
                        if ($scope.PaymentTerm_Data[i].pT_TERM_NAME == data.pT_TERM_NAME && $scope.PaymentTerm_Data[i].pT_COMP_CODE == data.pT_COMP_CODE) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        // $rootScope.strMessage = 'Record Already exists';
                        swal({ title: "Alert", text: "Record Already exists", imageUrl: "assets/img/check.png", });
                        // $('#message').modal('show');
                    }
                    else {

                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/PaymentTermDetails/PaymentTermCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            $scope.table1 = true;
                            $scope.operation = false;
                            data = [];
                            data1 = [];
                            $scope.edt = '';
                            $http.get(ENV.apiUrl + "api/PaymentTermDetails/getPaymentTerm").then(function (getPaymentTerm_Data) {
                                $scope.PaymentTerm_Data = getPaymentTerm_Data.data;
                                $scope.totalItems = $scope.PaymentTerm_Data.length;
                                $scope.todos = $scope.PaymentTerm_Data;
                                $scope.makeTodos();

                                $scope.msg1 = msg.data;
                            });

                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully.", width: 300, height: 200 });
                                // $rootScope.strMessage = 'Record Inserted Successfully';
                                //   $('#message').modal('show');
                                   
                            }
                            else if ($scope.msg1 == false) {
                                swal({ title: "Alert", text: "Record Not Inserted. " , imageUrl: "assets/img/check.png", });
                                //  $rootScope.strMessage = 'Record not Inserted';
                                //  $('#message').modal('show');
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                            $scope.edt = '';
                        });
                       
                        $scope.table1 = true;
                        $scope.operation = false;
                    }
                }
            }

            $scope.Update = function () {

                var data = $scope.edt;
                data.opr = 'U';
                data1.push(data);
                $http.post(ENV.apiUrl + "api/PaymentTermDetails/PaymentTermCUD", data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    $scope.operation = false;
                    $http.get(ENV.apiUrl + "api/PaymentTermDetails/getPaymentTerm").then(function (getPaymentTerm_Data) {
                        $scope.PaymentTerm_Data = getPaymentTerm_Data.data;
                        $scope.totalItems = $scope.PaymentTerm_Data.length;
                        $scope.todos = $scope.PaymentTerm_Data;
                        $scope.makeTodos();
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ title: "Alert", text: "Record not Updated. " , width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });

                })
                $scope.operation = false;
                $scope.table1 = true;

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {

                        var v = document.getElementById(i);
                        v.checked = true;
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {

                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }
            }

            $scope.checkonebyonedelete = function () {

                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
            }

            $scope.deleterecord = function () {
                
                var deletecode = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    var v = document.getElementById(i);

                    if (v.checked == true) {
                        var deletetermcode = ({
                            'PT_COMP_Code': $scope.filteredTodos[i].pT_COMP_Code,
                            'pT_TERM_NAME': $scope.filteredTodos[i].pT_TERM_NAME,
                           
                            'opr': 'D'
                        });
                        deletecode.push(deletetermcode);
                    }
                }
                $http.post(ENV.apiUrl + "api/PaymentTermDetails/PaymentTermCUD", deletecode).then(function (msg) {
                    $scope.msg1 = msg.data;
                    $scope.table1 = true;
                    $scope.operation = false;
                    $http.get(ENV.apiUrl + "api/PaymentTermDetails/getPaymentTerm").then(function (getPaymentTerm_Data) {
                        $scope.PaymentTerm_Data = getPaymentTerm_Data.data;
                        $scope.totalItems = $scope.PaymentTerm_Data.length;
                        $scope.todos = $scope.PaymentTerm_Data;
                        $scope.makeTodos();

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Deleted Successfully", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {

                            swal({ title: "Alert", text: "Record not Deleted. " , width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                });
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.PaymentTerm_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.PaymentTerm_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                     item.company_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.pT_TERM_NAME.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    //  item.lc_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                   //  item.lc_debit_acno_nm.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.pT_NO_OF_DAYS == toSearch) ? true : false;
            }


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


        }])
})();
