﻿
(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('TrialBalanceViewCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.edt = {};

            var today = new Date();
            $scope.get_date = today;
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();

            $scope.check_today = dd + '-' + mm + '-' + yyyy;

            //$scope.edt = {
            //    from_date: dd + '-' + mm + '-' + yyyy,
            //    to_date: dd + '-' + mm + '-' + yyyy,
            //}



            //  $scope.edt.from_date='30-09-2017'
            //  $scope.edt.to_date = '01-12-2017'
            $scope.main_true = true;
            // $scope.busy = false;

            $scope.assetmaster_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.dept_comp_code;
            $scope.slma_acno_dept_no;
            $scope.finyr;
            $scope.valstatus = false;

            $scope.edit_data = false;
            $scope.display = false;
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.grid = true;
            $scope.grid1 = false;
            $scope.main_table = true;
            //      $scope.edt.view_mode = false;
            $scope.show_report_header = false;

            $scope.ng_if_schedule_group = false;
            $scope.ng_if_show_schedule = false;
            $scope.ng_if_sims_show_budget = false;
            $scope.ng_if_sims_show_budget_varience = false;
            $scope.ng_if_sims_show_varience_percentage = false;
        
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            var user = $rootScope.globals.currentUser.username.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            $http.get(ENV.apiUrl + "api/GLSLQuery/getDate_SL_Query?finance_year=" + finance_year).then(function (res) {
                $scope.getDates = res.data;
                $scope.edt.from_date = $scope.getDates[0].sims_financial_year_start_date;
                
                if ($scope.check_today < $scope.getDates[0].sims_financial_year_end_date && $scope.check_today > $scope.getDates[0].sims_financial_year_start_date) {
                    $scope.edt.to_date = $scope.check_today;
                } else {
                    $scope.edt.to_date = $scope.getDates[0].sims_financial_year_end_date;
                }

                console.log("from date to date :" + $scope.getDates[0].sims_financial_year_start_date + " " + $scope.getDates[0].sims_financial_year_end_date);
            });

            $http.get(ENV.apiUrl + "api/PrintVoucher/getTrialBalanceGroup").then(function (res1) {
                debugger;
                $scope.group_data = res1.data;
                setTimeout(function () {
                    $('#group_id').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%',
                });
                    $("#group_id").multipleSelect("checkAll");
                }, 1000);

            });

            $http.get(ENV.apiUrl + "api/StudentFee/getDecimalPlaces").then(function (getDecimal) {
                debugger;
                $scope.decimal_degit = getDecimal.data;
                $scope.decimal_dsc = $scope.decimal_degit[0].comp_curcy_dec;
            });

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                debugger
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $scope.main_ex = true;
            $scope.mainexpand = function (str) {
                $scope.main_ex = str;
                for (var i = 0; i < $scope.shedule_lst.length; i++) {
                    if (str) {
                        $scope.shedule_lst[i]['glsc_sched_name_show'] = false;

                    }
                    else $scope.shedule_lst[i]['glsc_sched_name_show'] = true;

                }
            }


            $scope.expand = function (str, trail) {
                if (str['glsc_sched_name_show'])
                    str['glsc_sched_name_show'] = false;
                else
                    str['glsc_sched_name_show'] = true;


                $scope.glma_op_bal_amt_dr = 0;
                $scope.glma_op_bal_amt_cr = 0;
                $scope.transaction_amt_dr = 0;
                $scope.transaction_amt_cr = 0;

                $scope.closing_balance_dr = 0;
                $scope.closing_balance_cr = 0;
                $scope.acct_name = 'Total : ';
                $scope.codp_dept_name = '';
                $scope.gsgd_group_code = '';
                $scope.glac_sched_code = '';
                $scope.chk_acc = true;

                for (var i = 0; i < trail.length; i++) {

                    if (str.gsgd_group_code == trail[i].gsgd_group_code && str.glsc_sched_code == trail[i].glac_sched_code) {

                        if (trail[i].acct_name != $scope.acct_name) {
                            $scope.transaction_amt_dr = parseFloat($scope.transaction_amt_dr) + parseFloat(trail[i].transaction_amt_dr);
                            $scope.transaction_amt_cr = parseFloat($scope.transaction_amt_cr) + parseFloat(trail[i].transaction_amt_cr);

                            $scope.glma_op_bal_amt_dr = parseFloat($scope.glma_op_bal_amt_dr) + parseFloat(trail[i].glma_op_bal_amt_dr);
                            $scope.glma_op_bal_amt_cr = parseFloat($scope.glma_op_bal_amt_cr) + parseFloat(trail[i].glma_op_bal_amt_cr);

                            $scope.closing_balance_dr = parseFloat($scope.closing_balance_dr) + parseFloat(trail[i].closing_balance_dr);
                            $scope.closing_balance_cr = parseFloat($scope.closing_balance_cr) + parseFloat(trail[i].closing_balance_cr);
                            $scope.gsgd_group_code = str.gsgd_group_code;
                            $scope.glac_sched_code = str.glsc_sched_code;


                        }
                        else { $scope.chk_acc = false; }
                    }
                }

                var data = {
                    transaction_amt_dr: $scope.transaction_amt_dr,
                    transaction_amt_cr: $scope.transaction_amt_cr,

                    glma_op_bal_amt_dr: $scope.glma_op_bal_amt_dr,
                    glma_op_bal_amt_cr: $scope.glma_op_bal_amt_cr,

                    closing_balance_dr: $scope.closing_balance_dr,
                    closing_balance_cr: $scope.closing_balance_cr,

                    gsgd_group_code: $scope.gsgd_group_code,
                    glac_sched_code: $scope.glac_sched_code,

                    codp_dept_name: $scope.codp_dept_name,
                    acct_name: $scope.acct_name,

                };
                if ($scope.chk_acc)
                    trail.push(data);
            }

            $scope.Search = function () {
                debugger
                $scope.trialbalance = [];
                $scope.busy = true;
                $scope.main_table = true;

                if ($scope.show_schedule_group == true) {
                    $scope.ng_if_schedule_group = true;
                }
                else { $scope.ng_if_schedule_group = false;  }

                if ($scope.show_schedule == true) {
                    $scope.ng_if_show_schedule = true;
                } else {
                    $scope.ng_if_show_schedule = false;
                }
                
                if ($scope.sims_show_budget == true) {
                    $scope.ng_if_sims_show_budget = true;
                } else {
                    $scope.ng_if_sims_show_budget = false;
                }
                
                if ($scope.sims_show_budget_varience == true) {
                    $scope.ng_if_sims_show_budget_varience = true;
                } else {
                    $scope.ng_if_sims_show_budget_varience = false;
                }
                

                if ($scope.sims_show_varience_percentage == true) {
                    $scope.ng_if_sims_show_varience_percentage = true;
                } else {
                    $scope.ng_if_sims_show_varience_percentage = false;
                }


                $scope.printing_user = $rootScope.globals.currentUser.username;
                $scope.selected_groups_arr = '';
                $scope.selected_groups = $('#group_id').multipleSelect('getSelects', 'text');
                for (var i = 0; i < $scope.selected_groups.length; i++) {
                    $scope.selected_groups_arr = $scope.selected_groups_arr + $scope.selected_groups[i]+',';
                }

                console.log($scope.selected_groups_arr);

                $http.get(ENV.apiUrl + "api/PrintVoucher/getTrialBalance?from_date=" + $scope.edt.from_date + "&to_date=" + $scope.edt.to_date + "&viewmode=" + $scope.edt.view_mode + "&comp_code=" + comp_code + "&year=" + finance_year + "&group=" +$scope.temp.sims_group).then(function (res) {
                    $scope.trialbalance = res.data[0];
                    $scope.group_lst = res.data[1];
                    $scope.shedule_lst = res.data[2];

                    $scope.totalItems = $scope.trialbalance.length;
                    $scope.todos = $scope.trialbalance;
                    $scope.makeTodos();

                    //glma_op_bal_amt_dr
                    $scope.first_schedule = $scope.trialbalance[0].glac_sched_code;
                    
                        $scope.glma_op_bal_amt_dr1 = 0;
                        $scope.glma_op_bal_amt_cr1 = 0;
                        $scope.transaction_amt_dr1 = 0;
                        $scope.transaction_amt_cr1 = 0;

                        $scope.closing_balance_dr1 = 0;
                        $scope.closing_balance_cr1 = 0;

                        for (var i = 0; i < $scope.trialbalance.length; i++) {


                      if ($scope.trialbalance[i].acct_name != 'Total : ') {
                                $scope.transaction_amt_dr1 = parseFloat($scope.transaction_amt_dr1) + parseFloat($scope.trialbalance[i].transaction_amt_dr);
                                $scope.transaction_amt_cr1 = parseFloat($scope.transaction_amt_cr1) + parseFloat($scope.trialbalance[i].transaction_amt_cr);

                                $scope.glma_op_bal_amt_dr1 = parseFloat($scope.glma_op_bal_amt_dr1) + parseFloat($scope.trialbalance[i].glma_op_bal_amt_dr);
                                $scope.glma_op_bal_amt_cr1 = parseFloat($scope.glma_op_bal_amt_cr1) + parseFloat($scope.trialbalance[i].glma_op_bal_amt_cr);

                                $scope.closing_balance_dr1 = parseFloat($scope.closing_balance_dr1) + parseFloat($scope.trialbalance[i].closing_balance_dr);
                                $scope.closing_balance_cr1 = parseFloat($scope.closing_balance_cr1) + parseFloat($scope.trialbalance[i].closing_balance_cr);
                            }
                        }

                    if ($scope.ng_if_show_schedule == true) {
                        for (var j = 0; j < $scope.shedule_lst.length; j++) {

                            $scope.glma_op_bal_amt_dr = 0;
                            $scope.glma_op_bal_amt_cr = 0;
                            $scope.transaction_amt_dr = 0;
                            $scope.transaction_amt_cr = 0;

                            $scope.closing_balance_dr = 0;
                            $scope.closing_balance_cr = 0;

                            $scope.glpr_bud_amt = 0;
                            $scope.budget_variance = 0;

                            $scope.acct_name = 'Total : ';
                            $scope.codp_dept_name = '';
                            $scope.gsgd_group_code = '';
                            $scope.glac_sched_code = '';
                            $scope.chk_acc = true;

                            for (var i = 0; i < $scope.trialbalance.length; i++) {

                                if ($scope.shedule_lst[j].gsgd_group_code == $scope.trialbalance[i].gsgd_group_code && $scope.shedule_lst[j].glsc_sched_code == $scope.trialbalance[i].glac_sched_code) {

                                    if ($scope.trialbalance[i].acct_name != $scope.acct_name) {
                                        $scope.transaction_amt_dr = parseFloat($scope.transaction_amt_dr) + parseFloat($scope.trialbalance[i].transaction_amt_dr);
                                        $scope.transaction_amt_cr = parseFloat($scope.transaction_amt_cr) + parseFloat($scope.trialbalance[i].transaction_amt_cr);

                                        $scope.glma_op_bal_amt_dr = parseFloat($scope.glma_op_bal_amt_dr) + parseFloat($scope.trialbalance[i].glma_op_bal_amt_dr);
                                        $scope.glma_op_bal_amt_cr = parseFloat($scope.glma_op_bal_amt_cr) + parseFloat($scope.trialbalance[i].glma_op_bal_amt_cr);

                                        $scope.closing_balance_dr = parseFloat($scope.closing_balance_dr) + parseFloat($scope.trialbalance[i].closing_balance_dr);
                                        $scope.closing_balance_cr = parseFloat($scope.closing_balance_cr) + parseFloat($scope.trialbalance[i].closing_balance_cr);

                                        $scope.glpr_bud_amt = parseFloat($scope.glpr_bud_amt) + parseFloat($scope.trialbalance[i].glpr_bud_amt);
                                        $scope.budget_variance = parseFloat($scope.budget_variance) + parseFloat($scope.trialbalance[i].budget_variance);


                                        $scope.gsgd_group_code = $scope.shedule_lst[j].gsgd_group_code;
                                        $scope.glac_sched_code = $scope.shedule_lst[j].glsc_sched_code;


                                    }
                                    else { $scope.chk_acc = false; }
                                }
                            }

                            var data = {
                                transaction_amt_dr: $scope.transaction_amt_dr,
                                transaction_amt_cr: $scope.transaction_amt_cr,

                                glma_op_bal_amt_dr: $scope.glma_op_bal_amt_dr,
                                glma_op_bal_amt_cr: $scope.glma_op_bal_amt_cr,

                                closing_balance_dr: $scope.closing_balance_dr,
                                closing_balance_cr: $scope.closing_balance_cr,

                                glpr_bud_amt: $scope.glpr_bud_amt,
                                budget_variance: $scope.budget_variance,

                                gsgd_group_code: $scope.gsgd_group_code,
                                glac_sched_code: $scope.glac_sched_code,

                                codp_dept_name: $scope.codp_dept_name,
                                acct_name: $scope.acct_name,

                            };
                            if ($scope.chk_acc)
                                $scope.trialbalance.push(data);
                            console.log("comment_code");

                        }
                    }

                    //Group wise total
                    if ($scope.ng_if_schedule_group == true){
                    for (var j = 0; j < $scope.group_lst.length; j++) {

                        $scope.glma_op_bal_amt_dr2 = 0;
                        $scope.glma_op_bal_amt_cr2 = 0;
                        $scope.transaction_amt_dr2 = 0;
                        $scope.transaction_amt_cr2 = 0;

                        $scope.closing_balance_dr2 = 0;
                        $scope.closing_balance_cr2 = 0;

                        $scope.glpr_bud_amt2 = 0;
                        $scope.budget_variance2 = 0;

                        $scope.acct_name2 = ' Group Total : ';
                        $scope.codp_dept_name2 = '';
                        $scope.gsgd_group_code2 = '';
                        $scope.glac_sched_code2 = '';
                        $scope.chk_acc = true;


                        for (var i = 0; i < $scope.trialbalance.length; i++) {

                            if ($scope.group_lst[j].glsg_group_code == $scope.trialbalance[i].gsgd_group_code && $scope.trialbalance[i].acct_name == 'Total : ') {

                                if ($scope.trialbalance[i].acct_name != $scope.acct_name1) {
                                    $scope.transaction_amt_dr2 = parseFloat($scope.transaction_amt_dr2) + parseFloat($scope.trialbalance[i].transaction_amt_dr);
                                    $scope.transaction_amt_cr2 = parseFloat($scope.transaction_amt_cr2) + parseFloat($scope.trialbalance[i].transaction_amt_cr);

                                    $scope.glma_op_bal_amt_dr2 = parseFloat($scope.glma_op_bal_amt_dr2) + parseFloat($scope.trialbalance[i].glma_op_bal_amt_dr);
                                    $scope.glma_op_bal_amt_cr2 = parseFloat($scope.glma_op_bal_amt_cr2) + parseFloat($scope.trialbalance[i].glma_op_bal_amt_cr);

                                    $scope.closing_balance_dr2 = parseFloat($scope.closing_balance_dr2) + parseFloat($scope.trialbalance[i].closing_balance_dr);
                                    $scope.closing_balance_cr2 = parseFloat($scope.closing_balance_cr2) + parseFloat($scope.trialbalance[i].closing_balance_cr);

                                    $scope.glpr_bud_amt2 = parseFloat($scope.glpr_bud_amt2) + parseFloat($scope.trialbalance[i].glpr_bud_amt);
                                    $scope.budget_variance2= parseFloat($scope.budget_variance2) + parseFloat($scope.trialbalance[i].budget_variance);


                                    $scope.gsgd_group_code2 = $scope.group_lst[j].glsg_group_code;

                                    for (var k = 0; k < $scope.shedule_lst.length; k++) {
                                        if ($scope.shedule_lst[k].gsgd_group_code == $scope.group_lst[j].glsg_group_code) {
                                            $scope.glac_sched_code2 = $scope.shedule_lst[k].glsc_sched_code;
                                        }
                                                          //  $scope.glac_sched_code = $scope.shedule_lst.slice(Math.max($scope.shedule_lst.length - 1, 0))//$scope.shedule_lst[$scope.shedule_lst.length - 1];// _.last($scope.shedule_lst[j].glsc_sched_code);
                                                               //  array.slice(Math.max(array.length - n, 0))
                                    }
                                    

                                }
                                else { $scope.chk_acc = false; }
                            }
                        }

                        var data = {
                            transaction_amt_dr: $scope.transaction_amt_dr2,
                            transaction_amt_cr: $scope.transaction_amt_cr2,

                            glma_op_bal_amt_dr: $scope.glma_op_bal_amt_dr2,
                            glma_op_bal_amt_cr: $scope.glma_op_bal_amt_cr2,

                            closing_balance_dr: $scope.closing_balance_dr2,
                            closing_balance_cr: $scope.closing_balance_cr2,

                            glpr_bud_amt: $scope.glpr_bud_amt2,
                            budget_variance: $scope.budget_variance2,

                            gsgd_group_code: $scope.gsgd_group_code2,
                            glac_sched_code: $scope.glac_sched_code2,

                            codp_dept_name: $scope.codp_dept_name2,
                            acct_name: $scope.acct_name2,

                        };
                        if ($scope.chk_acc)
                            $scope.trialbalance.push(data);
                        console.log("comment_code");

                    }
                    ///////////end

                    //}, 10000);
                    }
                    $scope.busy = false;
                    $scope.main_table = false;
                    console.log("trialbalance",$scope.trialbalance);
                });

                $scope.main_true = false;
                $scope.grid1 = true;
                

            }

            $scope.show_columns = function (str) {
                debugger
                if (str == $scope.first_schedule) {
                    return true;
                }
            }

            $scope.getAccountData = function (str) {
                debugger
                $scope.full_account_code = str.acct_name;
                $scope.timedate_from = $scope.edt.from_date;
                $scope.timedate_to = $scope.edt.to_date;


                var account_code1 = [];
                account_code1 = str.glma_acct_code;
                $scope.opening_bal_cr = str.glma_op_bal_amt_cr;
                $scope.opening_bal_dr = str.glma_op_bal_amt_dr;

                $scope.closing_balance_dr_bottom = str.closing_balance_dr;


                $http.get(ENV.apiUrl + "api/PrintVoucher/getTrialBalanceAccountCode?comp_code=" + comp_code + "&year=" + finance_year + "&account_code=" + account_code1 + "&from_date=" + $scope.edt.from_date + "&to_date=" + $scope.edt.to_date).then(function (res) {
                    $scope.child_window_data = res.data;

                    setTimeout(function () {
                        $('#TrialModal').modal({ backdrop: 'static', keyboard: false });
                    }, 500)


                    $scope.final_total_dr = 0;
                    $scope.final_total_cr = 0;
                    for (var i = 0; i < $scope.child_window_data.length; i++) {
                        $scope.final_total_dr = parseFloat($scope.child_window_data[i].debit) + parseFloat($scope.final_total_dr);
                        $scope.final_total_cr = parseFloat($scope.child_window_data[i].credit) + parseFloat($scope.final_total_cr);

                    }

                    $scope.final_total_dr = parseFloat($scope.final_total_dr) + parseFloat($scope.opening_bal_dr);
                    $scope.final_total_cr = parseFloat($scope.final_total_cr) + parseFloat($scope.opening_bal_cr);

                    $scope.closing_balance_child_win = 0;
                    $scope.closing_balance_child_win = parseFloat($scope.final_total_dr) - parseFloat($scope.final_total_cr);

                    if ($scope.closing_balance_child_win >= 0) {
                        $scope.label_cr_dr = "dr";
                    }
                    else {
                        $scope.label_cr_dr = "cr";
                    }

                });

            }


            $http.get(ENV.apiUrl + "api/PrintVoucher/getPurchasedeatils").then(function (result) {
                $scope.total_counts = result.data;

            });

            setTimeout(function () {
                $('#group_id').change(function () {
                    console.log($(this).val());
                }).multipleSelect({
                    width: '100%'
                });
            }, 1000);



            $scope.report_show = true;
            $scope.Report = function (str) {
                debugger;

                $scope.report_show = false;

                var data = {
                    location: $scope.total_counts[0].fins_appl_form_field_value1,
                    parameter: {
                        comp_detail: str.gltr_comp_code,
                        doc_code: str.gltr_doc_code,
                        doc_no: str.gltr_our_doc_no
                    },
                    state: 'main.FINR15',
                    ready: function () {
                        this.refreshReport();
                    },
                }

                window.localStorage["ReportDetails"] = JSON.stringify(data)

                // });

                //For Report Tool////////////////////////////////////////////////////////////////////////////////


                try {
                    $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                    $scope.location = $scope.rpt.location;
                    $scope.parameter = $scope.rpt.parameter;
                    $scope.state = $scope.rpt.state;
                }
                catch (ex) {
                }
                var s;

                //if ($http.defaults.headers.common['schoolId'] == 'dpsmis')
                //    s = "SimsReports." + $scope.location + ",SimsReports";


                //else
                //    s = "SimsReports." + $scope.location + ",SimsReports" + $http.defaults.headers.common['schoolId'];


                //  window.localStorage["Finn_comp"] = JSON.stringify(data)

                s = "SimsReports." + $scope.location + ",SimsReports";

                var url = window.location.href;
                var domain = url.substring(0, url.indexOf(':'))
                if ($http.defaults.headers.common['schoolId'] == 'sms') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                }
                else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc' || $http.defaults.headers.common['schoolId'] == 'christ') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'imert') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                }

                else {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                }
                console.log(service_url);


                $scope.parameters = {}

                $("#reportViewer1")
                               .telerik_ReportViewer({
                                   //serviceUrl: ENV.apiUrl + "api/reports/",
                                   serviceUrl: service_url,

                                   viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                   scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                   // Zoom in and out the report using the scale
                                   // 1.0 is equal to 100%, i.e. the original size of the report
                                   scale: 1.0,
                                   ready: function () {
                                       //this.refreshReport();
                                   }

                               });





                var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                reportViewer.reportSource({
                    report: s,
                    parameters: $scope.parameter,
                });

                //var rv = $("#reportViewer1").data("telerik_ReportViewer");
                //rv.commands.print.exec();

                setInterval(function () {

                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                    //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });

                }, 1000);


                $timeout(function () {
                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                    //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });


                }, 100)




            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };
            $scope.search = function () {
                debugger
                $scope.todos = $scope.searched($scope.trialbalance, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.trialbalance;
                }
                //$scope.makeTodos();
            }


            //function searchUtil(item, toSearch) {
            //    debugger
            //    /* Search Text in all 2 fields */
            //    return (item.glac_display_acct_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
            //      ) ? true : false;
            //}



            $scope.search_type = function (type) {

                //if (type == null || type == undefined) {
                //    swal({ title: "Alert", text: "Please Select Asset Type", showCloseButton: true, width: 380, });                    
                //}
                //else {
                $http.get(ENV.apiUrl + "api/common/AssetMaster/GetFins_AssetMaster?type=" + $scope.edt.gam_asst_type + "&gam_items=" + $scope.edt.gam_item_no).then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.grid1 = true;
                    $scope.assetmaster_data = res.data;
                    $scope.totalItems = $scope.assetmaster_data.length;
                    $scope.todos = $scope.assetmaster_data;
                    $scope.makeTodos();
                    if (res.data.length == 0) {
                        swal({ title: "Alert", text: "Sorry!! No Data Found", showCloseButton: true, width: 380, });
                        $scope.grid1 = false;
                    }
                });
                // }
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };





            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/common/AssetMaster/GetFins_AssetMaster").then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.assetmaster_data = res.data;
                    $scope.totalItems = $scope.assetmaster_data.length;
                    $scope.todos = $scope.assetmaster_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                });
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }


            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }



            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;

                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.assetmaster_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str;
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
                main.checked = false;
            }





            $scope.back = function () {

                $scope.report_show = true;

            }

            $scope.Report1 = function () {
                var check = true;
                debugger;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " TrialBalanceByAccount.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                          debugger;
                            var blob = new Blob([document.getElementById('percentage').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + " TrialBalanceByAccount" + ".xls");
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Report Not Save", showCloseButton: true, width: 380, })
                }
            }


            $scope.Report2 = function (str) {
                var check = true;
                debugger;
                
                if (check == true) {
                    //$scope.table_show_budget = true;
                    //$scope.table_show_budget_variance = true;
                    //$scope.table_show_budget_percetage = true;
                    //$scope.table_show_shedule_name = true;
                    //$scope.table_show_shedule_group = true;
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " TrialBalance.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger;
                            var blob = new Blob([document.getElementById('dataprint').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + " TrialBalance" + ".xls");
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Report Not Save", showCloseButton: true, width: 380, })
                }
            }

            $scope.print = function () {
                debugger;
                $scope.colsvis = true;
                $scope.show_report_header = true;

                swal({
                    title: "Alert",
                    text: "Do you want to Print?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var docHead = document.head.outerHTML;
                        var printContents = document.getElementById('dataprint').outerHTML;
                        var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
                        var newWin = window.open("", "_blank", winAttr);
                        var writeDoc = newWin.document;
                        writeDoc.open();
                        writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                        writeDoc.close();
                        newWin.focus();
                        $scope.detailsField = false;
                        $scope.colsvis = false;
                        $scope.show_report_header = false;
                        $scope.getsubdetail();

                    }
                });
            };


            $scope.ischeck = function () {
                debugger
                $scope.checklick = function () {
                    var v = document.getElementById('Show_Schedule');
                    if (v.checked == true) {
                        v.checked == false;
                    }

                }
            }





            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }])
})();
