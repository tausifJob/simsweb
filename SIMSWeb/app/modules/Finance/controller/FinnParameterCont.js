﻿
(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [], dataupdate = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('FinnParameterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.insertflag = true;


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

           
            $http.get(ENV.apiUrl + "api/common/FinnParameter/getCompanyName").then(function (res1) {
               
                $scope.display = false;
                $scope.table = true;
                $scope.comp = res1.data;
                $scope.temp =
                    { fins_comp_code: $scope.comp[0].fins_comp_code }
                
            });

            $http.get(ENV.apiUrl + "api/common/FinnParameter/getApplicationName").then(function (res1) {
                $scope.appl_name = res1.data;
            });

            $http.get(ENV.apiUrl + "api/common/FinnParameter/getAllFinnParameter").then(function (res1) {
                $scope.obj1 = res1.data;
                $scope.totalItems = $scope.obj1.length;
                $scope.todos = $scope.obj1;
                $scope.makeTodos();
            });

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.obj1;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                 $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);


                $scope.filteredTodos = $scope.todos.slice(begin, end);

                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    $scope.filteredTodos[i].icon = 'fa fa-plus-circle';
                }
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj1, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj1;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.comn_appl_name_en.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.fins_appl_form_field_value1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.fins_appl_form_field.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.fins_appl_parameter == toSearch) ? true : false;

            }

            $scope.New = function () {

                $scope.disabled = false;
                $scope.readonly = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.insertflag = true;
                $scope.temp = "";
                $scope.temp = {};
                $scope.temp =
                    { fins_comp_code: $scope.comp[0].fins_comp_code }

                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger;
                
                if ($scope.insertflag) {

                    if (Myform) {

                        var data = {
                            fins_comp_code: $scope.temp.fins_comp_code
                        , fins_appl_code: $scope.temp.fins_application_code
                        , fins_appl_form_field: $scope.temp.fins_appl_form_field
                        , fins_appl_parameter: $scope.temp.fins_appl_parameter
                        , fins_appl_form_field_value1: $scope.temp.fins_appl_form_field_value1
                        , fins_appl_form_field_value2: $scope.temp.fins_appl_form_field_value2
                        , fins_appl_form_field_value3: $scope.temp.fins_appl_form_field_value3
                        , fins_appl_form_field_value4: $scope.temp.fins_appl_form_field_value4
                        , opr: 'I'
                        };

                        datasend.push(data);
                        $http.post(ENV.apiUrl + "api/common/FinnParameter/CUDFinnParameter", datasend).then(function (msg) {
                            $scope.msg1 = msg.data;

                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", showCloseButton: true, width: 380, });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ title: "Alert", text: "Record Not Inserted. ", showCloseButton: true, width: 380, });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }

                            $http.get(ENV.apiUrl + "api/common/FinnParameter/getAllFinnParameter").then(function (res1) {
                                $scope.obj1 = res1.data;
                                $scope.totalItems = $scope.obj1.length;
                                $scope.todos = $scope.obj1;
                                $scope.makeTodos();
                            });
                        })
                        datasend = [];
                        $scope.display = false;
                        $scope.table = true;
                    }
                }
            }

            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                $scope.temp.fins_comp_code = "";
                $scope.temp.fins_application_code = "";
                $scope.temp.fins_appl_form_field = "";
                $scope.temp.fins_appl_parameter = "";
                $scope.temp.fins_appl_form_field_value1 = "";
                $scope.temp.fins_appl_form_field_value2 = "";
                $scope.temp.fins_appl_form_field_value3 = "";
                $scope.temp.fins_appl_form_field_value4 = "";
            }

            $scope.edit = function (str) {
                $scope.table = false;
                $scope.disabled = true;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.insertflag = false;

                $scope.temp = {
                    fins_comp_code: str.fins_comp_code
                , fins_application_code: str.fins_appl_code
                , fins_appl_form_field: str.fins_appl_form_field
                , fins_appl_parameter: str.fins_appl_parameter
                , fins_appl_form_field_value1: str.fins_appl_form_field_value1
                , fins_appl_form_field_value2: str.fins_appl_form_field_value2
                , fins_appl_form_field_value3: str.fins_appl_form_field_value3
                , fins_appl_form_field_value4: str.fins_appl_form_field_value4
                , fins_appl_form_field_old: str.fins_appl_form_field
                , fins_appl_parameter_old: str.fins_appl_parameter
                , fins_appl_form_field_value1_old: str.fins_appl_form_field_value1
                , fins_appl_form_field_value2_old: str.fins_appl_form_field_value2
                , fins_appl_form_field_value3_old: str.fins_appl_form_field_value3
                , fins_appl_form_field_value4_old: str.fins_appl_form_field_value4
                }
            }

            var dataupdate = [];
            $scope.update = function () {

                var data = {
                    fins_comp_code: $scope.temp.fins_comp_code
                  , fins_appl_code: $scope.temp.fins_application_code
                  , fins_appl_form_field: $scope.temp.fins_appl_form_field
                  , fins_appl_parameter: $scope.temp.fins_appl_parameter
                  , fins_appl_form_field_value1: $scope.temp.fins_appl_form_field_value1
                  , fins_appl_form_field_value2: $scope.temp.fins_appl_form_field_value2
                  , fins_appl_form_field_value3: $scope.temp.fins_appl_form_field_value3
                  , fins_appl_form_field_value4: $scope.temp.fins_appl_form_field_value4

                  , fins_appl_form_field_old: $scope.temp.fins_appl_form_field_old
                  , fins_appl_parameter_old: $scope.temp.fins_appl_parameter_old
                  , fins_appl_form_field_value1_old: $scope.temp.fins_appl_form_field_value1_old
                  , fins_appl_form_field_value2_old: $scope.temp.fins_appl_form_field_value2_old
                  , fins_appl_form_field_value3_old: $scope.temp.fins_appl_form_field_value3_old
                  , fins_appl_form_field_value4_old: $scope.temp.fins_appl_form_field_value4_old
                  , opr: 'U'
                };
                dataupdate.push(data);

                $http.post(ENV.apiUrl + "api/common/FinnParameter/CUDFinnParameter", dataupdate).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Record Updated Successfully", showCloseButton: true, width: 380, });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ title: "Alert", text: "Record Not Updated. " , showCloseButton: true, width: 380, });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                    $http.get(ENV.apiUrl + "api/common/FinnParameter/getAllFinnParameter").then(function (res1) {
                        $scope.obj1 = res1.data;
                        $scope.totalItems = $scope.obj1.length;
                        $scope.todos = $scope.obj1;
                        $scope.makeTodos();
                    });
                });
                dataupdate = [];
                $scope.display = false;
                $scope.table = true;
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.OkDelete = function () {
                
                var data1 = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            //'fins_comp_code': $scope.filteredTodos[i].fins_comp_code,
                            'fins_appl_code': $scope.filteredTodos[i].fins_appl_code,
                            'fins_appl_form_field': $scope.filteredTodos[i].fins_appl_form_field,
                            'fins_appl_parameter': $scope.filteredTodos[i].fins_appl_parameter,
                            'fins_appl_form_field_value1': $scope.filteredTodos[i].fins_appl_form_field_value1,
                            'opr': "D"
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/common/FinnParameter/CUDFinnParameter", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/common/FinnParameter/getAllFinnParameter").then(function (res1) {
                                                $scope.obj1 = res1.data;
                                                $scope.totalItems = $scope.obj1.length;
                                                $scope.todos = $scope.obj1;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ title: "Alert", text: "Record Not Deleted. " , showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/common/FinnParameter/getAllFinnParameter").then(function (res1) {
                                                $scope.obj1 = res1.data;
                                                $scope.totalItems = $scope.obj1.length;
                                                $scope.todos = $scope.obj1;
                                                $scope.makeTodos();
                                            });
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                data1 = [];
            }

            var dom; $scope.flag = true;
            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='table table-bordered table-hover table-condensed' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                         "<tr style='background-color: #edefef'><td class='semi-bold'>" + "Field Value 1" + "</td> <td class='semi-bold'>" + "Field Value 2" + " </td><td class='semi-bold'>" + "Field Value 3" + "</td><td class='semi-bold'>" + "Field Value 4" + "</td>" +
                        "</tr>" +

                          "<tr><td>" + (info.fins_appl_form_field_value1) + "</td> <td>" + (info.fins_appl_form_field_value2) + " </td><td>" + (info.fins_appl_form_field_value3) + "</td><td>" + (info.fins_appl_form_field_value4) + "</td>" +
                        "</tr>" +

                        " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom); $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }

            };



        }])

})();
