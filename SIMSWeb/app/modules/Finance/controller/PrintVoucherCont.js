﻿
(function () {
    'use strict';
    var chksearch;
    var main1;
    var prdocdata = [];
    var data1 = [];
    var data = [];
    var doc_no;
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('PrintVoucherCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {
            $scope.pagesize = "15";

            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
           
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
            
            //$http.get(ENV.apiUrl + "api/PrintVoucher/getPurchasedeatils").then(function (result) {
            //    $scope.total_counts = result.data;
              
            //});

            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=Single").then(function (res) {
                $scope.final_doc_url = res.data;
            });


            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=cheque").then(function (res1) {
                $scope.cheque_report_url = res1.data;
            });

            

            //$http.get(ENV.apiUrl + "api/PrintVoucher/getprintvoucherForCheque").then(function (result) {
            //    $scope.total_cheque = result.data;
            //});


            $scope.search_textbox = false;
            $scope.disabled_fromdate = false;

            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.countData = [
              { val: 15, data: 15 },
              { val: 50, data: 50 },
              { val: 100, data: 100 },

            ]
            debugger
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');

            $scope.edt =
                          {
                              from_date: $scope.ddMMyyyy,
                              to_date: $scope.ddMMyyyy,
                          }

            $scope.size = function (str) {
                if (str == 15 || str == 50 || str == 100) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }
               
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;  $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                
                $scope.makeTodos();

            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

               

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 15, $scope.maxSize = 15;

            $scope.createdate = function (date) {
                if ($scope.edt.from_date != '') {
                    if ($scope.edt.from_date > $scope.edt.to_date) {
                        swal({ title: 'Please Select future Date', width: 380, height: 100 });
                        $scope.edt.to_date = '';
                    }
                }
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            

            ////checked one by one
            //var ind;
            //var data = [];
            //$scope.checkonebyonedelete = function (str, index) {
            //    debugger;
            //    $("input[type='checkbox']").change(function (e) {
            //        if ($(this).is(":checked")) { //If the checkbox is checked
            //            $(this).closest('tr').addClass("row_selected");
            //            //Add class on checkbox checked
            //            $scope.color = '#edefef';
            //            //str.status = true;
            //        }
            //        else {
            //            $(this).closest('tr').removeClass("row_selected");
            //            //Remove class on checkbox uncheck
            //            $scope.color = '#edefef';
            //            //str.status = false;
            //        }
            //    });
            //    str.status = str.datastatus;
            //    ind = index;
            //    //data = str;
            //}


            $scope.checkonebyoneselect = function () {

                $("input[type='checkbox']").change(function (e) {

                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                alert(main);
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");

                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            //check all 
            $scope.CheckAllChecked = function () {
            
                debugger;
                var main = document.getElementById('mainchk');
                //alert(main);
                if (main.checked == true) {
                  
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].gltd_final_doc_no + i);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].gltd_final_doc_no + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }



            $scope.Show = function (from_date, to_date, search, comp_cd) {
                debugger
                if ($scope.edt.search == undefined || $scope.edt.search == "") {
                    $scope.edt.search = '';
                }
                $scope.table1 = true;
                $scope.filteredTodos = [];
                $scope.pagesize = '15';
                $scope.currentPage = 1;
                $scope.numPerPage = 15;
                $http.get(ENV.apiUrl + "api/PrintVoucher/getAllVoucherForPrint?from_date=" + $scope.edt.from_date + "&to_date=" + $scope.edt.to_date + "&search=" + $scope.edt.search + "&comp_code=" + comp_code).then(function (AllVoucherForPrint_Data) {
                    $scope.table1 = true;
                    $scope.VoucherForPrint_Data = AllVoucherForPrint_Data.data;
                    if ($scope.VoucherForPrint_Data.length > 0) {
                        $scope.pager = true;

                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.VoucherForPrint_Data.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.VoucherForPrint_Data.length, data: 'All' })
                        }

                        $scope.totalItems = $scope.VoucherForPrint_Data.length;
                        $scope.todos = $scope.VoucherForPrint_Data;
                        $scope.makeTodos();
                        $scope.table1 = true;
                        $scope.page_index = true;
                        $scope.print_vouchers = true;
                        $scope.currentPage = 1;
                    }
                    else {
                        swal({ title: "Alert", text: "Vouchers Not Found", width: 300, height: 200 });
                        $scope.page_index = false;
                        $scope.print_vouchers = false;
                        $scope.pager = false;
                        $scope.filteredTodos = [];
                    }
                });
            }
            document.onkeydown = function (event) {
                debugger
                event = event || window.event;
                if (event.keyCode == 27) {
                    $('#feemodal').modal('hide');
                }
            }
            $scope.report_show = true;
            $scope.Report = function (str) {
                debugger;
                $http.get(ENV.apiUrl + "api/JVCreation/GetDocReportUrl?opr=J&sub_opr=Doc_URL&comp_code=" + comp_code + "&fyear=" + finance_year + "&condition=" + str.gltd_doc_code + "&condition1=" + '' + "&condition2=" + '' + "&condition3=" + '' + "&condition4=" + '' + "&condition5=" + '').then(function (res) {
                    $scope.final_doc_url = res.data;
                //});
                $('#feemodal').modal('show');
                $scope.report_show = false;

               // $http.get(ENV.apiUrl + "api/StudentFee/GetReceipt").then(function (res) {
                   // $scope.reportparameter = res.data;
                    var data = {
                        location: $scope.final_doc_url,
                        parameter: {
                        comp_detail:comp_code,
                        doc_code:str.gltd_doc_code,
                        doc_no:str.gltd_final_doc_no

                        },
                        state: 'main.FINS15',
                        ready: function () {
                            this.refreshReport();
                        },
                    }
                
                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                    //$state.go('main.ReportCardParameter')
                try {
                    $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                    $scope.location = $scope.rpt.location;
                    $scope.parameter = $scope.rpt.parameter;
                    $scope.state = $scope.rpt.state;
                }
                catch (ex) {
                }
                    var s;
                    s = "SimsReports." + $scope.location + ",SimsReports";
                    var url = window.location.href;
                    var domain = url.substring(0, url.indexOf(':'))
                    if ($http.defaults.headers.common['schoolId'] == 'sms') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'imert') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'ibserp') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.ibskuwait.com/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.ibskuwait.com/report/api/reports/';
                    }


                    else {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    }
                    console.log(service_url);
                    $scope.parameters = {}

                    $("#reportViewer1")
                                   .telerik_ReportViewer({                                      
                                       serviceUrl: service_url,
                                       viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                       scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                       // Zoom in and out the report using the scale
                                       // 1.0 is equal to 100%, i.e. the original size of the report
                                       scale: 1.0,
                                       ready: function () {
                                           //this.refreshReport();
                                       }

                                   });
                    var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                    reportViewer.reportSource({
                        report: s,
                        parameters: $scope.parameter,
                    });

                    setInterval(function () {

                        $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                        $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                    }, 1000);

                    $timeout(function () {
                        $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                        $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                    }, 100)
                });
            }

            $scope.print_PV = function (str) {
                debugger
                $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=PV").then(function (res1) {
                    $scope.paymentVoucher_report_url = res1.data;
                    $('#feemodal').modal('show');
                    $scope.report_show = false;

                    // $http.get(ENV.apiUrl + "api/StudentFee/GetReceipt").then(function (res) {
                    // $scope.reportparameter = res.data;
                    var data = {
                        location: $scope.paymentVoucher_report_url,
                        parameter: {
                            comp_detail: comp_code,
                            doc_code: str.gltd_doc_code,
                            doc_no: str.gltd_final_doc_no

                        },
                        state: 'main.FINS15',
                        ready: function () {
                            this.refreshReport();
                        },
                    }

                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                    //$state.go('main.ReportCardParameter')
                    try {
                        $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                        $scope.location = $scope.rpt.location;
                        $scope.parameter = $scope.rpt.parameter;
                        $scope.state = $scope.rpt.state;
                    }
                    catch (ex) {
                    }
                    var s;
                    s = "SimsReports." + $scope.location + ",SimsReports";
                    var url = window.location.href;
                    var domain = url.substring(0, url.indexOf(':'))
                    if ($http.defaults.headers.common['schoolId'] == 'sms') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'imert') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'ibserp') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.ibskuwait.com/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.ibskuwait.com/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'staging2') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/report/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/report/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'staging') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/report/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/report/';
                    }

                    else {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    }
                    console.log(service_url);
                    $scope.parameters = {}

                    $("#reportViewer1")
                                   .telerik_ReportViewer({
                                       serviceUrl: service_url,
                                       viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                       scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                       // Zoom in and out the report using the scale
                                       // 1.0 is equal to 100%, i.e. the original size of the report
                                       scale: 1.0,
                                       ready: function () {
                                           //this.refreshReport();
                                       }

                                   });
                    var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                    reportViewer.reportSource({
                        report: s,
                        parameters: $scope.parameter,
                    });

                    setInterval(function () {

                        $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                        $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                    }, 1000);

                    $timeout(function () {
                        $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                        $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                    }, 100)

                });
            }
            $scope.print_cheque = function (str) {
                debugger
                $('#feemodal').modal('show');
                $scope.report_show = false;
                var data = {
                    location: $scope.cheque_report_url,
                    parameter: {
                        //  comp_details: comp_code,
                        doc_code: str.gltd_doc_code,
                        doc_no: str.gltd_final_doc_no
                    },

                    state: 'main.FINS15'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                // $state.go('main.ReportCardParameter')
                try {
                    $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                    $scope.location = $scope.rpt.location;
                    $scope.parameter = $scope.rpt.parameter;
                    $scope.state = $scope.rpt.state;
                }
                catch (ex) {
                }
                var s;
                s = "SimsReports." + $scope.location + ",SimsReports";
                var url = window.location.href;
                var domain = url.substring(0, url.indexOf(':'))
                if ($http.defaults.headers.common['schoolId'] == 'sms') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                }
                else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'ibserp') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.ibskuwait.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.ibskuwait.com/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'staging2') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'staging') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'imert') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                }

                else {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                }
                console.log(service_url);
                $scope.parameters = {}

                $("#reportViewer1")
                               .telerik_ReportViewer({
                                   serviceUrl: service_url,
                                   viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                   scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                   // Zoom in and out the report using the scale
                                   // 1.0 is equal to 100%, i.e. the original size of the report
                                   scale: 1.0,
                                   ready: function () {
                                       //this.refreshReport();
                                   }

                               });
                var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                reportViewer.reportSource({
                    report: s,
                    parameters: $scope.parameter,
                });

                setInterval(function () {

                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                }, 1000);

                $timeout(function () {
                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                }, 100)

            }



            $scope.check_searchnull = function () {
                chksearch = document.getElementById("chkmarks1");

                if (chksearch.checked == true) {
                    $scope.search_textbox = true;
                }
                else {
                    $scope.search_textbox = false;
                }
            }

            $scope.check_fromdate = function () {
                main1 = document.getElementById("chkmarks");

                if (main1.checked == true) {
                    $scope.edt.from_date = '';
                    $scope.disabled_fromdate = true;
                }
                else {
                    $scope.disabled_fromdate = false;
                    $scope.edt =
                          {
                              from_date: $scope.ddMMyyyy,
                              to_date: $scope.ddMMyyyy,
                          }
                }
            }


            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=Final").then(function (res) {
                $scope.multi_print_final_doc_url = res.data;
            });

            $scope.report_show = true;

            $scope.print_multiple_voucher = function () {
                debugger;
                var cnt = 0;


                //$scope.doc_cd_final_doc_no = '';
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].chk_multi_print == true) {
                        //     $scope.doc_cd_final_doc_no = $scope.doc_cd_final_doc_no + $scope.filteredTodos[i].gltd_doc_code + $scope.filteredTodos[i].gltd_final_doc_no + ',';
               
                        cnt = cnt+1;
                    }

                    }
               
                if (cnt <= 1) {

                    swal({ title: "Alert", text: "please select more than one record", width: 300, height: 200 });

                }
                else 
                {
                    
                    $('#feemodal').modal('show');
                    $scope.report_show = false;
                    $scope.doc_cd_final_doc_no = '';
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        if ($scope.filteredTodos[i].chk_multi_print == true) {
                            $scope.doc_cd_final_doc_no = $scope.doc_cd_final_doc_no + $scope.filteredTodos[i].gltd_doc_code + $scope.filteredTodos[i].gltd_final_doc_no + ',';
                        }
                    }
                    var data = {
                        location: $scope.multi_print_final_doc_url,
                        parameter: { comp_detail: comp_code, doc: $scope.doc_cd_final_doc_no },
                        state: 'main.FINS15'
                    }

                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                    // $state.go('main.ReportCardParameter')

                    try {
                        $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                        $scope.location = $scope.rpt.location;
                        $scope.parameter = $scope.rpt.parameter;
                        $scope.state = $scope.rpt.state;
                    }
                    catch (ex) {
                    }
                    var s;

                    //if ($http.defaults.headers.common['schoolId'] == 'dpsmis')
                    //    s = "SimsReports." + $scope.location + ",SimsReports";


                    //else
                    //    s = "SimsReports." + $scope.location + ",SimsReports" + $http.defaults.headers.common['schoolId'];


                    //  window.localStorage["Finn_comp"] = JSON.stringify(data)

                    s = "SimsReports." + $scope.location + ",SimsReports";

                    var url = window.location.href;
                    var domain = url.substring(0, url.indexOf(':'))
                    if ($http.defaults.headers.common['schoolId'] == 'sms') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'imert') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'staging2') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/reportsss/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/reportsss/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'staging') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/reportsss/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/reportsss/api/reports/';
                    }
                   
                    else {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    }
                    console.log(service_url);


                    $scope.parameters = {}
                    debugger;
                    $("#reportViewer1")
                                   .telerik_ReportViewer({
                                       //serviceUrl: ENV.apiUrl + "api/reports/",
                                       serviceUrl: service_url,

                                       viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                       scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                       // Zoom in and out the report using the scale
                                       // 1.0 is equal to 100%, i.e. the original size of the report
                                       scale: 1.0,
                                       ready: function () {
                                           //this.refreshReport();
                                       }

                                   });





                    var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                    reportViewer.reportSource({
                        report: s,
                        parameters: $scope.parameter,
                    });

                    //var rv = $("#reportViewer1").data("telerik_ReportViewer");
                    //rv.commands.print.exec();

                    setInterval(function () {

                        $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                        $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                        //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });

                    }, 1000);


                    $timeout(function () {
                        $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                        $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                        //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });


                    }, 100)
                }
            }
            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                
                $('input[type="text"]', $(this).parent()).focus();
            });
        }])
})();
