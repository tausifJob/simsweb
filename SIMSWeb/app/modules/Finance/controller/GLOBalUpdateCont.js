﻿
(function () {
    'use strict';

    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('GLOBalUpdateCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.grid = true;
            $scope.btn_save = true;
            var main = '';
            $scope.edt = {};


            setTimeout(function () {
                $("#account_codes1").select2();
            }, 100);
            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            console.log($scope.finnDetail)
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;



            $scope.setCheck = function (str,str1) {
                debugger
                var main = document.getElementById('global-' + str);
                if(str1 != undefined) {
                    if (str1 != ''){
                        main.checked = true;
                    }
                }
                else {
                    if( main.checked == true)
                        main.checked = false;
                }
            }

            $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetAll_glmaster_Department_Code?co_code=" + comp_code + "&year=" + finance_year).then(function (GetAll_glmaster_Department_Code) {
                $scope.GetAll_glmaster_Department_Code = GetAll_glmaster_Department_Code.data;
            });

            $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetAll_ledgercode?co_code=" + comp_code + "&year=" + finance_year).then(function (GetAll_ledgercode) {
                $scope.GetAll_ledgercode = GetAll_ledgercode.data;
            });


            $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetAll_partyclass_Code?co_code=" + comp_code + "&year=" + finance_year).then(function (GetAll_partyclass_Code) {
                $scope.GetAll_partyclass_Code = GetAll_partyclass_Code.data;
            });

            $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetAll_Accountcode_openingBalance?co_code=" + comp_code + "&year=" + finance_year).then(function (GetAll_Accountcode) {
                $scope.GetAll_Accountcode = GetAll_Accountcode.data;
            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;


            //$http.get(ENV.apiUrl + "api/GLAccountsCreateView/getGLOpenidgBalance?co_code=" + comp_code + "&year=" + finance_year).then(function (getGL) {
            //    debugger
            //    if (getGL.data.length > 0) {
            //        $scope.obj = getGL.data;
            //        $scope.totalItems = $scope.obj.length;
            //        $scope.todos = $scope.obj;
            //        $scope.makeTodos();
            //    }
            //    else {
            //        swal({ title: "Alert", text: "Report Not Present", showCloseButton: true, width: 380, })
            //    }
            //});

            $scope.alldata = function () {
                debugger
                $http.get(ENV.apiUrl + "api/GLAccountsCreateView/getGLOpenidgBalance?co_code=" + comp_code + "&year=" + finance_year + "&dept_no=" + $scope.edt.glma_dept_no + "&account_code=" + $scope.edt.glma_acct_code).then(function (getGL) {
                    debugger
                    if (getGL.data.length > 0) {
                        $scope.obj = getGL.data;
                        $scope.totalItems = $scope.obj.length;
                        $scope.todos = $scope.obj;
                        $scope.makeTodos();
                    }
                    else {
                        swal({ title: "Alert", text: "Record Not Present", showCloseButton: true, width: 380, })
                    }
                });
            }

            $scope.Save = function () {
                debugger
                var Savedata = [];
                var system_date=new Date();
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    // if ($scope.filteredTodos[i].status == true) {
                    var v = document.getElementById("global-" + i);
                    if (v.checked == true) {
                        var deleteintcode = ({
                            'glma_comp_code': comp_code,
                            'glma_year': finance_year,
                            'glma_dept_no': $scope.filteredTodos[i].glma_dept_no,
                            'glma_acct_code': $scope.filteredTodos[i].glma_acct_code,
                            'glma_op_bal_amt': $scope.filteredTodos[i].glma_op_bal_amt,
                            'glma_op_bal_amt_old_val': $scope.filteredTodos[i].glma_op_bal_amt_old_val,
                            'login_user': user,
                            'current_date':system_date,
                            'opr': 'L',
                        });
                        $scope.insert = true;
                        Savedata.push(deleteintcode);
                    }
                }
                if ($scope.insert) {
                    $http.post(ENV.apiUrl + "api/GLAccountsCreateView/CUDGLOpeningBAl", Savedata).then(function (msg) {
                        debugger;
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Opening Balance Updated Successfully", width: 380, height: 200 });
                            $scope.busy = false;
                            deleteintcode = [];
                            Savedata = [];
                            $scope.alldata();
                        }
                        else {
                            swal({ title: "Alert", text: "Opening Balance Not Updated ", width: 380, height: 200 });
                            $scope.busy = false;
                            $scope.alldata
                        }

                    });
                }


            }
            



            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.obj;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.Reset = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
            }

            $scope.btn_cancel_Click = function () {
                $scope.grid = true;
                $scope.display = false;

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            var data = [];

           

            var deletedata = [];

           
            //$scope.table_row_Click = function (str) {
            //    debugger
            //    $scope.btn_save = false;
            //    $scope.grid = false;



            //    $scope.edt = {
            //        glma_dept_no: str.glma_dept_no,
            //        glma_acct_code: str.glma_acct_code,
            //        glma_pty_class: str.glma_pty_class,
            //        glma_ldgr_code: str.glma_ldgr_name,
            //        glma_op_bal_amt: str.glma_op_bal_amt,
            //        glma_qty_flag: str.glma_qty_flag,
            //        glma_status: str.glma_status,
            //        glma_fc_flag: str.glma_fc_flag,
            //    }


            //    //  $scope.edt = str;
            //    console.log($scope.edt);
            //}

         

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.codp_dept_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.glma_acct_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("global-"+i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("global-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }


            $scope.btn_New_Click = function () {
                $scope.grid = false;
                $scope.btn_save = true;

                $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetAll_glmaster_Department_Code?co_code=" + comp_code + "&year=" + finance_year).then(function (GetAll_glmaster_Department_Code) {
                    $scope.GetAll_glmaster_Department_Code = GetAll_glmaster_Department_Code.data;
                });

                $scope.edt = "";
                $scope.edt = {
                    glma_op_bal_amt: '0',
                    glpr_bud_amt: '0',
                    account_budget_quntity: '0'
                };
            }

        }])
})();