﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SLQueryCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.edt = "";
            $scope.operation = true;
            $scope.table1 = false;
            $scope.addbtn = true;
            $scope.print_pdf = false;
            $scope.btnDelete = false;
            $scope.users = [];
            var comp_code = "1";
            $scope.chkcost_Ac = false;
            $scope.data = [];
            $scope.Bankdetails = "";
            $scope.Bankdetails = [];
            $scope.chkcost = false;
            $scope.cost_combo = false;
            $scope.crAmount = 0;
            $scope.TotalAmount = 0;
            $scope.drAmount = 0;
            $scope.edt5 = [];
            
            var user = $rootScope.globals.currentUser.username.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=Single").then(function (res) {
                $scope.final_doc_url = res.data;
                });

            debugger;

            $http.get(ENV.apiUrl + "api/GLSLQuery/getDate_SL_Query?finance_year=" + finance_year).then(function (res) {
                $scope.getDates = res.data;

               /* $scope.edt = {
                    FromDate: $scope.getDates[0].sims_financial_year_start_date,
                    ToDate: $scope.getDates[0].sims_financial_year_end_date
                }*/

                if ($scope.getDates.length > 0) {
                    $scope.sDate = $scope.getDates[0].sims_financial_year_start_date;
                    $scope.eDate = $scope.getDates[0].sims_financial_year_end_date;

                    $scope.edt = {
                        FromDate: $scope.getDates[0].sims_financial_year_start_date,
                        ToDate: $scope.getDates[0].sims_financial_year_end_date
                    }

                    $(document).ready(function () {
                        $("#from_date").kendoDatePicker({

                            format: "dd-MM-yyyy",
                            min: new Date(moment($scope.sDate, 'DD-MM-YYYY').format('YYYY-MM-DD')),
                            max: new Date(moment($scope.eDate, 'DD-MM-YYYY').format('YYYY-MM-DD'))


                        });
                    });


                    $(document).ready(function () {
                        $("#to_date").kendoDatePicker({

                            format: "dd-MM-yyyy",
                            min: new Date(moment($scope.sDate, 'DD-MM-YYYY').format('YYYY-MM-DD')),
                            max: new Date(moment($scope.eDate, 'DD-MM-YYYY').format('YYYY-MM-DD'))

                            //format: "dd/MM/yyyy"
                            //format: "yyyy/MM/dd"
                        });
                    });
                }

                else {
                    $(document).ready(function () {
                        $("#from_date").kendoDatePicker({

                            format: "dd-MM-yyyy",


                        });
                    });


                    $(document).ready(function () {
                        $("#to_date").kendoDatePicker({

                            format: "dd-MM-yyyy",

                            //format: "dd/MM/yyyy"
                            //format: "yyyy/MM/dd"
                        });
                    });

                }

                console.log($scope.getDates)
            });

            $scope.positivevalue = function (str) {
                return Math.abs(str)
            }


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;  $scope.makeTodos();
            }

            //$http.get(ENV.apiUrl + "api/BankPayment/getDocCode?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {

            //    $scope.geDocCode = res.data;
            //    for (var i = 0; i < $scope.geDocCode.length; i++) {
            //        if ($scope.geDocCode[i].gltd_doc_code == "BP") {
            //            $scope.getDocCode = $scope.geDocCode[i].gltd_doc_code;
            //        }
            //    }
            //    $scope.temp3 = {
            //        gltd_doc_code: $scope.getDocCode
            //    }
            //    $http.get(ENV.apiUrl + "api/BankPayment/GetAllCashBankGLMasterAccountNo?cmp_cd=" + comp_code + "&financialyear=" + finance_year + "&doc_cd=" + $scope.getDocCode + "&username=" + user).then(function (docstatus2) {
                    
            //        $scope.bankname = docstatus2.data
            //        $scope.edt5['master_acno'] = docstatus2.data[0];
            //        $scope.getAccountName(docstatus2.data[0].master_ac_cdname)

            //        //    $scope.edt5={

            //        //        master_acno: $scope.bankname[0].master_acno,
            //        //        master_ac_cdname: $scope.bankname[0].master_ac_cdname
            //        //}
            //        //    $scope.getAccountName($scope.edt5.master_acno);
            //    });
            //});

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

               
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };




            /*$scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
              /*      return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.accountDatashow, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.accountDatashow;
                }
                $scope.makeTodos();
                //main.checked = false;
               // $scope.CheckAllChecked();

            }


            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                /*return (item.sltr_our_doc_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sltr_doc_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sltr_doc_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sltr_pstng_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sltr_user_id.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    
                        item.acno == toSearch) ? true : false;
            }*/


           

            $scope.CheckPrintReport = function () {


                var data = {
                    location: $scope.final_doc_url,
                    parameter: {
                        doc_code: $scope.temp3.gltd_doc_code,
                        doc_no: $scope.Docno,

                    },

                    state: 'main.Fin141'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')


            }
            $scope.New = function () {
                $scope.operation = true;
                $scope.addbtn = true;
                $scope.updatebtn = false;
                $scope.Delete = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

          
            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
           /* $scope.sdate = yyyy + '-' + mm + '-' + dd;
            $scope.edate = yyyy + '-' + mm + '-' + dd;
            $scope.edt4 = {
                DocDate: yyyy + '-' + mm + '-' + dd,
                PostDate: yyyy + '-' + mm + '-' + dd,
                ChequeDate: yyyy + '-' + mm + '-' + dd,
            }*/

            $scope.getSLAccNo = function (slcode) {

                $scope.getAllAccNos = [];
                //var acconcode = $scope.getAllAccNos.gldd_acct_code;
                //if (slcode == "00") {
                //    $scope.GetAllGLAcc(acconcode, comp_code);
                //}
                //else {

                    $http.get(ENV.apiUrl + "api/JVCreation/GetSLAccNumber?ldgr_code=" + slcode + "&cmp_cd=" + comp_code + "&year=" + finance_year).then(function (docstatus3) {
                        // $scope.sllc_ldgr_code = slcode;
                        $scope.getAllAccNos = docstatus3.data;
                        //$scope.slacno = docstatus3.data;
                    });

                    
                //}
             //   $("#cmb_acc_Code3").select2("val", cmbvalue);
            }

            setTimeout(function () {
                $("#cmb_acc_Code3").select2();

            }, 100);

          
            $http.get(ENV.apiUrl + "api/JVCreation/getDocCodeJV?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                $scope.geDocCode_jv = res.data;
                console.log('doc code', $scope.geDocCode_jv);
            });

            $scope.block2 = [];
            $scope.getDepartcode = function (ledgercode, accountcode) {
                
                $scope.block2 = [];
                debugger;
                $http.get(ENV.apiUrl + "api/GLSLQuery/getdataAccountChange?compcode=" + comp_code + "&financial_year=" + finance_year + "&ledger_code=" + ledgercode + "&slma_acno=" + accountcode).then(function (res) {

                    $scope.accountData = res.data;

                    if (res.data.length > 0)
                    {
                        $scope.block2.slma_yob_amt = res.data[0].slma_yob_amt;
                        $scope.block2.slma_cr_limit = res.data[0].slma_cr_limit;
                        $scope.block2.slma_credit_prd = res.data[0].slma_credit_prd;
                        $scope.block2.slma_stop_cr_ind = res.data[0].slma_stop_cr_ind;
                        $scope.block2.slma_pty_bank_acno = res.data[0].slma_pty_bank_acno;
                        $scope.block2.sltr_attr1 = res.data[0].sltr_attr1;
                        $scope.block2.sltr_attr2 = res.data[0].sltr_attr2;
                        $scope.block2.sltr_attr3 = res.data[0].sltr_attr3;
                        $scope.block2.sltr_attr4 = res.data[0].sltr_attr4;
                        $scope.block2.coad_pty_short_name = res.data[0].coad_pty_short_name;
                        $scope.block2.coad_dept_no = res.data[0].coad_dept_no;
                        $scope.block2.codp_dept_name = res.data[0].codp_dept_name;

                    }

                    });



            }

            var close_amt=0;
            $scope.Show = function (ledgercode, accountcode, doc_code, from_date, todate) {

                $scope.acct_name = $("#cmb_acc_Code3 option:selected").text();
                $scope.from = $scope.edt.FromDate;
                $scope.to = $scope.edt.ToDate;

                debugger;
                $http.get(ENV.apiUrl + "api/GLSLQuery/Get_SL_opening_bal?comp_code=" + comp_code + "&fyear=" + finance_year + "&ledgercode=" + ledgercode + "&acct_code=" + accountcode + "&from_date=" + from_date).then(function (res) {
                    $scope.OpBalanceAmt = res.data;
                    if ($scope.OpBalanceAmt >= 0) {
                        $scope.opbal = $scope.OpBalanceAmt;
                        $scope.cr = ' dr';
                    }
                    else if ($scope.OpBalanceAmt < 0) {
                        $scope.opbal = Math.abs($scope.OpBalanceAmt);
                        $scope.cr = ' cr';
                    }
                    else {
                        $scope.opbal = $scope.OpBalanceAmt;
                    }
                   

                });

                $scope.close_amt = 0;
                $scope.descvari = '';

                if (ledgercode != undefined && accountcode != undefined) {

                    $('#loader').modal({ backdrop: 'static', keyboard: false });

                    $http.get(ENV.apiUrl + "api/GLSLQuery/getdataAllShow?compcode=" + comp_code + "&ledger_code=" + ledgercode + "&slma_acno=" + accountcode + "&doc_code=" + doc_code + "&from_date=" + from_date + "&to_date=" + todate).then(function (res) {
                        $scope.accountDatashow = res.data;
                        $scope.totalItems = $scope.accountDatashow.length;
                        $scope.todos = $scope.accountDatashow;
                        $scope.makeTodos();
                        console.log($scope.accountDatashow)
                        
                        if ($scope.accountDatashow.length > 0) {
                            $scope.table1 = true;
                            $scope.drAmount = 0;
                            $scope.crAmount = 0;
                           
                            for (var i = 0; i < $scope.accountDatashow.length; i++) {
                                $scope.drAmount = parseFloat($scope.drAmount) + parseFloat($scope.accountDatashow[i].sltr_dr_amount);
                                $scope.crAmount = parseFloat($scope.crAmount) + parseFloat($scope.accountDatashow[i].sltr_cr_amount);
                               
                            }




                            if ($scope.drAmount != 0) {
                                $scope.drAmount = parseFloat($scope.drAmount).toFixed(3);
                            }

                            if ($scope.crAmount != 0) {
                                $scope.crAmount = parseFloat($scope.crAmount).toFixed(3);
                            }

                            
                            debugger;
                            if ($scope.crAmount != 0 || $scope.drAmount != 0) {
                                $scope.close_amt = parseFloat($scope.drAmount) - parseFloat($scope.crAmount);

                                if ($scope.OpBalanceAmt >= 0) {
                                    $scope.close_amt = (parseFloat($scope.close_amt) + parseFloat($scope.OpBalanceAmt)).toFixed(3);
                                }
                                else if ($scope.OpBalanceAmt < 0) {
                                    $scope.close_amt = (parseFloat($scope.close_amt) + parseFloat($scope.OpBalanceAmt)).toFixed(3);
                                }


                                if ($scope.close_amt >= 0) {
                                    $scope.closebal = parseFloat($scope.close_amt).toFixed(3);
                                    $scope.dr = ' dr';
                                }
                                else if ($scope.close_amt < 0) {
                                    $scope.closebal = Math.abs($scope.close_amt).toFixed(3);
                                    $scope.dr = ' cr';
                                }
                                else
                                    $scope.closebal = 0;
                            }

                            $('#loader').modal('hide');
                            $(".modal-backdrop").removeClass("modal-backdrop");

                        }
                        else {
                            $scope.table1 = false;
                            swal({ title: "Alert", text: "Data Not Found", showCloseButton: true, width: 300, height: 200 });
                            $scope.drAmount = 0;
                            $scope.crAmount = 0;
                            if ($scope.opbal >= 0) {
                                
                                $scope.closebal = $scope.opbal;
                                $scope.descvari = 'dr';
                                $scope.cr = ' dr';
                            }
                            else if ($scope.opbal < 0) {
                               
                                $scope.closebal = Math.abs($scope.opbal);
                                $scope.descvari = 'cr';
                                $scope.cr = ' cr';
                            }
                            else {
                              
                                $scope.closebal = $scope.opbal;
                            }
                            $('#loader').modal('hide');
                            $(".modal-backdrop").removeClass("modal-backdrop");

                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select LedgerCode and Account Number", showCloseButton: true, width: 300, height: 200 });
                }
            }
            //Cheque Detail
            $scope.Cheque_Details = function (ledgercode, accountcode)
            {
                if (ledgercode != undefined && accountcode != undefined) {
                    
                    $http.get(ENV.apiUrl + "api/GLSLQuery/getChequeDetails?compcode=" + comp_code + "&ledger_code=" + ledgercode + "&slma_acno=" + accountcode).then(function (res) {
                        $scope.ChequeDatashow = res.data;
                        })
                    $('#MyModal2').modal('show');
                }
                else {
                        swal({ title: "Alert", text: "Please Select LedgerCode and Account Number", showCloseButton: true, width: 300, height: 200 });
                }
            }

            //Bill Details
            $scope.Bill_Details = function (ledgercode, accountcode) {
                
                if (ledgercode != undefined && accountcode != undefined) {
                    $http.get(ENV.apiUrl + "api/GLSLQuery/getBillDetails?compcode=" + comp_code + "&ledger_code=" + ledgercode + "&slma_acno=" + accountcode).then(function (res) {
                        $scope.billDatashow = res.data;
                        })
                    $('#MyModal3').modal('show');
                }
                else {
                    swal({ title: "Alert", text: "Please Select LedgerCode and Account Number", showCloseButton: true, width: 300, height: 200 });
                }
            }


            //Fill Combo Ledger
            $http.get(ENV.apiUrl + "api/BankPayment/GetLedgerNumber?financialyear=" + finance_year + "&comp_code=" + comp_code).then(function (docstatus2) {
                $scope.LdgrCode=[];
                //$scope.LdgrCode = docstatus2.data;

                for (var i = 0; i < docstatus2.data.length; i++)
                {
                    //if (i != 0)
                    if(docstatus2.data[i].sllc_ldgr_code!="00")
                    {

                        $scope.LdgrCode.push(docstatus2.data[i]);
                    }
                }

                });



            $scope.onChange1 = function () {
                
                var check_date = moment($scope.eDate, 'DD-MM-YYYY').format('YYYY-MM-DD')
                var check_date1 = moment($scope.edt.ToDate, 'DD-MM-YYYY').format('YYYY-MM-DD')

                if (moment(check_date1) > moment(check_date)) {
                    // this.value() = '';
                    $scope.edt.FromDate = '';
                    swal({ title: "Alert", text: "Please select date between " + $scope.sDate + " to " + $scope.eDate + " in financial year", showCloseButton: true, width: 380, })
                }



            }

            $scope.onChange = function () {
                debugger
                var check_date = moment($scope.sDate, 'DD-MM-YYYY').format('YYYY-MM-DD')
                var check_date1 = moment($scope.edt.FromDate, 'DD-MM-YYYY').format('YYYY-MM-DD')

                if (moment(check_date1) < moment(check_date)) {

                    $scope.edt.FromDate = '';
                    swal({ title: "Alert", text: "Please select date between " + $scope.sDate + " to " + $scope.eDate + " in financial year", showCloseButton: true, width: 380, })

                }

            }
            $scope.Fetchdata = function (cmp_cd, docd, final_doc_no) {
                debugger
                $scope.docCode1 = docd;
                $scope.provNo1 = final_doc_no;

                $('#myModal4').modal('show')
                //$scope.prdocdata = [];

                $http.get(ENV.apiUrl + "api/VoucherReversal/getAllRecords?doccode=" + docd + "&prnumber=" + final_doc_no + "&comp_code=" + cmp_cd).then(function (prdoc_data) {

                    $scope.prdocdata = prdoc_data.data;
                    // $scope.block1 = [];
                    $scope.temp_doc = [];
                    $scope.totalDebit = 0;
                    $scope.totalcredit = 0;

                    for (var i = 0; i < $scope.prdocdata.length; i++) {

                        if (i == 0) {
                            $scope.temp_doc.gltd_comp_code = $scope.prdocdata[i].gltd_comp_code;
                            $scope.temp_doc.gltd_doc_code = $scope.prdocdata[i].gltd_doc_code;

                            $scope.temp_doc.gltd_doc_narr = $scope.prdocdata[i].gltd_doc_narr;
                            $scope.temp_doc.gltd_remarks = $scope.prdocdata[i].gltd_remarks;
                            $scope.temp_doc.gltd_doc_date = $scope.prdocdata[i].gltd_doc_date;
                            $scope.temp_doc.gltd_post_date = $scope.prdocdata[i].gltd_post_date;
                            $scope.temp_doc.gltd_prov_doc_no = $scope.prdocdata[i].gltd_prov_doc_no;
                            $scope.temp_doc.gltd_final_doc_no = final_doc_no;
                            $scope.temp_doc.gltd_paid_to = $scope.prdocdata[i].gltd_paid_to;
                            $scope.temp_doc.gltd_cheque_no = $scope.prdocdata[i].gltd_cheque_no;
                            $scope.temp_doc.gldu_authorize = $scope.prdocdata[i].gldu_authorize;
                            $scope.temp_doc.gldu_verify = $scope.prdocdata[i].gldu_verify;
                            $scope.temp_doc.gltd_authorize_date = $scope.prdocdata[i].gltd_authorize_date;
                            $scope.temp_doc.gltd_verify_date = $scope.prdocdata[i].gltd_verify_date;

                        }
                        $scope.cost = $scope.cost || ($scope.prdocdata[i].gldd_cost_center_code != '' || $scope.prdocdata[i].gldd_cost_center_code != "");
                        $scope.totalDebit = parseFloat($scope.totalDebit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_debit);
                        $scope.totalcredit = parseFloat($scope.totalcredit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_credit);

                    }

                });

            }
            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=Single").then(function (res) {
                $scope.final_doc_url = res.data;
            });

            $scope.CheckPrintReport = function () {


                var data = {
                    location: $scope.final_doc_url,
                    parameter: {
                        comp_detail: comp_code,
                        doc_code: $scope.temp_doc.gltd_doc_code,
                        doc_no: $scope.temp_doc.gltd_final_doc_no,

                    },

                    state: 'main.Fin123'
                }

                window.localStorage["ReportDetails"] = JSON.stringify(data)
                //first hide modal and open report
                $('#myModal4').modal('hide');
                setTimeout(function () {
                    $state.go('main.ReportCardParameter')
                }, 700);


            }
            
            $scope.printStaffResigned = function (div) {
                debugger
                // $scope.gldd_acct_code = '';

                var docHead = document.head.outerHTML;
                var printContents = document.getElementById('printdata4').outerHTML;
                var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";

                var newWin = window.open("", "_blank", winAttr);
                var writeDoc = newWin.document;
                writeDoc.open();
                writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                writeDoc.close();
                newWin.focus();
                //$scope.show_table_header = false;
            }
            $scope.exportStaffResigned = function () {
                var check = true;
                //$scope.acct_name = $("#cmb_acc_Code3 option:selected").text();
                //$scope.from = $scope.block1.from_date;
                //$scope.to = $scope.block1.to_date;
                // $scope.block1.gldd_acct_code = '';

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.acct_name + "_SLQueryDetails.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            var blob = new Blob([document.getElementById('printdata3').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + $scope.acct_name + "_SLQueryDetails" + ".xls");
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Report Not Save", showCloseButton: true, width: 380, })
                }
            }
            $scope.back = function () {

                $scope.report_show = true;

            }
            $scope.report_show = true;
            $scope.Report = function () {
                debugger;

                $scope.report_show = false;

                var data = {
                    location: $scope.final_doc_url,
                    parameter: {
                        comp_detail: comp_code,
                        doc_code: $scope.temp_doc.gltd_doc_code,
                        doc_no: $scope.temp_doc.gltd_final_doc_no,

                    },

                    state: 'main.Fin123',

                    /*ready: function () {
                        this.refreshReport();
                    },*/
                }

                window.localStorage["ReportDetails"] = JSON.stringify(data)

                // });

                //For Report Tool////////////////////////////////////////////////////////////////////////////////


                try {
                    $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                    $scope.location = $scope.rpt.location;
                    $scope.parameter = $scope.rpt.parameter;
                    $scope.state = $scope.rpt.state;
                }
                catch (ex) {
                }
                var s;

                //if ($http.defaults.headers.common['schoolId'] == 'dpsmis')
                //    s = "SimsReports." + $scope.location + ",SimsReports";


                //else
                //    s = "SimsReports." + $scope.location + ",SimsReports" + $http.defaults.headers.common['schoolId'];


                //  window.localStorage["Finn_comp"] = JSON.stringify(data)

                s = "SimsReports." + $scope.location + ",SimsReports";

                var url = window.location.href;
                var domain = url.substring(0, url.indexOf(':'))
                if ($http.defaults.headers.common['schoolId'] == 'sms') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                }
                else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc' || $http.defaults.headers.common['schoolId'] == 'christ') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'imert') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                }

                else {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                }
                // console.log(service_url);


                $scope.parameters = {}

                $("#reportViewer1")
                               .telerik_ReportViewer({
                                   //serviceUrl: ENV.apiUrl + "api/reports/",
                                   serviceUrl: service_url,

                                   viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                   scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                   // Zoom in and out the report using the scale
                                   // 1.0 is equal to 100%, i.e. the original size of the report
                                   scale: 1.0,
                                   ready: function () {
                                       //this.refreshReport();
                                   }

                               });





                var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                reportViewer.reportSource({
                    report: s,
                    parameters: $scope.parameter,
                });

                //var rv = $("#reportViewer1").data("telerik_ReportViewer");
                //rv.commands.print.exec();

                setInterval(function () {

                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                    //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });

                }, 1000);


                $timeout(function () {
                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                    //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });


                }, 100)




            }
          
        }]
        )
})();
