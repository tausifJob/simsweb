﻿(function () {
    'use strict';
    var main;
    var data2 = [];
    var Bankdetails = [];
    var coad_pty_short_name = [];
    var status = "";
    var msg_flag1 = false;
    var acconcode = "";
    var bankslno;
    var cmbvalue = "";
    var DuplicateBankDetails = [];
    var DuplicateEditBankDetails = [];
    var sano, chno, acno, chdate, amt;
    //var companyCode = "1";
    var payable_acc_code="";//IN there payable_acc_code i have put the value of Receiveable account number.....
    var values = [];
    var accountDescription;
    var currentdate;
    var accountname;


    var simsController = angular.module('sims.module.Finance');


    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PDCReceipt(BankReceipt)Cont',
        ['$scope', '$state', '$rootScope', '$filter', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $filter, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '5';
            $scope.edt = [];
            $scope.operation = true;
            $scope.table1 = true;
            $scope.Add = true;
            $scope.ldgrcode = false;
            $scope.btnDelete = false;
            $scope.preparebtn = true;
            $scope.verifybtn = true;
            $scope.authorizebtn = true;
            $scope.Bankdetails = "";
            $scope.Ac_code = false;
            $scope.Ac_code_main = true;
            $scope.temp = [];
            $scope.edt5 = [];
            $scope.geDocCode_new = [];

            //payable_acc_code = "1603070";
            $scope.Bankdetails = [];
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            //$scope.sdate = yyyy + '-' + mm + '-' + dd;
            //$scope.edate = yyyy + '-' + mm + '-' + dd;
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;

            $scope.temp = {
                //DocDate: yyyy + '-' + mm + '-' + dd,
                //PostDate: yyyy + '-' + mm + '-' + dd,
                DocDate: dd + '-' + mm + '-' + yyyy,
                PostDate: dd + '-' + mm + '-' + yyyy,

            }
            $scope.temp1 = {
                //ChequeDate: yyyy + '-' + mm + '-' + dd,
                ChequeDate: dd + '-' + mm + '-' + yyyy,
            }

            //$scope.temp.gltd_doc_code = 'PDC Payment Document';

            var user = $rootScope.globals.currentUser.username.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
            var vat_percent = $scope.finnDetail.comp_vat_per;
            var comp_vat_status = $scope.finnDetail.comp_vat_status;
            var input_vat_acct = $scope.finnDetail.input_vat_acct;
            var vat_ac_name = '';


            //$http.get(ENV.apiUrl + "api/BankPayment/getDocCode?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {

            //    $scope.geDocCode = res.data;
            //    for (var i = 0; i < $scope.geDocCode.length; i++) {
            //        if ($scope.geDocCode[i].gltd_doc_code == "PD") {
            //            $scope.getDocCode = $scope.geDocCode[i].gltd_doc_code;
            //        }
            //    }
            //    $scope.temp['gltd_doc_code']=$scope.getDocCode;
                
            //    $scope.edt['NoOfCheque'] = '1';
                
            //});


            $http.get(ENV.apiUrl + "api/JVCreation/getDocCodeJV?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {

                $scope.geDocCode = res.data;

                for (var i = 0; i < $scope.geDocCode.length; i++) {
                    if ($scope.geDocCode[i].gldc_doc_code_type == "PD") {
                        $scope.geDocCode_new.push($scope.geDocCode[i]);
                    }
                }
                if ($scope.geDocCode_new.length > 1) {
                    $scope.dis_doc_cd = false;
                    $scope.temp['gltd_doc_code'] = $scope.geDocCode_new[0].gltd_doc_code;

                    $http.get(ENV.apiUrl + "api/BankPayment/GetAllCashBankGLMasterAccountNo?cmp_cd=" + comp_code + "&financialyear=" + finance_year + "&doc_cd=" + $scope.temp.gltd_doc_code + "&username=" + user).then(function (docstatus2) {

                        $scope.bankname = docstatus2.data
                        $scope.edt5['master_acno'] = docstatus2.data[0];
                        $scope.getAccountName(docstatus2.data[0])


                        $http.get(ENV.apiUrl + "api/BankPayment/GetAllGLAccountNos?glma_accountcode=" + $scope.edt5.master_acno.master_acno + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {

                            $scope.getAllAccNos = docstatus.data;

                        });
                    });
                }
                else if ($scope.geDocCode_new.length == 1) {
                    $scope.dis_doc_cd = true;
                    $scope.temp['gltd_doc_code'] = $scope.geDocCode_new[0].gltd_doc_code;
                    $http.get(ENV.apiUrl + "api/BankPayment/GetAllCashBankGLMasterAccountNo?cmp_cd=" + comp_code + "&financialyear=" + finance_year + "&doc_cd=" + $scope.geDocCode_new[0].gltd_doc_code + "&username=" + user).then(function (docstatus2) {

                        $scope.bankname = docstatus2.data
                        $scope.edt5['master_acno'] = docstatus2.data[0];
                        $scope.getAccountName(docstatus2.data[0])


                        $http.get(ENV.apiUrl + "api/BankPayment/GetAllGLAccountNos?glma_accountcode=" + $scope.edt5.master_acno.master_acno + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {

                            $scope.getAllAccNos = docstatus.data;

                        });
                    });
                }
                $scope.edt['NoOfCheque'] = '1';

            });

            $scope.getAccountName = function (str) {
                for (var i = 0; i < $scope.bankname.length; i++) {
                    if ($scope.bankname[i].master_acno == str.master_acno) {
                        acconcode = $scope.bankname[i].master_acno;
                        $scope.edt3 = {
                            master_ac_cdname: $scope.bankname[i].master_ac_cdname,
                        }
                    }

                }
            }


            $http.get(ENV.apiUrl + "api/BankPayment/GetVerifyAuthorizeUsers?username=" + user + "&comp_code=" + comp_code + "&fyear=" + finance_year).then(function (users_list) {
               
                $scope.users = users_list.data;
               
                if ($scope.users.length != 0) {
                  
                    if ($scope.users[0] == user && $scope.users[1] == user) {
                        $scope.preparebtn = false;
                        $scope.verifybtn = false;
                        $scope.authorizebtn = false;
                    }
                    else if ($scope.users[0] == user && ($scope.users[1] == "" || $scope.users[1] != user)) {
                        $scope.verifybtn = false;
                        $scope.preparebtn = false;
                        $scope.authorizebtn = true;
                    }
                    else {
                        $scope.preparebtn = false;
                        $scope.verifybtn = true;
                        $scope.authorizebtn = true;
                    }
                }
            });

            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=Single").then(function (res) {
                $scope.final_doc_url = res.data;
            });


            $scope.Authorizeuser = function () {
                
              
                $http.get(ENV.apiUrl + "api/BankPayment/GetVerifyAuthorizeUsers?username=" + user + "&comp_code=" + comp_code + "&fyear=" + finance_year).then(function (users_list) {
                    
                    $scope.users = users_list.data;
                   
                    if ($scope.users.length != 0) {
                        
                        if ($scope.users[0] == user && $scope.users[1] == user) {
                            $scope.preparebtn = false;
                            $scope.verifybtn = false;
                            $scope.authorizebtn = false;
                        }
                        else if ($scope.users[0] == user && ($scope.users[1] == "" || $scope.users[1] != user)) {
                            $scope.verifybtn = false;
                            $scope.preparebtn = false;
                            $scope.authorizebtn = true;
                        }
                        else {
                            $scope.preparebtn = false;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                        }
                    }
           
                });

               
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);
            
            $scope.myFunct = function (keyEvent) {

                if (keyEvent.which == 13)
                    //$scope.temp.PostDate = yyyy + '-' + mm + '-' + dd;
                    $scope.temp.PostDate = dd + '-' + mm + '-' + yyyy;
            }

            $scope.myFunct1 = function (keyEvent) {

                if (keyEvent.which == 13)
                 //$scope.temp1.ChequeDate = yyyy + '-' + mm + '-' + dd;
                   $scope.temp1.ChequeDate = dd + '-' + mm + '-' + yyyy;
            }

            $scope.size = function (str) {

                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }


            
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);



                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.New = function () {
                $scope.operation = true;
                $scope.Add = true;
                $scope.updatebtn = false;
                $scope.Delete = false;
                $scope.edt = [];
            }

            $scope.totalAmountCount = function () {
                
                $scope.total = 0;

                for (var i = 0; i < $scope.Bankdetails.length; i++) {

                    $scope.total = parseFloat($scope.total) + parseFloat($scope.Bankdetails[i].gldd_doc_amount);

                }
            }

            $scope.GetAllGLAcc = function (acccode, cmpnycode) {


                $http.get(ENV.apiUrl + "api/BankPayment/GetAllGLAccountNos?glma_accountcode=" + acccode + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {
                    $scope.getAllAccNos = docstatus.data;

                });
            }

            //$http.get(ENV.apiUrl + "api/PDCBill/Get_doc_narration?cmpnycode=" + companyCode + "&doccode=" + $scope.temp.gltd_doc_code).then(function (docstatus) {

            //    $scope.getdoc_narration = docstatus.data;

            //});

            $http.get(ENV.apiUrl + "api/BankPayment/GetAllGLAccountNos?glma_accountcode=" + payable_acc_code + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {
                $scope.getAllAccNos = docstatus.data;

            });

            $http.get(ENV.apiUrl + "api/PDCBill/getBankDeatails").then(function (getBankDetails) {
                $scope.BankDetails = getBankDetails.data;

            });

            //$http.get(ENV.apiUrl + "api/PDCBill/getPdcReceivableAcc").then(function (GetPdcReceivableAcc) {
                
            //    $scope.BankReceiveableDetails1 = GetPdcReceivableAcc.data;

            //    $scope.temp['BankReceiveableDetails'] = $scope.BankReceiveableDetails1[0].fins_appl_parameter;


            //    var bnkrecedet = $scope.BankReceiveableDetails1[0].fins_appl_parameter;
            //    values = bnkrecedet.split("-");

            //    payable_acc_code = values[0];
            //    var value2 = values[1];

            //                });




            $scope.getSLAccNo = function (slcode) {
                
                $scope.getAllAccNos = [];
                if (slcode == "00" || slcode == undefined) {
                    $scope.GetAllGLAcc( $scope.edt5.master_acno.master_acno, comp_code);
                }
                else {

                    $http.get(ENV.apiUrl + "api/JVCreation/GetSLAccNumber?ldgr_code=" + slcode + "&cmp_cd=" + comp_code + "&year=" + finance_year).then(function (docstatus3) {
                        // $scope.sllc_ldgr_code = slcode;
                        $scope.getAllAccNos = docstatus3.data;
                        //$scope.slacno = docstatus3.data;
                    });

                }
               

                
            }


            $scope.getDepartcode = function (str) {

                debugger;
                for (var i = 0; $scope.getAllAccNos.length; i++) {
                    if ($scope.getAllAccNos[i].gldd_acct_code == str) {
                        $scope.edt1 = {
                            gldd_acct_code: $scope.getAllAccNos[i].gldd_acct_code,
                            coad_dept_no: $scope.getAllAccNos[i].gldd_dept_code,
                            gldd_acct_name: $scope.getAllAccNos[i].gldd_acct_name,
                            gldd_dept_name: $scope.getAllAccNos[i].gldd_dept_name
                        }
                    }
                }

            }

            $http.get(ENV.apiUrl + "api/BankPayment/GetLedgerNumber?financialyear="+finance_year + "&comp_code="+comp_code).then(function (docstatus2) {
           
                $scope.LdgrCode = docstatus2.data;

            });
            
            //$("#PostDate").datepicker({
            //    //dateFormat: "yyyy-mm-dd",
            //    dateFormat: "dd-mm-yyyy",
            //    maxDate: new Date()
            //});

            $scope.getBankCode = function (str) {

                bankslno = "";
                for (var i = 0; i < $scope.BankDetails.length; i++) {
                    if ($scope.BankDetails[i].slma_pty_bank_id == str) {
                        bankslno = $scope.BankDetails[i].pb_gl_acno;
                    }
                }
            }

            $scope.Insert_temp_docs = function () {

                var datasend = [];

                var verifyUser, verifyDate, authorize_user, authorize_date;
                if (status == "Verify") {
                    verifyUser = user;
                    verifyDate = $scope.temp.DocDate;
                }
                else if (status == "Authorize") {
                    verifyUser = user;
                    verifyDate = $scope.temp.DocDate;
                    authorize_user = "";
                    authorize_date = "";
                }
                else {
                    verifyUser = "";
                    verifyDate = "";
                    authorize_user = "";
                    authorize_date = "";

                }
                var data = {
                    gltd_comp_code: comp_code,
                    gltd_prepare_user: user,
                    gltd_doc_code: $scope.temp.gltd_doc_code,
                    gltd_doc_date: $scope.temp.DocDate,
                    gltd_cur_status: status,
                    gltd_post_date: $scope.temp.PostDate,
                    gltd_doc_narr: $scope.temp1.gltd_doc_narr1,
                    gltd_remarks: $scope.temp1.gltd_doc_remark,
                    gltd_prepare_date: $scope.temp.DocDate,
                    gltd_final_doc_no: "0",
                    gltd_verify_user: verifyUser,
                    gltd_verify_date: verifyDate,
                    gltd_authorize_user: authorize_user,
                    gltd_authorize_date: authorize_date,
                    gltd_paid_to: $scope.temp1.gltd_paid_to,
                    gltd_cheque_no: $scope.temp1.gltd_cheque_No
                }
                datasend.push(data);

                $http.post(ENV.apiUrl + "api/BankPayment/Insert_Fins_temp_docs", datasend).then(function (msg) {
                    $scope.prvno = msg.data;

                    if ($scope.prvno != "" && $scope.prvno != null) {
                        $scope.Insert_Fins_temp_doc_details();
                    }
                    else {
                        swal({ title: "Alert", text: "Document Not Created. " , width: 300, height: 200 });
                        $scope.Authorizeuser();
                    }
                });

            }

            //   data.em_img = '1' + $scope.photo_filename.split("-")[1];


            $scope.Insert_Fins_temp_doc_details = function () {
                
                var dataSend = [];
                var j = 1;

                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    var data = {
                        gldd_doc_narr: $scope.Bankdetails[i].gldd_doc_narr,
                        gldd_dept_code: $scope.Bankdetails[i].coad_dept_no,
                        gldd_ledger_code: $scope.Bankdetails[i].sllc_ldgr_code,
                        gldd_acct_code: $scope.Bankdetails[i].slma_acno,
                        pc_bank_code: $scope.Bankdetails[i].slma_pty_bank_id,
                        gldd_party_ref_no: $scope.Bankdetails[i].check_no,
                        gldd_party_ref_date: $scope.Bankdetails[i].ChequeDate,
                        gldd_doc_amount: parseFloat("-" + $scope.Bankdetails[i].gldd_doc_amount),
                        gldd_comp_code: comp_code,
                        gldd_doc_code: $scope.temp.gltd_doc_code,
                        gldd_prov_doc_no: $scope.prvno,
                        gldd_final_doc_no: "0",
                        gldd_line_no: j,

                    }
                    j++;
                    dataSend.push(data);

                }
                var data = {
                    gldd_comp_code: comp_code,
                    gldd_doc_code: $scope.temp.gltd_doc_code,
                    gldd_prov_doc_no: $scope.prvno,
                    gldd_final_doc_no: "0",
                    gldd_line_no: j,
                    gldd_ledger_code: "00",
                    gldd_acct_code: $scope.edt5.master_acno.master_acno,
                    gldd_doc_amount: $scope.total,
                    gldd_fc_amount_debit: 0,
                    gldd_fc_code: "",
                    gldd_fc_rate: 0,
                    gldd_doc_narr: $scope.temp1.gltd_doc_narr1,
                    gldd_dept_code: $scope.edt5.master_acno.gldd_dept_code,
                    gldd_party_ref_no: "",
                    gldd_party_ref_date: "",
                    gldd_cost_center_code: $scope.edt5.glco_cost_centre_code,

                }
                dataSend.push(data);
                $http.post(ENV.apiUrl + "api/BankPayment/Insert_Fins_temp_doc_details", dataSend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if (status == "Authorize") {
                        msg_flag1 = true;
                        $scope.Authorize_Insert_temp_doc_details($scope.msg1);

                    }
                    else if (status == "Save") {
                        msg_flag1 = false;
                        $scope.Authorize_Insert_temp_doc_details($scope.msg1);

                    }
                    else if (status == "Verify") {
                        msg_flag1 = false;
                        $scope.Authorize_Insert_temp_doc_details($scope.msg1);

                    }
                    if ($scope.msg1 == true) {

                    }
                    else {
                        swal({ title: "Alert", text: "Data Not Inserted. ", width: 300, height: 200 });
                    }
                });

            }

            $scope.Authorize_Insert_temp_doc_details = function (str) {

                if (msg_flag1) {
                    msg_flag1 = false;
                    var auth_date = $scope.temp.DocDate;
                    $http.post(ENV.apiUrl + "api/BankPayment/Authorize_Posting?comp_code=" + comp_code + "&doc_code=" + $scope.temp.gltd_doc_code + "&prv_no=" + $scope.prvno + "&auth_user=" + user + "&auth_date=" + auth_date).then(function (Auth) {
                        
                        $scope.Docno = Auth.data;
                        if ($scope.Docno != "") {
                            swal({ title: "Alert", text: "Posted Successfully\n Final Doc No :" + $scope.Docno, width: 300, height: 200 });
                            $scope.checkprintbtn = true;
                            $scope.ClearInsert();
                            $scope.Authorizeuser();
                        }
                        else {
                            swal({ title: "Alert", text: "Posting Failed", width: 300, height: 200 });
                            $scope.ClearInsert();
                            $scope.Authorizeuser();
                        }
                    });

                }
                else if ((msg_flag1 == false) && (status == "Save")) {
                    msg_flag1 = false;
                    if (str) {
                        swal({ title: "Alert", text: "Voucher Created Successfully\nProvision No:-" + $scope.prvno, width: 300, height: 200 });
                        $scope.ClearInsert();
                        $scope.Authorizeuser();
                    }
                    else {
                        swal({ title: "Alert", text: "Voucher Not Created", width: 300, height: 200 });
                    }

                }

                else if ((msg_flag1 == false) && (status == "Verify")) {
                    msg_flag1 = false;
                    if (str) {
                        swal({ title: "Alert", text: "Voucher Verified Successfully\nProvision No:-" + $scope.prvno, width: 300, height: 200 });
                        $scope.ClearInsert();
                        $scope.Authorizeuser();
                    }
                    else {
                        swal({ title: "Alert", text: "Voucher Verification Failed", width: 300, height: 200 });
                    }

                }

                $scope.ClearInsert();
                $scope.Authorizeuser();
                $scope.cancel();

            }

            $scope.CheckPrintReport = function () {

                $('#pdcbills').modal('show');
                $scope.report_show = false;
                var data = {
                    location: $scope.final_doc_url,
                    parameter: {
                        comp_detail: comp_code,
                        doc_code: $scope.temp.gltd_doc_code,
                        doc_no: $scope.Docno,

                    },

                    state: 'main.fin212'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                //$state.go('main.ReportCardParameter')
                try {
                    $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                    $scope.location = $scope.rpt.location;
                    $scope.parameter = $scope.rpt.parameter;
                    $scope.state = $scope.rpt.state;
                }
                catch (ex) {
                }
                var s;
                s = "SimsReports." + $scope.location + ",SimsReports";
                var url = window.location.href;
                var domain = url.substring(0, url.indexOf(':'))
                if ($http.defaults.headers.common['schoolId'] == 'sms') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                }
                else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                }

                else {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                }
                console.log(service_url);
                $scope.parameters = {}

                $("#reportViewer1")
                               .telerik_ReportViewer({
                                   serviceUrl: service_url,
                                   viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                   scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                   // Zoom in and out the report using the scale
                                   // 1.0 is equal to 100%, i.e. the original size of the report
                                   scale: 1.0,
                                   ready: function () {
                                       //this.refreshReport();
                                   }

                               });
                var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                reportViewer.reportSource({
                    report: s,
                    parameters: $scope.parameter,
                });

                setInterval(function () {

                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                }, 1000);

                $timeout(function () {
                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                }, 100)


            }
      
            $scope.Prepare = function () {
                try {
                    if ($scope.Bankdetails.length > 0) {
                        $scope.flag1 = false;
                        if ($scope.temp.PostDate == null || $scope.temp.PostDate == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Select Posting Date", showCloseButton: true, width: 380, });

                        }
                        else if ( $scope.edt5.master_acno.master_acno == null ||  $scope.edt5.master_acno.master_acno == undefined ||  $scope.edt5.master_acno.master_acno == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Select Receivable account", showCloseButton: true, width: 380, });

                        }


                        else {

                            $scope.preparebtn = true;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                            status = "Save";
                            //$scope.InsertPDCBills();
                            $scope.Insert_temp_docs();
                            $scope.Ac_code = false;
                            $scope.Ac_code_main = true;
                        }
                    }
                    else {
                        swal({ title: "Alert", text: "Please Add Transaction Details", showCloseButton: true, width: 380, });
                    }
                }


                catch (e) {

                }


            }

            $scope.Verify = function () {
                try {
                    if ($scope.Bankdetails.length > 0) {
                        $scope.flag1 = false;
                        if ($scope.temp.PostDate == null || $scope.temp.PostDate == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Select Posting Date", showCloseButton: true, width: 380, });
                        }
                        else if ( $scope.edt5.master_acno.master_acno == null ||  $scope.edt5.master_acno.master_acno == undefined ||  $scope.edt5.master_acno.master_acno == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Define Receivable account", showCloseButton: true, width: 380, });

                        }

                        else {
                            $scope.preparebtn = true;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                            status = "Verify";
                            $scope.Insert_temp_docs();
                            
                            $scope.Ac_code = false;
                            $scope.Ac_code_main = true;
                        }
                    }
                    else {
                        swal({ title: "Alert", text: "Please Add Transaction Details", showCloseButton: true, width: 380, });
                    }


                } catch (e) {

                }

            }

            $scope.Authorize = function () {
                try {
                    if ($scope.Bankdetails.length > 0) {
                        $scope.flag1 = false;
                        if ($scope.temp.PostDate == null || $scope.temp.PostDate == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Select Posting Date", showCloseButton: true, width: 380, });

                        }
                        else if ( $scope.edt5.master_acno.master_acno == null ||  $scope.edt5.master_acno.master_acno == undefined ||  $scope.edt5.master_acno.master_acno == "") {
                            $scope.flag1 = true;
                            swal({ title: "Alert", text: "Please Define Receivable account", showCloseButton: true, width: 380, });

                        }

                        else {
                            $scope.preparebtn = true;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                            status = "Authorize";
                           
                            $scope.Insert_temp_docs();
                            $scope.Ac_code = false;
                            $scope.Ac_code_main = true;

                        }



                    }
                    else {
                        swal({ title: "Alert", text: "Please Add Transaction Details", showCloseButton: true, width: 380, });

                    }


                } catch (e) {

                }
            }

            $scope.cancel = function () {

                $scope.Add = true;
                $scope.noofchequereadonly = false;
                $scope.updatebtn = false;
                $scope.edt = [];
                $scope.edt1 = [];
                $scope.edt2 = [];
                $scope.GetAllGLAcc( $scope.edt5.master_acno.master_acno, comp_code);
                $("#cmb_acc_Code").select2("val", "");

                $scope.edt['NoOfCheque'] = '1';

                $scope.Ac_code = false;
                $scope.Ac_code_main = true;
                $scope.ldgrcode = false;
            }
            $scope.ClearInsert = function () {
                
                $scope.Bankdetails = [];
                $scope.total = "";
                $scope.temp1 = {
                    gltd_doc_narr1: "",
                    gltd_doc_remark: ""
                }
            }
            setTimeout(function () {
                $("#cmb_acc_Code").select2();

            }, 100);



            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                //format: "yyyy-mm-dd"
                format: "dd-mm-yyyy"
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.onlyNumbers = function (event) {
                
                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $scope.summ = function () {

                $scope.total = 0;

                for (var i = 0; i < $scope.Bankdetails.length; i++) {

                    $scope.total = $scope.total + parseInt($scope.Bankdetails[i].gldd_doc_amount);

                }

            }

            $scope.checkduplicatecheque = function (str) {
                


                //var checkno = $('#chno').val();
                //var bankId = $('#bankId').val();
                var countno = 0;
                var f = false;



               

                for (var j = 0; j < $scope.Bankdetails.length; j++) {
                    if ($scope.Bankdetails[j].check_no == str.check_no && $scope.Bankdetails[j].slma_pty_bank_id == str.slma_pty_bank_id) {
                        countno++;

                    }
                }
                if (countno > 1) {
                    // $('#chno').css.({ backgroundColor: "#FFF", border: "5px solid #ccc" })
                    //$('#chno').css('background-color', '#FFF');
                    //document.getElementById("chno").style.backgroundColor = "#ff0000";
                    $scope.Bankdetails = angular.copy(DuplicateBankDetails);
                    swal({ title: "Alert", text: "Check no and bank Code is already Present", showCloseButton: true, width: 380, });

                }
                else {
                    $scope.Bankdetails.push({
                        'gldd_comp_code': str.gldd_comp_code,
                        'sllc_ldgr_code': str.sllc_ldgr_code,
                        'slma_acno': str.slma_acno,
                        'coad_pty_full_name': str.coad_pty_full_name,
                        'coad_dept_no': str.coad_dept_no,
                        'bankslno': str.bankslno,
                        'check_no': str.check_no,
                        'ChequeDate': str.ChequeDate,
                        'coad_dept_name': str.coad_dept_name,
                        'gldd_doc_narr': str.coad_dept_name,
                        'gldd_doc_amount': str.gldd_doc_amount,
                        'slma_pty_bank_id': str.slma_pty_bank_id,
                        'noOfCheck': str.noOfCheck,
                    });
                    $scope.Bankdetails.splice(j, 1);
                    DuplicateBankDetails = angular.copy($scope.Bankdetails);

                }

            }


            //Add Button Code......
            $scope.AddintoGrid = function (myForm) {
                debugger;
                if (myForm) {
                    $scope.flag1 = false;
                    if ($scope.edt.gldd_acct_code == undefined || $scope.edt.gldd_acct_code == "") {
                        $scope.flag1 = true;
                        swal({ title: "Alert", text: "Please select Account name", showCloseButton: true, width: 380, });
                        return;

                    }
                    else if ($scope.edt.gldd_doc_amount == undefined || $scope.edt.gldd_doc_amount == "") {
                        $scope.flag1 = true;
                        swal({ title: "Alert", text: "Please Enter Amount", showCloseButton: true, width: 380, });
                        return;
                    }
                    else if ($scope.edt.slma_pty_bank_id == undefined || $scope.edt.slma_pty_bank_id == "") {
                        $scope.flag1 = true;
                        swal({ title: "Alert", text: "Please select bank", showCloseButton: true, width: 380, });
                        return;

                    }
                    else if ($scope.edt2 == undefined || $scope.edt2.chequeno1 == "") {
                        $scope.flag1 = true;
                        swal({ title: "Alert", text: "Please Enter Cheque Number", showCloseButton: true, width: 380, });
                        return;
                    }
                    else if ($scope.temp1.ChequeDate == null || $scope.temp1.ChequeDate == "") {
                        $scope.flag1 = true;
                        swal({ title: "Alert", text: "Please Enter Cheque Date", showCloseButton: true, width: 380, });
                        return;
                    }
                    else if ($scope.edt.NoOfCheque == null || $scope.edt.NoOfCheque == "") {
                        $scope.flag1 = true;
                        swal({ title: "Alert", text: "Please Enter Number of Cheque", showCloseButton: true, width: 380, });
                        return;
                    }
                    else {
                        $scope.flag1 = false;
                    }

                    if (!$scope.flag1) {
                        for (var i = 0; i < $scope.edt.NoOfCheque; i++) {
                            var f = false;
                            var ldgr_code = "";

                            var nextMonth = new Date($scope.temp1.ChequeDate);
                            nextMonth.setMonth(nextMonth.getMonth() + i);
                            var d = nextMonth.getDate();
                            var m = nextMonth.getMonth() + 1;
                            if (m < 10) {
                                m = '0' + m;
                            }
                            var y = nextMonth.getFullYear();
                            var bankname = document.getElementById("bankName");
                            var selectbankname = bankname.options[bankname.selectedIndex].text;

                            var terminal = document.getElementById("cmb_acc_Code");
                            $scope.selectedText = terminal.options[terminal.selectedIndex].text;
                            if ($scope.edt.gldd_doc_narr == undefined) {
                                $scope.edt.gldd_doc_narr = "";
                            }
                            $scope.gldd_doc_narr1 = $scope.edt.gldd_doc_narr + " - " + $scope.edt2.chequeno1 + " - " + $scope.temp1.ChequeDate;
                            if ($scope.edt.sllc_ldgr_code == undefined) {
                                ldgr_code = '00';
                            }
                            else {
                                ldgr_code = $scope.edt.sllc_ldgr_code;
                            }
                            for (var j = 0; j < $scope.Bankdetails.length; j++) {
                                if ($scope.Bankdetails[j].check_no == $scope.edt2.chequeno1 && $scope.Bankdetails[j].slma_pty_bank_id == $scope.edt.slma_pty_bank_id) {
                                    f = true;
                                }
                            }
                            if (f) {
                                $scope.edt2.chequeno1 = parseInt($scope.edt2.chequeno1) + 1;
                                i--;
                            }
                            else {
                                $scope.Bankdetails.push({
                                    'gldd_comp_code': comp_code,
                                    'sllc_ldgr_code': ldgr_code,
                                    'slma_acno': $scope.edt.gldd_acct_code,
                                    'coad_pty_full_name': $scope.selectedText,
                                    'coad_dept_no': $scope.edt1.coad_dept_no,
                                    'bankslno': bankslno,
                                    //  'check_no_old': $scope.edt2.chequeno1,
                                    'check_no': $scope.edt2.chequeno1,
                                    //'ChequeDate': d + '-' + m + '-' + y,
                                    'ChequeDate': $scope.temp1.ChequeDate,
                                    'coad_dept_name': selectbankname,
                                    'gldd_doc_narr': $scope.gldd_doc_narr1,
                                    'gldd_doc_amount': $scope.edt.gldd_doc_amount,
                                    'slma_pty_bank_id': $scope.edt.slma_pty_bank_id,
                                    'noOfCheck': $scope.edt.NoOfCheque,

                                });
                                $scope.edt2.chequeno1 = parseFloat($scope.edt2.chequeno1) + 1;
                            }



                        }
                        DuplicateBankDetails = angular.copy($scope.Bankdetails);

                        $scope.total = 0;

                        for (var i = 0; i < $scope.Bankdetails.length; i++) {

                            $scope.total = parseFloat($scope.total) + parseFloat($scope.Bankdetails[i].gldd_doc_amount);

                        }

                        $scope.edt = [];
                        $scope.edt1 = [];
                        $scope.edt2 = [];
                        $scope.edt['NoOfCheque'] = '1';
                        $scope.getSLAccNo($scope.edt.sllc_ldgr_code);
                        $("#cmb_acc_Code").select2("val", "");

                    }
                }


            }


            $scope.editupdate = function (str) {
                
                $scope.Ac_code = true;
                $scope.Ac_code_main = false;
                var data = angular.copy(str);
                DuplicateEditBankDetails[0] = data;
                sano = "";
                chno = "";
                acno = "";
                sano = data.slma_acno;
                chno = data.check_no;
                amt = data.gldd_doc_amount;
                chdate = data.ChequeDate;
                cmbvalue = data.slma_acno;
                acno = data.slma_acno;
                $scope.noofchequereadonly = true;
                $scope.Add = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.ldgrcode = true;
                $scope.edt1['coad_dept_no']= data.coad_dept_no;

                $scope.temp1['ChequeDate']= data.ChequeDate;

                //$scope.edt 
                   // gldd_acct_code: data.slma_acno,
                $scope.edt['accCode']=  data.coad_pty_full_name;
                $scope.edt['accountDescription']=  data.coad_pty_full_name;
                $scope.edt['sllc_ldgr_code']=  data.sllc_ldgr_code;
                $scope.edt['gldd_doc_amount']=  data.gldd_doc_amount;
                $scope.edt['slma_pty_bank_id']=  data.slma_pty_bank_id;
                $scope.edt['gldd_doc_narr']= data.gldd_doc_narr;
            

                $scope.edt2['chequeno1']= data.check_no;
                


                //if (data.sllc_ldgr_code != "00") {
                //    $("#cmb_acc_Code").select2("val", "");
                //    //$scope.getSLAccNo(data.sllc_ldgr_code);
                //}
                //else {
                //    $("#cmb_acc_Code").select2("val", data.slma_acno);
                //}
                
                accountDescription = $scope.edt.accCode;
                $scope.Ac_code = true;
                $scope.Ac_code_main = false;

            }

          
            $scope.Update = function () {
                
                if (myForm) {
                    var ff = false;
                    
                    if (DuplicateEditBankDetails[0].slma_pty_bank_id == $scope.edt.slma_pty_bank_id && DuplicateEditBankDetails[0].check_no == $scope.edt2.chequeno1) {
                        ff = false;
                    }
                    else {
                        for (var i = 0; i < $scope.Bankdetails.length; i++) {
                            if ($scope.Bankdetails[i].slma_pty_bank_id == $scope.edt.slma_pty_bank_id && $scope.Bankdetails[i].check_no == $scope.edt2.chequeno1) {
                                ff = true;
                            }
                        }
                    }
                    if (!ff) {
                        $scope.noofchequereadonly = false;
                        for (var i = 0; i < $scope.Bankdetails.length; i++) {
                            if ($scope.Bankdetails[i].check_no == chno && $scope.Bankdetails[i].slma_acno == sano && $scope.Bankdetails[i].gldd_doc_amount == amt && $scope.Bankdetails[i].ChequeDate == chdate) {
                                cmbvalue = "";
                                sano = "";
                                chno = "";
                                amt = "";
                                chdate = "";

                                var ldgr_code = "";
                                //var nextMonth = new Date($scope.temp1.ChequeDate);
                                //nextMonth.setMonth(nextMonth.getMonth() + i);
                                //var d = nextMonth.getDate();
                                //var m = nextMonth.getMonth() + 1;
                                //if (m < 10) {
                                //    m = '0' + m;
                                //}
                                //var y = nextMonth.getFullYear();
                                var bankname = document.getElementById("bankName");
                                var selectbankname = bankname.options[bankname.selectedIndex].text;

                                //var terminal = document.getElementById("cmb_acc_Code");
                                //$scope.selectedText = terminal.options[terminal.selectedIndex].text;
                                if ($scope.edt.gldd_doc_narr == undefined) {
                                    $scope.edt.gldd_doc_narr = "";
                                }
                                $scope.gldd_doc_narr1 = $scope.edt.gldd_doc_narr;
                                if ($scope.edt.sllc_ldgr_code == undefined) {
                                    ldgr_code = '00';
                                }
                                else {
                                    ldgr_code = $scope.edt.sllc_ldgr_code;
                                }

                                $scope.Bankdetails.push({
                                    'gldd_comp_code': comp_code,
                                    'sllc_ldgr_code': ldgr_code,
                                    'slma_acno': acno,
                                    'coad_pty_full_name': $scope.selectedText,
                                    'coad_dept_no': $scope.edt1.coad_dept_no,
                                    'bankslno': bankslno,
                                    'check_no': $scope.edt2.chequeno1,
                                    //'ChequeDate': y + '-' + m + '-' + d,
                                    'ChequeDate': $scope.temp1.ChequeDate,
                                    'coad_dept_name': selectbankname,
                                    'gldd_doc_narr': $scope.gldd_doc_narr1,
                                    'gldd_doc_amount': $scope.edt.gldd_doc_amount,
                                    'slma_pty_bank_id': $scope.edt.slma_pty_bank_id,
                                    'noOfCheck': $scope.edt.NoOfCheque,
                                });
                                $scope.Bankdetails.splice(i, 1);


                            }
                        }
                        
                        $scope.total = 0;

                        for (var i = 0; i < $scope.Bankdetails.length; i++) {

                            $scope.total = parseFloat($scope.total) + parseFloat($scope.Bankdetails[i].gldd_doc_amount);

                        }
                        $scope.updatebtn = false;
                        $scope.Add = true;
                        acno = '';

                        $scope.edt = [];
                        $scope.edt1 = [];
                        $scope.edt2 = [];
                        $scope.edt['NoOfCheque'] = '1';
                        $scope.getSLAccNo($scope.edt.sllc_ldgr_code);
                        $("#cmb_acc_Code").select2("val", "");

                    }

                    else {
                        swal({ title: "Alert", text: "Cheque No. Already Present", width: 300, height: 200 });
                    }
                }
                $scope.Ac_code = false;
                $scope.Ac_code_main = true;
                $scope.ldgrcode = false;

            }


            $scope.Delete = function (str) {

                for (var i = 0; i < $scope.Bankdetails.length; i++) {
                    if ($scope.Bankdetails[i].check_no == str.check_no && $scope.Bankdetails[i].slma_acno == str.slma_acno && $scope.Bankdetails[i].gldd_doc_amount == str.gldd_doc_amount && $scope.Bankdetails[i].ChequeDate == str.ChequeDate) {
                        $scope.Bankdetails.splice(i, 1);
                        break;
                    }
                }
                DuplicateBankDetails = angular.copy($scope.Bankdetails);
                $scope.total = 0;

                for (var i = 0; i < $scope.Bankdetails.length; i++) {

                    $scope.total = parseFloat($scope.total) + parseFloat($scope.Bankdetails[i].gldd_doc_amount);

                }
            }


            $scope.check_post_date = function () {
                if ($scope.temp.PostDate != undefined || $scope.temp.PostDate != '') {

                    $http.get(ENV.apiUrl + "api/JVCreation/Get_Check_Postdate?post_date=" + $scope.temp.PostDate + "&comp_cd=" + comp_code + "&fyear=" + finance_year).then(function (pronum) {
                        $scope.msg = pronum.data;

                        if ($scope.msg != '') {
                            swal({ title: "Alert", text: $scope.msg, width: 300, height: 200 });

                            $scope.preparebtn = true;
                            $scope.verifybtn = true;
                            $scope.authorizebtn = true;
                        }
                        else {
                            if ($scope.users.length != 0) {
                                if ($scope.users[0] == user && $scope.users[1] == user) {
                                    $scope.preparebtn = false;
                                    $scope.verifybtn = false;
                                    $scope.authorizebtn = false;
                                }
                                else if ($scope.users[0] == user && ($scope.users[1] == "" || $scope.users[1] != user)) {
                                    $scope.verifybtn = false;
                                    $scope.preparebtn = false;
                                    $scope.authorizebtn = true;
                                }
                                else {
                                    $scope.preparebtn = false;
                                    $scope.verifybtn = true;
                                    $scope.authorizebtn = true;
                                }
                            }
                        }

                    });

                }

            }


        }])
}

)();
