﻿
(function () {
    'use strict';
    var main, temp = [];
    var data1 = [];
    var data2 = [];
    var block1 = [];
    var status = "";
    var msg_flag1 = false;
    var prdocdata = [];
    var acconcode = "";
    var cmbvalue = "";
    var comp_code = "1";
    var chk;
    var data = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('AccountingViewQueryApplicationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.temp = {};
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.temp = {};
            $scope.display = false;
            $scope.dublicate = false;
            var user = $rootScope.globals.currentUser.username;
            $scope.tble_show = false;
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var financial_year = $scope.finnDetail.year;
            $scope.cost_combo = false;
            $scope.block1 = {};
            $scope.temp = []

            setTimeout(function () {
                $("#cmb_acc_Code3").select2();

            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


           

           

            $(function () {
                $('#grade_box').multipleSelect({ width: '100%' });
                $('#section_box').multipleSelect({ width: '100%' });
                $('#dep_box').multipleSelect({ width: '100%' });

            });

            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;

           

            $http.get(ENV.apiUrl + "api/accountingqueryview/getCompany").then(function (res1) {
                $scope.sims_comp = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['comp_code'] = res1.data[0].comp_code;

                    $scope.getfinYear(res1.data[0].comp_code);
                }
            });

            $http.get(ENV.apiUrl + "api/GLSLQuery/getDate_SL_Query?finance_year=" + financial_year).then(function (res) {
                $scope.getDates = res.data;

                $scope.block1 = {
                    from_date: $scope.getDates[0].sims_financial_year_start_date,
                    to_date: $scope.getDates[0].sims_financial_year_end_date
                }


                console.log($scope.getDates)
            });

            //$scope.getfinYear = function (comp_code) {
            //    debugger
            //    $http.get(ENV.apiUrl + "api/accountingqueryview/getfinYear?comp_code=" + comp_code).then(function (res1) {
            //        $scope.financial_year = res1.data;
            //        if (res1.data.length > 0) {
            //            $scope.temp['sims_financial_year'] = res1.data[0].sims_financial_year;
            //            $scope.getScheduleCode(comp_code, res1.data[0].sims_financial_year)
            //            $scope.getdepCode(comp_code, res1.data[0].sims_financial_year)
            //            $scope.getAllAccountTypes(comp_code)
            //        }
            //    });

            //}


            $scope.getfinYear = function (comp_code) {
                debugger
                $http.get(ENV.apiUrl + "api/accountingqueryview/getfinYear?comp_code=" + comp_code).then(function (res1) {
                    $scope.financial_year = res1.data;
                    if (res1.data.length > 0) {
                        for (var i = 0; i < $scope.financial_year.length; i++) {
                            if ($scope.financial_year[i].sims_financial_year_status == 'C') {
                                $scope.temp['sims_financial_year'] = $scope.financial_year[i].sims_financial_year;
                                $scope.getScheduleCode(comp_code, $scope.financial_year[i].sims_financial_year)
                                $scope.getdepCode(comp_code, $scope.financial_year[i].sims_financial_year)
                                $scope.getAllAccountTypes(comp_code)

                            }
                        }



                    }
                });

            }

           

            $scope.getdepCode = function (comp_code, financial_year) {
                debugger
                $http.get(ENV.apiUrl + "api/accountingqueryview/getdepCode?comp_code=" + comp_code + "&financial_year=" + financial_year).then(function (res1) {
                    $scope.dep_code = res1.data;
                    setTimeout(function () {
                        $('#dep_box').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }

            $scope.getScheduleCode = function (comp_code, financial_year) {
                debugger
                $http.get(ENV.apiUrl + "api/accountingqueryview/getScheduleCode?comp_code=" + comp_code + "&financial_year=" + financial_year).then(function (res1) {
                    $scope.schedule_code = res1.data;
                    setTimeout(function () {
                        $('#grade_box').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }

            $scope.getAllAccountTypes = function (comp_code) {
                debugger
                $http.get(ENV.apiUrl + "api/accountingqueryview/getAllAccountTypes?comp_code=" + comp_code).then(function (res1) {
                    $scope.account_type = res1.data;

                    setTimeout(function () {
                        $('#section_box').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }
            

            $scope.getAllFinsAccountCodes = function () {
                debugger
                debugger
                $scope.colsvis = false;
                   $http.get(ENV.apiUrl + "api/accountingqueryview/getAllFinsAccountCodes?comp_code=" + $scope.temp.comp_code + "&financial_year=" + $scope.temp.sims_financial_year + "&dep_code=" + $scope.temp.codp_dept_no + "&schedule_code=" + $scope.temp.glac_sched_code + "&acount_type=" + $scope.temp.glac_type_code).then(function (res1) {
                    if (res1.data.length > 0) {
                        $scope.report_data = res1.data;

                        $scope.temp.totaldebit = 0;
                        $scope.temp.totalcredit = 0;
                        $scope.temp.totalbalance = 0;
                         for (var j = 0; j < $scope.report_data.length; j++) {
                             $scope.temp.totaldebit = parseFloat($scope.temp.totaldebit) + parseFloat($scope.report_data[j].debit_amt);
                             $scope.temp.totalcredit = parseFloat($scope.temp.totalcredit) + parseFloat($scope.report_data[j].credit_amt);
                             $scope.temp.totalbalance = parseFloat($scope.temp.totalbalance) + parseFloat($scope.report_data[j].variance);
                            
                            //
                        }
                        angular.forEach($scope.report_data, function (f) {                           
                        });
                    }
                    else {
                        swal({ title: "Alert", text: " Data is not Available !!!!!", showCloseButton: true, width: 300, height: 200 });
                        $scope.report_data = '';
                    }
                    //angular.forEach($scope.report_data, function (f) {
                    //    f.sims_bell_lecture_per_week = parseFloat(f.sims_bell_lecture_per_week);
                    //});
                    console.log($scope.report_data);
                });
            }

            $http.get(ENV.apiUrl + "api/GLSLQuery/getDocCode_GL_Query?comp_code=" + comp_code + "&finance_year=" + financial_year).then(function (res) {
                $scope.geDocCode_jv = res.data;
            });

            $scope.getViewDetails = function () {
                debugger
                $scope.tble_show = true;

                $('#loader').modal({ backdrop: 'static', keyboard: false });

                var data = {
                    gltr_comp_code: comp_code,
                    gltr_doc_code: ($scope.block1['gltd_doc_code'] != undefined || $scope.block1['gltd_doc_code'] != "") ? $scope.block1['gltd_doc_code'] : null,
                    gltr_acct_code: ($scope.block1.gldd_acct_code != undefined || $scope.block1.gldd_acct_code != "") ? $scope.block1.gldd_acct_code : null,
                    gltr_pstng_date: ($scope.block1.from_date != undefined || $scope.block1.from_date != "") ? $scope.block1.from_date : null,
                    gltr_doc_date: ($scope.block1.to_date != undefined || $scope.block1.to_date != "") ? $scope.block1.to_date : null,
                    glco_cost_centre_code: ($scope.block1['glco_cost_centre_code'] != undefined || $scope.block1['glco_cost_centre_code'] != "") ? $scope.block1['glco_cost_centre_code'] : null,
                }
                var close_amt;
                $scope.closebal = 0;
                $http.post(ENV.apiUrl + "api/GLSLQuery/list_All_GL_Query", data).then(function (res1) {
                    $scope.trans_data = res1.data;

                    $('#loader').modal('hide');
                    $(".modal-backdrop").removeClass("modal-backdrop");

                    if ($scope.trans_data.length > 0) {
                        $scope.length = $scope.trans_data.length - 1;

                        if ($scope.block1.sltr_yob_amt > 0) {
                            $scope.trans_data[$scope.length].gltr_total_dr_amount = parseFloat($scope.trans_data[$scope.length].gltr_total_dr_amount) + $scope.block1.sltr_yob_amt;
                        }
                        else if ($scope.block1.sltr_yob_amt < 0) {
                            $scope.trans_data[$scope.length].gltr_total_cr_amount = parseFloat($scope.trans_data[$scope.length].gltr_total_cr_amount) + Math.abs($scope.block1.sltr_yob_amt);
                        }
                        close_amt = parseFloat($scope.trans_data[$scope.length].gltr_total_dr_amount) - parseFloat($scope.trans_data[$scope.length].gltr_total_cr_amount);

                        if (close_amt > 0) {
                            $scope.closebal = close_amt;
                            $scope.dr = ' dr';
                        }
                        else if (close_amt < 0) {
                            $scope.closebal = Math.abs(close_amt);
                            $scope.dr = ' cr';
                        }
                        else
                            $scope.closebal = close_amt;
                    }

                });

            }

            $scope.exportStaffResigned = function () {
                var check = true;


                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " GLQueryDetails.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            var blob = new Blob([document.getElementById('printdata3').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + " GLQueryDetails" + ".xls");
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Report Not Save", showCloseButton: true, width: 380, })
                }
            }

            //Code For Direct Print Button Option
            $scope.printStaffResigned = function (div) {

                var docHead = document.head.outerHTML;
                var printContents = document.getElementById(div).outerHTML;
                var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";

                var newWin = window.open("", "_blank", winAttr);
                var writeDoc = newWin.document;
                writeDoc.open();
                writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                writeDoc.close();
                newWin.focus();
            }


            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }





            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            //$scope.colsvis = false;


            $scope.exportData = function () {
                //$scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('rpt_data').innerHTML], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                        });
                        saveAs(blob, "DetentionSummaryReport.xls");
                        //$scope.colsvis = false;

                    }

                });
            };

            $scope.print = function (div) {

                //$scope.colsvis = false;

                var docHead = document.head.outerHTML;
                var printContents = document.getElementById('rpt_data').outerHTML;
                //$scope.colsvis = false;
                var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";


                var newWin = window.open("", "_blank", winAttr);
                var writeDoc = newWin.document;
                writeDoc.open();
                writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                writeDoc.close();
                newWin.focus();

            };
            $scope.block1.gldd_acct_code = ' ';
            $scope.viewsubtitution = function (obj) {
                debugger;
                $scope.block1.gldd_acct_code = obj.gltr_acct_code;
                $scope.block1.gldd_acct_name = obj.gltr_acct_code + '-' + obj.glac_name;
                $scope.block1.from_date = obj.sims_financial_year_start_date;
                $scope.block1.to_date = obj.sims_financial_year_end_date;
                $scope.block1.gltd_doc_code = '';
                $('#MyModal6').modal('show');

                $scope.tble_show = true;

                $('#loader').modal({ backdrop: 'static', keyboard: false });
                var data = [];
                 data = {
                    gltr_comp_code: comp_code,
                    gltr_doc_code: ($scope.block1['gltd_doc_code'] != undefined || $scope.block1['gltd_doc_code'] != "") ? $scope.block1['gltd_doc_code'] : null,
                    gltr_acct_code: ($scope.block1.gldd_acct_code != undefined || $scope.block1.gldd_acct_code != "") ? $scope.block1.gldd_acct_code : null,
                    gltr_pstng_date: ($scope.block1.from_date != undefined || $scope.block1.from_date != "") ? $scope.block1.from_date : null,
                    gltr_doc_date: ($scope.block1.to_date != undefined || $scope.block1.to_date != "") ? $scope.block1.to_date : null,
                    glco_cost_centre_code: ($scope.block1['glco_cost_centre_code'] != undefined || $scope.block1['glco_cost_centre_code'] != "") ? $scope.block1['glco_cost_centre_code'] : null,
                }
                var close_amt;
                $scope.closebal = 0;
                $http.post(ENV.apiUrl + "api/GLSLQuery/list_All_GL_Query", data).then(function (res1) {
                    $scope.trans_data = res1.data;

                    $('#loader').modal('hide');
                    $(".modal-backdrop").removeClass("modal-backdrop");

                    if ($scope.trans_data.length > 0) {
                        $scope.length = $scope.trans_data.length - 1;

                        if ($scope.block1.sltr_yob_amt > 0) {
                            $scope.trans_data[$scope.length].gltr_total_dr_amount = parseFloat($scope.trans_data[$scope.length].gltr_total_dr_amount) + $scope.block1.sltr_yob_amt;
                        }
                        else if ($scope.block1.sltr_yob_amt < 0) {
                            $scope.trans_data[$scope.length].gltr_total_cr_amount = parseFloat($scope.trans_data[$scope.length].gltr_total_cr_amount) + Math.abs($scope.block1.sltr_yob_amt);
                        }
                        close_amt = parseFloat($scope.trans_data[$scope.length].gltr_total_dr_amount) - parseFloat($scope.trans_data[$scope.length].gltr_total_cr_amount);

                        if (close_amt > 0) {
                            $scope.closebal = close_amt;
                            $scope.dr = ' dr';
                        }
                        else if (close_amt < 0) {
                            $scope.closebal = Math.abs(close_amt);
                            $scope.dr = ' cr';
                        }
                        else
                            $scope.closebal = close_amt;
                    }

                });
                
               
            }



          

        }])

})();

