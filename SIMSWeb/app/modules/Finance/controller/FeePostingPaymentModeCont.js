﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main;
    var finanacecode = [];
    var deletefin = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('FeePostingPaymentModeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            var user = $rootScope.globals.currentUser.username.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            setTimeout(function () {
                $("#Accno").select2();

            }, 100);

            setTimeout(function () {
                $("#cmb_adv_ac").select2();

            }, 100);

            setTimeout(function () {
                $("#cmb_refund_ac").select2();

            }, 100);


            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/FeePosting/getFeePosting?comp_code=" + '0'+comp_code + "&finance_year=" + finance_year).then(function (res1) {
                $scope.feepost = res1.data;
                $scope.totalItems = $scope.feepost.length;
                $scope.todos = $scope.feepost;
                $scope.makeTodos();

            });

            $scope.getdata = function () {
                $http.get(ENV.apiUrl + "api/FeePosting/getFeePosting?comp_code=" + '0' + comp_code + "&finance_year=" + finance_year).then(function (res1) {
                    $scope.feepost = res1.data;
                    $scope.totalItems = $scope.feepost.length;
                    $scope.todos = $scope.feepost;
                    $scope.makeTodos();

                });
            }

            //Fill Combo PaymentMode
            $http.get(ENV.apiUrl + "api/FeePosting/GetPaymentMode").then(function (paymode) {
                
                $scope.pay_mode = paymode.data;
            });



            //Fill Combo GLAccountNo
            $http.get(ENV.apiUrl + "api/FeePosting/GetGLAccountNumber?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (docstatus1) {
                $scope.GlACNO = docstatus1.data;
            });


            
            //$http.get(ENV.apiUrl + "api/FeePosting/GetGLAdvancedAccountNumber?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (docstatus18) {
            //    $scope.GlACNO1 = docstatus18.data;
            //});

            $scope.temp = {};
            //Currcode
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (currname) {
                $scope.Cname = currname.data;
                //$scope.temp = {
                //    sims_cur_code: $scope.Cname[0].sims_cur_code
                //}

                if (currname.data.length > 0) {
                    $scope.temp.sims_cur_code = currname.data[0].sims_cur_code;
                    $scope.getacyr($scope.temp.sims_cur_code);
                }
                $scope.getacyr($scope.Cname[0].sims_cur_code)
            });

            //Fill Combo AcademicYear
            $scope.getacyr = function (str) {

                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + str).then(function (Academicyear) {
                    $scope.Academic_year = Academicyear.data;
                    if (Academicyear.data.length > 0) {
                        $scope.temp.sims_academic_year = Academicyear.data[0].sims_academic_year;
                        //$scope.year($scope.edt.sims_cur_code, $scope.edt.sims_academic_year);
                    }
                })
            }


            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.feepost;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                 $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);


                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.feepost, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.feepost;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.fins_fee_payment_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.fins_fee_acno.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "/" + month + "/" + day;
            }


            //NEW BUTTON
            $scope.New = function () {
                $scope.temp = {};

                $scope.disabled = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
               // $scope.temp = "";
               
                $scope.currcode1 = false;
                $scope.acdyear1 = false;
                $scope.Ptype1 = false;

                $("#Accno").select2("val", "");
                $("#cmb_adv_ac").select2("val", "");
                $("#cmb_refund_ac").select2("val", "");

                $scope.temp.fins_fee_posting_status = true;

                if ($scope.Cname.length > 0) {
                    $scope.temp.sims_cur_code = $scope.Cname[0].sims_cur_code;
                    $scope.getacyr($scope.temp.sims_cur_code);
                }
                if ($scope.Academic_year.length > 0) {
                    $scope.temp.sims_academic_year = $scope.Academic_year[0].sims_academic_year;
                }


            }

            //EnableDesable

            $scope.setvisible = function () {
                
                $scope.ldgno = true;
                $scope.slno = true;
            }


            //DATA SAVE INSERT
            var dataforSave = [];
            $scope.savedata = function (Myform) {
                
                dataforSave = [];
                data = [];
                
                if (Myform) {
                    var data = $scope.temp;
                    data.opr = 'I';

                    //data.pb_comp_code = '2016';
                    dataforSave.push(data);
                    $http.post(ENV.apiUrl + "api/FeePosting/CUDFeePostingPaymentMode", dataforSave).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: $scope.msg1.strMessage, width: 300, height: 200 });
                          
                        }
                        else {
                            swal({ title: "Alert", text: $scope.msg1.strMessage, width: 300, height: 200 });
                        }
                        $scope.getdata();
                        $scope.table = true;
                        $scope.display = false;
                      

                    });
                    dataforSave = [];

                }
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger;
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;

               
                // $scope.temp = str;
                var fins_fee_acno_old = 'fins_fee_acno_old';

                $scope.temp = {
                    sims_cur_code: str.fins_fee_cur_code

                  , sims_academic_year: str.fins_fee_academic_year

                  , sims_appl_parameter: str.fins_fee_payment_type

                  , acno: str.fins_fee_acno
                  , acno_code: str.fins_fee_advance_acno
                  , fins_fee_refund_acno: str.fins_fee_refund_acno
                 , fins_fee_posting_status: str.fins_fee_posting_status
                   , fins_fee_acno_old: str.fins_fee_acno
                };
                $("#Accno").select2("val", str.fins_fee_acno);
                $("#cmb_adv_ac").select2("val", str.fins_fee_advance_acno);
                $("#cmb_refund_ac").select2("val", str.fins_fee_refund_acno);



              //  $scope.getacyr(str.fins_fee_cur_code);

                $scope.currcode1 = true;
                $scope.acdyear1 = true;
                $scope.Ptype1 = true;

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function () {
              
                var data = $scope.temp;

                
                data = {
                    opr: "U",
                    sims_cur_code: $scope.temp.sims_cur_code,
                    sims_academic_year: $scope.temp.sims_academic_year,
                    sims_appl_parameter: $scope.temp.sims_appl_parameter,
                    acno: $scope.temp.acno,
                    acno_code:$scope.temp.acno_code,
                    fins_fee_acno_old: $scope.temp.fins_fee_acno_old,
                    fins_fee_refund_acno: $scope.temp.fins_fee_refund_acno,
                    fins_fee_posting_status: $scope.temp.fins_fee_posting_status


                };
                dataforUpdate.push(data);
                //dataupdate.push(data);

                $http.post(ENV.apiUrl + "api/FeePosting/CUDFeePostingPaymentMode", dataforUpdate).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: $scope.msg1.strMessage, width: 300, height: 200 });
                    }
                    else {
                        swal({ title: "Alert", text: $scope.msg1.strMessage, width: 300, height: 200 });
                    }
                    $scope.getdata();
                    $scope.table = true;
                    $scope.display = false;

                });
                dataforUpdate = [];


            }

            //DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].fins_fee_cur_name + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].fins_fee_cur_name + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

         

            $scope.OkDelete = function () {
                
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].fins_fee_cur_name + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_cur_code': $scope.filteredTodos[i].fins_fee_cur_code,
                            'sims_academic_year': $scope.filteredTodos[i].fins_fee_academic_year,
                            'sims_appl_parameter': $scope.filteredTodos[i].fins_fee_payment_type,
                            'acno': $scope.filteredTodos[i].fins_fee_acno,
                            'acno_code': $scope.filteredTodos[i].fins_fee_advance_acno,
                            'fins_fee_refund_acno': $scope.filteredTodos[i].fins_fee_refund_acno,
                            'fins_fee_posting_status': $scope.filteredTodos[i].fins_fee_posting_status,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                          
                            $http.post(ENV.apiUrl + "api/FeePosting/CUDFeePostingPaymentMode", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: $scope.msg1.strMessage, width: 300, height: 200 }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getdata();
                                            $scope.table = true;
                                            $scope.display = false;
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal({ title: "Alert", text: $scope.msg1.strMessage, width: 300, height: 200 }).then(function (isConfirm) {
                                        if (isConfirm) {

                                            $scope.getdata();
                                            $scope.table = true;
                                            $scope.display = false;
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].fins_fee_cur_name + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;

            }




            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });


        }])

})();
