﻿(function () {
    'use strict';
    var del = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('VoucherDataImportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', 'XLSXReaderService', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, XLSXReaderService, $filter) {

            $scope.rgvtbl = false;        
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);

            var username = $rootScope.globals.currentUser.username;
            var comp_code = '';
            var finance_year ='';

            if (!jQuery.isEmptyObject(window.localStorage["Finn_comp"])) {
                $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
                comp_code = $scope.finnDetail.company;
                finance_year = $scope.finnDetail.year;
            }

            $http.get(ENV.apiUrl + "api/common/Empdashboard/GetDashboardData").then(function (res1) {

                $scope.portal_data = res1.data;

                if (res1.data.length > 0) {
                    $scope.portal_data = res1.data;
                }
                else {
                    $scope.portal_data_flg = '1'
                    $scope.portal_data.push({})
                }
            });

            ///////////////////////////// Upload Doc Code //////////////////////////////////////////

            $scope.displaytable = true;
            $scope.save = true;
            $scope.items = [];

            var date = new Date();
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss');
            $scope.showPreview = false;
            $scope.showJSONPreview = true;
            $scope.json_string = "";
            $scope.itemsPerPage = '50';
            $scope.currentPage = 0;
            $scope.filesize = false;
            $scope.sheets = [];
            $scope.vouchertableshow = true;
            $scope.budgettableshow = true;
            $scope.tileshow = true;

            var formdata = new FormData();
            $scope.getTheFiles = function ($files) {
                $scope.flag = 0;
                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);
                    var i = 0;
                    });
            };

            $scope.tileClick=function(cur){
                $scope.importCode=cur
                $scope.tileshow = false;
                if ($scope.importCode == 'B') {                 
                    $scope.selectedappl = "Budget Import";
                    $scope.bkcolor = '#6dabaf';
                }
                if ($scope.importCode == 'V') {
                    $scope.selectedappl = "Voucher Import";
                    $scope.bkcolor = '#6881b3';
                }
            }
            $scope.cancelClick = function (cur) {
                $scope.tileshow = true;
                $scope.bdgsheetsData = [];
                $scope.sheetsData = [];
                $scope.vouchertableshow = true;
                $scope.budgettableshow = true;
                angular.forEach(
                angular.element("input[type='file']"),
                function (inputElem) {
                    angular.element(inputElem).val(null);
                });
            }
            
            $scope.file_changed = function (element) {
                var photofile = element.files[0];
                $scope.filename = element.files[0];
                $scope.photo_filename = (photofile.type);
                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;
                    });
                };
                reader.readAsDataURL(photofile);
                var f = document.getElementById('file1').files[0];
                $scope.sheets = [];
                $scope.excelFile = element.files[0];
                XLSXReaderService.readFile($scope.excelFile, $scope.showPreview, $scope.showJSONPreview).then(function (xlsxData) {
                    $scope.display = false;
                    $scope.save = false;
                    var size = Object.keys(xlsxData.sheets.Sheet1[0]).length;
                    $scope.sheets = xlsxData.sheets
                    $scope.sheetsData = [];                    
                    if($scope.importCode=='V'){
                        ///////////////  voucher import excel import code  ///////////////                    
                        for (var i = 0; i < $scope.sheets.Sheet1.length; i++) {
                        if ($scope.sheets.Sheet1[i].doc_date != undefined || $scope.sheets.Sheet1[i].doc_date != '') {
                            var date = new Date($scope.sheets.Sheet1[i].doc_date);
                            var month = (date.getMonth() + 1);
                            var day = date.getDate();
                            if (month < 10)
                                month = "0" + month;
                            if (day < 10)
                                day = "0" + day;
                            $scope.d = date.getFullYear() + '-' + (month) + '-' + (day);
                        }
                        var data = {
                            acct_code: $scope.sheets.Sheet1[i].acct_code,
                            acct_name: $scope.sheets.Sheet1[i].acct_name,
                            doc_amount: $scope.sheets.Sheet1[i].doc_amount,
                            doc_code: $scope.sheets.Sheet1[i].doc_code,
                            doc_date:$scope.d,
                            final_doc_no: $scope.sheets.Sheet1[i].final_doc_no,
                            ledger_doc_narr: $scope.sheets.Sheet1[i].ledger_doc_narr,
                            username: username,
                            company_code: comp_code,
                            financial_year: finance_year
                        }
                        $scope.sheetsData.push(data);
                        $scope.vouchertableshow = false;
                        $scope.budgettableshow = true;
                    }
                        ///////////////////// voucher code end /////////////////////////
                    }

                    $scope.bdgsheetsData = [];
                    if ($scope.importCode == 'B') {
                        ///////////////  budget import excel import code  ///////////////
                        for (var i = 0; i < $scope.sheets.Sheet1.length; i++) {
                            if ($scope.sheets.Sheet1[i].doc_date != undefined || $scope.sheets.Sheet1[i].doc_date != '') {
                                var date = new Date($scope.sheets.Sheet1[i].doc_date);
                                var month = (date.getMonth() + 1);
                                var day = date.getDate();
                                if (month < 10)
                                    month = "0" + month;
                                if (day < 10)
                                    day = "0" + day;
                                $scope.d = date.getFullYear() + '-' + (month) + '-' + (day);
                            }
                            var data = {
                                glpr_dept_no: $scope.sheets.Sheet1[i].glpr_dept_no,
                                glpr_dept_name: $scope.sheets.Sheet1[i].glpr_dept_name,
                                glpr_acct_code: $scope.sheets.Sheet1[i].glpr_acct_code,
                                glpr_acct_name: $scope.sheets.Sheet1[i].glpr_acct_name,
                                glpr_bud_amt: $scope.sheets.Sheet1[i].glpr_bud_amt,
                                username: username,
                                company_code:comp_code,
                                financial_year: finance_year
                            }
                            $scope.bdgsheetsData.push(data);
                            $scope.vouchertableshow = true;
                            $scope.budgettableshow = false;
                        }
                        ///////////////////// budget code end /////////////////////////
                    }
                    $scope.busy = true;
                    $scope.errlabel = false;
                });
            };

            $scope.SaveData = function () {
                if ($scope.importCode == 'V') {
                    $http.post(ENV.apiUrl + "api/UpdateVoucher/insertVoucherData", $scope.sheetsData).then(function (res) {
                        if (res.data) {
                            swal('Records Successfully Saved')
                            angular.forEach(
                            angular.element("input[type='file']"),
                            function (inputElem) {
                                angular.element(inputElem).val(null);
                            });
                            $scope.sheetsData = [];
                        } else {
                            swal('Records Not Saved')
                        }
                    });
                }

                if ($scope.importCode == 'B') {
                    $http.post(ENV.apiUrl + "api/UpdateVoucher/insertBudgetData", $scope.bdgsheetsData).then(function (res) {
                        if (res.data!="") {
                            swal(res.data)
                            angular.forEach(
                            angular.element("input[type='file']"),
                            function (inputElem) {
                                angular.element(inputElem).val(null);
                            });
                            $scope.bdgsheetsData = [];
                        } else {
                            swal('Data Not Imported')
                        }
                    });
                }
            }


            $scope.uploadClick = function () {
                // $scope.filesize = true;
               formdata = new FormData();
            }



            $scope.cancel = function () {
                $scope.display = true;
                $scope.sheets = "";
            }


            $scope.showPreviewChanged = function (selectedSheetName) {
                $scope.sheets[$scope.selectedSheetName];
            }
          
            $scope.downloadFormat = function () {
                if ($scope.importCode == 'V') {
                    $scope.url = "http://api.mograsys.com/kindoapi/content/leams/VoucherDataImport.xls";
                    window.open($scope.url, '_self', 'location=yes');
                }
                if ($scope.importCode == 'B') {
                    $scope.url = "http://api.mograsys.com/kindoapi/content/leams/budget_data_import.xls";
                    window.open($scope.url, '_self', 'location=yes');
                }

            }

        }]);


    simsController.directive('ngFiles', ['$parse', '$scope', function ($parse, $scope) {
        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])


    simsController.factory("XLSXReaderService", ['$q', '$rootScope',
    function ($q, $rootScope) {
        var service = function (data) {
            angular.extend(this, data);
        }

        service.readFile = function (file, readCells, toJSON) {
            var deferred = $q.defer();

            XLSXReader(file, readCells, toJSON, function (data) {
                $rootScope.$apply(function () {
                    deferred.resolve(data);
                });
            });

            return deferred.promise;
        }


        return service;
    }
    ])



})();