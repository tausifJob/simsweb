﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Finance');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('SubledgerMasterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.grid = true;
            $scope.edit_data = false;
            $scope.subledger_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.dept_comp_code;
            $scope.slma_acno_dept_no;
            $scope.finyr;
            $scope.valstatus = false;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };



            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            /*$http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
                $scope.comp_data = res.data;
                $scope.dept_comp_code = $scope.comp_data[0].dept_comp_code;

                $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetCurrentFinancialYear").then(function (res) {
                    $scope.finyr_data = res.data;
                    $scope.finyr = $scope.finyr_data;

                    $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetAllLedgerCodes?comp_code=" + $scope.dept_comp_code + "&fin_yr=" + $scope.finyr).then(function (res) {
                        $scope.LedgerCode_data = res.data;
                    });

                    $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetAllSLAccountNos?comp_code=" + $scope.dept_comp_code + "&financial_year=" + $scope.finyr).then(function (res) {
                        $scope.accno_data = res.data;
                    });

                    $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetForeignCurrency?comp_code=" + $scope.dept_comp_code).then(function (res) {
                        $scope.currency_data = res.data;
                    });

                });
            });*/
            $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetAllLedgerCodes?comp_code=" + comp_code + "&fin_yr=" + finance_year).then(function (res) {
                $scope.LedgerCode_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetAllSLAccountNos?comp_code=" + comp_code + "&financial_year=" + finance_year).then(function (res) {
                $scope.accno_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetForeignCurrency?comp_code=" + comp_code).then(function (res) {
                $scope.currency_data = res.data;
            });
            $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetAllPartyType").then(function (res) {
                $scope.ptytype_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetAllAddressType").then(function (res) {
                $scope.addtype_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetAllPartyAddressId").then(function (res) {
                $scope.refadd_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetAllBanks").then(function (res) {
                $scope.bank_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetAllRegion").then(function (res) {
                $scope.region_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetAllCountry?region=" + null).then(function (res) {
                $scope.country_data = res.data;
            });

            $scope.getcountry = function (region) {
                if (region != undefined) {
                    $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetAllCountry?region=" + region).then(function (res) {
                        $scope.country_data = res.data;
                    });
                }
            }

            $scope.countData = [
                 { val: 10, data: 10 },
                 { val: 20, data: 20 },
                // { val: 15, data: 15 },

            ]

            var dom;
            $scope.flag = true;
            //var dom;
            //$scope.flag = true;
            $scope.icon = "fa fa-plus-circle";

            $scope.expand = function (mod, $event) {
                
                if ($scope.flag == true) {
                    $(dom).remove();
                    mod.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='18'>" +


                        "<table class='table  table-bordered  table-condensed dataTable'>" +
                        "<tbody>" +

                         "<tr style='background-color: #e9e4e4'><td class='semi-bold'>" + "Ledger Code" + "</td>"
                         + "<td class='semi-bold'>" + "Account No" + "</td>"
                         + "<td class='semi-bold'>" + "Address Id" + "</td>"
                         + "<td class='semi-bold'>" + "Year Opening Balance" + "</td>"
                         + "<td class='semi-bold'>" + "Outstanding DR Amt" + "</td>"
                         + "<td class='semi-bold'>" + "Outstanding CR Amt" + "</td> "
                         + "<td class='semi-bold'>" + "Forign Currency Balance Amt" + "</td>"
                         + "<td class='semi-bold'>" + "Today DR Amt" + "</td>"
                         + "<td class='semi-bold'>" + "Today CR Amt" + "</td>"
                         + "<td class='semi-bold'>" + "Credit Limit" + "</td>"
                         + "<td class='semi-bold'>" + "Loan Installment Amt" + "</td>"
                         + "<td class='semi-bold'>" + "Origin Loan Amt" + "</td>"
                         + "<td class='semi-bold'>" + "Override Amt" + "</td>"
                         + "<td class='semi-bold'>" + "Bank Closing Amt" + "</td>"
                         + "<td class='semi-bold'>" + "Bank Statement Date" + "</td>"
                         + "<td class='semi-bold'>" + "Document Date" + "</td>"
                         + "<td class='semi-bold'>" + "Loan Installment Type" + "</td>"
                         + "<td class='semi-bold'>" + "Credit Period" + "</td>"
                         + "<td class='semi-bold'>" + "Override Period" + "</td>"
                         + "<td class='semi-bold'>" + "Statement Period" + "</td>"
                         + "<td class='semi-bold'>" + "Schedule Code" + "</td>"
                         + "<td class='semi-bold'>" + "last Payment Date" + "</td>"
                         + "<td class='semi-bold'>" + "Interest Rate" + "</td>"
                         + "<td class='semi-bold'>" + "Interest Period" + "</td>"
                         + "<td class='semi-bold'>" + "Term Name" + "</td>"
                         + "<td class='semi-bold'>" + "Initial outstanding DR Amt" + "</td>"
                         + "<td class='semi-bold'>" + "Initial outstanding CR Amt" + "</td>"
                         + "<td class='semi-bold'>" + "Initial Year Opening Balance" + "</td>"
                         + "<td class='semi-bold'>" + "Revaluation Date" + "</td>"
                         + "<td class='semi-bold'>" + "Origional Party Class" + "</td>"
                         + "<td class='semi-bold'>" + "Credit Remark" + "</td></tr>" +

                          "<tr><td>" + (mod.slma_ldgrctl_code) + "</td> "
                          + "<td>" + (mod.slma_acno) + "</td>"
                          + "<td>" + (mod.slma_addr_id) + "</td>"
                          + "<td>" + (mod.slma_yob_amt) + "</td>"
                          + "<td>" + (mod.slma_outstg_dr_amt) + "</td>"
                          + "<td>" + (mod.slma_outstg_cr_amt) + "</td>"
                          + "<td>" + (mod.slma_for_curcy_bal_amt) + "</td>"
                          + "<td>" + (mod.slma_tdy_dr_amt) + "</td>"
                          + "<td>" + (mod.slma_tdy_cr_amt) + "</td>"
                          + "<td>" + (mod.slma_cr_limit) + "</td>"
                          + "<td>" + (mod.slma_loan_inst_amt) + "</td>"
                          + "<td>" + (mod.slma_orig_loan_amt) + "</td>"
                          + "<td>" + (mod.slma_ovrid_amt) + "</td>"
                          + "<td >" + (mod.slma_bank_clos_bal_amt) + "</td>"
                          + "<td>" + (mod.slma_bank_stmt_date) + "</td> "
                          + "<td>" + (mod.slma_doc_date) + "</td> "
                          + "<td>" + (mod.slma_loan_inst_type) + "</td> "
                          + "<td>" + (mod.slma_credit_prd) + "</td>"
                          + "<td>" + (mod.slma_ovrid_prd) + "</td>"
                          + "<td>" + (mod.slma_stmt_prd) + "</td>"
                          + "<td>" + (mod.slma_sched_code) + "</td>"
                          + "<td>" + (mod.slma_last_pmt_date) + "</td>"
                          + "<td>" + (mod.slma_int_rate) + "</td>"
                          + "<td>" + (mod.slma_int_prd) + "</td>"
                          + "<td>" + (mod.slma_term_name) + "</td>"
                          + "<td>" + (mod.slma_outstg_dr_amt_orig) + "</td>"
                          + "<td>" + (mod.slma_outstg_cr_amt_orig) + "</td>"
                          + "<td>" + (mod.slma_yob_amt_orig) + "</td>"
                          + "<td>" + (mod.slma_reval_date) + "</td>"
                          + "<td>" + (mod.slma_cntrl_class_org) + "</td>"
                          + "<td>" + (mod.slma_credit_remark) + "</td></tr>" +
                        "</tbody>" +
                        "</table>" + "</td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    mod.icon = "fa fa-plus-circle";
                    $scope.flag = true;
                }
            };
            $scope.getpartyclass = function (ledger_code) {
                debugger;
                if (ledger_code != undefined) {
                    $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetAllPartyControlClass?comp_code=" + comp_code + "&fin_yr=" + finance_year + "&ledger_code=" + ledger_code).then(function (res) {
                        $scope.partyclass_data = res.data;
                    });
                }
            }

            $scope.sublist = function () {
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
            }
            $scope.getsubledgerdata = function (ledger_code, detail) {
                
                detail = document.getElementById('txt_detail').value;
                $scope.display = false;
                $scope.grid = true;

                $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/Get_Subledger_Master?lgr_cd=" + ledger_code + "&search_text=" + detail + "&comp_code=" + comp_code + "&financial_year=" + finance_year).then(function (res) {

                    $scope.subledger_data = res.data;
                    if ($scope.subledger_data.length > 0) {

                        $scope.grid = true;
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.subledger_data.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.subledger_data.length, data: 'All' })
                        }

                        $scope.totalItems = $scope.subledger_data.length;
                        $scope.todos = $scope.subledger_data;
                        $scope.makeTodos();

                        for (var i = 0; i < $scope.totalItems; i++) {
                            $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                        }
                        // $scope.sublist();


                    }
                    else {

                        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                    }
                });
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edit = function (str) {
                debugger;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edit_data = true;
                $scope.edt = str;
                //document.getElementById('cmb_ref_led_code').disabled = true;
                //document.getElementById('cmb_ref_acc_no').disabled = true;
                $scope.getpartyclass($scope.edt.slma_ldgrctl_code);
                $scope.getcountry($scope.edt.coad_region_code);

                //$http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
                //    $scope.comp_data = res.data;
                
                //    $scope.dept_comp_code = $scope.comp_data[0].dept_comp_code;

                //    $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetCurrentFinancialYear").then(function (res) {
                //        $scope.finyr_data = res.data;
                //        $scope.finyr = $scope.finyr_data;

                //        $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetAllLedgerCodes?comp_code=" + $scope.dept_comp_code + "&fin_yr=" + $scope.finyr).then(function (res) {
                //            $scope.LedgerCode_data = res.data;
                //        });

                //        $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetAllSLAccountNos?comp_code=" + $scope.dept_comp_code + "&financial_year=" + $scope.finyr).then(function (res) {
                //            $scope.accno_data = res.data;
                //        });

                //        $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetForeignCurrency?comp_code=" + $scope.dept_comp_code).then(function (res) {
                //            $scope.currency_data = res.data;
                //        });

                //    });
                //});
            }

            $scope.New = function () {
                var autoid;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edit_data = false;
                $scope.edt = [];
                var dt = new Date();
                $scope.edt['slma_pmt_start_date'] = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                $scope.edt.slma_status = true;
                //document.getElementById('cmb_ref_led_code').disabled = false;
                //document.getElementById('cmb_ref_acc_no').disabled = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();

            }

            function convertdate(dt) {
                if (dt != null) {
                    var d1 = new Date(dt);
                    var month = d1.getMonth() + 1;
                    var day = d1.getDate();
                    if (month < 10)
                        month = "0" + month;
                    if (day < 10)
                        day = "0" + day;
                    var d = d1.getFullYear() + "-" + (month) + "-" + (day);
                    $scope.convertdated = d;
                    return d;
                }
            }


           
            $scope.chkAccountNo = function (acc_no) {
                
                if (acc_no != undefined || acc_no != "") {
                    $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/getCheck_SL_acno?comp_code=" + comp_code + "&financial_year=" + finance_year + "&ctl_code=" + $scope.edt.slma_ldgrctl_code + "&acc_no=" + $scope.edt.slma_acno).then(function (res) {
                        $scope.SL_acno_data = res.data;
                        if ($scope.SL_acno_data == true) {
                            swal({ title: "Alert", text: "Account No already Exist"});
                            $scope.edt.slma_acno = "";
                        }
                    });
                }
                $scope.edt.slma_addr_id = $scope.edt.slma_acno;
            }

            $scope.chkRefAccntNo = function (acc_no) {
                if (acc_no != undefined || acc_no != "") {
                    if (acc_no == $scope.edt.slma_acno) {
                        swal({ title: "Alert", text: "Please Select Another Reference Account No", imageUrl: "assets/img/check.png", });
                        $scope.edt.slma_xref_acno = "";
                    }
                }
            }

            $scope.Save = function (isvalidate) {
                debugger;
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = ({
                            slma_comp_code: comp_code,
                            financialyear: finance_year,
                            slma_ldgrctl_code: $scope.edt.slma_ldgrctl_code,
                            slma_acno: $scope.edt.slma_acno,
                            slma_addr_id: $scope.edt.slma_addr_id,
                            slma_addr_type: $scope.edt.slma_addr_type,
                            slma_status: $scope.edt.slma_status,
                            slma_cntrl_class: $scope.edt.slma_cntrl_class,
                            slma_pty_type: $scope.edt.slma_pty_type,
                            slma_ctry_code: $scope.edt.slma_ctry_code,
                            slma_xref_ldgr_code: $scope.edt.slma_xref_ldgr_code,
                            slma_xref_acno: $scope.edt.slma_xref_acno,
                            slma_fc_flag: $scope.edt.slma_fc_flag,
                            slma_qty_flag: $scope.edt.slma_qty_flag,
                            slma_remove_tran: $scope.edt.slma_remove_tran,
                            slma_cr_limit: $scope.edt.slma_cr_limit,
                            slma_curcy_code: $scope.edt.slma_curcy_code,
                            slma_pty_bank_acno: $scope.edt.slma_pty_bank_acno,
                            slma_pty_bank_id: $scope.edt.slma_pty_bank_id,
                            slma_pmt_start_date: $scope.edt.slma_pmt_start_date,
                            slma_credit_prd: $scope.edt.slma_credit_prd,
                            slma_sched_code: $scope.edt.slma_sched_code,
                            coad_dept_no: $scope.partyclass_data[0].slma_acno_dept_no,
                            coad_xref_addr_id: $scope.edt.slma_addr_id,
                            coad_pty_full_name: $scope.edt.coad_pty_full_name,
                            coad_pty_short_name: $scope.edt.coad_pty_short_name,
                            coad_alt_key: $scope.edt.coad_alt_key,
                            coad_pty_arab_name: $scope.edt.coad_pty_arab_name,
                            coad_line_1: $scope.edt.coad_line_1,
                            coad_line_2: $scope.edt.coad_line_2,
                            coad_po_box: $scope.edt.coad_po_box,
                            coad_tel_no: $scope.edt.coad_tel_no,
                            coad_fax: $scope.edt.coad_fax,
                            coad_telex: $scope.edt.coad_telex,
                            coad_region_code: $scope.edt.coad_region_code,
                            coad_class_level1: $scope.edt.coad_class_level1,
                            coad_class_level2: $scope.edt.coad_class_level2,
                            coad_class_level3: $scope.edt.coad_class_level3,
                            coad_class_level4: $scope.edt.coad_class_level4,
                            coad_class_level5: $scope.edt.coad_class_level5,
                            coad_sms_no: $scope.edt.coad_sms_no,
                            coad_email: $scope.edt.coad_email,
                            opr: 'I',
                            opr_ins: 'C'
                        });

                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/common/SubLedgerMaster/CUDInsertSubLedgerMaster", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Added Successfully", imageUrl: "assets/img/check.png" });
                                $scope.getsubledgerdata($scope.edt.slma_ldgrctl_code, $scope.edt.sims_search);

                            }
                            else if ($scope.msg1 == false) {
                                swal({ title: "Alert", text: "Record Not Inserted. " , imageUrl: "assets/img/notification-alert.png" });
                                $scope.getsubledgerdata($scope.edt.slma_ldgrctl_code, $scope.edt.sims_search);
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.Update = function () {
                debugger
               // if (isvalidate) {
                    var data = ({
                        slma_comp_code: comp_code,
                        financialyear: finance_year,
                        slma_ldgrctl_code: $scope.edt.slma_ldgrctl_code,
                        slma_acno: $scope.edt.slma_acno,
                        slma_addr_id: $scope.edt.slma_addr_id,
                        slma_addr_type: $scope.edt.slma_addr_type,
                        slma_status: $scope.edt.slma_status,
                        slma_cntrl_class: $scope.edt.slma_cntrl_class,
                        slma_pty_type: $scope.edt.slma_pty_type,
                        slma_ctry_code: $scope.edt.slma_ctry_code,
                        slma_xref_ldgr_code: $scope.edt.slma_xref_ldgr_code,
                        slma_xref_acno: $scope.edt.slma_xref_acno,
                        slma_fc_flag: $scope.edt.slma_fc_flag,
                        slma_qty_flag: $scope.edt.slma_qty_flag,
                        slma_remove_tran: $scope.edt.slma_remove_tran,
                        slma_cr_limit: $scope.edt.slma_cr_limit,
                        slma_curcy_code: $scope.edt.slma_curcy_code,
                        slma_pty_bank_acno: $scope.edt.slma_pty_bank_acno,
                        slma_pty_bank_id: $scope.edt.slma_pty_bank_id,
                        slma_pmt_start_date: $scope.edt.slma_pmt_start_date,
                        slma_credit_prd: $scope.edt.slma_credit_prd,
                        slma_sched_code: $scope.edt.slma_sched_code,
                        coad_dept_no: $scope.partyclass_data[0].slma_acno_dept_no,
                        coad_xref_addr_id: $scope.edt.slma_addr_id,
                        coad_pty_full_name: $scope.edt.coad_pty_full_name,
                        coad_pty_short_name: $scope.edt.coad_pty_short_name,
                        coad_alt_key: $scope.edt.coad_alt_key,
                        coad_pty_arab_name: $scope.edt.coad_pty_arab_name,
                        coad_line_1: $scope.edt.coad_line_1,
                        coad_line_2: $scope.edt.coad_line_2,
                        coad_po_box: $scope.edt.coad_po_box,
                        coad_tel_no: $scope.edt.coad_tel_no,
                        coad_fax: $scope.edt.coad_fax,
                        coad_telex: $scope.edt.coad_telex,
                        coad_region_code: $scope.edt.coad_region_code,
                        coad_class_level1: $scope.edt.coad_class_level1,
                        coad_class_level2: $scope.edt.coad_class_level2,
                        coad_class_level3: $scope.edt.coad_class_level3,
                        coad_class_level4: $scope.edt.coad_class_level4,
                        coad_class_level5: $scope.edt.coad_class_level5,
                        coad_sms_no: $scope.edt.coad_sms_no,
                        coad_email: $scope.edt.coad_email,
                        opr: 'U',
                        opr_ins: ''
                    });

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/common/SubLedgerMaster/CUDUpdateSubLedgerMaster", data1).then(function (res) {
                        
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            
                            swal({ title: "Alert", text: "Subledger Data Updated Successfully", imageUrl: "assets/img/check.png" });

                            $scope.getsubledgerdata($scope.edt.slma_ldgrctl_code, $scope.edt.sims_search);
                            data1 = [];
                        }
                        else if ($scope.msg1 == false) {
                            swal({ title: "Alert", text: "Subledger Data Not Updated Successfully. ", imageUrl: "assets/img/notification-alert.png" });
                            $scope.getsubledgerdata($scope.edt.slma_ldgrctl_code, $scope.edt.sims_search);

                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
              //  }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
            }

            $scope.Delete = function () {
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);

                    if (v.checked == true) {
                        var deletemodercode = ({
                            'sims_attendance_cur_code': $scope.filteredTodos[i].sims_attendance_cur_code,
                            'sims_academic_year': $scope.filteredTodos[i].sims_academic_year,
                            'sims_comm_code': $scope.filteredTodos[i].sims_comm_code,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }
                $http.post(ENV.apiUrl + "api/common/Moderator/CUDModerator", deletecode).then(function (res) {
                    $scope.msg1 = res.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: "Subledger Data Deleted Successfully", imageUrl: "assets/img/check.png", },
                        function () {
                            $scope.getgrid();
                        });

                    }
                    else if ($scope.msg1 == false){
                        swal({ title: "Alert", text: "Subledger Data Not Deleted Successfully. " , imageUrl: "assets/img/notification-alert.png", });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                });
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
            }

            $scope.size = function (str) {
                //if (str == 10 || str == 20) {
                //    $scope.pager = true;
                //}
                //else {
                //    $scope.pager = false;
                //}

                //$scope.pagesize = str;
                //$scope.currentPage = 1;
              
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.subledger_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                $scope.pagesize = str;
                $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str;  //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }])
})();