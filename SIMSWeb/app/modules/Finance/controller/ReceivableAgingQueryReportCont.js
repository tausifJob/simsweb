﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Finance');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('ReceivableAgingQueryReportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.temp = {};
            $scope.temp.sims_appl_parameter = '';
            $scope.pagesize = "50";
            $scope.pageindex = 0;
            $scope.pager = true;
            $scope.busy = false;
            $scope.datashow = false;
            $scope.temp.pdc_in_hand = true;
            $scope.temp.pdc_dishonured = false;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.route_dir1 = '';
            $scope.route_name1 = '';
            $scope.date = '';
            var user = $rootScope.globals.currentUser.username;
            $scope.SchoolLogo = ENV.apiUrl + 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/SchoolLogo/';


            $(function () {
                $('#route_dir_box1').multipleSelect({ width: '100%' });
                $('#route_name_box1').multipleSelect({ width: '100%', filter: true });

            });

            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_start_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });


            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_end_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });



            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res1) {
                $scope.cur_data = res1.data;
                $scope.temp.sims_cur_code = $scope.cur_data[0].sims_cur_code;
                $scope.getAcademic_year($scope.cur_data[0].sims_cur_code);
            });

            $scope.getAcademic_year = function (cur_code1) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code1).then(function (res1) {
                    $scope.acad_data = res1.data;
                    $scope.temp.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                    $scope.getGrade($scope.temp.sims_academic_year);

                });
            }

            $http.get(ENV.apiUrl + "api/family_statement/getParent_student_status").then(function (res1) {
                $scope.satus = res1.data;
                $scope.temp.parent_flag = $scope.satus[0].code;
            });

            $scope.getGrade = function (cur_code, acad_year) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%',
                            filter: true
                        });
                    }, 1000);

                });
            }


            $scope.getSection = function (cur_code, academic_year, grade_code) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.temp.sims_cur_code + "&grade_code=" + $scope.temp.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%',
                            filter: true
                        });
                        //$("#cmb_section_code").multipleSelect("checkAll");
                    }, 1000);
                });
            }
            //$scope.parameter = function () {
                $http.get(ENV.apiUrl + "api/recievable_details/getsatusDetails").then(function (acad_status) {
                    $scope.acad_status_data = acad_status.data;
                    setTimeout(function () {
                        $('#cmb_status').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%',
                            filter: true
                        });
                        $("#cmb_status").multipleSelect("checkAll");
                    }, 1000);
                });
            //}

                if ($scope.temp.parent_flag == '1') {
                    debugger;
                    $scope.datashow = true;
                    $scope.colspan = 5;
                }
                else {
                    $scope.datashow = false;
                    $scope.colspan = 3;
                }


                $scope.getdetail = function () {

                    if ($scope.temp.parent_flag == '1') {
                        debugger;
                        $scope.datashow = true;
                        $scope.colspan = 5;
                    }
                    else {
                        $scope.datashow = false;
                        $scope.colspan = 3;
                    }

                $scope.grade = $('#cmb_grade_code').multipleSelect('getSelects', 'text');
                $scope.section = $('#cmb_section_code').multipleSelect('getSelects', 'text');
                $scope.status = $('#cmb_section_code').multipleSelect('getSelects', 'text');
                $scope.cur = $scope.selected_academic_year = $("#cur option:selected").text();
                $scope.acad = $scope.selected_academic_year = $("#acad option:selected").text();
                debugger;
                $scope.pagesize = "50";
                $scope.busy = true;
                $scope.colsvis = false;
                $http.get(ENV.apiUrl + "api/recievable_details/getAllagingDetails?cur_code=" + $scope.temp.sims_cur_code + "&acad_year=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&acade_status=" + $scope.temp.sims_appl_parameter + "&parent_flag=" + $scope.temp.parent_flag).then(function (res1) {
                    if (res1.data.length > 0) {
                        $scope.total_length = angular.copy(res1.data.length);
                        $scope.report_data = res1.data;
                        $scope.totalItems = $scope.report_data.length;
                        $scope.todos = $scope.report_data;
                        $scope.makeTodos();
                        $scope.pager = true;
                        console.log("report_data", $scope.report_data);
                        $scope.temp.balance0to1Year_tatal = 0;
                        $scope.temp.balance1to2Year_tatal = 0;
                        $scope.temp.balance2to3Year_tatal = 0;
                        $scope.temp.graterThan3Year_tatal = 0;
                        $scope.temp.pdcInHand_tatal = 0;
                        $scope.temp.pdcDishonored_tatal = 0;
                        $scope.temp.net_outstanding_tatal = 0;
                        for (var j = 0; j < $scope.report_data.length; j++) {
                            $scope.temp.balance0to1Year_tatal = parseFloat($scope.temp.balance0to1Year_tatal) + parseFloat($scope.report_data[j].balance0to1Year);
                            $scope.temp.balance1to2Year_tatal = parseFloat($scope.temp.balance1to2Year_tatal) + parseFloat($scope.report_data[j].balance1to2Year);
                            $scope.temp.balance2to3Year_tatal = parseFloat($scope.temp.balance2to3Year_tatal) + parseFloat($scope.report_data[j].balance2to3Year);
                            $scope.temp.graterThan3Year_tatal = parseFloat($scope.temp.graterThan3Year_tatal) + parseFloat($scope.report_data[j].graterThan3Year);
                            $scope.temp.pdcInHand_tatal = parseFloat($scope.temp.pdcInHand_tatal) + parseFloat($scope.report_data[j].pdcInHand);
                            $scope.temp.pdcDishonored_tatal = parseFloat($scope.temp.pdcDishonored_tatal) + parseFloat($scope.report_data[j].pdcDishonored);
                            $scope.temp.net_outstanding_tatal = parseFloat($scope.temp.net_outstanding_tatal) + parseFloat($scope.report_data[j].net_outstanding);

                            //
                        }
                                              

                        $scope.busy = false;


                    }
                    else {
                        swal({ title: "Alert", text: " Data Not Found", showCloseButton: true, width: 300, height: 200 });
                        $scope.filteredTodos = [];
                        $scope.pager = false;
                        $scope.temp.balance0to1Year_tatal = 0;
                        $scope.temp.balance1to2Year_tatal = 0;
                        $scope.temp.balance2to3Year_tatal = 0;
                        $scope.temp.graterThan3Year_tatal = 0;
                        $scope.temp.pdcInHand_tatal = 0;
                        $scope.temp.pdcDishonored_tatal = 0;
                        $scope.temp.net_outstanding_tatal = 0;
                    }



                });


                //---------------------------------BreakDown-------------------------------------------//
                $http.get(ENV.apiUrl + "api/recievable_details/getbreakdownDetails?cur_code=" + $scope.temp.sims_cur_code + "&acad_year=" + $scope.temp.sims_academic_year + "&grade_code=" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code + "&acade_status=" + $scope.temp.sims_appl_parameter).then(function (res2) {
                    debugger
                    if (res2.data.length > 0) {
                        $scope.report_data1 = res2.data;
                        //$scope.totalItems = $scope.report_data.length;
                        //$scope.todos = $scope.report_data;
                        //$scope.makeTodos();
                        //$scope.pager = true;
                        console.log("report_data1", $scope.report_data1);
                        $scope.OutstandingData = $scope.report_data1;
                        $scope.cnt_90_total = 0
                        $scope.cnt_75_total = 0
                        $scope.cnt_50_total = 0
                        $scope.cnt_bel_50_total = 0
                        $scope.bal_90_total = 0
                        $scope.bal_75_total = 0
                        $scope.bal_50_total = 0
                        $scope.bal_bel_50_total = 0
                        $scope.fam_amt_total = 0;
                        $scope.bal_amt_total = 0;


                        for (var i = 0; i < $scope.OutstandingData.length; i++) {
                            $scope.bal_90_total = parseFloat($scope.OutstandingData[i].greaterthannintyAMT) + parseFloat($scope.bal_90_total);
                            $scope.bal_75_total = parseFloat($scope.OutstandingData[i].seventyFIVE_nintyAMT) + parseFloat($scope.bal_75_total);
                            $scope.bal_50_total = parseFloat($scope.OutstandingData[i].fifty_seventyfourAMT) + parseFloat($scope.bal_50_total);
                            $scope.bal_bel_50_total = parseFloat($scope.OutstandingData[i].lessthanfiftyAMT) + parseFloat($scope.bal_bel_50_total);
                           

                            $scope.cnt_90_total = parseFloat($scope.OutstandingData[i].greaterthannintyFAMITY) + parseFloat($scope.cnt_90_total);
                            $scope.cnt_75_total = parseFloat($scope.OutstandingData[i].seventyFIVE_nintyFAMITY) + parseFloat($scope.cnt_75_total);
                            $scope.cnt_50_total = parseFloat($scope.OutstandingData[i].fifty_seventyfourFamily) + parseFloat($scope.cnt_50_total);
                            $scope.cnt_bel_50_total = parseFloat($scope.OutstandingData[i].lessthanfiftyFAMITY) + parseFloat($scope.cnt_bel_50_total);

                        }

                        $scope.fam_amt_total = $scope.cnt_90_total + $scope.cnt_75_total + $scope.cnt_50_total  + $scope.cnt_bel_50_total;
                        $scope.bal_amt_total = $scope.bal_90_total + $scope.bal_75_total + $scope.bal_50_total  + $scope.bal_bel_50_total;                    

                        


                    }
                    else {
                        $scope.filteredTodos = [];
                        $scope.pager = false;
                        $scope.busy = false;
                        $scope.cnt_90_total = 0
                        $scope.cnt_75_total = 0
                        $scope.cnt_50_total = 0
                        $scope.cnt_bel_50_total = 0


                        $scope.bal_90_total = 0
                        $scope.bal_75_total = 0
                        $scope.bal_50_total = 0
                        $scope.bal_bel_50_total = 0
                        $scope.fam_amt_total = 0;
                        $scope.bal_amt_total = 0;
                         }



                });
            }


            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.report_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.report_data;
                }
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].report_data;
                        var v = document.getElementById(t);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }

                }


            }


            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_sibling_parent_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.parent_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.colsvis = false;

           
            $scope.exportData = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Print?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('rpt_data1').innerHTML],
                        {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                        });
                        $scope.detailsField = false;
                        saveAs(blob, "ReceivableAgingQueryReport.xls");
                        $scope.colsvis = false;
                        $scope.getdetail();

                    }
                    $scope.getdetail();
                });

            };

            $scope.print = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Print?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var docHead = document.head.outerHTML;
                        var printContents = document.getElementById('rpt_data1').outerHTML;
                        var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
                        var newWin = window.open("", "_blank", winAttr);
                        var writeDoc = newWin.document;
                        writeDoc.open();
                        writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                        writeDoc.close();
                        newWin.focus();
                        $scope.getdetail();

                    }
                    $scope.getdetail();
                });

            };





            $scope.reset_data = function () {
                debugger;              
                $scope.filteredTodos = [];
                $scope.temp.sims_grade_code = '';
                $scope.temp.sims_section_code = '';
                $scope.sims_appl_parameter = false;
                $scope.grade = '';
                $scope.section = '';
                $scope.total_length = '';
                $scope.status = '';
                $scope.searchText = '';
                $scope.cur = '';
                $scope.acad = '';
                $scope.temp.balance0to1Year_tatal = 0;
                $scope.temp.balance1to2Year_tatal = 0;
                $scope.temp.balance2to3Year_tatal = 0;
                $scope.temp.graterThan3Year_tatal = 0;
                $scope.temp.pdcInHand_tatal = 0;
                $scope.temp.pdcDishonored_tatal = 0;
                $scope.temp.net_outstanding_tatal = 0;
                $scope.cnt_90_total = 0
                $scope.cnt_75_total = 0
                $scope.cnt_50_total = 0
                $scope.cnt_bel_50_total = 0
                $scope.bal_90_total = 0
                $scope.bal_75_total = 0
                $scope.bal_50_total = 0
                $scope.bal_bel_50_total = 0
                $scope.fam_amt_total = 0;
                $scope.bal_amt_total = 0;
                $scope.busy = false;
                $scope.pager = false;

                try {
                    $('#cmb_grade_code').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_section_code').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_status').multipleSelect('checkAll');
                } catch (e) {

                }


            }


            //sort
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.size = function (str) {
                debugger;

                if (str == "All") {
                    $scope.currentPage = 1;
                    $scope.filteredTodos = $scope.report_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 50, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                debugger;
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };



        }])

})();

