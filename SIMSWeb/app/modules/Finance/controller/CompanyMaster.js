﻿(function () {
    'use strict';
    var modulecode = [];
    var del = [];
    var main;
    var deletefin = [];
    var flag;
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CompanyMasterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
          
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;

            $scope.div_hide = false;
            $scope.display = false;
            $scope.display1 = false;

            $scope.grid = true;
            $scope.save1 = false;//
            $scope.value = false;
            $scope.itemsPerPage = "5";
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });


            setTimeout(function () {
                $("#PF_Posting").select2();
                $("#Sales_Tax_Posting").select2();
                $("#VAT_Posting").select2();
                $("#Input_VAT_Posting").select2();
                $("#VAT_Roundup_Posting").select2(); 
                $("#languages").select2();
                $('#final_pl_acct').select2();

            }, 100);
           



            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/finance/getAllCompanyMaster_new").then(function (res) {
                $scope.obj = res.data;
                for (var i = 0; i < $scope.obj.length; i++) {
                    $scope.obj[i].icon = "fa fa-plus-circle";
                }               
                $scope.totalItems = $scope.obj.length;
                $scope.todos = $scope.obj;
                $scope.makeTodos();
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            });

            
            $http.get(ENV.apiUrl + "api/BankPayment/GetAllGLAccountNos?glma_accountcode=" + null + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {
                $scope.GlACNO = docstatus.data;
            });


            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);


                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                //console.log(str);
                //$scope.itemsPerPage = str;
                //$scope.currentPage = 1;
                
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.obj;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                }console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();
            }
          

            $http.get(ENV.apiUrl + "api/finance/getComCode").then(function (res) {
                $scope.compcode = res.data;
            });
           
            $scope.currency = function (contrycode) {
               
                $http.get(ENV.apiUrl + "api/finance/getCurrencydesc?contrycode=" + contrycode).then(function (res) {
                    $scope.currencyDesc = res.data;
                });
            }
                     
            $scope.edit = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.value = true;
                    $scope.display = true;
                    $scope.display1 = false;
                    $scope.grid = false;
                    $scope.save1 = false;
                    $scope.update1 = true;
                    $scope.delete1 = false;
                    $scope.edt = str;
                    $scope.currency(str.comp_cntry_code);

                    $("#PF_Posting").select2("val", str.comp_pf_posting_account_code);
                    $("#Sales_Tax_Posting").select2("val", str.comp_sales_tax_posting_account_code);
                    $("#VAT_Posting").select2("val", str.comp_vat_posting_account_code);
                    $("#Input_VAT_Posting").select2("val", str.comp_input_vat_posting_account_code);
                    $("#VAT_Roundup_Posting").select2("val", str.comp_vat_roundup_posting_account_code);
                    $('#final_pl_acct').select2("val", str.comp_final_pl_acct);


                    $scope.edt.vat_percentage = str.comp_vat_per;


                }
            }
            
            $scope.valid365 = function (str) {
                if (str > 365) {
                    swal('', 'Renuwal Remind days should be less than 365');
                    $scope.edt.comp_incorporation_renewal_remind_days = '';
                }
                else {
                    $scope.edt.comp_incorporation_renewal_remind_days = str;
                }

            }
              
            $http.get(ENV.apiUrl + "api/finance/getAllCountryName").then(function (res) {
                $scope.countryNames = res.data;
              
            });

            $scope.Update = function (isvalidate) {
                debugger;
               // if (isvalidate) {
                    $scope.edt.opr = "U";
                    data.push($scope.edt)
                    $http.post(ENV.apiUrl + "api/finance/CUDCompanyMaster_new", data).then(function (msg) {
                        if (msg.data==true) {
                            $scope.grid = true;
                            $scope.display = false;
                            data = [];
                            swal('', 'Record Updated Successfully');

                            $scope.maindata();
                        } else if (msg.data==false) {

                            swal('', "Record Not Updated. " );
                        }
                        else {
                            swal("Error-" + msg.data)
                        }
                        $scope.display = false;
                        $scope.grid = true;
                    });
              //  }
            }
            var data = [];

            $scope.Save = function (isvalidate) {
                debugger;
                if (isvalidate) {
                    $scope.edt.opr = "I"
                    data.push($scope.edt);
                    $http.post(ENV.apiUrl + "api/finance/CUDCompanyMaster_new", data).then(function (msg) {
                        if (msg.data==true) {
                            data = [];
                            swal('', 'Record Inserted Successfully');

                            $scope.maindata();
                        } else if (msg.data==false) {
                            data = [];
                            swal('', "Record Not Inserted. " );
                        }
                        else {
                            swal("Error-" + msg.data)
                        }
                        $scope.display = false;
                        $scope.grid = true;
                    });
                }

            }

            $scope.New = function () {
                debugger;
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $("#PF_Posting").select2("val", "");
                    $("#Sales_Tax_Posting").select2("val", "");
                    $("#VAT_Posting").select2("val", "");
                    $("#Input_VAT_Posting").select2("val", "");
                    $("#VAT_Roundup_Posting").select2("val", "");
                    $("#languages").select2("val", "");
                    $("#final_pl_acct").select2("val", "");

                    $('#vat_percentage').val('0');

                    $scope.display = true;
                    $scope.display1 = false;
                    $scope.grid = false;
                    $scope.save1 = true;
                    $scope.value = false;
                    $scope.update1 = false;
                    $scope.delete1 = false;
                    $scope.vat_percentage = true;

                    $scope.edt = "";
                    $scope.edt.comp_vat_per = 0;
                }
           }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.maindata();
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            //$scope.Delete = function () {
            //    $scope.row1 = '';
            //    //for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //    //    var v = document.getElementById(i);
            //    //    if (v.checked == true) {
            //    //        $scope.filteredTodos[i].opr = "D";
            //    //        del.push($scope.filteredTodos[i]);
            //    //    }
            //    //}
            //    //$http.post(ENV.apiUrl + "api/finance/CUDCompanyMaster", del).then(function (msg) {
            //    //    if (msg.data) {
            //    //        del = [];
            //    //        swal('', 'Record Deleted Successfully');
            //    //             $scope.maindata();
            //    //        $scope.currentPage = 0;
            //    //    } else {
            //    //        del = [];
            //    //        swal('', 'Record Not Deleted');
            //    //    }
            //    //})
            //}
                                   
            $scope.maindata = function () {
                $http.get(ENV.apiUrl + "api/finance/getAllCompanyMaster_new").then(function (res) {
                    $scope.obj = res.data;
                    $scope.div_hide = false;
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.save1 = false;
                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.makeTodos();
                    for (var i = 0; i < $scope.obj.length; i++) {

                        $scope.obj[i].icon = "fa fa-plus-circle";

                    }
                });
            }
        
            $scope.multipledelete = function () {
               
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                   
                    var v = document.getElementById($scope.filteredTodos[i].comp_code+i);
                    $scope.color = '';
                if (main.checked == true) {
                    v.checked = true;
                    $scope.color = '#edefef';
                    $('tr').addClass("row_selected");
                }
                else {
                        v.checked = false;
                        main.checked = false;
                        $('tr').removeClass("row_selected");
                        $scope.color = '';
                    }
                }
                
            }

            $scope.delete_onebyone = function () {
              
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                }
            }


            $scope.Delete = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    deletefin = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].comp_code + i);
                        if (v.checked == true) {
                            $scope.filteredTodos[i].opr = "D";
                            deletefin.push($scope.filteredTodos[i]);
                            $scope.flag = true;
                        }
                    }

                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {

                            if (isConfirm) {

                                $http.post(ENV.apiUrl + "api/finance/CUDCompanyMaster_new", deletefin).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm)
                                        {
                                            if (isConfirm)
                                            {
                                                $scope.maindata();
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    {
                                                        $scope.row1 = '';
                                                    }
                                                }
                                            }

                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ title: "Alert", text: "Record Not Deleted. " , showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.maindata();
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    {
                                                        $scope.row1 = '';
                                                    }
                                                }
                                            }
                                        });
                                        $scope.multipledelete();
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });
                            }
                            else {
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById($scope.filteredTodos[i].comp_code+i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        //$scope.row1 = '';
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                    }
                    $scope.currentPage = true;
                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
            var dom;
            var count = 0;
        

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.comp_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.comp_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.comp_short_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }
            $scope.flag = true;

            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    //dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                    //    "<table class='inner-table' cellpadding='5' cellspacing='0'>" +
                    //    "<tbody>" +
                    //     "<tr><td class='semi-bold'>" + "COUNTRY CODE" + " </td><td class='semi-bold'>" + "CURRENCY CODE" + "</td> <td class='semi-bold'>" + "CURRENCY DECIMAL" + " </td><td class='semi-bold'>" + "DEPT MAX" + "</td><td class='semi-bold'>" + "DEPT MIN" + "</td><td class='semi-bold'>" + "ADDRESS LINE1" + "</td></tr>" +
                    //     "<tr><td class='v-align-middle'>" + (info.comp_cntry_name) + " </td><td>" + (info.comp_curcy_name) + "</td><td class='v-align-middle'>" + (info.comp_curcy_dec) + " </td><td>" + (info.comp_max_dept) + "</td><td>" + (info.comp_min_dept) + "</td><td>" + (info.comp_addr_line1) + "</td> </tr>" +

                    //     "<tr> <td class='semi-bold'>" + "ADDRESS LINE2" + " </td><td class='semi-bold'>" + "ADDRESS LINE3" + "</td> <td class='semi-bold'>" + "TELEPHONE NO" + "</td><td class='semi-bold'>" + "FAX NO" + " </td>" +
                    //    "<td class='semi-bold'>" + "EMAIL" + "</td><td class='semi-bold'>" + "WEB SITE" + " </td></tr>" +

                    //      "<tr><td>" + (info.comp_addr_line2) + " </td><td>" + (info.comp_addr_line3) + "</td><td>" + (info.comp_tel_no) + "</td><td>" + (info.comp_fax_no) + "</td>" +
                    //    "<td>" + (info.comp_email) + "</td><td>" + (info.comp_web_site) + " </td></tr>" +

                    //    " </table></td></tr>")

                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='table table-hover table-bordered table-condensed' cellpadding='5' cellspacing='0'>" +
                        "<tbody>" +
                         "<tr style='background-color: #edefef'><td class='semi-bold'>" + "Country Code" + " </td><td class='semi-bold'>" + "Currency Code" + "</td> <td class='semi-bold'>" + "Currency Decimal" + " </td><td class='semi-bold'>" + "Dept. Max." + "</td><td class='semi-bold'>" + "Dept. Min." + "</td><td class='semi-bold'>" + "Address Line1" + "</td></tr>" +
                         "<tr><td class='v-align-middle'>" + (info.comp_cntry_name) + " </td><td>" + (info.comp_curcy_name) + "</td><td class='v-align-middle'>" + (info.comp_curcy_dec) + " </td><td>" + (info.comp_max_dept) + "</td><td>" + (info.comp_min_dept) + "</td><td>" + (info.comp_addr_line1) + "</td> </tr>" +

                         "<tr style='background-color: #edefef'> <td class='semi-bold'>" + "Address Line2" + " </td><td class='semi-bold'>" + "Address Line3" + "</td> <td class='semi-bold'>" + "Telephone No." + "</td><td class='semi-bold'>" + "Fax No." + " </td>" +
                        "<td class='semi-bold'>" + "Email" + "</td><td class='semi-bold'>" + "Web Site" + " </td></tr>" +

                          "<tr><td>" + (info.comp_addr_line2) + " </td><td>" + (info.comp_addr_line3) + "</td><td>" + (info.comp_tel_no) + "</td><td>" + (info.comp_fax_no) + "</td>" +
                        "<td>" + (info.comp_email) + "</td><td>" + (info.comp_web_site) + " </td></tr>" +




                         "<tr style='background-color: #edefef'> <td class='semi-bold'>" + "Incorporation No." + " </td><td class='semi-bold'>" + "Incorporation Reg Date" + "</td> <td class='semi-bold'>" + "Incorporation Renewal Date" + "</td><td class='semi-bold'>" + "PF No." + " </td>" +
                        "<td class='semi-bold'>" + "PF No." + "</td><td class='semi-bold'>" + "PF Reg. Date" + " </td></tr>" +

                         "<tr><td>" + (info.comp_incorporation_number) + " </td><td>" + (info.comp_incorporation_reg_date) + "</td><td>" + (info.comp_incorporation_renewal_date) + "</td><td>" + (info.comp_incorporation_renewal_remind_days) + "</td>" +
                        "<td>" + (info.comp_pf_number) + "</td><td>" + (info.comp_pf_reg_date) + " </td></tr>" +




                          "<tr style='background-color: #edefef'> <td class='semi-bold'>" + "PF Posting A/c" + " </td><td class='semi-bold'>" + "Sales TAX No" + "</td> <td class='semi-bold'>" + "Sales TAX Reg. Date" + "</td><td class='semi-bold'>" + "Sales TAX Posting A/C" + " </td>" +
                        "<td class='semi-bold'>" + "VAT No." + "</td><td class='semi-bold'>" + "VAT Reg. Date" + " </td></tr>" +

                         "<tr><td>" + (info.comp_pf_posting_account_code) + " </td><td>" + (info.comp_sales_tax_number) + "</td><td>" + (info.comp_sales_tax_reg_date) + "</td><td>" + (info.comp_sales_tax_posting_account_code) + "</td>" +
                        "<td>" + (info.comp_vat_number) + "</td><td>" + (info.comp_vat_reg_date) + " </td></tr>" +


                        "<tr style='background-color: #edefef'> <td class='semi-bold'>" + "VAT Posting A/c" +
                        "<td class='semi-bold'>" + "Input VAT Posting A/c" + " </td><td class='semi-bold'>" + "VAT Enable" +
                        " </td><td class='semi-bold'>" + "VAT Roundup Calculation" + "</td> <td class='semi-bold'>" + "VAT Posting Account Code" +
                        

                         "<tr><td>" + (info.comp_vat_posting_account_code) + " </td><td>" + (info.comp_input_vat_posting_account_code) + " </td><td>" + (info.include_vat_enable_status) + " </td><td>" + (info.comp_vat_roundup_calculation) + "</td><td>" + (info.comp_vat_roundup_posting_account_code) +
                      




                        " </table></td></tr>")

                    $($event.currentTarget).parents("tr").after(dom);

                    $scope.flag = false;
                } else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.obj.length; i++) {

                        $scope.obj[i].icon = "fa fa-plus-circle";

                    }
                    $scope.flag = true;
                }
            };

            $scope.IsChecked = function () {
                
                if ($scope.edt.include_vat_enable_status) {
                    $scope.vat_percentage = false;
                    if ($scope.save1) {
                        $scope.edt.comp_vat_per = 0;
                    }
                    else {
                        $scope.edt.comp_vat_per = $scope.edt.vat_percentage;
                    }
                }
                else {
                    $scope.edt.comp_vat_per = 0;
                    $scope.vat_percentage = true;
                    
                }
            }

        }])
})();