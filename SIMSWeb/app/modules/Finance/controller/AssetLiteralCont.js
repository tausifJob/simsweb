﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('AssetLiteralCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.grid = true;
            $scope.edit_data = false;
            $scope.assetliteral_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.dept_comp_code;
            $scope.slma_acno_dept_no;
            $scope.finyr;
            $scope.valstatus = false;

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
            }, 100);

            setTimeout(function () {
                $("#cmb_accnt_no").select2();
            }, 100);
            
            setTimeout(function () {
                $("#cmb_depr_acc_no").select2();
            }, 100);

            setTimeout(function () {
                $("#cmb_res_acc_no").select2();
            }, 100);

         

            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;



            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            //$http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
            //    $scope.comp_data = res.data;
            //    $scope.dept_comp_code = $scope.comp_data[0].dept_comp_code;
            //});


            debugger
            $http.get(ENV.apiUrl + "api/common/AssetLiteral/GetAllAccountCodes?comp_code=" + comp_code).then(function (res) {
                $scope.accNo_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/AssetLiteral/Get_Asset_Literals?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                $scope.display = false;
                $scope.grid = true;
                $scope.assetliteral_data = res.data;
                $scope.totalItems = $scope.assetliteral_data.length;
                $scope.todos = $scope.assetliteral_data;
                $scope.makeTodos();
                $scope.grid = true;
            });

            $http.get(ENV.apiUrl + "api/common/AssetLiteral/GetAllAssetLiteralsReceiptType").then(function (res) {
                $scope.rectype_data = res.data;
            });

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };
            var t_resrv_acno;
            var t_gal_asset_acno;
            var t_gal_deprn_acno;
            $scope.edit = function (str) {
                debugger
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edit_data = true;
             //   $scope.edt = str;

                $scope.t_resrv_acno = str.gal_resrv_acno;
                $scope.t_gal_asset_acno = str.gal_asset_acno;
                $scope.t_gal_deprn_acno = str.gal_deprn_acno;

                var resrv_acno1 = $scope.t_resrv_acno.toString().slice(-4);
                var gal_asset_acno1 = $scope.t_gal_asset_acno.toString().slice(-4);
                var gal_deprn_acno1 = $scope.t_gal_deprn_acno.toString().slice(-4);

                //var resrv_acno1 = $scope.t_resrv_acno;
                //var gal_asset_acno1 = $scope.t_gal_asset_acno;
                //var gal_deprn_acno1 = $scope.t_gal_deprn_acno;

                $scope.edt = {

                    gal_asst_type: str.gal_asst_type,
                    gal_rec_type: str.gal_rec_type,
                    gal_type_desc: str.gal_type_desc,
                    gal_asset_acno: str.gal_asset_acno,
                    gal_deprn_acno: str.gal_deprn_acno,
                    gal_resrv_acno: str.gal_resrv_acno,
                    gal_deprn_percent: str.gal_deprn_percent
                }

                $("#cmb_accnt_no").select2("val", str.gal_asset_acno);
                $("#cmb_depr_acc_no").select2("val", str.gal_deprn_acno);
                $("#cmb_res_acc_no").select2("val", str.gal_resrv_acno);



            //                  gal_asset_acno: "1100131"
            //                  gal_asset_acno_name: "0131 - COMPUTER EQUIPMENTS"
            //                  gal_asst_type: "Co"
            //                  gal_comp_code: "1"
            //                  gal_comp_code_name: "Demo School"
            //                  gal_deprn_acno: "1107101"
            //                  gal_deprn_acno_name: "7101 - DEPRECIATION"
            //                  gal_deprn_percent: "30.000"
            //                  gal_rec_type: "1"
            //                  gal_rec_type_name: "New"
            //                  gal_resrv_acno: "1100121"
            //                  gal_resrv_acno_name: "0121 - OFFICE EQUIPMENTS"
            //                  gal_type_desc: "Computer"
            //}
            }

            $scope.New = function () {
                var autoid;
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edit_data = false;
                $scope.edt = [];
                $("#cmb_accnt_no").select2("val", "null");
                $("#cmb_depr_acc_no").select2("val", "null");
                $("#cmb_res_acc_no").select2("val", "null");

                if ($rootScope.globals.currentUser.username != null) {
                    $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
                        $scope.comp_data = res.data;
                    });
                }
                else {
                    $rootScope.globals.currentUser.username = null;
                }
            }

            $scope.Save = function (isvalidate) {
                debugger
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                        var data = ({
                            gal_comp_code: comp_code,
                            gal_asst_type: $scope.edt.gal_asst_type,
                            gal_rec_type: $scope.edt.gal_rec_type,
                            gal_type_desc: $scope.edt.gal_type_desc,
                            gal_asset_acno: $scope.edt.gal_asset_acno,
                            gal_deprn_acno: $scope.edt.gal_deprn_acno,
                            gal_resrv_acno: $scope.edt.gal_resrv_acno,
                            gal_deprn_percent:$scope.edt.gal_deprn_percent,

                            opr: 'I',
                        });

                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/common/AssetLiteral/CUDInsert_Asset_Literals", data1).then(function (res) {
                            $scope.display = true;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({  text: "Asset Literal Added Successfully",imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else if ($scope.msg1 == false) {
                                swal({  text: "Record Already Exists. " ,imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });
                    }
                    else {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/common/AssetLiteral/Get_Asset_Literals?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.assetliteral_data = res.data;
                    $scope.totalItems = $scope.assetliteral_data.length;
                    $scope.todos = $scope.assetliteral_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                });
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Update = function (isvalidate) {
                debugger
                var data1 = [];
                if (isvalidate) {
                    var data = ({
                        gal_comp_code: comp_code,
                        gal_asst_type: $scope.edt.gal_asst_type,
                        gal_rec_type: $scope.edt.gal_rec_type,
                        gal_type_desc: $scope.edt.gal_type_desc,
                        gal_asset_acno: $scope.edt.gal_asset_acno,//+$scope.edt.gal_asset_acno,
                        gal_deprn_acno: $scope.edt.gal_deprn_acno,//$scope.edt.gal_deprn_acno,
                        gal_resrv_acno: $scope.edt.gal_resrv_acno,// + $scope.edt.gal_resrv_acno,
                        gal_deprn_percent: $scope.edt.gal_deprn_percent,
                        opr: 'U',
                    });

                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/common/AssetLiteral/CUDUpdate_Asset_Literals", data1).then(function (res) {
                        $scope.display = true;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({  text: "Asset Literal Updated Successfully", imageUrl: "assets/img/check.png",showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else if ($scope.msg1 == false) {
                            swal({  text: "Asset Literal Not Updated Successfully. ",imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                }
                            });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Delete = function () {
                var deletecode = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i);

                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodercode = ({
                            'gal_comp_code': comp_code,
                            'gal_asst_type': $scope.filteredTodos[i].gal_asst_type,
                            'opr': 'D'
                        });
                        deletecode.push(deletemodercode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/common/AssetLiteral/CUDUpdate_Asset_Literals", deletecode).then(function (res) {
                                $scope.msg1 = res.data;
                                if ($scope.msg1 == true) {
                                    swal({  text: "Asset Literal Deleted Successfully", imageUrl: "assets/img/check.png",showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });

                                }
                                else if ($scope.msg1 == false) {
                                    swal({  text: "Record Already Mapped.Can't be Deleted. " , imageUrl: "assets/img/close.png",showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                                $scope.color = '#edefef';
                                $scope.row1 = '';
                            }

                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                }
                            }
                        }

                    });
                }
                else {
                    swal({  text: "Please Select Atleast One Record",imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.assetliteral_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                $scope.numPerPage = str;
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();
                main.checked = false;
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.assetliteral_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.assetliteral_data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.gal_asst_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.gal_type_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1);
            }
        }])
})();