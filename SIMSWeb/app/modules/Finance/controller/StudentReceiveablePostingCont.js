﻿(function () {
    'use strict';
    var obj1, temp, opr, comp, doc_cd_final_doc_no;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StudentReceiveablePostingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {

            $scope.pagesize = "All";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.display = true;
            $scope.table = false;
            $scope.table2 = false;
            $scope.save_data = false;
            var values = [];
            var abc="All"
            var doc_no;
            var doc_code;
            $scope.btn_report = false;
            $scope.busy = false;

            var username = $rootScope.globals.currentUser.username;
            debugger;
            var dateyear;
            var date = new Date();
            dateyear = $filter('date')(new Date(), 'yyyy/MM/dd');
            var year = dateyear.split("/")[0];

           

            //$scope['year_no'] = year;
          
            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            console.log($scope.finnDetail)
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;


            


            $http.get(ENV.apiUrl + "api/DepreciationJVPost/getMonths").then(function (a_months) {
                debugger
                $scope.months = a_months.data;
            });


           

        

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();

           // $scope.sdate = yyyy + '-' + mm + '-' + dd;
            // $scope.edate = yyyy + '-' + mm + '-' + dd;

            $scope.sdate =  dd + '-' + mm + '-' + yyyy ;
            $scope.edate =  dd + '-' + mm + '-' + yyyy; 



            //$scope.edt = {
            //    from_date: yyyy + '-' + mm + '-' + dd,
            //    to_date: yyyy + '-' + mm + '-' + dd,
            //}
            $scope.from_date = dd + '-' + mm + '-' + yyyy;

            debugger
            var datasend = [];
          

           

           

            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.months_obj;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = 'fa fa-plus-circle';
                }
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.months_obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.months_obj;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_enroll_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_sip_achievement_point == toSearch) ? true : false;
            }

          

            $scope.submit = function () {
                //  $('#loader07123').modal({ backdrop: 'static', keyboard: true });
                debugger
                $scope.busy = true;
                $http.get(ENV.apiUrl + "api/DepreciationJVPost/getstudentreceivableposting?months=" + $scope.month_no + "&comp_code=" + comp_code + "&finance_year=" + finance_year + "&user=" + user).then(function (res) {
                    
                    $scope.months_obj = res.data;
                    if ($scope.months_obj.length > 0) {
                        $scope.totalItems = $scope.months_obj.length;
                        $scope.todos = $scope.months_obj;
                        $scope.makeTodos();
                        $scope.table = true;
                        $scope.save_data = true;
                    }
                    else {
                        swal({ title: "Alert", text: "Data Not Found", showCloseButton: true, width: 380, });
                        $scope.table = false;
                        $scope.save_data = false;
                    }
                  //  $('#loader07123').modal('hide');
                    // $scope.getMonths();
                    $scope.busy = false;
                    $scope.size(abc);
                    $scope.total_expec_amt = 0;
                    $scope.total_paid_amt = 0;
                    $scope.total_posted_amt = 0;
                    $scope.total_posting_amt = 0;

                    var grand_total = 0;

                    for (var i = 0; i < $scope.filteredTodos.length; i++)
                    {
                        $scope.total_expec_amt = parseFloat($scope.total_expec_amt) + parseFloat($scope.filteredTodos[i].expected_amount)

                     //   $scope.total_paid_amt = parseFloat($scope.total_paid_amt) + parseFloat($scope.filteredTodos[i].paid_amount)

                        $scope.total_posted_amt = parseFloat($scope.total_posted_amt) + parseFloat($scope.filteredTodos[i].posted_amount)

                        $scope.total_posting_amt = parseFloat($scope.total_posting_amt) + parseFloat($scope.filteredTodos[i].posting_amount)
                    }
                   

                });
            }

            $scope.Save = function () {
                $http.get(ENV.apiUrl + "api/DepreciationJVPost/getMonthsallUpdateData?months=" + $scope.month_no + "&comp_code=" + comp_code + "&finance_year=" + finance_year + "&user=" + user + "&date=" + $scope.from_date).then(function (pos_data) {
                    $scope.posting_obj = pos_data.data;
                    
                    swal({ title: "Alert", text: "Posting successfully\n" + $scope.posting_obj[0].final_doc_number, showCloseButton: true, width: 380, });
                    $scope.table = false;
                });
            }
            

          
            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy/mm/dd"
                // format: "dd/mm/yyyy"
            });

           

        }])

})();
