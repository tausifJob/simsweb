﻿(function () {
    'use strict';
    var opr = '';
    var deletecode = [];
    var deleteaccountcode = [];
    var main;
    var data1 = [];
    var cmbvalue = "";
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('LedgerCntrlCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            setTimeout(function () {
                $("#account_code").select2();

            }, 100);
            $scope.table1 = true;
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            var user = $rootScope.globals.currentUser.username.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            $http.get(ENV.apiUrl + "api/LedgerCont/getLedgerCntrl?sllc_comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (getLedgerCntrl_Data) {
                $scope.LedgerCntrl_Data = getLedgerCntrl_Data.data;
                $scope.totalItems = $scope.LedgerCntrl_Data.length;
                $scope.todos = $scope.LedgerCntrl_Data;
                $scope.makeTodos();
                });

            $http.get(ENV.apiUrl + "api/common/getFinsmatch").then(function (getFinsmatch_Data) {
                $scope.Finsmatch_Data = getFinsmatch_Data.data;
                });

            $http.get(ENV.apiUrl + "api/common/getAcaYearbyCompCode").then(function (getAcaYearbyCompCode) {
                $scope.AcaYearbyCompCode = getAcaYearbyCompCode.data;
                });

            $http.get(ENV.apiUrl + "api/common/getFinsmatchCode").then(function (getFinsmatchCode) {
                $scope.FinsmatchCode = getFinsmatchCode.data;
                });

            $scope.operation = false;
            $scope.editmode = false;

          


            $scope.size = function (str) {
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;  $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.LedgerCntrl_Data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.edt = "";

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.newmode = true;
                $scope.check = true;
                $scope.edt = '';
                $scope.temp = '';
                $scope.editmode = false;
                opr = 'S';
                $("#account_code").select2("val", "");
                $scope.readonly = false;
                $scope.table1 = false;
                $scope.operation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
               

            }

            $scope.up = function (str) {
                opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.table1 = false;
                $scope.operation = true;

                $scope.edt = str;

                
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.edt = "";

                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.Save = function (myForm) {
                
                if (myForm) {
                    data1 = [];

                    var data = {
                        sllc_comp_code: $scope.finnDetail.company,
                        sllc_year: $scope.finnDetail.year,
                        sllc_ldgr_code: $scope.edt.sllc_ldgr_code,
                        sllc_ldgr_name: $scope.edt.sllc_ldgr_name,
                        sllc_ldgr_type: $scope.edt.sllc_ldgr_type,
                        sllc_match_type: $scope.edt.sllc_match_type,
                        sllc_earlst_pstng_date: $scope.edt.sllc_earlst_pstng_date,
                        opr: 'I'
                    };

                    $scope.exist = false;
                    for (var i = 0; i < $scope.LedgerCntrl_Data.length; i++) {
                        if ($scope.LedgerCntrl_Data[i].sllc_ldgr_code == data.sllc_ldgr_code &&
                            $scope.LedgerCntrl_Data[i].sllc_comp_code == data.sllc_comp_code &&
                            $scope.LedgerCntrl_Data[i].sllc_year == data.sllc_year) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ title: "Alert", text: "Record Already Exits", width: 300, height: 200 });
                    }
                    else {

                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/LedgerCont/LedgerControlCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ title: "Alert", text: "Record Not Inserted. " , width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                            $scope.operation = false;
                            $http.get(ENV.apiUrl + "api/LedgerCont/getLedgerCntrl?sllc_comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (getLedgerCntrl_Data) {
                                $scope.LedgerCntrl_Data = getLedgerCntrl_Data.data;
                                $scope.totalItems = $scope.LedgerCntrl_Data.length;
                                $scope.todos = $scope.LedgerCntrl_Data;
                                $scope.makeTodos();
                                 });
                        });
                    }
                    $scope.table1 = true;
                    $scope.operation = false;
                }
            }

            $scope.Update = function (myForm) {
                if (myForm) {
                    data1 = [];
                    var data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/LedgerCont/LedgerControlCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ title: "Alert", text: "Record Not Updated. ", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $scope.operation = false;
                        $http.get(ENV.apiUrl + "api/LedgerCont/getLedgerCntrl?sllc_comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (getLedgerCntrl_Data) {
                            $scope.LedgerCntrl_Data = getLedgerCntrl_Data.data;
                            $scope.totalItems = $scope.LedgerCntrl_Data.length;
                            $scope.todos = $scope.LedgerCntrl_Data;
                            $scope.makeTodos();
                            });

                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            //$scope.CheckAllChecked = function () {
            //    main = document.getElementById('mainchk');
            //    if (main.checked == true) {
            //        for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //            var v = document.getElementById(i);
            //            v.checked = true;

            //            $('tr').addClass("row_selected");
            //        }
            //    }
            //    else {

            //        for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //            var v = document.getElementById(i);
            //            v.checked = false;
            //            main.checked = false;
            //            $scope.row1 = '';
            //        }
            //    }

            //}
            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sllc_ldgr_code + i);
                    if (main.checked == true) {
                        v.checked = true;

                        $('tr').addClass("row_selected");
                    }

                    else {
                        v.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.deleterecord = function () {
                
                deletecode = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sllc_ldgr_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deleteLedgerCntrlcode = ({
                            'sllc_ldgr_code': $scope.filteredTodos[i].sllc_ldgr_code,
                            'sllc_comp_code': $scope.filteredTodos[i].sllc_comp_code,
                            'sllc_year': $scope.filteredTodos[i].sllc_year,
                            'opr': 'D'
                        });
                        deletecode.push(deleteLedgerCntrlcode);
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/LedgerCont/LedgerControlCUD", deletecode).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/LedgerCont/getLedgerCntrl?sllc_comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (getLedgerCntrl_Data) {
                                                $scope.LedgerCntrl_Data = getLedgerCntrl_Data.data;
                                                $scope.totalItems = $scope.LedgerCntrl_Data.length;
                                                $scope.todos = $scope.LedgerCntrl_Data;
                                                $scope.makeTodos();
                                                });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ title: "Alert", text: "Record Not Deleted. " , showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/LedgerCont/getLedgerCntrl?sllc_comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (getLedgerCntrl_Data) {
                                                $scope.LedgerCntrl_Data = getLedgerCntrl_Data.data;
                                                $scope.totalItems = $scope.LedgerCntrl_Data.length;
                                                $scope.todos = $scope.LedgerCntrl_Data;
                                                $scope.makeTodos();
                                                });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    $scope.row1 = '';
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }

                $scope.currentPage = true;

            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.LedgerCntrl_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.LedgerCntrl_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                     item.company_name1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sllc_ldgr_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.ldgr_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sllc_match_type_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.sllc_year == toSearch) ? true : false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            //----------------------------Modal_PopUp------------------------------------------------------------------------//
            $scope.OpenLdgrControl = function (obj) {
                
                $('#MyModal').modal('show');
                $scope.pagesize1 = '5';


                $scope.slac_comp_code = obj.sllc_comp_code;
                $scope.slac_year = obj.sllc_year;
                $scope.slac_ldgr_code = obj.sllc_ldgr_code;

                $scope.size1 = function (str1) {
                    $scope.pagesize1 = str1;
                    $scope.currentPage1 = 1;
                    $scope.numPerPage1 = str1;  $scope.makeTodos1();
                }
                $scope.index1 = function (str1) {
                    $scope.pageindex1 = str1;
                    $scope.currentPage1 = str1;  $scope.makeTodos1();
                    main1.checked = false;
                    $scope.CheckAllCheckedModal();
                }

                $scope.filteredTodos1 = [], $scope.currentPage1 = 1, $scope.numPerPage1 = 5, $scope.maxSize1 = 5;

                $scope.makeTodos1 = function () {
                    var rem1 = parseInt($scope.totalItems1 % $scope.numPerPage1);
                    if (rem1 == '0') {
                        $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1);
                    }
                    else {
                        $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1) + 1;
                    }
                    var begin1 = (($scope.currentPage1 - 1) * $scope.numPerPage1);
                    var end1 = parseInt(begin1) + parseInt($scope.numPerPage1);

                    
                    $scope.filteredTodos1 = $scope.todos1.slice(begin1, end1);
                };
                $scope.modaloperation = false;
                $scope.Modaltable = true;
                

                $http.get(ENV.apiUrl + "api/LedgerCont/getAccountCode?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (getAccountCode_Data) {
                    $scope.AccountCode_Data = getAccountCode_Data.data;
                    });
              

                $http.get(ENV.apiUrl + "api/LedgerCont/getLdgrAccountControl?ledger_code=" + $scope.slac_ldgr_code + "&slac_comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (getLdgrAccountControl_Data) {
                    $scope.gLdgrAccountControl_Data = getLdgrAccountControl_Data.data;
                    $scope.totalItems1 = $scope.gLdgrAccountControl_Data.length;
                    $scope.todos1 = $scope.gLdgrAccountControl_Data;
                    $scope.makeTodos1();
                });

                $scope.New1 = function () {
                    
                    $scope.temp = '';
                    opr = 'S';
                    $scope.savebtn1 = true;
                    $scope.updatebtn1 = false;
                    $scope.modaloperation = true;
                    $scope.Modaltable = false;
                    $scope.Modaltable = false;
                    $scope.temp = '';
                    $("#account_code").select2("val", "");
                    $scope.myFormmodal.$setPristine();
                    $scope.myFormmodal.$setUntouched();
                    
                }

                $scope.up1 = function (str1) {
                    $scope.modaloperation = true;
                    $scope.Modaltable = false;
                    var old_slac_cntrl_class = 'old_slac_cntrl_class';
                    $scope.savebtn1 = false;
                    $scope.updatebtn1 = true;
                    $scope.readonly = true;
                    $scope.temp = {
                        slac_comp_code: str1.slac_comp_code,
                        Company_name: str1.Company_name,
                        slac_ldgr_code: str1.slac_ldgr_code,
                        slac_year: str1.slac_year,
                        slac_cntrl_class: str1.slac_cntrl_class,
                        slac_cntrl_class_desc: str1.slac_cntrl_class_desc,
                        slac_ctrl_acno: str1.slac_ctrl_acno,
                        glma_acct_name: str1.glma_acct_name,
                        slac_sales_clr_acno: str1.slac_sales_clr_acno
                    };
                    $scope.temp[old_slac_cntrl_class] = str1.slac_cntrl_class;

                    

                  


                }

                $scope.cancel1 = function () {
                    $scope.modaloperation = false;
                    $scope.Modaltable = true;
                    $scope.temp = "";

                    $scope.myFormmodal.$setPristine();
                    $scope.myFormmodal.$setUntouched();
                }


                $scope.Save1 = function (myFormmodal) {
                    if (myFormmodal) {
                        data1 = [];

                        var data = {

                            slac_cntrl_class: $scope.temp.slac_cntrl_class,
                            slac_cntrl_class_desc: $scope.temp.slac_cntrl_class_desc,
                            slac_ctrl_acno: $scope.temp.slac_ctrl_acno,
                            slac_sales_clr_acno: $scope.temp.slac_sales_clr_acno,

                            slac_comp_code: $scope.slac_comp_code,
                            slac_year: $scope.slac_year,
                            slac_ldgr_code: $scope.slac_ldgr_code,
                            opr: 'I',
                        };
                        $scope.exist = false;

                        for (var i = 0; i < $scope.gLdgrAccountControl_Data.length; i++) {
                            if ($scope.gLdgrAccountControl_Data[i].slac_ldgr_code == data.slac_ldgr_code &&
                                 $scope.gLdgrAccountControl_Data[i].slac_cntrl_class == data.slac_cntrl_class &&
                                $scope.gLdgrAccountControl_Data[i].slac_comp_code == data.slac_comp_code &&
                                $scope.gLdgrAccountControl_Data[i].slac_year == data.slac_year) {
                                $scope.exist = true;
                            }
                        }
                        if ($scope.exist) {
                            swal({ title: 'Alert', text: "Control Class Already Exits", width: 300, height: 200 })
                        }
                        else {
                            data1.push(data);
                            $http.post(ENV.apiUrl + "api/LedgerCont/LdgrAccountCntrlCUD", data1).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: 'Alert', text: "Record Inserted Successfully", width: 300, height: 200 })
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ title: 'Alert', text: "Record Not Inserted.", width: 300, height: 200 })
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }

                                $http.get(ENV.apiUrl + "api/LedgerCont/getLdgrAccountControl?ledger_code=" + $scope.slac_ldgr_code + "&slac_comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (getLdgrAccountControl_Data) {
                                   $scope.gLdgrAccountControl_Data = getLdgrAccountControl_Data.data;
                                   $scope.totalItems1 = $scope.gLdgrAccountControl_Data.length;
                                   $scope.todos1 = $scope.gLdgrAccountControl_Data;
                                   $scope.makeTodos1();
                               });
                            })
                        }
                        $scope.modaloperation = false;
                        $scope.Modaltable = true;
                    }
                }

                $scope.Update1 = function (myFormmodal) {
                    if (myFormmodal) {
                        data1 = [];
                        var data = $scope.temp;
                        data = {
                            opr: 'U',
                            slac_cntrl_class: $scope.temp.slac_cntrl_class,
                            slac_cntrl_class_desc: $scope.temp.slac_cntrl_class_desc,
                            slac_ctrl_acno: $scope.temp.slac_ctrl_acno,
                            slac_sales_clr_acno: $scope.temp.slac_sales_clr_acno,

                            slac_comp_code: $scope.slac_comp_code,
                            slac_year: $scope.slac_year,
                            slac_ldgr_code: $scope.slac_ldgr_code,
                            old_slac_cntrl_class: $scope.temp.old_slac_cntrl_class,
                        };

                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/LedgerCont/LdgrAccountCntrlCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: 'Alert', text: "Record Updated Successfully", width: 300, height: 200 })
                            }
                            else if ($scope.msg1 == false) {
                                swal({ title: 'Alert', text: "Record Not Updated. " , width: 300, height: 200 })
                            }
                            else ("Error-" + $scope.msg1)
                            $scope.modaloperation = false;
                            $http.get(ENV.apiUrl + "api/LedgerCont/getLdgrAccountControl?ledger_code=" + $scope.slac_ldgr_code + "&slac_comp_code=" + +comp_code + "&finance_year=" + finance_year).then(function (getLdgrAccountControl_Data) {
                                $scope.gLdgrAccountControl_Data = getLdgrAccountControl_Data.data;
                                $scope.totalItems1 = $scope.gLdgrAccountControl_Data.length;
                                $scope.todos1 = $scope.gLdgrAccountControl_Data;
                                $scope.makeTodos1();
                            });
                        })
                        $scope.modaloperation = false;
                        $scope.Modaltable = true;
                    }
                }

                $scope.CheckAllCheckedModal = function () {
                    
                    var main1 = document.getElementById('mainchk1');
                    for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                        var v = document.getElementById($scope.filteredTodos1[i].slac_ldgr_code + $scope.filteredTodos1[i].slac_cntrl_class + i);
                        if (main1.checked == true) {
                            v.checked = true;
                            //$('tr').addClass("row_selected");
                        }

                        else {
                            v.checked = false;
                            //  $('tr').removeClass("row_selected");
                        }
                    }

                }

                //$scope.checkonebyonedeleteModal = function () {

                //    $("input[type='checkbox']").change(function (e) {
                //        if ($(this).is(":checked")) {
                //            $(this).closest('tr').addClass("row_selected");
                //            $scope.color = '#edefef';
                //        } else {
                //            $(this).closest('tr').removeClass("row_selected");
                //            $scope.color = '#edefef';
                //        }
                //    });

                //    var main1 = document.getElementById('mainchk1');

                //    if (main1.checked == true) {
                //        main1.checked = false;
                //        $("input[type='checkbox']").change(function (e) {
                //            if ($(this).is(":checked")) {
                //                $(this).closest('tr').addClass("row_selected");
                //            }
                //            else {
                //                $(this).closest('tr').removeClass("row_selected");
                //            }
                //        });
                //    }
                //}

                $scope.checkonebyonedeleteModal = function () {

                    if (main1.checked == true) {
                        main1.checked = false;
                        $scope.row1 = '';
                    }
                }


                $scope.deleterecord1 = function () {

                    deleteaccountcode = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                        var v = document.getElementById($scope.filteredTodos1[i].slac_ldgr_code + $scope.filteredTodos1[i].slac_cntrl_class + i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deleteLedgerAccountCntrl = ({
                                'slac_ldgr_code': $scope.filteredTodos1[i].slac_ldgr_code,
                                'slac_cntrl_class': $scope.filteredTodos1[i].slac_cntrl_class,
                                'slac_year': $scope.filteredTodos1[i].slac_year,
                                'slac_comp_code': $scope.filteredTodos1[i].slac_comp_code,
                                'opr': 'C'
                            });
                            deleteaccountcode.push(deleteLedgerAccountCntrl);
                        }
                    }

                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                $http.post(ENV.apiUrl + "api/LedgerCont/LdgrAccountCntrlCUD", deleteaccountcode).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/LedgerCont/getLdgrAccountControl?ledger_code=" + $scope.slac_ldgr_code + "&slac_comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (getLdgrAccountControl_Data) {
                                                    $scope.gLdgrAccountControl_Data = getLdgrAccountControl_Data.data;
                                                    $scope.totalItems1 = $scope.gLdgrAccountControl_Data.length;
                                                    $scope.todos1 = $scope.gLdgrAccountControl_Data;
                                                    $scope.makeTodos1();
                                                });
                                                main1 = document.getElementById('mainchk1');
                                                if (main1.checked == true) {
                                                    main1.checked = false;
                                                    {
                                                        $scope.row2 = '';
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ title: "Alert", text: "Record Not Deleted. " , showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/LedgerCont/getLdgrAccountControl?ledger_code=" + $scope.slac_ldgr_code + "&slac_comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (getLdgrAccountControl_Data) {
                                                    $scope.gLdgrAccountControl_Data = getLdgrAccountControl_Data.data;
                                                    $scope.totalItems1 = $scope.gLdgrAccountControl_Data.length;
                                                    $scope.todos1 = $scope.gLdgrAccountControl_Data;
                                                    $scope.makeTodos1();
                                                });
                                                main1 = document.getElementById('mainchk1');
                                                if (main1.checked == true) {
                                                    main1.checked = false;
                                                    {

                                                    }
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });
                            }
                            else {
                                for (var i = 0; i < $scope.filteredTodos1.length; i++) {
                                    var v = document.getElementById($scope.filteredTodos1[i].slac_ldgr_code + $scope.filteredTodos1[i].slac_cntrl_class + i);
                                    if (v.checked == true) {
                                        v.checked = false;

                                        main1.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                    }

                    $scope.currentPage1 = true;
                    main1.checked = false;
                }
            }


            //----------------------------Modal_PopUp_End--------------------------------------------------------------------//
        }])
})();
