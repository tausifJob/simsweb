﻿(function () {
    'use strict';
    var main;
    var prdocdata = [];
    var data1 = [];
    var data = [];
    var doc_no;
    var simsController = angular.module('sims.module.Finance');
    simsController.controller('PrintVouchersCont',
          ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {
              $scope.pagesize = '5';
              $scope.pager = true;
              $scope.pagggg = false;
              var user = $rootScope.globals.currentUser.username;
              var today = new Date();
              var dd = today.getDate();
              var mm = today.getMonth() + 1;
              var yyyy = today.getFullYear();
              $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

              $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
              console.log($scope.finnDetail.company);

              $scope.size = function (str) {
                  1
                  if (str == "All") {
                      $scope.currentPage = '1';
                      $scope.filteredTodos = $scope.FeeReceiptData;
                      $scope.pager = false;
                  }
                  else {
                      $scope.pager = true;
                      $scope.pagesize = str;
                      $scope.currentPage = 1;
                      $scope.numPerPage = str;
                      $scope.makeTodos();
                  }
              }

              $scope.index = function (str) {
                  $scope.pageindex = str;
                  $scope.currentPage = str;
                  console.log("currentPage=" + $scope.currentPage);
                  $scope.makeTodos();
                  main.checked = false;
                  $scope.row1 = '';
              }

              $scope.makeTodos = function () {
                  var rem = parseInt($scope.totalItems % $scope.numPerPage);
                  if (rem == '0') {
                      $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                  }
                  else {
                      $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                  }
                  var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                  var end = parseInt(begin) + parseInt($scope.numPerPage);

                  console.log("begin=" + begin); console.log("end=" + end);

                  $scope.filteredTodos = $scope.todos.slice(begin, end);
              };

              $('*[data-datepicker="true"] input[type="text"]').datepicker({
                  todayBtn: true,
                  orientation: "top left",
                  autoclose: true,
                  todayHighlight: true,
                  format: "yyyy-mm-dd"
              });

              $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                  $('input[type="text"]', $(this).parent()).focus();
              });

              $scope.createdate = function (end_date, start_date, name) {
                  var month1 = end_date.split("/")[0];
                  var day1 = end_date.split("/")[1];
                  var year1 = end_date.split("/")[2];
                  var new_end_date = year1 + "/" + month1 + "/" + day1;
                  //var new_end_date = day1 + "/" + month1 + "/" + year1;

                  var year = start_date.split("/")[0];
                  var month = start_date.split("/")[1];
                  var day = start_date.split("/")[2];
                  var new_start_date = year + "/" + month + "/" + day;
                  // var new_start_date = day + "/" + month + "/" + year;

                  if (new_end_date < new_start_date) {
                      swal({ title: 'Please Select Next Date From ', width: 380, height: 100 });
                      $scope.edt[name] = '';
                  }
                  else {
                      $scope.edt[name] = new_end_date;
                  }
              }

              $scope.paginationview1 = document.getElementById("paginationview");

              $scope.showdate = function (date, name) {
                  debugger
                  var month = date.split("/")[0];
                  var day = date.split("/")[1];
                  var year = date.split("/")[2];
                  $scope.edt[name] = year + "/" + month + "/" + day;
              }

              var today = new Date();
              var dd = today.getDate();
              var mm = today.getMonth() + 1; //January is 0!
              if (mm < 10) {
                  mm = '0' + mm;
              }
              var yyyy = today.getFullYear();
              $scope.sdate = yyyy + '-' + mm + '-' + dd;
              $scope.edate = yyyy + '-' + mm + '-' + dd;
              $scope.edt = {
                  from_date: yyyy + '-' + mm + '-' + dd,
                  to_date: yyyy + '-' + mm + '-' + dd,

              }

              $timeout(function () {
                  $("#fixTable").tableHeadFixer({ 'top': 1 });
              }, 100);

              $timeout(function () {
                  $("#fixTable1").tableHeadFixer({ 'top': 1 });
              }, 100);

              $scope.Show = function (from_date, to_date, search) {

                  if ($scope.edt.search == undefined || $scope.edt.search == "") {
                      $scope.edt.search = '';
                  }
                  $scope.table1 = true;
                  $scope.pagggg = true;
                  $scope.ImageView = false;
                  $http.get(ENV.apiUrl + "api/StudentReport/getPrintVouchers?com_code=" + $scope.finnDetail.company + "&search=" + $scope.edt.search + "&from_date=" + $scope.edt.from_date + "&to_date=" + $scope.edt.to_date).then(function (FeeReceipt_Data) {
                      $scope.FeeReceiptData = FeeReceipt_Data.data;
                      $scope.totalItems = $scope.FeeReceiptData.length;
                      $scope.todos = $scope.FeeReceiptData;
                      $scope.makeTodos();
                      console.log($scope.FeeReceiptData);
                      if (FeeReceipt_Data.data.length > 0) { }
                      else {
                          $scope.ImageView = true;
                          $scope.pager = false;
                      }
                  });
                  //}
              }

              $(function () {
                  $("#chkmarks").click(function () {
                      if ($(this).is(":checked")) {
                          $("#fdate").attr("disabled", "disabled");
                          $scope.edt["from_date"] = '';
                      } else {

                          $("#fdate").removeAttr("disabled");
                          $("#fdate").focus();
                          $scope.edt = {
                              from_date: yyyy + '-' + mm + '-' + dd,
                              to_date: yyyy + '-' + mm + '-' + dd,
                          }

                      }
                  });
              });

              $scope.Report = function (str) {
                  var data = {
                      location: 'FINN.FINR10',
                      parameter: {
                          fee_rec_no: str.doc_no,
                      },
                      state: 'main.Fin226',
                      ready: function () {
                          this.refreshReport();
                      },
                  }
                  window.localStorage["ReportDetails"] = JSON.stringify(data)
                  $state.go('main.ReportCardParameter')
              }

          }]
        )
})();