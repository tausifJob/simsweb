﻿(function () {
    'use strict';
    var obj1, obj2, temp, main;
    var opr = '';
    var categorycode = [];

    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ChartaccountCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.ScheduleGroupDetail = false;
            $scope.editmode = false;
            $scope.edt = "";
            var data1 = [];
            $scope.ScheduleGroupOperation = true;
            $scope.newmode = true;
            $scope.canclebtn = false;

            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();
           


            function getTree() {
                // Some logic to retrieve, or generate tree structure

                var tree = [
  {
      text: "Schedule Group 1",
      nodes: [
        {
            text: "Child 1",
            nodes: [
              {
                  text: "Grandchild 1",
                  code: 1
              },
              {
                  text: "Grandchild 2"
              }
            ]
        },
        {
            text: "Child 2"
        }
      ]
  },
  {
      text: "Schedule Group 2",
      nodes: [
        {
            text: "Child 2",
            nodes: [
              {
                  text: "Grandchild 1"
              },
              {
                  text: "Grandchild 2",
                  nodes: [
              {
                  text: "Grandchild 11"
              }]
              }
            ]
        },
        {
            text: "Child 2"
        }
      ]

  },
  {
      text: "Schedule Group 3",
      nodes: [
       {
           text: "Child 2",
           nodes: [
             {
                 text: "Grandchild 1"
             },
             {
                 text: "Grandchild 2",
                 nodes: [
             {
                 text: "Grandchild 11"
             }]
             }
           ]
       },
       {
           text: "Child 2"
       }
      ]

  },
  {
      text: "Schedule Group 4",
      nodes: [
       {
           text: "Child 2",
           nodes: [
             {
                 text: "Grandchild 1"
             },
             {
                 text: "Grandchild 2",
                 nodes: [
             {
                 text: "Grandchild 11"
             }]
             }
           ]
       },
       {
           text: "Child 2"
       }
      ]

  },
  {
      text: "Schedule Group 5",
      nodes: [
        {
            text: "Child 2",
            nodes: [
              {
                  text: "Grandchild 1"
              },
              {
                  text: "Grandchild 2",
                  nodes: [
              {
                  text: "Grandchild 11"
              }]
              }
            ]
        },
        {
            text: "Child 2"
        }
      ]

  }
                ];
                return tree;
            }

            $('#tree').treeview({ data: getTree() });

            $('#tree').treeview('collapseAll', { silent: true });

            $('#tree').on('nodeSelected', function (event, data) {
                // Your logic goes here
                console.log(data);
            });



        }]);

})();