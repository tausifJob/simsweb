﻿
(function () {
    'use strict';
    var chksearch;
    var main1;
    var prdocdata = [];
    var data1 = [];
    var data = [];
    var doc_no;
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('InvoiceReceiveableCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {
            $scope.pagesize = '5';

            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.etemp = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                $scope.getAccYear();
            });

            $scope.getAccYear = function () {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + $scope.etemp.sims_cur_code).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                    
                });
            }


            $scope.search_textbox = false;
            $scope.disabled_fromdate = false;

            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.countData = [
              { val: 5, data: 5 },
              { val: 10, data: 10 },
              { val: 15, data: 15 },

            ]
            debugger
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');

            $scope.edt =
                          {
                              from_date: $scope.ddMMyyyy,
                              to_date: $scope.ddMMyyyy,
                          }

            $scope.size = function (str) {
                if (str == 5 || str == 10 || str == 15) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }

                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;

                $scope.makeTodos();

            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);



                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.createdate = function (date) {
                if ($scope.edt.from_date != '') {
                    if ($scope.edt.from_date > $scope.edt.to_date) {
                        swal({ title: 'Please Select future Date', width: 380, height: 100 });
                        $scope.edt.to_date = '';
                    }
                }
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=SRI").then(function (res) {
                $scope.final_doc_url = res.data;
                console.log($scope.final_doc_url)
            });

            $scope.Show = function (from_date, to_date, search, comp_cd) {

                if ($scope.edt.search == undefined || $scope.edt.search == "") {
                    $scope.edt.search = '';
                }
                $scope.table1 = true;
                $scope.filteredTodos = [];
                $scope.pagesize = '5';
                $scope.currentPage = 1;
                $scope.numPerPage = 5;
                $http.get(ENV.apiUrl + "api/PrintVoucher/getAllInvoiceReceivablePrint?from_date=" + $scope.edt.from_date + "&to_date=" + $scope.edt.to_date + "&search=" + $scope.edt.search + "&comp_code=" + comp_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (AllInvoiceReceivable) {
                    $scope.table1 = true;
                    $scope.InvoiceReceivable_Data = AllInvoiceReceivable.data;
                    if ($scope.InvoiceReceivable_Data.length > 0) {
                        $scope.pager = true;

                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.InvoiceReceivable_Data.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.InvoiceReceivable_Data.length, data: 'All' })
                        }

                        $scope.totalItems = $scope.InvoiceReceivable_Data.length;
                        $scope.todos = $scope.InvoiceReceivable_Data;
                        $scope.makeTodos();
                        $scope.table1 = true;
                        $scope.page_index = true;
                        $scope.currentPage = 1;
                    }
                    else {
                        swal({ title: "Alert", text: "Invoice Not Found", width: 300, height: 200 });
                        $scope.page_index = false;
                        $scope.pager = false;
                        $scope.filteredTodos = [];
                    }
                });
            }

            $scope.Report = function (str) {
                debugger;
                var data = {
                    location: $scope.final_doc_url,
                    parameter: {
                        doc_code: str.gltr_doc_code,
                        doc_no: str.gltr_our_doc_no
                    },
                    state: 'main.INVREC',
                    ready: function () {
                        this.refreshReport();
                    },
                }

                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')
                // });
            }





            $scope.check_searchnull = function () {
                chksearch = document.getElementById("chkmarks1");

                if (chksearch.checked == true) {
                    $scope.search_textbox = true;
                }
                else {
                    $scope.search_textbox = false;
                }
            }

            $scope.check_fromdate = function () {
                main1 = document.getElementById("chkmarks");

                if (main1.checked == true) {
                    $scope.edt.from_date = '';
                    $scope.disabled_fromdate = true;
                }
                else {
                    $scope.disabled_fromdate = false;
                    $scope.edt =
                          {
                              from_date: $scope.ddMMyyyy,
                              to_date: $scope.ddMMyyyy,
                          }
                }
            }



            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {

                $('input[type="text"]', $(this).parent()).focus();
            });
        }])
})();
