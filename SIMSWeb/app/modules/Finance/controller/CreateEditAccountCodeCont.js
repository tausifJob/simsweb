﻿
(function () {
    'use strict';

    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CreateEditAccountCodeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.pagesize = "All";
            $scope.pageindex = "1";
            $scope.grid = true;
            $scope.save = true;
            $scope.pager = true;
            $scope.edt = {};
            $scope.edt = {
                glac_status: true,
                glac_post_allow:true,
            }
            $scope.showDept = false;
           
            var main = '';
            var user = $rootScope.globals.currentUser.username.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            setTimeout(function () {
                $("#account_codes3").select2();
                $("#account_codes4").select2();
            }, 100);

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.upperCase = function (str) {
                if ($scope.edt.glac_name != undefined) {
                    $scope.edt['glac_name'] = str.toUpperCase();
                }
            }

            $http.get(ENV.apiUrl + "api/GLAccountsCreateView/GetAll_glmaster_Department_Code?co_code=" + comp_code + "&year=" + finance_year).then(function (GetAll_glmaster_Department_Code) {
                $scope.GetAll_glmaster_Department_Code = GetAll_glmaster_Department_Code.data;
            });

            $http.get(ENV.apiUrl + "api/CreateEditAccount/getAllFinsAccountCodes?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                $scope.obj = res.data;
                $scope.totalItems = $scope.obj.length;
                $scope.todos = $scope.obj;
                $scope.makeTodos();
            });

            $http.get(ENV.apiUrl + "api/CreateEditAccount/getScheduleCode?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (ScheduleCode) {
                $scope.Schedule_Code = ScheduleCode.data;
                setTimeout(function () {
                    $('#cmb_glma_dept_no').multipleSelect({
                        selectAll: false
                    })
                }, 100);
            });

            $http.get(ENV.apiUrl + "api/CreateEditAccount/getAllAccountTypes?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (AllAccountTypes) {
                $scope.All_Account_Types = AllAccountTypes.data;
            });

            $http.get(ENV.apiUrl + "api/CreateEditAccount/getAllParentAccounts?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (AllParentAccounts) {
                $scope.All_Parent_Accounts = AllParentAccounts.data;


            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }

                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
  
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.obj;
                    $scope.pager = false;
                }
                else {
                    $scope.pagesize = str;
                    $scope.pager = true;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;  $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();
            }

            $scope.Reset = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = "";
                $scope.dis_acct_code = false;
                $scope.save = true;
                $('#cmb_glma_dept_no').multipleSelect('enable');
              
                setTimeout(function () {
                    $("#account_codes3").select2("val", '');
                    $("#account_codes4").select2("val", '');

                }, 500)
              
                $scope.filteredTodos = $scope.obj;
                $scope.totalItems = $scope.obj.length;
                $scope.todos = $scope.obj;
                $scope.makeTodos();
                $http.get(ENV.apiUrl + "api/CreateEditAccount/getAllParentAccounts?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (AllParentAccounts) {
                    $scope.All_Parent_Accounts = AllParentAccounts.data;

                    $('#cmb_glma_dept_no').multipleSelect('uncheckAll');
                });
                
            }

            $scope.check = function () {
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].glac_acct_code;
                    var v = document.getElementById(t);
                    if (main.checked == true) {
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }

                    else {
                        v.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.check1 = function (str) {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.dis_acct_code = false;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            var data = [];
            var dept, dept_code;

            $scope.Save = function (isvalid) {
                debugger
               
                if ($scope.edt.glma_dept_no == "" || $scope.edt.glma_dept_no == undefined || $scope.edt.glma_dept_no.length == 0)
                {
                    swal({ text: 'Please Select Department ', width: 380 });
                    return;
                }

                if (true) {
                    
                    if ($scope.edt.glac_acct_code != "" && $scope.edt.glac_acct_code != undefined) {

                        dept_code = [];

                        if ($scope.edt.glma_dept_no.length == 0 || $scope.edt == undefined) {
                            dept_code = '';
                        }
                        else {
                            dept = $scope.edt.glma_dept_no;
                            dept_code = dept_code + ',' + dept;
                        }


                        $scope.save = true;
                        $scope.edt.opr = "I";
                        $scope.edt.comp_code = comp_code;
                        $scope.edt.glac_year = finance_year;
                        $scope.edt.user_name = user;
                        $scope.edt.glma_dept_no = dept_code;

                        data.push($scope.edt);
                        $http.post(ENV.apiUrl + "api/CreateEditAccount/CUDAccountCodeDetails", data).then(function (msg) {
                            $scope.display = true;
                            data = [];
                            $scope.msg1 = msg.data;
                            if ($scope.msg1) {
                                swal('', $scope.msg1);

                                $scope.Reset();

                                $http.get(ENV.apiUrl + "api/CreateEditAccount/getAllFinsAccountCodes?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                                    $scope.obj = res.data;
                                    $scope.totalItems = $scope.obj.length;
                                    $scope.todos = $scope.obj;
                                    $scope.makeTodos();
                                });

                            }
                            /*
                            else {
                                swal('', 'Account Not Created. ' );
                            }*/
                        });
                    }
                }
                else {

                    swal('', 'Please Insert Code');
                }
            }

            var deletedata = [];

            $scope.Delete = function () {
                var flag = false;
                $scope.save = true;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var t = $scope.filteredTodos[i].glac_acct_code;
                    var v = document.getElementById(t);
                    if (v.checked == true) {
                        flag = true;
                        $scope.filteredTodos[i].opr = "D";
                        $scope.filteredTodos[i].comp_code = comp_code;
                        $scope.filteredTodos[i].glac_year = finance_year;
                        //$scope.filteredTodos[i].user_name = user;

                        deletedata.push($scope.filteredTodos[i]);
                    }
                }

                if (flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/CreateEditAccount/CUDAccountCodeDetails", deletedata).then(function (msg) {
                                $scope.display = true;
                                deletedata = [];
                                $scope.msg1 = msg.data;
                                if ($scope.msg1) {
                                    swal('', $scope.msg1);
                                    $scope.Reset();
                                    $http.get(ENV.apiUrl + "api/CreateEditAccount/getAllFinsAccountCodes?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                                        $scope.obj = res.data;
                                        $scope.totalItems = $scope.obj.length;
                                        $scope.todos = $scope.obj;
                                        $scope.makeTodos();
                                    });
                                }
                               /* else if ($scope.msg1 == false) {
                                    swal({ text: 'Account Not Deleted. ', width: 380 });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }*/
                            });
                        }
                    });

                }

                else {

                    swal({ text: 'Select At Least One Record To Delete', width: 380 });
                }

            }

            $scope.edit = function (str) {
                debugger
                $('#cmb_glma_dept_no').multipleSelect('disable');
                $scope.save = false;
                $scope.dis_acct_code = true;
                $scope.edt = {
                    glac_acct_code: str.glac_acct_code,
                    glac_name: str.glac_name,
                    glac_status: str.glac_status,
                    glac_post_allow:str.glac_post_allow,
                    glac_type_code: str.glac_type1,
                    glac_parent_acct_code: str.glac_parent_acct1,
                    glac_sched_code: str.glac_sched_code1,
                    glac_display_acct_code: str.glac_display_acct_code,
                    glma_dept_no: str.glma_dept_no,
                }


                setTimeout(function () {
                    $("#account_codes3").select2("val", str.glac_sched_code1);
                    $("#account_codes4").select2("val", str.glac_parent_acct1);

                }, 500)

                }

            $scope.Update = function () {
                $scope.edt.opr = "U";
                $scope.edt.comp_code = comp_code;
                $scope.edt.glac_year = finance_year;

                data.push($scope.edt);

                $http.post(ENV.apiUrl + "api/CreateEditAccount/CUDAccountCodeDetails", data).then(function (msg) {
                    $scope.display = true;
                    data = [];
                    $scope.msg1 = msg.data;
                    if ($scope.msg1) {
                        swal('', $scope.msg1);
                        
                        $http.get(ENV.apiUrl + "api/CreateEditAccount/getAllFinsAccountCodes?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                            $scope.obj = res.data;
                            $scope.totalItems = $scope.obj.length;
                            $scope.todos = $scope.obj;
                            $scope.makeTodos();
                        });

                        $scope.Reset();
                    }
                   /* else if ($scope.msg1 == false) {
                        swal('', 'Account Data Not Updated');
                    }
                    else {
                        swal("Error-"+$scope.msg1)
                    }*/
                });
                $scope.save = true;
            }

            $scope.searchedName = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtilName(i, toSearch);
                });
            };

            $scope.searchName = function () {
                $scope.todos = $scope.searchedName($scope.obj, $scope.edt.glac_name);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.edt.glac_name == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtilName(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.glac_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }


            $scope.searchedCode = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtilCode(i, toSearch);
                });
            };

            $scope.searchCode = function () {
                $scope.todos = $scope.searchedCode($scope.obj, $scope.edt.glac_acct_code);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.edt.glac_acct_code == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtilCode(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.glac_acct_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.glac_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.glac_acct_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.glac_sched_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }


            $scope.searchedScheduleCode = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtilScheduleCode(i, toSearch);
                });
            };

            $scope.searchScheduleCode = function () {
                $scope.todos = $scope.searchedScheduleCode($scope.obj, $scope.edt.glac_sched_code);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.edt.glac_sched_code == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtilScheduleCode(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.glac_sched_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }
            $scope.displayAccount = function () {
               
                $http.get(ENV.apiUrl + "api/CreateEditAccount/getAllFinsAccountCodes?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                    $scope.obj = res.data;
                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.makeTodos();
                });
            
            }

            $scope.AddDisplayAC = function () {
               
                $scope.edt.glac_display_acct_code = $scope.edt.glac_acct_code;

            }

        }])
})();