﻿(function () {
    'use strict';
    var main, temp;
    var data1 = [];
    var data2 = [];
    var data3 = [];
    var status = "";
    var msg_flag1 = false,jvaut=false,jver=false;
    var prdocdata = [], doc_detail_data=[];
    var acconcode = "";
    var prvno = "";
    var cmbvalue = "";
    var comp_code = "1", chk_rcur;
    var data = [];
    var chk;
    var cost;
    var coad_pty_short_name = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('Petty_Cash_DPSCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.edt = "";
            $scope.amount_difference = 0;
            $scope.operation = true;
            $scope.chkcost = true;
            $scope.cost_combo = true;
            // $scope.vis = {};
            $scope.doc_cd_prov_no = '';
            $scope.doc_cd_final_doc_no = '';
            $scope.block1 = [];
            $scope.block2 = [];
            $scope.block3 = [];
            $scope.temp_doc = [];
            $scope.vis = [];
            $scope.vis['main'] = true;
            $scope.vis.onchangedoccodeshow = false;
            $scope.dramount = false;
            $scope.cramount = false;
            $scope.vis.doc_detail_grid = true;
            $scope.vis.main_acct_grid = true;
            $scope.save1 = true;
            $scope.vis.add_grid = true;
            $scope.checkprintbtn = false;
            $scope.vis.Add_show_hide = true;
            $scope.prdocdata = [];
            $scope.doc_detail_data = [];
            $scope.temp = {};
            $scope.temp['temp_chk_doc_details'] = "";

            $scope.vis.update = false;
            //new sgr
            $scope.chkcost = false;
            $scope.cost_combo = false;
            var costcentername;

            $scope.readonly = true;
            //$scope.vis.SendToVerify = false;
            $scope.vis.ok = true;
            $scope.vis.is_recur = true;
            $scope.preparebtn = true;
            $scope.verifybtn = true;
            $scope.authorizebtn = true;

            $scope.vis.cmb_acct = true;
            $scope.vis.txt_acct = false;
            $scope.dis_recur_date = true;


            $scope.edt2 = [];
            $scope.edt = [];
            
            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
            //var comp_code = '1';
            //var finance_year = '2016';
            setTimeout(function () {
                $("#cmb_acc_Code3").select2();

            }, 100);

            $scope.maxmize = function () {
                $('#jv_Voucher').css({ 'height': '100%', 'width': '100%' })
                $scope.windowflg = true;
            }

            $scope.manimize = function () {
                $('#jv_Voucher').css({ 'height': '600px', 'width': '900px' })
                $scope.windowflg = false;

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
                $("#Table1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $scope.Verify = function () {
                //   $('#myModal_Verify').modal('show');
                $scope.edt = "";
                $scope.edt1 = "";
                $scope.edt2 = "";
                $scope.vis['main'] = false;
                $scope.jvtitle = '- Verify';

                $scope.vis.prepare = false;
                $scope.vis.verify = true;
                $scope.vis.Autorize = false;
                $scope.vis.user_vry = false;
                $scope.vis.user_aut = false;

                $http.get(ENV.apiUrl + "api/PettyCash/getFinn140_SearchFins_temp_docs_DPS?compcode=" + comp_code + "&username=" + user).then(function (prdoc_data) {

                    $scope.verify_data = prdoc_data.data;

                });



            }

            $scope.Authorize = function (str) {
                $scope.jvtitle = '- Authorize';
                $scope.edt = "";
                $scope.edt1 = "";
                $scope.edt2 = "";
                $scope.vis['main'] = false;

                $scope.vis.prepare = false;
                $scope.vis.verify = false;
                $scope.vis.Autorize = true;

                $scope.vis.user_vry = true;
                $scope.vis.user_aut = false;


                
                $http.get(ENV.apiUrl + "api/PettyCash/getFinn142_SearchFins_temp_docs_DPS?compcode=" + comp_code + "&username=" + user).then(function (prdoc_data) {

                    $scope.Authorize_data = prdoc_data.data;

                });

                //   $('#myModal_Authorize').modal('show');
            }

            //Back button
            $scope.back = function () {

                $scope.vis.main = true;
                $scope.jvtitle = ' ';
                $scope.checkprintbtn = false;
                $scope.totalDebit = 0;
                $scope.totalcredit = 0;
                $scope.prvno = '';

                $scope.dcode = false;
                $scope.block1 = [];
                $scope.block2 = [];
                $scope.block3 = [];
                $scope.temp_doc = [];

                $scope.edt2 = [];
                $scope.edt = [];
                $scope.prdocdata = [];

                $scope.vis.cmb_acct = true;
                $scope.vis.txt_acct = false;
                $scope.ldgcd = false;

                $scope.vis.is_recur = true;
                $scope.vis.ok = true;
                $scope.vis.update = false;
                $scope.vis.SendToVerify = false;
                $scope.vis.onchangedoccodeshow = false;

                //$scope.Chk_recur_voucher($scope.vis.is_recur);

                $scope.doc_data = false;
                $scope.Doc_table = false;
                $scope.doc_type_combo = false;
                $scope.doc_type_input = false;


                $scope.dis_recur_date = true;
                $scope.lbl_recur = '';
                $scope.vis.recur_periods = false;
                $scope.temp.temp_chk_doc_details = false;


                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                if (mm < 10) {
                    mm = '0' + mm;
                }
                var yyyy = today.getFullYear();
                $scope.sdate = dd + '-' + mm + '-' + yyyy;
                $scope.edate = dd + '-' + mm + '-' + yyyy;
                $scope.temp_doc = {
                    gltd_doc_date: dd + '-' + mm + '-' + yyyy,
                    gltd_post_date: dd + '-' + mm + '-' + yyyy
                }

            }

            //reset button
            $scope.clear = function () {
                $scope.dcode = false;
                $scope.block1 = [];
                $scope.block2 = [];
                $scope.block3 = [];
                $scope.temp_doc = [];
                $scope.checkprintbtn = false;
                $scope.prvno = '';

                $scope.totalDebit = 0;
                $scope.totalcredit = 0;

                $scope.edt2 = [];
                $scope.edt = [];
                $scope.prdocdata = [];

                $scope.vis.cmb_acct = true;
                $scope.vis.txt_acct = false;
                $scope.ldgcd = false;

                $scope.vis.is_recur = true;
                $scope.vis.ok = true;
                $scope.vis.update = false;
                $scope.vis.SendToVerify = false;
                $scope.vis.onchangedoccodeshow = false;

                $scope.dis_recur_date = true;
                $scope.lbl_recur = '';
                $scope.vis.recur_periods = false;
                jvaut = false;
                jver = false;
                //$scope.Chk_recur_voucher($scope.vis.is_recur);
                
                $scope.doc_data = false;
                $scope.Doc_table = false;
                $scope.doc_type_combo = false;
                $scope.doc_type_input = false;
                $scope.temp.temp_chk_doc_details = false;


                var today = new Date();
                var dd = today.getDate();
                var mm = today.getMonth() + 1; //January is 0!
                if (mm < 10) {
                    mm = '0' + mm;
                }
                var yyyy = today.getFullYear();
                $scope.sdate = dd + '-' + mm + '-' + yyyy;
                $scope.edate = dd + '-' + mm + '-' + yyyy;
                $scope.temp_doc = {
                    gltd_doc_date: dd + '-' + mm + '-' + yyyy,
                    gltd_post_date: dd + '-' + mm + '-' + yyyy
                }

                $http.get(ENV.apiUrl + "api/BankPayment/GetVerifyAuthorizeUsers?username=" + user).then(function (users_list) {
                    
                    $scope.users = users_list.data;
                    
                    if ($scope.users.length != 0) {
                        
                        if ($scope.users[0] == user && $scope.users[1] == user) {

                            $scope.vis.autoriz = true;
                            $scope.vis.very = true;
                        }
                        else if ($scope.users[0] == user && ($scope.users[1] == "" || $scope.users[1] != user)) {

                            $scope.vis.autoriz = false;
                            $scope.vis.very = true;
                        }
                        else {

                            $scope.vis.autoriz = false;
                            $scope.vis.very = false;
                        }
                    }
                });
            }

            //Single Check click

            $scope.chk_click_ver = function (obj) {
                if (obj.gltd_revert) {
                    obj.gltd_revert = false;
                    obj.gltd_verify = true;
                }

            }

            $scope.chk_click_rev = function (obj) {
                if (obj.gltd_verify) {
                    obj.gltd_revert = true;
                    obj.gltd_verify = false;
                }

            }

            $scope.chk_click_aut = function (obj) {

                if (obj.gltd_verify) {
                    obj.gltd_verify = false;
                    obj.gltd_authorize = true;
                }


            }

            $scope.chk_click_very = function (obj) {
                if (obj.gltd_authorize) {
                    obj.gltd_verify = true;
                    obj.gltd_authorize = false;
                }

            }

            //check all Authorize button

            $scope.chk_clickAll = function (str) {
                
                if (str) {
                    for (var i = 0; i < $scope.Authorize_data.length; i++) {
                        $scope.Authorize_data[i].gltd_authorize = true;
                    }
                }
                else {
                    for (var i = 0; i < $scope.Authorize_data.length; i++) {
                        $scope.Authorize_data[i].gltd_authorize = false;
                    }
                }
            }

            //check all verify button

            $scope.chk_click_verifyAll = function (str) {

                if (str) {
                    for (var i = 0; i < $scope.verify_data.length; i++) {
                        $scope.verify_data[i].gltd_verify = true;
                        $scope.verify_data[i].gltd_revert = false;
                    }
                    $scope.v_data.gltd_revert1 = false;
                }
                else {
                    for (var i = 0; i < $scope.verify_data.length; i++) {
                        $scope.verify_data[i].gltd_verify = false;
                    }
                }
            }

            //check all revert button

            $scope.chk_click_revertAll = function (str) {

                if (str) {
                    for (var i = 0; i < $scope.verify_data.length; i++) {
                        $scope.verify_data[i].gltd_revert = true;
                        $scope.verify_data[i].gltd_verify = false;
                    }
                    $scope.v_data.gltd_verify1 = false;
                }
                else {
                    for (var i = 0; i < $scope.verify_data.length; i++) {
                        $scope.verify_data[i].gltd_revert = false;
                    }
                }
            }

            //check all authorize button

            $scope.chk_clickAll = function (str) {

                if (str) {
                    for (var i = 0; i < $scope.Authorize_data.length; i++) {
                        $scope.Authorize_data[i].gltd_authorize = true;
                        $scope.Authorize_data[i].gltd_verify = false;
                    }
                    $scope.A_data.gltd_very1 = false;
                }
                else {
                    for (var i = 0; i < $scope.Authorize_data.length; i++) {
                        $scope.Authorize_data[i].gltd_authorize = false;
                    }
                }
            }

            //check all revert verify button

            $scope.very_chk_clickAll = function (str) {

                if (str) {
                    for (var i = 0; i < $scope.Authorize_data.length; i++) {
                        $scope.Authorize_data[i].gltd_verify = true;
                        $scope.Authorize_data[i].gltd_authorize = false;

                    }
                    $scope.A_data.gltd_authorize1 = false;
                }
                else {
                    for (var i = 0; i < $scope.Authorize_data.length; i++) {
                        $scope.Authorize_data[i].gltd_verify = false;
                    }
                }
            }



            $scope.ShowFilterText = function (str) {
                
                if (str == "") {
                    $scope.filterVisible = false;
                }
                else {

                    $scope.filterVisible = true;
                }
                $scope.block1.chk_previous_clear = true;
                $scope.edt.txt_filter_range = '0';
            }

            $scope.ShowFilterText_Aut = function (str) {
                if (str == "") {
                    $scope.filterVisible_aut = false;
                }
                else {

                    $scope.filterVisible_aut = true;
                }
                $scope.block1.chk_previous_clear_aut = true;
                $scope.edt.txt_filter_range_aut = '0';

            }

            $http.get(ENV.apiUrl + "api/PettyCash/GetVerifyAuthorizeUsers_PC?username=" + user + "&comp_code=" + comp_code + "&fyear=" + finance_year + "&doc_code=1").then(function (users_list) {

                $scope.users = users_list.data;
               
                if ($scope.users.length != 0) {

                    if ($scope.users[0] == user && $scope.users[1] == user) {
                        //$scope.preparebtn = true;
                        $scope.verifybtn = false;
                        $scope.authorizebtn = false;
                    }
                    else if ($scope.users[0] == user && ($scope.users[1] == "" || $scope.users[1] != user)) {
                        //$scope.preparebtn = true;
                        $scope.verifybtn = false;
                        $scope.authorizebtn = true;
                    }
                    else {
                        //$scope.preparebtn = false;
                        $scope.verifybtn = true;
                        $scope.authorizebtn = true;
                    }
                }
            });


        
            
            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=Final").then(function (res) {
                $scope.final_doc_url = res.data;
                
            });

            
            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=Prov").then(function (res) {
                $scope.prov_doc_url = res.data;
                
            });

            //send to verify

            $scope.Verify_data = function () {
                var dataSend = [];
                var vcnt = 0, rcnt = 0;

                

                for (var i = 0; i < $scope.verify_data.length; i++) {

                    if ($scope.verify_data[i].gltd_verify == true || $scope.verify_data[i].gltd_revert == true) {

                        if ($scope.verify_data[i].gltd_verify == true) {
                            vcnt = vcnt + 1;
                        }
                        else if ($scope.verify_data[i].gltd_revert == true) {
                            rcnt = rcnt + 1;
                        }

                        var data = {
                            gltd_comp_code: $scope.verify_data[i].gltd_comp_code,
                            gltd_doc_code: $scope.verify_data[i].gltd_doc_code,
                            gltd_prov_doc_no: $scope.verify_data[i].gltd_prov_doc_no,
                            gltd_verify: $scope.verify_data[i].gltd_verify,
                            gltd_revert: $scope.verify_data[i].gltd_revert,
                            gltd_verify_user: user,
                            gltd_verify_date: dd + '-' + mm + '-' + yyyy

                        }
                        dataSend.push(data);
                    }

                }


                $http.post(ENV.apiUrl + "api/JVCreation/UpdateFinn140_temp_docs", dataSend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true && vcnt > 0) {
                        swal({ title: "Alert", text: "Verified Successfully", width: 300, height: 200 });
                        // $scope.checkprintbtn = true;
                        $scope.Verify();

                    }
                    else if ($scope.msg1 == true && rcnt > 0) {
                        swal({ title: "Alert", text: "Reverted Successfully", width: 300, height: 200 });
                        // $scope.checkprintbtn = true;
                        $scope.Verify();

                    }
                    else {
                        swal({ title: "Alert", text: "Documents Not Verified", width: 300, height: 200 });
                    }

                });
            }

            $scope.Authorize_click = function () {
                var dataSend = [];
                var acnt = 0, arcnt = 0;

                $scope.doc_cd_prov_no = '';
                
                for (var i = 0; i < $scope.Authorize_data.length; i++) {

                    if ($scope.Authorize_data[i].gltd_authorize == true || $scope.Authorize_data[i].gltd_verify == true) {


                        if ($scope.Authorize_data[i].gltd_authorize == true) {
                            acnt = acnt + 1;
                        }
                        else if ($scope.Authorize_data[i].gltd_verify == true) {
                            arcnt = arcnt + 1;
                        }

                        var data = {
                            gltd_comp_code: $scope.Authorize_data[i].gltd_comp_code,
                            gltd_doc_code: $scope.Authorize_data[i].gltd_doc_code,
                            gltd_prov_doc_no: $scope.Authorize_data[i].gltd_prov_doc_no,
                            gltd_verify: $scope.Authorize_data[i].gltd_verify,
                            gltd_authorize: $scope.Authorize_data[i].gltd_authorize,
                            gltd_authorize_user: user,
                            gltd_authorize_date: dd + '-' + mm + '-' + yyyy

                        }
                        dataSend.push(data);
                        if ($scope.Authorize_data[i].gltd_authorize == true) {
                            $scope.doc_cd_prov_no = $scope.doc_cd_prov_no + $scope.Authorize_data[i].gltd_doc_code + $scope.Authorize_data[i].gltd_prov_doc_no + ',';
                        }
                    }

                }


                $http.post(ENV.apiUrl + "api/JVCreation/UpdateFinn142_temp_docs", dataSend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true && acnt > 0) {
                        swal({ title: "Alert", text: "Authorized Successfully", width: 300, height: 200 });


                        $http.get(ENV.apiUrl + "api/JVCreation/Getdoc_cd_prov_no?doc_prov_no=" + $scope.doc_cd_prov_no + "&comp_code=" + comp_code).then(function (final_doc_no) {
                            $scope.doc_cd_final_doc_no = final_doc_no.data;
                            
                            //var data = {
                            //    location: $scope.final_doc_url,
                            //    parameter: { doc: $scope.doc_cd_final_doc_no },
                            //    state: 'main.Fin230'
                            //}

                            var data = {
                                location: $scope.final_doc_url,
                                parameter: { comp_detail: comp_code, doc: $scope.doc_cd_final_doc_no },
                                state: 'main.Fin230'
                            }

                            window.localStorage["ReportDetails"] = JSON.stringify(data)
                            $state.go('main.ReportCardParameter')
                            // sims.Controls.Childwindow.AdmRptViewer jvrpt = new Controls.Childwindow.AdmRptViewer($scope.doc_cd_final_doc_no,true);
                            //jvrpt.Show();
                            //Report Name:-FinR19
                        });

                        $scope.Authorize();

                    }
                    else if ($scope.msg1 == true && arcnt > 0) {
                        swal({ title: "Alert", text: "Reverted Successfully", width: 300, height: 200 });

                        $scope.Authorize();

                    }
                    else {
                        swal({ title: "Alert", text: "Documents Not Authorized", width: 300, height: 200 });
                    }

                });
            }

            $scope.JVCreates = [];

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.temp_doc = {
                gltd_doc_date: dd + '-' + mm + '-' + yyyy,
                gltd_post_date: dd + '-' + mm + '-' + yyyy,
                ChequeDate: dd + '-' + mm + '-' + yyyy,
            }

            $scope.myFunct = function (keyEvent) {

                if (keyEvent.which == 13)
                    $scope.temp_doc.gltd_post_date = dd + '-' + mm + '-' + yyyy;
            }

            $scope.myFunct1 = function (keyEvent) {

                if (keyEvent.which == 13)
                    $scope.temp.ChequeDate = dd + '-' + mm + '-' + yyyy;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd-mm-yyyy'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.FilterCode = function () {

                if ($scope.block1.chk_previous_clear == true) {

                    for (var i = 0; i < $scope.verify_data.length; i++) {
                        $scope.verify_data[i].gltd_revert = false;
                        $scope.verify_data[i].gltd_verify = false;
                    }
                }
                if ($scope.edt.SelectOption == 'Select Verify Records') {

                    //if ($scope.verify_data.length >= $scope.edt.txt_filter_range) {
                    for (var i = 0; i < $scope.edt.txt_filter_range; i++) {
                        $scope.verify_data[i].gltd_verify = true;
                        $scope.verify_data[i].gltd_revert = false;

                    }
                    //}
                }
                else if ($scope.edt.SelectOption == 'Select Revert Records') {

                    // if ($scope.verify_data.length >= $scope.edt.txt_filter_range) {
                    for (var i = 0; i < $scope.edt.txt_filter_range; i++) {
                        $scope.verify_data[i].gltd_revert = true;
                        $scope.verify_data[i].gltd_verify = false;

                    }
                    //}
                }
            }

            $scope.FilterCode_aut = function () {
                

                if ($scope.block1.chk_previous_clear_aut == true) {

                    for (var i = 0; i < $scope.Authorize_data.length; i++) {
                        $scope.Authorize_data[i].gltd_authorize = false;
                        $scope.Authorize_data[i].gltd_verify = false;
                    }
                }
                if ($scope.edt.SelectOption_aut == 'Select Revert Records') {

                    //if ($scope.Authorize_data.length >= $scope.edt.txt_filter_range_aut) {
                    for (var i = 0; i < $scope.edt.txt_filter_range_aut; i++) {
                        $scope.Authorize_data[i].gltd_verify = true;
                        $scope.Authorize_data[i].gltd_authorize = false;
                    }
                    //}
                }
                else if ($scope.edt.SelectOption_aut == 'Select Authorize Records') {

                    //if ($scope.Authorize_data.length >= $scope.edt.txt_filter_range_aut) {
                    for (var i = 0; i < $scope.edt.txt_filter_range_aut; i++) {
                        $scope.Authorize_data[i].gltd_authorize = true;
                        $scope.Authorize_data[i].gltd_verify = false;

                    }
                    //}
                }
            }
            $scope.docCode1;
            $scope.provNo1;
            $scope.CheckPrintReport = function () {

                var data = {
                    location: $scope.prov_doc_url,
                    parameter: {
                        comp_detail: comp_code,
                        doc_code: $scope.docCode1,
                        doc_no: $scope.provNo1,

                    },

                    state: 'main.Fin230'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')


            }


            $scope.PrintReport = function () {
                
                var data = {
                    location: $scope.prov_doc_url,
                    parameter: {
                        comp_detail: comp_code,
                        doc_code: $scope.temp_doc.gltd_doc_code,
                        doc_no: $scope.prvno,

                    },

                    state: 'main.Fin230'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')


            }

            $scope.Fetchdata = function (cmp_cd, docd, prov_n) {
                $scope.docCode1 = docd;
                $scope.provNo1 = prov_n;
                
                $('#myModal4').modal('show')


                $http.get(ENV.apiUrl + "api/JVCreation/getAllRecords_DPS?doccode=" + docd + "&prnumber=" + prov_n + "&comp_code=" + cmp_cd).then(function (prdoc_data) {

                    $scope.prdocdata = prdoc_data.data;
                    $scope.block1 = [];

                    $scope.totalDebit = 0;
                    $scope.totalcredit = 0;

                    for (var i = 0; i < $scope.prdocdata.length; i++) {

                        if (i == 0) {
                            $scope.temp_doc.gltd_comp_code = $scope.prdocdata[i].gltd_comp_code;
                            $scope.temp_doc.gltd_doc_code = $scope.prdocdata[i].gltd_doc_code;
                            $scope.temp_doc.gldc_doc_short_name = $scope.prdocdata[i].gldc_doc_short_name;
                            $scope.temp_doc.gltd_doc_narr = $scope.prdocdata[i].gltd_doc_narr;
                            $scope.temp_doc.gltd_remarks = $scope.prdocdata[i].gltd_remarks;
                            $scope.temp_doc.gltd_doc_date = $scope.prdocdata[i].gltd_doc_date;
                            $scope.temp_doc.gltd_post_date = $scope.prdocdata[i].gltd_post_date;
                            $scope.temp_doc.gltd_prov_doc_no = prov_n;

                            $scope.temp_doc.gldu_authorize = $scope.prdocdata[i].gldu_authorize;
                            $scope.temp_doc.gldu_verify = $scope.prdocdata[i].gldu_verify;
                            $scope.temp_doc.gltd_authorize_date = $scope.prdocdata[i].gltd_authorize_date;
                            $scope.temp_doc.gltd_verify_date = $scope.prdocdata[i].gltd_verify_date;

                        }
                        $scope.cost = $scope.cost || ($scope.prdocdata[i].gldd_cost_center_code != '' || $scope.prdocdata[i].gldd_cost_center_code != "");
                        $scope.totalDebit = parseFloat($scope.totalDebit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_debit);
                        $scope.totalcredit = parseFloat($scope.totalcredit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_credit);

                       

                    }

                });

            }


        }]
        )
})();
