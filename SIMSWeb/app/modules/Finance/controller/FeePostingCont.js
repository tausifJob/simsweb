﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('FeePostingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.grid = true;
            $scope.edit_data = false;
            $scope.feeposting_data = [];
            var data1 = [];
            $scope.edt = {};
            var deletecode = [];
            $scope.dept_comp_code;
            $scope.slma_acno_dept_no;
            $scope.finyr;
            $scope.valstatus = false;
            $scope.onesec = true;
            $scope.tt = true;

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
                $scope.comp_data = res.data;
                $scope.dept_comp_code = $scope.comp_data[0].dept_comp_code;

                $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetCurrentFinancialYear").then(function (res) {
                    $scope.finyr_data = res.data;
                    $scope.finyr = $scope.finyr_data;

                    $http.get(ENV.apiUrl + "api/common/FeePosting/GetAllAccount_Name?comp_code=" + $scope.dept_comp_code + "&financialyr=" + $scope.finyr).then(function (res) {
                        $scope.AccountNo_data = res.data;
                         });
                });
            });
          
            $http.get(ENV.apiUrl + "api/common/FeePosting/GetAllCurName").then(function (res) {
                $scope.cur_data = res.data;
                if ($scope.cur_data.length > 0)
                {
                    $scope.edt.sims_cur_code = $scope.cur_data[0].sims_cur_code;
                    $scope.getacadyr($scope.edt.sims_cur_code);
                }
            });

            $scope.getacadyr = function (cur_code) {
                $http.get(ENV.apiUrl + "api/common/FeePosting/GetAllAcademicYear?cur_code=" + cur_code).then(function (res) {
                    $scope.acadyr_data = res.data;
                    if ($scope.acadyr_data.length > 0) {
                        $scope.edt.sims_academic_year = $scope.acadyr_data[0].sims_academic_year;
                    }
                    $scope.getgrade($scope.edt.sims_cur_code,$scope.edt.sims_academic_year)
                });
            }

            $(function () {
                $('#cmb_grade,#cmb_section').multipleSelect({
                    width: '100%'
                });
            });

            $scope.getgrade = function (cur, acad_yr) {
                debugger
                if (cur != null && acad_yr != null) {
                    $http.get(ENV.apiUrl + "api/common/getAllGradesFeePost?cur_code=" + cur + "&ac_year=" + acad_yr).then(function (res) {
                        $scope.grade = res.data;
                        setTimeout(function () {
                            $('#cmb_grade').change(function () {
                                console.log($(this).val());
                            }).multipleSelect({
                                width: '100%'
                            });
                        }, 1000);
                        $scope.getsection($scope.edt.sims_cur_code, $scope.edt.acadyr_data, $scope.edt.sims_grade_code)
                    });


                }
            }

            $scope.getsection = function (cur, acad_yr, grade) {
                if (cur != null && acad_yr != null) {
                    if (grade != "null") {
                        $http.get(ENV.apiUrl + "api/common/SectionScreening/getAllSection?cur_code=" + cur + "&ac_year=" + acad_yr + "&grade=" + grade).then(function (res) {
                            $scope.section = res.data;
                           
                            setTimeout(function () {
                                $('#cmb_section').change(function () {
                                    console.log($(this).val());
                                }).multipleSelect({
                                    width: '100%'
                                });
                            }, 1000);
                        });
                    }
                }
            }

            $scope.getfeeposting_accrualdata = function (cur_code, acad_yr, grade_code, sec_code) {
                $scope.pagershow = false;
                debugger
                $http.get(ENV.apiUrl + "api/common/FeePosting/GetAllfins_fees_posting_accrual?cur_code=" + cur_code + "&ac_year=" + acad_yr + "&g_code=" + grade_code + "&sec_code=" + $scope.edt.sims_section_code).then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.feeposting_data = res.data;
                    $scope.totalItems = $scope.feeposting_data.length;
                    $scope.todos = $scope.feeposting_data;
                    $scope.makeTodos();
                    if ($scope.feeposting_data.length > 0) {
                        $scope.tt = false;
                    }
                });
            }

            $scope.getAllAccountData = function (accNo, mod) {
                if (accNo != null) {
                    //swal({ title: "Alert", text: "Do You Want To Copy For All Months", imageUrl: "assets/img/notification-alert.png",},
                    //function ()
                    //{
                    //    
                    mod.fins_advance_received_acno_feb = accNo;
                    mod.fins_advance_received_acno_mar = accNo;
                    mod.fins_advance_received_acno_apr = accNo;
                    mod.fins_advance_received_acno_may = accNo;
                    mod.fins_advance_received_acno_jun = accNo;
                    mod.fins_advance_received_acno_jul = accNo;
                    mod.fins_advance_received_acno_aug = accNo;
                    mod.fins_advance_received_acno_sep = accNo;
                    mod.fins_advance_received_acno_oct = accNo;
                    mod.fins_advance_received_acno_nov = accNo;
                    mod.fins_advance_received_acno_dec = accNo;
                    // });

                }
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.edt =
                {
                    sims_singlesec: 'true'
                }

            $scope.Save = function (isvalidate) {
                debugger
                var data1 = [];
                var data = [];
                $scope.insert = false;

                if (isvalidate) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++)
                    {
                        if ($scope.filteredTodos[i].fins_fee_number1 == true)
                        {
                            if ($scope.filteredTodos[i].fins_revenue_acno != "" || $scope.filteredTodos[i].fins_receivable_acno != "" ||
                                $scope.filteredTodos[i].fins_discount_acno != "" || $scope.filteredTodos[i].fins_advance_academic_year_acno != "" ||
                                $scope.filteredTodos[i].fins_advance_received_acno_jan != "" || $scope.filteredTodos[i].fins_advance_received_acno_feb != "" ||
                                $scope.filteredTodos[i].fins_advance_received_acno_mar != "" || $scope.filteredTodos[i].fins_advance_received_acno_apr != "" ||
                                $scope.filteredTodos[i].fins_advance_received_acno_may != "" || $scope.filteredTodos[i].fins_advance_received_acno_jun != "" ||
                                $scope.filteredTodos[i].fins_advance_received_acno_jul != "" || $scope.filteredTodos[i].fins_advance_received_acno_aug != "" ||
                                $scope.filteredTodos[i].fins_advance_received_acno_sep != "" || $scope.filteredTodos[i].fins_advance_received_acno_oct != "" ||
                                $scope.filteredTodos[i].fins_advance_received_acno_nov != "" || $scope.filteredTodos[i].fins_advance_received_acno_dec != "") {
                                var data = {
                                    fins_fee_cur_code: $scope.edt.sims_cur_code,
                                    fins_fee_academic_year: $scope.edt.sims_academic_year,
                                    fins_fee_grade_code: null,
                                    fins_fee_number: $scope.filteredTodos[i].fins_fee_number,
                                    fins_revenue_acno: $scope.filteredTodos[i].fins_revenue_acno,
                                    fins_advance_received_acno_jan: $scope.filteredTodos[i].fins_advance_received_acno_jan,
                                    fins_advance_received_acno_feb: $scope.filteredTodos[i].fins_advance_received_acno_feb,
                                    fins_advance_received_acno_mar: $scope.filteredTodos[i].fins_advance_received_acno_mar,
                                    fins_advance_received_acno_apr: $scope.filteredTodos[i].fins_advance_received_acno_apr,
                                    fins_advance_received_acno_may: $scope.filteredTodos[i].fins_advance_received_acno_may,
                                    fins_advance_received_acno_jun: $scope.filteredTodos[i].fins_advance_received_acno_jun,
                                    fins_advance_received_acno_jul: $scope.filteredTodos[i].fins_advance_received_acno_jul,
                                    fins_advance_received_acno_aug: $scope.filteredTodos[i].fins_advance_received_acno_aug,
                                    fins_advance_received_acno_sep: $scope.filteredTodos[i].fins_advance_received_acno_sep,
                                    fins_advance_received_acno_oct: $scope.filteredTodos[i].fins_advance_received_acno_oct,
                                    fins_advance_received_acno_nov: $scope.filteredTodos[i].fins_advance_received_acno_nov,
                                    fins_advance_received_acno_dec: $scope.filteredTodos[i].fins_advance_received_acno_dec,
                                    fins_receivable_acno: $scope.filteredTodos[i].fins_receivable_acno,
                                    fins_fees_posting_status: $scope.filteredTodos[i].fins_fees_posting_status,
                                    fins_discount_acno: $scope.filteredTodos[i].fins_discount_acno,
                                    fins_advance_academic_year_acno: $scope.filteredTodos[i].fins_advance_academic_year_acno,
                                    opr: 'U'
                                };

                                $scope.insert = true;
                                data1.push(data);
                                }
                        }
                        
                    }

                    if ($scope.insert)
                    {
                        $http.post(ENV.apiUrl + "api/common/FeePosting/CUDUpdate_fins_fees_posting", data1).then(function (res) {
                            $scope.feepost = res.data;

                            if ($scope.feepost == true) {
                                swal({ title: "Alert", text: "Fee Posting Updated Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getfeeposting_accrualdata($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.edt.sims_grade_code, $scope.edt.sims_section_code);
                                    }
                                }
                                    );
                                str.status = false;
                            }
                            else if ($scope.feepost == false) {
                                swal({ title: "Alert", text: "Fee Posting Not Updated Successfully. " , imageUrl: "assets/img/notification-alert.png", },
                                     function () {
                                         $scope.getfeeposting_accrualdata($scope.edt.sims_cur_code, $scope.edt.sims_academic_year, $scope.edt.sims_grade_code, $scope.edt.sims_section_code);
                                     }
                                    );
                                str.status = false;
                            }
                            else ("Error-" + $scope.feepost)
                        });
                    }
                    else
                    {
                        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                    }
                }
            }

            $scope.cancel = function (str) {
                str.status = false;
            }

            $scope.mainCancel = function () {
                $scope.edt = [];
                $scope.feeposting_data = [];
                $scope.filteredTodos = [];
                $scope.totalItems = [];
                $scope.todos = [];
                $scope.pagershow = true;
                try {
                    $("#cmb_grade").multipleSelect("uncheckAll");
                }
                catch (ex)
                { }
                try {
                    $("#cmb_section").multipleSelect("uncheckAll");
                }
                catch (ex) { }
            }

            $scope.edit = function (str) {
                str.status = true;
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.size = function (str) {
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;  $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.feeposting_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.feeposting_data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.fins_fee_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_fee_code_description.toLowerCase().indexOf(toSearch.toLowerCase()) > -1);
            }

        }])
})();