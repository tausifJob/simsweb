﻿
(function () {
    'use strict';
    var chksearch;
    var main1;
    var prdocdata = [];
    var data1 = [];
    var data = [];
    var doc_no;
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('FinanceAuditCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {
            $scope.pagesize = "15";

            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            //$http.get(ENV.apiUrl + "api/PrintVoucher/getPurchasedeatils").then(function (result) {
            //    $scope.total_counts = result.data;

            //});

            //$http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=Single").then(function (res) {
            //    $scope.final_doc_url = res.data;
            //});


            //$http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=cheque").then(function (res1) {
            //    $scope.cheque_report_url = res1.data;
            //});

            //$http.get(ENV.apiUrl + "api/PrintVoucher/getprintvoucherForCheque").then(function (result) {
            //    $scope.total_cheque = result.data;
            //});


            $scope.search_textbox = false;
            $scope.disabled_fromdate = false;

            $scope.propertyName = null;
            $scope.reverse = false;
            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $scope.countData = [
              { val: 15, data: 15 },
              { val: 50, data: 50 },
              { val: 100, data: 100 },

            ]
            debugger
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');

            $scope.edt =
                          {
                              from_date: $scope.ddMMyyyy,
                              to_date: $scope.ddMMyyyy,
                          }

            $scope.size = function (str) {
                if (str == 15 || str == 50 || str == 100) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }

                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;

                $scope.makeTodos();

            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);



                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 15, $scope.maxSize = 15;

            $scope.createdate = function (date) {
                if ($scope.edt.from_date != '') {
                    if ($scope.edt.from_date > $scope.edt.to_date) {
                        swal({ title: 'Please Select future Date', width: 380, height: 100 });
                        $scope.edt.to_date = '';
                    }
                }
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);





            $scope.Show = function (from_date, to_date, search, comp_cd) {

                if ($scope.edt.search == undefined || $scope.edt.search == "") {
                    $scope.edt.search = '';
                }
                $scope.table1 = true;
                $scope.filteredTodos = [];
                $scope.pagesize = '15';
                $scope.currentPage = 1;
                $scope.numPerPage = 15;
                $http.get(ENV.apiUrl + "api/FinanceReportRoute/getAllVoucherForPrint?from_date=" + $scope.edt.from_date + "&to_date=" + $scope.edt.to_date + "&search=" + $scope.edt.search + "&comp_code=" + comp_code).then(function (AllVoucherForPrint_Data) {
                    $scope.table1 = true;
                    $scope.VoucherForPrint_Data = AllVoucherForPrint_Data.data;
                    if ($scope.VoucherForPrint_Data.length > 0) {
                        $scope.pager = true;

                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.VoucherForPrint_Data.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.VoucherForPrint_Data.length, data: 'All' })
                        }

                        $scope.totalItems = $scope.VoucherForPrint_Data.length;
                        $scope.todos = $scope.VoucherForPrint_Data;
                        $scope.makeTodos();
                        $scope.table1 = true;
                        $scope.page_index = true;
                        $scope.currentPage = 1;
                    }
                    else {
                        swal({ title: "Alert", text: "Vouchers Not Found", width: 300, height: 200 });
                        $scope.page_index = false;
                        $scope.pager = false;
                        $scope.filteredTodos = [];
                    }
                });
            }
            document.onkeydown = function (event) {
                debugger
                event = event || window.event;
                if (event.keyCode == 27) {
                    $('#feemodal').modal('hide');
                }
            }
            $scope.report_show = true;
            $scope.Report = function (str) {
                debugger;
                $('#feemodal').modal('show');
                $scope.report_show = false;

                // $http.get(ENV.apiUrl + "api/StudentFee/GetReceipt").then(function (res) {
                // $scope.reportparameter = res.data;
                var data = {
                    location: $scope.final_doc_url,
                    parameter: {
                        comp_detail: comp_code,
                        doc_code: str.gltd_doc_code,
                        doc_no: str.gltd_final_doc_no

                    },
                    state: 'main.FINS15',
                    ready: function () {
                        this.refreshReport();
                    },
                }

                window.localStorage["ReportDetails"] = JSON.stringify(data)
                //$state.go('main.ReportCardParameter')
                try {
                    $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                    $scope.location = $scope.rpt.location;
                    $scope.parameter = $scope.rpt.parameter;
                    $scope.state = $scope.rpt.state;
                }
                catch (ex) {
                }
                var s;
                s = "SimsReports." + $scope.location + ",SimsReports";
                var url = window.location.href;
                var domain = url.substring(0, url.indexOf(':'))
                if ($http.defaults.headers.common['schoolId'] == 'sms') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                }
                else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                }

                else {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                }
                console.log(service_url);
                $scope.parameters = {}

                $("#reportViewer1")
                               .telerik_ReportViewer({
                                   serviceUrl: service_url,
                                   viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                   scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                   // Zoom in and out the report using the scale
                                   // 1.0 is equal to 100%, i.e. the original size of the report
                                   scale: 1.0,
                                   ready: function () {
                                       //this.refreshReport();
                                   }

                               });
                var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                reportViewer.reportSource({
                    report: s,
                    parameters: $scope.parameter,
                });

                setInterval(function () {

                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                }, 1000);

                $timeout(function () {
                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                }, 100)
                // });
            }


            $scope.print_cheque = function (str) {
                debugger
                $('#feemodal').modal('show');
                $scope.report_show = false;
                var data = {
                    location: $scope.cheque_report_url,
                    parameter: {
                        //  comp_details: comp_code,
                        doc_code: str.gltd_doc_code,
                        doc_no: str.gltd_final_doc_no
                    },

                    state: 'main.FINS15'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                // $state.go('main.ReportCardParameter')
                try {
                    $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                    $scope.location = $scope.rpt.location;
                    $scope.parameter = $scope.rpt.parameter;
                    $scope.state = $scope.rpt.state;
                }
                catch (ex) {
                }
                var s;
                s = "SimsReports." + $scope.location + ",SimsReports";
                var url = window.location.href;
                var domain = url.substring(0, url.indexOf(':'))
                if ($http.defaults.headers.common['schoolId'] == 'sms') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                }
                else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                }

                else {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                }
                console.log(service_url);
                $scope.parameters = {}

                $("#reportViewer1")
                               .telerik_ReportViewer({
                                   serviceUrl: service_url,
                                   viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                   scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                   // Zoom in and out the report using the scale
                                   // 1.0 is equal to 100%, i.e. the original size of the report
                                   scale: 1.0,
                                   ready: function () {
                                       //this.refreshReport();
                                   }

                               });
                var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                reportViewer.reportSource({
                    report: s,
                    parameters: $scope.parameter,
                });

                setInterval(function () {

                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                }, 1000);

                $timeout(function () {
                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                }, 100)

            }



            $scope.check_searchnull = function () {
                chksearch = document.getElementById("chkmarks1");

                if (chksearch.checked == true) {
                    $scope.search_textbox = true;
                }
                else {
                    $scope.search_textbox = false;
                }
            }

            $scope.check_fromdate = function () {
                main1 = document.getElementById("chkmarks");

                if (main1.checked == true) {
                    $scope.edt.from_date = '';
                    $scope.disabled_fromdate = true;
                }
                else {
                    $scope.disabled_fromdate = false;
                    $scope.edt =
                          {
                              from_date: $scope.ddMMyyyy,
                              to_date: $scope.ddMMyyyy,
                          }
                }
            }



            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {

                $('input[type="text"]', $(this).parent()).focus();
            });
        }])
})();
