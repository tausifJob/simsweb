﻿(function () {
    'use strict';
    var obj1, obj2, temp, main;
    var opr = '';
    var categorycode = [];

    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ScheduleGroupCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.ScheduleGroupDetail = true;
            $scope.editmode = false;
            $scope.edt = "";
            var data1 = [];

            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;


            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            //var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;

                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.SGDExamData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            
            
            $http.get(ENV.apiUrl + "api/ScheduleGroup/getScheduleGroup?comp_code=" + comp_code).then(function (SGD_Data) {
                $scope.SGDExamData = SGD_Data.data;
                $scope.totalItems = $scope.SGDExamData.length;
                $scope.todos = $scope.SGDExamData;
                $scope.makeTodos();

            });

            $http.get(ENV.apiUrl + "api/ScheduleGroup/getTypeGroup").then(function (res) {
                $scope.gtname = res.data;
               
            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);


                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.SGDExamData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.SGDExamData;
                }

                $scope.makeTodos();
                $scope.check_all();
                main.checked = false;
            }

            //function searchUtil(item, toSearch) {
            //    /* Search Text in all 3 fields */
            //    return (item.glsg_group_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.glsg_group_code == toSearch) ? true : false;
            //}

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                    item.glsg_group_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.glsg_group_description.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.glsg_group_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.glsg_group_ind.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.glsg_parent_group.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.glsg_parent_group == toSearch) ? true : false;

            }


            $scope.cancel = function () {
                $scope.ScheduleGroupDetail = true;
                $scope.ScheduleGroupOperation = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.myForm.$setPristine();
                    $scope.myForm.$setUntouched();
                    $scope.newmode = true;
                    $scope.edt = '';
                    $scope.editmode = false;
                    opr = 'S';
                    $scope.readonly = false;
                    $scope.ScheduleGroupDetail = false;
                    $scope.ScheduleGroupOperation = true;
                    $scope.savebtn = true;
                    $scope.updatebtn = false;
                    $scope.edt = {};
                }
            }

            $scope.Save = function (myForm) {
                
                if (myForm) {

                    var data = $scope.edt;
                    data.opr = 'I';
                    data.glsg_comp_code = comp_code;
                  //  $scope.edt['comp_code'] = comp_code;
                    $scope.exist = false;
                    data1.push(data);
                    for (var i = 0; i < $scope.SGDExamData.length; i++) {
                        if ($scope.SGDExamData[i].glsg_group_code == data.glsg_group_code) {
                            $scope.exist = true;
                        }

                    }
                    if ($scope.exist) {
                        swal({ title: "Alert", text: "Record Already exists", width: 300, height: 200 });
                    }

                    else {

                        $http.post(ENV.apiUrl + "api/ScheduleGroup/ScheduleGroupCUD?simsobj=", data1).then(function (msg) {
                            $scope.msg1 = msg.data;

                            if ($scope.msg1 == true) {

                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ title: "Alert", text: "Record Not Inserted. " , width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                           

                            $http.get(ENV.apiUrl + "api/ScheduleGroup/getScheduleGroup?comp_code=" + comp_code).then(function (SGD_Data) {
                                $scope.SGDExamData = SGD_Data.data;
                                $scope.totalItems = $scope.SGDExamData.length;
                                $scope.todos = $scope.SGDExamData;
                                $scope.makeTodos();
                               

                            });

                        });

                        $scope.ScheduleGroupDetail = true;
                        $scope.ScheduleGroupOperation = false;
                    }
                    data1 = [];
                }

                else {
                    $scope.Update();

                }

            }

            $scope.up = function (str) {
                if ($scope.user_access.data_update==false)
                {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else
                {
                opr = 'U';
                // $scope.edt = str;
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.ScheduleGroupDetail = false;
                $scope.ScheduleGroupOperation = true;
                $scope.edt = {
                    glsg_group_code: str.glsg_group_code
                           , glsg_group_description: str.glsg_group_description
                           , glsg_group_type: str.glsg_group_type
                           , glsg_group_ind: str.glsg_group_ind
                           , glsg_parent_group: str.glsg_parent_group



                };
               }

            }

            $scope.Update = function () {
              
                    var data = $scope.edt;
                    data.opr = 'U';
                    data.glsg_comp_code = comp_code;
                    $scope.edt = "";
                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/ScheduleGroup/ScheduleGroupCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ title: "Alert", text: "Record Not Updated. ", width: 300, height: 200 });
                        }
                        else {
                            swl("Error-" + $scope.msg1)
                        }
                      
                        $http.get(ENV.apiUrl + "api/ScheduleGroup/getScheduleGroup?comp_code=" + comp_code).then(function (SGD_Data) {
                            $scope.SGDExamData = SGD_Data.data;
                            $scope.totalItems = $scope.SGDExamData.length;
                            $scope.todos = $scope.SGDExamData;
                            $scope.makeTodos();

                           
                        });

                    })
                    $scope.ScheduleGroupDetail = true;
                    $scope.ScheduleGroupOperation = false;
                    data1 = [];
                
            }

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("sch-"+i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("sch-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.check_once = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }

            $scope.deleterecord = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    var data1 = [];
                    $scope.flag = false;
                    var deleteleave = [];
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("sch-" + i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodulecode = ({
                                'glsg_group_code': $scope.filteredTodos[i].glsg_group_code,
                                opr: 'D'
                            });
                            deleteleave.push(deletemodulecode);
                        }
                    }
                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                $http.post(ENV.apiUrl + "api/ScheduleGroup/ScheduleGroupCUD", deleteleave).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/ScheduleGroup/getScheduleGroup?comp_code=" + comp_code).then(function (SGD_Data) {
                                                    $scope.SGDExamData = SGD_Data.data;
                                                    $scope.totalItems = $scope.SGDExamData.length;
                                                    $scope.todos = $scope.SGDExamData;
                                                    $scope.makeTodos();
                                                });
                                                main = document.getElementById('all_chk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ title: "Alert", text: "Record Not Deleted. ", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/ScheduleGroup/getScheduleGroup?comp_code=" + comp_code).then(function (SGD_Data) {
                                                    $scope.SGDExamData = SGD_Data.data;
                                                    $scope.totalItems = $scope.SGDExamData.length;
                                                    $scope.todos = $scope.SGDExamData;
                                                    $scope.makeTodos();
                                                });
                                                main = document.getElementById('all_chk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    $scope.row1 = '';
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });
                            }
                            else {
                                main = document.getElementById('all_chk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById(i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                    }
                    $scope.currentPage = str;
                }
            }


        }]);

})();