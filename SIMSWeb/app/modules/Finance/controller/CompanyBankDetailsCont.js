﻿(function () {
    'use strict';
    var del = [];
    var main;
    var date1;
    var date3;
    var simsController = angular.module('sims.module.Setup');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CompanyBankDetailsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.edit_code = false;
            $scope.edt = {};
            $scope.grid = true;
            $scope.display = false;

            var data = [];
            //var deletecode = [];
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

           
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;



            $http.get(ENV.apiUrl + "api/PDCBill/getBankDeatails").then(function (getBankDetails) {
                $scope.BankDetails = getBankDetails.data;

            });

           
            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/finance/getAllCompanyBankDetails?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                    $scope.Bankmaster = res.data;
                    $scope.totalItems = $scope.Bankmaster.length;
                    $scope.todos = $scope.Bankmaster;
                    $scope.makeTodos();
                    if (res.data.length > 0) {
                        $scope.Showsave = false;
                        $scope.rpt_list = res.data;
                    }
                    else {
                        swal('No Record Found');
                    }
                })
            }

            $scope.getgrid();


             
            //$http.get(ENV.apiUrl + "api/finance/getAllCompanyBankDetails?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res1) {

            //    $scope.Bankmaster = res1.data;
            //    $scope.totalItems = $scope.Bankmaster.length;
            //    $scope.todos = $scope.Bankmaster;
            //    $scope.makeTodos();

            //});

            
            $scope.edit = function(str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.display = true ;
                    $scope.grid = false;
                    $scope.save1 = false;
                    $scope.update1 = true;
                    $scope.delete1 = false;
                    $scope.edt =
                        {
                           
                            comp_bank_code: str.comp_bank_code,
                            comp_bank_name: str.comp_bank_name,
                            comp_bank_branch_name: str.comp_bank_branch_name,
                            comp_bank_address: str.comp_bank_address,
                            comp_bank_account_no: str.comp_bank_account_no,
                            comp_bank_iban_no: str.comp_bank_iban_no,
                            comp_route_code: str.comp_route_code,
                            comp_ifsc_code: str.comp_ifsc_code,
                            comp_swift_code: str.comp_swift_code,
                            comp_bank_status: str.comp_bank_status

                        }

                    $scope.Update = true;

                }
            }


            $scope.Save = function (isvalidate) {
                //debugger;
                var data = [];
                if (isvalidate) {
                    if ($scope.update1 == false) {
                       /* var data = ({
                            comp_bank_name: $scope.edt.comp_bank_name,
                            comp_bank_branch_name: $scope.edt.comp_bank_branch_name,
                            comp_bank_address: $scope.edt.comp_bank_address,
                            comp_bank_account_no: $scope.edt.comp_bank_account_no,
                            comp_bank_iban_no: $scope.edt.comp_bank_iban_no,
                            comp_route_code: $scope.edt.comp_route_code,
                            comp_ifsc_code:$scope.edt.comp_ifsc_code,
                            opr: 'I'
                        });*/
                        var data = $scope.edt;
                        data.opr = 'I';
                        data.comp_code = comp_code;
                        
                        //data1.push(data);

                        $http.post(ENV.apiUrl + "api/finance/CBDAllCompanyBankDetails",data).then(function (res) {
                            debugger;
                          
                            $scope.grid = true;
                            $scope.display = false;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Company Bank Details Added Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                       
                                        $scope.getgrid();
                                    }
                                 
                                });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ text:" Company Bank Details Record Already Exists. ", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });
                    }
                    else {
                      $scope.Update2(isvalidate);
                    }
                }
            }

            

            $scope.Update2 = function (isvalidate) {
                //debugger;
                var data = [];
                if (isvalidate) {
                    if ($scope.update1 == true) {
                        var data = $scope.edt;
                        data.opr = 'U';
                        data.comp_code = comp_code;
                                
            
                        $http.post(ENV.apiUrl + "api/finance/CBDAllCompanyBankDetails", data).then(function (res) {
                            debugger;
                            $scope.grid = true;
                            $scope.display = false;
                            $scope.msg1 = res.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Company Bank Details Updated Successfully.", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {

                                        $scope.getgrid();
                                    }
                                });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ title: "Alert", text: "Only one Company Bank Details can be set as current at a time.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {

                                        $scope.getgrid();
                                    }
                                });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            } s
                        });
                    }
                }
            }


            $scope.New = function () {

                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.grid = false;
                    $scope.display = true;
                    $scope.grid = false;

                    $scope.save1 = true;
                    $scope.update1 = false;
                    $scope.delete1 = false;
                    $scope.edt = {};
                    $scope.edit_code = false;

                }
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

      
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.Bankmaster.length; i++) {
                        
                        var v = document.getElementById("bank-" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    } Bankmaster
                }
                else {
                    for (var i = 0; i < $scope.Bankmaster.length; i++) {
                       
                        var v = document.getElementById("bank-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }
            

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }

            }
           
       
                  
            $scope.Delete = function () {
                debugger;
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    var deletemodercode = [];
                    //$scope.flag = false;
                    for (var i = 0; i < $scope.Bankmaster.length; i++) {
                        var v = document.getElementById("bank-" + i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            //var data = $scope.edt;
                            //data.opr = 'D';
                           // data.comp_code = comp_code;
                            var deletemodercode = ({
                               'comp_bank_code': $scope.Bankmaster[i].comp_bank_code,
                                'comp_code' :comp_code,
                                'comp_bank_name': $scope.Bankmaster[i].comp_bank_name,
                                'comp_bank_branch_name': $scope.Bankmaster[i].comp_bank_branch_name,
                                'comp_bank_address': $scope.Bankmaster[i].comp_bank_address,
                                'comp_bank_account_no': $scope.Bankmaster[i].comp_bank_account_no,
                                'comp_bank_iban_no': $scope.Bankmaster[i].comp_bank_iban_no,
                                'comp_route_code': $scope.Bankmaster[i].comp_route_code,
                                'comp_ifsc_code': $scope.Bankmaster[i].comp_ifsc_code,
                                'comp_swift_code': $scope.Bankmaster[i].comp_swift_code,
                                'comp_bank_status': $scope.Bankmaster[i].comp_bank_status,
                                 opr: 'D'
                            });
                            ////deletecode.push(deletemodercode);
                        }
                    }

                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {


                                $http.post(ENV.apiUrl + "api/finance/CBDAllCompanyBankDetails", deletemodercode).then(function (res) {
                                    debugger;
                                    $scope.msg1 = res.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Company Bank Details Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Record Already Mapped.Can't be Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $scope.getgrid();
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });


                            }
                            else {
                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }

                                for (var i = 0; i < $scope.Bankmaster.length; i++) {
                                    
                                    var v = document.getElementById("bank-" + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }

                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                }
            }

            $scope.size = function (str) {
                if (str == 10 || str == 20) {
                    $scope.pager = true;
                }

                else {

                    $scope.pager = false;
                }
                if (str == "*") {
                    $scope.numPerPage = $scope.acadyr_data.length;
                    $scope.Bankmaster = $scope.acadyr_data;
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                }
                $scope.makeTodos();

            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }
            $scope.Bankmaster = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);


                $scope.Bankmaster = $scope.todos.slice(begin, end);
            };
            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


        }])
})();