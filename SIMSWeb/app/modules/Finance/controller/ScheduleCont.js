﻿(function () {
    'use strict';
    var opr = '';
    var deletecode = [];
    var main;
    var data1 = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ScheduleCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            //$scope.pagesize = '5';
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.table1 = true;
            
            //var user = $rootScope.globals.currentUser.username;
            //var comp_code = $scope.global_finnComp;
            //var finance_year = $scope.global_acd;


            //$scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            
            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
           // var user = $rootScope.globals.currentUser.username;
            $http.get(ENV.apiUrl + "api/schedulegroupdetails/getGroupName").then(function (res) {
                $scope.gn = res.data;

            });
            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            $http.get(ENV.apiUrl + "api/ScheduleDetails/getScheduleCode?glsc_comp_code=" + comp_code + "&fyear=" + finance_year).then(function (getScheduleCode_Data) {
                $scope.ScheduleCode_Data = getScheduleCode_Data.data;
                $scope.totalItems = $scope.ScheduleCode_Data.length;
                $scope.todos = $scope.ScheduleCode_Data;
                $scope.makeTodos();
            });

            //$http.get(ENV.apiUrl + "api/common/getFinsCompCode").then(function (getFinsCompCode_Data) {
            //    $scope.FinsCompCode_Data = getFinsCompCode_Data.data;
               
            //});

            //$http.get(ENV.apiUrl + "api/common/getAcaYearbyCompCode").then(function (getAcaYearbyCompCode) {
            //    $scope.AcaYearbyCompCode = getAcaYearbyCompCode.data;
                
            //});

            $scope.operation = false;

            $scope.editmode = false;

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.ScheduleCode_Data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                //$scope.CheckAllChecked();
                $scope.row1 = '';
            }

            $scope.edt = "";

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.edt = '';
                    $scope.newmode = true;
                    $scope.check = true;
                    $scope.editmode = false;
                    $scope.readonly = false;
                    $scope.table1 = false;
                    $scope.operation = true;
                    $scope.savebtn = true;
                    $scope.updatebtn = false;
                    $scope.edt = {};
                    $scope.edt['gsgd_sign'] = true;
                    $scope.myForm.$setPristine();
                    $scope.myForm.$setUntouched();
                }
            }

            $scope.up = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.editmode = true;
                    $scope.newmode = false;
                    $scope.savebtn = false;
                    $scope.updatebtn = true;
                    $scope.readonly = true;
                    $scope.table1 = false;
                    $scope.operation = true;

                    $scope.edt = {
                        glsc_comp_code: str.glsc_comp_code,
                        company_name: str.company_name,
                        glsc_year: str.glsc_year,
                        glsc_sched_code: str.glsc_sched_code,
                        glsc_sched_name: str.glsc_sched_name,
                        gsgd_group_code: str.glsg_group_code,
                        gsgd_sign: str.gsgd_sign
                    };
                }
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.edt = {
                    glsc_comp_code: "",
                    company_name: "",
                    glsc_year: "",
                    glsc_sched_code: "",
                    glsc_sched_name: ""
                }

            }

            $scope.saveUpdate = function (myForm) {
                debugger;
                if ($scope.savebtn == true) {
                    $scope.Save(myForm);
                }
                else if ($scope.updatebtn == true) {
                    $scope.Update(myForm);
                }
            }
            $scope.Save = function (myForm) {
                debugger;
                if ($scope.savebtn == true) {
                    if (myForm) {
                        data1 = [];
                        var data = {
                            glsc_comp_code: comp_code,
                            glsc_year: finance_year,
                            glsc_sched_code: $scope.edt.glsc_sched_code,
                            glsc_sched_name: $scope.edt.glsc_sched_name,
                            glsc_sched_grp_code: $scope.edt.gsgd_group_code,
                            gsgd_sign: $scope.edt.gsgd_sign,
                            opr: 'I'
                        };
                        $scope.exist = false;
                        for (var i = 0; i < $scope.ScheduleCode_Data.length; i++) {
                            if ($scope.ScheduleCode_Data[i].glsc_sched_code == data.glsc_sched_code &&
                                $scope.ScheduleCode_Data[i].glsc_comp_code == data.glsc_comp_code &&
                                $scope.ScheduleCode_Data[i].glsc_year == data.glsc_year) {
                                $scope.exist = true;
                            }
                        }
                        if ($scope.exist) {
                            swal({ title: 'Alert', text: "Schedule Code Already Exists", width: 300, height: 200 })
                        }
                        else {
                            data1.push(data);
                            $http.post(ENV.apiUrl + "api/ScheduleDetails/ScheduleCUD", data1).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ title: "Alert", text: "Record Not Inserted. ", width: 300, height: 200 });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                                $scope.operation = false;
                                $http.get(ENV.apiUrl + "api/ScheduleDetails/getScheduleCode?glsc_comp_code=" + comp_code + "&fyear=" + finance_year).then(function (getScheduleCode_Data) {
                                    $scope.ScheduleCode_Data = getScheduleCode_Data.data;
                                    $scope.totalItems = $scope.ScheduleCode_Data.length;
                                    $scope.todos = $scope.ScheduleCode_Data;
                                    $scope.makeTodos();
                                });
                            })
                        }
                        $scope.table1 = true;
                        $scope.operation = false;
                    }
                }
                else if ($scope.updatebtn == true) {
                    
                }
                
            }

            $scope.Update = function (myForm) {
                debugger;
                if ($scope.savebtn == true) {
                    
                }
                else if ($scope.updatebtn == true) {
                    if (myForm) {
                        data1 = [];
                        var data = {
                            glsc_comp_code: comp_code,
                            glsc_year: finance_year,
                            glsc_sched_code: $scope.edt.glsc_sched_code,
                            glsc_sched_name: $scope.edt.glsc_sched_name,
                            glsc_sched_grp_code: $scope.edt.gsgd_group_code,
                            opr: 'U'
                        };
                        data1.push(data);
                        $http.post(ENV.apiUrl + "api/ScheduleDetails/ScheduleCUD", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Data Updated Successfully", width: 300, height: 200 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ title: "Alert", text: "Data Not Updated. ", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                            $scope.operation = false;
                            $http.get(ENV.apiUrl + "api/ScheduleDetails/getScheduleCode?glsc_comp_code=" + comp_code + "&fyear=" + finance_year).then(function (getScheduleCode_Data) {
                                $scope.ScheduleCode_Data = getScheduleCode_Data.data;
                                $scope.totalItems = $scope.ScheduleCode_Data.length;
                                $scope.todos = $scope.ScheduleCode_Data;
                                $scope.makeTodos();
                            });
                        })
                        $scope.operation = false;
                        $scope.table1 = true;
                    }
                }
                
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

              

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.deleterecord = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    deletecode = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deleteSchedulecode = ({
                                'glsc_sched_code': $scope.filteredTodos[i].glsc_sched_code,
                                'glsc_comp_code': $scope.filteredTodos[i].glsc_comp_code,
                                'glsc_year': $scope.filteredTodos[i].glsc_year,
                                'opr': 'D'
                            });
                            deletecode.push(deleteSchedulecode);
                        }
                    }
                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                $http.post(ENV.apiUrl + "api/ScheduleDetails/ScheduleCUD", deletecode).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/ScheduleDetails/getScheduleCode?glsc_comp_code=" + comp_code + "&fyear=" + finance_year).then(function (getScheduleCode_Data) {
                                                    $scope.ScheduleCode_Data = getScheduleCode_Data.data;
                                                    $scope.totalItems = $scope.ScheduleCode_Data.length;
                                                    $scope.todos = $scope.ScheduleCode_Data;
                                                    $scope.makeTodos();
                                                });
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    {
                                                        $scope.row1 = '';
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ title: "Alert", text: "Record Not Deleted. ", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/ScheduleDetails/getScheduleCode?glsc_comp_code=" + comp_code + "&fyear=" + finance_year).then(function (getScheduleCode_Data) {
                                                    $scope.ScheduleCode_Data = getScheduleCode_Data.data;
                                                    $scope.totalItems = $scope.ScheduleCode_Data.length;
                                                    $scope.todos = $scope.ScheduleCode_Data;
                                                    $scope.makeTodos();
                                                });
                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    {
                                                        $scope.row1 = '';
                                                    }
                                                }
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                    $scope.currentPage = true;
                                });
                            }
                            else {
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById(i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        main.checked = false;
                                        $scope.row1 = '';
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                    }
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.ScheduleCode_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.ScheduleCode_Data;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                     item.company_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.glsc_sched_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    //  item.lc_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                   //  item.lc_debit_acno_nm.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                     item.glsc_sched_code == toSearch) ? true : false;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);
        }])
})();
