﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('FeeTypeFeePostingAccountMappingCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.display = false;
            $scope.pagesize = "10";
            $scope.pageindex = "1";
            $scope.grid = true;
            $scope.edit_data = false;
            $scope.feeposting_data = [];
            var data1 = [];
            var deletecode = [];
            $scope.dept_comp_code;
            $scope.slma_acno_dept_no;
            $scope.finyr;
            $scope.valstatus = false;
            $scope.onesec = true;
            $scope.tt = true;
            $scope.selected_data = '';
            $scope.temp = [];
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            console.log($scope.finnDetail)
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;




            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.temp = {
                    'sims_cur_code': $scope.curriculum[0].sims_cur_code
                }
                $scope.getAccYear($scope.temp.sims_cur_code)
            });
           
            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    debugger;
                    $scope.temp ['sims_academic_year']= $scope.Acc_year[0].sims_academic_year;
                       
                });

            }


         

            $scope.Show = function (cur,a_year) {

              debugger

              $http.get(ENV.apiUrl + "api/PrintVoucher/getfee_posting_acno_mapping_fee_type?curcode=" + cur + "&academic_year=" + a_year).then(function (res) {
                  $scope.all_data = res.data;
                  $scope.totalItems = $scope.all_data.length;
                  $scope.todos = $scope.all_data;
                  $scope.makeTodos();
                })

            }

            $scope.getAccountall = function (str) {
                
                console.log(str.vat_ac_status);
                debugger

                $scope.fee_type_desc = str.sims_fee_code_description;
                $scope.selected_year = str.sims_fee_academic_year;
                $scope.seclected_fee_code = str.sims_fee_code;
                $scope.temp['vat_ac_status'] = str.vat_ac_status;
               
                    setTimeout(function () {
                        $('#FeeposModal').modal({ backdrop: 'static', keyboard: false });
                    }, 500)
                    $scope.selected_data = str;

                    $("#account_codes").select2("val", str.pb_gl_acno);

                    $("#account_codes1").select2("val", str.fins_revenue_acno);
                    $("#account_codes2").select2("val", str.fins_receivable_acno);
                    $("#account_codes3").select2("val", str.fins_discount_acno);
                    $("#account_codes4").select2("val", str.fins_advance_academic_year_acno);

                    if (str.vat_ac_status == 'E') {
                        $("#vat_posting_account").select2("val", str.fins_vat_posting_acno);
                        $scope.vat_posting_acno = true;
                    } else {
                        $("#vat_posting_account").select2("val", '');
                        $scope.vat_posting_acno = false;

                    }
                  


                    $("#cmb_jan1").select2("val", str.fins_curr_advance_received_acno_jan);
                    $("#cmb_feb2").select2("val", str.fins_curr_advance_received_acno_feb);
                    $("#cmb_mar3").select2("val", str.fins_curr_advance_received_acno_mar);
                    $("#cmb_apr4").select2("val", str.fins_curr_advance_received_acno_apr);
                    $("#cmb_may5").select2("val", str.fins_curr_advance_received_acno_may);
                    $("#cmb_jun6").select2("val", str.fins_curr_advance_received_acno_jun);
                    $("#cmb_jul7").select2("val", str.fins_curr_advance_received_acno_jul);
                    $("#cmb_aug8").select2("val", str.fins_curr_advance_received_acno_aug);
                    $("#cmb_sep9").select2("val", str.fins_curr_advance_received_acno_sep);
                    $("#cmb_oct10").select2("val", str.fins_curr_advance_received_acno_oct);
                    $("#cmb_nov11").select2("val", str.fins_curr_advance_received_acno_nov);
                    $("#cmb_dec12").select2("val", str.fins_curr_advance_received_acno_dec);


                    $("#cmb_fins_jan1").select2("val", str.fins_receivable_acno_jan);
                    $("#cmb_fins_feb2").select2("val", str.fins_receivable_acno_feb);
                    $("#cmb_fins_mar3").select2("val", str.fins_receivable_acno_mar);
                    $("#cmb_fins_apr4").select2("val", str.fins_receivable_acno_apr);
                    $("#cmb_fins_may5").select2("val", str.fins_receivable_acno_may);
                    $("#cmb_fins_jun6").select2("val", str.fins_receivable_acno_jun);
                    $("#cmb_fins_jul7").select2("val", str.fins_receivable_acno_jul);
                    $("#cmb_fins_aug8").select2("val", str.fins_receivable_acno_aug);
                    $("#cmb_fins_sep9").select2("val", str.fins_receivable_acno_sep);
                    $("#cmb_fins_oct10").select2("val", str.fins_receivable_acno_oct);
                    $("#cmb_fins_nov11").select2("val", str.fins_receivable_acno_nov);
                    $("#cmb_fins_dec12").select2("val", str.fins_receivable_acno_dec);


                    $scope.temp['fins_fees_posting_status'] = true;


            }

            $scope.Save1 = function () {

                debugger
                var data1 = [];
                var data = [];
                $scope.insert = false;
                         if ($scope.temp.fins_revenue_acno != "") {
                             $scope.insert = true;
                                var data = {
                                    sims_fee_cur_code: $scope.temp.sims_cur_code,
                                    sims_fee_academic_year: $scope.temp.sims_academic_year,
                                    sims_fee_code: $scope.seclected_fee_code,
                                   // vat_ac_status:$scope.temp.vat_ac_status,
                                    fins_revenue_acno: $scope.temp.fins_revenue_acno,
                                    fins_receivable_acno: $scope.temp.fins_receivable_acno,
                                    fins_discount_acno: $scope.temp.fins_discount_acno,
                                    fins_advance_academic_year_acno: $scope.temp.fins_advance_academic_year_acno,
                                    fins_vat_posting_acno:$scope.temp.fins_vat_posting_acno,




                                    fins_curr_advance_received_acno_jan: $scope.temp.fins_curr_advance_received_acno_jan,
                                    fins_curr_advance_received_acno_feb: $scope.temp.fins_curr_advance_received_acno_feb,
                                    fins_curr_advance_received_acno_mar: $scope.temp.fins_curr_advance_received_acno_mar,
                                    fins_curr_advance_received_acno_apr: $scope.temp.fins_curr_advance_received_acno_apr,
                                    fins_curr_advance_received_acno_may: $scope.temp.fins_curr_advance_received_acno_may,
                                    fins_curr_advance_received_acno_jun: $scope.temp.fins_curr_advance_received_acno_jun,
                                    fins_curr_advance_received_acno_jul: $scope.temp.fins_curr_advance_received_acno_jul,
                                    fins_curr_advance_received_acno_aug: $scope.temp.fins_curr_advance_received_acno_aug,
                                    fins_curr_advance_received_acno_sep: $scope.temp.fins_curr_advance_received_acno_sep,
                                    fins_curr_advance_received_acno_oct: $scope.temp.fins_curr_advance_received_acno_oct,
                                    fins_curr_advance_received_acno_nov: $scope.temp.fins_curr_advance_received_acno_nov,
                                    fins_curr_advance_received_acno_dec: $scope.temp.fins_curr_advance_received_acno_dec,


                                    fins_receivable_acno_jan: $scope.temp.fins_receivable_acno_jan,  // fins_curr_adv_receivable_acno_jan,
                                    fins_receivable_acno_feb: $scope.temp.fins_receivable_acno_feb,  // fins_curr_adv_receivable_acno_feb,
                                    fins_receivable_acno_mar: $scope.temp.fins_receivable_acno_mar,   //fins_curr_adv_receivable_acno_mar,
                                    fins_receivable_acno_apr: $scope.temp.fins_receivable_acno_apr,   //fins_curr_adv_receivable_acno_apr,
                                    fins_receivable_acno_may: $scope.temp.fins_receivable_acno_may,   //fins_curr_adv_receivable_acno_may,
                                    fins_receivable_acno_jun: $scope.temp.fins_receivable_acno_jun,   //fins_curr_adv_receivable_acno_jun,
                                    fins_receivable_acno_jul: $scope.temp.fins_receivable_acno_jul,   //fins_curr_adv_receivable_acno_jul,
                                    fins_receivable_acno_aug: $scope.temp.fins_receivable_acno_aug,   //fins_curr_adv_receivable_acno_aug,
                                    fins_receivable_acno_sep: $scope.temp.fins_receivable_acno_sep,   //fins_curr_adv_receivable_acno_sep,
                                    fins_receivable_acno_oct: $scope.temp.fins_receivable_acno_oct,   //fins_curr_adv_receivable_acno_oct,
                                    fins_receivable_acno_nov: $scope.temp.fins_receivable_acno_nov,   //fins_curr_adv_receivable_acno_nov,
                                    fins_receivable_acno_dec: $scope.temp.fins_receivable_acno_dec,   //fins_curr_adv_receivable_acno_dec,




                                    fins_fees_posting_status: $scope.temp.fins_fees_posting_status,
                                    opr: 'I'
                                };

                                $scope.insert = true;
                                data1.push(data);
                                console.log(data1);
                            }
                        

                    

                    if ($scope.insert) {
                        $http.post(ENV.apiUrl + "api/PrintVoucher/updateFeePosting_new", data1).then(function (res) {
                            $scope.feepost = res.data;

                            if ($scope.feepost == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully..." });
                                $('#FeeposModal').modal('hide');
                                $scope.Show($scope.temp.sims_cur_code, $scope.temp.sims_academic_year)

                                //$http.get(ENV.apiUrl + "api/PrintVoucher/getfee_posting_acno_mapping_fee_type?curcode=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (res) {
                                //    $scope.all_data = res.data;
                                //    $scope.totalItems = $scope.all_data.length;
                                //    $scope.todos = $scope.all_data;
                                //    $scope.makeTodos();
                                //})

                            }
                            else if ($scope.feepost == false)
                                swal({ title: "Alert", text: "Record not Inserted..." });
                            else
                                swal("Error-" + $scope.feepost)
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                    }
                
            }



            


            $http.get(ENV.apiUrl + "api/BankPayment/GetAllGLAccountNos?glma_accountcode=" + null + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {
                $scope.GlACNO = docstatus.data;

                setTimeout(function () {
                    $("#account_codes1").select2();
                    $("#account_codes2").select2();
                    $("#account_codes3").select2();
                    $("#account_codes4").select2();
                    $("#account_codes5").select2(); 
                    $("#vat_posting_account").select2();

                    $("#cmb_jan1").select2();
                    $("#cmb_feb2").select2();
                    $("#cmb_mar3").select2();
                    $("#cmb_apr4").select2();
                    $("#cmb_may5").select2();
                    $("#cmb_jun6").select2();
                    $("#cmb_jul7").select2();
                    $("#cmb_aug8").select2();
                    $("#cmb_sep9").select2();
                    $("#cmb_oct10").select2();
                    $("#cmb_nov11").select2();
                    $("#cmb_dec12").select2();

                    $("#cmb_fins_jan1").select2();
                    $("#cmb_fins_feb2").select2();
                    $("#cmb_fins_mar3").select2();
                    $("#cmb_fins_apr4").select2();
                    $("#cmb_fins_may5").select2();
                    $("#cmb_fins_jun6").select2();
                    $("#cmb_fins_jul7").select2();
                    $("#cmb_fins_aug8").select2();
                    $("#cmb_fins_sep9").select2();
                    $("#cmb_fins_oct10").select2();
                    $("#cmb_fins_nov11").select2();
                    $("#cmb_fins_dec12").select2();





                }, 100);

            });

            debugger;
            $http.get(ENV.apiUrl + "api/DeliveryReasons/GetAllGLAccountNos?glma_accountcode=" + '' + "&cmpnycode=" + comp_code + "&finacial_year=" + finance_year).then(function (res) {
                debugger
                $scope.getFeeaccouncodes = res.data;
                console.log($scope.getFeeaccouncodes);
            });

            $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
                $scope.comp_data = res.data;
                console.log($scope.comp_data);
                $scope.dept_comp_code = $scope.comp_data[0].dept_comp_code;

                $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetCurrentFinancialYear").then(function (res) {
                    $scope.finyr_data = res.data;
                    $scope.finyr = $scope.finyr_data;

                    $http.get(ENV.apiUrl + "api/common/FeePosting/GetAllAccount_Name?comp_code=" + $scope.dept_comp_code + "&financialyr=" + $scope.finyr).then(function (res) {
                        $scope.AccountNo_data = res.data;
                        console.log($scope.AccountNo_data);
                    });
                });
            });

            $http.get(ENV.apiUrl + "api/common/FeePosting/GetAllCurName").then(function (res) {
                $scope.cur_data = res.data;
            });

            $scope.getacadyr = function (cur_code) {
                $http.get(ENV.apiUrl + "api/common/FeePosting/GetAllAcademicYear?cur_code=" + cur_code).then(function (res) {
                    $scope.acadyr_data = res.data;
                });
            }

            $(function () {
                $('#cmb_grade,#cmb_section').multipleSelect({
                    width: '100%'
                });
            });

            

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);

                setTimeout(function () {
                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                  
                        $("#cmb_revenue_acno"+i).select2();
                        $("#cmb_fins_receivable_acno"+i).select2();
                        $("#cmb_fins_discount_acno"+i).select2();
                        $("#cmb_fins_curr_advance_received_acno" + i).select2();

                        $("#cmb_fins_curr_adv_receivable_acno_jan" + i).select2();
                        $("#cmb_fins_curr_adv_receivable_acno_feb" + i).select2();
                        $("#cmb_fins_curr_adv_receivable_acno_mar" + i).select2();
                        $("#cmb_fins_curr_adv_receivable_acno_apr" + i).select2();
                        $("#cmb_fins_curr_adv_receivable_acno_may" + i).select2();
                        $("#cmb_fins_curr_adv_receivable_acno_jun" + i).select2();
                        $("#cmb_fins_curr_adv_receivable_acno_jul" + i).select2();
                        $("#cmb_fins_curr_adv_receivable_acno_aug" + i).select2();
                        $("#cmb_fins_curr_adv_receivable_acno_sep" + i).select2();
                        $("#cmb_fins_curr_adv_receivable_acno_oct" + i).select2();
                        $("#cmb_fins_curr_adv_receivable_acno_nov" + i).select2();
                        $("#cmb_fins_curr_adv_receivable_acno_dec" + i).select2();
                        $("#cmb_fins_advance_academic_year_acno" + i).select2();




                        $("#cmb_fins_advance_academic_year_acno_jan" + i).select2();
                        $("#cmb_fins_advance_academic_year_acno_feb" + i).select2();
                        $("#cmb_fins_advance_academic_year_acno_mar" + i).select2();
                        $("#cmb_fins_advance_academic_year_acno_apr" + i).select2();
                        $("#cmb_fins_advance_academic_year_acno_may" + i).select2();
                        $("#cmb_fins_advance_academic_year_acno_jun" + i).select2();
                        $("#cmb_fins_advance_academic_year_acno_jul" + i).select2();
                        $("#cmb_fins_advance_academic_year_acno_aug" + i).select2();
                        $("#cmb_fins_advance_academic_year_acno_sep" + i).select2();
                        $("#cmb_fins_advance_academic_year_acno_oct" + i).select2();
                        $("#cmb_fins_advance_academic_year_acno_nov" + i).select2();
                        $("#cmb_fins_advance_academic_year_acno_dec" + i).select2();

                   

                }
                }, 1000);
            };

            $scope.edt =
                {
                    sims_singlesec: 'true'
                }

            $scope.Save = function (isvalidate) {
                debugger
                var data1 = [];
                var data = [];
                $scope.insert = false;

                if (true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        if ($scope.filteredTodos[i].fins_fee_number1 == true) {
                            if ($scope.filteredTodos[i].fins_revenue_acno != "") {

                                var data = {
                                    sims_fee_cur_code: $scope.filteredTodos[i].sims_fee_cur_code,
                                    sims_fee_academic_year: $scope.filteredTodos[i].sims_fee_academic_year,
                                    sims_fee_code: $scope.filteredTodos[i].sims_fee_code,

                                    fins_revenue_acno: $scope.filteredTodos[i].fins_revenue_acno,
                                    fins_receivable_acno: $scope.filteredTodos[i].fins_receivable_acno,
                                    fins_discount_acno: $scope.filteredTodos[i].fins_discount_acno,
                                    fins_advance_academic_year_acno: $scope.filteredTodos[i].fins_advance_academic_year_acno,
                                    fins_vat_posting_acno:$scope.temp.fins_vat_posting_acno,




                                    fins_curr_advance_received_acno_jan: $scope.filteredTodos[i].fins_curr_advance_received_acno_jan,
                                    fins_curr_advance_received_acno_feb: $scope.filteredTodos[i].fins_curr_advance_received_acno_feb,
                                    fins_curr_advance_received_acno_mar: $scope.filteredTodos[i].fins_curr_advance_received_acno_mar,
                                    fins_curr_advance_received_acno_apr: $scope.filteredTodos[i].fins_curr_advance_received_acno_apr,
                                    fins_curr_advance_received_acno_may: $scope.filteredTodos[i].fins_curr_advance_received_acno_may,
                                    fins_curr_advance_received_acno_jun: $scope.filteredTodos[i].fins_curr_advance_received_acno_jun,
                                    fins_curr_advance_received_acno_jul: $scope.filteredTodos[i].fins_curr_advance_received_acno_jul,
                                    fins_curr_advance_received_acno_aug: $scope.filteredTodos[i].fins_curr_advance_received_acno_aug,
                                    fins_curr_advance_received_acno_sep: $scope.filteredTodos[i].fins_curr_advance_received_acno_sep,
                                    fins_curr_advance_received_acno_oct: $scope.filteredTodos[i].fins_curr_advance_received_acno_oct,
                                    fins_curr_advance_received_acno_nov: $scope.filteredTodos[i].fins_curr_advance_received_acno_nov,
                                    fins_curr_advance_received_acno_dec: $scope.filteredTodos[i].fins_curr_advance_received_acno_dec,
                                   

                                fins_receivable_acno_jan: $scope.filteredTodos[i].fins_receivable_acno_jan,  // fins_curr_adv_receivable_acno_jan,
                                fins_receivable_acno_feb: $scope.filteredTodos[i].fins_receivable_acno_feb,  // fins_curr_adv_receivable_acno_feb,
                                fins_receivable_acno_mar: $scope.filteredTodos[i].fins_receivable_acno_mar,   //fins_curr_adv_receivable_acno_mar,
                                fins_receivable_acno_apr: $scope.filteredTodos[i].fins_receivable_acno_apr,   //fins_curr_adv_receivable_acno_apr,
                                fins_receivable_acno_may: $scope.filteredTodos[i].fins_receivable_acno_may,   //fins_curr_adv_receivable_acno_may,
                                fins_receivable_acno_jun: $scope.filteredTodos[i].fins_receivable_acno_jun,   //fins_curr_adv_receivable_acno_jun,
                                fins_receivable_acno_jul: $scope.filteredTodos[i].fins_receivable_acno_jul,   //fins_curr_adv_receivable_acno_jul,
                                fins_receivable_acno_aug: $scope.filteredTodos[i].fins_receivable_acno_aug,   //fins_curr_adv_receivable_acno_aug,
                                fins_receivable_acno_sep: $scope.filteredTodos[i].fins_receivable_acno_sep,   //fins_curr_adv_receivable_acno_sep,
                                fins_receivable_acno_oct: $scope.filteredTodos[i].fins_receivable_acno_oct,   //fins_curr_adv_receivable_acno_oct,
                                fins_receivable_acno_nov: $scope.filteredTodos[i].fins_receivable_acno_nov,   //fins_curr_adv_receivable_acno_nov,
                                fins_receivable_acno_dec: $scope.filteredTodos[i].fins_receivable_acno_dec,   //fins_curr_adv_receivable_acno_dec,




                                    fins_fees_posting_status:$scope.filteredTodos[i].fins_fees_posting_status,
                                    opr: 'I'
                                };

                                $scope.insert = true;
                                data1.push(data);
                                console.log(data1);
                            }
                        }

                    }

                    if ($scope.insert) {
                        $http.post(ENV.apiUrl + "api/PrintVoucher/updateFeePosting_new", data1).then(function (res) {
                            $scope.feepost = res.data;

                            if ($scope.feepost == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully..." });
                                $http.get(ENV.apiUrl + "api/PrintVoucher/getfee_posting_acno_mapping_fee_type?curcode=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (res) {
                                    $scope.all_data = res.data;
                                    $scope.totalItems = $scope.all_data.length;
                                    $scope.todos = $scope.all_data;
                                    $scope.makeTodos();
                                })

                            }
                            else if ($scope.feepost == false)
                                swal({ title: "Alert", text: "Record not Inserted..." });
                            else
                                swal("Error-" + $scope.feepost)
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                    }
                }
            }

          





            $scope.mainCancel = function () {

                $('#FeeposModal').modal('hide');

            }

           

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i$scope.filteredTodos[i].pc_code);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i$scope.filteredTodos[i].pc_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }

            }

            $scope.checkonebyonedelete = function (str) {
                //debugger
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                        //  str.status = true;
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                        //str.status = false;
                    }
                });



                str.status = str.fins_fee_number1;


            }

            $scope.size = function (str) {

                if (str == "All" || str == "all") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.getFeeData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }


            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.getFeeData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.getFeeData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.pc_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1);
            }

        }])
})();