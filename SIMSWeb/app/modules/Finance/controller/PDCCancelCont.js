﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var payment_mode, from_dt, to_dt, reference_no, bankcode;
    var acconcode = '';
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PDCCancelCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {


            $scope.save_btn = true;
            $scope.tot_btn = true;
            $scope.Maintabledata = false;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.records = false;
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $scope.edt = [];
            var user = $rootScope.globals.currentUser.username;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            setTimeout(function () {
                $("#account_codes8").select2();
            }, 100)


            //$scope.block1.cheque =true;
            $scope.block1 = {
                cheque: 'cheque',
                pdc_return_date: dd + '-' + mm + '-' + yyyy,
            }

           

            //Fill BAnk Name
            $http.get(ENV.apiUrl + "api/PDCSubmission/getBankName?comp_code=" + comp_code).then(function (docstatus1) {
                $scope.bank_name = docstatus1.data;
            });

            debugger
            //Fill department
            $http.get(ENV.apiUrl + "api/PDCSubmission/getDepartment?finance_year=" + finance_year + "&comp_code=" + comp_code).then(function (docstatus2) {
                $scope.Department = docstatus2.data;
                console.log($scope.Department);
            });


            //Fill Ledger
            $http.get(ENV.apiUrl + "api/BankPayment/GetLedgerNumber?financialyear=" + finance_year + "&comp_code=" + comp_code).then(function (docstatus2) {
                $scope.LdgrCode = docstatus2.data;
            });

            $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {

                $scope.getAllAccNos = docstatus.data;
            });

            $scope.getSLAccNo = function () {

                
                var acconcode = $scope.getAllAccNos.gldd_acct_code;
                if ($scope.block1.gldd_ledger_code != "00") {

                    $http.get(ENV.apiUrl + "api/JVCreation/GetSLAccNumber?ldgr_code=" + $scope.block1.gldd_ledger_code + "&cmp_cd=" + comp_code + "&year=" + finance_year).then(function (docstatus3) {
                        // $scope.sllc_ldgr_code = slcode;
                        $scope.getAllAccNos = docstatus3.data;
                        //$scope.slacno = docstatus3.data;
                    });
                }
                else {
                    $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {

                        $scope.getAllAccNos = docstatus.data;
                    });
                }


            }

            //Account number


            //Select Data SHOW
            var showdetails = [];
            $scope.getViewDetails = function () {

                $('#loader').modal({ backdrop: 'static', keyboard: false });

                
                //$scope.busy = true;
                if ($scope.block1.cheque == 'cheque') {
                    payment_mode = 'Ch';
                }
                else {
                    payment_mode = 'Dd';
                }

                //bankcode = [];
                //if ($scope.block1.bank_code.length == 0 || $scope.block1 == undefined) {
                //    bankcode = '';
                //}
                //else {
                //    bankcode1 = $scope.block1.bank_code;
                //    bankcode = bankcode + ',' + bankcode1;;
                //}

                var data = {
                    ps_comp_code: comp_code,
                    ps_dept_no: $scope.block1.dept_no,
                    ps_ldgr_code: $scope.block1.gldd_ledger_code,
                    ps_sl_acno: $scope.block1.gldd_acct_code,
                    ps_discd: 'O',
                    pc_calendar: 'B',
                    ps_bank_code: $scope.block1.bank_code,
                    filterText: $scope.edt.SelectOption,
                    filterRange: $scope.edt.txt_filter_range,
                    //recFromDate: ($scope.block1.receipt_date) ? $scope.block1.from_date : null,
                    //recToDate: ($scope.block1.receipt_date) ? $scope.block1.to_date : null,

                    ps_due_dateStr: $scope.block1.from_date,
                    subMissionDate: $scope.block1.to_date,
                    pc_ref_no: $scope.block1.submission_no,
                    pc_payment_mode: payment_mode,

                }
                


                $http.post(ENV.apiUrl + "api/PDCSubmission/AllPDCSubmission_new_data", data).then(function (res1) {
                    $scope.pdcSubmissionList = res1.data;
                    $scope.pdc_details = res1.data;
                    //$scope.totalItems = $scope.pdc_details.length;
                    //$scope.todos = $scope.pdc_details;
                    $scope.Maintabledata = true;

                    
                    $scope.total = 0;
                    for (var i = 0; i < res1.data.length; i++) {
                        $scope.total = $scope.total + res1.data[i].ps_amount;

                    }
                    $scope.subtotal = 0;
                    // $scope.busy = false;
                    $('#loader').modal('hide');
                    $(".modal-backdrop").removeClass("modal-backdrop");

                });


                showdetails = []


            }


            var cntr = 0;
            $scope.selectonebyone = function (str, chq_amt) {
                
                //var d=document.getElementById(str);
                if (str == true) {
                    cntr = cntr + 1;
                    $scope.subtotal = parseFloat($scope.subtotal) + parseFloat(chq_amt);
                }
                else {
                    cntr = cntr - 1;
                    $scope.subtotal = parseFloat($scope.subtotal) - parseFloat(chq_amt);
                }
            }



            //DATA SAVE INSERT for cheque Transfer
            var datasend = [];
            $scope.savedata = function () {//Myform
                $scope.disable_save_btn = true;
                if ($scope.block1.cheque == 'cheque') {
                    payment_mode = 'Ch';
                }
                else {
                    payment_mode = 'Dd';
                }



                $('#loader').modal({ backdrop: 'static', keyboard: false });

                
                //  var data = $scope.temp;
                //data.opr = 'I';
                for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {

                    if ($scope.pdcSubmissionList[i].ps_cheque_no_check == true) {

                        $scope.pdcSubmissionList[i].ps_dept_name = "Cancellation of Cheque";
                        $scope.pdcSubmissionList[i].ps_ldgr_code_name = user;
                        $scope.pdcSubmissionList[i].pc_payment_mode = payment_mode;
                        $scope.pdcSubmissionList[i].ps_approv_date = $scope.block1.pdc_return_date;

                        

                        datasend.push($scope.pdcSubmissionList[i]);
                    }
                }

                if (datasend.length > 0) {
                    $http.post(ENV.apiUrl + "api/PDCSubmission/Finn102_PDC_Cancellation", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        swal({ text: $scope.msg1.strMessage, timer: 5000 });
                        //swal({ title: "Alert", text: $scope.msg1.strMessage, imageUrl: "assets/img/check.png", });
                        $('#loader').modal('hide');
                        $(".modal-backdrop").removeClass("modal-backdrop");
                        $scope.disable_save_btn = false;
                        $scope.getViewDetails();
                    });

                }
                else {
                    swal({ text:'Please Select Cheque', timer: 5000});
                    $scope.disable_save_btn = false;
                    $('#loader').modal('hide');
                }
                datasend = [];
            }

            //Date FORMAT
            $scope.showdate = function (date, name1) {

                //var month = date.split("/")[0];
                //var day = date.split("/")[1];
                //var year = date.split("/")[2];

                //$scope.temp[name1] = year + "-" + month + "-" + day;
                var day = date.split("-")[0];
                var month = date.split("-")[1];
                var year = date.split("-")[2];

                $scope.temp[name1] = day + "-" + month + "-" + year;
            }


            $scope.CheckAllChecked = function () {
                
                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                    if (main.checked == true) {
                        $scope.pdcSubmissionList[i].ps_cheque_no_check = true;
                        $scope.subtotal = parseFloat($scope.subtotal) + parseFloat($scope.pdcSubmissionList[i].ps_amount);
                        $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                    }
                    else {
                        $scope.pdcSubmissionList[i].ps_cheque_no_check = false;
                        $scope.subtotal = parseFloat($scope.subtotal) - parseFloat($scope.pdcSubmissionList[i].ps_amount);
                        $scope.pdcSubmissionList[i].row_color = ''
                    }
                }

            }

            $scope.ShowFilterText = function (str) {
                if (str == "") {
                    $scope.filterVisible = false;
                }
                else {

                    $scope.filterVisible = true;
                }
            }

            $scope.FilterCode = function () {
                
                var splitno = [];
                if ($scope.temp.chk_previous_clear == true) {
                    main = document.getElementById('mainchk');
                    for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {

                        var v = document.getElementById(i + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $scope.pdcSubmissionList[i].row_color = 'white';

                    }
                    if ($scope.edt.SelectOption == 'Select Records') {
                        

                        for (var i = 0; i < $scope.edt.txt_filter_range; i++) {
                            var v = document.getElementById(i + i);
                            v.checked = true;
                            $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                        }
                    }

                    else if ($scope.edt.SelectOption == 'Cheque Amount between') {


                        if ($scope.edt.txt_filter_range != undefined) {
                            splitno = $scope.edt.txt_filter_range.split('-');
                        }
                        if (splitno.length >= 2) {

                            for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                                if ($scope.pdcSubmissionList[i].ps_amount >= splitno[0] && $scope.pdcSubmissionList[i].ps_amount <= splitno[1]) {

                                    var v = document.getElementById(i + i);
                                    v.checked = true;
                                    $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                                }
                            }
                        }
                    }
                    else if ($scope.edt.SelectOption == 'Cheque Series between') {


                        if ($scope.edt.txt_filter_range != undefined) {
                            
                            splitno = $scope.edt.txt_filter_range.split('-');
                        }
                        if (splitno.length >= 2) {
                            $scope.minchkno = 0;
                            $scope.maxchkno = 0;
                            for (var j = 0; j < splitno.length; j++) {
                                if (j == 0) {
                                    $scope.minchkno = parseInt(splitno[0]);
                                }
                                else {
                                    $scope.maxchkno = parseInt(splitno[1]);
                                }

                            }

                            for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                                
                                if ($scope.pdcSubmissionList[i].ps_cheque_no >= $scope.minchkno && $scope.pdcSubmissionList[i].ps_cheque_no <= $scope.maxchkno) {

                                    var v = document.getElementById(i + i);
                                    v.checked = true;
                                    $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                                }
                            }
                        }

                    }
                }

                else {

                    if ($scope.edt.SelectOption == 'Select Records') {

                        if ($scope.pdcSubmissionList.length >= $scope.edt.txt_filter_range) {
                            for (var i = 0; i < $scope.edt.txt_filter_range; i++) {
                                var v = document.getElementById(i + i);
                                v.checked = true;
                                $scope.pdcSubmissionList[i].row_color = '#ffffcc'

                            }
                        }
                    }

                    else if ($scope.edt.SelectOption == 'Cheque Amount between') {


                        if ($scope.edt.txt_filter_range != undefined) {
                            splitno = $scope.edt.txt_filter_range.split('-');
                        }
                        if (splitno.length >= 2) {

                            for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                                if ($scope.pdcSubmissionList[i].ps_amount >= splitno[0] && $scope.pdcSubmissionList[i].ps_amount <= splitno[1]) {

                                    var v = document.getElementById(i + i);
                                    v.checked = true;
                                    $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                                }
                            }
                        }
                    }
                    else if ($scope.edt.SelectOption == 'Cheque Series between') {


                        if ($scope.edt.txt_filter_range != undefined) {
                            splitno = $scope.edt.txt_filter_range.split('-');
                        }
                        if (splitno.length >= 2) {

                            $scope.minchkno = 0;
                            $scope.maxchkno = 0;
                            for (var j = 0; j < splitno.length; j++) {
                                if (j == 0) {
                                    $scope.minchkno = parseInt(splitno[0]);
                                }
                                else {
                                    $scope.maxchkno = parseInt(splitno[1]);
                                }

                            }

                            for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                                
                                if ($scope.pdcSubmissionList[i].ps_cheque_no >= $scope.minchkno && $scope.pdcSubmissionList[i].ps_cheque_no <= $scope.maxchkno) {

                                    var v = document.getElementById(i + i);
                                    v.checked = true;
                                    $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                                }
                            }

                            //for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                            //    if ($scope.pdcSubmissionList[i].ps_cheque_no >= splitno[0] && $scope.pdcSubmissionList[i].ps_cheque_no <= splitno[1]) {

                            //        var v = document.getElementById(i);
                            //        v.checked = true;
                            //        $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                            //    }
                            //}
                        }

                    }

                }
            }

            $scope.search = function () {
                debugger;
                $scope.todos = $scope.searched($scope.pdcSubmissionList, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.pdcSubmissionList;
                }
            }

            $scope.searched = function (valLists, toSearch) {

                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            function searchUtil(item, toSearch) {
                debugger
                /* Search Text in all 2 fields */
                return (item.ps_cheque_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.ps_bank_code_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.pc_dept_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sllc_ldgr_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    || item.pc_parent_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.pc_father_full_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    || item.pc_our_doc_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.ps_due_date.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    || item.ps_cheque_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.ps_amount.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                  ) ? true : false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd-mm-yyyy'
                //format: 'yyyy-mm-dd',

            });


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

        }])

})();