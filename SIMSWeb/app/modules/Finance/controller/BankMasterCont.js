﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('BankMasterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;


            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            setTimeout(function () {
                $("#account_codes").select2();
            }, 100);

            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;


            
            
            $http.get(ENV.apiUrl + "api/BankMaster/getAllBankMaster?comp_code="+comp_code+"&finance_year=" + finance_year).then(function (res1) {
                
                $scope.Bankmaster = res1.data;
                $scope.totalItems = $scope.Bankmaster.length;
                $scope.todos = $scope.Bankmaster;
                $scope.makeTodos();

            });

            //Combobox Dependency

            $scope.getApplication = function (modcode) {
                $scope.app_Name = [];
                $http.get(ENV.apiUrl + "api/UserAudit/getApplicationName?modcode=" + modcode).then(function (appName) {
                    $scope.app_Name = appName.data;
                    });
            }


            ////Fill ComboBox Status
            //$http.get(ENV.apiUrl + "api/Financialyear/getDocumentStatus").then(function (docstatus) {

            //    $scope.docstatus_Data = docstatus.data;
            //});


            //Fill Combo GLAccountNo
            //$http.get(ENV.apiUrl + "api/BankMaster/GetGLAccountNumber?comp_code="+comp_code+"&finance_year=" + finance_year).then(function (docstatus1) {
            $http.get(ENV.apiUrl + "api/BankPayment/GetAllGLAccountNos?glma_accountcode=" + null + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {
                $scope.GlACNO = docstatus.data;
                });

            //Fill Combo Ledger
            //$http.get(ENV.apiUrl + "api/BankPayment/GetLedgerNumber?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (docstatus2) {
                $http.get(ENV.apiUrl + "api/BankPayment/GetLedgerNumber?financialyear=" + finance_year + "&comp_code=" + comp_code).then(function (docstatus2) {    
                $scope.LdgrCode = docstatus2.data;
                });


            //Fill Combo SLACCNO
            $scope.getSLAccNo = function (slcode) {
                $scope.glaccno = true;

                $http.get(ENV.apiUrl + "api/BankPayment/GetSLAccNumber?pbslcode=" + slcode + "&cmp_cd=" + comp_code + "&fyear=" + finance_year).then(function (docstatus3) {
                    
                    $scope.slacno = docstatus3.data;
                    });
            }

            //Select Data SHOW

           




            //$http.get(ENV.apiUrl + "api/GoalTarget/getAcademicYear").then(function (Acyear) {
            //    $scope.Acc_year = Acyear.data;
            //});
            $scope.size = function (str) {
               
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;

                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.Bankmaster;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

                main.checked = false;
                }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Bankmaster, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Bankmaster;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.pb_bank_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.pb_bank_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.pb_srl_no == toSearch) ? true : false;
            }

            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "/" + month + "/" + day;
            }


            //NEW BUTTON
            $scope.New = function () {

                $scope.disabled = false;
                $scope.gtcreadonly = true;
                $scope.readonlybankname = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.ldgno = false;
                $scope.slno = false;
                $scope.cmbstatus = true;
                $scope.txtyear = false;
                $scope.glaccno = false;
                $scope.temp = "";
                $scope.gtdreadonlyBnkcode = false;
                $scope.temp = {};
                $scope.temp.pb_posting_flag = true;

                $("#account_codes").select2("val", "null");

            }

            //EnableDesable

            $scope.setvisible = function () {
                
              
                $scope.ldgno = true;
                $scope.slno = true;
                if ($scope.temp.update == true) {
                    swal({
                        title: '',
                        text: "Changing the GL A/C will not change already posted vouchers. Do you want continue.",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.update(true);
                        }
                        else {
                            $scope.temp.acno= $scope.oldaccnu;
                            $scope.temp.update= true

                            setTimeout(function () {
                                $("#account_codes").select2("val", $scope.oldaccnu);
                            }, 100);

                           
                        }
                       

                    });

                }
            }


            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                
                if (Myform) {

                    if ($scope.temp.acno == undefined && $scope.temp.slma_acno == undefined) {
                        //alert('Please Select atlist one Account Number')
                        swal({  text: "Please Select Atleast one Account Number", showCloseButton: true, width: 300, height: 200 });
                    }
                    else {
                        var data = $scope.temp;
                        data.opr = 'I';
                        data.comp_code = comp_code;
                        dataforSave.push(data);
                        $http.post(ENV.apiUrl + "api/BankMaster/CUDBankMaster", dataforSave).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({  text: "Record Inserted Successfully", imageUrl: "assets/img/notification-alert.png",showCloseButton: true, width: 300, height: 200 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({  text: "Bank code allready present. ", imageUrl: "assets/img/check.png",showCloseButton: true, width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                            $http.get(ENV.apiUrl + "api/BankMaster/getAllBankMaster?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res1) {
                                
                                $scope.Bankmaster = res1.data;
                                $scope.totalItems = $scope.Bankmaster.length;
                                $scope.todos = $scope.Bankmaster;
                                $scope.makeTodos();
                            });
                            $scope.table = true;
                            $scope.display = false;

                        });
                        datasend = [];
                    }
                }
            }

            //Date FORMAT
            $scope.showdate = function (date, name1) {
                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "-" + month + "-" + day;
            }
            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }
            //DATA EDIT
            $scope.edit = function (str) {
                

                $scope.gtcreadonly = true;
                $scope.readonlybankname = false;
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.txtyear = true;
                $scope.cmbstatus = false;
                // $scope.temp = str;
                $scope.ldgno = true;
                $scope.slno = true;
                $scope.gtdreadonlyBnkcode = true;
                $scope.pb_gl_acno = str.pb_gl_acno;
               

                $scope.temp = {
                    pb_bank_code: str.pb_bank_code
                , pb_bank_name: str.pb_bank_name
                , acno: str.pb_gl_acno
                , slma_acno: str.pb_sl_acno
                , sllc_ldgr_code: str.pb_sl_code
                , pb_srl_no: str.pb_srl_no
                , pb_posting_flag: str.pb_posting_flag
                    ,update:true

                };

                $("#account_codes").select2("val", str.pb_gl_acno);
                 $scope.oldaccnu = angular.copy($scope.temp.acno);

            }


            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                
                if (Myform) {

                    var data = $scope.temp;
                    data.opr = "U";
                    data.comp_code = comp_code;
                    dataforUpdate.push(data);
                    //dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/BankMaster/CUDBankMaster", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({  text: "Record Updated Successfully",imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({  text: "Record Not Updated. ",imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $http.get(ENV.apiUrl + "api/BankMaster/getAllBankMaster?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res1) {
                            
                            $scope.Bankmaster = res1.data;
                            $scope.totalItems = $scope.Bankmaster.length;
                            $scope.todos = $scope.Bankmaster;
                            $scope.makeTodos();
                        });
                        $scope.table = true;
                        $scope.display = false;
                    });
                    data2 = [];
                    $scope.temp.update = false;
                }
            }

            //DELETE RECORD
            $scope.CheckAllChecked = function () {
                
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("bank-"+i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("bank-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }




            $scope.OkDelete = function () {
                
                var deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById("bank-" + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'pb_bank_code': $scope.filteredTodos[i].pb_bank_code,
                            'comp_code': $scope.filteredTodos[i].pb_comp_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/BankMaster/CUDBankMaster", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({  text: "Record Deleted Successfully",imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BankMaster/getAllBankMaster?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res1) {
                                                
                                                $scope.Bankmaster = res1.data;
                                                $scope.totalItems = $scope.Bankmaster.length;
                                                $scope.todos = $scope.Bankmaster;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({  text: "Record Not Deleted. ", imageUrl: "assets/img/close.png",showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/BankMaster/getAllBankMaster?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res1) {
                                                
                                                $scope.Bankmaster = res1.data;
                                                $scope.totalItems = $scope.Bankmaster.length;
                                                $scope.todos = $scope.Bankmaster;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById("bank-" + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");

                                }
                            }
                        }
                    });
                }
                else {
                    swal({  text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png",showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                format: "yyyy-mm-dd",
                autoclose: true,
                todayHighlight: true
            });






        }])

})();
