﻿(function () {
    'use strict';
    var main;
    var prdocdata = [];
    var data1 = [];
    var data = [], temp_doc = [];
    //var totalDebit=0;
    //var totalcredit=0;
    var comp_code = "1", chk_rcur;
    var acconcode = "";
    var msg_flag1 = false, jvaut = false, jver = false;

    var simsController = angular.module('sims.module.Finance');
    simsController.controller('ReplicateVoucherCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {
            $scope.pagesize = '5';
            $scope.edt = [];
            $scope.block1 = {};
            $scope.temp_doc = {};
            $scope.operation = true;
            $scope.table1 = true;
            $scope.save1 = true;
            $scope.Add = true;
            $scope.update = false;
            $scope.Update = false;
            var user = $rootScope.globals.currentUser.username;

            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
            var vat_percent = $scope.finnDetail.comp_vat_per;
            var comp_vat_status = $scope.finnDetail.comp_vat_status;
            var input_vat_acct = $scope.finnDetail.input_vat_acct;
            var vat_ac_name = '';
            $scope.report_show = true;

            $scope.btn_reverse = false;
            //$scope.btn_verify = true;
            $scope.prdocdata = [];
            $scope.temp_doc = [];
            //$scope.schoolUrl = window.location.href;
            //var conStr = $scope.schoolUrl.substring(0, $scope.schoolUrl.indexOf('#'));
            $scope.operation = true;
            $scope.chkcost = true;
            $scope.cost_combo = true;
            // $scope.vis = {};
            $scope.doc_cd_prov_no = '';
            $scope.doc_cd_final_doc_no = '';
            $scope.block1 = [];
            $scope.block2 = [];
            $scope.block3 = [];
            $scope.temp_doc = [];
            $scope.vis = [];
            $scope.vis['main'] = true;
            $scope.vis.onchangedoccodeshow = false;
            $scope.dramount = false;
            $scope.cramount = false;
            $scope.vis.doc_detail_grid = true;
            $scope.vis.main_acct_grid = true;
            $scope.save1 = true;
            $scope.vis.add_grid = true;
            $scope.checkprintbtn = false;
            $scope.vis.Add_show_hide = true;
            $scope.prdocdata = [];
            $scope.cmb_doc_cd = [];
            $scope.vis.update = false;
            //new sgr
            $scope.chkcost = false;
            $scope.block3.gldd_vat_enable = false;
            $scope.block3.gldd_include_vat = false;
            $scope.block3.gldd_doc_vat_amt = 0;

            $scope.cost_combo = false;
            var costcentername;

            $scope.readonly = true;
            //$scope.vis.SendToVerify = false;
            $scope.vis.ok = true;
            $scope.vis.is_recur = true;
            

            $scope.vis.cmb_acct = true;
            $scope.vis.txt_acct = false;
            $scope.dis_recur_date = true;
           
           

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
                $("#fixedtable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            setTimeout(function () {
                $("#cmb_acc_Code3").select2();

            }, 100);

            $scope.SearchVouReverData = [];
            $scope.ref_docprvno = '';
            $scope.ref_final_doc_no = '';
            $scope.new_prov_data = '';
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                //format: 'yyyy-mm-dd'
                format: 'dd-mm-yyyy'
            });
            
            $scope.todaysdate = dd + '-' + mm + '-' + yyyy;
            
            $scope.block1 = {

                from_date: dd + '-' + mm + '-' + yyyy,
                to_date: dd + '-' + mm + '-' + yyyy,
            }
            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $http.get(ENV.apiUrl + "api/GLSLQuery/getDocCode_GL_Query?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {

                $scope.getDocCode = res.data;
            });

            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=Single").then(function (res) {
                $scope.final_doc_url = res.data;

            });

          

            $http.get(ENV.apiUrl + "api/BankPayment/GetVerifyAuthorizeUsers?username=" + user + "&comp_code=" + comp_code + "&fyear=" + finance_year).then(function (users_list) {

                $scope.users = users_list.data;
                if ($scope.users.length != 0) {

                    if ($scope.users[0] == user && $scope.users[1] == user) {
                        $scope.preparebtn = false;
                        $scope.verifybtn = false;
                        $scope.authorizebtn = false;

                        $scope.vis.autoriz = true;
                        $scope.vis.very = true;
                    }
                    else if ($scope.users[0] == user && ($scope.users[1] == "" || $scope.users[1] != user)) {
                        $scope.verifybtn = false;
                        $scope.preparebtn = false;
                        $scope.authorizebtn = true;

                        $scope.vis.autoriz = false;
                        $scope.vis.very = true;
                    }
                    else {
                        $scope.preparebtn = false;
                        $scope.verifybtn = true;
                        $scope.authorizebtn = true;

                        $scope.vis.autoriz = false;
                        $scope.vis.very = false;
                    }
                }
            });
            $scope.Show = function () {

                //if (doc_prov_no == undefined)
                //    doc_prov_no = 0;
                //from_date = $scope.edt.from_date;
                //to_date = $scope.edt.to_date;
                $('#loader').modal({ backdrop: 'static', keyboard: false });

                $http.get(ENV.apiUrl + "api/VoucherReversal/getAllVouchers?comp_cd=" + comp_code + "&doc_cd=" + $scope.block1.gltd_doc_code + "&final_doc_no=" + $scope.block1.gltd_final_doc_no +
                    "&from_date=" + $scope.block1.from_date + "&to_date=" + $scope.block1.to_date + "&cheque_no=" + $scope.block1.gltd_cheque_no).then(function (VouRever_Data) {
                        $scope.verify_data = VouRever_Data.data;


                        $('#loader').modal('hide');
                        $(".modal-backdrop").removeClass("modal-backdrop");
                    });
            }

           

            $scope.close = function () {

                $scope.edt = [];

                $scope.Show();
            }

            $scope.Fetchdata = function (cmp_cd, docd, final_doc_no) {
                debugger;
                $scope.docCode1 = docd;
                $scope.provNo1 = final_doc_no;
                $scope.temp_doc = {};
                $('#myModal4').modal('show')
                
                //$scope.prdocdata = [];

                $http.get(ENV.apiUrl + "api/VoucherReversal/getAllRecords?doccode=" + docd + "&prnumber=" + final_doc_no + "&comp_code=" + cmp_cd).then(function (prdoc_data) {

                    $scope.prdocdata = prdoc_data.data;
                    // $scope.block1 = [];
                    $scope.temp_doc = [];
                    $scope.totalDebit = 0;
                    $scope.totalcredit = 0;

                    for (var i = 0; i < $scope.prdocdata.length; i++) {

                        if (i == 0) {
                            $scope.temp_doc.gltd_comp_code = $scope.prdocdata[i].gltd_comp_code;
                            $scope.temp_doc.gltd_doc_code = $scope.prdocdata[i].gltd_doc_code;

                            $scope.temp_doc.gltd_doc_narr = $scope.prdocdata[i].gltd_doc_narr;
                            $scope.temp_doc.gltd_remarks = $scope.prdocdata[i].gltd_remarks;
                            //$scope.temp_doc.gltd_doc_date = $scope.prdocdata[i].gltd_doc_date;
                            $scope.temp_doc.gltd_doc_date = $filter('date')(new Date(), 'dd-MM-yyyy');
                            //$scope.temp_doc.gltd_post_date = $scope.prdocdata[i].gltd_post_date;
                            $scope.temp_doc.gltd_post_date = $filter('date')(new Date(), 'dd-MM-yyyy');
                            $scope.temp_doc.gltd_prov_doc_no = $scope.prdocdata[i].gltd_prov_doc_no;
                            $scope.temp_doc.gltd_final_doc_no = final_doc_no;

                            $scope.temp_doc.gldu_authorize = $scope.prdocdata[i].gldu_authorize;
                            $scope.temp_doc.gldu_verify = $scope.prdocdata[i].gldu_verify;
                            $scope.temp_doc.gltd_authorize_date = $scope.prdocdata[i].gltd_authorize_date;
                            $scope.temp_doc.gltd_verify_date = $scope.prdocdata[i].gltd_verify_date;

                        }
                        $scope.cost = $scope.cost || ($scope.prdocdata[i].gldd_cost_center_code != '' || $scope.prdocdata[i].gldd_cost_center_code != "");
                        $scope.totalDebit = parseFloat(parseFloat($scope.totalDebit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_debit)).toFixed(3);
                        $scope.totalcredit = parseFloat(parseFloat($scope.totalcredit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_credit)).toFixed(3);

                    }

                });
                
            }

            $scope.maxmize = function () {
                $('#jv_Voucher').css({ 'height': '100%', 'width': '100%' })
                //$('#modalcontentbox').css({ 'height': '100%', 'width': '100%' })

                $scope.windowflg = true;
            }

            $scope.manimize = function () {
                $('#jv_Voucher').css({ 'height': '600px', 'width': '900px' })
                $scope.windowflg = false;

            }

            $scope.PrintReport = function () {

                var data = {
                    location: $scope.prov_doc_url,
                    parameter: {
                        comp_detail: comp_code,
                        doc_code: $scope.temp_doc.gltd_doc_code,
                        doc_no: $scope.prvno,

                    },

                    state: 'main.Fin150'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')


            }
          


           

            $scope.report_show1 = true;
            $scope.Print = function (data_obj) {
                
                $scope.report_show1 = false;
                $('#voucherReport').modal('show');
                var data = {
                    location: $scope.final_doc_url,
                    parameter: {
                        comp_detail: comp_code,
                        doc_code: data_obj.gltd_doc_code,
                        doc_no: data_obj.gltd_final_doc_no,

                    },

                    state: 'main.RepVou'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                //$state.go('main.ReportCardParameter')
                try {
                    $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                    $scope.location = $scope.rpt.location;
                    $scope.parameter = $scope.rpt.parameter;
                    $scope.state = $scope.rpt.state;
                }
                catch (ex) {
                }
                var s;

                //if ($http.defaults.headers.common['schoolId'] == 'dpsmis')
                //    s = "SimsReports." + $scope.location + ",SimsReports";


                //else
                //    s = "SimsReports." + $scope.location + ",SimsReports" + $http.defaults.headers.common['schoolId'];


                //  window.localStorage["Finn_comp"] = JSON.stringify(data)

                s = "SimsReports." + $scope.location + ",SimsReports";

                var url = window.location.href;
                var domain = url.substring(0, url.indexOf(':'))
                if ($http.defaults.headers.common['schoolId'] == 'sms') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                }
                else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc' || $http.defaults.headers.common['schoolId'] == 'christ') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'imert') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                }

                else {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                }
                // console.log(service_url);


                $scope.parameters = {}

                $("#reportViewer2")
                               .telerik_ReportViewer({
                                   //serviceUrl: ENV.apiUrl + "api/reports/",
                                   serviceUrl: service_url,

                                   viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                   scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                   // Zoom in and out the report using the scale
                                   // 1.0 is equal to 100%, i.e. the original size of the report
                                   scale: 1.0,
                                   ready: function () {
                                       //this.refreshReport();
                                   }

                               });





                var reportViewer = $("#reportViewer2").data("telerik_ReportViewer");
                reportViewer.reportSource({
                    report: s,
                    parameters: $scope.parameter,
                });

                //var rv = $("#reportViewer1").data("telerik_ReportViewer");
                //rv.commands.print.exec();

                setInterval(function () {

                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                    //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });

                }, 1000);


                $timeout(function () {
                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                    //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });


                }, 100)


            }

           
            $http.get(ENV.apiUrl + "api/BankPayment/GetLedgerNumber?financialyear=" + finance_year + "&comp_code=" + comp_code).then(function (docstatus2) {
                $scope.LdgrCode = docstatus2.data;
            });
            $scope.GetAllGLAcc = function (acconcode, cmpnycode) {
                $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {

                    $scope.getAllAccNos = docstatus.data;
                });


            }

            $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {

                $scope.getAllAccNos = docstatus.data;
            });

            $scope.getSLAccNo = function () {


                var acconcode = $scope.getAllAccNos.gldd_acct_code;
                if ($scope.block3.gldd_ledger_code != "00") {

                    $http.get(ENV.apiUrl + "api/JVCreation/GetSLAccNumber?ldgr_code=" + $scope.block3.gldd_ledger_code + "&cmp_cd=" + comp_code + "&year=" + finance_year).then(function (docstatus3) {
                        // $scope.sllc_ldgr_code = slcode;
                        $scope.getAllAccNos = docstatus3.data;
                        //$scope.slacno = docstatus3.data;
                    });
                }
                else {
                    $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {

                        $scope.getAllAccNos = docstatus.data;
                    });
                }


            }
            $scope.Add_inner_grid = function () {
                //code sgr
                $scope.chkcost = false;
                $scope.cost_combo = false;
                $scope.temp = { cost_chk_ac: false }
                
                var ledcode = "";
                // if (myForm) {
                if ($scope.block3.gldd_acct_code != undefined) {

                    if ($scope.block3.gldd_ledger_code == undefined) {
                        ledcode = '00';
                    }
                    else {
                        ledcode = $scope.block3.gldd_ledger_code;
                    }

                    if ($scope.block3.gldd_doc_amount_debit == "" || $scope.block3.gldd_doc_amount_debit == undefined || $scope.block3.gldd_doc_amount_debit == '0.00') {
                        $scope.block3.gldd_doc_amount_debit = '0.00';
                    }
                    if ($scope.block3.gldd_doc_amount_credit == "" || $scope.block3.gldd_doc_amount_credit == undefined || $scope.block3.gldd_doc_amount_credit == '0.00') {
                        $scope.block3.gldd_doc_amount_credit = '0.00';
                    }

                    if (parseFloat($scope.block3.gldd_doc_amount_debit) <= 0 && parseFloat($scope.block3.gldd_doc_amount_credit) <= 0) {
                        swal({ text: "Please Enter Debit/Credit Amount", width: 380 });
                        return;
                    }
                    var terminal = document.getElementById("cmb_acc_Code3");
                    $scope.selectedText = terminal.options[terminal.selectedIndex].text;
                    var found = false;

                    for (var j = 0; j < $scope.prdocdata.length; j++) {
                        if ($scope.prdocdata[j].gldd_acct_code == $scope.block3.gldd_acct_code && $scope.prdocdata[j].gldd_doc_narr == $scope.block3.gldd_doc_narr) {
                            found = true;
                            break;
                        }
                    }
                    if (found == true) {
                        swal({ text: $scope.block3.gldd_acct_name + " A/c Already Added", width: 380 });
                    }
                    else {
                        //code sgr
                        var costcenter1 = document.getElementById("costCenter")
                        var costcentername = costcenter1.options[costcenter1.selectedIndex].text;
                        $scope.center = $scope.center || ($scope.block2.glco_cost_centre_code != '')

                        //if ($scope.block3.gldd_include_vat == false) {
                        //    if ($scope.block3.gldd_doc_amount_debit != 0) {
                        //        $scope.checkevaluesdebit();
                        //    }
                        //    else {
                        //        $scope.checkevaluescredit();
                        //    }
                        //}
                        if (comp_vat_status == 'Y' && $scope.block3.gldd_include_vat == true && $scope.block3.gldd_vat_enable == true) {

                            if ($scope.block3.gldd_doc_amount_debit != 0) {
                                var vat_deb_amt = parseFloat($scope.block3.gldd_doc_amount_debit);
                                $scope.block3.gldd_doc_vat_amt = parseFloat(vat_deb_amt * parseFloat(vat_percent) / (100 + parseFloat(vat_percent))).toFixed(2);
                                $scope.block3.gldd_doc_amount_debit = parseFloat(vat_deb_amt) - parseFloat(vat_deb_amt * parseFloat(vat_percent) / (100 + parseFloat(vat_percent))).toFixed(2);
                                //$scope.gldd_doc_vat_amt = 0;
                                //$scope.block3.gldd_include_vat = true;
                            }
                            else {
                                var vat_cre_amt = parseFloat($scope.block3.gldd_doc_amount_credit);
                                $scope.block3.gldd_doc_vat_amt = parseFloat(vat_cre_amt * parseFloat(vat_percent) / (100 + parseFloat(vat_percent))).toFixed(2);
                                $scope.block3.gldd_doc_amount_credit = parseFloat(vat_cre_amt) - parseFloat(vat_cre_amt * parseFloat(vat_percent) / (100 + parseFloat(vat_percent))).toFixed(2);
                                //$scope.block3.gldd_include_vat = true;
                            }

                        }

                        $scope.prdocdata.push({
                            'gldd_comp_code': comp_code,
                            //'gldd_doc_code': $scope.edt.gltd_doc_code,
                            'gldd_ledger_code': ledcode,
                            'gldd_acct_code': $scope.block3.gldd_acct_code,
                            'gldd_acct_name': $scope.block3.gldd_acct_name,
                            'gldd_dept_code': $scope.block3.gldd_dept_code,
                            'gldd_party_ref_no': $scope.block3.gldd_party_ref_no,
                            'gldd_doc_narr': $scope.block3.gldd_doc_narr,

                            'gldd_doc_amount_debit': $scope.block3.gldd_doc_amount_debit,
                            'gldd_doc_amount_credit': $scope.block3.gldd_doc_amount_credit,
                            'glco_cost_centre_code': $scope.block2.glco_cost_centre_code,
                            'coce_cost_centre_name': costcentername,
                        });

                        if (comp_vat_status == 'Y' && $scope.block3.gldd_vat_enable == true) {
                            if (vat_ac_name == '')
                                for (var i = 0; i < $scope.getAllAccNos.length; i++) {
                                    if ($scope.getAllAccNos[i].gldd_acct_code == input_vat_acct) {
                                        vat_ac_name = $scope.getAllAccNos[i].gldd_acct_name;
                                    }
                                }

                            $scope.prdocdata.push({

                                'gldd_comp_code': comp_code,
                                //'gldd_doc_code': $scope.edt.gltd_doc_code,
                                'gldd_ledger_code': ledcode,
                                'gldd_acct_code': input_vat_acct,
                                'gldd_acct_name': vat_ac_name,
                                'gldd_dept_code': input_vat_acct.substr(1, 2),
                                'gldd_party_ref_no': '',
                                'gldd_doc_narr': 'Vat amount' + ' - ' + $scope.block3.gldd_acct_name,

                                'gldd_doc_amount_debit': ($scope.block3.gldd_doc_amount_debit != 0) ? $scope.block3.gldd_doc_vat_amt : 0,
                                'gldd_doc_amount_credit': ($scope.block3.gldd_doc_amount_credit != 0) ? $scope.block3.gldd_doc_vat_amt : 0,

                                'glco_cost_centre_code': $scope.block2.glco_cost_centre_code,
                                'coce_cost_centre_name': costcentername,
                            });
                        }

                        $scope.totalDebit = 0;
                        $scope.totalcredit = 0;

                        for (var i = 0; i < $scope.prdocdata.length; i++) {

                            $scope.totalDebit = parseFloat(parseFloat($scope.totalDebit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_debit)).toFixed(3);

                            $scope.totalcredit = parseFloat(parseFloat($scope.totalcredit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_credit)).toFixed(3);
                        }
                        //$scope.btnDelete = true;


                        //$scope.edt = "";
                        $scope.edt1 = "";
                        $scope.edt2 = "";
                        $scope.block3.gldd_doc_vat_amt = 0;
                        $("#cmb_acc_Code3").select2("val", "");
                        // $scope.edt.slma_acno = '';
                        $scope.myForm.$setPristine();
                        $scope.myForm.$setUntouched();
                        // Bankdetails = [];
                        $scope.dcode = false;

                        $scope.cancel();
                    }
                }
                else {
                    swal({ text: "  please select Account Code ", width: 380 });

                }
                var acconcode = $scope.getAllAccNos.gldd_acct_code;
                $scope.GetAllGLAcc(acconcode, comp_code);
            };


            $scope.Delete_Grid = function (item, $event, index, str) {


                $scope.totalDebit = parseFloat(parseFloat($scope.totalDebit) - parseFloat(str.gldd_doc_amount_debit)).toFixed(3);
                $scope.totalcredit = parseFloat(parseFloat($scope.totalcredit) - parseFloat(str.gldd_doc_amount_credit)).toFixed(3);

                item.splice(index, 1);


            }
            $scope.update_grid = function (obj) {
                $("#cmb_acc_Code3").select2();
                $scope.vis.main_acct_grid = true;

                $scope.tempobj = obj;
                $scope.vis.updatebtn = true;
                $scope.vis.add_grid = false;
                $scope.vis.cmb_acct = false;
                $scope.vis.txt_acct = true;
                $scope.ldgcd = true;


                $scope.block3 = JSON.parse(JSON.stringify(obj));


                $scope.getSLAccNo();

            }
            $scope.getDepartcode = function (str) {

                //Code sgr

                $http.get(ENV.apiUrl + "api/BankPayment/getCostCenter?aacno=" + str + "&comp_cd=" + comp_code + "&fyear=" + finance_year).then(function (cost_center) {

                    $scope.cost_center = cost_center.data;
                    if ($scope.cost_center.length > 0) {
                        $scope.chkcost = true;
                    }
                    else {
                        $scope.chkcost = false;
                        $scope.temp.cost_chk = false;
                        $scope.cost_combo = false;
                    }
                });


                for (var i = 0; i < $scope.getAllAccNos.length; i++) {
                    if ($scope.getAllAccNos[i].gldd_acct_code == $scope.block3.gldd_acct_code) {

                        $scope.block3.gldd_dept_code = $scope.getAllAccNos[i].gldd_dept_code;
                        $scope.block3.gldd_party_ref_no = $scope.getAllAccNos[i].gldd_party_ref_no;
                        $scope.block3.codp_dept_name = $scope.getAllAccNos[i].gldd_dept_name;
                        $scope.block3.gldd_acct_name = $scope.getAllAccNos[i].gldd_acct_name;

                    }
                }



            }

            //Authorize & verify Single Voucher
            $scope.JVerify_click = function () {
                jver = true;
                jvaut = false;
                $scope.Insert_temp_docs();
            }

            $scope.JVAuthorize_click = function () {
                jvaut = true;
                jver = false;
                $scope.Insert_temp_docs();
            }

            //Insert  Temp Doc
            $scope.Insert_temp_docs = function () {

                if ($scope.temp_doc.gltd_doc_code != "" && $scope.temp_doc.gltd_doc_code != undefined) {

                    if ($scope.temp_doc.gltd_post_date == "" || $scope.temp_doc.gltd_post_date == undefined) {
                        swal({ text: "  please select Post Date ", width: 380 });
                        return;
                    }
                    if ($scope.prdocdata.length <= 0) {
                        swal({ text: "  Please Add Transaction Details ", width: 380 });
                        return;
                    }
                    if ($scope.totalDebit != $scope.totalcredit) {
                        swal({ text: "Debit Amount: " + $scope.totalDebit + "  Is Not Equal To Credit Amount: " + $scope.totalcredit, width: 380 });
                        return;
                    }
                    if (($scope.temp_doc.chk_recur == true) && ($scope.temp_doc.gltd_recur_period == "0" || $scope.temp_doc.gltd_recur_period == "" || $scope.temp_doc.gltd_recur_period == undefined)) {
                        swal({ text: "Please Enter Recurring Period", width: 380 });
                        return;
                    }
                    else {
                        var datasend = [];

                        if (jvaut == true) {
                            status = "Aut";
                        }
                        else if (jver == true) {
                            status = "Verify";
                        }
                        else
                            status = "Save";

                        if ($scope.temp == undefined) {
                            $scope.temp = "";
                        }
                        if ($scope.temp_doc.chk_recur == true) {
                            $scope.chk_rcur = "R";
                        }
                        var data = {
                            gltd_comp_code: comp_code,
                            gltd_prepare_user: user,
                            gltd_doc_code: $scope.temp_doc.gltd_doc_code,
                            gltd_doc_date: $scope.temp_doc.gltd_doc_date,
                            gltd_cur_status: status,
                            gltd_post_date: $scope.temp_doc.gltd_post_date,
                            gltd_doc_narr: $scope.temp_doc.gltd_doc_narr,
                            gltd_remarks: $scope.temp_doc.gltd_remarks,
                            gltd_prepare_date: $scope.temp_doc.gltd_doc_date,
                            gltd_final_doc_no: "0",
                            gltd_verify_user: (jver == true) ? user : null,
                            gltd_verify_date: (jver == true) ? $scope.temp_doc.gltd_doc_date : null,
                            gltd_authorize_user: (jvaut == true) ? user : null,
                            gltd_authorize_date: (jvaut == true) ? $scope.temp_doc.gltd_doc_date : null,
                            gltd_paid_to: $scope.block1.gltd_paid_to,
                            gltd_cheque_no: $scope.block1.gltd_cheque_No,
                            gltd_batch_source: $scope.chk_rcur
                        }
                        datasend.push(data);

                        $http.post(ENV.apiUrl + "api/BankPayment/Insert_Fins_temp_docs", datasend).then(function (msg) {
                            $scope.prvno = msg.data;

                            if ($scope.prvno != "" && $scope.prvno != null) {
                                $scope.Insert_Fins_temp_doc_details();
                            }
                            else {
                                swal({ title: "Alert", text: "Record Not inserted. " + $scope.prvno, width: 300, height: 200 });
                            }
                        });
                    }
                }
                else {
                    swal({ text: "  please select Doc Code ", width: 380 });
                }
            }

            //Insert  Temp Doc Details

            $scope.Insert_Fins_temp_doc_details = function () {

                var dataSend = [];
                var j = 1;

                for (var i = 0; i < $scope.prdocdata.length; i++) {
                    var data = {
                        gldd_doc_narr: $scope.prdocdata[i].gldd_doc_narr,
                        gldd_dept_code: $scope.prdocdata[i].gldd_dept_code,
                        gldd_ledger_code: $scope.prdocdata[i].gldd_ledger_code,
                        gldd_acct_code: $scope.prdocdata[i].gldd_acct_code,
                        gldd_party_ref_no: $scope.prdocdata[i].gldd_party_ref_no,
                        gldd_party_ref_date: $scope.prdocdata[i].ptyref_date,
                        gldd_cost_center_code: $scope.prdocdata[i].glco_cost_centre_code,
                        gldd_doc_amount_debit: $scope.prdocdata[i].gldd_doc_amount_debit,
                        gldd_doc_amount_credit: $scope.prdocdata[i].gldd_doc_amount_credit,
                        gldd_comp_code: $scope.prdocdata[i].gldd_comp_code,
                        gldd_doc_code: $scope.temp_doc.gltd_doc_code,
                        gldd_prov_doc_no: $scope.prvno,
                        gldd_final_doc_no: "0",
                        gldd_line_no: j,

                    }
                    j++;
                    dataSend.push(data);

                }


                $http.post(ENV.apiUrl + "api/BankPayment/Insert_JV_Fins_temp_doc_details", dataSend).then(function (msg) {

                    $scope.msg1 = msg.data;
                    if ($scope.prvno != "" && $scope.msg1 == true) {
                        if ($scope.vis.update == true) {
                            swal({ title: "Alert", text: "Document Updated Successfully\nProvision No.:" + $scope.temp_doc.gltd_doc_code + "-" + $scope.prvno, width: 300, height: 200 });
                            //$scope.checkprintbtn = true;
                        }
                        else {
                            if (($scope.temp_doc.chk_recur == true) && ($scope.temp_doc.gltd_doc_code != undefined) && ($scope.prvno != undefined || $scope.prvno != "")) {


                                $http.post(ENV.apiUrl + "api/JVCreation/Recurring_Fins_temp_doc_details?comp_cd=" + comp_code + "&doccd=" + $scope.temp_doc.gltd_doc_code + "&prv_no=" + $scope.prvno + "&recur_period=" + $scope.temp_doc.gltd_recur_period + "&recur_days=" + $scope.temp_doc.gltd_recur_period_days).then(function (msg) {
                                    $scope.msg1 = msg.data;

                                    if ($scope.msg1 == true) {

                                        if (jvaut == true) {
                                            $http.post(ENV.apiUrl + "api/BankPayment/Authorize_Posting?comp_code=" + comp_code + "&doc_code=" + $scope.temp_doc.gltd_doc_code + "&prv_no=" + $scope.prvno + "&auth_user=" + user + "&auth_date=" + $scope.temp_doc.gltd_doc_date).then(function (Auth) {

                                                $scope.Docno = Auth.data;
                                                if ($scope.Docno != "") {
                                                    swal({ title: "Alert", text: "Voucher Posted Successfully\nFinal Doc No.: " + $scope.Docno, width: 300, height: 200 });
                                                    $scope.report_show = false;
                                                    $('#vouchermodal').modal('show');

                                                    var data = {
                                                        location: $scope.final_doc_url,
                                                        parameter: { comp_detail: comp_code, doc: $scope.temp_doc.gltd_doc_code + $scope.Docno },
                                                        state: 'main.RepVou'
                                                    }

                                                    window.localStorage["ReportDetails"] = JSON.stringify(data)
                                                    //$state.go('main.ReportCardParameter')
                                                        try {
                                                            $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                                                            $scope.location = $scope.rpt.location;
                                                            $scope.parameter = $scope.rpt.parameter;
                                                            $scope.state = $scope.rpt.state;
                                                        }
                                                        catch (ex) {
                                                        }
                                                    var s;

                                                    //if ($http.defaults.headers.common['schoolId'] == 'dpsmis')
                                                    //    s = "SimsReports." + $scope.location + ",SimsReports";


                                                    //else
                                                    //    s = "SimsReports." + $scope.location + ",SimsReports" + $http.defaults.headers.common['schoolId'];


                                                    //  window.localStorage["Finn_comp"] = JSON.stringify(data)

                                                    s = "SimsReports." + $scope.location + ",SimsReports";

                                                    var url = window.location.href;
                                                    var domain = url.substring(0, url.indexOf(':'))
                                                    if ($http.defaults.headers.common['schoolId'] == 'sms') {
                                                        if (domain == 'https')
                                                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                                                        else
                                                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                                                    }
                                                    else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc' || $http.defaults.headers.common['schoolId'] == 'christ') {
                                                        if (domain == 'https')
                                                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                                                        else
                                                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                                                    }

                                                    else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                                                        if (domain == 'https')
                                                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                                                        else
                                                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                                                    }

                                                    else if ($http.defaults.headers.common['schoolId'] == 'imert') {
                                                        if (domain == 'https')
                                                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                                                        else
                                                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                                                    }
                                                    else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                                                        if (domain == 'https')
                                                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                                                        else
                                                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                                                    }

                                                    else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                                                        if (domain == 'https')
                                                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                                                        else
                                                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                                                    }

                                                    else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                                                        if (domain == 'https')
                                                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                                                        else
                                                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                                                    }
                                                    else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                                                        if (domain == 'https')
                                                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                                                        else
                                                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                                                    }

                                                    else {
                                                        if (domain == 'https')
                                                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                                                        else
                                                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                                                    }
                                                    // console.log(service_url);


                                                    $scope.parameters = {}

                                                    $("#reportViewer1")
                                                                   .telerik_ReportViewer({
                                                                       //serviceUrl: ENV.apiUrl + "api/reports/",
                                                                       serviceUrl: service_url,

                                                                       viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                                                       scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                                                       // Zoom in and out the report using the scale
                                                                       // 1.0 is equal to 100%, i.e. the original size of the report
                                                                       scale: 1.0,
                                                                       ready: function () {
                                                                           //this.refreshReport();
                                                                       }

                                                                   });





                                                    var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                                                    reportViewer.reportSource({
                                                        report: s,
                                                        parameters: $scope.parameter,
                                                    });

                                                    //var rv = $("#reportViewer1").data("telerik_ReportViewer");
                                                    //rv.commands.print.exec();

                                                    setInterval(function () {

                                                        $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                                                        $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                                                        //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                                                        //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                                                        //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                                                        //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                                                        //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                                                        //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });

                                                    }, 1000);


                                                    $timeout(function () {
                                                        $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                                                        $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                                                        //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                                                        //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                                                        //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                                                        //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                                                        //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                                                        //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });


                                                    }, 100)

                                                }
                                                else {
                                                    swal({ title: "Alert", text: "Posting Failed...!\nDocument Created Successfully\nProvision No.: " + $scope.temp_doc.gltd_doc_code + "-" + $scope.prvno, width: 300, height: 200 });
                                                }
                                            });
                                        }
                                        else if (jver == true) {
                                            swal({ title: "Alert", text: "Document Verified Successfully\nProvision No.:" + $scope.temp_doc.gltd_doc_code + "-" + $scope.prvno, width: 300, height: 200 });
                                            $('#myModal4').modal('hide')
                                            $scope.cancel();
                                            return;
                                        }
                                        else {
                                            swal({ title: "Alert", text: "Document Created Successfully\nProvision No.:" + $scope.temp_doc.gltd_doc_code + "-" + $scope.prvno, width: 300, height: 200 });
                                            $('#myModal4').modal('hide')
                                            $scope.cancel();
                                            return;

                                        }
                                    }
                                    else {
                                        swal({ title: "Alert", text: "All Document Not Created Successfully\nProvision No.:" + $scope.temp_doc.gltd_doc_code + "-" + $scope.prvno, width: 300, height: 200 });
                                    }
                                });
                            }
                            else {
                                if (jvaut == true) {
                                    $http.post(ENV.apiUrl + "api/BankPayment/Authorize_Posting?comp_code=" + comp_code + "&doc_code=" + $scope.temp_doc.gltd_doc_code + "&prv_no=" + $scope.prvno + "&auth_user=" + user + "&auth_date=" + $scope.temp_doc.gltd_doc_date).then(function (Auth) {

                                        $scope.Docno = Auth.data;
                                        if ($scope.Docno != "") {
                                            swal({ title: "Alert", text: "Voucher Posted Successfully\nFinal Doc No.: " + $scope.Docno, width: 300, height: 200 });
                                            $scope.report_show = false;
                                            $('#vouchermodal').modal('show');
                                            var data = {
                                                location: $scope.final_doc_url,
                                                parameter: { comp_detail: comp_code, doc: $scope.temp_doc.gltd_doc_code + $scope.Docno },
                                                state: 'main.RepVou'
                                            }

                                            window.localStorage["ReportDetails"] = JSON.stringify(data)
                                            // $state.go('main.ReportCardParameter')
                                                try {
                                                    $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                                                    $scope.location = $scope.rpt.location;
                                                    $scope.parameter = $scope.rpt.parameter;
                                                    $scope.state = $scope.rpt.state;
                                                }
                                                catch (ex) {
                                                }
                                            var s;

                                            //if ($http.defaults.headers.common['schoolId'] == 'dpsmis')
                                            //    s = "SimsReports." + $scope.location + ",SimsReports";


                                            //else
                                            //    s = "SimsReports." + $scope.location + ",SimsReports" + $http.defaults.headers.common['schoolId'];


                                            //  window.localStorage["Finn_comp"] = JSON.stringify(data)

                                            s = "SimsReports." + $scope.location + ",SimsReports";

                                            var url = window.location.href;
                                            var domain = url.substring(0, url.indexOf(':'))
                                            if ($http.defaults.headers.common['schoolId'] == 'sms') {
                                                if (domain == 'https')
                                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                                                else
                                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                                            }
                                            else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc' || $http.defaults.headers.common['schoolId'] == 'christ') {
                                                if (domain == 'https')
                                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                                                else
                                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                                            }

                                            else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                                                if (domain == 'https')
                                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                                                else
                                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                                            }

                                            else if ($http.defaults.headers.common['schoolId'] == 'imert') {
                                                if (domain == 'https')
                                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                                                else
                                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                                            }
                                            else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                                                if (domain == 'https')
                                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                                                else
                                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                                            }

                                            else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                                                if (domain == 'https')
                                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                                                else
                                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                                            }

                                            else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                                                if (domain == 'https')
                                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                                                else
                                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                                            }
                                            else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                                                if (domain == 'https')
                                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                                                else
                                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                                            }

                                            else {
                                                if (domain == 'https')
                                                    var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                                                else
                                                    var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                                            }
                                            // console.log(service_url);


                                            $scope.parameters = {}

                                            $("#reportViewer1")
                                                           .telerik_ReportViewer({
                                                               //serviceUrl: ENV.apiUrl + "api/reports/",
                                                               serviceUrl: service_url,

                                                               viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                                               scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                                               // Zoom in and out the report using the scale
                                                               // 1.0 is equal to 100%, i.e. the original size of the report
                                                               scale: 1.0,
                                                               ready: function () {
                                                                   //this.refreshReport();
                                                               }

                                                           });





                                            var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                                            reportViewer.reportSource({
                                                report: s,
                                                parameters: $scope.parameter,
                                            });

                                            //var rv = $("#reportViewer1").data("telerik_ReportViewer");
                                            //rv.commands.print.exec();

                                            setInterval(function () {

                                                $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                                                $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                                                //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                                                //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                                                //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                                                //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                                                //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                                                //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });

                                            }, 1000);


                                            $timeout(function () {
                                                $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                                                $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                                                //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                                                //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                                                //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                                                //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                                                //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                                                //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });


                                            }, 100)

                                        }
                                        else {
                                            swal({ title: "Alert", text: "Posting Failed...!\nDocument Created Successfully\nProvision No.: " + $scope.temp_doc.gltd_doc_code + "-" + $scope.prvno, width: 300, height: 200 });
                                        }
                                    });
                                }
                                else if (jver == true) {
                                    swal({ title: "Alert", text: "Document Verified Successfully\nProvision No.:" + $scope.temp_doc.gltd_doc_code + "-" + $scope.prvno, width: 300, height: 200 });
                                    $('#myModal4').modal('hide')
                                    $scope.cancel();
                                   
                                    return;
                                }
                                else {
                                    swal({ title: "Alert", text: "Document Created Successfully\nProvision No.:" + $scope.temp_doc.gltd_doc_code + "-" + $scope.prvno, width: 300, height: 200 });
                                    $('#myModal4').modal('hide')
                                    $scope.cancel();
                                    return;
                                }
                            }


                        }
                        $('#myModal4').modal('hide')
                        if (jvaut == true) {


                        }
                        else {
                            $scope.vis.ok = false;
                            $scope.checkprintbtn = true;
                            $scope.vis.update = false;
                            $scope.vis.SendToVerify = true;

                            $scope.vis.autoriz = false;
                            $scope.vis.very = false;
                        }
                    }
                    else {

                        swal({ title: "Alert", text: "Record Not inserted in doc details. ", width: 300, height: 200 });
                    }

                });

            }

            //send to verify

            $scope.Verify_data = function () {
                var dataSend = [];
                var vcnt = 0, rcnt = 0;



                for (var i = 0; i < $scope.verify_data.length; i++) {

                    if ($scope.verify_data[i].gltd_verify == true || $scope.verify_data[i].gltd_revert == true) {

                        if ($scope.verify_data[i].gltd_verify == true) {
                            vcnt = vcnt + 1;
                        }
                        else if ($scope.verify_data[i].gltd_revert == true) {
                            rcnt = rcnt + 1;
                        }

                        var data = {
                            gltd_comp_code: $scope.verify_data[i].gltd_comp_code,
                            gltd_doc_code: $scope.verify_data[i].gltd_doc_code,
                            gltd_prov_doc_no: $scope.verify_data[i].gltd_prov_doc_no,
                            gltd_verify: $scope.verify_data[i].gltd_verify,
                            gltd_revert: $scope.verify_data[i].gltd_revert,
                            gltd_verify_user: user,
                            gltd_verify_date: dd + '-' + mm + '-' + yyyy

                        }
                        dataSend.push(data);
                    }

                }


                $http.post(ENV.apiUrl + "api/JVCreation/UpdateFinn140_temp_docs", dataSend).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true && vcnt > 0) {
                        swal({ title: "Alert", text: "Verified Successfully", width: 300, height: 200 });
                        // $scope.checkprintbtn = true;
                        $scope.Verify();

                    }
                    else if ($scope.msg1 == true && rcnt > 0) {
                        swal({ title: "Alert", text: "Reverted Successfully", width: 300, height: 200 });
                        // $scope.checkprintbtn = true;
                        $scope.Verify();

                    }
                    else {
                        swal({ title: "Alert", text: "Documents Not Verified. ", width: 300, height: 200 });
                    }

                });
            }

            $scope.update_grid_data = function () {

                $scope.temp_obj = $scope.block3;

                if ($scope.block3.gldd_doc_amount_debit == "" || $scope.block3.gldd_doc_amount_debit == undefined || $scope.block3.gldd_doc_amount_debit == '0.00') {
                    $scope.block3.gldd_doc_amount_debit = '0.00';
                }
                if ($scope.block3.gldd_doc_amount_credit == "" || $scope.block3.gldd_doc_amount_credit == undefined || $scope.block3.gldd_doc_amount_credit == '0.00') {
                    $scope.block3.gldd_doc_amount_credit = '0.00';
                }

                if (parseFloat($scope.block3.gldd_doc_amount_debit) <= 0 && parseFloat($scope.block3.gldd_doc_amount_credit) <= 0) {
                    swal({ text: "Please Enter Debit/Credit Amount", width: 380 });
                    return;
                }

                $scope.totalDebit = 0;
                $scope.totalcredit = 0;
                for (var i = 0; i < $scope.prdocdata.length; i++) {

                    if ($scope.prdocdata[i].$$hashKey == $scope.tempobj.$$hashKey) {
                        $scope.prdocdata[i].gldd_doc_amount_credit = $scope.block3.gldd_doc_amount_credit
                        $scope.prdocdata[i].gldd_doc_amount_debit = $scope.block3.gldd_doc_amount_debit;
                        $scope.prdocdata[i].gldd_doc_narr = $scope.block3.gldd_doc_narr;
                    }
                    $scope.totalDebit = parseFloat(parseFloat($scope.totalDebit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_debit)).toFixed(3);
                    $scope.totalcredit = parseFloat(parseFloat($scope.totalcredit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_credit)).toFixed(3);
                }
                $scope.cancel();
            }
            $scope.cancel = function () {

                $scope.vis.updatebtn = false;
                $scope.vis.add_grid = true;
                $scope.cramount = false;
                $scope.dramount = false;
                $scope.vis.cmb_acct = true;
                $scope.vis.txt_acct = false;
                $scope.ldgcd = false;
                $scope.chkcost = false;
                $scope.cost_combo = false;
                $scope.block3 = [];
                $scope.block3.gldd_include_vat = false;
                $scope.block3.gldd_vat_enable = false;
                $("#cmb_acc_Code3").select2("val", "");

                if ($scope.cmb_doc_cd.gldc_doc_code_type == "CN") {
                    $scope.vis.main_acct_grid = false;

                }
            }
            $scope.closemodal = function ()
            {
                $("#cmb_acc_Code3").select2('val', '');
                $scope.cancel();

                //$scope.vis.updatebtn = false;
                //$scope.vis.add_grid = true;
                //$scope.vis.ok = true;
                //$scope.vis.autoriz = true;
                //$scope.vis.very = true;


                if ($scope.users.length != 0) {

                    if ($scope.users[0] == user && $scope.users[1] == user) {
                        $scope.preparebtn = false;
                        $scope.verifybtn = false;
                        $scope.authorizebtn = false;

                        $scope.vis.autoriz = true;
                        $scope.vis.very = true;
                    }
                    else if ($scope.users[0] == user && ($scope.users[1] == "" || $scope.users[1] != user)) {
                        $scope.verifybtn = false;
                        $scope.preparebtn = false;
                        $scope.authorizebtn = true;

                        $scope.vis.autoriz = false;
                        $scope.vis.very = true;
                    }
                    else {
                        $scope.preparebtn = false;
                        $scope.verifybtn = true;
                        $scope.authorizebtn = true;

                        $scope.vis.autoriz = false;
                        $scope.vis.very = false;
                    }
                }
            }
            $scope.check_post_date = function () {
                if ($scope.temp_doc.gltd_post_date != undefined || $scope.temp_doc.gltd_post_date != '') {

                    $http.get(ENV.apiUrl + "api/JVCreation/Get_Check_Postdate?post_date=" + $scope.temp_doc.gltd_post_date + "&comp_cd=" + comp_code + "&fyear=" + finance_year).then(function (pronum) {
                        $scope.msg = pronum.data;

                        if ($scope.msg != '') {
                            swal({ title: "Alert", text: $scope.msg, width: 300, height: 200 });

                            $scope.vis.ok = false;
                            $scope.vis.autoriz = false;
                            $scope.vis.very = false;
                        }
                        else {
                            if ($scope.users.length != 0) {

                                if ($scope.users[0] == user && $scope.users[1] == user) {

                                    $scope.vis.ok = true;
                                    $scope.vis.autoriz = true;
                                    $scope.vis.very = true;
                                }
                                else if ($scope.users[0] == user && ($scope.users[1] == "" || $scope.users[1] != user)) {

                                    $scope.vis.ok = true;
                                    $scope.vis.autoriz = false;
                                    $scope.vis.very = true;
                                }
                                else {
                                    $scope.vis.ok = true;
                                    $scope.vis.autoriz = false;
                                    $scope.vis.very = false;
                                }
                            }
                        }

                    });

                }

            }

        }]
        )
})();
