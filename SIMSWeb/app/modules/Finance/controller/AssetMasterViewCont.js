﻿
(function () {
    'use strict';

    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('AssetMasterViewCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var username1 = $rootScope.globals.currentUser.username;
            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;

            console.log($scope.finnDetail)
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
            $scope.edt = {};

            //$http.get(ENV.apiUrl + "api/assetmaster/GetFins_AssetMaster").then(function (GetFins_AssetMaster) {
            //    $scope.FinsAssetMaster = GetFins_AssetMaster.data;

            //    if ($scope.FinsAssetMaster.length > 0) {

            //        $scope.totalItems = $scope.FinsAssetMaster.length;
            //        $scope.todos = $scope.FinsAssetMaster;
            //        $scope.makeTodos();

            //    }
            //    else {

            //        swal({ text: 'Record Not Found', width: 380 });
            //    }
            //});


            $scope.Search = function ()
            {
                $http.get(ENV.apiUrl + "api/common/AssetMaster/GetFins_AssetMaster_view?department=" + $scope.edt.gam_dept_code + "&asset_tyep=" + $scope.edt.gam_asst_type + "&desc=" + $scope.edt.gam_desc_1 + "&comp_cd=" + comp_code + "&fyear=" + finance_year).then(function (GetFins_AssetMaster) {
                    $scope.FinsAssetMaster = GetFins_AssetMaster.data;

                    if ($scope.FinsAssetMaster.length > 0) {

                        $scope.totalItems = $scope.FinsAssetMaster.length;
                        $scope.todos = $scope.FinsAssetMaster;
                        $scope.makeTodos();

                    }
                    else {

                        swal({ text: 'Data Not Found', width: 380, imageUrl: "assets/img/check.png" });
                    }
                });
            }

            

            $http.get(ENV.apiUrl + "api/common/AssetMaster/GetAllDepartmentName?comp_code=" + comp_code + "&fins_year=" + finance_year).then(function (res) {
                $scope.dept_data = res.data;
            });


            $http.get(ENV.apiUrl + "api/common/AssetMaster/GetAssetType?comp_code=" + comp_code).then(function (res) {
                $scope.assetType_data = res.data;
            });



            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                }
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.CheckMultiple();
                }
            };
            
            $scope.size = function (str) {
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;  $scope.makeTodos();

                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.FinsAssetMaster;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();
            }
            $scope.flag = true;
            var dom;
            $scope.expand = function (info, $event) {
                if ($scope.flag == true) {
                    $(dom).remove();
                    info.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                    "<table class='inner-table table table-bordered table-hover table-condensed' cellpadding='5' cellspacing='0'>" +
                    "<tbody>" +
                     "<tr style='background-color: #edefef'><td class='semi-bold'>"+"MTH.DPER"+"</td><td class='semi-bold'>"+"YRLY.DEPR"+"</td><td class='semi-bold'>"+"VAL.ASST.CUM"+"</td> <td class='semi-bold'>" + "VAL.ASST.MTH +" + "</td> <td class='semi-bold'>" + "VAL.ASST.YR.+" + " </td><td class='semi-bold'>" + "SALE VALUE" + "</td><td class='semi-bold'>" + "REPLACEMENT COST" + "</td><td class='semi-bold'>" + "ACCOUNT CODE" + " </td>" +
                      "<tr><td>" + (info.gam_mth_deprn) + "</td> <td>" + (info.gam_ytd_deprn) + " </td><td>" + (info.gam_val_add_cum) + "</td><td>" + (info.gam_val_add_mtd) + "</td><td>" + (info.gam_val_add_ytd) + "</td><td>" + (info.gam_sale_value) + "</td><td>" + (info.gam_repl_cost) + "</td><td>" + (info.gam_acct_code) + "</td></tr>" +

                      "<tr style='background-color: #edefef'><td class='semi-bold'>" + "ORDER NO" + "</td><td class='semi-bold'>" + "INV.NO." + "</td><td class='semi-bold'>" + "RECEIPT DATE" + "</td> <td class='semi-bold'>" + "SALE DATE" + "</td> <td class='semi-bold'>" + "ENTRY DATE" + " </td><td class='semi-bold'>" + "AMEND DATE" + "</td><td class='semi-bold'>" + "NEW SECTION" + "</td><td class='semi-bold'>" + "PAGE NO" + " </td>" +
                      "<tr><td>" + (info.gam_order_no) + "</td> <td>" + (info.gam_invoice_no) + " </td><td>" + (info.gam_receipt_date) + "</td><td>" + (info.gam_sale_date) + "</td><td>" + (info.gam_entry_date) + "</td><td>" + (info.gam_amend_date) + "</td><td>" + (info.gam_new_sec) + "</td><td>" + (info.gam_page_no) + "</td></tr>" +

                       "<tr style='background-color: #edefef'><td class='semi-bold'>" + "LINE NO" + "</td><td class='semi-bold'>" + "DEPR.%" + "</td><td class='semi-bold'>" + "USED ON ITEM" + "</td> <td class='semi-bold'>" + "LOCATION" + "</td> <td class='semi-bold'>" + "DESCRIPTION 2" + " </td><td class='semi-bold'>" + "SUPPLIER NAME" + "</td><td class='semi-bold'>&nbsp;</td><td class='semi-bold'>&nbsp;</td>" +
                      "<tr><td>" + (info.gam_line_no) + "</td> <td>" + (info.gam_deprn_percent) + " </td><td>" + (info.gam_used_on_item) + "</td><td>" + (info.gam_location_name) + "</td><td>" + (info.gam_desc_2) + "</td><td>" + (info.gam_supl_name) + "</td><td>&nbsp;</td><td>&nbsp;</td></tr>" +
                    " </table></td></tr>")
                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }

            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.FinsAssetMaster, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.FinsAssetMaster;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.gam_supl_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.gam_asst_type_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.gam_quantity == toSearch || item.gam_invoice_amount == toSearch) ? true : false;
            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);
        }])
})();