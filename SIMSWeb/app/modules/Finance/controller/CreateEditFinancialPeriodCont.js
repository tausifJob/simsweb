﻿(function () {
    'use strict';
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('CreateEditFinancialPeriodCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
     
            $scope.display = false;
            //$scope.pagesize = "5";
            //$scope.pageindex = "1";
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.grid = true;
            $scope.FinPeriod_data = [];
            $scope.edit_data = false;
            $scope.comp_edit = false;
            var comn_code;
            var data1 = [];
            // var date3;


            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetFinancialPeriod").then(function (res) {
                $scope.FinPeriod_data = res.data;
                $scope.totalItems = $scope.FinPeriod_data.length;
                $scope.todos = $scope.FinPeriod_data;
                $scope.makeTodos();
                $scope.grid = true;
            });

            $scope.makeTodos = function ()
            {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.New = function ()
            {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
               // $scope.edt = "";
                $scope.comp_edit = false;
                $scope.edit_data = true;
                $scope.edt = {};
                $scope.edt.status = true;
                var dt=new Date();
                $scope.edt.prd_st_dt=('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                $scope.edt.prd_end_dt = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                if ($rootScope.globals.currentUser.username != null)
                {
                    $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
                        $scope.comp_data = res.data;
                        if ($scope.comp_data.length > 0) {
                            $scope.edt.dept_comp_code = $scope.comp_data[0].dept_comp_code;
                        }
                    });
                }
                else
                {
                    $rootScope.globals.currentUser.username = null;
                }

            }


            $scope.edit = function (str)
            {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;
                $scope.edt = str;
                $scope.comp_edit = true;
                $scope.edit_data = true;
                $scope.edt.prd_st_dt = convertdate($scope.edt.prd_st_dt);
                $scope.edt.prd_end_dt = convertdate($scope.edt.prd_end_dt);

                if ($rootScope.globals.currentUser.username != null)
                {
                    $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {
                        $scope.comp_data = res.data;
                    });
                }
                else
                {
                    $rootScope.globals.currentUser.username = null;
                }
            }
            
            function convertdate(dt)
            {
                if (dt != null)
                {
                    //var d1 = new Date(dt);
                    //var month = d1.getMonth() + 1;
                    //var day = d1.getDate();
                    //if (month < 10)
                    //    month = "0" + month;
                    //if (day < 10)
                    //    day = "0" + day;
                    //var d = d1.getFullYear() + "-" + (month) + "-" + (day);
                    var d = dt;
                   
                    $scope.convertdated = d;
                    return d;
                }
            }
            
            $scope.createdate = function (date)
            {
                //var d1 = new Date(date);
                //var month = d1.getMonth() + 1;
                //var day = d1.getDate();
                //if (month < 10)
                //    month = "0" + month;
                //if (day < 10)
                //    day = "0" + day;
                //var d = d1.getFullYear() + "-" + (month) + "-" + (day);
                var d = date;
                $scope.edt.prd_st_dt = d;

                if (date != null)
                {
                    if (day != '01')
                    {
                        swal({ title: "Alert", text: "Start Date Will be First of This Month", imageUrl: "assets/img/check.png", });
                    }

                    var nextYear = new Date(date);
                    nextYear.setDate(nextYear.getDate() - 1);

                    var year1 = new Date(date).getFullYear() + 1;
                    var month1 = nextYear.getMonth() + 1;
                    var day1 = nextYear.getDate();
                    if (month1 < 10)
                        month1 = "0" + month1;
                    if (day1 < 10)
                        day1 = "0" + day1;
                    //$scope.edt.prd_end_dt = year1 + "-" + month1 + "-" + day1;
                    $scope.edt.prd_end_dt = day1 + "-" + month1 + "-" + year1;
                }
            }
                
            $scope.Save = function (isvalidate)
            {

                debugger;
                var data1 = [];
                if (isvalidate) {
                    if ($scope.update1 == false)
                    {
                        var d1 = new Date($scope.edt.prd_st_dt);
                        var year = d1.getFullYear();

                        $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getCheckFinancialPeriod?comp_code=" + comp_code + "&year=" + finance_year).then(function (res) {
                            $scope.chkfinperiod = res.data;

                            if ($scope.chkfinperiod == '0') {
                                swal({ title: "Alert", text: "Record Not Exist in Financial Year", imageUrl: "assets/img/check.png", });
                                $scope.edt = "";
                            }
                            if ($scope.chkfinperiod == '2') {
                                swal({ title: "Alert", text: "Record Already Exist For this Financial Year", imageUrl: "assets/img/check.png", });
                                $scope.edt = "";
                            }
                            if ($scope.chkfinperiod == '1') {
                                var data = ({
                                    dept_comp_code: $scope.edt.dept_comp_code,
                                    prd_year: year,
                                    prd_st_dt: $scope.edt.prd_st_dt,
                                    prd_end_dt: $scope.edt.prd_end_dt,
                                    status: $scope.edt.status,
                                    opr: 'I'
                                });
                                data1.push(data);

                                $http.post(ENV.apiUrl + "api/common/FinancialPeriod/CUDFinancialPeriod", data1).then(function (res) {
                                    $scope.display = true;
                                    $scope.msg1 = res.data;
                                    if ($scope.msg1 == true) {
                                        swal({ title: "Alert", text: "Financial Period Data Added Successfully", })
                                            $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetFinancialPeriod").then(function (res) {
                                                $scope.FinPeriod_data = res.data;
                                                $scope.totalItems = $scope.FinPeriod_data.length;
                                                $scope.todos = $scope.FinPeriod_data;
                                                $scope.makeTodos();
                                                $scope.grid = true;
                                                $scope.display = false;
                                            })
                                       
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ title: "Alert", text: "Financial Period Data Not Added Successfully. ", imageUrl: "assets/img/notification-alert.png", });
                                        $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetFinancialPeriod").then(function (res) {
                                            $scope.FinPeriod_data = res.data;
                                            $scope.totalItems = $scope.FinPeriod_data.length;
                                            $scope.todos = $scope.FinPeriod_data;
                                            $scope.makeTodos();
                                            $scope.grid = true;
                                            $scope.display = false;
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });

                            }
                        });
                    }
                    else
                    {
                        $scope.Update(isvalidate);
                    }
                }
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetFinancialPeriod").then(function (res) {
                    $scope.FinPeriod_data = res.data;
                    $scope.totalItems = $scope.FinPeriod_data.length;
                    $scope.todos = $scope.FinPeriod_data;
                    $scope.makeTodos();
                    $scope.grid = true;
                    $scope.display = false;
                });
            }


            $scope.Update = function (isvalidate)
            {
                var data1 = [];
                if (isvalidate)
                {
                    var d1 = new Date($scope.edt.prd_st_dt);
                    var year = d1.getFullYear();

                    var data = ({
                        dept_comp_code: $scope.edt.dept_comp_code,
                        prd_year: year,
                        prd_st_dt: $scope.edt.prd_st_dt,
                        prd_end_dt: $scope.edt.prd_end_dt,
                        status: $scope.edt.status,
                        opr: 'U'
                    });
                    data1.push(data);

                    $http.post(ENV.apiUrl + "api/common/FinancialPeriod/CUDFinancialPeriod",data1).then(function (res) {
                        $scope.display = true;
                        $scope.grid = false;
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true)
                        {
                            swal({ title: "Alert", text: "Financial Period Data Updated Successfully", imageUrl: "assets/img/check.png", })
                                $scope.getgrid();
                           
                        }
                        else if ($scope.msg1 == false)
                        {
                            swal({ title: "Alert", text: "Financial Period Data Not Updated Successfully. " , imageUrl: "assets/img/notification-alert.png", });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                }
            }

            $scope.cancel = function ()
            {
                $scope.grid = true;
                $scope.display = false;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
               
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.FinPeriod_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }
                $scope.numPerPage = str;
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
            }
        }])
})();