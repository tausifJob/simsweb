﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var temp;
    var companycode = '1';
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('DocumentUser_PCCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {


            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.obj = [];
            $scope.autorise = [];

            //var user = $rootScope.globals.currentUser.username;
            //user = user.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            //Select Data
            //$http.get(ENV.apiUrl + "api/DocumentUser/getAllDocumentUser").then(function (res1) {
            //    $scope.docuser = res1.data;
            //    $scope.totalItems = $scope.docuser.length;
            //    $scope.todos = $scope.docuser;
            //    $scope.makeTodos();
            //});


            //New Select Data
            $http.get(ENV.apiUrl + "api/DocumentUser_PC/getDocumentUserDetails_PC?comp_code=" + comp_code + "&fyear=" + finance_year).then(function (res1) {
                $scope.docuser = res1.data;
                $scope.totalItems = $scope.docuser.length;
                $scope.todos = $scope.docuser;
                $scope.makeTodos();
            });
            //department
            $http.get(ENV.apiUrl + "api/DocumentUser_PC/GetDepartment_PC?comp_code=" + comp_code + "&fyear=" + finance_year).then(function (compdept) {
                
                $scope.dept = compdept.data;
                });

            $http.get(ENV.apiUrl + "api/JVCreation/getDocCodeJV_DPS?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                $scope.geDocCode_jv = res.data;
            });

            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                 $scope.makeTodos();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.docuser, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.docuser;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.gldu_doc_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                       item.gldu_doc_code_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                       item.acno == toSearch) ? true : false;
            }

            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "/" + month + "/" + day;
            }


            //NEW BUTTON
            $scope.New = function () {

                $scope.disabled = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.compname1 = false;
                $scope.year1 = false;
                $scope.dept1 = false;
                $scope.document1 = false;
                $scope.search_btn = true;
                $scope.temp = "";
                $scope.temp = {};
                $scope.obj['gldu_prepare'] = true;
                $scope.Search_Employee1 = [];
                $scope.temp = "";
                $scope.edt = "";
                $scope.autorise = [];
                $scope.user = "";
                //   

            }

            //EnableDesable

            $scope.setvisible = function () {

                $scope.ldgno = true;
                $scope.slno = true;
            }


            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                if (Myform) {
                   
                    $scope.ListColl = [];
                    var gldu_verify, gldu_authorize;
                    var uniqueusercode = $scope.user.comn_user_name.split('/');
                    for (var i = 0; i < $scope.autorise.length; i++) {

                        if ($scope.autorise[i].gldu_user_name_data == uniqueusercode[1]) {
                            gldu_verify = $scope.autorise[i].gldu_verify;
                            gldu_authorize = $scope.autorise[i].gldu_authorize;
                        }
                    }
                    for (var i = 0; i < $scope.autorise.length; i++) {
                        var gldu_authorize_user = '';
                        var gldu_verify_user = '';
                        if ($scope.autorise[i].gldu_verify == true) {
                            gldu_verify_user = $scope.autorise[i].gldu_user_name_data;
                        }
                        if ($scope.autorise[i].gldu_authorize == true) {
                            gldu_authorize_user = $scope.autorise[i].gldu_user_name_data;
                        }
                        var data = {
                            gldu_user_name_data: uniqueusercode[1],
                            gldu_verify: gldu_verify,
                            gldu_authorize: gldu_authorize,
                            gldu_prepare: $scope.obj.gldu_prepare,
                            gldu_dept_no: $scope.temp.codp_dept_no,
                            gldu_doc_code: $scope.temp.gltd_doc_code,
                            gldu_comp_code: comp_code,
                            gldu_authorize_user: gldu_authorize_user,
                            gldu_verify_user: gldu_verify_user,
                        }
                        $scope.ListColl.push(data);
                    }

                    $http.post(ENV.apiUrl + "api/DocumentUser_PC/InsertDocumentUser_PC", $scope.ListColl).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.Search_Employee1 = [];
                        $scope.temp = "";
                        $scope.edt = "";
                        swal({ text: $scope.msg1.strMessage, timer: 5000 });
                        $scope.getGrid();

                    });
                    datasend = [];
                }
                $scope.table = true;
                $scope.display = false;

            }


            $scope.getGrid = function () {
                $http.get(ENV.apiUrl + "api/DocumentUser_PC/getDocumentUserDetails_PC?comp_code=" + comp_code + "&fyear=" + finance_year).then(function (res1) {
                    $scope.docuser = res1.data;
                    $scope.totalItems = $scope.docuser.length;
                    $scope.todos = $scope.docuser;
                    $scope.makeTodos();
                });
            }

            //Date FORMAT
            $scope.showdate = function (date, name1) {

                var month = date.split("/")[0];
                var day = date.split("/")[1];
                var year = date.split("/")[2];

                $scope.temp[name1] = year + "-" + month + "-" + day;
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;

                $scope.user_name = false;
                $scope.search_one = false;
                $scope.Prepare_status = false;

            }

            $scope.okbuttonclick = function () {
                $('#MyModal').modal('hide');
                $scope.display = true;
                $scope.edt.comn_user_name = '';
                $scope.edt.EmpName = '';

            }

            //DATA EDIT
            $scope.edit = function (str) {
                
                $scope.autorise = [];
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.small_display = true;
                $scope.search_btn = true;
                $scope.user_name = true;
                $scope.search_one = true;
                $scope.Prepare_status = true;


                $scope.user = {
                    comn_user_name: str.gldu_user_name_data,
                }
                $scope.obj = {
                    gldu_prepare: str.gldu_prepare,
                }

                $scope.temp = {
                    gldu_user_name: str.gldu_user_name,

                    codp_dept_no: str.gldu_dept_no,
                    gltd_doc_code: str.gldu_doc_code,
                    gldc_doc_short_name: str.gldu_doc_code_name,
                    
                }


                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].gldu_doc_code == str.gldu_doc_code && $scope.filteredTodos[i].gldu_user_name_data == str.gldu_user_name_data && $scope.filteredTodos[i].gldu_dept_no == str.gldu_dept_no) {
                        
                        var flag = 0;

                        $scope.autorise = $scope.filteredTodos[i].listColl;
                    }
                }


                $scope.edt = "";

                $scope.user_name = true;
                $scope.Prepare = true;
                $scope.dept1 = true;

            }


            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function () {
              

                //$scope.ListColl = [];
                //for (var i = 0; i < $scope.autorise.length; i++) {
                //    var data ={
                //        gldu_user_name_data: $scope.user.comn_user_name,
                //        gldu_verify: $scope.autorise[i].gldu_verify,
                //        gldu_authorize: $scope.autorise[i].gldu_authorize,
                //        gldu_prepare: $scope.obj.gldu_prepare,
                //        gldu_dept_no:$scope.temp.codp_dept_no,
                //        gldu_comp_code: comp_code,
                //        gldu_authorize_user: $scope.autorise[i].gldu_user_name_data,
                //        gldu_verify_user: $scope.autorise[i].gldu_user_name_data,
                //    }

                //    $scope.ListColl.push(data);
                //}

                $scope.ListColl = [];
                var f = false;
                var gldu_verify, gldu_authorize;
                //var uniqueusercode = $scope.user.comn_user_name.split('/');
                for (var i = 0; i < $scope.autorise.length; i++) {


                    if ($scope.autorise[i].gldu_user_name_data == $scope.user.comn_user_name) {
                        gldu_verify = $scope.autorise[i].gldu_verify;
                        gldu_authorize = $scope.autorise[i].gldu_authorize;
                        f = true;
                    }
                }
                if (!f) {
                    var data = {
                        gldu_user_name_data: $scope.user.comn_user_name,
                        gldu_verify: gldu_verify,
                        gldu_authorize: gldu_authorize,
                        gldu_prepare: $scope.obj.gldu_prepare,
                        gldu_dept_no: $scope.temp.codp_dept_no,
                        gldu_doc_code: $scope.temp.gltd_doc_code,
                        gldu_comp_code: comp_code,
                        gldu_authorize_user: gldu_authorize_user,
                        gldu_verify_user: gldu_verify_user,
                    }
                    $scope.ListColl.push(data);

                }


                for (var i = 0; i < $scope.autorise.length; i++) {
                    var gldu_authorize_user = '';
                    var gldu_verify_user = '';
                    if ($scope.autorise[i].gldu_verify == true) {
                        gldu_verify_user = $scope.autorise[i].gldu_user_name_data;
                    }
                    if ($scope.autorise[i].gldu_authorize == true) {
                        gldu_authorize_user = $scope.autorise[i].gldu_user_name_data;
                    }
                    var data = {
                        gldu_user_name_data: $scope.user.comn_user_name,
                        gldu_verify: gldu_verify,
                        gldu_authorize: gldu_authorize,
                        gldu_prepare: $scope.obj.gldu_prepare,
                        gldu_dept_no: $scope.temp.codp_dept_no,
                        gldu_doc_code: $scope.temp.gltd_doc_code,
                        gldu_comp_code: comp_code,
                        gldu_authorize_user: gldu_authorize_user,
                        gldu_verify_user: gldu_verify_user,
                    }
                    $scope.ListColl.push(data);
                }

                $http.post(ENV.apiUrl + "api/DocumentUser_PC/UpdateDocumentUser_PC", $scope.ListColl).then(function (msg) {
                    $scope.msg1 = msg.data;
                    $scope.Search_Employee1 = [];
                    $scope.temp = "";
                    $scope.edt = "";
                    swal({ text: $scope.msg1.strMessage, timer: 5000 });
                    $scope.getGrid();
                });
                datasend = [];
                $scope.table = true;
                $scope.display = false;
            }


            //DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("doc-"+i);
                        v.checked = true;
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById("doc-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
            }





            //$scope.OkDelete = function () {
            //    deletefin = [];
            //    debugger
            //    for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //        var v = document.getElementById(i);
            //        if (v.checked == true) {
            //            var deletemodulecode = ({
            //                                 gldu_dept_no: $scope.filteredTodos[i].gldu_dept_no,
            //                                 gldu_user_name_data: $scope.filteredTodos[i].gldu_user_name_data,
            //                      });
            //            deletefin.push(deletemodulecode);
            //        }
            //    }
            //    $http.post(ENV.apiUrl + "api/DocumentUser/delDocumentUser", deletefin).then(function (msg) {
            //        $scope.msg1 = msg.data;
            //        swal({ text: $scope.msg1.strMessage, timer: 5000 });

            //        $scope.getGrid();

            //    });
            //    deletefin = [];

            //}
            $scope.OkDelete = function () {
                
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById("doc-" + i);
                    $scope.flag = true;
                    if (v.checked == true) {
                        var deletemodulecode = ({
                            gldu_doc_code: $scope.filteredTodos[i].gldu_doc_code,
                            gldu_dept_no: $scope.filteredTodos[i].gldu_dept_no,
                            gldu_user_name_data: $scope.filteredTodos[i].gldu_user_name_data,
                            gldu_comp_code: comp_code
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/DocumentUser_PC/delDocumentUser_PC", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: $scope.msg1.strMessage, timer: 5000, }).then(function (isConfirm) {
                                        $scope.getGrid();
                                        if (isConfirm) {
                                           
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            $scope.currentPage = true;
                                        }

                                    });
                                }
                                else {
                                    swal({ text: $scope.msg1.strMessage, timer: 5000, }).then(function (isConfirm) {
                                        $scope.getGrid();
                                        if (isConfirm) {
                                            
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                            //main.checked = false;
                                            // $('tr').removeClass("row_selected");
                                        }
                                    });
                                }

                            });
                        }
                        else {
                            
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById("doc-" + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                    //  $scope.row1 = '';

                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
            }



            //New search
            $scope.newSearch = function (str) {
                $scope.Search_Employee1 = [];
                $scope.Seachbtn = str;
                
                $scope.searchtable = false;
                $scope.buttons = false;
                $scope.busy = false;
                $scope.user_name = false;
                $scope.search_one = false;
                $scope.Prepare_status = false;

                // $scope.temp = '';
                $scope.sibling_result = '';
                $('.nav-tabs a[href="#Employee_Details"]').tab('show')
                $scope.active = 'active';
                $('#MyModal').modal('show');

                $scope.searchtable = false;
            }

            $scope.Removeuser = function ($event, index, str) {
                //for(var i=0;)
                str.splice(index, 1);

            }

            $scope.SearchEmployeedata = function () {
                
                $http.post(ENV.apiUrl + "api/DocumentUser/SearchEmployee?data=" + JSON.stringify($scope.edt)).then(function (GetSearchEmployee1) {
                    $scope.Search_Employee1 = GetSearchEmployee1.data;
                });
            }
            $scope.autorise = [];


            $scope.getUsername = function (str, s) {
               
                //First Search button
                if ($scope.Seachbtn == 1) {
                    $scope.user = [];
                    var f = false;
                    if ($scope.autorise == undefined) {
                        $scope.autorise = '';
                    }
                    for (var i = 0; i < $scope.autorise.length; i++) {
                        if ($scope.autorise[i].gldu_user_name_data == str.comn_user_name) {
                            f = true;
                        }
                    }
                    if (!f) {
                        var data = {
                            gldu_user_name_data: str.comn_user_name,
                            gldu_user_name: str.empName + '/' + str.comn_user_name,
                            gldu_verify: false,
                            gldu_authorize: false
                        }
                        $scope.autorise.push(data);
                    }
                    $scope.user = { comn_user_name: data.gldu_user_name };
                }




                    //Second Search button
                else if ($scope.Seachbtn == 2) {
                    
                    var f = false;
                    if ($scope.autorise == undefined) {
                        $scope.autorise = '';
                    }
                    for (var i = 0; i < $scope.autorise.length; i++) {
                        if ($scope.autorise[i].gldu_user_name_data == str.comn_user_name) {
                            f = true;
                        }
                    }
                    if (!f) {
                        var data = {
                            gldu_user_name_data: str.comn_user_name,
                            gldu_user_name: str.empName + '/' + str.comn_user_name,
                            gldu_verify: false,
                            gldu_authorize: false
                        }
                        $scope.autorise.push(data);
                    }
                }
            }


            $scope.newSearch1 = function () {
                
                $scope.searchtable = false;
                $scope.buttons = false;
                $scope.busy = false;
                $scope.temp = '';
                $scope.sibling_result = '';
                $('.nav-tabs a[href="#Employee_Details1"]').tab('show')
                $scope.active = 'active';
                $('#MyModal1').modal('show');

                $scope.searchtable = false;
            }
            $scope.SearchEmployeedata1 = function () {
                
                $http.post(ENV.apiUrl + "api/DocumentUser/SearchEmployee?data=" + JSON.stringify($scope.edt)).then(function (GetSearchEmployee1) {
                    $scope.Search_Employee2 = GetSearchEmployee1.data;
                });
            }

            $scope.getUsername1 = function (str) {
                
                for (var i = 0; i < $scope.Search_Employee2.length; i++) {
                    $scope.Search_Employee2[i].status = false
                }

                str.status = true
                $scope.temp = { comn_user_name: str.comn_user_name + "-" + str.empName };
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

        }])

})();
