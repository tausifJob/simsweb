﻿(function () {
    'use strict';
    var obj1, obj2, temp, main;
    var opr = '';
    var categorycode = [];
    var data1 = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CostCentreCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.CostCentreDetail = true;
            $scope.editmode = false;
            $scope.edt = "";
            $scope.pager = true;
            var user = $rootScope.globals.currentUser.username.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.cost_centre_data;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.cost_centre_data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.cost_centre_data;
                }

                $scope.makeTodos();
                $scope.check_all();
                main.checked = false;
            }

            function searchUtil(item, toSearch) {
               
                /* Search Text in all 3 fields */
                return (
                        item.coce_cost_centre_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.coce_cost_centre_person_responsible.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.coce_cost_centre_currency.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.coce_cost_centre_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.coce_cost_centre_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.coce_cost_centre_address == toSearch) ? true : false;
            }

            $scope.cancel = function () {
                $scope.CostCentreDetail = true;
                $scope.CostCentreOperation = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
                $scope.newmode = true;
                $scope.edt = '';
                $scope.editmode = false;
                opr = 'S';
                $scope.readonly = false;
                $scope.CostCentreDetail = false;
                $scope.CostCentreOperation = true;
                $scope.savebtn = true;
                $scope.updatebtn = false;
                $scope.edt = {};
                $scope.edt.coce_status = true;
            }

            $http.get(ENV.apiUrl + "api/CostCentre/getCostCentreDetail").then(function (cost_centre) {

                $scope.cost_centre_data = cost_centre.data;
                $scope.totalItems = $scope.cost_centre_data.length;
                $scope.todos = $scope.cost_centre_data;
                $scope.makeTodos();
                });

            $http.get(ENV.apiUrl + "api/CostCentre/getCurrencyDetail").then(function (cost_centre_currency) {

                $scope.currency = cost_centre_currency.data;
            });

            $scope.Save = function (myForm) {
                if (myForm) {
                    var data = $scope.edt;
                    var data = {
                        comp_code: comp_code
                       , finance_year: finance_year
                       , coce_cost_centre_code: $scope.edt.coce_cost_centre_code
                       , coce_cost_centre_type: $scope.edt.coce_cost_centre_type
                       , coce_cost_centre_currency: $scope.edt.coce_cost_centre_currency
                               , coce_cost_centre_name: $scope.edt.coce_cost_centre_name
                               , coce_cost_centre_person_responsible: $scope.edt.coce_cost_centre_person_responsible
                               , coce_cost_centre_address: $scope.edt.coce_cost_centre_address
                       , coce_status: $scope.edt.coce_status
                    }
                    data.opr = 'I';
                    $scope.exist = false;
                    data1.push(data);
                    for (var i = 0; i < $scope.cost_centre_data.length; i++) {
                        if ($scope.cost_centre_data[i].coce_cost_centre_code == data.coce_cost_centre_code || $scope.cost_centre_data[i].coce_cost_centre_name == data.coce_cost_centre_name) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ title: "Alert", text: "Record Already exists", width: 300, height: 200 });
                    }

                    else {

                        $http.post(ENV.apiUrl + "api/CostCentre/CostCentreDetailCUD?simsobj=", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            $scope.CostCentreOperation = false;
                            $http.get(ENV.apiUrl + "api/CostCentre/getCostCentreDetail").then(function (cost_centre) {
                                $scope.cost_centre_data = cost_centre.data;
                                $scope.totalItems = $scope.cost_centre_data.length;
                                $scope.todos = $scope.cost_centre_data;
                                $scope.makeTodos();
                                if ($scope.msg1 == true) {

                                    swal({ title: "Alert", text: "Record Inserted Successfully", showCloseButton: true, width: 380 });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ title: "Alert", text: "Record Not Inserted. ", showCloseButton: true, width: 380 });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });

                        });

                        $scope.CostCentreDetail = true;
                        $scope.CostCentreOperation = false;
                    }
                    data1 = [];
                }
            }

            $scope.up = function (str) {
                opr = 'U';
                $scope.editmode = true;
                $scope.newmode = false;
                $scope.savebtn = false;
                $scope.updatebtn = true;
                $scope.readonly = true;
                $scope.CostCentreDetail = false;
                $scope.CostCentreOperation = true;
                $scope.edt = {
                    coce_cost_centre_code: str.coce_cost_centre_code
                          , coce_cost_centre_type: str.coce_cost_centre_type
                          , coce_cost_centre_name: str.coce_cost_centre_name
                          , coce_cost_centre_person_responsible: str.coce_cost_centre_person_responsible
                          , coce_cost_centre_currency: str.coce_cost_centre_currency
                          , currency_desc: str.currency_desc
                          , coce_cost_centre_address: str.coce_cost_centre_address
                          , coce_status: str.coce_status
                };
            }

            $scope.Update = function () {
                var data = $scope.edt;
                data.opr = 'U';
                $scope.edt = "";
                data1.push(data);
                $http.post(ENV.apiUrl + "api/CostCentre/CostCentreDetailCUD", data1).then(function (msg) {
                    $scope.msg1 = msg.data;
                    $scope.CostCentreOperation = false;
                    $http.get(ENV.apiUrl + "api/CostCentre/getCostCentreDetail").then(function (cost_centre) {
                        $scope.cost_centre_data = cost_centre.data;
                        $scope.totalItems = $scope.cost_centre_data.length;
                        $scope.todos = $scope.cost_centre_data;
                        $scope.makeTodos();

                        if ($scope.msg1 == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", showCloseButton: true, width: 380 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ title: "Alert", text: "Record Not Updated. ", showCloseButton: true, width: 380 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                })
                $scope.CostCentreOperation = false;
                $scope.CostCentreDetail = true;
                data1 = [];
            }

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i+$scope.filteredTodos[i].coce_cost_centre_code);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i+$scope.filteredTodos[i].coce_cost_centre_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }
            }

            $scope.check_once = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.deleterecord = function () {
                var data1 = [];
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i+$scope.filteredTodos[i].coce_cost_centre_code);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'coce_cost_centre_code': $scope.filteredTodos[i].coce_cost_centre_code,
                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            $http.post(ENV.apiUrl + "api/CostCentre/CostCentreDetailCUD", deleteleave).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/CostCentre/getCostCentreDetail").then(function (cost_centre) {
                                                $scope.cost_centre_data = cost_centre.data;
                                                $scope.totalItems = $scope.cost_centre_data.length;
                                                $scope.todos = $scope.cost_centre_data;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                                else if ($scope.msg1 == false) {

                                    //swal({ title: "Alert", text: "Record Not Deleted. ", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/CostCentre/getCostCentreDetail").then(function (cost_centre) {
                                                $scope.cost_centre_data = cost_centre.data;
                                                $scope.totalItems = $scope.cost_centre_data.length;
                                                $scope.todos = $scope.cost_centre_data;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('all_chk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                $scope.row1 = '';
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1);
                                }
                            });
                        }
                        else {

                            main = document.getElementById('all_chk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i+$scope.filteredTodos[i].coce_cost_centre_code);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = str;
                $scope.row1 = '';
            }

        }]);

})();