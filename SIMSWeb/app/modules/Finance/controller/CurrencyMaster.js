﻿(function () {
    'use strict';
    var modulecode = [];
    var del = [];
    var main;
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CurrencyMasterCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.display = false;
            $scope.obj = [];
            $scope.itemsPerPage = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $timeout(function () {
                $("#Table1").tableHeadFixer({ 'top': 1 });
            }, 100);


            $scope.minrate = function (minval) {
                $scope.minvalue = minval;
            }
            $scope.maxrate = function (maxval) {
                $scope.maxvalue = maxval;
                //if ($scope.maxvalue != undefined || $scope.maxvalue != "") {
                //    if($scope.minvalue!=undefined||$scope.minvalue!=""){
                //        if ($scope.maxvalue < 0 || $scope.maxvalue < $scope.minvalue) {
                //            $scope.edt = { excg_max_rate: $scope.minvalue };
                //        }
                //    }
                //}
            }

            $http.get(ENV.apiUrl + "api/finance/getCurrencyMaster").then(function (res) {
                $scope.obj = res.data;
                $scope.div_hide = false;
                $scope.display = false;
                $scope.grid = true;
                $scope.save1 = false;

                $scope.totalItems = $scope.obj.length;
                $scope.todos = $scope.obj;
                $scope.makeTodos();

            });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.size = function (str) {
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.obj;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;  $scope.makeTodos();
            }

            $scope.range = function () {
                var rangeSize = 10;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;


            };


            $scope.edit = function (str) {

                debugger;

                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = false;
                $scope.update1 = true;
                $scope.delete1 = false;


                $scope.edt = angular.copy(str);
            }


            $scope.showdate = function (date, name1) {
                var date = new Date(date);
                var month = (date.getMonth() + 1);
                var day = date.getDate();
                if (month < 10)
                    month = "0" + month;
                if (day < 10)
                    day = "0" + day;
                $scope.edt[name1] = date.getFullYear() + '-' + (month) + '-' + (day)

            }


            $scope.Update = function (isvalidate) {
                debugger;
                if (isvalidate) {
                    $scope.edt.opr = "U";
                    data.push($scope.edt)
                    $http.post(ENV.apiUrl + "api/finance/CUDCurrencyMaster", data).then(function (msg) {
                        $scope.grid = true;
                        $scope.display = false;
                        data = [];
                        swal('', 'Record Updated Successfully');
                        $scope.maindata();
                        $scope.display = false;
                        $scope.grid = true;
                    });
                }
                else {
                    swal('Error!', 'Fill Field to be Inserted' + msg.data);
                }
            }

            var data = [];

            $scope.Save = function (isvalidate) {
                debugger;
                if ($scope.update1) {
                    $scope.Update(isvalidate);
                }
                else {
                    
                    //concole.log($scope.getElementById)
                    if (isvalidate) {
                        $scope.edt.opr = "I"
                        data.push($scope.edt);


                        if ($scope.edt.excg_curcy_code != "" && $scope.edt.excg_curcy_code && $scope.edt.excg_curcy_desc != "" && $scope.edt.excg_curcy_desc) {
                            $scope.msg1 = false;
                            $http.post(ENV.apiUrl + "api/finance/CUDCurrencyMaster", data).then(function (msg) {
                                data = [];
                                swal('', msg.data);
                                $scope.maindata();
                                $scope.display = false;
                                $scope.grid = true;
                            });
                        }
                        else {
                            $scope.msg1 =

                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'Please Enter Currency Code and Description',
                                    width: 350,
                                    showCloseButon: true
                                });

                        }


                    } else {
                        swal('Error!', 'Fill Field to be Inserted. ' + msg.data);
                    }
                }
            }

            $http.get(ENV.apiUrl + "api/finance/getComCode").then(function (res) {
                $scope.compcode = res.data;
                if (res.data.length > 0) {
                    $scope.edt.excg_comp_code = res.data[0].gdua_comp_code;
                }
            });
            $scope.New = function () {
                $scope.display = true;
                $scope.grid = false;
                $scope.save1 = true;
                $scope.update1 = false;
                $scope.delete1 = false;
                $scope.edt = {};
                $scope.edt.excg_enabled_flag = true;
               // $scope.edt = "";
                var dt = new Date();
                $scope.edt.excg_effective_date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();

                if ($scope.compcode.length > 0) {
                    $scope.edt.excg_comp_code = $scope.compcode[0].gdua_comp_code;
                }

               
            }

            $scope.cancel = function () {
                $scope.grid = true;
                $scope.display = false;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            //$scope.Delete = function () {


            //    $scope.row1 = '';
            //    for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //        var v = document.getElementById($scope.obj[i].excg_curcy_code + i);
            //        if (v.checked == true) {
            //            $scope.filteredTodos[i].opr = "D";
            //            del.push($scope.filteredTodos[i]);
            //        }
            //    }
            //    $http.post(ENV.apiUrl + "api/finance/CUDCurrencyMaster", del).then(function (msg) {
            //            del = [];
            //            swal('', 'Record Deleted Successfully');

            //                 $scope.maindata();

            //            $scope.currentPage = 0;

            //    })
            //}


            $scope.Delete = function () {
                
                //deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].excg_curcy_code + i);
                    if (v.checked == true) {
                        $scope.filteredTodos[i].opr = "D";
                        del.push($scope.filteredTodos[i]);
                        $scope.flag = true;
                    }
                }

                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            
                            $http.post(ENV.apiUrl + "api/finance/CUDCurrencyMaster", del).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 != "") {
                                    swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.maindata();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }

                                    });
                                }
                                else  {
                                    swal({ title: "Alert", text: "Record Not Deleted. " , showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.maindata();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                    $scope.multipledelete();
                                }
                                /*else {
                                    swal("Error-" + scope.msg1)
                                }*/
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].excg_curcy_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    //$scope.row1 = '';
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }



            $scope.maindata = function () {
                $http.get(ENV.apiUrl + "api/finance/getCurrencyMaster").then(function (res) {
                    $scope.obj = res.data;
                    $scope.div_hide = false;
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.save1 = false;

                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.makeTodos();
                    $scope.myForm.$setPristine();
                    $scope.myForm.$setUntouched();
                });
            }

            $scope.multipledelete = function () {

                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    var v = document.getElementById($scope.filteredTodos[i].excg_curcy_code + i);
                    if (main.checked == true) {
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                    else {
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.delete_onebyone = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.excg_curcy_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.excg_curcy_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.excg_deci_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }



            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'

            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
            var dom;
            var count = 0;

        }])
})();