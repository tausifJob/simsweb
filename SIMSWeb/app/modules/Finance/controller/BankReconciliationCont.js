﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('BankReconciliationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);
          
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();

            $scope.temp =
                {enddate1 : dd + '-' + mm + '-' + yyyy}




            var now = new Date();
            var month = (now.getMonth() + 1);
            var day = now.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            $scope.currentDate = day + '-' + month + '-' + now.getFullYear();
            //Select Data SHOW
         //   $scope.temp.enddate1 = now.getFullYear() + '/' + month + '/' + day;
            $scope.Prepare = function (start_date, end_date, acct_code)
            {
                
                $http.get(ENV.apiUrl + "api/BankRecancellation/getAllBankRecancellation?start_date=" + start_date + "&end_date=" + end_date + "&acct_code=" + acct_code).then(function (res1) {
                    $scope.All_Details = res1.data;
                   
                    for (var i = 0; i < $scope.All_Details.length; i++) {
                        $scope.All_Details[i].glbr_creation_user = user;
                        $scope.All_Details[i].glbr_creation_date = $scope.currentDate;
                    }

                    $scope.totalItems = $scope.All_Details.length;
                    $scope.todos = $scope.All_Details;
                    $scope.makeTodos();
                    });
            }

            $scope.All = function (start_date, end_date, acct_code)
            {
                
                $scope.filteredTodos = [];
                $http.get(ENV.apiUrl + "api/BankRecancellation/getAllBankRecancellation?start_date=" + start_date + "&end_date=" + end_date + "&acct_code=" + acct_code).then(function (res1) {
                    $scope.All_Details = res1.data;

                    for (var i = 0; i < $scope.All_Details.length; i++) {
                        $scope.All_Details[i].glbr_creation_user = user;
                        $scope.All_Details[i].glbr_creation_date = $scope.currentDate;
                    }

                  //  $scope.totalItems = $scope.All_Details.length;
                    $scope.filteredTodos = $scope.All_Details;
                 //   $scope.makeTodos();
                    });

            }

           
            var user = $rootScope.globals.currentUser.username;
            //$scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);
           
         
            //$scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy/MM/dd');


            $http.get(ENV.apiUrl + "api/BankRecancellation/GetAccountNumber").then(function (AccountName) {
               
                $scope.Account_Name = AccountName.data;
                 });

             var date = new Date();


            $scope.size = function (str) {
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                
                main.checked = false;
                $scope.row1 = '';
                // $scope.CheckAllChecked();

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {

                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_library_attribute_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_library_attribute_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

           

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.Save = function () {
                debugger
                var sdata = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++)
                {
                    if ($scope.filteredTodos[i].glbr_bank_date != '')
                    {
                        if ($scope.filteredTodos[i].glbr_doc_remark != '')
                        {
                            $scope.filteredTodos[i]['opr'] = "U";
                            sdata.push($scope.filteredTodos[i])
                        }
                    }

                }
                    //dataupdate.push(data);
                    $http.post(ENV.apiUrl + "api/BankRecancellation/CUDBankRacancellation", sdata).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({  text: "Record Updated Successfully",imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {

                            swal({  text: "Record Not Updated. ", imageUrl: "assets/img/close.png",width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        //    $http.get(ENV.apiUrl + "api/BankRecancellation/getAllBankRecancellation?start_date=" + start_date + "&end_date=" + end_date + "&acct_code=" + acct_code).then(function (res1) {
                        //    $scope.All_Details = res1.data;
                        //    for (var i = 0; i < $scope.All_Details.length; i++) {
                        //        $scope.All_Details[i].glbr_creation_user = user;
                        //        $scope.All_Details[i].glbr_creation_date = $scope.currentDate;
                        //    }
                        //    $scope.totalItems = $scope.All_Details.length;
                        //    $scope.todos = $scope.All_Details;
                        //    $scope.makeTodos();
                        //    });
                    });
                    dataforUpdate = [];
                    $scope.table = true;
                    $scope.display = false;
                
            }

            $scope.dateChange=function(str)
            {
                $scope.tempObj.glbr_bank_date = str;
                //$scope.date1 = str;
            }

            $scope.getDate=function(str)
            {
                $scope.tempObj = str;
                
                $('#myModal4').modal('show')

                 // j.glbr_bank_date = temp.enddate;

            }


          



        }])

})();
