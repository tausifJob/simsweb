﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CumulativeSubledgerSummaryReportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            // Pre-Required Functions and Variables
            // Start
            //$scope.name = "Subject Teacher List";
            $scope.temp = {};
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.temp = {};
            $scope.display = false;
            $scope.dublicate = false;

            $http.get(ENV.apiUrl + "api/CumSubSumReport/getCompCode").then(function (res1) {
                $scope.sims_cur = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['comp_code'] = res1.data[0].comp_code;

                    $scope.getAllLedgerCode(res1.data[0].comp_code);
                }
            });

            $scope.getAllLedgerCode = function (comp_code) {
                $http.get(ENV.apiUrl + "api/CumSubSumReport/getAllLedgerCode?comp_code=" + comp_code).then(function (res1) {
                    $scope.ledger_code = res1.data;

                    setTimeout(function () {
                        $('#grade_box').change(function () {

                        }).multipleSelect({
                            width: '100%'
                        });
                        //$("#show_box").multipleSelect("checkAll");
                    }, 1000);
                });
            }

            $http.get(ENV.apiUrl + "api/CumSubSumReport/getViwReport").then(function (res1) {
                //debugger;
                $scope.view_report = res1.data;
                if (res1.data.length > 0) {
                    $scope.temp['comn_appl_parameter'] = res1.data[0].comn_appl_parameter;

                }

                });


            $(function () {
                $('#grade_box').multipleSelect({ width: '100%' });
                $('#section_box').multipleSelect({ width: '100%' });
            });

            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_start_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });


            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_end_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });    

            $scope.cummulativeSummaryReport = function () {
                debugger
                $scope.colsvis = false;
               
                $http.get(ENV.apiUrl + "api/CumSubSumReport/getCummulativeSummaryReport?comp_code=" + $scope.temp.comp_code + "&view_report=" + $scope.temp.comn_appl_parameter + "&ldgr_code=" + $scope.temp.sllc_ldgr_code + "&mom_start_date=" + $scope.mom_start_date + "&mom_end_date=" + $scope.mom_end_date).then(function (res1) {
                    debugger;
                    if (res1.data.length > 0) {
                        debugger;
                        $scope.report_data = res1.data;
                        $scope.temp.total_debit = 0;
                        $scope.temp.total_credit = 0;
                        $scope.temp.totalclosing = 0;
                        $scope.temp.total_opening_amt = 0;

                        for (var j = 0; j < $scope.report_data.length; j++) {
                            debugger;

                            $scope.temp.total_opening_amt = parseFloat($scope.temp.total_opening_amt) + parseFloat($scope.report_data[j].slma_op_bal_amt);
                            $scope.temp.total_debit = parseFloat($scope.temp.total_debit) + parseFloat($scope.report_data[j].slma_tran_amt_dr);
                            $scope.temp.total_credit =parseFloat( $scope.temp.total_credit) + parseFloat($scope.report_data[j].slma_tran_amt_cr);
                            $scope.temp.totalclosing = parseFloat($scope.temp.totalclosing) + parseFloat($scope.report_data[j].slma_close_bal_amt);
                        }


                        angular.forEach($scope.report_data, function (f) {
                            
                        });
                    }
                    else {
                        swal({ title: "Alert", text: " Sorry !!!!Data is not Available", showCloseButton: true, width: 300, height: 200 });
                        $scope.report_data = res1.data;
                        $scope.report_data = res1.data;
                        $scope.temp.total_debit = "";
                        $scope.temp.total_credit = "";
                        $scope.temp.totalclosing = "";
                        $scope.temp.total_opening_amt = "";
                                           }
                    console.log($scope.report_data);
                    console.log($scope.totalclosing);
                });
            }
            var data_send = [];
            var data1 = [];
           
            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            //$scope.colsvis = false;


            $scope.exportData = function () {
                //$scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('rpt_data').innerHTML], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                        });
                        saveAs(blob, "CumulativeSubledgerSummaryReport.xls");
                        //$scope.colsvis = false;

                    }

                });
            };

            $scope.print = function (div) {

                //$scope.colsvis = false;

                var docHead = document.head.outerHTML;
                var printContents = document.getElementById('rpt_data').outerHTML;
                //$scope.colsvis = false;
                var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";


                var newWin = window.open("", "_blank", winAttr);
                var writeDoc = newWin.document;
                writeDoc.open();
                writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                writeDoc.close();
                newWin.focus();

            };

            



           
        }])

})();

