﻿(function () {
    'use strict';
    var main;
    var prdocdata = [];
    var data1 = [];
    var data = [], temp_doc = [];
    
    var comp_code = "1", chk_rcur;
    var acconcode = "";
    var msg_flag1 = false, jvaut = false, jver = false;
    var simsController = angular.module('sims.module.Finance');
    simsController.controller('UpdatePostedVoucherCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.edt = [];
            $scope.block1 = {};
            $scope.operation = true;
            $scope.table1 = true;
            $scope.save1 = true;
            $scope.Add = true;
            $scope.update = false;
            $scope.Update = false;
            $scope.vis = [];
            $scope.vis.doc_detail_grid = true;
            $scope.vis.main_acct_grid = true;
            $scope.vis.cmb_acct = true;
            $scope.vis.add_grid = true;

            var user = $rootScope.globals.currentUser.username;

            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            $scope.btn_reverse = false;
           
            $scope.prdocdata = [];
            $scope.temp_doc = [];
           
          
            $scope.edt = [];
            $scope.block1 = {};

            $scope.operation = true;
            $scope.table1 = true;
            $scope.save1 = true;
            $scope.Add = true;
            $scope.update = false;
            $scope.Update = false;
            var user = $rootScope.globals.currentUser.username;

            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;
            var vat_percent = $scope.finnDetail.comp_vat_per;
            var comp_vat_status = $scope.finnDetail.comp_vat_status;
            var input_vat_acct = $scope.finnDetail.input_vat_acct;
            var vat_ac_name = '';

            $scope.prdocdata = [];
            $scope.temp_doc = [];
          
            $scope.operation = true;
            $scope.chkcost = true;
            $scope.cost_combo = true;
           
            $scope.doc_cd_prov_no = '';
            $scope.doc_cd_final_doc_no = '';
            $scope.block1 = [];
            $scope.block2 = [];
            $scope.block3 = [];
            $scope.temp_doc = [];
            $scope.vis = [];
            $scope.vis['main'] = true;
            $scope.vis.onchangedoccodeshow = false;
            $scope.dramount = false;
            $scope.cramount = false;
            $scope.vis.doc_detail_grid = true;
            $scope.vis.main_acct_grid = true;
            $scope.save1 = true;
            $scope.vis.add_grid = true;
            $scope.checkprintbtn = false;
            $scope.vis.Add_show_hide = true;
            $scope.prdocdata = [];
            $scope.cmb_doc_cd = [];
            $scope.vis.update = false;
            //new sgr
            $scope.chkcost = false;
            $scope.block3.gldd_vat_enable = false;
            $scope.block3.gldd_include_vat = false;
            $scope.block3.gldd_doc_vat_amt = 0;

            $scope.cost_combo = false;
            var costcentername;

            $scope.readonly = true;
            //$scope.vis.SendToVerify = false;
            $scope.vis.ok = true;
            $scope.vis.is_recur = true;


            $scope.vis.cmb_acct = true;
            $scope.vis.txt_acct = false;
            $scope.dis_recur_date = true;

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
                $("#fixedtable1").tableHeadFixer({ 'top': 1 });
            }, 100);

            setTimeout(function () {
                $("#cmb_acc_Code3").select2();

            }, 100);

            $scope.SearchVouReverData = [];
            $scope.ref_docprvno = '';
            $scope.ref_final_doc_no = '';
            $scope.new_prov_data = '';
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                //format: 'yyyy-mm-dd'
                format: 'dd-mm-yyyy'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $http.get(ENV.apiUrl + "api/GLSLQuery/getDocCode_GL_Query?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {

                $scope.getDocCode = res.data;
            });

            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=Single").then(function (res) {
                $scope.final_doc_url = res.data;

            });

           

            $scope.ContraEntry = function (vcrrev) {
                debugger;
                $('#myModal_ContraEntry').modal('show');


                $scope.btn_reverse = false;
                //$scope.btn_verify = true;
                $scope.ref_docprvno = '';
                $scope.ref_final_doc_no = '';
                $scope.lbl_recur = '';

                $http.get(ENV.apiUrl + "api/VoucherReversal/Get_vouchers_data?doc_cd=" + vcrrev.gltd_doc_code + "&final_doc_no=" + vcrrev.gltd_final_doc_no + "&usrname=" + user +
                   "&comp_code=" + comp_code + "&fyear=" + finance_year).then(function (voucher_data) {

                       $scope.ref_docprvno = vcrrev.gltd_prov_doc_no;
                       $scope.ref_final_doc_no = vcrrev.gltd_final_doc_no;
                       $scope.lbl_recur = 'RV';

                       $scope.lbl_rev_doc = vcrrev.gltd_doc_code + '-' + vcrrev.gltd_final_doc_no;

                       $scope.SearchVouReverData = voucher_data.data;

                       $scope.edt = {
                           gltd_postdate: vcrrev.gltd_post_date,
                           //gltd_docdate: yyyy + '-' + mm + '-' + dd,
                           gltd_docdate: dd + '-' + mm + '-' + yyyy,
                           //gltd_docdate: vcrrev.gltd_docdate,
                           gltd_doc_narr: vcrrev.gltd_doc_narr,
                           gltd_remarks: vcrrev.gltd_remarks,
                           gltd_doc_code: vcrrev.gltd_doc_code
                       }
                       $scope.totalDebit = 0;
                       $scope.totalcredit = 0;
                       for (var i = 0; i < $scope.SearchVouReverData.length; i++) {
                           $scope.totalDebit = parseFloat($scope.totalDebit) + parseFloat($scope.SearchVouReverData[i].gldd_doc_amount_debit);
                           $scope.totalcredit = parseFloat($scope.totalcredit) + parseFloat($scope.SearchVouReverData[i].gldd_doc_amount_credit);
                       }

                       $scope.totalDebit = parseFloat($scope.totalDebit).toFixed(3);
                       $scope.totalcredit = parseFloat($scope.totalcredit).toFixed(3);

                       $scope.amountdifference = $scope.totalcredit - $scope.totalDebit;
                   });
            }


            $scope.Show = function () {

                //if (doc_prov_no == undefined)
                //    doc_prov_no = 0;
                //from_date = $scope.edt.from_date;
                //to_date = $scope.edt.to_date;
                $('#loader').modal({ backdrop: 'static', keyboard: false });

                $http.get(ENV.apiUrl + "api/VoucherReversal/getAllVouchers?comp_cd=" + comp_code + "&doc_cd=" + $scope.block1.gltd_doc_code + "&final_doc_no=" + $scope.block1.gltd_final_doc_no +
                    "&from_date=" + $scope.block1.from_date + "&to_date=" + $scope.block1.to_date + "&cheque_no=" + $scope.block1.gltd_cheque_no).then(function (VouRever_Data) {
                        $scope.verify_data = VouRever_Data.data;


                        $('#loader').modal('hide');
                        $(".modal-backdrop").removeClass("modal-backdrop");
                    });
            }

            $scope.UpdateVoucher = function (vdata) {
                
                debugger;
                $('#updatVoucher').modal('show');
                $scope.dramount = true;
                $scope.cramount = true;

                $http.get(ENV.apiUrl + "api/VoucherReversal/Get_vouchers_data?doc_cd=" + vdata.gltd_doc_code + "&final_doc_no=" + vdata.gltd_final_doc_no + "&usrname=" + user +
                   "&comp_code=" + comp_code + "&fyear=" + finance_year).then(function (voucher_data) {

                       $scope.ref_docprvno = vdata.gltd_prov_doc_no;
                       $scope.ref_final_doc_no = vdata.gltd_final_doc_no;
                       $scope.lbl_recur = 'RV';

                       $scope.lbl_rev_doc = vdata.gltd_doc_code + '-' + vdata.gltd_final_doc_no;

                       $scope.SearchVouReverData = voucher_data.data;

                       $scope.voucher = {
                           gltd_post_date: vdata.gltd_post_date,
                           //gltd_docdate: yyyy + '-' + mm + '-' + dd,
                           gltd_doc_date: dd + '-' + mm + '-' + yyyy,
                           //gltd_docdate: vdata.gltd_docdate,
                           gltd_doc_narr: vdata.gltd_doc_narr,
                           gltd_remarks: vdata.gltd_remarks,
                           gltd_doc_code: vdata.gltd_doc_code
                       }
                       $scope.totalDebit = 0;
                       $scope.totalcredit = 0;
                       for (var i = 0; i < $scope.SearchVouReverData.length; i++) {
                           $scope.totalDebit = parseFloat($scope.totalDebit) + parseFloat($scope.SearchVouReverData[i].gldd_doc_amount_debit);
                           $scope.totalcredit = parseFloat($scope.totalcredit) + parseFloat($scope.SearchVouReverData[i].gldd_doc_amount_credit);
                       }

                       $scope.totalDebit = parseFloat($scope.totalDebit).toFixed(3);
                       $scope.totalcredit = parseFloat($scope.totalcredit).toFixed(3);

                       $scope.amountdifference = $scope.totalcredit - $scope.totalDebit;
                   });
                
                $http.get(ENV.apiUrl + "api/VoucherReversal/getAllRecords?doccode=" + vdata.gltd_doc_code + "&prnumber=" + vdata.gltd_final_doc_no + "&comp_code=" + vdata.gltd_comp_code).then(function (prdoc_data) {

                    $scope.prdocdata = prdoc_data.data;
                    // $scope.block1 = [];
                    $scope.temp_doc = [];
                    $scope.totalDebit = 0;
                    $scope.totalcredit = 0;

                    for (var i = 0; i < $scope.prdocdata.length; i++) {

                        if (i == 0) {
                            $scope.temp_doc.gltd_comp_code = $scope.prdocdata[i].gltd_comp_code;
                            $scope.temp_doc.gltd_doc_code = $scope.prdocdata[i].gltd_doc_code;

                            $scope.temp_doc.gltd_doc_narr = $scope.prdocdata[i].gltd_doc_narr;
                            $scope.temp_doc.gltd_remarks = $scope.prdocdata[i].gltd_remarks;
                            $scope.temp_doc.gltd_doc_date = $scope.prdocdata[i].gltd_doc_date;
                            $scope.temp_doc.gltd_post_date = $scope.prdocdata[i].gltd_post_date;
                            $scope.temp_doc.gltd_prov_doc_no = $scope.prdocdata[i].gltd_prov_doc_no;
                            $scope.temp_doc.gltd_final_doc_no = $scope.prdocdata[i].gldd_final_doc_no;

                            $scope.temp_doc.gldu_authorize = $scope.prdocdata[i].gldu_authorize;
                            $scope.temp_doc.gldu_verify = $scope.prdocdata[i].gldu_verify;
                            $scope.temp_doc.gltd_authorize_date = $scope.prdocdata[i].gltd_authorize_date;
                            $scope.temp_doc.gltd_verify_date = $scope.prdocdata[i].gltd_verify_date;

                        }
                        $scope.cost = $scope.cost || ($scope.prdocdata[i].gldd_cost_center_code != '' || $scope.prdocdata[i].gldd_cost_center_code != "");
                        $scope.totalDebit = parseFloat($scope.totalDebit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_debit);
                        $scope.totalcredit = parseFloat($scope.totalcredit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_credit);

                    }

                });
            }

            $scope.RevertVoucher = function () {
                if ($scope.totalDebit == $scope.totalcredit) {

                    debugger;

                    $scope.btn_reverse = true;

                    $('#loader').modal({ backdrop: 'static', keyboard: false });

                    var data = {};
                    data.gltd_comp_code = comp_code;
                    data.gltd_doc_code = $scope.lbl_recur;
                    data.gltd_doc_date = $scope.edt.gltd_docdate;
                    data.gltd_post_date = $scope.edt.gltd_postdate;
                    data.gltd_doc_narr = $scope.edt.gltd_doc_narr;
                    data.gltd_remarks = $scope.edt.gltd_remarks;
                    data.gltd_prepare_user = user;
                    data.gltd_reference_doc_code = $scope.edt.gltd_doc_code;
                    data.gltd_reference_final_doc_no = $scope.ref_final_doc_no;

                    data.detailsdata = $scope.SearchVouReverData;



                    $http.post(ENV.apiUrl + "api/VoucherReversal/Insert_ReverseVoucher", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 != "") {
                            swal({ title: "Alert", text: "Voucher Reversed Successfully\n Final Doc No.- " + $scope.msg1, showCloseButton: true, width: 300, height: 200 });

                            $('#myModal_ContraEntry').modal('hide');
                            $scope.Show();
                        }
                        else {
                            swal({ title: "Alert", text: "Voucher Not Reversed", width: 300, height: 200 });

                            $scope.btn_reverse = false;
                            //$scope.btn_verify = true;
                        }

                        $('#loader').modal('hide');
                        $(".modal-backdrop").removeClass("modal-backdrop");
                    });
                }
                else {
                    swal({ title: "Alert", text: "Debit and Credit amount do not match.Cannot Revert Voucher.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.btn_reverse = false;
                            //$scope.btn_verify = true;
                        }
                    });

                }
            }

            $scope.close = function () {

                $scope.edt = [];

                $scope.Show();
            }

            $scope.Fetchdata = function (cmp_cd, docd, final_doc_no) {
                $scope.docCode1 = docd;
                $scope.provNo1 = final_doc_no;

                $('#myModal4').modal('show')
                //$scope.prdocdata = [];

                $http.get(ENV.apiUrl + "api/VoucherReversal/getAllRecords?doccode=" + docd + "&prnumber=" + final_doc_no + "&comp_code=" + cmp_cd).then(function (prdoc_data) {

                    $scope.prdocdata = prdoc_data.data;
                    // $scope.block1 = [];
                    $scope.temp_doc = [];
                    $scope.totalDebit = 0;
                    $scope.totalcredit = 0;

                    for (var i = 0; i < $scope.prdocdata.length; i++) {

                        if (i == 0) {
                            $scope.temp_doc.gltd_comp_code = $scope.prdocdata[i].gltd_comp_code;
                            $scope.temp_doc.gltd_doc_code = $scope.prdocdata[i].gltd_doc_code;

                            $scope.temp_doc.gltd_doc_narr = $scope.prdocdata[i].gltd_doc_narr;
                            $scope.temp_doc.gltd_remarks = $scope.prdocdata[i].gltd_remarks;
                            $scope.temp_doc.gltd_doc_date = $scope.prdocdata[i].gltd_doc_date;
                            $scope.temp_doc.gltd_post_date = $scope.prdocdata[i].gltd_post_date;
                            $scope.temp_doc.gltd_prov_doc_no = $scope.prdocdata[i].gltd_prov_doc_no;
                            $scope.temp_doc.gltd_final_doc_no = final_doc_no;

                            $scope.temp_doc.gldu_authorize = $scope.prdocdata[i].gldu_authorize;
                            $scope.temp_doc.gldu_verify = $scope.prdocdata[i].gldu_verify;
                            $scope.temp_doc.gltd_authorize_date = $scope.prdocdata[i].gltd_authorize_date;
                            $scope.temp_doc.gltd_verify_date = $scope.prdocdata[i].gltd_verify_date;

                        }
                        $scope.cost = $scope.cost || ($scope.prdocdata[i].gldd_cost_center_code != '' || $scope.prdocdata[i].gldd_cost_center_code != "");
                        $scope.totalDebit = parseFloat($scope.totalDebit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_debit);
                        $scope.totalcredit = parseFloat($scope.totalcredit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_credit);

                    }

                });

            }

            $scope.maxmize = function () {
                $('#jv_Voucher').css({ 'height': '100%', 'width': '100%' })
                $scope.windowflg = true;
            }

            $scope.manimize = function () {
                $('#jv_Voucher').css({ 'height': '600px', 'width': '900px' })
                $scope.windowflg = false;

            }


            $scope.CheckPrintReport = function () {


                var data = {
                    location: $scope.final_doc_url,
                    parameter: {
                        comp_detail: comp_code,
                        doc_code: $scope.temp_doc.gltd_doc_code,
                        doc_no: $scope.temp_doc.gltd_final_doc_no,

                    },

                    state: 'main.Fin209'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')


            }

            $http.get(ENV.apiUrl + "api/BankPayment/GetLedgerNumber?financialyear=" + finance_year + "&comp_code=" + comp_code).then(function (docstatus2) {
                $scope.LdgrCode = docstatus2.data;
            });
            $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {

                $scope.getAllAccNos = docstatus.data;
            });

            $scope.getSLAccNo = function () {


                var acconcode = $scope.getAllAccNos.gldd_acct_code;
                if ($scope.block3.gldd_ledger_code != "00") {

                    $http.get(ENV.apiUrl + "api/JVCreation/GetSLAccNumber?ldgr_code=" + $scope.block3.gldd_ledger_code + "&cmp_cd=" + comp_code + "&year=" + finance_year).then(function (docstatus3) {
                        // $scope.sllc_ldgr_code = slcode;
                        $scope.getAllAccNos = docstatus3.data;
                        //$scope.slacno = docstatus3.data;
                    });
                }
                else {
                    $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {

                        $scope.getAllAccNos = docstatus.data;
                    });
                }


            }
            $scope.Add_inner_grid = function () {
                //code sgr
                $scope.chkcost = false;
                $scope.cost_combo = false;
                $scope.temp = { cost_chk_ac: false }

                var ledcode = "";
                // if (myForm) {
                if ($scope.block3.gldd_acct_code != undefined) {

                    if ($scope.block3.gldd_ledger_code == undefined) {
                        ledcode = '00';
                    }
                    else {
                        ledcode = $scope.block3.gldd_ledger_code;
                    }

                    if ($scope.block3.gldd_doc_amount_debit == "" || $scope.block3.gldd_doc_amount_debit == undefined || $scope.block3.gldd_doc_amount_debit == '0.00') {
                        $scope.block3.gldd_doc_amount_debit = '0.00';
                    }
                    if ($scope.block3.gldd_doc_amount_credit == "" || $scope.block3.gldd_doc_amount_credit == undefined || $scope.block3.gldd_doc_amount_credit == '0.00') {
                        $scope.block3.gldd_doc_amount_credit = '0.00';
                    }

                    if (parseFloat($scope.block3.gldd_doc_amount_debit) <= 0 && parseFloat($scope.block3.gldd_doc_amount_credit) <= 0) {
                        swal({ text: "Please Enter Debit/Credit Amount", width: 380 });
                        return;
                    }
                    var terminal = document.getElementById("cmb_acc_Code3");
                    $scope.selectedText = terminal.options[terminal.selectedIndex].text;
                    var found = false;

                    for (var j = 0; j < $scope.prdocdata.length; j++) {
                        if ($scope.prdocdata[j].gldd_acct_code == $scope.block3.gldd_acct_code && $scope.prdocdata[j].gldd_doc_narr == $scope.block3.gldd_doc_narr) {
                            found = true;
                            break;
                        }
                    }
                    if (found == true) {
                        swal({ text: $scope.block3.gldd_acct_name + " A/c Already Added", width: 380 });
                    }
                    else {
                        //code sgr
                        var costcenter1 = document.getElementById("costCenter")
                        var costcentername = costcenter1.options[costcenter1.selectedIndex].text;
                        $scope.center = $scope.center || ($scope.block2.glco_cost_centre_code != '')

                        //if ($scope.block3.gldd_include_vat == false) {
                        //    if ($scope.block3.gldd_doc_amount_debit != 0) {
                        //        $scope.checkevaluesdebit();
                        //    }
                        //    else {
                        //        $scope.checkevaluescredit();
                        //    }
                        //}
                        if (comp_vat_status == 'Y' && $scope.block3.gldd_include_vat == true && $scope.block3.gldd_vat_enable == true) {

                            if ($scope.block3.gldd_doc_amount_debit != 0) {
                                var vat_deb_amt = parseFloat($scope.block3.gldd_doc_amount_debit);
                                $scope.block3.gldd_doc_vat_amt = parseFloat(vat_deb_amt * parseFloat(vat_percent) / (100 + parseFloat(vat_percent))).toFixed(2);
                                $scope.block3.gldd_doc_amount_debit = parseFloat(vat_deb_amt) - parseFloat(vat_deb_amt * parseFloat(vat_percent) / (100 + parseFloat(vat_percent))).toFixed(2);
                                //$scope.gldd_doc_vat_amt = 0;
                                //$scope.block3.gldd_include_vat = true;
                            }
                            else {
                                var vat_cre_amt = parseFloat($scope.block3.gldd_doc_amount_credit);
                                $scope.block3.gldd_doc_vat_amt = parseFloat(vat_cre_amt * parseFloat(vat_percent) / (100 + parseFloat(vat_percent))).toFixed(2);
                                $scope.block3.gldd_doc_amount_credit = parseFloat(vat_cre_amt) - parseFloat(vat_cre_amt * parseFloat(vat_percent) / (100 + parseFloat(vat_percent))).toFixed(2);
                                //$scope.block3.gldd_include_vat = true;
                            }

                        }

                        $scope.prdocdata.push({
                            'gldd_comp_code': comp_code,
                            //'gldd_doc_code': $scope.edt.gltd_doc_code,
                            'gldd_ledger_code': ledcode,
                            'gldd_acct_code': $scope.block3.gldd_acct_code,
                            'gldd_acct_name': $scope.block3.gldd_acct_name,
                            'gldd_dept_code': $scope.block3.gldd_dept_code,
                            'gldd_party_ref_no': $scope.block3.gldd_party_ref_no,
                            'gldd_doc_narr': $scope.block3.gldd_doc_narr,

                            'gldd_doc_amount_debit': $scope.block3.gldd_doc_amount_debit,
                            'gldd_doc_amount_credit': $scope.block3.gldd_doc_amount_credit,
                            'glco_cost_centre_code': $scope.block2.glco_cost_centre_code,
                            'coce_cost_centre_name': costcentername,
                        });


                        if (comp_vat_status == 'Y' && $scope.block3.gldd_vat_enable == true) {
                            if (vat_ac_name == '')
                                for (var i = 0; i < $scope.getAllAccNos.length; i++) {
                                    if ($scope.getAllAccNos[i].gldd_acct_code == input_vat_acct) {
                                        vat_ac_name = $scope.getAllAccNos[i].gldd_acct_name;
                                    }
                                }

                            $scope.prdocdata.push({

                                'gldd_comp_code': comp_code,
                                //'gldd_doc_code': $scope.edt.gltd_doc_code,
                                'gldd_ledger_code': ledcode,
                                'gldd_acct_code': input_vat_acct,
                                'gldd_acct_name': vat_ac_name,
                                'gldd_dept_code': input_vat_acct.substr(1, 2),
                                'gldd_party_ref_no': '',
                                'gldd_doc_narr': 'Vat amount' + ' - ' + $scope.block3.gldd_acct_name,

                                'gldd_doc_amount_debit': ($scope.block3.gldd_doc_amount_debit != 0) ? $scope.block3.gldd_doc_vat_amt : 0,
                                'gldd_doc_amount_credit': ($scope.block3.gldd_doc_amount_credit != 0) ? $scope.block3.gldd_doc_vat_amt : 0,

                                'glco_cost_centre_code': $scope.block2.glco_cost_centre_code,
                                'coce_cost_centre_name': costcentername,
                            });
                        }

                        $scope.totalDebit = 0;
                        $scope.totalcredit = 0;

                        for (var i = 0; i < $scope.prdocdata.length; i++) {

                            $scope.totalDebit = parseFloat(parseFloat($scope.totalDebit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_debit)).toFixed(3);

                            $scope.totalcredit = parseFloat(parseFloat($scope.totalcredit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_credit)).toFixed(3);
                        }
                        //$scope.btnDelete = true;


                        //$scope.edt = "";
                        $scope.edt1 = "";
                        $scope.edt2 = "";
                        $scope.block3.gldd_doc_vat_amt = 0;
                        $("#cmb_acc_Code3").select2("val", "");
                        // $scope.edt.slma_acno = '';
                        $scope.myForm.$setPristine();
                        $scope.myForm.$setUntouched();
                        // Bankdetails = [];
                        $scope.dcode = false;

                        $scope.cancel();
                    }
                }
                else {
                    swal({ text: "  please select Account Code ", width: 380 });

                }
                var acconcode = $scope.getAllAccNos.gldd_acct_code;
                $scope.GetAllGLAcc(acconcode, comp_code);
            };

            $scope.GetAllGLAcc = function (acconcode, cmpnycode) {
                $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {

                    $scope.getAllAccNos = docstatus.data;
                });


            }

            $scope.UpdateData = function () {
                debugger;
               
                var dataSend = [];
                var j = 1;
                console.log($scope.prdocdata.length);
               
                if ($scope.totalDebit != $scope.totalcredit) {
                    swal({ text: "Debit Amount: " + $scope.totalDebit + "  Is Not Equal To Credit Amount: " + $scope.totalcredit, width: 380 });

                }
                else {



                    for (var i = 0; i < $scope.prdocdata.length; i++) {
                        var alldata = {
                            gldd_comp_code: $scope.prdocdata[i].gldd_comp_code,
                            gldd_doc_code: $scope.prdocdata[i].gldd_doc_code,
                            gltd_prov_doc_no: $scope.prdocdata[i].gltd_prov_doc_no,
                            gldd_final_doc_no: $scope.prdocdata[i].gldd_final_doc_no,
                            gldd_line_no: j,
                            gldd_doc_narr: $scope.prdocdata[i].gldd_doc_narr,
                            gldd_ledger_code: $scope.prdocdata[i].gldd_ledger_code,
                            gldd_acct_code: $scope.prdocdata[i].gldd_acct_code,
                            gldd_doc_amount: $scope.prdocdata[i].gldd_doc_amount,
                            gldd_fc_code: $scope.prdocdata[i].gldd_fc_code,
                            gldd_fc_rate: $scope.prdocdata[i].gldd_fc_rate,
                            gldd_fc_amount: $scope.prdocdata[i].gldd_fc_amount,
                            gldd_dept_code: $scope.prdocdata[i].gldd_dept_code,
                            gldd_party_ref_no: $scope.prdocdata[i].gldd_party_ref_no,
                            gldd_party_ref_date: $scope.prdocdata[i].gldd_party_ref_date,
                            gldd_bank_date: $scope.prdocdata[i].gldd_bank_date,
                            gldd_cost_center_code: $scope.prdocdata[i].gldd_cost_center_code,
                            gldd_bank_code: $scope.prdocdata[i].gldd_bank_code,

                        }
                        j++;
                        dataSend.push(alldata);

                    }

                    $http.post(ENV.apiUrl + "api/UpdateVoucherPosted/InsertVoucherDetails", dataSend).then(function (res) {
                        debugger;
                        $scope.result = res.data;
                        if ($scope.result == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });


                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                    });

                    //swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });



                }

                /*var dataSend = [];
                var j = 1;
                if ($scope.totalDebit != $scope.totalcredit) {
                    swal({ text: "Debit Amount: " + $scope.totalDebit + "  Is Not Equal To Credit Amount: " + $scope.totalcredit, width: 380 });

                }
                else {
                   
                    for (var i = 0; i < $scope.prdocdata.length; i++) {
                        var data = {
                            gltd_doc_code: $scope.prdocdata[i].gltd_doc_code,
                            gltd_doc_date: $scope.prdocdata[i].gltd_doc_date,
                            gltd_post_date: $scope.prdocdata[i].gltd_post_date,
                            gltd_remarks: $scope.prdocdata[i].gltd_remarks,
                            gltd_doc_narr: $scope.prdocdata[i].gltd_doc_narr,
                            gltd_comp_code: $scope.prdocdata[i].gltd_comp_code,
                            gltd_prepare_date: $scope.prdocdata[i].gltd_doc_date,
                            gltd_verify_date: $scope.prdocdata[i].gltd_verify_date,
                            gltd_authorize_date: $scope.prdocdata[i].gltd_authorize_date,
                            gltd_paid_to: $scope.prdocdata[i].gltd_paid_to,
                            gltd_cheque_no: $scope.prdocdata[i].gltd_cheque_No,
                            gltd_prov_doc_no: $scope.prdocdata[i].gltd_prov_doc_no,
                            user_name: user,
                            finance_year: finance_year,

                            gldd_doc_narr: $scope.prdocdata[i].gldd_doc_narr,
                            gldd_dept_code: $scope.prdocdata[i].gldd_dept_code,
                            gldd_ledger_code: $scope.prdocdata[i].gldd_ledger_code,
                            gldd_acct_code: $scope.prdocdata[i].gldd_acct_code,
                            gldd_party_ref_no: $scope.prdocdata[i].gldd_party_ref_no,
                            gldd_cost_center_code: $scope.prdocdata[i].gldd_cost_center_code,
                            gldd_doc_amount_debit: $scope.prdocdata[i].gldd_doc_amount_debit,
                            gldd_doc_amount_credit: $scope.prdocdata[i].gldd_doc_amount_credit,
                            gldd_comp_code: $scope.prdocdata[i].gldd_comp_code,
                            gldd_doc_code: $scope.prdocdata[i].gldd_doc_code,
                            gldd_final_doc_no: $scope.prdocdata[i].gldd_final_doc_no,
                            gldd_line_no: j,

                        }
                        j++;
                        dataSend.push(data);

                    }
                    console.log(dataSend);
                    $http.post(ENV.apiUrl + "api/UpdateVoucherPosted/UpdatePostedVoucher", dataSend).then(function (res) {
                        debugger;
                        $scope.result = res.data;
                        if ($scope.result == true) {
                            swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });


                        }
                        else {
                            swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                    });
                }*/
                
            }

            $scope.update_grid_data = function () {

                debugger;
                console.log($scope.old_gldd_acccount_code);
                $scope.temp_obj = $scope.block3;
               
                if ($scope.block3.gldd_doc_amount_debit == "" || $scope.block3.gldd_doc_amount_debit == undefined || $scope.block3.gldd_doc_amount_debit == '0.00') {
                    $scope.block3.gldd_doc_amount_debit = '0.00';
                }
                if ($scope.block3.gldd_doc_amount_credit == "" || $scope.block3.gldd_doc_amount_credit == undefined || $scope.block3.gldd_doc_amount_credit == '0.00') {
                    $scope.block3.gldd_doc_amount_credit = '0.00';
                }

                if (parseFloat($scope.block3.gldd_doc_amount_debit) <= 0 && parseFloat($scope.block3.gldd_doc_amount_credit) <= 0) {
                    swal({ text: "Please Enter Debit/Credit Amount", width: 380 });
                    return;
                }

                $scope.totalDebit = 0;
                $scope.totalcredit = 0;
                for (var i = 0; i < $scope.prdocdata.length; i++) {

                    if ($scope.prdocdata[i].$$hashKey == $scope.tempobj.$$hashKey) {
                        $scope.prdocdata[i].gldd_doc_amount_credit = $scope.block3.gldd_doc_amount_credit
                        $scope.prdocdata[i].gldd_doc_amount_debit = $scope.block3.gldd_doc_amount_debit;
                        $scope.prdocdata[i].gldd_doc_narr = $scope.block3.gldd_doc_narr;
                    }
                    $scope.totalDebit = parseFloat(parseFloat($scope.totalDebit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_debit)).toFixed(3);
                    $scope.totalcredit = parseFloat(parseFloat($scope.totalcredit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_credit)).toFixed(3);
                }

                var data = {
                    gldd_doc_code: $scope.block3.gldd_doc_code,
                    gldd_comp_code: $scope.block3.gldd_comp_code,
                    gldd_acct_code: $scope.block3.gldd_acct_code,
                    gldd_ledger_code:$scope.block3.gldd_ledger_code,
                    gldd_final_doc_no: $scope.block3.gldd_final_doc_no,
                    old_gldd_acct_code: $scope.old_gldd_acccount_code,
                    gldd_party_ref_no: $scope.block3.gldd_party_ref_no,
                    gldd_doc_narr:$scope.block3.gldd_doc_narr,
                    gltd_comp_code: $scope.temp_doc.gltd_comp_code,
                    gltd_doc_code: $scope.temp_doc.gltd_doc_code,
                    gltd_final_doc_no: $scope.temp_doc.gltd_final_doc_no,
                    gltd_doc_date: $scope.voucher.gltd_doc_date,
                    gltd_post_date: $scope.voucher.gltd_post_date,
                    gltd_remarks: $scope.voucher.gltd_remarks,
                    gltd_doc_narr: $scope.voucher.gltd_doc_narr,
                    user_name: user
                }
                console.log(data);

               $http.post(ENV.apiUrl + "api/UpdateVoucherPosted/UpdateAcctCode", data).then(function (msg) {
                    debugger;
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 ==true) {
                        $('#updatVoucher').modal('hide');
                        swal({ title: "Alert", text: "Updated Successfully", showCloseButton: true, width: 300, height: 200 });
                        $('#cmb_acc_Code3').select2("val","");
                   
                    }
                    else {
                        swal({ title: "Alert", text: "Not Updated", width: 300, height: 200 });

                    }

                });
                
                $scope.cancel();
            }
            $scope.Delete_Grid = function (item, $event, index, str) {


                $scope.totalDebit = parseFloat(parseFloat($scope.totalDebit) - parseFloat(str.gldd_doc_amount_debit)).toFixed(3);
                $scope.totalcredit = parseFloat(parseFloat($scope.totalcredit) - parseFloat(str.gldd_doc_amount_credit)).toFixed(3);

                item.splice(index, 1);


            }

            $scope.update_grid = function (obj) {
                debugger;
                $scope.old_gldd_acccount_code = obj.gldd_acct_code;
                $scope.vis.main_acct_grid = true;

                $scope.tempobj = obj;
                $scope.vis.updatebtn = true;
                $scope.vis.add_grid = false;
                $scope.vis.cmb_acct = false;
                $scope.vis.txt_acct = true;
                $scope.ldgcd = true;


                $scope.block3 = JSON.parse(JSON.stringify(obj));


                $scope.getSLAccNo();

            }
            $scope.getDepartcode = function (str) {

                //Code sgr

                $http.get(ENV.apiUrl + "api/BankPayment/getCostCenter?aacno=" + str + "&comp_cd=" + comp_code + "&fyear=" + finance_year).then(function (cost_center) {

                    $scope.cost_center = cost_center.data;
                    if ($scope.cost_center.length > 0) {
                        $scope.chkcost = true;
                    }
                    else {
                        $scope.chkcost = false;
                        $scope.temp.cost_chk = false;
                        $scope.cost_combo = false;
                    }
                });


                for (var i = 0; i < $scope.getAllAccNos.length; i++) {
                    if ($scope.getAllAccNos[i].gldd_acct_code == $scope.block3.gldd_acct_code) {

                        $scope.block3.gldd_dept_code = $scope.getAllAccNos[i].gldd_dept_code;
                        $scope.block3.gldd_party_ref_no = $scope.getAllAccNos[i].gldd_party_ref_no;
                        $scope.block3.codp_dept_name = $scope.getAllAccNos[i].gldd_dept_name;
                        $scope.block3.gldd_acct_name = $scope.getAllAccNos[i].gldd_acct_name;

                    }
                }



            }
            $scope.cancel = function () {

                $scope.vis.updatebtn = false;
                $scope.vis.add_grid = true;
                $scope.cramount = false;
                $scope.dramount = false;
                $scope.vis.cmb_acct = true;
                $scope.vis.txt_acct = false;
                $scope.ldgcd = false;
                $scope.chkcost = false;
                $scope.cost_combo = false;
                $scope.block3 = [];
                $scope.block3.gldd_include_vat = false;
                $scope.block3.gldd_vat_enable = false;


                if ($scope.cmb_doc_cd.gldc_doc_code_type == "CN") {
                    $scope.vis.main_acct_grid = false;

                }
            }
            $scope.Print = function (data_obj) {


                var data = {
                    location: $scope.final_doc_url,
                    parameter: {
                        comp_detail: comp_code,
                        doc_code: data_obj.gltd_doc_code,
                        doc_no: data_obj.gltd_final_doc_no,

                    },

                    state: 'main.UVP01'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')


            }

            
           


        }]
        )
})();
