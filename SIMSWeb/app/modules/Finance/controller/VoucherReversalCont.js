﻿(function () {
    'use strict';
    var main;
    var prdocdata = [];
    var data1 = [];
    var data = [], temp_doc = [];
    //var totalDebit=0;
    //var totalcredit=0;

    var simsController = angular.module('sims.module.Finance');
    simsController.controller('VoucherReversalCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '5';
            $scope.edt = [];
            $scope.block1 = {};
            $scope.geDocCode_new = [];
            $scope.operation = true;
            $scope.table1 = true;
            $scope.save1 = true;
            $scope.Add = true;
            $scope.update = false;
            $scope.Update = false;
            $scope.PostingDateShow = false;
            var user = $rootScope.globals.currentUser.username;

            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            $scope.btn_reverse = true;
            //$scope.btn_verify = true;
            $scope.prdocdata = [];
            $scope.temp_doc = [];
            //$scope.schoolUrl = window.location.href;
            //var conStr = $scope.schoolUrl.substring(0, $scope.schoolUrl.indexOf('#'));
           

            $timeout(function () {
                $("#fixedtable").tableHeadFixer({ 'top': 1 });
                $("#fixedtable1").tableHeadFixer({ 'top': 1 });
            }, 100);


            $scope.SearchVouReverData = [];
            $scope.ref_docprvno = '';
            $scope.ref_final_doc_no = '';
            $scope.new_prov_data = '';
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                //format: 'yyyy-mm-dd'
                format: 'dd-mm-yyyy'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $http.get(ENV.apiUrl + "api/GLSLQuery/getDocCode_GL_Query?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {

                $scope.getDocCode = res.data;
            });

            $http.get(ENV.apiUrl + "api/JVCreation/GetReportUrl?opr=J&sub_opr=Single").then(function (res) {
                $scope.final_doc_url = res.data;
              
            });

            $http.get(ENV.apiUrl + "api/JVCreation/getDocCodeJV?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (res) {
                debugger;
                $scope.doc_codes = res.data;

                for (var i = 0; i < $scope.doc_codes.length; i++) {
                    if ($scope.doc_codes[i].gldc_doc_code_type == "RV") {
                        $scope.geDocCode_new.push($scope.doc_codes[i]);
                    }
                }
                if ($scope.geDocCode_new.length > 1) {
                    $scope.dis_doc_cd = false;
                    $scope.lbl_recur = $scope.geDocCode_new[0].gltd_doc_code;
                }
                else if ($scope.geDocCode_new.length == 1) {
                    $scope.dis_doc_cd = true;
                    $scope.lbl_recur = $scope.geDocCode_new[0].gltd_doc_code;
                }

            });

          
            $scope.ContraEntry = function (vcrrev) {
                $('#myModal_ContraEntry').modal('show');

                
                //$scope.btn_reverse = false;
                //$scope.btn_verify = true;
                $scope.ref_docprvno = '';
                $scope.ref_final_doc_no = '';
                //$scope.lbl_recur = '';
                $scope.postdate = vcrrev.gltd_post_date;
               
                $http.get(ENV.apiUrl + "api/VoucherReversal/Get_vouchers_data?doc_cd=" + vcrrev.gltd_doc_code + "&final_doc_no=" + vcrrev.gltd_final_doc_no + "&usrname=" + user +
                   "&comp_code=" + comp_code + "&fyear=" + finance_year).then(function (voucher_data) {
              
                       $http.get(ENV.apiUrl + "api/VoucherReversal/GetPostingDateParam?comp_code=" + comp_code + "&year=" + finance_year).then(function (res) {

                       $scope.ref_docprvno = vcrrev.gltd_prov_doc_no;
                       $scope.ref_final_doc_no = vcrrev.gltd_final_doc_no;
                       //$scope.lbl_recur = 'RV';

                      
                       
                       
                       $scope.lbl_rev_doc = vcrrev.gltd_doc_code + '-' + vcrrev.gltd_final_doc_no;

                       $scope.SearchVouReverData = voucher_data.data;
                      
                       $scope.edt = {
                           //gltd_postdate: vcrrev.gltd_post_date,
                           //gltd_docdate: yyyy + '-' + mm + '-' + dd,
                           gltd_postdate: dd + '-' + mm + '-' + yyyy,
                           gltd_docdate: dd + '-' + mm + '-' + yyyy,
                           //gltd_docdate: vcrrev.gltd_docdate,
                           gltd_doc_narr: vcrrev.gltd_doc_narr,
                           gltd_remarks: vcrrev.gltd_remarks,
                           gltd_doc_code: vcrrev.gltd_doc_code
                       }
                       $scope.PostingDate = res.data;
                       if ($scope.PostingDate.length > 0) {
                           if ($scope.PostingDate[0].sims_appl_parameter == 'Y') {
                               $scope.PostingDateShow = true;
                               $scope.edt['gltd_postdate'] = $scope.postdate;
                           }
                           else {
                               $scope.PostingDateShow = false;
                               $scope.edt['gltd_postdate'] = dd + '-' + mm + '-' + yyyy;

                           }
                       }
                       else {
                           $scope.edt['gltd_postdate'] = dd + '-' + mm + '-' + yyyy;
                       }

                       $scope.totalDebit = 0;
                       $scope.totalcredit = 0;
                       for (var i = 0; i < $scope.SearchVouReverData.length; i++) {
                           $scope.totalDebit = parseFloat($scope.totalDebit) + parseFloat($scope.SearchVouReverData[i].gldd_doc_amount_debit);
                           $scope.totalcredit = parseFloat($scope.totalcredit) + parseFloat($scope.SearchVouReverData[i].gldd_doc_amount_credit);
                       }

                       $scope.totalDebit = parseFloat($scope.totalDebit).toFixed(3);
                       $scope.totalcredit = parseFloat($scope.totalcredit).toFixed(3);

                       $scope.amountdifference = $scope.totalcredit - $scope.totalDebit;

                       $scope.check_post_date();
                   });

                   
                   
                    
                    
                });

              
               
            }


            $scope.Show = function () {
                
                //if (doc_prov_no == undefined)
                //    doc_prov_no = 0;
                //from_date = $scope.edt.from_date;
                //to_date = $scope.edt.to_date;
                $('#loader').modal({ backdrop: 'static', keyboard: false });

                $http.get(ENV.apiUrl + "api/VoucherReversal/getAllVouchers?comp_cd=" + comp_code + "&doc_cd=" + $scope.block1.gltd_doc_code + "&final_doc_no=" + $scope.block1.gltd_final_doc_no +
                    "&from_date=" + $scope.block1.from_date + "&to_date=" + $scope.block1.to_date + "&cheque_no=" + $scope.block1.gltd_cheque_no).then(function (VouRever_Data) {
                        $scope.verify_data = VouRever_Data.data;
                       

                        $('#loader').modal('hide');
                        $(".modal-backdrop").removeClass("modal-backdrop");
                    });
            }

            $scope.RevertVoucher = function () {
                if ($scope.totalDebit == $scope.totalcredit) {

                    debugger;

                    $scope.btn_reverse = true;

                    $('#loader').modal({ backdrop: 'static', keyboard: false });

                    var data = {};
                    data.gltd_comp_code = comp_code;
                    data.gltd_doc_code = $scope.lbl_recur;
                    data.gltd_doc_date = $scope.edt.gltd_docdate;
                    data.gltd_post_date = $scope.edt.gltd_postdate;
                    data.gltd_doc_narr = $scope.edt.gltd_doc_narr;
                    data.gltd_remarks = $scope.edt.gltd_remarks;
                    data.gltd_prepare_user = user;
                    data.gltd_reference_doc_code = $scope.edt.gltd_doc_code;
                    data.gltd_reference_final_doc_no = $scope.ref_final_doc_no;

                    data.detailsdata = $scope.SearchVouReverData;

                    console.log(data);

                    $http.post(ENV.apiUrl + "api/VoucherReversal/Insert_ReverseVoucher", data).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 != "") {
                            swal({ title: "Alert", text: "Voucher Reversed Successfully\n Final Doc No.- " + $scope.msg1, showCloseButton: true, width: 300, height: 200 });
                            
                            $scope.btn_reverse = true;

                            $('#myModal_ContraEntry').modal('hide');
                            $scope.Show();
                        }
                        else {
                            swal({ title: "Alert", text: "Voucher Not Reversed", width: 300, height: 200 });
                            
                                    $scope.btn_reverse = false;
                                    //$scope.btn_verify = true;
                        }

                        $('#loader').modal('hide');
                        $(".modal-backdrop").removeClass("modal-backdrop");
                    });
                }
                else {
                    swal({ title: "Alert", text: "Debit and Credit amount do not match.Cannot Revert Voucher.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.btn_reverse = false;
                            //$scope.btn_verify = true;
                        }
                    });

                }
            }

            $scope.close = function () {
                
                $scope.edt = [];

                $scope.Show();
            }

            $scope.Fetchdata = function (cmp_cd, docd, final_doc_no) {
                $scope.docCode1 = docd;
                $scope.provNo1 = final_doc_no;
               
                $('#myModal4').modal('show')
                //$scope.prdocdata = [];

                $http.get(ENV.apiUrl + "api/VoucherReversal/getAllRecords?doccode=" + docd + "&prnumber=" + final_doc_no + "&comp_code=" + cmp_cd).then(function (prdoc_data) {

                    $scope.prdocdata = prdoc_data.data;
                   // $scope.block1 = [];
                    $scope.temp_doc = [];
                    $scope.totalDebit = 0;
                    $scope.totalcredit = 0;

                    for (var i = 0; i < $scope.prdocdata.length; i++) {

                        if (i == 0) {
                            $scope.temp_doc.gltd_comp_code = $scope.prdocdata[i].gltd_comp_code;
                            $scope.temp_doc.gltd_doc_code = $scope.prdocdata[i].gltd_doc_code;

                            $scope.temp_doc.gltd_doc_narr = $scope.prdocdata[i].gltd_doc_narr;
                            $scope.temp_doc.gltd_remarks = $scope.prdocdata[i].gltd_remarks;
                            $scope.temp_doc.gltd_doc_date = $scope.prdocdata[i].gltd_doc_date;
                            $scope.temp_doc.gltd_post_date = $scope.prdocdata[i].gltd_post_date;
                            $scope.temp_doc.gltd_prov_doc_no = $scope.prdocdata[i].gltd_prov_doc_no;
                            $scope.temp_doc.gltd_final_doc_no = final_doc_no;

                            $scope.temp_doc.gldu_authorize = $scope.prdocdata[i].gldu_authorize;
                            $scope.temp_doc.gldu_verify = $scope.prdocdata[i].gldu_verify;
                            $scope.temp_doc.gltd_authorize_date = $scope.prdocdata[i].gltd_authorize_date;
                            $scope.temp_doc.gltd_verify_date = $scope.prdocdata[i].gltd_verify_date;

                        }
                        $scope.cost = $scope.cost || ($scope.prdocdata[i].gldd_cost_center_code != '' || $scope.prdocdata[i].gldd_cost_center_code != "");
                        $scope.totalDebit = parseFloat($scope.totalDebit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_debit);
                        $scope.totalcredit = parseFloat($scope.totalcredit) + parseFloat($scope.prdocdata[i].gldd_doc_amount_credit);

                    }

                });

            }

            $scope.maxmize = function () {
                $('#jv_Voucher').css({ 'height': '100%', 'width': '100%' })
                $scope.windowflg = true;
            }

            $scope.manimize = function () {
                $('#jv_Voucher').css({ 'height': '600px', 'width': '900px' })
                $scope.windowflg = false;

            }


            $scope.CheckPrintReport = function () {


                var data = {
                    location: $scope.final_doc_url,
                    parameter: {
                        comp_detail: comp_code,
                        doc_code: $scope.temp_doc.gltd_doc_code,
                        doc_no: $scope.temp_doc.gltd_final_doc_no,

                    },

                    state: 'main.Fin209'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')


            }


            $scope.Print = function (data_obj) {
                debugger;
                $('#displayReport').modal('show')
                
                var data = {
                    location: $scope.final_doc_url,
                    parameter: {
                        comp_detail: comp_code,
                        doc_code: data_obj.gltd_doc_code,
                        doc_no: data_obj.gltd_final_doc_no,

                    },

                    state: 'main.Fin209'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data);
               // $state.go('main.ReportCardParameter');


                try {
                    $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                    $scope.location = $scope.rpt.location;
                    $scope.parameter = $scope.rpt.parameter;
                    $scope.state = $scope.rpt.state;
                }
                catch (ex) {
                }
                var s;

                //if ($http.defaults.headers.common['schoolId'] == 'dpsmis')
                //    s = "SimsReports." + $scope.location + ",SimsReports";


                //else
                //    s = "SimsReports." + $scope.location + ",SimsReports" + $http.defaults.headers.common['schoolId'];


                //  window.localStorage["Finn_comp"] = JSON.stringify(data)

                s = "SimsReports." + $scope.location + ",SimsReports";

                var url = window.location.href;
                var domain = url.substring(0, url.indexOf(':'))
                if ($http.defaults.headers.common['schoolId'] == 'sms') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                }
                else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc' || $http.defaults.headers.common['schoolId'] == 'christ') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'imert') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.co.in/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                }

                else {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                }
                // console.log(service_url);


                $scope.parameters = {}

                $("#reportViewer1")
                               .telerik_ReportViewer({
                                   //serviceUrl: ENV.apiUrl + "api/reports/",
                                   serviceUrl: service_url,

                                   viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                   scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                   // Zoom in and out the report using the scale
                                   // 1.0 is equal to 100%, i.e. the original size of the report
                                   scale: 1.0,
                                   ready: function () {
                                       //this.refreshReport();
                                   }

                               });





                var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                reportViewer.reportSource({
                    report: s,
                    parameters: $scope.parameter,
                });

                //var rv = $("#reportViewer1").data("telerik_ReportViewer");
                //rv.commands.print.exec();

                setInterval(function () {

                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                    //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });

                }, 1000);


                $timeout(function () {
                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                    //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });


                }, 100)
                
            }


            $scope.check_post_date = function () {
                if ($scope.edt.gltd_postdate != undefined || $scope.edt.gltd_postdate != '') {

                    $http.get(ENV.apiUrl + "api/JVCreation/Get_Check_Postdate?post_date=" + $scope.edt.gltd_postdate + "&comp_cd=" + comp_code + "&fyear=" + finance_year).then(function (pronum) {
                        $scope.msg = pronum.data;

                        if ($scope.msg != '') {
                            swal({ text: $scope.msg, width: 300, height: 200 });
                            $scope.btn_reverse = true;
                        }
                        else {
                            $scope.btn_reverse = false;
                        }

                    });

                }

            }
           

           
        }]
        )
})();
