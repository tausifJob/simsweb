﻿(function () {
    'use strict';
    var obj1, obj2, main;
    var opr = '';
    var departmentcode = [];
    var formdata = new FormData();
    var data1 = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CreateEditDepartmentCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.DepartmentDetail = true;
            $scope.editmode = false;
            $scope.edt = "";


            var user = $rootScope.globals.currentUser.username;
            user = user.toLowerCase();
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
          //  var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);
            $scope.size = function (str) {
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                // $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.DepartData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.check_all();
            }

            $http.get(ENV.apiUrl + "api/CreateEditDepartment/getDepartType").then(function (res) {
                $scope.DepartType = res.data;
            });



            $http.get(ENV.apiUrl + "api/common/getCompanyCurrency?comp_code=" + comp_code).then(function (res1) {
                $scope.comp_currency = res1.data;
              
            });


            $scope.edt = {};
            $http.get(ENV.apiUrl + "api/CreateEditDepartment/getCompany").then(function (res) {
                $scope.getCompany = res.data;
                if ($scope.getCompany.length > 0) {
                    $scope.edt.codp_comp_code = $scope.getCompany[0].codp_comp_code;
                }
            });

            $http.get(ENV.apiUrl + "api/CreateEditDepartment/getDepartmentDetail?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (Depart_Data) {
                
                $scope.DepartData = Depart_Data.data;
                $scope.totalItems = $scope.DepartData.length;
                $scope.todos = $scope.DepartData;
                $scope.makeTodos();

                });

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.DepartData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.DepartData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.codp_dept_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.codp_dept_no.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.codp_dept_type.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.codp_short_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                    item.codp_short_name == toSearch) ? true : false;
            }

            $scope.cancel = function () {
                $scope.DepartmentDetail = true;
                $scope.DepartmentOperation = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.myForm.$setPristine();
                    $scope.myForm.$setUntouched();
                    $scope.newmode = true;
                    $scope.check = true;
                    $scope.edt = '';
                    $scope.editmode = false;
                    opr = 'S';
                    $scope.readonly = false;
                    $scope.DepartmentDetail = false;
                    $scope.DepartmentOperation = true;
                    $scope.savebtn = true;
                    $scope.updatebtn = false;
                    $scope.edt = {};
                    // $scope.edt['sims_board_status'] = true;
                    $scope.edt.codp_status = true;
                    if ($scope.getCompany.length > 0) {
                        $scope.edt.codp_comp_code = $scope.getCompany[0].codp_comp_code;
                    }
                }
            }

            $scope.Save = function (myForm) {
                debugger;
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'I';
                    data.codp_comp_code = comp_code;
                    data.codp_year = finance_year;
                    $scope.exist = false;
                    data1.push(data);
                    for (var i = 0; i < $scope.DepartData.length; i++) {
                        if ($scope.DepartData[i].codp_dept_no == data.codp_dept_no && $scope.DepartData[i].codp_dept_name == data.codp_dept_name && $scope.DepartData[i].codp_short_name == data.codp_short_name) {
                            $scope.exist = true;
                        }

                    }
                    if ($scope.exist) {
                        swal({ title: "Alert", text: "Record Already Present", width: 300, height: 200 });
                    }

                    else {

                        $http.post(ENV.apiUrl + "api/CreateEditDepartment/DepartmentDetailCUD?simsobj=", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Inserted Successfully", width: 300, height: 200 });
                            }
                            /*else if ($scope.msg1 == false) {
                                swal({ title: "Alert", text: "Department Number Exceed ", width: 300, height: 200 });
                            }*/
                            else {
                               // swal("Error-" + $scope.msg1)
                                swal({ title: "Alert", text: "Department No. already exists", width: 300, height: 200 });
                                //swal({ title: "Alert", text: "Record not inserted", width: 300, height: 200 });
                            }

                            $scope.DepartmentOperation = false;
                            $http.get(ENV.apiUrl + "api/CreateEditDepartment/getDepartmentDetail?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (Depart_Data) {

                                $scope.DepartData = Depart_Data.data;
                                $scope.totalItems = $scope.DepartData.length;
                                $scope.todos = $scope.DepartData;
                                $scope.makeTodos();
                            });

                        });

                        data1 = [];
                        $scope.DepartmentDetail = true;
                        $scope.DepartmentOperation = false;

                    }

                }


                else {
                    $scope.Update(myForm);

                }




            }


               

            $scope.up = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    opr = 'U';
                    //$scope.edt = str;
                    $scope.editmode = true;
                    $scope.newmode = false;
                    $scope.savebtn = false;
                    $scope.updatebtn = true;
                    $scope.readonly = true;
                    $scope.DepartmentDetail = false;
                    $scope.DepartmentOperation = true;
                    $scope.myForm.$setPristine();
                    $scope.myForm.$setUntouched();
                    $scope.edt = {
                        codp_comp_code: str.codp_comp_code
                             , codp_dept_no: str.codp_dept_no
                             , codp_dept_name: str.codp_dept_name
                             , codp_short_name: str.codp_short_name
                             , codp_dept_type: str.codp_dept_type
                             , codp_year: str.codp_year
                             , codp_status: str.codp_status

                    };
                }
            }

            $scope.Update = function (myForm) {
                debugger;
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'U';
                    data.codp_comp_code = comp_code;
                    data.codp_year = finance_year;
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/CreateEditDepartment/DepartmentDetailCUD", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.DepartmentOperation = false;
                        $http.get(ENV.apiUrl + "api/CreateEditDepartment/getDepartmentDetail?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (Depart_Data) {
                
                            $scope.DepartData = Depart_Data.data;
                            $scope.totalItems = $scope.DepartData.length;
                            $scope.todos = $scope.DepartData;
                            $scope.makeTodos();

                     
                            if ($scope.msg1 == true) {
                                swal({ title: "Alert", text: "Record Updated Successfully", width: 300, height: 200 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({ title: "Alert", text: "Record Not Updated. ", width: 300, height: 200 });
                            }
                            else {
                                swal("Error-" + $scope.msg1)
                            }
                        });

                    })
                    $scope.DepartmentOperation = false;
                    $scope.DepartmentDetail = true;
                    data1 = [];
                }
            }

            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].codp_dept_no);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].codp_dept_no);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.check_once = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }

            }

            $scope.deleterecord = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    var departmentcode = [];
                    var data1 = [];
                    $scope.flag = false;

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].codp_dept_no);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodulecode = ({
                                'codp_dept_no': $scope.filteredTodos[i].codp_dept_no,
                                opr: 'D'
                            });
                            departmentcode.push(deletemodulecode);
                        }
                    }
                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $http.post(ENV.apiUrl + "api/CreateEditDepartment/DepartmentDetailCUD", departmentcode).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ title: "Alert", text: "Record Deleted Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/CreateEditDepartment/getDepartmentDetail?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (Depart_Data) {
                                                    $scope.DepartData = Depart_Data.data;
                                                    $scope.totalItems = $scope.DepartData.length;
                                                    $scope.todos = $scope.DepartData;
                                                    $scope.makeTodos();

                                                });

                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ title: "Alert", text: "Record Already mapped!. ", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/CreateEditDepartment/getDepartmentDetail?comp_code=" + comp_code + "&finance_year=" + finance_year).then(function (Depart_Data) {
                                                    $scope.DepartData = Depart_Data.data;
                                                    $scope.totalItems = $scope.DepartData.length;
                                                    $scope.todos = $scope.DepartData;
                                                    $scope.makeTodos();
                                                });

                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });
                            }
                            else {
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById(i + $scope.filteredTodos[i].codp_dept_no);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ title: "Alert", text: "Please Select Atleast One Record", showCloseButton: true, width: 380, });
                    }
                    data1 = [];
                }
            }

        }]);

})();