﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var is_receipt, payment_mode, from_dt, to_dt, reference_no, bankcode;

    var finanacecode = [];
    var simsController = angular.module('sims.module.Finance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('PDCSubmission_ABQISCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.save_btn = true;
            $scope.tot_btn = true;
            $scope.Maintabledata = false;
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.records = false;
            $scope.edt = [];
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            var user = $rootScope.globals.currentUser.username;
            $scope.finnDetail = JSON.parse(window.localStorage["Finn_comp"]);


            setTimeout(function () {
                $("#account_codes7").select2();
            }, 100)

            var comp_code = $scope.finnDetail.company;
            var finance_year = $scope.finnDetail.year;

            var acconcode = '';
            var dt = new Date();
            $scope.block1 = {
                receipt_date: 'receipt',
                cheque: 'cheque',
                from_date: ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear(),
                to_date: ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear()
            }

            $(function () {
                $('#Select4').multipleSelect({
                    width: '100%'
                });
            });

            //Sorting
            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };


            //Fill ComboBox Status
            $http.get(ENV.apiUrl + "api/Financialyear/getDocumentStatus").then(function (docstatus) {

                $scope.docstatus_Data = docstatus.data;

            });


            //Fill BAnk Name
            $http.get(ENV.apiUrl + "api/PDCSubmission/getBankName?comp_code=" + comp_code).then(function (docstatus1) {
                $scope.bank_name = docstatus1.data;

                setTimeout(function () {
                    $('#Select4').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);

            });

            $http.get(ENV.apiUrl + "api/PDCSubmission/GetCreatedBy_user").then(function (data1) {
                $scope.user_list = data1.data;

            });

            debugger

            //Fill department
            $http.get(ENV.apiUrl + "api/PDCSubmission/getDepartment?finance_year=" + finance_year + "&comp_code=" + comp_code).then(function (docstatus2) {
                $scope.Department = docstatus2.data;

            });


            //Fill Ledger
            //$http.get(ENV.apiUrl + "api/PDCSubmission/getLedgerName").then(function (docstatus3) {
            //    $scope.ledger = docstatus3.data;
            //   
            //});
            $http.get(ENV.apiUrl + "api/BankPayment/GetLedgerNumber?financialyear=" + finance_year + "&comp_code=" + comp_code).then(function (docstatus2) {
                $scope.LdgrCode = docstatus2.data;

            });


            $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {

                $scope.getAllAccNos = docstatus.data;
            });

            $scope.getSLAccNo = function () {


                var acconcode = $scope.getAllAccNos.gldd_acct_code;
                if ($scope.block1.gldd_ledger_code != "00") {

                    $http.get(ENV.apiUrl + "api/JVCreation/GetSLAccNumber?ldgr_code=" + $scope.block1.gldd_ledger_code + "&cmp_cd=" + comp_code + "&year=" + finance_year).then(function (docstatus3) {
                        // $scope.sllc_ldgr_code = slcode;
                        $scope.getAllAccNos = docstatus3.data;
                        //$scope.slacno = docstatus3.data;
                    });
                }
                else {
                    $http.get(ENV.apiUrl + "api/JVCreation/GetAllGLAccountNos?glma_accountcode=" + acconcode + "&cmpnycode=" + comp_code + "&fyear=" + finance_year + "&user=" + user).then(function (docstatus) {

                        $scope.getAllAccNos = docstatus.data;
                    });
                }


            }

            //Account number


            //Select Data SHOW
            var showdetails = [], bankcode1;
            $scope.getViewDetails = function () {

                $('#loader').modal({ backdrop: 'static', keyboard: false });

                if ($scope.block1.cheque == 'cheque') {
                    payment_mode = 'Ch';
                }
                else {
                    payment_mode = 'Dd';

                }
                if ($scope.block1.receipt_date == 'receipt') {
                    is_receipt = 'R';
                }
                else {
                    is_receipt = 'C';

                }

                bankcode = [];
                if ($scope.block1.bank_code.length == 0 || $scope.block1 == undefined) {
                    bankcode = '';
                }
                else {
                    bankcode1 = $scope.block1.bank_code;
                    bankcode = bankcode + ',' + bankcode1;;
                }

                var data = {
                    ps_comp_code: comp_code,
                    ps_dept_no: $scope.block1.dept_no,
                    ps_ldgr_code: $scope.block1.gldd_ledger_code,
                    ps_sl_acno: $scope.block1.gldd_acct_code,
                    ps_discd: 'O',
                    pc_calendar: null,
                    ps_bank_code: bankcode,
                    filterText: $scope.edt.SelectOption,
                    filterRange: $scope.edt.txt_filter_range,
                    createdBY: $scope.block1.createdby_usr,

                    recFromDate: (is_receipt == 'R') ? $scope.block1.from_date : null,
                    recToDate: (is_receipt == 'R') ? $scope.block1.to_date : null,

                    ps_due_dateStr: (is_receipt == 'C') ? $scope.block1.from_date : null,
                    subMissionDate: (is_receipt == 'C') ? $scope.block1.to_date : null,

                    pc_ref_no: null,
                    pc_payment_mode: payment_mode,

                }



                $http.post(ENV.apiUrl + "api/PDCSubmission/AllPDCSubmission_new_data", data).then(function (res1) {
                    $timeout(function () {
                        $("#fixTable").tableHeadFixer({ 'top': 1 });
                    }, 100);
                    // j.ps_cheque_no_check

                    if (res1.data.length > 0) {
                        $scope.pdcSubmissionList = res1.data;
                        $scope.pdc_details = res1.data;
                        //$scope.totalItems = $scope.pdc_details.length;
                        //$scope.todos = $scope.pdc_details;
                        //$scope.makeTodos();

                        $scope.checkprintbtn = true;

                        $scope.Maintabledata = true;


                        $scope.total = 0;
                        for (var i = 0; i < res1.data.length; i++) {
                            $scope.total = $scope.total + res1.data[i].ps_amount;
                        }
                        $scope.subtotal = 0;
                    }
                    else {
                        swal({ text: "No Data Found...!", timer: 50000 });
                        $scope.pdcSubmissionList = [];
                        $scope.total = 0;
                        $scope.subtotal = 0;
                    }

                    $('#loader').modal('hide');
                    $(".modal-backdrop").removeClass("modal-backdrop");
                });


                showdetails = []


            }


            $scope.SearcModelDetails = function (dept_no, ldgr_code, slma_acno, from_date, to_date) {

                if (dept_no == undefined)
                    dept_no = '';
                if (ldgr_code == undefined)
                    ldgr_code = '';
                if (slma_acno == undefined)
                    slma_acno = '';
                if (from_date == undefined)
                    from_date = '';
                if (to_date == undefined)
                    to_date = '';

                $http.get(ENV.apiUrl + "api/PDCSubmission/getAllPDCSubmissions?dept_no=" + dept_no + "&ldgr_code=" + ldgr_code + "&slma_acno=" + slma_acno + "&from_date=" + from_date + "&to_date=" + to_date).then(function (getAllPDCSubmissions_Data) {
                    $scope.AllPDCSubmissions = getAllPDCSubmissions_Data.data;
                });
            }


            var cntr = 0;
            $scope.selectonebyone = function (str, chq_amt) {

                //var d = document.getElementById(str);
                if (str == true) {
                    cntr = cntr + 1;
                    $scope.subtotal = $scope.subtotal + chq_amt;
                }
                else {
                    cntr = cntr - 1;
                    $scope.subtotal = $scope.subtotal - chq_amt;
                }
            }


            $scope.clear = function () {
                $scope.records = false;

                $scope.temp = [];
            }

            //DATA SAVE INSERT for cheque Transfer
            var datasend = [];
            $scope.savedata = function () {//Myform

                $('#loader').modal({ backdrop: 'static', keyboard: false });

                if ($scope.block1.cheque == 'cheque') {
                    payment_mode = 'Ch';
                }
                else {
                    payment_mode = 'Dd';

                }

                // if (Myform) {

                //  var data = $scope.temp;
                //data.opr = 'I';
                for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                    //var e = document.getElementById(i + i);
                    if ($scope.pdcSubmissionList[i].ps_cheque_no_check == true) {
                        $scope.pdcSubmissionList[i].cnt = 1;
                        $scope.pdcSubmissionList.opr = 'I';
                        $scope.pdcSubmissionList[i].ps_ldgr_code_name = user;
                        $scope.pdcSubmissionList[i].pc_payment_mode = payment_mode;
                        datasend.push($scope.pdcSubmissionList[i]);
                    }
                }
                if (datasend.length > 0) {


                    $http.post(ENV.apiUrl + "api/PDCSubmission/PDC_Submission_or_ReSubmission", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        //swal({ text: $scope.msg1.strMessage, timer: 5000 });
                        swal({ title: "Alert", text: $scope.msg1.strMessage + "\n Submission No. " + $scope.msg1.systemMessage, imageUrl: "assets/img/check.png", });
                        $scope.checkprintbtn = false;

                        reference_no = $scope.msg1.systemMessage;

                        $('#loader').modal('hide');
                        $(".modal-backdrop").removeClass("modal-backdrop");

                        //$scope.getViewDetails();
                    });
                }
                else {
                    swal({ title: "Alert", text: 'Please Select Cheque', });

                    $('#loader').modal('hide');
                    $(".modal-backdrop").removeClass("modal-backdrop");

                }

                // getViewDetails();
                datasend = [];
            }


            var cntr1 = 0;
            $scope.selectmodeldata = function (str1) {

                var d = document.getElementById('chk1-' + str1);
                if (d.checked == true) {
                    cntr1 = cntr1 + 1;
                }
                else { cntr1 = cntr1 - 1; }

            }

            //DATA SAVE for Cheque Revert
            var datasend = [];
            $scope.selectmodeldatasave = function () {

                $('#loader').modal({ backdrop: 'static', keyboard: false });

                for (var i = 0; i < $scope.AllPDCSubmissions.length; i++) {
                    var e = document.getElementById('chk1-' + i);
                    if (e.checked == true) {
                        $scope.AllPDCSubmissions[i].cnt = 3;
                        $scope.AllPDCSubmissions[i].ps_ldgr_code_name = user;
                        $scope.AllPDCSubmissions.opr = 'I';
                        datasend.push($scope.AllPDCSubmissions[i]);
                    }
                }
                if (datasend.length > 0) {
                    $http.post(ENV.apiUrl + "api/PDCSubmission/PDC_Submission_or_ReSubmission", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        swal({ title: "Alert", text: $scope.msg1.strMessage, imageUrl: "assets/img/check.png", });

                        $('#loader').modal('hide');
                        $(".modal-backdrop").removeClass("modal-backdrop");
                    });
                }
                else {
                    swal({ title: "Alert", text: 'Please Select Cheque', });

                    $('#loader').modal('hide');
                    $(".modal-backdrop").removeClass("modal-backdrop");

                }

                datasend = [];

            }



            //Pull Data
            $scope.pulldata = function () {




                if ($scope.block1.from_date == undefined && $scope.block1.to_date == undefined) {
                    swal({ text: "please select from date and to date", timer: 5000 });
                }
                else {
                    if ($scope.block1.cheque == 'cheque') {
                        payment_mode = 'Ch';
                    }
                    else {
                        payment_mode = 'Dd';

                    }
                    if ($scope.block1.receipt_date == 'receipt') {
                        is_receipt = 'R';
                    }
                    else {
                        is_receipt = 'C';

                    }

                    var data = {
                        ps_comp_code: comp_code,
                        ps_ldgr_code: is_receipt,
                        recFromDate: $scope.block1.from_date,
                        recToDate: $scope.block1.to_date,
                        pc_payment_mode: payment_mode,

                    }


                    $http.post(ENV.apiUrl + "api/PDCSubmission/PDC_Pool_Submission_data", data).then(function (res1) {
                        $scope.msg1 = res1.data;

                        swal({ text: $scope.msg1.strMessage, timer: 50000 });

                    });




                }
            }

            $scope.chkrevert = function () {
                $('#MyModal').modal('show');
                $scope.display = true;

            }


            $scope.CheckPrintReport = function () {


                var data = {
                    location: 'Finn.FINR23',
                    parameter: {
                        doc_no: reference_no,

                    },

                    state: 'main.Fin102'
                }
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')


            }

            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                    //var v = document.getElementById(i + i);
                    if (main.checked == true) {
                        $scope.pdcSubmissionList[i].ps_cheque_no_check = true;
                        $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                    }
                    else {
                        $scope.pdcSubmissionList[i].ps_cheque_no_check = false;
                        $scope.pdcSubmissionList[i].row_color = ''
                    }
                }

            }

            $scope.ShowFilterText = function (str) {
                if (str == "") {
                    $scope.filterVisible = false;
                }
                else {

                    $scope.filterVisible = true;
                }
            }

            $scope.FilterCode = function () {

                var splitno = [];
                if ($scope.temp.chk_previous_clear == true) {
                    main = document.getElementById('mainchk');
                    for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {

                        var v = document.getElementById(i + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $scope.pdcSubmissionList[i].row_color = 'white';

                    }
                    if ($scope.edt.SelectOption == 'Select Records') {


                        for (var i = 0; i < $scope.edt.txt_filter_range; i++) {
                            var v = document.getElementById(i + i);
                            v.checked = true;
                            $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                        }



                    }

                    else if ($scope.edt.SelectOption == 'Cheque Amount between') {


                        if ($scope.edt.txt_filter_range != undefined) {
                            splitno = $scope.edt.txt_filter_range.split('-');
                        }
                        if (splitno.length >= 2) {

                            for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                                if ($scope.pdcSubmissionList[i].ps_amount >= splitno[0] && $scope.pdcSubmissionList[i].ps_amount <= splitno[1]) {

                                    var v = document.getElementById(i + i);
                                    v.checked = true;
                                    $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                                }
                            }
                        }
                    }
                    else if ($scope.edt.SelectOption == 'Cheque Series between') {


                        if ($scope.edt.txt_filter_range != undefined) {

                            splitno = $scope.edt.txt_filter_range.split('-');
                        }
                        if (splitno.length >= 2) {
                            $scope.minchkno = 0;
                            $scope.maxchkno = 0;
                            for (var j = 0; j < splitno.length; j++) {
                                if (j == 0) {
                                    $scope.minchkno = parseInt(splitno[0]);
                                }
                                else {
                                    $scope.maxchkno = parseInt(splitno[1]);
                                }

                            }

                            for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {

                                if ($scope.pdcSubmissionList[i].ps_cheque_no >= $scope.minchkno && $scope.pdcSubmissionList[i].ps_cheque_no <= $scope.maxchkno) {

                                    var v = document.getElementById(i + i);
                                    v.checked = true;
                                    $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                                }
                            }
                        }

                    }
                }

                else {

                    if ($scope.edt.SelectOption == 'Select Records') {

                        if ($scope.pdcSubmissionList.length >= $scope.edt.txt_filter_range) {
                            for (var i = 0; i < $scope.edt.txt_filter_range; i++) {
                                var v = document.getElementById(i + i);
                                v.checked = true;
                                $scope.pdcSubmissionList[i].row_color = '#ffffcc'

                            }
                        }
                    }

                    else if ($scope.edt.SelectOption == 'Cheque Amount between') {


                        if ($scope.edt.txt_filter_range != undefined) {
                            splitno = $scope.edt.txt_filter_range.split('-');
                        }
                        if (splitno.length >= 2) {

                            for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                                if ($scope.pdcSubmissionList[i].ps_amount >= splitno[0] && $scope.pdcSubmissionList[i].ps_amount <= splitno[1]) {

                                    var v = document.getElementById(i + i);
                                    v.checked = true;
                                    $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                                }
                            }
                        }
                    }
                    else if ($scope.edt.SelectOption == 'Cheque Series between') {


                        if ($scope.edt.txt_filter_range != undefined) {
                            splitno = $scope.edt.txt_filter_range.split('-');
                        }
                        if (splitno.length >= 2) {

                            $scope.minchkno = 0;
                            $scope.maxchkno = 0;
                            for (var j = 0; j < splitno.length; j++) {
                                if (j == 0) {
                                    $scope.minchkno = parseInt(splitno[0]);
                                }
                                else {
                                    $scope.maxchkno = parseInt(splitno[1]);
                                }

                            }

                            for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {

                                if ($scope.pdcSubmissionList[i].ps_cheque_no >= $scope.minchkno && $scope.pdcSubmissionList[i].ps_cheque_no <= $scope.maxchkno) {

                                    var v = document.getElementById(i + i);
                                    v.checked = true;
                                    $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                                }
                            }

                            //for (var i = 0; i < $scope.pdcSubmissionList.length; i++) {
                            //    if ($scope.pdcSubmissionList[i].ps_cheque_no >= splitno[0] && $scope.pdcSubmissionList[i].ps_cheque_no <= splitno[1]) {

                            //        var v = document.getElementById(i);
                            //        v.checked = true;
                            //        $scope.pdcSubmissionList[i].row_color = '#ffffcc'
                            //    }
                            //}
                        }

                    }

                }
            }


            //Date FORMAT
            $scope.showdate = function (date, name1) {

                ////var month = date.split("/")[0];
                ////var day = date.split("/")[1];
                ////var year = date.split("/")[2];

                ////$scope.temp[name1] = year + "-" + month + "-" + day;
                var day = date.split("-")[0];
                var month = date.split("-")[1];
                var year = date.split("-")[2];

                $scope.temp[name1] = day + "-" + month + "-" + year;
                //$scope.temp[name1] = date;
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd-mm-yyyy',
                //format: 'yyyy-mm-dd',


            });

        }])

})();