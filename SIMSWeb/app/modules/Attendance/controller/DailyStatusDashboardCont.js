﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, modulecode = [];
    var simsController = angular.module('sims.module.Attendance');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('DailyStatusDashboardCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http','$filter', '$compile', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http,$filter,$compile, ENV) {


            var date = new Date();
            var month = (date.getMonth() + 1);
            var day = date.getDate();
            if (month < 10)
                month = "0" + month;
            if (day < 10)
                day = "0" + day;
            $scope.todaydate = (day) + '-' + (month) + '-' + date.getFullYear();

            $scope.flip=function() {
                $('.card').toggleClass('flipped');
            }

            var dateyear = $filter('date')(new Date(), 'dd-MM-yyyy');
            $scope.dt = {
                sims_from_date: $scope.todaydate,
                sims_leave_date: $scope.todaydate,
                sims_attendance_date: $scope.todaydate,
                sims_admission_date: $scope.todaydate,
                sims_admission_cancel_date: $scope.todaydate,
                sims_collection_date: $scope.todaydate,
            }

            $scope.frm = {
                sims_admission_date: '',
            }
            $scope.to = {
                sims_admission_date: '',
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'dd/mm/yyyy'
            });

            $scope.edt = [];

            $http.get(ENV.apiUrl + "api/dailystatusdash/getdailystatus").then(function (res1) {
                $scope.daily_status = res1.data;               
            });

            $http.get(ENV.apiUrl + "api/dailystatusdash/getPunchedEmployee?date=" + $scope.dt.sims_from_date).then(function (res) {
                $scope.punched_employee = res.data;
            });

            $http.get(ENV.apiUrl + "api/dailystatusdash/getNotPunchedEmployee?date=" + $scope.dt.sims_from_date).then(function (res2) {
                $scope.not_punched_employee = res2.data;
            });


            $scope.dateChange = function (selecteddate) {
                $http.get(ENV.apiUrl + "api/dailystatusdash/getPunchedEmployee?date=" + selecteddate).then(function (res) {
                    $scope.punched_employee = res.data;
                });
                $http.get(ENV.apiUrl + "api/dailystatusdash/getNotPunchedEmployee?date=" + selecteddate).then(function (res2) {
                    $scope.not_punched_employee = res2.data;
                });
            }
            $scope.leaveTypes = [];
            $http.get(ENV.apiUrl + "api/dailystatusdash/getleaveType").then(function (lev) {
                $scope.leaveTypes = lev.data;
                if ($scope.leaveTypes.length > 0) {
                    $scope.leaveTypes.push({leave_code:'All',leave_type:'All'})
                    $scope.edt['leave_code'] = 'All'; //$scope.leaveTypes[0].leave_code;
                $scope.getLeaveType($scope.edt['leave_code'], $scope.dt.sims_leave_date);
                }
            });

            $scope.dateleaveChange = function (leavedate) {
                $scope.getLeaveType($scope.edt['leave_code'],leavedate);
            }

            $scope.getLeaveType=function(curleavecode,selectdate){
                $http.get(ENV.apiUrl + "api/dailystatusdash/getleaveDetails?leavecode=" + curleavecode + "&date=" + $scope.dt.sims_leave_date).then(function (levcnt) {
                    $scope.empleavesCount = levcnt.data;
                });
            }
            $scope.datecollectionChange = function (curdate) {
                    $scope.getCollectionAmount($scope.edt['fee_payment_mode'], curdate);
            }


            $scope.getCollectionAmount = function (currFeeCode, selecteddate) {
                $scope.totalshow = true;
                $http.get(ENV.apiUrl + "api/dailystatusdash/getCollectionSummary?paymentmode=" + currFeeCode + "&date="+ selecteddate).then(function (coll) {
                    $scope.collectionSummary = coll.data;
                    if ($scope.collectionSummary.length > 0) {
                        $scope.feeAmount = 0;
                        for (var i = 0; $scope.collectionSummary.length > i; i++) {
                            if (currFeeCode == "TO") {
                                $scope.totalshow = false;                                
                                $scope.feeAmount = parseInt($scope.feeAmount) + parseInt($scope.collectionSummary[i].fee_amount);
                            } else {
                                if ($scope.collectionSummary[i].dd_fee_payment_mode == currFeeCode) {
                                    $scope.feeAmount = $scope.collectionSummary[i].fee_amount;
                                    break;
                                } else {
                                    $scope.feeAmount = '0'
                                }
                            }
                        }
                    } else {
                        $scope.feeAmount = '0'
                    }
                  
                        $scope.getCollectionAmountPaymodeWise(currFeeCode, selecteddate);
                    
                });
            }

            $scope.getCollectionAmountPaymodeWise = function (currFeeCode, selecteddate) {
                $http.get(ENV.apiUrl + "api/dailystatusdash/getCollectionSummaryPaymentModeWise?paymentmode=" + currFeeCode + "&date=" + selecteddate).then(function (coll) {
                    $scope.collectionPaymentModeWise = coll.data;
                    if (currFeeCode == "TO") {
                        $scope.totalshow = false;
                        $scope.toatalData = $scope.collectionSummary;
                    } else {
                        $scope.totalshow = true;
                    }
                });
            }
            $scope.feepaymentmodes = [];
            $http.get(ENV.apiUrl + "api/dailystatusdash/getFeePaymentMode").then(function (fee) {
                    $scope.feepaymentmodes = fee.data;
                    if ($scope.feepaymentmodes.length > 0) {
                        $scope.feepaymentmodes.push({ fee_payment_mode: 'TO', fee_payment_mode_desc: 'Total' })
                        $scope.edt['fee_payment_mode'] ='TO'  //$scope.feepaymentmodes[0].fee_payment_mode

                        $scope.getCollectionAmount($scope.edt['fee_payment_mode'], $scope.dt.sims_collection_date);
                    }
            });

            $http.get(ENV.apiUrl + "api/dailystatusdash/getNotPunchedEmployee").then(function (res2) {
                $scope.not_punched_employee = res2.data;
            });

            $scope.dateattendanceChange = function (curdate) {
                $scope.getAttendanceCountData(curdate)
            }


            $scope.getAttendanceCountData = function (selecteddate) {
                $scope.presentCount = []
                $scope.absentCount = []
                $scope.lateCount = []
                $scope.lwpCount = []

                $http.get(ENV.apiUrl + "api/dailystatusdash/getEmpAttendanceCount?date=" + selecteddate).then(function (att) {
                    $scope.attendanceCount = att.data;
                    $scope.attdate = $scope.attendanceCount[0].att_date;
                    for (var i = 0; $scope.attendanceCount.length > i; i++) {
                        if ($scope.attendanceCount[i].att_absent_flag == 'P') {
                            $scope.presentCount.push($scope.attendanceCount[i]);
                        }
                        if ($scope.attendanceCount[i].att_absent_flag == 'A') {
                            $scope.absentCount.push($scope.attendanceCount[i]);
                        }
                        if ($scope.attendanceCount[i].att_absent_flag == 'LA') {
                            $scope.lateCount.push($scope.attendanceCount[i]);
                        }
                        if ($scope.attendanceCount[i].att_absent_flag == 'LWP') {
                            $scope.lwpCount.push($scope.attendanceCount[i]);
                        }
                    }
                });
            }

            $scope.getAttendanceCountData($scope.dt.sims_attendance_date);
            $scope.selecteto = 'TO';
            $scope.selecteCA = 'CA';
            $http.get(ENV.apiUrl + "api/dailystatusdash/getAcademicYear").then(function (res2) {
                $scope.acayears = res2.data;
                $scope.edt['sims_academic_year'] = $scope.acayears[0].sims_academic_year
                $scope.edt['sims_academic_year1'] = $scope.acayears[0].sims_academic_year

                $scope.getAdmissionData($scope.edt['sims_academic_year'], $scope.dt.sims_admission_date, $scope.selecteto);
                $scope.getCancelAdmissionData($scope.edt['sims_academic_year1'], $scope.dt.sims_admission_cancel_date, $scope.selecteCA);
            });

            $scope.close = function () {
                $http.get(ENV.apiUrl + "api/dailystatusdash/getAcademicYear").then(function (res2) {
                    $scope.acayears = res2.data;
                    $scope.edt['sims_academic_year'] = $scope.acayears[0].sims_academic_year;
                    $scope.edt['sims_academic_year1'] = $scope.acayears[0].sims_academic_year;
                    $scope.getAdmissionData($scope.edt['sims_academic_year'], $scope.dt.sims_admission_date, $scope.selecteto);
                    $scope.getCancelAdmissionData($scope.edt['sims_academic_year1'], $scope.dt.sims_admission_cancel_date, $scope.selecteCA);
                });
            }

            $scope.dateadmissionChange = function (curdate) {
                $scope.selecteto = '';
                $scope.getAdmissionData($scope.edt['sims_academic_year'], curdate, $scope.selecteto)
            }
            $scope.getAcadyear = function (curyear,todt,frmdt) {
                $scope.edt['sims_academic_year'] = curyear;
                $scope.getTodate(todt, frmdt);
            }
            $scope.getAdmissionData = function (curyear, selecteddate, selecteto) {
                if (selecteto == 'TO') {
                    $http.get(ENV.apiUrl + "api/dailystatusdash/getAdmissionData?year=" + curyear + "&fromdate=" + selecteddate + "&todate=" + selecteddate + "&subopr=TO").then(function (adm) {
                        $scope.admissionData = adm.data;
                    });
                } else {
                    $http.get(ENV.apiUrl + "api/dailystatusdash/getAdmissionData?year=" + curyear + "&fromdate=" + selecteddate + "&todate=" + selecteddate + "&subopr=").then(function (adm) {
                        $scope.admissionData = adm.data;
                    });
                }
            }

//            $scope.getAdmissionData($scope.edt['sims_academic_year'], $scope.dt.sims_admission_date, $scope.selecteto);

            //$scope.getFromdate = function (fromdate, todate) {
            //    debugger;
            //    $http.get(ENV.apiUrl + "api/dailystatusdash/getAdmissionData?year=" + $scope.edt['sims_academic_year'] + "&fromdate=" + fromdate + "&todate=" + todate + "&subopr=").then(function (adm) {
            //        $scope.admissionData = adm.data;
            //    });
            //}
            $scope.getAdmTodate = function (todate, fromdate) {          
                $http.get(ENV.apiUrl + "api/dailystatusdash/getAdmissionData?year=" + $scope.edt['sims_academic_year'] + "&fromdate=" + fromdate + "&todate=" + todate + "&subopr=").then(function (adm) {
                    $scope.admissionData = adm.data;
                });
            }


            $scope.dateadmissionCancelChange = function (curdate) {
                $scope.selecteCA = '';
                $scope.getCancelAdmissionData($scope.edt['sims_academic_year1'], curdate, $scope.selecteCA);
            }

            $scope.getCancelAdmissionData = function (curyear, selecteddate, selecteCA) {
                if (selecteCA == 'CA') {
                    $http.get(ENV.apiUrl + "api/dailystatusdash/getCancelAdmissionData?year=" + curyear + "&fromdate=" + selecteddate + "&todate=" + selecteddate + "&subopr=CA").then(function (adm) {
                        $scope.canceladmissionData = adm.data;
                    });
                } else {
                    $http.get(ENV.apiUrl + "api/dailystatusdash/getCancelAdmissionData?year=" + curyear + "&date=" + selecteddate + "&subopr=").then(function (adm) {
                        $scope.canceladmissionData = adm.data;
                    });
                }
            }
            $scope.getCancelAcadyear = function (curyear,todt,frmdt) {
                $scope.edt['sims_academic_year1'] = curyear;
            }


         //   $scope.getCancelAdmissionData($scope.edt['sims_academic_year1'], $scope.dt.sims_admission_cancel_date, $scope.selecteCA);

            $scope.getNewcancldata = function (todate, fromdate) {
                $http.get(ENV.apiUrl + "api/dailystatusdash/getCancelAdmissionData?year=" + $scope.edt['sims_academic_year1'] + "&fromdate=" + fromdate + "&todate=" + todate + "&subopr=").then(function (adm) {
                    $scope.canceladmissionData = adm.data;
                });
            }


            $scope.punchModal = function () {
                $('#punchModal').modal({ backdrop: 'static', keyboard: false });
            }
            $scope.leaveModal = function () {
                $('#leaveModal').modal({ backdrop: 'static', keyboard: false });
            }
            $scope.attendanceModal = function () {
                $('#attendanceModal').modal({ backdrop: 'static', keyboard: false });
            }
            $scope.admissionModal = function () {
                $scope.frm = {sims_admission_date: '',}
                $scope.to = {sims_admission_date: '',}
                $('#admissionModal').modal({ backdrop: 'static', keyboard: false });
            }
            $scope.canceladmissionModal = function () {
                $scope.frm = { sims_admission_date: '', }
                $scope.to = { sims_admission_date: '', }
                $('#canceladmissionModal').modal({ backdrop: 'static', keyboard: false });
            }
            $scope.collectionModal = function () {
                $('#collectionModal').modal({ backdrop: 'static', keyboard: false });
            }

        }])

})();
