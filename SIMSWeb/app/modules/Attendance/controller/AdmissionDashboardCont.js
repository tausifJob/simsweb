﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, modulecode = [];
    var simsController = angular.module('sims.module.Attendance');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('AdmissionDashboardCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$compile', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $compile,ENV) {
          
            (function (ChartJsProvider) {
                ChartJsProvider.setOptions({ colors: ['#803690', '#00ADF9', '#DCDCDC', '#46BFBD', '#FDB45C', '#949FB1', '#4D5360'] });
            });
     
            var sdate = '', edate = '';
            $scope.busyindicator = false;
            $scope.boys_lst = [{ cnt: 0, confirm: 0, rejected:0},{ cnt: 0, confirm: 0, rejected:0}];

            //$scope.labels = ["Download Sales", "In-Store Sales", "Mail-Order Sales"];
            //$scope.data = [300, 500, 100];



            //$scope.onClick = function (points, evt) {
            //    debugger
            //   
            //};

            var buyerData = {
                labels: ["January", "February", "March", "April", "May", "June"],
                datasets: [
                    {
                        fillColor: "rgba(172,194,132,0.4)",
                        strokeColor: "#ACC26D",
                        pointColor: "#fff",
                        pointStrokeColor: "#9DB86D",
                        data: [203, 156, 99, 251, 305, 247]
                    }
                ]
            }

            var buyers = document.getElementById('buyers').getContext('2d');
            new Chart(buyers).Line(buyerData);

           

            $http.get(ENV.apiUrl + "api/common/getCuriculumCommon").then(function (res) {
                $scope.cmbCur = res.data;
            })

            




            $scope.CurChange = function (str) {

                $http.get(ENV.apiUrl + "api/common/getAcademicYearCommon?curCode="+str).then(function (res) {
                    $scope.cmbAcademic = res.data;
                })
            }

            $scope.academicChange = function (str) {

                $http.get(ENV.apiUrl + "api/common/getAllGradesCommon?cur_code=" + $scope.sims_cur_code + "&academic=" + str).then(function (res) {
                    $scope.cmbGrade = res.data;
                })
            }
            $scope.gradeChange = function (str) {

                $scope.grade_code = str;
                
            }


          

            $('input[name="daterange"]').on('apply.daterangepicker', function (ev, picker) {
                sdate = (picker.startDate.format('DD-MM-YYYY'));
                edate = (picker.endDate.format('DD-MM-YYYY'));
            });


            $scope.Show_Data = function () {
              
                
                //if ($scope.adm == undefined || $scope.adm == '') {

                //    sdate = '', edate = ''

                //}
                  
                //else {
                //    var visible = $scope.adm_date.split("-");
                //    if (visible.length > 0) {
                //        sdate = visible[0];
                //        edate = visible[1];
                //    }
                //}
                //$http.get(ENV.apiUrl + "api/AdmissionDash/get_Section_Strength?cur_code=" + $scope.sims_cur_code + "&academic=" + $scope.sims_academic_year + "&grade=" + '' + "&sd=" + '' + "&ed=" + '').then(function (res) {
                //   
                //});

                $http.get(ENV.apiUrl + "api/AdmissionDash/get_Section_Strength?cur_code=" + $scope.sims_cur_code + "&academic=" + $scope.sims_academic_year + "&grade=" + '' + "&sd=" + sdate + "&ed=" + edate).then(function (res) {

                $scope.cmbSection = res.data;
                   
                    

                    for (var i = 0 ; i < res.data.length; i++) {


                        var t = 'gauge0';
                        //if (i == 0) {
                        //    t = 'gauge0';
                        //}

                        //else if (i == 1) {
                        //    t = 'gauge1';
                        //}

                        //else if (i == 2) {
                        //    t = 'gauge2';
                        //}

                        //else if (i == 3) {
                        //    t = 'gauge3';
                        //}

                        //else if (i == 4) {
                        //    t = 'gauge4';
                        //}
                        //else if (i == 5) {
                        //    t = 'gauge5';
                        //}

                        //else if (i == 6) {
                        //    t = 'gauge6';
                        //}

                        $('#newTransaction').replaceWith($('#newTransaction'));

                        $('#newTransaction').append('<canvas style="height:188px;width:188px"  id="gauge' + i + '"></canvas>');
                     
              
                        // change ng-click
                        $('#gauge' + i).attr('ng-click', 'clickbtn(' + res.data[i].sims_grade_code + ')');

                        // compile the element
                        $compile($('#gauge'+i))($scope);



                        var bar=  [
                                 { from: 0, to: res.data[i].total_stregth, color: 'rgba(50,   205, 50, 3329330)' },
                                // { from: 30, to: 60, color: 'rgba(255, 255, 0, 65535)' },
                              //   { from: 60, to: 100, color: 'rgba(255, 69,  0, 17919)' },

                        ]


                        var gauge = new Gauge({
                            renderTo: "gauge" + i ,
                            width: 180,
                            height: 180,
                            glow: true,
                            units: res.data[i].sims_grade_name_en,
                            title: false,
                            minValue: 0,
                            maxValue: res.data[i].total_stregth,
                            // majorTicks: ['0', '10', '20', '30', '40', '50', '60', '70', '80', '90', '100'],
                            minorTicks: 2,
                            strokeTicks: false,
                            highlights:bar,
                            colors: {
                                plate: '#fff',
                                majorTicks: '#234567',
                                minorTicks: '#340099',
                                title: '#fff',
                                units: '#234567',
                                numbers: '#234567',
                                needle: { start: 'rgba(240, 128, 128, 1)', end: 'rgba(255, 160, 122, .9)' }
                            }//#1a8abf
                        });

                        gauge.setValue(res.data[i].confirm);
                        gauge.draw();


                    }




                })


                $http.get(ENV.apiUrl + "api/AdmissionDash/get_All_status?cur_code=" + $scope.sims_cur_code + "&academic=" + $scope.sims_academic_year + "&grade=" + '' + "&sd=" + sdate + "&ed=" + edate).then(function (res) {
                    $scope.lbl = res.data;

                    $scope.allStatus = res.data;


                 
                    
                });


                $http.get(ENV.apiUrl + "api/AdmissionDash/get_All_Boys_Girls_status?cur_code=" + $scope.sims_cur_code + "&academic=" + $scope.sims_academic_year + "&grade=" + '' + "&sd=" + sdate + "&ed=" +edate).then(function (res) {
                    $scope.boys_lst = [];
                    if (res.data.length > 0) {
                        $scope.boys_lst = res.data;
                        //$scope.girls_lst = res.data;

                    }
                  

                });
                
                $http.get(ENV.apiUrl + "api/AdmissionDash/get_All_status?cur_code=" + $scope.sims_cur_code + "&academic=" + $scope.sims_academic_year + "&grade=" + '' + "&sd=" + sdate + "&ed=" + edate + '&opr=E').then(function (res) {
                    $scope.allreligion = [];
                
                    for (var i = 0; i < res.data.length; i++) {
                        
                        if (res.data[i].value > 0) {
                            var color = Math.floor(Math.random() * 16777216).toString(16);
                            // Avoid loops.
                            res.data[i].highlight = '#000000'.slice(0, -color.length) + color;
                            res.data[i].color = '#000000'.slice(0, -color.length) + color;
                            $scope.allreligion.push(res.data[i]);
                        }
 
                    }

                    
                   

                    var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
                    var pieChart = new Chart(pieChartCanvas);

                    var pieOptions = {
                        //Boolean - Whether we should show a stroke on each segment
                        segmentShowStroke: true,
                        //String - The colour of each segment stroke
                        segmentStrokeColor: "#fff",
                        //Number - The width of each segment stroke
                        segmentStrokeWidth: 1,
                        //Number - The percentage of the chart that we cut out of the middle
                        percentageInnerCutout: 50, // This is 0 for Pie charts
                        //Number - Amount of animation steps
                        animationSteps: 100,
                        //String - Animation easing effect
                        animationEasing: "easeOutBounce",
                        //Boolean - Whether we animate the rotation of the Doughnut
                        animateRotate: true,
                        //Boolean - Whether we animate scaling the Doughnut from the centre
                        animateScale: false,
                        //Boolean - whether to make the chart responsive to window resizing
                        responsive: true,
                        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                        maintainAspectRatio: false,
                        //String - A legend template
                        //legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
                        //String - A tooltip template
                        tooltipTemplate: "<%=value %> <%=label%> "
                    };
                    //Create pie or douhnut chart
                    // You can switch between pie and douhnut using the method below.
                    var tcx = pieChart.Doughnut($scope.allreligion, pieOptions);

                    document.getElementById('s').innerHTML = tcx.generateLegend();


                    $("#pieChart").click(
        function (evt) {
            


            $scope.activePoints = tcx.activeElements[0].label;


            $http.get(ENV.apiUrl + "api/AdmissionDash/get_All_Religion_details?cur_code=" + $scope.sims_cur_code + "&academic=" + $scope.sims_academic_year + "&grade=" + '' + "&sd=" + sdate + "&ed=" + edate + '&religion=' + $scope.activePoints).then(function (res) {
                $scope.religonLst = res.data;
            });
            /* do something */
        }
    );
                
                });

             
                
                $http.get(ENV.apiUrl + "api/AdmissionDash/get_All_status?cur_code=" + $scope.sims_cur_code + "&academic=" + $scope.sims_academic_year + "&grade=" + '' + "&sd=" + sdate + "&ed=" +edate + '&opr=C').then(function (res) {
                    $scope.nationalityLst = res.data;
                });
            }


            $scope.clickbtn = function (str) {
                
                if (str == 1 || str == 2 || str == 3 || str == 4 || str == 5 || str == 6 || str == 7 || str == 8 || str == 9) {
                    str = '0' + str;
                }
                $http.get(ENV.apiUrl + "api/AdmissionDash/get_All_status?cur_code=" + $scope.sims_cur_code + "&academic=" + $scope.sims_academic_year + "&grade=" + str + "&sd=" + sdate + "&ed=" + edate).then(function (res) {
                    $scope.lbl = res.data;

                    $scope.allStatus = res.data;

                    $http.get(ENV.apiUrl + "api/AdmissionDash/get_All_Boys_Girls_status?cur_code=" + $scope.sims_cur_code + "&academic=" + $scope.sims_academic_year + "&grade=" + str + "&sd=" + sdate + "&ed=" + edate).then(function (res) {
                        $scope.boys_lst = [];
                        if (res.data.length > 0) {
                            $scope.boys_lst = res.data;
                            //$scope.girls_lst = res.data;

                        }
                        

                    });


                    $http.get(ENV.apiUrl + "api/AdmissionDash/get_All_status?cur_code=" + $scope.sims_cur_code + "&academic=" + $scope.sims_academic_year + "&grade=" + str + "&sd=" + sdate + "&ed=" + edate + '&opr=E').then(function (res) {
                        $scope.allreligion = [];

                        for (var i = 0; i < res.data.length; i++) {
                            
                            if (res.data[i].value > 0) {
                                var color = Math.floor(Math.random() * 16777216).toString(16);
                                // Avoid loops.
                                res.data[i].highlight = '#000000'.slice(0, -color.length) + color;
                                res.data[i].color = '#000000'.slice(0, -color.length) + color;
                                $scope.allreligion.push(res.data[i]);
                            }

                        }

                       


                        var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
                        var pieChart = new Chart(pieChartCanvas);

                        var pieOptions = {
                            //Boolean - Whether we should show a stroke on each segment
                            segmentShowStroke: true,
                            //String - The colour of each segment stroke
                            segmentStrokeColor: "#fff",
                            //Number - The width of each segment stroke
                            segmentStrokeWidth: 1,
                            //Number - The percentage of the chart that we cut out of the middle
                            percentageInnerCutout: 50, // This is 0 for Pie charts
                            //Number - Amount of animation steps
                            animationSteps: 100,
                            //String - Animation easing effect
                            animationEasing: "easeOutBounce",
                            //Boolean - Whether we animate the rotation of the Doughnut
                            animateRotate: true,
                            //Boolean - Whether we animate scaling the Doughnut from the centre
                            animateScale: false,
                            //Boolean - whether to make the chart responsive to window resizing
                            responsive: true,
                            // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                            maintainAspectRatio: false,
                            //String - A legend template
                            //legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
                            //String - A tooltip template
                            tooltipTemplate: "<%=value %> <%=label%> "
                        };
                        //Create pie or douhnut chart
                        // You can switch between pie and douhnut using the method below.
                        var tcx = pieChart.Doughnut($scope.allreligion, pieOptions);

                        document.getElementById('s').innerHTML = tcx.generateLegend();


                        $("#pieChart").click(
            function (evt) {
                


                $scope.activePoints = tcx.activeElements[0].label;


                $http.get(ENV.apiUrl + "api/AdmissionDash/get_All_Religion_details?cur_code=" + $scope.sims_cur_code + "&academic=" + $scope.sims_academic_year + "&grade=" + str + "&sd=" + sdate + "&ed=" + edate + '&religion=' + $scope.activePoints).then(function (res) {
                    $scope.religonLst = res.data;
                });
                /* do something */
            }
        );

                    });


                    $http.get(ENV.apiUrl + "api/AdmissionDash/get_All_status?cur_code=" + $scope.sims_cur_code + "&academic=" + $scope.sims_academic_year + "&grade=" + str + "&sd=" + sdate + "&ed=" +edate + '&opr=C').then(function (res) {
                        $scope.nationalityLst = res.data;
                    });
                });

            
             

            }

        //    $('input[name="birthdate"]').daterangepicker({
        //        singleDatePicker: true,
        //        showDropdowns: true,
        //        timePicker: true,
        //timePickerIncrement: 30,
        //locale: {
        //    format: 'MM/DD/YYYY h:mm A'
        //}
        //    });



            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            function cb(start, end) {
                $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            }
            cb(moment().subtract(29, 'days'), moment());


            $('input[name="daterange"]').daterangepicker(
                {
        ranges: {
           'Today': [moment(), moment()],
           'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
           'Last 7 Days': [moment().subtract(6, 'days'), moment()],
           'Last 30 Days': [moment().subtract(29, 'days'), moment()],
           'This Month': [moment().startOf('month'), moment().endOf('month')],
           'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        }
    }, cb);
        

        }])

    

})();
