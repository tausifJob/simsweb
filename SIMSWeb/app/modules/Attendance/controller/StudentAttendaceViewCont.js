﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, modulecode = [];
    var simsController = angular.module('sims.module.Attendance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StudentAttendaceViewCont',
         ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

             $scope.pagesize = "10";
             $scope.pageindex = 0;
             $scope.pager = true;
             $scope.showdisabled = true;

             $timeout(function () {
                 $("#example").tableHeadFixer({ 'top': 1 });
             }, 100);

             var dateyear = $filter('date')(new Date(), 'dd-MM-yyyy');
             $scope.dt = {
                 sims_from_date: dateyear,
                 sims_to_date: dateyear,
             }


             $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                 $('input[type="text"]', $(this).parent()).focus();
             });

             $('*[data-datepicker="true"] input[type="text"]').datepicker({
                 todayBtn: true,
                 orientation: "top left",
                 autoclose: true,
                 todayHighlight: true,
                 format: 'dd-mm-yyyy'
             });

             $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

             $scope.size = function (str) {
                 if (str == 10 || str == 20 || str == 'All') {
                     $scope.pager = true;
                 }
                 else {
                     $scope.pager = false;
                 }
                 /*$scope.pagesize = str;
                 $scope.currentPage = 1;
                 $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();*/

                 main = document.getElementById('mainchk');
                 if (main.checked == true) {
                     main.checked = false;
                     $scope.row1 = '';
                     $scope.color = '#edefef';
                 }

                 if (str == "All") {
                     $scope.currentPage = 1;
                     $scope.numPerPage = $scope.studlist.length;
                     $scope.makeTodos();

                 }
                 else {
                     $scope.pagesize = str;
                     $scope.currentPage = 1;
                     $scope.numPerPage = str;
                     $scope.makeTodos();
                 }

             }

             $scope.index = function (str) {
                 $scope.pageindex = str;
                 $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                 $scope.chk = {}
                 $scope.chk['check_all'] = false;
                 $scope.row1 = '';
             }

             $scope.makeTodos = function () {
                 var rem = parseInt($scope.totalItems % $scope.numPerPage);
                 if (rem == '0') {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                 }
                 else {
                     $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                 }

                 var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                 var end = parseInt(begin) + parseInt($scope.numPerPage);

                 console.log("begin=" + begin); console.log("end=" + end);

                 $scope.filteredTodos = $scope.todos.slice(begin, end);
             };

             $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                 $scope.curriculum = AllCurr.data;
                 $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                 $scope.getAccYear($scope.edt.sims_cur_code);
             });

             $scope.getAccYear = function (curCode) {
                 $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                     $scope.Acc_year = Acyear.data;
                     $scope.temp = { sims_academic_year: $scope.Acc_year[0].sims_academic_year };
                     $scope.getGrade(curCode, $scope.temp.sims_academic_year);
                 });
             }

             $scope.GetGrade = function () {
                 $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                     $scope.Grade_code = Gradecode.data;
                     setTimeout(function () {
                         $('#cmb_grade_code').change(function () {
                             console.log($(this).val());
                         }).multipleSelect({
                             width: '100%'
                         });
                     }, 1000);

                 });
             }

             $scope.getGrade = function (curCode, accYear) {
                 $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                     $scope.Grade_code = Gradecode.data;
                     setTimeout(function () {
                         $('#cmb_grade_code').change(function () {
                             console.log($(this).val());
                         }).multipleSelect({
                             width: '100%'
                         });
                     }, 1000);

                 });
             }

             $(function () {
                 $('#cmb_grade_code').multipleSelect({
                     width: '100%'
                 });
             });

             $scope.getSection = function (curCode, gradeCode, accYear) {
                 $http.get(ENV.apiUrl + "api/StudentReport/getSectionFromGrade?cur_code=" + $scope.edt.sims_cur_code + "&grade_code=" + $scope.edt.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                     $scope.Section_code = Sectioncode.data;
                     setTimeout(function () {
                         $('#cmb_section_code').change(function () {
                             console.log($(this).val());
                         }).multipleSelect({
                             width: '100%'
                         });
                     }, 1000);

                 });
             }

             $(function () {
                 $('#cmb_section_code').multipleSelect({
                     width: '100%'
                 });
             });

             $scope.Show_Data = function (str, str1) {
                 if (str != undefined && str1 != undefined) {
                     $http.get(ENV.apiUrl + "api/StudentReport/getStudentattendanceview?cur_name=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year + "&grd_code=" + $scope.edt.sims_grade_code + "&sec_code=" + $scope.edt.sims_section_code + "&from_date=" + $scope.dt.sims_from_date + "&upto_date=" + $scope.dt.sims_to_date).then(function (res) {
                         $scope.studlist = res.data;
                         $scope.totalItems = $scope.studlist.length;
                         $scope.todos = $scope.studlist;
                         $scope.makeTodos();

                         if ($scope.studlist.length == 0) {
                             swal({ text: "Please Try Again Record Not Found...",imageUrl: "assets/img/close.png", showCloseButton: true, width: 450, height: 200 });
                         }
                         else {
                             $scope.table = true;
                         }
                     })
                 }
                 else {
                     swal({  text: "Please Select Curriculum & Academic Year Then Try Again...",imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 450, height: 200 });
                 }
             }

             $scope.searched = function (valLists, toSearch) {
                 return _.filter(valLists,
                 function (i) {
                     return searchUtil(i, toSearch);
                 });
             };

             $scope.search = function () {
                 $scope.todos = $scope.searched($scope.studlist, $scope.searchText);
                 $scope.totalItems = $scope.todos.length;
                 $scope.currentPage = '1';
                 if ($scope.searchText == '') {
                     $scope.todos = $scope.studlist;
                 }
                 $scope.makeTodos();
             }

             function searchUtil(item, toSearch) {
                 return (item.sims_enrollment_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.percentage.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                         item.sagar == toSearch) ? true : false;
             }

             $scope.Reset = function () {
                 $scope.edt = {
                     sims_cur_code: '',
                     sims_grade_code: '',
                     sims_section_code: '',
                 }
                 $scope.temp = {
                     sims_academic_year: '',
                 }
                 $scope.dt = {
                     sims_from_date: dateyear,
                     sims_to_date: dateyear,
                 }
                 $scope.table = false;
             }

         }])
})();