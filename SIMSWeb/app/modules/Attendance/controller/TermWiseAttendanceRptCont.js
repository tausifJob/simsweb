﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Attendance');


    simsController.controller('TermWiseAttendanceRptCont', ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV','$filter',
    function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV,$filter) {

            $scope.hidesummary = true;

            $(function () {
                $('#cmb_grades').multipleSelect({ width: '100%' });
                $('#cmb_sections').multipleSelect({ width: '100%' });
               
            });

            //$scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '', attDate: new Date().getFullYear() + '-' + (new Date().getMonth()+1) + '-' + new Date().getDate() };
            $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '', attDate: ('0' + new Date().getDate()).slice(-2) + '-' + ('0' + (new Date().getMonth() + 1)).slice(-2) + '-' + new Date().getFullYear() };
            
            var username = $rootScope.globals.currentUser.username;
            $http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (curiculum) {
                $scope.curiculum = curiculum.data;
                $scope.edt['sims_cur_code'] = curiculum.data[0].sims_cur_code;
                $scope.cur_change();
            });
            $scope.cur_change = function () {

                //$http.get(ENV.apiUrl + "api/attendance/getAttendanceyear?cur_code=" + $scope.edt.sims_cur_code).then(function (academicyears) {
                //    $scope.academicyears = academicyears.data;
                //});

                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllAcademicYears?cur_code=" + $scope.edt.sims_cur_code).then(function (AcademicYears) {
                    $scope.academicyears = AcademicYears.data;
                    for (var i = 0; i < $scope.academicyears.length; i++) {
                        if ($scope.academicyears[i].sims_academic_year_status == 'C' || $scope.academicyears[i].sims_academic_year_status == 'Current') {
                            //   $scope.edt['sims_cur_code'] = cur_values;
                            $scope.edt['sims_academic_year'] = $scope.academicyears[i].sims_academic_year;
                        }
                    }
                    $scope.acdm_yr_change();
                });

                $http.get(ENV.apiUrl + "api/attendance/getAttendanceCode?cur_code=" + $scope.edt.sims_cur_code).then(function (attendance_code) {
                    $scope.attendance_code = attendance_code.data;
                    //$scope.rdbtn = $scope.attendance_code[0].sims_attendance_code;
                });
            }
            $scope.acdm_yr_change = function () {
                $http.get(ENV.apiUrl + "api/attendance/getAttendancetermTWAR?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year).then(function (term) {
                    $scope.terms = term.data;
                    setTimeout(function () {
                        $('#cmb_Term').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });

                $http.get(ENV.apiUrl + "api/attendance/getAttendancegrade?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&userName=" + username).then(function (grades) {
                    console.log(grades.data);
                    $scope.grades = grades.data;
                    setTimeout(function () {
                        $('#cmb_grades').change(function () {
                            console.log($(this).val());
                            $scope.grade_data = $(this).val();
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                   
                });

               
            }
            $scope.grade_change = function ()
            {
                if ($scope.grade_data != null || $scope.grade_data != undefined) {
                    $http.get(ENV.apiUrl + "api/attendance/getAttendancesectionTWAR?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&gradeCode=" + $scope.grade_data + "&userName=" + username).then(function (section) {
                        $scope.section = section.data;

                        setTimeout(function () {
                            $('#cmb_sections').change(function () {
                                console.log($(this).val());
                            }).multipleSelect({
                                width: '100%'
                            });
                        }, 1000);
                    });
                }
            }
         
            $scope.getAttendance = function () {
                
                $http.get(ENV.apiUrl + "api/Attendance/getAttendancetableTWAR?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&sectionCode=" + $scope.edt.sims_section_code + "&termCode=" + $scope.edt.sims_term_code).then(function (term_Data_rpt) {
                    $scope.term_Data_rpt = term_Data_rpt.data;
                      //  $scope.busy = false;
                    });
            }
                                
            $timeout(function () {
                $("#customers").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.exportData = function () {
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('example_wrapper').innerHTML], {
                            type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                        });
                        saveAs(blob, "TermWiseAttendanceReport.xls");
                    }
                });
            };

        }])

})();