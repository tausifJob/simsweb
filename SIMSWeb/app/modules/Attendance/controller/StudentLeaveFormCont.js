﻿

(function () {
    'use strict';
    var simsController = angular.module('sims.module.Attendance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('StudentLeaveFormCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {
            var user = $rootScope.globals.currentUser.username;
            $scope.temp = {};
            $scope.edt = {};
            $scope.detailsField = true;
            $scope.hideprintpdf = false;
            $scope.msg1 = '';

            $scope.pagesize = '30';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.mom_end_date_check = true;
           // $scope.mom_start_date_check = true;
            $scope.temp = {};
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            //$scope.mom_start_date = '';
            $scope.mom_end_date = '';
            $scope.report_status = '';
            $scope.reportdataparameter = [];
           

            $(function () {
                $('#bell_box').multipleSelect({ width: '100%' });
                $('#teacher_box').multipleSelect({ width: '100%', filter: true });

            });
            //var att_time = $filter('date')(data.att_time, 'h:mm a');
            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_start_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });
            if ($scope.mom_start_date_check == true) {
                $scope.mom_start_date = '';
            }
            else {
                $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
            }

            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_end_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });
            if ($scope.mom_end_date_check == true) {
                $scope.mom_end_date = '';
            }
            else {
                $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
            }
            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {

                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['chk_all'] = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 30, $scope.maxSize = 100;

            $scope.makeTodos = function () {
                debugger;
                if ($scope.pagesize == 'All') {
                    $scope.currentPage = 1;
                    $scope.showPager = false;
                    $scope.filteredTodos = $scope.report_data;
                }
                else {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                    $scope.filteredTodos_copy =angular.copy($scope.todos.slice(begin, end));

                    
                }
            };


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res1) {
                $scope.cur_data = res1.data;
                $scope.temp.sims_cur_code = $scope.cur_data[0].sims_cur_code;
                $scope.getAcademic_year($scope.cur_data[0].sims_cur_code);
            });

            $scope.getAcademic_year = function (cur_code1) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code1).then(function (res1) {
                    $scope.acad_data = res1.data;
                    $scope.temp.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);

                });
            }


            $scope.getGrade = function (cur_code, acad_year) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                        $("#cmb_grade_code").multipleSelect("checkAll");
                    }, 1000);

                });
            }


            $scope.getSection = function (cur_code, academic_year, grade_code) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.temp.sims_cur_code + "&grade_code=" + $scope.temp.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                        $("#cmb_section_code").multipleSelect("checkAll");
                    }, 1000);
                });
            }


            //var main, del = '', delvar = [];
            //$scope.chk = function () {
            //    debugger;
            //    main = document.getElementById('chk_min');
            //    if (main.checked == true) {
            //        for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //            var v = document.getElementById($scope.filteredTodos[i].sims_enrollment_number + i);

            //            v.checked = true;
            //            $scope.row1 = 'row_selected';
            //            $('tr').addClass("row_selected");
            //        }
            //    }
            //    else {
            //        for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //            var v = document.getElementById($scope.filteredTodos[i].sims_enrollment_number + i);
            //            v.checked = false;
            //            main.checked = false;
            //            $scope.row1 = '';
            //            $('tr').removeClass("row_selected");
            //        }
            //    }

            //}

            $scope.InsertIntoDetensionTransaction = function (data) {
                debugger;
                $scope.savedata = [];
                $scope.reportdataparameter = [];
                if (data.att_time.substring(0, 2) > 24) {
                    alert("Time fill up 24 fromat ");
                }
                else{
                //var att_time = $filter('date')(data.att_time, 'h:mm a');
                var att_time = data.att_time.substring(0, 2) + ':' + data.att_time.substring(2, 4);
                var datasend = {
                    'sims_cur_code': data.sims_cur_code,
                    'sims_acad_year': data.sims_academic_year,
                    'sims_grade_code': data.sims_grade_code,
                    'sims_section_code': data.sims_section_code,
                    'sims_enroll_number': data.sims_enrollment_number,
                    'date': data.sims_attendance_date,
                    'sims_attendance_code': data.sims_attendance_code,
                    'sims_slot_code': data.sims_slot_code,
                    'issued_by': user,
                    'remark': data.remark,
                    'att_time': att_time,
                    
                    
                }
                $scope.savedata.push(datasend);
                $scope.reportdataparameter.push(datasend);
                $http.post(ENV.apiUrl + "api/HolidayAttendance/InsertSims_leaveSlip", datasend).then(function (msg) {
                    $scope.msg1 = msg.data;
                     
                        
                        if ($scope.msg1 == true) {
                            $scope.report_status = data.sims_attendance_code
                            $scope.Report();
                            console.log("msg", msg);
                        }
                    
                });

            }
            }

            $scope.ReInsertIntoDetensionTrans = function (data) {
                debugger;
              //  $scope.savedata = [];
                $scope.reportdataparameter = [];
                var att_time = $filter('date')(data.att_time, 'h:mm a');
                $scope.msg1 = true;

                var datasend = {
                    'sims_cur_code': data.sims_cur_code,
                    'sims_acad_year': data.sims_academic_year,
                    'sims_grade_code': data.sims_grade_code,
                    'sims_section_code': data.sims_section_code,
                    'sims_enroll_number': data.sims_enrollment_number,
                    'date': data.sims_attendance_date,
                    'sims_attendance_code': data.sims_attendance_code,
                    'sims_slot_code': data.sims_slot_code,
                    'issued_by': user,
                    'remark': data.remark,
                    'att_time': att_time,
                    

                    //   if ($scope.msg1 == true) {
                    //       $scope.report_status = data.sims_attendance_code
                    //       $scope.Report();
                    //       console.log("msg", msg);
                    //   }
                }
                //$scope.savedata.push(datasend);
                $scope.reportdataparameter.push(datasend);
                //$http.post(ENV.apiUrl + "api/HolidayAttendance/ReInsertSims_leaveSlip", datasend).then(function (msg) {
                //    $scope.msg1 = msg.data;


                    if ($scope.msg1 == true) {
                        $scope.report_status = data.sims_attendance_code
                        $scope.Report();
                        console.log("msg", msg);
                    }

                //});
            }



            $scope.getARule = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/HolidayAttendance/getAttendanceRule?sims_cur_code=" + $scope.temp.sims_cur_code).then(function (attrule) {
                    $scope.arule = attrule.data;
                    //$scope.edt = {
                    //    sims_attendance_code: $scope.arule[0].sims_attendance_code,
                    //    sims_cur_code: $scope.temp.sims_cur_code
                    //};

                    //$scope.edt.sims_attendance_code = $scope.arule[0].sims_attendance_code;
                   // $scope.edt.sims_attendance_code ='L'
                    $scope.edt.sims_cur_code= $scope.temp.sims_cur_code
                });
            }
          

            $scope.getSRule = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/HolidayAttendance/getSlotRule").then(function (attrule) {
                    $scope.slot = attrule.data;
                    $scope.edt = {
                        sims_bell_slot_code: $scope.slot[0].sims_bell_slot_code,
                        
                    };
                });
            }
            //debugger;
            //    $http.get(ENV.apiUrl + "api/HolidayAttendance/getsearch").then(function (searchoptions) {
            //        $scope.search_for = searchoptions.data;
            //        $scope.temp.sims_appl_parameter = $scope.searchoptions[0].sims_appl_parameter;
            //    });
           


           // $http.get(ENV.apiUrl + "api/HolidayAttendance/getsearch").then(function (searchoptions) {
           //     $scope.searchoptions = searchoptions.data;
           //
           //     for (var i = 0; i < $scope.searchoptions.length; i++) {
           //         if ($scope.searchoptions[i].sims_search_code_status == 'Y') {
           //             $scope.edt['sims_appl_parameter'] = $scope.searchoptions[0].sims_appl_parameter;
           //         }
           //     }
           // });


            $http.get(ENV.apiUrl + "api/HolidayAttendance/getsearch").then(function (searchoptions) {
                $scope.searchoptions = searchoptions.data;
                $scope.temp.search_by = $scope.searchoptions[0].sims_appl_parameter;
                
            });


            //$scope.getdetails = function () {
            //    debugger;
            //    $http.get(ENV.apiUrl + "api/classlist/getlistbyteacher?cur_code=" + $scope.temp.sims_cur_code + "&acad_year=" + $scope.temp.sims_academic_year + "&grade_code =" + $scope.temp.sims_grade_code + "&section_code=" + $scope.temp.sims_section_code).then(function (res1) {
            //        $scope.report_data = res1.data;


            //    });
            //}

            $scope.getdetails = function () {
                debugger;
                //var att_time = $filter('date')(data.att_time, 'h:mm a');
                //var today = new Date();
                $scope.getARule();
                $scope.getSRule();

                $scope.colsvis = false;
                $http.get(ENV.apiUrl + "api/HolidayAttendance/getSlipForStudents?sims_cur_code=" + $scope.temp.sims_cur_code + "&sims_acad_year=" + $scope.temp.sims_academic_year + "&sims_grade_code=" + $scope.temp.sims_grade_code +
                    "&sims_section_code=" + $scope.temp.sims_section_code + "&date=" + $scope.mom_start_date + "&search=" + $scope.temp.sims_search + "&src_by=" + $scope.temp.search_by).then(function (res1) {
                        if (res1.data.length > 0) {
                            $scope.report_data = res1.data;
                            $scope.totalItems = $scope.report_data.length;
                            $scope.todos = $scope.report_data;
                          //  $scope.att_time = $scope.att_time;
                            
                               
                            

                            $scope.makeTodos();
                          
                        }
                        else {
                            swal({ title: "Alert", text: "Data is not Available", showCloseButton: true, width: 300, height: 200 });
                            $scope.report_data = [];

                        }



                    });

                
            }
         
        
            $scope.saveReport = function (res) {


            }

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };
            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $scope.colsvis = false;

            $scope.exportData = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('rpt_data1').innerHTML],
                         {
                             type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                         });
                        $scope.detailsField = false;
                        saveAs(blob, "Student Leave Form .xls");
                        $scope.colsvis = false;
                        $scope.getdetails();

                    }
                    $scope.getdetails();
                });

            };

            $scope.print = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Print?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var docHead = document.head.outerHTML;
                        var printContents = document.getElementById('rpt_data1').outerHTML;
                        var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
                        var newWin = window.open("", "_blank", winAttr);
                        var writeDoc = newWin.document;
                        writeDoc.open();
                        writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
                        writeDoc.close();
                        newWin.focus();
                        $scope.getdetails();

                    }
                    $scope.getdetails();
                });

            };

            $scope.reset_data = function () {
                debugger;
                $scope.temp.sims_grade_code = '';
                $scope.filteredTodos = [];
                $scope.pager = false;


                try {
                    $('#cmb_grade_code').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_section_code').multipleSelect('uncheckAll');
                } catch (e) {

                }
                $scope.temp.sims_section_code = '';
                $scope.filteredTodos = [];


            }

            $scope.ischeck = function () {
                debugger
                $scope.checklick = function () {
                    var v = document.getElementById('expire');
                    if (v.checked == true) {
                        v.checked == false;
                    }
                }
            }

            $(document).ready(function () {
                $("#sleep_time_am").kendoDateTimePicker({
                    format: "hh:mm tt"
                    //twelvehour: true,
                });
            });
            $('.clockpicker').clockpicker();

            //scope.report_show = true;
            //$scope.Report=function()
            //{
            //    debugger;
            //    $scope.report_show = false;
            //    $scope.Report_loc = '';
            //    if ($scope.report_status = 'L') {
            //        $scope.Report_loc = 'Invs.INVR02rakaag'
            //    } else if ($scope.report_status == 'LE' || $scope.report_status == 'LX') {
            //        $scope.Report_loc = 'Invs.INVR02rakaag1'
            //    }
            //    var rname = res.data;
            //    var data = {
            //        location:  $scope.Report_loc,
            //        parameter: {
            //            //orderno: $scope.msg1,
            //            cur_details: $scope.reportdataparameter[0].sims_cur_code,
            //            acad_year: $scope.reportdataparameter[0].sims_acad_year,
            //            grade_code: $scope.reportdataparameter[0].sims_grade_code,
            //            section_code: $scope.reportdataparameter[0].sims_section_code,
            //            search: $scope.reportdataparameter[0].sims_enrollment_number,
                           
            //        },
            //        state: 'main.SLFQ55',
            //        ready: function () {
            //            this.refreshReport();
            //        },
            //    }

            //    window.localStorage["ReportDetails"] = JSON.stringify(data);
            //    $state.go('main.ReportCardParameter');

            //}
      

            $scope.report_show = true;
            $scope.Report = function () {
                debugger;
                //$('#feemodal').modal({ backdrop: 'static', keyboard: false });
                $('#feemodal').modal('show');
                //$scope.newDate = $filter('date')($scope.mom_start_date, 'yyyy-MM-dd');
                $scope.newDate = moment($scope.mom_start_date, "DD-MM-YYYY").format('YYYY-MM-DD');

                $scope.report_show = false;
                $scope.Report_loc = '';
                if ($scope.report_status == 'L' || $scope.report_status == 'PL') {
                    $scope.Report_loc = 'Invs.INVR02rakaag1'
                } else if ($scope.report_status == 'LE' || $scope.report_status == 'LX') {
                    $scope.Report_loc = 'Invs.INVR02rakaag'
                }
              //  $http.get(ENV.apiUrl + "api/StudentFee/GetReceiptCon").then(function (res) {
                    //var rname = res.data;
                    var data = {
                        //location: 'Sims.SIMR51FeeRec',
                        location: $scope.Report_loc,
                        parameter: {
                            cur_details: $scope.reportdataparameter[0].sims_cur_code,
                            acad_year: $scope.reportdataparameter[0].sims_acad_year,
                            grade_code: $scope.reportdataparameter[0].sims_grade_code,
                            section_code: $scope.reportdataparameter[0].sims_section_code,
                            search: $scope.reportdataparameter[0].sims_enroll_number,
                            date: $scope.newDate,
                          

                        },
                        state: 'main.SLFQ55',
                        ready: function () {
                            this.refreshReport();
                        },
                    }


                    console.log(data);
                    window.localStorage["ReportDetails"] = JSON.stringify(data)

                    // });

                    //For Report Tool////////////////////////////////////////////////////////////////////////////////


                    try {
                        $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                        $scope.location = $scope.rpt.location;
                        $scope.parameter = $scope.rpt.parameter;
                        $scope.state = $scope.rpt.state;
                    }
                    catch (ex) {
                    }
                    var s;

                //if ($http.defaults.headers.common['schoolId'] == 'dpsmis')
                //    s = "SimsReports." + $scope.location + ",SimsReports";


                //else
                //    s = "SimsReports." + $scope.location + ",SimsReports" + $http.defaults.headers.common['schoolId'];


                //  window.localStorage["Finn_comp"] = JSON.stringify(data)

                    s = "SimsReports." + $scope.location + ",SimsReports";

                    var url = window.location.href;
                    var domain = url.substring(0, url.indexOf(':'))
                    if ($http.defaults.headers.common['schoolId'] == 'sms') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    }

                    else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'staging' || $http.defaults.headers.common['schoolId'] == 'staging2' || $http.defaults.headers.common['schoolId'] == 'aisdxb' || $http.defaults.headers.common['schoolId'] == 'tiadxb' || $http.defaults.headers.common['schoolId'] == 'tiashj' || $http.defaults.headers.common['schoolId'] == 'tosdxb') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.leamseducation.com/report/api/reports/';
                    }
                    else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    }

                    else {
                        if (domain == 'https')
                            var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                        else
                            var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    }
                    console.log(service_url);


                    $scope.parameters = {}
                    debugger;
                    $("#reportViewer1")
                                   .telerik_ReportViewer({
                                       //serviceUrl: ENV.apiUrl + "api/reports/",
                                       serviceUrl: service_url,

                                       viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                       scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                       // Zoom in and out the report using the scale
                                       // 1.0 is equal to 100%, i.e. the original size of the report
                                       scale: 1.0,
                                       ready: function () {
                                           //this.refreshReport();
                                       }

                                   });





                    var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                    reportViewer.reportSource({
                        report: s,
                        parameters: $scope.parameter,
                    });

                //var rv = $("#reportViewer1").data("telerik_ReportViewer");
                //rv.commands.print.exec();

                    setInterval(function () {

                        $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                        $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                        //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });

                    }, 1000);


                    $timeout(function () {
                        $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                        $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                        //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                        //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });


                    }, 100)


           // });
        }

            var flag=false;
            $scope.Issue_buttonDisply = function (data ,i) {
                debugger;
                //for (var i = 0; i < $scope.filteredTodos.length; i++) {
                if ($scope.filteredTodos_copy[i].att_time == data.att_time && $scope.filteredTodos_copy[i].sims_attendance_code == data.sims_attendance_code && $scope.filteredTodos_copy[i].sims_slot_code == data.sims_slot_code) {

                    $scope.filteredTodos[i].flag = false;
                }
                else {
                    $scope.filteredTodos[i].flag = true;

                }
               // }

            }
            



            document.onkeydown = function (event) {
                debugger
                event = event || window.event;
                if (event.keyCode == 27) {
                    $('#feemodal').modal('hide');
                }
            }

        }]
        )
        })();