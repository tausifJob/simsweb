﻿(function () {
    'use strict';
    var attendancecode = [];
    var curcode = [];
    var main;
    var simsController = angular.module('sims.module.Attendance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AttendanceAbsentReportCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            //$scope.operation = true;
            //$scope.table1 = true;
            $scope.editmode = false;
            $scope.fcommn = [];
            var data1 = [];
            $scope.busyindicator = true;

           
            $scope.size = function (str) {

                if (str == 'All') {

                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.filteredTodos = $scope.obj;
                    $scope.numPerPage = $scope.obj.length;
                    $scope.maxSize = $scope.obj.length
                    $scope.makeTodos();
                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
            }
            $scope.edt = "";
            $scope.table1 = true;

            $http.get(ENV.apiUrl + "api/LibraryAttribute/getCuriculum").then(function (res1) {
                debugger
                $scope.cur_data = res1.data;
                $scope.fcommn.sims_cur_code = $scope.cur_data[0].sims_cur_code;
                $scope.getAcademic_year($scope.cur_data[0].sims_cur_code);
            });

            $scope.getAcademic_year = function (cur_code1) {
                debugger
                $http.get(ENV.apiUrl + "api/LibraryAttribute/getAcademicYear?curCode=" + cur_code1).then(function (res1) {
                    $scope.acad_data = res1.data;
                    $scope.fcommn.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                    $scope.getAllGrades();
                });
            }

            $scope.getAllGrades = function () {
                debugger
                $http.get(ENV.apiUrl + "api/LibraryAttribute/getAllGrades?cur_code=" + $scope.fcommn.sims_cur_code + "&academic_year=" + $scope.fcommn.sims_academic_year).then(function (res1) {
                    $scope.grade_data = res1.data;
                    $scope.fcommn.sims_grade_code = $scope.grade_data[0].sims_grade_code;
                    $scope.getSectionFromGrade();
                });
            }

            $scope.getSectionFromGrade = function (cur_code1, grade_code1, academic_year1) {
                debugger
                $http.get(ENV.apiUrl + "api/LibraryAttribute/getSectionFromGrade?cur_code=" + $scope.fcommn.sims_cur_code + "&grade_code=" + $scope.fcommn.sims_grade_code + "&academic_year=" + $scope.fcommn.sims_academic_year).then(function (res1) {
                    $scope.section_data = res1.data;
                    $scope.fcommn.sims_section_code = $scope.section_data[0].sims_section_code;
                });
            }
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item['sims_enrollment_number'].toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item['student_name'].toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            $scope.getstudentattendance = function () {
                var data ={};
                data = {
                    sims_cur_code: $scope.fcommn.sims_cur_code,
                    sims_academic_year: $scope.fcommn.sims_academic_year,
                    sims_grade_code: $scope.fcommn.sims_grade_code,
                    sims_section_code: $scope.fcommn.sims_section_code,
                    from: $scope.fcommn.from_date,
                    to: $scope.fcommn.to_date
                }
                debugger
                $http.post(ENV.apiUrl + "api/AttendanceAbsentReport/CGDstudentattendance", data).then(function (res1) {
                    debugger
                   // $scope.reportHeadig = [];
                    $scope.obj = res1.data.table;
                    //$scope.headers = res1.data.table[0];
                    //angular.forEach($scope.headers, function (value, key) {
                    //    if (
                    //        key != 'sims_cur_code' && key != 'sims_academic_year' && key != 'sims_grade_code' && key != 'sims_section_code' && key != 'sims_grade_name_en' &&
                    //        key != 'sims_section_name_en' 
                    //        ) {
                    //        var obj = {
                    //            heading: key
                    //        }
                    //        $scope.reportHeadig.push(obj);
                    //    }
                    //    console.log($scope.reportHeadig);
                    //});
                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.makeTodos();
                });
            }

            $scope.OpenReport = function (str) {
                debugger
                //$scope.report_para = [];
                //$scope.arr = [];
                //$scope.arr = v;
                //angular.forEach($scope.arr, function (value, key) {
                //    debugger
                //    if (str.heading == key) {
                //        var obj = {
                //            sims_enrollment_number: v.sims_enrollment_number,
                //            student_name: v.student_name,
                //            sims_academic_year: v.sims_academic_year,
                //            attendance_code: value
                //        }
                //        $scope.report_para.push(obj);
                //    }
                //});
                //console.log($scope.report_para);
                //if ($scope.report_para[0].attendance_code != 'A') {
                //    swal({ text: 'Cannot Open Report For Other Than Apsent Report. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                //}
                //else {
                //    swal({ text: 'Cannot Open Report For Other Than Apsent Report. ', imageUrl: "assets/img/close.png", width: 320, showCloseButton: true });
                //}
                var data = {

                    location: "Sims.SIMR153",
                    parameter: {
                        sims_cur_code: str.sims_cur_code,
                        sims_academic_year: str.sims_academic_year,
                        sims_grade_code: str.sims_grade_code,
                        sims_section_code: str.sims_section_code,
                        sims_enroll_number: str.sims_enroll_number,
                        from: $scope.fcommn.from_date,
                        to: $scope.fcommn.to_date
                    },
                    state: 'main.ATTRPT',
                    ready: function () {
                        this.refreshReport();
                    },
                }
                console.log(data);
                window.localStorage["ReportDetails"] = JSON.stringify(data)
                $state.go('main.ReportCardParameter')
            }


        }]
        )
})();