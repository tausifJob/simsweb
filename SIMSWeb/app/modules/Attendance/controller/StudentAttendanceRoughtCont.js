﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Attendance');

    simsController.directive('ngRightClick', function ($parse) {
        return function (scope, element, attrs) {
            var fn = $parse(attrs.ngRightClick);
            element.bind('contextmenu', function (event) {
                scope.$apply(function () {
                    event.preventDefault();
                    fn(scope, { $event: event });
                });
            });
        };
    });

    simsController.controller('StudentAttendanceRoughtCont', ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter',
    function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {

        $scope.hidesummary = true;

        $('*[data-datepicker="true"] input[type="text"]').datepicker(
            {
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd',
                endDate: (new Date())
            });


        console.log(moment(new Date(), "yyyy-MM-dd"), 'date');
        $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '', attDate: moment(new Date()).format('dd-MM-yyyy') };
        $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
            $('input[type="text"]', $(this).parent()).focus();
        });


         $scope.username = $rootScope.globals.currentUser.username;
         $scope.edt.attDate = $filter('date')(new Date(), 'dd-MM-yyyy');

        $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum?username=" + $rootScope.globals.currentUser.username).then(function (res) {
            debugger;
            $scope.curiculum = res.data;
            $scope.edt['sims_cur_code'] = $scope.curiculum[0].sims_cur_code;
            $scope.cur_change();
        });

        $http.get(ENV.apiUrl + "api/attendanceRought/getRoughtInOut").then(function (res) {
            debugger;
            $scope.inout = res.data;
            
        });


        
       
        $scope.cur_change = function () {

            debugger;

            $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllAcademicYears?cur_code=" + $scope.edt.sims_cur_code).then(function (AcademicYears) {
                $scope.academicyears = AcademicYears.data;
                
                for (var i = 0; i < $scope.academicyears.length; i++) {
                    if ($scope.academicyears[i].sims_academic_year_status == 'C' || $scope.academicyears[i].sims_academic_year_status == 'Current') {
                        //   $scope.edt['sims_cur_code'] = cur_values;
                        $scope.edt['sims_academic_year'] = $scope.academicyears[i].sims_academic_year;
                    }
                }
                $scope.acdm_yr_change();
                $scope.getAttendancerought();
                //$scope.date();
            });

           

            $http.get(ENV.apiUrl + "api/attendanceRought/getAttendanceBusCode?cur_code=" + $scope.edt.sims_cur_code).then(function (attendance_code) {
                $scope.attendance_code = attendance_code.data;
                //$scope.rdbtn = $scope.attendance_code[0].sims_attendance_code;
            });
        }
        $scope.acdm_yr_change = function () {
            $http.get(ENV.apiUrl + "api/attendanceRought/getAttendancegrade?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&userName=" + $scope.username).then(function (grades) {

                $scope.grades = grades.data;
            });
        }
        $scope.grade_change = function () {

            $http.get(ENV.apiUrl + "api/attendanceRought/getAttendancesection?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&gradeCode=" + $scope.edt.sims_grade_code + "&userName=" + $scope.username).then(function (section) {
                $scope.section = section.data;
            });

        }

        $scope.grade_change = function () {

            $http.get(ENV.apiUrl + "api/attendanceRought/getAttendancesection?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&gradeCode=" + $scope.edt.sims_grade_code + "&userName=" + $scope.username).then(function (section) {
                $scope.section = section.data;
            });

        }

        $scope.date = function () {
            debugger;
            $http.get(ENV.apiUrl + "api/attendanceRought/getAttendancedate").then(function (date) {
                $scope.todate = date.data;
                $scope.edt.attDate = $scope.todate.currentdate;

            });

        }

        //
        $scope.getAttendance = function () {
            debugger;
            $scope.searchbox = true;
            if ($scope.edt.attDate == '') {
                swal({ text: "Please select Attendance date.", imageUrl: "assets/img/notification-alert.png", width: 400, showCloseButton: true });

            }
            else {
                if ($scope.edt.sims_rought_parameter == 'I') {
                    $scope.dayValue = 'AM'

                }
                else {
                    $scope.dayValue = 'PM'
                }
                $http.get(ENV.apiUrl + "api/attendanceRought/getNewBusStudent?routecode=" + $scope.edt.sims_transport_route_code + "&attdate=" + $scope.edt.attDate + "&daycode=" + $scope.dayValue + "&picdrop=" + $scope.edt.sims_transport_parameter).then(function (attendance) {
                        if (attendance.data.length <= 0) {
                            swal({ text: "No Attendance Data is found for this date.", imageUrl: "assets/img/close.png", width: 400, showCloseButton: true });
                            $scope.attendance = [];

                        }
                        else {
                            if (attendance.data[0].sims_isholiday == true) {
                                swal({ text: "Attendance can not be marked on Weekends And Holidays.", imageUrl: "assets/img/close.png", width: 400, showCloseButton: true });
                                $scope.attendance = [];
                            }
                            else {

                                $scope.attendance = attendance.data;
                                $scope.attendance_copy = angular.copy($scope.attendance);

                                //for (var i = 0; i < $scope.attendance.length; i++)
                                //{
                                //    if ($scope.attendance[i].sims_attendance_pickup_drop_time == '12:00AM')
                                //    {
                                //        $scope.attendance[i].sims_attendance_pickup_drop_time = '';
                                //    }
                                //}
                                $scope.stud_table = true;
                                $scope.selecteddate = $scope.edt.attDate;
                                $scope.totalstudents = $scope.attendance.length;
                                $scope.hidesummary = false;
                            }
                        }
                       // $scope.update();
                    });
               
            }
        }
        $scope.sims_attendance_code = '';
        $scope.value = '';
        $scope.attcode = function (info) {
            debugger;
            $scope.mnuCard = info;
            $scope.value = info;
            $scope.sims_attendance_code = $scope.value.sims_attendance_code;
        }

        $scope.Mark_Attendance = function (ob) {
            ob['attendance_mark_code'] = $scope.sims_attendance_code;
            ob['attendance_day1_color_code'] = $scope.value.sims_attendance_color;
        }

        //save data new bus attenddence
        $scope.att_list = [];
        $scope.submit = function () {
            debugger;
            $scope.res = {};
            var dataPost = [];
            
                $scope.res['med_det'] = [];
                var rt_code = [];
                
                
            
                for (var i = 0; i < $scope.attendance.length; i++) {

                    var dat = $scope.attendance[i];
                   dat.opr = 'NA';

                    if ($scope.edt.sims_rought_parameter == 'I') {
                        dat.subopr = 'AM';
                    } else {
                        dat.subopr = 'PM';
                    }

                    if ($scope.edt.sims_transport_parameter == 'P') {
                        dat.ssubopr = 'P';
                    }
                    else {
                        dat.ssubopr = 'D';
                    }


                    dat['ayear'] = $scope.edt.sims_academic_year;
                    dat['sims_transport_date'] = $scope.edt.attDate;
                    //dat['sims_transport_route_code'] = $scope.edt.sims_transport_route_code;
                    dat['caretaker_code'] = $scope.username;
                    

                    //dat['sims_transport_enrollment_number'] = $scope.attendance[i].sims_transport_enrollment_number;
                    //dat['sims_attendance_pickup_drop_time'] = $scope.attendance[i].sims_attendance_pickup_drop_time;
                    if ($scope.attendance[i].sims_attendance_pickup_drop_time != undefined)

                            dataPost.push(dat);
                    
                    }
                   // $scope.res['med_det'].push(rt_code);
                    $scope.flag = true;

                    if ($scope.flag == false) {
                        swal('', 'Please select the records to Insert');
                        return;
                    }

                

                $http.post(ENV.apiUrl + "api/attendanceRought/markNewBusStudentAttendance", dataPost).then(function (attendance) {
                    if (attendance.data) {
                        swal({ text: "Successfully", imageUrl: "assets/img/check.png", width: 350, showCloseButton: true })
                        $scope.getAttendance();
                    }
                    else {
                        $scope.attendance = $scope.attendance_copy;
                        swal({ text: "Not Successfully", imageUrl: "assets/img/close.png", width: 350, showCloseButton: true })
                    }

                    $scope.att_list = [];
                });


            
            }
        
         $scope.markattendnace = function (objinp) {
            if ($scope.sims_attendance_code != '' && ($scope.edt.attDate != '' || $scope.edt.attDate != undefined)) {
                if ($scope.sims_attendance_code != 'W' && $scope.sims_attendance_code != 'H') {
                    objinp.sims_attendance_code = $scope.value.sims_attendance_code;
                    objinp.sims_attendance_color = $scope.value.sims_attendance_color;
                   
                    var obj1 = { cur_code: $scope.edt.sims_cur_code, ayear: $scope.edt.sims_academic_year, grade: $scope.edt.sims_grade_code, section: $scope.edt.sims_section_code, enrollNumber: objinp.sims_student_enroll, att_code: objinp.sims_attendance_code, attednacedate: $scope.edt.attDate, user_name: $rootScope.globals.currentUser.username }
                    $scope.att_list.push(obj1)
                }
                else { swal({ text: "Attendance Code Not Marked", imageUrl: "assets/img/close.png", width: 350, showCloseButton: true }); }


            }
            else {
                swal({ text: "Please select Attendance Code", imageUrl: "assets/img/notification-alert.png", width: 350, showCloseButton: true });
            }
        }
        $scope.reset = function () {
            $scope.sims_attendance_code = '';
            $scope.value = '';
            $scope.attendance = '';
            $scope.attendance_code = '';
            $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', attDate: '' };
            $scope.totalstudents = 0;
            $scope.hidesummary = true;
            $scope.total_present = 0;
            $scope.total_absent = 0;
            $scope.att_list = [];
        }
       
        
        $scope.markAllAttendance = function (att_Code) {
            debugger;

            for (var i = 0; i <$scope.attendance_code.length; i++) {
    
                if(att_Code==$scope.attendance_code[i].sims_attendance_code)
                    $scope.color =$scope.attendance_code[i].sims_attendance_color;
                    }

            for (var i = 0; i < $scope.attendance.length; i++) {
                $scope.attendance[i].attendance_mark_code = att_Code;
                $scope.attendance[i].attendance_day1_color_code =  $scope.color;


                }
                            }

         
 


        $scope.comment = function (sobj) {

            $scope.copy_comment = angular.copy(sobj);
            $scope.attednacedate2 = $scope.edt.attDate

            $("#comment_dailog").modal("show");

            $http.get(ENV.apiUrl + "api/attendanceRought/get_AttendanceComment?enroll=" + $scope.copy_comment.sims_student_enroll + "&date=" + $scope.edt.attDate).then(function (res) {
                if (res.data.length > 0) {
                    $scope.copy_comment['sims_attendance_day_comment'] = res.data[0].sims_attendance_day_comment;
                }
            });

        }

        $scope.save_comment = function () {

            $http.post(ENV.apiUrl + "api/attendanceRought/markAttendanceComment_update?enroll=" + $scope.copy_comment.sims_student_enroll + "&date=" + $scope.edt.attDate + "&comment=" + $scope.copy_comment.sims_attendance_day_comment).then(function (res) {
                $("#comment_dailog").modal("hide");
            });
        }

        $scope.getAttendancerought = function () {
           
            $http.get(ENV.apiUrl + "api/attendanceRought/getBusRoute?caretaker_code=" + $scope.username).then(function (rought) {
                $scope.rought = rought.data;
            });

        }
        //$scope.getAttendancerought();



        $scope.getDays_in_month = function () {
            $scope.list_student_data = [];
            $scope.stud_table = false;
            var year = sims_month_name.split('-')[1];
            var month = sims_month_name.split('-')[0];
            month = new Date(Date.parse(month + " 1," + year)).getMonth() + 1;
            $scope.month_number = month;
            $scope.att_year = year;
            //   debugger
            $scope.rowscount = new Date(year, month, 0).getDate();
            var dayNameArr = new Array("SUN", "MON", "TUE", "WED", "THR", "FRD", "SAT");
            /* var daysInMonth = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
             var monthNames = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
             if ((month == 2) && (year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) {
                 $scope.rowscount = 29;
             } else {
                 $scope.rowscount = daysInMonth[month];
             }
             */
            $scope.temp = [];
            $scope.days = [];
            for (var i = 1; i <= $scope.rowscount; i++) {
                var data = [];
                var d = new Date(year + '-' + month + '-' + i);
                var dayNo = (i % 7);
                if (dayNo == 0) {
                    dayNo = 7;
                }
                data['id'] = i + ' ' + dayNameArr[d.getDay()];
                $scope.temp.push(data);

                $scope.days.push({ day: i, mon: dayNameArr[d.getDay()] })
            }

        }


         //|| $scope.term_code == undefined || $scope.term_code == '' || $scope.sims_month_name == undefined || $scope.sims_month_name == ''
    
        //|| $scope.term_code == undefined || $scope.term_code == '' || $scope.sims_month_name == undefined || $scope.sims_month_name == ''

        $scope.daycodeClick = function (count) {
            debugger;
            switch (count) {
                case 'I':
                    $scope.dayValue = 'AM'

                   // $scope.pickupDrop($scope.edt['sims_appl_parameter']);
                    // $scope.edt.sims_rought_parameter = $scope.dayValue;
                    $scope.pickupDrop();
                    break;
                case 'O':
                    $scope.dayValue = 'PM'
                   // $scope.pickupDrop($scope.edt['sims_appl_parameter']);
                   //  $scope.edt.sims_rought_parameter = $scope.dayValue;
                    $scope.pickupDrop();
                    break;
            }
        }

         $scope.pickupDrop = function () {
            debugger;
            $http.get(ENV.apiUrl + "api/attendanceRought/getRoughtPickDrop").then(function (res) {
                debugger;
                $scope.pickdrop = res.data;

            });
         }
    }])

})();