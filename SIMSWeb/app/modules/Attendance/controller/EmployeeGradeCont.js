﻿(function () {
    'use strict';
    var obj1, obj2, temp, main;
    var opr = '';
    var gradecode = [];
  


    var simsController = angular.module('sims.module.Attendance');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('EmployeeGradeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.BoardDetail = true;
            $scope.editmode = false;
            var formdata = new FormData();
            $scope.edt = {};
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            $http.get(ENV.apiUrl + "api/EmpGrade/getComapany").then(function (res) {
               
                $scope.compname = res.data;
                $scope.edt.eg_company_code = $scope.compname[0].comp_code;
                
            })
     
            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            //$scope.size = function (str) {

            //    if (str == 'All') {

            //        $scope.totalItems = $scope.BoardData.length;
            //        $scope.todos = $scope.BoardData;
            //        $scope.filteredTodos = $scope.GetAllStudentFee;
            //        $scope.numPerPage = $scope.BoardData.length;
            //        $scope.maxSize = $scope.BoardData.length
            //        $scope.makeTodos();
            //    }
            //    else {
            //        $scope.pagesize = str;
            //        $scope.currentPage = 1;
            //        $scope.numPerPage = str;
            //        $scope.makeTodos();
            //    }

            //}

            $scope.index = function (str) {
                $scope.pageindex = str;
             
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            }


            $scope.cancel = function () {
                $scope.BoardDetail = true;
                $scope.BoardData1 = false;
                $scope.edt = {};
                $scope.edt.eg_company_code = $scope.compname[0].comp_code;
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    $scope.myForm.$setPristine();
                    $scope.newmode = true;
                    $scope.check = true;
                    $scope.editmode = false;
                    opr = 'S';
                    $scope.readonly = false;
                    $scope.BoardDetail = false;
                    $scope.BoardData1 = true;
                    $scope.savebtn = true;
                    $scope.updatebtn = false;
                }
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists, function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.BoardData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.BoardData;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.eg_grade_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.eg_grade_desc == toSearch) ? true : false;
            }

            $scope.getEmpGradeDetails = function () {
                $http.get(ENV.apiUrl + "api/EmpGrade/getEmpgradeDetail").then(function (Board_Data) {
                    $scope.BoardData = Board_Data.data;
                    $scope.totalItems = $scope.BoardData.length;
                    $scope.todos = $scope.BoardData;
                    $scope.makeTodos();
                    console.log($scope.BoardData);
                });
            }
            
            $scope.getEmpGradeDetails();

            //$http.get(ENV.apiUrl + "api/EmpGrade/getEmpgradeDetail").then(function (res) {
            //    $scope.BoardData1 = false;
            //    $scope.BoardDetail = true;
            //    $scope.obj = res.data;
            //    console.log($scope.obj);
            //});


            $scope.Save = function (myForm) {
                debugger;
                if (myForm) {

                    if(parseFloat($scope.edt.eg_max_basic) < parseFloat($scope.edt.eg_min_basic)) {
                        swal({  text: "Maximum basic should be greater than minimum basic",imageUrl: "assets/img/notification-alert.png", width: 360 });
                        return;
                    }

                    var data = $scope.edt;
                    data.opr = 'I';
                   
                    $scope.exist = false;
                    debugger;
                    for (var i = 0; i < $scope.BoardData.length; i++) {
                        if ($scope.BoardData[i].eg_grade_code == data.eg_grade_code) {
                            $scope.exist = true;
                        }

                    }
                    if ($scope.exist) {                       
                        swal({ text: "Grade code already exists", imageUrl: "assets/img/notification-alert.png",width: 320 });
                    }   

                    else {

                        $http.post(ENV.apiUrl + "api/EmpGrade/EmpGradeCUD", data).then(function (msg) {
                            $scope.msg1 = msg.data;
                            $scope.BoardData1 = false;                            
                            if ($scope.msg1 == true) {
                                $rootScope.strMessage = 'Record Inserted Successfully';
                                swal({ text: "Record Inserted Successfully",imageUrl: "assets/img/check.png", width: 320 });
                                $scope.getEmpGradeDetails();
                                $scope.cancel();

                            }
                            else {

                                $rootScope.strMessage = 'Record Not Inserted';

                                swal({  text: "Record Not Inserted. " + $scope.msg1, imageUrl: "assets/img/close.png",width: 320 });
                            }


                        });
                    }
                    $scope.data1 = "";
                    $scope.BoardDetail = true;
                    $scope.BoardData1 = false;



                   
                }
            }


            $scope.up = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    opr = 'U';
                    //$scope.edt = str;
                    $scope.editmode = true;
                    $scope.newmode = false;
                    $scope.savebtn = false;
                    $scope.updatebtn = true;
                    $scope.readonly = true;
                    $scope.BoardDetail = false;
                    $scope.BoardData1 = true;

                    $scope.edt = {
                        eg_company_code: str.eg_company_code
                                , eg_grade_code: str.eg_grade_code
                                , eg_grade_desc: str.eg_grade_desc
                                , eg_min_basic: str.eg_min_basic
                                , eg_max_basic: str.eg_max_basic

                    };




                }
            }

            $scope.Update = function () {

                if (parseFloat($scope.edt.eg_max_basic) < parseFloat($scope.edt.eg_min_basic)) {
                    swal({ text: "Maximum basic should be greater than minimum basic",imageUrl: "assets/img/notification-alert.png", width: 360 });
                    return;
                }

                var data = $scope.edt;
                data.opr = 'U';
                $http.post(ENV.apiUrl + "api/EmpGrade/EmpGradeCUD", data).then(function (msg) {
                    $scope.msg1 = msg.data;
                    $scope.BoardData1 = false;                    
                    if ($scope.msg1 == true) {
                        $rootScope.strMessage = 'Record Updated Successfully';                        
                        swal({ text: "Record Updated Successfully",imageUrl: "assets/img/check.png", width: 320 });
                        $scope.getEmpGradeDetails();
                        $scope.cancel();
                    }
                    else {
                        $rootScope.strMessage = 'Record Not Updated';
                        swal({  text: "Record Not Updated. " + $scope.msg1,imageUrl: "assets/img/close.png", width: 320 });
                    }
                })
                $scope.BoardData1 = false;
                $scope.BoardDetail = true;
            }


         
            $scope.check_all = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].eg_grade_code;

                        var v = document.getElementById(t + i);
                        v.checked = true;
                        gradecode = gradecode + $scope.filteredTodos[i].eg_grade_code + ','
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].eg_grade_code;
                        var v = document.getElementById(t + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';

                    }
                }
            }

            $scope.check_once = function () {
                main = document.getElementById('all_chk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                }
                else {
                    main.checked = false;
                    $scope.row1 = '';
                }
            }

          

           

            $scope.deleterecord = function () {
                debugger;
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    gradecode = [];
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].eg_grade_code;
                        var v = document.getElementById(t + i);

                        if (v.checked == true)
                            gradecode = gradecode + $scope.filteredTodos[i].eg_grade_code + ',';
                    }

                    var deletecountrycode = ({
                        'eg_grade_code': gradecode,

                        'opr': 'D'
                    });

                    $http.post(ENV.apiUrl + "api/EmpGrade/EmpGradeCUD", deletecountrycode).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.BoardDetail = true;
                        $scope.BoardData1 = false;
                        $rootScope.strMessage = $scope.msg1.strMessage;
                        if ($scope.msg1 == true) {
                            $rootScope.strMessage = 'Record Deleted Successfully';
                            $scope.cancel();
                            $scope.getEmpGradeDetails();
                            swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", width: 320 });
                        }
                        else {

                            $rootScope.strMessage = 'Record Not Deleted';
                            //$('#message').modal('show');
                            swal({ text: "Record Not Deleted. " + $scope.msg1, imageUrl: "assets/img/close.png", width: 320 });
                        }
                    });
                }
            }

           
        }]);

})();