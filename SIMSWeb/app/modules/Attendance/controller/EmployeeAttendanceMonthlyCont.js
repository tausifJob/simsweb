﻿(function () {
    'use strict';
    var prv;
    var nxt;
    var simsController = angular.module('sims.module.Attendance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    //simsController.directive('contextMenu', ["$parse", "$q", function ($parse, $q) {

    //    var contextMenus = [];

    //    var removeContextMenus = function (level) {
    //        while (contextMenus.length && (!level || contextMenus.length > level)) {
    //            contextMenus.pop().remove();
    //        }
    //        if (contextMenus.length == 0 && $currentContextMenu) {
    //            $currentContextMenu.remove();
    //        }
    //    };

    //    var $currentContextMenu = null;

    //    var renderContextMenu = function ($scope, event, options, model, level) {
    //        if (!level) { level = 0; }
    //        if (!$) { var $ = angular.element; }
    //        $(event.currentTarget).addClass('context');
    //        var $contextMenu = $('<div>');
    //        if ($currentContextMenu) {
    //            $contextMenu = $currentContextMenu;
    //        } else {
    //            $currentContextMenu = $contextMenu;
    //        }
    //        $contextMenu.addClass('dropdown clearfix');
    //        var $ul = $('<ul>');
    //        $ul.addClass('dropdown-menu');
    //        $ul.attr({ 'role': 'menu' });
    //        $ul.css({
    //            display: 'block',
    //            position: 'absolute',
    //            left: event.pageX + 'px',
    //            top: event.pageY + 'px',
    //            "z-index": 10000
    //        });
    //        angular.forEach(options, function (item, i) {
    //            var $li = $('<li>');
    //            if (item === null) {
    //                $li.addClass('divider');
    //            } else {
    //                var nestedMenu = angular.isArray(item[1])
    //                  ? item[1] : angular.isArray(item[2])
    //                  ? item[2] : angular.isArray(item[3])
    //                  ? item[3] : null;
    //                var $a = $('<a>');
    //                $a.css("padding-right", "8px");
    //                $a.attr({ tabindex: '-1' });
    //                var text = typeof item[0] == 'string' ? item[0] : item[0].call($scope, $scope, event, model);
    //                $q.when(text).then(function (text) {
    //                    $a.text(text);
    //                    if (nestedMenu) {
    //                        $a.css("cursor", "default");
    //                        $a.append($('<strong style="font-family:monospace;font-weight:bold;float:right;">&gt;</strong>'));
    //                    }
    //                });
    //                $li.append($a);

    //                var enabled = angular.isFunction(item[2]) ? item[2].call($scope, $scope, event, model, text) : true;
    //                if (enabled) {
    //                    var openNestedMenu = function ($event) {
    //                        removeContextMenus(level + 1);
    //                        var ev = {
    //                            pageX: event.pageX + $ul[0].offsetWidth - 1,
    //                            pageY: $ul[0].offsetTop + $li[0].offsetTop - 3
    //                        };
    //                        renderContextMenu($scope, ev, nestedMenu, model, level + 1);
    //                    }
    //                    $li.on('click', function ($event) {
    //                        //$event.preventDefault();
    //                        $scope.$apply(function () {
    //                            if (nestedMenu) {
    //                                openNestedMenu($event);
    //                            } else {
    //                                $(event.currentTarget).removeClass('context');
    //                                removeContextMenus();
    //                                item[1].call($scope, $scope, event, model);
    //                            }
    //                        });
    //                    });

    //                    $li.on('mouseover', function ($event) {
    //                        $scope.$apply(function () {
    //                            if (nestedMenu) {
    //                                openNestedMenu($event);
    //                            }
    //                        });
    //                    });
    //                } else {
    //                    $li.on('click', function ($event) {
    //                        $event.preventDefault();
    //                    });
    //                    $li.addClass('disabled');
    //                }
    //            }
    //            $ul.append($li);
    //        });
    //        $contextMenu.append($ul);
    //        var height = Math.max(
    //            document.body.scrollHeight, document.documentElement.scrollHeight,
    //            document.body.offsetHeight, document.documentElement.offsetHeight,
    //            document.body.clientHeight, document.documentElement.clientHeight
    //        );
    //        $contextMenu.css({
    //            width: '100%',
    //            height: height + 'px',
    //            position: 'absolute',
    //            top: 0,
    //            left: 0,
    //            zIndex: 9999
    //        });
    //        $(document).find('body').append($contextMenu);
    //        $contextMenu.on("mousedown", function (e) {
    //            if ($(e.target).hasClass('dropdown')) {
    //                $(event.currentTarget).removeClass('context');
    //                removeContextMenus();
    //            }
    //        }).on('contextmenu', function (event) {
    //            $(event.currentTarget).removeClass('context');
    //            event.preventDefault();
    //            removeContextMenus(level);
    //        });
    //        $scope.$on("$destroy", function () {
    //            removeContextMenus();
    //        });

    //        contextMenus.push($ul);
    //    };
    //    return function ($scope, element, attrs) {
    //        element.on('contextmenu', function (event) {
    //            event.stopPropagation();
    //            $scope.$apply(function () {
    //                event.preventDefault();
    //                var options = $scope.$eval(attrs.contextMenu);
    //                var model = $scope.$eval(attrs.model);
    //                if (options instanceof Array) {
    //                    if (options.length === 0) { return; }
    //                    renderContextMenu($scope, event, options, model);
    //                } else {
    //                    throw '"' + attrs.contextMenu + '" not an array';
    //                }
    //            });
    //        });
    //    };
    //}]);

    simsController.controller('EmployeeAttendanceMonthlyCont',
      ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

          var att_date1, att_date;
          $scope.prevDisabled = true;
          $scope.subDisabled = true;
          $scope.Atten_Codes = false;
          $scope.stud_table = true;
          $scope.data1 = [];
          $scope.showForDPSMIS = false;
          $scope.markWeekendForIIS = false;

          //current date & user name
          var today = new Date();
          var dd = today.getDate();
          var mm = today.getMonth() + 1;
          var yyyy = today.getFullYear();
          $scope.todays_date = dd + '-' + mm + '-' + yyyy;
          $scope.todays_date1 = yyyy + '-' + '0' + mm + '-' + dd;
          $scope.ddaayy = dd;
          var Current_Username = $rootScope.globals.currentUser.username;
          $scope.leaveCode_data = [];

          $http.get(ENV.apiUrl + "api/emp/attendance/getCompanyName").then(function (comp) {
              $scope.comp_data = comp.data;
              $scope.temp = {
                  'company_code': $scope.comp_data[0].company_code,
              }
              $scope.Comp_Change($scope.temp['company_code']);              
          });

          var divisionFilter = ['dpsmis'];

          if (divisionFilter.includes($http.defaults.headers.common['schoolId'])) {
              $scope.showForDPSMIS = true;

              $http.get(ENV.apiUrl + "api/Dps/HRMS/getEmpData").then(function (res) {
                  $scope.obj = res.data;
              });
          }
          var weekendMark = ['iis'];
          if (weekendMark.includes($http.defaults.headers.common['schoolId'])) {
              $scope.markWeekendForIIS = true;
          }


          $scope.attendance_codes_obj = {};
          $http.get(ENV.apiUrl + "api/emp/attendance/getAttendanceCode").then(function (attendance_code) {
              $scope.attendance_codes_obj = attendance_code.data;
              console.log("attendance_codes_obj",$scope.attendance_codes_obj);
          });

          $scope.get_attend_code = function (codes) {
             debugger
             $scope.subDisabled = false;
             $scope.pays_attendance_code = '';
             $scope.pays_attendance_color = '';
              $scope.pays_attendance_color = codes.pays_attendance_color;
              $scope.pays_attendance_code = codes.pays_attendance_code;
              $scope.pays_attendance_teacher_can_use_flag = codes.pays_attendance_teacher_can_use_flag;
          }

          $scope.Comp_Change = function (company_code) {
              $http.get(ENV.apiUrl + "api/emp/attendance/getDeptName?comp_code=" + $scope.temp.company_code).then(function (dept) {
                  $scope.dept_data = dept.data;
                  $scope.Atten_Codes = true;
                  $scope.prevDisabled = false;
                  $scope.stud_table = false;
              });

              $http.get(ENV.apiUrl + "api/PayrollEmployeeDPSD/Get_designation_Code?company_code=" + company_code).then(function (Designation) {
                  $scope.designation = Designation.data;
              });

              $http.get(ENV.apiUrl + "api/generatepayroll/GetAllPaysGradeName?company_code=" + company_code).then(function (GetAllPaysGradeName) {
                  $scope.GetAllPaysGradeName = GetAllPaysGradeName.data;
              });
          }
          
          $http.get(ENV.apiUrl + "api/emp/attendance/getGradeName").then(function (grade) {
              $scope.grade_data = grade.data;
          });

          $http.get(ENV.apiUrl + "api/emp/attendance/getStaffType").then(function (stafftype) {
              $scope.stafftype_data = stafftype.data;
          });

          $http.get(ENV.apiUrl + "api/emp/attendance/getDesignation").then(function (designation) {
              $scope.designation_data = designation.data;
          });

          $http.get(ENV.apiUrl + "api/emp/attendance/getSort").then(function (sort) {
              $scope.sort_data = sort.data;

          });

          $http.get(ENV.apiUrl + "api/emp/attendance/GetPayCycleDays").then(function (res) {
              if (res.data.length > 0) {
                  $scope.payCycleDays = res.data[0].pays_appl_form_field_value1;
                  $scope.payCycleDay = parseInt($scope.payCycleDays) - 1;
              }
              console.log("payCycleDays", $scope.payCycleDay);
          });

          $http.get(ENV.apiUrl + "api/emp/attendance/getYear").then(function (year) {
              $scope.year_data = year.data;
              $scope.temp = {
                  'sims_attendance_year': $scope.year_data[0].sims_academic_year,
                  'company_code': $scope.comp_data[0].company_code,
              }
              $scope.getMonthbyYear($scope.year_data[0].sims_academic_year);
              $scope.getMonthbyYearNew($scope.year_data[0].sims_academic_year);
          });



          $scope.colors = function (code) {
              for (var x = 0; x < $scope.attendance_codes_obj.length; x++)
                  if ($scope.attendance_codes_obj[x].pays_attendance_code == code)
                      return $scope.attendance_codes_obj[x].pays_attendance_color;
          }


          $scope.getMonthbyYear = function (str) {
              $scope.Comp_Change($scope.temp.company_code)
              $scope.data = [];

              var startdate = new Date();
              var enddate = new Date();
              
              for (var i = 0; i < $scope.year_data.length; i++) {
                  if ($scope.year_data[i].sims_academic_year == str) {
                      startdate = moment($scope.year_data[i].sims_academic_year_start_date).format('MM/DD/YYYY');
                      enddate = moment($scope.year_data[i].sims_academic_year_end_date).format('MM/DD/YYYY');
                  }
              }

              var check_date = startdate;
              $scope.data.push({ date: moment(check_date).format('MMMM-YYYY'), dateCode: moment(check_date).format('MM'), dateYear: moment(check_date).format('YYYY') })
              for (; new Date(check_date) < new Date(moment(enddate, 'MM/DD/YYYY').add('months', -1)) ;) {

                  check_date = moment(check_date, 'MM/DD/YYYY').add('months', 1);
                  check_date = moment(check_date).format('MM/DD/YYYY');
                  $scope.data.push({ date: moment(check_date).format('MMMM-YYYY'), dateCode: moment(check_date).format('MM'), dateYear: moment(check_date).format('YYYY') })
              }
          }

          $scope.getMonthbyYearNew = function (str) {
              $scope.Comp_Change($scope.temp.company_code);
              var starDate = '', endDate = '';
              for (var i = 0; i < $scope.year_data.length; i++) {
                  if ($scope.year_data[i].sims_academic_year == str) {
                      starDate = $scope.year_data[i].sims_academic_year_start_date;
                      endDate = $scope.year_data[i].sims_academic_year_end_date;
                      break;
                  }
              }
              

              $http.get(ENV.apiUrl + "api/attendance/getAttendanceMonthForEMP?startDate=" + starDate + "&endDate=" + endDate).then(function (monthDetails) {
                  $scope.monthData = monthDetails.data;
                  
              });

          }
          $scope.getDays_in_month = function (sims_month_name, sims_year) {
              var monthNoSplit = sims_month_name.split("-");
              $scope.temp.sims_academic_month = monthNoSplit[0];
              $scope.temp.sims_academic_year = monthNoSplit[1];

              sims_year = monthNoSplit[1];
              sims_month_name = monthNoSplit[0];

              
              var month, year;
              month = new Date(Date.parse(sims_month_name + " 1," + sims_year)).getMonth() + 1;
              year = sims_year;

              //if (sims_month_name == "10" || sims_month_name == "11" || sims_month_name == "12") {
              //    month = sims_month_name;
              //}
              //else {
              //    month = sims_month_name.split(0)[1];
              //}

              //if (sims_month_name == "09" || sims_month_name == "10" || sims_month_name == "11" || sims_month_name == "12") {
              //    year = parseInt(sims_year) - 1;
              //}
              //else {
              //    year = sims_year;
              //}

              //var daysInMonth = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
              //var monthNames = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
              //if ((month == 2) && (year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) {
              //    $scope.rowscount = 29;
              //} else {
              //    $scope.rowscount = daysInMonth[month];
              //}

              $scope.rowscount = new Date(year, month, 0).getDate();
              var dayNameArr = new Array("SUN", "MON", "TUE", "WED", "THR", "FRD", "SAT");

              $scope.temp1 = [];
              $scope.days = [];

              for (var i = 1; i <= $scope.rowscount; i++) {
                  var data = [];
                  var d = new Date(year + '-' + month + '-' + i);
                  var dayNo = (i % 7);
                  if (dayNo == 0) {
                      dayNo = 7;
                  }
                  data['id'] = i + ' ' + dayNameArr[d.getDay()];
                  $scope.temp1.push(data);

                  $scope.days.push({ day: i, mon: dayNameArr[d.getDay()] })
              }

              //for (var i = 1; i <= $scope.rowscount; i++) {
              //    var data = [];
              //    var dayNo = (i % $scope.rowscount);
              //    if (dayNo == 0) {
              //        dayNo = $scope.rowscount;
              //    }
              //    data['id'] = dayNo;
              //    $scope.temp1.push(data);
              //    $scope.dayN = dayNo;
              //}
          }

          //All Employee 

          $scope.getStudentMonthlyAttendance = function (comp_code, dept_code, grade_code, stafftype_code, designation_code, att_year, month_no, employee_no, Myformvalid) {
              debugger
              if (Myformvalid) {
                  if ($scope.temp.sims_attendance_month == undefined || $scope.temp.sims_attendance_month == "" || $scope.temp.sims_attendance_month == null) {
                      swal({ title: "Alert!", text: "Please select month", showCloseButton: true, width: 380, });
                      return;
                  }
                  var monthNoSplit = month_no.split("-");

                  if (month_no == "09" || month_no == "10" || month_no == "11" || month_no == "12") {
                      //$scope.att_date1 = parseInt(att_year) - 1;
                      $scope.att_date1 = monthNoSplit[1];
                  }
                  else {
                      //$scope.att_date1 = att_year;
                      $scope.att_date1 = monthNoSplit[1];
                  }



                  $scope.stud_table = false;
                  //$scope.day_column_obj = $scope.temp1;
                  $scope.Atten_Codes = true;
                  $scope.loading = true;
                  $http.get(ENV.apiUrl + "api/emp/attendance/getAllEmployeeAttendance?comp_code=" + comp_code + "&dept_code=" + dept_code + "&grade_code=" + grade_code +
                       "&stafftype_code=" + stafftype_code + "&designation_code=" + designation_code + "&att_year=" + monthNoSplit[1] + "&month_no=" + monthNoSplit[0] + "&employee_no=" + employee_no).then(function (monthlyAttendance) {
                           $scope.emp_monthlyAttendance_obj = monthlyAttendance.data;

                           if ($scope.emp_monthlyAttendance_obj == "") {
                               swal({  text: "Records Not Found",imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, });
                               $scope.Atten_Codes = false;
                               $scope.stud_table = false;
                           }
                           else {
                               $scope.subDisabled = false;
                               $scope.Atten_Codes = true;
                               $scope.stud_table = true;
                               for (var i = 0; i < $scope.emp_monthlyAttendance_obj.length; i++) {
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_1_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_1_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_2_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_2_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_3_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_3_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_4_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_4_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_5_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_5_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_4_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_4_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_6_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_6_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_7_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_7_att_code = "UM";

                                   } if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_8_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_8_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_9_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_9_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_10_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_10_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_11_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_11_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_12_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_12_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_13_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_13_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_14_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_14_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_15_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_15_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_16_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_16_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_17_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_17_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_18_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_18_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_19_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_19_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_20_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_20_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_21_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_21_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_22_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_22_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_23_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_23_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_24_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_24_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_25_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_25_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_26_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_26_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_27_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_27_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_28_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_28_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_29_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_29_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_30_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_30_att_code = "UM";
                                   }
                                   if ($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_31_att_code == "") {
                                       $scope.emp_monthlyAttendance_obj[i].pays_attendance_day_31_att_code = "UM";
                                   }

                                   $scope.emp_monthlyAttendance_obj[i].attendance_day1_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_1_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day2_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_2_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day3_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_3_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day4_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_4_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day5_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_5_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day6_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_6_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day7_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_7_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day8_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_8_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day9_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_9_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day10_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_10_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day11_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_11_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day12_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_12_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day13_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_13_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day14_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_14_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day15_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_15_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day16_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_16_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day17_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_17_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day18_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_18_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day19_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_19_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day20_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_20_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day21_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_21_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day22_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_22_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day23_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_23_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day24_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_24_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day25_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_25_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day26_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_26_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day27_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_27_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day28_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_28_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day29_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_29_att_code);
                                   //if ((month_no == "02") && (att_year % 4 == 0) && ((att_year % 100 != 0) || (att_year % 400 == 0)))
                                   //{$scope.emp_monthlyAttendance_obj[i].attendance_day29_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_29_att_code);}
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day30_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_30_att_code);
                                   $scope.emp_monthlyAttendance_obj[i].attendance_day31_color_code = $scope.colors($scope.emp_monthlyAttendance_obj[i].pays_attendance_day_31_att_code);


                               }
                           }

                           $scope.total_students = $scope.emp_monthlyAttendance_obj.length;
                           //$scope.stud_table = true;
                           $scope.loading = false;
                           $scope.btn_mark = true;
                           var d = new Date();
                           var n = d.getDate();
                           
                           var month = month_no.split(0)[1];
                           var mon = month.split("-");
                           $timeout(function () {
                               $("#fixTable").tableHeadFixer({ "left": 2 });
                           }, 100);

                           //var mon = parseInt(month);
                           debugger
                           var dy_str = 'pays_attendance_day_' + $scope.ddaayy + '_att_code';
                           $scope.P_cnt = 0, $scope.A_cnt = 0, $scope.AE_cnt = 0, $scope.T_cnt = 0, $scope.TE_cnt = 0, $scope.UM_cnt = 0, $scope.NA_cnt = 0;
                           for (var i = 0; i < $scope.emp_monthlyAttendance_obj.length; i++) {
                               //$scope.emp_monthlyAttendance_obj[i].yearno == att_year &&
                               if ($scope.emp_monthlyAttendance_obj[i][dy_str] && $scope.emp_monthlyAttendance_obj[i].monthYear == mon[0] && dy_str) {
                                   if ($scope.emp_monthlyAttendance_obj[i][dy_str] == 'P') {
                                       $scope.P_cnt = $scope.P_cnt + 1;
                                   }
                                   else if ($scope.emp_monthlyAttendance_obj[i][dy_str] == 'A') {
                                       $scope.A_cnt = $scope.A_cnt + 1;
                                   }
                                   else if ($scope.emp_monthlyAttendance_obj[i][dy_str] == 'AE') {
                                       $scope.AE_cnt = $scope.AE_cnt + 1;
                                   }
                                   else if ($scope.emp_monthlyAttendance_obj[i][dy_str] == 'T') {
                                       $scope.T_cnt = $scope.T_cnt + 1;
                                   }
                                   else if ($scope.emp_monthlyAttendance_obj[i][dy_str] == 'TE') {
                                       $scope.TE_cnt = $scope.TE_cnt + 1;
                                   }
                                   else if ($scope.emp_monthlyAttendance_obj[i][dy_str] == 'UM') {
                                       $scope.UM_cnt = $scope.UM_cnt + 1;
                                   }
                                   else if ($scope.emp_monthlyAttendance_obj[i][dy_str] == 'NA') {
                                       $scope.NA_cnt = $scope.NA_cnt + 1;
                                   }
                               }
                           }
                       }, function () {
                           //swal({ title: "Alert", text: "Internal Server Error", imageUrl: "assets/img/notification-alert.png", });
                       });
              }

          }

          //column wise attendance
          $scope.menuOptionsDayWise = [
                    ['Mark All Present', function ($itemScope) {
                        debugger
                        $('.dropdown-menu').attr('style', 'display: none');
                        $('.dropdown').remove();

                        $("#" + $scope.pays_attendance_code).attr('checked', false);

                        $scope.pays_attendance_code = 'P';
                        var d = $itemScope.i.day;

                        var month = '';
                        if ($itemScope.$parent.emp_monthlyAttendance_obj[0].monthYear < 10) {
                            month = '0' + $itemScope.$parent.emp_monthlyAttendance_obj[0].monthYear;
                        } else {
                            month = $itemScope.$parent.emp_monthlyAttendance_obj[0].monthYear;
                        }

                        var day = '';
                        if (d < 10) {
                            day = '0' + d;
                        }
                        else {
                            day = d;
                        }

                        var Mark_all_Stud_atte_DayWise1 = [];
                        var payCycleFlag = false;
                        $scope.empID = "";
                        for (var i = 0; i < $itemScope.$parent.emp_monthlyAttendance_obj.length; i++) {
                            var Mark_all_Stud_atte_DayWise = {};                            
                            if (($itemScope.$parent.emp_monthlyAttendance_obj[i].pe_status == 'P' || $itemScope.$parent.emp_monthlyAttendance_obj[i].pe_status == 'F') && (d <= $scope.payCycleDay)) {
                                payCycleFlag = true;
                                $scope.empID = $scope.empID + $itemScope.$parent.emp_monthlyAttendance_obj[i].att_emp_id + ',';
                            }
                            else {                             
                                Mark_all_Stud_atte_DayWise['opr'] = 'O';
                                Mark_all_Stud_atte_DayWise['att_emp_id'] = $itemScope.$parent.emp_monthlyAttendance_obj[i].att_emp_id;
                                Mark_all_Stud_atte_DayWise['att_date'] = $itemScope.$parent.emp_monthlyAttendance_obj[i].yearno + '-' + month + '-' + day;
                                Mark_all_Stud_atte_DayWise['att_flag'] = $scope.pays_attendance_code;
                                Mark_all_Stud_atte_DayWise1.push(Mark_all_Stud_atte_DayWise);
                            }                            
                        }

                        swal({
                            title: "Are you sure?", text: "You will not be able to recover this Data", showCancelButton: true,
                            confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, Mark it", closeOnConfirm: false, width: 380
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $scope.loading = true;
                                $http.post(ENV.apiUrl + "api/emp/attendance/MarkAllPreAbEmpAttendance", Mark_all_Stud_atte_DayWise1).then(function (res) {
                                    console.log("res", res);
                                    $scope.pays_attendance_code = null;
                                    $scope.pays_attendance_color = null;
                                    //if (res.data == true) {
                                    swal({ text: "Attendance Marked Successfully", imageUrl: "assets/img/check.png",showCloseButton: true, width: 380 });
                                        $scope.getStudentMonthlyAttendance($scope.temp.company_code, $scope.temp.dept_code, $scope.temp.gr_code, $scope.temp.staff_type_code, $scope.temp.dg_code, $scope.temp.sims_attendance_year, $scope.temp.sims_attendance_month, $scope.temp.att_emp_id, true);
                                    //}
                                        
                                });
                            }
                            
                            $scope.loading = false;
                        });

                        //if (payCycleFlag == true) {
                        //    swal({ title: 'Alert', text: 'You can not mark attendance up to ' + $scope.payCycleDay + ' days.Payroll is finalized for ' + $scope.empID });                            
                        //}

                    }],
                    null,
                    ['Mark All Absent', function ($itemScope) {

                        $("#" + $scope.pays_attendance_code).attr('checked', false);

                        $scope.pays_attendance_code = 'A';
                        var d = $itemScope.i.day;

                        $('.dropdown-menu').attr('style', 'display: none');
                        $('.dropdown').remove();

                        var month = '';
                        if ($itemScope.$parent.emp_monthlyAttendance_obj[0].monthYear < 10) {
                            month = '0' + $itemScope.$parent.emp_monthlyAttendance_obj[0].monthYear;
                        } else {
                            month = $itemScope.$parent.emp_monthlyAttendance_obj[0].monthYear;
                        }

                        var day = '';
                        if (d < 10) {
                            day = '0' + d;
                        }
                        else {
                            day = d;
                        }
                        var payCycleFlag = false;
                        $scope.empID = "";
                        var Mark_all_Stud_atte_DayWise1 = [];
                        for (var i = 0; i < $itemScope.$parent.emp_monthlyAttendance_obj.length; i++) {
                            var Mark_all_Stud_atte_DayWise = {};
                            if (($itemScope.$parent.emp_monthlyAttendance_obj[i].pe_status == 'P' || $itemScope.$parent.emp_monthlyAttendance_obj[i].pe_status == 'F') && (d <= $scope.payCycleDay)) {
                                payCycleFlag = true;
                                $scope.empID = $scope.empID + $itemScope.$parent.emp_monthlyAttendance_obj[i].att_emp_id + ',';
                            }
                            else {
                                Mark_all_Stud_atte_DayWise['opr'] = 'O';
                                Mark_all_Stud_atte_DayWise['att_emp_id'] = $itemScope.$parent.emp_monthlyAttendance_obj[i].att_emp_id;
                                Mark_all_Stud_atte_DayWise['att_date'] = $itemScope.$parent.emp_monthlyAttendance_obj[i].yearno + '-' + month + '-' + day;
                                Mark_all_Stud_atte_DayWise['att_flag'] = $scope.pays_attendance_code;
                                Mark_all_Stud_atte_DayWise1.push(Mark_all_Stud_atte_DayWise);
                            }                            
                        }

                        
                        swal({
                            title: "Are you sure?", text: "You will not be able to recover this Data", showCancelButton: true,
                            confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, Mark it", closeOnConfirm: false, width: 380,
                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $scope.loading = true;
                                $http.post(ENV.apiUrl + "api/emp/attendance/MarkAllPreAbEmpAttendance", Mark_all_Stud_atte_DayWise1).then(function (res) {
                                    console.log("res", res);
                                    $scope.pays_attendance_code = null;
                                    $scope.pays_attendance_color = null;
                                    //if (res.data == true) {
                                        swal({ title: "Success", text: "Attendance Marked Successfully", showCloseButton: true, width: 380 });
                                        $scope.getStudentMonthlyAttendance($scope.temp.company_code, $scope.temp.dept_code, $scope.temp.gr_code, $scope.temp.staff_type_code, $scope.temp.dg_code, $scope.temp.sims_attendance_year, $scope.temp.sims_attendance_month, $scope.temp.att_emp_id, true);
                                    //}
                                });
                            }
                            
                            $scope.loading = false;
                        });

                        //if (payCycleFlag == true) {
                        //    swal({ title: 'Alert', text: 'You can not mark attendance up to ' + $scope.payCycleDay + ' days.Payroll is finalized for ' + $scope.empID });
                        //    return;
                        //}
                    }],
                    null,
          ];
          //row wise 
          $scope.menuOptions = [
                  ['Mark Present For Whole Month', function ($itemScope) {
                      $('.dropdown-menu').attr('style', 'display: none');
                      $('.dropdown').remove();
                      
                      $("#" + $scope.pays_attendance_code).attr('checked', false);
                      
                      $scope.pays_attendance_code = 'P';
                      for (var i = 0; i < $itemScope.$parent.emp_monthlyAttendance_obj.length; i++) {
                          if ($itemScope.emp == $itemScope.$parent.emp_monthlyAttendance_obj[i]) {
                              $scope.emp_id = $itemScope.emp.att_emp_id;
                              $scope.pe_status = $itemScope.emp.pe_status;                              
                          }
                      }
                      $scope.markStudentAttendanceForMonth();                                          
                  }],
                  null,
                  ['Mark Absent For Whole Month', function ($itemScope) {
                      $('.dropdown-menu').attr('style', 'display: none');
                      $('.dropdown').remove();
                      $scope.pays_attendance_code = 'A';
                      for (var i = 0; i < $itemScope.$parent.emp_monthlyAttendance_obj.length; i++) {
                          if ($itemScope.emp == $itemScope.$parent.emp_monthlyAttendance_obj[i]) {
                              $scope.emp_id = $itemScope.emp.att_emp_id;
                              $scope.pe_status = $itemScope.emp.pe_status;
                              console.log("pe_status", $scope.pe_status);
                          }
                      }
                      $scope.markStudentAttendanceForMonth();
                  }],
                  null,
          ];

          $scope.markStudentAttendanceForMonth = function () {
             debugger
              if ($scope.temp.sims_academic_month == "09" || $scope.temp.sims_academic_month == "10"
                  || $scope.temp.sims_academic_month == "11" || $scope.temp.sims_academic_month == "12") {
                  //$scope.att_date1 = parseInt($scope.temp.sims_academic_year) - 1;
                  $scope.att_date1 = $scope.temp.sims_academic_year ;
              }
              else {
                  $scope.att_date1 = $scope.temp.sims_academic_year;
              }

              if ($scope.temp.sims_academic_month == '01' || $scope.temp.sims_academic_month == '03' || $scope.temp.sims_academic_month == '05' || $scope.temp.sims_academic_month == '07' || $scope.temp.sims_academic_month == '08'
                  || $scope.temp.sims_academic_month == '10' || $scope.temp.sims_academic_month == '12') {
                  $scope.day = 31;
              }
              else if (($scope.temp.sims_academic_month == '02') && ($scope.att_date1 % 4 == 0) && (($scope.att_date1 % 100 != 0) || ($scope.att_date1 % 400 == 0))) {
                  $scope.day = 29;
              }
              else if ($scope.temp.sims_academic_month == '02') { $scope.day = 28; }
              else { $scope.day = 30; }



              var emp_mark_monthly = [];
              for (var i = 1; i <= $scope.day; i++) {
                  if (i <= 9) {
                      var j = '0' + i;
                  }
                  else {
                      var j = i;
                  }

                  var stud_mark_monthly = {};
                  stud_mark_monthly['opr'] = 'O';
                  stud_mark_monthly['att_flag'] = $scope.pays_attendance_code;
                  stud_mark_monthly['att_emp_id'] = $scope.emp_id;
                  stud_mark_monthly['att_date'] = $scope.att_date1 + '-' + $scope.temp.sims_academic_month + '-' + j;
                  emp_mark_monthly.push(stud_mark_monthly);
              }
              console.log("emp_mark_monthly",emp_mark_monthly);

              if ($scope.pe_status == 'P' || $scope.pe_status == 'F') {
                  swal({ text: 'You can not mark attendance up to ' + $scope.payCycleDay + ' days.Payroll is finalized for ' + $scope.emp_id, imageUrl: "assets/img/notification-alert.png" });
                  return;
              }
              debugger
              swal({
                  title: "Are you sure?", text: "You will not be able to recover this Data", showCancelButton: true,
                  confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, Mark it", closeOnConfirm: false, width: 380
              }).then(function (isConfirm) {
                  if (isConfirm) {
                      $scope.loading = true;
                      $http.post(ENV.apiUrl + "api/emp/attendance/MarkAllPreAbEmpAttendance", emp_mark_monthly).then(function (emp_mark_AllData) {
                          console.log("emp_mark_AllData",emp_mark_AllData);
                          if (emp_mark_AllData.data == true) {
                              swal({ text: "Attendance Marked Successfully", imageUrl: "assets/img/check.png",showCloseButton: true, width: 380 });
                          }                         
                          $scope.getStudentMonthlyAttendance($scope.temp.company_code, $scope.temp.dept_code, $scope.temp.gr_code, $scope.temp.staff_type_code, $scope.temp.dg_code, $scope.temp.sims_attendance_year, $scope.temp.sims_attendance_month, $scope.temp.att_emp_id, true);
                      });

                  }

                  $scope.pays_attendance_code = null;
                  $scope.pays_attendance_color = null;
                  $scope.loading = false;
              });


          }



          $scope.otherMenuOptions = [
                ['Favorite Color', function ($itemScope, $event, color) {
                    alert(color);
                }]
          ]
          $scope.clear = function () {
              $scope.emp_monthlyAttendance_obj = [];
              $scope.stud_table = false;
              $scope.Atten_Codes = false;
              //$scope.prevDisabled = true;
              $scope.subDisabled = true;
              $scope.temp.employee_no = "";
              //$scope.temp.company_code = "";
              $scope.temp.dept_code = "";
              $scope.temp.gr_code = "";
              $scope.temp.staff_type_code = "";
              $scope.temp.dg_code = "";
              $scope.temp.gd_division_code = "";
              //$scope.temp.sims_academic_year = "";
              $scope.temp.sims_academic_month = "";
              $scope.temp.sims_attendance_month = '';
              $scope.temp.sort_code = "";
              $scope.temp.att_emp_id = "";             
          }

          //single attendance mark entry

          $scope.list_student_data = []; $scope.pays_attendance_code = null; $scope.pays_attendance_color = null;

          $scope.Mark_Attendance_1 = function (emp, id, str, column_id, element) {
              if ($scope.pays_attendance_code != null) {
                  emp[str] = $scope.pays_attendance_color;
                  emp[id] = $scope.pays_attendance_code;
              }
          }

          $scope.Mark_Attendance = function (emp, id, str, column_id) {

             debugger
             var d = emp[id];

             if ((emp.pe_status == 'P' || emp.pe_status == 'F') && (column_id <= $scope.payCycleDay)) {
                  swal({ text: 'You can not mark attendance.Payroll is finalized for ' + emp.att_emp_id, imageUrl: "assets/img/notification-alert.png" });
                  return;
              }

             //if ($http.defaults.headers.common['schoolId'] != 'iis' || $http.defaults.headers.common['schoolId'] != 'oes') {
             //     if ($scope.pays_attendance_code == "W" || $scope.pays_attendance_code == "H") {
             //         swal({ title:'Alert',text: 'You can not mark attendance', imageUrl: "assets/img/notification-alert.png" });
             //         return;
             //     }
             //}


              if ($scope.pays_attendance_teacher_can_use_flag == "Y") {
                  swal({ title: 'Alert', text: 'You can not mark attendance', imageUrl: "assets/img/notification-alert.png" });
                  return;
              }
              
              for (var obj in $scope.attendance_codes_obj) {
                  console.log("obj", obj);
                  console.log("ob", $scope.attendance_codes_obj[obj]);
                  if ($scope.attendance_codes_obj[obj].pays_attendance_code == d && $scope.attendance_codes_obj[obj].pays_attendance_teacher_can_use_flag == "Y") {
                      swal({ title: 'Alert', text: 'Leave is approved.You can not mark attendance', imageUrl: "assets/img/notification-alert.png" });
                      return;
                  }
              }
              //if ($scope.leaveCode_data.length > 0) {
              //    angular.forEach($scope.leaveCode_data, function (value, key) {
              //        if (value.le_leave_code == d) {
              //            swal({ title: 'Alert', text: 'You can not mark attendance', imageUrl: "assets/img/notification-alert.png" });
              //            return;
              //        }
              //    });
              //}

              //if ($scope.pays_attendance_code != "P" && $scope.pays_attendance_code != "A" && $scope.pays_attendance_code != "UM") {
              //    swal({ title:'Alert',text: 'You can mark attendance only for present,absent or Unmark', imageUrl: "assets/img/notification-alert.png" });
              //    return;
              //}


                  //else {              
                  //if (d == "W") {
                  //    swal({ text: 'You can not mark attendance for weekend', imageUrl: "assets/img/notification-alert.png" })
                  //    return;
                  //        //$scope.pays_attendance_code = "W";
                  //        //$scope.pays_attendance_color = rgb(51, 136, 85);
                  //    //if ($scope.pays_attendance_code != "A") {
                  //    //    swal({ text: 'You can assign only absent for weekend' })
                  //    //    var k = emp.att_emp_id;
                  //    //    $scope.pays_attendance_code = "W";
                  //    //    $scope.pays_attendance_color = rgb(51, 136, 85);
                  //    //}
                  //}


              if ($http.defaults.headers.common['schoolId'] == 'iis' || $http.defaults.headers.common['schoolId'] == 'oes') {
                      if ($scope.pays_attendance_code != null) {
                          emp[str] = $scope.pays_attendance_color;
                          emp[id] = $scope.pays_attendance_code;
                      }
                      else {
                          swal({  text: "Please Select Attendance Code", imageUrl: "assets/img/notification-alert.png",showCloseButton: true, width: 360, });
                      }
                      var d = '';
                      var data = {};
                      data['opr'] = 'I';
                      data['pays_attendance_code'] = $scope.pays_attendance_code;
                      data['att_emp_id'] = emp.att_emp_id;
                      data['pays_employee_name'] = emp.pays_employee_name;
                      data['em_company_code'] = emp.em_company_code;
                      data['em_dept_code'] = emp.em_dept_code;
                      data['em_desg_code'] = emp.em_desg_code;
                      data['em_grade_code'] = emp.em_grade_code;
                      data['em_staff_type'] = emp.em_staff_type;
                      data['monthYear'] = emp.monthYear;
                      data['yearno'] = emp.yearno;
                      data['att_date'] = emp.yearno + '-' + emp.monthYear + '-' + column_id;
                      $scope.list_student_data.push(data);
                  }
                  else {
                    if ( d != 'H') {  //d != 'W' &&
                            if ($scope.pays_attendance_code != null) {
                                emp[str] = $scope.pays_attendance_color;
                                emp[id] = $scope.pays_attendance_code;
                            }
                            else {
                                swal({ text: "Please Select Attendance Code", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 360 });
                            }
                          var d = '';
                          var data = {};
                          data['opr'] = 'I';
                          data['pays_attendance_code'] = $scope.pays_attendance_code;
                          data['att_emp_id'] = emp.att_emp_id;
                          data['pays_employee_name'] = emp.pays_employee_name;
                          data['em_company_code'] = emp.em_company_code;
                          data['em_dept_code'] = emp.em_dept_code;
                          data['em_desg_code'] = emp.em_desg_code;
                          data['em_grade_code'] = emp.em_grade_code;
                          data['em_staff_type'] = emp.em_staff_type;
                          data['monthYear'] = emp.monthYear;
                          data['yearno'] = emp.yearno;
                          data['att_date'] = emp.yearno + '-' + emp.monthYear + '-' + column_id;
                          $scope.list_student_data.push(data);
                      }
                      else {
                          swal({ text: 'You can not mark attendance', width: 360 });
                      }
                  }               
          }

          $scope.Mark_All_Attendance = function () {
            
              $scope.subDisabled = false;
              if ($scope.list_student_data.length != 0) {
                  $scope.loading = true;
                  $http.post(ENV.apiUrl + "api/emp/attendance/InsertEmpAttendance", $scope.list_student_data).then(function (res) {
                      $scope.mark_result = res.data;
                      $scope.loading = false;
                      if ($scope.mark_result == true) {
                          swal({  text: "Employee Attendance Added Successfully",imageUrl: "assets/img/check.png", showCloseButton: true, width: 380 });
                          $scope.getStudentMonthlyAttendance($scope.temp.company_code, $scope.temp.dept_code, $scope.temp.gr_code, $scope.temp.staff_type_code, $scope.temp.dg_code, $scope.temp.sims_attendance_year, $scope.temp.sims_attendance_month, $scope.temp.att_emp_id, true);
                          $scope.stud_table = true;                                                    
                      }

                  }, function () {
                      //swal({ title: "Alert", text: "Internal Server Error", imageUrl: "assets/img/notification-alert.png", });
                  });
              }
              else {
                  swal({  text: "Please Assign Attendance to Employee",imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
              }
          }

          $('body').addClass('grey condense-menu');
          $('#main-menu').addClass('mini');
          $('.page-content').addClass('condensed');
          $rootScope.isCondensed = true;
      }])

})();
