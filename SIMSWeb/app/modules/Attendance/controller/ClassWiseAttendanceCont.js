﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Attendance');
    var main;
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });



    simsController.controller('ClassWiseAttendanceCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.attend_date = null; $scope.display = false;


            //$http.get(ENV.apiUrl + "api/classwiseattendance/getAttendanceCuriculum").then(function (res) {
            //    $scope.cur_obj = res.data;
            //    $scope.sims_cur_code = $scope.cur_obj[0].sims_cur_code;
            //    $scope.display = true;
            //});

            function getCur(flag, comp_code) {
                if (flag) {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                        $scope.cur_obj = res.data;
                        $scope.sims_cur_code = $scope.cur_obj[0].sims_cur_code;
                        $scope.display = true;

                    });
                }
                else {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                        $scope.cur_obj = res.data;
                        $scope.sims_cur_code = $scope.cur_obj[0].sims_cur_code;
                        $scope.display = true;
                    });


                }

            }

            $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.global_count_comp = res.data;
                if ($scope.global_count_comp) {
                    getCur(true, $scope.user_details.comp);
                }
                else {
                    getCur(false, $scope.user_details.comp)
                }
            });

            $scope.getSectionNames = function (isvalid) {
                if (isvalid) {

                    if ($http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dmc') {
                        $http.get(ENV.apiUrl + "api/classwiseattendance/getSectionNamesCurCodeWise?cur_code=" + $scope.sims_cur_code + "&user=" + $rootScope.globals.currentUser.username).then(function (res) {
                            $scope.section_names_obj = res.data;
                        });
                    }
                    else {
                        $http.get(ENV.apiUrl + "api/classwiseattendance/getSectionNamesCurCodeWise?cur_code=" + $scope.sims_cur_code).then(function (res) {
                            $scope.section_names_obj = res.data;
                        });
                    }
                }
            }
        

            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.attend_date = dd + '-' + mm + '-' + yyyy;
           


            var chckbx_checked_list = [];

            $scope.chck_section_click = function (sec_code, grade_code, index) {
             
                var v = document.getElementById(sec_code + 1);//textbox
                var s = 'sec-' + index;
                var chck_bx = document.getElementById(s);//checkbox
                if (chck_bx.checked == true) {
                    v.disabled = false;
                    chckbx_checked_list.push({
                        'sims_section_code': sec_code,
                        'sims_grade_code': grade_code,
                        'index': s
                    });
                }
                else {
                    v.disabled = true;
                    // debugger
                    var index = chckbx_checked_list.map(function (e) { return e.sims_section_code; }).indexOf(sec_code);
                    // var index = chckbx_checked_list.indexOf(sec_code);
                    if (index > -1) {
                        chckbx_checked_list.splice(index, 1);
                        v.value = "";
                    }
                }
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.section_names_obj.length; i++) {
                        var v = document.getElementById("sec-" + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");

                        $scope.chck_section_click($scope.section_names_obj[i].sims_section_code, $scope.section_names_obj[i].sims_grade_code, i)

                    }
                }
                else {
                    for (var i = 0; i < $scope.section_names_obj.length; i++) {
                        var v = document.getElementById("sec-" + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $scope.chck_section_click($scope.section_names_obj[i].sims_section_code, $scope.section_names_obj[i].sims_grade_code, i)

                    }
                }

            }


            $scope.btn_present_click = function () {
               
                $scope.loading = true;
                for (var i = 0; i < chckbx_checked_list.length; i++) {
                    var t = document.getElementById(chckbx_checked_list[i].sims_section_code + 1);
                    var f = t.value;
                    chckbx_checked_list[i]['Enrollment_list'] = t.value;
                    chckbx_checked_list[i]['attendance_code'] = 'P';
                    chckbx_checked_list[i]['attedance_date'] = $scope.attend_date;
                    chckbx_checked_list[i]['sims_cur_code'] = $scope.sims_cur_code;
                }

               

                $http.post(ENV.apiUrl + "api/classwiseattendance/classWiseStudentAttendanceUpdate", JSON.stringify(chckbx_checked_list)).then(function (res) {
                    $scope.result = res.data;
                    $scope.loading = false;
                    $scope.clear();
                   
                    if ($scope.result == true) {
                        // swal("Marked", "Student Attendance Marked Successfully", "success")
                        swal({ text: "Student Attendance Marked Successfully", imageUrl: "assets/img/check.png",width: 300, height: 200 });
                    }
                });
            }

            $scope.btn_absent_click = function () {
               
                $scope.loading = true;
                for (var i = 0; i < chckbx_checked_list.length; i++) {
                    var t = document.getElementById(chckbx_checked_list[i].sims_section_code + 1);
                    var f = t.value;
                    chckbx_checked_list[i]['Enrollment_list'] = t.value;
                    chckbx_checked_list[i]['attendance_code'] = 'A';
                    chckbx_checked_list[i]['attedance_date'] = $scope.attend_date;
                    chckbx_checked_list[i]['sims_cur_code'] = $scope.sims_cur_code;
                }

                

                $http.post(ENV.apiUrl + "api/classwiseattendance/classWiseStudentAttendanceUpdate", JSON.stringify(chckbx_checked_list)).then(function (res) {
                    $scope.result = res.data;
                    $scope.loading = false;

                   
                    if ($scope.result == true) {
                        //swal("Marked", "Student Attendance Marked Successfully", "success")
                        swal({ text: "Student Attendance Marked Successfully", imageUrl: "assets/img/check.png",width: 300, height: 200 });
                    }
                    $scope.clear();
                });
            }

            $scope.clear = function () {
               
                for (var i = 0; i < chckbx_checked_list.length; i++) {
                    var t = document.getElementById(chckbx_checked_list[i].sims_section_code + 1);
                    t.value = "";
                    t.disabled = true;
                    var s = 'sec-' + i;
                    var c = document.getElementById(chckbx_checked_list[i].index);
                    c.checked = false;
                }
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            //for collapse main menu
            $('body').addClass('grey condense-menu');
            $('#main-menu').addClass('mini');
            $('.page-content').addClass('condensed');
            $rootScope.isCondensed = true;

        }]
        )
})();