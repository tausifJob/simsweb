﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Attendance');

    simsController.controller('SubjectwisetAttendanceNewCont', ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV',
        function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var username = $rootScope.globals.currentUser.username;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            //$scope.att_date = mm + '/' + dd + '/' + yyyy;
            $scope.att_date = dd + '-' + mm + '-' + yyyy;

            setTimeout(function () {
                $scope.img_url = $scope.obj1.lic_website_url + '/Images/StudentImage/'
               
            }, 3000);

            $scope.operation = false;
            $http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (curiculum) {
                $scope.curiculum = curiculum.data;

            });

            $http.get(ENV.apiUrl + "api/attendance/getAttendancetype").then(function (atttypes) {
                $scope.atttypes = atttypes.data;
            });

            $http.get(ENV.apiUrl + "api/attendance/getAttendancesort").then(function (sorttypes) {
                $scope.sorttypes = sorttypes.data;
            });
            $scope.type_change = function () {

                if ($scope.type_section == "1") {
                    cur_code
                    $http.get(ENV.apiUrl + "api/attendance/getAttendancesubtype").then(function (attsubtypes) {
                        $scope.attsubtypes = attsubtypes.data;
                    });
                }
                else {
                    $scope.attsubtypes = null;
                }

            }

            $scope.cur_change = function () {

                $http.get(ENV.apiUrl + "api/attendance/getAttendanceyear?cur_code=" + $scope.cur_code).then(function (academicyears) {
                    $scope.academicyears = academicyears.data;
                });

                $http.get(ENV.apiUrl + "api/attendance/getAttendanceCode?cur_code=" + $scope.cur_code).then(function (attendance_code) {
                    $scope.attendance_code = attendance_code.data;
                    //$scope.rdbtn = $scope.attendance_code[0].sims_attendance_code;
                });


            }

            $scope.acdm_yr_change = function () {
                $http.get(ENV.apiUrl + "api/SubjectwiseAttendance/GetAttednace_Grade?cur_code=" + $scope.cur_code + "&a_year=" + $scope.acdm_yr + "&userName=" + username).then(function (grades) {
                    $scope.grades = grades.data;
                });

            }
            $scope.grade_change = function () {

                $http.get(ENV.apiUrl + "api/SubjectwiseAttendance/GetAttednace_Section?cur_code=" + $scope.cur_code + "&a_year=" + $scope.acdm_yr + "&grade_code=" + $scope.grade_code + "&username=" + username).then(function (section) {
                    $scope.section = section.data;
                });

            }
            $scope.totalstudents = 0;
            $scope.totalmarkedstudents = 0;
            $scope.totalunmarkedstudents = 0;


            $scope.getAttendance = function (isvalid) {
                if (isvalid) {
                    if ($scope.att_date == undefined) {
                        alert("select date");
                    }
                    else {

                        $scope.busyindicator = false;
                        $scope.info1 = [];
                        var demo = [];
                        var demo1 = [];
                        $scope.table = false;
                        $scope.subject = false;

                        var date = $scope.att_date
                        //var month = date.split("/")[0];
                        //var day = date.split("/")[1];
                        //var year = date.split("/")[2];
                        //var date1 = year + "-" + month + "-" + day;
                        var date1 = date;

                        $http.get(ENV.apiUrl + "api/SubjectwiseAttendance/GetAttednaceNew?cur_code=" + $scope.cur_code + "&ayear=" + $scope.acdm_yr + "&grade=" + $scope.grade_code + "&section=" + $scope.sec_code + "&attednacedate=" + date1 + "&sims_teacher_code=" + username).then(function (allSectionStudent) {

                            $scope.SectionStudent = allSectionStudent.data;
                            $scope.operation = true;
                            $scope.busyindicator = true;

                            if (allSectionStudent.data.length > 0 == 0) {
                                swal('No Data Found for this Date', '')
                            }

                            if (allSectionStudent.data.length > 0) {
                                for (var j = 0; j < $scope.SectionStudent[0].sublist1.length; j++) {
                                    demo = {
                                        'sims_subject_name': $scope.SectionStudent[0].sublist1[j].sims_subject_name,
                                        'sims_subject_code': $scope.SectionStudent[0].sublist1[j].sims_subject_code
                                    }
                                    demo1.push(demo);
                                }

                               

                                $scope.info1 = demo1;

                                $scope.table = true;

                                $timeout(function () {
                                    $("#fixTable").tableHeadFixer({ "left": 2 });
                                }, 100);
                            }
                            else {

                                $scope.subject = true;

                            }
                        });








                        // $scope.mycolor = 'background-color:red';
                        //  $scope.operation = false;

                        //$http.get(ENV.apiUrl + "api/SubjectwiseAttendance/GetAttednace?cur_code=" + $scope.cur_code + "&ayear=" + $scope.acdm_yr + "&grade=" + $scope.grade_code + "&section=" + $scope.sec_code + "&attednacedate=" + date1 + "&sims_teacher_code=" + username).then(function (attendance) {
                        //    $scope.attendance = attendance.data;
                        //    $scope.operation = true;
                        //    $scope.totalstudents = $scope.attendance.length;
                        //    if ($scope.totalmarkedstudents == 0) {
                        //        swal('No Data Found for this Date','')
                        //    }


                       
                        //    updateCount();
                        //});
                    }
                }
            }


            $scope.Mark_Subject_Attendance = function (obj, str) {

                obj["sims_attedance_code"] = $scope.sims_attendance_code;
                obj["sims_att_color_code"] = $scope.sims_attendance_color;



                var data = {
                    sims_cur_code: str.sims_cur_code,
                    sims_academic_year: str.sims_academic_year,
                    sims_grade_code: str.sims_grade_code,
                    sims_section_code: str.sims_section_code,
                    sims_subject_code: obj.sims_subject_code,
                    sims_teacher_code: username,
                    sims_attedance_slot: obj.sims_attedance_slot,
                    sims_attedance_code: $scope.sims_attendance_code,
                    sims_enroll_number: str.sims_enroll_number,
                    sims_attedance_date: str.sims_attedance_date,
                }

                $http.post(ENV.apiUrl + "api/SubjectwiseAttendance/UpdateAttednaceNew", data).then(function (res) {
                    $scope.ins = res.data;
                    if (res.data) {
                        // swal({ title: "Alert", text: "Attendance Marked Successfully.", imageUrl: "assets/img/check.png", });


                    }
                    else
                        swal({ text: "Record Not Marked.", imageUrl: "assets/img/close.png", });


                });


            }


            $scope.get_attend_code = function (codes) {
                
                $scope.sims_attendance_color = codes.sims_attendance_color;
                $scope.sims_attendance_code = codes.sims_attendance_code;
            }








            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


            //for collapse main menu
            $('body').addClass('grey condense-menu');
            $('#main-menu').addClass('mini');
            $('.page-content').addClass('condensed');
            $rootScope.isCondensed = true;

        }])

})();