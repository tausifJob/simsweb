﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, modulecode = [];
    var simsController = angular.module('sims.module.Attendance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('AttendanceDashboardCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$compile', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $compile,ENV) {
          
     

            $scope.busyindicator = false;

            $http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (res) {
                $scope.cmbCur = res.data;
            });

            


            $http.get(ENV.apiUrl + "api/EmployeeReport/GetDepartmentName").then(function (res) {
                $scope.cmbDepartment = res.data;
            })


            $scope.CurChange = function (str) {
                $http.get(ENV.apiUrl + "api/attendance/getAttendanceyear?cur_code=" + str).then(function (res) {

               // $http.get(ENV.apiUrl + "api/common/getAcademicYearCommon?curCode="+str).then(function (res) {
                    $scope.cmbAcademic = res.data;
                })
            }

            $scope.academicChange = function (str) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.sims_cur_code + "&academic_year=" + str).then(function (res) {

                //$http.get(ENV.apiUrl + "api/common/getAllGradesCommon?cur_code=" + $scope.sims_cur_code + "&academic=" + str).then(function (res) {
                    $scope.cmbGrade = res.data;
                })
            }
            $scope.gradeChange = function (str) {

                $scope.grade_code = str;
                
            }


          

            

            $scope.Show_Data = function () {
                //$scope.table = true;
                //$scope.busyindicator = true;
                
                //$http.get(ENV.apiUrl + "api/common/getSectionCommon?cur_code=" + $scope.sims_cur_code + "&acadmic=" + $scope.sims_academic_year + "&grade_code=" + $scope.grade_code).then(function (res) {
                  
                $http.get(ENV.apiUrl + "api/SubjectwiseAttendance/Get_get_total_present?cur_code=" + $scope.sims_cur_code + "&academic=" + $scope.sims_academic_year + "&grade=" + $scope.grade_code + "&sec=" + '' + "&st=" + '2017-05-18').then(function (res) {
                
                $scope.cmbSection = res.data;
                    
                    

                    for (var i = 0 ; i < res.data.length; i++) {


                        var t = 'gauge0';
                        //if (i == 0) {
                        //    t = 'gauge0';
                        //}

                        //else if (i == 1) {
                        //    t = 'gauge1';
                        //}

                        //else if (i == 2) {
                        //    t = 'gauge2';
                        //}

                        //else if (i == 3) {
                        //    t = 'gauge3';
                        //}

                        //else if (i == 4) {
                        //    t = 'gauge4';
                        //}
                        //else if (i == 5) {
                        //    t = 'gauge5';
                        //}

                        //else if (i == 6) {
                        //    t = 'gauge6';
                        //}

                        $('#newTransaction').replaceWith($('#newTransaction'));

                        $('#newTransaction').append('<canvas id="gauge' + i + '"></canvas>');
                     
              
                        // change ng-click
                        $('#gauge' + i).attr('ng-click', 'clickbtn(' + res.data[i].sims_section_code + ')');

                        // compile the element
                        $compile($('#gauge'+i))($scope);



                        var bar=  [
                                 { from: 0, to: res.data[i].sims_std_nm, color: 'rgba(50,   205, 50, 3329330)' },
                                // { from: 30, to: 60, color: 'rgba(255, 255, 0, 65535)' },
                              //   { from: 60, to: 100, color: 'rgba(255, 69,  0, 17919)' },

                        ]


                        var gauge = new Gauge({
                            renderTo: "gauge" + i ,
                            width: 180,
                            height: 180,
                            glow: true,
                            units: res.data[i].sims_section_nm,
                            title: false,
                            minValue: 0,
                            maxValue: res.data[i].sims_std_nm,
                            // majorTicks: ['0', '10', '20', '30', '40', '50', '60', '70', '80', '90', '100'],
                            minorTicks: 2,
                            strokeTicks: false,
                            highlights:bar,
                            colors: {
                                plate: '#fff',
                                majorTicks: '#234567',
                                minorTicks: '#340099',
                                title: '#fff',
                                units: '#234567',
                                numbers: '#234567',
                                needle: { start: 'rgba(240, 128, 128, 1)', end: 'rgba(255, 160, 122, .9)' }
                            }//#1a8abf
                        });

                        gauge.setValue(res.data[i].present);
                        gauge.draw();


                    }




                })


                $http.get(ENV.apiUrl + "api/SubjectwiseAttendance/Get_get_all_status?cur_code=" + $scope.sims_cur_code + "&academic=" + $scope.sims_academic_year + "&grade=" + $scope.grade_code + "&sec=" + '' + "&st=" + '2015-04-01').then(function (res) {
                    $scope.lbl = res.data;

                    $scope.allStatus = [];

                    for (var i = 0; i < res.data.length; i++) {
  
                        if (res.data[i].label == 'Total') {

                        }
                        else {
                            $scope.allStatus.push(res.data[i])
                        }
                      
                    }

                 
                    var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
                    var pieChart = new Chart(pieChartCanvas);
                    var PieData = [
                      {
                          value: 700,
                          color: "#f56954",
                          highlight: "#f56954",
                          label: "Present"
                      },
                      {
                          value: 70,
                          color: "#00a65a",
                          highlight: "#00a65a",
                          label: "Absent"
                      },
                      {
                          value: 400,
                          color: "#f39c12",
                          highlight: "#f39c12",
                          label: "Unmarked"
                      },
                      {
                          value: 600,
                          color: "#00c0ef",
                          highlight: "#00c0ef",
                          label: "Absent Excused"
                      },
                      {
                          value: 300,
                          color: "#3c8dbc",
                          highlight: "#3c8dbc",
                          label: "Tardy"
                      },
                      {
                          value: 100,
                          color: "#d2d6de",
                          highlight: "#d2d6de",
                          label: "Tardy Absent"
                      }
                    ];
                    var pieOptions = {
                        //Boolean - Whether we should show a stroke on each segment
                        segmentShowStroke: true,
                        //String - The colour of each segment stroke
                        segmentStrokeColor: "#fff",
                        //Number - The width of each segment stroke
                        segmentStrokeWidth: 1,
                        //Number - The percentage of the chart that we cut out of the middle
                        percentageInnerCutout: 50, // This is 0 for Pie charts
                        //Number - Amount of animation steps
                        animationSteps: 100,
                        //String - Animation easing effect
                        animationEasing: "easeOutBounce",
                        //Boolean - Whether we animate the rotation of the Doughnut
                        animateRotate: true,
                        //Boolean - Whether we animate scaling the Doughnut from the centre
                        animateScale: false,
                        //Boolean - whether to make the chart responsive to window resizing
                        responsive: true,
                        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                        maintainAspectRatio: false,
                        //String - A legend template
                        //legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
                        //String - A tooltip template
                        tooltipTemplate: "<%=value %> <%=label%> "
                    };
                    //Create pie or douhnut chart
                    // You can switch between pie and douhnut using the method below.
                    var tcx = pieChart.Doughnut($scope.allStatus, pieOptions);

                    document.getElementById('s').innerHTML = tcx.generateLegend();

                });

                

               
            }


            $scope.clickbtn = function (str) {
             

                $http.get(ENV.apiUrl + "api/SubjectwiseAttendance/Get_get_all_status?cur_code=" + $scope.sims_cur_code + "&academic=" + $scope.sims_academic_year + "&grade=" + $scope.grade_code + "&sec=" + str + "&st=" + '2015-04-01').then(function (res) {
                    $scope.lbl = [];

                    $scope.lbl = res.data;

                    $scope.allStatus = [];

                    for (var i = 0; i < res.data.length; i++) {

                        if (res.data[i].label == 'Total') {

                        }
                        else {
                            $scope.allStatus.push(res.data[i])
                        }

                    }


                    var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
                    var pieChart = new Chart(pieChartCanvas);
                    var PieData = [
                      {
                          value: 700,
                          color: "#f56954",
                          highlight: "#f56954",
                          label: "Present"
                      },
                      {
                          value: 70,
                          color: "#00a65a",
                          highlight: "#00a65a",
                          label: "Absent"
                      },
                      {
                          value: 400,
                          color: "#f39c12",
                          highlight: "#f39c12",
                          label: "Unmarked"
                      },
                      {
                          value: 600,
                          color: "#00c0ef",
                          highlight: "#00c0ef",
                          label: "Absent Excused"
                      },
                      {
                          value: 300,
                          color: "#3c8dbc",
                          highlight: "#3c8dbc",
                          label: "Tardy"
                      },
                      {
                          value: 100,
                          color: "#d2d6de",
                          highlight: "#d2d6de",
                          label: "Tardy Absent"
                      }
                    ];
                    var pieOptions = {
                        //Boolean - Whether we should show a stroke on each segment
                        segmentShowStroke: true,
                        //String - The colour of each segment stroke
                        segmentStrokeColor: "#fff",
                        //Number - The width of each segment stroke
                        segmentStrokeWidth: 1,
                        //Number - The percentage of the chart that we cut out of the middle
                        percentageInnerCutout: 50, // This is 0 for Pie charts
                        //Number - Amount of animation steps
                        animationSteps: 100,
                        //String - Animation easing effect
                        animationEasing: "easeOutBounce",
                        //Boolean - Whether we animate the rotation of the Doughnut
                        animateRotate: true,
                        //Boolean - Whether we animate scaling the Doughnut from the centre
                        animateScale: false,
                        //Boolean - whether to make the chart responsive to window resizing
                        responsive: true,
                        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
                        maintainAspectRatio: false,
                        //String - A legend template
                        //legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>",
                        //String - A tooltip template
                        tooltipTemplate: "<%=value %> <%=label%> "

                    };
                    //Create pie or douhnut chart
                    // You can switch between pie and douhnut using the method below.
                    var tcx = pieChart.Doughnut($scope.allStatus, pieOptions);

                    document.getElementById('s').innerHTML = tcx.generateLegend();

                    $("#pieChart").click(
                        
                      function (evt) {
                          
                          var activePoints = tcx.getSegmentsAtEvent(evt);
                          var activeLabel = activePoints[0].label;
                          var segments = tcx.segments;
                          for (var index = 0; index < segments.length; index++) {
                              if (activeLabel == segments[index].label) {
                                  tcx.removeData(index);
                              }
                          }
                      }
                  );

                    pieChartCanvas.destroy();
                });


            }



            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

          


        

        }])

    

})();
