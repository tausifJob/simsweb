﻿(function () {
    'use strict';

    var simsController = angular.module('sims.module.Attendance');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.directive('contextMenu', ["$parse", "$q", function ($parse, $q) {

        var contextMenus = [];

        var removeContextMenus = function (level) {
            while (contextMenus.length && (!level || contextMenus.length > level)) {
                contextMenus.pop().remove();
            }
            if (contextMenus.length == 0 && $currentContextMenu) {
                $currentContextMenu.remove();
            }
        };

        var $currentContextMenu = null;

        var renderContextMenu = function ($scope, event, options, model, level) {
            if (!level) { level = 0; }
            if (!$) { var $ = angular.element; }
            $(event.currentTarget).addClass('context');
            var $contextMenu = $('<div>');
            if ($currentContextMenu) {
                $contextMenu = $currentContextMenu;
            } else {
                $currentContextMenu = $contextMenu;
            }
            $contextMenu.addClass('dropdown clearfix');
            var $ul = $('<ul>');
            $ul.addClass('dropdown-menu');
            $ul.attr({ 'role': 'menu' });
            $ul.css({
                display: 'block',
                position: 'absolute',
                left: event.pageX + 'px',
                top: event.pageY + 'px',
                "z-index": 10000
            });
            angular.forEach(options, function (item, i) {
                var $li = $('<li>');
                if (item === null) {
                    $li.addClass('divider');
                } else {
                    var nestedMenu = angular.isArray(item[1])
                      ? item[1] : angular.isArray(item[2])
                      ? item[2] : angular.isArray(item[3])
                      ? item[3] : null;
                    var $a = $('<a>');
                    $a.css("padding-right", "8px");
                    $a.attr({ tabindex: '-1' });
                    var text = typeof item[0] == 'string' ? item[0] : item[0].call($scope, $scope, event, model);
                    $q.when(text).then(function (text) {
                        $a.text(text);
                        if (nestedMenu) {
                            $a.css("cursor", "default");
                            $a.append($('<strong style="font-family:monospace;font-weight:bold;float:right;">&gt;</strong>'));
                        }
                    });
                    $li.append($a);

                    var enabled = angular.isFunction(item[2]) ? item[2].call($scope, $scope, event, model, text) : true;
                    if (enabled) {
                        var openNestedMenu = function ($event) {
                            removeContextMenus(level + 1);
                            var ev = {
                                pageX: event.pageX + $ul[0].offsetWidth - 1,
                                pageY: $ul[0].offsetTop + $li[0].offsetTop - 3
                            };
                            renderContextMenu($scope, ev, nestedMenu, model, level + 1);
                        }
                        $li.on('click', function ($event) {
                            //$event.preventDefault();
                            $scope.$apply(function () {
                                if (nestedMenu) {
                                    openNestedMenu($event);
                                } else {
                                    $(event.currentTarget).removeClass('context');
                                    removeContextMenus();
                                    item[1].call($scope, $scope, event, model);
                                }
                            });
                        });

                        $li.on('mouseover', function ($event) {
                            $scope.$apply(function () {
                                if (nestedMenu) {
                                    openNestedMenu($event);
                                }
                            });
                        });
                    } else {
                        $li.on('click', function ($event) {
                            $event.preventDefault();
                        });
                        $li.addClass('disabled');
                    }
                }
                $ul.append($li);
            });
            $contextMenu.append($ul);
            var height = Math.max(
                document.body.scrollHeight, document.documentElement.scrollHeight,
                document.body.offsetHeight, document.documentElement.offsetHeight,
                document.body.clientHeight, document.documentElement.clientHeight
            );
            $contextMenu.css({
                width: '100%',
                height: height + 'px',
                position: 'absolute',
                top: 0,
                left: 0,
                zIndex: 9999
            });
            $(document).find('body').append($contextMenu);
            $contextMenu.on("mousedown", function (e) {
                if ($(e.target).hasClass('dropdown')) {
                    $(event.currentTarget).removeClass('context');
                    removeContextMenus();
                }
            }).on('contextmenu', function (event) {
                $(event.currentTarget).removeClass('context');
                event.preventDefault();
                removeContextMenus(level);
            });
            $scope.$on("$destroy", function () {
                removeContextMenus();
            });

            contextMenus.push($ul);
        };
        return function ($scope, element, attrs) {
            element.on('contextmenu', function (event) {
                event.stopPropagation();
                $scope.$apply(function () {
                    event.preventDefault();
                    var options = $scope.$eval(attrs.contextMenu);
                    var model = $scope.$eval(attrs.model);
                    if (options instanceof Array) {
                        if (options.length === 0) { return; }
                        renderContextMenu($scope, event, options, model);
                    } else {
                        throw '"' + attrs.contextMenu + '" not an array';
                    }
                });
            });
        };
    }]);


    simsController.controller('StudentAttendanceMonthlyN',
      ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {




          $http.get(ENV.apiUrl + "api/attendance/getAttendancesort").then(function (sorttypes) {
              $scope.sorttypes = sorttypes.data;
          });

          $scope.propertyName = null;
          $scope.reverse = false;

          $scope.sortBy = function (propertyName) {
              $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
              $scope.propertyName = propertyName;
          };

          $scope.markStudentAttendanceForMonth = function () {
              swal({
                  title: "Are you sure?", text: "You will not be able to recover this Data!", showCancelButton: true,
                  confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, Mark it!", closeOnConfirm: false
              }).then(function (isConfirm) {
                  if (isConfirm) {

                      var stud_mark_monthly = {};
                      stud_mark_monthly['sims_cur_code'] = $scope.single_stud_mark_atten.sims_cur_code;
                      stud_mark_monthly['sims_attendance_code'] = $scope.sims_attendance_code_m;
                      stud_mark_monthly['monthYear'] = $scope.single_stud_mark_atten.monthYear;
                      stud_mark_monthly['yearno'] = $scope.single_stud_mark_atten.yearno;
                      stud_mark_monthly['sims_academic_year'] = $scope.single_stud_mark_atten.sims_academic_year;
                      stud_mark_monthly['sims_enroll_number'] = $scope.single_stud_mark_atten.sims_enroll_number;
                      stud_mark_monthly['sims_grade_code'] = $scope.single_stud_mark_atten.sims_grade_code;
                      stud_mark_monthly['sims_section_code'] = $scope.single_stud_mark_atten.sims_section_code;
                      stud_mark_monthly['Current_Username'] = Current_Username;
                      $http.post(ENV.apiUrl + "api/attendance/markStudentAttendanceForMonth", stud_mark_monthly).then(function (result) {
                          if (result.data == true) {
                              swal("Marked!", "Attendance Marked Successfully", "success");
                              $scope.getStudentMonthlyAttendance();
                          }
                      });
                  }
              });
          }


          $scope.menuOptionsDayWise = [
                ['Mark All Present', function ($itemScope) {
                    $('.dropdown-menu').attr('style', 'display: none');

                
                    $scope.sims_attendance_code_m = 'P';
                    var d = $itemScope.i.day;
                    var code = $itemScope.$parent.stud_monthlyAttendance_obj[0]['attendance_day' + d + '_code']
                    if (code != 'W' && code != 'H') {

                        swal({
                            title: "Are you sure?", text: "You will not be able to recover this Data!", showCancelButton: true,
                            confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, Mark it!", closeOnConfirm: false
                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                var Mark_all_Stud_atte_DayWise = {};
                                Mark_all_Stud_atte_DayWise['sims_cur_code'] = $itemScope.$parent.stud_monthlyAttendance_obj[0].sims_cur_code;
                                Mark_all_Stud_atte_DayWise['sims_attendance_code'] = $scope.sims_attendance_code_m;
                                Mark_all_Stud_atte_DayWise['sims_academic_year'] = $itemScope.$parent.stud_monthlyAttendance_obj[0].sims_academic_year;
                                Mark_all_Stud_atte_DayWise['attednacedate'] = $itemScope.$parent.stud_monthlyAttendance_obj[0].yearno + '-' + $itemScope.$parent.stud_monthlyAttendance_obj[0].monthYear + ' -' + d;
                                Mark_all_Stud_atte_DayWise['sims_grade_code'] = $itemScope.$parent.stud_monthlyAttendance_obj[0].sims_grade_code;
                                Mark_all_Stud_atte_DayWise['sims_section_code'] = $itemScope.$parent.stud_monthlyAttendance_obj[0].sims_section_code;
                                Mark_all_Stud_atte_DayWise['Current_Username'] = Current_Username;
                                $http.post(ENV.apiUrl + "api/attendance/markAllStudentAttendanceDayWise", Mark_all_Stud_atte_DayWise).then(function (result) {
                                    if (result.data == true) {
                                        swal("Marked!", "Attendance Marked Successfully", "success");
                                        $scope.getStudentMonthlyAttendance();
                                    }
                                });
                            }

                        });
                    }
                    else {
                        swal({ text: "Cant Mark attendance For Weekend.", imageUrl: "assets/img/close.png" });

                    }

                }],
                null,
                ['Mark All Absent', function ($itemScope) {
                    $('.dropdown-menu').attr('style', 'display: none');

                    $scope.sims_attendance_code_m = 'A';
                    var d = $itemScope.i.day;
                    var code = $itemScope.$parent.stud_monthlyAttendance_obj[0]['attendance_day' + d + '_code']
                    if (code != 'W' && code != 'H') {

                        swal({
                            title: "Are you sure?", text: "You will not be able to recover this Data!", showCancelButton: true,
                            confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, Mark it!", closeOnConfirm: false
                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                var Mark_all_Stud_atte_DayWise = {};
                                Mark_all_Stud_atte_DayWise['sims_cur_code'] = $itemScope.$parent.stud_monthlyAttendance_obj[0].sims_cur_code;
                                Mark_all_Stud_atte_DayWise['sims_attendance_code'] = $scope.sims_attendance_code_m;
                                Mark_all_Stud_atte_DayWise['sims_academic_year'] = $itemScope.$parent.stud_monthlyAttendance_obj[0].sims_academic_year;
                                Mark_all_Stud_atte_DayWise['attednacedate'] = $itemScope.$parent.stud_monthlyAttendance_obj[0].yearno + '-' + $itemScope.$parent.stud_monthlyAttendance_obj[0].monthYear + ' -' + d;
                                Mark_all_Stud_atte_DayWise['sims_grade_code'] = $itemScope.$parent.stud_monthlyAttendance_obj[0].sims_grade_code;
                                Mark_all_Stud_atte_DayWise['sims_section_code'] = $itemScope.$parent.stud_monthlyAttendance_obj[0].sims_section_code;
                                Mark_all_Stud_atte_DayWise['Current_Username'] = Current_Username;
                                $http.post(ENV.apiUrl + "api/attendance/markAllStudentAttendanceDayWise", Mark_all_Stud_atte_DayWise).then(function (result) {
                                    if (result.data == true) {
                                        swal("Marked!", "Attendance Marked Successfully", "success");
                                        $scope.getStudentMonthlyAttendance();
                                    }
                                });
                            }
                        });
                    }
                    else {
                        swal({ text: "Cant Mark attendance For Weekend.", imageUrl: "assets/img/close.png" });

                    }
                }],
                null,

          ];

          $scope.menuOptions = [
              ['Mark All Present', function ($itemScope) {
                  $('.dropdown-menu').attr('style', 'display: none');

                  $scope.sims_attendance_code_m = 'P';
                  $scope.single_stud_mark_atten = $itemScope.stud;
                  $scope.markStudentAttendanceForMonth();
              }],
              null,
              ['Mark All Absent', function ($itemScope) {
                  $('.dropdown-menu').attr('style', 'display: none');

                  $scope.sims_attendance_code_m = 'A';
                  $scope.single_stud_mark_atten = $itemScope.stud;
                  $scope.markStudentAttendanceForMonth();
              }],
              null,
              //['More...', [
              //    ['Alert Cost', function ($itemScope) {
              //        alert($itemScope.item.cost);
              //    }],
              //    ['Alert Player Gold', function ($itemScope) {
              //        alert($scope.player.gold);
              //    }]
              //]]
          ];
          $scope.otherMenuOptions = [
            ['Favorite Color', function ($itemScope, $event, color) {
                alert(color);
            }]
          ]


          $scope.P_cnt = 0, $scope.A_cnt = 0, $scope.AE_cnt = 0, $scope.T_cnt = 0, $scope.TE_cnt = 0, $scope.UM_cnt = 0, $scope.NA_cnt = 0, $scope.total_students = 0;

          var today = new Date();
          var dd = today.getDate();
          var mm = today.getMonth() + 1; //January is 0!

          var yyyy = today.getFullYear();
          $scope.todays_date = dd + '-' + mm + '-' + yyyy;
          //$http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (curiculum) {
          //    $scope.Curriculum_obj = curiculum.data;
          //    if (curiculum.data.length > 0) {
          //        $scope.cur_code = curiculum.data[0].sims_cur_code;
          //        $scope.Cur_Change();
          //    }
          //});


          //function getCur(flag, comp_code) {
          //    if (flag) {

          //        $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
          //            $scope.Curriculum_obj = res.data;
          //            if ($scope.Curriculum_obj.length > 0) {
          //                $scope.cur_code = $scope.Curriculum_obj[0].sims_cur_code;
          //                $scope.Cur_Change();
          //            }

          //        });
          //    }
          //    else {

          //        $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
          //            $scope.Curriculum_obj = res.data;
          //            if ($scope.Curriculum_obj.length > 0) {
          //                $scope.cur_code = $scope.Curriculum_obj[0].sims_cur_code;
          //                $scope.Cur_Change();
          //            }
          //        });


          //    }

          //}

          //$http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
          //    $scope.global_count_comp = res.data;

          //    if ($scope.global_count_comp) {
          //        getCur(true, $scope.user_details.comp);


          //    }
          //    else {
          //        getCur(false, $scope.user_details.comp)
          //    }
          //});


          $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum?username=" + $rootScope.globals.currentUser.username).then(function (res) {
              debugger;
              $scope.Curriculum_obj = res.data;
              if ($scope.Curriculum_obj.length > 0) {
                  $scope.cur_code = $scope.Curriculum_obj[0].sims_cur_code;
                  $scope.Cur_Change();
              }
          });



          var Current_Username = $rootScope.globals.currentUser.username;
          $scope.btn_mark = false;
          $scope.Cur_Change = function () {
              $http.get(ENV.apiUrl + "api/attendance/getAttendanceyear?cur_code=" + $scope.cur_code).then(function (academicyears) {
                  $scope.academicyears_obj = academicyears.data;
                  if (academicyears.data.length > 0) {
                      $scope.Acdm_yr = academicyears.data[0].sims_academic_year;
                      $scope.acdm_yr_change();
                  }

              });
              $scope.attendance_codes_obj = {};
              $http.get(ENV.apiUrl + "api/attendance/getAttendanceCode?cur_code=" + $scope.cur_code).then(function (attendance_code) {
                  $scope.attendance_codes_obj = attendance_code.data;
                  //  $scope.attendance_codes_obj.push($scope.shahaji);                  
              });
          }

          $scope.acdm_yr_change = function () {
              $http.get(ENV.apiUrl + "api/attendance/getAttendancegrade?cur_code=" + $scope.cur_code + "&academic_year=" + $scope.Acdm_yr + "&userName=" + Current_Username).then(function (grades) {
                  $scope.grades_obj = grades.data;
              });

              $http.get(ENV.apiUrl + "api/attendance/getTerms?cur_code=" + $scope.cur_code + "&ayear=" + $scope.Acdm_yr).then(function (terms) {
                  $scope.terms_obj = terms.data;
                  $scope.term_change();
              });
          }
          $scope.grade_change = function () {

              $http.get(ENV.apiUrl + "api/attendance/getAttendancesection?cur_code=" + $scope.cur_code + "&academic_year=" + $scope.Acdm_yr
                  + "&gradeCode=" + $scope.grade_code + "&userName=" + Current_Username).then(function (section) {
                      $scope.sections_names_obj = section.data;
                  });

          }
          $scope.getDays_in_month = function (sims_month_name) {
              $scope.list_student_data = [];
              $scope.stud_table = false;
              var year = sims_month_name.split('-')[1];
              var month = sims_month_name.split('-')[0];
              month = new Date(Date.parse(month + " 1," + year)).getMonth() + 1;
              $scope.month_number = month;
              $scope.att_year = year;
              //   debugger
              $scope.rowscount = new Date(year, month, 0).getDate();
              var dayNameArr = new Array("SUN", "MON", "TUE", "WED", "THR", "FRD", "SAT");
              /* var daysInMonth = [0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
               var monthNames = ['', 'January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
               if ((month == 2) && (year % 4 == 0) && ((year % 100 != 0) || (year % 400 == 0))) {
                   $scope.rowscount = 29;
               } else {
                   $scope.rowscount = daysInMonth[month];
               }
               */
              $scope.temp = [];
              $scope.days = [];
              for (var i = 1; i <= $scope.rowscount; i++) {
                  var data = [];
                  var d = new Date(year + '-' + month + '-' + i);
                  var dayNo = (i % 7);
                  if (dayNo == 0) {
                      dayNo = 7;
                  }
                  data['id'] = i + ' ' + dayNameArr[d.getDay()];
                  $scope.temp.push(data);

                  $scope.days.push({ day: i, mon: dayNameArr[d.getDay()] })
              }
             
          }

          $scope.term_change = function () {
              
              var starDate = '', endDate = '';
             // for (var i = 0; i < $scope.terms_obj.length; i++) {
               //   if ($scope.terms_obj[i].sims_term_code == $scope.term_code) {
                      starDate = $scope.terms_obj[0].sims_term_start_date;
                      endDate = $scope.terms_obj[0].sims_term_end_date;
                   //   break;
                 // }
              //}
              //var date = starDate;
              //var year = date.split("-")[0];
              //var month = date.split("-")[1];
              //var day = date.split("-")[2];

              //var starDate = year + "-" + month + "-" + day;
              //date = endDate;
              //year = date.split("-")[0];
              //month = date.split("-")[1];
              //day = date.split("-")[2];

              //endDate = year + "-" + month + "-" + day;

              $http.get(ENV.apiUrl + "api/attendance/getTermDetails?startDate=" + starDate + "&endDate=" + endDate).then(function (termsDetails) {
                  $scope.termsDetails_obj = termsDetails.data;
              });
          }
          //   swal("Event Updated Successfully!", "success")
          //   swal({ title: "Alert", text: "Event Can not Update", imageUrl: "assets/img/notification-alert.png", });
          $scope.get_attend_code = function (codes, index) {

              $scope.mnuCard = codes;
              //var s='#'+index
              //$scope.sty = { 'border-bottom': '3px solid ' + codes.sims_attendance_color };
              //$(s).css( $scope.sty);
              $scope.sims_attendance_color = codes.sims_attendance_color;
              $scope.sims_attendance_code = codes.sims_attendance_code;
          }

          $scope.hidesummary = true;

          $scope.getAttendancePerDay = function () {
              $scope.curDate = $filter('date')(new Date(), 'yyyy-MM-dd');
              var dateObj = new Date();
              var month = dateObj.getUTCMonth() + 1;
              if (month == $scope.month_number) {
                  $http.get(ENV.apiUrl + "api/attendance/getStudentAttendance?cur_code=" + $scope.cur_code + "&ayear=" + $scope.Acdm_yr + "&grade=" + $scope.grade_code + "&section=" + $scope.section_code + "&attednacedate=" + $scope.curDate).then(function (attendance) {
                      $scope.attendance = attendance.data;
                      $scope.totalstudents = $scope.attendance.length;
                      $scope.hidesummary = false;
                      $scope.update();
                  });
              }
          }

          $scope.update = function () {
              $scope.total_present = 0;
              $scope.total_absent = 0;
              for (var i = 0; i < $scope.attendance.length; i++) {
                  if ($scope.attendance[i].sims_attendance_code == 'P') $scope.total_present += 1;
                  if ($scope.attendance[i].sims_attendance_code == 'A') $scope.total_absent += 1;
              }
          }
          $scope.getUserRole = function () {
              $http.get(ENV.apiUrl + "api/attendance/getUserRole?username=" + user + "&sims_grade_code=" + $scope.grade_code + "&sims_section_code=" + $scope.section_code).then(function (userRole) {
                  if (userRole.data.length > 0) {
                      $scope.userStatus = userRole.data[0].user_status;                      
                  }                  
              });
          }

          $scope.getUserAttendanceTime = function () {
              $http.get(ENV.apiUrl + "api/attendance/getUserAttendanceTimeStatus").then(function (userAttendanceTime) {
                  if (userAttendanceTime.data.length > 0) {
                      $scope.userTimeStatus = userAttendanceTime.data[0].user_status;                      
                  }
              });
          }

          $scope.stud_table = false;
          $scope.getStudentMonthlyAttendance = function () {
              $scope.day_column_obj = $scope.temp;
              $scope.stud_table = false;
              $scope.btn_mark = false;
              $scope.hidesummary = true;
              $scope.loading = true;
              $http.get(ENV.apiUrl + "api/attendance/getMonthlyAttendance?cur_code=" + $scope.cur_code + "&ayear=" + $scope.Acdm_yr + "&grade=" + $scope.grade_code +
                   "&section=" + $scope.section_code + "&month=" + $scope.month_number + "&year=" + $scope.att_year).then(function (monthlyAttendance) {
                       $scope.stud_monthlyAttendance_obj = monthlyAttendance.data;                      
                       $scope.getAttendancePerDay();
                       $scope.getUserRole();
                       $scope.getUserAttendanceTime();
                       if (monthlyAttendance.data.length <= 0) {
                           swal({ text: "No Attendance for this month", imageUrl: "assets/img/close.png" });
                       }                       
                       $scope.total_students = $scope.stud_monthlyAttendance_obj.length;
                       $scope.stud_table = true;
                       $scope.loading = false;
                       $scope.btn_mark = true;
                       var d = new Date();
                       var n = d.getDate();
                       if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                           $timeout(function () {
                               $("#fixTable").tableHeadFixer({ 'top': 1 });
                           }, 100);
                       }
                       else {
                           $timeout(function () {
                               $("#fixTable").tableHeadFixer({ "left": 3 });
                               //  $("#fixTable").tableHeadFixer({ 'top': 1 });

                           }, 100);

                       }
                       var dy_str = 'attendance_day' + n + '_code';
                       //attendance_day18_code;

                       $scope.P_cnt = 0, $scope.A_cnt = 0, $scope.AE_cnt = 0, $scope.T_cnt = 0, $scope.TE_cnt = 0, $scope.UM_cnt = 0, $scope.NA_cnt = 0;
                       for (var i = 0; i < $scope.stud_monthlyAttendance_obj.length; i++) {
                           if ($scope.stud_monthlyAttendance_obj[i][dy_str] && $scope.stud_monthlyAttendance_obj[i].yearno == yyyy && $scope.stud_monthlyAttendance_obj[i].monthYear == mm) {
                               if ($scope.stud_monthlyAttendance_obj[i][dy_str] == 'P') {
                                   $scope.P_cnt = $scope.P_cnt + 1;
                               }
                               else if ($scope.stud_monthlyAttendance_obj[i][dy_str] == 'A') {
                                   $scope.A_cnt = $scope.A_cnt + 1;
                               }
                               else if ($scope.stud_monthlyAttendance_obj[i][dy_str] == 'AE') {
                                   $scope.AE_cnt = $scope.AE_cnt + 1;
                               }
                               else if ($scope.stud_monthlyAttendance_obj[i][dy_str] == 'T') {
                                   $scope.T_cnt = $scope.T_cnt + 1;
                               }
                               else if ($scope.stud_monthlyAttendance_obj[i][dy_str] == 'TE') {
                                   $scope.TE_cnt = $scope.TE_cnt + 1;
                               }
                               else if ($scope.stud_monthlyAttendance_obj[i][dy_str] == 'UM') {
                                   $scope.UM_cnt = $scope.UM_cnt + 1;
                               }
                               else if ($scope.stud_monthlyAttendance_obj[i][dy_str] == 'NA') {
                                   $scope.NA_cnt = $scope.NA_cnt + 1;
                               }
                           }
                       }

                     

                   }, function () {
                       swal({ text: "Internal Server Error", imageUrl: "assets/img/notification-alert.png" });
                   });
          }

          $scope.list_student_data = []; $scope.sims_attendance_code = null; $scope.sims_attendance_color = null;

          var mark_attendance_school = ['asd'];
          var user = $rootScope.globals.currentUser.username;

          $scope.Mark_Attendance = function (stud, date, code) {

              $scope.getUserAttendanceTime();
              var dateObj = new Date();
              var month = dateObj.getUTCMonth() + 1; //months from 1-12
              var day = dateObj.getUTCDate();
              var year = dateObj.getUTCFullYear();

              var mothVal = document.getElementById("monthName");
              var monthYear = mothVal.options[mothVal.selectedIndex].text;
              var yearsplit = monthYear.split("-");

              $scope.attednacedate = $scope.att_year + '-' + $scope.month_number + '-' + date;

                  if ($scope.sims_attendance_code != 'W' && $scope.sims_attendance_code != 'H') {
                      if (code != 'W' && code != 'H') {
                          if ($scope.sims_attendance_code != null) {
                              //                        
                              if (parseInt(yearsplit[1]) >= year && parseInt($scope.month_number) >= month && parseInt(date) > day) {
                                  swal({ text: "Not allowed to mark attendance for Future Date", imageUrl: "assets/img/close.png" });
                                  return;
                              }
                              if ($scope.userStatus == 'N') {
                                  if (parseInt(yearsplit[1]) <= year && parseInt($scope.month_number) <= month && parseInt(date) < day) {
                                      swal({ text: "Not allowed to mark attendance for Previous Date", imageUrl: "assets/img/close.png" });
                                      return;
                                  }
                                  else {
                                      if (parseInt(yearsplit[1]) <= year && parseInt($scope.month_number) <= month && parseInt(date) > day) {
                                          swal({ text: "Not allowed to mark attendance for Previous Date", imageUrl: "assets/img/close.png" });
                                          return;
                                      }
                                  }
                                  if ($scope.userTimeStatus == '0') {
                                      swal({ text: "Not allowed to mark attendance after restricted time", imageUrl: "assets/img/close.png" });
                                      return;
                                  }
                              }

                              $scope.flag = true;
                              if ($scope.list_student_data.length == 0) {
                                  $scope.flag = false;
                                  var v = document.getElementById(stud.sims_enroll_number + date);
                                  v.style.backgroundColor = $scope.sims_attendance_color;
                                  v.textContent = $scope.sims_attendance_code;

                                  var data = {};
                                  data['sims_cur_code'] = stud.sims_cur_code;
                                  data['sims_academic_year'] = stud.sims_academic_year;
                                  data['sims_enroll_number'] = stud.sims_enroll_number;
                                  data['sims_grade_code'] = stud.sims_grade_code;
                                  data['sims_section_code'] = stud.sims_section_code;
                                  data['sims_student_name'] = stud.sims_student_name;
                                  data['monthYear'] = stud.monthYear;
                                  data['yearno'] = stud.yearno;
                                  data['attednacedate'] = $scope.att_year + '-' + $scope.month_number + '-' + date;
                                  data['sims_attendance_code'] = $scope.sims_attendance_code;
                                  $scope.list_student_data.push(data);
                              }
                              else {
                                  for (var i = 0; i < $scope.list_student_data.length; i++) {
                                      if ($scope.list_student_data[i].sims_enroll_number == stud.sims_enroll_number && $scope.list_student_data[i].attednacedate == $scope.attednacedate && $scope.list_student_data[i].sims_attendance_code) {
                                          $scope.flag = false;
                                          var v = document.getElementById(stud.sims_enroll_number + date);
                                          v.style.backgroundColor = $scope.sims_attendance_color;
                                          v.textContent = $scope.sims_attendance_code;
                                          $scope.list_student_data[i].sims_attendance_code = $scope.sims_attendance_code;
                                          // $scope.list_student_data[i].attednacedate = $scope.attednacedate;
                                          break;
                                      }
                                  }
                              }
                              if ($scope.flag == true) {
                                  var v = document.getElementById(stud.sims_enroll_number + date);
                                  v.style.backgroundColor = $scope.sims_attendance_color;
                                  v.textContent = $scope.sims_attendance_code;

                                  var data = {};
                                  data['sims_cur_code'] = stud.sims_cur_code;
                                  data['sims_academic_year'] = stud.sims_academic_year;
                                  data['sims_enroll_number'] = stud.sims_enroll_number;
                                  data['sims_grade_code'] = stud.sims_grade_code;
                                  data['sims_section_code'] = stud.sims_section_code;
                                  data['sims_student_name'] = stud.sims_student_name;
                                  data['monthYear'] = stud.monthYear;
                                  data['yearno'] = stud.yearno;
                                  data['attednacedate'] = $scope.att_year + '-' + $scope.month_number + '-' + date;
                                  data['sims_attendance_code'] = $scope.sims_attendance_code;
                                  $scope.list_student_data.push(data);
                              }
                          }
                          else {
                              swal({ text: "Please Select Attendance Code", imageUrl: "assets/img/notification-alert.png" });
                          }
                      }                  
              }
              
              /*
              $http.post(ENV.apiUrl + "api/attendance/markStudentAttendance?cur_code=" + $scope.student_data.sims_cur_code + "&ayear=" + $scope.student_data.sims_academic_year
                  + "&grade=" + $scope.student_data.sims_grade_code + "&section=" + $scope.student_data.sims_section_code
                 + "&enrollNumber=" + $scope.student_data.sims_enroll_number + "&att_code=" + $scope.sims_attendance_code + "&attednacedate=" + $scope.attednacedate).then(function (res) {
                     $scope.result = res.data;

                     

                   $http.get(ENV.apiUrl + "api/attendance/getMonthlyAttendance?cur_code=" + $scope.cur_code + "&ayear=" + $scope.Acdm_yr + "&grade=" + $scope.grade_code +
                   "&section=" + $scope.section_code + "&month=" + $scope.month_number + "&year=" + $scope.att_year).then(function (monthlyAttendance) {
                       $scope.stud_monthlyAttendance_obj = monthlyAttendance.data;
                       $scope.total_students = $scope.stud_monthlyAttendance_obj.length;
                       $scope.stud_table = true;
                      
                   });
                 });
              */
          }

          $scope.Mark_All_Attendance = function () {              
              if ($scope.list_student_data.length != 0) {
                  $scope.loading = true;
                  $http.post(ENV.apiUrl + "api/attendance/markAllStudentAttendance", $scope.list_student_data).then(function (res) {
                      $scope.mark_result = res.data;
                      $scope.getStudentMonthlyAttendance();
                      $scope.loading = false;
                      if ($scope.mark_result == true) {
                          swal("Student Attendance Added Successfully!", "success")
                      }
                     
                  }, function () {
                      swal({ text: "Internal Server Error", imageUrl: "assets/img/notification-alert.png" });
                  });
              }
              else {
                  swal({ text: "Please Assign Attendance to Student", imageUrl: "assets/img/notification-alert.png" });
              }
          }


          $scope.getStudentMonthlyAttendanceWindow = function (str) {


              $http.get(ENV.apiUrl + "api/attendance/getAttendanceCount?cur=" + $scope.cur_code).then(function (res) {
                  $scope.att_count_lst = res.data;
              });
              $("#attendance_detail_model").modal("show");
              $scope.std_new = angular.copy(str);
              $scope.std_new['sims_grade'] = $("[name='cmb_garde_new'] option:selected").text();
              $scope.std_new['sims_section'] = $("[name='cmb_section_new'] option:selected").text();

          }

          $scope.getStudentMonthlyAttendance_details = function () {
              $http.get(ENV.apiUrl + "api/attendance/getAttendanceCount_details?cur=" + $scope.std_new.sims_cur_code + "&academic_year=" + $scope.std_new.sims_academic_year + '&grade=' + $scope.std_new.sims_grade_code + '&section=' + $scope.std_new.sims_section_code + '&enroll=' + $scope.std_new.sims_enroll_number + '&count=' + $scope.category_count).then(function (res) {
                  $scope.category_date_lst = res.data;
                  $timeout(function () {
                      $("#fixTable1").tableHeadFixer({ 'top': 1 });
                  }, 100);
              });
          }

          $scope.comment = function (sobj, date, day_code) {

              $scope.copy_comment = angular.copy(sobj);
              $scope.attednacedate1 = $scope.att_year + '-' + $scope.month_number + '-' + date;

              $scope.attednacedate2 = date + '-' + $scope.month_number + '-' + $scope.att_year;


              $("#comment_dailog").modal("show");

              $http.get(ENV.apiUrl + "api/attendance/get_AttendanceComment?enroll=" + $scope.copy_comment.sims_enroll_number + "&date=" + $scope.attednacedate2).then(function (res) {
                  if (res.data.length > 0) {
                      $scope.copy_comment['sims_attendance_day_comment'] = res.data[0].sims_attendance_day_comment;
                  }
              });

          }

          $scope.save_comment = function () {

              $http.post(ENV.apiUrl + "api/attendance/markAttendanceComment_update?enroll=" + $scope.copy_comment.sims_enroll_number + "&date=" + $scope.attednacedate2 + "&comment=" + $scope.copy_comment.sims_attendance_day_comment).then(function (res) {
                  $("#comment_dailog").modal("hide");
              });
          }


          //$('body').addClass('grey condense-menu');
          //$('#main-menu').addClass('mini');
          //$('.page-content').addClass('condensed');
          //$rootScope.isCondensed = true;



      }])

})();