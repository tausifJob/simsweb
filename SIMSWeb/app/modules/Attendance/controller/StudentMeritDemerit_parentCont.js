﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Attendance');

    simsController.directive('ngRightClick', function ($parse) {
        return function (scope, element, attrs) {
            var fn = $parse(attrs.ngRightClick);
            element.bind('contextmenu', function (event) {
                scope.$apply(function () {
                    event.preventDefault();
                    fn(scope, { $event: event });
                });
            });
        };
    });





    simsController.controller('StudentMeritDemerit_parentCont', ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter',
    function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {

        $scope.hidesummary = true;
      //  $scope.merit3 = true;

        $('*[data-datepicker="true"] input[type="text"]').datepicker(
            {
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd',
                endDate: (new Date())
            });

        //$scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '', attDate: new Date().getFullYear() + '-' + (new Date().getMonth()+1) + '-' + new Date().getDate() };

        if ($http.defaults.headers.common['schoolId'] == 'clarionschool') {
            $scope.show_mobile = true;
        }

        console.log(moment(new Date(), "DD-MM-YYYY"), 'date');
        $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '', attDate: moment(new Date()).format('DD-MM-YYYY') };
        $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
            $('input[type="text"]', $(this).parent()).focus();
        });


        $scope.temp = {};
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        $scope.mom_start_date = '';
        $scope.mom_end_date = '';

        $(function () {
            $('#bell_box').multipleSelect({ width: '100%' });
            $('#teacher_box').multipleSelect({ width: '100%', filter: true });

        });

        var today = new Date();
        var dd = today.getDate();
        if (dd < 10) {
            dd = '0' + dd;
        }
        var mm = today.getMonth() + 1; //January is 0!
        if (mm < 10) {
            mm = '0' + mm;
        }
        var yyyy = today.getFullYear();
        $scope.sdate = dd + '-' + mm + '-' + yyyy;
        $scope.edate = dd + '-' + mm + '-' + yyyy;
        $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
        $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;

        $(function () {
            $("#chkmarks").click(function () {
                if ($(this).is(":checked")) {
                    $("#fdate").attr("disabled", "disabled");
                    $scope.mom_start_date = '';

                } else {

                    $("#fdate").removeAttr("disabled");
                    $("#fdate").focus();
                    $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
                }
            });
        });
        if ($scope.mom_start_date_check == true) {
            $scope.mom_start_date = '';
        }
        else {
            $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
        }

        $(function () {
            $("#chkmarks1").click(function () {
                if ($(this).is(":checked")) {
                    $("#fdate").attr("disabled", "disabled");
                    $scope.mom_end_date = '';

                } else {

                    $("#fdate").removeAttr("disabled");
                    $("#fdate").focus();
                    $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
                }
            });
        });
        if ($scope.mom_end_date_check == true) {
            $scope.mom_end_date = '';
        }
        else {
            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
        }





        var username = $rootScope.globals.currentUser.username;
        //$http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (curiculum) {
        //    $scope.curiculum = curiculum.data;
        //    $scope.edt['sims_cur_code'] = curiculum.data[0].sims_cur_code;
        //    $scope.cur_change();
        //});

        $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum?username=" + $rootScope.globals.currentUser.username).then(function (res) {
            debugger;
            $scope.curiculum = res.data;
            $scope.edt['sims_cur_code'] = $scope.curiculum[0].sims_cur_code;
            $scope.cur_change();
        });

       


        $scope.cur_change = function () {

            //$http.get(ENV.apiUrl + "api/attendance/getAttendanceyear?cur_code=" + $scope.edt.sims_cur_code).then(function (academicyears) {
            //    $scope.academicyears = academicyears.data;
            //});

            $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllAcademicYears?cur_code=" + $scope.edt.sims_cur_code).then(function (AcademicYears) {
                $scope.academicyears = AcademicYears.data;
                for (var i = 0; i < $scope.academicyears.length; i++) {
                    if ($scope.academicyears[i].sims_academic_year_status == 'C' || $scope.academicyears[i].sims_academic_year_status == 'Current') {
                        //   $scope.edt['sims_cur_code'] = cur_values;
                        $scope.edt['sims_academic_year'] = $scope.academicyears[i].sims_academic_year;
                    }
                }
                $scope.acdm_yr_change();
            });

            $http.get(ENV.apiUrl + "api/attendance/getAttendanceCode?cur_code=" + $scope.edt.sims_cur_code).then(function (attendance_code) {
                $scope.attendance_code = attendance_code.data;
                //$scope.rdbtn = $scope.attendance_code[0].sims_attendance_code;
            });
        }
        $scope.acdm_yr_change = function () {
            $http.get(ENV.apiUrl + "api/attendance/getAttendancegrade?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&userName=" + username).then(function (grades) {

                $scope.grades = grades.data;
            });
        }
        $scope.grade_change = function () {

            $http.get(ENV.apiUrl + "api/attendance/getAttendancesection?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&gradeCode=" + $scope.edt.sims_grade_code + "&userName=" + username).then(function (section) {
                $scope.section = section.data;
            });

        }



        $scope.getData = function () {
           
            //+ "&trans_number=" + ' '
            debugger;
            $http.get(ENV.apiUrl + "api/MeritDemeritForm/getMeritDemerit?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&grade_code=" + $scope.edt.sims_grade_code + "&section_code=" + $scope.edt.sims_section_code + "&mom_start_date=" + ' ' + "&mom_end_date=" + ' ' + "&search=" + ' ' +"&var=" +'1').then(function (attendance) {
                $scope.attendance = attendance.data;
                if (attendance.attendance.length > 0) {
                   
                    $scope.attendance = attendance.data;
                    $scope.attendance_copy = angular.copy($scope.attendance);

                    $scope.operation = true;
                    $scope.totalstudents = $scope.attendance.length;
                    $scope.hidesummary = false;
                    
                    
                }
                else {
                    swal({ title: "Alert", text: " Data is not Available for selected date", showCloseButton: true, width: 300, height: 200 });
                }
               
                console.log($scope.attendance);

                //var t = 0;
                //for (var i = 0; i < $scope.attendance.length; i++) {

                //    t = t + parseInt($scope.attendance[i].sims_detention_point)
                //}

                //$scope.attendance[0]['sims_detention_point_total'] = t;


          
            });
        }



      


        //
        $scope.img_url = 'http://api.mograsys.com/kindoapi/Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/';
        //$scope.getAttendance = function () {
        //    $scope.searchbox = true;
        //    if ($scope.edt.attDate == '') {
        //        swal({ text: "Please select Attendance date.", imageUrl: "assets/img/notification-alert.png", width: 400, showCloseButton: true });

        //    }
        //    else {
        //        $http.get(ENV.apiUrl + "api/attendance/getStudentAttendance?cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&attednacedate=" + $scope.edt.attDate).then(function (attendance) {
        //            if (attendance.data.length <= 0) {
        //                swal({ text: "No Attendance Data is found for this date.", imageUrl: "assets/img/close.png", width: 400, showCloseButton: true });
        //                $scope.attendance = [];

        //            }
        //            else {
        //                if (attendance.data[0].sims_isholiday == true) {
        //                    swal({ text: "Attendance can not be marked on Weekends And Holidays.", imageUrl: "assets/img/close.png", width: 400, showCloseButton: true });
        //                    $scope.attendance = [];
        //                }
        //                else {

        //                    $scope.attendance = attendance.data;
        //                    $scope.attendance_copy = angular.copy($scope.attendance);

        //                    $scope.operation = true;
        //                    $scope.totalstudents = $scope.attendance.length;
        //                    $scope.hidesummary = false;
        //                }
        //            }
        //            $scope.update();
        //        });
        //    }
        //}
        $scope.sims_attendance_code = '';
        $scope.value = '';
        $scope.attcode = function (info) {
            debugger;
            $scope.mnuCard = info;
            $scope.value = info;
            $scope.sims_attendance_code = $scope.value.sims_attendance_code;
        }
        $scope.att_list = [];
        $scope.submit = function () {
            $http.post(ENV.apiUrl + "api/attendance/markStudentAttendanceList", $scope.att_list).then(function (attendance) {
                if (attendance.data) {
                    swal({ text: "Attendance Marked Successfully", imageUrl: "assets/img/check.png", width: 350, showCloseButton: true })
                    $scope.getAttendance();
                }
                else {
                    $scope.attendance = $scope.attendance_copy;
                    swal({ text: "Attendance Not Marked", imageUrl: "assets/img/close.png", width: 350, showCloseButton: true })
                }

                $scope.att_list = [];
            });

            // $scope.getAttendance();
            $scope.update();
        }
        $scope.markattendnace = function (objinp) {
            if ($scope.sims_attendance_code != '' && ($scope.edt.attDate != '' || $scope.edt.attDate != undefined)) {
                if ($scope.sims_attendance_code != 'W' && $scope.sims_attendance_code != 'H') {
                    objinp.sims_attendance_code = $scope.value.sims_attendance_code;
                    objinp.sims_attendance_color = $scope.value.sims_attendance_color;
                    //$http.post(ENV.apiUrl + "api/attendance/markStudentAttendance?cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&enrollNumber=" + objinp.sims_student_enroll + "&att_code=" + objinp.sims_attendance_code + "&attednacedate=" + $scope.edt.attDate).then(function (attendance) {
                    //});
                    //$scope.update();
                    var obj1 = { cur_code: $scope.edt.sims_cur_code, ayear: $scope.edt.sims_academic_year, grade: $scope.edt.sims_grade_code, section: $scope.edt.sims_section_code, enrollNumber: objinp.sims_student_enroll, att_code: objinp.sims_attendance_code, attednacedate: $scope.edt.attDate, user_name: $rootScope.globals.currentUser.username }
                    $scope.att_list.push(obj1)
                }
                else { swal({ text: "Attendance Code Not Marked", imageUrl: "assets/img/close.png", width: 350, showCloseButton: true }); }


            }
            else {
                swal({ text: "Please select Attendance Code", imageUrl: "assets/img/notification-alert.png", width: 350, showCloseButton: true });
            }
        }
        $scope.reset = function () {
            $scope.sims_attendance_code = '';
            $scope.value = '';
            $scope.attendance = '';
            $scope.attendance_code = '';
            $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', attDate: '' };
            $scope.totalstudents = 0;
            $scope.hidesummary = true;
            $scope.total_present = 0;
            $scope.total_absent = 0;
            $scope.att_list = [];
        }
        $scope.update = function () {
            $scope.total_present = 0;
            $scope.total_absent = 0;
            for (var i = 0; i < $scope.attendance.length; i++) {
                if ($scope.attendance[i].sims_attendance_code == 'P') $scope.total_present += 1;
                if ($scope.attendance[i].sims_attendance_code == 'A') $scope.total_absent += 1;

            }
        }
        //$scope.markAllAttendance = function (att_Code) {
        //    
        //    $http.get(ENV.apiUrl + "api/attendanceDaily/markStudentAttendance?cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&enrollNumber=''&att_code=" + att_Code + "&attednacedate=" + $scope.edt.attDate).then(function (attendance) {
        //        $scope.getAttendance();
        //    });

        //}
        $scope.markAllAttendance = function (att_Code) {
            //$http.post(ENV.apiUrl + "api/attendanceDaily/markStudentAttendance?att_code=" + att_Code + "&attDate=" + $scope.edt.attDate + "&cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code).then(function (attendance) {
            //    $scope.getAttendance();
            //});
            for (var i = 0; i < $scope.attendance_code.length; i++) {
                if ($scope.attendance_code[i].sims_attendance_code == att_Code) {
                    $scope.att_color = $scope.attendance_code[i].sims_attendance_color;
                }
            }

            $scope.att_list = [];
            for (var i = 0; i < $scope.attendance.length; i++) {
                $scope.attendance[i].sims_attendance_code = att_Code;
                $scope.attendance[i].sims_attendance_color = $scope.att_color;

                var obj1 = { cur_code: $scope.edt.sims_cur_code, ayear: $scope.edt.sims_academic_year, grade: $scope.edt.sims_grade_code, section: $scope.edt.sims_section_code, enrollNumber: $scope.attendance[i].sims_student_enroll, att_code: att_Code, attednacedate: $scope.edt.attDate, user_name: $rootScope.globals.currentUser.username }
                $scope.att_list.push(obj1)
            }

        }

       
        $scope.meritModal = function (str)
        {
            debugger;
            $scope.merit1 = false;
            $scope.merit2 = false;
            $scope.back = true;
            $scope.merit3 =true;

            //$scope.merit3 = false;
            
            $("#merit_demerit_main").modal("show");
            $http.get(ENV.apiUrl + "api/MeritDemeritForm/getMeritDemerit?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&grade_code=" + $scope.edt.sims_grade_code + "&section_code=" + $scope.edt.sims_section_code + "&mom_start_date=" + ' ' + "&mom_end_date=" + ' ' + "&search=" + str.sims_enroll_number + "&var=" +'2').then(function (res) {
                if (res.data.length > 0) {
                     $scope.meritdata = res.data;

                   
                }
            });
          

        }

        $scope.meritsubModal = function (str2) {
            debugger;
            $scope.merit1 = true;
            $scope.merit2 = true;
            $scope.merit3 = true;
         
            $scope.back = false;
           
           
            $("#only_merit").modal("show");
            $http.get(ENV.apiUrl + "api/MeritDemeritForm/getMeritDemeritSubModel?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&grade_code=" + $scope.edt.sims_grade_code + "&section_code=" + $scope.edt.sims_section_code + "&search=" + ' ' + "&date=" + str2.sims_detention_transaction_created_date + "&trans_number=" + str2.sims_detention_transaction_number).then(function (res) {
                if (res.data.length > 0) {

                    $scope.meritsubdata = res.data;
                }
            });
            
            
        }
        
        var M;
        var D;
        $scope.meritdemeritwise = function (str) {
            //$scope.mdata = [];
            $scope.ddata = [];
            var str2;
            debugger;
            if (str == 'M') {
                str2 = 'Merit';
                $http.get(ENV.apiUrl + "api/MeritDemeritForm/getMeritDemeritParameter?meritdemerit=" + str2).then(function (res1) {
                    debugger;
                    $scope.parameter_data = res1.data;
                    //$scope.temp.parameter = $scope.parameter_data[0].sims_appl_parameter;


                
                for (var i = 0; i < $scope.meritdata.length; i++) {
                    for (var j = 0; j < $scope.parameter_data.length;j++){
                        if ($scope.meritdata[i].sims_detention_level_code == $scope.parameter_data[j].sims_appl_parameter) {
                        $scope.ddata.push($scope.meritdata[i]);


                        }
                }
                }


                var t = 0;
                for (var i = 0; i < $scope.ddata.length; i++) {
                   
                    t = t + parseInt($scope.ddata[i].sims_detention_point)
                }

                $scope.ddata[0]['sims_detention_point_total'] = t;


                $scope.ddata_copy = angular.copy($scope.ddata);
                });

            
         
            }

         
                
            else if (str == '1') {
                str2 = 'Demerit';
                $http.get(ENV.apiUrl + "api/MeritDemeritForm/getMeritDemeritParameter?meritdemerit=" + str2).then(function (res1) {
                    debugger;
                    $scope.parameter_data = res1.data;
                    //$scope.temp.parameter = $scope.parameter_data[0].sims_appl_parameter;


              

                for (var i = 0; i < $scope.meritdata.length; i++) {
                    for (var j = 0; j < $scope.parameter_data.length; j++) {
                        if ($scope.meritdata[i].sims_detention_level_code == $scope.parameter_data[j].sims_appl_parameter) {
                            $scope.ddata.push($scope.meritdata[i]);
                        }

                    }
                }

                var t = 0;
                for (var i = 0; i < $scope.ddata.length; i++) {

                    t = t + parseInt($scope.ddata[i].sims_detention_point)
                }

                $scope.ddata[0]['sims_detention_point_total'] = t;

                $scope.ddata_copy = angular.copy($scope.ddata);

                });

           
            }
            $scope.merit1 = false;
            $scope.merit3 = false;
            $scope.short_data = false;

        }





        $scope.searchDate = function () {
            debugger;
            $scope.ddata_new = [];
            var sdate = moment($scope.mom_start_date, 'DD-MM-YYYY').format('YYYY-MM-DD');
            var edate = moment($scope.mom_end_date, 'DD-MM-YYYY').format('YYYY-MM-DD');
            console.log('sdate', sdate)
            for (var i = 0; i < $scope.ddata_copy.length; i++) {
                if ($scope.mom_start_date_check && $scope.mom_end_date_check == false) {
                    if (new Date(moment($scope.ddata_copy[i].sims_detention_transaction_created_date, 'DD-MM-YYYY').format('YYYY-MM-DD')) <= new Date(edate)) {
                        $scope.ddata_new.push($scope.ddata_copy[i]);

                    }
                }
                else if ($scope.mom_end_date_check==true && $scope.mom_start_date_check == true) {
                    $scope.ddata_new.push($scope.ddata_copy[i]);

                }
                else if (new Date(moment($scope.ddata_copy[i].sims_detention_transaction_created_date, 'DD-MM-YYYY').format('YYYY-MM-DD')) >= new Date(sdate) && new Date(moment($scope.ddata_copy[i].sims_detention_transaction_created_date, 'DD-MM-YYYY').format('YYYY-MM-DD')) <=new Date( edate)) {

                    $scope.ddata_new.push($scope.ddata_copy[i]);

                }


            }


            $scope.ddata = $scope.ddata_new;
            
        }


        $scope.back_data = function () {
            $scope.merit1 = false;
            $scope.short_data = false;
            $scope.merit3 = false;
            $scope.back = true;
            $scope.merit2 = false;

        }



        //$scope.demeritsubModal = function () {
        //    debugger;
        //    $scope.merit1 = true;
        //    $scope.merit2 = true;
        //    $("#only_merit").modal("show");
        //    $http.get(ENV.apiUrl + "api/MeritDemeritForm/getMeritDemerit?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&grade_code=" + $scope.edt.sims_grade_code + "&section_code=" + $scope.edt.sims_section_code + "&mom_start_date=" + $scope.mom_start_date + "&mom_end_date=" + $scope.mom_end_date + "&search=" + ' ').then(function (res) {
        //        if (res.data.length > 0) {
        //            $scope.demeridata = res.data;
                   
        //        }
        //    });
        //}


        //$scope.demeritsubModal = function () {
        //    $("#only_demerit").modal("show");
        //}


     


        //$scope.comment = function (sobj) {

        //    $scope.copy_comment = angular.copy(sobj);
        //    $scope.attednacedate2 = $scope.edt.attDate

        //    $("#comment_dailog").modal("show");

        //    $http.get(ENV.apiUrl + "api/attendance/get_AttendanceComment?enroll=" + $scope.copy_comment.sims_student_enroll + "&date=" + $scope.edt.attDate).then(function (res) {
        //        if (res.data.length > 0) {
        //            $scope.copy_comment['sims_attendance_day_comment'] = res.data[0].sims_attendance_day_comment;
        //        }
        //    });

        //}

        $scope.modal_cancel = function () {
            debugger;
            $scope.merit3 =true;
            $scope.short_data = true;

        }


        $scope.save_comment = function () {

            $http.post(ENV.apiUrl + "api/attendance/markAttendanceComment_update?enroll=" + $scope.copy_comment.sims_student_enroll + "&date=" + $scope.edt.attDate + "&comment=" + $scope.copy_comment.sims_attendance_day_comment).then(function (res) {
                $("#comment_dailog").modal("hide");
            });
        }



    }])

})();