﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Attendance');


    simsController.controller('StudentAttendanceASDCont', ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter',
    function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {

        $scope.hidesummary = true;
        var mark_attendance_school = ['asd'];
        var user = $rootScope.globals.currentUser.username;
        $scope.edt = {};
        

        $scope.getUserRole = function () {
            $http.get(ENV.apiUrl + "api/attendance/getUserRole?username=" + user + "&sims_grade_code=" + $scope.edt.sims_grade_code + "&sims_section_code=" + $scope.edt.sims_section_code).then(function (userRole) {
                if (userRole.data.length > 0) {
                    $scope.userStatus = userRole.data[0].user_status;                    
                }
            });
        }
        

        $("#start_date").kendoDatePicker({
            format: "dd-MM-yyyy",
            max: new Date()
        });
        
        //$('*[data-datepicker="true"] input[type="text"]').datepicker(
        //{
        //    todayBtn: true,
        //    orientation: "top left",
        //    autoclose: true,
        //    todayHighlight: true,
        //    format: 'yyyy-mm-dd',
        //    endDate: (new Date())
        //});
        
        $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '', attDate: moment(new Date()).format('DD-MM-YYYY') };
        $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
            $('input[type="text"]', $(this).parent()).focus();
        });

        var username = $rootScope.globals.currentUser.username;
        //$http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (curiculum) {
        //    $scope.curiculum = curiculum.data;
        //    $scope.edt['sims_cur_code'] = curiculum.data[0].sims_cur_code;
        //    $scope.cur_change();
        //});


        $scope.cur_change = function () {

            //$http.get(ENV.apiUrl + "api/attendance/getAttendanceyear?cur_code=" + $scope.edt.sims_cur_code).then(function (academicyears) {
            //    $scope.academicyears = academicyears.data;
            //});

            $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllAcademicYears?cur_code=" + $scope.edt.sims_cur_code).then(function (AcademicYears) {
                $scope.academicyears = AcademicYears.data;
                for (var i = 0; i < $scope.academicyears.length; i++) {
                    if ($scope.academicyears[i].sims_academic_year_status == 'C' || $scope.academicyears[i].sims_academic_year_status == 'Current') {
                        //   $scope.edt['sims_cur_code'] = cur_values;
                        $scope.edt['sims_academic_year'] = $scope.academicyears[i].sims_academic_year;
                    }
                }
                $scope.acdm_yr_change();
            });

            $http.get(ENV.apiUrl + "api/attendance/getAttendanceCode?cur_code=" + $scope.edt.sims_cur_code).then(function (attendance_code) {
                $scope.attendance_code = attendance_code.data;

                for (var i = 0; i < $scope.attendance_code.length; i++) {
                    if ($scope.attendance_code[i].sims_attendance_code == 'P') {
                        $scope.present_color = $scope.attendance_code[i].sims_attendance_color;
                        $scope.value = angular.copy($scope.attendance_code[i]);
                        $scope.sims_attendance_code = $scope.value.sims_attendance_code
                    }
                    if ($scope.attendance_code[i].sims_attendance_code == 'A')
                        $scope.absent_color = $scope.attendance_code[i].sims_attendance_color;
                }

                //$scope.rdbtn = $scope.attendance_code[0].sims_attendance_code;
            });
        }

        function getCur(flag, comp_code) {
            if (flag) {

                $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                    $scope.curiculum = res.data;
                    $scope.edt['sims_cur_code'] = $scope.curiculum[0].sims_cur_code;
                    $scope.cur_change();

                });
            }
            else {

                $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                    $scope.curiculum = res.data;
                    $scope.edt['sims_cur_code'] = $scope.curiculum[0].sims_cur_code;
                    $scope.cur_change();
                });


            }

        }

        $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
            $scope.global_count_comp = res.data;

            if ($scope.global_count_comp) {
                getCur(true,'1');         //$scope.user_details.comp


            }
            else {
                getCur(false,'1')  //$scope.user_details.comp
            }
        });

        $scope.acdm_yr_change = function () {
            $http.get(ENV.apiUrl + "api/attendance/getAttendancegrade?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&userName=" + username).then(function (grades) {                
                $scope.grades = grades.data;
            });
        }
        $scope.grade_change = function () {

            $http.get(ENV.apiUrl + "api/attendance/getAttendancesection?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&gradeCode=" + $scope.edt.sims_grade_code + "&userName=" + username).then(function (section) {
                $scope.section = section.data;
            });

        }
        //
        $scope.img_url = ENV.apiUrl+ 'Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/';

        $scope.getAttendance = function () {
            $scope.getUserRole();
            $scope.getUserAttendanceTime();

            if ($scope.edt.attDate == '') {
                swal({ text: "Please select Attendance date.",imageUrl: "assets/img/notification-alert.png", width: 400, showCloseButton: true });

            }
            else {
                $http.get(ENV.apiUrl + "api/attendance/getStudentAttendance?cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&attednacedate=" + $scope.edt.attDate).then(function (attendance) {
                    if (attendance.data.length <= 0) {
                        swal({ text: "No Attendance Data is found for this date.",imageUrl: "assets/img/close.png", width: 400, showCloseButton: true });
                    }
                    else {
                        $scope.attendance = attendance.data;
                        $scope.operation = true;
                        $scope.totalstudents = $scope.attendance.length;
                        $scope.hidesummary = false;
                    }
                    $scope.update();
                });
            }
        }
        $scope.sims_attendance_code = '';
        $scope.value = '';
        $scope.attcode = function (info) {
            $scope.value=angular.copy(info);
            $scope.sims_attendance_code = $scope.value.sims_attendance_code;
        }

        $scope.getUserAttendanceTime = function () {
            $http.get(ENV.apiUrl + "api/attendance/getUserAttendanceTimeStatus?user=" + $rootScope.globals.currentUser.username).then(function (userAttendanceTime) {
                if (userAttendanceTime.data.length > 0) {
                    $scope.userTimeStatus = userAttendanceTime.data[0].user_status;
                    console.log("userTimeStatus",$scope.userTimeStatus);
                }
            });
        }

        var currentDate = $filter('date')(new Date(), 'dd-MM-yyyy');

              $scope.att_list = [];
            $scope.submit = function () {
                $http.post(ENV.apiUrl + "api/attendance/markStudentAttendanceList", $scope.att_list).then(function (attendance) {
                    if (attendance.data) {
                        swal({ text: "Attendance Marked Successfully",imageUrl: "assets/img/check.png", width: 350, showCloseButton: true })
                        $scope.getAttendance();
                    }
                    else {
                        $scope.attendance = $scope.attendance_copy;
                        swal({ text: "Attendance Not Marked",imageUrl: "assets/img/close.png", width: 350, showCloseButton: true })
                    }

                    $scope.att_list =[];
                });

               // $scope.getAttendance();
              $scope.update();
            }
        $scope.markattendnace = function (objinp) {
            debugger
            $scope.getUserAttendanceTime();            
            
                if ($scope.userStatus == 'N') {
                    if (new Date($scope.edt.attDate) < new Date(currentDate)) {
                        swal({ title: "Alert", text: "Not allowed to mark attendance for previous date", imageUrl: "assets/img/close.png" });
                        return;
                    }
                    else if (new Date($scope.edt.attDate) > new Date(currentDate)) {
                        swal({ title: "Alert", text: "Not allowed to mark attendance for future Date", imageUrl: "assets/img/close.png" });
                        return;
                    }
                    
                    if ($scope.userTimeStatus == '0') {
                        swal({ title: "Alert", text: "Not allowed to mark attendance after restricted time", imageUrl: "assets/img/close.png" });
                        return;
                    }
                }
                if ($scope.sims_attendance_code != '' && ($scope.edt.attDate != '' || $scope.edt.attDate != undefined)) {
                    objinp.sims_attendance_code = $scope.value.sims_attendance_code;
                    objinp.sims_attendance_color = $scope.value.sims_attendance_color;
                    //$http.post(ENV.apiUrl + "api/attendance/markStudentAttendance?cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&enrollNumber=" + objinp.sims_student_enroll + "&att_code=" + objinp.sims_attendance_code + "&attednacedate=" + $scope.edt.attDate).then(function (attendance) {
                    //});
                    //$scope.update();
                    var obj1 = { cur_code: $scope.edt.sims_cur_code, ayear: $scope.edt.sims_academic_year, grade: $scope.edt.sims_grade_code, section: $scope.edt.sims_section_code, enrollNumber: objinp.sims_student_enroll, att_code: objinp.sims_attendance_code, attednacedate: $scope.edt.attDate, user_name: $rootScope.globals.currentUser.username }
                    $scope.att_list.push(obj1)
                }
                else {
                    swal({ text: "Please select Attendance Code",imageUrl: "assets/img/check.png", width: 350, showCloseButton: true });
                }

        }

        $scope.markattendnace1 = function (objinp) {
            debugger
            $scope.getUserAttendanceTime();

            if ($scope.userStatus == 'N') {
                if (new Date($scope.edt.attDate) < new Date(currentDate)) {
                    swal({ title: "Alert", text: "Not allowed to mark attendance for previous date", imageUrl: "assets/img/close.png" });
                    return;
                }
                else if (new Date($scope.edt.attDate) > new Date(currentDate)) {
                    swal({ title: "Alert", text: "Not allowed to mark attendance for future Date", imageUrl: "assets/img/close.png" });
                    return;
                }

                if ($scope.userTimeStatus == '0') {
                    swal({ title: "Alert", text: "Not allowed to mark attendance after restricted time", imageUrl: "assets/img/close.png" });
                    return;
                }
            }
            if ($scope.sims_attendance_code != '' && ($scope.edt.attDate != '' || $scope.edt.attDate != undefined)) {

                if ($scope.value.sims_attendance_code == 'P') {
                    objinp.sims_attendance_code = 'A';
                    objinp.sims_attendance_color = $scope.absent_color;

                    $scope.value.sims_attendance_code='A';
                    $scope.value.sims_attendance_color = $scope.absent_color;

                }
                else {
                    objinp.sims_attendance_code = 'P';
                    objinp.sims_attendance_color = $scope.present_color;
                    $scope.value.sims_attendance_code = 'P';
                    $scope.value.sims_attendance_color = $scope.present_color;
                }
                //$http.post(ENV.apiUrl + "api/attendance/markStudentAttendance?cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&enrollNumber=" + objinp.sims_student_enroll + "&att_code=" + objinp.sims_attendance_code + "&attednacedate=" + $scope.edt.attDate).then(function (attendance) {
                //});
                //$scope.update();
                var obj1 = { cur_code: $scope.edt.sims_cur_code, ayear: $scope.edt.sims_academic_year, grade: $scope.edt.sims_grade_code, section: $scope.edt.sims_section_code, enrollNumber: objinp.sims_student_enroll, att_code: objinp.sims_attendance_code, attednacedate: $scope.edt.attDate, user_name: $rootScope.globals.currentUser.username }
                $scope.att_list.push(obj1)
            }
            else {
                swal({ text: "Please select Attendance Code", imageUrl: "assets/img/check.png", width: 350, showCloseButton: true });
            }

        }
        $scope.reset = function () {
            $scope.sims_attendance_code = '';
            $scope.value = '';
            $scope.attendance = '';
            $scope.attendance_code = '';
            $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', attDate: '' };
            $scope.totalstudents = 0;
            $scope.hidesummary = true;
            $scope.total_present = 0;
            $scope.total_absent = 0;
            $scope.total_late = 0;
            $scope.total_leave_early = 0;
            $scope.total_leave_excused = 0;
            $scope.total_absent_excused = 0;


            $scope.att_list = [];
        }
        $scope.update = function () {
            //$scope.total_present = 0;
            //$scope.total_absent = 0;
            //$scope.total_late = 0;
            //$scope.total_leave_early = 0;
            //$scope.total_leave_excused = 0;
            //$scope.total_absent_excused = 0;
            //for (var i = 0; i < $scope.attendance.length; i++) {
            //    if ($scope.attendance[i].sims_attendance_code == 'P' || $scope.attendance[i].sims_attendance_code == 'T' || $scope.attendance[i].sims_attendance_code == 'LE' || $scope.attendance[i].sims_attendance_code == 'TE') $scope.total_present += 1;
            //    if ($scope.attendance[i].sims_attendance_code == 'A' || $scope.attendance[i].sims_attendance_code == 'AE') $scope.total_absent += 1;
            //    if ($scope.attendance[i].sims_attendance_code == 'T') $scope.total_late += 1;
            //    if ($scope.attendance[i].sims_attendance_code == 'LE') $scope.total_leave_early += 1;
            //    if ($scope.attendance[i].sims_attendance_code == 'TE') $scope.total_late_excused += 1;
            //    if ($scope.attendance[i].sims_attendance_code == 'AE') $scope.total_absent_excused += 1;
            //     }


            debugger;

            $http.get(ENV.apiUrl + "api/attendance/getStudentAttendance_subdata?cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&attednacedate=" + $scope.edt.attDate).then(function (attendance_data) {
                $scope.attendance_data = attendance_data.data;
                $scope.absent=$scope.attendance_data[0].absent;
                $scope.absent_excused=$scope.attendance_data[0].absent_excused;
                $scope.late=$scope.attendance_data[0].late;
                $scope.late_excused=$scope.attendance_data[0].late_excused;
                $scope.present = $scope.attendance_data[0].present;
                $scope.leave_early = $scope.attendance_data[0].leave_early;
                
             });

   }
        //$scope.markAllAttendance = function (att_Code) {
        //    
        //    $http.get(ENV.apiUrl + "api/attendanceDaily/markStudentAttendance?cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&enrollNumber=''&att_code=" + att_Code + "&attednacedate=" + $scope.edt.attDate).then(function (attendance) {
        //        $scope.getAttendance();
        //    });

        //}
        
        $scope.markAllAttendance = function (att_Code) {
            $scope.getUserAttendanceTime();
            if ($scope.userStatus == 'N') {
                if (new Date($scope.edt.attDate) < new Date(currentDate)) {
                    swal({ text: "Not allowed to mark attendance for Previous Date", imageUrl: "assets/img/close.png" });
                    return;
                }
                if ($scope.userTimeStatus == '0') {
                    swal({ text: "Not allowed to mark attendance after restricted time", imageUrl: "assets/img/close.png" });
                    return;
                }
            }

            //$http.post(ENV.apiUrl + "api/attendanceDaily/markStudentAttendance?att_code=" + att_Code + "&attDate=" + $scope.edt.attDate + "&cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code).then(function (attendance) {
            //    $scope.getAttendance();
            //});

            for (var i = 0; i < $scope.attendance_code.length; i++) {
                if ($scope.attendance_code[i].sims_attendance_code == att_Code) {
                    $scope.att_color = $scope.attendance_code[i].sims_attendance_color;
                }
            }

            $scope.att_list = [];
            for (var i = 0; i < $scope.attendance.length; i++) {
                $scope.attendance[i].sims_attendance_code = att_Code;
                $scope.attendance[i].sims_attendance_color = $scope.att_color;

                var obj1 = { cur_code: $scope.edt.sims_cur_code, ayear: $scope.edt.sims_academic_year, grade: $scope.edt.sims_grade_code, section: $scope.edt.sims_section_code, enrollNumber: $scope.attendance[i].sims_student_enroll, att_code: att_Code, attednacedate: $scope.edt.attDate ,user_name: $rootScope.globals.currentUser.username}
                $scope.att_list.push(obj1)
            }
        }


        $scope.comment = function (sobj) {

            $scope.copy_comment = angular.copy(sobj);
            $scope.attednacedate2 = $scope.edt.attDate

            $("#comment_dailog").modal("show");

            $http.get(ENV.apiUrl + "api/attendance/get_AttendanceComment?enroll=" + $scope.copy_comment.sims_student_enroll + "&date=" + $scope.edt.attDate).then(function (res) {
                if (res.data.length > 0) {
                    $scope.copy_comment['sims_attendance_day_comment'] = res.data[0].sims_attendance_day_comment;
                }
            });

        }

        $scope.save_comment = function () {

            $http.post(ENV.apiUrl + "api/attendance/markAttendanceComment_update?enroll=" + $scope.copy_comment.sims_student_enroll + "&date=" + $scope.edt.attDate + "&comment=" + $scope.copy_comment.sims_attendance_day_comment).then(function (res) {
                $("#comment_dailog").modal("hide");
            });
        }


    }])

})();