﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Attendance');

    simsController.controller('StudentAttendanceNewCont', ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV',
        function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.hidesummary = true;

            $('*[data-datepicker="true"] input[type="text"]').datepicker(
                {
                    todayBtn: true,
                    orientation: "top left",
                    autoclose: true,
                    todayHighlight: true,
                    format: 'yyyy-mm-dd',
                    endDate: (new Date())
                });

            //$scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '', attDate: new Date().getFullYear() + '-' + (new Date().getMonth()+1) + '-' + new Date().getDate() };
            $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '', attDate: new Date().getDate() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getFullYear() };
            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            var username = $rootScope.globals.currentUser.username;
            $http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (curiculum) {
                $scope.curiculum = curiculum.data;
                $scope.edt['sims_cur_code'] = curiculum.data[0].sims_cur_code;
                $scope.cur_change();
            });
            $scope.cur_change = function () {

                //$http.get(ENV.apiUrl + "api/attendance/getAttendanceyear?cur_code=" + $scope.edt.sims_cur_code).then(function (academicyears) {
                //    $scope.academicyears = academicyears.data;
                //});

                $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllAcademicYears?cur_code=" + $scope.edt.sims_cur_code).then(function (AcademicYears) {
                    $scope.academicyears = AcademicYears.data;
                    for (var i = 0; i < $scope.academicyears.length; i++) {
                        if ($scope.academicyears[i].sims_academic_year_status == 'C') {
                            //   $scope.edt['sims_cur_code'] = cur_values;
                            $scope.edt['sims_academic_year'] = $scope.academicyears[i].sims_academic_year;
                        }
                    }
                    $scope.acdm_yr_change();
                });

                $http.get(ENV.apiUrl + "api/attendance/getAttendanceCode?cur_code=" + $scope.edt.sims_cur_code).then(function (attendance_code) {
                    $scope.attendance_code = attendance_code.data;
                    //$scope.rdbtn = $scope.attendance_code[0].sims_attendance_code;
                });
            }
            $scope.acdm_yr_change = function () {
                $http.get(ENV.apiUrl + "api/attendance/getAttendancegrade?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&userName=" + username).then(function (grades) {
                    
                    $scope.grades = grades.data;
                });
            }
            $scope.grade_change = function () {

                $http.get(ENV.apiUrl + "api/attendance/getAttendancesection?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&gradeCode=" + $scope.edt.sims_grade_code + "&userName=" + username).then(function (section) {
                    $scope.section = section.data;
                });

            }
            //
            $scope.img_url = 'http://api.mograsys.com/APIERP/Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/';
            $scope.getAttendance = function () {

                if ($scope.edt.attDate == '') {
                    swal({ text: "Please select Attendance date.",imageUrl: "assets/img/notification-alert.png", width: 400, showCloseButton: true });

                }
                else {
                    $http.get(ENV.apiUrl + "api/attendance/getStudentAttendanceNew?cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&attednacedate=" + $scope.edt.attDate).then(function (attendance) {
                        if (attendance.data.length <= 0) {
                            swal({ text: "No Attendance Data is found for this date.",imageUrl: "assets/img/close.png", width: 400, showCloseButton: true });
                        }
                        else {
                            $scope.attendance = attendance.data;
                            $scope.operation = true;
                            $scope.totalstudents = $scope.attendance.length;
                            $scope.hidesummary = false;
                        }
                        $scope.update();
                    });
                }
            }
            $scope.sims_attendance_code = '';
            $scope.value = '';
            $scope.attcode = function (info) {
                $scope.value = info;
                $scope.sims_attendance_code = $scope.value.sims_attendance_code;
            }
            $scope.markattendnace = function (objinp) {
                if ($scope.sims_attendance_code != '' && ($scope.edt.attDate != '' || $scope.edt.attDate != undefined)) {
                    objinp.sims_attendance_code = $scope.value.sims_attendance_code;
                    objinp.sims_attendance_color = $scope.value.sims_attendance_color;
                    $http.post(ENV.apiUrl + "api/attendance/markStudentAttendanceNew?cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&enrollNumber=" + objinp.sims_student_enroll + "&att_code=" + objinp.sims_attendance_code + "&attednacedate=" + $scope.edt.attDate).then(function (attendance) {
                        $scope.getAttendance();
                    });
                    $scope.update();
                }
                else {
                    swal({ text: "Please select Attendance Code",imageUrl: "assets/img/notification-alert.png", width: 350, showCloseButton: true });
                }
            }
            $scope.reset = function () {
                $scope.sims_attendance_code = '';
                $scope.value = '';
                $scope.attendance = '';
                $scope.attendance_code = '';
                $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', attDate: '' };
                $scope.totalstudents = 0;
                $scope.hidesummary = true;
                $scope.total_present = 0;
                $scope.total_absent = 0;
            }
            $scope.update = function () {
                $scope.total_present = 0;
                $scope.total_absent = 0;
                for (var i = 0; i < $scope.attendance.length; i++) {
                    if ($scope.attendance[i].sims_attendance_code == 'P') $scope.total_present += 1;
                    if ($scope.attendance[i].sims_attendance_code == 'A') $scope.total_absent += 1;

                }
            }
            //$scope.markAllAttendance = function (att_Code) {
            //    
            //    $http.get(ENV.apiUrl + "api/attendanceDaily/markStudentAttendance?cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&enrollNumber=''&att_code=" + att_Code + "&attednacedate=" + $scope.edt.attDate).then(function (attendance) {
            //        $scope.getAttendance();
            //    });

            //}
            $scope.markAllAttendance = function (att_Code) {


                $http.post(ENV.apiUrl + "api/attendance/markStudentAttendanceALLNew?cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&enrollNumber=" + '' + "&att_code=" + att_Code + "&attednacedate=" + $scope.edt.attDate).then(function (attendance) {

                    //$http.post(ENV.apiUrl + "api/attendance/markStudentAttendanceALLNew?att_code=" + att_Code + "&attDate=" + $scope.edt.attDate + "&cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code).then(function (attendance) {
                    $scope.getAttendance();
                });

            }


        }])

})();