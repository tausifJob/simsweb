﻿/// <reference path="CancelAdmFromTcCont.js" />

(function () {
    'use strict';
    var simsController = angular.module('sims.module.Attendance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });
    simsController.controller('FireRegisterRptCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.temp = {};
            $scope.detailsField = true;
            $scope.hideprintpdf = false;
            $scope.reportdataparameter=[];


            $scope.temp = {};
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            var yyyy = today.getFullYear();
            $scope.mom_start_date = '';
            $scope.mom_end_date = '';

            $(function () {
                $('#bell_box').multipleSelect({ width: '100%' });
                $('#teacher_box').multipleSelect({ width: '100%', filter: true });

            });

            var today = new Date();
            var dd = today.getDate();
            if (dd < 10) {
                dd = '0' + dd;
            }
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.sdate = dd + '-' + mm + '-' + yyyy;
            $scope.edate = dd + '-' + mm + '-' + yyyy;
            $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
            $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;

            $(function () {
                $("#chkmarks").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_start_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });
            if ($scope.mom_start_date_check == true) {
                $scope.mom_start_date = '';
            }
            else {
                $scope.mom_start_date = dd + '-' + mm + '-' + yyyy;
            }

            $(function () {
                $("#chkmarks1").click(function () {
                    if ($(this).is(":checked")) {
                        $("#fdate").attr("disabled", "disabled");
                        $scope.mom_end_date = '';

                    } else {

                        $("#fdate").removeAttr("disabled");
                        $("#fdate").focus();
                        $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
                    }
                });
            });
            if ($scope.mom_end_date_check == true) {
                $scope.mom_end_date = '';
            }
            else {
                $scope.mom_end_date = dd + '-' + mm + '-' + yyyy;
            }










            $scope.pagesize = '30';
            $scope.pageindex = "0";
            $scope.pager = true;

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }
            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; console.log("currentPage=" + $scope.currentPage);
                $scope.makeTodos();
                $scope.chk = {}
                $scope.chk['chk_all'] = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 30, $scope.maxSize = 100;

            $scope.makeTodos = function () {
                debugger;
                if ($scope.pagesize == 'All') {
                    $scope.currentPage = 1;
                    $scope.showPager = false;
                    $scope.filteredTodos = $scope.report_data_new;
                }
                else {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }
                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                    console.log("begin=" + begin); console.log("end=" + end);

                    $scope.filteredTodos = $scope.todos.slice(begin, end);
                }
            };


            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res1) {
                $scope.cur_data = res1.data;
                $scope.temp.sims_cur_code = $scope.cur_data[0].sims_cur_code;
                $scope.getAcademic_year($scope.cur_data[0].sims_cur_code);
            });


            $http.get(ENV.apiUrl + "api/promotestudlist/getcanceladmissionsummaryView").then(function (res1) {
                debugger;
                $scope.view_mode = res1.data;
                $scope.temp.comn_appl_parameter = $scope.view_mode[0].comn_appl_parameter;

            });

            $scope.getAcademic_year = function (cur_code1) {
                debugger
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + cur_code1).then(function (res1) {
                    $scope.acad_data = res1.data;
                    $scope.temp.sims_academic_year = $scope.acad_data[0].sims_academic_year;
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                    //  $scope.getSection($scope.temp.sims_academic_year, $scope.temp.grade_code, $scope.temp.sims_cur_code);
                });
            }

            $scope.getGrade = function (cur_code, academic_year) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                    setTimeout(function () {
                        $('#cmb_grade_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);



                });
                $scope.getSection($scope.temp.sims_cur_code, $scope.temp.sims_academic_year, $scope.temp.grade_code);
            }


            $scope.sims_attendance_day_attendance_code_flag;
            $scope.getdetails = function () {

                debugger;
                $http.get(ENV.apiUrl + "api/HolidayAttendance/getFireRegisterReport?sims_cur_code=" + $scope.temp.sims_cur_code + "&sims_acad_year=" + $scope.temp.sims_academic_year + "&sims_grade_code=" + $scope.temp.sims_grade_code + "&sims_section_code=" + $scope.temp.sims_section_code + "&date=" + $scope.mom_start_date).then(function (res1) {
                    if (res1.data.length > 0) {
                    $scope.report_data = res1.data;
                    $scope.report_data = res1.data;
                    $scope.totalItems = $scope.report_data.length;
                    $scope.todos = $scope.report_data;
                    for (var i = 0; i < $scope.report_data.length; i++) {
                        if ($scope.report_data[i].sims_attendance_day_attendance_code == 'P') {
                            $scope.report_data[i].sims_flag = true;
                        }
                        else {
                            $scope.report_data[i].sims_flag = false;
                        }
                    }
                    console.log("$scope.report_data", $scope.report_data);

                    $scope.makeTodos();
                    }
                    else {
                        swal({ title: "Alert", text: "Data is not Available", showCloseButton: true, width: 300, height: 200 });
                        $scope.report_data = [];

                    }

                });
            }



            $scope.getSection = function (cur_code, academic_year, grade_code) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + $scope.temp.sims_cur_code + "&grade_code=" + $scope.temp.sims_grade_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }




            $scope.getCircdetail = function () {
                debugger;
                $scope.report_data_new = [];
                $scope.detailsField = true;
                //string cur_code, string acad_year, string circular_status, string user_group,string from,string to,string search,string report_view)
                $http.get(ENV.apiUrl + "api/CircularStatusByParent/GetCircularuserstatus?cur_code=" + $scope.temp.sims_cur_code + "&acad_year=" + $scope.temp.sims_academic_year + "&circular_status=" + $scope.temp.sims_status + "&user_group=" +
                    $scope.temp.user_group_code + "&from=" + $scope.from_date + "&to=" + $scope.to_date + "&search=" + $scope.searchParent + "&report_view=" + $scope.temp.show_expire_circular).then(function (res1) {
                        if (res1.data.length > 0) {
                            $scope.report_data_new = res1.data;
                            $scope.totalItems = $scope.report_data_new.length;
                            $scope.todos = $scope.report_data_new;
                            $scope.makeTodos();
                        }
                        else {
                            $scope.filteredTodos = [];
                            swal({ title: "Alert", text: " Sorry !!!!Data is not Available", showCloseButton: true, width: 300, height: 200 });
                            $scope.report_data_new = [];
                        }

                    });
            }


            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };
            $scope.search = function () {

                $scope.todos = $scope.searched($scope.studlist, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.report_data_new;
                }
                $scope.makeTodos();
            }



            $scope.colname = null;
            $scope.reverse = false;

            $scope.sort = function (col) {
                $scope.colname = col;
                $scope.reverse = ($scope.colname === col) ? !$scope.reverse : false;
            }

            $scope.colsvis = false;


            $scope.exportData = function () {
                debugger;
                $scope.colsvis = true;
                swal({
                    title: "Alert",
                    text: "Do you want to Export in MS-Excel?",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',
                }).then(function (isConfirm) {
                    if (isConfirm) {
                        var blob = new Blob([document.getElementById('pdf_print').innerHTML],
                         {
                             type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                         });
                        $scope.detailsField = false;
                        saveAs(blob, "SubstituteLectureDetailsReport.xls");
                        $scope.colsvis = false;
                        $scope.getsubdetail();

                    }

                });
                $scope.colsvis = true;

            };


            //$scope.print = function () {
            //    debugger;
            //    $scope.colsvis = true;
            //    swal({
            //        title: "Alert",
            //        text: "Do you want to Print?",
            //        showCloseButton: true,
            //        showCancelButton: true,
            //        confirmButtonText: 'Yes',
            //        width: 380,
            //        cancelButtonText: 'No',
            //    }).then(function (isConfirm) {
            //        if (isConfirm) {
            //            var docHead = document.head.outerHTML;
            //            var printContents = document.getElementById('pdf_print').outerHTML;
            //            var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=800, height=600, resizable=yes, screenX=250, screenY=10, personalbar=no, scrollbars=yes";
            //            var newWin = window.open("", "_blank", winAttr);
            //            var writeDoc = newWin.document;
            //            writeDoc.open();
            //            writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            //            writeDoc.close();
            //            newWin.focus();
            //            $scope.detailsField = false;
            //            $scope.colsvis = false;
            //            $scope.getsubdetail();

            //        }
            //    });
            //};


           $scope.report_show = true;
            $scope.Report = function () {
                debugger;
                //$('#feemodal').modal({ backdrop: 'static', keyboard: false });
                $('#feemodal').modal('show');
                //$scope.newDate = $filter('date')($scope.mom_start_date, 'yyyy-MM-dd');
                $scope.newDate = moment($scope.mom_start_date, "DD-MM-YYYY").format('YYYY-MM-DD');

                $scope.report_show = false;
                //$scope.Report_loc = '';
                //if ($scope.report_status == 'L') {
                //    $scope.Report_loc = 'Invs.INVR02rakaag1'
                //} else if ($scope.report_status == 'LE' || $scope.report_status == 'LX') {
                //    $scope.Report_loc = 'Invs.INVR02rakaag'
                //}
                //  $http.get(ENV.apiUrl + "api/StudentFee/GetReceiptCon").then(function (res) {
                //var rname = res.data;
                var data = {
                    location: 'Sims.FRR01New',
                    
                    parameter: {
                    cur_code:$scope.temp.sims_cur_code,
                    acad_year:$scope.temp.sims_academic_year,
                    grade_code:$scope.temp.sims_grade_code,
                    section_code: $scope.temp.sims_section_code,
                    date: $scope.newDate,


                    },
                    state: 'main.FRR01',
                    ready: function () {
                        this.refreshReport();
                    },
                }


                console.log(data);
                window.localStorage["ReportDetails"] = JSON.stringify(data)

                // });

                //For Report Tool////////////////////////////////////////////////////////////////////////////////


                try {
                    $scope.rpt = JSON.parse(window.localStorage["ReportDetails"]);

                    $scope.location = $scope.rpt.location;
                    $scope.parameter = $scope.rpt.parameter;
                    $scope.state = $scope.rpt.state;
                }
                catch (ex) {
                }
                var s;

                //if ($http.defaults.headers.common['schoolId'] == 'dpsmis')
                //    s = "SimsReports." + $scope.location + ",SimsReports";


                //else
                //    s = "SimsReports." + $scope.location + ",SimsReports" + $http.defaults.headers.common['schoolId'];


                //  window.localStorage["Finn_comp"] = JSON.stringify(data)

                s = "SimsReports." + $scope.location + ",SimsReports";

                var url = window.location.href;
                var domain = url.substring(0, url.indexOf(':'))
                if ($http.defaults.headers.common['schoolId'] == 'sms') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.obacenter.ae/report/api/reports/';

                }
                else if ($http.defaults.headers.common['schoolId'] == 'svcc' || $http.defaults.headers.common['schoolId'] == 'svvm' || $http.defaults.headers.common['schoolId'] == 'svvmc' || $http.defaults.headers.common['schoolId'] == 'hedgewar' || $http.defaults.headers.common['schoolId'] == 'mptesdvs' || $http.defaults.headers.common['schoolId'] == 'ctbc' || $http.defaults.headers.common['schoolId'] == 'dvhs' || $http.defaults.headers.common['schoolId'] == 'dvhss' || $http.defaults.headers.common['schoolId'] == 'dvps' || $http.defaults.headers.common['schoolId'] == 'vision' || $http.defaults.headers.common['schoolId'] == 'dmc') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.appsis.co.in/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'asdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitydubai.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'portal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.rakaag.sch.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'elcportal' || $http.defaults.headers.common['schoolId'] == 'lwgportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amityelc.ae/report/api/reports/';
                }

                else if ($http.defaults.headers.common['schoolId'] == 'apsportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.amitysharjah.ae/report/api/reports/';
                }
                else if ($http.defaults.headers.common['schoolId'] == 'csdportal') {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.sigeducation.com/reportsss/api/reports/';
                }

                else {
                    if (domain == 'https')
                        var service_url = 'https://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                    else
                        var service_url = 'http://' + $http.defaults.headers.common['schoolId'] + '.mograsys.com/report/api/reports/';
                }
                console.log(service_url);


                $scope.parameters = {}
                debugger;
                $("#reportViewer1")
                               .telerik_ReportViewer({
                                   //serviceUrl: ENV.apiUrl + "api/reports/",
                                   serviceUrl: service_url,

                                   viewMode: telerikReportViewer.ViewModes.INTERACTIVE,
                                   scaleMode: telerikReportViewer.ScaleModes.SPECIFIC,
                                   // Zoom in and out the report using the scale
                                   // 1.0 is equal to 100%, i.e. the original size of the report
                                   scale: 1.0,
                                   ready: function () {
                                       //this.refreshReport();
                                   }

                               });





                var reportViewer = $("#reportViewer1").data("telerik_ReportViewer");
                reportViewer.reportSource({
                report: s,
                parameters: $scope.parameter,
                });

                //var rv = $("#reportViewer1").data("telerik_ReportViewer");
                //rv.commands.print.exec();

                setInterval(function () {

                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });

                    //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });

                }, 1000);


                $timeout(function () {
                    $('#k-item k-state-default k-state-disabled').css({ 'display': 'block' });
                    $('.trv-report-viewer input[type=number]').css({ 'min-height': '0px' });
                    //$('.k-group  li:nth-child(2)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(3)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(4)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(5)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(6)').css({ 'display': 'none' });
                    //$('.k-group  li:nth-child(7)').css({ 'display': 'none' });


                }, 100)


                // });
            }

            $scope.reset_data = function () {
                debugger;
                // $scope.temp.sims_grade_code = '';
                $scope.filteredTodos = [];


                try {
                    $('#cmb_grade_code').multipleSelect('uncheckAll');
                } catch (e) {

                }

                try {
                    $('#cmb_section_code').multipleSelect('uncheckAll');
                } catch (e) {

                }
                $scope.temp.sims_bell_code = '';
                $scope.report_data_new = [];


            }

        


        }])

})();

