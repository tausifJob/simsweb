﻿(function () {
    'use strict';
    var attendancecode = [];
    var curcode = [];
    var main;
    var simsController = angular.module('sims.module.Attendance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('EmployeeAttendanceCodeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            $scope.pagesize = '10';
            //$scope.operation = true;
            //$scope.table1 = true;
            $scope.editmode = false;
            var data1 = [];
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });



            $scope.countData = [
                   { val: 10, data: 10 },
                   { val: 20, data: 20 },
                   //{ val: 15, data: 15 },

            ]

            $scope.size = function (str) {

                if (str == 10 || str == 20) {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }


                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; $scope.makeTodos();

            }

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });

            }, 100);

            $http.get(ENV.apiUrl + "api/common/LeaveType/Get_leave_codes").then(function (res) {
                $scope.leaveCode_data = res.data;
                console.log($scope.leaveCode_data);
            });

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str; $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }
            $scope.edt = "";
            $scope.table1 = true;

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/EmployeeAttendanceCode/getEmpAttendanceCode").then(function (get_AttendanceCode) {
                    $scope.AttendanceCode_Data = get_AttendanceCode.data;                    
                    //$scope.table1 = true;
                    if ($scope.AttendanceCode_Data.length > 0) {
                        //$scope.table1 = true;
                        $scope.pager = true;
                        if ($scope.countData.length > 3) {
                            $scope.countData.splice(3, 1);
                            $scope.countData.push({ val: $scope.AttendanceCode_Data.length, data: 'All' })
                        }
                        else {
                            $scope.countData.push({ val: $scope.AttendanceCode_Data.length, data: 'All' })
                        }
                        $scope.totalItems = $scope.AttendanceCode_Data.length;
                        $scope.todos = $scope.AttendanceCode_Data;
                        $scope.makeTodos();

                    }
                    else {
                       //$scope.table1 = false;
                        swal({ text: "Record Not Found",imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }

                })
            }

            $scope.getgrid();
            //$http.get(ENV.apiUrl + "api/EmployeeAttendanceCode/getAttendanceCode").then(function (get_AttendanceCode) {

            //    $scope.AttendanceCode_Data = get_AttendanceCode.data;
            //    if ($scope.AttendanceCode_Data.length > 0) {
            //        $scope.table1 = true;
            //        $scope.pager = true;
            //        if ($scope.countData.length > 3) {
            //            $scope.countData.splice(3, 1);
            //            $scope.countData.push({ val: $scope.AttendanceCode_Data.length, data: 'All' })
            //        }
            //        else {
            //            $scope.countData.push({ val: $scope.AttendanceCode_Data.length, data: 'All' })
            //        }
            //        $scope.totalItems = $scope.AttendanceCode_Data.length;
            //        $scope.todos = $scope.AttendanceCode_Data;
            //        $scope.makeTodos();

            //    }
            //    else {
            //        $scope.table1 = false;
            //        swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
            //    }

            //})

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (res) {
                $scope.curriculum = res.data;
            })

            $scope.clear = function () {
                $scope.edt = undefined;
            }

            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    $scope.newmode = true;
                    $scope.readonlyAttendanceCode = false;
                    $scope.check = true;
                    $scope.edt = '';
                    $scope.editmode = false;
                    $scope.opr = 'S';
                    $scope.readonly = false;
                    $scope.table1 = false;
                    $scope.operation = true;
                    $scope.savebtn = true;
                    $scope.updatebtn = false;
                    $scope.edt = {};
                    $scope.edt.pays_attendance_status = true;
                    var dt = new Date();
                    //$scope.edt.pays_attendance_status_inactive_date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                    $scope.myForm.$setPristine();
                    $scope.myForm.$setUntouched();
                    $scope.temp = '';
                }
            }

            $scope.up = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    $scope.opr = 'U';
                    $scope.readonlyAttendanceCode = true;
                    $scope.editmode = true;
                    $scope.newmode = false;
                    $scope.savebtn = false;
                    $scope.updatebtn = true;
                    $scope.readonly = true;
                    $scope.table1 = false;
                    $scope.operation = true;
                    //$scope.edt = str;

                    $scope.edt = {
                        pays_attendance_code: str.pays_attendance_code,
                        pays_attendance_code_numeric: str.pays_attendance_code_numeric,
                        pays_attendance_short_desc: str.pays_attendance_short_desc,
                        pays_attendance_description: str.pays_attendance_description,
                        pays_attendance_present_value: str.pays_attendance_present_value,
                        pays_attendance_absent_value: str.pays_attendance_absent_value,
                        pays_attendance_tardy_value: str.pays_attendance_tardy_value,
                        pays_attendance_type_value: str.pays_attendance_type_value,
                        pays_attendance_color_code: str.pays_attendance_color_code,
                        pays_attendance_status_inactive_date: str.pays_attendance_status_inactive_date,
                        pays_attendance_status: str.pays_attendance_status,
                        pays_attendance_unexcused_flag: str.pays_attendance_unexcused_flag,
                        pays_attendance_teacher_can_use_flag: str.pays_attendance_teacher_can_use_flag,
                        pays_attendance_leave_code: str.pays_attendance_leave_code
                    }

                }
            }

            $scope.cancel = function () {
                $scope.table1 = true;
                $scope.operation = false;
                $scope.edt = "";
                $scope.myForm.$setPristine();
                $scope.myForm.$setUntouched();
            }


            $scope.Save = function (myForm) {
              
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'I';
                    $scope.exist = false;
                    for (var i = 0; i < $scope.AttendanceCode_Data.length; i++) {
                        if ($scope.AttendanceCode_Data[i].pays_attendance_code == $scope.edt.pays_attendance_code) {
                            $scope.exist = true;
                        }
                    }
                    if ($scope.exist) {
                        swal({ text:"Attendance Code Already exists",imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }
                    else {

                        data1.push(data);
                        
                        $http.post(ENV.apiUrl + "api/EmployeeAttendanceCode/CUDEmpAttendanceCode", data1).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Added Successfully",imageUrl: "assets/img/check.png", width: 320, height: 200 });
                            }
                            else if ($scope.msg1 == false) {
                                swal({  text: "Record Not Added. ", imageUrl: "assets/img/close.png",width: 320, height: 200 });
                            }
                            else {

                                swal("Error-" + $scope.msg1)
                            }
                            $scope.operation = false;
                            $scope.getgrid();
                        });

                        $scope.table1 = true;
                        $scope.operation = false;
                   }
                }
            }

            $scope.Update = function (myForm) {
               
                if (myForm) {
                    var data = $scope.edt;
                    data.opr = 'U';
                    data1.push(data);
                    $http.post(ENV.apiUrl + "api/EmployeeAttendanceCode/CUDEmpAttendanceCode", data1).then(function (msg) {
                        $scope.msg1 = msg.data;
                        $scope.operation = false;
                        $http.get(ENV.apiUrl + "api/EmployeeAttendanceCode/getEmpAttendanceCode").then(function (get_AttendanceCode) {
                            $scope.AttendanceCode_Data = get_AttendanceCode.data;
                            $scope.totalItems = $scope.AttendanceCode_Data.length;
                            $scope.todos = $scope.AttendanceCode_Data;
                            $scope.makeTodos();

                            if ($scope.msg1 == true) {
                                swal({ text: "Record Updated Successfully",imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            }
                            else if($scope.msg1 == false) {
                                swal({ text: "Record Not Updated. " , imageUrl: "assets/img/close.png",width: 300, height: 200 });
                            }
                        else{
                                swal("Error-" + $scope.msg1)
                            }
                        });

                    })
                    $scope.operation = false;
                    $scope.table1 = true;
                }
            }
            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;
            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }
                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('test-' + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('test-' + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }

            }

            var deletecode = [];

            $scope.deleterecord = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    var data1 = [];
                    $scope.flag = false;
                    var deleteleave = [];
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('test-' + i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodulecode = ({
                                'pays_attendance_code': $scope.filteredTodos[i].pays_attendance_code,
                                opr: 'D'
                            });
                            deleteleave.push(deletemodulecode);
                        }
                    }
                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {

                                $http.post(ENV.apiUrl + "api/EmployeeAttendanceCode/CUDEmpAttendanceCode", deleteleave).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/EmployeeAttendanceCode/getEmpAttendanceCode").then(function (getAttendance_data) {
                                                    $scope.getAttendancedata = getAttendance_data.data;
                                                    $scope.totalItems = $scope.getAttendancedata.length;
                                                    $scope.todos = $scope.getAttendancedata;
                                                    $scope.makeTodos();
                                                });
                                            }
                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        //if(){}
                                        swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {
                                                $http.get(ENV.apiUrl + "api/EmployeeAttendanceCode/getEmpAttendanceCode").then(function (getAttendance_data) {
                                                    $scope.getAttendancedata = getAttendance_data.data;
                                                    $scope.totalItems = $scope.getAttendancedata.length;
                                                    $scope.todos = $scope.getAttendancedata;
                                                    $scope.makeTodos();
                                                });
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }
                                });
                            }
                            else {
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById('test-' + i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        $scope.row1 = '';
                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    data1 = [];
                }
            }

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.AttendanceCode_Data, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.AttendanceCode_Data;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (
                       item.pays_attendance_description.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                       item.pays_attendance_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||                       
                       item.pays_attendance_code_numeric.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                       item.pays_attendance_short_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                       item.pays_attendance_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.pays_attendance_code == toSearch) ? true : false;
            }


            $scope.nullcheck = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var t = $scope.filteredTodos[i].sims_category_code;
                        var v = document.getElementById(t + i);
                        v.checked = false;
                        $scope.row1 = '';
                    }
                }
                main.checked = false;
            }

            $scope.setTeacherFlag = function () {
                if ($scope.edt.pays_attendance_leave_code == undefined || $scope.edt.pays_attendance_leave_code == "" || $scope.edt.pays_attendance_leave_code == null) {
                    $scope.edt.pays_attendance_teacher_can_use_flag = false;
                }
                else {
                    $scope.edt.pays_attendance_teacher_can_use_flag = true;
                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({

                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

        }]
        )
})();