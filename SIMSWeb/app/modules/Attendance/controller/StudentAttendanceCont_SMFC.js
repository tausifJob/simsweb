﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Attendance');

    simsController.directive('ngRightClick', function ($parse) {
        return function (scope, element, attrs) {
            var fn = $parse(attrs.ngRightClick);
            element.bind('contextmenu', function (event) {
                scope.$apply(function () {
                    event.preventDefault();
                    fn(scope, { $event: event });
                });
            });
        };
    });

    simsController.controller('StudentAttendanceCont_SMFC', ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$filter',
    function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $filter) {

        $scope.hidesummary = true;

        $('*[data-datepicker="true"] input[type="text"]').datepicker(
            {
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd',
                endDate: (new Date())
            });

        //$scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '', attDate: new Date().getFullYear() + '-' + (new Date().getMonth()+1) + '-' + new Date().getDate() };

        if ($http.defaults.headers.common['schoolId'] == 'clarionschool') {
            $scope.show_mobile = true;
        }

        console.log(moment(new Date(), "DD-MM-YYYY"), 'date');
        $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', sims_enroll_number: '', attDate: moment(new Date()).format('DD-MM-YYYY') };
        $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
            $('input[type="text"]', $(this).parent()).focus();
        });


        var username = $rootScope.globals.currentUser.username;
        //$http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (curiculum) {
        //    $scope.curiculum = curiculum.data;
        //    $scope.edt['sims_cur_code'] = curiculum.data[0].sims_cur_code;
        //    $scope.cur_change();
        //});

        $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum?username=" + $rootScope.globals.currentUser.username).then(function (res) {
            debugger;
            $scope.curiculum = res.data;
            $scope.edt['sims_cur_code'] = $scope.curiculum[0].sims_cur_code;
            $scope.cur_change();
        });

        //function getCur(flag, comp_code) {
        //    if (flag) {

        //        $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
        //            $scope.curiculum = res.data;
        //            $scope.edt['sims_cur_code'] = $scope.curiculum[0].sims_cur_code;
        //            $scope.cur_change();

        //        });
        //    }
        //    else {

        //        $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
        //            $scope.curiculum = res.data;
        //            $scope.edt['sims_cur_code'] = $scope.curiculum[0].sims_cur_code;
        //            $scope.cur_change();
        //        });


        //    }

        //}

        //$http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
        //    $scope.global_count_comp = res.data;

        //    if ($scope.global_count_comp) {
        //        getCur(true, $scope.user_details.comp);


        //    }
        //    else {
        //        getCur(false, $scope.user_details.comp)
        //    }
        //});




        $scope.cur_change = function () {

            //$http.get(ENV.apiUrl + "api/attendance/getAttendanceyear?cur_code=" + $scope.edt.sims_cur_code).then(function (academicyears) {
            //    $scope.academicyears = academicyears.data;
            //});

            $http.get(ENV.apiUrl + "api/Fee/SFS/GetAllAcademicYears?cur_code=" + $scope.edt.sims_cur_code).then(function (AcademicYears) {
                $scope.academicyears = AcademicYears.data;
                for (var i = 0; i < $scope.academicyears.length; i++) {
                    if ($scope.academicyears[i].sims_academic_year_status == 'C' || $scope.academicyears[i].sims_academic_year_status == 'Current') {
                        //   $scope.edt['sims_cur_code'] = cur_values;
                        $scope.edt['sims_academic_year'] = $scope.academicyears[i].sims_academic_year;
                    }
                }
                $scope.acdm_yr_change();
            });

            $http.get(ENV.apiUrl + "api/attendance/getAttendanceCode?cur_code=" + $scope.edt.sims_cur_code).then(function (attendance_code) {
                $scope.attendance_code = attendance_code.data;
                //$scope.rdbtn = $scope.attendance_code[0].sims_attendance_code;
            });
        }
        $scope.acdm_yr_change = function () {
            $http.get(ENV.apiUrl + "api/attendance/getAttendancegrade?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&userName=" + username).then(function (grades) {

                $scope.grades = grades.data;
            });
        }
        $scope.grade_change = function () {

            $http.get(ENV.apiUrl + "api/attendance/getAttendancesection?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&gradeCode=" + $scope.edt.sims_grade_code + "&userName=" + username).then(function (section) {
                $scope.section = section.data;
            });

        }


        //Gender Data
        debugger;
        $http.get(ENV.apiUrl + "api/attendance/getAttendanceGender").then(function (gender_data) {
            $scope.gender_data = gender_data.data;
            debugger;
            setTimeout(function () {
                $('#gender').change(function () {
                    console.log($(this).val());
                }).multipleSelect({
                    width: '100%',
                });
                $("#gender").multipleSelect("checkAll");
            }, 1000);


        });

        

        //
        $scope.img_url = 'http://api.mograsys.com/kindoapi/Content/' + $http.defaults.headers.common['schoolId'] + '/Images/StudentImages/';
        $scope.getAttendance = function () {
            debugger;
            $scope.searchbox = true;
            if ($scope.edt.attDate == '') {
                swal({ text: "Please select Attendance date.", imageUrl: "assets/img/notification-alert.png", width: 400, showCloseButton: true });

            }
            else {
                $http.get(ENV.apiUrl + "api/attendance/getStudentAttendance_SMFC?cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&attednacedate=" + $scope.edt.attDate + "&gender=" + $scope.temp.sims_student_gender).then(function (attendance) {
                    if (attendance.data.length <= 0) {
                        swal({ text: "No Attendance Data is found for this date.", imageUrl: "assets/img/close.png", width: 400, showCloseButton: true });
                        $scope.attendance = [];

                    }
                    else {
                        if (attendance.data[0].sims_isholiday == true) {
                            swal({ text: "Attendance can not be marked on Weekends And Holidays.", imageUrl: "assets/img/close.png", width: 400, showCloseButton: true });
                            $scope.attendance = [];
                        }
                        else {

                            $scope.attendance = attendance.data;
                            $scope.attendance_copy = angular.copy($scope.attendance);

                            $scope.operation = true;
                            $scope.totalstudents = $scope.attendance.length;
                            $scope.hidesummary = false;
                        }
                    }
                    $scope.update();
                });
            }
        }
        $scope.sims_attendance_code = '';
        $scope.value = '';
        $scope.attcode = function (info) {
            debugger;
            $scope.mnuCard = info;
            $scope.value = info;
            $scope.sims_attendance_code = $scope.value.sims_attendance_code;
        }
        $scope.att_list = [];
        $scope.submit = function () {
            $http.post(ENV.apiUrl + "api/attendance/markStudentAttendanceList", $scope.att_list).then(function (attendance) {
                if (attendance.data) {
                    swal({ text: "Attendance Marked Successfully", imageUrl: "assets/img/check.png", width: 350, showCloseButton: true })
                    $scope.getAttendance();
                }
                else {
                    $scope.attendance = $scope.attendance_copy;
                    swal({ text: "Attendance Not Marked", imageUrl: "assets/img/close.png", width: 350, showCloseButton: true })
                }

                $scope.att_list = [];
            });

            // $scope.getAttendance();
            $scope.update();
        }
        $scope.markattendnace = function (objinp) {
            if ($scope.sims_attendance_code != '' && ($scope.edt.attDate != '' || $scope.edt.attDate != undefined)) {
                if ($scope.sims_attendance_code != 'W' && $scope.sims_attendance_code != 'H') {
                    objinp.sims_attendance_code = $scope.value.sims_attendance_code;
                    objinp.sims_attendance_color = $scope.value.sims_attendance_color;
                    //$http.post(ENV.apiUrl + "api/attendance/markStudentAttendance?cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&enrollNumber=" + objinp.sims_student_enroll + "&att_code=" + objinp.sims_attendance_code + "&attednacedate=" + $scope.edt.attDate).then(function (attendance) {
                    //});
                    //$scope.update();
                    var obj1 = { cur_code: $scope.edt.sims_cur_code, ayear: $scope.edt.sims_academic_year, grade: $scope.edt.sims_grade_code, section: $scope.edt.sims_section_code, enrollNumber: objinp.sims_student_enroll, att_code: objinp.sims_attendance_code, attednacedate: $scope.edt.attDate, user_name: $rootScope.globals.currentUser.username }
                    $scope.att_list.push(obj1)
                }
                else { swal({ text: "Attendance Code Not Marked", imageUrl: "assets/img/close.png", width: 350, showCloseButton: true }); }


            }
            else {
                swal({ text: "Please select Attendance Code", imageUrl: "assets/img/notification-alert.png", width: 350, showCloseButton: true });
            }
        }
        $scope.reset = function () {
            $scope.sims_attendance_code = '';
            $scope.value = '';
            $scope.attendance = '';
            $scope.attendance_code = '';
            $scope.edt = { sims_cur_code: '', sims_academic_year: '', sims_grade_code: '', sims_section_code: '', attDate: '' };
            $scope.totalstudents = 0;
            $scope.hidesummary = true;
            $scope.total_present = 0;
            $scope.total_absent = 0;
            $scope.total_late = 0;
            $scope.att_list = [];


        }
        $scope.update = function () {
            debugger;
            $http.get(ENV.apiUrl + "api/attendance/getStudentAttendance_subdata?cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&attednacedate=" + $scope.edt.attDate).then(function (attendance_data) {
                $scope.attendance_data = attendance_data.data;
                $scope.absent = $scope.attendance_data[0].absent;
                $scope.absent_excused = $scope.attendance_data[0].absent_excused;
                $scope.late = $scope.attendance_data[0].late;
                $scope.late_excused = $scope.attendance_data[0].late_excused;
                $scope.present = $scope.attendance_data[0].present;
                $scope.leave_early = $scope.attendance_data[0].leave_early;

            });



        }
        //$scope.markAllAttendance = function (att_Code) {
        //    
        //    $http.get(ENV.apiUrl + "api/attendanceDaily/markStudentAttendance?cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code + "&enrollNumber=''&att_code=" + att_Code + "&attednacedate=" + $scope.edt.attDate).then(function (attendance) {
        //        $scope.getAttendance();
        //    });

        //}
        $scope.markAllAttendance = function (att_Code) {
            //$http.post(ENV.apiUrl + "api/attendanceDaily/markStudentAttendance?att_code=" + att_Code + "&attDate=" + $scope.edt.attDate + "&cur_code=" + $scope.edt.sims_cur_code + "&ayear=" + $scope.edt.sims_academic_year + "&grade=" + $scope.edt.sims_grade_code + "&section=" + $scope.edt.sims_section_code).then(function (attendance) {
            //    $scope.getAttendance();
            //});
            for (var i = 0; i < $scope.attendance_code.length; i++) {
                if ($scope.attendance_code[i].sims_attendance_code == att_Code) {
                    $scope.att_color = $scope.attendance_code[i].sims_attendance_color;
                }
            }

            $scope.att_list = [];
            for (var i = 0; i < $scope.attendance.length; i++) {
                $scope.attendance[i].sims_attendance_code = att_Code;
                $scope.attendance[i].sims_attendance_color = $scope.att_color;

                var obj1 = { cur_code: $scope.edt.sims_cur_code, ayear: $scope.edt.sims_academic_year, grade: $scope.edt.sims_grade_code, section: $scope.edt.sims_section_code, enrollNumber: $scope.attendance[i].sims_student_enroll, att_code: att_Code, attednacedate: $scope.edt.attDate, user_name: $rootScope.globals.currentUser.username }
                $scope.att_list.push(obj1)
            }

        }


        $scope.comment = function (sobj) {

            $scope.copy_comment = angular.copy(sobj);
            $scope.attednacedate2 = $scope.edt.attDate

            $("#comment_dailog").modal("show");

            $http.get(ENV.apiUrl + "api/attendance/get_AttendanceComment?enroll=" + $scope.copy_comment.sims_student_enroll + "&date=" + $scope.edt.attDate).then(function (res) {
                if (res.data.length > 0) {
                    $scope.copy_comment['sims_attendance_day_comment'] = res.data[0].sims_attendance_day_comment;
                }
            });

        }

        $scope.save_comment = function () {

            $http.post(ENV.apiUrl + "api/attendance/markAttendanceComment_update?enroll=" + $scope.copy_comment.sims_student_enroll + "&date=" + $scope.edt.attDate + "&comment=" + $scope.copy_comment.sims_attendance_day_comment).then(function (res) {
                $("#comment_dailog").modal("hide");
            });
        }



    }])

})();
