﻿(function () {
    'use strict';
    var simsController = angular.module('sims.module.Attendance');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.directive('contextMenu', ["$parse", "$q", function ($parse, $q) {

        var contextMenus = [];

        var removeContextMenus = function (level) {
            while (contextMenus.length && (!level || contextMenus.length > level)) {
                contextMenus.pop().remove();
            }
            if (contextMenus.length == 0 && $currentContextMenu) {
                $currentContextMenu.remove();
            }
        };

        var $currentContextMenu = null;

        var renderContextMenu = function ($scope, event, options, model, level) {
            if (!level) { level = 0; }
            if (!$) { var $ = angular.element; }
            $(event.currentTarget).addClass('context');
            var $contextMenu = $('<div>');
            if ($currentContextMenu) {
                $contextMenu = $currentContextMenu;
            } else {
                $currentContextMenu = $contextMenu;
            }
            $contextMenu.addClass('dropdown clearfix');
            var $ul = $('<ul>');
            $ul.addClass('dropdown-menu');
            $ul.attr({ 'role': 'menu' });
            $ul.css({
                display: 'block',
                position: 'absolute',
                left: event.pageX + 'px',
                top: event.pageY + 'px',
                "z-index": 10000
            });
            angular.forEach(options, function (item, i) {
                var $li = $('<li>');
                if (item === null) {
                    $li.addClass('divider');
                } else {
                    var nestedMenu = angular.isArray(item[1])
                      ? item[1] : angular.isArray(item[2])
                      ? item[2] : angular.isArray(item[3])
                      ? item[3] : null;
                    var $a = $('<a>');
                    $a.css("padding-right", "8px");
                    $a.attr({ tabindex: '-1' });
                    var text = typeof item[0] == 'string' ? item[0] : item[0].call($scope, $scope, event, model);
                    $q.when(text).then(function (text) {
                        $a.text(text);
                        if (nestedMenu) {
                            $a.css("cursor", "default");
                            $a.append($('<strong style="font-family:monospace;font-weight:bold;float:right;">&gt;</strong>'));
                        }
                    });
                    $li.append($a);

                    var enabled = angular.isFunction(item[2]) ? item[2].call($scope, $scope, event, model, text) : true;
                    if (enabled) {
                        var openNestedMenu = function ($event) {
                            removeContextMenus(level + 1);
                            var ev = {
                                pageX: event.pageX + $ul[0].offsetWidth - 1,
                                pageY: $ul[0].offsetTop + $li[0].offsetTop - 3
                            };
                            renderContextMenu($scope, ev, nestedMenu, model, level + 1);
                        }
                        $li.on('click', function ($event) {
                            //$event.preventDefault();
                            $scope.$apply(function () {
                                if (nestedMenu) {
                                    openNestedMenu($event);
                                } else {
                                    $(event.currentTarget).removeClass('context');
                                    removeContextMenus();
                                    item[1].call($scope, $scope, event, model);
                                }
                            });
                        });

                        $li.on('mouseover', function ($event) {
                            $scope.$apply(function () {
                                if (nestedMenu) {
                                    openNestedMenu($event);
                                }
                            });
                        });
                    } else {
                        $li.on('click', function ($event) {
                            $event.preventDefault();
                        });
                        $li.addClass('disabled');
                    }
                }
                $ul.append($li);
            });
            $contextMenu.append($ul);
            var height = Math.max(
                document.body.scrollHeight, document.documentElement.scrollHeight,
                document.body.offsetHeight, document.documentElement.offsetHeight,
                document.body.clientHeight, document.documentElement.clientHeight
            );
            $contextMenu.css({
                width: '100%',
                height: height + 'px',
                position: 'absolute',
                top: 0,
                left: 0,
                zIndex: 9999
            });
            $(document).find('body').append($contextMenu);
            $contextMenu.on("mousedown", function (e) {
                if ($(e.target).hasClass('dropdown')) {
                    $(event.currentTarget).removeClass('context');
                    removeContextMenus();
                }
            }).on('contextmenu', function (event) {
                $(event.currentTarget).removeClass('context');
                event.preventDefault();
                removeContextMenus(level);
            });
            $scope.$on("$destroy", function () {
                removeContextMenus();
            });

            contextMenus.push($ul);
        };
        return function ($scope, element, attrs) {
            element.on('contextmenu', function (event) {
                event.stopPropagation();
                $scope.$apply(function () {
                    event.preventDefault();
                    var options = $scope.$eval(attrs.contextMenu);
                    var model = $scope.$eval(attrs.model);
                    if (options instanceof Array) {
                        if (options.length === 0) { return; }
                        renderContextMenu($scope, event, options, model);
                    } else {
                        throw '"' + attrs.contextMenu + '" not an array';
                    }
                });
            });
        };
    }]);


    simsController.directive('ngRightClick', function ($parse) {
        return function (scope, element, attrs) {
            var fn = $parse(attrs.ngRightClick);
            element.bind('contextmenu', function (event) {
                scope.$apply(function () {
                    event.preventDefault();
                    fn(scope, { $event: event });
                });
            });
        };
    });

    simsController.controller('SubjectwisetAttendanceCont', ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV',
        function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var username = $rootScope.globals.currentUser.username;
            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!

            var yyyy = today.getFullYear();
            //$scope.att_date = mm + '/' + dd + '/' + yyyy;
            $scope.att_date = ('0' + dd).slice(-2) + '-' + ('0' + (mm)).slice(-2) + '-' + yyyy;

            setTimeout(function () {
                $scope.img_url = $scope.obj1.lic_website_url + '/Images/StudentImage/'
               
            }, 3000);

            $scope.operation = false;

            $http.get(ENV.apiUrl + "api/attendance/getAttendanceCuriculum").then(function (curiculum) {
                $scope.curiculum = curiculum.data;
                $scope.cur_code = $scope.curiculum[0].sims_cur_code;
               
                $scope.cur_change();
            });

            function getCur(flag, comp_code) {
                if (flag) {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                        $scope.curiculum = res.data;
                        $scope.cur_code = $scope.curiculum[0].sims_cur_code;
                        $scope.cur_change();

                    });
                }
                else {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                        $scope.curiculum = res.data;
                        $scope.cur_code = $scope.curiculum[0].sims_cur_code;
                        $scope.cur_change();
                    });


                }

            }

            $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.global_count_comp = res.data;

                if ($scope.global_count_comp) {
                    getCur(true, $scope.user_details.comp);


                }
                else {
                    getCur(false, $scope.user_details.comp)
                }
            });








            $http.get(ENV.apiUrl + "api/attendance/getAttendancetype").then(function (atttypes) {
                $scope.atttypes = atttypes.data;
            });

            $http.get(ENV.apiUrl + "api/attendance/getAttendancesort").then(function (sorttypes) {
                $scope.sorttypes = sorttypes.data;
            });
            $scope.type_change = function () {

                if ($scope.type_section == "1") {
                    cur_code
                    $http.get(ENV.apiUrl + "api/attendance/getAttendancesubtype").then(function (attsubtypes) {
                        $scope.attsubtypes = attsubtypes.data;
                    });
                }
                else {
                    $scope.attsubtypes = null;
                }

            }

            $scope.cur_change = function () {

                $http.get(ENV.apiUrl + "api/attendance/getAttendanceyear?cur_code=" + $scope.cur_code).then(function (academicyears) {
                    $scope.academicyears = academicyears.data;
                    $scope.acdm_yr = $scope.academicyears[0].sims_academic_year;
                    $scope.acdm_yr_change();
                });

                $http.get(ENV.apiUrl + "api/attendance/getAttendanceCode?cur_code=" + $scope.cur_code).then(function (attendance_code) {
                    $scope.attendance_code = attendance_code.data;
                    //$scope.rdbtn = $scope.attendance_code[0].sims_attendance_code;
                });


            }

            $scope.acdm_yr_change = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/SubjectwiseAttendance/GetAttednace_Grade?cur_code=" + $scope.cur_code + "&a_year=" + $scope.acdm_yr + "&userName=" + username).then(function (grades) {
                    $scope.grades = grades.data;
                    $scope.grade_change();
                });
            }
            $scope.grade_change = function () {
                debugger;
                $http.get(ENV.apiUrl + "api/SubjectwiseAttendance/GetAttednace_Section?cur_code=" + $scope.cur_code + "&a_year=" + $scope.acdm_yr + "&grade_code=" + $scope.grade_code + "&username=" + username).then(function (section) {
                    $scope.section = section.data;
                    $scope.sec_change();
                });
            }
            $scope.totalstudents = 0;
            $scope.totalmarkedstudents = 0;
            $scope.totalunmarkedstudents = 0;


            $scope.getAttendance = function (isvalid) {
                $scope.searchbox = true;
                if (isvalid) {
                    if ($scope.att_date == undefined) {
                        alert("select date");
                    }
                    else {
                       
                        $scope.busyindicator = false;
                        $scope.info1 = [];
                        var demo = [];
                        var demo1 = [];
                        $scope.table = false;
                        $scope.subject = false;

                        var date = $scope.att_date
                        //var month = date.split("/")[0];
                        //var day = date.split("/")[1];
                        //var year = date.split("/")[2];
                        //var date1 = year + "-" + month + "-" + day;
                        var date1 = date;

                        $http.get(ENV.apiUrl + "api/SubjectwiseAttendance/GetAttednace?cur_code=" + $scope.cur_code + "&ayear=" + $scope.acdm_yr + "&grade=" + $scope.grade_code + "&section=" + $scope.sec_code + "&attednacedate=" + date1 + "&sims_teacher_code=" + username).then(function (allSectionStudent) {

                            $scope.SectionStudent = allSectionStudent.data;
                            $scope.operation = true;
                            $scope.busyindicator = true;

                            if (allSectionStudent.data.length > 0 == 0) {
                                swal('No Data Found for this Date', '')
                            }

                            if (allSectionStudent.data.length > 0) {
                                for (var j = 0; j < $scope.SectionStudent[0].sublist1.length; j++) {
                                    demo = {
                                        'sims_subject_name': $scope.SectionStudent[0].sublist1[j].sims_subject_name,
                                        'sims_subject_code': $scope.SectionStudent[0].sublist1[j].sims_subject_code
                                    }
                                    demo1.push(demo);
                                }

                               
                                $scope.info1 = demo1;

                                $scope.table = true;

                                $timeout(function () {
                                    $("#fixTable").tableHeadFixer({ "left": 2 });
                                }, 100);
                            }
                            else {

                                $scope.subject = true;

                            }
                        });








                        // $scope.mycolor = 'background-color:red';
                        //  $scope.operation = false;

                        //$http.get(ENV.apiUrl + "api/SubjectwiseAttendance/GetAttednace?cur_code=" + $scope.cur_code + "&ayear=" + $scope.acdm_yr + "&grade=" + $scope.grade_code + "&section=" + $scope.sec_code + "&attednacedate=" + date1 + "&sims_teacher_code=" + username).then(function (attendance) {
                        //    $scope.attendance = attendance.data;
                        //    $scope.operation = true;
                        //    $scope.totalstudents = $scope.attendance.length;
                        //    if ($scope.totalmarkedstudents == 0) {
                        //        swal('No Data Found for this Date','')
                        //    }


                        //  
                        //    updateCount();
                        //});
                    }
                }
            }


            $scope.Mark_Subject_Attendance = function (obj, str) {

                obj["sims_attedance_code"] = $scope.sims_attendance_code;
                obj["sims_att_color_code"] = $scope.sims_attendance_color;

                var d = moment().format('YYYY-MM-DD');
                var d1 = moment(str.sims_attedance_date, 'DD-MM-YYYY').format('YYYY-MM-DD')

                
              
                if (d >= d1) {
                    var data = {
                        sims_cur_code: str.sims_cur_code,
                        sims_academic_year: str.sims_academic_year,
                        sims_grade_code: str.sims_grade_code,
                        sims_section_code: str.sims_section_code,
                        sims_subject_code: obj.sims_subject_code,
                        sims_teacher_code: username,
                        sims_attedance_slot: obj.sims_attedance_slot,
                        sims_attedance_code: $scope.sims_attendance_code,
                        sims_enroll_number: str.sims_enroll_number,
                        sims_attedance_date: str.sims_attedance_date,
                    }

                    

                    $http.post(ENV.apiUrl + "api/SubjectwiseAttendance/UpdateAttednace", data).then(function (res) {
                        $scope.ins = res.data;
                        if (res.data) {
                            // swal({ title: "Alert", text: "Attendance Marked Successfully.", imageUrl: "assets/img/check.png", });


                        }
                        else
                            swal({ text: "Record Not Marked.", imageUrl: "assets/img/notification-alert.png", });


                    });
                }
                else { swal({ text: "Future date attendance marking not allowed.", imageUrl: "assets/img/notification-alert.png", }); }
                

            }


            $scope.get_attend_code = function (codes) {
               
                $scope.sims_attendance_color = codes.sims_attendance_color;
                $scope.sims_attendance_code = codes.sims_attendance_code;
            }

            $scope.comment = function (obj, str) {
                
                var data = {
                    sims_cur_code: str.sims_cur_code,
                    sims_academic_year: str.sims_academic_year,
                    sims_grade_code: str.sims_grade_code,
                    sims_section_code: str.sims_section_code,
                    sims_subject_code: obj.sims_subject_code,
                    sims_teacher_code: username,
                    sims_attedance_slot: obj.sims_attedance_slot,
                    sims_attedance_code: $scope.sims_attendance_code,
                    sims_enroll_number: str.sims_enroll_number,
                    sims_attedance_date: str.sims_attedance_date,
                }


                $scope.copy_comment = {};
                $scope.copy_comment = angular.copy(data);
                $scope.copy_comment['sims_enroll_number'] = str.sims_enroll_number;
                $scope.copy_comment['sims_student_name'] = str.sims_student_name;
                $scope.attednacedate2= str.sims_attedance_date;

                $("#comment_dailog").modal("show");

                $http.post(ENV.apiUrl + "api/SubjectwiseAttendance/CommentGetAttednace", $scope.copy_comment).then(function (res) {
                   // if (res.data.length > 0) {
                        $scope.copy_comment['sims_attendance_day_comment'] = res.data;
                   // }
                });

            }


            $scope.show_comment = function (obj, str) {

                var data = {
                    sims_cur_code: str.sims_cur_code,
                    sims_academic_year: str.sims_academic_year,
                    sims_grade_code: str.sims_grade_code,
                    sims_section_code: str.sims_section_code,
                    sims_subject_code: obj.sims_subject_code,
                    sims_teacher_code: username,
                    sims_attedance_slot: obj.sims_attedance_slot,
                    sims_attedance_code: $scope.sims_attendance_code,
                    sims_enroll_number: str.sims_enroll_number,
                    sims_attedance_date: str.sims_attedance_date,
                }


                $scope.copy_comment = {};
                $scope.copy_comment = angular.copy(data);
                $http.post(ENV.apiUrl + "api/SubjectwiseAttendance/CommentGetAttednace", $scope.copy_comment).then(function (res) {
                    if (res.data.length > 0) {
                        $scope.stud_comment =' Comment: '+ res.data;
                    }
                    else
                        $scope.stud_comment = '';
                });
               
            }

            $scope.save_comment = function () {

                $http.post(ENV.apiUrl + "api/SubjectwiseAttendance/UpdateCommentAttednace", $scope.copy_comment).then(function (res) {

                    $("#comment_dailog").modal("hide");
                });
            }



            $scope.menuOptionsDayWise = [
             ['Mark All Present', function ($itemScope) {
                 $('.dropdown-menu').attr('style', 'display: none');

                 $('.dropdown').remove();
                

                 $scope.sims_attendance_code_m = 'P';
                // var d = $itemScope.i.day;
                
                     swal({
                         title: "Are you sure?", text: "You will not be able to recover this Data!", showCancelButton: true,
                         confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, Mark it!", closeOnConfirm: false
                     }).then(function (isConfirm) {
                         if (isConfirm) {
                             

                             var slot = $itemScope.i.sims_subject_name.split('(');
                             var slot1 = slot[1].split(')');

                             var data = {
                                 sims_cur_code: $scope.cur_code,
                                 sims_academic_year: $scope.acdm_yr,
                                 sims_grade_code: $scope.grade_code,
                                 sims_section_code: $scope.sec_code,
                                 sims_subject_code: $itemScope.i.sims_subject_code,
                                 sims_teacher_code: username,
                                 sims_attedance_slot: slot1[0],
                                 sims_attedance_code:'P',
                               //  sims_enroll_number: str.sims_enroll_number,
                                 sims_attedance_date: $scope.att_date,
                             }

                             $http.post(ENV.apiUrl + "api/SubjectwiseAttendance/MarkAllAttednace", data).then(function (res) {
                                 $scope.ins = res.data;
                                 if (res.data) {
                                     swal({  text: "Attendance Marked Successfully.", imageUrl: "assets/img/check.png", });
                                     $scope.getAttendance(true);

                                 }
                                 else
                                     swal({ text: "Record Not Marked.", imageUrl: "assets/img/close.png",});

                             });
 
                         }

                     });
                 

             }],
             null,
             ['Mark All Absent', function ($itemScope) {
                 $('.dropdown-menu').attr('style', 'display: none');
                 $('.dropdown').remove();

               

                     swal({
                         title: "Are you sure?", text: "You will not be able to recover this Data!", showCancelButton: true,
                         confirmButtonColor: "#DD6B55", confirmButtonText: "Yes, Mark it!", closeOnConfirm: false
                     }).then(function (isConfirm) {
                         if (isConfirm) {


                             var slot = $itemScope.i.sims_subject_name.split('(');
                             var slot1 = slot[1].split(')');

                             var data = {
                                 sims_cur_code: $scope.cur_code,
                                 sims_academic_year: $scope.acdm_yr,
                                 sims_grade_code: $scope.grade_code,
                                 sims_section_code: $scope.sec_code,
                                 sims_subject_code: $itemScope.i.sims_subject_code,
                                 sims_teacher_code: username,
                                 sims_attedance_slot: slot1[0],
                                 sims_attedance_code: 'A',
                                 //  sims_enroll_number: str.sims_enroll_number,
                                 sims_attedance_date: $scope.att_date,
                             }

                             $http.post(ENV.apiUrl + "api/SubjectwiseAttendance/MarkAllAttednace", data).then(function (res) {
                                 $scope.ins = res.data;
                                 if (res.data) {
                                     swal({ text: "Attendance Marked Successfully.", imageUrl: "assets/img/check.png", });
                                     $scope.getAttendance(true);

                                 }
                                 else
                                     swal({ text: "Record Not Marked.", imageUrl: "assets/img/close.png", });

                             });

                          
                         }
                     });
                 
                 
             }],
             null,

            ];



            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


            //for collapse main menu
            $('body').addClass('grey condense-menu');
            $('#main-menu').addClass('mini');
            $('.page-content').addClass('condensed');
            $rootScope.isCondensed = true;

        }])

})();