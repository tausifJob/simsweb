﻿(function () {
    'use strict';
    var obj1, temp, opr, comp, edt;
    var main, savefin = [], savecode = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Library');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('LibraryItemVerificationApproveCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            var user = $rootScope.globals.currentUser.username;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100)

            $http.get(ENV.apiUrl + "api/LibraryItemVerificationApprove/GetSims_ItemVerificationStatus").then(function (res) {
                debugger
                $scope.approve_status = res.data;
            });
            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.CreDiv;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                debugger;
                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_library_bin_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                        item.sims_library_bin_code == toSearch) ? true : false;
            }

       
            //DATA CANCEL
            $scope.Reset = function () {
                debugger
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                //$scope.Myform.$setPristine();
                //$scope.Myform.$setUntouched();
                $scope.date_from = "";
                $scope.date_upto = "";
                $scope.acc_no = "";
                $scope.reqdetails = [];
            }

           

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function () {
                debugger;
                for (var i = 0; i < $scope.reqdetails.length; i++) {
                    if ($scope.reqdetails[i].sims_library_item_approved_status_code == undefined || $scope.reqdetails[i].sims_library_item_approved_status_code == '') {
                        $scope.reqdetails[i].sims_library_item_approved_status_code =null
                    }
                    $scope.reqdetails[i].opr = 'U';
                    $scope.reqdetails[i];
                    $scope.reqdetails[i].sims_library_item_approved_by = user;
                    datasend.push($scope.reqdetails[i]);
                }                    
                   
                    $http.post(ENV.apiUrl + "api/LibraryItemVerificationApprove/CUDLibraryItemVerification", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Saved Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.Reset();
                        }
                        else {
                            swal({ text: "Record Not Saved", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }
                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
               
            }

            //DATA UPDATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                if (Myform) {

                    var data = $scope.temp;
                    data.opr = "U";
                    dataforUpdate.push(data);
                    //dataupdate.push(data);
                    $http.post(ENV.apiUrl + "api/LibraryBin/CUDLibraryBin", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.getgrid();
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                    });
                    dataforUpdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }
            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_library_bin_desc + $scope.filteredTodos[i].sims_library_bin_code + i);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_library_bin_desc + $scope.filteredTodos[i].sims_library_bin_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }
            
            $scope.Show = function () {
                debugger;
                $scope.requesttable = true;
                $http.get(ENV.apiUrl + "api/LibraryItemVerificationApprove/Get_RequestDetail?date1=" + $scope.date_from + "&date2=" + $scope.date_upto + "&acc_no=" + $scope.acc_no).then(function (details) {

                    $scope.reqdetails = details.data;
                    $scope.totalItems = $scope.reqdetails.length;
                    $scope.todos = $scope.reqdetails;
                    $scope.makeTodos();
                    //console.log($scope.reqdetails);
                    //if (details.data.length > 0) { }
                    //else {
                    //    $scope.ImageView = true;
                    //}
                })
            }           

        }])

})();