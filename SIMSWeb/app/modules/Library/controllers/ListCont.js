﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Library');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('ListCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 1;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            var deletefin = [];
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //Select Data SHOW
            $scope.getdata = function () {
                $http.get(ENV.apiUrl + "api/List/getAllList").then(function (res1) {
                    $scope.CreDiv = res1.data;
                    $scope.totalItems = $scope.CreDiv.length;
                    $scope.todos = $scope.CreDiv;
                    $scope.makeTodos();

                });
            }
            

            $http.get(ENV.apiUrl + "api/List/getAllList").then(function (res1) {
                $scope.CreDiv = res1.data;
                $scope.totalItems = $scope.CreDiv.length;
                $scope.todos = $scope.CreDiv;
                $scope.makeTodos();

            });

            $scope.size = function (str) {

                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);


                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                
                return (item.sims_list_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_list_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_list_description.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_list_status == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {
                debugger;
              //  $scope.disabled = false;
                //  $scope.gdisabled = false;
                
                //$scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.divcode_readonly = false;
                $scope.temp.sims_list_name = '';
                $scope.temp.sims_list_description = '';
                
                
               
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {

                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.divcode_readonly = true;
                //   $scope.temp = str;

                $scope.temp = {
                    sims_list_name: str.sims_list_name,
                    sims_list_status: str.sims_list_status,
                    sims_list_code: str.sims_list_code,
                    sims_list_description: str.sims_list_description

                };
            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                if (Myform) {
                    var data = $scope.temp;
                    data.opr = 'I';
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/List/CUDList", datasend).then(function (msg) {
                        datasend = [];
                        $scope.msg1 = msg.data;
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        }
                        else {
                            swal({ text: "Record already present. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }

                        $scope.getdata();
                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }

            }

            //DATA UPDATE
            var dataforUpdate = [];
            $scope.Update = function (Myform) {
                if (Myform){
                    debugger;
                    var data = $scope.temp;
                    data.opr = "U";
                    dataforUpdate.push(data);
                    //dataupdate.push(data);

                    $http.post(ENV.apiUrl + "api/List/CUDList", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $scope.getdata();
                    });
                    dataforUpdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

          
            // Data DELETE RECORD

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].sims_list_code);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].sims_list_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }
      

          


            $scope.OkDelete = function () {
                debugger;
                deletefin = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i + $scope.filteredTodos[i].sims_list_code);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_list_code': $scope.filteredTodos[i].sims_list_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/List/CUDList", deletefin).then(function (msg) {
                                debugger;
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/List/getAllList").then(function (res1) {
                                                $scope.CreDiv = res1.data;
                                                $scope.totalItems = $scope.CreDiv.length;
                                                $scope.todos = $scope.CreDiv;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted Successfully. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/List/getAllList").then(function (res1) {
                                                $scope.CreDiv = res1.data;
                                                $scope.totalItems = $scope.CreDiv.length;
                                                $scope.todos = $scope.CreDiv;
                                                $scope.makeTodos();

                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i + $scope.filteredTodos[i].sims_list_code);
                                if (v.checked == true) {
                                    v.checked = false;
                                    //  $scope.row1 = '';
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }

        
            $(document).ready(function () {
                $scope.getdata();
                
            });
        }])

})();

           

        


