﻿(function () {

    var simsController = angular.module('sims.module.Library');

    simsController.controller('LibraryBooksetCont', ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

        $scope.display = false;
        $scope.table = true;


        $scope.pagesize = '10';
        $scope.pageindex = "0";
        $scope.pager = true;
        $timeout(function () {
            $("#fixTable").tableHeadFixer({ 'top': 1 });
        }, 100);


        $scope.size = function (str) {
            //console.log(str);
            //$scope.pagesize = str;
            //$scope.currentPage = 1;
            //$scope.numPerPage = str;
            debugger;
            if (str == "All") {
                $scope.currentPage = '1';
                $scope.filteredTodos = $scope.CreDiv;
                $scope.pager = false;
            }
            else {
                $scope.pager = true;
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                $scope.makeTodos();
            }
            console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
        }

        $scope.index = function (str) {
            $scope.pageindex = str;
            $scope.currentPage = str;
            console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
            main.checked = false;
            $scope.CheckAllChecked();
        }

        $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

        $scope.makeTodos = function () {
            var rem = parseInt($scope.totalItems % $scope.numPerPage);
            if (rem == '0') {
                $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
            }
            else {
                $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
            }

            var begin = (($scope.currentPage - 1) * $scope.numPerPage);
            var end = parseInt(begin) + parseInt($scope.numPerPage);

            console.log("begin=" + begin); console.log("end=" + end);

            $scope.filteredTodos = $scope.todos.slice(begin, end);
        };

        $scope.searched = function (valLists, toSearch) {
            return _.filter(valLists,

            function (i) {
                /* Search Text in all  fields */
                return searchUtil(i, toSearch);
            });
        };

        //  new
        $scope.New = function () {
            var datasend = [];
            $scope.int = "";
            $scope.table = false;
            $scope.display = true;
            $scope.save_btn = true;
            $scope.Update_btn = false;
            $scope.move = true;
        }

        //cancle
        $scope.Cancel = function () {
            $scope.temp = "";
            $scope.table = true;
            $scope.display = false;
            $scope.int = " ";
        }

        //DATA EDIT  
        $scope.edit = function (str) {
            debugger
            $scope.Update_btn = true;
            $scope.gdisabled = false;
            $scope.aydisabled = true;
            $scope.table = false;
            $scope.display = true;
            $scope.save_btn = false;

            $scope.divcode_readonly = true;


            debugger
            $scope.int = {
                sims_library_bookset_number: str.sims_library_bookset_number,
                sims_library_bookset_name: str.sims_library_bookset_name,
                sims_library_bookset_book_qty: str.sims_library_bookset_book_qty,
                sims_library_bookset_status: str.sims_library_bookset_status,
               
                id: str.id,
            }
        }
        // grid data
        $scope.getGrid = function () {

            $http.get(ENV.apiUrl + "api/LibraryBookset/get_bookSetDetail").then(function (res1) {
                debugger
                $scope.CreDiv = res1.data;
                $scope.totalItems = $scope.CreDiv.length;
                // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                $scope.todos = $scope.CreDiv;
                $scope.makeTodos();
            });
        }
        $scope.getGrid();

        //insert data 
        $scope.save_section = function (Myform) {
            var datasend = [];
            if (Myform) {
                $scope.data = $scope.int;

                $scope.data['opr'] = 'I';
                datasend.push($scope.data);
                $http.post(ENV.apiUrl + "api/LibraryBookset/CUDLibraryBookset", datasend).then(function (res) {

                    $scope.msg1 = res.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        $scope.getGrid();
                        $scope.int = {};
                        $scope.Cancel();
                        
                    }
                    else {
                        //swal({ title: "Alert", text: "Record NOt Inserted ", showCloseButton: true, width: 300, height: 200 });
                        swal('', 'Record not inserted');

                    }
                });
            }
        }

        //DATA UPDATE
        var dataforUpdate = [];
        $scope.update = function (Myform) {
            if (Myform) {
                debugger;
                var data = $scope.int;
                data.sims_library_bookset_number = $scope.int.sims_library_bookset_number;
                data.opr = "U";
                dataforUpdate.push(data);
                //dataupdate.push(data);
                $http.post(ENV.apiUrl + "api/LibraryBookset/CUDLibraryBookset", dataforUpdate).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        $scope.getGrid();
                    }
                    else {
                        swal({ text: "Record Not Updated", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                    }
                    //search data show                        
                    //$scope.getGrid();
                    //$http.get(ENV.apiUrl + "api/demo/get_demo").then(function (res1) {
                    //    $scope.CreDiv = res1.data;
                    //    $scope.totalItems = $scope.CreDiv.length;
                    //    $scope.todos = $scope.CreDiv;
                    //    $scope.makeTodos();

                    //});

                });
                dataforUpdate = [];
                $scope.table = true;
                $scope.display = false;
            }
        }
        // Data DELETE RECORD
  $scope.CheckAllChecked = function () {
            main = document.getElementById('mainchk');
            if (main.checked == true) {
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i + $scope.filteredTodos[i].sims_library_bookset_number);
                    v.checked = true;
                    $scope.row1 = 'row_selected';
                    $('tr').addClass("row_selected");
                }
            }
            else {

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i + $scope.filteredTodos[i].sims_library_bookset_number);
                    v.checked = false;
                    main.checked = false;
                    $scope.row1 = '';
                    $('tr').removeClass("row_selected");
                }
            }

        }

        $scope.checkonebyonedelete = function () {

            $("input[type='checkbox']").change(function (e) {
                if ($(this).is(":checked")) {
                    $(this).closest('tr').addClass("row_selected");
                    $scope.color = '#edefef';
                } else {
                    $(this).closest('tr').removeClass("row_selected");
                    $scope.color = '#edefef';
                }
            });

            main = document.getElementById('mainchk');
            if (main.checked == true) {
                main.checked = false;
                $scope.color = '#edefef';
                $scope.row1 = '';
            }
        }

        //delete
        $scope.OkDelete = function () {
            debugger
            deletefin = [];
            $scope.flag = false;
            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                var v = document.getElementById(i + $scope.filteredTodos[i].sims_library_bookset_number);
                if (v.checked == true) {
                    $scope.flag = true;
                    var deletemodulecode = {
                        'sims_library_bookset_number': $scope.filteredTodos[i].sims_library_bookset_number,
                        opr: 'D'
                    }
                    deletefin.push(deletemodulecode);
                }
            }
            if ($scope.flag) {
                swal({
                    title: '',
                    text: "Are you sure you want to Delete? Delete this field Related all record",
                    showCloseButton: true,
                    showCancelButton: true,
                    confirmButtonText: 'Yes',
                    width: 380,
                    cancelButtonText: 'No',

                }).then(function (isConfirm) {

                    if (isConfirm) {
                        debugger;
                        $http.post(ENV.apiUrl + "api/LibraryBookset/CUDLibraryBookset", deletefin).then(function (msg) {
                            $scope.msg1 = msg.data;
                            if ($scope.msg1 == true) {
                                swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getGrid();
                                        main = document.getElementById('mainchk');
                                        if (main.checked == true) {
                                            main.checked = false;
                                        }

                                        $scope.CheckAllChecked();
                                    }
                                    $scope.currentPage = true;
                                });
                            }
                            else {
                                swal({ text: "Record Not Deleted", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                    if (isConfirm) {
                                        $scope.getGrid();
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                    }
                                });
                            }

                        });
                        deletefin = [];
                    }
                    else {
                        for (var i = 0; i < $scope.filteredTodos.length; i++) {
                            var v = document.getElementById($scope.filteredTodos[i].sims_library_bookset_number + i);
                            if (v.checked == true) {
                                v.checked = false;
                                main.checked = false;
                                $('tr').removeClass("row_selected");
                            }
                        }
                    }
                });
            }
            else {
                swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
            }
            $scope.currentPage = true;
        }
}])
})();