﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Library');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('LibraryLocationCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/LibraryLocation/getAllLibraryLocation").then(function (res1) {

                $scope.LocData = res1.data;
                $scope.totalItems = $scope.LocData.length;
                $scope.todos = $scope.LocData;
                $scope.makeTodos();

            });

            //$scope.size = function (str) {
            //    console.log(str);
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str;
            //    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}
            $scope.size = function (str) {
                //debugger;
                //if (str == "All") {
                //    $scope.currentPage = '1';
                //    $scope.filteredTodos = $scope.getAllTerm_details;
                //    $scope.pager = false;
                //}
                //else {
                //    $scope.pager = true;
                //    $scope.pagesize = str;
                //    $scope.currentPage = 1;
                //    $scope.numPerPage = str;
                //    $scope.makeTodos();
                //}
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.LocData;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

                $scope.numPerPage = str; console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.LocData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.LocData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_library_location_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_library_location_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {

                $scope.disabled = false;

                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.divcode_readonly = false;
                $scope.temp = "";
                $scope.codecatagory = false;
                $scope.temp = {};
                $scope.temp['sims_library_attribute_status'] = true;

                $http.get(ENV.apiUrl + "api/LibraryCategory/getAutoGenerateCode").then(function (res) {
                    $scope.autogendesg = res.data;
                    $scope.temp = {
                        sims_library_category_code: $scope.autogendesg[0].sims_library_category_code,
                    };
                    console.log($scope.autogendesg);
                });
                $scope.temp["sims_library_category_name"] = "";

            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {

                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.divcode_readonly = true;
                $scope.codecatagory = true;
                $scope.temp = str;

                $scope.temp = {
                    sims_library_location_code: str.sims_library_location_code,
                    sims_library_location_name: str.sims_library_location_name,
                    sims_library_location_address1: str.sims_library_location_address1,
                    sims_library_location_address2: str.sims_library_location_address2,
                    sims_library_location_contact_person: str.sims_library_location_contact_person,
                    sims_library_location_phone: str.sims_library_location_phone
                }

            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {

                if (Myform) {
                    var data = $scope.temp;
                    data.opr = 'I';
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/LibraryLocation/CUDLibraryLocation", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                        $http.get(ENV.apiUrl + "api/LibraryLocation/getAllLibraryLocation").then(function (res1) {
                            $scope.LocData = res1.data;
                            $scope.totalItems = $scope.LocData.length;
                            $scope.todos = $scope.LocData;
                            $scope.makeTodos();

                        });

                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                if (Myform) {
                    var data = $scope.temp;
                    data.opr = "U";
                    dataforUpdate.push(data);
                    //dataupdate.push(data);
                    $http.post(ENV.apiUrl + "api/LibraryLocation/CUDLibraryLocation", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $http.get(ENV.apiUrl + "api/LibraryLocation/getAllLibraryLocation").then(function (res1) {
                            $scope.LocData = res1.data;
                            $scope.totalItems = $scope.LocData.length;
                            $scope.todos = $scope.LocData;
                            $scope.makeTodos();

                        });

                    });
                    dataforUpdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].sims_library_location_code);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].sims_library_location_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }


       

            $scope.OkDelete = function () {
                debugger;
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i + $scope.filteredTodos[i].sims_library_location_code);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_library_location_code': $scope.filteredTodos[i].sims_library_location_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/LibraryLocation/CUDLibraryLocation", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/LibraryLocation/getAllLibraryLocation").then(function (res1) {
                                                $scope.LocData = res1.data;
                                                $scope.totalItems = $scope.LocData.length;
                                                $scope.todos = $scope.LocData;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/LibraryLocation/getAllLibraryLocation").then(function (res1) {
                                                $scope.LocData = res1.data;
                                                $scope.totalItems = $scope.LocData.length;
                                                $scope.todos = $scope.LocData;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i + $scope.filteredTodos[i].sims_library_location_code);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
               
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });


        }])

})();
