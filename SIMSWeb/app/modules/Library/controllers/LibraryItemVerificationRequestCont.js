﻿(function () {
    'use strict';
    var obj1, temp, opr, comp, edt;
    var main, savefin = [], savecode = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Library');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('LibraryItemVerificationRequestCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = "0";
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            var user = $rootScope.globals.currentUser.username;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100)

            $(function () {
                $('#cmb_category').multipleSelect({
                    width: '100%'
                });
            });

            $(function () {
                $('#cmb_subcategory').multipleSelect({
                    width: '100%'
                });
            });

            $http.get(ENV.apiUrl + "api/LibraryItemVerificationApprove/GetSims_ItemVerificationStatus").then(function (res) {
                debugger
                $scope.approve_status = res.data;
            });

            $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetLibraryCategory").then(function (CategoryTypes) {

                $scope.Library_category_code = CategoryTypes.data;
                console.log($scope.Library_category_code);
                setTimeout(function () {
                    debugger;
                    $('#cmb_category').change(function () {
                        console.log($(this).val());
                        $scope.getSubCat($scope.temp.sims_library_item_category);
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });

            $scope.getSubCat = function (sub_cat) {
                debugger
                if (sub_cat == undefined || sub_cat == '') {
                    sub_cat
                    return;
                }
                $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetLibrarySubCategory?SubCategory=" + sub_cat).then(function (SubCategoryTypes) {
                    $scope.Library_subcategory_code = SubCategoryTypes.data;
                    console.log($scope.Library_subcategory_code);
                    setTimeout(function () {
                        debugger;
                        $('#cmb_subcategory').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });

            }

            $scope.size = function (str) {
                debugger;
                if (str == 10 || str == 20 || str == 'All') {
                    $scope.pager = true;
                }
                else {
                    $scope.pager = false;
                }
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }

                if (str == "All") {
                    $scope.currentPage = 1;
                    $scope.numPerPage = $scope.totalItems;
                    $scope.makeTodos();

                }
                else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.reqdetails = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                debugger;
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.reqdetails = $scope.todos.slice(begin, end);
            };

            $scope.Show = function () {
                debugger;
                $scope.requesttable = true;
                $scope.busy = true;
                $scope.tempnew = {};
                //$scope.temp = {};
                $scope.tempnew.sims_library_item_category = $scope.temp.sims_library_item_category + "";
                $scope.tempnew.sims_library_item_subcategory = $scope.temp.sims_library_item_subcategory + "";
                $scope.tempnew.sims_library_item_accession_number = $scope.temp.sims_library_item_accession_number;                
                $http.post(ENV.apiUrl + "api/LibraryItemVerificationApprove/ItemSearch", $scope.tempnew).then(function (res) {
                    debugger
                    $scope.busy = false;
                    $scope.reqdetails = res.data;
                   // $scope.reqdetails.sims_library_item_request_status_code = $scope.approve_status_
                    $scope.totalItems = $scope.reqdetails.length;
                    $scope.todos = $scope.reqdetails;
                    $scope.makeTodos();
                });
            }

                   


            //DATA CANCEL
            $scope.Reset = function () {
                debugger
                $state.go($state.current, {}, { reload: true });
            }



            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function () {
                debugger;
                for (var i = 0; i < $scope.reqdetails.length; i++) {                    
                    if ($scope.reqdetails[i]['ischange']) {
                        if ($scope.reqdetails[i].sims_library_item_approved_status_code == undefined || $scope.reqdetails[i].sims_library_item_approved_status_code == null) {
                            swal('', 'Please enter request status for Item no: ' + $scope.reqdetails[i].sims_library_item_number);
                            return;
                        }
                        
                    }
                }

                for (var i = 0; i < $scope.reqdetails.length; i++) {
                    debugger;
                    if ($scope.reqdetails[i].sims_library_item_approved_status_code != undefined || $scope.reqdetails[i].sims_library_item_approved_status_code != null) {
                        $scope.reqdetails[i].opr = 'I';
                        $scope.reqdetails[i];
                        $scope.reqdetails[i].sims_library_item_requested_by = user;
                        datasend.push($scope.reqdetails[i]);
                    }
                }
               
                        
                
                $http.post(ENV.apiUrl + "api/LibraryItemVerificationApprove/CUDLibraryItemVerificationInsert", datasend).then(function (msg) {
                    $scope.msg1 = msg.data;

                    if ($scope.msg1 == true) {
                        swal({ text: "Record Saved Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        $scope.Reset();
                    }
                    else {
                        swal({ text: "Record Not Saved/Already Requested", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                    }
                });
                datasend = [];
                $scope.table = true;
                $scope.display = false;

            }

            //DATA UPDATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                if (Myform) {

                    var data = $scope.temp;
                    data.opr = "U";
                    dataforUpdate.push(data);
                    //dataupdate.push(data);
                    $http.post(ENV.apiUrl + "api/LibraryBin/CUDLibraryBin", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.getgrid();
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                    });
                    dataforUpdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }
            // Data DELETE RECORD
            $scope.CheckAllCheckedbox = function () {
                debugger
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.reqdetails.length; i++) {
                        var v = document.getElementById($scope.reqdetails[i].sims_library_item_number + i);
                        v.checked = true;
                        $scope.reqdetails[i].ischange = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {
                    for (var i = 0; i < $scope.reqdetails.length; i++) {
                        var v = document.getElementById($scope.reqdetails[i].sims_library_item_number + i);
                        v.checked = false;
                        $scope.reqdetails[i].ischange = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }


            $scope.checkonebyone = function (info) {
                debugger;
                for (var i = 0; i < $scope.reqdetails.length; i++) {
                    var v = document.getElementById($scope.reqdetails[i].sims_library_item_number+ i);
                    if (v.checked == true) {
                        $scope.reqdetails[i].ischange = true;
                        //$scope.row1 = 'row_selected';
                        //$('tr').addClass("row_selected");
                        //$scope.color = '#edefef';
                    }
                    else {
                        $scope.reqdetails[i].ischange = false;
                        //$scope.row1 = '';
                        //$('tr').removeClass("row_selected");
                        //$scope.color = '#edefef';
                    }
                }

                $("input[type='checkbox']").change(function (e) {
                    debugger;
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';

                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';

                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    $scope.color = '#edefef';
                    $scope.row1 = '';

                }

                //main = document.getElementById('$index');
                //if (main.checked == true) {

                //    main.checked = false;
                //    $scope.color = '#edefef';
                //    $scope.row1 = '';
                //}
            }

           

        }])

})();