﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, savefin = [];
    var savestud = [], savebook = [];
    var simsController = angular.module('sims.module.Library');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

    simsController.controller('LibraryTransactionDetailCont',
   ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
       $scope.temp = {};
       $scope.pagesize = "5";
       $scope.pageindex = 0;
       $scope.save_btn = true;
       var dataforSave = [];
       $scope.Update_btn = false;
       $scope.display = false;
       $scope.table = true;
       $scope.grid = true;
       $scope.add_bks = false;
       $scope.txnstudent = [];
    //   $scope.info = {};
       var user = $rootScope.globals.currentUser.username;
       $('body').addClass('grey condense-menu');
       $('#main-menu').addClass('mini');
       $('.page-content').addClass('condensed');
       $('*[data-datepicker="true"] input[type="text"]').datepicker({
           todayBtn: true,
           orientation: "top left",
           autoclose: true,
           todayHighlight: true,
           dateFormat: 'mm-dd-yyyy'
       });

       $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
           $('input[type="text"]', $(this).parent()).focus();
       });
       $timeout(function () {
           $("#fixTable").tableHeadFixer({ 'top': 1 });
       }, 100);

       $scope.propertyName = null;
       $scope.reverse = false;

       $scope.sortBy = function (propertyName) {
           $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
           $scope.propertyName = propertyName;
       };

       //Class Room ComboBoxes
       $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
           $scope.ComboBoxValues = AllComboBoxValues.data;
           //$scope.temp = {
           //    sims_academic_year: $scope.ComboBoxValues[0].sims_acadmic_year,
           //    s_cur_code: $scope.ComboBoxValues[0].sims_cur_code,
           //};
           setTimeout(function () {
               $('#cmb_section_code').change(function () {
                   console.log($(this).val());
               }).multipleSelect({
                   width: '100%'
               });
           }, 1000);
           console.log($scope.ComboBoxValues);
       });
       $(function () {
           $('#cmb_section_code').multipleSelect({
               width: '100%'
           });
       });

       $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
           $scope.curriculum = AllCurr.data;

           $scope.temp.sims_cur_code=$scope.curriculum[0].sims_cur_code;

           console.log($scope.temp.sims_cur_cod);
           $scope.getAccYear($scope.temp.sims_cur_code)
       });

       $scope.getAccYear = function (curCode) {
           $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
               $scope.Acc_year = Acyear.data;
               debugger;
               $scope.temp.sims_academic_year= $scope.Acc_year[0].sims_academic_year;
                  
               $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
           });

       }

       $scope.getGrade = function (curCode, accYear) {
           $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
               $scope.Grade_code = Gradecode.data;

               //$scope.temp = {
               //    'sims_cur_code': $scope.curriculum[0].sims_cur_code,
               //    'sims_academic_year': $scope.Acc_year[0].sims_academic_year
               //}
               //setTimeout(function () {
               //    $('#cmb_grade_code').change(function () {
               //        console.log($(this).val());
               //    }).multipleSelect({
               //        width: '100%'
               //    });
               //}, 1000);
              // $scope.getSection($scope.temp.sims_cur_code, $scope.temp.sims_academic_year, $scope.temp.sims_grade_code)
           });
       }

       $scope.getSection = function (curCode, gradeCode, accYear) {
           debugger
           $http.get(ENV.apiUrl + "api/StudentReport/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
               $scope.Section_code = Sectioncode.data;
               setTimeout(function () {
                   $('#cmb_section_code').change(function () {
                       console.log($(this).val());
                   }).multipleSelect({
                       width: '100%'
                   });
               }, 1000);

           });
       }
       $(function () {
           $('#cmb_section_code').multipleSelect({
               width: '100%'
           });
       });




       //Home Room Comboboxes
       $scope.hide2 = function () {
           $scope.grid1 = false;
         //  $scope.temp = "";

           $http.get(ENV.apiUrl + "api/HomeRoomAttendance/getAttendanceCuriculum").then(function (curiculum) {
               $scope.curiculum = curiculum.data;
               $scope.edt.sims_cur_code = curiculum.data[0].sims_cur_code;
               $scope.cur_change();
               $scope.acdm_yr_change();
           });

           $scope.cur_change = function () {

               $http.get(ENV.apiUrl + "api/HomeRoomAttendance/getAttendanceyear?cur_code=" + $scope.edt.sims_cur_code).then(function (academicyears) {
                   $scope.academicyears = academicyears.data;
               });


               //$http.get(ENV.apiUrl + "api/attendance/getAttendanceCode?cur_code=" + $scope.temp.sims_cur_code).then(function (attendance_code) {
               //$scope.attendance_code = attendance_code.data;
               //$scope.rdbtn = $scope.attendance_code[0].sims_attendance_code;
               //});

           }
           $scope.acdm_yr_change = function () {
               debugger
               $http.get(ENV.apiUrl + "api/HomeRoomAttendance/getHomeRoomBatch?cur_code=" + $scope.edt.sims_cur_code + "&academic_year=" + $scope.edt.sims_academic_year + "&userName=" + user).then(function (grades) {
                   $scope.getsec = grades.data;
                   console.log($scope.getsec);
               });

               // $http.get(ENV.apiUrl + "api/HomeRoomAttendance/getTeacherSubject?academic_year=" + $scope.temp.sims_academic_year + "&username=" + user).then(function (subjects) {
               //  $scope.getsub = subjects.data;
               // console.log($scope.getsec);
               //   });

           }
       }
       $scope.pagesize = "5";
       $scope.pageindex = 0;
       $scope.size = function (str) {
           console.log(str);
           $scope.pagesize = str;
           $scope.currentPage = 1;
           $scope.numPerPage = str;
           console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
       }

       $scope.index = function (str) {
           $scope.pageindex = str;
           $scope.currentPage = str;
           console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
       }

       $scope.student = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

       $scope.makeTodos = function () {
           var rem = parseInt($scope.totalItems % $scope.numPerPage);
           if (rem == '0') {
               $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
           }
           else {
               $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
           }

           var begin = (($scope.currentPage - 1) * $scope.numPerPage);
           var end = parseInt(begin) + parseInt($scope.numPerPage);

           console.log("begin=" + begin); console.log("end=" + end);

           $scope.student = $scope.todos.slice(begin, end);
       };

       $scope.CheckAllCheckedbox = function () {
           debugger
           main = document.getElementById('mainCheckbox');
           if (main.checked == true) {
               for (var i = 0; i < $scope.student.length; i++) {
                   var v = document.getElementById(i + $scope.student[i].studentEnroll);
                   v.checked = true;
                   $scope.row1 = 'row_selected';
                   $('tr').addClass("row_selected");
               }
           }
           else {
               for (var i = 0; i < $scope.student.length; i++) {
                   var v = document.getElementById(i + $scope.student[i].studentEnroll);
                   v.checked = false;
                   main.checked = false;
                   $scope.row1 = '';
               }
           }

       }


       $scope.checkonebyone = function (info) {
           debugger;
           for (var i = 0; i < $scope.student.length; i++) {
               var v = document.getElementById(i + $scope.student[i].studentEnroll);
               if (v.checked == true) {
                   $scope.student[i].ischange = true;
                   //$scope.row1 = 'row_selected';
                   //$('tr').addClass("row_selected");
                   //$scope.color = '#edefef';
               }
               else {
                   $scope.student[i].ischange = false;
                   //$scope.row1 = '';
                   //$('tr').removeClass("row_selected");
                   //$scope.color = '#edefef';
               }
           }

           $("input[type='checkbox']").change(function (e) {
               debugger;
               if ($(this).is(":checked")) { //If the checkbox is checked
                   $(this).closest('tr').addClass("row_selected");
                   //Add class on checkbox checked
                   $scope.color = '#edefef';

               }
               else {
                   $(this).closest('tr').removeClass("row_selected");
                   //Remove class on checkbox uncheck
                   $scope.color = '#edefef';

               }
           });

           main = document.getElementById('mainchk');
           if (main.checked == true) {
               $scope.color = '#edefef';
               $scope.row1 = '';

           }

           //main = document.getElementById('$index');
           //if (main.checked == true) {

           //    main.checked = false;
           //    $scope.color = '#edefef';
           //    $scope.row1 = '';
           //}
       }

       

       //Search Books (Pop-up Window)
       $scope.BookSearch = function () {
           debugger;
           $scope.busy = false;
           $scope.searchtable = false;
           $scope.Flag = false;
           for (var i = 0; i < $scope.student.length; i++) {
               if ($scope.student[i].ischange == true) {
                   $('#myModal').modal('show');

                   $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetLibraryCategory").then(function (CategoryTypes) {
                       $scope.Library_category_code = CategoryTypes.data;
                   });

                   $scope.getSubCat = function (sub_cat) {
                       $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetLibrarySubCategory?SubCategory=" + sub_cat).then(function (SubCategoryTypes) {
                           $scope.Library_subcategory_code = SubCategoryTypes.data;
                       });

                   }

                   $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetLibraryCatelogue").then(function (LibraryCatalogue) {
                       $scope.Library_catalogue_code = LibraryCatalogue.data;
                   });
                   $scope.Flag = true;
               }

           }
           if ($scope.Flag == false) {
               swal('', 'Please Select Student');
           }

           //Book search
           $scope.searchbook = function () {
               $scope.pagesize = "5";
               $scope.pageindex = 0;
               $scope.searchtable = true;
               $scope.busy = true;

               $scope.ImageView1 = false;
               $http.post(ENV.apiUrl + "api/LibraryTransactionDetail/ItemSearch?data=" + JSON.stringify($scope.temp)).then(function (res1) {
                   $scope.busy = true;
                   $scope.booksearch = res1.data;
                   $scope.totalItems = $scope.booksearch.length;
                   $scope.todos = $scope.booksearch;
                   $scope.makeTodos();
                   $scope.busy = false;
                   console.log($scope.booksearch);
                   if (res1.data.length > 0) { }
                   else {
                       $scope.ImageView1 = true;
                   }
               });
               $scope.temp.sims_library_item_category = '';
               $scope.temp.sims_library_item_subcategory = '';
               $scope.sims_library_item_catalogue_code = '';
           }

           //Cancel Button
           $scope.Cancel = function () {

               //$scope.temp = "";
               $('#myModal').modal('hide');
           }

           $scope.size = function (str) {
               console.log(str);
               $scope.pagesize = str;
               $scope.currentPage = 1;
               $scope.numPerPage = str;
               console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
           }

           $scope.index = function (str) {
               $scope.pageindex = str;
               $scope.currentPage = str;
               console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
           }

           $scope.booksearch = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

           $scope.makeTodos = function () {

               var rem = parseInt($scope.totalItems % $scope.numPerPage);
               if (rem == '0') {
                   $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
               }
               else {
                   $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
               }

               var begin = (($scope.currentPage - 1) * $scope.numPerPage);
               var end = parseInt(begin) + parseInt($scope.numPerPage);

               console.log("begin=" + begin); console.log("end=" + end);

               $scope.booksearch = $scope.todos.slice(begin, end);
           };

           $scope.CheckAllChecked1 = function () {
               main = document.getElementById('Checkbox2');

               if (main.checked == true) {
                   for (var i = 0; i < $scope.booksearch.length; i++) {
                       var v = document.getElementById($scope.booksearch[i].sims_library_item_number);
                       v.checked = true;
                   }
               }
               else {

                   for (var i = 0; i < $scope.booksearch.length; i++) {
                       var v = document.getElementById($scope.booksearch[i].sims_library_item_number);
                       v.checked = false;
                       main.checked = false;
                       $scope.row1 = '';
                   }
               }
           }

           $scope.checkonebyone1 = function () {

               $("input[type='checkbox']").change(function (e) {
                   if ($(this).is(":checked")) { //If the checkbox is checked
                       $(this).closest('tr').addClass("row_selected");
                       //Add class on checkbox checked
                       $scope.color = '#edefef';
                   }
                   else {
                       $(this).closest('tr').removeClass("row_selected");
                       //Remove class on checkbox uncheck
                       $scope.color = '#edefef';
                   }
               });

               main = document.getElementById('Checkbox2');
               if (main.checked == true) {
                   main.checked = false;
                   $scope.color = '#edefef';
                   $scope.row1 = '';
               }
           }


           var txnstudent1 = '';
           var item = '';
           $scope.add = function () {
               debugger;
               var data = [];
               $scope.txnstudent1 = '';

               for (var j = 0; j < $scope.booksearch.length; j++) {
                   var v = document.getElementById($scope.booksearch[j].sims_library_item_number);
                   if (v.checked == true) {
                       $scope.txnstudent1 = $scope.txnstudent1 + $scope.booksearch[j].sims_library_item_number + ',';
                   }
               }
               for (var i = 0; i < $scope.student.length; i++) {
                   if ($scope.student[i].ischange == true) {

                       $scope.student[i].studentTransType = 'Issue';
                       try {
                           var cnt = parseInt($scope.student[i].studentBalance);
                           $scope.student[i].studentBalance = cnt;
                       }
                       catch (x) {
                           $scope.student[i].studentBalance = 0;
                       }
                       var arr = $scope.txnstudent1.split(',');
                       var len = 0;
                       for (var a = 0; a < arr.length; a++) {
                           var aa = arr[a].trim();
                           if (aa.length > 0) {
                               len++;
                           }
                       }
                       $scope.student[i].studentBalance = $scope.student[i].studentBalance + len;
                       //studentBookIssueDate = new Date();

                       var eDate = $scope.student[i].studentBookIssueDate;
                       var dt = Date.parse(eDate);
                       dt = new Date(dt);
                       dt.setDate(dt.getDate() + $scope.student[i].studentGraceDays);
                       dt = new Date(dt);
                       $scope.student[i].studentBookExpReturnDate = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                       if ($scope.student[i].sims_library_item_number == undefined || $scope.student[i].sims_library_item_number == null)
                           $scope.student[i].sims_library_item_number = '';
                       var innos = $scope.student[i].sims_library_item_number.split(',');
                       var sel = $scope.txnstudent1.split(',');
                       var final = "";
                       for (var x = 0; x < innos.length; x++) {
                           if (innos[x] != '' && innos[x] != undefined) {
                               if (!sel.includes(innos[x])) {
                                   final = final + innos[x] + ',';
                               }
                           }
                       }
                       final = final + $scope.txnstudent1;

                       //txnstudent1 = txnstudent1 + $scope.booksearch[j].sims_library_item_number + ',';
                       $scope.student[i].sims_library_item_number = final;
                       data.push($scope.student[i]);
                       break;
                   }
               }
               $scope.txnstudent.push(data[0]);
               $scope.student.push(data[0]);
           }
       }

       //Issue Checklist
       $scope.Assign = function () {

           $scope.busy = false;
           $scope.transtable = true;
           $('#myModal1').modal('show');


           //Get Txn Type
           $http.get(ENV.apiUrl + "api/LibraryTransactionDetail/GetLibrary_TxnType").then(function (GetTxn) {
               $scope.Txn_data = GetTxn.data;
           });

           //Remove Students From Transaction List
           $scope.RemoveStudNo = function ($event, index, str) {
               str.splice(index, 1)
           }

           $scope.checkonebyone2 = function (info) {

               $("input[type='checkbox']").change(function (e) {
                   if ($(this).is(":checked")) { //If the checkbox is checked
                       $(this).closest('tr').addClass("row_selected");
                       //Add class on checkbox checked
                       $scope.color = '#edefef';
                   }
                   else {
                       $(this).closest('tr').removeClass("row_selected");
                       //Remove class on checkbox uncheck
                       $scope.color = '#edefef';
                   }
               });

               main = document.getElementById('Checkbox3');
               if (main.checked == true) {
                   main.checked = false;
                   $scope.color = '#edefef';
                   $scope.row1 = '';
               }
           }
       }

       //Get Student Data
       $scope.Preview = function () {
           $scope.busy = true;
           $scope.ImageView = false;
           $scope.grid = true;
           $scope.grid1 = false;
           $http.get(ENV.apiUrl + "api/LibraryTransactionDetail/GetAllLibStudent?cur=" + $scope.temp.sims_cur_code + '&academic=' + $scope.temp.sims_academic_year + '&grade=' + $scope.temp.sims_grade_code + '&section=' + $scope.temp.sims_section_code + '&enroll=' + $scope.temp.search_std_enroll_no).then(function (Allstudent) {
               $scope.student = Allstudent.data;
               //$scope.totalItems = $scope.student.length;
               //$scope.todos = $scope.student;
               //$scope.makeTodos();
               $scope.busy = false;
               console.log($scope.student);
               if (Allstudent.data.length > 0) { }
               else {
                   $scope.ImageView = true;
               }
           });
       }

       $scope.Preview1 = function () {
           if ($scope.edt.sims_homeroom_code == null || $scope.edt.sims_homeroom_code == undefined) {
               swal('', 'Please Select Homeroom');
               return;
           }
           $scope.busy = true;
           $scope.ImageView = false;
           $scope.grid1 = true;
           $scope.grid = false;
           $http.get(ENV.apiUrl + "api/LibraryTransactionDetail/GetAllHomeRoomStudent?cur=" + $scope.edt.sims_cur_code + '&academic=' + $scope.edt.sims_academic_year + '&home=' + $scope.getsec[0].sims_homeroom_code).then(function (Allstudent) {
               $scope.student = Allstudent.data;
               $scope.totalItems = $scope.student.length;
               $scope.todos = $scope.student;
               $scope.makeTodos();
               $scope.busy = false;
               console.log($scope.student);
               if (Allstudent.data.length > 0) { }
               else {
                   $scope.ImageView = true;
               }
           });
       }

       //Reset
       $scope.Reset = function () {
           debugger;
           $state.go($state.current, {}, { reload: true });
       }

       $scope.Details = function (req_code) {

           $http.get(ENV.apiUrl + "api/LibraryTransactionDetail/GetSims_BookDetails?req_code=" + req_code).then(function (item_code) {

               $scope.bookdetails = item_code.data;
               $('#myModal2').modal('show');
               $scope.booktable = true;
           });
       }

       //Submit Books
       savestud = [];
       $scope.submit = function () {
           debugger;
           var item_no = [];
           var no_of_items = [];
           var modulecode = [];
          
           
           //var a = [];
           //a = [5, 11, 17, 5];

           for (var i = 0; i < $scope.student.length; i++) {
               for (var x = i + 1; x < $scope.student.length; x++) {
                   if ($scope.student[i].sims_library_item_number == $scope.student[x].sims_library_item_number && $scope.student[i].sims_library_item_number!= undefined) {
                       swal('', 'Already entered Item/Accession no. ' + $scope.student[x].sims_library_item_number);
                       return;
                   }
               }
           }
           
           for (var i = 0; i < $scope.student.length; i++) {
               if ($scope.student[i]['ischange']) {
                   if ($scope.student[i].sims_library_item_number == undefined || $scope.student[i].sims_library_item_number == null) {
                       swal('', 'Please enter item number for: ' + $scope.student[i].studentName + ' ' + '(' + $scope.student[i].studentEnroll + ')');
                       return;
                   }
                   
               no_of_items = $scope.student[i].sims_library_item_number;
               item_no = no_of_items.split(',');
               for (var j = 0; j < item_no.length; j++) {
                   var fls = false;
                   if (item_no[j] == undefined || item_no[j] == null || item_no[j] == "") {
                       //swal('', 'Please Uncheck Students');
                       fls = true;
                       //break;
                   }
                   else {
                        modulecode = ({
                           'CREATED_BY': user,
                           'ISSUE_DATE': $scope.student[i].studentBookIssueDate,
                           'sims_library_item_number': item_no[j], //$scope.student[i].sims_library_item_number,
                           //'sims_library_item_purchase_price' : $scope.txnstudent[i].sims_library_item_purchase_price
                           'ITEM_QTY': $scope.student[i].studentBookQty,
                           'L_User_No': $scope.student[i].studentLibUserNumber,
                           'REMARK': $scope.student[i].mobiStudentRemarkle_no,
                           'RETURN_DATE': $scope.student[i].studentBookExpReturnDate,
                           'TransType': '08',
                           'TransStatus': 'I'

                       });
                       savestud.push(modulecode);
                   }
               }
           }
       }
               if (savestud.length == 0) {
                   savestud = [];
               }
               else {
                   $http.post(ENV.apiUrl + "api/LibraryTransactionDetail/MakeTransaction", savestud).then(function (savedata) {
                       $scope.msg1 = savedata.data;
                       swal({ text: $scope.msg1.strMessage, timer: 5000 });
                       $scope.Preview();
                   });
                   savestud = [];
                 //  $scope.student = [];
                   //$('#myModal1').modal('hide');
                   main.checked = false;
                 //  $scope.Reset();
               }
               $("#cmb_section_code").multipleSelect("UncheckAll");
          // }
       }

       //Return Books
       var datasend = [];
       $scope.Returnsubmit = function () {
           debugger
           $scope.flag = false;
           for (var i = 0; i < $scope.takenbook.length; i++){
               var v = document.getElementById($scope.takenbook[i].transNo + i);
               if (v.checked == true) {
                   if ($scope.takenbook[i].remARK == undefined)
                       $scope.takenbook[i].remARK = 'Return';
                   var retcode = ({
                       'REMARK': $scope.takenbook[i].remARK,
                       'L_User_No': $scope.takenbook[i].l_User_No,
                       'TransNo': $scope.takenbook[i].transNo,
                       'TransLineNo': $scope.takenbook[i].transLineNo,
                       'opr': 'R',
                       'ITEM_NO': $scope.takenbook[i].iteM_NO,
                       'ITEM_QTY': $scope.takenbook[i].iteM_QTY,
                       'ISSUE_DATE': $scope.takenbook[i].return_DATE1,
                       'CREATED_BY': user,
                       //'StudGrade': $scope.takenbook[i].studentGraceDays,
                       'StudSection': $scope.takenbook[i].studSection,
                       'TransType': '09',
                       //'TransStatus': 'R'
                   });
                   $scope.flag = true;
                   datasend.push(retcode);

               }
           }
           if ($scope.flag == false) {
               swal('', 'Please Select Books');
               exit;
           }
           $http.post(ENV.apiUrl + "api/LibraryTransactionDetail/StudentTrans", datasend).then(function (res) {
               $scope.msg1 = res.data;
               swal({ text: $scope.msg1.strMessage, timer: 5000 });
               $scope.Preview();
           });
           datasend = [];
           $('#myModal5').modal('hide');
           main.checked = false;
           $scope.takenbook = [];
           // $scope.Preview();
       }

      

       $scope.CheckAllChecked = function () {
           debugger
           main = document.getElementById('mainchk');

           if (main.checked == true) {
               for (var i = 0; i < $scope.takenbook.length; i++) {
                   var v = document.getElementById($scope.takenbook[i].transNo + i);
                   v.checked = true;
               }
           }
           else {
               for (var i = 0; i < $scope.takenbook.length; i++) {
                   var v = document.getElementById($scope.takenbook[i].transNo + i);
                   v.checked = false;
                   main.checked = false;
                   $scope.row1 = '';
               }
           }
       }

       $scope.checkonebyonedelete = function () {
           debugger

           $("input[type='checkbox']").change(function (e) {
               if ($(this).is(":checked")) { //If the checkbox is checked
                   $(this).closest('tr').addClass("row_selected");
                   //Add class on checkbox checked
                   $scope.color = '#edefef';
               }
               else {
                   $(this).closest('tr').removeClass("row_selected");
                   //Remove class on checkbox uncheck
                   $scope.color = '#edefef';
               }
           });

           main = document.getElementById('mainchk');
           if (main.checked == true) {
               main.checked = false;
               $scope.color = '#edefef';
               $scope.row1 = '';
           }
       }

       //Re-Issue
       var datasend = [];
       $scope.reissue = function () {

           $scope.flag = false;
           for (var j = 0; j < $scope.student.length; j++) {
               var grcday = $scope.student[j].studentGraceDays;
           }
           for (var i = 0; i < $scope.takenbook.length; i++) {

               var v = document.getElementById($scope.takenbook[i].transNo + i);
               if (v.checked == true) {

                   var riscode = ({
                       'REMARK': $scope.takenbook[i].remARK,
                       'L_User_No': $scope.takenbook[i].l_User_No,
                       'TransNo': $scope.takenbook[i].transNo,
                       'TransLineNo': $scope.takenbook[i].transLineNo,
                       'opr': 'Y',
                       'ITEM_NO': $scope.takenbook[i].iteM_NO,
                       'ITEM_QTY': $scope.takenbook[i].iteM_QTY,
                       'ISSUE_DATE': $scope.takenbook[i].return_DATE1,
                       'CREATED_BY': user,
                       'StudGrade': grcday,
                       'StudSection': $scope.takenbook[i].studSection

                   });
                   $scope.flag = true;
               }
           }
           if ($scope.flag == false) {
               swal('', 'Please Select Books');
               exit;
           }
           datasend.push(riscode);

           $http.post(ENV.apiUrl + "api/LibraryTransactionDetail/StudentTransReissue", datasend).then(function (Reisubmit) {
               $scope.msg1 = Reisubmit.data;
               swal({ text: $scope.msg1.strMessage, timer: 5000 });
           });
           datasend = [];
           $('#myModal5').modal('hide');
       }

       $scope.CheckAllChecked1 = function () {

           main = document.getElementById('Checkbox1');

           if (main.checked == true) {
               for (var i = 0; i < $scope.reissuebook.length; i++) {
                   var v = document.getElementById($scope.reissuebook[i].l_User_No + i);
                   v.checked = true;
               }
           }
           else {
               for (var i = 0; i < $scope.reissuebook.length; i++) {
                   var v = document.getElementById($scope.reissuebook[i].l_User_No + i);
                   v.checked = false;
                   main.checked = false;
                   $scope.row1 = '';
               }
           }
       }

       $scope.checkonebyonedelete1 = function () {

           $("input[type='checkbox']").change(function (e) {
               if ($(this).is(":checked")) { //If the checkbox is checked
                   $(this).closest('tr').addClass("row_selected");
                   //Add class on checkbox checked
                   $scope.color = '#edefef';
               }
               else {
                   $(this).closest('tr').removeClass("row_selected");
                   //Remove class on checkbox uncheck
                   $scope.color = '#edefef';
               }
           });

           main = document.getElementById('Checkbox1');
           if (main.checked == true) {
               main.checked = false;
               $scope.color = '#edefef';
               $scope.row1 = '';
           }
       }

       //Books Taken
       $scope.BooksTaken = function (info, $index) {

           $('#myModal5').modal('show');
           $scope.takenbooktable = true;
           $scope.busy = true;
           //var dt = new Date();
           //$scope.return_DATE1 = (dt.getMonth() + 1) + "/" + dt.getDate() + "/" + dt.getFullYear();
           $scope.ImageView2 = false;
           $http.get(ENV.apiUrl + "api/LibraryTransactionDetail/GetBorrwList?lib_no=" + info.studentLibUserNumber).then(function (GetBorrowList) {
               $scope.takenbook = GetBorrowList.data;
               $scope.busy = false;
               console.log($scope.takenbook);
               if (GetBorrowList.data.length > 0) { }
               else {
                   $scope.ImageView2 = true;
               }
           });

           $scope.takenCancel = function () {
               debugger;
               $('#myModal5').modal('hide');
           }

       }


       $scope.hide1 = function () {
           $scope.grid = false;
           $scope.edt = "";
       }

       //Return book window
       $scope.BookReturn = function () {
           debugger;
           var lib_code=[];
           for (var i=0;i< $scope.student.length; i++)
           {               
               var checkBoxChecked = document.getElementById(i + $scope.student[i].studentEnroll);
               if (checkBoxChecked.checked == true) {
                   lib_code = lib_code + $scope.student[i].studentLibUserNumber + ',';
               }             
           }
           if (lib_code.length == 0) {
               swal('', 'Please fetch/preview data.');
               return;
           }
           console.log("Return All :- "+lib_code);
           $http.get(ENV.apiUrl + "api/LibraryTransactionDetail/GetIssueBorrwList?txn_no=" + "I" + "&libcode=" + lib_code).then(function (item_code) {
               debugger
               $scope.takenbook = item_code.data;
               console.log($scope.takenbook);
               $('#myModal5').modal('show');
               $scope.takenbooktable = true;
           });

       }

       $scope.getitemDetails = function (str,obj) {
            $http.get(ENV.apiUrl + "api/LibraryTransactionDetail/GetBookNameAdd?sims_library_item_number=" + str).then(function (item_no_db) {
               
                debugger
                $scope.item_no_data = item_no_db.data;

                obj['book_name'] = item_no_db.data[0].studentLibraryDesc;
           });
       }

   }])
})();