﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Library');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('LibrarySubcategoryCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.Subcategory_Code = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $(function () {
                $('#cmb_category').multipleSelect({
                    width: '100%',
                    color: 'grey'
                });
            });

            //Select Data SHOW
            $http.get(ENV.apiUrl + "api/LibrarySubcategory/getAllLibrarySubCategory").then(function (res1) {
                $scope.Obj = res1.data;
                $scope.totalItems = $scope.Obj.length;
                $scope.todos = $scope.Obj;
                $scope.makeTodos();

            });

            //Fill Combo catalougecode
            $http.get(ENV.apiUrl + "api/LibrarySubcategory/GetcategaryCodeName").then(function (docstatus1) {

                //$scope.catacode = docstatus1.data;
                //console.log($scope.docstatus1);
                $scope.Library_category_code = docstatus1.data;
                console.log($scope.docstatus1);
                setTimeout(function () {
                    debugger;
                    $('#cmb_category').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });

           

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.Obj;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.Obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.Obj;
                }
                $scope.makeTodos();
                main.checked = false();
                $scope.checkonebyonedelete();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_library_subcategory_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_library_subcategory_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {
                debugger;
                event.target.value = '';
                $scope.disabled = false;
                $scope.Subcategory_Code = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.divcode_readonly = false;
                $scope.subcodecatagory = true;
                $scope.cat_insert = true;
                $scope.cat_update = false;
                $scope.temp = "";
                $scope.temp = [];
                $scope.temp = {};
                $scope.temp['sims_library_attribute_status'] = true;

                //$http.get(ENV.apiUrl + "api/LibrarySubcategory/autoIncrementLibrary").then(function (res) {
                //    $scope.autogendesg = res.data;
                //    $scope.temp = {
                //        sims_library_subcategory_code: $scope.autogendesg[0].sims_library_subcategory_code,
                //    };
                //    console.log($scope.autogendesg);
                //});


            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }


            //DATA EDIT
            $scope.edit = function (str) {
                debugger;
                $scope.Subcategory_Code = true;
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.catageryname = true;
                $scope.Update_btn = true;
                $scope.divcode_readonly = true;
                $scope.subcodecatagory = true;
                $scope.temp = str;
                $scope.cat_update = false;
                $scope.cat_insert = false;
                
                $scope.temp = {
                    sims_library_category_code: str.sims_library_category_code,
                    sims_library_subcategory_code: str.sims_library_subcategory_code,
                    sims_library_subcategory_name: str.sims_library_subcategory_name

                }
            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                debugger
                if (Myform) {
                    var data = $scope.temp;
                    // data.opr = 'I';
                    // datasend.push(data);
                    $scope.datasend = {};
                    $scope.datasend.opr = 'I';
                    $scope.datasend.sims_library_category_code = $scope.temp.sims_library_category_code + "";
                    $scope.datasend.sims_library_subcategory_code = $scope.temp.sims_library_subcategory_code;
                    $scope.datasend.sims_library_subcategory_name = $scope.temp.sims_library_subcategory_name;

                    $http.post(ENV.apiUrl + "api/LibrarySubcategory/CUDLibrarySubCategory", $scope.datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({
                                text: "Record Inserted Successfully", imageUrl: "assets/img/check.png",
                                width: 300, height: 200
                            });
                          
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }


                        $http.get(ENV.apiUrl + "api/LibrarySubcategory/getAllLibrarySubCategory").then(function (res1) {
                            $scope.Obj = res1.data;
                            $scope.totalItems = $scope.Obj.length;
                            $scope.todos = $scope.Obj;
                            $scope.makeTodos();

                            $state.go($state.current, {}, { reload: true });
                        });

                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                debugger
                if (Myform) {
                    var data = $scope.temp;
                    //data.opr = "U";
                    //dataforUpdate.push(data);
                    $scope.dataforUpdate = {};
                    $scope.dataforUpdate.opr = 'U';
                    $scope.dataforUpdate.sims_library_category_code = $scope.temp.sims_library_category_code;
                    $scope.dataforUpdate.sims_library_subcategory_code = $scope.temp.sims_library_subcategory_code;
                    $scope.dataforUpdate.sims_library_subcategory_name = $scope.temp.sims_library_subcategory_name;
                    $http.post(ENV.apiUrl + "api/LibrarySubcategory/CUDLibrarySubCategory", $scope.dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                        $http.get(ENV.apiUrl + "api/LibrarySubcategory/getAllLibrarySubCategory").then(function (res1) {
                            $scope.Obj = res1.data;
                            $scope.totalItems = $scope.Obj.length;
                            $scope.todos = $scope.Obj;
                            $scope.makeTodos();

                        });

                    });
                    dataforUpdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].sims_library_subcategory_code);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + $scope.filteredTodos[i].sims_library_subcategory_code);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.OkDelete = function () {
                debugger;
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById(i + $scope.filteredTodos[i].sims_library_subcategory_code);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_library_category_code': $scope.filteredTodos[i].sims_library_category_code,
                            'sims_library_subcategory_code': $scope.filteredTodos[i].sims_library_subcategory_code,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                        //$scope.deletefin = {};
                        //$scope.deletefin.opr = 'D';
                        //$scope.deletefin.sims_library_category_code = deletemodulecode.sims_library_category_code;
                        //$scope.deletefin.sims_library_subcategory_code = deletemodulecode.sims_library_subcategory_code;
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/LibrarySubcategory/DLibrarySubCategory", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/LibrarySubcategory/getAllLibrarySubCategory").then(function (res1) {
                                                $scope.Obj = res1.data;
                                                $scope.totalItems = $scope.Obj.length;
                                                $scope.todos = $scope.Obj;
                                                $scope.makeTodos();
                                            });

                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/LibrarySubcategory/getAllLibrarySubCategory").then(function (res1) {
                                                $scope.Obj = res1.data;
                                                $scope.totalItems = $scope.Obj.length;
                                                $scope.todos = $scope.Obj;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i + $scope.filteredTodos[i].sims_library_subcategory_code);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
                //    main.checked = false;

            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });


        }])

})();
