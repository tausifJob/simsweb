﻿(function () {
    'use strict';
    var obj1, temp, opr, comp, j;
    var date1, date2;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Library');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('LibraryRequestDetailCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.View_btn = true;
            $scope.display = false;
            $scope.table = true;
            $scope.grid = false;
            $scope.Update_btn = true;
            $scope.show_btn = true;
            $scope.ret_btn = true;
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable2").tableHeadFixer({ 'top': 1 });
            }, 100);

            var user = $rootScope.globals.currentUser.username;

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });


            //Submit button
            $scope.getRequest = function (req_code) {
               
                $scope.Update_btn = true;
                $scope.ImageView = false;
                $http.get(ENV.apiUrl + "api/LibraryRequestDetail/GetSims_LibraryRequestDetail?req_code=" + req_code).then(function (res1) {
                    $scope.CreDiv = res1.data;
                    //swal({ text: $scope.res1.strMessage, timer: 5000 });
                    $scope.totalItems = $scope.CreDiv.length;
                    $scope.todos = $scope.CreDiv;
                    $scope.makeTodos();
                    $scope.grid = true;
                    $scope.makeTodos();
                    //console.log($scope.CreDiv);
                    //if (res1.data.length > 0) { }
                    //else {
                    //    $scope.ImageView = true;
                    //}
                });

            }

            $scope.size = function (str) {
               
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
              $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

               
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };



            //DATA UPDATE
            var dataforUpdate = [];
            $scope.Update = function () {
                
                var flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    if ($scope.filteredTodos[i].sims_library_item_qty == "0") {
                        swal('', 'Please Enter Quantity Atleast 1');
                        //flag = false;
                        return;
                    }
                    if ($scope.filteredTodos[i].sims_library_item_status != 'Completed') {

                        $scope.filteredTodos[i].opr = 'U';
                        $scope.filteredTodos[i];
                        dataforUpdate.push($scope.filteredTodos[i]);
                        flag = true;
                    }
                }

                if (flag == true) {
                    $http.post(ENV.apiUrl + "api/LibraryRequestDetail/CUDLibraryRequestDetail", dataforUpdate).then(function (msg) {

                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            dataforUpdate = [];
                            $scope.filteredTodos = [];
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    })
                }
                else {

                    swal('', 'Request Already Completed');
                }

                $scope.temp = "";
                $scope.totalItems = 0;
                $scope.todos = [];
                $scope.makeTodos();
            }

            $scope.Cancel = function () {
                $('#myModal').modal('hide');
                $scope.reqdetails = [];
                $scope.date_from = '';
                $scope.date_upto = ''
                $scope.sims_library_item_title = '';
                $scope.reqno = '';
                $scope.j = '';
                $scope.requesttable = false;
            }

            $scope.getRequesttxn = function () {

                $('#myModal').modal('show');
                //$scope.temp.sims_library_request_number = '';
                $scope.reqdetails = [];
                $scope.k = '';
                $scope.totalItems = 0;
                $scope.todos = [];
                $scope.makeTodos();
                $scope.show_btn = true;
                $scope.ret_btn = true;
                $scope.requesttable = true;
                $http.get(ENV.apiUrl + "api/LibraryRequestDetail/Get_RequestDetail?user_code=" + user + "&date1=" + $scope.date_from + "&date2=" + $scope.date_upto).then(function (details) {
                    
                    $scope.reqdetails = details.data;
                    $scope.totalItems = $scope.reqdetails.length;
                    $scope.todos = $scope.reqdetails;
                    $scope.makeTodos1();
                    //console.log($scope.reqdetails);
                    //if (details.data.length > 0) { }
                    //else {
                    //    $scope.ImageView = true;
                    //}
                })
            }

            $scope.size = function (str) {
               
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                 $scope.makeTodos1();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos1();
            }

            $scope.reqdetails = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos1 = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

              
                $scope.reqdetails = $scope.todos.slice(begin, end);
            };

            //$scope.searched = function (valLists, toSearch) {
            //    return _.filter(valLists,

            //    function (i) {
            //        /* Search Text in all  fields */
            //        return searchUtil(i, toSearch);
            //    });
            //};

            ////Search
            //$scope.search = function () {
            //    debugger
            //    $scope.todos = $scope.searched($scope.reqdetails, $scope.searchText);
            //    $scope.totalItems = $scope.todos.length;
            //    $scope.currentPage = '1';
            //    if ($scope.searchText == '') {
            //        $scope.todos = $scope.reqdetails;
            //    }
            //    $scope.makeTodos1();
            //}

            //function searchUtil(item, toSearch) {
            //    /* Search Text in all 3 fields */
            //    return (item.sims_library_item_title.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
            //            item.sims_library_item_number ||
            //            item.sims_library_request_number == toSearch) ? true : false;
            //}


            $scope.Show = function () {
                $scope.requesttable = true;
                $http.get(ENV.apiUrl + "api/LibraryRequestDetail/Get_RequestDetail?user_code=" + user + "&date1=" + $scope.date_from + "&date2=" + $scope.date_upto + "&data=" + $scope.sims_library_item_title + "&reqno=" + $scope.reqno).then(function (details) {
                    
                    $scope.reqdetails = details.data;
                    $scope.totalItems = $scope.reqdetails.length;
                    $scope.todos = $scope.reqdetails;
                    $scope.makeTodos1();
                   
                    //if (details.data.length > 0) { }
                    //else {
                    //    $scope.ImageView = true;
                    //}
                })
            }

            $scope.checkonebyonedelete = function (info) {
                for (var i = 0; i < $scope.reqdetails.length; i++) {
                    var d = document.getElementById($scope.reqdetails[i].sims_library_request_number + i);
                    if (d.checked == true) {
                        $scope.reqdetails[i].ischange = true;
                    }
                    else {
                        $scope.reqdetails[i].ischange = false;
                    }
                }

                $("input[type='radio']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('$index');
                if (main.checked == true) {

                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.submit = function () {
               
                var savedata;
                for (var j = 0; j < $scope.reqdetails.length; j++) {
                    var v = document.getElementById($scope.reqdetails[j].sims_library_request_number + j);
                    if (v.checked == true) {
                        savedata = $scope.reqdetails[j].sims_library_request_number;
                        //$scope.temp = $scope.savedata;  
                        $http.get(ENV.apiUrl + "api/LibraryRequestDetail/GetSims_LibraryRequestDetail?req_code=" + savedata).then(function (res2) {
                            $scope.CreDiv = res2.data;
                            //swal({ text: $scope.res1.strMessage, timer: 5000 });
                            $scope.currentPage = 1;
                            $scope.totalItems = $scope.CreDiv.length;
                            $scope.todos = $scope.CreDiv;
                            $scope.makeTodos();
                            $scope.grid = true;
                            $scope.makeTodos();
                            
                            //$scope.makeTodos();
                            $scope.reqdetails = [];
                            $scope.date_from = '';
                            $scope.date_upto = '';
                        });
                    }

                }

                $('#myModal').modal('hide');
            }

            $scope.onlyNumbers = function (event) {
               
                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $scope.Reset = function () {
                $scope.reqdetails = [];
                $scope.totalItems = 0;
                $scope.todos = [];
                $scope.date_from = '';
                $scope.date_upto = ''
                $scope.sims_library_item_title = '';
                $scope.reqno = '';
                $scope.j = '';

                $scope.makeTodos1();

            }
        }])

})();