﻿(function () {

    var simsController = angular.module('sims.module.Library');

    simsController.controller('LibraryBookReviewCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
            
            
            
$scope.display = true;
$scope.show_table = true;
$scope.editor = true;
$scope.book_ratting = true;
$scope.pagesize = '10';
$scope.pageindex = "0";
$scope.pager = true;
$scope.New = true;
            // user name
var user = $rootScope.globals.currentUser.username;
console.log("user=" + $scope.user);
            // current date 

//$scope.date = moment(new Date()).format('DD/MM/YYYY')
//console.log($scope.date = moment(new Date()).format('DD-MM-YYYY'))
//console.log("date=" + $scope.date);

$timeout(function () {
    $("#fixTable").tableHeadFixer({ 'top': 1 });
}, 100);

$scope.size = function (str) {
    //console.log(str);
    //$scope.pagesize = str;
    //$scope.currentPage = 1;
    //$scope.numPerPage = str;
    debugger;
    if (str == "All") {
        $scope.currentPage = '1';
        $scope.filteredTodos = $scope.CreDiv;
        $scope.pager = false;
    }
    else {
        $scope.pager = true;
        $scope.pagesize = str;
        $scope.currentPage = 1;
        $scope.numPerPage = str;
        $scope.makeTodos();
    }
    console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
}

$scope.index = function (str) {
    $scope.pageindex = str;
    $scope.currentPage = str;
    console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
    main.checked = false;
    $scope.CheckAllChecked();
}

$scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

$scope.makeTodos = function () {
    var rem = parseInt($scope.totalItems % $scope.numPerPage);
    if (rem == '0') {
        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
    }
    else {
        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
    }

    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
    var end = parseInt(begin) + parseInt($scope.numPerPage);

    console.log("begin=" + begin); console.log("end=" + end);

    $scope.filteredTodos = $scope.todos.slice(begin, end);
};

  $scope.propertyName = null;
$scope.reverse = false;

$scope.sortBy = function (propertyName) {
    $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
    $scope.propertyName = propertyName;
};

//

$scope.move = function () {
    var elem = document.getElementById("myBar");
    var width = 10;
    var id = setInterval(frame, 10);
    function frame() {
        if (width >= 100) {
            clearInterval(id);
        } else {
            width++;
            elem.style.width = width + '%';
            elem.innerHTML = width * 1 + '%';
        }
    }
}

 // search

$scope.searched = function (valLists, toSearch) {
    return _.filter(valLists,
    function (i) {
        /* Search Text in all  fields */
        return searchUtil(i, toSearch);
    });
};

$scope.search = function () {
    $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
    $scope.totalItems = $scope.todos.length;
    $scope.currentPage = '1';
    if ($scope.searchText == '') {
        $scope.todos = $scope.CreDiv;
    }
    $scope.makeTodos();
    main.checked = false;
    $scope.CheckAllChecked();
}

function searchUtil(item, toSearch) {
    /* Search Text in all 3 fields */
    return (item.sims_library_item_title.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
            item.login_id == toSearch) ? true : false;
}

         ScrollRate = 100;

function scrollDiv_init() {
	DivElmnt = document.getElementById('MyDivName');
	ReachedMaxScroll = false;
	
	DivElmnt.scrollTop = 0;
	PreviousScrollTop  = 0;
	
	ScrollInterval = setInterval('scrollDiv()', ScrollRate);
}

function scrollDiv() {
	
	if (!ReachedMaxScroll) {
		DivElmnt.scrollTop = PreviousScrollTop;
		PreviousScrollTop++;
		
		ReachedMaxScroll = DivElmnt.scrollTop >= (DivElmnt.scrollHeight - DivElmnt.offsetHeight);
	}
	else {
		ReachedMaxScroll = (DivElmnt.scrollTop == 0)?false:true;
		
		DivElmnt.scrollTop = PreviousScrollTop;
		PreviousScrollTop--;
	}
}

function pauseDiv() {
	clearInterval(ScrollInterval);
}

function resumeDiv() {
	PreviousScrollTop = DivElmnt.scrollTop;
	ScrollInterval    = setInterval('scrollDiv()', ScrollRate);
}


//  new button
$scope.New = function () {
    var datasend = [];
    $scope.int = "";
    $scope.show_table = false;
    $scope.display = true;
    $scope.save_btn = false;
    $scope.Update_btn = false;
    $scope.move = true;
    $scope.Evidences = false;
    $scope.BookDetaildisplay = false;
    $scope.book_ratting = true;
}
            //cancle    BooksTaken
$scope.Cancel = function () {
    $scope.temp = "";
    $scope.show_table = false;
    $scope.display = false;
    $scope.int = " ";
    $scope.Responsedisplay = false;
    $scope.Table2 = false;
     
   
}

            //get all book Accession number dropdownlist
$scope.getAllAccessionbylist = function () {
    debugger;
    $http.get(ENV.apiUrl + "api/LibraryBookReview/get_accessionbyList").then(function (Accession_data) {
        $scope.Accessionbylist = Accession_data.data;
        console.log("Accessionbylist = ", $scope.Accessionbylist);
    });
}

            // get grid
//$scope.getgrid = function () {
//    $http.get(ENV.apiUrl + "api/LibraryBookReview/get_grid").then(function (res1) {
//        debugger
//        $scope.CreDiv = res1.data;
//       // scope.create_on_date = db.DBYYYYMMDDformat(CreDiv[0].create_on_date)
//        $scope.totalItems = $scope.CreDiv.length;
//        // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
//        $scope.todos = $scope.CreDiv;
//        $scope.makeTodos();
//        $scope.show_table = true;
//        $scope.display = false;
        
//    });
//}


            //get all select book Details
$scope.changBookData = function () {
    debugger
    // $scope.Table2 = true;
    $scope.int.rating = '';
    $scope.int.comment = '';
    $scope.BookDetaildisplay = true;
    
    if ($scope.int.sims_library_item_accession_number == '' || $scope.int.sims_library_item_accession_number == undefined) {

        $scope.int.sims_library_item_accession_number = '';
        $scope.int.sims_library_item_title = '';
        $scope.int.sims_library_item_author1 = '';
        $scope.int.sims_library_item_name_of_publisher = '';
        $scope.int.sims_library_item_status_code = '';
        $scope.int.sims_library_isbn_number = '';
        $scope.int.sims_library_category_name = '';
        $scope.int.sims_library_subcategory_name = '';
        $scope.int.sims_library_item_place_of_publication = '';
    }
    else {
        $scope.save_btn = true;
        console.log($scope.int.sims_library_item_accession_number);
        $http.get(ENV.apiUrl + "api/LibraryBookReview/get_bookdata?str=" + $scope.int.sims_library_item_accession_number).then(function (res9) {
            debugger
            $scope.CreDiv = res9.data;
            console.log($scope.CreDiv.sims_library_item_accession_number);
            $scope.int.sims_library_item_accession_number = $scope.CreDiv[0].sims_library_item_accession_number;
            $scope.int.sims_library_item_title = $scope.CreDiv[0].sims_library_item_title;
            $scope.int.sims_library_item_author1 = $scope.CreDiv[0].sims_library_item_author1;
            $scope.int.sims_library_item_name_of_publisher = $scope.CreDiv[0].sims_library_item_name_of_publisher;
            $scope.int.sims_library_item_status_code = $scope.CreDiv[0].sims_library_item_status_code;

            $scope.int.sims_library_isbn_number = $scope.CreDiv[0].sims_library_isbn_number;
            $scope.int.sims_library_category_name = $scope.CreDiv[0].sims_library_category_name;
            $scope.int.sims_library_subcategory_name = $scope.CreDiv[0].sims_library_subcategory_name;
            $scope.int.sims_library_item_place_of_publication = $scope.CreDiv[0].sims_library_item_place_of_publication;
            $scope.totalItems = $scope.CreDiv.length;
            // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
            $scope.todos = $scope.CreDiv;
            $scope.makeTodos();
            $scope.book_ratting = true;
        });

        $http.get(ENV.apiUrl + "api/LibraryBookReview/get_grid?str=" + $scope.int.sims_library_item_accession_number).then(function (res1) {
            debugger
            $scope.CreDiv = res1.data;
            // scope.create_on_date = db.DBYYYYMMDDformat(CreDiv[0].create_on_date)
            $scope.totalItems = $scope.CreDiv.length;
            // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
            $scope.todos = $scope.CreDiv;
            $scope.makeTodos();
            $scope.show_table = true;
            $scope.display = false;

        });
    }
}
 
            //insert data 
$scope.save = function (Myform) {
    var datasend = [];
    debugger
    if ($scope.int.sims_library_item_accession_number == '' || $scope.int.sims_library_item_accession_number == undefined) {
        swal('', 'Please select Book');
        return;
    }
    
    if (Myform) {
        debugger
         code =
               {
                   'opr': 'I',
                   'login_id': user,
                   'rating': $scope.int.rating,
                   'comment': $scope.int.comment,
                       'accession_number': $scope.int.sims_library_item_accession_number
                    //'create_on_date': $scope.date
                   }
            datasend.push(code);
        }


        
        console.log(datasend);
        $http.post(ENV.apiUrl + "api/LibraryBookReview/CUDLibBookReview", datasend).then(function (res) {


            $scope.changBookData();

            $scope.msg1 = res.data;

            if ($scope.msg1 == true) {
                swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 300, height: 200 });
              
                $scope.changBookData();
            //$scope.show_table = true;
               }
            else {
                //swal({ title: "Alert", text: "Record NOt Inserted ", showCloseButton: true, width: 300, height: 200 });
                swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 300, height: 200 });
                $scope.changBookData();
            }
        });
    }


            //check all 
$scope.CheckAllChecked = function () {
    debugger;
    main = document.getElementById('mainchk');
    if (main.checked == true) {
        for (var i = 0; i < $scope.filteredTodos.length; i++) {
            var v = document.getElementById($scope.filteredTodos[i].sims_library_item_title + i);
            v.checked = true;
            $('tr').addClass("row_selected");
        }
    }
    else {

        for (var i = 0; i < $scope.filteredTodos.length; i++) {
            var v = document.getElementById($scope.filteredTodos[i].sims_library_item_title + i);
            v.checked = false;
            main.checked = false;
            $scope.row1 = '';
            $('tr').removeClass("row_selected");
        }
    }

}

            // onebyone  check
$scope.checkonebyonedelete = function () {

    $("input[type='checkbox']").change(function (e) {
        if ($(this).is(":checked")) {
            $(this).closest('tr').addClass("row_selected");
            $scope.color = '#edefef';
        } else {
            $(this).closest('tr').removeClass("row_selected");
            $scope.color = '#edefef';
        }
    });

    main = document.getElementById('mainchk');
    if (main.checked == true) {
        main.checked = false;
        $scope.color = '#edefef';
        $scope.row1 = '';
    }
}

                 //delete
$scope.OkDelete = function () {
    debugger
    deletefin = [];
    $scope.flag = false;
    for (var i = 0; i < $scope.filteredTodos.length; i++) {
        var v = document.getElementById($scope.filteredTodos[i].sims_library_item_title + i);
        if (v.checked == true) {
            $scope.flag = true;
            var deletemodulecode = {
                'id': $scope.filteredTodos[i].id,
                opr: 'D'
            }
            deletefin.push(deletemodulecode);
        }
    }
    if ($scope.flag) {
        swal({
            title: '',
            text: "Are you sure you want to Delete? Delete this field Related all record",
            showCloseButton: true,
            showCancelButton: true,
            confirmButtonText: 'Yes',
            width: 380,
            cancelButtonText: 'No',

        }).then(function (isConfirm) {

            if (isConfirm) {
                debugger;
                $http.post(ENV.apiUrl + "api/LibraryBookReview/CUDLibBookReview", deletefin).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            $scope.getgrid();
                            if (isConfirm) {

                                main = document.getElementById('mainchk');
                                if (main.checked == true) {
                                    main.checked = false;
                                }

                                $scope.CheckAllChecked();
                            }
                            $scope.currentPage = true;
                        });
                    }
                    else {
                        swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                $scope.getgrid();
                                main.checked = false;
                                $('tr').removeClass("row_selected");
                            }
                        });
                    }

                });
                deletefin = [];
            }
            else {
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].id + i);
                    if (v.checked == true) {
                        v.checked = false;
                        main.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                }
            }
        });
    }
    else {
        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
    }
    $scope.currentPage = true;
}

            //DATA EDIT  
$scope.edit = function (str) {
    debugger
    $scope.Update_btn = true;
    $scope.gdisabled = false;
    $scope.aydisabled = true;
    $scope.show_table = false;
    $scope.display = true;
    $scope.save_btn = false;
    $scope.BookDetaildisplay = true;
    $scope.book_ratting = true;
    $scope.divcode_readonly = true;

    $scope.BookDetaildisplay = true;


   // $scope.int.sims_library_item_accession_number = str.accession_number

    //if ($scope.int.sims_library_item_accession_number == '' || $scope.int.sims_library_item_accession_number == undefined) {

    //    $scope.int.sims_library_item_accession_number = '';
    //    $scope.int.sims_library_item_title = '';
    //    $scope.int.sims_library_item_author1 = '';
    //    $scope.int.sims_library_item_name_of_publisher = '';
    //    $scope.int.sims_library_item_status_code = '';

    //}
    //else {
         
    //    console.log($scope.int.sims_library_item_accession_number);
    //    $http.get(ENV.apiUrl + "api/LibraryBookReview/get_bookdata?str=" + $scope.int.sims_library_item_accession_number).then(function (res9) {
    //        debugger
    //        $scope.CreDiv = res9.data;
    //        console.log($scope.CreDiv.sims_library_item_accession_number);
    //        $scope.int.sims_library_item_accession_number = $scope.CreDiv[0].sims_library_item_accession_number;
    //        $scope.int.sims_library_item_title = $scope.CreDiv[0].sims_library_item_title;
    //        $scope.int.sims_library_item_author1 = $scope.CreDiv[0].sims_library_item_author1;
    //        $scope.int.sims_library_item_name_of_publisher = $scope.CreDiv[0].sims_library_item_name_of_publisher;
    //        $scope.int.sims_library_item_status_code = $scope.CreDiv[0].sims_library_item_status_code;
    //        $scope.totalItems = $scope.CreDiv.length;
    //        // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
    //        $scope.todos = $scope.CreDiv;
    //        $scope.makeTodos();
    //        $scope.book_ratting = true;
    //    });
    //}
    debugger
    $scope.int = {
          rating: str.rating,
        comment: str.comment,
        id: str.id,
        sims_library_item_accession_number: str.accession_number,
        sims_library_item_title: str.sims_library_item_title,
        sims_library_item_author1: str.sims_library_item_author1,
        sims_library_item_name_of_publisher: str.sims_library_item_name_of_publisher,
        sims_library_item_status_code: str.sims_library_item_status_code,

        sims_library_isbn_number: str.sims_library_isbn_number,
        sims_library_category_name: str.sims_library_category_name,
        sims_library_subcategory_name: str.sims_library_subcategory_name,
        sims_library_item_place_of_publication: str.sims_library_item_place_of_publication,

    }
}

            //DATA UPDATE
var dataforUpdate = [];
$scope.update = function (Myform) {
    if (Myform) {
        debugger;
        var data = $scope.int;
        data.accession_number = $scope.int.sims_library_item_accession_number;
        data.opr = "U";
        data.login_id = user;
        dataforUpdate.push(data);
        //dataupdate.push(data);
        $http.post(ENV.apiUrl + "api/LibraryBookReview/CUDLibBookReview", dataforUpdate).then(function (msg) {
            $scope.msg1 = msg.data;
            if ($scope.msg1 == true) {
                swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 300, height: 200 });
                $scope.getgrid();
            }
            else {
                swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 300, height: 200 });
            }
                                  
            $scope.getgrid();
            //$http.get(ENV.apiUrl + "api/demo/get_demo").then(function (res1) {
            //    $scope.CreDiv = res1.data;
            //    $scope.totalItems = $scope.CreDiv.length;
            //    $scope.todos = $scope.CreDiv;
            //    $scope.makeTodos();

            //});

        });
        dataforUpdate = [];
        $scope.show_table = true;
        $scope.display = false;
    }
}

    $(document).ready(function () {
        $scope.getAllAccessionbylist();
      //  $scope.getgrid();

         });


        }])
})();