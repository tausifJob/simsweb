﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Library');
    simsController.controller('LibraryPrivilegeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };


            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/LibraryPrivilege/getAllLibraryPrivilege").then(function (res1) {
                $scope.CreDiv = res1.data;
                $scope.totalItems = $scope.CreDiv.length;
                $scope.todos = $scope.CreDiv;
                $scope.makeTodos();


            });

            //UserGroupCode
            $http.get(ENV.apiUrl + "api/LibraryPrivilege/GetUserGroupCode").then(function (docstatus1) {

                $scope.UserCode = docstatus1.data;
                
            });


            //UserGroupCode
            $http.get(ENV.apiUrl + "api/LibraryPrivilege/GetMembershipType").then(function (docstatus1) {

                $scope.MembershipType = docstatus1.data;
               
            });



            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
               
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.CreDiv;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
              //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
               
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

               

                $scope.filteredTodos = $scope.todos.slice(begin, end);
                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    $scope.filteredTodos[i].icon = 'fa fa-plus-circle';
                }

            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //$scope.showdate = function (date, name1) {

            //    var month = date.split("/")[0];
            //    var day = date.split("/")[1];
            //    var year = date.split("/")[2];

            //    $scope.temp[name1] = year + "/" + month + "/" + day;
            //}



            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_library_privilege_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_library_duration.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_library_membership_type1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_library_user_group_code1.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {
              
                $scope.disabled = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.usrgrpcode = false;
                $scope.memtype = false;
                $scope.divcode_readonly = false;
                $scope.temp = "";
                var dt = new Date();
                $scope.temp = {};
                $scope.temp.sims_library_no_issue_after = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();
                $scope.temp['sims_library_promote_flag'] = true;
                $scope.temp['sims_library_auto_renew_flag'] = true;
                $scope.temp['sims_library_privilege_status'] = true;
                $scope.temp['sims_library_reservation_flag'] = true;

            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

                $scope.Myform.temp["sims_library_privilege_name"] = "";
                $scope.Myform.temp["sims_library_privilege_number"] = "";
                $scope.Myform.temp["comn_user_group_code"] = "";
                $scope.Myform.temp["sims_library_membership_type_code"] = "";
                $scope.Myform.temp["sims_library_borrow_limit"] = "";
                $scope.Myform.temp["sims_library_allowed_limit"] = "";
                $scope.Myform.temp["sims_library_reservation_limit"] = "";
                $scope.Myform.temp["sims_library_duration"] = "";
                $scope.Myform.temp["sims_library_quota"] = "";
                $scope.Myform.temp["sims_library_grace_days"] = "";
                $scope.Myform.temp["sims_library_reservation_release_limit"] = "";

            }

            //DATA EDIT
            $scope.edit = function (str) {
                
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.divcode_readonly = true;

                $scope.temp = {
                    sims_library_allowed_limit: str.sims_library_allowed_limit,
                    sims_library_auto_renew_flag: str.sims_library_auto_renew_flag,
                    sims_library_borrow_limit: str.sims_library_borrow_limit,
                    sims_library_duration: str.sims_library_duration,
                    sims_library_grace_days: str.sims_library_grace_days,
                    sims_library_no_issue_after: str.sims_library_no_issue_after,
                    sims_library_reservation_release_limit: str.sims_library_reservation_release_limit,
                    sims_library_privilege_number: str.sims_library_privilege_number,
                    sims_library_privilege_status: str.sims_library_privilege_status,
                    sims_library_promote_flag: str.sims_library_promote_flag,
                    sims_library_quota: str.sims_library_quota,
                    sims_library_reservation_flag: str.sims_library_reservation_flag,
                    sims_library_reservation_limit: str.sims_library_reservation_limit,
                    comn_user_group_code: str.sims_library_user_group_code,
                    sims_library_membership_type_code: str.sims_library_membership_type,
                    sims_library_privilege_name: str.sims_library_privilege_name,
                    sims_library_issue_limit_days: str.sims_library_issue_limit_days,
                    sims_library_no_of_times_issue_limit: str.sims_library_no_of_times_issue_limit,
                    sims_library_locking_period: str.sims_library_locking_period
                };

                $scope.privilege_no = true;
                $scope.usrgrpcode = true;
                $scope.memtype = true;

            }

            //DATA SAVE INSERT
            var datasend = [];
            $scope.savedata = function (Myform) {
                if (Myform) {
                    
                    var data = $scope.temp;
                    data.opr = 'I';
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/LibraryPrivilege/CUDLibraryPrivilege", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        }
                        else {
                            
                            swal({ text: "Please change it to Privilege No. already exists", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }

                        $http.get(ENV.apiUrl + "api/LibraryPrivilege/getAllLibraryPrivilege").then(function (res1) {
                            $scope.CreDiv = res1.data;
                            $scope.totalItems = $scope.CreDiv.length;
                            $scope.todos = $scope.CreDiv;
                            $scope.makeTodos();
                        });

                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function () {
                
                var data = $scope.temp;
                data.opr = "U";
                dataforUpdate.push(data);
                //dataupdate.push(data);
                $http.post(ENV.apiUrl + "api/LibraryPrivilege/CUDLibraryPrivilege", dataforUpdate).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                    $http.get(ENV.apiUrl + "api/LibraryPrivilege/getAllLibraryPrivilege").then(function (res1) {
                        $scope.CreDiv = res1.data;
                        $scope.totalItems = $scope.CreDiv.length;
                        $scope.todos = $scope.CreDiv;
                        $scope.makeTodos();
                    });

                });
                dataforUpdate = [];
                $scope.table = true;
                $scope.display = false;
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + 5);

                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i + 5);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';

                    }
                });
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }


            $scope.OkDelete = function () {
                
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {

                    var v = document.getElementById(i + 5);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_library_privilege_number': $scope.filteredTodos[i].sims_library_privilege_number,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/LibraryPrivilege/CUDLibraryPrivilege", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/LibraryPrivilege/getAllLibraryPrivilege").then(function (res1) {
                                                $scope.CreDiv = res1.data;
                                                $scope.totalItems = $scope.CreDiv.length;
                                                $scope.todos = $scope.CreDiv;
                                                $scope.makeTodos();
                                            });
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $http.get(ENV.apiUrl + "api/LibraryPrivilege/getAllLibraryPrivilege").then(function (res1) {
                                                $scope.CreDiv = res1.data;
                                                $scope.totalItems = $scope.CreDiv.length;
                                                $scope.todos = $scope.CreDiv;
                                                $scope.makeTodos();
                                            });

                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                                {
                                                    $scope.row1 = '';
                                                }
                                            }
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById(i + 5);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;

            }

            //Expand function for Record.....
            var dom;
            $scope.flag = true;
            $scope.expand = function (j, $event) {

                if ($scope.flag == true) {
                    $(dom).remove();
                    j.icon = "fa fa-minus-circle";
                    dom = $("<tr id='innerRow'><td class='details' colspan='11'>" +
                        "<table class='inner-table' cellpadding='5' cellspacing='0' width='100%'>" +
                        "<tbody>" +

                         "<tr><td class='semi-bold'>" + "PROMOTE FLAG" + "</td> <td class='semi-bold'>" + "LIBRARY DURATION" + "</td> <td class='semi-bold'>" + "LIBRARY QUOTA" + " </td><td class='semi-bold'>" + "NO ISSUE AFTER" + "</td> <td class='semi-bold'>" + "AUTO REVIEW FLAG" + "</td>" +
                          "<tr> <td>" + (j.sims_library_promote_flag) + "</td> <td>" + (j.sims_library_duration) + "</td> <td>" + (j.sims_library_quota) + "</td> <td>" + (j.sims_library_no_issue_after) + "</td><td>" + (j.sims_library_auto_renew_flag) + "</td>" +
                           "<tr><td class='semi-bold'>" + "GRACE DAY" + "</td> <td class='semi-bold'>" + "PREVILEGE STATUS" + " </td><td class='semi-bold'>" + "RESERVATION FLAG" + "</td><td class='semi-bold'>" + "RELEASE LIMIT" + "</td><td class='semi-bold'>" + "Issue Limit" + "</td><td class='semi-bold'>" + "Number Of Time Issue" + "</td>" +"<td class='semi-bold'>" + "Locking Period" + "</td>"+
                        "</tr>" + "<td>" + (j.sims_library_grace_days) + "</td> <td>" + (j.sims_library_privilege_status) + " </td><td>" + (j.sims_library_reservation_flag) + "</td><td>" + (j.sims_library_reservation_release_limit) + "</td><td>" + (j.sims_library_issue_limit_days) + "</td><td>" + (j.sims_library_no_of_times_issue_limit) + "</td><td>" + (j.sims_library_locking_period)+"</td>"+

                        "</tr>" +

                        " </table>")

                    $($event.currentTarget).parents("tr").after(dom);
                    $scope.flag = false;
                }
                else {
                    $('#innerRow').css({ 'display': 'none' });
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        $scope.filteredTodos[i].icon = "fa fa-plus-circle";
                    }
                    $scope.flag = true;
                }
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: "yyyy-mm-dd"
            });


        }])

})();
