﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, modulecode = [];
    var simsController = angular.module('sims.module.Library');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('LibraryReportAppCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            var active = '';
            var inactive = '';

            $scope.showdisabled = true;

            $timeout(function () {
                $("#example").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.exportData = function () {
                var check = true;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                        alasql('SELECT * INTO XLSX("LibraryDetails.xlsx",{headers:true}) \ FROM HTML("#example",{headers:true,skipdisplaynone:true})');

                        }
                    });
                }
                else {
                    swal({ text: "Report Not Save", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, })
                }
            };

            $scope.items = [{
                "Name": "ANC101",
                "Date": "10/02/2014",
                "Terms": ["samsung", "nokia", "apple"],
                "temp": "ANC101"

            }, {
                "Name": "ABC102",
                "Date": "10/02/2014",
                "Terms": ["motrolla", "nokia", "iPhone"],
                "temp": "ANC102"
            }]

            $scope.busyindicator = false;
            $scope.ShowCheckBoxes = true;

            function getCur(flag, comp_code) {
                if (flag) {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculum_new").then(function (res) {
                        $scope.curriculum = res.data;
                        $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                      //  $scope.getAccYear($scope.edt.sims_cur_code);

                    });
                }

                else {

                    $http.get(ENV.apiUrl + "api/ERP/Grade/getCuriculumfor_Company?comp_code=" + comp_code).then(function (res) {
                        $scope.curriculum = res.data;
                        $scope.edt = { sims_cur_code: $scope.curriculum[0].sims_cur_code };
                      //  $scope.getAccYear($scope.edt.sims_cur_code);

                    });
                }

            }

            $http.get(ENV.apiUrl + "api/ERP/Grade/getAccecssUser?user=" + $rootScope.globals.currentUser.username).then(function (res) {
                $scope.global_count_comp = res.data;

                if ($scope.global_count_comp) {
                    getCur(true, $scope.user_details.comp);


                }
                else {
                    getCur(false, $scope.user_details.comp)
                }
            });

            debugger
            $http.get(ENV.apiUrl + "api/LibraryAttribute/GetCategory").then(function (Catcode) {
                $scope.cat_data = Catcode.data;
                setTimeout(function () {
                    $('#Catcode').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);

            });

            $scope.ChangeSubcategory = function (category) {
                $http.get(ENV.apiUrl + "api/LibraryAttribute/GetSubCategoryByCategory?category=" + category).then(function (Subcatcode) {
                    $scope.subcat = Subcatcode.data;
                });
            }

            $http.get(ENV.apiUrl + "api/LibraryAttribute/GetLibStatus").then(function (res) {
                    $scope.libdata = res.data;

                    setTimeout(function () {
                        $('#issue_return_status').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });      

            debugger
            $http.get(ENV.apiUrl + "api/LibraryAttribute/GetSubCategory").then(function (Subcatcode) {
               $scope.subcat = Subcatcode.data;
              
               setTimeout(function () {
                   $('#SubCatCode').change(function () {
                       console.log($(this).val());
                   }).multipleSelect({
                       width: '100%'
                   });
               }, 1000);              
           });
  
            debugger
            $http.get(ENV.apiUrl + "api/LibraryAttribute/GetCatalogue").then(function (Catalogue) {
                $scope.catalogue = Catalogue.data;

                setTimeout(function () {
                    $('#catalogue').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });
         
            $scope.Show_Data = function (cat, subcat, catalogue, issue_return_status) {
                debugger
                if ($scope.transaction_status == true || $scope.chk_item_no == true || $scope.chk_accession_number == true || $scope.chk_isbn_number == true || $scope.chk_bnn_number == true || $scope.chk_dewey_no == true || $scope.chk_item_title == true || $scope.chk_item_desc == true || $scope.chk_item_category == true || $scope.chk_item_subcategory == true || $scope.chk_catalogue == true || $scope.chk_publication == true || $scope.chk_publisher == true || $scope.chk_date_publication == true || $scope.chk_qr_code == true || $scope.chk_bar_code == true || $scope.chk_purchase_price == true || $scope.chk_copies_available == true || $scope.chk_bin_code == true || $scope.chk_entry_date == true || $scope.chk_location == true || $scope.chk_remark == true || $scope.chk_issue_to_user == true || $scope.chk_library_transaction_date || $scope.chk_library_transaction_date == true || $scope.chk_item_expected_return_date == true || $scope.chk_actual_return_date == true || $scope.chk_issue_return_status==true) {
                    $http.get(ENV.apiUrl + "api/LibraryAttribute/getLibraryReport?category=" + cat + "&subcategory=" + subcat + "&catalogue=" + catalogue + "&status=" + issue_return_status).then(function (res) {
                            $scope.liblist = res.data;
                            console.log($scope.liblist);
                            if ($scope.liblist.length == 0) {
                                swal({ text: "Please Try Again Record Not Found...", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 450, height: 200 });
                                $scope.ShowCheckBoxes = true;
                            }
                            else {
                                $scope.table = true;
                                $scope.busyindicator = true;
                                $scope.ShowCheckBoxes = false;
                            }
                        })
                    }
                    else {
                    swal({ text: "Please Select Atleast One Checkbox Then Try Again...", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 450, height: 200 });
                    }
                }
  
            $scope.back = function () {
                debugger;
                $scope.table = false;
                $scope.busyindicator = false;
                $scope.ShowCheckBoxes = true;

            }

            $scope.checkallClick = function (str) {
                if (str) {
                    $scope.showdisabled = false;
                    $scope.chk_item_no = true;
                    $scope.chk_accession_number = true;
                    $scope.chk_isbn_number = true;
                    $scope.chk_bnn_number = true;
                    $scope.chk_dewey_no = true;
                    $scope.chk_item_title = true;
                    $scope.chk_item_desc = true;
                    $scope.chk_item_category = true;
                    $scope.chk_item_subcategory = true;
                    $scope.chk_catalogue = true;
                    $scope.chk_publication = true;
                    $scope.chk_publisher = true;
                    $scope.chk_date_publication = true;
                    $scope.chk_qr_code = true;
                    $scope.chk_bar_code = true;
                    $scope.chk_purchase_price = true;
                    $scope.chk_copies_available = true;
                    $scope.chk_bin_code = true;
                    $scope.chk_entry_date = true;
                    $scope.chk_location = true;
                    $scope.chk_remark = true;
                    $scope.chk_issue_to_user = true;
                    $scope.chk_library_transaction_date = true;
                    $scope.chk_item_expected_return_date = true;
                    $scope.chk_actual_return_date = true;
                    $scope.transaction_status = true;
                    $scope.chk_issue_return_status = true;
                }
                else {
                    $scope.Uncheck();
                }
            }

            $scope.Uncheck = function () {
                $scope.showdisabled = true;
                $scope.chk_item_no          = false;
                $scope.chk_accession_number = false;
                $scope.chk_isbn_number      = false;
                $scope.chk_bnn_number       = false;
                $scope.chk_dewey_no         = false;
                $scope.chk_item_title       = false;
                $scope.chk_item_desc        = false;
                $scope.chk_item_category    = false;
                $scope.chk_item_subcategory = false;
                $scope.chk_catalogue        = false;
                $scope.chk_publication      = false;
                $scope.chk_publisher        = false;
                $scope.chk_date_publication = false;
                $scope.chk_qr_code          = false;
                $scope.chk_bar_code         = false;
                $scope.chk_purchase_price   = false;
                $scope.chk_copies_available = false;
                $scope.chk_bin_code         = false;
                $scope.chk_entry_date       = false;
                $scope.chk_location         = false;
                $scope.chk_remark           = false;
                $scope.chk_issue_to_user            = false;
                $scope.chk_library_transaction_date = false;
                $scope.chk_item_expected_return_date = false;
                $scope.chk_actual_return_date       = false;
                $scope.transaction_status = false;
                $scope.chk_issue_return_status = false;
            }

            $scope.Reset = function () {
                $scope.Uncheck();
                $scope.select_all = false;
                $scope.edt = {
                    cat: '',
                    subcat: '',
                    catalogue: '',
                }
            }



            //funtion for check event

            $scope.checkevent = function () {
                if ($scope.transaction_status == true || $scope.chk_item_no == true || $scope.chk_accession_number == true || $scope.chk_isbn_number == true || $scope.chk_bnn_number == true || $scope.chk_dewey_no == true || $scope.chk_item_title == true || $scope.chk_item_desc == true || $scope.chk_item_category == true || $scope.chk_item_subcategory == true || $scope.chk_catalogue == true || $scope.chk_publication == true || $scope.chk_publisher == true || $scope.chk_date_publication == true || $scope.chk_qr_code == true || $scope.chk_bar_code == true || $scope.chk_purchase_price == true || $scope.chk_copies_available == true || $scope.chk_bin_code == true || $scope.chk_entry_date == true || $scope.chk_location == true || $scope.chk_remark == true || $scope.chk_issue_to_user == true || $scope.chk_library_transaction_date || $scope.chk_library_transaction_date == true || $scope.chk_item_expected_return_date == true || $scope.chk_actual_return_date == true || $scope.chk_issue_return_status == true) {
                    $scope.showdisabled = false;                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
                }                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               
            else {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
                    $scope.showdisabled = true;
                }
            }
        }])
})();