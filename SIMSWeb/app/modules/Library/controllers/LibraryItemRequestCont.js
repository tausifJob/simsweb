﻿(function () {
    'use strict';
    var obj1, temp, opr, comp, edt;
    var main, savefin = [], savecode = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Library');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('LibraryItemRequestCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter','$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog,$filter, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = false;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.reset_btn = true;
            $scope.display = false;
            $scope.table = true;
            $scope.filteredTodos = [];
            $scope.temp = {};

            var user = $rootScope.globals.currentUser.username;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $timeout(function () {
                $("#fixTable1").tableHeadFixer({ 'top': 1 });
            }, 100);


            $http.get(ENV.apiUrl + "api/LibraryItemRequest/GetLibraryRequestType").then(function (RequestTypes) {

                $scope.Grade_Book_Types = RequestTypes.data;
            });
            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
            var dateyear = $filter('date')(new Date(), 'dd-MM-yyyy');
            $scope.edt = {
                sims_library_request_date: dateyear,
            }

            //var dt = new Date();
            //$scope.edt = { sims_library_request_date: (dt.getDate() + "-" + dt.getMonth() + 1) + "-" + dt.getFullYear() };

            // Form Close
            $scope.close = function () {
                $scope.table = true;
                $scope.display = false;
                $scope.grid2 = false;
                $scope.grid1 = false;
                $scope.save_btn = false;
            }


            //Book Search Window(Search book button)

            $scope.New = function () {
                $('#myModal').modal('show');
                $scope.disabled = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = true;
                $scope.display = false;
                $scope.grid2 = false;
                //$scope.save_btn = true;
                $scope.add_btn = false;
                $scope.divcode_readonly = false;

                $scope.searchbook = function () {
                    debugger;
                    $scope.add_btn = true;
                    //$scope.pagesize = "5";
                    //$scope.pageindex = 0;
                    $scope.searchtable = true;
                    $scope.busy = true;
                    $scope.tempnew = {};
                    if ($scope.temp != undefined) {
                        $scope.tempnew.sims_library_item_accession_number = $scope.temp.sims_library_item_accession_number;
                        $scope.tempnew.sims_library_isbn_number = $scope.temp.sims_library_isbn_number;
                        $scope.tempnew.sims_library_item_category = $scope.temp.sims_library_item_category;
                        $scope.tempnew.sims_library_item_subcategory = $scope.temp.sims_library_item_subcategory;
                        $scope.tempnew.sims_library_item_catalogue_code = $scope.temp.sims_library_item_catalogue_code;
                        $scope.tempnew.sims_library_item_title = $scope.temp.sims_library_item_title;
                                                
                        if ($scope.tempnew.sims_library_item_accession_number == "undefined" || $scope.tempnew.sims_library_item_accession_number == '') {
                            $scope.tempnew.sims_library_item_accession_number = null;
                        }
                        if ($scope.tempnew.sims_library_isbn_number == "undefined" || $scope.tempnew.sims_library_isbn_number == '') {
                            $scope.tempnew.sims_library_isbn_number = null;
                        }
                        if ($scope.tempnew.sims_library_item_category == "undefined" || $scope.tempnew.sims_library_item_category == '') {
                            $scope.tempnew.sims_library_item_category = null;
                        }
                        if ($scope.tempnew.sims_library_item_subcategory == "undefined" || $scope.tempnew.sims_library_item_subcategory == '') {
                            $scope.tempnew.sims_library_item_subcategory = null;
                        }                       
                        if ($scope.tempnew.sims_library_item_catalogue_code == "undefined" || $scope.tempnew.sims_library_item_catalogue_code == '') {
                            $scope.tempnew.sims_library_item_catalogue_code = null;
                        }
                        if ($scope.tempnew.sims_library_item_title == "undefined" || $scope.tempnew.sims_library_item_title == '') {
                            $scope.tempnew.sims_library_item_title = null;
                        }
                    }
                    $scope.ImageView1 = false;
                    $http.post(ENV.apiUrl + "api/LibraryTransactionDetail/ItemSearch", $scope.tempnew).then(function (res1) {
                        $scope.busy = true;
                        $scope.booksearch = res1.data;
                        $scope.totalItems = $scope.booksearch.length;
                        $scope.todos = $scope.booksearch;
                        $scope.makeTodos();
                        $scope.busy = false;
                       
                        if (res1.data.length > 0) { }
                        else {
                            $scope.ImageView1 = true;
                        }
                    });
                    $scope.temp['sims_library_item_category'] = '';
                    $scope.temp['sims_library_item_subcategory'] = '';
                    $scope.sims_library_item_catalogue_code = '';
                }


                $scope.size = function (str) {
                 
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }

                $scope.index = function (str) {
                    $scope.pageindex = str;
                    $scope.currentPage = str;
                    $scope.makeTodos();
                    main.checked = false;
                    $scope.CheckAllChecked();
                }

                $scope.booksearch = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

                $scope.makeTodos = function () {
                    var rem = parseInt($scope.totalItems % $scope.numPerPage);
                    if (rem == '0') {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                    }
                    else {
                        $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                    }

                    var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                    var end = parseInt(begin) + parseInt($scope.numPerPage);

                  

                    $scope.booksearch = $scope.todos.slice(begin, end);
                };

                $scope.searched = function (valLists, toSearch) {
                    return _.filter(valLists,

                    function (i) {
                        /* Search Text in all  fields */
                        return searchUtil(i, toSearch);
                    });
                };

                function searchUtil(item, toSearch) {

                    return (item.sims_library_item_title.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                        || item.sims_library_item_number == toSearch
                        || item.sims_library_item_accession_number == toSearch
                        || item.sims_library_isbn_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                        || item.sims_library_item_category_name == toSearch
                        || item.sims_library_item_subcategory_name == toSearch
                        || item.sims_library_bnn_number == toSearch
                        || item.sims_library_item_dewey_number == toSearch) ? true : false;
                }

                $scope.CheckAllChecked1 = function () {                    
                    for (var i = 0; i < $scope.booksearch.length; i++) {
                        $scope.booksearch[i].ischecked = $scope.ch2selectAll;
                        $scope.booksearch[i].bg = $scope.ch2selectAll ? '#fef8ae' : '';
                    }
                }

                $scope.checkonebyone1 = function (item) {

                    if (item.ischecked) {
                        item.bg = '#fef8ae';
                        var t = true;
                        for (var v = 0; v < $scope.booksearch.length; v++) {
                            t = t && $scope.booksearch[v].ischecked;
                        }
                        $scope.ch2selectAll = t;
                    }
                    else {
                        $scope.ch2selectAll = false;
                        item.bg = '';
                    }
                }

                //Search
                $scope.Record_search = function () {
                    $scope.todos = $scope.searched($scope.BookSearch_Data, $scope.searchText);
                    $scope.totalItems = $scope.BookSearch_Data.length;
                    $scope.currentPage = '1';
                    if ($scope.searchText == '') {
                        $scope.todos = $scope.BookSearch_Data;
                    }
                    $scope.makeTodos();
                }

                $http.get(ENV.apiUrl + "api/LibraryItemRequest/GetLibraryCategory").then(function (CategoryTypes) {
                    $scope.Library_category_code = CategoryTypes.data;
                });

                $scope.getSubCat = function (sub_cat) {
                    $http.get(ENV.apiUrl + "api/LibraryItemRequest/GetLibrarySubCategory?SubCategory=" + sub_cat).then(function (SubCategoryTypes) {
                        $scope.Library_subcategory_code = SubCategoryTypes.data;
                    });
                }

                $http.get(ENV.apiUrl + "api/LibraryItemRequest/GetLibraryCatelogue").then(function (LibraryCatalogue) {
                    $scope.Library_catalogue_code = LibraryCatalogue.data;
                });

                $scope.add = function () {
                    var flg = false;
                    $scope.save_btn = true;
                    $scope.grid2 = true;
                    for (var i = 0; i < $scope.booksearch.length; i++) {
                        flg = false;
                        var v = $scope.booksearch[i].ischecked;
                        if (v == true) {
                            for (var j = 0; j < $scope.filteredTodos.length; j++) {
                                if ($scope.filteredTodos[j].sims_library_item_number == $scope.booksearch[i].sims_library_item_number) {
                                    flg = true;
                                    break;
                                }
                            }
                            if (flg == false) {
                              
                                var ob = {};
                                ob['sims_library_item_qty'] = "0";
                                ob['sims_library_item_number'] = $scope.booksearch[i].sims_library_item_number;
                                ob['sims_library_item_accession_number'] = $scope.booksearch[i].sims_library_item_accession_number;
                                ob['sims_library_isbn_number'] = $scope.booksearch[i].sims_library_isbn_number;
                                ob['sims_library_bnn_number'] = $scope.booksearch[i].sims_library_bnn_number;
                                ob['sims_library_item_dewey_number'] = $scope.booksearch[i].sims_library_item_dewey_number;
                                ob['sims_library_item_title'] = $scope.booksearch[i].sims_library_item_title;
                                ob['sims_library_item_desc'] = $scope.booksearch[i].sims_library_item_desc;
                                ob['sims_library_item_purchase_price'] = $scope.booksearch[i].sims_library_item_purchase_price;
                                ob['sims_library_item_storage_hint'] = $scope.booksearch[i].sims_library_item_storage_hint;
                                ob['sims_library_item_status_name'] = $scope.booksearch[i].sims_library_item_status_name;

                                $scope.filteredTodos.push(ob);

                            }
                        }
                        $scope.booksearch[i].ischecked = false;
                        $scope.booksearch[i].bg = '';
                        $scope.ch2selectAll = false;
                    }

                    $('tr').removeClass("row_selected");
                }

                //Finish Button
                $scope.Cancel = function () {
                    $scope.temp = "";
                    $('#myModal').modal('hide');
                    $scope.booksearch = [];
                    $scope.totalItems = 0;
                    $scope.todos = [];
                    $scope.makeTodos();
                }

                $scope.getcurlevel = function (cur_level) {
                    $http.get(ENV.apiUrl + "api/LibraryItemRequest/GetCurLevelcode?curlevel=" + cur_level).then(function (CurLevelCode) {
                        $scope.Library_cur_level_code = CurLevelCode.data;
                    });
                }

                $scope.getGrade = function (cur_code, level_code) {
                    $http.get(ENV.apiUrl + "api/LibraryItemRequest/GetCurGradecode?cur=" + cur_code + "&level_code=" + level_code).then(function (CurGradeCode) {
                        $scope.Library_grade_code = CurGradeCode.data;
                    });
                }

                //Search button(pop-up window)
                $scope.Search = function () {
                    $scope.display = false;
                    $scope.table = false;
                    $scope.grid2 = true;
                    $scope.grid1 = false;
                    var datasend = [];
                }
            }

            $scope.Delete = function ($event, index, str) {
                str.splice(index, 1);
            }


            $scope.Save = function () {
                savefin = [];
                savecode = [];

                if ($scope.edt.sims_library_request_type == "" || $scope.edt.sims_library_request_type == null) {
                    swal('', 'Please Select Request Type');
                    exit;
                }

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var fls = false;
                    if ($scope.filteredTodos[i].sims_library_item_qty == "0") {
                        swal('', 'Please Enter Quantity Atleast 1');
                        fls = true;
                        break;
                    }
                    savecode = {
                        sims_library_request_type: $scope.edt.sims_library_request_type,
                        sims_library_request_date: $scope.edt.sims_library_request_date,
                        sims_library_request_remarks: $scope.edt.sims_library_request_remarks,
                        sims_library_request_number: $scope.filteredTodos[i].sims_library_request_number,
                        sims_library_request_line_number: $scope.filteredTodos[i].sims_library_request_line_number,
                        sims_library_item_number: $scope.filteredTodos[i].sims_library_item_number,
                        sims_library_item_accession_number: $scope.filteredTodos[i].sims_library_item_accession_number,
                        sims_library_isbn_number: $scope.filteredTodos[i].sims_library_isbn_number,
                        sims_library_bnn_number: $scope.filteredTodos[i].sims_library_bnn_number,
                        sims_library_item_dewey_number: $scope.filteredTodos[i].sims_library_item_dewey_number,
                        sims_library_item_title: $scope.filteredTodos[i].sims_library_item_title,
                        sims_library_item_desc: $scope.filteredTodos[i].sims_library_item_desc,
                        sims_library_item_qty: $scope.filteredTodos[i].sims_library_item_qty,
                        sims_library_item_amount: $scope.filteredTodos[i].sims_library_item_amount,
                        sims_library_item_source_hint: $scope.filteredTodos[i].sims_library_item_source_hint,
                        sims_library_user_code: user,
                        opr: 'I',
                    }
                    savefin.push(savecode);

                }
                if (fls == true) {
                    savefin = [];

                }
                else {
                    $http.post(ENV.apiUrl + "api/LibraryItemRequest/CUDLibraryRequest", savefin).then(function (savedata) {
                        $scope.msg1 = savedata.data;
                        swal({ title: "Alert", text: $scope.msg1, width: 300, height: 200 });
                        $scope.grid2 = '';
                        $scope.edt.sims_library_request_type = '';
                        $scope.edt.sims_library_request_remarks = '';
                        $scope.save_btn = false;
                        savefin = [];
                        $scope.filteredTodos = [];
                    });
                }
            }

            $scope.Reset = function () {
                $scope.edt.sims_library_request_type = '';
                $scope.edt.sims_library_request_remarks = '';
                $scope.filteredTodos = [];
                $scope.save_btn = false;
            }

            $scope.onlyNumbers = function (event) {
                var keys = {
                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };
        }])
})();