﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Library');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('LibraryItemArrivalCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            var username = $rootScope.globals.currentUser.username;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100)

           

            //Select Data SHOW
            //Class Room ComboBoxes
            //$http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
            //    $scope.ComboBoxValues = AllComboBoxValues.data;
            //    $scope.temp = {
            //        sims_academic_year: $scope.ComboBoxValues[0].sims_acadmic_year,
            //        s_cur_code: $scope.ComboBoxValues[0].sims_cur_code,
            //    };               
            //    console.log($scope.ComboBoxValues);
            //});
           

            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;

                $scope.temp.sims_cur_code = $scope.curriculum[0].sims_cur_code;

                console.log($scope.temp.sims_cur_code);
                $scope.getAccYear($scope.temp.sims_cur_code)
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    debugger;
                    $scope.temp.sims_academic_year = $scope.Acc_year[0].sims_academic_year;
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                });

            }

            $scope.getGrade = function (curCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;
                });
            }

            $scope.getSection = function (curCode, gradeCode, accYear) {
                debugger
                $http.get(ENV.apiUrl + "api/StudentReport/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#cmb_section_code').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);

                });
            }
            $(function () {
                $('#cmb_section_code').multipleSelect({
                    width: '100%'
                });                
            });
            $http.get(ENV.apiUrl + "api/LibraryItemArrival/GetNewArrivalItems").then(function (res1) {

                $scope.CreDiv = res1.data;
                $scope.totalItems = $scope.CreDiv.length;
                // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                $scope.todos = $scope.CreDiv;
                $scope.makeTodos();

            });

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };
            
            
            $scope.size = function (str) {
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.CreDiv;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search
            $scope.search = function () {
                debugger;
                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_library_bin_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||

                        item.sims_library_bin_code == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {
                debugger;
                $scope.temp = {};
                $scope.disabled = false;                
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.divcode_readonly = false;
                $scope.temp.sims_library_item_status = true;
                $scope.acccodelist_readonly = true;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                

                //$http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
                //    debugger;
                //    $scope.ComboBoxValues = AllComboBoxValues.data;
                //    $scope.temp = {
                //        sims_academic_year: $scope.ComboBoxValues[0].sims_acadmic_year,
                //        s_cur_code: $scope.ComboBoxValues[0].sims_cur_code,
                //    };
                    
                //    console.log($scope.ComboBoxValues);
                //});
                

                $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                    $scope.curriculum = AllCurr.data;

                    $scope.temp.sims_cur_code = $scope.curriculum[0].sims_cur_code;

                    console.log($scope.temp.sims_cur_code);
                    $scope.getAccYear($scope.temp.sims_cur_code)
                });

                $scope.getAccYear = function (curCode) {
                    $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                        $scope.Acc_year = Acyear.data;
                        debugger;
                        $scope.temp.sims_cur_code = $scope.temp.sims_cur_code;
                        $scope.temp.sims_academic_year = $scope.Acc_year[0].sims_academic_year;
                        $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                    });

                }

                $scope.getGrade = function (curCode, accYear) {
                    $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + curCode + "&academic_year=" + accYear).then(function (Gradecode) {
                        $scope.Grade_code = Gradecode.data;
                    });
                }

                $scope.getSection = function (curCode, gradeCode, accYear) {
                    debugger
                    $http.get(ENV.apiUrl + "api/StudentReport/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                        $scope.Section_code = Sectioncode.data;
                        setTimeout(function () {
                            $('#cmb_section_code').change(function () {
                                console.log($(this).val());
                            }).multipleSelect({
                                width: '100%'
                            });
                        }, 1000);

                    });
                }
                $(function () {
                    $('#cmb_section_code').multipleSelect({
                        width: '100%'
                    });
                });

            }

            //DATA CANCEL
            $scope.Cancel = function () {
                debugger
                $scope.temp = {};
               // $scope.Section_code = [];
                $scope.table = true;
                $scope.display = false;
                $scope.temp.sims_library_item_status = true;
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
                setTimeout(function () {
                    debugger;
                    $('#cmb_section_code').change(function () {
                        // console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 800);
            }

            function addtoList() {
                debugger
                if ($scope.msg==false) {
                    swal('', 'Library Item not exists.');
                    return;
                }
                
                if ($scope.temp.sims_library_item_accession_number_list == undefined) {
                    $scope.temp['sims_library_item_accession_number_list'] = '';
                }

                //if ($http.defaults.headers.common['schoolId'] == 'abps') {
                //    var q = $scope.temp.sims_library_item_accession_number_list.split('/');
                //    if (q.includes('')) {
                //        q.splice(q.indexOf(''), 1);
                //    }
                //    if (q.length < parseInt($scope.temp.sims_library_item_number_of_copies)) {
                //        list = (list + $scope.temp.sims_library_item_accession_number) + '/';
                //        if (q.includes($scope.temp.sims_library_item_accession_number) == false) {

                //            q.push($scope.temp.sims_library_item_accession_number);
                //            //$scope.temp.sims_library_item_accession_number_list = q + '/';
                //            $scope.temp.sims_library_item_accession_number_list = list;
                //        }
                //    }
                //}
                var q = $scope.temp.sims_library_item_accession_number_list.split(',');

                if (q.includes('')) {
                    q.splice(q.indexOf(''), 1);
                }
                $scope.temp.sims_library_item_number_of_copies = "0";
                //if (q.length > $scope.temp.sims_library_item_number_of_copies) {
                    if (q.includes($scope.temp.sims_library_item_accession_number) == false) {
                        q.push($scope.temp.sims_library_item_accession_number);
                        $scope.temp.sims_library_item_accession_number_list = q + '';
                    }
               // }
                $scope.temp.sims_library_item_accession_number = '';
            }

            $scope.Clear = function () {
                $scope.temp.sims_library_item_accession_number_list = '';
                list = '';
            }


            $scope.access_nos = function (event) {
                debugger
                if (event.key == "Tab" || event.key == "Enter") {
                    $scope.accesssearch($scope.temp.sims_library_item_accession_number, addtoList);

                }

            }
            $scope.accesssearch = function (AccessNo, callback) {
                debugger
                $http.post(ENV.apiUrl + "api/LibraryItemMaster/CheckAccNo?AccessNo=" + AccessNo).then(function (AccessionNum) {
                    $scope.msg = AccessionNum.data;
                    $scope.message = $scope.msg;
                    callback.call();
                });

            }

            //DATA EDIT
            $scope.edit = function (str) {
                debugger
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.divcode_readonly = true;

                $scope.temp = {                   
                    sims_cur_code: str.sims_library_cur_code,
                    sims_cur_name: str.sims_library_cur_name,
                    sims_academic_year: str.sims_library_academic_year,
                    sims_grade_code: str.sims_library_grade_code,
                    sims_grade_name: str.sims_library_grade_name,
                    sims_section_code: str.sims_section_name,
                    sims_library_item_accession_number: str.sims_library_item_accession_number,
                    sims_library_item_from_date: str.sims_library_item_from_date,
                    sims_library_item_to_date: str.sims_library_item_to_date,
                    sims_library_item_status: str.sims_library_item_status
                };
            }

            //DATA SAVE INSERT
            var datasend = [];
            var sec_code = [];
            $scope.savedata = function (Myform) {
                debugger
                if (Myform) {

                   
                    var sec = $scope.temp.sims_section_code;
                    sec_code = sec_code + ',' + sec;
                    var str2 = sec_code.substr(sec_code.indexOf(',') + 1);
                                                  
                    var data = {
                        'opr' : 'I',
                        'transactionstatus' : 'I',
                        'sims_library_section': str2,
                        'sims_library_item_accession_number': $scope.temp.sims_library_item_accession_number_list,
                        'sims_library_item_from_date': $scope.temp.sims_library_item_from_date,
                        'sims_library_item_to_date': $scope.temp.sims_library_item_to_date,
                        'sims_library_item_created_by': username,
                        'sims_library_item_created_date': $scope.temp.sims_library_item_created_date,
                        'sims_library_cur_code': $scope.temp.sims_cur_code,
                        'sims_library_academic_year': $scope.temp.sims_academic_year,
                        'sims_library_grade_code': $scope.temp.sims_grade_code,
                        //'sims_library_section: str2,
                        'sims_library_item_status': $scope.temp.sims_library_item_status
                    }
                    datasend.push(data);
                    //console.log(datasend);
                    $http.post(ENV.apiUrl + "api/LibraryItemArrival/CUDNewArrivalItems", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({
                                text: "Record Inserted Successfully", imageUrl: "assets/img/check.png",
                                showCloseButton: true, width: 300, height: 200
                            });
                            $scope.getgrid();
                        }
                        else {
                            swal({ text: "Record Not Inserted.", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }

                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }

            }

            //DATA UPDATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                if (Myform) {

                    var data = $scope.temp;
                    data.opr = "U";
                    dataforUpdate.push(data);
                    //dataupdate.push(data);
                    $http.post(ENV.apiUrl + "api/LibraryItemArrival/CUDNewArrivalItems", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                            $scope.Cancel();
                            $scope.getgrid();
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }

                    });
                    dataforUpdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_library_bin_desc + $scope.filteredTodos[i].sims_library_bin_code + i);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_library_bin_desc + $scope.filteredTodos[i].sims_library_bin_code + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.OkDelete = function () {
                debugger
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_library_bin_desc + $scope.filteredTodos[i].sims_library_bin_code + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_library_bin_code': $scope.filteredTodos[i].sims_library_bin_code,
                            'sims_library_bin_desc': $scope.filteredTodos[i].sims_library_bin_desc,
                            opr: 'D'
                        });
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            debugger;
                            $http.post(ENV.apiUrl + "api/LibraryItemArrival/CUDNewArrivalItems", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                            }

                                            $scope.CheckAllChecked();
                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main.checked = false;
                                            $('tr').removeClass("row_selected");
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }

                            });
                            deletefin = [];
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_library_bin_desc + $scope.filteredTodos[i].sims_library_bin_code + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }

            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/LibraryItemArrival/GetNewArrivalItems").then(function (res1) {
                    $scope.CreDiv = res1.data;
                    $scope.totalItems = $scope.CreDiv.length;
                    $scope.todos = $scope.CreDiv;
                    $scope.makeTodos();

                });
            }

        }])

})();