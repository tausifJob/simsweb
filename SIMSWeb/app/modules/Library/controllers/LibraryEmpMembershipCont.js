﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, savefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Library');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

    simsController.controller('LibraryEmpMembershipCont',
   ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

       $scope.pagesize = "10";
       $scope.pageindex = 0;
       $scope.pagesize1 = "10";
       $scope.pageindex1 = 0;
       $scope.save_btn = true;
       $scope.pager = false;
       $scope.pager1 = false;
       $scope.pageno = false;
       var dataforSave = [];
       $scope.display = false;
       $scope.table = true;
       $scope.searchtable = false;
       $scope.mem = false;
       $scope.grid1 = false;
       var user = $rootScope.globals.currentUser.username;

       var dt = new Date();
       $scope.sims_library_joining_date = dt.getDate() + "-" + (dt.getMonth() + 1) + "-" + dt.getFullYear();

       $timeout(function () {
           $("#fixTable").tableHeadFixer({ 'top': 1 });
       }, 100);

       $timeout(function () {
           $("#fixTable1").tableHeadFixer({ 'top': 1 });
       }, 100);
       //$scope.countData = [

       //          { val: 5, data: 5 },
       //          { val: 10, data: 10 },
       //          { val: 15, data: 15 },

       //]

       $('*[data-datepicker="true"] input[type="text"]').datepicker({
           todayBtn: true,
           orientation: "top left",
           autoclose: true,
           todayHighlight: true
       });

       $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
           $('input[type="text"]', $(this).parent()).focus();
       });



       //$http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
       //    debugger
       //    $scope.ComboBoxValues = AllComboBoxValues.data;
       //    $scope.temp = {
       //        sims_academic_year: $scope.ComboBoxValues[0].sims_acadmic_year,
       //        s_cur_code: $scope.ComboBoxValues[0].sims_cur_code,
       //    };

       //    console.log($scope.ComboBoxValues);
       //});


       $http.get(ENV.apiUrl + "api/LibraryEmpMembership/GetAllLibraryMembershipType").then(function (certificate) {
           $scope.member_data = certificate.data;
       });

       $scope.getgrid = function () {
           $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
               debugger
               $scope.ComboBoxValues = AllComboBoxValues.data;
               $scope.temp = {
                   sims_academic_year: $scope.ComboBoxValues[0].sims_acadmic_year,
                   s_cur_code: $scope.ComboBoxValues[0].sims_cur_code,
               };

               console.log($scope.ComboBoxValues);
           });
       }
       $scope.getgrid();
       // Form Close
       $scope.close = function () {
           $scope.table = true;
           $scope.display = false;
           $scope.grid2 = false;
           $scope.grid1 = false;
           $scope.sims_library_renewed_date = '';
           // $scope.save_btn = false;
       }
       
       //Pages
       
       $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

       $scope.makeTodos = function () {
           var rem = parseInt($scope.totalItems % $scope.numPerPage);
           if (rem == '0') {
               $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
           }
           else {
               $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
           }

           var begin = (($scope.currentPage - 1) * $scope.numPerPage);
           var end = parseInt(begin) + parseInt($scope.numPerPage);

           console.log("begin=" + begin); console.log("end=" + end);

           $scope.filteredTodos = $scope.todos.slice(begin, end);
       };
       
       $scope.size = function (str) {
           debugger;
           if (str == "All") {
               $scope.currentPage = '1';
               $scope.filteredTodos = $scope.student;
               $scope.pager = false;
           }
           else {
               $scope.pager = true;
               $scope.pagesize = str;
               $scope.currentPage = 1;
               $scope.numPerPage = str;
               $scope.makeTodos();
           }
       }

       $scope.index = function (str) {
           $scope.pageindex = str;
           $scope.currentPage = str;
           console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
           main.checked = false;
           $scope.CheckAllChecked();
       }

       ///////////////////Employee Tab
       $scope.searchemployee = function () {
           debugger
           $scope.grid = true;
           $scope.pager = true;
           $scope.pageno = true;
           $scope.searchtableemployee = true;
           $scope.mem = true;
           $scope.curr = true;
           $scope.acad = true;
           $scope.date = true;
           $scope.save = true;
           $scope.ImageView = false;
           if ($scope.temp == undefined || $scope.temp == null) {
               $scope.temp = '';
           }
           //if (name == undefined || name == '\"\"') {
           //    name = '';
           //}
           $http.get(ENV.apiUrl + "api/LibraryEmpMembership/GetEmployeeDetails?emcode=" + $scope.temp.emp_number).then(function (GetEmp) {
               debugger
               $scope.student = GetEmp.data;            
               
               //$scope.totalItems = $scope.student.length;
               //$scope.todos = $scope.student;
               //$scope.makeTodos();
               $scope.busy = false;
               console.log($scope.student);
               if (GetEmp.data.length > 0) {

               }
               else {
                   $scope.ImageView = true;
                   swal({ text: "No Record Found...", imageUrl: "assets/img/close.png", showCloseButton: true, width: 450, height: 200 });
               }

              


           });
       }

       $scope.CheckAllChecked = function () {

           main = document.getElementById('mainchk');

           if (main.checked == true) {
               for (var i = 0; i < $scope.student.length; i++) {
                   var v = document.getElementById($scope.student[i].em_number + i);
                   v.checked = true;
               }
           }
           else {
               for (var i = 0; i < $scope.student.length; i++) {
                   var v = document.getElementById($scope.student[i].em_number + i);
                   v.checked = false;
                   main.checked = false;
                   $scope.row1 = '';
               }
           }
       }

       $scope.checkonebyonedelete = function () {

           $("input[type='checkbox']").change(function (e) {
               if ($(this).is(":checked")) { //If the checkbox is checked
                   $(this).closest('tr').addClass("row_selected");
                   //Add class on checkbox checked
                   $scope.color = '#edefef';
               }
               else {
                   $(this).closest('tr').removeClass("row_selected");
                   //Remove class on checkbox uncheck
                   $scope.color = '#edefef';
               }
           });

           main = document.getElementById('mainchk');
           if (main.checked == true) {
               main.checked = false;
               $scope.color = '#edefef';
               $scope.row1 = '';
           }
       }

       $scope.Saveemp = function () {
           debugger
           savefin = [];
           var flag = false;
           if ($scope.temp.s_cur_code === undefined) {
               swal('', 'Enter Curriculum');
               return;
           }
           if ($scope.temp.sims_academic_year === undefined) {
               swal('', 'Enter Academic Year');
               return;
           }
           if ($scope.temp.sims_library_membership_type_code === undefined) {
               swal('', 'Enter Membership Type');
               return;
           }
           for (var i = 0; i < $scope.student.length; i++) {
               var v = document.getElementById($scope.student[i].em_number + i);
               if (v.checked == true) {
                   var modulecode = ({
                       'sims_employee_code': $scope.student[i].em_number,
                       'sims_library_membership_type': $scope.temp.sims_library_membership_type_code,
                       'sims_library_joining_date': $scope.sims_library_joining_date,
                       'cur_code': $scope.temp.s_cur_code,
                       'academic_year': $scope.temp.sims_academic_year,
                   });
                   flag = true;
                   savefin.push(modulecode);
               }
               //else {
               //    swal('', 'Please Select Employees');
               //    return;
               //}
           }
           if (flag == false) {
               swal('', 'Please Select Records');
               flag = true;
               return;
           }

           $http.post(ENV.apiUrl + "api/LibraryEmpMembership/Updateemp_RenewLibrary", savefin).then(function (savedata) {
               $scope.msg1 = savedata.data;
               if ($scope.msg1 == true) {
                   swal({  text: "Membership Created Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                   savefin = [];
                   $scope.student = [];
                   $scope.getgrid();
               }
               else {
                   swal({ text: "Sorry! Membership Not Created", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                   return;
               }
           });
           savefin = [];
           $('tr').removeClass("row_selected");
           main.checked = false;
           $scope.Reset();
       }
    
       //Cancel Button
       $scope.Reset = function () {
           $scope.pager = false;
           $scope.pager1 = false;
           $scope.pageno = false;
           $scope.mem = false;
           $scope.temp = "";
           $scope.table = true;
           $scope.searchtable = false;
           $scope.grid = false;
           $scope.student = [];
           $scope.edt = "";
          
           $scope.stud = false;
           $scope.grid1 = false;
           $scope.students = [];
           $scope.getgrid();
           //$scope.sims_library_renewed_date = '';
          
       }

       ///////////////////Student Tab

       $scope.students = [], $scope.currentPage1 = 1, $scope.numPerPage1 = 10, $scope.maxSize1 = 10;

       $scope.makeTodos1 = function () {
           var rem = parseInt($scope.totalItems % $scope.numPerPage1);
           if (rem == '0') {
               $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1);
           }
           else {
               $scope.pagersize1 = parseInt($scope.totalItems1 / $scope.numPerPage1) + 1;
           }

           var begin1 = (($scope.currentPage1 - 1) * $scope.numPerPage1);
           var end1 = parseInt(begin1) + parseInt($scope.numPerPage1);

         //  console.log("begin=" + begin); console.log("end=" + end);

           $scope.students = $scope.todos1.slice(begin1, end1);
       };

       $scope.size1 = function (str) {
           debugger;
           if (str == "All") {
               $scope.currentPage1 = '1';
               $scope.filteredTodos1 = $scope.students;
               $scope.pager1 = false;
           }
           else {
               $scope.pager1 = true;
               $scope.pagesize1 = str;
               $scope.currentPage1 = 1;
               $scope.numPerPage1 = str;
               $scope.makeTodos1();
           }
       }

       $scope.index1 = function (str) {
           $scope.pageindex1 = str;
           $scope.currentPage1 = str;
           console.log("currentPage=" + $scope.currentPage); $scope.makeTodos1();
           main.checked = false;
           $scope.CheckAllChecked1();
       }

       $scope.searchstudent = function () {
           debugger;
           $scope.stud = true;
           $scope.grid1 = true;
           $scope.pager1 = true;
           $scope.pager = false;
           $scope.ImageView = false;
           $http.get(ENV.apiUrl + "api/LibraryEmpMembership/GetStudentDetails?enroll=" + $scope.temp.student_data).then(function (GetStud) {
               debugger
               $scope.students = GetStud.data;
               
               //$scope.totalItems1 = $scope.students.length;
               //$scope.todos1 = $scope.students;
               //$scope.makeTodos1();
               $scope.busy = false;
               console.log($scope.students);
               if (GetStud.data.length > 0) {
                   
               }
               else {
                   // $scope.Reset();
                   swal({ text: "No Record Found...", imageUrl: "assets/img/close.png", showCloseButton: true, width: 450, height: 200 });
                   $scope.ImageView = true;
               }
           });
       }

       $scope.CheckAllChecked1 = function () {

           main = document.getElementById('Checkbox1');

           if (main.checked == true) {
               for (var i = 0; i < $scope.students.length; i++) {
                   var v = document.getElementById($scope.students[i].sims_enroll_number + i);
                   v.checked = true;
                   $('tr').addClass("row_selected");
               }
           }
           else {
               for (var i = 0; i < $scope.students.length; i++) {
                   var v = document.getElementById($scope.students[i].sims_enroll_number + i);
                   v.checked = false;
                   main.checked = false;
                   $scope.row1 = '';
                   $('tr').removeClass("row_selected");
               }
           }
       }

       $scope.checkonebyonedelete1 = function () {

           $("input[type='checkbox']").change(function (e) {
               if ($(this).is(":checked")) {
                   $(this).closest('tr').addClass("row_selected");
                   $scope.color = '#edefef';
               } else {
                   $(this).closest('tr').removeClass("row_selected");
                   $scope.color = '#edefef';
               }
           });

           main = document.getElementById('Checkbox1');
           if (main.checked == true) {
               main.checked = false;
               $scope.color = '#edefef';
               $scope.row1 = '';
           }
       }

       $scope.Savestudent= function()
       {
           debugger;
           var dataupdate = [];
           var flag = false;
           if ($scope.temp.s_cur_code === undefined) {
               swal('', 'Enter Curriculum');
               return;
           }
           if ($scope.temp.sims_academic_year === undefined) {
               swal('', 'Enter Academic Year');
               return;
           }
           if ($scope.temp.sims_library_membership_type_code === undefined) {
               swal('', 'Enter Membership Type');
               return;
           }
           for (var i = 0; i < $scope.students.length; i++) {
               var v = document.getElementById($scope.students[i].sims_enroll_number + i);
               if (v.checked == true) {
                   var code = ({
                       'sims_enroll_number': $scope.students[i].sims_enroll_number,
                       'sims_library_membership_type': $scope.temp.sims_library_membership_type_code,
                       'sims_library_joining_date': $scope.sims_library_joining_date,
                       'cur_code': $scope.temp.s_cur_code,
                       'academic_year': $scope.temp.sims_academic_year,
                   });
                   flag = true;
                   dataupdate.push(code);
               }
               //else {
               //    swal('', 'Please Select Employees');
               //    return;
               //}
           }
           if (flag == false) {
               swal('', 'Please Select Records');
               flag = true;
               return;
           }

           $http.post(ENV.apiUrl + "api/LibraryEmpMembership/Updatestudent_RenewLibrary", dataupdate).then(function (savedata) {
               $scope.msg1 = savedata.data;
               if ($scope.msg1 == true) {
                   swal({ text: "Membership Created Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                   
                   $scope.student = [];
                   $scope.getgrid();
               }
               else {
                   swal({ text: "Sorry! Membership Not Created", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                   return;
               }
           });
           dataupdate = [];
           $('tr').removeClass("row_selected");
           main.checked = false;
           $scope.Reset();
       }
   }])

})();