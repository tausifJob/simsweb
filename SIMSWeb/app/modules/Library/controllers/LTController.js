﻿
(function () {
    'use strict';
    var simsController = angular.module('sims.module.Library');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('LibraryTranController',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', '$location', '$anchorScroll', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV, $location, $anchorScroll) {
            // Pre-Required Functions and Variables
            // Start

            $('*[data-datepicker="true"] input[type="text"]').datepicker(
                {
                    todayBtn: true,
                    orientation: "top left",
                    autoclose: true,
                    todayHighlight: true,
                    format: 'yyyy-mm-dd'
                });


            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            var user = $rootScope.globals.currentUser.username;
            var main1;
            $scope.edt = '';
            $scope.isexpanded = false;
            $scope.hide_error_message = false;
            $scope.isshowMemberHis = false;

            $scope.sims_date_for = new Date().getDate() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getFullYear();
            $scope.searchuser_btn = function () {
                $rootScope.visible_parent = false;
                $rootScope.visible_search_parent = false;
                $rootScope.visible_stud = true;
                $rootScope.visible_teacher = true;
                $rootScope.visible_User = true;
                $rootScope.visible_Employee = true;
                $rootScope.chkMulti = false;


                if ($scope.search_user == '') {
                    $scope.global_Search_click();
                    $('#Global_Search_Modal').modal({ backdrop: "static" });
                }
                else {

                    $scope.temp_new_books = [];
                    $http.get(ENV.apiUrl + "api/LibTransaction/GetLibraryDetails?Search_Code=" + $scope.search_user).then(function (userDetails) {
                        if (userDetails.data.length > 0) {
                            $scope.userDetails = userDetails.data[0];
                            $scope.user_details = $scope.userDetails;
                            $scope.user_name = $scope.userDetails.sims_library_user_number + '-';
                            $scope.user_number = $scope.userDetails.sims_library_user_number;
                            $scope.prev_img = $scope.userDetails.sims_library_user_photo;
                            if ($scope.userDetails.sims_library_StudentName != '') $scope.user_name += $scope.userDetails.sims_library_StudentName;
                            if ($scope.userDetails.sims_library_TeacherName != '') $scope.user_name += $scope.userDetails.sims_library_TeacherName;
                            //Get Transactions
                            $scope.isEnabled = false;
                            $scope.fetchTransactions();
                            $scope.isshowMemberHis = true;
                        }
                        else {
                            swal(
                               {
                                   showCloseButton: true,
                                   text: 'Invalid User Name.Please check library membership or validity',
                                   imageUrl: "assets/img/notification-alert.png",
                                   width: 350,
                                   showCloseButon: true
                               });
                            $scope.search_user = '';
                            $scope.isEnabled = true;
                            $scope.reset();
                            $scope.isshowMemberHis = false;
                            var element = document.getElementById('searchuser');
                            if (element) element.focus();
                        }
                    });

                }
            }

            $scope.searchuser = function ($event) {
                if ($event.keyCode == 13) {
                    $scope.onFetchData();
                    $rootScope.visible_parent = false;
                    $rootScope.visible_search_parent = false;
                    $rootScope.visible_stud = true;
                    $rootScope.visible_teacher = true;
                    $rootScope.visible_User = true;
                    $rootScope.visible_Employee = true;
                    $rootScope.chkMulti = false;

                    // $scope.global_Search_click();
                    // $('#Global_Search_Modal').modal({ backdrop: "static" });

                }
            }

            $scope.$on('global_cancel', function (str) {
                var flag = true;

                $scope.list = { sims_comm_user_name: '', sims_comm_recepient_id: '' };
                console.log($scope.SelectedUserLst);
                if ($scope.SelectedUserLst.length > 0) {

                    if ($scope.SelectedUserLst[0]['tab'] == 'tab6')
                        $scope.search_user = $scope.SelectedUserLst[0].em_login_code;
                    else if ($scope.SelectedUserLst[0]['tab'] == 'tab1')
                        $scope.search_user = $scope.SelectedUserLst[0].user_name;
                    else if ($scope.SelectedUserLst[0]['tab'] == 'tab4')
                        $scope.search_user = $scope.SelectedUserLst[0].user_name;
                    else if ($scope.SelectedUserLst[0]['tab'] == 'tab5')
                        $scope.search_user = $scope.SelectedUserLst[0].user_name;
                    else
                        $scope.search_user = '';
                    $scope.onFetchData();
                }
            });


            $scope.search_user = '';
            $scope.user_details = '';
            $scope.remark = '';
            $scope.userDetails = [];
            $scope.isEnabled = true;
            $scope.fetchTransactions = function () {
                $http.get(ENV.apiUrl + "api/LibTransaction/GetLibraryTransactionDetails?Search_Code=" + $scope.user_number).then(function (trans) {
                    $scope.transactions = trans.data;
                    $scope.borrowed_items = $scope.transactions.length;
                    $scope.allowed_items = $scope.user_details.sims_library_borrow_limit - $scope.borrowed_items;
                    for (var x = 0; x < $scope.temp_new_books.length; x++)
                        if ($scope.temp_new_books[x].sims_library_item_accession_number != '')
                            $scope.transactions.push($scope.temp_new_books[x]);

                });
            }

            $scope.prev_img = '';
            $scope.onFetchData = function () {
                // $scope.reset();
                if ($scope.search_user == '') {
                    swal(
                                {
                                    showCloseButton: true,
                                    text: 'Please enter User No.',
                                    width: 350,
                                    showCloseButon: true
                                });
                    $scope.isshowMemberHis = false;

                }
                else {

                    $scope.temp_new_books = [];
                    $http.get(ENV.apiUrl + "api/LibTransaction/GetLibraryDetails?Search_Code=" + $scope.search_user).then(function (userDetails) {
                        if (userDetails.data.length > 0) {
                            $scope.isexpanded = true;
                            $scope.userDetails = userDetails.data[0];
                            $scope.user_details = $scope.userDetails;
                            $scope.user_name = $scope.userDetails.sims_library_user_number + '-';
                            $scope.user_number = $scope.userDetails.sims_library_user_number;
                            $scope.prev_img = $scope.userDetails.sims_library_user_photo;
                            if ($scope.userDetails.sims_library_StudentName != '') $scope.user_name += $scope.userDetails.sims_library_StudentName;
                            if ($scope.userDetails.sims_library_TeacherName != '') $scope.user_name += $scope.userDetails.sims_library_TeacherName;
                            //Get Transactions
                            $scope.isEnabled = false;
                            $scope.fetchTransactions();
                            $scope.isshowMemberHis = true;
                        }
                        else {
                            swal(
                               {
                                   showCloseButton: true,
                                   text: 'Invalid User Name.Please check library membership or validity',
                                   imageUrl: "assets/img/notification-alert.png",
                                   width: 350,
                                   showCloseButon: true
                               });
                            $scope.search_user = '';
                            $scope.isEnabled = true;
                            $scope.reset();
                            $scope.isshowMemberHis = false;
                            var element = document.getElementById('searchuser');
                            if (element) element.focus();
                        }
                    });
                }
            }

            $scope.book_search = '';
            $scope.searchBook_key = function ($event) {
                debugger;
                if ($event.keyCode == 13) {
                    if ($http.defaults.headers.common['schoolId'] == 'abps') {
                        $scope.book_search = $scope.book_search + "/";
                    }
                    $http.get(ENV.apiUrl + "api/LibTransaction/GetBookSearch?Search_Code=" + $scope.book_search + "&graceDays=" + $scope.userDetails.sims_library_duration + "&seldate=" + $scope.sims_date_for).then(function (book_search_data) {
                        debugger;
                        $scope.book_search_data = book_search_data.data;
                        //console.log($scope.book_search_data[0].user_detail);
                        //if ($scope.book_search_data.length > 0 && $scope.book_search_data[0].user_detail !='' && $scope.book_search_data[0].sims_library_item_number !='') {
                        if ($scope.book_search_data.length > 0 && $scope.book_search_data[0].user_detail == '' && $scope.book_search_data[0].sims_library_item_number != '') {
                             var blimit = $scope.user_details.sims_library_borrow_limit;
                            var alimit = $scope.user_details.sims_library_allowed_limit;
                            var actual_new_item_limit = (blimit - $scope.borrowed_items) > alimit ? alimit : (blimit - $scope.borrowed_items);
                            if ($scope.temp_new_books.length != actual_new_item_limit) {
                                var flag = false;
                                for (var x = 0; x < $scope.book_search_data.length; x++) {
                                    flag = false;
                                    var b_acc_no = $scope.book_search_data[x].sims_library_item_accession_number;
                                    for (var u = 0; u < $scope.transactions.length; u++) {
                                        var acc_no = $scope.transactions[u].sims_library_item_accession_number;
                                        if (b_acc_no == acc_no) {
                                            flag = true; break;
                                        }
                                    }
                                    if (flag == false) {
                                        $scope.book_search = '';
                                        $scope.transactions.push($scope.book_search_data[x]);
                                        $scope.temp_new_books.push($scope.book_search_data[x]);
                                    }
                                    else {
                                        $scope.book_search = '';
                                        $scope.hide_error_message = true;
                                        $scope.error_message = "Book Details Already Added";
                                    }
                                }
                            }
                            else {
                                $scope.hide_error_message = true;
                                $scope.error_message = "Allowed limit Exceeded";
                                $scope.book_search = '';
                            }
                        }
                        else {
                            console.log($scope.book_search_data);
                            if ($scope.book_search_data.length == 0) {
                                $scope.hide_error_message = true;
                                $scope.error_message = "Reference Book/Item can not be issued";
                                $scope.book_search = '';
                                return;
                            }
                            $scope.hide_error_message = true;
                            $scope.error_message = "Item Details Not found Or Item Already Issued to User " + $scope.book_search_data[0].user_detail;
                            $scope.book_search = '';
                        }
                    });
                    //$scope.searchBook();
                }
                // $scope.book_search = '';
            }




            $scope.book_showFilter = true;
            $scope.book_filter = function () {
                $scope.book_showFilter = !$scope.book_showFilter;
            }


            $scope.edtbs = {};
            $scope.seachOrderKeyPress = function ($event) {
                if ($event.keyCode == 13) {
                    $scope.search_books();
                }
            }

            $scope.reset_book_filter = function () {
                $scope.edtbs = {};
                $scope.book_search_data1 = '';
                var main = document.getElementById('mainchk');
                main.checked = false;
            }

            $scope.CheckOneByOne = function (info) {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                    }
                });

                var main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    //$scope.color = '#edefef';
                }

            }

            $scope.CheckMultiple = function () {
                var main = document.getElementById('mainchk');
                for (var i = 0; i < $scope.book_search_data1.length; i++) {
                    if (main.checked == true) {
                        $scope.book_search_data1[i].items_check_status = true;
                        $('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else if (main.checked == false) {
                        $('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                        main.checked = false;
                        $scope.book_search_data1[i].items_check_status = false;
                    }
                }
            }

            $scope.search_books = function () {
                debugger;
                var main = document.getElementById('mainchk');
                main.checked = false;
                $http.post(ENV.apiUrl + "api/LibTransaction/booksearchdetails", $scope.edtbs).then(function (book_search_data) {
                    $scope.book_search_data1 = book_search_data.data;
                    if ($scope.book_search_data1.length <= 0) {
                        swal({ text: "No books found.", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }                    
                });
            }

            $scope.temp_new_books = [];

            //$("#booksearch").draggable({
            //    handle: ".modal-header"
            //});
            $scope.searchBook = function () {
                $scope.reset_book_filter();
                $('#booksearch').modal({ backdrop: 'static', keyboard: true });
            }


            $scope.addbooktoIssue = function () {
                debugger
                console.log($scope.book_search_data1);
                $scope.selectedBooks = [];
                for (var i = 0; i < $scope.book_search_data1.length; i++) {
                    if ($scope.book_search_data1[i].items_check_status == true) {
                        $scope.selectedBooks.push($scope.book_search_data1[i].sims_library_item_number);
                    }
                }
                if ($scope.selectedBooks.length <= 0) {
                    swal({ text: "Please select at least one book for issue/reissue.", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    return;
                }
                console.log($scope.selectedBooks);

                for (var c = 0; c < $scope.selectedBooks.length; c++) {
                    $http.get(ENV.apiUrl + "api/LibTransaction/GetBookSearch?Search_Code=" + $scope.selectedBooks[c] + "&graceDays=" + $scope.userDetails.sims_library_duration + "&seldate=" + $scope.sims_date_for).then(function (book_search_data) {
                        $scope.book_search_data = book_search_data.data;
                        if ($scope.book_search_data.length > 0) {
                            var blimit = $scope.user_details.sims_library_borrow_limit;
                            var alimit = $scope.user_details.sims_library_allowed_limit;
                            var actual_new_item_limit = (blimit - $scope.borrowed_items) > alimit ? alimit : (blimit - $scope.borrowed_items);
                            if ($scope.temp_new_books.length != actual_new_item_limit) {
                                var flag = false;
                                for (var x = 0; x < $scope.book_search_data.length; x++) {
                                    flag = false;
                                    var b_acc_no = $scope.book_search_data[x].sims_library_item_accession_number;

                                    for (var u = 0; u < $scope.transactions.length; u++) {
                                        var acc_no = $scope.transactions[u].sims_library_item_accession_number;
                                        if (b_acc_no == acc_no) {
                                            flag = true; break;
                                        }

                                    }
                                    if (flag == false) {
                                        $scope.book_search = '';
                                        $scope.transactions.push($scope.book_search_data[x]);
                                        $scope.temp_new_books.push($scope.book_search_data[x]);
                                    }
                                    else {
                                        $scope.book_search = '';
                                        $scope.hide_error_message = true;
                                        $scope.error_message = "Book Details Already Added";
                                    }
                                }
                            }
                            else {
                                $scope.hide_error_message = true;
                                $scope.error_message = "Allowed limit Exceeded";
                            }
                        }
                        else {
                            $scope.hide_error_message = true;
                            $scope.error_message = "Item Details Not found Or Item Already Issue to User Or Reference Book/Item";

                        }
                    });
                }
                $('#booksearch').modal('hide');

            }


            $scope.date_change = function (str) {
                // $scope.transactions.sims_library_transaction_date = str;
                for (var i = 0; i < $scope.transactions.length; i++) {
                    for (var j = 0; j < $scope.temp_new_books.length; j++) {
                        if ($scope.transactions[i]['sims_library_item_accession_number'] == $scope.temp_new_books[j]['sims_library_item_accession_number']) {
                            $scope.transactions[i]['sims_library_transaction_date'] = str;
                            $scope.temp_new_books[j]['sims_library_transaction_date'] = str;
                        }
                    }
                }
            }
            $scope.timeInMs = 10000;

            /* BOOK SEARCH FILTERS */
            /*BOOK CUR*/
            $http.get(ENV.apiUrl + "api/LibTransaction/getBSCur").then(function (search_book_cur) {
                $scope.search_book_cur = search_book_cur.data;
            });

            /*BOOK CUR*/
            $http.get(ENV.apiUrl + "api/LibTransaction/booksearchdetails").then(function (search_book_style) {
                $scope.search_book_style = search_book_style.data;
            });

            /*BOOK Lang*/
            $http.get(ENV.apiUrl + "api/LibTransaction/getBSLanguage").then(function (search_book_lang) {
                $scope.search_book_lang = search_book_lang.data;
            });

            /*BOOK Cat*/
            $http.get(ENV.apiUrl + "api/LibTransaction/getBSCategory").then(function (search_book_cat) {
                $scope.search_book_cat = search_book_cat.data;
            });

            $scope.onCat_chagne = function () {
                $http.get(ENV.apiUrl + "api/LibTransaction/getBSSubCategory?cat_Code=" + $scope.edtbs.sims_library_item_category).then(function (search_book_subcat) {
                    $scope.search_book_subcat = search_book_subcat.data;
                });

            }


            /* BOOK SEARCH FILTERS */


            //--------
            $scope.temp_new_books = [];

            $scope.timeInMs = 10000;

            var countUp = function () {
                $scope.hide_error_message = false;
                $timeout(countUp, 10000);
            }
            $timeout(countUp, 10000);

            $scope.removeNewBooks = function (bookDetails) {
                $scope.transactions.pop(bookDetails);
                $scope.temp_new_books.pop(bookDetails);
            }

            $scope.returnBook = function (info) {
                debugger;
                info.sims_library_transaction_date = $scope.sims_date_for;
                console.log($scope.info);
                $http.post(ENV.apiUrl + "api/LibTransaction/BookReturn", info).then(function (result) {
                    if (result.data) {
                        swal(
                        {
                            showCloseButton: true,
                            text: 'Book/Item Returned successfully',
                            imageUrl: "assets/img/check.png",
                            width: 350,
                            showCloseButon: true
                        });
                        $scope.fetchTransactions();
                    }
                });
            }

            $scope.reissueBook = function (info) {
                info.sims_library_transaction_date = $scope.sims_date_for;
                $http.post(ENV.apiUrl + "api/LibTransaction/BookReIssue?days=" + $scope.userDetails.sims_library_grace_days, info).then(function (result) {
                    if (result.data) {
                        swal(
                        {
                            showCloseButton: true,
                            text: ' Book/Item Reissued successfully',
                            imageUrl: "assets/img/check.png",
                            width: 350,
                            showCloseButon: true
                        });
                        $scope.fetchTransactions();
                        $scope.reset();
                    }
                    else {
                        $scope.hide_error_message = true;
                        $scope.error_message = "This book is not allowed for Re-Issue.";
                    }
                });
            }

            $scope.issueBooks = function () {
                debugger
                if ($scope.temp_new_books.length <= 0) {
                    $scope.hide_error_message = true;
                    $scope.error_message = "There are no New book(s) to Issue";
                }
                else {
                    // New Book Issues
                    $scope.create_transaction = {
                        sims_library_user_number: '', sims_library_transaction_date: '', sims_library_transaction_type: '',
                        sims_library_transaction_remarks: '', sims_library_transaction_total: '', sims_library_transaction_created_by: ''
                    }
                    $scope.create_transaction.sims_library_user_number = $scope.search_user;
                    $scope.create_transaction.sims_library_transaction_date = $scope.sims_date_for; //(new Date());
                    $scope.create_transaction.sims_library_transaction_type = '01'.toString();
                    $scope.create_transaction.sims_library_transaction_remarks = $scope.remark;
                    $scope.create_transaction.sims_library_transaction_created_by = $rootScope.globals.currentUser.username;
                    $http.post(ENV.apiUrl + "api/LibTransaction/Insert_NewBooks", $scope.create_transaction).then(function (result) {
                        if (result.data) {
                            $scope.remark = '';
                            $scope.book_search = '';
                            var tran_no = result.data;
                            //Details
                            console.log($scope.temp_new_books);
                            for (var x = 0; x < $scope.temp_new_books.length; x++) {
                                $scope.temp_new_books[x].sims_library_transaction_number = tran_no;
                                $scope.temp_new_books[x].sims_library_transaction_line_number = (x + 1);
                            }
                            //Transaction Details Entry
                            $scope.temp_new_books.sims_library_transaction_date = $scope.sims_date_for;
                            $http.post(ENV.apiUrl + "api/LibTransaction/InsertTransactionDetails", $scope.temp_new_books).then(function (result) {
                                swal(
                                {
                                    showCloseButton: true,
                                    text: 'Books Issued Successfully. Your Transaction No is :' + tran_no,
                                    imageUrl: "assets/img/check.png",
                                    width: 350,
                                    showCloseButon: true
                                });
                                $scope.temp_new_books = [];
                                debugger;
                                //$scope.fetchTransactions();                               
                                $scope.reset();
                            });
                        }
                    });
                }
            }


            $scope.book_history = function (search_code, info) {
                $http.get(ENV.apiUrl + "api/LibTransaction/GetBookSearchHistoryDetails?Search_Code=" + search_code).then(function (book_search_histroy_data) {
                    $('#MyModal').modal({ backdrop: 'static', keyboard: true });
                    $scope.history = book_search_histroy_data.data;
                    $scope.title = info.sims_library_item_title;
                    $scope.accession_no = info.sims_library_item_accession_number;
                });
            }
            $scope.win_member_history = function (info) {
                if ($scope.isshowMemberHis) {
                    $http.get(ENV.apiUrl + "api/LibTransaction/GetMemberSearchHistoryDetails?Search_Code=" + $scope.user_number).then(function (member_search_histroy_data) {
                        $scope.member_history = member_search_histroy_data.data;
                    });
                    $('#MemberModel').modal({ backdrop: 'static', keyboard: true });
                }
            }
            $scope.reset = function () {
                $scope.isshowMemberHis = false;
                $scope.user_details = [];
                $scope.temp_new_books = [];
                $scope.book_search = '';
                $scope.search_user = '';
                $scope.remark = '';
                $scope.user_details = '';
                $scope.borrowed_items = '';
                $scope.allowed_items = '';
                $scope.user_name = '';
                $scope.userDetails = [];
                $scope.isEnabled = false;
                $scope.transactions = [];
                $scope.prev_img = '';
                $scope.sims_date_for = new Date().getDate() + "-" + (new Date().getMonth() + 1) + "-" + new Date().getFullYear();
            }


            /* RETURN MULTIPLE BOOKS*/
            $scope.return_items = function () {
                $scope.return_itemlist = [];
                $scope.temp.sims_library_item_accession_number = '';
                $scope.temp.sims_library_isbn_number = '';
                $scope.temp.sims_library_item_name = '';
                $scope.temp.sims_library_item_title = '';

                $('#ReturnBooks').modal({ backdrop: "static" });
            }

            $scope.reset_returnbook = function () {
                $scope.return_itemlist = [];
                $scope.temp = {};
            }

            $scope.temp = {};
            $scope.searchissue_books = function () {
                debugger;
                if ($scope.temp.sims_library_item_accession_number == "undefined" || $scope.temp.sims_library_item_accession_number == '') {
                    $scope.temp.sims_library_item_accession_number = null;
                }
                $scope.tempnew = {};
                $scope.tempnew.sims_library_item_accession_number = $scope.temp.sims_library_item_accession_number;
                $scope.tempnew.sims_library_isbn_number = $scope.temp.sims_library_isbn_number;
                $scope.tempnew.sims_student_enroll_number = $scope.temp.sims_student_enroll_number;
                $scope.tempnew.sims_library_item_title = $scope.temp.sims_library_item_title;
                $scope.tempnew.sims_library_item_name = $scope.temp.sims_library_item_name;
                $scope.tempnew.studentname = $scope.temp.studentname;
                $scope.tempnew.sims_library_item_bar_code = $scope.temp.sims_library_item_bar_code;
                $http.post(ENV.apiUrl + "api/LibTransaction/UseritemSearch", $scope.tempnew).then(function (item_return) {
                    $scope.return_itemlist = item_return.data;
                    if ($scope.return_itemlist.length <= 0) {
                        swal({ text: "No books found.", imageUrl: "assets/img/notification-alert.png", width: 300, height: 200 });
                    }

                });
            }

            $scope.CheckAllChecked = function () {
                debugger
                main1 = document.getElementById('check1');
                if (main1.checked == true) {
                    for (var i = 0; i < $scope.return_itemlist.length; i++) {
                        var v = document.getElementById($scope.return_itemlist[i].sims_library_transaction_number + $scope.return_itemlist[i].sims_library_transaction_line_number + i);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.return_itemlist.length; i++) {
                        var v = document.getElementById($scope.return_itemlist[i].sims_library_transaction_number + $scope.return_itemlist[i].sims_library_transaction_line_number + i);
                        v.checked = false;
                        main1.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }
            }


            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main1 = document.getElementById('check1');
                if (main1.checked == true) {
                    main1.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            $scope.returnall_books = function () {
                debugger;
                var item_code = [];
                for (var i = 0; i < $scope.return_itemlist.length; i++) {
                    var v = document.getElementById($scope.return_itemlist[i].sims_library_transaction_number + $scope.return_itemlist[i].sims_library_transaction_line_number + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var modulecode = ({
                            'sims_library_item_number': $scope.return_itemlist[i].sims_library_item_number,
                            'sims_library_transaction_number': $scope.return_itemlist[i].sims_library_transaction_number,
                            'sims_library_transaction_line_number': $scope.return_itemlist[i].sims_library_transaction_line_number,
                            'sims_library_transaction_created_by': $scope.return_itemlist[i].sims_library_transaction_created_by,
                            'sims_library_transaction_remarks': $scope.return_itemlist[i].sims_library_transaction_remarks,
                        });
                        item_code.push(modulecode);
                    }
                    
                }

                console.log(item_code);
                $http.post(ENV.apiUrl + "api/LibTransaction/Returnitem", item_code).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Items Returned Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, })

                        main1 = document.getElementById('check1');
                        if (main1.checked == true) {
                            main1.checked = false;
                            $scope.CheckAllChecked();
                        }

                        $('#ReturnBooks').modal('hide');
                        $scope.return_itemlist = [];
                        $scope.reset();
                    }
                    else {
                        swal({ text: "Items Not Returned", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, })
                        main1.checked = false;
                        $('tr').removeClass("row_selected");
                    }
                });
            }

            //Events End
        }])
})();




