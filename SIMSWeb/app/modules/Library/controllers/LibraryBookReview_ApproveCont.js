﻿(function () {

    var simsController = angular.module('sims.module.Library');

    simsController.controller('LibraryBookReview_ApproveCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {
     
            
            $scope.book_ratting = true;
            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;



            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.CreDiv;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            //

            $scope.move = function () {
                var elem = document.getElementById("myBar");
                var width = 10;
                var id = setInterval(frame, 10);
                function frame() {
                    if (width >= 100) {
                        clearInterval(id);
                    } else {
                        width++;
                        elem.style.width = width + '%';
                        elem.innerHTML = width * 1 + '%';
                    }
                }
            }

            // search

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,
                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {
                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_library_item_title.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_library_item_title.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.$index == toSearch) ? true : false;
            }



            //check all 
            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk11');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            //checked one by one
            var ind;
            var data = [];
            $scope.checkonebyonedelete = function (str, index) {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                        //str.status = true;
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                        //str.status = false;
                    }
                });
                str.status = str.datastatus;
                ind = index;
                //data = str;
            }

           //  get grid
            $scope.getgrid = function () {
                $http.get(ENV.apiUrl + "api/LibraryBookReviewApprove/get_grid").then(function (res1) {
                    debugger
                    $scope.CreDiv = res1.data;
                     // scope.create_on_date = db.DBYYYYMMDDformat(CreDiv[0].create_on_date)
                    $scope.totalItems = $scope.CreDiv.length;
                    // $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                    $scope.todos = $scope.CreDiv;
                    $scope.makeTodos();
                    $scope.show_table = true;
                    $scope.display = false;

                });
            }

            $scope.getgrid();
                

            //insert data 
            $scope.save = function (Myform) {
                var datasend = [];
                debugger
                if ($scope.int.sims_library_item_accession_number == '' || $scope.int.sims_library_item_accession_number == undefined) {
                    swal('', 'Please select Book');
                    return;
                }

                if (Myform) {
                    debugger
                    code =
                          {
                              'opr': 'I',
                              'login_id': user,
                              'rating': $scope.int.rating,
                              'comment': $scope.int.comment,
                              'accession_number': $scope.int.sims_library_item_accession_number
                              //'create_on_date': $scope.date
                          }
                    datasend.push(code);
                }



                console.log(datasend);
                $http.post(ENV.apiUrl + "api/LibraryBookReview/CUDLibBookReview", datasend).then(function (res) {


                    $scope.changBookData();

                    $scope.msg1 = res.data;

                    if ($scope.msg1 == true) {
                        swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 300, height: 200 });

                        $scope.changBookData();
                        //$scope.show_table = true;
                    }
                    else {
                        //swal({ title: "Alert", text: "Record NOt Inserted ", showCloseButton: true, width: 300, height: 200 });
                        swal({ title: "Alert", text: $scope.msg1.strMessage, showCloseButton: true, width: 300, height: 200 });
                        $scope.changBookData();
                    }
                });
            }
          
            // update
            $scope.update = function () {
                debugger;
                var send = [];
                code = [];
                $scope.flag = false;
             
                
              
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById('mainchk11' + $scope.filteredTodos[i].accession_number + i);


                        if (v.checked == true) {
                            //if ($scope.filteredTodos[i].approve_status == false || $scope.filteredTodos[i].approve_status == '' || $scope.filteredTodos[i].approve_status == undefined) {

                            //    $scope.approve_status = 'I'

                            //}
                            //else {
                            //    $scope.approve_status = 'A'
                            //}
                            //if ($scope.filteredTodos[i].display_cmt_flag == false || $scope.filteredTodos[i].display_cmt_flag == '' || $scope.filteredTodos[i].display_cmt_flag == undefined) {

                            //    $scope.display_cmt_flag = 'I'

                            //}
                            //else {
                            //    $scope.display_cmt_flag = 'A'
                            //}


                         code = {
                                'opr': 'U',
                                'id': $scope.filteredTodos[i].id,
                                'approve_status': $scope.filteredTodos[i].approve_status,
                                'display_cmt_flag': $scope.filteredTodos[i].display_cmt_flag,
                                'comment': $scope.filteredTodos[i].comment,
                                 }

                         
                            $scope.flag = true;
                            send.push(code);
                        }
                    }
                    if ($scope.flag == false) {
                        swal('', 'Please select the records to update');
                        return;
                    }
               
                    $http.post(ENV.apiUrl + "api/LibraryBookReviewApprove/CUDLibBookReviewApprove", send).then(function (res) {
                    $scope.msg1 = res.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 300, height: 200 });
                        $scope.getgrid();
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 300, height: 200 });
                    }
                   
                    $scope.getgrid();

                });
                $scope.display = false;
                $scope.table = true;
                $scope.save_btn = false;
                $scope.getgrid();
            }


            //delete
            $scope.OkDelete = function () {
                debugger
                deletefin = [];
                $scope.flag = false;
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById('mainchk11' + $scope.filteredTodos[i].accession_number + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = {
                            'id': $scope.filteredTodos[i].id,
                            opr: 'D'
                        }
                        deletefin.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete? Delete this field Related all record",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {

                        if (isConfirm) {
                            debugger;
                            $http.post(ENV.apiUrl + "api/LibraryBookReviewApprove/CUDLibBookReviewApprove", deletefin).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        $scope.getgrid();
                                        if (isConfirm) {

                                            main = document.getElementById('mainchk11');
                                            if (main.checked == true) {
                                                main.checked = false;
                                            }

                                            $scope.CheckAllChecked();
                                        }
                                        $scope.currentPage = true;
                                    });
                                }
                                else {
                                    swal({ text: "Record Not Deleted", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                            $scope.getgrid();
                                            main.checked = false;
                                            $('tr').removeClass("row_selected");
                                        }
                                    });
                                }

                            });
                            deletefin = [];
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].id + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.currentPage = true;
            }

        }])
})();