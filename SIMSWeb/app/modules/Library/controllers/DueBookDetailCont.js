﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Library');
    simsController.controller('DueBookDetailCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.export_btn = false;
            $scope.showStudHeading = true;
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);
            $scope.temp = {};
            $scope.temp.sims_grade_code = [];
            $scope.temp.sims_section_code = [];


            //CUR
            $http.get(ENV.apiUrl + "api/common/getCuriculum").then(function (AllCurr) {
                $scope.curriculum = AllCurr.data;
                $scope.temp = {
                    'sims_cur_code': $scope.curriculum[0].sims_cur_code
                }
                $scope.getAccYear($scope.temp.sims_cur_code)
            });

            $scope.getAccYear = function (curCode) {
                $http.get(ENV.apiUrl + "api/common/getAcademicYear?curCode=" + curCode).then(function (Acyear) {
                    $scope.Acc_year = Acyear.data;
                    debugger;
                    $scope.temp = {
                        'sims_cur_code': $scope.curriculum[0].sims_cur_code,
                        'sims_academic_year': $scope.Acc_year[0].sims_academic_year
                    }
                    $scope.getGrade($scope.temp.sims_cur_code, $scope.temp.sims_academic_year);
                });

            }

            //Grade
            $scope.getGrade = function (curCode, accYear) {
                debugger;
                $http.get(ENV.apiUrl + "api/common/getAllGrades?cur_code=" + $scope.temp.sims_cur_code + "&academic_year=" + $scope.temp.sims_academic_year).then(function (Gradecode) {
                    $scope.Grade_code = Gradecode.data;

                    setTimeout(function () {
                        $('#Select2').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
                
                $scope.getSection($scope.temp.sims_cur_code, $scope.temp.sims_grade_code, $scope.temp.sims_academic_year)
            }

            //Section
            $scope.getSection = function (curCode, gradeCode, accYear) {
                $http.get(ENV.apiUrl + "api/common/getSectionFromGrade?cur_code=" + curCode + "&grade_code=" + gradeCode + "&academic_year=" + accYear).then(function (Sectioncode) {
                    $scope.Section_code = Sectioncode.data;
                    setTimeout(function () {
                        $('#Select10').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });
            }



            $http.get(ENV.apiUrl + "api/LibraryAttribute/getAllDuedetail").then(function (res1) {
                $scope.DueDetail = res1.data;
                console.log($scope.DueDetail);
            });

          

            $scope.Submit=function()
            {
                debugger;
                var grade_code = '';
                var section_code = '';
                for (var i = 0; i < $scope.temp.sims_grade_code.length; i++) {
                    grade_code += $scope.temp.sims_grade_code[i] + ',';
                    
                };
                for (var j = 0; j < $scope.temp.sims_section_code.length; j++) {
                    section_code += $scope.temp.sims_section_code[j] + ',';

                };
                $scope.IP = {
                    'cur_code': $scope.temp.sims_cur_code,
                    'academic_year': $scope.temp.sims_academic_year,
                    'grade': grade_code,
                    'section': section_code
                };

                debugger;
                $http.post(ENV.apiUrl + "api/LibraryAttribute/InsertAllDuedetail", $scope.IP).then(function (res) {
                   
                    $scope.rows = res.data;
                    $scope.Allrows = res.data;
                    if ($scope.Allrows.length <= 0) {
                        swal({ title: "Alert", text: "Records not found", showCloseButton: true, width: 380, });
                        $scope.export_btn = false;
                    }
                    else {
                        for (var r in $scope.rows) {
                            $scope.rows[r]['sims_icon'] = "fa fa-plus-circle";
                            $scope.rows[r]['isexpanded'] = "none";
                            $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].sims_library_transaction_user_code);
                        }
                        $scope.export_btn = true;
                    }
                    console.log($scope.rows);
                    console.log($scope.Allrows);
                });
            }
            
            $scope.Reset = function ()
            {
                debugger
                $scope.temp = {
                    sims_grade_code: '',
                    sims_section_code:''
                }
                $scope.rows = '';
                $scope.Allrows = '';
            }
           

            function getSubitems(dno) {
                var arr = [];
                for (var i = 0; i < $scope.Allrows.length; i++) {
                    if ($scope.Allrows[i].sims_library_transaction_user_code == dno) {
                        arr.push($scope.Allrows[i]);
                    }
                }
                return arr;
            }

            $scope.expand = function (j, $event) {
                console.log(j);
                if (j.isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.isexpanded = "grid";
                    console.log(j);
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.isexpanded = "none";
                }

                if (j.subItems[0].sims_library_user_group_type == "Stduent") {
                    $scope.showStudHeading = true;
                }
                else {
                    $scope.showStudHeading = false;

                }
            }

            $scope.reset = function () {
                $scope.IP = {};
                $scope.rows = {};
                $scope.Allrows = {};
            }




            $scope.size = function (str) {
                console.log(str);
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str;
                console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_library_catalogue_code.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sims_library_catalogue_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.acno == toSearch) ? true : false;
            }

           

            $scope.export = function () {
                var check = true;
                debugger;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + "DueBookDetail.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {

                            var blob = new Blob([document.getElementById('printdata').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + " DueBookDetail" + ".xls");
                            //alasql('SELECT * INTO XLSX("Report.xlsx",{headers:true}) \ FROM HTML("#printdata",{headers:true,skipdisplaynone:true})');

                        }
                    });
                }
                else {
                    swal({ title: "Alert", text: "Report Not Save", showCloseButton: true, width: 380, })
                }

            }



        }])

})();
