﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, savefin = [], deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Library');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('LibraryItemSearchCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.save_btn = false;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = false;
            $scope.grid = true;
            $scope.filter_btn = true;
            $scope.filterhide_btn = false;
            $scope.Accessno = false;
            $scope.accessnolabel = false;
            $scope.list = false;
            $scope.clr_btn = false;
            $scope.acccodelist_readonly = true;
            $scope.pager = true;
           // var user = $rootScope.globals.currentUser.username;

            var user=''

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $(function () {
                $('#cmb_catalogue').multipleSelect({
                    width: '100%'
                });
            });

            $(function () {
                $('#cmb_category').multipleSelect({
                    width: '100%'
                });
            });

            $(function () {
                $('#cmb_subcategory').multipleSelect({
                    width: '100%'
                });
            });

            //$scope.countData = [

            //      { val: 5, data: 5 },
            //      { val: 10, data: 10 },
            //      { val: 15, data: 15 },

            //]

            $scope.show_filter = function () {
                $scope.table = true;
                $scope.filter_btn = false;
                $scope.filterhide_btn = true;
            }

            $scope.hide_filter = function () {
                $scope.table = false;
                $scope.filter_btn = true;
                $scope.filterhide_btn = false;
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }


                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);


                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            // Form Close
            $scope.close = function () {
                $scope.table = true;
                $scope.display = false;
                $scope.grid2 = false;
                $scope.grid1 = false;
                $scope.save_btn = false;
            }

            $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetLibraryCatelogue").then(function (LibraryCatalogue) {
                $scope.Library_catalogue_code = LibraryCatalogue.data;
               
                setTimeout(function () {
                   
                    $('#cmb_catalogue').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });

            $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetLibraryCategory").then(function (CategoryTypes) {
                $scope.Library_category_code = CategoryTypes.data;
               
                setTimeout(function () {
                   
                    $('#cmb_category').change(function () {
                        console.log($(this).val());
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });

            $scope.getSubCat = function (sub_cat) {
               
                $http.get(ENV.apiUrl + "api/LibraryItemSearch/GetLibrarySubCategory?SubCategory=" + sub_cat).then(function (SubCategoryTypes) {
                    $scope.Library_subcategory_code = SubCategoryTypes.data;
                  
                    setTimeout(function () {
                       
                        $('#cmb_subcategory').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });

            }

            $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetBookStyle").then(function (BookStyleCode) {
                $scope.BookStyle_code = BookStyleCode.data;
            });

            $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetBookStatus").then(function (BookStatus) {
                $scope.Book_Status = BookStatus.data;
            });

            $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetSims_Location").then(function (GetLocation) {
                debugger;
                $scope.Location = GetLocation.data;
            });

            $scope.getBins = function (bin) {
                debugger
                $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetBin?location_code=" + bin).then(function (GetBin) {
                $scope.Bin_Status = GetBin.data;
            });
            }



           

            $scope.Cancelled = function () {
                $('#myModal').modal('hide');
                $scope.searchtable = false;
                $scope.info = "";
            }

            //Show Data  
            $scope.Preview = function () {
              
                $scope.tempnew = {};
                if ($scope.temp != undefined) {
                    $scope.tempnew.sims_library_item_number = $scope.temp.sims_library_item_number;
                    $scope.tempnew.sims_library_isbn_number = $scope.temp.sims_library_isbn_number;
                    $scope.tempnew.sims_library_item_accession_number = $scope.temp.sims_library_item_accession_number;
                    $scope.tempnew.sims_library_item_title = $scope.temp.sims_library_item_title;
                    $scope.tempnew.sims_library_item_desc = $scope.temp.sims_library_item_desc;
                    $scope.tempnew.sims_library_item_category = $scope.temp.sims_library_item_category + "";
                    $scope.tempnew.sims_library_item_subcategory = $scope.temp.sims_library_item_subcategory + "";
                    $scope.tempnew.sims_library_item_catalogue_code = $scope.temp.sims_library_item_catalogue_code + "";
                    $scope.tempnew.sims_library_item_author1 = $scope.temp.sims_library_item_author1;
                    $scope.tempnew.sims_library_item_name_of_publisher = $scope.temp.sims_library_item_name_of_publisher;
                    $scope.tempnew.sims_library_item_bin_code = $scope.temp.sims_library_item_bin_code;
                    $scope.tempnew.sims_library_item_status_code = $scope.temp.sims_library_item_status_code;
                    $scope.tempnew.sims_library_item_bar_code = $scope.temp.sims_library_item_bar_code;
                    $scope.tempnew.sims_library_item_location_code = $scope.temp.sims_library_item_location_code;
                    if ($scope.tempnew.sims_library_item_number == "undefined" || $scope.tempnew.sims_library_item_number == '') {
                        $scope.tempnew.sims_library_item_number = null;
                    }
                    if ($scope.tempnew.sims_library_isbn_number == "undefined" || $scope.tempnew.sims_library_isbn_number == '') {
                        $scope.tempnew.sims_library_isbn_number = null;
                    }
                    if ($scope.tempnew.sims_library_item_accession_number == "undefined" || $scope.tempnew.sims_library_item_accession_number == '') {
                        $scope.tempnew.sims_library_item_accession_number = null;
                    }
                    if ($scope.tempnew.sims_library_item_title == "undefined" || $scope.tempnew.sims_library_item_title == '') {
                        $scope.tempnew.sims_library_item_title = null;
                    }
                    if ($scope.tempnew.sims_library_item_desc == "undefined" || $scope.tempnew.sims_library_item_desc == '') {
                        $scope.tempnew.sims_library_item_desc = null;
                    }

                    if ($scope.tempnew.sims_library_item_catalogue_code == "undefined" || $scope.tempnew.sims_library_item_catalogue_code == '') {
                        $scope.tempnew.sims_library_item_catalogue_code = null;
                    }
                    if ($scope.tempnew.sims_library_item_category == "undefined" || $scope.tempnew.sims_library_item_category == '') {
                        $scope.tempnew.sims_library_item_category = null;
                    }
                    if ($scope.tempnew.sims_library_item_subcategory == "undefined" || $scope.tempnew.sims_library_item_subcategory == '') {
                        $scope.tempnew.sims_library_item_subcategory = null;
                    }
                    if ($scope.tempnew.sims_library_item_catalogue_code == "undefined" || $scope.tempnew.sims_library_item_catalogue_code == '') {
                        $scope.tempnew.sims_library_item_catalogue_code = null;
                    }
                    if ($scope.tempnew.sims_library_item_author1 == "undefined" || $scope.tempnew.sims_library_item_author1 == '') {
                        $scope.tempnew.sims_library_item_author1 = null;
                    }
                    if ($scope.tempnew.sims_library_item_name_of_publisher == "undefined" || $scope.tempnew.sims_library_item_name_of_publisher == '') {
                        $scope.tempnew.sims_library_item_name_of_publisher = null;
                    }
                    if ($scope.tempnew.sims_library_item_bin_code == "undefined" || $scope.tempnew.sims_library_item_bin_code == '') {
                        $scope.tempnew.sims_library_item_bin_code = null;
                    }
                    if ($scope.tempnew.sims_library_item_status_code == "undefined" || $scope.tempnew.sims_library_item_status_code == '') {
                        $scope.tempnew.sims_library_item_status_code = null;
                    }
                    if ($scope.tempnew.sims_library_item_bar_code == "undefined" || $scope.tempnew.sims_library_item_bar_code == '') {
                        $scope.tempnew.sims_library_item_bar_code = null;
                    }
                    if ($scope.tempnew.sims_library_item_location_code == "undefined" || $scope.tempnew.sims_library_item_location_code == '') {
                        $scope.tempnew.sims_library_item_location_code = null;
                    }
                }
                $http.post(ENV.apiUrl + "api/LibraryItemSearch/ItemSearch", $scope.tempnew).then(function (Allstudent) {
                    $scope.filteredTodos = Allstudent.data;
                    $scope.count = $scope.filteredTodos.length;
                    if ($scope.filteredTodos.length != 0) {
                        $scope.totalItems = $scope.filteredTodos.length;
                        $scope.todos = $scope.filteredTodos;
                        $scope.makeTodos();
                        $scope.pager = true;
                    }
                    else {
                        $scope.pager = false;
                    }
                    //if ($scope.filteredTodos.length > 0) {
                    //    $scope.table = true;
                    //    $scope.pager = true;
                    //    if ($scope.countData.length > 3) {
                    //        $scope.countData.splice(3, 1);
                    //        $scope.countData.push({ val: $scope.filteredTodos.length, data: 'All' })
                    //    }
                    //    else {
                    //        $scope.countData.push({ val: $scope.filteredTodos.length, data: 'All' })
                    //    }
                    //    $scope.totalItems = $scope.filteredTodos.length;
                    //    $scope.todos = $scope.filteredTodos;
                    //    $scope.makeTodos();
                    //}
                    //else {
                    //    $scope.table = false;
                    //    swal({ title: "Alert", text: "Record Not Found", width: 300, height: 200 });
                    //    $scope.filteredTodos = [];
                    //}


                    //$scope.filteredTodos = Allstudent.data;
                    //$scope.totalItems = $scope.filteredTodos.length;
                    //$scope.todos = $scope.filteredTodos;
                    //$scope.makeTodos();
                    //$scope.busy = false;
                    //$scope.searchtable = true;
                    //console.log($scope.filteredTodos);
                    //if (Allstudent.data.length > 0) { }
                    //else {
                    //    swal({ title: "Alert", text: "Sorry! No data found", showCloseButton: true, width: 300, height: 200 });
                    //}
                });
            }

            

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                //console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                }

                if (str == "All") {
                    $scope.currentPage = 1;
                    $scope.numPerPage = $scope.obj.length;
                    $scope.makeTodos();

                }
                    else {
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                //$scope.pageindex = str;
                //$scope.currentPage = str;
                //console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                //// main.checked = false;
                //$scope.CheckAllChecked();
                $scope.pageindex = str;
                $scope.currentPage = str;
                $scope.makeTodos();

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.row1 = '';
                    $scope.color = '#edefef';
                    }
            }


            //DATA CANCEL
            $scope.Cancel = function () {
              
                $scope.temp = "";
                $scope.table = true;
                $scope.display = false;
                $scope.list = false;
                $scope.clr_btn = false;

            }

            //Reset
            $scope.Reset = function () {
                $scope.temp = '';
                $scope.tempnew = '';
                $scope.filteredTodos = [];
                $scope.count = '';
                $scope.CreDiv = [];
                $scope.pager = false;
                $state.go($state.current, {}, { reload: true });
            }

            //Navigate To Item Master
            $scope.New = function () {
                $state.go('main.Sim121', { 'ob': true });
            }

            $scope.onlyNumbers = function (event) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $scope.onlyNumbers1 = function (event) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $scope.onlyNumbers2 = function (event) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 45,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57,
                    'A': 65, 'B': 66, 'C': 67, 'D': 68, 'E': 69, 'F': 70, 'G': 71, 'H': 72, 'I': 73, 'J': 74, 'K': 75, 'L': 76, 'M': 77,
                    'N': 78, 'O': 79, 'P': 80, 'Q': 81, 'R': 82, 'S': 83, 'T': 84, 'U': 85, 'V': 86, 'W': 87, 'X': 88, 'Y': 89, 'Z': 90,
                    'a': 97, 'b': 98, 'c': 99, 'd': 100, 'e': 101, 'f': 102, 'g': 103, 'h': 104, 'i': 105, 'j': 106, 'k': 107, 'l': 108, 'm': 109, 'n': 110, 'o': 111,
                    'p': 112, 'q': 113, 'r': 114, 's': 115, 't': 116, 'u': 117, 'v': 118, 'w': 119, 'x': 120, 'y': 121, 'z': 122
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };


            $scope.Export = function () {
              
                    var check = true;

                    if (check == true) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Save " + $scope.obj1.lic_school_name + " LibraryItems.xls ?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 390,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                              
                                var blob = new Blob([document.getElementById('printdata3').innerHTML], {
                                    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                                });
                                saveAs(blob, $scope.obj1.lic_school_name + " LibraryItems" + ".xls");
                            }
                        });
                    }
                    else {
                        swal({ text: "Report Not Save", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, })
                    }
                
            }

           $scope.Preview();
        }])

})();