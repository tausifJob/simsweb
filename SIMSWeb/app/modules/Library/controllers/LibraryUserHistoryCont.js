﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, savefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Library');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    $(function () {
        $('#grade_box').multipleSelect({ width: '100%' });
        $('#section_box').multipleSelect({ width: '100%' });

    });
    simsController.directive('ngFiles', ['$parse', function ($parse) {

        function fn_link(scope, element, attrs) {
            var onChange = $parse(attrs.ngFiles);
            element.on('change', function (event) {
                onChange(scope, { $files: event.target.files });
            });
        };

        return {
            link: fn_link
        }
    }])

    simsController.controller('LibraryUserHistoryCont',
   ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

       $scope.pagesize = "10";
       $scope.pageindex = 0;
       $scope.pagesize1 = "10";
       $scope.pageindex1 = 0;
       $scope.save_btn = true;
       $scope.pager = false;
       $scope.pager1 = false;
       $scope.pageno = false;
       var dataforSave = [];
       $scope.display = false;
       $scope.table = true;
       $scope.searchtable = false;
       $scope.mem = true;
       $scope.grid1 = true;
       $scope.temp = {};
       //var user = $rootScope.globals.currentUser.username;
       var user = $rootScope.globals.currentUser.username;
        $scope.empuser = user;
       //console.log("My USER: ",$scope.empuser);

       var dt = new Date();
       $scope.sims_library_joining_date = dt.getDate() + "-" + (dt.getMonth() + 1) + "-" + dt.getFullYear();

       $timeout(function () {
           $("#fixTable").tableHeadFixer({ 'top': 1 });
       }, 100);

       $timeout(function () {
           $("#fixTable1").tableHeadFixer({ 'top': 1 });
       }, 100);
       



       $http.get(ENV.apiUrl + "api/LibraryUserHistory/getCuriculumLib").then(function (res1) {
           debugger;
           $scope.sims_cur = res1.data;
           //  if (res1.data.length > 0) {

           //.temp.sims_cur_code = $scope.sims_cur[0].sims_cur_code;

           $scope.temp = {
               'sims_cur_code': $scope.sims_cur[0].sims_cur_code
           }
           $scope.getAccYear($scope.sims_cur[0].sims_cur_code);
          // }
       });

       $scope.getAccYear = function (cur_code) {
           debugger
           $http.get(ENV.apiUrl + "api/LibraryUserHistory/getAcademicYear?curCode=" + cur_code).then(function (res1) {
               $scope.academic_year = res1.data;
               if (res1.data.length > 0) {
                   $scope.temp['sims_academic_year'] = res1.data[0].sims_academic_year;
                   $scope.getAllGrades($scope.sims_cur[0].sims_cur_code, res1.data[0].sims_academic_year)

               }
           });

       }

       $scope.getAllGrades = function (cur_code, academic_year) {
           debugger
           $http.get(ENV.apiUrl + "api/LibraryUserHistory/getAllGradesNewLib?cur_code=" + cur_code + "&academic_year=" + academic_year + "&user=" + user).then(function (res1) {
               $scope.grade_code = res1.data;
               setTimeout(function () {
                   $('#grade_box').change(function () {

                   }).multipleSelect({
                       width: '100%'
                   });
                   //$("#show_box").multipleSelect("checkAll");
               }, 1000);
           });
       }

       

       $scope.getSectionFromGrade = function (cur_code, academic_year, grade_code) {
           debugger
           $http.get(ENV.apiUrl + "api/LibraryUserHistory/getSectionFromGradeNewLib?cur_code=" + cur_code + "&academic_year=" + academic_year + "&grade_code=" + grade_code + "&user=" + user).then(function (res1) {
               $scope.section_code = res1.data;

               setTimeout(function () {
                   $('#section_box').change(function () {

                   }).multipleSelect({
                       width: '100%'
                   });
                   //$("#show_box").multipleSelect("checkAll");
               }, 1000);
           });
       }
            
       $http.get(ENV.apiUrl + "api/LibraryEmpMembership/GetAllLibraryMembershipType").then(function (certificate) {
           $scope.member_data = certificate.data;
       });

       $scope.getgrid = function () {
           $http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2017&routeCode=01").then(function (AllComboBoxValues) {
               debugger
               $scope.ComboBoxValues = AllComboBoxValues.data;
               $scope.temp = {
                   sims_academic_year: $scope.ComboBoxValues[0].sims_acadmic_year,
                   s_cur_code: $scope.ComboBoxValues[0].sims_cur_code,
               };

               console.log($scope.ComboBoxValues);
           });
       }

       //$scope.getgrid();
       // Form Close
       $scope.close = function () {
           $scope.table = true;
           $scope.display = false;
           $scope.grid2 = false;
           $scope.grid1 = false;
           $scope.sims_library_renewed_date = '';
           // $scope.save_btn = false;
       }
      
       ///////////////////Employee Tab
       $scope.searchemployee = function () {
           debugger
           $scope.grid = true;
           $scope.pager = true;
           $scope.pageno = true;
           $scope.searchtableemployee = true;
           $scope.mem = true;
           $scope.curr = true;
           $scope.acad = true;
           $scope.date = true;
           $scope.save = true;
           $scope.ImageView = false;
           if ($scope.temp == undefined || $scope.temp == null) {
               $scope.temp = '';
           }
           //if (name == undefined || name == '\"\"') {
           //    name = '';
           //}
           debugger;
           $http.get(ENV.apiUrl + "api/LibraryUserHistory/GetAllLibraryEmployeeHistory?emcode=" + $scope.empuser + "&from_date=" + $scope.temp.from_date + "&to_date=" + $scope.temp.to_date + "&cur_code=" + $scope.temp.sims_cur_code + "&acad_year=" + $scope.temp.sims_academic_year).then(function (GetEmp) {
               debugger;
               $scope.LibHistoryEmpeData = GetEmp.data;
               //$scope.totalItems = $scope.student.length;
               
              // console.log("EMP DATA:", $scope.LibHistoryEmpeData);
               //$scope.totalItems = $scope.student.length;
               //$scope.todos = $scope.student;
               //$scope.makeTodos();
               $scope.busy = false;
               console.log($scope.student);
               if (GetEmp.data.length > 0) {

               }
               else {
                   // $scope.Reset();
                   $scope.ImageView = true;
               }
           });
       }

       $scope.searchstudent = function () {
           debugger;
           $scope.stud = true;
           $scope.grid1 = true;
           $scope.pager1 = true;
           $scope.pager = false;
           $scope.ImageView = false;
           debugger;
           $http.get(ENV.apiUrl + "api/LibraryUserHistory/GetAllLibraryStudentHistory?enroll=" + $scope.temp.student_data + "&from_date=" + $scope.temp.from_date + "&to_date=" + $scope.temp.to_date + "&cur_code=" + $scope.temp.sims_cur_code + "&acad_year=" + $scope.temp.sims_academic_year + "&grade=" + $scope.temp.sims_grade_code + "&section=" + $scope.temp.sims_section_code).then(function (LibHistory) {  //+ "cur_code" + temp.s_cur_code + "acad_year" + temp.sims_academic_year
               debugger;
               $scope.LibHistoryStudeData = LibHistory.data;               
               $scope.totalItems = $scope.LibHistoryStudeData.length;
               $scope.todos = $scope.LibHistoryStudeData;
               $scope.makeTodos();
              
               $scope.busy = false;               
               if (LibHistory.data.length > 0) {

               }
               else {
                   // $scope.Reset();
                   $scope.ImageView = true;
               }
           });
       }      

       ///////////////////Student Tab
      $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 100000, $scope.maxSize = 100000;
       //$scope.LibHistoryStudeData = [], $scope.currentPage1 = 1, $scope.numPerPage1 = 10, $scope.maxSize1 = 10;
       $scope.size = function (str) {
           debugger;
           if (str == "All") {
               $scope.currentPage = '1';
               $scope.filteredTodos = $scope.LibHistoryStudeData;
               $scope.pager = false;
           }
           else {
               $scope.pager = true;
               $scope.pagesize = str;
               $scope.currentPage = 1;
               $scope.numPerPage = str;
               $scope.makeTodos();
           }
       }

       $scope.index = function (str) {
           $scope.pageindex = str;
           $scope.currentPage = str;
           console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
           main.checked = false;
           $scope.CheckAllChecked();
       }
       $scope.makeTodos = function () {
           debugger;
           var rem = parseInt($scope.totalItems % $scope.numPerPage);
           if (rem == '0') {
               $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
           }
           else {
               $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
           }

           var begin = (($scope.currentPage - 1) * $scope.numPerPage);
           var end = parseInt(begin) + parseInt($scope.numPerPage);

           //console.log("begin=" + begin); console.log("end=" + end);

           $scope.filteredTodos = $scope.todos.slice(begin, end);
       };

       $scope.searched = function (valLists, toSearch) {
           return _.filter(valLists,

           function (i) {
               /* Search Text in all  fields */
               return searchUtil(i, toSearch);
           });
       };

       $scope.search = function () {

           debugger;
           $scope.todos = $scope.searched($scope.LibHistoryStudeData, $scope.searchText);
           $scope.totalItems = $scope.todos.length;
           $scope.currentPage = '1';
           if ($scope.searchText == '') {
               $scope.todos = $scope.LibHistoryStudeData;
           }
           $scope.makeTodos();
       }

      
       function searchUtil(item, toSearch) {
           return (item.student_enroll.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
               || item.student_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
       }            

       $scope.Reset = function () {
           debugger;
           $scope.pager = false;
           $scope.pager1 = false;
           $scope.pageno = false;
           $scope.mem = true;

           $scope.table = true;
           $scope.searchtable = false;
           $scope.grid = false;
           $scope.student = [];
           $scope.edt = "";
           $scope.grade_code = [];
           $scope.LibHistoryStudeData = [];

           $scope.stud = false;
           $scope.grid1 = true;
           $scope.students = [];

           $state.go($state.current, {}, { reload: true });

       }

   }])

})();