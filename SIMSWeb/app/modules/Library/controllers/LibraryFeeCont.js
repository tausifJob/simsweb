﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var simsController = angular.module('sims.module.Library');
    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('LibraryFeeCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pageindex = 0;
            $scope.display = false;
            $scope.table1 = true;
            $scope.fee_no = true;
            $scope.table = true;

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //$scope.size = function (str) {
            //    $scope.pagesize = str;
            //    $scope.currentPage = 1;
            //    $scope.numPerPage = str;
            //    console.log("numPerPage=" + $scope.numPerPage); $scope.makeTodos();
            //}
            $scope.size = function (str) {
                
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.getAllTerm_details;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
              $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

              
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.showdata = function () {
                $http.get(ENV.apiUrl + "api/LibraryFee/getLibraryFee").then(function (res1) {
                    $scope.obj = res1.data;
                    $scope.totalItems = $scope.obj.length;
                    $scope.todos = $scope.obj;
                    $scope.makeTodos();
                    if ($scope.obj.length == 0) {
                        swal({ text: "No Record Found...", imageUrl: "assets/img/close.png", showCloseButton: true, width: 450, height: 200 });
                        $scope.display = false;
                        $scope.table = false;
                        $scope.table1 = true;
                    }
                    else {
                        $scope.display = false;
                        $scope.table = true;
                        $scope.table1 = true;
                    }
                })
            }

            $scope.showdata();

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.search = function () {

                $scope.todos = $scope.searched($scope.obj, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.obj;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sims_library_fee_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_library_privilege_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_library_fee_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_library_fee_amount.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sims_library_fee_amount.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 || item.sager == toSearch) ? true : false;

            }

            $scope.New = function () {
                $scope.clearvalue();
                $scope.display = true;
                $scope.table = false;
                $scope.table1 = false;
                $scope.instaminimuma = true;
                $scope.edt['sims_library_fee_status'] = true;
                $scope.Dropdownvalue();
                $http.get(ENV.apiUrl + "api/LibraryFee/getFeeNo").then(function (res4) {
                    $scope.obj3 = res4.data;
                    $scope.temp = {
                        sims_library_fee_number: $scope.obj3[0].sims_library_fee_number,
                    }
                })
                $scope.update1 = false;
                $scope.save1 = true;
            }

            $scope.CheckInstallment = function () {
               
                if ($scope.edt.sims_library_fee_installment_mode == true) {
                    $scope.instaminimuma = false;
                }
                else {
                    $scope.instaminimuma = true;
                }
            }

            $scope.obj1 = [];
            $scope.obj2 = [];

            $scope.Dropdownvalue = function () {

                $http.get(ENV.apiUrl + "api/LibraryFee/getAllLibraryPrivilege").then(function (res2) {
                    $scope.obj1 = res2.data;
                })

                $http.get(ENV.apiUrl + "api/LibraryFee/getAllFeeMode").then(function (res3) {
                    $scope.obj2 = res3.data;
                })

            }

            $scope.Checkfeecal = function () {
                
                if ($scope.edt.sims_appl_parameter != undefined) {
                    $scope.CalculateAmount($scope.edt.sims_appl_parameter);
                }
            }

            $scope.CalculateAmount = function (feeFrequency) {
                
                var decimal = ($scope.edt.sims_library_fee_amount.toString().split(".")[1]);

                if (decimal == undefined) {
                    decimal = '00'
                }
                var amount = Math.abs(($scope.edt.sims_library_fee_amount));
                var temp_value1 = 0;
                temp_value1 = (parseInt(Math.sign(amount / 3) * Math.ceil(Math.abs(amount / 3) / 5) * 5));

                if (feeFrequency == 'O' || feeFrequency == 'Y') {
                    $scope.edt["sims_library_fee_period1"] = $scope.edt.sims_library_fee_amount;
                    $scope.edt["sims_library_fee_period2"] = '0.0';
                    $scope.edt["sims_library_fee_period3"] = '0.0';
                    $scope.edt["sims_library_fee_period4"] = '0.0';
                    $scope.edt["sims_library_fee_period5"] = '0.0';
                    $scope.edt["sims_library_fee_period6"] = '0.0';
                    $scope.edt["sims_library_fee_period7"] = '0.0';
                    $scope.edt["sims_library_fee_period8"] = '0.0';
                    $scope.edt["sims_library_fee_period9"] = '0.0';
                    $scope.edt["sims_library_fee_period10"] = '0.0';
                    $scope.edt["sims_library_fee_period11"] = '0.0';
                    $scope.edt["sims_library_fee_period12"] = '0.0';
                }

                if (feeFrequency == 'T') {
                    var temp_value2 = temp_value1;
                    var temp_value3 = (parseInt(amount - (temp_value1 * 2)));
                    if (amount < 6) { temp_value1 = amount; temp_value2 = '0'; temp_value3 = '0'; }
                    if (temp_value3 < 0) {
                        temp_value2 = parseFloat(temp_value2) + parseFloat(temp_value3); if (temp_value2 < 0) { temp_value2 = '0'; }
                        temp_value3 = '0';
                    }
                    $scope.edt["sims_library_fee_period1"] = temp_value1;
                    $scope.edt["sims_library_fee_period2"] = temp_value2;
                    $scope.edt["sims_library_fee_period3"] = temp_value3 + '.' + decimal;
                    $scope.edt["sims_library_fee_period4"] = '0.0';
                    $scope.edt["sims_library_fee_period5"] = '0.0';
                    $scope.edt["sims_library_fee_period6"] = '0.0';
                    $scope.edt["sims_library_fee_period7"] = '0.0';
                    $scope.edt["sims_library_fee_period8"] = '0.0';
                    $scope.edt["sims_library_fee_period9"] = '0.0';
                    $scope.edt["sims_library_fee_period10"] = '0.0';
                    $scope.edt["sims_library_fee_period11"] = '0.0';
                    $scope.edt["sims_library_fee_period12"] = '0.0';
                }

                if (feeFrequency == 'M') {
                    var temp_value = parseInt(amount / 10);
                    var rem_amt = parseInt(amount - (temp_value * 10));
                    $scope.edt["sims_library_fee_period1"] = (temp_value);
                    $scope.edt["sims_library_fee_period2"] = (temp_value);
                    $scope.edt["sims_library_fee_period3"] = (temp_value) + '.' + decimal;
                    $scope.edt["sims_library_fee_period4"] = temp_value;
                    $scope.edt["sims_library_fee_period5"] = temp_value;
                    $scope.edt["sims_library_fee_period6"] = '0.0';
                    $scope.edt["sims_library_fee_period7"] = '0.0';
                    $scope.edt["sims_library_fee_period8"] = temp_value;
                    $scope.edt["sims_library_fee_period9"] = temp_value;
                    $scope.edt["sims_library_fee_period10"] = temp_value;
                    $scope.edt["sims_library_fee_period11"] = temp_value;
                    $scope.edt["sims_library_fee_period12"] = temp_value + rem_amt;

                }

            }

            $scope.submit = function (isvalid) {
                if (isvalid) {
                    var Savedata = [];

                    var deleteintcode = ({
                        'sims_library_fee_number': $scope.temp.sims_library_fee_number,
                        'sims_library_privilege_number': $scope.edt.sims_library_privilege_number,
                        'sims_library_fee_name': $scope.edt.sims_library_fee_name,
                        'sims_library_fee_deposit_amount': $scope.edt.sims_library_fee_deposit_amount,
                        'sims_library_fee_amount': $scope.edt.sims_library_fee_amount,
                        'sims_library_fee_manual_receipt_number': $scope.edt.sims_library_fee_manual_receipt_number,
                        'sims_appl_form_field_value1': $scope.edt.sims_appl_parameter,
                        'sims_library_fee_installment_minimum_amount': $scope.edt.sims_library_fee_installment_minimum_amount,
                        'sims_library_fee_period1': $scope.edt.sims_library_fee_period1,
                        'sims_library_fee_period2': $scope.edt.sims_library_fee_period2,
                        'sims_library_fee_period3': $scope.edt.sims_library_fee_period3,
                        'sims_library_fee_period4': $scope.edt.sims_library_fee_period4,
                        'sims_library_fee_period5': $scope.edt.sims_library_fee_period5,
                        'sims_library_fee_period6': $scope.edt.sims_library_fee_period6,
                        'sims_library_fee_period7': $scope.edt.sims_library_fee_period7,
                        'sims_library_fee_period8': $scope.edt.sims_library_fee_period8,
                        'sims_library_fee_period9': $scope.edt.sims_library_fee_period9,
                        'sims_library_fee_period10': $scope.edt.sims_library_fee_period10,
                        'sims_library_fee_period11': $scope.edt.sims_library_fee_period11,
                        'sims_library_fee_period12': $scope.edt.sims_library_fee_period12,
                        'sims_library_fee_installment_mode': $scope.edt.sims_library_fee_installment_mode,
                        'sims_library_fee_status': $scope.edt.sims_library_fee_status,
                        opr: 'I',
                    });
                    Savedata.push(deleteintcode);

                    $http.post(ENV.apiUrl + "api/LibraryFee/CUDLibraryFee", Savedata).then(function (msg) {
                        ;
                        $scope.msg1 = msg.data;
                        if ($scope.msg1.strMessage != undefined) {
                            if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                                $scope.showdata();
                                $scope.currentPage = true;
                            }
                        }
                        else {
                            swal({  text: "Please Select Atleast One Student...",imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                        }
                    });
                }
            }

            $scope.cancel = function () {
                $scope.showdata();
                $scope.display = false;
                $scope.table = true;
                $scope.table1 = true;
                $scope.clearvalue();
            }

            $scope.CheckAllChecked = function () {
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_library_fee_number + i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_library_fee_number + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyoneselect = function () {

                $("input[type='checkbox']").change(function (e) {

                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }

            $scope.edit = function (j) {
                $scope.update1 = true;
                $scope.save1 = false;
                $scope.edt = j;
                $scope.Dropdownvalue();
                $scope.onPriChange = {
                    sims_library_privilege_number: j.sims_library_privilege_number,
                    sims_library_privilege_name: j.sims_library_privilege_name,
                }

                for (var i = 0; i < $scope.obj1.length; i++) {
                    if ($scope.obj1[i].sims_library_privilege_number == $scope.onPriChange.sims_library_privilege_number) {
                        $scope.edt.sims_library_privilege_number = $scope.obj1[i].sims_library_privilege_number;
                        break;
                    }
                }

                $scope.onParChange = {
                    sims_appl_parameter: j.sims_appl_parameter,
                    sims_appl_form_field_value1: j.sims_appl_form_field_value1,
                }

                for (var i = 0; i < $scope.obj2.length; i++) {
                    if ($scope.obj2[i].sims_appl_parameter == $scope.onParChange.sims_appl_parameter) {
                        $scope.edt.sims_appl_parameter = $scope.obj2[i].sims_appl_parameter;
                        break;
                    }
                }

                $scope.temp = {
                    'sims_library_fee_number': j.sims_library_fee_number,
                }

                $scope.display = true;
                $scope.table = false;
                $scope.table1 = false;
            }

            $scope.Update = function () {
                var Savedata = [];
                var deleteintcode = ({
                    'sims_library_fee_number': $scope.temp.sims_library_fee_number,
                    'sims_library_privilege_number': $scope.edt.sims_library_privilege_number,
                    'sims_library_fee_name': $scope.edt.sims_library_fee_name,
                    'sims_library_fee_deposit_amount': $scope.edt.sims_library_fee_deposit_amount,
                    'sims_library_fee_amount': $scope.edt.sims_library_fee_amount,
                    'sims_library_fee_manual_receipt_number': $scope.edt.sims_library_fee_manual_receipt_number,
                    'sims_appl_form_field_value1': $scope.edt.sims_appl_parameter,
                    'sims_library_fee_installment_minimum_amount': $scope.edt.sims_library_fee_installment_minimum_amount,
                    'sims_library_fee_period1': $scope.edt.sims_library_fee_period1,
                    'sims_library_fee_period2': $scope.edt.sims_library_fee_period2,
                    'sims_library_fee_period3': $scope.edt.sims_library_fee_period3,
                    'sims_library_fee_period4': $scope.edt.sims_library_fee_period4,
                    'sims_library_fee_period5': $scope.edt.sims_library_fee_period5,
                    'sims_library_fee_period6': $scope.edt.sims_library_fee_period6,
                    'sims_library_fee_period7': $scope.edt.sims_library_fee_period7,
                    'sims_library_fee_period8': $scope.edt.sims_library_fee_period8,
                    'sims_library_fee_period9': $scope.edt.sims_library_fee_period9,
                    'sims_library_fee_period10': $scope.edt.sims_library_fee_period10,
                    'sims_library_fee_period11': $scope.edt.sims_library_fee_period11,
                    'sims_library_fee_period12': $scope.edt.sims_library_fee_period12,
                    'sims_library_fee_installment_mode': $scope.edt.sims_library_fee_installment_mode,
                    'sims_library_fee_status': $scope.edt.sims_library_fee_status,
                    opr: 'U',
                });
                Savedata.push(deleteintcode);

                $http.post(ENV.apiUrl + "api/LibraryFee/CUDLibraryFee", Savedata).then(function (msg) {
                    $scope.msg1 = msg.data;
                    if ($scope.msg1.strMessage != undefined) {
                        if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                            swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 });
                            $scope.showdata();
                            $scope.currentPage = true;
                            //$scope.clearvalue();
                        }
                    }
                    else {
                        swal({ text: "Please Select Atleast One Student...", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                });

            }

            $scope.OkDelete = function () {
                ;
                var data1 = [];
                $scope.flag = false;
                var deleteleave = [];
                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_library_fee_number + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_library_fee_number': $scope.filteredTodos[i].sims_library_fee_number,
                            opr: 'D'
                        });
                        deleteleave.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '', text: "Are you sure you want to Delete?", showCloseButton: true, showCancelButton: true, confirmButtonText: 'Yes', width: 380, cancelButtonText: 'No',
                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            $http.post(ENV.apiUrl + "api/LibraryFee/CUDLibraryFee", deleteleave).then(function (msg) {
                                {

                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1.strMessage != undefined) {

                                        if ($scope.msg1.strMessage != null || $scope.msg1.strMessage != "" || $scope.msg1.strMessage != undefined) {
                                            swal({ title: "Alert", text: $scope.msg1.strMessage, width: 380, height: 200 }).then(function (isConfirm) {
                                                if (isConfirm) {
                                                    $scope.showdata();
                                                    main = document.getElementById('mainchk');
                                                    if (main.checked == true) {
                                                        main.checked = false;
                                                        $scope.row1 = '';
                                                    }
                                                }
                                                $scope.currentPage = true;
                                            });
                                        }
                                    }
                                }
                            });
                        }
                        else {
                            main = document.getElementById('mainchk');
                            if (main.checked == true) {
                                main.checked = false;
                            }
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_library_fee_number + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                }
                            }
                        }
                    });
                }

                else {
                    swal({  text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }

                $scope.currentPage = str;
                $scope.row1 = '';
            }

            $scope.clearvalue = function () {

                $scope.edt = {
                    sims_library_privilege_number: '',
                    sims_library_fee_name: '',
                    sims_library_fee_deposit_amount: '',
                    sims_library_fee_amount: '',
                    sims_library_fee_manual_receipt_number: '',
                    sims_appl_parameter: '',
                    sims_library_fee_installment_minimum_amount: '',
                    sims_library_fee_period1: '',
                    sims_library_fee_period2: '',
                    sims_library_fee_period3: '',
                    sims_library_fee_period4: '',
                    sims_library_fee_period5: '',
                    sims_library_fee_period6: '',
                    sims_library_fee_period7: '',
                    sims_library_fee_period8: '',
                    sims_library_fee_period9: '',
                    sims_library_fee_period10: '',
                    sims_library_fee_period11: '',
                    sims_library_fee_period12: '',
                    sims_library_fee_installment_mode: '',
                    sims_library_fee_status: '',
                }
                $scope.searchText = '';
                $scope.MyForm.$setPristine();
                $scope.MyForm.$setUntouched();
            }

        }])
})();
