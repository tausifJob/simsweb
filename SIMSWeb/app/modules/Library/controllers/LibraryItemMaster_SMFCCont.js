﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, savefin = [], deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Library');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('LibraryItemMaster_SMFCCont',
        ['$scope', '$state', '$rootScope', '$timeout', '$stateParams', 'gettextCatalog', '$http', 'ENV','$filter', function ($scope, $state, $rootScope, $timeout, $stateParams, gettextCatalog, $http, ENV, $filter) {

            $scope.pagesize = '10';
            $scope.pageindex = "0";
            $scope.pager = true;

            $scope.ob = $stateParams.ob;
            $scope.save_btn = false;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            
            $scope.Accessno = false;
            $scope.accessnolabel = false;
            $scope.list = false;
            $scope.clr_btn = false;
            $scope.add_btn = false;
            $scope.google_bk = true;
            $scope.acccodelist_readonly = true;
            $scope.busy = false;
            $scope.temp = {};


            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            $scope.countData = [

                  { val: 10, data: 10 },
                  { val: 20, data: 20 },
                  //{ val: 15, data: 15 },
            ]

            $(function () {
                $('#cmb_category').multipleSelect({
                    width: '100%'
                });
            });

            $(function () {
                $('#cmb_subcategory').multipleSelect({
                    width: '100%'
                });
            });

            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };

            $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetLibraryCategory").then(function (CategoryTypes) {

                $scope.Library_category_code = CategoryTypes.data;
                console.log($scope.Library_category_code);
                setTimeout(function () {
                    debugger;
                    $('#cmb_category').change(function () {
                        console.log($(this).val());
                        $scope.getSubCat($scope.temp.sims_library_item_category);
                    }).multipleSelect({
                        width: '100%'
                    });
                }, 1000);
            });

            $scope.getSubCat = function (sub_cat) {
                debugger
                if (sub_cat == undefined || sub_cat == '') {
                    sub_cat
                    return;
                }
                $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetLibrarySubCategory?SubCategory=" + sub_cat).then(function (SubCategoryTypes) {
                    $scope.Library_subcategory_code = SubCategoryTypes.data;
                    console.log($scope.Library_subcategory_code);
                    setTimeout(function () {
                        debugger;
                        $('#cmb_subcategory').change(function () {
                            console.log($(this).val());
                        }).multipleSelect({
                            width: '100%'
                        });
                    }, 1000);
                });

            }
            //sort list
            $scope.propertyName = null;
            $scope.reverse = false;

            $scope.sortBy = function (propertyName) {
                $scope.reverse = ($scope.propertyName === propertyName) ? !$scope.reverse : false;
                $scope.propertyName = propertyName;
            };
            //Show Data
            $scope.view = function () {
                debugger;
                $scope.todos = {};
                $scope.totalItems = [];
                $scope.CreDiv = {};
                $scope.busy = true;
                $scope.tempnew = {};
                //$scope.temp = {};
                $scope.tempnew.sims_library_item_category = $scope.temp.sims_library_item_category + "";
                $scope.tempnew.sims_library_item_subcategory = $scope.temp.sims_library_item_subcategory + "";
                $http.post(ENV.apiUrl + "api/LibraryItemMaster/ItemSearch", $scope.tempnew).then(function (res1) {
                    debugger
                    $scope.CreDiv = res1.data;
                    if ($scope.countData.length > 3) {
                        $scope.countData.splice(3, 1);
                        $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                    }
                    else {
                        $scope.countData.push({ val: $scope.CreDiv.length, data: 'All' })
                    }
                    $scope.totalItems = $scope.CreDiv.length;
                    $scope.todos = $scope.CreDiv;
                    $scope.makeTodos();
                    $scope.busy = false;
                    //$scope.CreDiv = res1.data;
                    //$scope.totalItems = $scope.CreDiv.length;
                    //$scope.todos = $scope.CreDiv;
                    //$scope.makeTodos();

                });
            }
            var user = $rootScope.globals.currentUser.username;

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $scope.size = function (str) {
                //console.log(str);
                //$scope.pagesize = str;
                //$scope.currentPage = 1;
                //$scope.numPerPage = str;
                debugger;
                if (str == "All") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.CreDiv;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
                console.log("numPerPage=" + $scope.numPerPage); //$scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();
                // main.checked = false;
                $scope.CheckAllChecked();
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

                console.log("begin=" + begin); console.log("end=" + end);

                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            $scope.exportData = function () {
                var check = true;
                debugger;

                if (check == true) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Export " + $scope.obj1.lic_school_name + " Library Items.xls ?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 390,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            debugger;
                            var blob = new Blob([document.getElementById('dataexport').innerHTML], {
                                type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=utf-8"
                            });
                            saveAs(blob, $scope.obj1.lic_school_name + " Library Items" + ".xls");


                        }
                    });
                }
                else {
                    swal({ text: "Report Not Saved", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, })
                }

            }


            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.CreDiv, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.CreDiv;
                }
                $scope.makeTodos();
            }

            function searchUtil(item, toSearch) {
                return (item.sims_library_item_title.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    || item.sims_library_item_number == toSearch
                    || item.sims_library_item_accession_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    || item.sims_library_isbn_number.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    //|| item.sims_library_item_category_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    //|| item.sims_library_item_subcategory_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1
                    //|| item.sims_library_bnn_number == toSearch
                    //|| item.sims_library_item_dewey_number == toSearch
                    ) ? true : false;
            }

            // Form Close
            $scope.close = function () {
                $scope.table = true;
                $scope.display = false;
                $scope.grid2 = false;
                $scope.grid1 = false;
                $scope.save_btn = false;
            }

            $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetLibraryCatelogue").then(function (LibraryCatalogue) {
                $scope.Library_catalogue_code = LibraryCatalogue.data;
            });

            $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetLibraryCategory").then(function (CategoryTypes) {
                $scope.Library_category_code = CategoryTypes.data;
            });

            $scope.getSubCategory = function (sub_cat) {
                $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetLibrarySubCategory?SubCategory=" + sub_cat).then(function (SubCategoryTypes) {
                    $scope.Library_subcategory_code = SubCategoryTypes.data;
                    $scope.temp.sims_library_item_subcategory = $scope.Library_subcategory_code[0].sims_library_item_subcategory;

                });

            }


            $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetLanguageCode").then(function (LanguageCode) {
                $scope.Language_code = LanguageCode.data;
            });

            $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetBookStyle").then(function (BookStyleCode) {
                $scope.BookStyle_code = BookStyleCode.data;
            });

            $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetBookStatus").then(function (BookStatus) {
                $scope.Book_Status = BookStatus.data;
            });

            $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetCurcode").then(function (CurCode) {
                $scope.Library_cur_code = CurCode.data;
                debugger;
                // $scope.temp = {
                $scope.temp.sims_library_item_cur_code = $scope.Library_cur_code[0].sims_library_item_cur_code;
                // };
            });

            $scope.getBins = function (bin) {
                debugger
                $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetBin?location_code=" + bin).then(function (GetBin) {
                    $scope.Bin_Status = GetBin.data;                    
                });
            }



            $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetSims_Location").then(function (GetLocation) {
                $scope.Location = GetLocation.data;
                $scope.temp.sims_library_item_location_code = $scope.Location[0].sims_library_item_location_code;
                $scope.getBins($scope.temp.sims_library_item_location_code);
            });

            $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetSims_Currency").then(function (GetCurrency) {
                $scope.Currency_code = GetCurrency.data;
            });


            $scope.getcurlevel = function (cur_level) {
                $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetCurLevelcode?curlevel=" + cur_level).then(function (CurLevelCode) {
                    $scope.Library_cur_level_code = CurLevelCode.data;
                });
            }

            $scope.getGrade = function (cur_code, level_code) {
                $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetCurGradecode?cur=" + cur_code + "&level_code=" + level_code).then(function (CurGradeCode) {
                    $scope.Library_grade_code = CurGradeCode.data;
                });
            }

            $scope.temp.sims_library_item_accession_number_list = '';
            var list = '';

            function addtoList() {
                debugger
                if ($scope.msg) {
                    swal('', 'Accession No. Already Exists');
                    return;
                }
                if ($scope.temp.sims_library_item_number_of_copies == undefined || $scope.temp.sims_library_item_number_of_copies == '') {
                    swal('', 'Please Enter No of copies');
                    return;
                }
                if ($scope.temp.sims_library_item_accession_number_list == undefined) {
                    $scope.temp['sims_library_item_accession_number_list'] = '';
                }

                if ($http.defaults.headers.common['schoolId'] == 'abps') {
                    var q = $scope.temp.sims_library_item_accession_number_list.split('/');
                    if (q.includes('')) {
                        q.splice(q.indexOf(''), 1);
                    }
                    if (q.length < parseInt($scope.temp.sims_library_item_number_of_copies)) {
                        list = (list + $scope.temp.sims_library_item_accession_number) + '/';
                        if (q.includes($scope.temp.sims_library_item_accession_number) == false) {

                            q.push($scope.temp.sims_library_item_accession_number);
                            //$scope.temp.sims_library_item_accession_number_list = q + '/';
                            $scope.temp.sims_library_item_accession_number_list = list;
                        }
                    }
                }
                var q = $scope.temp.sims_library_item_accession_number_list.split(',');

                if (q.includes('')) {
                    q.splice(q.indexOf(''), 1);
                }
                if (q.length < parseInt($scope.temp.sims_library_item_number_of_copies)) {
                    if (q.includes($scope.temp.sims_library_item_accession_number) == false) {
                        q.push($scope.temp.sims_library_item_accession_number);
                        $scope.temp.sims_library_item_accession_number_list = q + '';
                    }
                }
                $scope.temp.sims_library_item_accession_number = '';
            }

            $scope.Clear = function () {
                $scope.temp.sims_library_item_accession_number_list = '';
                list = '';
            }

            $scope.Add = function () {
                debugger;

                //var a = {

                //    sims_library_isbn_number:$scope.temp.sims_library_isbn_number
                //}

                //$http.get("https://www.googleapis.com/books/v1/volumes?q=isbn:" + $scope.temp.sims_library_isbn_number).then(function (res_data) {
                //    debugger;
                //    $scope.temp.sims_library_item_title = res_data.items[0].volumeInfo.title;
                //    $scope.temp.sims_library_item_desc = res_data.items[0].volumeInfo.description;
                //    $scope.temp.sims_library_item_category_name = res_data.items[0].volumeInfo.categories;
                //    $scope.temp.sims_library_item_date_of_publication = res_data.items[0].volumeInfo.publishedDate;
                //    $scope.temp.sims_library_item_name_of_publisher = res_data.items[0].volumeInfo.publisher;
                //    $scope.temp.sims_library_item_author1 = res_data.items[0].volumeInfo.authors[0] === undefined ? '' : res_data.items[0].volumeInfo.authors[0];
                //    $scope.temp.sims_library_item_author2 = res_data.items[0].volumeInfo.authors[1] === undefined ? '' : res_data.items[0].volumeInfo.authors[1];
                //    $scope.temp.sims_library_item_author3 = res_data.items[0].volumeInfo.authors[2] === undefined ? '' : res_data.items[0].volumeInfo.authors[2];
                //    $scope.temp.sims_library_item_author4 = res_data.items[0].volumeInfo.authors[3] === undefined ? '' : res_data.items[0].volumeInfo.authors[3];
                //    $scope.temp.sims_library_item_author5 = res_data.items[0].volumeInfo.authors[4] === undefined ? '' : res_data.items[0].volumeInfo.authors[4];
                //    $scope.temp.sims_library_item_language_code = res_data.items[0].volumeInfo.language;
                //});
                //$scope.loading = true;

                $.ajax({
                    url: "https://www.googleapis.com/books/v1/volumes?q=isbn:" + $scope.temp.sims_library_isbn_number,
                    type: "GET",
                    success: function (res_data) {
                        debugger;
                        //$scope.loading = false;
                        if (res_data.totalItems> 0)
                        {
                        console.log("res_data for google api return", res_data);
                        $scope.temp.sims_library_item_title = res_data.items[0].volumeInfo.title;
                        $scope.temp.sims_library_item_desc = res_data.items[0].volumeInfo.description;
                        $scope.temp.sims_library_item_category_name = res_data.items[0].volumeInfo.categories;
                        // $scope.temp.sims_library_item_place_of_publication = res_data.items[0].volumeInfo.categories;
                        $scope.temp.sims_library_item_date_of_publication = $filter('date')(res_data.items[0].volumeInfo.publishedDate, 'dd-MM-yyyy');
                        $scope.temp.sims_library_item_name_of_publisher = res_data.items[0].volumeInfo.publisher;
                        $scope.temp.sims_library_item_author1 = res_data.items[0].volumeInfo.authors[0] === undefined ? '' : res_data.items[0].volumeInfo.authors[0];
                        $scope.temp.sims_library_item_author2 = res_data.items[0].volumeInfo.authors[1] === undefined ? '' : res_data.items[0].volumeInfo.authors[1];
                        $scope.temp.sims_library_item_author3 = res_data.items[0].volumeInfo.authors[2] === undefined ? '' : res_data.items[0].volumeInfo.authors[2];
                        $scope.temp.sims_library_item_author4 = res_data.items[0].volumeInfo.authors[3] === undefined ? '' : res_data.items[0].volumeInfo.authors[3];
                        $scope.temp.sims_library_item_author5 = res_data.items[0].volumeInfo.authors[4] === undefined ? '' : res_data.items[0].volumeInfo.authors[4];
                        $scope.temp.sims_library_item_language_code = res_data.items[0].volumeInfo.language;
                        //console.log("res_data for google api return", $scope.temp);
                        $scope.$apply();
                        }
                        else {
                            //  $scope.loading = false;
                            swal('', 'Given ISBN no is not available with Google Api');

                        }

                    },
                    error: function (request, status, error) {
                        $scope.loading = false;
                        swal('', 'No Record Found');
                    }
                });



            }

            $scope.access_nos = function (event) {
                debugger
                if (event.key == "Tab" || event.key == "Enter") {
                    $scope.accesssearch($scope.temp.sims_library_item_accession_number, addtoList);

                }

            }

            //New Button
            $scope.tempopr = 'I';
            $scope.New = function () {
                debugger;
                $scope.tempopr = 'I';
                list = '';
                //$scope.temp = [];

                $scope.disabled = false;
                $scope.Accessno = false;
                $scope.accessnolabel = false;
                $scope.gdisabled = false;
                $scope.aydisabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.divcode_readonly = false;
                $scope.acccode_readonly = false;
                $scope.add_btn = true;
                $scope.acccodelist_readonly = true;
                //$scope.temp['sims_library_item_number_of_copies'] = "1";
                var a = document.getElementById('yes');
                a.checked = true;
                //$scope.temp['Checkbox1'] = true;
                //$scope.Checkbox1.checked = true;
                var dt = new Date();
                $scope.temp.sims_library_item_number_of_copies = '';
                $scope.temp.sims_library_item_accession_number_list = '';
                $scope.temp.sims_library_item_accession_number = '';
                $scope.temp = {};
                $scope.info = {};
                $scope.temp.sims_library_item_entry_date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();;
                $scope.temp.sims_library_item_date_of_publication = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();;
                $scope.temp.sims_library_item_printing_date = ('0' + dt.getDate()).slice(-2) + '-' + ('0' + (dt.getMonth() + 1)).slice(-2) + '-' + dt.getFullYear();;

                if ($http.defaults.headers.common['schoolId'] == 'abps') {
                    var v = document.getElementById('no');
                    v.checked = true;
                    $scope.accessnolabel = true;
                    $scope.Accessno = true;
                    $scope.list = true;
                    $scope.clr_btn = true;
                }

                $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetCurcode").then(function (CurCode) {
                    $scope.Library_cur_code = CurCode.data;
                    debugger;
                    // $scope.temp = {
                    $scope.temp.sims_library_item_cur_code = $scope.Library_cur_code[0].sims_library_item_cur_code;
                    // };
                    $scope.getcurlevel($scope.temp.sims_library_item_cur_code);
                });

                $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetSims_Location").then(function (GetLocation) {
                    $scope.Location = GetLocation.data;
                    $scope.temp.sims_library_item_location_code = $scope.Location[0].sims_library_item_location_code;
                });

                $scope.radio1Click = function () {
                    if ($http.defaults.headers.common['schoolId'] == 'abps') {
                        swal('', 'Auto Accession no. is not allowed. Please insert by selecting Manual Accession no only');
                    }
                    $scope.accessnolabel = false;
                    $scope.Accessno = false;
                    $scope.message = false;
                    $scope.list = false;
                    $scope.clr_btn = false;
                    $scope.add_btn = true;
                    $scope.google_bk = true;
                }

                $scope.radio2Click = function () {
                    $scope.accessnolabel = true;
                    $scope.Accessno = true;
                    $scope.list = true;
                    $scope.add_btn = false;
                    $scope.clr_btn = true;
                    $scope.google_bk = true;
                }

                $scope.accesssearch = function (AccessNo, callback) {
                    debugger
                    $http.post(ENV.apiUrl + "api/LibraryItemMaster/CheckAccNo?AccessNo=" + AccessNo).then(function (AccessionNum) {
                        $scope.msg = AccessionNum.data;
                        $scope.message = $scope.msg;
                        callback.call();
                    });

                }

                //DATA SAVE INSERT
                var datasend = [];
                var accession_code = [];
                $scope.savedata = function (Myform) {
                    debugger;
                    if ($scope.temp.sims_library_item_printing_date < $scope.temp.sims_library_item_date_of_publication) {
                        swal('', 'Printing date must be after the date of publication');
                        return;
                    }
                    if ($scope.temp.sims_library_item_entry_date < $scope.temp.sims_library_item_date_of_publication) {
                        swal('', 'Entry date must be after the date of publication');
                        return;
                    }
                    if ($http.defaults.headers.common['schoolId'] == 'abps') {
                        var v = document.getElementById('yes');
                        if (v.checked == true) {
                            swal('', 'Auto Accession no. is not allowed. Please insert by selecting Manual Accession no only');
                            return;
                        }
                    }
                    if (Myform) {
                        var data = [];
                        /////For ABPS School logic
                        if ($http.defaults.headers.common['schoolId'] == 'abps') {
                            data.insert_mode = 'M'
                            var ac_no = $scope.temp.sims_library_item_accession_number_list;
                            accession_code = accession_code + '/' + ac_no;
                            $scope.temp.sims_library_item_accession_number = accession_code.substr(accession_code.indexOf('/') + 1);

                            //var cnt = $scope.temp.sims_library_item_accession_number_list.split('/').length;
                            //if ($scope.temp.sims_library_item_number_of_copies != cnt) {
                            //    swal('', 'Please enter accession nos equal to no of copies');
                            //    $scope.temp.sims_library_item_accession_number = '';
                            //    return;
                            //}
                            var code = {
                                opr: 'I',
                                insert_mode: data.insert_mode,
                                sims_library_item_number: $scope.temp.sims_library_item_number,
                                sims_library_item_accession_number: $scope.temp.sims_library_item_accession_number_list,
                                // sims_library_item_accession_number: $scope.temp.sims_library_item_accession_number,
                                sims_library_isbn_number: $scope.temp.sims_library_isbn_number,
                                sims_library_bnn_number: $scope.temp.sims_library_bnn_number,
                                sims_library_item_dewey_number: $scope.temp.sims_library_item_dewey_number,
                                sims_library_item_name: $scope.temp.sims_library_item_name,
                                sims_library_item_title: $scope.temp.sims_library_item_title,
                                sims_library_item_desc: $scope.temp.sims_library_item_desc,
                                sims_library_item_category: $scope.temp.sims_library_item_category,
                                sims_library_item_subcategory: $scope.temp.sims_library_item_subcategory,
                                sims_library_item_catalogue_code: $scope.temp.sims_library_item_catalogue_code,
                                sims_library_item_cur_code: $scope.temp.sims_library_item_cur_code,
                                sims_library_item_cur_level_code: $scope.temp.sims_library_item_cur_level_code,
                                sims_library_item_grade_code: $scope.temp.sims_library_item_grade_code,
                                sims_library_item_language_code: $scope.temp.sims_library_item_language_code,
                                sims_library_item_remark: $scope.temp.sims_library_item_remark,
                                sims_library_item_author1: $scope.temp.sims_library_item_author1,
                                sims_library_item_author2: $scope.temp.sims_library_item_author2,
                                sims_library_item_author3: $scope.temp.sims_library_item_author3,
                                sims_library_item_author4: $scope.temp.sims_library_item_author4,
                                sims_library_item_author5: $scope.temp.sims_library_item_author5,
                                sims_library_item_age_from: $scope.temp.sims_library_item_age_from,
                                sims_library_item_age_to: $scope.temp.sims_library_item_age_to,
                                sims_library_item_edition_statement: $scope.temp.sims_library_item_edition_statement,
                                sims_library_item_place_of_publication: $scope.temp.sims_library_item_place_of_publication,
                                sims_library_item_name_of_publisher: $scope.temp.sims_library_item_name_of_publisher,
                                sims_library_item_date_of_publication: $scope.temp.sims_library_item_date_of_publication,
                                sims_library_item_extent_of_item: $scope.temp.sims_library_item_extent_of_item,
                                sims_library_item_accompanying_item: $scope.temp.sims_library_item_accompanying_item,
                                sims_library_item_printing_date: $scope.temp.sims_library_item_printing_date,
                                sims_library_item_entry_date: $scope.temp.sims_library_item_entry_date,
                                sims_library_item_qr_code: $scope.temp.sims_library_item_qr_code,
                                sims_library_item_bar_code: $scope.temp.sims_library_item_bar_code,
                                sims_library_item_storage_hint: $scope.temp.sims_library_item_storage_hint,
                                sims_library_item_purchase_price: $scope.temp.sims_library_item_purchase_price,
                                sims_library_item_keywords: $scope.temp.sims_library_item_keywords,
                                sims_library_item_number_of_copies: $scope.temp.sims_library_item_number_of_copies,
                                sims_library_item_known_as: $scope.temp.sims_library_item_known_as,
                                sims_library_item_book_style: $scope.temp.sims_library_item_book_style,
                                sims_library_item_status_code: $scope.temp.sims_library_item_status_code,
                                sims_library_item_curcy_code: $scope.temp.sims_library_item_curcy_code,
                                sims_library_item_qty: $scope.temp.sims_library_item_qty,
                                sims_library_item_location_code: $scope.temp.sims_library_item_location_code,
                                sims_library_item_bin_code: $scope.temp.sims_library_item_bin_code,
                                sims_library_item_supplier: $scope.temp.sims_library_item_supplier,
                                sims_library_year_of_publication: $scope.temp.sims_library_year_of_publication,
                                sims_library_item_status_update_date: $scope.temp.sims_library_item_status_update_date
                            }
                            //$scope.AttributesVal
                            datasend.push(code);
                            var dataarr = [];
                            dataarr.push($scope.AttributesVal);
                            dataarr.push(datasend);
                            console.log(dataarr);
                            $http.post(ENV.apiUrl + "api/LibraryItemMaster/CUDLibraryItemMasterNEWSMFC", dataarr).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if (msg.data.length > 0) {
                                  
                                    $('#myModal').modal('show');
                                    $scope.searchtable = true;
                                    swal({ text: "Record Inserted Successfully. ", imageUrl: "assets/img/check.png", width: 800, height: 500 });
                                }
                                else if (msg.data.length == false) {
                                    swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                            // $scope.getgrid();
                            datasend = [];
                            dataarr = [];
                            $scope.table = true;
                            $scope.display = false;

                        }

                            //For Other School insert logic
                        else {
                            var v = document.getElementById('yes');

                            if (v.checked == true) {
                                data.insert_mode = 'A'
                            }
                            else {
                                data.insert_mode = 'M'
                                var ac_no = $scope.temp.sims_library_item_accession_number_list;
                                accession_code = accession_code + ',' + ac_no;
                                $scope.temp.sims_library_item_accession_number = accession_code.substr(accession_code.indexOf(',') + 1);

                                var cnt = $scope.temp.sims_library_item_accession_number_list.split(',').length;
                                if ($scope.temp.sims_library_item_number_of_copies != cnt) {
                                    swal('', 'Please enter accession nos equal to no of copies');
                                    $scope.temp.sims_library_item_accession_number = '';
                                    return;
                                }

                            }
                            var code = {
                                opr: 'I',
                                insert_mode: data.insert_mode,
                                sims_library_item_number: $scope.temp.sims_library_item_number,
                                sims_library_item_accession_number: $scope.temp.sims_library_item_accession_number_list,
                                // sims_library_item_accession_number: $scope.temp.sims_library_item_accession_number,
                                sims_library_isbn_number: $scope.temp.sims_library_isbn_number,
                                sims_library_bnn_number: $scope.temp.sims_library_bnn_number,
                                sims_library_item_dewey_number: $scope.temp.sims_library_item_dewey_number,
                                sims_library_item_name: $scope.temp.sims_library_item_name,
                                sims_library_item_title: $scope.temp.sims_library_item_title,
                                sims_library_item_desc: $scope.temp.sims_library_item_desc,
                                sims_library_item_category: $scope.temp.sims_library_item_category,
                                sims_library_item_subcategory: $scope.temp.sims_library_item_subcategory,
                                sims_library_item_catalogue_code: $scope.temp.sims_library_item_catalogue_code,
                                sims_library_item_cur_code: $scope.temp.sims_library_item_cur_code,
                                sims_library_item_cur_level_code: $scope.temp.sims_library_item_cur_level_code,
                                sims_library_item_grade_code: $scope.temp.sims_library_item_grade_code,
                                sims_library_item_language_code: $scope.temp.sims_library_item_language_code,
                                sims_library_item_remark: $scope.temp.sims_library_item_remark,
                                sims_library_item_author1: $scope.temp.sims_library_item_author1,
                                sims_library_item_author2: $scope.temp.sims_library_item_author2,
                                sims_library_item_author3: $scope.temp.sims_library_item_author3,
                                sims_library_item_author4: $scope.temp.sims_library_item_author4,
                                sims_library_item_author5: $scope.temp.sims_library_item_author5,
                                sims_library_item_age_from: $scope.temp.sims_library_item_age_from,
                                sims_library_item_age_to: $scope.temp.sims_library_item_age_to,
                                sims_library_item_edition_statement: $scope.temp.sims_library_item_edition_statement,
                                sims_library_item_place_of_publication: $scope.temp.sims_library_item_place_of_publication,
                                sims_library_item_name_of_publisher: $scope.temp.sims_library_item_name_of_publisher,
                                sims_library_item_date_of_publication: $scope.temp.sims_library_item_date_of_publication,
                                sims_library_item_extent_of_item: $scope.temp.sims_library_item_extent_of_item,
                                sims_library_item_accompanying_item: $scope.temp.sims_library_item_accompanying_item,
                                sims_library_item_printing_date: $scope.temp.sims_library_item_printing_date,
                                sims_library_item_entry_date: $scope.temp.sims_library_item_entry_date,
                                sims_library_item_qr_code: $scope.temp.sims_library_item_qr_code,
                                sims_library_item_bar_code: $scope.temp.sims_library_item_bar_code,
                                sims_library_item_storage_hint: $scope.temp.sims_library_item_storage_hint,
                                sims_library_item_purchase_price: $scope.temp.sims_library_item_purchase_price,
                                sims_library_item_keywords: $scope.temp.sims_library_item_keywords,
                                sims_library_item_number_of_copies: $scope.temp.sims_library_item_number_of_copies,
                                sims_library_item_known_as: $scope.temp.sims_library_item_known_as,
                                sims_library_item_book_style: $scope.temp.sims_library_item_book_style,
                                sims_library_item_status_code: $scope.temp.sims_library_item_status_code,
                                sims_library_item_curcy_code: $scope.temp.sims_library_item_curcy_code,
                                sims_library_item_qty: $scope.temp.sims_library_item_qty,
                                sims_library_item_location_code: $scope.temp.sims_library_item_location_code,
                                sims_library_item_bin_code: $scope.temp.sims_library_item_bin_code,
                                sims_library_item_supplier: $scope.temp.sims_library_item_supplier,
                                sims_library_year_of_publication: $scope.temp.sims_library_year_of_publication,
                                sims_library_item_status_update_date: $scope.temp.sims_library_item_status_update_date
                            }
                            //$scope.AttributesVal
                            datasend.push(code);
                            var dataarr = [];
                            dataarr.push($scope.AttributesVal);
                            dataarr.push(datasend);
                            $http.post(ENV.apiUrl + "api/LibraryItemMaster/CUDLibraryItemMasterNEWSMFC", dataarr).then(function (msg) {
                                $scope.msg1 = msg.data;
                                if (msg.data.length > 0) {                                 
                                    //$('#myModal').modal('show');
                                    $scope.searchtable = true;
                                    //swal({ title: "Alert", text: $scope.msg1, width: 800, height: 500 });
                                }
                                else if (msg.data.length == false) {
                                    swal({ text: "Record Not Inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                            // $scope.getgrid();
                            datasend = [];
                            dataarr = [];
                            $scope.table = true;
                            $scope.display = false;
                        }
                        $scope.Reset();
                        $scope.view();
                    }
                  
                }

            }

            /// GET ATTRIBUTES
            $scope.getAttributes = function () {
                //$scope.tempopr
                //$scope.temp.sims_library_item_catalogue_code
                $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetLibItemAttValues?opr=" + $scope.tempopr + "&catalogue=" + $scope.temp.sims_library_item_catalogue_code + "&item_code=").then(function (AttributesVal) {
                    $scope.AttributesVal = AttributesVal.data;
                    console.log($scope.AttributesVal);
                });
            }

            if ($scope.ob == true) {
                $scope.New();
            }

            $scope.Cancelled = function () {
                $('#myModal').modal('hide');
                $scope.searchtable = false;
                $scope.info = "";
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                debugger
                $scope.temp = {};
                $scope.table = true;
                $scope.display = false;
                $scope.list = false;
                $scope.clr_btn = false;
                $scope.info = '';
                //$scope.New();
            }

            //DATA EDIT
            $scope.edit = function (str) {
                $scope.tempopr = 'U';
                debugger
                $scope.gdisabled = false;
                $scope.aydisabled = true;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.divcode_readonly = true;
                $scope.acccode_readonly = true;
                $scope.acccodelist_readonly = true;

                $scope.temp =
                {
                    sims_library_item_number: str.sims_library_item_number,
                    sims_library_item_accession_number: str.sims_library_item_accession_number,
                    sims_library_isbn_number: str.sims_library_isbn_number,
                    sims_library_bnn_number: str.sims_library_bnn_number,
                    sims_library_item_dewey_number: str.sims_library_item_dewey_number,
                    sims_library_item_name: str.sims_library_item_name,
                    sims_library_item_title: str.sims_library_item_title,
                    sims_library_item_desc: str.sims_library_item_desc,
                    sims_library_item_category: str.sims_library_item_category,
                    sims_library_item_category_name: str.sims_library_item_category_name,
                    sims_library_item_subcategory: str.sims_library_item_subcategory,
                    sims_library_item_subcategory_name: str.sims_library_item_subcategory_name,


                    sims_library_item_catalogue_code: str.sims_library_item_catalogue_code,
                    sims_library_item_catalogue_name: str.sims_library_item_catalogue_name,

                    sims_library_item_cur_code: str.sims_library_item_cur_code,
                    sims_library_item_cur_name: str.sims_library_item_cur_name,
                    sims_library_item_cur_level_code: str.sims_library_item_cur_level_code,
                    sims_library_item_cur_level_name: str.sims_library_item_cur_level_name,
                    sims_library_item_grade_code: str.sims_library_item_grade_code,
                    sims_library_item_grade_name: str.sims_library_item_grade_name,
                    sims_library_item_language_code: str.sims_library_item_language_code,
                    sims_library_item_language_name: str.sims_library_item_language_name,
                    sims_library_item_remark: str.sims_library_item_remark,
                    sims_library_item_author1: str.sims_library_item_author1,
                    sims_library_item_author2: str.sims_library_item_author2,
                    sims_library_item_author3: str.sims_library_item_author3,
                    sims_library_item_author4: str.sims_library_item_author4,
                    sims_library_item_author5: str.sims_library_item_author5,
                    sims_library_item_age_from: str.sims_library_item_age_from,
                    sims_library_item_age_to: str.sims_library_item_age_to,
                    sims_library_item_edition_statement: str.sims_library_item_edition_statement,
                    sims_library_item_place_of_publication: str.sims_library_item_place_of_publication,
                    sims_library_item_name_of_publisher: str.sims_library_item_name_of_publisher,
                    sims_library_item_date_of_publication: str.sims_library_item_date_of_publication,
                    sims_library_item_extent_of_item: str.sims_library_item_extent_of_item,
                    sims_library_item_accompanying_item: str.sims_library_item_accompanying_item,
                    sims_library_item_printing_date: str.sims_library_item_printing_date,
                    sims_library_item_qr_code: str.sims_library_item_qr_code,
                    sims_library_item_bar_code: str.sims_library_item_bar_code,
                    sims_library_item_storage_hint: str.sims_library_item_storage_hint,
                    sims_library_item_purchase_price: str.sims_library_item_purchase_price,
                    sims_library_item_keywords: str.sims_library_item_keywords,
                    sims_library_item_number_of_copies: str.sims_library_item_number_of_copies,
                    sims_library_item_known_as: str.sims_library_item_known_as,
                    sims_library_item_book_style_name: str.sims_library_item_book_style_name,
                    sims_library_item_status_code: str.sims_library_item_status_code,
                    sims_library_item_status_name: str.sims_library_item_status_name,
                    sims_library_item_curcy_code: str.sims_library_item_curcy_code,
                    sims_library_item_curcy_name: str.sims_library_item_curcy_name,
                    sims_library_item_qty: str.sims_library_item_qty,
                    sims_library_item_location_code: str.sims_library_item_location_code,
                    sims_library_item_location_nm: str.sims_library_item_location_nm,
                    sims_library_item_bin_code: str.sims_library_item_bin_code,
                    sims_library_item_bin_nm: str.sims_library_item_bin_nm,
                    sims_library_item_entry_date: str.sims_library_item_entry_date,
                    sims_library_item_book_style: str.sims_library_item_book_style,
                    sims_library_item_supplier: str.sims_library_item_supplier,
                    sims_library_item_status_update_date: str.sims_library_item_status_update_date
                };

                console.log($scope.temp);
                console.log($scope.str);
                $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetLibrarySubCategory?SubCategory=" + str.sims_library_item_category).then(function (SubCategoryTypes) {
                    $scope.Library_subcategory_code = SubCategoryTypes.data;
                });
                //$scope.getSubCat(str.sims_library_item_category);
                $scope.getcurlevel(str.sims_library_item_cur_level_code);
                $scope.getGrade(str.sims_library_item_cur_code, str.sims_library_item_cur_level_code);
                $scope.getBins(str.sims_library_item_location_code);

                /*Attribute Value*/
                $http.get(ENV.apiUrl + "api/LibraryItemMaster/GetLibItemAttValues?opr=" + $scope.tempopr + "&catalogue=" + str.sims_library_item_catalogue_code + "&item_code=" + str.sims_library_item_number).then(function (AttributesVal) {
                    $scope.AttributesVal = AttributesVal.data;
                    console.log($scope.AttributesVal);
                });
            }

            $scope.functionToaddDescription=function(){
                $scope.temp.sims_library_item_desc= $scope.temp.sims_library_item_title;
            }
            //DATA UPDATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                debugger
                if (Myform) {
                    if ($scope.temp.sims_library_item_desc=='') {
                        $scope.temp.sims_library_item_desc = $scope.temp.sims_library_item_title;
                    }
                    //var data = $scope.temp;
                    //data.opr = "Usims_library_item_title
                    var data = {
                        opr: 'U',
                        //insert_mode: data.insert_mode,
                        sims_library_item_number: $scope.temp.sims_library_item_number,
                        sims_library_item_accession_number: $scope.temp.sims_library_item_accession_number,
                        sims_library_isbn_number: $scope.temp.sims_library_isbn_number,
                        sims_library_bnn_number: $scope.temp.sims_library_bnn_number,
                        sims_library_item_dewey_number: $scope.temp.sims_library_item_dewey_number,
                        sims_library_item_name: $scope.temp.sims_library_item_name,
                        sims_library_item_title: $scope.temp.sims_library_item_title,
                        sims_library_item_desc: $scope.temp.sims_library_item_desc,
                        sims_library_item_category: $scope.temp.sims_library_item_category,
                        sims_library_item_subcategory: $scope.temp.sims_library_item_subcategory,
                        sims_library_item_catalogue_code: $scope.temp.sims_library_item_catalogue_code,
                        sims_library_item_cur_code: $scope.temp.sims_library_item_cur_code,
                        sims_library_item_cur_level_code: $scope.temp.sims_library_item_cur_level_code,
                        sims_library_item_grade_code: $scope.temp.sims_library_item_grade_code,
                        sims_library_item_language_code: $scope.temp.sims_library_item_language_code,
                        sims_library_item_remark: $scope.temp.sims_library_item_remark,
                        sims_library_item_author1: $scope.temp.sims_library_item_author1,
                        sims_library_item_author2: $scope.temp.sims_library_item_author2,
                        sims_library_item_author3: $scope.temp.sims_library_item_author3,
                        sims_library_item_author4: $scope.temp.sims_library_item_author4,
                        sims_library_item_author5: $scope.temp.sims_library_item_author5,
                        sims_library_item_age_from: $scope.temp.sims_library_item_age_from,
                        sims_library_item_age_to: $scope.temp.sims_library_item_age_to,
                        sims_library_item_edition_statement: $scope.temp.sims_library_item_edition_statement,
                        sims_library_item_place_of_publication: $scope.temp.sims_library_item_place_of_publication,
                        sims_library_item_name_of_publisher: $scope.temp.sims_library_item_name_of_publisher,
                        sims_library_item_date_of_publication: $scope.temp.sims_library_item_date_of_publication,
                        sims_library_item_extent_of_item: $scope.temp.sims_library_item_extent_of_item,
                        sims_library_item_accompanying_item: $scope.temp.sims_library_item_accompanying_item,
                        sims_library_item_printing_date: $scope.temp.sims_library_item_printing_date,
                        sims_library_item_entry_date: $scope.temp.sims_library_item_entry_date,
                        sims_library_item_qr_code: $scope.temp.sims_library_item_qr_code,
                        sims_library_item_bar_code: $scope.temp.sims_library_item_bar_code,
                        sims_library_item_storage_hint: $scope.temp.sims_library_item_storage_hint,
                        sims_library_item_purchase_price: $scope.temp.sims_library_item_purchase_price,
                        sims_library_item_keywords: $scope.temp.sims_library_item_keywords,
                        sims_library_item_number_of_copies: $scope.temp.sims_library_item_number_of_copies,
                        sims_library_item_known_as: $scope.temp.sims_library_item_known_as,
                        sims_library_item_book_style: $scope.temp.sims_library_item_book_style,
                        sims_library_item_status_code: $scope.temp.sims_library_item_status_code,
                        sims_library_item_curcy_code: $scope.temp.sims_library_item_curcy_code,
                        sims_library_item_qty: $scope.temp.sims_library_item_qty,
                        sims_library_item_location_code: $scope.temp.sims_library_item_location_code,
                        sims_library_item_bin_code: $scope.temp.sims_library_item_bin_code,
                        sims_library_item_supplier: $scope.temp.sims_library_item_supplier,
                        sims_library_year_of_publication: $scope.temp.sims_library_year_of_publication,
                        sims_library_item_status_update_date: $scope.temp.sims_library_item_status_update_date
                    }
                    //$scope.AttributesVal

                    dataforUpdate.push(data);
                    var dataarr = [];
                    dataarr.push($scope.AttributesVal);
                    dataarr.push(dataforUpdate);
                    $http.post(ENV.apiUrl + "api/LibraryItemMaster/CUDLibraryItemMasterNEWSMFC", dataarr).then(function (msg) {
                        $scope.msg1 = msg.data;

                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            
                            //$scope.view();
                            //swal({ title: "Alert", text: "Record Not Updated", width: 300, height: 200 });
                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                    });
                    //  $scope.getgrid();
                    
                    dataforUpdate = [];
                    dataarr = [];
                    $scope.info = {};
                    $scope.table = true;
                    $scope.display = false;
                    $scope.Reset();
                    $scope.view();
                }

            }
            //Delete Record

            $scope.OkDelete = function () {
                debugger
                var dataarr = [];
                // deletefin = [];
                $scope.flag = false;

                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                    var v = document.getElementById($scope.filteredTodos[i].sims_library_item_number + i);
                    if (v.checked == true) {
                        $scope.flag = true;
                        var deletemodulecode = ({
                            'sims_library_item_number': $scope.filteredTodos[i].sims_library_item_number,
                            opr: 'D'
                        });
                        dataarr.push(deletemodulecode);
                    }
                }
                if ($scope.flag) {
                    swal({
                        title: '',
                        text: "Are you sure you want to Delete?",
                        showCloseButton: true,
                        showCancelButton: true,
                        confirmButtonText: 'Yes',
                        width: 380,
                        cancelButtonText: 'No',

                    }).then(function (isConfirm) {
                        if (isConfirm) {
                            var a = [];

                            $http.post(ENV.apiUrl + "api/LibraryItemMaster/CUDLibraryItemMaster", dataarr).then(function (msg) {
                                $scope.msg1 = msg.data;
                                debugger;
                                if ($scope.msg1 == true) {
                                    swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                           // $scope.getgrid();
                                            main = document.getElementById('mainchk');
                                            if (main.checked == true) {
                                                main.checked = false;
                                            }
                                            $scope.CheckAllChecked();
                                        }
                                       // $scope.currentPage = true;
                                    });
                                    //$scope.view();
                                   // $scope.Reset();
                                }
                                else if ($scope.msg1 == false) {
                                    swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                           // $scope.getgrid();
                                            main.checked = false;
                                            $('tr').removeClass("row_selected");
                                        }
                                    });
                                }
                                else {
                                    swal("Error-" + $scope.msg1)
                                }
                            });
                        }
                        else {
                            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                var v = document.getElementById($scope.filteredTodos[i].sims_library_item_number + i);
                                if (v.checked == true) {
                                    v.checked = false;
                                    main.checked = false;
                                    $('tr').removeClass("row_selected");
                                    //  $scope.row1 = '';
                                }
                            }
                        }
                    });
                }
                else {
                    swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                }
                $scope.row1 = '';
                //$scope.currentPage = str;
                main.checked = false;
                //deletefin = [];
            }

            $scope.CheckAllChecked = function () {

                main = document.getElementById('mainchk');

                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_library_item_number + i);
                        v.checked = true;
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById($scope.filteredTodos[i].sims_library_item_number + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                        $('tr').removeClass("row_selected");
                    }
                }

            }

            $scope.checkonebyonedelete = function () {

                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) { //If the checkbox is checked
                        $(this).closest('tr').addClass("row_selected");
                        //Add class on checkbox checked
                        $scope.color = '#edefef';
                    }
                    else {
                        $(this).closest('tr').removeClass("row_selected");
                        //Remove class on checkbox uncheck
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $scope.color = '#edefef';
                    $scope.row1 = '';
                }
            }

            //$scope.getgrid = function () {
            //    $http.post(ENV.apiUrl + "api/LibraryItemMaster/ItemSearch").then(function (res1) {
            //        $scope.CreDiv = res1.data;
            //        $scope.totalItems = $scope.CreDiv.length;
            //        $scope.todos = $scope.CreDiv;
            //        $scope.makeTodos();
            //    });
            //}

            $scope.Reset = function () {
                $scope.temp = {};
                $scope.filteredTodos = [];
                $scope.count = '';
                $scope.CreDiv = [];
                //$scope.pager = false;
            }

            $scope.onlyNumbers = function (event) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $scope.onlyNumbers1 = function (event) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            $scope.onlyNumbers2 = function (event) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 45,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57,
                    'A': 65, 'B': 66, 'C': 67, 'D': 68, 'E': 69, 'F': 70, 'G': 71, 'H': 72, 'I': 73, 'J': 74, 'K': 75, 'L': 76, 'M': 77,
                    'N': 78, 'O': 79, 'P': 80, 'Q': 81, 'R': 82, 'S': 83, 'T': 84, 'U': 85, 'V': 86, 'W': 87, 'X': 88, 'Y': 89, 'Z': 90,
                    'a': 97, 'b': 98, 'c': 99, 'd': 100, 'e': 101, 'f': 102, 'g': 103, 'h': 104, 'i': 105, 'j': 106, 'k': 107, 'l': 108, 'm': 109, 'n': 110, 'o': 111,
                    'p': 112, 'q': 113, 'r': 114, 's': 115, 't': 116, 'u': 117, 'v': 118, 'w': 119, 'x': 120, 'y': 121, 'z': 122
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };
        }])

})();