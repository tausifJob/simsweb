﻿(function () {
    'use strict';
    var temp = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('StoreIssueDetailsCont',
      ['$scope', '$state', '$rootScope', '$stateParams', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $stateParams, $timeout, gettextCatalog, $http, ENV) {
          console.log('Hello u r in 2nd application');
          console.log($stateParams.IP);
          var main = '';
          $scope.submitBtnDisabled = false;
          $scope.temp = [];
          var dt_code = $stateParams.IP.dt_code;
         
          var uname = $rootScope.globals.currentUser.username;
          var today = new Date();
          today = ('0' + today.getDate()).slice(-2) + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + today.getFullYear();          
          $scope.temp.issue_date = today
          
          var doc_prov_date = $stateParams.IP.doc_prov_date
          var sims_cur_code = $stateParams.IP.sims_cur_code
          var sims_acaedmic_year = $stateParams.IP.sims_acaedmic_year
          var sims_grade_code = $stateParams.IP.sims_grade_code
          var sims_section_code = $stateParams.IP.sims_section_code
          $scope.temp.invoice_no = $stateParams.IP.doc_prov_no;
          $scope.temp.enroll_no = $stateParams.IP.enroll_No;
          $scope.temp.student_name = $stateParams.IP.student_name;
          $scope.temp.creation_user = $stateParams.IP.creation_user;
          debugger;
          $scope.loading = true;
          $http.get(ENV.apiUrl + "api/StoreIssueController/getDetailsinnerTableSMFC?cur=" + sims_cur_code + "&aca=" + sims_acaedmic_year + "&grd=" + sims_grade_code + "&section=" + sims_section_code + "&docprovDate=" + doc_prov_date + "&docprovNo=" + $scope.temp.invoice_no).then(function (getFullData) {
              var temp_sublist1 = getFullData.data;
              debugger;
              //$scope.sublist1 = getFullData.data;
              console.log(temp_sublist1);
              if (temp_sublist1.length > 0) {
                  
                  //setTimeout(function(){
                      for (var j = 0; j < temp_sublist1.length; j++) {
                          temp_sublist1[j].creation_user = $rootScope.globals.currentUser.username;
                          var invoice_qty = temp_sublist1[j].dd_qty;
                          var current_qty = temp_sublist1[j].id_cur_qty;
                          var deleverd_qty = temp_sublist1[j].issued_dd_issue_qty;
                          if (deleverd_qty == '') {
                              deleverd_qty = 0;
                          }
                          var total_minus_value = 0;
                          if (parseFloat(invoice_qty) < parseFloat(current_qty)) {
                              temp_sublist1[j].inner_checked = true;
                              if (parseFloat(deleverd_qty) > parseFloat(invoice_qty)) {
                                  total_minus_value = parseFloat(deleverd_qty) - parseFloat(invoice_qty);
                              } else {
                                  total_minus_value = parseFloat(invoice_qty) - parseFloat(deleverd_qty);
                              }

                              total_minus_value = parseInt(total_minus_value)
                              if (parseInt(current_qty) > total_minus_value) {
                                  temp_sublist1[j].issue_quantity = total_minus_value;

                              } else {
                                  temp_sublist1[j].issue_quantity_dis = true;
                              }
                              if (total_minus_value == 0) {
                                  temp_sublist1[j].inner_disabled = true;
                                  $("#test-" + j).css('color', '#0ddb38');
                                  $("#color-" + j).css('color', '#0ddb38');
                                  temp_sublist1[j].inner_checked = false;

                              } else {
                                  temp_sublist1[j].doc_issue_date = "";
                              }
                          }
                          else {

                              temp_sublist1[j].inner_disabled = true;
                              temp_sublist1[j].issue_quantity_dis = true;
                              temp_sublist1[j].remarks_dis = true;
                          }
                      }
                  //},200); 
                  $scope.sublist1 = temp_sublist1;
                  //$scope.totalItems = $scope.sublist1.length;
                  //$scope.todos = $scope.sublist1;
                  //for (var i = 0; i < $scope.totalItems; i++) {
                  //    $scope.icon = "fa fa-plus-circle";
                  //}
              }
              else {
                  swal({ text: "Record not found. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
              }
              $scope.loading = false;
          });

          $scope.GOBack = function () {
              $state.go('main.storeI');
          }
          var stroe_report_name = [];
          $http.get(ENV.apiUrl + "api/StoreIssueController/getreport_store_1").then(function (getFullData) {
              $scope.stroe_report_name = getFullData.data;
          });
          $scope.onlyNumbers = function (obj, event) {
              debugger;
              var keys = {

                  'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                  '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
              };
              for (var index in keys) {
                  if (!keys.hasOwnProperty(index)) continue;
                  if (parseInt(event.target.value) < 0) {
                      event.target.value = "";
                  }
                  if (/^\d*$/.test(parseFloat(event.target.value))) {

                  }
                  else {
                      event.target.value = "";
                  }
                  if (parseInt(event.target.value) > parseInt(obj.id_cur_qty)) {
                      event.target.value = "";
                  }
                  if (parseInt(obj.dd_qty) < parseInt(event.target.value)) {
                      event.target.value = "";
                  }
                  if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                      return; //default event
                  }
              }
              event.preventDefault();
          };
          $scope.CheckAllChecked = function () {
              debugger;
              main = document.getElementById('mainchk');
              for (var i = 0; i < $scope.sublist1.length; i++) {
                  var v = document.getElementById($scope.sublist1[i].im_item_code + i);
                  if ($scope.sublist1[i].inner_disabled == true)
                  {
                      continue;
                  }
                  if (main.checked == true) {
                      $scope.sublist1[i].inner_checked = true;
                      v.checked = true;

                      $('tr').addClass("row_selected");
                  }
                  else {
                      
                      for (var i = 0; i < $scope.sublist1.length; i++) {
                          if ($scope.sublist1[i].inner_disabled == false) {
                              continue;
                          }
                          var v = document.getElementById($scope.sublist1[i].im_item_code + i);
                          $scope.sublist1[i].inner_checked = false;
                          v.checked = false;
                          main.checked = false;
                          $('tr').removeClass("row_selected");
                      }
                  }
              }

          }
          $scope.checkonebyonedelete = function () {
              $("input[type='checkbox']").change(function (e) {
                  if ($(this).is(":checked")) { //If the checkbox is checked
                      $(this).closest('tr').addClass("row_selected");
                      //Add class on checkbox checked
                      $scope.color = '#edefef';
                  } else {
                      $(this).closest('tr').removeClass("row_selected");
                      //Remove class on checkbox uncheck
                      $scope.color = '#edefef';
                  }
              });

              main = document.getElementById('mainchk');
              if (main.checked == true) {
                  main.checked = false;
              }
          }

          $scope.stor_report_after_submit = function (row) {
              debugger;
              $http.get(ENV.apiUrl + "api/StoreIssueController/get_afterSubmitgetreport_single?doc_prov_no=" + row.invoice_no).then(function (report_data) {
                  var doc_sr_code_user = report_data.data;
                  console.log(doc_sr_code_user);
                  console.log($scope.stroe_report_name);
                  console.log(stroe_report_name);
                  var data = {
                      location: $scope.stroe_report_name,
                      parameter: {
                          doc_sr_code: doc_sr_code_user,
                      },
                      state: 'main.storeI',
                      ready: function () {
                          this.refreshReport();
                      },
                  }
                  console.log(data);
                  window.localStorage["ReportDetails"] = JSON.stringify(data);
                  $state.go('main.ReportCardParameter');
              });
          }
         
          $scope.insertion_data = function () {
              debugger;
              var data_temp = [];
              $scope.submitBtnDisabled = true;

              if ($scope.sublist1 != "undefined") {
                  var sending_Data = [];
                  
                      var value = 0;
                      for (var j = 0; j < $scope.sublist1.length; j++) {                          
                              if ($scope.sublist1[j].inner_checked == true) {
                                  $scope.sublist1[j].creation_date = $scope.temp.issue_creation_date;
                                  sending_Data.push($scope.sublist1[j])
                                  if (value == 0) {
                                      if (data_temp.length == 0) {
                                          data_temp = {
                                              invoice_no: $scope.sublist1[j].invoice_no + $scope.sublist1[j].enroll_no,
                                              doc_prov_date: $scope.sublist1[j].doc_prov_date,
                                          }
                                       }
                                      //else {
                                      //    data_temp = {
                                      //        invoice_no: data_temp.invoice_no + "," + $scope.invoice_no + $scope.enroll_no,
                                      //        doc_prov_date: data_temp.doc_prov_date + "," + $scope.doc_prov_date,
                                      //    }
                                      //}
                                  }
                                  value++;
                              }                          
                      }                  
                      console.log(sending_Data);                     
                  if (sending_Data.length > 0) {
                      if (dt_code=='01') {
                          $http.post(ENV.apiUrl + "api/StoreIssueController/insertStoreIssueData", sending_Data).then(function (insertedFlag) {
                              debugger;
                              var flag = insertedFlag.data;
                              console.log(flag);
                              if (flag) {
                                  swal({ text: "Record Insert Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });                                  
                                  $scope.stor_report_after_submit(data_temp);
                              }
                              else {
                                  swal({ text: "Record not inserted. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                              }
                              $scope.submitBtnDisabled = false;
                          });
                      }
                      else if (dt_code == '02') {                          
                          $http.post(ENV.apiUrl + "api/StoreIssueController/updateRecordData", sending_Data).then(function (insertedFlag) {                              
                              var flag = insertedFlag.data;
                              console.log(flag);
                              if (flag) {
                                  swal({ text: "Sales invoice return inserted Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });                                  
                                  ///$scope.stor_report_after_submit(data_temp);
                                  $scope.GOBack();
                              }
                              else {
                                  swal({ text: "Sales invoice return not inserted Successfully. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                              }
                              $scope.submitBtnDisabled = false;
                          });
                      }
                  }
              }
          }
      }])


})();