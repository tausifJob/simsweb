﻿(function () {
    'use strict';
    var del = [];
    var main;
    var date1;
    var date3;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('SalesDocument_internalCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.disable_btn = true;
            $scope.save_btn = true;
            
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.temp1 = {
                im_inv_no: undefined, invs021_sg_name: undefined, im_item_code: undefined, im_desc: undefined, invs021_dep_code: undefined, sec_code: undefined, invs021_sup_code: undefined, im_assembly_ind_s: undefined, category_code: undefined, subcategory_code: undefined

            };
            //$scope.pagesize = "5";
            // $scope.pageindex = "1";
            $scope.itemsPerPage = '5';
            $scope.currentPage = 0;
            $scope.obj = [];
            $scope.itemList = [];
            $scope.temp = {};
            $scope.temp.directIssue = '1';

            console.log('chk', Math.round(2.5));

            if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                $scope.disable_sale = true;
            }
            setTimeout(function () {
                $("#rcbItem").select2();

            }, 100);

            var today = new Date();
            //var dd = today.getDate();
            //var mm = today.getMonth() + 1; //January is 0!
            //var yyyy = today.getFullYear();

            //if (dd < 10) {
            //    dd = '0' + dd
            //}

            //if (mm < 10) {
            //    mm = '0' + mm
            //}

            ////today = yyyy + '-' + mm + '-' + dd;
            //today = dd + '-' + mm + '-' + yyyy;

            today = ('0' + today.getDate()).slice(-2) + '-' + ('0' + (today.getMonth() + 1)).slice(-2) + '-' + today.getFullYear();
            $scope.tdate = today;

            $scope.temp['provDate'] = today
            $scope.paymodeList = [
                { name: 'Cash', value: 'CS' },
                 { name: 'Cheques', value: 'CH' },
                  { name: 'Bank Transfer', value: 'BT' },
                    { name: 'Card Payment', value: 'CP' },
            ]

            $scope.temp['paymentMode'] = 'CS';
            $scope.size = function (pagesize) {
                $scope.itemsPerPage = pagesize;

            }

            $scope.enroll_change = function (str) {
                if ($scope.temp.sal_type == '05') {

                    $scope.inCustFlg = true;

                    for (var i = 0; i < $scope.rcbInternal.length; i++) {
                        if (str == $scope.rcbInternal[i].up_name) {
                            $scope.internalUser = $scope.rcbInternal[i].up_num;
                            $scope.inCustFlg = false;
                            break;
                        }
                    }

                    if ($scope.inCustFlg) {
                        $http.post(ENV.apiUrl + "api/Sales_internal/Insert_User_profiles?name=" + str).then(function (res) {

                            $scope.internalUser = res.data;
                            console.log($scope.internalUser)
                            //  $scope.rcbCategories = res.data;
                            //  if(res.data.length>0)
                            //   $scope.temp['name'] = res.data.studentname;

                        });
                    }

                }
                else if ($scope.temp.sal_type == '04') {
                    $http.get(ENV.apiUrl + "api/Sales_internal/GetStudentInfo?enrollnum=" + str).then(function (res) {
                        debugger
                        //  $scope.rcbCategories = res.data;
                        if (res.data.studentname != undefined)
                            $scope.temp['name'] = res.data.studentname + ' Class:' + res.data.grade_name + '-' + res.data.section_name;

                    });

                }

                else if ($scope.temp.sal_type == '03') {
                    $http.get(ENV.apiUrl + "api/Sales_internal/GetEmployeeInfo?enrollnum=" + str).then(function (res) {
                        debugger
                        //  $scope.rcbCategories = res.data;
                        if (res.data.studentname != undefined)
                            $scope.temp['name'] = res.data.studentname;

                    });

                }
                if (str == '' || str == undefined) {
                }
                else {
                    $scope.disable_btn = false;

                }
            }

            $http.get(ENV.apiUrl + "api/InterDepartmentTransfer_internal/GetAllItemsInSubCategoryforidt").then(function (res) {
                $scope.rcbItems = res.data;
                $scope.rcbitemOld = $scope.rcbItems;
                console.log("$scope.rcbItems", $scope.rcbItems);
            });


            $scope.range = function () {
                var rangeSize = 5;
                var ret = [];
                var start;

                start = $scope.currentPage;
                if (start > $scope.pageCount() - rangeSize) {
                    start = $scope.pageCount() - rangeSize + 1;
                }

                for (var i = start; i < start + rangeSize; i++) {
                    if (i >= 0)
                        ret.push(i);
                }

                return ret;


            };

            $scope.prevPage = function () {
                if ($scope.currentPage > 0) {
                    $scope.currentPage--;
                }
            };

            $scope.prevPageDisabled = function () {
                return $scope.currentPage === 0 ? "disabled" : "";
            };

            $scope.pageCount = function () {

                return Math.ceil($scope.obj.length / $scope.itemsPerPage) - 1;
            };

            $scope.nextPage = function () {
                if ($scope.currentPage < $scope.pageCount()) {
                    $scope.currentPage++;
                }
            };

            $scope.nextPageDisabled = function () {
                return $scope.currentPage === $scope.pageCount() ? "disabled" : "";
            };

            $scope.setPage = function (n) {
                $scope.currentPage = n;
            };


            //$http.get(ENV.apiUrl + "api/common/getAllComboBoxValues?academicYear=2016&routeCode=01").then(function (AllComboBoxValues) {
            //    $scope.ComboBoxValues = AllComboBoxValues.data;
            //});

            $scope.searchstudent = function () {
                $scope.busy = true;
                $scope.searchtable = false;
                $http.get(ENV.apiUrl + "api/common/GlobalSearch/getSearchStudent?data=" + JSON.stringify($scope.temp)).then(function (Allstudent) {

                    $scope.student = Allstudent.data;

                    $scope.busy = false;

                    $scope.searchtable = true;
                });
            }

            $scope.GetSalesTypeChange = function (str) {
                $scope.searchDisable = false;
                $scope.rcbInternal = [];
                if (str == '03') {   //staff

                    $rootScope.visible_stud = false;
                    $rootScope.visible_parent = false;
                    $rootScope.visible_search_parent = false;
                    $rootScope.visible_teacher = false;
                    $rootScope.visible_User = false;
                    $rootScope.visible_Employee = true;

                    $rootScope.chkMulti = false;


                    $scope.chk_internal = false;
                    for (var i = 0; i < $scope.rcbDocType.length; i++) {

                        if ($scope.rcbDocType[i].dt_code == '02') {
                            $scope.chk_internal = true;
                        }
                    }
                    if (!$scope.chk_internal)
                        $scope.rcbDocType.push($scope.internal_tars);


                }
                else if (str == '04') { //student

                    $rootScope.visible_stud = true;
                    $rootScope.visible_parent = false;
                    $rootScope.visible_search_parent = false;
                    $rootScope.visible_teacher = false;
                    $rootScope.visible_User = false;
                    $rootScope.visible_Employee = false;

                    $rootScope.chkMulti = false;

                    for (var i = 0; i < $scope.rcbDocType.length; i++) {

                        if ($scope.rcbDocType[i].dt_code == '02') {
                            $scope.internal_tars = angular.copy($scope.rcbDocType[i]);
                            $scope.rcbDocType.splice(i, 1);
                        }
                    }

                }
                else if (str == '05') {
                    $scope.searchDisable = true

                    $http.get(ENV.apiUrl + "api/Sales_internal/GetUserProfiles").then(function (res) {

                        $scope.rcbInternal = res.data;
                        // console.log(res.data[0])
                    });

                    for (var i = 0; i < $scope.rcbDocType.length; i++) {

                        if ($scope.rcbDocType[i].dt_code == '02') {
                            $scope.internal_tars = angular.copy($scope.rcbDocType[i]);
                            $scope.rcbDocType.splice(i, 1);
                        }
                    }
                }
            }


            $scope.$on('global_cancel', function (str) {

                if ($scope.SelectedUserLst.length > 0) {
                    debugger
                    if ($scope.temp.sal_type == '03') {
                        $scope.temp['enroll'] = $scope.SelectedUserLst[0].em_number;
                        $scope.temp['name'] = $scope.SelectedUserLst[0].empName;

                    }
                    else if ($scope.temp.sal_type == '04') {
                        $scope.temp['enroll'] = $scope.SelectedUserLst[0].s_enroll_no;
                        $scope.temp['name'] = $scope.SelectedUserLst[0].name + ' Class: ' + $scope.SelectedUserLst[0].s_class;

                    }
                }

                if ($scope.temp.enroll == '' || $scope.temp.enroll == undefined) {
                }
                else {
                    $scope.disable_btn = false;

                }
                // $scope.getstudentList();
            });

            $(window).bind('keydown', function (event) {

                if (event.ctrlKey || event.metaKey) {
                    switch (String.fromCharCode(event.which).toLowerCase()) {
                        case 'p':
                            event.preventDefault();
                            $scope.print('tableIDDiv');

                            break;


                    }
                }
            });

            $scope.edit = function (info) {
                $scope.temp['enroll'] = info.s_enroll_no;
                //  $scope.SearchStudentData();
            }

            $http.get(ENV.apiUrl + "api/Sales_internal/get_supplier_code").then(function (res) {

                $scope.cmb_supplier = res.data;
            });

            $http.get(ENV.apiUrl + "api/Sales_internal/GetAllSalesType").then(function (res) {
                debugger;
                $scope.rcbSalesType = res.data;
                if ($scope.rcbSalesType.length > 0) {
                    $scope.temp['sal_type'] = res.data[0].sal_type;
                    $scope.GetSalesTypeChange(res.data[0].sal_type);
                }

            });

            $http.get(ENV.apiUrl + "api/Sales_internal/GetSalesmanName1?sales_code=" + $rootScope.globals.currentUser.username).then(function (res) {
                debugger;
                $scope.salesman = res.data;
                console.log("saleman", res.data)
                console.log("saleman 2", $scope.salesman.sm_code);
            });

            $http.get(ENV.apiUrl + "api/Sales_internal/GetAllDocumentType").then(function (res) {

                debugger;
                $scope.rcbDocType = res.data;
                if ($scope.rcbDocType.length > 0) {
                    $scope.temp['doc_type'] = res.data[0].dt_code

                    //for (var i = 0; i < $scope.rcbDocType.length; i++) {

                    //    if ($scope.rcbDocType[i].dt_code == '02') {
                    //        $scope.internal_tars = angular.copy($scope.rcbDocType[i]);
                    //        $scope.rcbDocType.splice(i, 1);
                    //    }
                    //}
                }
            });

            $http.get(ENV.apiUrl + "api/Sales_internal/GetAllSupplierGroupName").then(function (res) {

                $scope.rcbGroup = res.data;

            });

            $http.get(ENV.apiUrl + "api/Sales_internal/get_Categories").then(function (res) {

                $scope.rcbCategories = res.data;

            });

            $http.get(ENV.apiUrl + "api/Sales_internal/GetAllBanks").then(function (res) {

                $scope.bankList = res.data;

            });

            //$scope.print = function (div) {

            //    $("#tableIDDiv").css({ display: "block" });


            //    debugger;
            //    var docHead = document.head.outerHTML;
            //    var printContents = document.getElementById(div).outerHTML;
            //    var winAttr = "location=yes, statusbar=no, menubar=no, titlebar=no, toolbar=no,dependent=no, width=865, height=600, resizable=yes, screenX=200, screenY=200, personalbar=no, scrollbars=yes";

            //    var newWin = window.open("", "_blank", winAttr);
            //    var writeDoc = newWin.document;
            //    writeDoc.open();
            //    writeDoc.write('<!doctype html><html>' + docHead + '<body onLoad="window.print()">' + printContents + '</body></html>');
            //    writeDoc.close();
            //    newWin.focus();

            //    // $scope.shds = false;
            //    $timeout(function () {
            //        $("#tableIDDiv").css({ display: "none" });

            //    }, 200);

            //}
            $scope.print = function () {
                console.log("docProvNo", $scope.docProvNo);
                if ($scope.docProvNo == undefined || $scope.docProvNo == "" || $scope.docProvNo == null) {
                    swal({ title: "Alert", text: 'Please finalized first !!', width: 360 });
                }
                else {
                    var data = {
                        location: 'Invs.INVR38ASD',
                        parameter: {
                            doc_prov_no: $scope.docProvNo
                        },
                        state: 'main.InvSII',
                        ready: function () {
                            this.refreshReport();
                        },
                    }
                    console.log(data);

                    window.localStorage["ReportDetails"] = JSON.stringify(data);
                    $state.go('main.ReportCardParameter');
                }
            }

            $scope.cancel = function () {
                $scope.temp = {}
                $scope.temp1 = {}
                $scope.GetItems = [];
                $scope.itemList = [];
                $scope.rcbInternal = [];
                $scope.im_item_code1 = '';
                $scope.temp['paymentMode'] = 'CS';
                $scope.temp['provDate'] = today
                $scope.cardpay = false;
                if ($scope.rcbSalesType.length > 0)
                    $scope.temp['sal_type'] = $scope.rcbSalesType[1].sal_type;

                if ($scope.rcbDocType.length > 0)
                    $scope.temp['doc_type'] = $scope.rcbDocType[0].dt_code



            }

            $scope.getCategories = function (str) {

                if ($scope.temp.subcategory_code == undefined)
                    $scope.rcbItems = [];
                if ($scope.temp.subcategory_code == '')
                    $scope.rcbItems = [];

                $http.get(ENV.apiUrl + "api/Sales_internal/get_SubCategories?pc_parentcode=" + str).then(function (res) {
                    $scope.rcbSubCategories = res.data;
                    console.log("rcbSubCategories",$scope.rcbSubCategories);
                });

            }

            $scope.getSubCategories = function (str) {

                $http.get(ENV.apiUrl + "api/InterDepartmentTransfer_internal/GetAllItemsInSubCategoryNewidt?pc_code=" + str).then(function (res) {

                    $scope.rcbItems = res.data;

                });

            }

            $scope.qtychange = function (str) {

                if (parseInt($scope.resitem.original_qty) >= parseInt($scope.temp.qty) || $scope.temp.qty == '') {
                    $scope.temp.finalValue = ($scope.temp.qty * parseFloat($scope.temp.im_sell_price));
                    //$scope.totalAmtClick()
                    $scope.totalAmtClick();
                }
                else {
                    swal({
                        title: "Alert", text: "Entered " + $scope.temp.qty + " Item quantity is more than current quantity " + $scope.resitem.original_qty + ".",
                    });

                    $scope.temp['qty'] = 1;
                    $scope.temp.finalValue = ($scope.temp.qty * parseFloat($scope.temp.im_sell_price));
                    $scope.totalAmtClick();

                }

            }

            $scope.qtychangeby = function (str) {

                if (parseInt(str.original_qty) >= parseInt(str.dd_qty) || str.dd_qty == '') {
                    str.dd_sell_value_final = (str.dd_qty * parseFloat(str.dd_sell_price));
                    // $scope.totalAmtClick()
                    $scope.totalAmtClick();
                }

                else {
                    swal({ title: "Alert", text: "Entered " + str.dd_qty + " Item quantity is more than current quantity " + str.original_qty + ".", });

                    str['dd_qty'] = 1;
                    str.dd_sell_value_final = (str.dd_qty * parseFloat(str.dd_sell_price));
                    $scope.totalAmtClick();

                }

            }

            var im_item_code = '';
            var im_inv_no = '';

            $scope.getitemCode = function (str) {
                console.log("str", str);
                $('#dept').val(str.dep_code); // set department value
                $scope.checkIssued = false;

                $http.get(ENV.apiUrl + "api/Sales_internal/GetAlreadyIssueItem?im_inv_no=" + str.im_inv_no + '&cus_account_no=' + $scope.temp.enroll).then(function (res) {
                    $scope.checkAlreadyIssueItem = res.data;
                    console.log("checkAlreadyIssueItem", $scope.checkAlreadyIssueItem);
                    if ($scope.checkAlreadyIssueItem.length > 0) {
                        swal({ title: 'Alert', text: str.im_item_code + " Item already issue to " + $scope.temp.enroll, showCloseButton: true, width: 380 });
                        $scope.disable_btn = true;
                        $scope.checkIssued = true;
                    }
                });

                if ($scope.checkIssued == false) {
                    $http.get(ENV.apiUrl + "api/Sales_internal/GetItemCodeDetails?item_code=" + str.im_item_code + '&sg_name=' + $scope.temp.gcode).then(function (res) {

                        $scope.resitem = res.data[0];
                        console.log("resitem", $scope.resitem);
                        if ($scope.resitem.original_qty == 0) {
                            $scope.disable_btn = true;
                            swal({
                                title: 'Alert',
                                text: str.im_item_code +" Item quantity is 0.Please Update Selected Item Quantity.",
                                showCloseButton: true,
                                showCancelButton: true,
                                confirmButtonText: 'Yes',
                                width: 380,
                                cancelButtonText: 'No',

                            }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $state.go('main.InvPRD');

                                    //$scope.$emit('sendItemToRequestDetails', $scope.sendItemToRequestDetails);
                                    if ($scope.temp.doc_prov_no == undefined || $scope.temp.doc_prov_no == "" || $scope.temp.doc_prov_no == null) {
                                        localStorage.setItem('itemCode', str.im_item_code);
                                        localStorage.setItem('deptCode', str.dep_code);
                                        localStorage.setItem('reqType', 'I');
                                        localStorage.setItem('fromItemIssue', 'Y');
                                        //$rootScope.fromItemIssue = 'Y';
                                    }
                                    else {
                                        localStorage.setItem('itemCode', $scope.sendItemToRequestDetails.im_item_code);
                                        localStorage.setItem('deptCode', $scope.sendItemToRequestDetails.dep_code);
                                        localStorage.setItem('reqType', $scope.sendItemToRequestDetails.req_type);
                                        localStorage.setItem('fromItemIssue', 'Y');
                                        //$rootScope.fromItemIssue = 'Y';
                                    }
                                    console.log("broad cast calleed", $scope.sendItemToRequestDetails);
                                }
                            });
                        }
                        else {
                            //$scope.disable_btn = false;
                            localStorage.clear();
                        }

                        //if ($scope.resitem.original_qty > 0) {

                        //    if ($scope.temp.doc_type == '02') {
                        //        $scope.temp.qty = 1;
                        //        $scope.temp.im_sell_price = $scope.resitem.im_sell_price;
                        //        $scope.temp.finalValue = 0;
                        //    }
                        //    else {
                        //        $scope.temp.qty = 1;
                        //        $scope.temp.im_sell_price = $scope.resitem.im_sell_price;
                        //        $scope.temp.finalValue = (1 * parseFloat($scope.resitem.im_sell_price));
                        //    }

                        //}
                        //else {
                        //    swal({ title: "Alert", text: "Item quantity is 0.Please Update Selected Item Quantity.", imageUrl: "assets/img/notification-alert.png", });

                        //}


                        //$http.get(ENV.apiUrl + "api/Sales_internal/GetItemCodeLocations?im_inv_no=" + str.im_inv_no).then(function (res) {

                        //    $scope.resloc = res.data;
                        //    if ($scope.resloc.length > 0)
                        //        $scope.temp.loc_code = $scope.resloc[0].loc_code;

                        //    $scope.temp['gcode'] = str.sg_name
                        //    //$scope.temp.loc_code
                        //});

                    });

                }

                ///////////////////////////////////////////

                $scope.item_desc = str;
                im_item_code = '';
                $scope.resitem = [];
                $scope.temp['qty'] = '';
                $scope.temp['im_sell_price'] = '';
                $scope.temp['finalValue'] = '';
                $scope.temp['gcode'] = '';

                // var im_code = $scope.item_desc.split('-')[0];

                for (var i = 0; i < $scope.rcbItems.length; i++) {
                    if (str.im_item_code == $scope.rcbItems[i].im_item_code) {

                        im_item_code = $scope.rcbItems[i].im_item_code;
                        im_inv_no = $scope.rcbItems[i].im_inv_no;
                        $scope.temp.qty = 1;
                        $scope.temp.im_sell_price = $scope.rcbItems[i].im_sell_price;
                        $scope.temp.finalValue = (1 * parseFloat($scope.rcbItems[i].im_sell_price));
                        $scope.temp['gcode'] = $scope.rcbItems[i].sg_name;
                        $scope.original_qty = $scope.rcbItems[i].original_qty;
                        $scope.temp.original_qty = $scope.rcbItems[i].original_qty;
                        break;
                    }
                }


                $scope.resloc = [];

                $scope.temp['loc_code'] = '';

                $http.get(ENV.apiUrl + "api/InterDepartmentTransfer_internal/GetItemCodeLocationsidt?im_inv_no=" + im_inv_no).then(function (res) {

                    $scope.resloc = res.data;
                    if ($scope.resloc.length > 0)
                        $scope.temp.loc_code = $scope.resloc[0].loc_code;
                    $scope.temp.loc_code
                });



                //document.getElementById('addbtn').focus();
            }

            $scope.add = function () {
                debugger;

                if ($scope.im_item_code1.im_inv_no == '' || $scope.im_item_code1.im_inv_no == undefined) {
                }

                else {
                    $scope.totalFinal = 0;
                    $scope.flg = false;
                    //for (var i = 0; i < $scope.itemList.length; i++) {
                    //    if ($scope.itemList[i].im_inv_no == $scope.im_item_code1.im_inv_no) {

                    //        $scope.itemList[i].dd_qty += 1;
                    //        $scope.itemList[i].dd_sell_value_final += $scope.temp.im_sell_price
                    //        // $scope.itemList[i].im_inv_no
                    //        $scope.flg = true;
                    //        i = $scope.itemList.length;
                    //    }
                    //}

                    //= false;
                    if ($scope.flg == false) {

                        if ($scope.temp.doc_type == '02') {
                            var data =
                                {
                                    //   sg_name: $("#rcbGroup").find("option:selected").text(),

                                    sg_name: $scope.temp.gcode,
                                    sg_code: $scope.temp.gcode,
                                    im_inv_no: $scope.im_item_code1.im_inv_no,
                                    im_item_code: $scope.im_item_code1.im_item_code,
                                    im_desc: $("#rcbItem").find("option:selected").text(),
                                    // item_location_name: $("#rcbLocation").find("option:selected").text(),
                                    item_location_name: $scope.temp.loc_code,
                                    loc_code: $scope.temp.loc_code,
                                    original_qty: $scope.temp.original_qty,
                                    dd_qty: $scope.temp.qty,
                                    dd_sell_price: $scope.temp.im_sell_price,
                                    dd_sell_value_final: 0,
                                    doc_remark: $scope.temp.doc_remark,
                                    dep_code: $('#dept').val()

                                }
                            $scope.flg_chk = true;
                            for (var j = 0; j < $scope.itemList.length; j++) {

                                debugger;

                                if ($scope.itemList[j].im_item_code == $scope.im_item_code1.im_item_code) {
                                    $scope.flg_chk = false;
                                }
                            }

                            if ($scope.flg_chk) {
                                $scope.itemList.push(data)
                            }
                        }
                        else {

                            var data =
                               {
                                   //   sg_name: $("#rcbGroup").find("option:selected").text(),

                                   sg_name: $scope.temp.gcode,
                                   sg_code: $scope.temp.gcode,
                                   im_inv_no: $scope.im_item_code1.im_inv_no,
                                   im_item_code: $scope.im_item_code1.im_item_code,
                                   im_desc: $("#rcbItem").find("option:selected").text(),
                                   // item_location_name: $("#rcbLocation").find("option:selected").text(),
                                   item_location_name: $scope.temp.loc_code,
                                   loc_code: $scope.temp.loc_code,
                                   original_qty: $scope.temp.original_qty,
                                   dd_qty: $scope.temp.qty,
                                   dd_sell_price: $scope.temp.im_sell_price,
                                   dd_sell_value_final: $scope.temp.finalValue,
                                   doc_remark: $scope.temp.doc_remark,
                                   dep_code: $('#dept').val()

                               }
                            $scope.flg_chk = true;
                            for (var j = 0; j < $scope.itemList.length; j++) {

                                debugger;

                                if ($scope.itemList[j].im_item_code == $scope.im_item_code1.im_item_code) {
                                    $scope.flg_chk = false;
                                }
                            }

                            if ($scope.flg_chk) {
                                $scope.itemList.push(data)
                            }

                        }

                    }
                }
                $scope.totalAmtClick();


                $scope.CancelItem();



                document.getElementById("rcbItem").focus();

            }

            $scope.finalize = function () {
                $scope.disable_btn = true;

                //if (!string.IsNullOrEmpty(salesman_cd))
                //    invsObj.sm_code = salesman_cd;
                //else
                //    invsObj.sm_code = txt_sm_code.Text;
                debugger;

                if ($scope.temp.sal_type == '05') {
                    $scope.temp['enroll'] = $scope.internalUser;
                }
                if ($('#dept').val() == undefined || $('#dept').val() == null || $('#dept').val() == "") {
                    var deptId = '10';
                }
                else {
                    var deptId = $('#dept').val();
                }

                var data = {
                    //dep_code: '10',
                    dep_code: deptId,
                    doc_prov_no: '',
                    doc_prov_date: $scope.temp.provDate,
                    dt_code: $scope.temp.doc_type,
                    sm_code: $scope.salesman.sm_code,//101
                    up_name: 'admin',
                    creation_user: $rootScope.globals.currentUser.username,
                    sal_type: $scope.temp.sal_type,
                    doc_total_amount: $scope.temp.totalFinal,
                    //doc_status: '2',
                    doc_status: '2',
                    cus_account_no: $scope.temp.enroll,
                    doc_special_name: $scope.temp.special_nm,
                    dep_code_caused_by: deptId,
                    doc_order_ref_no: '',
                    doc_other_charge_amount: $scope.temp.otherCharges,
                    doc_discount_pct: $scope.temp.overalldis,
                    doc_discount_amount: $scope.temp.disamt,
                    creation_date: today,
                    doc_date: $scope.temp.provDate,
                    doc_narration: $scope.temp.doc_narration
                }

                $http.post(ENV.apiUrl + "api/Sales_internal/Insert_Sale_Documents", data).then(function (msg) {
                    var d = msg.data;
                    $scope.docProvNo = d;
                    $scope.salesDetails = [];
                    for (var i = 0; i < $scope.itemList.length; i++) {
                        debugger;
                        var paymode = $scope.temp.paymentMode;
                        //if (paymode=='CH')

                        var data = {
                            dep_code: deptId,
                            doc_prov_no: msg.data,
                            dd_line_no: i + 1,
                            //dd_status: '2',
                            dd_status: '2',
                            dd_payment_mode: paymode,
                            im_inv_no: $scope.itemList[i].im_inv_no,
                            loc_code: $scope.itemList[i].loc_code,
                            dd_sell_price: $scope.itemList[i].dd_sell_price,
                            dd_sell_price_discounted: 0,
                            dd_sell_price_final: 0,
                            dd_sell_value_final: $scope.itemList[i].dd_sell_value_final,
                            dd_sell_cost_total: 0,
                            dd_qty: $scope.itemList[i].dd_qty,
                            dd_outstanding_qty: 0,
                            dd_line_discount_pct: 0,
                            dd_line_discount_amount: 0,
                            dd_line_total: 0,
                            dd_physical_qty: 0,
                            dd_cheque_number: $scope.temp.chequeNo,
                            dd_cheque_date: $scope.temp.chequeDate,
                            dd_cheque_bank_code: $scope.temp.bankCode,
                            dd_remarks: $scope.itemList[i].doc_remark

                            //dd_physical_qty:$scope.itemList[i].original_qty,

                        }
                        $scope.salesDetails.push(data)
                    }
                    $http.post(ENV.apiUrl + "api/Sales_internal/Insert_Sale_Documents_Details1", $scope.salesDetails).then(function (msg) {
                        if (msg.data) {
                            //swal({ title: "Alert", text: d + " Record Inserted Success.", imageUrl: "assets/img/notification-alert.png", });
                            swal({
                                title: '',
                                text: "Receipt  " + d + "  inserted Successfully.Do you want print receipt?",
                                imageUrl: "assets/img/check.png",
                                showCloseButton: true,
                                showCancelButton: true,
                                confirmButtonText: 'Yes',
                                width: 380,
                                cancelButtonText: 'No',
                                allowOutsideClick: false,
                            }).then(function (isConfirm) {
                                if (isConfirm) {

                                    var data = {
                                        location: 'Invs.INVR38DPSD',
                                        parameter: {
                                            doc_prov_no: d
                                        },
                                        state: 'main.InvSII',
                                        ready: function () {
                                            this.refreshReport();
                                        },
                                    }
                                    console.log(data);

                                    window.localStorage["ReportDetails"] = JSON.stringify(data);
                                    $state.go('main.ReportCardParameter');

                                }
                            });
                            $scope.cancel();

                        }
                        else {
                            swal({ text: "Record Not Inserted .", imageUrl: "assets/img/close.png", });
                            $scope.disable_btn = false;
                        }
                    });
                });
            }

            $scope.itemSearch = function () {
                $scope.GetItems = []
                $scope.temp1 = {}
                $('#itemSearch').modal('show');
                $scope.itemsetflg = false;
            }

            $scope.ItemSetSearch = function () {
                $scope.GetItems = []
                $scope.temp1 = {}

                $('#itemSearch').modal('show');
                $scope.itemsetflg = true;
                $scope.temp1['im_assembly_ind'] = true;
                $http.post(ENV.apiUrl + "api/Sales_internal/postgetItemSerch", $scope.temp1).then(function (res) {
                    if (res.data.length > 0)
                        $scope.GetItems = res.data;
                    else
                        swal({ text: "No Record Found.", imageUrl: "assets/img/close.png", });


                });

            }

            $scope.getfetch = function () {

                //var data = $scope.temp1;
                if ($scope.itemsetflg) {
                    $scope.temp1['im_assembly_ind'] = true;
                }
                else {
                    $scope.temp1['im_assembly_ind'] = false;


                }
                //$scope.temp1['im_assembly_ind'] = true;
                //$scope.temp1['im_inv_no'] = '';

                debugger;
                $http.post(ENV.apiUrl + "api/Sales_internal/postgetItemSerch", $scope.temp1).then(function (res) {
                    if (res.data.length > 0)
                        $scope.GetItems = res.data;
                    else
                        swal({ text: "No Record Found.", imageUrl: "assets/img/close.png", });


                });
            }

            $scope.okbuttonclick = function (j) {

                //old Code
                for (var i = 0; i < $scope.GetItems.length; i++) {
                    var t = $scope.GetItems[i].im_inv_no + i;
                    var v = document.getElementById(t);
                    $scope.flg_chk = true;
                    if (v.checked == true) {

                        //check if this item is already added
                        for (var j = 0; j < $scope.itemList.length; j++) {

                            debugger;

                            if ($scope.itemList[j].im_item_code == $scope.GetItems[i].im_item_code) {
                                $scope.flg_chk = false;
                            }
                        }

                        if ($scope.flg_chk) {
                            var data =
                            {
                                sg_name: $scope.GetItems[i].sg_desc,
                                //  sg_code: $scope.GetItems[i].gcode,
                                im_inv_no: $scope.GetItems[i].im_inv_no,
                                im_item_code: $scope.GetItems[i].im_item_code,
                                im_desc: $scope.GetItems[i].im_desc,
                                item_location_name: $scope.GetItems[i].loc_code,
                                loc_code: $scope.GetItems[i].loc_code,
                                original_qty: $scope.GetItems[i].id_cur_qty,
                                dd_qty: 1,
                                dd_sell_price: $scope.GetItems[i].im_sell_price,
                                dd_sell_value_final: 0,
                            }

                            $scope.itemList.push(data)
                        }
                    }
                }
            }

            $scope.calculateTotal = function () {
                $scope.totalFinal = 0;
                for (var i = 0; i < $scope.itemList.length; i++) {

                    $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_final);
                }
                $scope.temp['totalFinal'] = $scope.totalFinal;

                $scope.temp.totalFinal = Math.round(parseFloat($scope.temp.totalFinal));


            }
            $scope.chk_click_item = function (obj, flg) {
                if ($scope.itemsetflg) {

                    //    obj.im_inv_no

                    $http.post(ENV.apiUrl + "api/Sales_internal/Fetch_ItemDetails_ItemSet?im_inv_no=" + obj.im_inv_no).then(function (res) {

                        console.log(res.data);
                        // $scope.rcbSubCategories = res.data;

                        for (var i = 0; i < res.data.length; i++) {
                            if ($scope.temp.doc_type == '02') {
                                $scope.internal_im_sell_price = 0;
                            }
                            else {
                                $scope.internal_im_sell_price = parseFloat(res.data[i].im_sell_price);
                            }

                            var data =
                   {
                       sg_name: res.data[i].sg_name,
                       sg_code: res.data[i].sg_name,
                       im_inv_no: res.data[i].im_inv_no,
                       im_item_code: res.data[i].im_item_code,
                       im_desc: res.data[i].im_desc,
                       item_location_name: res.data[i].loc_code,
                       loc_code: res.data[i].loc_code,
                       original_qty: res.data[i].id_cur_qty,
                       dd_qty: res.data[i].ia_component_qty,
                       dd_sell_price: $scope.internal_im_sell_price,
                       dd_sell_value_final: $scope.internal_im_sell_price,

                   }
                            $scope.itemList.push(data)

                        }
                        $scope.totalAmtClick();



                    });

                    $('#itemSearch').modal('hide');
                }
                else {
                    $scope.Exflg = false;

                    if ($scope.temp.doc_type == '02') {
                        $scope.internal_im_sell_price = parseFloat(obj.im_sell_price);
                        $scope.internal_im_sell_price_final = 0;

                    }
                    else {
                        $scope.internal_im_sell_price = parseFloat(obj.im_sell_price);
                        $scope.internal_im_sell_price_final = parseFloat(obj.im_sell_price);

                    }
                    var data =
                    {
                        sg_name: obj.sg_name,
                        //  sg_code: $scope.GetItems[i].gcode,
                        im_inv_no: obj.im_inv_no,

                        im_item_code: obj.im_item_code,
                        im_desc: obj.im_desc,
                        item_location_name: obj.loc_code,
                        loc_code: obj.loc_code,
                        original_qty: obj.id_cur_qty,
                        dd_qty: 1,
                        dd_sell_price: $scope.internal_im_sell_price,
                        dd_sell_value_final: $scope.internal_im_sell_price_final,

                    }

                    if (flg) {

                        //for (var i = 0; i < $scope.itemList.length; i++) {
                        //    if ($scope.itemList[i].im_inv_no == obj.im_inv_no) {

                        //        $scope.itemList[i].dd_qty += 1;
                        //        $scope.itemList[i].dd_sell_value_final += parseFloat(obj.im_sell_price)
                        //        // $scope.itemList[i].im_inv_no
                        //        $scope.Exflg = true;
                        //        i = $scope.itemList.length;
                        //    }
                        //}

                        if ($scope.Exflg == false)
                            //$scope.itemList.push(data)

                            $scope.totalAmtClick();


                    }
                    else {
                        //   $scope.itemList.pop(data)

                        for (var i = 0; i < $scope.itemList.length; i++) {
                            if ($scope.itemList[i].im_inv_no == obj.im_inv_no) {
                                if ($scope.itemList[i].dd_qty > 1) {
                                    $scope.itemList[i].dd_qty -= 1;
                                    $scope.itemList[i].dd_sell_value_final -= parseFloat(obj.im_sell_price)

                                }
                                else
                                    $scope.itemList.splice(i, 1);

                            }
                        }
                        $scope.totalAmtClick();

                    }
                }

            }

            $scope.CancelItem = function () {
                $scope.temp['finalValue'] = ''
                $scope.temp['im_sell_price'] = ''
                $scope.temp['qty'] = ''
                $scope.temp['loc_code'] = ''
                $scope.temp['subcategory_code'] = ''
                $scope.temp['category_code'] = ''
                $scope.temp['gcode'] = ''
                $scope.temp['doc_remark'] = ''
                $scope.rcbItems = [];
                //  $scope.rcbCategories = [];
                $scope.rcbSubCategories = [];
                $scope.im_item_code1 = '';
                $("#rcbItem").select2("val", "");


                $scope.rcbItems = $scope.rcbitemOld;

                //$http.get(ENV.apiUrl + "api/Sales_internal/GetAllItemsInSubCategoryNew?pc_code=" + '').then(function (res) {

                //    $scope.rcbItems = res.data;

                //});


            }

            $scope.studentSearch = function () {
                $scope.global_Search_click();
                $('#Global_Search_Modal').modal({ backdrop: "static" });
                //$('#stdSearch').modal('show');

            }

            $scope.totalAmtClick = function () {
                $scope.totalFinal = 0;
                for (var i = 0; i < $scope.itemList.length; i++) {

                    $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_final);
                }
                $scope.temp['totalFinal'] = $scope.totalFinal;
                if ($scope.temp.otherCharges == undefined || $scope.temp.otherCharges == '') {
                }
                else {
                    $scope.temp.totalFinal += parseFloat($scope.temp.otherCharges);
                }

                if ($scope.temp.disamt == undefined || $scope.temp.disamt == '' || isNaN(parseFloat($scope.temp.disamt))) {
                }
                else {
                    $scope.temp.totalFinal -= parseFloat($scope.temp.disamt);
                }

                //   $scope.temp['totalFinal'] = $scope.totalFinal + (parseFloat($scope.temp.otherCharges) - parseFloat($scope.temp.disamt));

                $scope.temp.totalFinal = Math.round(parseFloat($scope.temp.totalFinal));


            }

            $scope.otherChargesChange = function () {
                $scope.totalFinal = 0;
                for (var i = 0; i < $scope.itemList.length; i++) {

                    $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_final);
                }
                $scope.temp['totalFinal'] = $scope.totalFinal;
                if ($scope.temp.otherCharges == undefined || $scope.temp.otherCharges == '') {
                }
                else {
                    $scope.temp.totalFinal += parseFloat($scope.temp.otherCharges);
                }

                if ($scope.temp.disamt == undefined || $scope.temp.disamt == '') {
                }
                else {
                    $scope.temp.totalFinal -= parseFloat($scope.temp.disamt);
                }

                $scope.temp.totalFinal = Math.round(parseFloat($scope.temp.totalFinal));

            }

            $scope.txt_discounted_price_TextChanged = function (str) {
                if (str <= 100 || str <= '100') {

                    $scope.totalFinal = 0;
                    for (var i = 0; i < $scope.itemList.length; i++) {

                        $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_final);
                    }
                    if (str == undefined || str == '') {
                        $scope.temp['disamt'] = 0;
                    }
                    else {
                        $scope.temp['disamt'] = (parseFloat($scope.totalFinal)) * (parseFloat(str) / 100);
                    }
                    $scope.df = ($scope.totalFinal) - $scope.temp['disamt'];

                    //$scope.temp['totalFinal'] = $scope.df;


                    $scope.temp['totalFinal'] = $scope.totalFinal;
                    if ($scope.temp.otherCharges == undefined || $scope.temp.otherCharges == '') {
                    }
                    else {
                        $scope.temp.totalFinal += parseFloat($scope.temp.otherCharges);
                    }

                    if ($scope.temp.disamt == undefined || $scope.temp.disamt == '') {
                    }
                    else {
                        $scope.temp.totalFinal -= parseFloat($scope.temp.disamt);
                    }

                }
                else {
                    $scope.totalFinal = 0;
                    for (var i = 0; i < $scope.itemList.length; i++) {

                        $scope.totalFinal += parseFloat($scope.itemList[i].dd_sell_value_final);
                    }

                    $scope.temp['totalFinal'] = $scope.totalFinal;
                    //  str = 0;
                    $scope.temp['overalldis'] = 0;
                    $scope.temp['disamt'] = 0;
                    if ($scope.temp.otherCharges == undefined || $scope.temp.otherCharges == '') {
                    }
                    else {
                        $scope.temp.totalFinal += parseFloat($scope.temp.otherCharges);
                    }

                    if ($scope.temp.disamt == undefined || $scope.temp.disamt == '') {
                    }
                    else {
                        $scope.temp.totalFinal -= parseFloat($scope.temp.disamt);
                    }

                    swal({ text: "discount Cant greater than 100%.", imageUrl: "assets/img/notification-alert.png", });

                }

                $scope.temp.totalFinal = Math.round(parseFloat($scope.temp.totalFinal));

            }

            $scope.paymentChange = function (str) {
                $scope.cardpay = false;
                if (str == 'CS') {
                    $scope.chequeShow = false;
                    $scope.bankShow = false;
                }
                else if (str == 'CH') {
                    $scope.chequeShow = true;
                    $scope.bankShow = false;
                }
                else if (str == 'BT') {
                    $scope.chequeShow = false;
                    $scope.bankShow = true;
                }

                else if (str == 'CP') {
                    $scope.chequeShow = false;
                    $scope.bankShow = false;
                    $scope.cardpay = true;

                }

            }

            $scope.removeItem = function (obj, index) {

                $scope.itemList.splice(index, 1);
                $scope.totalAmtClick();
                $scope.disable_btn = false;
            }

            $scope.onlyNumbers = function (event) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };


            $scope.getItemRequest = function () {
                $http.get(ENV.apiUrl + "api/ItemRequests/GetItemRequest?login_user=" + $rootScope.globals.currentUser.username).then(function (res) {
                    $scope.ItemRequest = res.data;
                    console.log("ItemRequest", $scope.ItemRequest);
                });
            }
            $scope.getItemRequest();

            $scope.getApprovedItemRequest = function (doc_prov_no) {
                $http.get(ENV.apiUrl + "api/ItemRequests/GetApprovedItemRequest?doc_prov_no=" + doc_prov_no).then(function (res) {
                    $scope.ApprovedItemRequest = res.data.table;
                    $scope.itemList = [];

                    console.log("ApprovedItemRequest", $scope.ApprovedItemRequest);
                    if ($scope.ApprovedItemRequest.length > 0) {
                        $scope.disable_btn = false;

                        $('#dept').val($scope.ApprovedItemRequest[0].dep_code);
                        $scope.temp.enroll = $scope.ApprovedItemRequest[0].reqcreatedby;
                        $scope.enroll_change($scope.temp.enroll);

                        for (var i = 0; i < $scope.ApprovedItemRequest.length; i++) {
                            var data = {
                                sg_name: $scope.ApprovedItemRequest[i].rd_item_group,
                                //sg_code: $scope.ApprovedItemRequest[0].rd_item_code,
                                im_inv_no: $scope.ApprovedItemRequest[i].im_inv_no,
                                im_item_code: $scope.ApprovedItemRequest[i].rd_item_code,
                                im_desc: $scope.ApprovedItemRequest[i].rd_item_desc,
                                // item_location_name: $("#rcbLocation").find("option:selected").text(),
                                item_location_name: $scope.ApprovedItemRequest[i].loc_code,
                                loc_code: $scope.ApprovedItemRequest[i].loc_code,
                                original_qty: $scope.ApprovedItemRequest[i].rd_current_qty,
                                dd_qty: $scope.ApprovedItemRequest[i].final_qnt,
                                //dd_sell_price: $scope.ApprovedItemRequest[0].im_sell_price,
                                //dd_sell_value_final: $scope.ApprovedItemRequest[0].finalValue,
                                doc_remark: $scope.ApprovedItemRequest[i].doc_remark,
                                dep_code: $scope.ApprovedItemRequest[i].dep_code,
                            }
                            $scope.itemList.push(data);
                            $scope.getitemCode(data);
                        }

                        $scope.sendItemToRequestDetails = {
                            im_item_code: $scope.ApprovedItemRequest[0].rd_item_code,
                            dep_code: $scope.ApprovedItemRequest[0].dep_code,
                            req_type: $scope.ApprovedItemRequest[0].req_type
                        }
                      
                        
                        
                    }
                });


                // listen for the event in the relevant $scope
                //$scope.$on('myCustomEvent', function (event, data) {
                //  console.log(data); // 'Data to send'
                //});


            }
            $scope.getApprovedItemRequest();

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });
        }])
})();