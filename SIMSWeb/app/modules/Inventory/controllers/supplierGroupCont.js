﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('supplierGroupCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "10";
            $scope.pager = true;
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            $scope.oldsgname = '';
            $scope.user_access = [];

            $scope.appcode = $state.current.name.split('.');
            var user = $rootScope.globals.currentUser.username;

            $http.get(ENV.apiUrl + "api/AlertTransaction/Get_user_access_rights?username=" + user + "&appl_code=" + $scope.appcode[1]).then(function (usr_rights) {
                debugger;
                $scope.user_rights = usr_rights.data;

                for (var i = 0; i < $scope.user_rights.length; i++) {

                    if (i == 0) {
                        $scope.user_access['data_read'] = $scope.user_rights[i].comn_user_read;
                        $scope.user_access['data_insert'] = $scope.user_rights[i].comn_user_insert;
                        $scope.user_access['data_update'] = $scope.user_rights[i].comn_user_update;
                        $scope.user_access['data_delete'] = $scope.user_rights[i].comn_user_delete;
                        $scope.user_access['data_massupdate'] = $scope.user_rights[i].comn_user_massupdate;
                        $scope.user_access['is_admin'] = $scope.user_rights[i].comn_user_admin;
                    }
                }

                //console.log($scope.user_access);
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            //Select Data SHOW


            $http.get(ENV.apiUrl + "api/SupplierGroup/getSupplierGroupsType").then(function (res) {

                $scope.cmbsg_type = res.data;
                

            });


            $http.get(ENV.apiUrl + "api/SupplierGroup/getSupplierGroups").then(function (res) {

                $scope.sg_name = res.data;
                $scope.totalItems = $scope.sg_name.length;
                $scope.todos = $scope.sg_name;
                $scope.makeTodos();
               
                console.log($scope.sg_name);

            });

            
            $scope.size = function (str) {
                if (str == "All" || str == "all") {
                    $scope.currentPage = '1';
                    $scope.filteredTodos = $scope.todos;
                    $scope.pager = false;
                }
                else {
                    $scope.pager = true;
                    $scope.pagesize = str;
                    $scope.currentPage = 1;
                    $scope.numPerPage = str;
                    $scope.makeTodos();
                }
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
                console.log("currentPage=" + $scope.currentPage); $scope.makeTodos();

                main.checked = false;
                $scope.row1 = '';
                // $scope.CheckAllChecked();

            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 10, $scope.maxSize = 10;

            $scope.makeTodos = function () {
                $scope.filteredTodos = [];
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);


                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };


            //Search here
            $scope.search = function () {

                $scope.todos = $scope.searched($scope.sg_name, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.sg_name;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
               

            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.sg_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.sg_desc.toLowerCase().indexOf(toSearch.toLowerCase()) > -1) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {
                if ($scope.user_access.data_insert == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {

                    $scope.divcode_readonly = false;
                    $scope.table = false;
                    $scope.display = true;
                    $scope.save_btn = true;
                    $scope.Update_btn = false;
                    $scope.temp = {};
                    $scope.temp.sg_name = '';
                    $scope.temp.sg_desc = '';
                    $scope.temp.sg_type = '';
                    $scope.Myform.$setPristine();
                    $scope.Myform.$setUntouched();
                }
            }

            //DATA CANCEL
            $scope.Cancel = function () {
                $scope.temp = {};
                $scope.temp.sg_name = '';
                $scope.temp.sg_desc = '';
                $scope.temp.sg_type = '';
                $scope.table = true;
                $scope.display = false;

                $http.get(ENV.apiUrl + "api/SupplierGroup/getSupplierGroups").then(function (res) {

                    $scope.sg_name = res.data;
                    $scope.totalItems = $scope.sg_name.length;
                    $scope.todos = $scope.sg_name;
                    $scope.makeTodos();
                });

                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {
                if ($scope.user_access.data_update == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {


                    $scope.table = false;
                    $scope.display = true;
                    $scope.save_btn = false;
                    $scope.Update_btn = true;
                    $scope.divcode_readonly = true;

                    $scope.temp = str;
                }
            }

            //DATA SAVE INSERT
           
            $scope.savedata = function (Myform) {
                var datasend = [];
                if (Myform) {
                    var data = $scope.temp;
                    data.opr = 'I';
                    datasend.push(data);
                    $http.post(ENV.apiUrl + "api/SupplierGroup/CUDSupplierGroups", datasend).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({
                                text: "Record Inserted Successfully", imageUrl: "assets/img/check.png",
                                width: 380, height: 200
                            });
                         
                            $http.get(ENV.apiUrl + "api/SupplierGroup/getSupplierGroups").then(function (res) {
                                $scope.sg_name = res.data;
                                $scope.totalItems = $scope.sg_name.length;
                                $scope.todos = $scope.sg_name;
                                $scope.makeTodos();
                            });
                        }
                        else {
                            swal({ text: "Supplier Group Name Already Present. " + $scope.msg1, imageUrl: "assets/img/close.png", width: 380, height: 200 });
                        }

                    });
                    datasend = [];
                    $scope.table = true;
                    $scope.display = false;
                }

            }

            //DATA UPADATE
            var dataforUpdate = [];
            $scope.update = function (Myform) {
                debugger;
                if (Myform) {
                    var data = $scope.temp;
                    data.opr = "U";
                    dataforUpdate.push(data);
                    //dataupdate.push(data);
                    $http.post(ENV.apiUrl + "api/SupplierGroup/CUDSupplierGroups", dataforUpdate).then(function (msg) {
                        $scope.msg1 = msg.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                            $http.get(ENV.apiUrl + "api/SupplierGroup/getSupplierGroups").then(function (res) {
                                $scope.sg_name = res.data;
                                $scope.totalItems = $scope.sg_name.length;
                                $scope.todos = $scope.sg_name;
                                $scope.makeTodos();
                                console.log($scope.sg_name);

                            });

                        }
                        else if ($scope.msg1 == false) {
                            swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                        }
                        else {
                            swal("Error-" + $scope.msg1)
                        }
                      

                    });
                    dataforUpdate = [];
                    $scope.table = true;
                    $scope.display = false;
                }
            }

            // Data DELETE RECORD
            $scope.CheckAllChecked = function () {
                debugger;
                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('test-'+i);
                        v.checked = true;
                        $scope.row1 = 'row_selected';
                        $('tr').addClass("row_selected");
                    }
                }
                else {

                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('test-' + i);
                        v.checked = false;
                        main.checked = false;
                        $scope.row1 = '';
                    }
                }

            }

            $scope.checkonebyonedelete = function () {
                debugger;
                $("input[type='checkbox']").change(function (e) {
                    if ($(this).is(":checked")) {
                        $(this).closest('tr').addClass("row_selected");
                        $scope.color = '#edefef';
                    } else {
                        $(this).closest('tr').removeClass("row_selected");
                        $scope.color = '#edefef';
                    }
                });

                main = document.getElementById('mainchk');
                if (main.checked == true) {
                    main.checked = false;
                    $("input[type='checkbox']").change(function (e) {
                        if ($(this).is(":checked")) {
                            $(this).closest('tr').addClass("row_selected");
                        }
                        else {
                            $(this).closest('tr').removeClass("row_selected");
                        }
                    });
                }
            }


            $scope.OkDelete = function () {
                if ($scope.user_access.data_delete == false) {
                    swal({ title: "Alert", text: "Login user does not have required privilege to perform this action.", width: 350, height: 200 });
                }
                else {
                    deletefin = [];
                    $scope.flag = false;
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById('test-' + i);
                        if (v.checked == true) {
                            $scope.flag = true;
                            var deletemodulecode = ({
                                'sg_name': $scope.filteredTodos[i].sg_name,
                                opr: 'D'
                            });
                            deletefin.push(deletemodulecode);
                        }
                    }
                    if ($scope.flag) {
                        swal({
                            title: '',
                            text: "Are you sure you want to Delete?",
                            showCloseButton: true,
                            showCancelButton: true,
                            confirmButtonText: 'Yes',
                            width: 380,
                            cancelButtonText: 'No',

                        }).then(function (isConfirm) {
                            if (isConfirm) {
                                $http.post(ENV.apiUrl + "api/SupplierGroup/CUDSupplierGroups", deletefin).then(function (msg) {
                                    $scope.msg1 = msg.data;
                                    if ($scope.msg1 == true) {
                                        swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {

                                                $http.get(ENV.apiUrl + "api/SupplierGroup/getSupplierGroups").then(function (res) {
                                                    $scope.sg_name = res.data;
                                                    $scope.totalItems = $scope.sg_name.length;
                                                    $scope.todos = $scope.sg_name;
                                                    $scope.makeTodos();

                                                });

                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    {
                                                        $scope.row1 = '';
                                                    }
                                                }
                                                $scope.currentPage = true;
                                            }

                                        });
                                    }
                                    else if ($scope.msg1 == false) {
                                        swal({ text: "Record Not Deleted. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                            if (isConfirm) {

                                                $http.get(ENV.apiUrl + "api/SupplierGroup/getSupplierGroups").then(function (res) {
                                                    $scope.sg_name = res.data;
                                                    $scope.totalItems = $scope.sg_name.length;
                                                    $scope.todos = $scope.sg_name;
                                                    $scope.makeTodos();
                                                });

                                                main = document.getElementById('mainchk');
                                                if (main.checked == true) {
                                                    main.checked = false;
                                                    {
                                                        $scope.row1 = '';
                                                    }
                                                }
                                                //main.checked = false;
                                                // $('tr').removeClass("row_selected");
                                            }
                                        });
                                    }
                                    else {
                                        swal("Error-" + $scope.msg1)
                                    }

                                });
                            }
                            else {
                                debugger
                                for (var i = 0; i < $scope.filteredTodos.length; i++) {
                                    var v = document.getElementById(i);
                                    if (v.checked == true) {
                                        v.checked = false;
                                        main.checked = false;
                                        $('tr').removeClass("row_selected");
                                        //  $scope.row1 = '';

                                    }
                                }
                            }
                        });
                    }
                    else {
                        swal({ text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    $scope.currentPage = str;
                }
            }


            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true
            });


        }])

})();
