﻿(function () {
    'use strict';
    var obj1, temp, opr, CurrentDate, comp;
    var main, deletefin = [], data = [], data1 = [], ItemDetails = [];
    var finanacecode = [];
    var check;
    var reqno;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('OrderApproveCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {
            $scope.table = true;

            var date = new Date();
            //$scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss');
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-MM-yyyy');
            $scope.IP = {
                rd_from_date: dd + '-' + mm + '-' + yyyy,
                rd_up_date: dd + '-' + mm + '-' + yyyy
            }

            $http.get(ENV.apiUrl + "api/CreateOrder/getRequestTypeNew").then(function (reqtypedetailsNew) {
                $scope.req_type_detailsNew = reqtypedetailsNew.data;
                $scope.temp['request_mode_code'] = $scope.req_type_detailsNew[0].request_mode_code;

            });

            $scope.hide_save = false;
            $scope.hide_saves = false;

            $scope.IP = {};
            $scope.sel = {};

            $http.get(ENV.apiUrl + "api/CreateOrder/getDeparments_approve").then(function (deptdata) {
                $scope.dept_data = deptdata.data;
                // $scope.IP['dept_code'] = $scope.dept_data[0].dept_code;
                console.log($scope.dept_data);
            });

            //var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd');
            //$scope.IP = {
            //    rd_from_required: dateyear,
            //    rd_up_required: dateyear,
            //}


            $scope.SelectIetm = function () {
                debugger;

                $scope.IP.loginuser = $rootScope.globals.currentUser.username;
                $http.get(ENV.apiUrl + "api/CreateOrder/getOrderDetailsforapprove?data=" + JSON.stringify($scope.IP)).then(function (res) {
                    $scope.rows = res.data.table;
                    $scope.Allrows = res.data.table1;
                    console.log("rows", $scope.rows);
                    console.log("Allrows", $scope.Allrows);
                    if ($scope.Allrows.length <= 0) {
                        //$scope.remarks = '';
                        //$scope.hide_save = true;
                        //$scope.hide_saves = false;
                        swal({ text: "Records not found", imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                    else {
                        for (var r in $scope.rows) {
                            $scope.rows[r]['sims_icon'] = "fa fa-minus-circle";
                            $scope.rows[r]['isexpanded'] = "grid";
                            $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].ord_no);
                            $scope.remarks = $scope.rows[r].ord_approval_comment;
                            console.log("r remarks", $scope.remarks);
                        }
                        console.log("remarks", $scope.remarks);


                        $scope.hide_save = false;
                        $scope.hide_saves = true;

                    }

                    console.log(res.data.table);
                    console.log(res.data.table1);
                });
            }

            $scope.SelectItem_1 = function () {
                debugger;
                $scope.remarks = '';
                $scope.hide_save = false;
                $scope.hide_saves = false;

                $http.get(ENV.apiUrl + "api/CreateOrder/getOrderDetailsforapprove?data=" + JSON.stringify($scope.IP)).then(function (res) {
                    $scope.rows = res.data.table;
                    $scope.Allrows = res.data.table1;
                    if ($scope.Allrows.length <= 0) {
                        //swal({ title: "Alert", text: "Records not found", showCloseButton: true, width: 380, });
                        $scope.remarks = '';
                        $scope.hide_save = false;
                        $scope.hide_saves = false;
                    }
                    else {
                        for (var r in $scope.rows) {
                            $scope.rows[r]['sims_icon'] = "fa fa-minus-circle";
                            $scope.rows[r]['isexpanded'] = "grid";
                            $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].ord_no);
                        }
                        $scope.hide_save = false;
                        $scope.hide_saves = true;

                    }

                    console.log(res.data.table);
                    console.log(res.data.table1);
                });
            }

            function getSubitems(dno) {
                var arr = [];
                for (var i = 0; i < $scope.Allrows.length; i++) {
                    if ($scope.Allrows[i].ord_no == dno) {
                        arr.push($scope.Allrows[i]);
                    }
                }
                return arr;
            }

            $scope.expand = function (j, $event) {
                if (j.isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.isexpanded = "grid";
                    console.log(j);
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.isexpanded = "none";
                }
            }

            $scope.reset = function () {
                $scope.IP = {};
                $scope.rows = {};
                $scope.Allrows = {};
                $scope.IP.rd_from_date = '';
                $scope.IP.rd_up_date = '';
            }

            $scope.checkRange = function (s) {
                debugger
                var rq = parseFloat(s.final_qnt);
                //if (rq == NaN)
                //    rq = 0;
                if (isNaN(rq)) {
                    rq = 0;
                }
                s.final_qnt = rq;
            }

            $scope.ordclick = function (gh) {
                var l = gh.subItems.length;
                for (var i = 0; i < l; i++) {
                    gh.subItems[i]['isChecked'] = gh.isChecked;
                }
            }

            $scope.ordlineclick = function (r, m) {
                var l = r.subItems.length;
                var f = false;
                if (m) {
                    f = true;
                    for (var i = 0; i < l; i++) {
                        f = f && r.subItems[i]['isChecked'];
                    }
                }
                r.isChecked = f;
            }

            $http.get(ENV.apiUrl + "api/AdjustmentReason/getDept").then(function (res) {
                $scope.sel['Dept'] = res.data.table;
            });

            $scope.save = function (s) {
                $scope.hide_save = true;
                $scope.hide_saves = true;
                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {
                    debugger;
                    for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                        if ($scope.rows[i].subItems[j].isChecked) {
                            var ob = {};
                            ob.od_order_qty = $scope.rows[i].subItems[j].od_order_qty;
                            ob.final_qnt = $scope.rows[i].subItems[j].final_qnt;
                            ob.im_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.od_line_no = $scope.rows[i].subItems[j].od_line_no;
                            ob.ord_no = $scope.rows[i].subItems[j].ord_no;
                            ob.req_type = $scope.rows[i].subItems[j].req_type;
                            ob.req_no = $scope.rows[i].subItems[j].req_no;
                            ob.req_remarks = $scope.remarks;
                            ob.ord_approved_by = $rootScope.globals.currentUser.username;
                            //ob.adj_status = $scope.rows[i].subItems[j].adj_status;
                            ob.sims_workflow_no = $scope.rows[i].subItems[j].sims_workflow_no;
                            ob.val = r;
                            ar.push(ob);
                        }
                    }
                }
                if (ar.length == 0) {
                    alert("Nothing Selected");
                    return;
                }

                $http.post(ENV.apiUrl + "api/CreateOrder/UpdateOrderDetailsforapprove", ar).then(function (res) {
                    console.log("res", res);
                    console.log("res.data", res.data);

                    //swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });
                    //$scope.SelectIetm();

                    if (res.data != '0') {
                        //res.data != null && res.data != '' &&
                        swal({ text: 'Purchase Order No ' + res.data + ' Approved.', imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                $scope.SelectItem_1();

                                if($http.defaults.headers.common['schoolId'] == 'siso') {
                                    var data = {
                                        location: 'Invs.invr21Siso',
                                        parameter: {
                                            //orderno: $scope.msg1,
                                            orderno: res.data,
                                        },
                                        state: 'main.ordapp',
                                        ready: function () {
                                            this.refreshReport();
                                        },
                                    }
                                }
                                else if ($http.defaults.headers.common['schoolId'] == 'rakmps' || $http.defaults.headers.common['schoolId'] == 'rmps') {
                                    var data = {
                                        location: 'Invs.invr21RAKMPS',
                                        parameter: {
                                            //orderno: $scope.msg1,
                                            orderno: res.data,
                                        },
                                        state: 'main.ordapp',
                                        ready: function () {
                                            this.refreshReport();
                                        },
                                    }
                                }

                                else {
                                    var data = {
                                        location: 'Invs.invr21',
                                        parameter: {
                                            //orderno: $scope.msg1,
                                            orderno: res.data,
                                        },
                                        state: 'main.ordapp',
                                        ready: function () {
                                            this.refreshReport();
                                        },
                                    }
                                }
                         
                                
                                console.log(data);

                                window.localStorage["ReportDetails"] = JSON.stringify(data);
                                $state.go('main.ReportCardParameter');
                            }
                        });
                    }
                    else if (res.data == '0') {
                        swal({ text: "Purchase order approved pending for next approval.", imageUrl: "assets/img/check.png", width: 300, height: 200 });
                        setTimeout(function () {
                            $scope.SelectIetm();
                        }, 3000);
                    }
                    else {
                        swal({ text: "Purchase order not approved.", imageUrl: "assets/img/close.png", width: 300, height: 200 });
                    }

                });

            }

            $scope.reject = function (s) {
                $scope.hide_save = true;
                $scope.hide_saves = true;
                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {
                    debugger;
                    for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                        if ($scope.rows[i].subItems[j].isChecked) {
                            var ob = {};
                            ob.od_order_qty = $scope.rows[i].subItems[j].od_order_qty;
                            ob.final_qnt = $scope.rows[i].subItems[j].final_qnt;
                            ob.im_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.od_line_no = $scope.rows[i].subItems[j].od_line_no;
                            ob.ord_no = $scope.rows[i].subItems[j].ord_no;
                            ob.req_type = $scope.rows[i].subItems[j].req_type;
                            ob.req_no = $scope.rows[i].subItems[j].req_no;
                            ob.req_remarks = $scope.remarks;
                            ob.ord_approved_by = $rootScope.globals.currentUser.username;
                            //ob.adj_status = $scope.rows[i].subItems[j].adj_status;
                            ob.sims_workflow_no = $scope.rows[i].subItems[j].sims_workflow_no;
                            ob.val = r;
                            ar.push(ob);
                        }
                    }
                }
                if (ar.length == 0) {
                    alert("Nothing Selected");
                    return;
                }

                $http.post(ENV.apiUrl + "api/CreateOrder/UpdateOrderDetailsforreject", ar).then(function (res) {
                    console.log(res);

                    //swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });
                    //$scope.SelectIetm();

                    swal({ text: 'Purchase Order No ' + res.data + ' Rejected.', imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.SelectItem_1();

                        }
                    });
                });
            }

            //$scope.Reset = function () {
            //    $scope.IP = {
            //        rd_from_required: '',
            //        rd_up_required: '',
            //        req_no: '',
            //        req_range_no: '',
            //        request_mode_code: '',
            //        rd_item_desc: '',
            //    }
            //}

            //$scope.save = function (s) {
            //    $scope.hide_save = true;
            //    $scope.hide_saves = true;
            //    var ar = [];
            //    var r = Math.random() * 10;
            //    for (var i = 0; i < $scope.rows.length; i++) {
            //        for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
            //            if ($scope.rows[i].subItems[j].isChecked) {
            //                var ob = {};
            //                ob.od_order_qty = $scope.rows[i].subItems[j].od_order_qty;
            //                ob.final_qnt = $scope.rows[i].subItems[j].final_qnt;
            //                ob.im_inv_no = $scope.rows[i].subItems[j].im_inv_no;
            //                ob.od_line_no = $scope.rows[i].subItems[j].od_line_no;
            //                ob.ord_no = $scope.rows[i].subItems[j].ord_no;
            //                //ob.t_qty = $scope.rows[i].subItems[j].ad_qty;
            //                //ob.adj_status = $scope.rows[i].subItems[j].adj_status;
            //                ob.val = r;
            //                ar.push(ob);
            //            }
            //        }
            //    }
            //    if (ar.length == 0) {
            //        alert("Nothing Selected");
            //        return;
            //    }

            //    $http.post(ENV.apiUrl + "api/CreateOrder/UpdateOrderDetailsforapprove", ar).then(function (res) {
            //        console.log(res);

            //        swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });
            //        $scope.SelectIetm();
            //    });

            //}

            $scope.Reset = function () {
                $scope.IP = {
                    rd_from_required: '',
                    rd_up_required: '',
                    req_no: '',
                    req_range_no: '',
                    request_mode_code: '',
                    rd_item_desc: '',
                    loginuser: '',
                }
            }

            //Upload Documents Functionality
            $scope.UploadDocuments = function (ordno) {
                debugger;
                $scope.Upload_doc = true;
                $scope.images_newfiles = [];
                $scope.images = [];
                $scope.requestdocs = '';

                $('#UploadDocModal').modal({ backdrop: 'static', keyboard: true });
                var doccnt = $scope.images_newfiles.length;
                $scope.doccount = doccnt + ' file(s) uploaded.';
                $scope.ordno = ordno;
                var vouchertype = '2';

                $http.get(ENV.apiUrl + "api/CreateOrder/GetOrderDocsApprove?vtype=" + vouchertype + "&vouchernum=" + ordno).then(function (res) {
                    $scope.requestdocs = res.data;
                    console.log($scope.requestdocs);

                    //setTimeout(function () {
                    debugger;
                    for (var k = 0; k < $scope.requestdocs.length; k++) {
                        var or_filename = $scope.requestdocs[k].filename;
                        var new_filename = $scope.requestdocs[k].filename_en;

                        $scope.images_newfiles.push({
                            new_filename: new_filename,
                            or_filename: or_filename
                        });

                        var doccnt = $scope.images_newfiles.length;
                        $scope.doccount = doccnt + ' file(s) uploaded.';
                    }
                    //}, 1000);
                });
            }

            /*start_attached Files*/

            var formdata = new FormData();
            $scope.images = [];
            $scope.images_att = [];
            var under_file = 1;

            $scope.getTheFiles = function ($files) {
                debugger;
                $scope.filesize = true;
                var i = 0, exists = "no";

                angular.forEach($files, function (value, key) {
                    formdata.append(key, value);

                    var i = 0;
                    if ($files[i].size > 2000000) {
                        $scope.filesize = false;
                        // $scope.edt.photoStatus = false;
                        swal({ title: "Alert", text: "File Should Not Exceed 2MB.", showCloseButton: true, width: 380, });
                    }
                    else {
                        //arr_files.push($files[i].name);
                        if ($scope.images.length > 0) {
                            for (var j = 0 ; j < $scope.images.length; j++) {
                                if ($scope.images[j].name == $files[i].name) {
                                    exists = "yes";
                                    return;
                                }
                            }

                            if (exists == "no") {

                                $scope.images.push({
                                    name: $files[i].name,
                                    file: $files[i].size
                                });
                                if ($scope.images.length > 10) {
                                    $scope.images.splice(10, $scope.images.length);
                                    swal({ title: "Alert", text: "You cannot upload more than 10 files.", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                        if (isConfirm) {
                                        }
                                    });
                                    return;
                                }
                            }
                            exists = "no";
                        }
                        else {
                            $scope.images.push({
                                name: $files[i].name,
                                file: $files[i].size

                            });
                        }
                        // $scope.$apply();
                    }
                    i++;
                });
            };

            $scope.file_changed = function (element, str) {
                debugger;
                var photofile = element.files[0];
                var v = photofile.name.indexOf('.');
                $scope.name = photofile.name.substr(0, v);
                $scope.fileext = photofile.name.substr(v + 1);
                $scope.photo_filename = (photofile.type);

                var reader = new FileReader();
                reader.onload = function (e) {
                    $scope.$apply(function () {
                        $scope.prev_img = e.target.result;

                    });
                };
                reader.readAsDataURL(photofile);

                if (element.files[0].size < 2000000) {
                    var filenm = '';
                    //var cnumber = document.getElementById('txt_number').value;
                    var cnumber = '1';
                    var ind = 0;

                    filenm = cnumber + '_' + '(' + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss') + ')';
                    var filenm_images = cnumber + '_' + '(' + $filter('date')(new Date(), 'dd-MM-yyyy') + '-' + $filter('date')(new Date(), 'HH-mm-ss') + ')' + '.' + $scope.fileext;
                    $scope.images_newfiles.push({
                        new_filename: filenm_images,
                        or_filename: element.files[0].name
                    });

                    var request = {
                        method: 'POST',
                        url: ENV.apiUrl + 'api/file/uploadOrderDocument?filename=' + filenm + "&location=" + "OrderDocuments",
                        data: formdata,
                        headers: {
                            'Content-Type': undefined
                        }
                    };
                    debugger
                    var doccnt = $scope.images_newfiles.length;
                    $scope.doccount = doccnt + ' file(s) uploaded.';
                    $http(request).success(function (d) {
                        under_file = under_file + 1;
                    });

                    console.log($scope.name.type);
                    console.log($scope.name);
                }
            };

            $scope.uploadClick = function (str) {
                $scope.filesize = true;
                $scope.edt1 = str;
                $scope.ins = false;
                formdata = new FormData();
            }

            $scope.delpush = [];

            $scope.CancelFileUpload = function (idx) {
                debugger;
                $scope.delpush.push(idx);

                $scope.images.splice(idx, 1);
                $scope.images_newfiles.splice(idx, 1);

                var doccnt = $scope.images_newfiles.length;
                $scope.doccount = doccnt + ' file(s) uploaded.';
                console.log($scope.images);
            };

            /*end_attached Files*/

            //File Download
            $scope.download = function (str) {
                window.open(ENV.apiUrl + '/content/' + $http.defaults.headers.common['schoolId'] + "/Images/OrderDocuments/" + str, "_new");
            };

            $scope.saveorderdocs = function () {
                debugger;
                //Save Order Docs

                var comcode = '1';
                var vouchertype = '2';
                var filepath = 'content/images/OrderDocuments';
                var data_doc = {};
                var datasend_doc = [];
                if ($scope.images_newfiles.length > 0) {

                    for (var j = 0 ; j < $scope.images_newfiles.length; j++) {

                        var original_filename = $scope.images_newfiles[j].or_filename;
                        var new_filename = $scope.images_newfiles[j].new_filename;
                        var doclinenum = j + 1;

                        data_doc = {
                            'comcode': comcode,
                            'vouchernum': $scope.ordno,
                            'vouchertype': vouchertype,
                            'doclineno': doclinenum,
                            'filename': original_filename,
                            'filename_en': new_filename,
                            'orddate': $scope.ddMMyyyy,
                            'filepath': filepath,
                        }
                        datasend_doc.push(data_doc);
                    }
                }

                $http.post(ENV.apiUrl + "api/CreateOrder/DelOrderDocs", datasend_doc).then(function (msg) {

                    $http.post(ENV.apiUrl + "api/CreateOrder/SaveOrderDocs", datasend_doc).then(function (msg) {

                        //     swal({ title: "Alert", text: "Documents Uploaded.", width: 300, height: 200 });
                        //$('#UploadDocModal').modal('hide');

                    });
                });
            };

            $scope.cancelorderdocs = function () {
                $('#UploadDocModal').modal('hide');
            }
            //End Documents Funtionality//


            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            })

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            var today = new Date();
            var dd = today.getDate();
            var mm = today.getMonth() + 1; //January is 0!
            if (mm < 10) {
                mm = '0' + mm;
            }
            var yyyy = today.getFullYear();
            $scope.IP = {
                rd_from_date: dd + '-' + mm + '-' + yyyy,
                rd_up_date: dd + '-' + mm + '-' + yyyy
            }


        }])

})();
