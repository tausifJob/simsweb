﻿(function () {
    'use strict';
    var obj1, temp, opr, comp;
    var main, deletefin = [];
    var finanacecode = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('UomConversionsCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, ENV) {

            $scope.pagesize = "5";
            $scope.pageindex = 0;
            $scope.save_btn = true;
            var dataforSave = [];
            $scope.Update_btn = false;
            $scope.display = false;
            $scope.table = true;
            //$scope.cmbstatus = true;
            //$scope.checked = false;
            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);


            //Select Data SHOW

            $http.get(ENV.apiUrl + "api/UomConversions/getAllUomConversions").then(function (res1) {

                $scope.UomConvesionData = res1.data;
                $scope.totalItems = $scope.UomConvesionData.length;
                $scope.todos = $scope.UomConvesionData;
                $scope.makeTodos();

            });

            $scope.size = function (str) {
              
                $scope.pagesize = str;
                $scope.currentPage = 1;
                $scope.numPerPage = str; $scope.makeTodos();
            }

            $scope.index = function (str) {
                $scope.pageindex = str;
                $scope.currentPage = str;
               
                $scope.makeTodos();
                main.checked = false;
                $scope.row1 = '';
            }

            $scope.filteredTodos = [], $scope.currentPage = 1, $scope.numPerPage = 5, $scope.maxSize = 5;

            $scope.makeTodos = function () {
                var rem = parseInt($scope.totalItems % $scope.numPerPage);
                if (rem == '0') {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage);
                }
                else {
                    $scope.pagersize = parseInt($scope.totalItems / $scope.numPerPage) + 1;
                }

                var begin = (($scope.currentPage - 1) * $scope.numPerPage);
                var end = parseInt(begin) + parseInt($scope.numPerPage);

              
                $scope.filteredTodos = $scope.todos.slice(begin, end);
            };

            $scope.searched = function (valLists, toSearch) {
                return _.filter(valLists,

                function (i) {
                    /* Search Text in all  fields */
                    return searchUtil(i, toSearch);
                });
            };

            //Bind Combo Box

            $http.get(ENV.apiUrl + "api/UnitOfMeasurement/getAllUnitOfMeasurement").then(function (getUomCode) {
                $scope.UOMCode_Data = getUomCode.data;
               
            });

            //Search
            $scope.search = function () {
                $scope.todos = $scope.searched($scope.UomConvesionData, $scope.searchText);
                $scope.totalItems = $scope.todos.length;
                $scope.currentPage = '1';
                if ($scope.searchText == '') {
                    $scope.todos = $scope.UomConvesionData;
                }
                $scope.makeTodos();
                main.checked = false;
                $scope.CheckAllChecked();
            }

            function searchUtil(item, toSearch) {
                /* Search Text in all 3 fields */
                return (item.uom_name.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||
                        item.uom_code_belongs.toLowerCase().indexOf(toSearch.toLowerCase()) > -1 ||                      
                        item.uc_factor == toSearch) ? true : false;
            }

            //NEW BUTTON
            $scope.New = function () {
               
                $scope.disabled = false;
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = true;
                $scope.Update_btn = false;
                $scope.uomcodeReadonly = false;
                $scope.uomBelongscodeReadonly = false;
                $scope.temp = [];
                $scope.temp.uc_factor = ""; 
                $scope.edt = "";
                $scope.temp.uom_name = "";
                $scope.UOM_Code = "";
                $scope.UOM_Code_Belongs = ""; 
                $scope.Uom_factor = "";
              
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();

            }

            //DATA CANCEL
            $scope.Cancel = function () {
               
                $scope.table = true;
                $scope.display = false;
                $scope.temp = [];
                $scope.temp.uc_factor = "";
                $scope.edt = "";
                $scope.temp.uc_factor = "";
                $scope.temp.uom_name = "";              
                $scope.UOM_Code = "";
                $scope.UOM_Code_Belongs = "";
                $scope.Uom_factor = "";
                $scope.Myform.$setPristine();
                $scope.Myform.$setUntouched();
            }

            //DATA EDIT
            $scope.edit = function (str) {
                
                $scope.table = false;
                $scope.display = true;
                $scope.save_btn = false;
                $scope.Update_btn = true;
                $scope.uomcodeReadonly = true;
                $scope.uomBelongscodeReadonly = true;
                $scope.temp = {
                  uom_name: str.uom_name,
                 uc_factor: str.uc_factor
                };
                $scope.edt = {
                    uom_name: str.uom_code_belongs
                };
        }

            //DATA SAVE INSERT
            var datasend = [];
    $scope.savedata = function (Myform) {
               
        if (Myform) {
            var data = {

                uom_name: $scope.temp.uom_name,
                uom_code_belongs: $scope.edt.uom_name,
                uc_factor: $scope.temp.uc_factor
            }
            data.opr = 'I';
            datasend.push(data);
            $http.post(ENV.apiUrl + "api/UomConversions/CUDUomConversions", datasend).then(function (msg) {
                $scope.msg1 = msg.data;
                if ($scope.msg1 == true) {
                    swal({ text: "Record Inserted Successfully", imageUrl: "assets/img/check.png", width: 380 });
                }
                else {
                    swal({ text: "Record code allready present. " + $scope.msg1, imageUrl: "assets/img/close.png", width: 380 });
                }
                $scope.getgrid();
            });
            datasend = [];
            $scope.table = true;
            $scope.display = false;
        }

    }

    //DATA UPADATE
    var dataforUpdate = [];
    $scope.update = function (Myform) {

        if (Myform) {
            var data = {

                 uom_name: $scope.temp.uom_name,
                uom_code_belongs: $scope.edt.uom_name,
                uc_factor: $scope.temp.uc_factor
            }
            data.opr = "U";
            dataforUpdate.push(data);
            //dataupdate.push(data);
            $http.post(ENV.apiUrl + "api/UomConversions/CUDUomConversions", dataforUpdate).then(function (msg) {
                $scope.msg1 = msg.data;
                if ($scope.msg1 == true) {
                    swal({ text: "Record Updated Successfully", imageUrl: "assets/img/check.png", width: 380 });
                }
                else if ($scope.msg1 == false) {
                    swal({ text: "Record Not Updated. ", imageUrl: "assets/img/close.png", width: 380 });
                }
                else {
                    swal("Error-" + $scope.msg1)
                }
                $scope.getgrid();
            });
            dataforUpdate = [];
            $scope.table = true;
            $scope.display = false;
        }
    }

    // Data DELETE RECORD
    $scope.CheckAllChecked = function () {
        main = document.getElementById('mainchk');
        if (main.checked == true) {
            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                var v = document.getElementById(i);
                v.checked = true;
                $scope.row1 = 'row_selected';
                $('tr').addClass("row_selected");
            }
        }
        else {
            for (var i = 0; i < $scope.filteredTodos.length; i++) {
                var v = document.getElementById(i);
                v.checked = false;
                main.checked = false;
                $scope.row1 = '';
            }
        }


    }

    $scope.checkonebyonedelete = function () {
        $("input[type='checkbox']").change(function (e) {
            if ($(this).is(":checked")) {
                $(this).closest('tr').addClass("row_selected");
                $scope.color = '#edefef';
            }
            else {
                $(this).closest('tr').removeClass("row_selected");
                $scope.color = '#edefef';
            }
        });

        main = document.getElementById('mainchk');
        if (main.checked == true) {
            main.checked = false;
            $("input[type='checkbox']").change(function (e) {
                if ($(this).is(":checked")) {
                    $(this).closest('tr').addClass("row_selected");
                }
                else {
                    $(this).closest('tr').removeClass("row_selected");
                }
            });
        }

    }

    $scope.getgrid = function () {
        $http.get(ENV.apiUrl + "api/UomConversions/getAllUomConversions").then(function (res1) {

            $scope.UomConvesionData = res1.data;
            $scope.totalItems = $scope.UomConvesionData.length;
            $scope.todos = $scope.UomConvesionData;
            $scope.makeTodos();

        });
    }

    $scope.Delete = function () {
        
        var deletecode = [];
        $scope.flag = false;

        for (var i = 0; i < $scope.filteredTodos.length; i++) {
            //var t = $scope.filteredTodos[i].uom_code;
            var v = document.getElementById(i);
            if (v.checked == true) {
                $scope.flag = true;
                var deletemodercode = ({
                    'uom_name': $scope.filteredTodos[i].uom_name,
                    'uom_code_belongs':$scope.filteredTodos[i].uom_code_belongs,
                    'opr': 'D'
                });
                deletecode.push(deletemodercode);
            }
        }

        if ($scope.flag) {
            
            swal({
                title: '',
                text: "Are you sure you want to Delete?",
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonText: 'Yes',
                width: 380,
                cancelButtonText: 'No',

            }).then(function (isConfirm) {
                if (isConfirm) {

                    $http.post(ENV.apiUrl + "api/UomConversions/CUDUomConversions", deletecode).then(function (res) {
                        $scope.msg1 = res.data;
                        if ($scope.msg1 == true) {
                            swal({ text: "Record Deleted Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                    main = document.getElementById('mainchk');
                                    if (main.checked == true) {
                                        main.checked = false;
                                        $scope.row1 = '';
                                    }
                                    $scope.currentPage = true;
                                }
                            });
                        }
                        else {
                            swal({ text: "Record Already Mapped.Can't be Deleted. " + $scope.msg1, imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                                if (isConfirm) {
                                    $scope.getgrid();
                                    if (main.checked == true) {
                                        main.checked = false;
                                        $scope.row1 = '';
                                    }
                                    $scope.currentPage = true;
                                }
                            });
                        }

                    });
                }
                else {
                    main = document.getElementById('mainchk');
                    if (main.checked == true) {
                        main.checked = false;
                    }
                    for (var i = 0; i < $scope.filteredTodos.length; i++) {
                        var v = document.getElementById(i);
                        //var t = $scope.filteredTodos[i].sims_fee_category;
                       
                        if (v.checked == true) {
                            v.checked = false;
                            main.checked = false;
                            $('tr').removeClass("row_selected");
                        }


                    }

                }

            });
        }
        else {
            swal({
                text: "Please Select Atleast One Record", imageUrl: "assets/img/notification-alert.png",
                showCloseButton: true, width: 380,
            });
        }
       
        $scope.currentPage = str;
    }



    $('*[data-datepicker="true"] input[type="text"]').datepicker({
        todayBtn: true,
        orientation: "top left",
        autoclose: true,
        todayHighlight: true,
        formate: "yyyy-mm-dd"
    });



}])

})();
