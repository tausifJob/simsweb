﻿(function () {
    'use strict';
    var opr = '';
    var costingcode = [];
    var main;
    var data1 = [];
    var data = [];
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });


    simsController.controller('CostingSheetApprovalCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$http', '$filter', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $http, $filter, ENV) {
            $scope.ip = {};
            $scope.CSEData = [];
            $scope.set = {};
            $scope.gTotal = 0;
            $scope.csbody = true;
            $scope.assetbody = false;
            $scope.auto_data = '';
            $scope.ifedit = false;
            $scope.group = true;
            $scope.edt = {};

            var date = new Date();
            //$scope.ddMMyyyy = $filter('date')(new Date(), 'yyyy-MM-dd');
            $scope.ddMMyyyy = $filter('date')(new Date(), 'dd-mm-yyyy');

            $http.get(ENV.apiUrl + "api/CreateCostingSheet/getDepartments").then(function (getDepartments_Data) {
                $scope.Departments_Data = getDepartments_Data.data;
            });

            $http.get(ENV.apiUrl + "api/CreateCostingSheet/getSupplier").then(function (getSupplier_Data) {
                $scope.Supplier_Data = getSupplier_Data.data;
            });

            $scope.up = function (str) {
                $("#SearchCostingSheetModal").modal('hide');
                $scope.edt = {
                    req_type: 'I',
                    shipmentNO: str.shipmentNO
                }
                $scope.getAllShipmentDetails($scope.edt);
            }

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            });

            $timeout(function () {
                $("#fixTable").tableHeadFixer({ 'top': 1 });
            }, 100);

            /*Expenses*/
            $scope.searchExpenses = function () {
                $("#CreateEditEXPMain").modal('show');
            }

            $scope.getCostingExpensesDetails = function (row) {


                var ob = {};
                ob.cs_prov_no = $scope.ip.cs_prov_no;
                $scope.tob = ob;
                ob.val = Math.random() * 100;
                $http.post(ENV.apiUrl + "api/AdjustmentReason/SheetExpGET", ob).then(function (res) {

                    $scope.CSEData = res.data.table;
                    $scope.Total_Amount = 0;
                    $scope.expenseSUM();
                    $scope.OrderSUM();
                    //for (var i = 0; i < $scope.CSEData.length; i++) {
                    //    //$scope.Total_Amount = $scope.Total_Amount + (parseFloat($scope.CSEData[i].im_sell_price) * parseInt($scope.OrderData[i].so_shipped_qty))
                    //}
                });
                $("#SearchCostingSheetModal").modal('hide');
            }

            $scope.clear = function () {
                $scope.tob = {};
                $scope.CSEData = [];
                $scope.expenseSUM();
                $scope.OrderSUM();
            }

            $scope.NewExpType = function () {
                //$scope.exp['lbl'] = 'New';
                $scope.edt = {};
                $scope.edt.cse_doc_no = '';
                $scope.edt.cse_doc_date = '';
                $scope.edt.cse_amount = '';
                $scope.edt.pet_desc = '';
                $scope.edt.pet_code = '';
                $scope.edt.isNew = true;
                $scope.edt.cs_prov_no = $scope.ip.cs_prov_no;
                $("#CreateEditEXPMain").modal('hide');
                setTimeout(function () { $("#CreateEditEXP").modal('show'); }, 500)

            }

            $scope.createEditEXPclose = function () {

                $("#CreateEditEXP").modal('hide');
                setTimeout(function () { $("#CreateEditEXPMain").modal('show'); }, 500)
            }

            $scope.EditExpType = function (ob) {
                //$scope.exp['lbl'] = 'Edit';
                $scope.edt = {};
                $scope.edt.cse_doc_no = ob.cse_doc_no;
                $scope.edt.cse_doc_date = ob.cse_doc_date;
                $scope.edt.cse_amount = ob.cse_amount;
                $scope.edt.pet_desc = ob.pet_desc;
                $scope.edt.pet_code = ob.pet_code;
                $scope.edt.cs_prov_no = ob.cs_prov_no;
                $scope.edt.isNew = false;
                $("#CreateEditEXPMain").modal('hide');
                setTimeout(function () { $("#CreateEditEXP").modal('show'); }, 500)
            }

            /*Expenses End*/

            /*Costing Sheet and details*/
            $scope.searchCostingSheets = function (id) {
                if (id == undefined || id == '') {
                    $("#CostingSheetModal").modal('show');
                }
                else {
                    $scope.searchCostingSheets1(id);
                }
            }

            $scope.resetCS = function () {
                $scope.cs = {};
                $scope.cs.cs_to_date = '';
                $scope.cs.cs_from_date = '';
                $scope.cs.cs_no = '';

                $scope.CSDataSearch = [];
            }

            $scope.searchCostingSheets1 = function (id) {
                $http.post(ENV.apiUrl + "api/AdjustmentReason/AllCostingSheetsGet", id).then(function (res) {

                    $scope.CSDataSearch = res.data.table;
                });
            }

            //CS Details
            $scope.GetCostingSheetsDetails = function (id) {
                var ob = {};
                $("#CostingSheetModal").modal('hide');
                ob.orderLineNO = id.cs_prov_no;
                $http.post(ENV.apiUrl + "api/AdjustmentReason/CostingSheetsGetDetails", ob).then(function (res) {

                    /*var data = res['table'];
                    if(data.)*/
                    $scope.ip = res.data.table[0];
                    $scope.OrderData = res.data.table1;
                    $scope.addlDetails = res.data.table2;
                    $scope.CSEData = res.data.table3;
                    $scope.expenseSUM();
                    $scope.OrderSUM();
                    $scope.forupdflag = true;
                    $scope.set.showFinal = true;
                    $scope.set.showExp = true;

                });
            }

            $scope.resetForm = function () {
                $scope.ip = {};
                $scope.OrderData = {};
                $scope.addlDetails = {};
                $scope.CSEData = [];
                $scope.expenseSUM();
                $scope.OrderSUM();
                $scope.ServiceData = [];
                $scope.forupdflag = false;
                $scope.showCsProvDateEdit = false;
                $scope.set.showExp = false;
                $scope.set.showFinal = false;
            }

            //finalizepostingdetails
            $scope.finalizeCostingSheet = function () {

                var id = {};
                id.cs_prov_no = $scope.ip.cs_prov_no;

                $http.post(ENV.apiUrl + "api/AdjustmentReason/CSGetPDetails?cs_prov_no=" + $scope.ip.cs_prov_no).then(function (res) {

                    $scope.ret = res.data;

                    if ($scope.ret == 'yes') {


                        $http.post(ENV.apiUrl + "api/AdjustmentReason/FinalizeCostingSheet", id).then(function (res) {
                            swal({ text: 'Saved successfully', imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, });
                            var data = {
                                location: 'Invs.INVR28',
                                parameter: {
                                    cs_prov_no: $scope.ip.cs_prov_no,
                                },
                                state: 'main.CostingSheetApproval'
                            }
                            window.localStorage["ReportDetails"] = JSON.stringify(data)
                            $state.go('main.ReportCardParameter')
                            $scope.resetForm();
                        });
                    }
                    else {
                        swal({ text: 'Please provide Account Details for items and suppliers to prepare costing sheet.', imageUrl: "assets/img/notification-alert.png", showCloseButton: true, width: 380, });
                    }
                });
            }

            $scope.expenseSUM = function () {
                var s = 0;
                for (var i = 0; i < $scope.CSEData.length; i++) {
                    s = s + parseFloat($scope.CSEData[i].cse_amount);
                }
                $scope.expenseSUMV = s.toFixed(2);
                $scope.gTotal = parseFloat(s) + parseFloat($scope.Total_Amount == undefined ? 0 : $scope.Total_Amount);
                return s.toFixed(2);
            }

            $scope.OrderSUM = function () {
                var s = 0;                
                for(var i = 0; i < $scope.OrderData.length; i++) {
                    //s = s + (parseFloat($scope.OrderData[i].rate) * parseInt($scope.OrderData[i].od_received_qty));
                    //s = s + ((parseFloat($scope.OrderData[i].rate) * parseInt($scope.OrderData[i].od_received_qty)) + parseFloat($scope.OrderData[i].od_item_vat_amount)) - parseFloat($scope.OrderData[i].od_discount_amt);
                    s = s + (parseFloat($scope.OrderData[i].rate) + parseFloat($scope.OrderData[i].od_item_vat_amount)) - parseFloat($scope.OrderData[i].od_discount_amt);
                }
                $scope.Total_Amount = s.toFixed(2);                
                console.log("Total_Amount", $scope.Total_Amount);                
                $scope.gTotal = parseFloat(s) + parseFloat($scope.expenseSUMV == undefined ? 0 : $scope.expenseSUMV);
                console.log("gTotal",$scope.gTotal);
               //return s.toFixed(2);
            }

            //postingdetails
            $scope.postingdetails = function () {

                $('#MyModal2').modal({ backdrop: 'static', keyboard: true });

                var id = {};
                id.cs_prov_no = $scope.ip.cs_prov_no;
                $http.post(ENV.apiUrl + "api/AdjustmentReason/CostingSheetsGetPostingDetails?cs_prov_no=" + $scope.ip.cs_prov_no).then(function (res) {
                    $scope.GetItems = res.data;
                });
            }

            //Assetization details
            $scope.assetizationdetails = function () {

                $('#AssetModal').modal({ backdrop: 'static', keyboard: true });
                $scope.csbody = true;
                $scope.assetbody = false;

                var id = {};
                id.cs_prov_no = $scope.ip.cs_prov_no;
                $http.post(ENV.apiUrl + "api/AdjustmentReason/CostingSheetsGetAssetizationDetails?cs_prov_no=" + $scope.ip.cs_prov_no).then(function (res) {
                    $scope.GetAssetItems = res.data;
                });
            }

            $scope.onlyNumbers = function (event, str) {

                var keys = {

                    'escape': 27, 'backspace': 8, 'tab': 9, 'enter': 13, 'del': 46,
                    '0': 48, '1': 49, '2': 50, '3': 51, '4': 52, '5': 53, '6': 54, '7': 55, '8': 56, '9': 57
                };
                for (var index in keys) {
                    if (!keys.hasOwnProperty(index)) continue;
                    if (event.charCode == keys[index] || event.keyCode == keys[index]) {
                        return; //default event
                    }
                }
                event.preventDefault();
            };

            //Assetization details
            $scope.assetmaster = function (str) {

                $scope.ifedit = true;
                $scope.csbody = false;
                $scope.assetbody = true;

                $scope.edt.gam_desc_1 = str.itemdesc1;
                $scope.edt.gam_supl_code = str.sup_code;
                $scope.edt.gam_order_no = str.ord_no;
                $scope.edt.gam_invoice_no = str.grn_inv_no;
                $scope.edt.gam_quantity = str.od_received_qty;
                $scope.originalr_qty = str.od_received_qty;
                $scope.edt.gam_sale_date = str.grndate;
                $scope.edt.gam_invoice_amount = (str.od_received_qty * str.csd_landed_cost);
                $scope.edt.gam_book_value = $scope.edt.gam_invoice_amount;
                $scope.edt.gam_amend_date = $scope.ddMMyyyy;

            }

            $scope.groupradio = function (str) {
                $scope.edt.gam_quantity = '1';
            }

            $scope.individualradio = function (str) {

                $scope.edt.gam_quantity = originalr_qty;

            }

            function convertdate(dt) {
                if (dt != null) {
                    var d1 = new Date(dt);
                    var month = d1.getMonth() + 1;
                    var day = d1.getDate();
                    if (month < 10)
                        month = "0" + month;
                    if (day < 10)
                        day = "0" + day;
                    var d = d1.getFullYear() + "-" + (month) + "-" + (day);

                    $scope.convertdated = d;
                    return d;
                }
            }


            $scope.Save = function () {

                var data1 = [];
                // if (isvalidate) {
                // if ($scope.update1 == false) {
                var data = ({
                    gam_comp_code: $scope.dept_comp_code,
                    financialyear: $scope.finyr,
                    gam_dept_code: $scope.edt.gam_dept_code,
                    gam_asst_type: $scope.edt.gam_asst_type,
                    gam_item_no: $scope.edt.gam_item_no,
                    gam_location: $scope.edt.gam_location,
                    gam_desc_1: $scope.edt.gam_desc_1,
                    gam_desc_2: $scope.edt.gam_desc_2,
                    gam_supl_code: $scope.edt.gam_supl_code,
                    gam_acct_code: $scope.edt.gam_acct_code,
                    gam_order_no: $scope.edt.gam_order_no,
                    gam_invoice_no: $scope.edt.gam_invoice_no,
                    gam_new_sec: $scope.edt.gam_new_sec,
                    gam_page_no: $scope.edt.gam_page_no,
                    gam_line_no: $scope.edt.gam_line_no,
                    gam_quantity: $scope.edt.gam_quantity,
                    gam_invoice_amount: $scope.edt.gam_invoice_amount,
                    gam_val_add_cum: $scope.edt.gam_val_add_cum,
                    gam_val_add_mtd: $scope.edt.gam_val_add_mtd,
                    gam_val_add_ytd: $scope.edt.gam_val_add_ytd,
                    gam_book_value: $scope.edt.gam_book_value,
                    gam_cum_deprn: $scope.edt.gam_cum_deprn,
                    gam_ytd_deprn: $scope.edt.gam_ytd_deprn,
                    gam_mth_deprn: $scope.edt.gam_mth_deprn,
                    gam_sale_value: $scope.edt.gam_sale_value,
                    gam_repl_cost: $scope.edt.gam_repl_cost,
                    gam_receipt_date: convertdate($scope.edt.gam_receipt_date),
                    gam_sale_date: convertdate($scope.edt.gam_sale_date),
                    gam_entry_date: convertdate($scope.edt.gam_entry_date),
                    gam_amend_date: convertdate($scope.edt.gam_amend_date),
                    gam_deprn_percent: $scope.edt.gam_deprn_percent,
                    gam_used_on_item: $scope.edt.gam_used_on_item,
                    gam_status_code: $scope.edt.gam_status_code,
                    empcode: $scope.edt.empcode,
                    opr: 'I',
                });

                data1.push(data);
                $http.post(ENV.apiUrl + "api/common/AssetMaster_dpsd/CUDInsertFins_AssetMaster", data1).then(function (res) {
                    $scope.display = true;
                    $scope.msg1 = res.data;
                    if ($scope.msg1 == true) {
                        swal({ text: "Record Added Successfully", imageUrl: "assets/img/check.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                //$scope.getgrid();
                                $scope.grid1 = false;
                            }
                        });
                    }
                    else if ($scope.msg1 == false) {
                        swal({ text: "Record Not Inserted.Please Check if Mandatory fields are entered. ", imageUrl: "assets/img/close.png", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                            if (isConfirm) {
                                //$scope.getgrid();
                            }
                        });
                    }
                    else {
                        swal("Error-" + $scope.msg1)
                    }
                });
                //}
                //else {
                //    $scope.Update(isvalidate);
                //}
                // }
            }

            ///////////////////////////////////////////////////////////////////////////////
            //Asset Master Functions
            ///////////////////////////////////////////////////////////////////////////////

            $http.get(ENV.apiUrl + "api/common/FinancialPeriod/getGetCompany").then(function (res) {

                $scope.comp_data = res.data;

                $scope.dept_comp_code = $scope.comp_data[0].dept_comp_code;

                $http.get(ENV.apiUrl + "api/common/SubLedgerMaster/GetCurrentFinancialYear").then(function (res) {

                    $scope.finyr_data = res.data;
                    $scope.finyr = $scope.finyr_data;

                    $http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetAllDepartmentName?comp_code=" + $scope.dept_comp_code + "&fins_year=" + $scope.finyr).then(function (res) {
                        $scope.dept_data = res.data;
                    });
                });
            });


            $http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetAutoGenerate_AssetItemNumber").then(function (res) {

                // $scope.auto_data = res.data;
                $scope.edt['gam_item_no'] = res.data;
            });

            //Custodian Change
            $scope.Custodian1 = [];
            setTimeout(function () {
                $("#cmb_sup").select2();
            }, 100);
            //Custodian Change
            $http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetCustodianEmployees").then(function (res122) {

                $scope.Custodian1 = res122.data;
            });

            $http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetAssetType").then(function (res) {
                $scope.assetType_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetAllLocationName").then(function (res) {
                $scope.location_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetAllSupplierNames").then(function (res) {
                $scope.sup_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetAllOrderNo").then(function (res) {
                $scope.order_data = res.data;
            });

            $http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetAssetStatus").then(function (res) {
                $scope.status_data = res.data;
            });

            $scope.getAccountNo = function (asset_type) {
                if (asset_type != null) {
                    //$http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetAccountNo?asset_type=" + asset_type).then(function (res) {
                    //    $scope.AccountNo_data = res.data;
                    //    console.log($scope.AccountNo_data);
                    //    for (var i = 0; i < $scope.AccountNo_data.length; i++) {
                    //        $scope.edt.gam_acct_code = $scope.AccountNo_data[0].gam_acct_code;
                    //        edt.gam_deprn_percent
                    //    }
                    //});

                    for (var i = 0; i < $scope.assetType_data.length; i++) {
                        if ($scope.assetType_data[i].gam_asst_type == asset_type) {

                            $scope.edt.gam_acct_code = $scope.assetType_data[i].gam_acct_code;
                            $scope.edt.gam_deprn_percent = $scope.assetType_data[i].gam_deprn_percent;


                        }
                    }

                }
                else {
                    $scope.edt.gam_acct_code = '';
                    $scope.edt.gam_deprn_percent = '';
                }
            }

            $scope.cancel = function () {
                $('#AssetModal').modal('hide');
            }

            $scope.search_type = function (type) {

                //if (type == null || type == undefined) {
                //    swal({ title: "Alert", text: "Please Select Asset Type", showCloseButton: true, width: 380, });                    
                //}
                //else {
                $http.get(ENV.apiUrl + "api/common/AssetMaster_dpsd/GetFins_AssetMaster?type=" + $scope.edt.gam_asst_type + "&gam_items=" + $scope.edt.gam_item_no).then(function (res) {
                    $scope.display = false;
                    $scope.grid = true;
                    $scope.grid1 = true;
                    $scope.assetmaster_data = res.data;
                    //console.log($scope.assetmaster_data);
                    $scope.totalItems = $scope.assetmaster_data.length;
                    $scope.todos = $scope.assetmaster_data;
                    $scope.makeTodos();
                    if (res.data.length == 0) {
                        swal({ title: "Alert", text: "Sorry!! No Data Found", showCloseButton: true, width: 380, });
                        $scope.grid1 = false;
                    }
                });
                // }
            }
        }])
})();