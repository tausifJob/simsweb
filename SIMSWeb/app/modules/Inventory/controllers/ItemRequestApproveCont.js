﻿(function () {
    'use strict';
    var obj1, temp, opr, CurrentDate, comp;
    var main, deletefin = [], data = [], data1 = [], ItemDetails = [];
    var finanacecode = [];
    var check;
    var reqno;
    var simsController = angular.module('sims.module.Inventory');

    simsController.run(function ($rootScope, $templateCache) {
        $rootScope.$on('$viewContentLoaded', function () {
            $templateCache.removeAll();
        });
    });

    simsController.controller('ItemRequestApproveCont',
        ['$scope', '$state', '$rootScope', '$timeout', 'gettextCatalog', '$filter', '$http', 'ENV', function ($scope, $state, $rootScope, $timeout, gettextCatalog, $filter, $http, ENV) {
            $scope.table = true;

            //$http.get(ENV.apiUrl + "api/CreateOrder/getRequestTypeNew").then(function (reqtypedetailsNew) {
            //    $scope.req_type_detailsNew = reqtypedetailsNew.data;
            //    $scope.temp['request_mode_code'] = $scope.req_type_detailsNew[0].request_mode_code;

            //});

            $scope.hide_save = false;
            $scope.hide_saves = false;
            $scope.isra = {};
            $scope.IP = {};
            $scope.sel = {};
            $scope.req_stat = '01';

            $scope.request_current = [];
          
            //$http.get(ENV.apiUrl + "api/CreateOrder/getDeparments_approve").then(function (deptdata) {
            //    $scope.dept_data = deptdata.data;
            //    $scope.IP['dept_code'] = $scope.dept_data[0].dept_code;
            //    console.log($scope.dept_data);
            //});

            //var dateyear = $filter('date')(new Date(), 'yyyy-MM-dd');
            //$scope.IP = {
            //    rd_from_required: dateyear,
            //    rd_up_required: dateyear,
            //}
            $scope.Getreqstat = function (str) {
                console.log(str);

                //if (str == '01') {
                //    $scope.hide_saves = true;
                //    $scope.hide_saves1 = false;
                //}
                //if (str == '02') {
                //    $scope.hide_saves1 = true;
                //    $scope.hide_saves = false;
                //}
            }

            $http.get(ENV.apiUrl + "api/CreateOrder/getRequestTypeNew").then(function (reqtypedetailsNew) {
                $scope.req_type_detailsNew = reqtypedetailsNew.data;
                $scope.temp['request_mode_code'] = $scope.req_type_detailsNew[0].request_mode_code;
            });

            $scope.SelectIetm = function () {
                debugger;
             
                $scope.IP.loginuser = $rootScope.globals.currentUser.username;
                $scope.IP.request_issuestatus = $scope.req_stat;
                $http.get(ENV.apiUrl + "api/ItemRequests/getRequestDetailsforapprove?data=" + JSON.stringify($scope.IP)).then(function (res) {
                    debugger;
                    $scope.rows = res.data.table;
                    $scope.Allrows = res.data.table1;
                    console.log("rows",$scope.rows)
                    console.log("Allrows", $scope.Allrows);                    
                    if ($scope.Allrows.length <= 0) {
                        // swal({ title: "Alert", text: "Records not found", showCloseButton: true, width: 380, });
                    }
                    else {
                        for (var r in $scope.rows) {
                            $scope.rows[r]['sims_icon'] = "fa fa-minus-circle";
                            $scope.rows[r]['isexpanded'] = "grid";
                            console.log($scope.rows[r].doc_prov_no);
                            $scope.rows[r]['subItems'] = getSubitems($scope.rows[r].doc_prov_no);
                            console.log("subItems",$scope.rows[r]['subItems']);
                        }
                        $scope.hide_save = false;
                        $scope.hide_saves = true;
                        
                    }

                   
                    if ($scope.req_stat == '01') {
                        $scope.hide_saves = true;
                        $scope.hide_saves1 = false;
                    }
                    if ($scope.req_stat == '02') {
                        $scope.hide_saves1 = true;
                        $scope.hide_saves = false;
                    }
                    if ($scope.req_stat == '03') {
                        $scope.hide_saves = true;
                        $scope.hide_saves1 = false;
                    }
                    if ($scope.req_stat == '04') {
                        $scope.hide_saves = true;
                        $scope.hide_saves1 = false;
                    }
                });
            }

            $scope.disableSelected = function () {
                debugger;
                for (var i = 0; i < $scope.isra.request_current.length; i++) {
                    $scope.isra.request_current[i].checkbox = false;
                }
            }

            $scope.openLastDataDetails = function (str) {
                debugger;
               
                $('#itemSearch').modal('show');
                $scope.isra.request_current = [];

                $scope.isra = [];
                $scope.isra.current_request = [];
                $scope.isra.request_current = [];
                var temp_request_current = [];
                $scope.isra.mat_code = str.doc_prov_no
                $scope.isra.emp_name = str.creation_user;
                if (str.subItems.length==1) {
                    temp_request_current = {
                        rd_item_desc: str.subItems[0].rd_item_desc
                    };
                    $scope.isra.request_current.push(temp_request_current);
                    //temp_current_request = {
                    //    rd_item_details: str.creation_user,
                    //    im_item_code: str.subItems[0].im_item_code,
                    //    rd_item_desc: str.subItems[0].rd_item_desc,
                    //    dd_qty: str.subItems[0].dd_qty,
                    //    final_qnt: str.subItems[0].final_qnt
                    //};
                    //$scope.isra.current_request.push(temp_current_request);
                }
                else {
                    for (var i = 0; i < str.subItems.length; i++) {
                        temp_request_current = {
                            rd_item_desc: str.subItems[i].rd_item_desc
                        };
                        $scope.isra.request_current.push(temp_request_current);
                        //temp_current_request = {
                        //    rd_item_details: str.creation_user,
                        //    im_item_code: str.subItems[i].im_item_code,
                        //    rd_item_desc: str.subItems[i].rd_item_desc,
                        //    dd_qty: str.subItems[i].dd_qty,
                        //    final_qnt: str.subItems[i].final_qnt
                        //};
                        //$scope.isra.current_request.push(temp_current_request);
                    }
                }
                $scope.isra.dateselect = '02';

            }
            $scope.checkonebyone = function (dataSelected) {
                debugger;
                var desc = [], temp_current_request = [], month_selected = [];
                $scope.isra.current_request = [];
                if (dataSelected.dateselect == "01") {
                    month_selected="1"
                }
                else if (dataSelected.dateselect == "02") {
                    month_selected = "3"
                }
                else if (dataSelected.dateselect == "03") {
                    month_selected = "6"
                }
                else if (dataSelected.dateselect == "04") {
                    month_selected = "12"
                }
                   
                for (var i = 0; i < dataSelected.request_current.length; i++) {
                    if (i==0) {
                        if (dataSelected.request_current[i].checkbox) {
                            desc = dataSelected.request_current[0].rd_item_desc;
                        }
                    }
                    else {
                        if (dataSelected.request_current[i].checkbox) {
                            desc = desc + "," + dataSelected.request_current[i].rd_item_desc;
                        }
                    }
                }
                if (desc.length!= 0) {
                    $http.get(ENV.apiUrl + "api/ItemRequests/getRequestDetailsforapprovePeriodWise?description=" + desc + "&month=" + month_selected).then(function (res) {
                        if (res.data.table.length >= 1) {
                            for (var i = 0; i < res.data.table.length; i++) {
                                temp_current_request = {
                                    rd_item_date: res.data.table[i].doc_prov_date,
                                    im_item_code: res.data.table[i].im_item_code,
                                    rd_item_desc: res.data.table[i].im_desc,
                                    dd_qty: res.data.table[i].dd_qty,
                                    final_qnt: res.data.table[i].final_qnt
                                };
                                $scope.isra.current_request.push(temp_current_request);
                            }
                        }
                    });
                }

            }
            function getSubitems(dno) {
                debugger;
                var arr = [];

                for (var i = 0; i < $scope.Allrows.length; i++) {
                    console.log($scope.Allrows[i].doc_prov_no);
                    console.log(dno);
                    if ($scope.Allrows[i].doc_prov_no == dno) {
                        arr.push($scope.Allrows[i]);
                    }
                }
                return arr;
            }

            $scope.expand = function (j, $event) {
                if (j.isexpanded == "none") {
                    j.sims_icon = "fa fa-minus-circle";
                    j.isexpanded = "grid";
                    console.log(j);
                }
                else {
                    j.sims_icon = "fa fa-plus-circle";
                    j.isexpanded = "none";
                }
            }

            //$scope.reset = function () {
            //    $scope.IP = {};
            //    $scope.rows = {};
            //    $scope.Allrows = {};
            //}

            $scope.checkRange = function (s) {
                var rq = parseInt(s.final_qnt);
                if (rq == NaN)
                    rq = 0;
                s.final_qnt = rq;
            }

            $scope.ordclick = function (gh) {
                var l = gh.subItems.length;
                for (var i = 0; i < l; i++) {
                    gh.subItems[i]['isChecked'] = gh.isChecked;
                }
            }

            $scope.ordlineclick = function (r, m) {
                var l = r.subItems.length;
                var f = false;
                if (m) {
                    f = true;
                    for (var i = 0; i < l; i++) {
                        f = f && r.subItems[i]['isChecked'];
                    }
                }
                r.isChecked = f;
            }

            $http.get(ENV.apiUrl + "api/AdjustmentReason/getDept").then(function (res) {
                $scope.sel['Dept'] = res.data.table;
            });

            $scope.save = function (s) {
                $scope.hide_save = true;
                $scope.hide_saves = true;
                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {
                    debugger;
                    for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                        if ($scope.rows[i].subItems[j].isChecked) {
                            var ob = {};
                            ob.od_order_qty = $scope.rows[i].subItems[j].dd_qty;
                            ob.final_qnt = $scope.rows[i].subItems[j].final_qnt;
                            ob.im_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.od_line_no = $scope.rows[i].subItems[j].dd_line_no;
                            ob.ord_no = $scope.rows[i].subItems[j].doc_prov_no;
                            ob.req_type = $scope.rows[i].subItems[j].req_type;
                            ob.req_no = $scope.rows[i].subItems[j].req_no;
                            ob.loguser = $rootScope.globals.currentUser.username;
                            ob.sims_workflow_no = $scope.rows[i].subItems[j].sims_workflow_no;
                            ob.reqcreatedby = $scope.rows[i].subItems[j].reqcreatedby;
                            //obj.dep_code = $scope.rows[i].subItems[j].dep_code;
                            //ob.adj_status = $scope.rows[i].subItems[j].adj_status;
                            ob.val = r;
                            ar.push(ob);
                        }
                    }
                }
                if (ar.length == 0) {
                    alert("Nothing Selected");
                    return;
                }
                
                $http.post(ENV.apiUrl + "api/ItemRequests/UpdateRequestDetailsforapprove", ar).then(function (res) {
                    console.log(res);
                    debugger;
                    swal({ title: "Alert", text: "Request Approved Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.Reset();
                            $scope.SelectIetm();
                        }
                    });
                    //var d = res.data;
                    //if (d!='' && d !=null) {
                    //    //swal({ title: "Alert", text: d + " Record Inserted Success.", imageUrl: "assets/img/notification-alert.png", });
                    //    swal({
                    //        title: '',
                    //        text: "Request Approved Successfully.Do you want print receipt ?",
                    //        showCloseButton: true,
                    //        showCancelButton: true,
                    //        confirmButtonText: 'Yes',
                    //        width: 380,
                    //        cancelButtonText: 'No',
                    //        allowOutsideClick: false,
                    //    }).then(function (isConfirm) {
                    //        if (isConfirm) {
                    //            //  $scope.OkRejectadm();
                    //            if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                    //                //var data = {
                    //                //    location: 'Invs.INVR02DPSMIS',
                    //                //    parameter: { doc_prov_no: d },
                    //                //    state: 'main.Inv035'
                    //                //}

                    //                var data = {
                    //                    doc_prov_no: d,
                    //                    sdate: '',
                    //                    edate: '',
                    //                    state: false,
                    //                    doc_status: '',
                    //                    back_state: 'main.ApIREQ'
                    //                }

                    //            }

                    //            else if ($http.defaults.headers.common['schoolId'] == 'brs') {
                    //                var data = {
                    //                    location: 'Invs.INVR02BRS',
                    //                    parameter: { doc_prov_no: d },
                    //                    state: 'main.ApIREQ'
                    //                }
                    //            }

                    //            else if ($http.defaults.headers.common['schoolId'] == 'sisqatar') {
                    //                var data = {
                    //                    location: 'Invs.INVR02SISQATAR',
                    //                    parameter: { doc_prov_no: d },
                    //                    state: 'main.ApIREQ'
                    //                }
                    //            }

                    //            else if ($http.defaults.headers.common['schoolId'] == 'tsis') {
                    //                var data = {
                    //                    location: 'Invs.INVR02TSIS',
                    //                    parameter: { doc_prov_no: d },
                    //                    state: 'main.ApIREQ'
                    //                }
                    //            }
                    //            else {
                    //                var data = {
                    //                    location: 'Invs.INVR02',
                    //                    parameter: { doc_prov_no: d },
                    //                    state: 'main.ApIREQ'
                    //                }
                    //            }
                    //            //$scope.disable_btn = false;
                    //            if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {

                    //                window.localStorage["ReportProvNumber"] = JSON.stringify(data)
                    //                $state.go('main.rpts')
                    //            }
                    //            else {

                    //                window.localStorage["ReportDetails"] = JSON.stringify(data)
                    //                $state.go('main.ReportCardParameter')
                    //            }
                    //        }
                    //        else {
                    //            $scope.Reset();
                    //            $state.reload();
                    //        }
                    //    });
                    //    $scope.Reset();
                    //}
                    //else {
                    //    swal({ title: "Alert", text: "Record Not Inserted .", imageUrl: "assets/img/notification-alert.png", });
                    //}

                });
            }

            $scope.reject = function (s) {
                $scope.hide_save = true;
                $scope.hide_saves = true;
                var ar = [];
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {
                    debugger;
                    for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                        if ($scope.rows[i].subItems[j].isChecked) {
                            var ob = {};
                            ob.od_order_qty = $scope.rows[i].subItems[j].dd_qty;
                            ob.final_qnt = $scope.rows[i].subItems[j].final_qnt;
                            ob.im_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.od_line_no = $scope.rows[i].subItems[j].dd_line_no;
                            ob.ord_no = $scope.rows[i].subItems[j].doc_prov_no;
                            ob.req_type = $scope.rows[i].subItems[j].req_type;
                            ob.req_no = $scope.rows[i].subItems[j].req_no;
                            ob.loguser = $rootScope.globals.currentUser.username;
                            ob.sims_workflow_no = $scope.rows[i].subItems[j].sims_workflow_no;
                            ob.reqcreatedby = $scope.rows[i].subItems[j].reqcreatedby;
                            //obj.dep_code = $scope.rows[i].subItems[j].dep_code;
                            //ob.adj_status = $scope.rows[i].subItems[j].adj_status;

                            ob.val = r;
                            ar.push(ob);
                        }
                    }
                }
                if (ar.length == 0) {
                    alert("Nothing Selected");
                    return;
                }

                $http.post(ENV.apiUrl + "api/ItemRequests/UpdateRequestDetailsforreject", ar).then(function (res) {
                    console.log(res);

                    //swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });
                    //$scope.SelectIetm();

                    swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        if (isConfirm) {
                            $scope.Reset();
                            $scope.SelectIetm();
                        }
                    });

                });

            }

            $scope.Issueitems = function (s) {
                $scope.hide_save = true;
                $scope.hide_saves = true;
                var ar = [];
                $scope.flag = true;
                var r = Math.random() * 10;
                for (var i = 0; i < $scope.rows.length; i++) {
                    debugger;
                    for (var j = 0; j < $scope.rows[i].subItems.length; j++) {
                        if ($scope.rows[i].subItems[j].isChecked) {
                            var ob = {};
                            ob.od_order_qty = $scope.rows[i].subItems[j].dd_qty;
                            ob.final_qnt = $scope.rows[i].subItems[j].final_qnt;
                            ob.im_inv_no = $scope.rows[i].subItems[j].im_inv_no;
                            ob.od_line_no = $scope.rows[i].subItems[j].dd_line_no;
                            ob.ord_no = $scope.rows[i].subItems[j].doc_prov_no;
                            ob.req_type = $scope.rows[i].subItems[j].req_type;
                            ob.req_no = $scope.rows[i].subItems[j].req_no;
                            ob.loguser = $rootScope.globals.currentUser.username;
                            ob.sims_workflow_no = $scope.rows[i].subItems[j].sims_workflow_no;
                            ob.reqcreatedby = $scope.rows[i].subItems[j].reqcreatedby;
                            //ob.adj_status = $scope.rows[i].subItems[j].adj_status;
                            ob.val = r;

                            if ($scope.rows[i].subItems[j].rd_current_qty <= 0)
                                $scope.flag = false;
                            ar.push(ob);
                        }
                    }
                }
                if (ar.length == 0) {
                    alert("Nothing Selected");
                    return;
                }
                if ($scope.flag) {
                    $http.post(ENV.apiUrl + "api/ItemRequests/UpdateRequestDetailstoissue", ar).then(function (res) {
                        console.log(res.data);
                        var d = res.data;
                       // d = d.substring(1);
                        debugger;
                        if (res.data != null && res.data != '') {

                            swal({
                                title: '',
                                text: "Sales Receipts " + res.data.substring(1) + "  issued successfully for selected requests.Do you want to print receipt?",
                                showCloseButton: true,
                                showCancelButton: true,
                                confirmButtonText: 'Yes',
                                width: 380,
                                cancelButtonText: 'No',
                                allowOutsideClick: false,
                            }).then(function (isConfirm) {
                                if (isConfirm) {
                                    var data = {
                                        location: 'Invs.INVR41DPSD',
                                        parameter: {
                                            doc_prov_no: d,
                                        },
                                        state: 'main.ApIREQ',
                                        ready: function () {
                                            this.refreshReport();
                                        },
                                    }
                                    console.log(data);

                                    window.localStorage["ReportDetails"] = JSON.stringify(data);
                                    $state.go('main.ReportCardParameter');
                                }
                            });
                        }
                        //swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, });
                        //$scope.SelectIetm();

                        //swal({ title: "Alert", text: res.data, showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        //swal({ title: "Alert", text: "Request Approved Successfully", showCloseButton: true, width: 380, }).then(function (isConfirm) {
                        //    if (isConfirm) {

                        //        if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {
                        //            //var data = {
                        //            //    location: 'Invs.INVR02DPSMIS',
                        //            //    parameter: { doc_prov_no: d },
                        //            //    state: 'main.Inv035'
                        //            //}

                        //            var data = {
                        //                doc_prov_no: res.data,
                        //                sdate: '',
                        //                edate: '',
                        //                state: false,
                        //                doc_status: '',
                        //                back_state: 'main.Inv035'
                        //            }

                        //        }

                        //        else if ($http.defaults.headers.common['schoolId'] == 'brs') {
                        //            var data = {
                        //                location: 'Invs.INVR02BRS',
                        //                parameter: { doc_prov_no: res.data },
                        //                state: 'main.Inv035'
                        //            }
                        //        }
                        //        else {
                        //            var data = {
                        //                location: 'Invs.INVR02',
                        //                parameter: { doc_prov_no: res.data },
                        //                state: 'main.Inv035'
                        //            }
                        //        }
                        //        //$scope.disable_btn = false;
                        //        if ($http.defaults.headers.common['schoolId'] == 'dpsmis') {

                        //            window.localStorage["ReportProvNumber"] = JSON.stringify(data)
                        //            $state.go('main.rpts')
                        //        }
                        //        else {

                        //            window.localStorage["ReportDetails"] = JSON.stringify(data)
                        //            $state.go('main.ReportCardParameter')
                        //        }

                        //        
                        //    }
                        //});
                        $scope.Reset();
                    });
                }
                else {
                    swal({ title: "Alert", text: "One of the Selected Item Current Quantity is 0.Cannot Issue Item.", showCloseButton: true, width: 380, });
                }
            }

            $scope.Reset = function () {
                $scope.IP = {
                    rd_from_required: '',
                    rd_up_required: '',
                    req_no: '',
                    req_range_no: '',
                    request_mode_code: '',
                    rd_item_desc: '',
                    loginuser: '',

                }
                $scope.req_stat = '01';
                $scope.remarks = "";
                //$scope.SelectIetm();
            }

            $(document).on('touch click', '*[data-datepicker="true"] .add-on', function (e) {
                $('input[type="text"]', $(this).parent()).focus();
            })

            $('*[data-datepicker="true"] input[type="text"]').datepicker({
                todayBtn: true,
                orientation: "top left",
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy-mm-dd'
            });

        }])

})();
